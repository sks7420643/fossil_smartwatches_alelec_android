package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.blesdk.obfuscated.q62;
import com.fossil.wearables.fossil.R;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w43 extends zr2 implements q62.b {
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public static /* final */ a p; // = new a((fd4) null);
    @DexIgnore
    public tr3<ga2> j;
    @DexIgnore
    public CommuteTimeSettingsDetailViewModel k;
    @DexIgnore
    public j42 l;
    @DexIgnore
    public q62 m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return w43.o;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final w43 a(AddressWrapper addressWrapper, ArrayList<String> arrayList) {
            w43 w43 = new w43();
            Bundle bundle = new Bundle();
            bundle.putParcelable("KEY_BUNDLE_SETTING_ADDRESS", addressWrapper);
            bundle.putStringArrayList("KEY_BUNDLE_LIST_ADDRESS", arrayList);
            w43.setArguments(bundle);
            return w43;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ ga2 f;

        @DexIgnore
        public b(View view, ga2 ga2) {
            this.e = view;
            this.f = ga2;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.e.getWindowVisibleDisplayFrame(rect);
            int height = this.e.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.f.v.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                ga2 ga2 = this.f;
                kd4.a((Object) ga2, "binding");
                ga2.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a = w43.p.a();
                local.d(a, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = this.f.q;
                kd4.a((Object) appCompatAutoCompleteTextView, "binding.autocompletePlaces");
                appCompatAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ w43 e;

        @DexIgnore
        public c(w43 w43, ga2 ga2) {
            this.e = w43;
        }

        @DexIgnore
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            q62 a = this.e.m;
            if (a != null) {
                AutocompletePrediction item = a.getItem(i);
                if (item != null) {
                    SpannableString fullText = item.getFullText((CharacterStyle) null);
                    kd4.a((Object) fullText, "item.getFullText(null)");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = w43.p.a();
                    local.i(a2, "Autocomplete item selected: " + fullText);
                    CommuteTimeSettingsDetailViewModel b = w43.b(this.e);
                    String spannableString = fullText.toString();
                    kd4.a((Object) spannableString, "primaryText.toString()");
                    if (spannableString != null) {
                        String obj = StringsKt__StringsKt.d(spannableString).toString();
                        String placeId = item.getPlaceId();
                        q62 a3 = this.e.m;
                        if (a3 != null) {
                            b.a(obj, placeId, a3.b());
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ga2 e;

        @DexIgnore
        public d(w43 w43, ga2 ga2) {
            this.e = ga2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.e.r;
            kd4.a((Object) imageView, "binding.closeIv");
            imageView.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ w43 e;

        @DexIgnore
        public e(w43 w43, ga2 ga2) {
            this.e = w43;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            kd4.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.e.S0();
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ w43 e;

        @DexIgnore
        public f(w43 w43) {
            this.e = w43;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            CommuteTimeSettingsDetailViewModel b = w43.b(this.e);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                String obj = StringsKt__StringsKt.d(valueOf).toString();
                if (obj != null) {
                    String upperCase = obj.toUpperCase();
                    kd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                    b.b(upperCase);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ w43 e;

        @DexIgnore
        public g(w43 w43) {
            this.e = w43;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.r(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ w43 e;

        @DexIgnore
        public h(w43 w43) {
            this.e = w43;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (w43.b(this.e).e()) {
                ds3 ds3 = ds3.c;
                FragmentManager childFragmentManager = this.e.getChildFragmentManager();
                kd4.a((Object) childFragmentManager, "childFragmentManager");
                ds3.p(childFragmentManager);
            } else if (w43.b(this.e).f()) {
                this.e.r(true);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ w43 e;
        @DexIgnore
        public /* final */ /* synthetic */ ga2 f;

        @DexIgnore
        public i(w43 w43, ga2 ga2) {
            this.e = w43;
            this.f = ga2;
        }

        @DexIgnore
        public final void onClick(View view) {
            CommuteTimeSettingsDetailViewModel.a(w43.b(this.e), (String) null, (String) null, (AutocompleteSessionToken) null, 7, (Object) null);
            this.f.q.setText("");
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ w43 a;

        @DexIgnore
        public j(w43 w43) {
            this.a = w43;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            w43.b(this.a).a(z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements cc<CommuteTimeSettingsDetailViewModel.b> {
        @DexIgnore
        public /* final */ /* synthetic */ w43 a;
        @DexIgnore
        public /* final */ /* synthetic */ ga2 b;

        @DexIgnore
        public k(w43 w43, ga2 ga2) {
            this.a = w43;
            this.b = ga2;
        }

        @DexIgnore
        public final void a(CommuteTimeSettingsDetailViewModel.b bVar) {
            int i;
            PortfolioApp portfolioApp;
            PlacesClient d = bVar.d();
            if (d != null) {
                this.a.a(d);
            }
            String c = bVar.c();
            if (c != null) {
                this.b.s.setText(c);
            }
            String a2 = bVar.a();
            if (a2 != null) {
                this.b.q.setText(a2);
            }
            Boolean b2 = bVar.b();
            if (b2 != null) {
                boolean booleanValue = b2.booleanValue();
                SwitchCompat switchCompat = this.b.w;
                kd4.a((Object) switchCompat, "binding.scAvoidTolls");
                switchCompat.setChecked(booleanValue);
            }
            Boolean f = bVar.f();
            if (f != null) {
                boolean booleanValue2 = f.booleanValue();
                FlexibleEditText flexibleEditText = this.b.s;
                kd4.a((Object) flexibleEditText, "binding.fetAddressName");
                flexibleEditText.setEnabled(booleanValue2);
                if (booleanValue2) {
                    this.b.s.requestFocus();
                }
            }
            Boolean g = bVar.g();
            if (g != null) {
                boolean booleanValue3 = g.booleanValue();
                FlexibleTextView flexibleTextView = this.b.x;
                kd4.a((Object) flexibleTextView, "binding.tvRight");
                flexibleTextView.setEnabled(booleanValue3);
                FlexibleTextView flexibleTextView2 = this.b.x;
                if (booleanValue3) {
                    portfolioApp = PortfolioApp.W.c();
                    i = R.color.primaryColor;
                } else {
                    portfolioApp = PortfolioApp.W.c();
                    i = R.color.disabled;
                }
                flexibleTextView2.setTextColor(k6.a((Context) portfolioApp, i));
            }
            Boolean e = bVar.e();
            if (e == null) {
                return;
            }
            if (e.booleanValue()) {
                this.a.b();
            } else {
                this.a.a();
            }
        }
    }

    /*
    static {
        String simpleName = w43.class.getSimpleName();
        kd4.a((Object) simpleName, "CommuteTimeSettingsDetai\u2026nt::class.java.simpleName");
        o = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ CommuteTimeSettingsDetailViewModel b(w43 w43) {
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = w43.k;
        if (commuteTimeSettingsDetailViewModel != null) {
            return commuteTimeSettingsDetailViewModel;
        }
        kd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void M(boolean z) {
        tr3<ga2> tr3 = this.j;
        if (tr3 != null) {
            ga2 a2 = tr3.a();
            if (a2 != null) {
                if (z) {
                    AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                    kd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(appCompatAutoCompleteTextView.getText())) {
                        U0();
                        return;
                    }
                }
                T0();
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void T0() {
        tr3<ga2> tr3 = this.j;
        if (tr3 != null) {
            ga2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                kd4.a((Object) flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void U0() {
        tr3<ga2> tr3 = this.j;
        if (tr3 != null) {
            ga2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                kd4.a((Object) flexibleTextView, "it.ftvAddressError");
                PortfolioApp c2 = PortfolioApp.W.c();
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                kd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(c2.getString(R.string.Customization_Buttons_CommuteTimeDestinationSettings_Text__NothingFoundForInputaddress, new Object[]{appCompatAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.t;
                kd4.a((Object) flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.W.c().g().a(new c53()).a(this);
        j42 j42 = this.l;
        ArrayList<String> arrayList = null;
        if (j42 != null) {
            ic a2 = lc.a((Fragment) this, (kc.b) j42).a(CommuteTimeSettingsDetailViewModel.class);
            kd4.a((Object) a2, "ViewModelProviders.of(th\u2026ailViewModel::class.java)");
            this.k = (CommuteTimeSettingsDetailViewModel) a2;
            CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.k;
            if (commuteTimeSettingsDetailViewModel != null) {
                Bundle arguments = getArguments();
                AddressWrapper addressWrapper = arguments != null ? (AddressWrapper) arguments.getParcelable("KEY_BUNDLE_SETTING_ADDRESS") : null;
                Bundle arguments2 = getArguments();
                if (arguments2 != null) {
                    arrayList = arguments2.getStringArrayList("KEY_BUNDLE_LIST_ADDRESS");
                }
                commuteTimeSettingsDetailViewModel.a(addressWrapper, arrayList);
                return;
            }
            kd4.d("mViewModel");
            throw null;
        }
        kd4.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        ga2 ga2 = (ga2) qa.a(layoutInflater, R.layout.fragment_commute_time_settings_detail, viewGroup, false, O0());
        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = ga2.q;
        appCompatAutoCompleteTextView.setDropDownBackgroundDrawable(k6.c(appCompatAutoCompleteTextView.getContext(), R.drawable.autocomplete_dropdown));
        appCompatAutoCompleteTextView.setOnItemClickListener(new c(this, ga2));
        appCompatAutoCompleteTextView.addTextChangedListener(new d(this, ga2));
        appCompatAutoCompleteTextView.setOnKeyListener(new e(this, ga2));
        ga2.s.addTextChangedListener(new f(this));
        ga2.u.setOnClickListener(new g(this));
        ga2.x.setOnClickListener(new h(this));
        ga2.r.setOnClickListener(new i(this, ga2));
        ga2.w.setOnCheckedChangeListener(new j(this));
        kd4.a((Object) ga2, "binding");
        View d2 = ga2.d();
        kd4.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, ga2));
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.k;
        if (commuteTimeSettingsDetailViewModel != null) {
            commuteTimeSettingsDetailViewModel.d().a(getViewLifecycleOwner(), new k(this, ga2));
            this.j = new tr3<>(this, ga2);
            return ga2.d();
        }
        kd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.k;
        if (commuteTimeSettingsDetailViewModel != null) {
            commuteTimeSettingsDetailViewModel.h();
            super.onPause();
            return;
        }
        kd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.k;
        if (commuteTimeSettingsDetailViewModel != null) {
            commuteTimeSettingsDetailViewModel.g();
        } else {
            kd4.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void r(boolean z) {
        if (z) {
            CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.k;
            if (commuteTimeSettingsDetailViewModel != null) {
                AddressWrapper c2 = commuteTimeSettingsDetailViewModel.c();
                if (c2 != null) {
                    Intent intent = new Intent();
                    intent.putExtra("KEY_SELECTED_ADDRESS", c2);
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.setResult(-1, intent);
                    }
                }
            } else {
                kd4.d("mViewModel");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public final void a(PlacesClient placesClient) {
        if (isActive()) {
            Context context = getContext();
            if (context != null) {
                kd4.a((Object) context, "context!!");
                this.m = new q62(context, placesClient);
                q62 q62 = this.m;
                if (q62 != null) {
                    q62.a((q62.b) this);
                }
                tr3<ga2> tr3 = this.j;
                if (tr3 != null) {
                    ga2 a2 = tr3.a();
                    if (a2 != null) {
                        a2.q.setAdapter(this.m);
                        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                        kd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                        Editable text = appCompatAutoCompleteTextView.getText();
                        kd4.a((Object) text, "it.autocompletePlaces.text");
                        CharSequence d2 = StringsKt__StringsKt.d(text);
                        if (d2.length() > 0) {
                            a2.q.setText(d2);
                            a2.q.setSelection(d2.length());
                            return;
                        }
                        T0();
                        return;
                    }
                    return;
                }
                kd4.d("mBinding");
                throw null;
            }
            kd4.a();
            throw null;
        }
    }
}
