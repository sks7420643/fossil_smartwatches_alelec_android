package com.fossil.blesdk.obfuscated;

import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class fm1 {
    @DexIgnore
    public static volatile Handler d;
    @DexIgnore
    public /* final */ vi1 a;
    @DexIgnore
    public /* final */ Runnable b;
    @DexIgnore
    public volatile long c;

    @DexIgnore
    public fm1(vi1 vi1) {
        bk0.a(vi1);
        this.a = vi1;
        this.b = new gm1(this, vi1);
    }

    @DexIgnore
    public final void a(long j) {
        a();
        if (j >= 0) {
            this.c = this.a.c().b();
            if (!b().postDelayed(this.b, j)) {
                this.a.d().s().a("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }

    @DexIgnore
    public final Handler b() {
        Handler handler;
        if (d != null) {
            return d;
        }
        synchronized (fm1.class) {
            if (d == null) {
                d = new m51(this.a.getContext().getMainLooper());
            }
            handler = d;
        }
        return handler;
    }

    @DexIgnore
    public abstract void c();

    @DexIgnore
    public final boolean d() {
        return this.c != 0;
    }

    @DexIgnore
    public final void a() {
        this.c = 0;
        b().removeCallbacks(this.b);
    }
}
