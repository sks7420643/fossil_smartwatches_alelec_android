package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ij0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sg1 extends ij0<kg1> {
    @DexIgnore
    public sg1(Context context, Looper looper, ij0.a aVar, ij0.b bVar) {
        super(context, looper, 93, aVar, bVar, (String) null);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
        if (queryLocalInterface instanceof kg1) {
            return (kg1) queryLocalInterface;
        }
        return new mg1(iBinder);
    }

    @DexIgnore
    public final int i() {
        return zd0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.measurement.internal.IMeasurementService";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.measurement.START";
    }
}
