package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gi0 implements ge0.b, ge0.c {
    @DexIgnore
    public /* final */ de0<?> e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public hi0 g;

    @DexIgnore
    public gi0(de0<?> de0, boolean z) {
        this.e = de0;
        this.f = z;
    }

    @DexIgnore
    public final void a(ud0 ud0) {
        a();
        this.g.a(ud0, this.e, this.f);
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        a();
        this.g.e(bundle);
    }

    @DexIgnore
    public final void f(int i) {
        a();
        this.g.f(i);
    }

    @DexIgnore
    public final void a(hi0 hi0) {
        this.g = hi0;
    }

    @DexIgnore
    public final void a() {
        bk0.a(this.g, (Object) "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
    }
}
