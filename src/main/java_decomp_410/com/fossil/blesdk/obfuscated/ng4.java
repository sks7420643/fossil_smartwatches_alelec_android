package com.fossil.blesdk.obfuscated;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ng4 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater b; // = AtomicIntegerFieldUpdater.newUpdater(ng4.class, "_handled");
    @DexIgnore
    public volatile int _handled;
    @DexIgnore
    public /* final */ Throwable a;

    @DexIgnore
    public ng4(Throwable th, boolean z) {
        kd4.b(th, "cause");
        this.a = th;
        this._handled = z ? 1 : 0;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [int, boolean] */
    public final boolean a() {
        return this._handled;
    }

    @DexIgnore
    public final boolean b() {
        return b.compareAndSet(this, 0, 1);
    }

    @DexIgnore
    public String toString() {
        return dh4.a((Object) this) + '[' + this.a + ']';
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ng4(Throwable th, boolean z, int i, fd4 fd4) {
        this(th, (i & 2) != 0 ? false : z);
    }
}
