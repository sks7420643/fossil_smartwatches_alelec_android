package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gv2 {
    @DexIgnore
    public /* final */ v73 a;
    @DexIgnore
    public /* final */ d23 b;
    @DexIgnore
    public /* final */ z53 c;
    @DexIgnore
    public /* final */ og3 d;
    @DexIgnore
    public /* final */ sv2 e;
    @DexIgnore
    public /* final */ yy2 f;
    @DexIgnore
    public /* final */ gg3 g;

    @DexIgnore
    public gv2(v73 v73, d23 d23, z53 z53, og3 og3, sv2 sv2, yy2 yy2, gg3 gg3) {
        kd4.b(v73, "mDashboardView");
        kd4.b(d23, "mDianaCustomizeView");
        kd4.b(z53, "mHybridCustomizeView");
        kd4.b(og3, "mProfileView");
        kd4.b(sv2, "mAlertsView");
        kd4.b(yy2, "mAlertsHybridView");
        kd4.b(gg3, "mUpdateFirmwareView");
        this.a = v73;
        this.b = d23;
        this.c = z53;
        this.d = og3;
        this.e = sv2;
        this.f = yy2;
        this.g = gg3;
    }

    @DexIgnore
    public final yy2 a() {
        return this.f;
    }

    @DexIgnore
    public final sv2 b() {
        return this.e;
    }

    @DexIgnore
    public final v73 c() {
        return this.a;
    }

    @DexIgnore
    public final d23 d() {
        return this.b;
    }

    @DexIgnore
    public final z53 e() {
        return this.c;
    }

    @DexIgnore
    public final og3 f() {
        return this.d;
    }

    @DexIgnore
    public final gg3 g() {
        return this.g;
    }
}
