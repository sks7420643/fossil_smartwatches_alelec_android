package com.fossil.blesdk.obfuscated;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kz0 extends WeakReference<Throwable> {
    @DexIgnore
    public /* final */ int a;

    @DexIgnore
    public kz0(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.a = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == kz0.class) {
            if (this == obj) {
                return true;
            }
            kz0 kz0 = (kz0) obj;
            return this.a == kz0.a && get() == kz0.get();
        }
    }

    @DexIgnore
    public final int hashCode() {
        return this.a;
    }
}
