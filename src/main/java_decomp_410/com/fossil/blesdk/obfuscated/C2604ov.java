package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ov */
public interface C2604ov {
    @DexIgnore
    /* renamed from: a */
    boolean mo4033a(com.fossil.blesdk.obfuscated.C2604ov ovVar);

    @DexIgnore
    /* renamed from: c */
    void mo4035c();

    @DexIgnore
    void clear();

    @DexIgnore
    /* renamed from: d */
    void mo4037d();

    @DexIgnore
    /* renamed from: e */
    boolean mo4038e();

    @DexIgnore
    /* renamed from: f */
    boolean mo4039f();

    @DexIgnore
    boolean isRunning();
}
