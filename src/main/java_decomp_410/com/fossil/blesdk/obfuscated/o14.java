package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class o14 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ int e;
    @DexIgnore
    public /* final */ /* synthetic */ g14 f;

    @DexIgnore
    public o14(g14 g14, int i) {
        this.f = g14;
        this.e = i;
    }

    @DexIgnore
    public void run() {
        this.f.b(this.e, true);
        this.f.b(this.e, false);
    }
}
