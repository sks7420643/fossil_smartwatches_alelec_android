package com.fossil.blesdk.obfuscated;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sj4 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater e;
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater f;
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater g;
    @DexIgnore
    public volatile Object _next; // = this;
    @DexIgnore
    public volatile Object _prev; // = this;
    @DexIgnore
    public volatile Object _removedRef; // = null;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends mj4<sj4> {
        @DexIgnore
        public sj4 b;
        @DexIgnore
        public /* final */ sj4 c;

        @DexIgnore
        public a(sj4 sj4) {
            kd4.b(sj4, "newNode");
            this.c = sj4;
        }

        @DexIgnore
        public void a(sj4 sj4, Object obj) {
            kd4.b(sj4, "affected");
            boolean z = obj == null;
            sj4 sj42 = z ? this.c : this.b;
            if (sj42 != null && sj4.e.compareAndSet(sj4, this, sj42) && z) {
                sj4 sj43 = this.c;
                sj4 sj44 = this.b;
                if (sj44 != null) {
                    sj43.b(sj44);
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }
    }

    /*
    static {
        Class<Object> cls = Object.class;
        Class<sj4> cls2 = sj4.class;
        e = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_next");
        f = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_prev");
        g = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_removedRef");
    }
    */

    @DexIgnore
    public final sj4 b() {
        sj4 sj4 = this;
        while (!(sj4 instanceof qj4)) {
            sj4 = sj4.d();
            if (ch4.a()) {
                if (!(sj4 != this)) {
                    throw new AssertionError();
                }
            }
        }
        return sj4;
    }

    @DexIgnore
    public final void c(sj4 sj4) {
        g();
        sj4.a(rj4.a(this._prev), (yj4) null);
    }

    @DexIgnore
    public final sj4 d() {
        return rj4.a(c());
    }

    @DexIgnore
    public final Object e() {
        while (true) {
            Object obj = this._prev;
            if (obj instanceof zj4) {
                return obj;
            }
            if (obj != null) {
                sj4 sj4 = (sj4) obj;
                if (sj4.c() == this) {
                    return obj;
                }
                a(sj4, (yj4) null);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        }
    }

    @DexIgnore
    public final sj4 f() {
        return rj4.a(e());
    }

    @DexIgnore
    public final void g() {
        Object c;
        sj4 i = i();
        Object obj = this._next;
        if (obj != null) {
            sj4 sj4 = ((zj4) obj).a;
            while (true) {
                sj4 sj42 = null;
                while (true) {
                    Object c2 = sj4.c();
                    if (c2 instanceof zj4) {
                        sj4.i();
                        sj4 = ((zj4) c2).a;
                    } else {
                        c = i.c();
                        if (c instanceof zj4) {
                            if (sj42 != null) {
                                break;
                            }
                            i = rj4.a(i._prev);
                        } else if (c != this) {
                            if (c != null) {
                                sj4 sj43 = (sj4) c;
                                if (sj43 != sj4) {
                                    sj4 sj44 = sj43;
                                    sj42 = i;
                                    i = sj44;
                                } else {
                                    return;
                                }
                            } else {
                                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                            }
                        } else if (e.compareAndSet(i, this, sj4)) {
                            return;
                        }
                    }
                }
                i.i();
                e.compareAndSet(sj42, i, ((zj4) c).a);
                i = sj42;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Removed");
        }
    }

    @DexIgnore
    public final boolean h() {
        return c() instanceof zj4;
    }

    @DexIgnore
    public final sj4 i() {
        Object obj;
        sj4 sj4;
        do {
            obj = this._prev;
            if (obj instanceof zj4) {
                return ((zj4) obj).a;
            }
            if (obj == this) {
                sj4 = b();
            } else if (obj != null) {
                sj4 = (sj4) obj;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        } while (!f.compareAndSet(this, obj, sj4.l()));
        return (sj4) obj;
    }

    @DexIgnore
    public boolean j() {
        Object c;
        sj4 sj4;
        do {
            c = c();
            if ((c instanceof zj4) || c == this) {
                return false;
            }
            if (c != null) {
                sj4 = (sj4) c;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        } while (!e.compareAndSet(this, c, sj4.l()));
        c(sj4);
        return true;
    }

    @DexIgnore
    public final sj4 k() {
        while (true) {
            Object c = c();
            if (c != null) {
                sj4 sj4 = (sj4) c;
                if (sj4 == this) {
                    return null;
                }
                if (sj4.j()) {
                    return sj4;
                }
                sj4.g();
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        }
    }

    @DexIgnore
    public final zj4 l() {
        zj4 zj4 = (zj4) this._removedRef;
        if (zj4 != null) {
            return zj4;
        }
        zj4 zj42 = new zj4(this);
        g.lazySet(this, zj42);
        return zj42;
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + '@' + Integer.toHexString(System.identityHashCode(this));
    }

    @DexIgnore
    public final boolean a(sj4 sj4) {
        kd4.b(sj4, "node");
        f.lazySet(sj4, this);
        e.lazySet(sj4, this);
        while (c() == this) {
            if (e.compareAndSet(this, this, sj4)) {
                sj4.b(this);
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final Object c() {
        while (true) {
            Object obj = this._next;
            if (!(obj instanceof yj4)) {
                return obj;
            }
            ((yj4) obj).a(this);
        }
    }

    @DexIgnore
    public final void b(sj4 sj4) {
        Object obj;
        do {
            obj = sj4._prev;
            if ((obj instanceof zj4) || c() != sj4) {
                return;
            }
        } while (!f.compareAndSet(sj4, obj, this));
        if (!(c() instanceof zj4)) {
            return;
        }
        if (obj != null) {
            sj4.a((sj4) obj, (yj4) null);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public final int a(sj4 sj4, sj4 sj42, a aVar) {
        kd4.b(sj4, "node");
        kd4.b(sj42, "next");
        kd4.b(aVar, "condAdd");
        f.lazySet(sj4, this);
        e.lazySet(sj4, sj42);
        aVar.b = sj42;
        if (!e.compareAndSet(this, sj42, aVar)) {
            return 0;
        }
        return aVar.a(this) == null ? 1 : 2;
    }

    @DexIgnore
    public final sj4 a(sj4 sj4, yj4 yj4) {
        Object obj;
        while (true) {
            sj4 sj42 = null;
            while (true) {
                obj = sj4._next;
                if (obj == yj4) {
                    return sj4;
                }
                if (obj instanceof yj4) {
                    ((yj4) obj).a(sj4);
                } else if (!(obj instanceof zj4)) {
                    Object obj2 = this._prev;
                    if (obj2 instanceof zj4) {
                        return null;
                    }
                    if (obj != this) {
                        if (obj != null) {
                            sj42 = sj4;
                            sj4 = (sj4) obj;
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                        }
                    } else if (obj2 == sj4) {
                        return null;
                    } else {
                        if (f.compareAndSet(this, obj2, sj4) && !(sj4._prev instanceof zj4)) {
                            return null;
                        }
                    }
                } else if (sj42 != null) {
                    break;
                } else {
                    sj4 = rj4.a(sj4._prev);
                }
            }
            sj4.i();
            e.compareAndSet(sj42, sj4, ((zj4) obj).a);
            sj4 = sj42;
        }
    }
}
