package com.fossil.blesdk.obfuscated;

import android.net.Uri;
import com.fossil.blesdk.obfuscated.de0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class im1 {
    @DexIgnore
    public static /* final */ de0.g<ic1> a; // = new de0.g<>();
    @DexIgnore
    public static /* final */ de0.a<ic1, Object> b; // = new om1();

    /*
    static {
        new de0("Phenotype.API", b, a);
        new hc1();
    }
    */

    @DexIgnore
    public static Uri a(String str) {
        String valueOf = String.valueOf(Uri.encode(str));
        return Uri.parse(valueOf.length() != 0 ? "content://com.google.android.gms.phenotype/".concat(valueOf) : new String("content://com.google.android.gms.phenotype/"));
    }
}
