package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.Utility;
import com.facebook.places.model.PlaceFields;
import com.fossil.blesdk.device.data.file.FileType;
import com.misfit.frameworks.common.constants.Constants;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kn4 {
    @DexIgnore
    public static /* final */ jn4[] a; // = {new jn4(jn4.i, ""), new jn4(jn4.f, "GET"), new jn4(jn4.f, "POST"), new jn4(jn4.g, (String) ZendeskConfig.SLASH), new jn4(jn4.g, "/index.html"), new jn4(jn4.h, "http"), new jn4(jn4.h, (String) Utility.URL_SCHEME), new jn4(jn4.e, "200"), new jn4(jn4.e, "204"), new jn4(jn4.e, "206"), new jn4(jn4.e, "304"), new jn4(jn4.e, "400"), new jn4(jn4.e, "404"), new jn4(jn4.e, "500"), new jn4("accept-charset", ""), new jn4("accept-encoding", "gzip, deflate"), new jn4("accept-language", ""), new jn4("accept-ranges", ""), new jn4("accept", ""), new jn4("access-control-allow-origin", ""), new jn4("age", ""), new jn4("allow", ""), new jn4((String) Constants.IF_AUTHORIZATION, ""), new jn4("cache-control", ""), new jn4("content-disposition", ""), new jn4("content-encoding", ""), new jn4("content-language", ""), new jn4("content-length", ""), new jn4("content-location", ""), new jn4("content-range", ""), new jn4("content-type", ""), new jn4("cookie", ""), new jn4("date", ""), new jn4((String) Constants.JSON_KEY_ETAG, ""), new jn4("expect", ""), new jn4("expires", ""), new jn4("from", ""), new jn4("host", ""), new jn4("if-match", ""), new jn4("if-modified-since", ""), new jn4("if-none-match", ""), new jn4("if-range", ""), new jn4("if-unmodified-since", ""), new jn4("last-modified", ""), new jn4("link", ""), new jn4((String) PlaceFields.LOCATION, ""), new jn4("max-forwards", ""), new jn4("proxy-authenticate", ""), new jn4("proxy-authorization", ""), new jn4("range", ""), new jn4("referer", ""), new jn4("refresh", ""), new jn4("retry-after", ""), new jn4("server", ""), new jn4("set-cookie", ""), new jn4("strict-transport-security", ""), new jn4("transfer-encoding", ""), new jn4("user-agent", ""), new jn4("vary", ""), new jn4("via", ""), new jn4("www-authenticate", "")};
    @DexIgnore
    public static /* final */ Map<ByteString, Integer> b; // = a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ List<jn4> a;
        @DexIgnore
        public /* final */ lo4 b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public jn4[] e;
        @DexIgnore
        public int f;
        @DexIgnore
        public int g;
        @DexIgnore
        public int h;

        @DexIgnore
        public a(int i, yo4 yo4) {
            this(i, i, yo4);
        }

        @DexIgnore
        public final void a() {
            int i = this.d;
            int i2 = this.h;
            if (i >= i2) {
                return;
            }
            if (i == 0) {
                b();
            } else {
                b(i2 - i);
            }
        }

        @DexIgnore
        public final void b() {
            Arrays.fill(this.e, (Object) null);
            this.f = this.e.length - 1;
            this.g = 0;
            this.h = 0;
        }

        @DexIgnore
        public List<jn4> c() {
            ArrayList arrayList = new ArrayList(this.a);
            this.a.clear();
            return arrayList;
        }

        @DexIgnore
        public final boolean d(int i) {
            return i >= 0 && i <= kn4.a.length - 1;
        }

        @DexIgnore
        public final void e(int i) throws IOException {
            if (d(i)) {
                this.a.add(kn4.a[i]);
                return;
            }
            int a2 = a(i - kn4.a.length);
            if (a2 >= 0) {
                jn4[] jn4Arr = this.e;
                if (a2 < jn4Arr.length) {
                    this.a.add(jn4Arr[a2]);
                    return;
                }
            }
            throw new IOException("Header index too large " + (i + 1));
        }

        @DexIgnore
        public void f() throws IOException {
            while (!this.b.g()) {
                byte readByte = this.b.readByte() & FileType.MASKED_INDEX;
                if (readByte == 128) {
                    throw new IOException("index == 0");
                } else if ((readByte & 128) == 128) {
                    e(a((int) readByte, 127) - 1);
                } else if (readByte == 64) {
                    g();
                } else if ((readByte & 64) == 64) {
                    f(a((int) readByte, 63) - 1);
                } else if ((readByte & 32) == 32) {
                    this.d = a((int) readByte, 31);
                    int i = this.d;
                    if (i < 0 || i > this.c) {
                        throw new IOException("Invalid dynamic table size update " + this.d);
                    }
                    a();
                } else if (readByte == 16 || readByte == 0) {
                    h();
                } else {
                    g(a((int) readByte, 15) - 1);
                }
            }
        }

        @DexIgnore
        public final void g(int i) throws IOException {
            this.a.add(new jn4(c(i), e()));
        }

        @DexIgnore
        public final void h() throws IOException {
            ByteString e2 = e();
            kn4.a(e2);
            this.a.add(new jn4(e2, e()));
        }

        @DexIgnore
        public a(int i, int i2, yo4 yo4) {
            this.a = new ArrayList();
            this.e = new jn4[8];
            this.f = this.e.length - 1;
            this.g = 0;
            this.h = 0;
            this.c = i;
            this.d = i2;
            this.b = so4.a(yo4);
        }

        @DexIgnore
        public final int d() throws IOException {
            return this.b.readByte() & FileType.MASKED_INDEX;
        }

        @DexIgnore
        public final ByteString c(int i) throws IOException {
            if (d(i)) {
                return kn4.a[i].a;
            }
            int a2 = a(i - kn4.a.length);
            if (a2 >= 0) {
                jn4[] jn4Arr = this.e;
                if (a2 < jn4Arr.length) {
                    return jn4Arr[a2].a;
                }
            }
            throw new IOException("Header index too large " + (i + 1));
        }

        @DexIgnore
        public final int a(int i) {
            return this.f + 1 + i;
        }

        @DexIgnore
        public final void g() throws IOException {
            ByteString e2 = e();
            kn4.a(e2);
            a(-1, new jn4(e2, e()));
        }

        @DexIgnore
        public final void a(int i, jn4 jn4) {
            this.a.add(jn4);
            int i2 = jn4.c;
            if (i != -1) {
                i2 -= this.e[a(i)].c;
            }
            int i3 = this.d;
            if (i2 > i3) {
                b();
                return;
            }
            int b2 = b((this.h + i2) - i3);
            if (i == -1) {
                int i4 = this.g + 1;
                jn4[] jn4Arr = this.e;
                if (i4 > jn4Arr.length) {
                    jn4[] jn4Arr2 = new jn4[(jn4Arr.length * 2)];
                    System.arraycopy(jn4Arr, 0, jn4Arr2, jn4Arr.length, jn4Arr.length);
                    this.f = this.e.length - 1;
                    this.e = jn4Arr2;
                }
                int i5 = this.f;
                this.f = i5 - 1;
                this.e[i5] = jn4;
                this.g++;
            } else {
                this.e[i + a(i) + b2] = jn4;
            }
            this.h += i2;
        }

        @DexIgnore
        public final int b(int i) {
            int i2 = 0;
            if (i > 0) {
                int length = this.e.length;
                while (true) {
                    length--;
                    if (length < this.f || i <= 0) {
                        jn4[] jn4Arr = this.e;
                        int i3 = this.f;
                        System.arraycopy(jn4Arr, i3 + 1, jn4Arr, i3 + 1 + i2, this.g);
                        this.f += i2;
                    } else {
                        jn4[] jn4Arr2 = this.e;
                        i -= jn4Arr2[length].c;
                        this.h -= jn4Arr2[length].c;
                        this.g--;
                        i2++;
                    }
                }
                jn4[] jn4Arr3 = this.e;
                int i32 = this.f;
                System.arraycopy(jn4Arr3, i32 + 1, jn4Arr3, i32 + 1 + i2, this.g);
                this.f += i2;
            }
            return i2;
        }

        @DexIgnore
        public ByteString e() throws IOException {
            int d2 = d();
            boolean z = (d2 & 128) == 128;
            int a2 = a(d2, 127);
            if (z) {
                return ByteString.of(rn4.b().a(this.b.f((long) a2)));
            }
            return this.b.d((long) a2);
        }

        @DexIgnore
        public final void f(int i) throws IOException {
            a(-1, new jn4(c(i), e()));
        }

        @DexIgnore
        public int a(int i, int i2) throws IOException {
            int i3 = i & i2;
            if (i3 < i2) {
                return i3;
            }
            int i4 = 0;
            while (true) {
                int d2 = d();
                if ((d2 & 128) == 0) {
                    return i2 + (d2 << i4);
                }
                i2 += (d2 & 127) << i4;
                i4 += 7;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ jo4 a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public int e;
        @DexIgnore
        public jn4[] f;
        @DexIgnore
        public int g;
        @DexIgnore
        public int h;
        @DexIgnore
        public int i;

        @DexIgnore
        public b(jo4 jo4) {
            this(4096, true, jo4);
        }

        @DexIgnore
        public final int a(int i2) {
            int i3 = 0;
            if (i2 > 0) {
                int length = this.f.length;
                while (true) {
                    length--;
                    if (length < this.g || i2 <= 0) {
                        jn4[] jn4Arr = this.f;
                        int i4 = this.g;
                        System.arraycopy(jn4Arr, i4 + 1, jn4Arr, i4 + 1 + i3, this.h);
                        jn4[] jn4Arr2 = this.f;
                        int i5 = this.g;
                        Arrays.fill(jn4Arr2, i5 + 1, i5 + 1 + i3, (Object) null);
                        this.g += i3;
                    } else {
                        jn4[] jn4Arr3 = this.f;
                        i2 -= jn4Arr3[length].c;
                        this.i -= jn4Arr3[length].c;
                        this.h--;
                        i3++;
                    }
                }
                jn4[] jn4Arr4 = this.f;
                int i42 = this.g;
                System.arraycopy(jn4Arr4, i42 + 1, jn4Arr4, i42 + 1 + i3, this.h);
                jn4[] jn4Arr22 = this.f;
                int i52 = this.g;
                Arrays.fill(jn4Arr22, i52 + 1, i52 + 1 + i3, (Object) null);
                this.g += i3;
            }
            return i3;
        }

        @DexIgnore
        public final void b() {
            Arrays.fill(this.f, (Object) null);
            this.g = this.f.length - 1;
            this.h = 0;
            this.i = 0;
        }

        @DexIgnore
        public b(int i2, boolean z, jo4 jo4) {
            this.c = Integer.MAX_VALUE;
            this.f = new jn4[8];
            this.g = this.f.length - 1;
            this.h = 0;
            this.i = 0;
            this.e = i2;
            this.b = z;
            this.a = jo4;
        }

        @DexIgnore
        public void b(int i2) {
            int min = Math.min(i2, RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE);
            int i3 = this.e;
            if (i3 != min) {
                if (min < i3) {
                    this.c = Math.min(this.c, min);
                }
                this.d = true;
                this.e = min;
                a();
            }
        }

        @DexIgnore
        public final void a(jn4 jn4) {
            int i2 = jn4.c;
            int i3 = this.e;
            if (i2 > i3) {
                b();
                return;
            }
            a((this.i + i2) - i3);
            int i4 = this.h + 1;
            jn4[] jn4Arr = this.f;
            if (i4 > jn4Arr.length) {
                jn4[] jn4Arr2 = new jn4[(jn4Arr.length * 2)];
                System.arraycopy(jn4Arr, 0, jn4Arr2, jn4Arr.length, jn4Arr.length);
                this.g = this.f.length - 1;
                this.f = jn4Arr2;
            }
            int i5 = this.g;
            this.g = i5 - 1;
            this.f[i5] = jn4;
            this.h++;
            this.i += i2;
        }

        @DexIgnore
        public void a(List<jn4> list) throws IOException {
            int i2;
            int i3;
            if (this.d) {
                int i4 = this.c;
                if (i4 < this.e) {
                    a(i4, 31, 32);
                }
                this.d = false;
                this.c = Integer.MAX_VALUE;
                a(this.e, 31, 32);
            }
            int size = list.size();
            for (int i5 = 0; i5 < size; i5++) {
                jn4 jn4 = list.get(i5);
                ByteString asciiLowercase = jn4.a.toAsciiLowercase();
                ByteString byteString = jn4.b;
                Integer num = kn4.b.get(asciiLowercase);
                if (num != null) {
                    i3 = num.intValue() + 1;
                    if (i3 > 1 && i3 < 8) {
                        if (jm4.a((Object) kn4.a[i3 - 1].b, (Object) byteString)) {
                            i2 = i3;
                        } else if (jm4.a((Object) kn4.a[i3].b, (Object) byteString)) {
                            i2 = i3;
                            i3++;
                        }
                    }
                    i2 = i3;
                    i3 = -1;
                } else {
                    i3 = -1;
                    i2 = -1;
                }
                if (i3 == -1) {
                    int i6 = this.g + 1;
                    int length = this.f.length;
                    while (true) {
                        if (i6 >= length) {
                            break;
                        }
                        if (jm4.a((Object) this.f[i6].a, (Object) asciiLowercase)) {
                            if (jm4.a((Object) this.f[i6].b, (Object) byteString)) {
                                i3 = kn4.a.length + (i6 - this.g);
                                break;
                            } else if (i2 == -1) {
                                i2 = (i6 - this.g) + kn4.a.length;
                            }
                        }
                        i6++;
                    }
                }
                if (i3 != -1) {
                    a(i3, 127, 128);
                } else if (i2 == -1) {
                    this.a.writeByte(64);
                    a(asciiLowercase);
                    a(byteString);
                    a(jn4);
                } else if (!asciiLowercase.startsWith(jn4.d) || jn4.i.equals(asciiLowercase)) {
                    a(i2, 63, 64);
                    a(byteString);
                    a(jn4);
                } else {
                    a(i2, 15, 0);
                    a(byteString);
                }
            }
        }

        @DexIgnore
        public void a(int i2, int i3, int i4) {
            if (i2 < i3) {
                this.a.writeByte(i2 | i4);
                return;
            }
            this.a.writeByte(i4 | i3);
            int i5 = i2 - i3;
            while (i5 >= 128) {
                this.a.writeByte(128 | (i5 & 127));
                i5 >>>= 7;
            }
            this.a.writeByte(i5);
        }

        @DexIgnore
        public void a(ByteString byteString) throws IOException {
            if (!this.b || rn4.b().a(byteString) >= byteString.size()) {
                a(byteString.size(), 127, 0);
                this.a.a(byteString);
                return;
            }
            jo4 jo4 = new jo4();
            rn4.b().a(byteString, jo4);
            ByteString y = jo4.y();
            a(y.size(), 127, 128);
            this.a.a(y);
        }

        @DexIgnore
        public final void a() {
            int i2 = this.e;
            int i3 = this.i;
            if (i2 >= i3) {
                return;
            }
            if (i2 == 0) {
                b();
            } else {
                a(i3 - i2);
            }
        }
    }

    @DexIgnore
    public static Map<ByteString, Integer> a() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(a.length);
        int i = 0;
        while (true) {
            jn4[] jn4Arr = a;
            if (i >= jn4Arr.length) {
                return Collections.unmodifiableMap(linkedHashMap);
            }
            if (!linkedHashMap.containsKey(jn4Arr[i].a)) {
                linkedHashMap.put(a[i].a, Integer.valueOf(i));
            }
            i++;
        }
    }

    @DexIgnore
    public static ByteString a(ByteString byteString) throws IOException {
        int size = byteString.size();
        int i = 0;
        while (i < size) {
            byte b2 = byteString.getByte(i);
            if (b2 < 65 || b2 > 90) {
                i++;
            } else {
                throw new IOException("PROTOCOL_ERROR response malformed: mixed case name: " + byteString.utf8());
            }
        }
        return byteString;
    }
}
