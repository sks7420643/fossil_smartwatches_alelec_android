package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.jp1;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jq1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ DataHolder e;
    @DexIgnore
    public /* final */ /* synthetic */ jp1.d f;

    @DexIgnore
    public jq1(jp1.d dVar, DataHolder dataHolder) {
        this.f = dVar;
        this.e = dataHolder;
    }

    @DexIgnore
    public final void run() {
        dp1 dp1 = new dp1(this.e);
        try {
            jp1.this.a(dp1);
        } finally {
            dp1.a();
        }
    }
}
