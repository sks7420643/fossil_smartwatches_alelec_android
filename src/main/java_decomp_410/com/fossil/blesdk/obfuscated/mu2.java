package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mu2 implements Factory<ConnectedAppsPresenter> {
    @DexIgnore
    public static ConnectedAppsPresenter a(ju2 ju2, UserRepository userRepository, PortfolioApp portfolioApp, zk2 zk2) {
        return new ConnectedAppsPresenter(ju2, userRepository, portfolioApp, zk2);
    }
}
