package com.fossil.blesdk.obfuscated;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class eq3 extends aq3 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ long u; // = TimeUnit.SECONDS.toMillis(15);
    @DexIgnore
    public static /* final */ long v; // = TimeUnit.SECONDS.toMillis(1);
    @DexIgnore
    public static /* final */ a w; // = new a((fd4) null);
    @DexIgnore
    public boolean f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h; // = 3;
    @DexIgnore
    public /* final */ d i; // = new d(this, u, v);
    @DexIgnore
    public /* final */ Handler j; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public boolean k;
    @DexIgnore
    public /* final */ Runnable l; // = new e(this);
    @DexIgnore
    public /* final */ f m; // = new f(this);
    @DexIgnore
    public /* final */ c n; // = new c(this);
    @DexIgnore
    public /* final */ b o; // = new b(this);
    @DexIgnore
    public /* final */ PortfolioApp p;
    @DexIgnore
    public /* final */ bq3 q;
    @DexIgnore
    public /* final */ rc r;
    @DexIgnore
    public /* final */ ql2 s;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return eq3.t;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ eq3 a;

        @DexIgnore
        public b(eq3 eq3) {
            this.a = eq3;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            kd4.b(context, "context");
            kd4.b(intent, "intent");
            if (kd4.a((Object) "android.bluetooth.adapter.action.STATE_CHANGED", (Object) intent.getAction())) {
                int intExtra = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = eq3.w.a();
                local.d(a2, "mBluetoothStateChangeReceiver - onReceive() - state = " + intExtra);
                if (intExtra == 10) {
                    FLogger.INSTANCE.getLocal().d(eq3.w.a(), "Bluetooth off");
                    this.a.g();
                    this.a.q.a();
                    this.a.q.B();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ eq3 a;

        @DexIgnore
        public c(eq3 eq3) {
            this.a = eq3;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            kd4.b(context, "context");
            kd4.b(intent, "intent");
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = eq3.w.a();
            local.d(a2, "mConnectionStateChangeReceiver onReceive: status = " + intExtra);
            if (intExtra == ConnectionStateChange.GATT_OFF.ordinal()) {
                BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
                kd4.a((Object) defaultAdapter, "BluetoothAdapter.getDefaultAdapter()");
                if (!defaultAdapter.isEnabled()) {
                    this.a.q.a();
                    this.a.q.B();
                    return;
                }
                this.a.g();
                this.a.d(false);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends CountDownTimer {
        @DexIgnore
        public /* final */ /* synthetic */ eq3 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(eq3 eq3, long j, long j2) {
            super(j, j2);
            this.a = eq3;
        }

        @DexIgnore
        public void onFinish() {
            FLogger.INSTANCE.getLocal().d(eq3.w.a(), "CountDownTimer onFinish");
            this.a.s.a(true, this.a.p.e(), 0, CalibrationEnums.HandId.HOUR);
            cancel();
            start();
        }

        @DexIgnore
        public void onTick(long j) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ eq3 e;

        @DexIgnore
        public e(eq3 eq3) {
            this.e = eq3;
        }

        @DexIgnore
        public final void run() {
            if (this.e.k) {
                this.e.q.a();
                this.e.o();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ eq3 a;

        @DexIgnore
        public f(eq3 eq3) {
            this.a = eq3;
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            kd4.b(communicateMode, "communicateMode");
            kd4.b(intent, "intent");
            boolean z = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal();
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = eq3.w.a();
            local.d(a2, "receiver mode=" + communicateMode + ", isSuccess=" + z);
            int i = fq3.a[communicateMode.ordinal()];
            if (i == 1) {
                this.a.f(z);
            } else if (i == 2) {
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                this.a.a(z, integerArrayListExtra, intExtra);
            } else if (i == 3) {
                this.a.a(z, intExtra);
            } else if (i == 4) {
                this.a.e(z);
            } else if (i == 5) {
                this.a.d(z);
            }
        }
    }

    /*
    static {
        String simpleName = eq3.class.getSimpleName();
        kd4.a((Object) simpleName, "CalibrationPresenter::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public eq3(PortfolioApp portfolioApp, bq3 bq3, rc rcVar, ql2 ql2) {
        kd4.b(portfolioApp, "mApp");
        kd4.b(bq3, "mView");
        kd4.b(rcVar, "mLocalBroadcastManager");
        kd4.b(ql2, "mWatchHelper");
        this.p = portfolioApp;
        this.q = bq3;
        this.r = rcVar;
        this.s = ql2;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(t, "start()");
        r();
        m();
        p();
        BleCommandResultManager.d.a(CommunicateMode.ENTER_CALIBRATION);
        this.h = DeviceHelper.o.a(this.p.e()) == MFDeviceFamily.DEVICE_FAMILY_SAM ? 3 : 2;
        this.q.a(this.g, this.h);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(t, "stop");
        if (this.g != 3) {
            this.s.a(this.p.e());
        }
        r();
        this.i.cancel();
        q();
    }

    @DexIgnore
    public void h() {
        FLogger.INSTANCE.getLocal().d(t, "back");
        int i2 = this.g;
        if (i2 == 0) {
            this.q.n();
        } else if (i2 == 1) {
            this.g = 0;
            this.q.k(this.p.e());
        } else if (i2 == 2) {
            this.g = 1;
            this.q.o(this.p.e());
        } else if (i2 == 3) {
            if (DeviceHelper.o.a(this.p.e()) == MFDeviceFamily.DEVICE_FAMILY_SAM) {
                this.g = 2;
                this.q.j(this.p.e());
            } else {
                this.g = 1;
                this.q.o(this.p.e());
            }
        }
        this.q.a(this.g, this.h);
    }

    @DexIgnore
    public List<DeviceHelper.ImageStyle> i() {
        ArrayList arrayList = new ArrayList();
        MFDeviceFamily a2 = DeviceHelper.o.a(this.p.e());
        if (a2 == MFDeviceFamily.DEVICE_FAMILY_SAM || a2 == MFDeviceFamily.DEVICE_FAMILY_SAM_SLIM || a2 == MFDeviceFamily.DEVICE_FAMILY_SAM_MINI) {
            arrayList.add(DeviceHelper.ImageStyle.HYBRID_WATCH_HOUR);
            arrayList.add(DeviceHelper.ImageStyle.HYBRID_WATCH_MINUTE);
            arrayList.add(DeviceHelper.ImageStyle.HYBRID_WATCH_SUBEYE);
        } else {
            arrayList.add(DeviceHelper.ImageStyle.DIANA_WATCH_HOUR);
            arrayList.add(DeviceHelper.ImageStyle.DIANA_WATCH_MINUTE);
        }
        return arrayList;
    }

    @DexIgnore
    public void j() {
        FLogger.INSTANCE.getLocal().d(t, "next");
        int i2 = this.g;
        if (i2 == 0) {
            a("device_calibrate", "Step", AppFilter.COLUMN_HOUR);
            this.g = 1;
            this.q.o(this.p.e());
        } else if (i2 == 1) {
            a("device_calibrate", "Step", MFSleepGoal.COLUMN_MINUTE);
            if (DeviceHelper.o.a(this.p.e()) == MFDeviceFamily.DEVICE_FAMILY_SAM) {
                this.g = 2;
                this.q.j(this.p.e());
            } else {
                this.g = 3;
                l();
            }
        } else if (i2 == 2) {
            a("device_calibrate", "Step", "subeye");
            this.g = 3;
            l();
        } else if (i2 == 3) {
            l();
        }
        this.q.a(this.g, this.h);
    }

    @DexIgnore
    public void k() {
        FLogger.INSTANCE.getLocal().d(t, "stopSmartMove");
        this.s.a();
    }

    @DexIgnore
    public final void l() {
        FLogger.INSTANCE.getLocal().d(t, "completeCalibration");
        this.i.cancel();
        this.q.b();
        this.s.b(this.p.e());
        this.g = 4;
    }

    @DexIgnore
    public final void m() {
        FLogger.INSTANCE.getLocal().d(t, "registerBroadcastReceiver()");
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.m, CommunicateMode.ENTER_CALIBRATION, CommunicateMode.RESET_HAND, CommunicateMode.MOVE_HAND, CommunicateMode.APPLY_HAND_POSITION, CommunicateMode.EXIT_CALIBRATION);
        rc rcVar = this.r;
        c cVar = this.n;
        rcVar.a(cVar, new IntentFilter(this.p.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        this.p.registerReceiver(this.o, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
    }

    @DexIgnore
    public void n() {
        this.q.a(this);
    }

    @DexIgnore
    public final void o() {
        if (!this.f) {
            this.q.o();
        }
    }

    @DexIgnore
    public final void p() {
        FLogger.INSTANCE.getLocal().d(t, "startCalibration");
        int i2 = this.g;
        if (i2 == 0) {
            this.q.k(this.p.e());
        } else if (i2 == 1) {
            this.q.o(this.p.e());
        } else if (i2 == 2) {
            this.q.j(this.p.e());
        }
        if (!this.f) {
            this.q.b();
            FLogger.INSTANCE.getLocal().e(t, "mStartCalibration");
            this.s.c(this.p.e());
            a(30);
        }
    }

    @DexIgnore
    public final void q() {
        FLogger.INSTANCE.getLocal().d(t, "stopSetConfigTimeOutTimer");
        this.k = false;
        this.j.removeCallbacks(this.l);
    }

    @DexIgnore
    public final void r() {
        FLogger.INSTANCE.getLocal().d(t, "unregisterBroadcastReceiver()");
        try {
            BleCommandResultManager.d.b((BleCommandResultManager.b) this.m, CommunicateMode.ENTER_CALIBRATION, CommunicateMode.RESET_HAND, CommunicateMode.MOVE_HAND, CommunicateMode.APPLY_HAND_POSITION, CommunicateMode.EXIT_CALIBRATION);
            this.r.a((BroadcastReceiver) this.n);
            this.p.unregisterReceiver(this.o);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = t;
            local.d(str, "unregisterBroadcastReceiver() - ex = " + e2);
        }
    }

    @DexIgnore
    public final void a(boolean z, ArrayList<Integer> arrayList, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.e(str, "onEnterCalibrationComplete isSuccess=" + z + ", lastErrorCode=" + i2);
        if (z) {
            this.s.d(this.p.e());
            return;
        }
        this.q.a();
        if (i2 != 1101 && i2 != 1112 && i2 != 1113) {
            o();
        } else if (arrayList != null) {
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(arrayList);
            kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026onErrorCode(errorCodes!!)");
            bq3 bq3 = this.q;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                bq3.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void b(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "startMove: clockwise = " + z);
        int i2 = FossilDeviceSerialPatternUtil.isDianaDevice(this.p.e()) ? 1 : 2;
        int i3 = this.g;
        if (i3 == 0) {
            this.s.a(z, this.p.e(), i2, CalibrationEnums.HandId.HOUR);
        } else if (i3 == 1) {
            this.s.a(z, this.p.e(), i2, CalibrationEnums.HandId.MINUTE);
        } else if (i3 == 2) {
            this.s.a(z, this.p.e(), i2, CalibrationEnums.HandId.SUB_EYE);
        }
    }

    @DexIgnore
    public void c(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "startSmartMove: clockwise = " + z + ", mCalibrationStep = " + this.g);
        int i2 = this.g;
        if (i2 == 0) {
            this.s.b(z, this.p.e(), 2, CalibrationEnums.HandId.HOUR);
        } else if (i2 == 1) {
            this.s.b(z, this.p.e(), 2, CalibrationEnums.HandId.MINUTE);
        } else if (i2 == 2) {
            this.s.b(z, this.p.e(), 2, CalibrationEnums.HandId.SUB_EYE);
        }
    }

    @DexIgnore
    public final void d(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onExitCalibrationComplete success=" + z);
        this.q.a();
        if (z) {
            this.q.n();
        } else {
            o();
        }
    }

    @DexIgnore
    public final void e(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onMoveHandComplete isSuccess=" + z);
        if (!z) {
            this.s.a(this.p.e());
            o();
        }
    }

    @DexIgnore
    public final void f(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.e(str, "onResetHandComplete isSuccess=" + z);
        this.q.a();
        if (z) {
            q();
            this.i.start();
            return;
        }
        o();
    }

    @DexIgnore
    public final void a(boolean z, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onApplyHandsComplete isSuccess=" + z);
        if (z) {
            a("device_calibrate_result", "errorCode", "N/A");
            return;
        }
        a("device_calibrate_result", "errorCode", String.valueOf(i2));
        this.q.a();
        o();
    }

    @DexIgnore
    public final void a(int i2) {
        q();
        this.k = true;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "startSetConfigTimeOutTimer:  timeout = " + i2);
        this.j.postDelayed(this.l, ((long) i2) * 1000);
    }

    @DexIgnore
    public void a(boolean z) {
        this.f = z;
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        AnalyticsHelper.f.c().b(str, str2, str3);
    }
}
