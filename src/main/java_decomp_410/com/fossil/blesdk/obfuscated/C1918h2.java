package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.h2 */
public class C1918h2 {

    @DexIgnore
    /* renamed from: k */
    public static /* final */ android.graphics.RectF f5638k; // = new android.graphics.RectF();

    @DexIgnore
    /* renamed from: l */
    public static java.util.concurrent.ConcurrentHashMap<java.lang.String, java.lang.reflect.Method> f5639l; // = new java.util.concurrent.ConcurrentHashMap<>();

    @DexIgnore
    /* renamed from: a */
    public int f5640a; // = 0;

    @DexIgnore
    /* renamed from: b */
    public boolean f5641b; // = false;

    @DexIgnore
    /* renamed from: c */
    public float f5642c; // = -1.0f;

    @DexIgnore
    /* renamed from: d */
    public float f5643d; // = -1.0f;

    @DexIgnore
    /* renamed from: e */
    public float f5644e; // = -1.0f;

    @DexIgnore
    /* renamed from: f */
    public int[] f5645f; // = new int[0];

    @DexIgnore
    /* renamed from: g */
    public boolean f5646g; // = false;

    @DexIgnore
    /* renamed from: h */
    public android.text.TextPaint f5647h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ android.widget.TextView f5648i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ android.content.Context f5649j;

    @DexIgnore
    public C1918h2(android.widget.TextView textView) {
        this.f5648i = textView;
        this.f5649j = this.f5648i.getContext();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11546a(android.util.AttributeSet attributeSet, int i) {
        android.content.res.TypedArray obtainStyledAttributes = this.f5649j.obtainStyledAttributes(attributeSet, com.fossil.blesdk.obfuscated.C1368a0.AppCompatTextView, i, 0);
        if (obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTextView_autoSizeTextType)) {
            this.f5640a = obtainStyledAttributes.getInt(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTextView_autoSizeTextType, 0);
        }
        float dimension = obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTextView_autoSizeStepGranularity) ? obtainStyledAttributes.getDimension(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTextView_autoSizeStepGranularity, -1.0f) : -1.0f;
        float dimension2 = obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTextView_autoSizeMinTextSize) ? obtainStyledAttributes.getDimension(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTextView_autoSizeMinTextSize, -1.0f) : -1.0f;
        float dimension3 = obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTextView_autoSizeMaxTextSize) ? obtainStyledAttributes.getDimension(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTextView_autoSizeMaxTextSize, -1.0f) : -1.0f;
        if (obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTextView_autoSizePresetSizes)) {
            int resourceId = obtainStyledAttributes.getResourceId(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTextView_autoSizePresetSizes, 0);
            if (resourceId > 0) {
                android.content.res.TypedArray obtainTypedArray = obtainStyledAttributes.getResources().obtainTypedArray(resourceId);
                mo11545a(obtainTypedArray);
                obtainTypedArray.recycle();
            }
        }
        obtainStyledAttributes.recycle();
        if (!mo11559k()) {
            this.f5640a = 0;
        } else if (this.f5640a == 1) {
            if (!this.f5646g) {
                android.util.DisplayMetrics displayMetrics = this.f5649j.getResources().getDisplayMetrics();
                if (dimension2 == -1.0f) {
                    dimension2 = android.util.TypedValue.applyDimension(2, 12.0f, displayMetrics);
                }
                if (dimension3 == -1.0f) {
                    dimension3 = android.util.TypedValue.applyDimension(2, 112.0f, displayMetrics);
                }
                if (dimension == -1.0f) {
                    dimension = 1.0f;
                }
                mo11541a(dimension2, dimension3, dimension);
            }
            mo11557i();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo11550b() {
        this.f5640a = 0;
        this.f5643d = -1.0f;
        this.f5644e = -1.0f;
        this.f5642c = -1.0f;
        this.f5645f = new int[0];
        this.f5641b = false;
    }

    @DexIgnore
    /* renamed from: c */
    public int mo11551c() {
        return java.lang.Math.round(this.f5644e);
    }

    @DexIgnore
    /* renamed from: d */
    public int mo11552d() {
        return java.lang.Math.round(this.f5643d);
    }

    @DexIgnore
    /* renamed from: e */
    public int mo11553e() {
        return java.lang.Math.round(this.f5642c);
    }

    @DexIgnore
    /* renamed from: f */
    public int[] mo11554f() {
        return this.f5645f;
    }

    @DexIgnore
    /* renamed from: g */
    public int mo11555g() {
        return this.f5640a;
    }

    @DexIgnore
    /* renamed from: h */
    public boolean mo11556h() {
        return mo11559k() && this.f5640a != 0;
    }

    @DexIgnore
    /* renamed from: i */
    public final boolean mo11557i() {
        if (!mo11559k() || this.f5640a != 1) {
            this.f5641b = false;
        } else {
            if (!this.f5646g || this.f5645f.length == 0) {
                float round = (float) java.lang.Math.round(this.f5643d);
                int i = 1;
                while (java.lang.Math.round(this.f5642c + round) <= java.lang.Math.round(this.f5644e)) {
                    i++;
                    round += this.f5642c;
                }
                int[] iArr = new int[i];
                float f = this.f5643d;
                for (int i2 = 0; i2 < i; i2++) {
                    iArr[i2] = java.lang.Math.round(f);
                    f += this.f5642c;
                }
                this.f5645f = mo11549a(iArr);
            }
            this.f5641b = true;
        }
        return this.f5641b;
    }

    @DexIgnore
    /* renamed from: j */
    public final boolean mo11558j() {
        int length = this.f5645f.length;
        this.f5646g = length > 0;
        if (this.f5646g) {
            this.f5640a = 1;
            int[] iArr = this.f5645f;
            this.f5643d = (float) iArr[0];
            this.f5644e = (float) iArr[length - 1];
            this.f5642c = -1.0f;
        }
        return this.f5646g;
    }

    @DexIgnore
    /* renamed from: k */
    public final boolean mo11559k() {
        return !(this.f5648i instanceof androidx.appcompat.widget.AppCompatEditText);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11542a(int i) {
        if (!mo11559k()) {
            return;
        }
        if (i == 0) {
            mo11550b();
        } else if (i == 1) {
            android.util.DisplayMetrics displayMetrics = this.f5649j.getResources().getDisplayMetrics();
            mo11541a(android.util.TypedValue.applyDimension(2, 12.0f, displayMetrics), android.util.TypedValue.applyDimension(2, 112.0f, displayMetrics), 1.0f);
            if (mo11557i()) {
                mo11539a();
            }
        } else {
            throw new java.lang.IllegalArgumentException("Unknown auto-size text type: " + i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11544a(int i, int i2, int i3, int i4) throws java.lang.IllegalArgumentException {
        if (mo11559k()) {
            android.util.DisplayMetrics displayMetrics = this.f5649j.getResources().getDisplayMetrics();
            mo11541a(android.util.TypedValue.applyDimension(i4, (float) i, displayMetrics), android.util.TypedValue.applyDimension(i4, (float) i2, displayMetrics), android.util.TypedValue.applyDimension(i4, (float) i3, displayMetrics));
            if (mo11557i()) {
                mo11539a();
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11547a(int[] iArr, int i) throws java.lang.IllegalArgumentException {
        if (mo11559k()) {
            int length = iArr.length;
            if (length > 0) {
                int[] iArr2 = new int[length];
                if (i == 0) {
                    iArr2 = java.util.Arrays.copyOf(iArr, length);
                } else {
                    android.util.DisplayMetrics displayMetrics = this.f5649j.getResources().getDisplayMetrics();
                    for (int i2 = 0; i2 < length; i2++) {
                        iArr2[i2] = java.lang.Math.round(android.util.TypedValue.applyDimension(i, (float) iArr[i2], displayMetrics));
                    }
                }
                this.f5645f = mo11549a(iArr2);
                if (!mo11558j()) {
                    throw new java.lang.IllegalArgumentException("None of the preset sizes is valid: " + java.util.Arrays.toString(iArr));
                }
            } else {
                this.f5646g = false;
            }
            if (mo11557i()) {
                mo11539a();
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11545a(android.content.res.TypedArray typedArray) {
        int length = typedArray.length();
        int[] iArr = new int[length];
        if (length > 0) {
            for (int i = 0; i < length; i++) {
                iArr[i] = typedArray.getDimensionPixelSize(i, -1);
            }
            this.f5645f = mo11549a(iArr);
            mo11558j();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final int[] mo11549a(int[] iArr) {
        if (r0 == 0) {
            return iArr;
        }
        java.util.Arrays.sort(iArr);
        java.util.ArrayList arrayList = new java.util.ArrayList();
        for (int i : iArr) {
            if (i > 0 && java.util.Collections.binarySearch(arrayList, java.lang.Integer.valueOf(i)) < 0) {
                arrayList.add(java.lang.Integer.valueOf(i));
            }
        }
        if (r0 == arrayList.size()) {
            return iArr;
        }
        int size = arrayList.size();
        int[] iArr2 = new int[size];
        for (int i2 = 0; i2 < size; i2++) {
            iArr2[i2] = ((java.lang.Integer) arrayList.get(i2)).intValue();
        }
        return iArr2;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11541a(float f, float f2, float f3) throws java.lang.IllegalArgumentException {
        if (f <= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            throw new java.lang.IllegalArgumentException("Minimum auto-size text size (" + f + "px) is less or equal to (0px)");
        } else if (f2 <= f) {
            throw new java.lang.IllegalArgumentException("Maximum auto-size text size (" + f2 + "px) is less or equal to minimum auto-size " + "text size (" + f + "px)");
        } else if (f3 > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.f5640a = 1;
            this.f5643d = f;
            this.f5644e = f2;
            this.f5642c = f3;
            this.f5646g = false;
        } else {
            throw new java.lang.IllegalArgumentException("The auto-size step granularity (" + f3 + "px) is less or equal to (0px)");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11539a() {
        int i;
        if (mo11556h()) {
            if (this.f5641b) {
                if (this.f5648i.getMeasuredHeight() > 0 && this.f5648i.getMeasuredWidth() > 0) {
                    if (((java.lang.Boolean) mo11537a((java.lang.Object) this.f5648i, "getHorizontallyScrolling", false)).booleanValue()) {
                        i = 1048576;
                    } else {
                        i = (this.f5648i.getMeasuredWidth() - this.f5648i.getTotalPaddingLeft()) - this.f5648i.getTotalPaddingRight();
                    }
                    int height = (this.f5648i.getHeight() - this.f5648i.getCompoundPaddingBottom()) - this.f5648i.getCompoundPaddingTop();
                    if (i > 0 && height > 0) {
                        synchronized (f5638k) {
                            f5638k.setEmpty();
                            f5638k.right = (float) i;
                            f5638k.bottom = (float) height;
                            float a = (float) mo11534a(f5638k);
                            if (a != this.f5648i.getTextSize()) {
                                mo11543a(0, a);
                            }
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
            this.f5641b = true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11543a(int i, float f) {
        android.content.res.Resources resources;
        android.content.Context context = this.f5649j;
        if (context == null) {
            resources = android.content.res.Resources.getSystem();
        } else {
            resources = context.getResources();
        }
        mo11540a(android.util.TypedValue.applyDimension(i, f, resources.getDisplayMetrics()));
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11540a(float f) {
        if (f != this.f5648i.getPaint().getTextSize()) {
            this.f5648i.getPaint().setTextSize(f);
            boolean isInLayout = android.os.Build.VERSION.SDK_INT >= 18 ? this.f5648i.isInLayout() : false;
            if (this.f5648i.getLayout() != null) {
                this.f5641b = false;
                try {
                    java.lang.reflect.Method a = mo11538a("nullLayouts");
                    if (a != null) {
                        a.invoke(this.f5648i, new java.lang.Object[0]);
                    }
                } catch (java.lang.Exception e) {
                    android.util.Log.w("ACTVAutoSizeHelper", "Failed to invoke TextView#nullLayouts() method", e);
                }
                if (!isInLayout) {
                    this.f5648i.requestLayout();
                } else {
                    this.f5648i.forceLayout();
                }
                this.f5648i.invalidate();
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo11534a(android.graphics.RectF rectF) {
        int length = this.f5645f.length;
        if (length != 0) {
            int i = length - 1;
            int i2 = 1;
            int i3 = 0;
            while (i2 <= i) {
                int i4 = (i2 + i) / 2;
                if (mo11548a(this.f5645f[i4], rectF)) {
                    int i5 = i4 + 1;
                    i3 = i2;
                    i2 = i5;
                } else {
                    i3 = i4 - 1;
                    i = i3;
                }
            }
            return this.f5645f[i3];
        }
        throw new java.lang.IllegalStateException("No available text sizes to choose from.");
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo11548a(int i, android.graphics.RectF rectF) {
        android.text.StaticLayout staticLayout;
        java.lang.CharSequence text = this.f5648i.getText();
        android.text.method.TransformationMethod transformationMethod = this.f5648i.getTransformationMethod();
        if (transformationMethod != null) {
            java.lang.CharSequence transformation = transformationMethod.getTransformation(text, this.f5648i);
            if (transformation != null) {
                text = transformation;
            }
        }
        int maxLines = android.os.Build.VERSION.SDK_INT >= 16 ? this.f5648i.getMaxLines() : -1;
        android.text.TextPaint textPaint = this.f5647h;
        if (textPaint == null) {
            this.f5647h = new android.text.TextPaint();
        } else {
            textPaint.reset();
        }
        this.f5647h.set(this.f5648i.getPaint());
        this.f5647h.setTextSize((float) i);
        android.text.Layout.Alignment alignment = (android.text.Layout.Alignment) mo11537a((java.lang.Object) this.f5648i, "getLayoutAlignment", android.text.Layout.Alignment.ALIGN_NORMAL);
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            staticLayout = mo11536a(text, alignment, java.lang.Math.round(rectF.right), maxLines);
        } else {
            staticLayout = mo11535a(text, alignment, java.lang.Math.round(rectF.right));
        }
        return (maxLines == -1 || (staticLayout.getLineCount() <= maxLines && staticLayout.getLineEnd(staticLayout.getLineCount() - 1) == text.length())) && ((float) staticLayout.getHeight()) <= rectF.bottom;
    }

    @DexIgnore
    /* renamed from: a */
    public final android.text.StaticLayout mo11536a(java.lang.CharSequence charSequence, android.text.Layout.Alignment alignment, int i, int i2) {
        android.text.TextDirectionHeuristic textDirectionHeuristic = (android.text.TextDirectionHeuristic) mo11537a((java.lang.Object) this.f5648i, "getTextDirectionHeuristic", android.text.TextDirectionHeuristics.FIRSTSTRONG_LTR);
        android.text.StaticLayout.Builder hyphenationFrequency = android.text.StaticLayout.Builder.obtain(charSequence, 0, charSequence.length(), this.f5647h, i).setAlignment(alignment).setLineSpacing(this.f5648i.getLineSpacingExtra(), this.f5648i.getLineSpacingMultiplier()).setIncludePad(this.f5648i.getIncludeFontPadding()).setBreakStrategy(this.f5648i.getBreakStrategy()).setHyphenationFrequency(this.f5648i.getHyphenationFrequency());
        if (i2 == -1) {
            i2 = Integer.MAX_VALUE;
        }
        return hyphenationFrequency.setMaxLines(i2).setTextDirection(textDirectionHeuristic).build();
    }

    @DexIgnore
    /* renamed from: a */
    public final android.text.StaticLayout mo11535a(java.lang.CharSequence charSequence, android.text.Layout.Alignment alignment, int i) {
        boolean z;
        float f;
        float f2;
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            f2 = this.f5648i.getLineSpacingMultiplier();
            f = this.f5648i.getLineSpacingExtra();
            z = this.f5648i.getIncludeFontPadding();
        } else {
            f2 = ((java.lang.Float) mo11537a((java.lang.Object) this.f5648i, "getLineSpacingMultiplier", java.lang.Float.valueOf(1.0f))).floatValue();
            f = ((java.lang.Float) mo11537a((java.lang.Object) this.f5648i, "getLineSpacingExtra", java.lang.Float.valueOf(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES))).floatValue();
            z = ((java.lang.Boolean) mo11537a((java.lang.Object) this.f5648i, "getIncludeFontPadding", true)).booleanValue();
        }
        java.lang.CharSequence charSequence2 = charSequence;
        android.text.StaticLayout staticLayout = new android.text.StaticLayout(charSequence2, this.f5647h, i, alignment, f2, f, z);
        return staticLayout;
    }

    @DexIgnore
    /* renamed from: a */
    public final <T> T mo11537a(java.lang.Object obj, java.lang.String str, T t) {
        try {
            return mo11538a(str).invoke(obj, new java.lang.Object[0]);
        } catch (java.lang.Exception e) {
            android.util.Log.w("ACTVAutoSizeHelper", "Failed to invoke TextView#" + str + "() method", e);
            return t;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.reflect.Method mo11538a(java.lang.String str) {
        try {
            java.lang.reflect.Method method = f5639l.get(str);
            if (method == null) {
                method = android.widget.TextView.class.getDeclaredMethod(str, new java.lang.Class[0]);
                if (method != null) {
                    method.setAccessible(true);
                    f5639l.put(str, method);
                }
            }
            return method;
        } catch (java.lang.Exception e) {
            android.util.Log.w("ACTVAutoSizeHelper", "Failed to retrieve TextView#" + str + "() method", e);
            return null;
        }
    }
}
