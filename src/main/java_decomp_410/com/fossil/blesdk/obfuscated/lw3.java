package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.yv3;
import com.squareup.okhttp.internal.framed.ErrorCode;
import com.squareup.okhttp.internal.framed.HeadersMode;
import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ProtocolException;
import java.util.List;
import java.util.zip.Deflater;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lw3 implements mw3 {
    @DexIgnore
    public static /* final */ byte[] a;

    /*
    static {
        try {
            a = "\u0000\u0000\u0000\u0007options\u0000\u0000\u0000\u0004head\u0000\u0000\u0000\u0004post\u0000\u0000\u0000\u0003put\u0000\u0000\u0000\u0006delete\u0000\u0000\u0000\u0005trace\u0000\u0000\u0000\u0006accept\u0000\u0000\u0000\u000eaccept-charset\u0000\u0000\u0000\u000faccept-encoding\u0000\u0000\u0000\u000faccept-language\u0000\u0000\u0000\raccept-ranges\u0000\u0000\u0000\u0003age\u0000\u0000\u0000\u0005allow\u0000\u0000\u0000\rauthorization\u0000\u0000\u0000\rcache-control\u0000\u0000\u0000\nconnection\u0000\u0000\u0000\fcontent-base\u0000\u0000\u0000\u0010content-encoding\u0000\u0000\u0000\u0010content-language\u0000\u0000\u0000\u000econtent-length\u0000\u0000\u0000\u0010content-location\u0000\u0000\u0000\u000bcontent-md5\u0000\u0000\u0000\rcontent-range\u0000\u0000\u0000\fcontent-type\u0000\u0000\u0000\u0004date\u0000\u0000\u0000\u0004etag\u0000\u0000\u0000\u0006expect\u0000\u0000\u0000\u0007expires\u0000\u0000\u0000\u0004from\u0000\u0000\u0000\u0004host\u0000\u0000\u0000\bif-match\u0000\u0000\u0000\u0011if-modified-since\u0000\u0000\u0000\rif-none-match\u0000\u0000\u0000\bif-range\u0000\u0000\u0000\u0013if-unmodified-since\u0000\u0000\u0000\rlast-modified\u0000\u0000\u0000\blocation\u0000\u0000\u0000\fmax-forwards\u0000\u0000\u0000\u0006pragma\u0000\u0000\u0000\u0012proxy-authenticate\u0000\u0000\u0000\u0013proxy-authorization\u0000\u0000\u0000\u0005range\u0000\u0000\u0000\u0007referer\u0000\u0000\u0000\u000bretry-after\u0000\u0000\u0000\u0006server\u0000\u0000\u0000\u0002te\u0000\u0000\u0000\u0007trailer\u0000\u0000\u0000\u0011transfer-encoding\u0000\u0000\u0000\u0007upgrade\u0000\u0000\u0000\nuser-agent\u0000\u0000\u0000\u0004vary\u0000\u0000\u0000\u0003via\u0000\u0000\u0000\u0007warning\u0000\u0000\u0000\u0010www-authenticate\u0000\u0000\u0000\u0006method\u0000\u0000\u0000\u0003get\u0000\u0000\u0000\u0006status\u0000\u0000\u0000\u0006200 OK\u0000\u0000\u0000\u0007version\u0000\u0000\u0000\bHTTP/1.1\u0000\u0000\u0000\u0003url\u0000\u0000\u0000\u0006public\u0000\u0000\u0000\nset-cookie\u0000\u0000\u0000\nkeep-alive\u0000\u0000\u0000\u0006origin100101201202205206300302303304305306307402405406407408409410411412413414415416417502504505203 Non-Authoritative Information204 No Content301 Moved Permanently400 Bad Request401 Unauthorized403 Forbidden404 Not Found500 Internal Server Error501 Not Implemented503 Service UnavailableJan Feb Mar Apr May Jun Jul Aug Sept Oct Nov Dec 00:00:00 Mon, Tue, Wed, Thu, Fri, Sat, Sun, GMTchunked,text/html,image/png,image/jpg,image/gif,application/xml,application/xhtml+xml,text/plain,text/javascript,publicprivatemax-age=gzip,deflate,sdchcharset=utf-8charset=iso-8859-1,utf-,*,enq=0.".getBytes(wv3.c.name());
        } catch (UnsupportedEncodingException unused) {
            throw new AssertionError();
        }
    }
    */

    @DexIgnore
    public yv3 a(lo4 lo4, boolean z) {
        return new a(lo4, z);
    }

    @DexIgnore
    public zv3 a(ko4 ko4, boolean z) {
        return new b(ko4, z);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements zv3 {
        @DexIgnore
        public /* final */ ko4 e;
        @DexIgnore
        public /* final */ jo4 f; // = new jo4();
        @DexIgnore
        public /* final */ ko4 g;
        @DexIgnore
        public /* final */ boolean h;
        @DexIgnore
        public boolean i;

        @DexIgnore
        public b(ko4 ko4, boolean z) {
            this.e = ko4;
            this.h = z;
            Deflater deflater = new Deflater();
            deflater.setDictionary(lw3.a);
            this.g = so4.a((xo4) new mo4((xo4) this.f, deflater));
        }

        @DexIgnore
        public void a(int i2, int i3, List<cw3> list) throws IOException {
        }

        @DexIgnore
        public void a(kw3 kw3) {
        }

        @DexIgnore
        public synchronized void a(boolean z, boolean z2, int i2, int i3, List<cw3> list) throws IOException {
            if (!this.i) {
                a(list);
                int B = (int) (this.f.B() + 10);
                boolean z3 = z | (z2 ? (char) 2 : 0);
                this.e.writeInt(-2147287039);
                this.e.writeInt(((z3 & true ? 1 : 0) << true) | (B & 16777215));
                this.e.writeInt(i2 & Integer.MAX_VALUE);
                this.e.writeInt(Integer.MAX_VALUE & i3);
                this.e.writeShort(0);
                this.e.a((yo4) this.f);
                this.e.flush();
            } else {
                throw new IOException("closed");
            }
        }

        @DexIgnore
        public synchronized void b(kw3 kw3) throws IOException {
            if (!this.i) {
                int c = kw3.c();
                this.e.writeInt(-2147287036);
                this.e.writeInt((((c * 8) + 4) & 16777215) | 0);
                this.e.writeInt(c);
                for (int i2 = 0; i2 <= 10; i2++) {
                    if (kw3.f(i2)) {
                        this.e.writeInt(((kw3.a(i2) & 255) << 24) | (i2 & 16777215));
                        this.e.writeInt(kw3.b(i2));
                    }
                }
                this.e.flush();
            } else {
                throw new IOException("closed");
            }
        }

        @DexIgnore
        public synchronized void close() throws IOException {
            this.i = true;
            wv3.a((Closeable) this.e, (Closeable) this.g);
        }

        @DexIgnore
        public synchronized void flush() throws IOException {
            if (!this.i) {
                this.e.flush();
            } else {
                throw new IOException("closed");
            }
        }

        @DexIgnore
        public synchronized void p() {
        }

        @DexIgnore
        public int r() {
            return 16383;
        }

        @DexIgnore
        public synchronized void a(int i2, ErrorCode errorCode) throws IOException {
            if (this.i) {
                throw new IOException("closed");
            } else if (errorCode.spdyRstCode != -1) {
                this.e.writeInt(-2147287037);
                this.e.writeInt(8);
                this.e.writeInt(i2 & Integer.MAX_VALUE);
                this.e.writeInt(errorCode.spdyRstCode);
                this.e.flush();
            } else {
                throw new IllegalArgumentException();
            }
        }

        @DexIgnore
        public synchronized void a(boolean z, int i2, jo4 jo4, int i3) throws IOException {
            a(i2, z ? 1 : 0, jo4, i3);
        }

        @DexIgnore
        public void a(int i2, int i3, jo4 jo4, int i4) throws IOException {
            if (!this.i) {
                long j = (long) i4;
                if (j <= 16777215) {
                    this.e.writeInt(i2 & Integer.MAX_VALUE);
                    this.e.writeInt(((i3 & 255) << 24) | (16777215 & i4));
                    if (i4 > 0) {
                        this.e.a(jo4, j);
                        return;
                    }
                    return;
                }
                throw new IllegalArgumentException("FRAME_TOO_LARGE max size is 16Mib: " + i4);
            }
            throw new IOException("closed");
        }

        @DexIgnore
        public final void a(List<cw3> list) throws IOException {
            this.g.writeInt(list.size());
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                ByteString byteString = list.get(i2).a;
                this.g.writeInt(byteString.size());
                this.g.a(byteString);
                ByteString byteString2 = list.get(i2).b;
                this.g.writeInt(byteString2.size());
                this.g.a(byteString2);
            }
            this.g.flush();
        }

        @DexIgnore
        public synchronized void a(boolean z, int i2, int i3) throws IOException {
            if (!this.i) {
                boolean z2 = false;
                if (this.h != ((i2 & 1) == 1)) {
                    z2 = true;
                }
                if (z == z2) {
                    this.e.writeInt(-2147287034);
                    this.e.writeInt(4);
                    this.e.writeInt(i2);
                    this.e.flush();
                } else {
                    throw new IllegalArgumentException("payload != reply");
                }
            } else {
                throw new IOException("closed");
            }
        }

        @DexIgnore
        public synchronized void a(int i2, ErrorCode errorCode, byte[] bArr) throws IOException {
            if (this.i) {
                throw new IOException("closed");
            } else if (errorCode.spdyGoAwayCode != -1) {
                this.e.writeInt(-2147287033);
                this.e.writeInt(8);
                this.e.writeInt(i2);
                this.e.writeInt(errorCode.spdyGoAwayCode);
                this.e.flush();
            } else {
                throw new IllegalArgumentException("errorCode.spdyGoAwayCode == -1");
            }
        }

        @DexIgnore
        public synchronized void a(int i2, long j) throws IOException {
            if (this.i) {
                throw new IOException("closed");
            } else if (j == 0 || j > 2147483647L) {
                throw new IllegalArgumentException("windowSizeIncrement must be between 1 and 0x7fffffff: " + j);
            } else {
                this.e.writeInt(-2147287031);
                this.e.writeInt(8);
                this.e.writeInt(i2);
                this.e.writeInt((int) j);
                this.e.flush();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements yv3 {
        @DexIgnore
        public /* final */ lo4 e;
        @DexIgnore
        public /* final */ boolean f;
        @DexIgnore
        public /* final */ hw3 g; // = new hw3(this.e);

        @DexIgnore
        public a(lo4 lo4, boolean z) {
            this.e = lo4;
            this.f = z;
        }

        @DexIgnore
        public boolean a(yv3.a aVar) throws IOException {
            boolean z = false;
            try {
                int readInt = this.e.readInt();
                int readInt2 = this.e.readInt();
                int i = (-16777216 & readInt2) >>> 24;
                int i2 = readInt2 & 16777215;
                if ((Integer.MIN_VALUE & readInt) != 0) {
                    int i3 = (2147418112 & readInt) >>> 16;
                    int i4 = readInt & 65535;
                    if (i3 == 3) {
                        switch (i4) {
                            case 1:
                                g(aVar, i, i2);
                                return true;
                            case 2:
                                f(aVar, i, i2);
                                return true;
                            case 3:
                                d(aVar, i, i2);
                                return true;
                            case 4:
                                e(aVar, i, i2);
                                return true;
                            case 6:
                                c(aVar, i, i2);
                                return true;
                            case 7:
                                a(aVar, i, i2);
                                return true;
                            case 8:
                                b(aVar, i, i2);
                                return true;
                            case 9:
                                h(aVar, i, i2);
                                return true;
                            default:
                                this.e.skip((long) i2);
                                return true;
                        }
                    } else {
                        throw new ProtocolException("version != 3: " + i3);
                    }
                } else {
                    int i5 = readInt & Integer.MAX_VALUE;
                    if ((i & 1) != 0) {
                        z = true;
                    }
                    aVar.a(z, i5, this.e, i2);
                    return true;
                }
            } catch (IOException unused) {
                return false;
            }
        }

        @DexIgnore
        public final void b(yv3.a aVar, int i, int i2) throws IOException {
            aVar.a(false, false, this.e.readInt() & Integer.MAX_VALUE, -1, this.g.a(i2 - 4), HeadersMode.SPDY_HEADERS);
        }

        @DexIgnore
        public final void c(yv3.a aVar, int i, int i2) throws IOException {
            boolean z = true;
            if (i2 == 4) {
                int readInt = this.e.readInt();
                if (this.f != ((readInt & 1) == 1)) {
                    z = false;
                }
                aVar.a(z, readInt, 0);
                return;
            }
            a("TYPE_PING length: %d != 4", Integer.valueOf(i2));
            throw null;
        }

        @DexIgnore
        public void close() throws IOException {
            this.g.a();
        }

        @DexIgnore
        public final void d(yv3.a aVar, int i, int i2) throws IOException {
            if (i2 == 8) {
                int readInt = this.e.readInt() & Integer.MAX_VALUE;
                int readInt2 = this.e.readInt();
                ErrorCode fromSpdy3Rst = ErrorCode.fromSpdy3Rst(readInt2);
                if (fromSpdy3Rst != null) {
                    aVar.a(readInt, fromSpdy3Rst);
                    return;
                }
                a("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(readInt2));
                throw null;
            }
            a("TYPE_RST_STREAM length: %d != 8", Integer.valueOf(i2));
            throw null;
        }

        @DexIgnore
        public final void e(yv3.a aVar, int i, int i2) throws IOException {
            int readInt = this.e.readInt();
            boolean z = false;
            if (i2 == (readInt * 8) + 4) {
                kw3 kw3 = new kw3();
                for (int i3 = 0; i3 < readInt; i3++) {
                    int readInt2 = this.e.readInt();
                    int i4 = (-16777216 & readInt2) >>> 24;
                    kw3.a(readInt2 & 16777215, i4, this.e.readInt());
                }
                if ((i & 1) != 0) {
                    z = true;
                }
                aVar.a(z, kw3);
                return;
            }
            a("TYPE_SETTINGS length: %d != 4 + 8 * %d", Integer.valueOf(i2), Integer.valueOf(readInt));
            throw null;
        }

        @DexIgnore
        public final void f(yv3.a aVar, int i, int i2) throws IOException {
            aVar.a(false, (i & 1) != 0, this.e.readInt() & Integer.MAX_VALUE, -1, this.g.a(i2 - 4), HeadersMode.SPDY_REPLY);
        }

        @DexIgnore
        public final void g(yv3.a aVar, int i, int i2) throws IOException {
            int readInt = this.e.readInt() & Integer.MAX_VALUE;
            int readInt2 = this.e.readInt() & Integer.MAX_VALUE;
            this.e.readShort();
            List<cw3> a = this.g.a(i2 - 10);
            aVar.a((i & 2) != 0, (i & 1) != 0, readInt, readInt2, a, HeadersMode.SPDY_SYN_STREAM);
        }

        @DexIgnore
        public final void h(yv3.a aVar, int i, int i2) throws IOException {
            if (i2 == 8) {
                int readInt = this.e.readInt() & Integer.MAX_VALUE;
                long readInt2 = (long) (this.e.readInt() & Integer.MAX_VALUE);
                if (readInt2 != 0) {
                    aVar.a(readInt, readInt2);
                    return;
                }
                a("windowSizeIncrement was 0", Long.valueOf(readInt2));
                throw null;
            }
            a("TYPE_WINDOW_UPDATE length: %d != 8", Integer.valueOf(i2));
            throw null;
        }

        @DexIgnore
        public void q() {
        }

        @DexIgnore
        public final void a(yv3.a aVar, int i, int i2) throws IOException {
            if (i2 == 8) {
                int readInt = this.e.readInt() & Integer.MAX_VALUE;
                int readInt2 = this.e.readInt();
                ErrorCode fromSpdyGoAway = ErrorCode.fromSpdyGoAway(readInt2);
                if (fromSpdyGoAway != null) {
                    aVar.a(readInt, fromSpdyGoAway, ByteString.EMPTY);
                    return;
                }
                a("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(readInt2));
                throw null;
            }
            a("TYPE_GOAWAY length: %d != 8", Integer.valueOf(i2));
            throw null;
        }

        @DexIgnore
        public static IOException a(String str, Object... objArr) throws IOException {
            throw new IOException(String.format(str, objArr));
        }
    }
}
