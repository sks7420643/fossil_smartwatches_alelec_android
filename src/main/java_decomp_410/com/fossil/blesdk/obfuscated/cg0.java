package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class cg0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ sf0 e;

    @DexIgnore
    public cg0(sf0 sf0) {
        this.e = sf0;
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public void run() {
        this.e.b.lock();
        try {
            if (!Thread.interrupted()) {
                a();
                this.e.b.unlock();
            }
        } catch (RuntimeException e2) {
            this.e.a.a(e2);
        } finally {
            this.e.b.unlock();
        }
    }

    @DexIgnore
    public /* synthetic */ cg0(sf0 sf0, tf0 tf0) {
        this(sf0);
    }
}
