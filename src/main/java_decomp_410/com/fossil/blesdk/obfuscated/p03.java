package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p03 implements Factory<LoaderManager> {
    @DexIgnore
    public static LoaderManager a(n03 n03) {
        LoaderManager c = n03.c();
        n44.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
