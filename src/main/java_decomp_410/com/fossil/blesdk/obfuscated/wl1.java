package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wl1 implements Parcelable.Creator<vl1> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v6, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        String str = null;
        String str2 = null;
        kl1 kl1 = null;
        String str3 = null;
        hg1 hg1 = null;
        hg1 hg12 = null;
        hg1 hg13 = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 2:
                    str = SafeParcelReader.f(parcel2, a);
                    break;
                case 3:
                    str2 = SafeParcelReader.f(parcel2, a);
                    break;
                case 4:
                    kl1 = SafeParcelReader.a(parcel2, a, kl1.CREATOR);
                    break;
                case 5:
                    j = SafeParcelReader.s(parcel2, a);
                    break;
                case 6:
                    z = SafeParcelReader.i(parcel2, a);
                    break;
                case 7:
                    str3 = SafeParcelReader.f(parcel2, a);
                    break;
                case 8:
                    hg1 = SafeParcelReader.a(parcel2, a, hg1.CREATOR);
                    break;
                case 9:
                    j2 = SafeParcelReader.s(parcel2, a);
                    break;
                case 10:
                    hg12 = SafeParcelReader.a(parcel2, a, hg1.CREATOR);
                    break;
                case 11:
                    j3 = SafeParcelReader.s(parcel2, a);
                    break;
                case 12:
                    hg13 = SafeParcelReader.a(parcel2, a, hg1.CREATOR);
                    break;
                default:
                    SafeParcelReader.v(parcel2, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel2, b);
        return new vl1(str, str2, kl1, j, z, str3, hg1, j2, hg12, j3, hg13);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new vl1[i];
    }
}
