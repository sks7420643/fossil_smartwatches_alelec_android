package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ro0 {
    @DexIgnore
    public static /* final */ de0<Object> a; // = zz0.F;
    @DexIgnore
    public static /* final */ de0<Object> b; // = tz0.F;
    @DexIgnore
    public static /* final */ uo0 c; // = new n11();
    @DexIgnore
    public static /* final */ de0<Object> d; // = p21.F;
    @DexIgnore
    public static /* final */ qo0 e; // = new k11();

    /*
    static {
        de0<Object> de0 = d01.F;
        new r11();
        new p11();
        de0<Object> de02 = h01.F;
        new s11();
        de0<Object> de03 = pz0.F;
        new m11();
        de0<Object> de04 = l21.F;
        if (Build.VERSION.SDK_INT >= 18) {
            new j11();
        } else {
            new v11();
        }
        new Scope("https://www.googleapis.com/auth/fitness.activity.read");
        new Scope("https://www.googleapis.com/auth/fitness.activity.write");
        new Scope("https://www.googleapis.com/auth/fitness.location.read");
        new Scope("https://www.googleapis.com/auth/fitness.location.write");
        new Scope("https://www.googleapis.com/auth/fitness.body.read");
        new Scope("https://www.googleapis.com/auth/fitness.body.write");
        new Scope("https://www.googleapis.com/auth/fitness.nutrition.read");
        new Scope("https://www.googleapis.com/auth/fitness.nutrition.write");
    }
    */

    @DexIgnore
    public static yo0 a(Context context, GoogleSignInAccount googleSignInAccount) {
        bk0.a(googleSignInAccount);
        return new yo0(context, so0.a(googleSignInAccount).a());
    }
}
