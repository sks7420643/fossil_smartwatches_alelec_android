package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dr2 implements Factory<UpdateFirmwareUsecase> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<en2> b;

    @DexIgnore
    public dr2(Provider<DeviceRepository> provider, Provider<en2> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static dr2 a(Provider<DeviceRepository> provider, Provider<en2> provider2) {
        return new dr2(provider, provider2);
    }

    @DexIgnore
    public static UpdateFirmwareUsecase b(Provider<DeviceRepository> provider, Provider<en2> provider2) {
        return new UpdateFirmwareUsecase(provider.get(), provider2.get());
    }

    @DexIgnore
    public UpdateFirmwareUsecase get() {
        return b(this.a, this.b);
    }
}
