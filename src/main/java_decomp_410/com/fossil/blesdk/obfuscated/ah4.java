package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ah4 {
    @DexIgnore
    public static final zg4 a(CoroutineContext coroutineContext) {
        kd4.b(coroutineContext, "context");
        if (coroutineContext.get(fi4.d) == null) {
            coroutineContext = coroutineContext.plus(ii4.a((fi4) null, 1, (Object) null));
        }
        return new oj4(coroutineContext);
    }
}
