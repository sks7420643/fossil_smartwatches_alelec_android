package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nz */
public class C2512nz implements com.fossil.blesdk.obfuscated.c00 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ int f7896a;

    @DexIgnore
    public C2512nz(int i) {
        this.f7896a = i;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.StackTraceElement[] mo9374a(java.lang.StackTraceElement[] stackTraceElementArr) {
        int length = stackTraceElementArr.length;
        int i = this.f7896a;
        if (length <= i) {
            return stackTraceElementArr;
        }
        int i2 = i / 2;
        int i3 = i - i2;
        java.lang.StackTraceElement[] stackTraceElementArr2 = new java.lang.StackTraceElement[i];
        java.lang.System.arraycopy(stackTraceElementArr, 0, stackTraceElementArr2, 0, i3);
        java.lang.System.arraycopy(stackTraceElementArr, stackTraceElementArr.length - i2, stackTraceElementArr2, i3, i2);
        return stackTraceElementArr2;
    }
}
