package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rb2 extends qb2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public long B;

    /*
    static {
        D.put(R.id.iv_back, 1);
        D.put(R.id.tv_title, 2);
        D.put(R.id.iv_device, 3);
        D.put(R.id.tv_device_name, 4);
        D.put(R.id.tv_range, 5);
        D.put(R.id.tv_location_status, 6);
        D.put(R.id.ftv_locate_on_map, 7);
        D.put(R.id.sw_locate, 8);
        D.put(R.id.ftv_error, 9);
        D.put(R.id.cl_location, 10);
        D.put(R.id.ftv_last_location_tile, 11);
        D.put(R.id.ftv_last_location_time, 12);
        D.put(R.id.ftv_last_location_address, 13);
        D.put(R.id.ftv_last_location_city, 14);
    }
    */

    @DexIgnore
    public rb2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 15, C, D));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.B != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.B = 1;
        }
        g();
    }

    @DexIgnore
    public rb2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[10], objArr[9], objArr[13], objArr[14], objArr[11], objArr[12], objArr[7], objArr[1], objArr[3], objArr[8], objArr[4], objArr[6], objArr[5], objArr[2]);
        this.B = -1;
        this.A = objArr[0];
        this.A.setTag((Object) null);
        a(view);
        f();
    }
}
