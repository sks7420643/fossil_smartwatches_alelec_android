package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.sg4;
import java.lang.Throwable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface sg4<T extends Throwable & sg4<T>> {
    @DexIgnore
    T createCopy();
}
