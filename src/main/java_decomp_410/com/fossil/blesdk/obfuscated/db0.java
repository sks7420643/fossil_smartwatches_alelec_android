package com.fossil.blesdk.obfuscated;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class db0 {
    @DexIgnore
    public static /* final */ a a; // = new a();
    @DexIgnore
    public static /* final */ LinkedBlockingQueue<Runnable> b; // = new LinkedBlockingQueue<>(128);
    @DexIgnore
    public static /* final */ ThreadPoolExecutor c; // = new ThreadPoolExecutor(1, 1, 1, TimeUnit.SECONDS, b, a);
    @DexIgnore
    public static /* final */ db0 d; // = new db0();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ThreadFactory {
        @DexIgnore
        public /* final */ AtomicInteger a; // = new AtomicInteger(1);

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            kd4.b(runnable, "r");
            return new Thread(runnable, db0.class.getSimpleName() + " #" + this.a.getAndIncrement());
        }
    }

    @DexIgnore
    public final boolean a(wc4<qa4> wc4) {
        kd4.b(wc4, "action");
        c.execute(new cb0(wc4));
        return true;
    }
}
