package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class f54 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public f54(String str, boolean z) {
        this.a = str;
        this.b = z;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || f54.class != obj.getClass()) {
            return false;
        }
        f54 f54 = (f54) obj;
        if (this.b != f54.b) {
            return false;
        }
        String str = this.a;
        String str2 = f54.a;
        return str == null ? str2 == null : str.equals(str2);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        return ((str != null ? str.hashCode() : 0) * 31) + (this.b ? 1 : 0);
    }
}
