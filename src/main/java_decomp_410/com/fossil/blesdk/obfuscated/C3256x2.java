package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.x2 */
public class C3256x2 {

    @DexIgnore
    /* renamed from: a */
    public android.content.res.ColorStateList f10818a;

    @DexIgnore
    /* renamed from: b */
    public android.graphics.PorterDuff.Mode f10819b;

    @DexIgnore
    /* renamed from: c */
    public boolean f10820c;

    @DexIgnore
    /* renamed from: d */
    public boolean f10821d;

    @DexIgnore
    /* renamed from: a */
    public void mo17595a() {
        this.f10818a = null;
        this.f10821d = false;
        this.f10819b = null;
        this.f10820c = false;
    }
}
