package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jk extends lk<Boolean> {
    @DexIgnore
    public jk(Context context, zl zlVar) {
        super(xk.a(context, zlVar).a());
    }

    @DexIgnore
    public boolean a(hl hlVar) {
        return hlVar.j.g();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(Boolean bool) {
        return !bool.booleanValue();
    }
}
