package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.fossil.blesdk.obfuscated.so;
import com.fossil.blesdk.obfuscated.sr;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vr<Model, Data> implements sr<Model, Data> {
    @DexIgnore
    public /* final */ List<sr<Model, Data>> a;
    @DexIgnore
    public /* final */ g8<List<Throwable>> b;

    @DexIgnore
    public vr(List<sr<Model, Data>> list, g8<List<Throwable>> g8Var) {
        this.a = list;
        this.b = g8Var;
    }

    @DexIgnore
    public sr.a<Data> a(Model model, int i, int i2, lo loVar) {
        int size = this.a.size();
        ArrayList arrayList = new ArrayList(size);
        jo joVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            sr srVar = this.a.get(i3);
            if (srVar.a(model)) {
                sr.a a2 = srVar.a(model, i, i2, loVar);
                if (a2 != null) {
                    joVar = a2.a;
                    arrayList.add(a2.c);
                }
            }
        }
        if (arrayList.isEmpty() || joVar == null) {
            return null;
        }
        return new sr.a<>(joVar, new a(arrayList, this.b));
    }

    @DexIgnore
    public String toString() {
        return "MultiModelLoader{modelLoaders=" + Arrays.toString(this.a.toArray()) + '}';
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Data> implements so<Data>, so.a<Data> {
        @DexIgnore
        public /* final */ List<so<Data>> e;
        @DexIgnore
        public /* final */ g8<List<Throwable>> f;
        @DexIgnore
        public int g; // = 0;
        @DexIgnore
        public Priority h;
        @DexIgnore
        public so.a<? super Data> i;
        @DexIgnore
        public List<Throwable> j;
        @DexIgnore
        public boolean k;

        @DexIgnore
        public a(List<so<Data>> list, g8<List<Throwable>> g8Var) {
            this.f = g8Var;
            tw.a(list);
            this.e = list;
        }

        @DexIgnore
        public void a(Priority priority, so.a<? super Data> aVar) {
            this.h = priority;
            this.i = aVar;
            this.j = this.f.a();
            this.e.get(this.g).a(priority, this);
            if (this.k) {
                cancel();
            }
        }

        @DexIgnore
        public DataSource b() {
            return this.e.get(0).b();
        }

        @DexIgnore
        public final void c() {
            if (!this.k) {
                if (this.g < this.e.size() - 1) {
                    this.g++;
                    a(this.h, this.i);
                    return;
                }
                tw.a(this.j);
                this.i.a((Exception) new GlideException("Fetch failed", (List<Throwable>) new ArrayList(this.j)));
            }
        }

        @DexIgnore
        public void cancel() {
            this.k = true;
            for (so<Data> cancel : this.e) {
                cancel.cancel();
            }
        }

        @DexIgnore
        public Class<Data> getDataClass() {
            return this.e.get(0).getDataClass();
        }

        @DexIgnore
        public void a() {
            List<Throwable> list = this.j;
            if (list != null) {
                this.f.a(list);
            }
            this.j = null;
            for (so<Data> a : this.e) {
                a.a();
            }
        }

        @DexIgnore
        public void a(Data data) {
            if (data != null) {
                this.i.a(data);
            } else {
                c();
            }
        }

        @DexIgnore
        public void a(Exception exc) {
            List<Throwable> list = this.j;
            tw.a(list);
            list.add(exc);
            c();
        }
    }

    @DexIgnore
    public boolean a(Model model) {
        for (sr<Model, Data> a2 : this.a) {
            if (a2.a(model)) {
                return true;
            }
        }
        return false;
    }
}
