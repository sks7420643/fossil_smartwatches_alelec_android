package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yb */
public class C3351yb extends android.app.Service implements androidx.lifecycle.LifecycleOwner {

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C1866gc f11220e; // = new com.fossil.blesdk.obfuscated.C1866gc(this);

    @DexIgnore
    public androidx.lifecycle.Lifecycle getLifecycle() {
        return this.f11220e.mo11186a();
    }

    @DexIgnore
    public android.os.IBinder onBind(android.content.Intent intent) {
        this.f11220e.mo11188b();
        return null;
    }

    @DexIgnore
    public void onCreate() {
        this.f11220e.mo11189c();
        super.onCreate();
    }

    @DexIgnore
    public void onDestroy() {
        this.f11220e.mo11190d();
        super.onDestroy();
    }

    @DexIgnore
    public void onStart(android.content.Intent intent, int i) {
        this.f11220e.mo11191e();
        super.onStart(intent, i);
    }

    @DexIgnore
    public int onStartCommand(android.content.Intent intent, int i, int i2) {
        return super.onStartCommand(intent, i, i2);
    }
}
