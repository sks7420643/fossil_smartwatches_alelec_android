package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kl1 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<kl1> CREATOR; // = new ll1();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ Long h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ String j;
    @DexIgnore
    public /* final */ Double k;

    @DexIgnore
    public kl1(ml1 ml1) {
        this(ml1.c, ml1.d, ml1.e, ml1.b);
    }

    @DexIgnore
    public final Object H() {
        Long l = this.h;
        if (l != null) {
            return l;
        }
        Double d = this.k;
        if (d != null) {
            return d;
        }
        String str = this.i;
        if (str != null) {
            return str;
        }
        return null;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e);
        kk0.a(parcel, 2, this.f, false);
        kk0.a(parcel, 3, this.g);
        kk0.a(parcel, 4, this.h, false);
        kk0.a(parcel, 5, (Float) null, false);
        kk0.a(parcel, 6, this.i, false);
        kk0.a(parcel, 7, this.j, false);
        kk0.a(parcel, 8, this.k, false);
        kk0.a(parcel, a);
    }

    @DexIgnore
    public kl1(String str, long j2, Object obj, String str2) {
        bk0.b(str);
        this.e = 2;
        this.f = str;
        this.g = j2;
        this.j = str2;
        if (obj == null) {
            this.h = null;
            this.k = null;
            this.i = null;
        } else if (obj instanceof Long) {
            this.h = (Long) obj;
            this.k = null;
            this.i = null;
        } else if (obj instanceof String) {
            this.h = null;
            this.k = null;
            this.i = (String) obj;
        } else if (obj instanceof Double) {
            this.h = null;
            this.k = (Double) obj;
            this.i = null;
        } else {
            throw new IllegalArgumentException("User attribute given of un-supported type");
        }
    }

    @DexIgnore
    public kl1(int i2, String str, long j2, Long l, Float f2, String str2, String str3, Double d) {
        this.e = i2;
        this.f = str;
        this.g = j2;
        this.h = l;
        if (i2 == 1) {
            this.k = f2 != null ? Double.valueOf(f2.doubleValue()) : null;
        } else {
            this.k = d;
        }
        this.i = str2;
        this.j = str3;
    }
}
