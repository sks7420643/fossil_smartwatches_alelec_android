package com.fossil.blesdk.obfuscated;

import com.facebook.share.internal.VideoUploader;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class uf4<T> extends li4 implements fi4, yb4<T>, zg4 {
    @DexIgnore
    public /* final */ CoroutineContext f; // = this.g.plus(this);
    @DexIgnore
    public /* final */ CoroutineContext g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public uf4(CoroutineContext coroutineContext, boolean z) {
        super(z);
        kd4.b(coroutineContext, "parentContext");
        this.g = coroutineContext;
    }

    @DexIgnore
    public CoroutineContext A() {
        return this.f;
    }

    @DexIgnore
    public void a(Throwable th, boolean z) {
        kd4.b(th, "cause");
    }

    @DexIgnore
    public final <R> void a(CoroutineStart coroutineStart, R r, yc4<? super R, ? super yb4<? super T>, ? extends Object> yc4) {
        kd4.b(coroutineStart, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        kd4.b(yc4, "block");
        k();
        coroutineStart.invoke(yc4, r, this);
    }

    @DexIgnore
    public final void f(Throwable th) {
        kd4.b(th, "exception");
        wg4.a(this.f, th);
    }

    @DexIgnore
    public final void g(Object obj) {
        if (obj instanceof ng4) {
            ng4 ng4 = (ng4) obj;
            a(ng4.a, ng4.a());
            return;
        }
        j(obj);
    }

    @DexIgnore
    public final CoroutineContext getContext() {
        return this.f;
    }

    @DexIgnore
    public final void h() {
        l();
    }

    @DexIgnore
    public boolean isActive() {
        return super.isActive();
    }

    @DexIgnore
    public int j() {
        return 0;
    }

    @DexIgnore
    public void j(T t) {
    }

    @DexIgnore
    public final void k() {
        a((fi4) this.g.get(fi4.d));
    }

    @DexIgnore
    public void l() {
    }

    @DexIgnore
    public final void resumeWith(Object obj) {
        b(og4.a(obj), j());
    }

    @DexIgnore
    public String g() {
        String a = tg4.a(this.f);
        if (a == null) {
            return super.g();
        }
        return '\"' + a + "\":" + super.g();
    }
}
