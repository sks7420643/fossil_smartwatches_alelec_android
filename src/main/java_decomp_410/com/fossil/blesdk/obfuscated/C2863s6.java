package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.s6 */
public class C2863s6 {
    @DexIgnore
    /* renamed from: a */
    public static boolean m13545a(org.xmlpull.v1.XmlPullParser xmlPullParser, java.lang.String str) {
        return xmlPullParser.getAttributeValue("http://schemas.android.com/apk/res/android", str) != null;
    }

    @DexIgnore
    /* renamed from: b */
    public static int m13546b(android.content.res.TypedArray typedArray, org.xmlpull.v1.XmlPullParser xmlPullParser, java.lang.String str, int i, int i2) {
        if (!m13545a(xmlPullParser, str)) {
            return i2;
        }
        return typedArray.getInt(i, i2);
    }

    @DexIgnore
    /* renamed from: c */
    public static int m13548c(android.content.res.TypedArray typedArray, org.xmlpull.v1.XmlPullParser xmlPullParser, java.lang.String str, int i, int i2) {
        if (!m13545a(xmlPullParser, str)) {
            return i2;
        }
        return typedArray.getResourceId(i, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public static float m13539a(android.content.res.TypedArray typedArray, org.xmlpull.v1.XmlPullParser xmlPullParser, java.lang.String str, int i, float f) {
        if (!m13545a(xmlPullParser, str)) {
            return f;
        }
        return typedArray.getFloat(i, f);
    }

    @DexIgnore
    /* renamed from: b */
    public static android.util.TypedValue m13547b(android.content.res.TypedArray typedArray, org.xmlpull.v1.XmlPullParser xmlPullParser, java.lang.String str, int i) {
        if (!m13545a(xmlPullParser, str)) {
            return null;
        }
        return typedArray.peekValue(i);
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m13544a(android.content.res.TypedArray typedArray, org.xmlpull.v1.XmlPullParser xmlPullParser, java.lang.String str, int i, boolean z) {
        if (!m13545a(xmlPullParser, str)) {
            return z;
        }
        return typedArray.getBoolean(i, z);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m13540a(android.content.res.TypedArray typedArray, org.xmlpull.v1.XmlPullParser xmlPullParser, java.lang.String str, int i, int i2) {
        if (!m13545a(xmlPullParser, str)) {
            return i2;
        }
        return typedArray.getColor(i, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2462n6 m13542a(android.content.res.TypedArray typedArray, org.xmlpull.v1.XmlPullParser xmlPullParser, android.content.res.Resources.Theme theme, java.lang.String str, int i, int i2) {
        if (m13545a(xmlPullParser, str)) {
            android.util.TypedValue typedValue = new android.util.TypedValue();
            typedArray.getValue(i, typedValue);
            int i3 = typedValue.type;
            if (i3 >= 28 && i3 <= 31) {
                return com.fossil.blesdk.obfuscated.C2462n6.m11059b(typedValue.data);
            }
            com.fossil.blesdk.obfuscated.C2462n6 b = com.fossil.blesdk.obfuscated.C2462n6.m11060b(typedArray.getResources(), typedArray.getResourceId(i, 0), theme);
            if (b != null) {
                return b;
            }
        }
        return com.fossil.blesdk.obfuscated.C2462n6.m11059b(i2);
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m13543a(android.content.res.TypedArray typedArray, org.xmlpull.v1.XmlPullParser xmlPullParser, java.lang.String str, int i) {
        if (!m13545a(xmlPullParser, str)) {
            return null;
        }
        return typedArray.getString(i);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.content.res.TypedArray m13541a(android.content.res.Resources resources, android.content.res.Resources.Theme theme, android.util.AttributeSet attributeSet, int[] iArr) {
        if (theme == null) {
            return resources.obtainAttributes(attributeSet, iArr);
        }
        return theme.obtainStyledAttributes(attributeSet, iArr, 0, 0);
    }
}
