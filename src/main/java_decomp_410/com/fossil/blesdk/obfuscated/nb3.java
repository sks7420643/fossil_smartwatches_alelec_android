package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nb3 implements Factory<ub3> {
    @DexIgnore
    public static ub3 a(kb3 kb3) {
        ub3 c = kb3.c();
        n44.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
