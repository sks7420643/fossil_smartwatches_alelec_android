package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.va */
public final class C3101va extends com.fossil.blesdk.obfuscated.C1472bb implements androidx.fragment.app.FragmentManager.C0174a, androidx.fragment.app.FragmentManagerImpl.C0190l {

    @DexIgnore
    /* renamed from: a */
    public /* final */ androidx.fragment.app.FragmentManagerImpl f10190a;

    @DexIgnore
    /* renamed from: b */
    public java.util.ArrayList<com.fossil.blesdk.obfuscated.C3101va.C3102a> f10191b; // = new java.util.ArrayList<>();

    @DexIgnore
    /* renamed from: c */
    public int f10192c;

    @DexIgnore
    /* renamed from: d */
    public int f10193d;

    @DexIgnore
    /* renamed from: e */
    public int f10194e;

    @DexIgnore
    /* renamed from: f */
    public int f10195f;

    @DexIgnore
    /* renamed from: g */
    public int f10196g;

    @DexIgnore
    /* renamed from: h */
    public int f10197h;

    @DexIgnore
    /* renamed from: i */
    public boolean f10198i;

    @DexIgnore
    /* renamed from: j */
    public boolean f10199j; // = true;

    @DexIgnore
    /* renamed from: k */
    public java.lang.String f10200k;

    @DexIgnore
    /* renamed from: l */
    public boolean f10201l;

    @DexIgnore
    /* renamed from: m */
    public int f10202m; // = -1;

    @DexIgnore
    /* renamed from: n */
    public int f10203n;

    @DexIgnore
    /* renamed from: o */
    public java.lang.CharSequence f10204o;

    @DexIgnore
    /* renamed from: p */
    public int f10205p;

    @DexIgnore
    /* renamed from: q */
    public java.lang.CharSequence f10206q;

    @DexIgnore
    /* renamed from: r */
    public java.util.ArrayList<java.lang.String> f10207r;

    @DexIgnore
    /* renamed from: s */
    public java.util.ArrayList<java.lang.String> f10208s;

    @DexIgnore
    /* renamed from: t */
    public boolean f10209t; // = false;

    @DexIgnore
    /* renamed from: u */
    public java.util.ArrayList<java.lang.Runnable> f10210u;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.va$a")
    /* renamed from: com.fossil.blesdk.obfuscated.va$a */
    public static final class C3102a {

        @DexIgnore
        /* renamed from: a */
        public int f10211a;

        @DexIgnore
        /* renamed from: b */
        public androidx.fragment.app.Fragment f10212b;

        @DexIgnore
        /* renamed from: c */
        public int f10213c;

        @DexIgnore
        /* renamed from: d */
        public int f10214d;

        @DexIgnore
        /* renamed from: e */
        public int f10215e;

        @DexIgnore
        /* renamed from: f */
        public int f10216f;

        @DexIgnore
        public C3102a() {
        }

        @DexIgnore
        public C3102a(int i, androidx.fragment.app.Fragment fragment) {
            this.f10211a = i;
            this.f10212b = fragment;
        }
    }

    @DexIgnore
    public C3101va(androidx.fragment.app.FragmentManagerImpl fragmentManagerImpl) {
        this.f10190a = fragmentManagerImpl;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17006a(java.lang.String str, java.io.FileDescriptor fileDescriptor, java.io.PrintWriter printWriter, java.lang.String[] strArr) {
        mo17007a(str, printWriter, true);
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C1472bb mo9130b(int i, androidx.fragment.app.Fragment fragment, java.lang.String str) {
        if (i != 0) {
            mo17003a(i, fragment, str, 2);
            return this;
        }
        throw new java.lang.IllegalArgumentException("Must use non-zero containerViewId");
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C1472bb mo9132c(androidx.fragment.app.Fragment fragment) {
        mo17005a(new com.fossil.blesdk.obfuscated.C3101va.C3102a(4, fragment));
        return this;
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C1472bb mo9134d(androidx.fragment.app.Fragment fragment) {
        mo17005a(new com.fossil.blesdk.obfuscated.C3101va.C3102a(3, fragment));
        return this;
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1472bb mo9136e(androidx.fragment.app.Fragment fragment) {
        mo17005a(new com.fossil.blesdk.obfuscated.C3101va.C3102a(5, fragment));
        return this;
    }

    @DexIgnore
    /* renamed from: f */
    public void mo17013f() {
        int size = this.f10191b.size();
        for (int i = 0; i < size; i++) {
            com.fossil.blesdk.obfuscated.C3101va.C3102a aVar = this.f10191b.get(i);
            androidx.fragment.app.Fragment fragment = aVar.f10212b;
            if (fragment != null) {
                fragment.setNextTransition(this.f10196g, this.f10197h);
            }
            switch (aVar.f10211a) {
                case 1:
                    fragment.setNextAnim(aVar.f10213c);
                    this.f10190a.mo2126a(fragment, false);
                    break;
                case 3:
                    fragment.setNextAnim(aVar.f10214d);
                    this.f10190a.mo2179l(fragment);
                    break;
                case 4:
                    fragment.setNextAnim(aVar.f10214d);
                    this.f10190a.mo2165f(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(aVar.f10213c);
                    this.f10190a.mo2189p(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(aVar.f10214d);
                    this.f10190a.mo2154c(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(aVar.f10213c);
                    this.f10190a.mo2120a(fragment);
                    break;
                case 8:
                    this.f10190a.mo2185o(fragment);
                    break;
                case 9:
                    this.f10190a.mo2185o((androidx.fragment.app.Fragment) null);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException("Unknown cmd: " + aVar.f10211a);
            }
            if (!(this.f10209t || aVar.f10211a == 1 || fragment == null)) {
                this.f10190a.mo2173i(fragment);
            }
        }
        if (!this.f10209t) {
            androidx.fragment.app.FragmentManagerImpl fragmentManagerImpl = this.f10190a;
            fragmentManagerImpl.mo2115a(fragmentManagerImpl.f1069p, true);
        }
    }

    @DexIgnore
    /* renamed from: g */
    public java.lang.String mo17014g() {
        return this.f10200k;
    }

    @DexIgnore
    /* renamed from: h */
    public boolean mo17015h() {
        for (int i = 0; i < this.f10191b.size(); i++) {
            if (m15120b(this.f10191b.get(i))) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: i */
    public void mo17016i() {
        java.util.ArrayList<java.lang.Runnable> arrayList = this.f10210u;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                this.f10210u.get(i).run();
            }
            this.f10210u = null;
        }
    }

    @DexIgnore
    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(java.lang.Integer.toHexString(java.lang.System.identityHashCode(this)));
        if (this.f10202m >= 0) {
            sb.append(" #");
            sb.append(this.f10202m);
        }
        if (this.f10200k != null) {
            sb.append(" ");
            sb.append(this.f10200k);
        }
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17007a(java.lang.String str, java.io.PrintWriter printWriter, boolean z) {
        java.lang.String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.f10200k);
            printWriter.print(" mIndex=");
            printWriter.print(this.f10202m);
            printWriter.print(" mCommitted=");
            printWriter.println(this.f10201l);
            if (this.f10196g != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(java.lang.Integer.toHexString(this.f10196g));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(java.lang.Integer.toHexString(this.f10197h));
            }
            if (!(this.f10192c == 0 && this.f10193d == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(java.lang.Integer.toHexString(this.f10192c));
                printWriter.print(" mExitAnim=#");
                printWriter.println(java.lang.Integer.toHexString(this.f10193d));
            }
            if (!(this.f10194e == 0 && this.f10195f == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(java.lang.Integer.toHexString(this.f10194e));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(java.lang.Integer.toHexString(this.f10195f));
            }
            if (!(this.f10203n == 0 && this.f10204o == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(java.lang.Integer.toHexString(this.f10203n));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.f10204o);
            }
            if (!(this.f10205p == 0 && this.f10206q == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(java.lang.Integer.toHexString(this.f10205p));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.f10206q);
            }
        }
        if (!this.f10191b.isEmpty()) {
            printWriter.print(str);
            printWriter.println("Operations:");
            str + "    ";
            int size = this.f10191b.size();
            for (int i = 0; i < size; i++) {
                com.fossil.blesdk.obfuscated.C3101va.C3102a aVar = this.f10191b.get(i);
                switch (aVar.f10211a) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    case 8:
                        str2 = "SET_PRIMARY_NAV";
                        break;
                    case 9:
                        str2 = "UNSET_PRIMARY_NAV";
                        break;
                    default:
                        str2 = "cmd=" + aVar.f10211a;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(aVar.f10212b);
                if (z) {
                    if (!(aVar.f10213c == 0 && aVar.f10214d == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(java.lang.Integer.toHexString(aVar.f10213c));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(java.lang.Integer.toHexString(aVar.f10214d));
                    }
                    if (aVar.f10215e != 0 || aVar.f10216f != 0) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(java.lang.Integer.toHexString(aVar.f10215e));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(java.lang.Integer.toHexString(aVar.f10216f));
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo9133c() {
        mo17012e();
        this.f10190a.mo2147b((androidx.fragment.app.FragmentManagerImpl.C0190l) this, false);
    }

    @DexIgnore
    /* renamed from: d */
    public void mo9135d() {
        mo17012e();
        this.f10190a.mo2147b((androidx.fragment.app.FragmentManagerImpl.C0190l) this, true);
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1472bb mo17012e() {
        if (!this.f10198i) {
            this.f10199j = false;
            return this;
        }
        throw new java.lang.IllegalStateException("This transaction is already being added to the back stack");
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C1472bb mo9131b(androidx.fragment.app.Fragment fragment) {
        mo17005a(new com.fossil.blesdk.obfuscated.C3101va.C3102a(6, fragment));
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo9129b() {
        return mo17000a(true);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo17011b(int i) {
        int size = this.f10191b.size();
        for (int i2 = 0; i2 < size; i2++) {
            androidx.fragment.app.Fragment fragment = this.f10191b.get(i2).f10212b;
            int i3 = fragment != null ? fragment.mContainerId : 0;
            if (i3 != 0 && i3 == i) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17010b(boolean z) {
        for (int size = this.f10191b.size() - 1; size >= 0; size--) {
            com.fossil.blesdk.obfuscated.C3101va.C3102a aVar = this.f10191b.get(size);
            androidx.fragment.app.Fragment fragment = aVar.f10212b;
            if (fragment != null) {
                fragment.setNextTransition(androidx.fragment.app.FragmentManagerImpl.m1039e(this.f10196g), this.f10197h);
            }
            switch (aVar.f10211a) {
                case 1:
                    fragment.setNextAnim(aVar.f10216f);
                    this.f10190a.mo2179l(fragment);
                    break;
                case 3:
                    fragment.setNextAnim(aVar.f10215e);
                    this.f10190a.mo2126a(fragment, false);
                    break;
                case 4:
                    fragment.setNextAnim(aVar.f10215e);
                    this.f10190a.mo2189p(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(aVar.f10216f);
                    this.f10190a.mo2165f(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(aVar.f10215e);
                    this.f10190a.mo2120a(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(aVar.f10216f);
                    this.f10190a.mo2154c(fragment);
                    break;
                case 8:
                    this.f10190a.mo2185o((androidx.fragment.app.Fragment) null);
                    break;
                case 9:
                    this.f10190a.mo2185o(fragment);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException("Unknown cmd: " + aVar.f10211a);
            }
            if (!(this.f10209t || aVar.f10211a == 3 || fragment == null)) {
                this.f10190a.mo2173i(fragment);
            }
        }
        if (!this.f10209t && z) {
            androidx.fragment.app.FragmentManagerImpl fragmentManagerImpl = this.f10190a;
            fragmentManagerImpl.mo2115a(fragmentManagerImpl.f1069p, true);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public androidx.fragment.app.Fragment mo17009b(java.util.ArrayList<androidx.fragment.app.Fragment> arrayList, androidx.fragment.app.Fragment fragment) {
        for (int i = 0; i < this.f10191b.size(); i++) {
            com.fossil.blesdk.obfuscated.C3101va.C3102a aVar = this.f10191b.get(i);
            int i2 = aVar.f10211a;
            if (i2 != 1) {
                if (i2 != 3) {
                    switch (i2) {
                        case 6:
                            break;
                        case 7:
                            break;
                        case 8:
                            fragment = null;
                            break;
                        case 9:
                            fragment = aVar.f10212b;
                            break;
                    }
                }
                arrayList.add(aVar.f10212b);
            }
            arrayList.remove(aVar.f10212b);
        }
        return fragment;
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m15120b(com.fossil.blesdk.obfuscated.C3101va.C3102a aVar) {
        androidx.fragment.app.Fragment fragment = aVar.f10212b;
        return fragment != null && fragment.mAdded && fragment.mView != null && !fragment.mDetached && !fragment.mHidden && fragment.isPostponed();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17005a(com.fossil.blesdk.obfuscated.C3101va.C3102a aVar) {
        this.f10191b.add(aVar);
        aVar.f10213c = this.f10192c;
        aVar.f10214d = this.f10193d;
        aVar.f10215e = this.f10194e;
        aVar.f10216f = this.f10195f;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1472bb mo9127a(androidx.fragment.app.Fragment fragment, java.lang.String str) {
        mo17003a(0, fragment, str, 1);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1472bb mo9125a(int i, androidx.fragment.app.Fragment fragment, java.lang.String str) {
        mo17003a(i, fragment, str, 1);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo17003a(int i, androidx.fragment.app.Fragment fragment, java.lang.String str, int i2) {
        java.lang.Class<?> cls = fragment.getClass();
        int modifiers = cls.getModifiers();
        if (cls.isAnonymousClass() || !java.lang.reflect.Modifier.isPublic(modifiers) || (cls.isMemberClass() && !java.lang.reflect.Modifier.isStatic(modifiers))) {
            throw new java.lang.IllegalStateException("Fragment " + cls.getCanonicalName() + " must be a public static class to be  properly recreated from" + " instance state.");
        }
        fragment.mFragmentManager = this.f10190a;
        if (str != null) {
            java.lang.String str2 = fragment.mTag;
            if (str2 == null || str.equals(str2)) {
                fragment.mTag = str;
            } else {
                throw new java.lang.IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.mTag + " now " + str);
            }
        }
        if (i != 0) {
            if (i != -1) {
                int i3 = fragment.mFragmentId;
                if (i3 == 0 || i3 == i) {
                    fragment.mFragmentId = i;
                    fragment.mContainerId = i;
                } else {
                    throw new java.lang.IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.mFragmentId + " now " + i);
                }
            } else {
                throw new java.lang.IllegalArgumentException("Can't add fragment " + fragment + " with tag " + str + " to container view with no id");
            }
        }
        mo17005a(new com.fossil.blesdk.obfuscated.C3101va.C3102a(i2, fragment));
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1472bb mo9124a(int i, androidx.fragment.app.Fragment fragment) {
        mo9130b(i, fragment, (java.lang.String) null);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1472bb mo9126a(androidx.fragment.app.Fragment fragment) {
        mo17005a(new com.fossil.blesdk.obfuscated.C3101va.C3102a(7, fragment));
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1472bb mo9128a(java.lang.String str) {
        if (this.f10199j) {
            this.f10198i = true;
            this.f10200k = str;
            return this;
        }
        throw new java.lang.IllegalStateException("This FragmentTransaction is not allowed to be added to the back stack.");
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17002a(int i) {
        if (this.f10198i) {
            if (androidx.fragment.app.FragmentManagerImpl.f1046I) {
                android.util.Log.v("FragmentManager", "Bump nesting in " + this + " by " + i);
            }
            int size = this.f10191b.size();
            for (int i2 = 0; i2 < size; i2++) {
                androidx.fragment.app.Fragment fragment = this.f10191b.get(i2).f10212b;
                if (fragment != null) {
                    fragment.mBackStackNesting += i;
                    if (androidx.fragment.app.FragmentManagerImpl.f1046I) {
                        android.util.Log.v("FragmentManager", "Bump nesting of " + r3.f10212b + " to " + r3.f10212b.mBackStackNesting);
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int mo9123a() {
        return mo17000a(false);
    }

    @DexIgnore
    /* renamed from: a */
    public int mo17000a(boolean z) {
        if (!this.f10201l) {
            if (androidx.fragment.app.FragmentManagerImpl.f1046I) {
                android.util.Log.v("FragmentManager", "Commit: " + this);
                java.io.PrintWriter printWriter = new java.io.PrintWriter(new com.fossil.blesdk.obfuscated.C1604d8("FragmentManager"));
                mo17006a("  ", (java.io.FileDescriptor) null, printWriter, (java.lang.String[]) null);
                printWriter.close();
            }
            this.f10201l = true;
            if (this.f10198i) {
                this.f10202m = this.f10190a.mo2140b(this);
            } else {
                this.f10202m = -1;
            }
            this.f10190a.mo2128a((androidx.fragment.app.FragmentManagerImpl.C0190l) this, z);
            return this.f10202m;
        }
        throw new java.lang.IllegalStateException("commit already called");
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo2214a(java.util.ArrayList<com.fossil.blesdk.obfuscated.C3101va> arrayList, java.util.ArrayList<java.lang.Boolean> arrayList2) {
        if (androidx.fragment.app.FragmentManagerImpl.f1046I) {
            android.util.Log.v("FragmentManager", "Run: " + this);
        }
        arrayList.add(this);
        arrayList2.add(false);
        if (!this.f10198i) {
            return true;
        }
        this.f10190a.mo2130a(this);
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17008a(java.util.ArrayList<com.fossil.blesdk.obfuscated.C3101va> arrayList, int i, int i2) {
        if (i2 == i) {
            return false;
        }
        int size = this.f10191b.size();
        int i3 = -1;
        for (int i4 = 0; i4 < size; i4++) {
            androidx.fragment.app.Fragment fragment = this.f10191b.get(i4).f10212b;
            int i5 = fragment != null ? fragment.mContainerId : 0;
            if (!(i5 == 0 || i5 == i3)) {
                for (int i6 = i; i6 < i2; i6++) {
                    com.fossil.blesdk.obfuscated.C3101va vaVar = arrayList.get(i6);
                    int size2 = vaVar.f10191b.size();
                    for (int i7 = 0; i7 < size2; i7++) {
                        androidx.fragment.app.Fragment fragment2 = vaVar.f10191b.get(i7).f10212b;
                        if ((fragment2 != null ? fragment2.mContainerId : 0) == i5) {
                            return true;
                        }
                    }
                }
                i3 = i5;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.fragment.app.Fragment mo17001a(java.util.ArrayList<androidx.fragment.app.Fragment> arrayList, androidx.fragment.app.Fragment fragment) {
        java.util.ArrayList<androidx.fragment.app.Fragment> arrayList2 = arrayList;
        androidx.fragment.app.Fragment fragment2 = fragment;
        int i = 0;
        while (i < this.f10191b.size()) {
            com.fossil.blesdk.obfuscated.C3101va.C3102a aVar = this.f10191b.get(i);
            int i2 = aVar.f10211a;
            if (i2 != 1) {
                if (i2 == 2) {
                    androidx.fragment.app.Fragment fragment3 = aVar.f10212b;
                    int i3 = fragment3.mContainerId;
                    androidx.fragment.app.Fragment fragment4 = fragment2;
                    int i4 = i;
                    boolean z = false;
                    for (int size = arrayList.size() - 1; size >= 0; size--) {
                        androidx.fragment.app.Fragment fragment5 = arrayList2.get(size);
                        if (fragment5.mContainerId == i3) {
                            if (fragment5 == fragment3) {
                                z = true;
                            } else {
                                if (fragment5 == fragment4) {
                                    this.f10191b.add(i4, new com.fossil.blesdk.obfuscated.C3101va.C3102a(9, fragment5));
                                    i4++;
                                    fragment4 = null;
                                }
                                com.fossil.blesdk.obfuscated.C3101va.C3102a aVar2 = new com.fossil.blesdk.obfuscated.C3101va.C3102a(3, fragment5);
                                aVar2.f10213c = aVar.f10213c;
                                aVar2.f10215e = aVar.f10215e;
                                aVar2.f10214d = aVar.f10214d;
                                aVar2.f10216f = aVar.f10216f;
                                this.f10191b.add(i4, aVar2);
                                arrayList2.remove(fragment5);
                                i4++;
                            }
                        }
                    }
                    if (z) {
                        this.f10191b.remove(i4);
                        i4--;
                    } else {
                        aVar.f10211a = 1;
                        arrayList2.add(fragment3);
                    }
                    i = i4;
                    fragment2 = fragment4;
                } else if (i2 == 3 || i2 == 6) {
                    arrayList2.remove(aVar.f10212b);
                    androidx.fragment.app.Fragment fragment6 = aVar.f10212b;
                    if (fragment6 == fragment2) {
                        this.f10191b.add(i, new com.fossil.blesdk.obfuscated.C3101va.C3102a(9, fragment6));
                        i++;
                        fragment2 = null;
                    }
                } else if (i2 != 7) {
                    if (i2 == 8) {
                        this.f10191b.add(i, new com.fossil.blesdk.obfuscated.C3101va.C3102a(9, fragment2));
                        i++;
                        fragment2 = aVar.f10212b;
                    }
                }
                i++;
            }
            arrayList2.add(aVar.f10212b);
            i++;
        }
        return fragment2;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17004a(androidx.fragment.app.Fragment.OnStartEnterTransitionListener onStartEnterTransitionListener) {
        for (int i = 0; i < this.f10191b.size(); i++) {
            com.fossil.blesdk.obfuscated.C3101va.C3102a aVar = this.f10191b.get(i);
            if (m15120b(aVar)) {
                aVar.f10212b.setOnStartEnterTransitionListener(onStartEnterTransitionListener);
            }
        }
    }
}
