package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xq4 extends yq4<AppCompatActivity> {
    @DexIgnore
    public xq4(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    @DexIgnore
    public void a(int i, String... strArr) {
        v5.a((Activity) b(), strArr, i);
    }

    @DexIgnore
    public boolean b(String str) {
        return v5.a((Activity) b(), str);
    }

    @DexIgnore
    public FragmentManager c() {
        return ((AppCompatActivity) b()).getSupportFragmentManager();
    }

    @DexIgnore
    public Context a() {
        return (Context) b();
    }
}
