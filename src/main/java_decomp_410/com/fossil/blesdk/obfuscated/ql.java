package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ql {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public SharedPreferences b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public ql(Context context) {
        this.a = context;
    }

    @DexIgnore
    public int a(int i, int i2) {
        synchronized (ql.class) {
            a();
            int a2 = a("next_job_scheduler_id");
            if (a2 >= i) {
                if (a2 <= i2) {
                    i = a2;
                }
            }
            a("next_job_scheduler_id", i + 1);
        }
        return i;
    }

    @DexIgnore
    public int b() {
        int a2;
        synchronized (ql.class) {
            a();
            a2 = a("next_alarm_manager_id");
        }
        return a2;
    }

    @DexIgnore
    public final int a(String str) {
        int i = 0;
        int i2 = this.b.getInt(str, 0);
        if (i2 != Integer.MAX_VALUE) {
            i = i2 + 1;
        }
        a(str, i);
        return i2;
    }

    @DexIgnore
    public final void a(String str, int i) {
        this.b.edit().putInt(str, i).apply();
    }

    @DexIgnore
    public final void a() {
        if (!this.c) {
            this.b = this.a.getSharedPreferences("androidx.work.util.id", 0);
            this.c = true;
        }
    }
}
