package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hn1 implements Parcelable.Creator<gn1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        ud0 ud0 = null;
        int i = 0;
        dk0 dk0 = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 == 2) {
                ud0 = (ud0) SafeParcelReader.a(parcel, a, ud0.CREATOR);
            } else if (a2 != 3) {
                SafeParcelReader.v(parcel, a);
            } else {
                dk0 = (dk0) SafeParcelReader.a(parcel, a, dk0.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new gn1(i, ud0, dk0);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new gn1[i];
    }
}
