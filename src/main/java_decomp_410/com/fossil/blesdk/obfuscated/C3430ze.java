package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ze */
public class C3430ze {
    @DexIgnore
    /* renamed from: a */
    public static int m17332a(androidx.recyclerview.widget.RecyclerView.State state, com.fossil.blesdk.obfuscated.C3214we weVar, android.view.View view, android.view.View view2, androidx.recyclerview.widget.RecyclerView.C0236m mVar, boolean z, boolean z2) {
        int i;
        if (mVar.mo2941e() == 0 || state.mo2737a() == 0 || view == null || view2 == null) {
            return 0;
        }
        int min = java.lang.Math.min(mVar.mo2962l(view), mVar.mo2962l(view2));
        int max = java.lang.Math.max(mVar.mo2962l(view), mVar.mo2962l(view2));
        if (z2) {
            i = java.lang.Math.max(0, (state.mo2737a() - max) - 1);
        } else {
            i = java.lang.Math.max(0, min);
        }
        if (!z) {
            return i;
        }
        return java.lang.Math.round((((float) i) * (((float) java.lang.Math.abs(weVar.mo17430a(view2) - weVar.mo17437d(view))) / ((float) (java.lang.Math.abs(mVar.mo2962l(view) - mVar.mo2962l(view2)) + 1)))) + ((float) (weVar.mo17440f() - weVar.mo17437d(view))));
    }

    @DexIgnore
    /* renamed from: b */
    public static int m17333b(androidx.recyclerview.widget.RecyclerView.State state, com.fossil.blesdk.obfuscated.C3214we weVar, android.view.View view, android.view.View view2, androidx.recyclerview.widget.RecyclerView.C0236m mVar, boolean z) {
        if (mVar.mo2941e() == 0 || state.mo2737a() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return state.mo2737a();
        }
        return (int) ((((float) (weVar.mo17430a(view2) - weVar.mo17437d(view))) / ((float) (java.lang.Math.abs(mVar.mo2962l(view) - mVar.mo2962l(view2)) + 1))) * ((float) state.mo2737a()));
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17331a(androidx.recyclerview.widget.RecyclerView.State state, com.fossil.blesdk.obfuscated.C3214we weVar, android.view.View view, android.view.View view2, androidx.recyclerview.widget.RecyclerView.C0236m mVar, boolean z) {
        if (mVar.mo2941e() == 0 || state.mo2737a() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return java.lang.Math.abs(mVar.mo2962l(view) - mVar.mo2962l(view2)) + 1;
        }
        return java.lang.Math.min(weVar.mo17442g(), weVar.mo17430a(view2) - weVar.mo17437d(view));
    }
}
