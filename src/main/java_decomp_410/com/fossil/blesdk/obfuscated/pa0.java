package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.gr4;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pa0 {
    @DexIgnore
    public static /* final */ pa0 a; // = new pa0();

    @DexIgnore
    public final oa0 a(String str, String str2) {
        kd4.b(str, "secretKey");
        kd4.b(str2, "accessKey");
        OkHttpClient.b bVar = new OkHttpClient.b();
        bVar.b((Interceptor) new qa0(str2, str));
        bVar.a(30000, TimeUnit.MILLISECONDS);
        bVar.b(60000, TimeUnit.MILLISECONDS);
        try {
            Retrofit.b bVar2 = new Retrofit.b();
            bVar2.a(bVar.a());
            bVar2.a((gr4.a) os4.a());
            bVar2.a("http://localhost/");
            return (oa0) bVar2.a().a(oa0.class);
        } catch (Exception e) {
            da0.l.a(e);
            return null;
        }
    }
}
