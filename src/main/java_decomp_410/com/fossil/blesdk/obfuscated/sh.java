package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sh extends yh implements uh {
    @DexIgnore
    public sh(Context context, ViewGroup viewGroup, View view) {
        super(context, viewGroup, view);
    }

    @DexIgnore
    public static sh a(ViewGroup viewGroup) {
        return (sh) yh.c(viewGroup);
    }

    @DexIgnore
    public void b(View view) {
        this.a.b(view);
    }

    @DexIgnore
    public void a(View view) {
        this.a.a(view);
    }
}
