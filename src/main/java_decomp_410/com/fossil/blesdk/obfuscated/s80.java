package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.legacy.LegacyFileControlOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s80 extends r80 {
    @DexIgnore
    public byte[] Q;
    @DexIgnore
    public short R;
    @DexIgnore
    public long S;
    @DexIgnore
    public /* final */ int T;
    @DexIgnore
    public /* final */ int U;
    @DexIgnore
    public float V;
    @DexIgnore
    public /* final */ boolean W;
    @DexIgnore
    public /* final */ byte[] X;
    @DexIgnore
    public byte[] Y;
    @DexIgnore
    public /* final */ long Z;
    @DexIgnore
    public /* final */ float a0;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ s80(short s, long j, Peripheral peripheral, int i, float f, int i2, fd4 fd4) {
        this(s, j, peripheral, (i2 & 8) != 0 ? 3 : i, (i2 & 16) != 0 ? 0.001f : f);
    }

    @DexIgnore
    public byte[] D() {
        return this.X;
    }

    @DexIgnore
    public boolean F() {
        return this.W;
    }

    @DexIgnore
    public byte[] G() {
        return this.Y;
    }

    @DexIgnore
    public void J() {
        float length = (((float) this.Q.length) * 1.0f) / ((float) this.Z);
        if (length - this.V > this.a0 || length == 1.0f) {
            this.V = length;
            a(length);
        }
    }

    @DexIgnore
    public final byte[] K() {
        return this.Q;
    }

    @DexIgnore
    public void b(byte[] bArr) {
        kd4.b(bArr, "<set-?>");
        this.Y = bArr;
    }

    @DexIgnore
    public void c(byte[] bArr) {
        kd4.b(bArr, "receivedData");
        d(bArr);
    }

    @DexIgnore
    public final void d(byte[] bArr) {
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        this.R = order.getShort(0);
        this.S = n90.b(order.getInt(bArr.length - 4));
        if (!(this.R == I() || this.S == Crc32Calculator.a.a(bArr, 0, bArr.length - 4, Crc32Calculator.CrcType.CRC32))) {
            b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.INVALID_RESPONSE_DATA, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
        }
        this.Q = bArr;
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(wa0.a(wa0.a(super.t(), JSONKey.TOTAL_FILE_SIZE, Long.valueOf(this.Z)), JSONKey.OFFSET, Integer.valueOf(this.T)), JSONKey.LENGTH, Integer.valueOf(this.U));
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(wa0.a(super.u(), JSONKey.FILE_CRC, Long.valueOf(Crc32Calculator.a.a(this.Q, Crc32Calculator.CrcType.CRC32))), JSONKey.LENGTH, Integer.valueOf(this.Q.length));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public s80(short s, long j, Peripheral peripheral, int i, float f) {
        super(LegacyFileControlOperationCode.LEGACY_GET_FILE, s, RequestId.LEGACY_GET_ACTIVITY_FILE, peripheral, i);
        kd4.b(peripheral, "peripheral");
        this.Z = j;
        this.a0 = f;
        this.Q = new byte[0];
        this.U = 65535;
        byte[] array = ByteBuffer.allocate(11).order(ByteOrder.LITTLE_ENDIAN).put(LegacyFileControlOperationCode.LEGACY_GET_FILE.getCode()).putShort(s).putInt(this.T).putInt(this.U).array();
        kd4.a((Object) array, "ByteBuffer.allocate(11)\n\u2026gth)\n            .array()");
        this.X = array;
        byte[] array2 = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(LegacyFileControlOperationCode.LEGACY_GET_FILE.responseCode()).array();
        kd4.a((Object) array2, "ByteBuffer.allocate(1)\n \u2026e())\n            .array()");
        this.Y = array2;
    }
}
