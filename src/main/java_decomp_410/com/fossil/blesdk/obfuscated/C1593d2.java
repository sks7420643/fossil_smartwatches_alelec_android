package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.d2 */
public class C1593d2 {
    @DexIgnore
    /* renamed from: a */
    public static android.view.inputmethod.InputConnection m5642a(android.view.inputmethod.InputConnection inputConnection, android.view.inputmethod.EditorInfo editorInfo, android.view.View view) {
        if (inputConnection != null && editorInfo.hintText == null) {
            android.view.ViewParent parent = view.getParent();
            while (true) {
                if (!(parent instanceof android.view.View)) {
                    break;
                } else if (parent instanceof com.fossil.blesdk.obfuscated.C1854g3) {
                    editorInfo.hintText = ((com.fossil.blesdk.obfuscated.C1854g3) parent).mo11117a();
                    break;
                } else {
                    parent = parent.getParent();
                }
            }
        }
        return inputConnection;
    }
}
