package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.internal.AnalyticsEvents;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dl1 implements vi1 {
    @DexIgnore
    public static volatile dl1 y;
    @DexIgnore
    public sh1 a;
    @DexIgnore
    public xg1 b;
    @DexIgnore
    public am1 c;
    @DexIgnore
    public dh1 d;
    @DexIgnore
    public zk1 e;
    @DexIgnore
    public tl1 f;
    @DexIgnore
    public /* final */ jl1 g;
    @DexIgnore
    public oj1 h;
    @DexIgnore
    public /* final */ xh1 i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public long m;
    @DexIgnore
    public List<Runnable> n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public FileLock t;
    @DexIgnore
    public FileChannel u;
    @DexIgnore
    public List<Long> v;
    @DexIgnore
    public List<Long> w;
    @DexIgnore
    public long x;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements cm1 {
        @DexIgnore
        public f61 a;
        @DexIgnore
        public List<Long> b;
        @DexIgnore
        public List<c61> c;
        @DexIgnore
        public long d;

        @DexIgnore
        public a(dl1 dl1) {
        }

        @DexIgnore
        public final void a(f61 f61) {
            bk0.a(f61);
            this.a = f61;
        }

        @DexIgnore
        public /* synthetic */ a(dl1 dl1, el1 el1) {
            this(dl1);
        }

        @DexIgnore
        public final boolean a(long j, c61 c61) {
            bk0.a(c61);
            if (this.c == null) {
                this.c = new ArrayList();
            }
            if (this.b == null) {
                this.b = new ArrayList();
            }
            if (this.c.size() > 0 && a(this.c.get(0)) != a(c61)) {
                return false;
            }
            long b2 = this.d + ((long) c61.b());
            if (b2 >= ((long) Math.max(0, jg1.s.a().intValue()))) {
                return false;
            }
            this.d = b2;
            this.c.add(c61);
            this.b.add(Long.valueOf(j));
            if (this.c.size() >= Math.max(1, jg1.t.a().intValue())) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public static long a(c61 c61) {
            return ((c61.e.longValue() / 1000) / 60) / 60;
        }
    }

    @DexIgnore
    public dl1(il1 il1) {
        this(il1, (xh1) null);
    }

    @DexIgnore
    public static dl1 a(Context context) {
        bk0.a(context);
        bk0.a(context.getApplicationContext());
        if (y == null) {
            synchronized (dl1.class) {
                if (y == null) {
                    y = new dl1(new il1(context));
                }
            }
        }
        return y;
    }

    @DexIgnore
    public final void A() {
        this.p++;
    }

    @DexIgnore
    public final xh1 B() {
        return this.i;
    }

    @DexIgnore
    public final ul1 b() {
        return this.i.b();
    }

    @DexIgnore
    public final hm0 c() {
        return this.i.c();
    }

    @DexIgnore
    public final tg1 d() {
        return this.i.d();
    }

    @DexIgnore
    public final void e() {
        this.i.a().e();
        l().z();
        if (this.i.t().e.a() == 0) {
            this.i.t().e.a(this.i.c().b());
        }
        v();
    }

    @DexIgnore
    public final void f() {
        this.i.a().e();
    }

    @DexIgnore
    public final rg1 g() {
        return this.i.r();
    }

    @DexIgnore
    public final Context getContext() {
        return this.i.getContext();
    }

    @DexIgnore
    public final nl1 h() {
        return this.i.s();
    }

    @DexIgnore
    public final xl1 i() {
        return this.i.u();
    }

    @DexIgnore
    public final jl1 j() {
        b((cl1) this.g);
        return this.g;
    }

    @DexIgnore
    public final tl1 k() {
        b((cl1) this.f);
        return this.f;
    }

    @DexIgnore
    public final am1 l() {
        b((cl1) this.c);
        return this.c;
    }

    @DexIgnore
    public final sh1 m() {
        b((cl1) this.a);
        return this.a;
    }

    @DexIgnore
    public final xg1 n() {
        b((cl1) this.b);
        return this.b;
    }

    @DexIgnore
    public final dh1 o() {
        dh1 dh1 = this.d;
        if (dh1 != null) {
            return dh1;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    @DexIgnore
    public final zk1 p() {
        b((cl1) this.e);
        return this.e;
    }

    @DexIgnore
    public final oj1 q() {
        b((cl1) this.h);
        return this.h;
    }

    @DexIgnore
    public final void r() {
        if (!this.j) {
            throw new IllegalStateException("UploadController is not initialized");
        }
    }

    @DexIgnore
    public final long s() {
        long b2 = this.i.c().b();
        fh1 t2 = this.i.t();
        t2.n();
        t2.e();
        long a2 = t2.i.a();
        if (a2 == 0) {
            a2 = 1 + ((long) t2.j().t().nextInt(DateTimeConstants.MILLIS_PER_DAY));
            t2.i.a(a2);
        }
        return ((((b2 + a2) / 1000) / 60) / 60) / 24;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:92|93) */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        r1.i.d().s().a("Failed to parse upload URL. Not uploading. appId", com.fossil.blesdk.obfuscated.tg1.a(r5), r6);
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:92:0x02a0 */
    public final void t() {
        String str;
        f();
        r();
        this.s = true;
        try {
            this.i.b();
            Boolean I = this.i.m().I();
            if (I == null) {
                this.i.d().v().a("Upload data called on the client side before use of service was decided");
            } else if (I.booleanValue()) {
                this.i.d().s().a("Upload called in the client side when service should be used");
                this.s = false;
                w();
            } else if (this.m > 0) {
                v();
                this.s = false;
                w();
            } else {
                f();
                if (this.v != null) {
                    this.i.d().A().a("Uploading requested multiple times");
                    this.s = false;
                    w();
                } else if (!n().t()) {
                    this.i.d().A().a("Network not connected, ignoring upload request");
                    v();
                    this.s = false;
                    w();
                } else {
                    long b2 = this.i.c().b();
                    String str2 = null;
                    a((String) null, b2 - xl1.u());
                    long a2 = this.i.t().e.a();
                    if (a2 != 0) {
                        this.i.d().z().a("Uploading events. Elapsed time since last upload attempt (ms)", Long.valueOf(Math.abs(b2 - a2)));
                    }
                    String x2 = l().x();
                    if (!TextUtils.isEmpty(x2)) {
                        if (this.x == -1) {
                            this.x = l().E();
                        }
                        List<Pair<f61, Long>> a3 = l().a(x2, this.i.u().b(x2, jg1.q), Math.max(0, this.i.u().b(x2, jg1.r)));
                        if (!a3.isEmpty()) {
                            Iterator<Pair<f61, Long>> it = a3.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    str = null;
                                    break;
                                }
                                f61 f61 = (f61) it.next().first;
                                if (!TextUtils.isEmpty(f61.u)) {
                                    str = f61.u;
                                    break;
                                }
                            }
                            if (str != null) {
                                int i2 = 0;
                                while (true) {
                                    if (i2 >= a3.size()) {
                                        break;
                                    }
                                    f61 f612 = (f61) a3.get(i2).first;
                                    if (!TextUtils.isEmpty(f612.u) && !f612.u.equals(str)) {
                                        a3 = a3.subList(0, i2);
                                        break;
                                    }
                                    i2++;
                                }
                            }
                            e61 e61 = new e61();
                            e61.c = new f61[a3.size()];
                            ArrayList arrayList = new ArrayList(a3.size());
                            boolean z = xl1.v() && this.i.u().c(x2);
                            for (int i3 = 0; i3 < e61.c.length; i3++) {
                                e61.c[i3] = (f61) a3.get(i3).first;
                                arrayList.add((Long) a3.get(i3).second);
                                e61.c[i3].t = Long.valueOf(this.i.u().n());
                                e61.c[i3].f = Long.valueOf(b2);
                                f61 f613 = e61.c[i3];
                                this.i.b();
                                f613.B = false;
                                if (!z) {
                                    e61.c[i3].K = null;
                                }
                            }
                            if (this.i.d().a(2)) {
                                str2 = j().b(e61);
                            }
                            byte[] a4 = j().a(e61);
                            String a5 = jg1.A.a();
                            URL url = new URL(a5);
                            bk0.a(!arrayList.isEmpty());
                            if (this.v != null) {
                                this.i.d().s().a("Set uploading progress before finishing the previous upload");
                            } else {
                                this.v = new ArrayList(arrayList);
                            }
                            this.i.t().f.a(b2);
                            String str3 = "?";
                            if (e61.c.length > 0) {
                                str3 = e61.c[0].q;
                            }
                            this.i.d().A().a("Uploading data. app, uncompressed size, data", str3, Integer.valueOf(a4.length), str2);
                            this.r = true;
                            xg1 n2 = n();
                            fl1 fl1 = new fl1(this, x2);
                            n2.e();
                            n2.q();
                            bk0.a(url);
                            bk0.a(a4);
                            bk0.a(fl1);
                            n2.a().b((Runnable) new ch1(n2, x2, url, a4, (Map<String, String>) null, fl1));
                        }
                    } else {
                        this.x = -1;
                        String a6 = l().a(b2 - xl1.u());
                        if (!TextUtils.isEmpty(a6)) {
                            ql1 b3 = l().b(a6);
                            if (b3 != null) {
                                a(b3);
                            }
                        }
                    }
                    this.s = false;
                    w();
                }
            }
        } finally {
            this.s = false;
            w();
        }
    }

    @DexIgnore
    public final boolean u() {
        f();
        r();
        return l().C() || !TextUtils.isEmpty(l().x());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01a0  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01be  */
    public final void v() {
        long j2;
        long j3;
        f();
        r();
        if (z() || this.i.u().a(jg1.o0)) {
            if (this.m > 0) {
                long abs = 3600000 - Math.abs(this.i.c().c() - this.m);
                if (abs > 0) {
                    this.i.d().A().a("Upload has been suspended. Will update scheduling later in approximately ms", Long.valueOf(abs));
                    o().a();
                    p().t();
                    return;
                }
                this.m = 0;
            }
            if (!this.i.H() || !u()) {
                this.i.d().A().a("Nothing to upload or uploading impossible");
                o().a();
                p().t();
                return;
            }
            long b2 = this.i.c().b();
            long max = Math.max(0, jg1.K.a().longValue());
            boolean z = l().D() || l().y();
            if (z) {
                String q2 = this.i.u().q();
                if (TextUtils.isEmpty(q2) || ".none.".equals(q2)) {
                    j2 = Math.max(0, jg1.E.a().longValue());
                } else {
                    j2 = Math.max(0, jg1.F.a().longValue());
                }
            } else {
                j2 = Math.max(0, jg1.D.a().longValue());
            }
            long a2 = this.i.t().e.a();
            long a3 = this.i.t().f.a();
            long j4 = j2;
            long j5 = max;
            long max2 = Math.max(l().A(), l().B());
            if (max2 != 0) {
                long abs2 = b2 - Math.abs(max2 - b2);
                long abs3 = b2 - Math.abs(a2 - b2);
                long abs4 = b2 - Math.abs(a3 - b2);
                long max3 = Math.max(abs3, abs4);
                long j6 = abs2 + j5;
                if (z && max3 > 0) {
                    j6 = Math.min(abs2, max3) + j4;
                }
                long j7 = j4;
                j3 = !j().a(max3, j7) ? max3 + j7 : j6;
                if (abs4 != 0 && abs4 >= abs2) {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= Math.min(20, Math.max(0, jg1.M.a().intValue()))) {
                            break;
                        }
                        j3 += Math.max(0, jg1.L.a().longValue()) * (1 << i2);
                        if (j3 > abs4) {
                            break;
                        }
                        i2++;
                    }
                }
                if (j3 != 0) {
                    this.i.d().A().a("Next upload time is 0");
                    o().a();
                    p().t();
                    return;
                } else if (!n().t()) {
                    this.i.d().A().a("No network");
                    o().b();
                    p().t();
                    return;
                } else {
                    long a4 = this.i.t().g.a();
                    long max4 = Math.max(0, jg1.B.a().longValue());
                    if (!j().a(a4, max4)) {
                        j3 = Math.max(j3, a4 + max4);
                    }
                    o().a();
                    long b3 = j3 - this.i.c().b();
                    if (b3 <= 0) {
                        b3 = Math.max(0, jg1.G.a().longValue());
                        this.i.t().e.a(this.i.c().b());
                    }
                    this.i.d().A().a("Upload scheduled in approximately ms", Long.valueOf(b3));
                    p().a(b3);
                    return;
                }
            }
            j3 = 0;
            if (j3 != 0) {
            }
        }
    }

    @DexIgnore
    public final void w() {
        f();
        if (this.q || this.r || this.s) {
            this.i.d().A().a("Not stopping services. fetch, network, upload", Boolean.valueOf(this.q), Boolean.valueOf(this.r), Boolean.valueOf(this.s));
            return;
        }
        this.i.d().A().a("Stopping uploading service(s)");
        List<Runnable> list = this.n;
        if (list != null) {
            for (Runnable run : list) {
                run.run();
            }
            this.n.clear();
        }
    }

    @DexIgnore
    public final boolean x() {
        f();
        try {
            this.u = new RandomAccessFile(new File(this.i.getContext().getFilesDir(), "google_app_measurement.db"), "rw").getChannel();
            this.t = this.u.tryLock();
            if (this.t != null) {
                this.i.d().A().a("Storage concurrent access okay");
                return true;
            }
            this.i.d().s().a("Storage concurrent data access panic");
            return false;
        } catch (FileNotFoundException e2) {
            this.i.d().s().a("Failed to acquire storage lock", e2);
            return false;
        } catch (IOException e3) {
            this.i.d().s().a("Failed to access storage lock file", e3);
            return false;
        }
    }

    @DexIgnore
    public final void y() {
        f();
        r();
        if (!this.l) {
            this.l = true;
            f();
            r();
            if ((this.i.u().a(jg1.o0) || z()) && x()) {
                int a2 = a(this.u);
                int E = this.i.l().E();
                f();
                if (a2 > E) {
                    this.i.d().s().a("Panic: can't downgrade version. Previous, current version", Integer.valueOf(a2), Integer.valueOf(E));
                } else if (a2 < E) {
                    if (a(E, this.u)) {
                        this.i.d().A().a("Storage version upgraded. Previous, current version", Integer.valueOf(a2), Integer.valueOf(E));
                    } else {
                        this.i.d().s().a("Storage version upgrade failed. Previous, current version", Integer.valueOf(a2), Integer.valueOf(E));
                    }
                }
            }
        }
        if (!this.k && !this.i.u().a(jg1.o0)) {
            this.i.d().y().a("This instance being marked as an uploader");
            this.k = true;
            v();
        }
    }

    @DexIgnore
    public final boolean z() {
        f();
        r();
        return this.k;
    }

    @DexIgnore
    public dl1(il1 il1, xh1 xh1) {
        this.j = false;
        bk0.a(il1);
        this.i = xh1.a(il1.a, (og1) null);
        this.x = -1;
        jl1 jl1 = new jl1(this);
        jl1.s();
        this.g = jl1;
        xg1 xg1 = new xg1(this);
        xg1.s();
        this.b = xg1;
        sh1 sh1 = new sh1(this);
        sh1.s();
        this.a = sh1;
        this.i.a().a((Runnable) new el1(this, il1));
    }

    @DexIgnore
    public static void b(cl1 cl1) {
        if (cl1 == null) {
            throw new IllegalStateException("Upload Component not created");
        } else if (!cl1.p()) {
            String valueOf = String.valueOf(cl1.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0332 A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x035f A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x03eb A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x041b A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01d2 A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01df A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01f1 A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x02d4 A[Catch:{ SQLiteException -> 0x0147, all -> 0x0449 }] */
    public final void c(rl1 rl1) {
        ql1 b2;
        int i2;
        dg1 dg1;
        String str;
        long i3;
        PackageInfo packageInfo;
        int i4;
        ApplicationInfo applicationInfo;
        boolean z;
        am1 l2;
        String f2;
        rl1 rl12 = rl1;
        f();
        r();
        bk0.a(rl1);
        bk0.b(rl12.e);
        if (!TextUtils.isEmpty(rl12.f) || !TextUtils.isEmpty(rl12.v)) {
            ql1 b3 = l().b(rl12.e);
            if (b3 != null && TextUtils.isEmpty(b3.c()) && !TextUtils.isEmpty(rl12.f)) {
                b3.j(0);
                l().a(b3);
                m().e(rl12.e);
            }
            if (!rl12.l) {
                d(rl1);
                return;
            }
            long j2 = rl12.q;
            if (j2 == 0) {
                j2 = this.i.c().b();
            }
            int i5 = rl12.r;
            if (!(i5 == 0 || i5 == 1)) {
                this.i.d().v().a("Incorrect app type, assuming installed app. appId, appType", tg1.a(rl12.e), Integer.valueOf(i5));
                i5 = 0;
            }
            l().t();
            try {
                b2 = l().b(rl12.e);
                if (b2 != null) {
                    this.i.s();
                    if (nl1.a(rl12.f, b2.c(), rl12.v, b2.h())) {
                        this.i.d().v().a("New GMP App Id passed in. Removing cached database data. appId", tg1.a(b2.f()));
                        l2 = l();
                        f2 = b2.f();
                        l2.q();
                        l2.e();
                        bk0.b(f2);
                        SQLiteDatabase v2 = l2.v();
                        String[] strArr = {f2};
                        int delete = v2.delete("events", "app_id=?", strArr) + 0 + v2.delete("user_attributes", "app_id=?", strArr) + v2.delete("conditional_properties", "app_id=?", strArr) + v2.delete("apps", "app_id=?", strArr) + v2.delete("raw_events", "app_id=?", strArr) + v2.delete("raw_events_metadata", "app_id=?", strArr) + v2.delete("event_filters", "app_id=?", strArr) + v2.delete("property_filters", "app_id=?", strArr) + v2.delete("audience_filter_values", "app_id=?", strArr);
                        if (delete > 0) {
                            l2.d().A().a("Deleted application data. app, records", f2, Integer.valueOf(delete));
                        }
                        b2 = null;
                    }
                }
            } catch (SQLiteException e2) {
                l2.d().s().a("Error deleting application data. appId, error", tg1.a(f2), e2);
            } catch (Throwable th) {
                l().u();
                throw th;
            }
            if (b2 != null) {
                if (b2.l() == -2147483648L) {
                    i2 = 1;
                    if (b2.e() != null && !b2.e().equals(rl12.g)) {
                        Bundle bundle = new Bundle();
                        bundle.putString("_pv", b2.e());
                        a(new hg1("_au", new eg1(bundle), "auto", j2), rl12);
                    }
                } else if (b2.l() != rl12.n) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("_pv", b2.e());
                    eg1 eg1 = new eg1(bundle2);
                    i2 = 1;
                    a(new hg1("_au", eg1, "auto", j2), rl12);
                }
                d(rl1);
                if (i5 != 0) {
                    dg1 = l().b(rl12.e, "_f");
                } else {
                    dg1 = i5 == i2 ? l().b(rl12.e, "_v") : null;
                }
                if (dg1 != null) {
                    long j3 = j2;
                    long j4 = ((j2 / 3600000) + 1) * 3600000;
                    if (i5 == 0) {
                        str = "_et";
                        a(new kl1("_fot", j3, Long.valueOf(j4), "auto"), rl12);
                        if (this.i.u().m(rl12.f)) {
                            f();
                            this.i.w().a(rl12.e);
                        }
                        f();
                        r();
                        Bundle bundle3 = new Bundle();
                        bundle3.putLong("_c", 1);
                        bundle3.putLong("_r", 1);
                        bundle3.putLong("_uwa", 0);
                        bundle3.putLong("_pfo", 0);
                        bundle3.putLong("_sys", 0);
                        bundle3.putLong("_sysu", 0);
                        if (this.i.u().s(rl12.e)) {
                            bundle3.putLong(str, 1);
                        }
                        if (this.i.u().i(rl12.e) && rl12.u) {
                            bundle3.putLong("_dac", 1);
                        }
                        if (this.i.getContext().getPackageManager() == null) {
                            this.i.d().s().a("PackageManager is null, first open report might be inaccurate. appId", tg1.a(rl12.e));
                        } else {
                            try {
                                try {
                                    packageInfo = bn0.b(this.i.getContext()).b(rl12.e, 0);
                                } catch (PackageManager.NameNotFoundException e3) {
                                    e = e3;
                                    this.i.d().s().a("Package info is null, first open report might be inaccurate. appId", tg1.a(rl12.e), e);
                                    packageInfo = null;
                                    if (packageInfo != null) {
                                    }
                                    i4 = 0;
                                    applicationInfo = bn0.b(this.i.getContext()).a(rl12.e, i4);
                                    if (applicationInfo != null) {
                                    }
                                    am1 l3 = l();
                                    String str2 = rl12.e;
                                    bk0.b(str2);
                                    l3.e();
                                    l3.q();
                                    i3 = l3.i(str2, "first_open_count");
                                    if (i3 >= 0) {
                                    }
                                    a(new hg1("_f", new eg1(bundle3), "auto", j3), rl12);
                                    if (!this.i.u().d(rl12.e, jg1.m0)) {
                                    }
                                    l().w();
                                    l().u();
                                }
                            } catch (PackageManager.NameNotFoundException e4) {
                                e = e4;
                                this.i.d().s().a("Package info is null, first open report might be inaccurate. appId", tg1.a(rl12.e), e);
                                packageInfo = null;
                                if (packageInfo != null) {
                                }
                                i4 = 0;
                                applicationInfo = bn0.b(this.i.getContext()).a(rl12.e, i4);
                                if (applicationInfo != null) {
                                }
                                am1 l32 = l();
                                String str22 = rl12.e;
                                bk0.b(str22);
                                l32.e();
                                l32.q();
                                i3 = l32.i(str22, "first_open_count");
                                if (i3 >= 0) {
                                }
                                a(new hg1("_f", new eg1(bundle3), "auto", j3), rl12);
                                if (!this.i.u().d(rl12.e, jg1.m0)) {
                                }
                                l().w();
                                l().u();
                            }
                            if (packageInfo != null || packageInfo.firstInstallTime == 0) {
                                i4 = 0;
                            } else {
                                if (packageInfo.firstInstallTime != packageInfo.lastUpdateTime) {
                                    bundle3.putLong("_uwa", 1);
                                    z = false;
                                } else {
                                    z = true;
                                }
                                i4 = 0;
                                a(new kl1("_fi", j3, Long.valueOf(z ? 1 : 0), "auto"), rl12);
                            }
                            try {
                                applicationInfo = bn0.b(this.i.getContext()).a(rl12.e, i4);
                            } catch (PackageManager.NameNotFoundException e5) {
                                this.i.d().s().a("Application info is null, first open report might be inaccurate. appId", tg1.a(rl12.e), e5);
                                applicationInfo = null;
                            }
                            if (applicationInfo != null) {
                                if ((applicationInfo.flags & 1) != 0) {
                                    bundle3.putLong("_sys", 1);
                                }
                                if ((applicationInfo.flags & 128) != 0) {
                                    bundle3.putLong("_sysu", 1);
                                }
                            }
                        }
                        am1 l322 = l();
                        String str222 = rl12.e;
                        bk0.b(str222);
                        l322.e();
                        l322.q();
                        i3 = l322.i(str222, "first_open_count");
                        if (i3 >= 0) {
                            bundle3.putLong("_pfo", i3);
                        }
                        a(new hg1("_f", new eg1(bundle3), "auto", j3), rl12);
                    } else {
                        str = "_et";
                        if (i5 == 1) {
                            a(new kl1("_fvt", j3, Long.valueOf(j4), "auto"), rl12);
                            f();
                            r();
                            Bundle bundle4 = new Bundle();
                            bundle4.putLong("_c", 1);
                            bundle4.putLong("_r", 1);
                            if (this.i.u().s(rl12.e)) {
                                bundle4.putLong(str, 1);
                            }
                            if (this.i.u().i(rl12.e) && rl12.u) {
                                bundle4.putLong("_dac", 1);
                            }
                            a(new hg1("_v", new eg1(bundle4), "auto", j3), rl12);
                        }
                    }
                    if (!this.i.u().d(rl12.e, jg1.m0)) {
                        Bundle bundle5 = new Bundle();
                        bundle5.putLong(str, 1);
                        if (this.i.u().s(rl12.e)) {
                            bundle5.putLong("_fr", 1);
                        }
                        a(new hg1("_e", new eg1(bundle5), "auto", j3), rl12);
                    }
                } else {
                    long j5 = j2;
                    if (rl12.m) {
                        a(new hg1("_cd", new eg1(new Bundle()), "auto", j5), rl12);
                    }
                }
                l().w();
                l().u();
            }
            i2 = 1;
            d(rl1);
            if (i5 != 0) {
            }
            if (dg1 != null) {
            }
            l().w();
            l().u();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0140  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x014e  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0156  */
    public final ql1 d(rl1 rl1) {
        boolean z;
        f();
        r();
        bk0.a(rl1);
        bk0.b(rl1.e);
        ql1 b2 = l().b(rl1.e);
        String b3 = this.i.t().b(rl1.e);
        if (b2 == null) {
            b2 = new ql1(this.i, rl1.e);
            b2.b(this.i.s().v());
            b2.e(b3);
        } else if (!b3.equals(b2.i())) {
            b2.e(b3);
            b2.b(this.i.s().v());
        } else {
            z = false;
            if (!TextUtils.equals(rl1.f, b2.c())) {
                b2.c(rl1.f);
                z = true;
            }
            if (!TextUtils.equals(rl1.v, b2.h())) {
                b2.d(rl1.v);
                z = true;
            }
            if (!TextUtils.isEmpty(rl1.o) && !rl1.o.equals(b2.b())) {
                b2.f(rl1.o);
                z = true;
            }
            long j2 = rl1.i;
            if (!(j2 == 0 || j2 == b2.n())) {
                b2.g(rl1.i);
                z = true;
            }
            if (!TextUtils.isEmpty(rl1.g) && !rl1.g.equals(b2.e())) {
                b2.a(rl1.g);
                z = true;
            }
            if (rl1.n != b2.l()) {
                b2.f(rl1.n);
                z = true;
            }
            String str = rl1.h;
            if (str != null && !str.equals(b2.m())) {
                b2.g(rl1.h);
                z = true;
            }
            if (rl1.j != b2.o()) {
                b2.h(rl1.j);
                z = true;
            }
            if (rl1.l != b2.d()) {
                b2.a(rl1.l);
                z = true;
            }
            if (!TextUtils.isEmpty(rl1.k) && !rl1.k.equals(b2.z())) {
                b2.h(rl1.k);
                z = true;
            }
            if (rl1.p != b2.B()) {
                b2.c(rl1.p);
                z = true;
            }
            if (rl1.s != b2.C()) {
                b2.b(rl1.s);
                z = true;
            }
            if (rl1.t != b2.D()) {
                b2.c(rl1.t);
                z = true;
            }
            if (z) {
                l().a(b2);
            }
            return b2;
        }
        z = true;
        if (!TextUtils.equals(rl1.f, b2.c())) {
        }
        if (!TextUtils.equals(rl1.v, b2.h())) {
        }
        b2.f(rl1.o);
        z = true;
        long j22 = rl1.i;
        b2.g(rl1.i);
        z = true;
        b2.a(rl1.g);
        z = true;
        if (rl1.n != b2.l()) {
        }
        String str2 = rl1.h;
        b2.g(rl1.h);
        z = true;
        if (rl1.j != b2.o()) {
        }
        if (rl1.l != b2.d()) {
        }
        b2.h(rl1.k);
        z = true;
        if (rl1.p != b2.B()) {
        }
        if (rl1.s != b2.C()) {
        }
        if (rl1.t != b2.D()) {
        }
        if (z) {
        }
        return b2;
    }

    @DexIgnore
    public final void b(rl1 rl1) {
        f();
        r();
        bk0.b(rl1.e);
        d(rl1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:194:0x074d, code lost:
        r2 = true;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0261 A[Catch:{ SQLiteException -> 0x022e, all -> 0x0800 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0299 A[Catch:{ SQLiteException -> 0x022e, all -> 0x0800 }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x02e6 A[Catch:{ SQLiteException -> 0x022e, all -> 0x0800 }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0313  */
    public final void b(hg1 hg1, rl1 rl1) {
        long j2;
        long intValue;
        bm1 bm1;
        dg1 dg1;
        long j3;
        ml1 ml1;
        hg1 hg12 = hg1;
        rl1 rl12 = rl1;
        bk0.a(rl1);
        bk0.b(rl12.e);
        long nanoTime = System.nanoTime();
        f();
        r();
        String str = rl12.e;
        if (j().a(hg12, rl12)) {
            if (!rl12.l) {
                d(rl12);
                return;
            }
            boolean z = false;
            if (m().b(str, hg12.e)) {
                this.i.d().v().a("Dropping blacklisted event. appId", tg1.a(str), this.i.r().a(hg12.e));
                if (m().g(str) || m().h(str)) {
                    z = true;
                }
                if (!z && !"_err".equals(hg12.e)) {
                    this.i.s().a(str, 11, "_ev", hg12.e, 0);
                }
                if (z) {
                    ql1 b2 = l().b(str);
                    if (b2 != null) {
                        if (Math.abs(this.i.c().b() - Math.max(b2.r(), b2.q())) > jg1.J.a().longValue()) {
                            this.i.d().z().a("Fetching config for blacklisted app");
                            a(b2);
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            if (this.i.d().a(2)) {
                this.i.d().A().a("Logging event", this.i.r().a(hg12));
            }
            l().t();
            d(rl12);
            if (!"_iap".equals(hg12.e)) {
                if (!"ecommerce_purchase".equals(hg12.e)) {
                    j2 = nanoTime;
                    boolean e2 = nl1.e(hg12.e);
                    boolean equals = "_err".equals(hg12.e);
                    String str2 = str;
                    bm1 a2 = l().a(s(), str, true, e2, false, equals, false);
                    intValue = a2.b - ((long) jg1.u.a().intValue());
                    if (intValue > 0) {
                        if (intValue % 1000 == 1) {
                            this.i.d().s().a("Data loss. Too many events logged. appId, count", tg1.a(str2), Long.valueOf(a2.b));
                        }
                        l().w();
                        l().u();
                        return;
                    }
                    if (e2) {
                        bm1 bm12 = a2;
                        long intValue2 = a2.a - ((long) jg1.w.a().intValue());
                        if (intValue2 > 0) {
                            if (intValue2 % 1000 == 1) {
                                this.i.d().s().a("Data loss. Too many public events logged. appId, count", tg1.a(str2), Long.valueOf(bm12.a));
                            }
                            this.i.s().a(str2, 16, "_ev", hg12.e, 0);
                            l().w();
                            l().u();
                            return;
                        }
                        bm1 = bm12;
                    } else {
                        bm1 = a2;
                    }
                    if (equals) {
                        long max = bm1.d - ((long) Math.max(0, Math.min(1000000, this.i.u().b(rl12.e, jg1.v))));
                        if (max > 0) {
                            if (max == 1) {
                                this.i.d().s().a("Too many error events logged. appId, count", tg1.a(str2), Long.valueOf(bm1.d));
                            }
                            l().w();
                            l().u();
                            return;
                        }
                    }
                    Bundle H = hg12.f.H();
                    this.i.s().a(H, "_o", (Object) hg12.g);
                    String str3 = str2;
                    if (this.i.s().c(str3)) {
                        this.i.s().a(H, "_dbg", (Object) 1L);
                        this.i.s().a(H, "_r", (Object) 1L);
                    }
                    if (this.i.u().p(rl12.e) && "_s".equals(hg12.e)) {
                        ml1 d2 = l().d(rl12.e, "_sno");
                        if (d2 != null && (d2.e instanceof Long)) {
                            this.i.s().a(H, "_sno", d2.e);
                        }
                    }
                    long c2 = l().c(str3);
                    if (c2 > 0) {
                        this.i.d().v().a("Data lost. Too many events stored on disk, deleted. appId", tg1.a(str3), Long.valueOf(c2));
                    }
                    xh1 xh1 = this.i;
                    String str4 = hg12.g;
                    String str5 = hg12.e;
                    long j4 = hg12.h;
                    String str6 = "_r";
                    String str7 = str4;
                    String str8 = str3;
                    cg1 cg1 = new cg1(xh1, str7, str3, str5, j4, 0, H);
                    dg1 b3 = l().b(str8, cg1.b);
                    if (b3 != null) {
                        cg1 = cg1.a(this.i, b3.e);
                        dg1 = b3.a(cg1.d);
                    } else if (l().f(str8) < 500 || !e2) {
                        dg1 = new dg1(str8, cg1.b, 0, 0, cg1.d, 0, (Long) null, (Long) null, (Long) null, (Boolean) null);
                    } else {
                        this.i.d().s().a("Too many event names used, ignoring event. appId, name, supported count", tg1.a(str8), this.i.r().a(cg1.b), 500);
                        this.i.s().a(str8, 8, (String) null, (String) null, 0);
                        l().u();
                        return;
                    }
                    l().a(dg1);
                    f();
                    r();
                    bk0.a(cg1);
                    bk0.a(rl1);
                    bk0.b(cg1.a);
                    bk0.a(cg1.a.equals(rl12.e));
                    f61 f61 = new f61();
                    f61.c = 1;
                    f61.k = "android";
                    f61.q = rl12.e;
                    f61.p = rl12.h;
                    f61.r = rl12.g;
                    f61.E = rl12.n == -2147483648L ? null : Integer.valueOf((int) rl12.n);
                    f61.s = Long.valueOf(rl12.i);
                    f61.A = rl12.f;
                    f61.N = rl12.v;
                    f61.x = rl12.j == 0 ? null : Long.valueOf(rl12.j);
                    if (this.i.u().d(rl12.e, jg1.p0)) {
                        f61.P = j().t();
                    }
                    Pair<String, Boolean> a3 = this.i.t().a(rl12.e);
                    if (a3 == null || TextUtils.isEmpty((CharSequence) a3.first)) {
                        if (!this.i.q().a(this.i.getContext()) && rl12.t) {
                            String string = Settings.Secure.getString(this.i.getContext().getContentResolver(), "android_id");
                            if (string == null) {
                                this.i.d().v().a("null secure ID. appId", tg1.a(f61.q));
                                string = "null";
                            } else if (string.isEmpty()) {
                                this.i.d().v().a("empty secure ID. appId", tg1.a(f61.q));
                            }
                            f61.H = string;
                        }
                    } else if (rl12.s) {
                        f61.u = (String) a3.first;
                        f61.v = (Boolean) a3.second;
                    }
                    this.i.q().n();
                    f61.m = Build.MODEL;
                    this.i.q().n();
                    f61.l = Build.VERSION.RELEASE;
                    f61.o = Integer.valueOf((int) this.i.q().s());
                    f61.n = this.i.q().t();
                    f61.t = null;
                    f61.f = null;
                    f61.g = null;
                    f61.h = null;
                    f61.J = Long.valueOf(rl12.p);
                    if (this.i.e() && xl1.v()) {
                        f61.K = null;
                    }
                    ql1 b4 = l().b(rl12.e);
                    if (b4 == null) {
                        b4 = new ql1(this.i, rl12.e);
                        b4.b(this.i.s().v());
                        b4.f(rl12.o);
                        b4.c(rl12.f);
                        b4.e(this.i.t().b(rl12.e));
                        b4.i(0);
                        b4.d(0);
                        b4.e(0);
                        b4.a(rl12.g);
                        b4.f(rl12.n);
                        b4.g(rl12.h);
                        b4.g(rl12.i);
                        b4.h(rl12.j);
                        b4.a(rl12.l);
                        b4.c(rl12.p);
                        l().a(b4);
                    }
                    f61.w = b4.a();
                    f61.D = b4.b();
                    List<ml1> a4 = l().a(rl12.e);
                    f61.e = new i61[a4.size()];
                    for (int i2 = 0; i2 < a4.size(); i2++) {
                        i61 i61 = new i61();
                        f61.e[i2] = i61;
                        i61.d = a4.get(i2).c;
                        i61.c = Long.valueOf(a4.get(i2).d);
                        j().a(i61, a4.get(i2).e);
                    }
                    try {
                        long a5 = l().a(f61);
                        am1 l2 = l();
                        if (cg1.f != null) {
                            Iterator<String> it = cg1.f.iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    if (str6.equals(it.next())) {
                                        break;
                                    }
                                } else {
                                    boolean c3 = m().c(cg1.a, cg1.b);
                                    bm1 a6 = l().a(s(), cg1.a, false, false, false, false, false);
                                    if (c3 && a6.e < ((long) this.i.u().a(cg1.a))) {
                                    }
                                }
                            }
                        }
                        boolean z2 = false;
                        if (l2.a(cg1, a5, z2)) {
                            this.m = 0;
                        }
                    } catch (IOException e3) {
                        this.i.d().s().a("Data loss. Failed to insert raw event metadata. appId", tg1.a(f61.q), e3);
                    }
                    l().w();
                    if (this.i.d().a(2)) {
                        this.i.d().A().a("Event recorded", this.i.r().a(cg1));
                    }
                    l().u();
                    v();
                    this.i.d().A().a("Background event processing time, ms", Long.valueOf(((System.nanoTime() - j2) + 500000) / 1000000));
                    return;
                }
            }
            String g2 = hg12.f.g("currency");
            if ("ecommerce_purchase".equals(hg12.e)) {
                double doubleValue = hg12.f.h("value").doubleValue() * 1000000.0d;
                if (doubleValue == 0.0d) {
                    doubleValue = ((double) hg12.f.f("value").longValue()) * 1000000.0d;
                }
                if (doubleValue > 9.223372036854776E18d || doubleValue < -9.223372036854776E18d) {
                    this.i.d().v().a("Data lost. Currency value is too big. appId", tg1.a(str), Double.valueOf(doubleValue));
                    j2 = nanoTime;
                    if (!z) {
                        l().w();
                        l().u();
                        return;
                    }
                    boolean e22 = nl1.e(hg12.e);
                    boolean equals2 = "_err".equals(hg12.e);
                    String str22 = str;
                    bm1 a22 = l().a(s(), str, true, e22, false, equals2, false);
                    intValue = a22.b - ((long) jg1.u.a().intValue());
                    if (intValue > 0) {
                    }
                } else {
                    j3 = Math.round(doubleValue);
                }
            } else {
                j3 = hg12.f.f("value").longValue();
            }
            if (!TextUtils.isEmpty(g2)) {
                String upperCase = g2.toUpperCase(Locale.US);
                if (upperCase.matches("[A-Z]{3}")) {
                    String valueOf = String.valueOf(upperCase);
                    String concat = valueOf.length() != 0 ? "_ltv_".concat(valueOf) : new String("_ltv_");
                    ml1 d3 = l().d(str, concat);
                    if (d3 != null) {
                        if (d3.e instanceof Long) {
                            j2 = nanoTime;
                            ml1 = new ml1(str, hg12.g, concat, this.i.c().b(), Long.valueOf(((Long) d3.e).longValue() + j3));
                            if (!l().a(ml1)) {
                                this.i.d().s().a("Too many unique user properties are set. Ignoring user property. appId", tg1.a(str), this.i.r().c(ml1.c), ml1.e);
                                this.i.s().a(str, 9, (String) null, (String) null, 0);
                            }
                            z = true;
                            if (!z) {
                            }
                            boolean e222 = nl1.e(hg12.e);
                            boolean equals22 = "_err".equals(hg12.e);
                            String str222 = str;
                            bm1 a222 = l().a(s(), str, true, e222, false, equals22, false);
                            intValue = a222.b - ((long) jg1.u.a().intValue());
                            if (intValue > 0) {
                            }
                        }
                    }
                    j2 = nanoTime;
                    am1 l3 = l();
                    int b5 = this.i.u().b(str, jg1.O) - 1;
                    bk0.b(str);
                    l3.e();
                    l3.q();
                    try {
                        l3.v().execSQL("delete from user_attributes where app_id=? and name in (select name from user_attributes where app_id=? and name like '_ltv_%' order by set_timestamp desc limit ?,10);", new String[]{str, str, String.valueOf(b5)});
                    } catch (SQLiteException e4) {
                        l3.d().s().a("Error pruning currencies. appId", tg1.a(str), e4);
                    } catch (Throwable th) {
                        l().u();
                        throw th;
                    }
                    ml1 = new ml1(str, hg12.g, concat, this.i.c().b(), Long.valueOf(j3));
                    if (!l().a(ml1)) {
                    }
                    z = true;
                    if (!z) {
                    }
                    boolean e2222 = nl1.e(hg12.e);
                    boolean equals222 = "_err".equals(hg12.e);
                    String str2222 = str;
                    bm1 a2222 = l().a(s(), str, true, e2222, false, equals222, false);
                    intValue = a2222.b - ((long) jg1.u.a().intValue());
                    if (intValue > 0) {
                    }
                }
            }
            j2 = nanoTime;
            z = true;
            if (!z) {
            }
            boolean e22222 = nl1.e(hg12.e);
            boolean equals2222 = "_err".equals(hg12.e);
            String str22222 = str;
            bm1 a22222 = l().a(s(), str, true, e22222, false, equals2222, false);
            intValue = a22222.b - ((long) jg1.u.a().intValue());
            if (intValue > 0) {
            }
        }
    }

    @DexIgnore
    public final void a(il1 il1) {
        this.i.a().e();
        am1 am1 = new am1(this);
        am1.s();
        this.c = am1;
        this.i.u().a((zl1) this.a);
        tl1 tl1 = new tl1(this);
        tl1.s();
        this.f = tl1;
        oj1 oj1 = new oj1(this);
        oj1.s();
        this.h = oj1;
        zk1 zk1 = new zk1(this);
        zk1.s();
        this.e = zk1;
        this.d = new dh1(this);
        if (this.o != this.p) {
            this.i.d().s().a("Not all upload components initialized", Integer.valueOf(this.o), Integer.valueOf(this.p));
        }
        this.j = true;
    }

    @DexIgnore
    public final String e(rl1 rl1) {
        try {
            return (String) this.i.a().a(new hl1(this, rl1)).get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e2) {
            this.i.d().s().a("Failed to get app instance id. appId", tg1.a(rl1.e), e2);
            return null;
        }
    }

    @DexIgnore
    public final th1 a() {
        return this.i.a();
    }

    @DexIgnore
    public final void a(hg1 hg1, String str) {
        hg1 hg12 = hg1;
        String str2 = str;
        ql1 b2 = l().b(str2);
        if (b2 == null || TextUtils.isEmpty(b2.e())) {
            this.i.d().z().a("No app data available; dropping event", str2);
            return;
        }
        Boolean b3 = b(b2);
        if (b3 == null) {
            if (!"_ui".equals(hg12.e)) {
                this.i.d().v().a("Could not find package. appId", tg1.a(str));
            }
        } else if (!b3.booleanValue()) {
            this.i.d().s().a("App version does not match; dropping event. appId", tg1.a(str));
            return;
        }
        rl1 rl1 = r2;
        ql1 ql1 = b2;
        rl1 rl12 = new rl1(str, b2.c(), b2.e(), b2.l(), b2.m(), b2.n(), b2.o(), (String) null, b2.d(), false, ql1.b(), ql1.B(), 0, 0, ql1.C(), ql1.D(), false, ql1.h());
        a(hg12, rl1);
    }

    @DexIgnore
    public final void a(hg1 hg1, rl1 rl1) {
        List<vl1> list;
        List<vl1> list2;
        List<vl1> list3;
        hg1 hg12 = hg1;
        rl1 rl12 = rl1;
        bk0.a(rl1);
        bk0.b(rl12.e);
        f();
        r();
        String str = rl12.e;
        long j2 = hg12.h;
        if (j().a(hg12, rl12)) {
            if (!rl12.l) {
                d(rl12);
                return;
            }
            l().t();
            try {
                am1 l2 = l();
                bk0.b(str);
                l2.e();
                l2.q();
                int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
                if (i2 < 0) {
                    l2.d().v().a("Invalid time querying timed out conditional properties", tg1.a(str), Long.valueOf(j2));
                    list = Collections.emptyList();
                } else {
                    list = l2.b("active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout", new String[]{str, String.valueOf(j2)});
                }
                for (vl1 next : list) {
                    if (next != null) {
                        this.i.d().z().a("User property timed out", next.e, this.i.r().c(next.g.f), next.g.H());
                        if (next.k != null) {
                            b(new hg1(next.k, j2), rl12);
                        }
                        l().f(str, next.g.f);
                    }
                }
                am1 l3 = l();
                bk0.b(str);
                l3.e();
                l3.q();
                if (i2 < 0) {
                    l3.d().v().a("Invalid time querying expired conditional properties", tg1.a(str), Long.valueOf(j2));
                    list2 = Collections.emptyList();
                } else {
                    list2 = l3.b("active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live", new String[]{str, String.valueOf(j2)});
                }
                ArrayList arrayList = new ArrayList(list2.size());
                for (vl1 next2 : list2) {
                    if (next2 != null) {
                        this.i.d().z().a("User property expired", next2.e, this.i.r().c(next2.g.f), next2.g.H());
                        l().c(str, next2.g.f);
                        if (next2.o != null) {
                            arrayList.add(next2.o);
                        }
                        l().f(str, next2.g.f);
                    }
                }
                int size = arrayList.size();
                int i3 = 0;
                while (i3 < size) {
                    Object obj = arrayList.get(i3);
                    i3++;
                    b(new hg1((hg1) obj, j2), rl12);
                }
                am1 l4 = l();
                String str2 = hg12.e;
                bk0.b(str);
                bk0.b(str2);
                l4.e();
                l4.q();
                if (i2 < 0) {
                    l4.d().v().a("Invalid time querying triggered conditional properties", tg1.a(str), l4.i().a(str2), Long.valueOf(j2));
                    list3 = Collections.emptyList();
                } else {
                    list3 = l4.b("active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout", new String[]{str, str2, String.valueOf(j2)});
                }
                ArrayList arrayList2 = new ArrayList(list3.size());
                for (vl1 next3 : list3) {
                    if (next3 != null) {
                        kl1 kl1 = next3.g;
                        ml1 ml1 = r4;
                        ml1 ml12 = new ml1(next3.e, next3.f, kl1.f, j2, kl1.H());
                        if (l().a(ml1)) {
                            this.i.d().z().a("User property triggered", next3.e, this.i.r().c(ml1.c), ml1.e);
                        } else {
                            this.i.d().s().a("Too many active user properties, ignoring", tg1.a(next3.e), this.i.r().c(ml1.c), ml1.e);
                        }
                        if (next3.m != null) {
                            arrayList2.add(next3.m);
                        }
                        next3.g = new kl1(ml1);
                        next3.i = true;
                        l().a(next3);
                    }
                }
                b(hg1, rl1);
                int size2 = arrayList2.size();
                int i4 = 0;
                while (i4 < size2) {
                    Object obj2 = arrayList2.get(i4);
                    i4++;
                    b(new hg1((hg1) obj2, j2), rl12);
                }
                l().w();
            } finally {
                l().u();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0241, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0242, code lost:
        r6 = r4;
        r8 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0042, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0043, code lost:
        r5 = r1;
        r8 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0047, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0048, code lost:
        r6 = null;
        r8 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:482:0x0af4, code lost:
        if (r24 != r14) goto L_0x0af6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:579:?, code lost:
        r8.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0042 A[ExcHandler: all (th java.lang.Throwable), PHI: r4 
  PHI: (r4v141 android.database.Cursor) = (r4v136 android.database.Cursor), (r4v136 android.database.Cursor), (r4v136 android.database.Cursor), (r4v147 android.database.Cursor), (r4v147 android.database.Cursor), (r4v147 android.database.Cursor), (r4v147 android.database.Cursor), (r4v0 android.database.Cursor), (r4v0 android.database.Cursor) binds: [B:46:0x00da, B:52:0x00e7, B:53:?, B:23:0x007c, B:29:0x0089, B:31:0x008d, B:32:?, B:9:0x0033, B:10:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:9:0x0033] */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0263 A[SYNTHETIC, Splitter:B:128:0x0263] */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x026a A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x0278 A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x039d A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x03a8 A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x03a9 A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:294:0x0663 A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:324:0x06e5 A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:367:0x0834 A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:373:0x084c A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:376:0x086c A[Catch:{ IOException -> 0x0220, all -> 0x0d97 }] */
    /* JADX WARNING: Removed duplicated region for block: B:489:0x0b13 A[Catch:{ all -> 0x0d76 }] */
    /* JADX WARNING: Removed duplicated region for block: B:493:0x0b60 A[Catch:{ all -> 0x0d76 }] */
    /* JADX WARNING: Removed duplicated region for block: B:570:0x0d7a  */
    /* JADX WARNING: Removed duplicated region for block: B:578:0x0d91 A[SYNTHETIC, Splitter:B:578:0x0d91] */
    /* JADX WARNING: Removed duplicated region for block: B:614:0x0849 A[SYNTHETIC] */
    public final boolean a(String str, long j2) {
        am1 l2;
        Cursor cursor;
        Throwable th;
        boolean z;
        boolean z2;
        f61 f61;
        dl1 dl1;
        am1 l3;
        SecureRandom secureRandom;
        f61 f612;
        int i2;
        int i3;
        c61[] c61Arr;
        HashMap hashMap;
        long j3;
        boolean z3;
        HashMap hashMap2;
        Long l4;
        boolean z4;
        long j4;
        ml1 ml1;
        int i4;
        boolean z5;
        boolean z6;
        boolean z7;
        c61 c61;
        long j5;
        c61 c612;
        boolean z8;
        char c2;
        boolean z9;
        String str2;
        Cursor cursor2;
        SQLiteException sQLiteException;
        String str3;
        String str4;
        String[] strArr;
        String str5;
        String[] strArr2;
        dl1 dl12 = this;
        String str6 = "_lte";
        l().t();
        try {
            Cursor cursor3 = null;
            a aVar = new a(dl12, (el1) null);
            l2 = l();
            long j6 = dl12.x;
            bk0.a(aVar);
            l2.e();
            l2.q();
            try {
                SQLiteDatabase v2 = l2.v();
                if (TextUtils.isEmpty((CharSequence) null)) {
                    int i5 = (j6 > -1 ? 1 : (j6 == -1 ? 0 : -1));
                    if (i5 != 0) {
                        try {
                            strArr2 = new String[]{String.valueOf(j6), String.valueOf(j2)};
                        } catch (SQLiteException e2) {
                            e = e2;
                            cursor2 = cursor3;
                        } catch (Throwable th2) {
                        }
                    } else {
                        strArr2 = new String[]{String.valueOf(j2)};
                    }
                    String str7 = i5 != 0 ? "rowid <= ? and " : "";
                    StringBuilder sb = new StringBuilder(str7.length() + 148);
                    sb.append("select app_id, metadata_fingerprint from raw_events where ");
                    sb.append(str7);
                    sb.append("app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;");
                    cursor3 = v2.rawQuery(sb.toString(), strArr2);
                    if (!cursor3.moveToFirst()) {
                        if (cursor3 != null) {
                            cursor3.close();
                        }
                        if (aVar.c != null) {
                            if (!aVar.c.isEmpty()) {
                                z = false;
                                if (z) {
                                    f61 f613 = aVar.a;
                                    f613.d = new c61[aVar.c.size()];
                                    boolean e3 = dl12.i.u().e(f613.q);
                                    boolean d2 = dl12.i.u().d(aVar.a.q, jg1.m0);
                                    c61 c613 = null;
                                    int i6 = 0;
                                    int i7 = 0;
                                    c61 c614 = null;
                                    boolean z10 = false;
                                    long j7 = 0;
                                    while (true) {
                                        z2 = z10;
                                        if (i6 >= aVar.c.size()) {
                                            break;
                                        }
                                        c61 c615 = aVar.c.get(i6);
                                        String str8 = str6;
                                        int i8 = i6;
                                        int i9 = i7;
                                        if (m().b(aVar.a.q, c615.d)) {
                                            f61 f614 = f613;
                                            dl12.i.d().v().a("Dropping blacklisted raw event. appId", tg1.a(aVar.a.q), dl12.i.r().a(c615.d));
                                            if (!m().g(aVar.a.q)) {
                                                if (!m().h(aVar.a.q)) {
                                                    z9 = false;
                                                    if (!z9 && !"_err".equals(c615.d)) {
                                                        dl12.i.s().a(aVar.a.q, 11, "_ev", c615.d, 0);
                                                    }
                                                    z7 = e3;
                                                    z6 = d2;
                                                    z10 = z2;
                                                    f613 = f614;
                                                }
                                            }
                                            z9 = true;
                                            dl12.i.s().a(aVar.a.q, 11, "_ev", c615.d, 0);
                                            z7 = e3;
                                            z6 = d2;
                                            z10 = z2;
                                            f613 = f614;
                                        } else {
                                            f61 f615 = f613;
                                            boolean c3 = m().c(aVar.a.q, c615.d);
                                            if (!c3) {
                                                j();
                                                String str9 = c615.d;
                                                bk0.b(str9);
                                                j5 = j7;
                                                int hashCode = str9.hashCode();
                                                if (hashCode != 94660) {
                                                    if (hashCode != 95025) {
                                                        if (hashCode == 95027) {
                                                            if (str9.equals("_ui")) {
                                                                c2 = 1;
                                                                if (c2 != 0 || c2 == 1 || c2 == 2) {
                                                                    z7 = e3;
                                                                    z6 = d2;
                                                                    c61 = c614;
                                                                    if (!dl12.i.u().l(aVar.a.q) && c3) {
                                                                        d61[] d61Arr = c615.c;
                                                                        int i10 = -1;
                                                                        int i11 = -1;
                                                                        for (int i12 = 0; i12 < d61Arr.length; i12++) {
                                                                            if ("value".equals(d61Arr[i12].c)) {
                                                                                i10 = i12;
                                                                            } else if ("currency".equals(d61Arr[i12].c)) {
                                                                                i11 = i12;
                                                                            }
                                                                        }
                                                                        if (i10 != -1) {
                                                                            if (d61Arr[i10].e == null && d61Arr[i10].g == null) {
                                                                                dl12.i.d().x().a("Value must be specified with a numeric type.");
                                                                                d61Arr = a(a(a(d61Arr, i10), "_c"), 18, "value");
                                                                            } else {
                                                                                if (i11 != -1) {
                                                                                    String str10 = d61Arr[i11].d;
                                                                                    if (str10 != null) {
                                                                                        if (str10.length() == 3) {
                                                                                            int i13 = 0;
                                                                                            while (i13 < str10.length()) {
                                                                                                int codePointAt = str10.codePointAt(i13);
                                                                                                if (Character.isLetter(codePointAt)) {
                                                                                                    i13 += Character.charCount(codePointAt);
                                                                                                }
                                                                                            }
                                                                                            z8 = false;
                                                                                        }
                                                                                    }
                                                                                    z8 = true;
                                                                                    break;
                                                                                }
                                                                                z8 = true;
                                                                                if (z8) {
                                                                                    dl12.i.d().x().a("Value parameter discarded. You must also supply a 3-letter ISO_4217 currency code in the currency parameter.");
                                                                                    d61Arr = a(a(a(d61Arr, i10), "_c"), 19, "currency");
                                                                                }
                                                                                c615.c = d61Arr;
                                                                            }
                                                                        }
                                                                        c615.c = d61Arr;
                                                                    }
                                                                    if (dl12.i.u().d(aVar.a.q, jg1.l0)) {
                                                                        if ("_e".equals(c615.d)) {
                                                                            j();
                                                                            if (jl1.a(c615, "_fr") == null) {
                                                                                if (c613 == null || Math.abs(c613.e.longValue() - c615.e.longValue()) > 1000 || !dl12.a(c615, c613)) {
                                                                                    c612 = c615;
                                                                                    if (z7 && !z6 && "_e".equals(c615.d)) {
                                                                                        if (c615.c != null) {
                                                                                            if (c615.c.length != 0) {
                                                                                                j();
                                                                                                Long l5 = (Long) jl1.b(c615, "_et");
                                                                                                if (l5 == null) {
                                                                                                    dl12.i.d().v().a("Engagement event does not include duration. appId", tg1.a(aVar.a.q));
                                                                                                } else {
                                                                                                    j5 += l5.longValue();
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        dl12.i.d().v().a("Engagement event does not contain any parameters. appId", tg1.a(aVar.a.q));
                                                                                    }
                                                                                    f613 = f615;
                                                                                    f613.d[i9] = c615;
                                                                                    c614 = c612;
                                                                                    i9++;
                                                                                    z10 = z2;
                                                                                    j7 = j5;
                                                                                }
                                                                            }
                                                                        } else if ("_vs".equals(c615.d)) {
                                                                            j();
                                                                            if (jl1.a(c615, "_et") == null) {
                                                                                if (c61 != null) {
                                                                                    c612 = c61;
                                                                                    if (Math.abs(c612.e.longValue() - c615.e.longValue()) <= 1000 && dl12.a(c612, c615)) {
                                                                                    }
                                                                                } else {
                                                                                    c612 = c61;
                                                                                }
                                                                                c613 = c615;
                                                                                if (c615.c != null) {
                                                                                }
                                                                                dl12.i.d().v().a("Engagement event does not contain any parameters. appId", tg1.a(aVar.a.q));
                                                                                f613 = f615;
                                                                                f613.d[i9] = c615;
                                                                                c614 = c612;
                                                                                i9++;
                                                                                z10 = z2;
                                                                                j7 = j5;
                                                                            }
                                                                        }
                                                                        c612 = null;
                                                                        c613 = null;
                                                                        if (c615.c != null) {
                                                                        }
                                                                        dl12.i.d().v().a("Engagement event does not contain any parameters. appId", tg1.a(aVar.a.q));
                                                                        f613 = f615;
                                                                        f613.d[i9] = c615;
                                                                        c614 = c612;
                                                                        i9++;
                                                                        z10 = z2;
                                                                        j7 = j5;
                                                                    }
                                                                    c612 = c61;
                                                                    if (c615.c != null) {
                                                                    }
                                                                    dl12.i.d().v().a("Engagement event does not contain any parameters. appId", tg1.a(aVar.a.q));
                                                                    f613 = f615;
                                                                    f613.d[i9] = c615;
                                                                    c614 = c612;
                                                                    i9++;
                                                                    z10 = z2;
                                                                    j7 = j5;
                                                                }
                                                            }
                                                        }
                                                    } else if (str9.equals("_ug")) {
                                                        c2 = 2;
                                                        if (c2 != 0 || c2 == 1 || c2 == 2) {
                                                        }
                                                    }
                                                } else if (str9.equals("_in")) {
                                                    c2 = 0;
                                                    if (c2 != 0 || c2 == 1 || c2 == 2) {
                                                    }
                                                }
                                                c2 = 65535;
                                                if (c2 != 0 || c2 == 1 || c2 == 2) {
                                                }
                                            } else {
                                                j5 = j7;
                                            }
                                            if (c615.c == null) {
                                                c615.c = new d61[0];
                                            }
                                            d61[] d61Arr2 = c615.c;
                                            int length = d61Arr2.length;
                                            z6 = d2;
                                            int i14 = 0;
                                            boolean z11 = false;
                                            boolean z12 = false;
                                            while (i14 < length) {
                                                int i15 = length;
                                                d61 d61 = d61Arr2[i14];
                                                d61[] d61Arr3 = d61Arr2;
                                                if ("_c".equals(d61.c)) {
                                                    d61.e = 1L;
                                                    z11 = true;
                                                } else if ("_r".equals(d61.c)) {
                                                    d61.e = 1L;
                                                    z12 = true;
                                                }
                                                i14++;
                                                length = i15;
                                                d61Arr2 = d61Arr3;
                                            }
                                            if (z11 || !c3) {
                                                z7 = e3;
                                            } else {
                                                z7 = e3;
                                                dl12.i.d().A().a("Marking event as conversion", dl12.i.r().a(c615.d));
                                                d61[] d61Arr4 = (d61[]) Arrays.copyOf(c615.c, c615.c.length + 1);
                                                d61 d612 = new d61();
                                                d612.c = "_c";
                                                d612.e = 1L;
                                                d61Arr4[d61Arr4.length - 1] = d612;
                                                c615.c = d61Arr4;
                                            }
                                            if (!z12) {
                                                dl12.i.d().A().a("Marking event as real-time", dl12.i.r().a(c615.d));
                                                d61[] d61Arr5 = (d61[]) Arrays.copyOf(c615.c, c615.c.length + 1);
                                                d61 d613 = new d61();
                                                d613.c = "_r";
                                                d613.e = 1L;
                                                d61Arr5[d61Arr5.length - 1] = d613;
                                                c615.c = d61Arr5;
                                            }
                                            c61 = c614;
                                            if (l().a(s(), aVar.a.q, false, false, false, false, true).e > ((long) dl12.i.u().a(aVar.a.q))) {
                                                int i16 = 0;
                                                while (true) {
                                                    if (i16 >= c615.c.length) {
                                                        break;
                                                    } else if ("_r".equals(c615.c[i16].c)) {
                                                        d61[] d61Arr6 = new d61[(c615.c.length - 1)];
                                                        if (i16 > 0) {
                                                            System.arraycopy(c615.c, 0, d61Arr6, 0, i16);
                                                        }
                                                        if (i16 < d61Arr6.length) {
                                                            System.arraycopy(c615.c, i16 + 1, d61Arr6, i16, d61Arr6.length - i16);
                                                        }
                                                        c615.c = d61Arr6;
                                                    } else {
                                                        i16++;
                                                    }
                                                }
                                            } else {
                                                z2 = true;
                                            }
                                            if (nl1.e(c615.d) && c3 && l().a(s(), aVar.a.q, false, false, true, false, false).c > ((long) dl12.i.u().b(aVar.a.q, jg1.x))) {
                                                dl12.i.d().v().a("Too many conversions. Not logging as conversion. appId", tg1.a(aVar.a.q));
                                                d61[] d61Arr7 = c615.c;
                                                int length2 = d61Arr7.length;
                                                int i17 = 0;
                                                boolean z13 = false;
                                                d61 d614 = null;
                                                while (i17 < length2) {
                                                    d61 d615 = d61Arr7[i17];
                                                    d61[] d61Arr8 = d61Arr7;
                                                    if ("_c".equals(d615.c)) {
                                                        d614 = d615;
                                                    } else if ("_err".equals(d615.c)) {
                                                        z13 = true;
                                                    }
                                                    i17++;
                                                    d61Arr7 = d61Arr8;
                                                }
                                                if (z13 && d614 != null) {
                                                    c615.c = (d61[]) fm0.a((T[]) c615.c, (T[]) new d61[]{d614});
                                                } else if (d614 != null) {
                                                    d614.c = "_err";
                                                    d614.e = 10L;
                                                } else {
                                                    dl12.i.d().s().a("Did not find conversion parameter. appId", tg1.a(aVar.a.q));
                                                }
                                            }
                                            if (!dl12.i.u().l(aVar.a.q)) {
                                            }
                                            if (dl12.i.u().d(aVar.a.q, jg1.l0)) {
                                            }
                                            c612 = c61;
                                            if (c615.c != null) {
                                            }
                                            dl12.i.d().v().a("Engagement event does not contain any parameters. appId", tg1.a(aVar.a.q));
                                            f613 = f615;
                                            f613.d[i9] = c615;
                                            c614 = c612;
                                            i9++;
                                            z10 = z2;
                                            j7 = j5;
                                        }
                                        i6 = i8 + 1;
                                        str6 = str8;
                                        i7 = i9;
                                        e3 = z7;
                                        d2 = z6;
                                    }
                                    String str11 = str6;
                                    boolean z14 = e3;
                                    int i18 = i7;
                                    long j8 = j7;
                                    if (d2) {
                                        long j9 = j8;
                                        int i19 = 0;
                                        while (i19 < i7) {
                                            c61 c616 = f613.d[i19];
                                            if ("_e".equals(c616.d)) {
                                                j();
                                                if (jl1.a(c616, "_fr") != null) {
                                                    System.arraycopy(f613.d, i19 + 1, f613.d, i19, (i7 - i19) - 1);
                                                    i7--;
                                                    i19--;
                                                    i19++;
                                                }
                                            }
                                            if (z14) {
                                                j();
                                                d61 a2 = jl1.a(c616, "_et");
                                                if (a2 != null) {
                                                    Long l6 = a2.e;
                                                    if (l6 != null && l6.longValue() > 0) {
                                                        j9 += l6.longValue();
                                                    }
                                                }
                                            }
                                            i19++;
                                        }
                                        j8 = j9;
                                    }
                                    if (i7 < aVar.c.size()) {
                                        f613.d = (c61[]) Arrays.copyOf(f613.d, i7);
                                    }
                                    if (z14) {
                                        String str12 = str11;
                                        ml1 d3 = l().d(f613.q, str12);
                                        if (d3 != null) {
                                            if (d3.e != null) {
                                                ml1 = new ml1(f613.q, "auto", "_lte", dl12.i.c().b(), Long.valueOf(((Long) d3.e).longValue() + j8));
                                                i61 i61 = new i61();
                                                i61.d = str12;
                                                i61.c = Long.valueOf(dl12.i.c().b());
                                                i61.f = (Long) ml1.e;
                                                i4 = 0;
                                                while (true) {
                                                    if (i4 < f613.e.length) {
                                                        z5 = false;
                                                        break;
                                                    } else if (str12.equals(f613.e[i4].d)) {
                                                        f613.e[i4] = i61;
                                                        z5 = true;
                                                        break;
                                                    } else {
                                                        i4++;
                                                    }
                                                }
                                                if (!z5) {
                                                    f613.e = (i61[]) Arrays.copyOf(f613.e, f613.e.length + 1);
                                                    f613.e[aVar.a.e.length - 1] = i61;
                                                }
                                                if (j8 > 0) {
                                                    l().a(ml1);
                                                    dl12.i.d().z().a("Updated lifetime engagement user property with value. Value", ml1.e);
                                                }
                                            }
                                        }
                                        ml1 = new ml1(f613.q, "auto", "_lte", dl12.i.c().b(), Long.valueOf(j8));
                                        i61 i612 = new i61();
                                        i612.d = str12;
                                        i612.c = Long.valueOf(dl12.i.c().b());
                                        i612.f = (Long) ml1.e;
                                        i4 = 0;
                                        while (true) {
                                            if (i4 < f613.e.length) {
                                            }
                                            i4++;
                                        }
                                        if (!z5) {
                                        }
                                        if (j8 > 0) {
                                        }
                                    }
                                    String str13 = f613.q;
                                    i61[] i61Arr = f613.e;
                                    c61[] c61Arr2 = f613.d;
                                    bk0.b(str13);
                                    f613.C = k().a(str13, c61Arr2, i61Arr);
                                    if (dl12.i.u().d(aVar.a.q)) {
                                        try {
                                            HashMap hashMap3 = new HashMap();
                                            c61[] c61Arr3 = new c61[f613.d.length];
                                            SecureRandom t2 = dl12.i.s().t();
                                            c61[] c61Arr4 = f613.d;
                                            int length3 = c61Arr4.length;
                                            int i20 = 0;
                                            int i21 = 0;
                                            while (i20 < length3) {
                                                c61 c617 = c61Arr4[i20];
                                                if (c617.d.equals("_ep")) {
                                                    j();
                                                    String str14 = (String) jl1.b(c617, "_en");
                                                    dg1 dg1 = (dg1) hashMap3.get(str14);
                                                    if (dg1 == null) {
                                                        dg1 = l().b(aVar.a.q, str14);
                                                        hashMap3.put(str14, dg1);
                                                    }
                                                    if (dg1.h == null) {
                                                        if (dg1.i.longValue() > 1) {
                                                            j();
                                                            c617.c = jl1.a(c617.c, "_sr", (Object) dg1.i);
                                                        }
                                                        if (dg1.j != null && dg1.j.booleanValue()) {
                                                            j();
                                                            c617.c = jl1.a(c617.c, "_efs", (Object) 1L);
                                                        }
                                                        int i22 = i21 + 1;
                                                        c61Arr3[i21] = c617;
                                                        f612 = f613;
                                                        secureRandom = t2;
                                                        c61Arr = c61Arr4;
                                                        i3 = length3;
                                                        i2 = i20;
                                                        i21 = i22;
                                                    } else {
                                                        f612 = f613;
                                                        secureRandom = t2;
                                                        c61Arr = c61Arr4;
                                                        i3 = length3;
                                                        i2 = i20;
                                                    }
                                                } else {
                                                    long f2 = m().f(aVar.a.q);
                                                    dl12.i.s();
                                                    long a3 = nl1.a(c617.e.longValue(), f2);
                                                    c61Arr = c61Arr4;
                                                    i3 = length3;
                                                    Long l7 = 1L;
                                                    if (!TextUtils.isEmpty("_dbg") && l7 != null) {
                                                        f612 = f613;
                                                        d61[] d61Arr9 = c617.c;
                                                        i2 = i20;
                                                        int length4 = d61Arr9.length;
                                                        j3 = f2;
                                                        int i23 = 0;
                                                        while (true) {
                                                            if (i23 >= length4) {
                                                                break;
                                                            }
                                                            d61 d616 = d61Arr9[i23];
                                                            d61[] d61Arr10 = d61Arr9;
                                                            if (!"_dbg".equals(d616.c)) {
                                                                i23++;
                                                                d61Arr9 = d61Arr10;
                                                            } else if (((l7 instanceof Long) && l7.equals(d616.e)) || (((l7 instanceof String) && l7.equals(d616.d)) || ((l7 instanceof Double) && l7.equals(d616.g)))) {
                                                                z3 = true;
                                                            }
                                                        }
                                                    } else {
                                                        f612 = f613;
                                                        i2 = i20;
                                                        j3 = f2;
                                                    }
                                                    z3 = false;
                                                    int d4 = !z3 ? m().d(aVar.a.q, c617.d) : 1;
                                                    if (d4 <= 0) {
                                                        dl12.i.d().v().a("Sample rate must be positive. event, rate", c617.d, Integer.valueOf(d4));
                                                        int i24 = i21 + 1;
                                                        c61Arr3[i21] = c617;
                                                        i21 = i24;
                                                        secureRandom = t2;
                                                    } else {
                                                        dg1 dg12 = (dg1) hashMap3.get(c617.d);
                                                        if (dg12 == null) {
                                                            dg12 = l().b(aVar.a.q, c617.d);
                                                            if (dg12 == null) {
                                                                dl12.i.d().v().a("Event being bundled has no eventAggregate. appId, eventName", aVar.a.q, c617.d);
                                                                dg12 = new dg1(aVar.a.q, c617.d, 1, 1, c617.e.longValue(), 0, (Long) null, (Long) null, (Long) null, (Boolean) null);
                                                            }
                                                        }
                                                        j();
                                                        Long l8 = (Long) jl1.b(c617, "_eid");
                                                        Boolean valueOf = Boolean.valueOf(l8 != null);
                                                        if (d4 == 1) {
                                                            int i25 = i21 + 1;
                                                            c61Arr3[i21] = c617;
                                                            if (valueOf.booleanValue() && !(dg12.h == null && dg12.i == null && dg12.j == null)) {
                                                                hashMap3.put(c617.d, dg12.a((Long) null, (Long) null, (Boolean) null));
                                                            }
                                                            secureRandom = t2;
                                                            i21 = i25;
                                                        } else if (t2.nextInt(d4) == 0) {
                                                            j();
                                                            long j10 = (long) d4;
                                                            secureRandom = t2;
                                                            c617.c = jl1.a(c617.c, "_sr", (Object) Long.valueOf(j10));
                                                            int i26 = i21 + 1;
                                                            c61Arr3[i21] = c617;
                                                            if (valueOf.booleanValue()) {
                                                                dg12 = dg12.a((Long) null, Long.valueOf(j10), (Boolean) null);
                                                            }
                                                            hashMap3.put(c617.d, dg12.a(c617.e.longValue(), a3));
                                                            i21 = i26;
                                                        } else {
                                                            secureRandom = t2;
                                                            if (!dl12.i.u().n(aVar.a.q)) {
                                                                hashMap2 = hashMap3;
                                                                l4 = l8;
                                                                if (Math.abs(c617.e.longValue() - dg12.f) >= LogBuilder.MAX_INTERVAL) {
                                                                }
                                                                z4 = false;
                                                                if (!z4) {
                                                                    j();
                                                                    c617.c = jl1.a(c617.c, "_efs", (Object) 1L);
                                                                    j();
                                                                    long j11 = (long) d4;
                                                                    c617.c = jl1.a(c617.c, "_sr", (Object) Long.valueOf(j11));
                                                                    int i27 = i21 + 1;
                                                                    c61Arr3[i21] = c617;
                                                                    if (valueOf.booleanValue()) {
                                                                        dg12 = dg12.a((Long) null, Long.valueOf(j11), true);
                                                                    }
                                                                    hashMap = hashMap2;
                                                                    hashMap.put(c617.d, dg12.a(c617.e.longValue(), a3));
                                                                    i21 = i27;
                                                                } else {
                                                                    hashMap = hashMap2;
                                                                    if (valueOf.booleanValue()) {
                                                                        hashMap.put(c617.d, dg12.a(l4, (Long) null, (Boolean) null));
                                                                    }
                                                                }
                                                                i20 = i2 + 1;
                                                                dl12 = this;
                                                                hashMap3 = hashMap;
                                                                c61Arr4 = c61Arr;
                                                                length3 = i3;
                                                                f613 = f612;
                                                                t2 = secureRandom;
                                                            } else if (dg12.g != null) {
                                                                j4 = dg12.g.longValue();
                                                                hashMap2 = hashMap3;
                                                                l4 = l8;
                                                            } else {
                                                                dl12.i.s();
                                                                l4 = l8;
                                                                hashMap2 = hashMap3;
                                                                j4 = nl1.a(c617.f.longValue(), j3);
                                                            }
                                                            z4 = true;
                                                            if (!z4) {
                                                            }
                                                            i20 = i2 + 1;
                                                            dl12 = this;
                                                            hashMap3 = hashMap;
                                                            c61Arr4 = c61Arr;
                                                            length3 = i3;
                                                            f613 = f612;
                                                            t2 = secureRandom;
                                                        }
                                                    }
                                                }
                                                hashMap = hashMap3;
                                                i20 = i2 + 1;
                                                dl12 = this;
                                                hashMap3 = hashMap;
                                                c61Arr4 = c61Arr;
                                                length3 = i3;
                                                f613 = f612;
                                                t2 = secureRandom;
                                            }
                                            HashMap hashMap4 = hashMap3;
                                            f61 = f613;
                                            if (i21 < f61.d.length) {
                                                f61.d = (c61[]) Arrays.copyOf(c61Arr3, i21);
                                            }
                                            for (Map.Entry value : hashMap4.entrySet()) {
                                                l().a((dg1) value.getValue());
                                            }
                                        } catch (Throwable th3) {
                                            th = th3;
                                            Throwable th4 = th;
                                            l().u();
                                            throw th4;
                                        }
                                    } else {
                                        f61 = f613;
                                    }
                                    f61.g = Long.valueOf(ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
                                    f61.h = Long.MIN_VALUE;
                                    for (c61 c618 : f61.d) {
                                        if (c618.e.longValue() < f61.g.longValue()) {
                                            f61.g = c618.e;
                                        }
                                        if (c618.e.longValue() > f61.h.longValue()) {
                                            f61.h = c618.e;
                                        }
                                    }
                                    String str15 = aVar.a.q;
                                    ql1 b2 = l().b(str15);
                                    if (b2 == null) {
                                        dl1 = this;
                                        try {
                                            dl1.i.d().s().a("Bundling raw events w/o app info. appId", tg1.a(aVar.a.q));
                                        } catch (SQLiteException e4) {
                                            l3.d().s().a("Failed to remove unused event metadata. appId", tg1.a(str15), e4);
                                        } catch (Throwable th5) {
                                            th = th5;
                                            Throwable th42 = th;
                                            l().u();
                                            throw th42;
                                        }
                                    } else {
                                        dl1 = this;
                                        if (f61.d.length > 0) {
                                            long k2 = b2.k();
                                            f61.j = k2 != 0 ? Long.valueOf(k2) : null;
                                            long j12 = b2.j();
                                            if (j12 != 0) {
                                                k2 = j12;
                                            }
                                            f61.i = k2 != 0 ? Long.valueOf(k2) : null;
                                            b2.s();
                                            f61.y = Integer.valueOf((int) b2.p());
                                            b2.d(f61.g.longValue());
                                            b2.e(f61.h.longValue());
                                            f61.z = b2.A();
                                            l().a(b2);
                                        }
                                    }
                                    if (f61.d.length > 0) {
                                        dl1.i.b();
                                        y51 b3 = m().b(aVar.a.q);
                                        if (b3 != null) {
                                            if (b3.c != null) {
                                                f61.I = b3.c;
                                                l().a(f61, z2);
                                            }
                                        }
                                        if (TextUtils.isEmpty(aVar.a.A)) {
                                            f61.I = -1L;
                                        } else {
                                            dl1.i.d().v().a("Did not find measurement config or missing version info. appId", tg1.a(aVar.a.q));
                                        }
                                        l().a(f61, z2);
                                    }
                                    am1 l9 = l();
                                    List<Long> list = aVar.b;
                                    bk0.a(list);
                                    l9.e();
                                    l9.q();
                                    StringBuilder sb2 = new StringBuilder("rowid in (");
                                    for (int i28 = 0; i28 < list.size(); i28++) {
                                        if (i28 != 0) {
                                            sb2.append(",");
                                        }
                                        sb2.append(list.get(i28).longValue());
                                    }
                                    sb2.append(")");
                                    int delete = l9.v().delete("raw_events", sb2.toString(), (String[]) null);
                                    if (delete != list.size()) {
                                        l9.d().s().a("Deleted fewer rows from raw events table than expected", Integer.valueOf(delete), Integer.valueOf(list.size()));
                                    }
                                    l3 = l();
                                    l3.v().execSQL("delete from raw_events_metadata where app_id=? and metadata_fingerprint not in (select distinct metadata_fingerprint from raw_events where app_id=?)", new String[]{str15, str15});
                                    l().w();
                                    l().u();
                                    return true;
                                }
                                dl1 dl13 = dl12;
                                l().w();
                                l().u();
                                return false;
                            }
                        }
                        z = true;
                        if (z) {
                        }
                    } else {
                        str2 = cursor3.getString(0);
                        str3 = cursor3.getString(1);
                        cursor3.close();
                        String str16 = str2;
                        cursor = cursor3;
                        str4 = str16;
                    }
                } else {
                    int i29 = (j6 > -1 ? 1 : (j6 == -1 ? 0 : -1));
                    String[] strArr3 = i29 != 0 ? new String[]{null, String.valueOf(j6)} : new String[]{null};
                    String str17 = i29 != 0 ? " and rowid <= ?" : "";
                    StringBuilder sb3 = new StringBuilder(str17.length() + 84);
                    sb3.append("select metadata_fingerprint from raw_events where app_id = ?");
                    sb3.append(str17);
                    sb3.append(" order by rowid limit 1;");
                    cursor3 = v2.rawQuery(sb3.toString(), strArr3);
                    if (!cursor3.moveToFirst()) {
                        if (cursor3 != null) {
                            cursor3.close();
                        }
                        if (aVar.c != null) {
                        }
                        z = true;
                        if (z) {
                        }
                    } else {
                        str3 = cursor3.getString(0);
                        cursor3.close();
                        cursor = cursor3;
                        str4 = null;
                    }
                }
                try {
                    SQLiteDatabase sQLiteDatabase = v2;
                    cursor = v2.query("raw_events_metadata", new String[]{"metadata"}, "app_id = ? and metadata_fingerprint = ?", new String[]{str4, str3}, (String) null, (String) null, "rowid", "2");
                    if (!cursor.moveToFirst()) {
                        l2.d().s().a("Raw event metadata record is missing. appId", tg1.a(str4));
                        if (cursor != null) {
                            cursor.close();
                        }
                        if (aVar.c != null) {
                        }
                        z = true;
                        if (z) {
                        }
                    } else {
                        byte[] blob = cursor.getBlob(0);
                        tb1 a4 = tb1.a(blob, 0, blob.length);
                        f61 f616 = new f61();
                        f616.a(a4);
                        if (cursor.moveToNext()) {
                            l2.d().v().a("Get multiple raw event metadata records, expected one. appId", tg1.a(str4));
                        }
                        cursor.close();
                        aVar.a(f616);
                        if (j6 != -1) {
                            str5 = "app_id = ? and metadata_fingerprint = ? and rowid <= ?";
                            strArr = new String[]{str4, str3, String.valueOf(j6)};
                        } else {
                            str5 = "app_id = ? and metadata_fingerprint = ?";
                            strArr = new String[]{str4, str3};
                        }
                        cursor2 = sQLiteDatabase.query("raw_events", new String[]{"rowid", "name", "timestamp", "data"}, str5, strArr, (String) null, (String) null, "rowid", (String) null);
                        try {
                            if (!cursor2.moveToFirst()) {
                                l2.d().v().a("Raw event data disappeared while in transaction. appId", tg1.a(str4));
                                if (cursor2 != null) {
                                    cursor2.close();
                                }
                                if (aVar.c != null) {
                                }
                                z = true;
                                if (z) {
                                }
                            } else {
                                do {
                                    long j13 = cursor2.getLong(0);
                                    byte[] blob2 = cursor2.getBlob(3);
                                    tb1 a5 = tb1.a(blob2, 0, blob2.length);
                                    c61 c619 = new c61();
                                    try {
                                        c619.a(a5);
                                        c619.d = cursor2.getString(1);
                                        c619.e = Long.valueOf(cursor2.getLong(2));
                                        if (!aVar.a(j13, c619)) {
                                            if (cursor2 != null) {
                                                cursor2.close();
                                            }
                                            if (aVar.c != null) {
                                            }
                                            z = true;
                                            if (z) {
                                            }
                                        }
                                    } catch (IOException e5) {
                                        l2.d().s().a("Data loss. Failed to merge raw event. appId", tg1.a(str4), e5);
                                    }
                                } while (cursor2.moveToNext());
                                if (cursor2 != null) {
                                    cursor2.close();
                                }
                                if (aVar.c != null) {
                                }
                                z = true;
                                if (z) {
                                }
                            }
                        } catch (SQLiteException e6) {
                            e = e6;
                            str2 = str4;
                            sQLiteException = e;
                            try {
                                l2.d().s().a("Data loss. Error selecting raw event. appId", tg1.a(str2), sQLiteException);
                                if (cursor2 != null) {
                                }
                                if (aVar.c != null) {
                                }
                                z = true;
                                if (z) {
                                }
                            } catch (Throwable th6) {
                                dl1 dl14 = dl12;
                                th = th6;
                                cursor = cursor2;
                                if (cursor != null) {
                                }
                                throw th;
                            }
                        } catch (Throwable th7) {
                            th = th7;
                            dl1 dl15 = dl12;
                            cursor = cursor2;
                            th = th;
                            if (cursor != null) {
                            }
                            throw th;
                        }
                    }
                } catch (SQLiteException e7) {
                    e = e7;
                    cursor2 = cursor;
                    str2 = str4;
                    sQLiteException = e;
                    l2.d().s().a("Data loss. Error selecting raw event. appId", tg1.a(str2), sQLiteException);
                    if (cursor2 != null) {
                    }
                    if (aVar.c != null) {
                    }
                    z = true;
                    if (z) {
                    }
                } catch (Throwable th8) {
                    th = th8;
                    dl1 dl16 = dl12;
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            } catch (SQLiteException e8) {
                sQLiteException = e8;
                cursor2 = null;
                str2 = null;
                l2.d().s().a("Data loss. Error selecting raw event. appId", tg1.a(str2), sQLiteException);
                if (cursor2 != null) {
                    cursor2.close();
                }
                if (aVar.c != null) {
                }
                z = true;
                if (z) {
                }
            } catch (Throwable th9) {
                th = th9;
                dl1 dl17 = dl12;
                cursor = null;
                th = th;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (IOException e9) {
            l2.d().s().a("Data loss. Failed to merge raw event metadata. appId", tg1.a(str4), e9);
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th10) {
            th = th10;
            dl1 dl18 = dl12;
        }
    }

    @DexIgnore
    public final Boolean b(ql1 ql1) {
        try {
            if (ql1.l() != -2147483648L) {
                if (ql1.l() == ((long) bn0.b(this.i.getContext()).b(ql1.f(), 0).versionCode)) {
                    return true;
                }
            } else {
                String str = bn0.b(this.i.getContext()).b(ql1.f(), 0).versionName;
                if (ql1.e() != null && ql1.e().equals(str)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public final void b(kl1 kl1, rl1 rl1) {
        f();
        r();
        if (TextUtils.isEmpty(rl1.f) && TextUtils.isEmpty(rl1.v)) {
            return;
        }
        if (!rl1.l) {
            d(rl1);
            return;
        }
        this.i.d().z().a("Removing user property", this.i.r().c(kl1.f));
        l().t();
        try {
            d(rl1);
            l().c(rl1.e, kl1.f);
            l().w();
            this.i.d().z().a("User property removed", this.i.r().c(kl1.f));
        } finally {
            l().u();
        }
    }

    @DexIgnore
    public final void b(vl1 vl1) {
        rl1 a2 = a(vl1.e);
        if (a2 != null) {
            b(vl1, a2);
        }
    }

    @DexIgnore
    public final void b(vl1 vl1, rl1 rl1) {
        bk0.a(vl1);
        bk0.b(vl1.e);
        bk0.a(vl1.g);
        bk0.b(vl1.g.f);
        f();
        r();
        if (TextUtils.isEmpty(rl1.f) && TextUtils.isEmpty(rl1.v)) {
            return;
        }
        if (!rl1.l) {
            d(rl1);
            return;
        }
        l().t();
        try {
            d(rl1);
            vl1 e2 = l().e(vl1.e, vl1.g.f);
            if (e2 != null) {
                this.i.d().z().a("Removing conditional user property", vl1.e, this.i.r().c(vl1.g.f));
                l().f(vl1.e, vl1.g.f);
                if (e2.i) {
                    l().c(vl1.e, vl1.g.f);
                }
                if (vl1.o != null) {
                    Bundle bundle = null;
                    if (vl1.o.f != null) {
                        bundle = vl1.o.f.H();
                    }
                    Bundle bundle2 = bundle;
                    b(this.i.s().a(vl1.e, vl1.o.e, bundle2, e2.f, vl1.o.h, true, false), rl1);
                }
            } else {
                this.i.d().v().a("Conditional user property doesn't exist", tg1.a(vl1.e), this.i.r().c(vl1.g.f));
            }
            l().w();
        } finally {
            l().u();
        }
    }

    @DexIgnore
    public final boolean a(c61 c61, c61 c612) {
        String str;
        bk0.a("_e".equals(c61.d));
        j();
        d61 a2 = jl1.a(c61, "_sc");
        String str2 = null;
        if (a2 == null) {
            str = null;
        } else {
            str = a2.d;
        }
        j();
        d61 a3 = jl1.a(c612, "_pc");
        if (a3 != null) {
            str2 = a3.d;
        }
        if (str2 == null || !str2.equals(str)) {
            return false;
        }
        j();
        d61 a4 = jl1.a(c61, "_et");
        Long l2 = a4.e;
        if (l2 != null && l2.longValue() > 0) {
            long longValue = a4.e.longValue();
            j();
            d61 a5 = jl1.a(c612, "_et");
            if (a5 != null) {
                Long l3 = a5.e;
                if (l3 != null && l3.longValue() > 0) {
                    longValue += a5.e.longValue();
                }
            }
            j();
            c612.c = jl1.a(c612.c, "_et", (Object) Long.valueOf(longValue));
            j();
            c61.c = jl1.a(c61.c, "_fr", (Object) 1L);
        }
        return true;
    }

    @DexIgnore
    public static d61[] a(d61[] d61Arr, String str) {
        int i2 = 0;
        while (true) {
            if (i2 >= d61Arr.length) {
                i2 = -1;
                break;
            } else if (str.equals(d61Arr[i2].c)) {
                break;
            } else {
                i2++;
            }
        }
        if (i2 < 0) {
            return d61Arr;
        }
        return a(d61Arr, i2);
    }

    @DexIgnore
    public static d61[] a(d61[] d61Arr, int i2) {
        d61[] d61Arr2 = new d61[(d61Arr.length - 1)];
        if (i2 > 0) {
            System.arraycopy(d61Arr, 0, d61Arr2, 0, i2);
        }
        if (i2 < d61Arr2.length) {
            System.arraycopy(d61Arr, i2 + 1, d61Arr2, i2, d61Arr2.length - i2);
        }
        return d61Arr2;
    }

    @DexIgnore
    public static d61[] a(d61[] d61Arr, int i2, String str) {
        for (d61 d61 : d61Arr) {
            if ("_err".equals(d61.c)) {
                return d61Arr;
            }
        }
        d61[] d61Arr2 = new d61[(d61Arr.length + 2)];
        System.arraycopy(d61Arr, 0, d61Arr2, 0, d61Arr.length);
        d61 d612 = new d61();
        d612.c = "_err";
        d612.e = Long.valueOf((long) i2);
        d61 d613 = new d61();
        d613.c = "_ev";
        d613.d = str;
        d61Arr2[d61Arr2.length - 2] = d612;
        d61Arr2[d61Arr2.length - 1] = d613;
        return d61Arr2;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void a(int i2, Throwable th, byte[] bArr, String str) {
        am1 l2;
        f();
        r();
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.r = false;
                w();
                throw th2;
            }
        }
        List<Long> list = this.v;
        this.v = null;
        boolean z = true;
        if ((i2 == 200 || i2 == 204) && th == null) {
            try {
                this.i.t().e.a(this.i.c().b());
                this.i.t().f.a(0);
                v();
                this.i.d().A().a("Successful upload. Got network response. code, size", Integer.valueOf(i2), Integer.valueOf(bArr.length));
                l().t();
                try {
                    for (Long next : list) {
                        try {
                            l2 = l();
                            long longValue = next.longValue();
                            l2.e();
                            l2.q();
                            if (l2.v().delete("queue", "rowid=?", new String[]{String.valueOf(longValue)}) != 1) {
                                throw new SQLiteException("Deleted fewer rows from queue than expected");
                            }
                        } catch (SQLiteException e2) {
                            l2.d().s().a("Failed to delete a bundle in a queue table", e2);
                            throw e2;
                        } catch (SQLiteException e3) {
                            if (this.w == null || !this.w.contains(next)) {
                                throw e3;
                            }
                        }
                    }
                    l().w();
                    l().u();
                    this.w = null;
                    if (!n().t() || !u()) {
                        this.x = -1;
                        v();
                    } else {
                        t();
                    }
                    this.m = 0;
                } catch (Throwable th3) {
                    l().u();
                    throw th3;
                }
            } catch (SQLiteException e4) {
                this.i.d().s().a("Database error while trying to delete uploaded bundles", e4);
                this.m = this.i.c().c();
                this.i.d().A().a("Disable upload, time", Long.valueOf(this.m));
            }
        } else {
            this.i.d().A().a("Network upload failed. Will retry later. code, error", Integer.valueOf(i2), th);
            this.i.t().f.a(this.i.c().b());
            if (i2 != 503) {
                if (i2 != 429) {
                    z = false;
                }
            }
            if (z) {
                this.i.t().g.a(this.i.c().b());
            }
            if (this.i.u().g(str)) {
                l().a(list);
            }
            v();
        }
        this.r = false;
        w();
    }

    @DexIgnore
    public final void a(ql1 ql1) {
        g4 g4Var;
        f();
        if (!TextUtils.isEmpty(ql1.c()) || (xl1.w() && !TextUtils.isEmpty(ql1.h()))) {
            xl1 u2 = this.i.u();
            Uri.Builder builder = new Uri.Builder();
            String c2 = ql1.c();
            if (TextUtils.isEmpty(c2) && xl1.w()) {
                c2 = ql1.h();
            }
            Uri.Builder encodedAuthority = builder.scheme(jg1.o.a()).encodedAuthority(jg1.p.a());
            String valueOf = String.valueOf(c2);
            encodedAuthority.path(valueOf.length() != 0 ? "config/app/".concat(valueOf) : new String("config/app/")).appendQueryParameter("app_instance_id", ql1.a()).appendQueryParameter("platform", "android").appendQueryParameter("gmp_version", String.valueOf(u2.n()));
            String uri = builder.build().toString();
            try {
                URL url = new URL(uri);
                this.i.d().A().a("Fetching remote configuration", ql1.f());
                y51 b2 = m().b(ql1.f());
                String c3 = m().c(ql1.f());
                if (b2 == null || TextUtils.isEmpty(c3)) {
                    g4Var = null;
                } else {
                    g4 g4Var2 = new g4();
                    g4Var2.put("If-Modified-Since", c3);
                    g4Var = g4Var2;
                }
                this.q = true;
                xg1 n2 = n();
                String f2 = ql1.f();
                gl1 gl1 = new gl1(this);
                n2.e();
                n2.q();
                bk0.a(url);
                bk0.a(gl1);
                n2.a().b((Runnable) new ch1(n2, f2, url, (byte[]) null, g4Var, gl1));
            } catch (MalformedURLException unused) {
                this.i.d().s().a("Failed to parse config URL. Not fetching. appId", tg1.a(ql1.f()), uri);
            }
        } else {
            a(ql1.f(), 204, (Throwable) null, (byte[]) null, (Map<String, List<String>>) null);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:59:0x013a A[Catch:{ all -> 0x018d, all -> 0x0196 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x014a A[Catch:{ all -> 0x018d, all -> 0x0196 }] */
    public final void a(String str, int i2, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        f();
        r();
        bk0.b(str);
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.q = false;
                w();
                throw th2;
            }
        }
        this.i.d().A().a("onConfigFetched. Response size", Integer.valueOf(bArr.length));
        l().t();
        ql1 b2 = l().b(str);
        boolean z = true;
        boolean z2 = (i2 == 200 || i2 == 204 || i2 == 304) && th == null;
        if (b2 == null) {
            this.i.d().v().a("App does not exist in onConfigFetched. appId", tg1.a(str));
        } else {
            if (!z2) {
                if (i2 != 404) {
                    b2.k(this.i.c().b());
                    l().a(b2);
                    this.i.d().A().a("Fetching config failed. code, error", Integer.valueOf(i2), th);
                    m().d(str);
                    this.i.t().f.a(this.i.c().b());
                    if (i2 != 503) {
                        if (i2 != 429) {
                            z = false;
                        }
                    }
                    if (z) {
                        this.i.t().g.a(this.i.c().b());
                    }
                    v();
                }
            }
            List list = map != null ? map.get("Last-Modified") : null;
            String str2 = (list == null || list.size() <= 0) ? null : (String) list.get(0);
            if (i2 != 404) {
                if (i2 != 304) {
                    if (!m().a(str, bArr, str2)) {
                        l().u();
                        this.q = false;
                        w();
                        return;
                    }
                    b2.j(this.i.c().b());
                    l().a(b2);
                    if (i2 != 404) {
                        this.i.d().x().a("Config not found. Using empty config. appId", str);
                    } else {
                        this.i.d().A().a("Successfully fetched config. Got network response. code, size", Integer.valueOf(i2), Integer.valueOf(bArr.length));
                    }
                    if (n().t() || !u()) {
                        v();
                    } else {
                        t();
                    }
                }
            }
            if (m().b(str) == null && !m().a(str, (byte[]) null, (String) null)) {
                l().u();
                this.q = false;
                w();
                return;
            }
            b2.j(this.i.c().b());
            l().a(b2);
            if (i2 != 404) {
            }
            if (n().t()) {
            }
            v();
        }
        l().w();
        l().u();
        this.q = false;
        w();
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        f();
        if (this.n == null) {
            this.n = new ArrayList();
        }
        this.n.add(runnable);
    }

    @DexIgnore
    public final int a(FileChannel fileChannel) {
        f();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.i.d().s().a("Bad channel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0);
            int read = fileChannel.read(allocate);
            if (read != 4) {
                if (read != -1) {
                    this.i.d().v().a("Unexpected data length. Bytes read", Integer.valueOf(read));
                }
                return 0;
            }
            allocate.flip();
            return allocate.getInt();
        } catch (IOException e2) {
            this.i.d().s().a("Failed to read from channel", e2);
            return 0;
        }
    }

    @DexIgnore
    public final boolean a(int i2, FileChannel fileChannel) {
        f();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.i.d().s().a("Bad channel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i2);
        allocate.flip();
        try {
            fileChannel.truncate(0);
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() != 4) {
                this.i.d().s().a("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            }
            return true;
        } catch (IOException e2) {
            this.i.d().s().a("Failed to write to channel", e2);
            return false;
        }
    }

    @DexIgnore
    public final void a(rl1 rl1) {
        if (this.v != null) {
            this.w = new ArrayList();
            this.w.addAll(this.v);
        }
        am1 l2 = l();
        String str = rl1.e;
        bk0.b(str);
        l2.e();
        l2.q();
        try {
            SQLiteDatabase v2 = l2.v();
            String[] strArr = {str};
            int delete = v2.delete("apps", "app_id=?", strArr) + 0 + v2.delete("events", "app_id=?", strArr) + v2.delete("user_attributes", "app_id=?", strArr) + v2.delete("conditional_properties", "app_id=?", strArr) + v2.delete("raw_events", "app_id=?", strArr) + v2.delete("raw_events_metadata", "app_id=?", strArr) + v2.delete("queue", "app_id=?", strArr) + v2.delete("audience_filter_values", "app_id=?", strArr) + v2.delete("main_event_params", "app_id=?", strArr);
            if (delete > 0) {
                l2.d().A().a("Reset analytics data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e2) {
            l2.d().s().a("Error resetting analytics data. appId, error", tg1.a(str), e2);
        }
        rl1 a2 = a(this.i.getContext(), rl1.e, rl1.f, rl1.l, rl1.s, rl1.t, rl1.q, rl1.v);
        if (!this.i.u().i(rl1.e) || rl1.l) {
            c(a2);
        }
    }

    @DexIgnore
    public final rl1 a(Context context, String str, String str2, boolean z, boolean z2, boolean z3, long j2, String str3) {
        String str4;
        String str5;
        int i2;
        String str6 = str;
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            this.i.d().s().a("PackageManager is null, can not log app install information");
            return null;
        }
        try {
            str4 = packageManager.getInstallerPackageName(str6);
        } catch (IllegalArgumentException unused) {
            this.i.d().s().a("Error retrieving installer package name. appId", tg1.a(str));
            str4 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        if (str4 == null) {
            str4 = "manual_install";
        } else if ("com.android.vending".equals(str4)) {
            str4 = "";
        }
        String str7 = str4;
        try {
            PackageInfo b2 = bn0.b(context).b(str6, 0);
            if (b2 != null) {
                CharSequence b3 = bn0.b(context).b(str6);
                if (!TextUtils.isEmpty(b3)) {
                    String charSequence = b3.toString();
                }
                String str8 = b2.versionName;
                i2 = b2.versionCode;
                str5 = str8;
            } else {
                i2 = Integer.MIN_VALUE;
                str5 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
            }
            this.i.b();
            return new rl1(str, str2, str5, (long) i2, str7, this.i.u().n(), this.i.s().a(context, str6), (String) null, z, false, "", 0, this.i.u().k(str6) ? j2 : 0, 0, z2, z3, false, str3);
        } catch (PackageManager.NameNotFoundException unused2) {
            this.i.d().s().a("Error retrieving newly installed package info. appId, appName", tg1.a(str), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            return null;
        }
    }

    @DexIgnore
    public final void a(kl1 kl1, rl1 rl1) {
        f();
        r();
        if (TextUtils.isEmpty(rl1.f) && TextUtils.isEmpty(rl1.v)) {
            return;
        }
        if (!rl1.l) {
            d(rl1);
            return;
        }
        int b2 = this.i.s().b(kl1.f);
        if (b2 != 0) {
            this.i.s();
            String a2 = nl1.a(kl1.f, 24, true);
            String str = kl1.f;
            this.i.s().a(rl1.e, b2, "_ev", a2, str != null ? str.length() : 0);
            return;
        }
        int b3 = this.i.s().b(kl1.f, kl1.H());
        if (b3 != 0) {
            this.i.s();
            String a3 = nl1.a(kl1.f, 24, true);
            Object H = kl1.H();
            this.i.s().a(rl1.e, b3, "_ev", a3, (H == null || (!(H instanceof String) && !(H instanceof CharSequence))) ? 0 : String.valueOf(H).length());
            return;
        }
        Object c2 = this.i.s().c(kl1.f, kl1.H());
        if (c2 != null) {
            if (this.i.u().p(rl1.e) && "_sno".equals(kl1.f)) {
                long j2 = 0;
                ml1 d2 = l().d(rl1.e, "_sno");
                if (d2 != null) {
                    Object obj = d2.e;
                    if (obj instanceof Long) {
                        j2 = ((Long) obj).longValue();
                        c2 = Long.valueOf(j2 + 1);
                    }
                }
                dg1 b4 = l().b(rl1.e, "_s");
                if (b4 != null) {
                    j2 = b4.c;
                    this.i.d().A().a("Backfill the session number. Last used session number", Long.valueOf(j2));
                }
                c2 = Long.valueOf(j2 + 1);
            }
            ml1 ml1 = new ml1(rl1.e, kl1.j, kl1.f, kl1.g, c2);
            this.i.d().z().a("Setting user property", this.i.r().c(ml1.c), c2);
            l().t();
            try {
                d(rl1);
                boolean a4 = l().a(ml1);
                l().w();
                if (a4) {
                    this.i.d().z().a("User property set", this.i.r().c(ml1.c), ml1.e);
                } else {
                    this.i.d().s().a("Too many unique user properties are set. Ignoring user property", this.i.r().c(ml1.c), ml1.e);
                    this.i.s().a(rl1.e, 9, (String) null, (String) null, 0);
                }
            } finally {
                l().u();
            }
        }
    }

    @DexIgnore
    public final void a(cl1 cl1) {
        this.o++;
    }

    @DexIgnore
    public final rl1 a(String str) {
        String str2 = str;
        ql1 b2 = l().b(str2);
        if (b2 == null || TextUtils.isEmpty(b2.e())) {
            this.i.d().z().a("No app data available; dropping", str2);
            return null;
        }
        Boolean b3 = b(b2);
        if (b3 == null || b3.booleanValue()) {
            ql1 ql1 = b2;
            return new rl1(str, b2.c(), b2.e(), b2.l(), b2.m(), b2.n(), b2.o(), (String) null, b2.d(), false, b2.b(), ql1.B(), 0, 0, ql1.C(), ql1.D(), false, ql1.h());
        }
        this.i.d().s().a("App version does not match; dropping. appId", tg1.a(str));
        return null;
    }

    @DexIgnore
    public final void a(vl1 vl1) {
        rl1 a2 = a(vl1.e);
        if (a2 != null) {
            a(vl1, a2);
        }
    }

    @DexIgnore
    public final void a(vl1 vl1, rl1 rl1) {
        bk0.a(vl1);
        bk0.b(vl1.e);
        bk0.a(vl1.f);
        bk0.a(vl1.g);
        bk0.b(vl1.g.f);
        f();
        r();
        if (TextUtils.isEmpty(rl1.f) && TextUtils.isEmpty(rl1.v)) {
            return;
        }
        if (!rl1.l) {
            d(rl1);
            return;
        }
        vl1 vl12 = new vl1(vl1);
        boolean z = false;
        vl12.i = false;
        l().t();
        try {
            vl1 e2 = l().e(vl12.e, vl12.g.f);
            if (e2 != null && !e2.f.equals(vl12.f)) {
                this.i.d().v().a("Updating a conditional user property with different origin. name, origin, origin (from DB)", this.i.r().c(vl12.g.f), vl12.f, e2.f);
            }
            if (e2 != null && e2.i) {
                vl12.f = e2.f;
                vl12.h = e2.h;
                vl12.l = e2.l;
                vl12.j = e2.j;
                vl12.m = e2.m;
                vl12.i = e2.i;
                vl12.g = new kl1(vl12.g.f, e2.g.g, vl12.g.H(), e2.g.j);
            } else if (TextUtils.isEmpty(vl12.j)) {
                vl12.g = new kl1(vl12.g.f, vl12.h, vl12.g.H(), vl12.g.j);
                vl12.i = true;
                z = true;
            }
            if (vl12.i) {
                kl1 kl1 = vl12.g;
                ml1 ml1 = new ml1(vl12.e, vl12.f, kl1.f, kl1.g, kl1.H());
                if (l().a(ml1)) {
                    this.i.d().z().a("User property updated immediately", vl12.e, this.i.r().c(ml1.c), ml1.e);
                } else {
                    this.i.d().s().a("(2)Too many active user properties, ignoring", tg1.a(vl12.e), this.i.r().c(ml1.c), ml1.e);
                }
                if (z && vl12.m != null) {
                    b(new hg1(vl12.m, vl12.h), rl1);
                }
            }
            if (l().a(vl12)) {
                this.i.d().z().a("Conditional property added", vl12.e, this.i.r().c(vl12.g.f), vl12.g.H());
            } else {
                this.i.d().s().a("Too many conditional properties, ignoring", tg1.a(vl12.e), this.i.r().c(vl12.g.f), vl12.g.H());
            }
            l().w();
        } finally {
            l().u();
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        v();
    }
}
