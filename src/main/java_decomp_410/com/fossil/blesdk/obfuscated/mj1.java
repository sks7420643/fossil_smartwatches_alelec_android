package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mj1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean e;
    @DexIgnore
    public /* final */ /* synthetic */ dj1 f;

    @DexIgnore
    public mj1(dj1 dj1, boolean z) {
        this.f = dj1;
        this.e = z;
    }

    @DexIgnore
    public final void run() {
        boolean e2 = this.f.a.e();
        boolean E = this.f.a.E();
        this.f.a.a(this.e);
        if (E == this.e) {
            this.f.a.d().A().a("Default data collection state already set to", Boolean.valueOf(this.e));
        }
        if (this.f.a.e() == e2 || this.f.a.e() != this.f.a.E()) {
            this.f.a.d().x().a("Default data collection is different than actual status", Boolean.valueOf(this.e), Boolean.valueOf(e2));
        }
        this.f.E();
    }
}
