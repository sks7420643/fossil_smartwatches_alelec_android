package com.fossil.blesdk.obfuscated;

import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qi4 extends qj4 implements ai4 {
    @DexIgnore
    public qi4 a() {
        return this;
    }

    @DexIgnore
    public final String a(String str) {
        kd4.b(str, "state");
        StringBuilder sb = new StringBuilder();
        sb.append("List{");
        sb.append(str);
        sb.append("}[");
        Object c = c();
        if (c != null) {
            boolean z = true;
            for (sj4 sj4 = (sj4) c; !kd4.a((Object) sj4, (Object) this); sj4 = sj4.d()) {
                if (sj4 instanceof ki4) {
                    ki4 ki4 = (ki4) sj4;
                    if (z) {
                        z = false;
                    } else {
                        sb.append(", ");
                    }
                    sb.append(ki4);
                }
            }
            sb.append("]");
            String sb2 = sb.toString();
            kd4.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
            return sb2;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public boolean isActive() {
        return true;
    }

    @DexIgnore
    public String toString() {
        return ch4.c() ? a("Active") : super.toString();
    }
}
