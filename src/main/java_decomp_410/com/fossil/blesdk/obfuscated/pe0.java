package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.me0;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class pe0<R extends me0, S extends me0> {
    @DexIgnore
    public abstract he0<S> a(R r);

    @DexIgnore
    public abstract Status a(Status status);
}
