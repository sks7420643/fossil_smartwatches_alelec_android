package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.k9 */
public interface C2190k9 {
    @DexIgnore
    /* renamed from: a */
    void mo8505a(android.view.View view);

    @DexIgnore
    /* renamed from: b */
    void mo8506b(android.view.View view);

    @DexIgnore
    /* renamed from: c */
    void mo8507c(android.view.View view);
}
