package com.fossil.blesdk.obfuscated;

import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class m24 extends TimerTask {
    @DexIgnore
    public /* final */ /* synthetic */ l24 e;

    @DexIgnore
    public m24(l24 l24) {
        this.e = l24;
    }

    @DexIgnore
    public void run() {
        if (h04.q()) {
            e24.b().e("TimerTask run");
        }
        j04.e(this.e.b);
        cancel();
        this.e.a();
    }
}
