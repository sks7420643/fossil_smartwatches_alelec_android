package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewPropertyAnimator;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class s2 extends HorizontalScrollView implements AdapterView.OnItemSelectedListener {
    @DexIgnore
    public Runnable e;
    @DexIgnore
    public c f;
    @DexIgnore
    public LinearLayoutCompat g; // = b();
    @DexIgnore
    public Spinner h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public ViewPropertyAnimator n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ View e;

        @DexIgnore
        public a(View view) {
            this.e = view;
        }

        @DexIgnore
        public void run() {
            s2.this.smoothScrollTo(this.e.getLeft() - ((s2.this.getWidth() - this.e.getWidth()) / 2), 0);
            s2.this.e = null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends BaseAdapter {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public int getCount() {
            return s2.this.g.getChildCount();
        }

        @DexIgnore
        public Object getItem(int i) {
            return ((d) s2.this.g.getChildAt(i)).a();
        }

        @DexIgnore
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                return s2.this.a((ActionBar.b) getItem(i), true);
            }
            ((d) view).a((ActionBar.b) getItem(i));
            return view;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements View.OnClickListener {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void onClick(View view) {
            ((d) view).a().e();
            int childCount = s2.this.g.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = s2.this.g.getChildAt(i);
                childAt.setSelected(childAt == view);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends AnimatorListenerAdapter {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public int b;

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            this.a = true;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            if (!this.a) {
                s2 s2Var = s2.this;
                s2Var.n = null;
                s2Var.setVisibility(this.b);
            }
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            s2.this.setVisibility(0);
            this.a = false;
        }
    }

    /*
    static {
        new DecelerateInterpolator();
    }
    */

    @DexIgnore
    public s2(Context context) {
        super(context);
        new e();
        setHorizontalScrollBarEnabled(false);
        s0 a2 = s0.a(context);
        setContentHeight(a2.e());
        this.k = a2.d();
        addView(this.g, new ViewGroup.LayoutParams(-2, -1));
    }

    @DexIgnore
    public final Spinner a() {
        AppCompatSpinner appCompatSpinner = new AppCompatSpinner(getContext(), (AttributeSet) null, r.actionDropDownStyle);
        appCompatSpinner.setLayoutParams(new LinearLayoutCompat.LayoutParams(-2, -1));
        appCompatSpinner.setOnItemSelectedListener(this);
        return appCompatSpinner;
    }

    @DexIgnore
    public final LinearLayoutCompat b() {
        LinearLayoutCompat linearLayoutCompat = new LinearLayoutCompat(getContext(), (AttributeSet) null, r.actionBarTabBarStyle);
        linearLayoutCompat.setMeasureWithLargestChildEnabled(true);
        linearLayoutCompat.setGravity(17);
        linearLayoutCompat.setLayoutParams(new LinearLayoutCompat.LayoutParams(-2, -1));
        return linearLayoutCompat;
    }

    @DexIgnore
    public final boolean c() {
        Spinner spinner = this.h;
        return spinner != null && spinner.getParent() == this;
    }

    @DexIgnore
    public final void d() {
        if (!c()) {
            if (this.h == null) {
                this.h = a();
            }
            removeView(this.g);
            addView(this.h, new ViewGroup.LayoutParams(-2, -1));
            if (this.h.getAdapter() == null) {
                this.h.setAdapter(new b());
            }
            Runnable runnable = this.e;
            if (runnable != null) {
                removeCallbacks(runnable);
                this.e = null;
            }
            this.h.setSelection(this.m);
        }
    }

    @DexIgnore
    public final boolean e() {
        if (!c()) {
            return false;
        }
        removeView(this.h);
        addView(this.g, new ViewGroup.LayoutParams(-2, -1));
        setTabSelected(this.h.getSelectedItemPosition());
        return false;
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Runnable runnable = this.e;
        if (runnable != null) {
            post(runnable);
        }
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        s0 a2 = s0.a(getContext());
        setContentHeight(a2.e());
        this.k = a2.d();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Runnable runnable = this.e;
        if (runnable != null) {
            removeCallbacks(runnable);
        }
    }

    @DexIgnore
    public void onItemSelected(AdapterView<?> adapterView, View view, int i2, long j2) {
        ((d) view).a().e();
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        boolean z = true;
        boolean z2 = mode == 1073741824;
        setFillViewport(z2);
        int childCount = this.g.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.j = -1;
        } else {
            if (childCount > 2) {
                this.j = (int) (((float) View.MeasureSpec.getSize(i2)) * 0.4f);
            } else {
                this.j = View.MeasureSpec.getSize(i2) / 2;
            }
            this.j = Math.min(this.j, this.k);
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.l, 1073741824);
        if (z2 || !this.i) {
            z = false;
        }
        if (z) {
            this.g.measure(0, makeMeasureSpec);
            if (this.g.getMeasuredWidth() > View.MeasureSpec.getSize(i2)) {
                d();
            } else {
                e();
            }
        } else {
            e();
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i2, makeMeasureSpec);
        int measuredWidth2 = getMeasuredWidth();
        if (z2 && measuredWidth != measuredWidth2) {
            setTabSelected(this.m);
        }
    }

    @DexIgnore
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @DexIgnore
    public void setAllowCollapse(boolean z) {
        this.i = z;
    }

    @DexIgnore
    public void setContentHeight(int i2) {
        this.l = i2;
        requestLayout();
    }

    @DexIgnore
    public void setTabSelected(int i2) {
        this.m = i2;
        int childCount = this.g.getChildCount();
        int i3 = 0;
        while (i3 < childCount) {
            View childAt = this.g.getChildAt(i3);
            boolean z = i3 == i2;
            childAt.setSelected(z);
            if (z) {
                a(i2);
            }
            i3++;
        }
        Spinner spinner = this.h;
        if (spinner != null && i2 >= 0) {
            spinner.setSelection(i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends LinearLayout {
        @DexIgnore
        public /* final */ int[] e; // = {16842964};
        @DexIgnore
        public ActionBar.b f;
        @DexIgnore
        public TextView g;
        @DexIgnore
        public ImageView h;
        @DexIgnore
        public View i;

        @DexIgnore
        public d(Context context, ActionBar.b bVar, boolean z) {
            super(context, (AttributeSet) null, r.actionBarTabStyle);
            this.f = bVar;
            z2 a = z2.a(context, (AttributeSet) null, this.e, r.actionBarTabStyle, 0);
            if (a.g(0)) {
                setBackgroundDrawable(a.b(0));
            }
            a.a();
            if (z) {
                setGravity(8388627);
            }
            b();
        }

        @DexIgnore
        public void a(ActionBar.b bVar) {
            this.f = bVar;
            b();
        }

        @DexIgnore
        public void b() {
            ActionBar.b bVar = this.f;
            View b = bVar.b();
            CharSequence charSequence = null;
            if (b != null) {
                ViewParent parent = b.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(b);
                    }
                    addView(b);
                }
                this.i = b;
                TextView textView = this.g;
                if (textView != null) {
                    textView.setVisibility(8);
                }
                ImageView imageView = this.h;
                if (imageView != null) {
                    imageView.setVisibility(8);
                    this.h.setImageDrawable((Drawable) null);
                    return;
                }
                return;
            }
            View view = this.i;
            if (view != null) {
                removeView(view);
                this.i = null;
            }
            Drawable c = bVar.c();
            CharSequence d = bVar.d();
            if (c != null) {
                if (this.h == null) {
                    AppCompatImageView appCompatImageView = new AppCompatImageView(getContext());
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams.gravity = 16;
                    appCompatImageView.setLayoutParams(layoutParams);
                    addView(appCompatImageView, 0);
                    this.h = appCompatImageView;
                }
                this.h.setImageDrawable(c);
                this.h.setVisibility(0);
            } else {
                ImageView imageView2 = this.h;
                if (imageView2 != null) {
                    imageView2.setVisibility(8);
                    this.h.setImageDrawable((Drawable) null);
                }
            }
            boolean z = !TextUtils.isEmpty(d);
            if (z) {
                if (this.g == null) {
                    AppCompatTextView appCompatTextView = new AppCompatTextView(getContext(), (AttributeSet) null, r.actionBarTabTextStyle);
                    appCompatTextView.setEllipsize(TextUtils.TruncateAt.END);
                    LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams2.gravity = 16;
                    appCompatTextView.setLayoutParams(layoutParams2);
                    addView(appCompatTextView);
                    this.g = appCompatTextView;
                }
                this.g.setText(d);
                this.g.setVisibility(0);
            } else {
                TextView textView2 = this.g;
                if (textView2 != null) {
                    textView2.setVisibility(8);
                    this.g.setText((CharSequence) null);
                }
            }
            ImageView imageView3 = this.h;
            if (imageView3 != null) {
                imageView3.setContentDescription(bVar.a());
            }
            if (!z) {
                charSequence = bVar.a();
            }
            b3.a(this, charSequence);
        }

        @DexIgnore
        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName(ActionBar.b.class.getName());
        }

        @DexIgnore
        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName(ActionBar.b.class.getName());
        }

        @DexIgnore
        public void onMeasure(int i2, int i3) {
            super.onMeasure(i2, i3);
            if (s2.this.j > 0) {
                int measuredWidth = getMeasuredWidth();
                int i4 = s2.this.j;
                if (measuredWidth > i4) {
                    super.onMeasure(View.MeasureSpec.makeMeasureSpec(i4, 1073741824), i3);
                }
            }
        }

        @DexIgnore
        public void setSelected(boolean z) {
            boolean z2 = isSelected() != z;
            super.setSelected(z);
            if (z2 && z) {
                sendAccessibilityEvent(4);
            }
        }

        @DexIgnore
        public ActionBar.b a() {
            return this.f;
        }
    }

    @DexIgnore
    public void a(int i2) {
        View childAt = this.g.getChildAt(i2);
        Runnable runnable = this.e;
        if (runnable != null) {
            removeCallbacks(runnable);
        }
        this.e = new a(childAt);
        post(this.e);
    }

    @DexIgnore
    public d a(ActionBar.b bVar, boolean z) {
        d dVar = new d(getContext(), bVar, z);
        if (z) {
            dVar.setBackgroundDrawable((Drawable) null);
            dVar.setLayoutParams(new AbsListView.LayoutParams(-1, this.l));
        } else {
            dVar.setFocusable(true);
            if (this.f == null) {
                this.f = new c();
            }
            dVar.setOnClickListener(this.f);
        }
        return dVar;
    }
}
