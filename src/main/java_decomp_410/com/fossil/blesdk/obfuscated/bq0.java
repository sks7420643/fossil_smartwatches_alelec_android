package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bq0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<bq0> CREATOR; // = new cq0();
    @DexIgnore
    public /* final */ g11 e;

    @DexIgnore
    public bq0(IBinder iBinder) {
        this.e = h11.a(iBinder);
    }

    @DexIgnore
    public final String toString() {
        return String.format("DisableFitRequest", new Object[0]);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e.asBinder(), false);
        kk0.a(parcel, a);
    }

    @DexIgnore
    public bq0(g11 g11) {
        this.e = g11;
    }
}
