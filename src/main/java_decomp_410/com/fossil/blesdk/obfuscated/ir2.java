package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.NotificationsRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ir2 implements Factory<hr2> {
    @DexIgnore
    public static hr2 a(NotificationsRepository notificationsRepository) {
        return new hr2(notificationsRepository);
    }
}
