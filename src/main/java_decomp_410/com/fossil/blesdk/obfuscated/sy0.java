package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.Log;
import com.fossil.blesdk.obfuscated.ey0;
import com.fossil.blesdk.obfuscated.md0;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sy0 implements md0.b {
    @DexIgnore
    public static /* final */ Charset b; // = Charset.forName("UTF-8");
    @DexIgnore
    public static /* final */ it0 c; // = new it0(im1.a("com.google.android.gms.clearcut.public")).a("gms:playlog:service:samplingrules_").b("LogSamplingRules__");
    @DexIgnore
    public static /* final */ it0 d; // = new it0(im1.a("com.google.android.gms.clearcut.public")).a("gms:playlog:service:sampling_").b("LogSampling__");
    @DexIgnore
    public static /* final */ ConcurrentHashMap<String, ys0<ey0>> e; // = new ConcurrentHashMap<>();
    @DexIgnore
    public static /* final */ HashMap<String, ys0<String>> f; // = new HashMap<>();
    @DexIgnore
    public static Boolean g; // = null;
    @DexIgnore
    public static Long h; // = null;
    @DexIgnore
    public static /* final */ ys0<Boolean> i; // = c.a("enable_log_sampling_rules", false);
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public sy0(Context context) {
        this.a = context;
        Context context2 = this.a;
        if (context2 != null) {
            ys0.a(context2);
        }
    }

    @DexIgnore
    public static long a(String str, long j) {
        if (str == null || str.isEmpty()) {
            return ny0.a(ByteBuffer.allocate(8).putLong(j).array());
        }
        byte[] bytes = str.getBytes(b);
        ByteBuffer allocate = ByteBuffer.allocate(bytes.length + 8);
        allocate.put(bytes);
        allocate.putLong(j);
        return ny0.a(allocate.array());
    }

    @DexIgnore
    public static ey0.b a(String str) {
        String str2;
        int i2;
        if (str == null) {
            return null;
        }
        int indexOf = str.indexOf(44);
        if (indexOf >= 0) {
            str2 = str.substring(0, indexOf);
            i2 = indexOf + 1;
        } else {
            str2 = "";
            i2 = 0;
        }
        int indexOf2 = str.indexOf(47, i2);
        if (indexOf2 <= 0) {
            String valueOf = String.valueOf(str);
            Log.e("LogSamplerImpl", valueOf.length() != 0 ? "Failed to parse the rule: ".concat(valueOf) : new String("Failed to parse the rule: "));
            return null;
        }
        try {
            long parseLong = Long.parseLong(str.substring(i2, indexOf2));
            long parseLong2 = Long.parseLong(str.substring(indexOf2 + 1));
            if (parseLong < 0 || parseLong2 < 0) {
                StringBuilder sb = new StringBuilder(72);
                sb.append("negative values not supported: ");
                sb.append(parseLong);
                sb.append(ZendeskConfig.SLASH);
                sb.append(parseLong2);
                Log.e("LogSamplerImpl", sb.toString());
                return null;
            }
            ey0.b.a n = ey0.b.n();
            n.a(str2);
            n.a(parseLong);
            n.b(parseLong2);
            return (ey0.b) n.i();
        } catch (NumberFormatException e2) {
            String valueOf2 = String.valueOf(str);
            Log.e("LogSamplerImpl", valueOf2.length() != 0 ? "parseLong() failed while parsing: ".concat(valueOf2) : new String("parseLong() failed while parsing: "), e2);
            return null;
        }
    }

    @DexIgnore
    public static boolean a(long j, long j2, long j3) {
        if (j2 < 0 || j3 <= 0) {
            return true;
        }
        return ((j > 0 ? 1 : (j == 0 ? 0 : -1)) >= 0 ? j % j3 : (((ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD % j3) + 1) + ((j & ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) % j3)) % j3) < j2;
    }

    @DexIgnore
    public static boolean a(Context context) {
        if (g == null) {
            g = Boolean.valueOf(bn0.b(context).a("com.google.android.providers.gsf.permission.READ_GSERVICES") == 0);
        }
        return g.booleanValue();
    }

    @DexIgnore
    public static long b(Context context) {
        if (h == null) {
            long j = 0;
            if (context == null) {
                return 0;
            }
            if (a(context)) {
                j = wy0.a(context.getContentResolver(), "android_id", 0);
            }
            h = Long.valueOf(j);
        }
        return h.longValue();
    }

    @DexIgnore
    public final boolean a(rd0 rd0) {
        List<ey0.b> list;
        uy0 uy0 = rd0.e;
        String str = uy0.k;
        int i2 = uy0.g;
        jy0 jy0 = rd0.m;
        int i3 = jy0 != null ? jy0.j : 0;
        String str2 = null;
        if (!i.a().booleanValue()) {
            if (str == null || str.isEmpty()) {
                str = i2 >= 0 ? String.valueOf(i2) : null;
            }
            if (str == null) {
                return true;
            }
            Context context = this.a;
            if (context != null && a(context)) {
                ys0<String> ys0 = f.get(str);
                if (ys0 == null) {
                    ys0 = d.a(str, (String) null);
                    f.put(str, ys0);
                }
                str2 = ys0.a();
            }
            ey0.b a2 = a(str2);
            if (a2 != null) {
                return a(a(a2.k(), b(this.a)), a2.l(), a2.m());
            }
            return true;
        }
        if (str == null || str.isEmpty()) {
            str = i2 >= 0 ? String.valueOf(i2) : null;
        }
        if (str == null) {
            return true;
        }
        if (this.a == null) {
            list = Collections.emptyList();
        } else {
            ys0 ys02 = e.get(str);
            if (ys02 == null) {
                ys02 = c.a(str, ey0.j(), ty0.a);
                ys0 putIfAbsent = e.putIfAbsent(str, ys02);
                if (putIfAbsent != null) {
                    ys02 = putIfAbsent;
                }
            }
            list = ((ey0) ys02.a()).i();
        }
        for (ey0.b next : list) {
            if ((!next.j() || next.i() == 0 || next.i() == i3) && !a(a(next.k(), b(this.a)), next.l(), next.m())) {
                return false;
            }
        }
        return true;
    }
}
