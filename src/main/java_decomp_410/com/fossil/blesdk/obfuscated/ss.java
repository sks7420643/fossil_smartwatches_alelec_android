package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ss implements mo<ByteBuffer, Bitmap> {
    @DexIgnore
    public /* final */ os a; // = new os();

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer, lo loVar) throws IOException {
        return true;
    }

    @DexIgnore
    public aq<Bitmap> a(ByteBuffer byteBuffer, int i, int i2, lo loVar) throws IOException {
        return this.a.a(ImageDecoder.createSource(byteBuffer), i, i2, loVar);
    }
}
