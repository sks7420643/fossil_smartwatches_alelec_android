package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m44<K, V> implements Factory<Map<K, Provider<V>>>, s34<Map<K, Provider<V>>> {
    @DexIgnore
    public /* final */ Map<K, Provider<V>> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<K, V> {
        @DexIgnore
        public /* final */ LinkedHashMap<K, Provider<V>> a;

        @DexIgnore
        public b<K, V> a(K k, Provider<V> provider) {
            LinkedHashMap<K, Provider<V>> linkedHashMap = this.a;
            n44.a(k, "key");
            n44.a(provider, "provider");
            linkedHashMap.put(k, provider);
            return this;
        }

        @DexIgnore
        public b(int i) {
            this.a = k44.b(i);
        }

        @DexIgnore
        public m44<K, V> a() {
            return new m44<>(this.a);
        }
    }

    @DexIgnore
    public static <K, V> b<K, V> a(int i) {
        return new b<>(i);
    }

    @DexIgnore
    public m44(Map<K, Provider<V>> map) {
        this.a = Collections.unmodifiableMap(map);
    }

    @DexIgnore
    public Map<K, Provider<V>> get() {
        return this.a;
    }
}
