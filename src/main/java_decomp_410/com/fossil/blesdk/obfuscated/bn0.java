package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bn0 {
    @DexIgnore
    public static bn0 b; // = new bn0();
    @DexIgnore
    public an0 a; // = null;

    @DexIgnore
    public static an0 b(Context context) {
        return b.a(context);
    }

    @DexIgnore
    public final synchronized an0 a(Context context) {
        if (this.a == null) {
            if (context.getApplicationContext() != null) {
                context = context.getApplicationContext();
            }
            this.a = new an0(context);
        }
        return this.a;
    }
}
