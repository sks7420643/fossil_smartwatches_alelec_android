package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.NumberPicker;
import java.text.DateFormatSymbols;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cs2 extends xs3 implements cm3 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a((fd4) null);
    @DexIgnore
    public bm3 m;
    @DexIgnore
    public tr3<o92> n;
    @DexIgnore
    public ju3 o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return cs2.q;
        }

        @DexIgnore
        public final cs2 b() {
            return new cs2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ cs2 a;

        @DexIgnore
        public b(cs2 cs2) {
            this.a = cs2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = cs2.r.a();
            local.d(a2, "Month was changed from " + i + " to " + i2);
            cs2.a(this.a).b(i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ cs2 a;

        @DexIgnore
        public c(cs2 cs2) {
            this.a = cs2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = cs2.r.a();
            local.d(a2, "Day was changed from " + i + " to " + i2);
            cs2.a(this.a).a(i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ cs2 a;

        @DexIgnore
        public d(cs2 cs2) {
            this.a = cs2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = cs2.r.a();
            local.d(a2, "Year was changed from " + i + " to " + i2);
            cs2.a(this.a).c(i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cs2 e;

        @DexIgnore
        public e(cs2 cs2) {
            this.e = cs2;
        }

        @DexIgnore
        public final void onClick(View view) {
            cs2.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cs2 e;

        @DexIgnore
        public f(cs2 cs2) {
            this.e = cs2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.dismiss();
        }
    }

    /*
    static {
        String simpleName = cs2.class.getSimpleName();
        kd4.a((Object) simpleName, "BirthdayFragment::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ bm3 a(cs2 cs2) {
        bm3 bm3 = cs2.m;
        if (bm3 != null) {
            return bm3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final o92 P0() {
        tr3<o92> tr3 = this.n;
        if (tr3 != null) {
            return tr3.a();
        }
        return null;
    }

    @DexIgnore
    public void b(int i, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "Update day range: from " + i + " to " + i2);
        o92 P0 = P0();
        if (P0 != null) {
            NumberPicker numberPicker = P0.s;
            if (numberPicker != null) {
                kd4.a((Object) numberPicker, "npDay");
                numberPicker.setMinValue(i);
                numberPicker.setMaxValue(i2);
            }
        }
    }

    @DexIgnore
    public void c(Date date) {
        kd4.b(date, "birthday");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProfileSetupFragment", "birthDay: " + date);
        ju3 ju3 = this.o;
        if (ju3 != null) {
            ju3.c().a(date);
            dismiss();
            return;
        }
        kd4.d("mUserBirthDayViewModel");
        throw null;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, R.style.BottomDialog);
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        FLogger.INSTANCE.getLocal().d(q, "onCreateDialog");
        View inflate = View.inflate(getContext(), R.layout.fragment_birthday, (ViewGroup) null);
        kd4.a((Object) inflate, "view");
        inflate.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        Context context = getContext();
        if (context != null) {
            sr1 sr1 = new sr1(context, getTheme());
            sr1.setContentView(inflate);
            sr1.setCanceledOnTouchOutside(true);
            return sr1;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        ViewDataBinding a2 = qa.a(layoutInflater, (int) R.layout.fragment_birthday, viewGroup, false);
        kd4.a((Object) a2, "DataBindingUtil.inflate(\u2026rthday, container, false)");
        o92 o92 = (o92) a2;
        this.n = new tr3<>(this, o92);
        return o92.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        bm3 bm3 = this.m;
        if (bm3 != null) {
            bm3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        bm3 bm3 = this.m;
        if (bm3 != null) {
            bm3.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ic a2 = lc.a(activity).a(ju3.class);
            kd4.a((Object) a2, "ViewModelProviders.of(ac\u2026DayViewModel::class.java)");
            this.o = (ju3) a2;
            o92 P0 = P0();
            if (P0 != null) {
                NumberPicker numberPicker = P0.t;
                if (numberPicker != null) {
                    DateFormatSymbols instance = DateFormatSymbols.getInstance(Locale.getDefault());
                    kd4.a((Object) instance, "DateFormatSymbols.getInstance(Locale.getDefault())");
                    numberPicker.setDisplayedValues(instance.getShortMonths());
                    numberPicker.setOnValueChangedListener(new b(this));
                }
            }
            o92 P02 = P0();
            if (P02 != null) {
                NumberPicker numberPicker2 = P02.s;
                if (numberPicker2 != null) {
                    numberPicker2.setOnValueChangedListener(new c(this));
                }
            }
            o92 P03 = P0();
            if (P03 != null) {
                NumberPicker numberPicker3 = P03.u;
                if (numberPicker3 != null) {
                    numberPicker3.setOnValueChangedListener(new d(this));
                }
            }
            o92 P04 = P0();
            if (P04 != null) {
                FlexibleButton flexibleButton = P04.r;
                if (flexibleButton != null) {
                    flexibleButton.setOnClickListener(new e(this));
                }
            }
            o92 P05 = P0();
            if (P05 != null) {
                FlexibleButton flexibleButton2 = P05.q;
                if (flexibleButton2 != null) {
                    flexibleButton2.setOnClickListener(new f(this));
                    return;
                }
                return;
            }
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void a(bm3 bm3) {
        kd4.b(bm3, "presenter");
        this.m = bm3;
    }

    @DexIgnore
    public void a(Pair<Integer, Integer> pair, Pair<Integer, Integer> pair2, Pair<Integer, Integer> pair3) {
        kd4.b(pair, "dayRange");
        kd4.b(pair2, "monthRange");
        kd4.b(pair3, "yearRange");
        o92 P0 = P0();
        if (P0 != null) {
            NumberPicker numberPicker = P0.u;
            if (numberPicker != null) {
                kd4.a((Object) numberPicker, "npYear");
                numberPicker.setMinValue(pair3.getFirst().intValue());
                numberPicker.setMaxValue(pair3.getSecond().intValue());
            }
        }
        o92 P02 = P0();
        if (P02 != null) {
            NumberPicker numberPicker2 = P02.t;
            if (numberPicker2 != null) {
                kd4.a((Object) numberPicker2, "npMonth");
                numberPicker2.setMinValue(pair2.getFirst().intValue());
                numberPicker2.setMaxValue(pair2.getSecond().intValue());
            }
        }
        o92 P03 = P0();
        if (P03 != null) {
            NumberPicker numberPicker3 = P03.s;
            if (numberPicker3 != null) {
                kd4.a((Object) numberPicker3, "npDay");
                numberPicker3.setMinValue(pair.getFirst().intValue());
                numberPicker3.setMaxValue(pair.getSecond().intValue());
            }
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            int i = arguments.getInt("DAY");
            int i2 = arguments.getInt("MONTH");
            int i3 = arguments.getInt("YEAR");
            o92 P04 = P0();
            if (P04 != null) {
                NumberPicker numberPicker4 = P04.s;
                if (numberPicker4 != null) {
                    numberPicker4.setValue(i);
                }
            }
            o92 P05 = P0();
            if (P05 != null) {
                NumberPicker numberPicker5 = P05.t;
                if (numberPicker5 != null) {
                    numberPicker5.setValue(i2);
                }
            }
            o92 P06 = P0();
            if (P06 != null) {
                NumberPicker numberPicker6 = P06.u;
                if (numberPicker6 != null) {
                    numberPicker6.setValue(i3);
                }
            }
            bm3 bm3 = this.m;
            if (bm3 != null) {
                bm3.a(i);
                bm3 bm32 = this.m;
                if (bm32 != null) {
                    bm32.b(i2);
                    bm3 bm33 = this.m;
                    if (bm33 != null) {
                        bm33.c(i3);
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
    }
}
