package com.fossil.blesdk.obfuscated;

import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface zb4 extends CoroutineContext.a {
    @DexIgnore
    public static final b b = b.a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <E extends CoroutineContext.a> E a(zb4 zb4, CoroutineContext.b<E> bVar) {
            kd4.b(bVar, "key");
            if (bVar != zb4.b) {
                return null;
            }
            if (zb4 != null) {
                return zb4;
            }
            throw new TypeCastException("null cannot be cast to non-null type E");
        }

        @DexIgnore
        public static void a(zb4 zb4, yb4<?> yb4) {
            kd4.b(yb4, "continuation");
        }

        @DexIgnore
        public static CoroutineContext b(zb4 zb4, CoroutineContext.b<?> bVar) {
            kd4.b(bVar, "key");
            return bVar == zb4.b ? EmptyCoroutineContext.INSTANCE : zb4;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineContext.b<zb4> {
        @DexIgnore
        public static /* final */ /* synthetic */ b a; // = new b();
    }

    @DexIgnore
    void b(yb4<?> yb4);

    @DexIgnore
    <T> yb4<T> c(yb4<? super T> yb4);
}
