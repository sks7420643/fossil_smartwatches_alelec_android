package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzfe$zzb;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q51 implements y81 {
    @DexIgnore
    public static /* final */ y81 a; // = new q51();

    @DexIgnore
    public final boolean zzb(int i) {
        return zzfe$zzb.zzb.zzt(i) != null;
    }
}
