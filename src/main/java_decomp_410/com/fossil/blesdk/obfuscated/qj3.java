package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.enums.Unit;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qj3 extends zr2 implements yj3, View.OnClickListener, ws3.g {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public tr3<we2> j;
    @DexIgnore
    public xj3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return qj3.m;
        }

        @DexIgnore
        public final qj3 b() {
            return new qj3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qj3 e;

        @DexIgnore
        public b(qj3 qj3) {
            this.e = qj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qj3 e;

        @DexIgnore
        public c(qj3 qj3) {
            this.e = qj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            qj3.a(this.e).b(Unit.METRIC);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qj3 e;

        @DexIgnore
        public d(qj3 qj3) {
            this.e = qj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            qj3.a(this.e).b(Unit.IMPERIAL);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qj3 e;

        @DexIgnore
        public e(qj3 qj3) {
            this.e = qj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            qj3.a(this.e).d(Unit.METRIC);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qj3 e;

        @DexIgnore
        public f(qj3 qj3) {
            this.e = qj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            qj3.a(this.e).d(Unit.IMPERIAL);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qj3 e;

        @DexIgnore
        public g(qj3 qj3) {
            this.e = qj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            qj3.a(this.e).a(Unit.METRIC);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qj3 e;

        @DexIgnore
        public h(qj3 qj3) {
            this.e = qj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            qj3.a(this.e).a(Unit.IMPERIAL);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qj3 e;

        @DexIgnore
        public i(qj3 qj3) {
            this.e = qj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            qj3.a(this.e).c(Unit.METRIC);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qj3 e;

        @DexIgnore
        public j(qj3 qj3) {
            this.e = qj3;
        }

        @DexIgnore
        public final void onClick(View view) {
            qj3.a(this.e).c(Unit.IMPERIAL);
        }
    }

    /*
    static {
        String simpleName = qj3.class.getSimpleName();
        if (simpleName != null) {
            kd4.a((Object) simpleName, "PreferredUnitFragment::class.java.simpleName!!");
            m = simpleName;
            return;
        }
        kd4.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ xj3 a(qj3 qj3) {
        xj3 xj3 = qj3.k;
        if (xj3 != null) {
            return xj3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void c(Unit unit) {
        tr3<we2> tr3 = this.j;
        if (tr3 != null) {
            we2 a2 = tr3.a();
            if (a2 != null) {
                if (unit != null) {
                    int i2 = rj3.a[unit.ordinal()];
                    if (i2 == 1) {
                        TabLayout.g c2 = a2.s.c(1);
                        if (c2 != null) {
                            c2.g();
                            return;
                        }
                        return;
                    } else if (i2 == 2) {
                        TabLayout.g c3 = a2.s.c(0);
                        if (c3 != null) {
                            c3.g();
                            return;
                        }
                        return;
                    }
                }
                TabLayout.g c4 = a2.s.c(0);
                if (c4 != null) {
                    c4.g();
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d(Unit unit) {
        tr3<we2> tr3 = this.j;
        if (tr3 != null) {
            we2 a2 = tr3.a();
            if (a2 != null) {
                if (unit != null) {
                    int i2 = rj3.b[unit.ordinal()];
                    if (i2 == 1) {
                        TabLayout.g c2 = a2.u.c(1);
                        if (c2 != null) {
                            c2.g();
                            return;
                        }
                        return;
                    } else if (i2 == 2) {
                        TabLayout.g c3 = a2.u.c(0);
                        if (c3 != null) {
                            c3.g();
                            return;
                        }
                        return;
                    }
                }
                TabLayout.g c4 = a2.u.c(0);
                if (c4 != null) {
                    c4.g();
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void e(Unit unit) {
        tr3<we2> tr3 = this.j;
        if (tr3 != null) {
            we2 a2 = tr3.a();
            if (a2 != null) {
                if (unit != null) {
                    int i2 = rj3.c[unit.ordinal()];
                    if (i2 == 1) {
                        TabLayout.g c2 = a2.r.c(1);
                        if (c2 != null) {
                            c2.g();
                            return;
                        }
                        return;
                    } else if (i2 == 2) {
                        TabLayout.g c3 = a2.r.c(0);
                        if (c3 != null) {
                            c3.g();
                            return;
                        }
                        return;
                    }
                }
                TabLayout.g c4 = a2.r.c(0);
                if (c4 != null) {
                    c4.g();
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void n() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void onClick(View view) {
        kd4.b(view, "v");
        if (view.getId() == R.id.aciv_back) {
            n();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        Context context = getContext();
        if (context != null) {
            Integer.valueOf(k6.a(context, (int) R.color.white));
            Context context2 = getContext();
            if (context2 != null) {
                Integer.valueOf(k6.a(context2, (int) R.color.fossilOrange));
                we2 we2 = (we2) qa.a(LayoutInflater.from(getContext()), R.layout.fragment_preferred_unit, (ViewGroup) null, false, O0());
                we2.q.setOnClickListener(new b(this));
                View childAt = we2.s.getChildAt(0);
                if (childAt != null) {
                    View childAt2 = ((LinearLayout) childAt).getChildAt(0);
                    if (childAt2 != null) {
                        childAt2.setOnClickListener(new c(this));
                    }
                    View childAt3 = we2.s.getChildAt(0);
                    if (childAt3 != null) {
                        View childAt4 = ((LinearLayout) childAt3).getChildAt(1);
                        if (childAt4 != null) {
                            childAt4.setOnClickListener(new d(this));
                        }
                        View childAt5 = we2.u.getChildAt(0);
                        if (childAt5 != null) {
                            View childAt6 = ((LinearLayout) childAt5).getChildAt(0);
                            if (childAt6 != null) {
                                childAt6.setOnClickListener(new e(this));
                            }
                            View childAt7 = we2.u.getChildAt(0);
                            if (childAt7 != null) {
                                View childAt8 = ((LinearLayout) childAt7).getChildAt(1);
                                if (childAt8 != null) {
                                    childAt8.setOnClickListener(new f(this));
                                }
                                View childAt9 = we2.r.getChildAt(0);
                                if (childAt9 != null) {
                                    View childAt10 = ((LinearLayout) childAt9).getChildAt(0);
                                    if (childAt10 != null) {
                                        childAt10.setOnClickListener(new g(this));
                                    }
                                    View childAt11 = we2.r.getChildAt(0);
                                    if (childAt11 != null) {
                                        View childAt12 = ((LinearLayout) childAt11).getChildAt(1);
                                        if (childAt12 != null) {
                                            childAt12.setOnClickListener(new h(this));
                                        }
                                        View childAt13 = we2.t.getChildAt(0);
                                        if (childAt13 != null) {
                                            View childAt14 = ((LinearLayout) childAt13).getChildAt(0);
                                            if (childAt14 != null) {
                                                childAt14.setOnClickListener(new i(this));
                                            }
                                            View childAt15 = we2.t.getChildAt(0);
                                            if (childAt15 != null) {
                                                View childAt16 = ((LinearLayout) childAt15).getChildAt(1);
                                                if (childAt16 != null) {
                                                    childAt16.setOnClickListener(new j(this));
                                                }
                                                this.j = new tr3<>(this, we2);
                                                kd4.a((Object) we2, "binding");
                                                return we2.d();
                                            }
                                            throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
                                        }
                                        throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
                                    }
                                    throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
                                }
                                throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
                            }
                            throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
                        }
                        throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
                    }
                    throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
                }
                throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        xj3 xj3 = this.k;
        if (xj3 != null) {
            xj3.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        xj3 xj3 = this.k;
        if (xj3 != null) {
            xj3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(xj3 xj3) {
        kd4.b(xj3, "presenter");
        st1.a(xj3);
        kd4.a((Object) xj3, "checkNotNull(presenter)");
        this.k = xj3;
    }

    @DexIgnore
    public void a(Unit unit) {
        tr3<we2> tr3 = this.j;
        if (tr3 != null) {
            we2 a2 = tr3.a();
            if (a2 != null) {
                if (unit != null) {
                    int i2 = rj3.d[unit.ordinal()];
                    if (i2 == 1) {
                        TabLayout.g c2 = a2.t.c(1);
                        if (c2 != null) {
                            c2.g();
                            return;
                        }
                        return;
                    } else if (i2 == 2) {
                        TabLayout.g c3 = a2.t.c(0);
                        if (c3 != null) {
                            c3.g();
                            return;
                        }
                        return;
                    }
                }
                TabLayout.g c4 = a2.t.c(0);
                if (c4 != null) {
                    c4.g();
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(int i2, String str) {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        kd4.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, "Inside .onDialogFragmentResult with TAG=" + str);
    }
}
