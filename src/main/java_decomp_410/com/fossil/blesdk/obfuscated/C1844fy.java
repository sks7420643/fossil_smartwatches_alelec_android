package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fy */
public class C1844fy implements com.fossil.blesdk.obfuscated.C2081iy {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1518bx f5326a;

    @DexIgnore
    public C1844fy(com.fossil.blesdk.obfuscated.C1518bx bxVar) {
        this.f5326a = bxVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1844fy m7184a() throws java.lang.NoClassDefFoundError, java.lang.IllegalStateException {
        return m7185a(com.fossil.blesdk.obfuscated.C1518bx.m5147w());
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1844fy m7185a(com.fossil.blesdk.obfuscated.C1518bx bxVar) throws java.lang.IllegalStateException {
        if (bxVar != null) {
            return new com.fossil.blesdk.obfuscated.C1844fy(bxVar);
        }
        throw new java.lang.IllegalStateException("Answers must be initialized before logging kit events");
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11086a(com.fossil.blesdk.obfuscated.C1983hy hyVar) {
        try {
            this.f5326a.mo9326a(hyVar.mo11790a());
        } catch (Throwable th) {
            android.util.Log.w("AnswersKitEventLogger", "Unexpected error sending Answers event", th);
        }
    }
}
