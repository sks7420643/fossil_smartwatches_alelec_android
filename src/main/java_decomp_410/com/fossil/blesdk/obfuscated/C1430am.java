package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.am */
public class C1430am implements com.fossil.blesdk.obfuscated.C3444zl {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2973tl f3526a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.os.Handler f3527b; // = new android.os.Handler(android.os.Looper.getMainLooper());

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.concurrent.Executor f3528c; // = new com.fossil.blesdk.obfuscated.C1430am.C1431a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.am$a")
    /* renamed from: com.fossil.blesdk.obfuscated.am$a */
    public class C1431a implements java.util.concurrent.Executor {
        @DexIgnore
        public C1431a() {
        }

        @DexIgnore
        public void execute(java.lang.Runnable runnable) {
            com.fossil.blesdk.obfuscated.C1430am.this.mo8790b(runnable);
        }
    }

    @DexIgnore
    public C1430am(java.util.concurrent.Executor executor) {
        this.f3526a = new com.fossil.blesdk.obfuscated.C2973tl(executor);
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.concurrent.Executor mo8787a() {
        return this.f3528c;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo8790b(java.lang.Runnable runnable) {
        this.f3527b.post(runnable);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8788a(java.lang.Runnable runnable) {
        this.f3526a.execute(runnable);
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2973tl mo8789b() {
        return this.f3526a;
    }
}
