package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.t81;
import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zztv;
import com.google.android.gms.internal.measurement.zzui;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import sun.misc.Unsafe;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class aa1<T> implements na1<T> {
    @DexIgnore
    public static /* final */ int[] r; // = new int[0];
    @DexIgnore
    public static /* final */ Unsafe s; // = kb1.d();
    @DexIgnore
    public /* final */ int[] a;
    @DexIgnore
    public /* final */ Object[] b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ w91 e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public /* final */ int[] j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ ea1 m;
    @DexIgnore
    public /* final */ i91 n;
    @DexIgnore
    public /* final */ eb1<?, ?> o;
    @DexIgnore
    public /* final */ j81<?> p;
    @DexIgnore
    public /* final */ r91 q;

    @DexIgnore
    public aa1(int[] iArr, Object[] objArr, int i2, int i3, w91 w91, boolean z, boolean z2, int[] iArr2, int i4, int i5, ea1 ea1, i91 i91, eb1<?, ?> eb1, j81<?> j81, r91 r91) {
        this.a = iArr;
        this.b = objArr;
        this.c = i2;
        this.d = i3;
        this.g = w91 instanceof t81;
        this.h = z;
        this.f = j81 != null && j81.a(w91);
        this.i = false;
        this.j = iArr2;
        this.k = i4;
        this.l = i5;
        this.m = ea1;
        this.n = i91;
        this.o = eb1;
        this.p = j81;
        this.e = w91;
        this.q = r91;
    }

    @DexIgnore
    public static <T> aa1<T> a(Class<T> cls, u91 u91, ea1 ea1, i91 i91, eb1<?, ?> eb1, j81<?> j81, r91 r91) {
        int i2;
        int i3;
        char c2;
        int[] iArr;
        char c3;
        char c4;
        int i4;
        char c5;
        char c6;
        int i5;
        int i6;
        String str;
        char c7;
        int i7;
        char c8;
        int i8;
        int i9;
        int i10;
        int i11;
        Class<?> cls2;
        int i12;
        int i13;
        Field field;
        int i14;
        char charAt;
        int i15;
        char c9;
        Field field2;
        Field field3;
        int i16;
        char charAt2;
        int i17;
        char charAt3;
        int i18;
        char charAt4;
        int i19;
        int i20;
        int i21;
        int i22;
        int i23;
        int i24;
        char charAt5;
        int i25;
        char charAt6;
        int i26;
        char charAt7;
        int i27;
        char charAt8;
        char charAt9;
        char charAt10;
        char charAt11;
        char charAt12;
        char charAt13;
        char charAt14;
        u91 u912 = u91;
        if (u912 instanceof la1) {
            la1 la1 = (la1) u912;
            char c10 = 0;
            boolean z = la1.c() == t81.e.j;
            String d2 = la1.d();
            int length = d2.length();
            char charAt15 = d2.charAt(0);
            if (charAt15 >= 55296) {
                char c11 = charAt15 & 8191;
                int i28 = 1;
                int i29 = 13;
                while (true) {
                    i2 = i28 + 1;
                    charAt14 = d2.charAt(i28);
                    if (charAt14 < 55296) {
                        break;
                    }
                    c11 |= (charAt14 & 8191) << i29;
                    i29 += 13;
                    i28 = i2;
                }
                charAt15 = (charAt14 << i29) | c11;
            } else {
                i2 = 1;
            }
            int i30 = i2 + 1;
            char charAt16 = d2.charAt(i2);
            if (charAt16 >= 55296) {
                char c12 = charAt16 & 8191;
                int i31 = 13;
                while (true) {
                    i3 = i30 + 1;
                    charAt13 = d2.charAt(i30);
                    if (charAt13 < 55296) {
                        break;
                    }
                    c12 |= (charAt13 & 8191) << i31;
                    i31 += 13;
                    i30 = i3;
                }
                charAt16 = c12 | (charAt13 << i31);
            } else {
                i3 = i30;
            }
            if (charAt16 == 0) {
                iArr = r;
                c6 = 0;
                c5 = 0;
                i4 = 0;
                c4 = 0;
                c3 = 0;
                c2 = 0;
            } else {
                int i32 = i3 + 1;
                char charAt17 = d2.charAt(i3);
                if (charAt17 >= 55296) {
                    char c13 = charAt17 & 8191;
                    int i33 = 13;
                    while (true) {
                        i19 = i32 + 1;
                        charAt12 = d2.charAt(i32);
                        if (charAt12 < 55296) {
                            break;
                        }
                        c13 |= (charAt12 & 8191) << i33;
                        i33 += 13;
                        i32 = i19;
                    }
                    charAt17 = (charAt12 << i33) | c13;
                } else {
                    i19 = i32;
                }
                int i34 = i19 + 1;
                char charAt18 = d2.charAt(i19);
                if (charAt18 >= 55296) {
                    char c14 = charAt18 & 8191;
                    int i35 = 13;
                    while (true) {
                        i20 = i34 + 1;
                        charAt11 = d2.charAt(i34);
                        if (charAt11 < 55296) {
                            break;
                        }
                        c14 |= (charAt11 & 8191) << i35;
                        i35 += 13;
                        i34 = i20;
                    }
                    charAt18 = c14 | (charAt11 << i35);
                } else {
                    i20 = i34;
                }
                int i36 = i20 + 1;
                char charAt19 = d2.charAt(i20);
                if (charAt19 >= 55296) {
                    char c15 = charAt19 & 8191;
                    int i37 = 13;
                    while (true) {
                        i21 = i36 + 1;
                        charAt10 = d2.charAt(i36);
                        if (charAt10 < 55296) {
                            break;
                        }
                        c15 |= (charAt10 & 8191) << i37;
                        i37 += 13;
                        i36 = i21;
                    }
                    charAt19 = (charAt10 << i37) | c15;
                } else {
                    i21 = i36;
                }
                int i38 = i21 + 1;
                c4 = d2.charAt(i21);
                if (c4 >= 55296) {
                    char c16 = c4 & 8191;
                    int i39 = 13;
                    while (true) {
                        i22 = i38 + 1;
                        charAt9 = d2.charAt(i38);
                        if (charAt9 < 55296) {
                            break;
                        }
                        c16 |= (charAt9 & 8191) << i39;
                        i39 += 13;
                        i38 = i22;
                    }
                    c4 = (charAt9 << i39) | c16;
                } else {
                    i22 = i38;
                }
                int i40 = i22 + 1;
                c3 = d2.charAt(i22);
                if (c3 >= 55296) {
                    char c17 = c3 & 8191;
                    int i41 = 13;
                    while (true) {
                        i27 = i40 + 1;
                        charAt8 = d2.charAt(i40);
                        if (charAt8 < 55296) {
                            break;
                        }
                        c17 |= (charAt8 & 8191) << i41;
                        i41 += 13;
                        i40 = i27;
                    }
                    c3 = (charAt8 << i41) | c17;
                    i40 = i27;
                }
                int i42 = i40 + 1;
                c6 = d2.charAt(i40);
                if (c6 >= 55296) {
                    char c18 = c6 & 8191;
                    int i43 = 13;
                    while (true) {
                        i26 = i42 + 1;
                        charAt7 = d2.charAt(i42);
                        if (charAt7 < 55296) {
                            break;
                        }
                        c18 |= (charAt7 & 8191) << i43;
                        i43 += 13;
                        i42 = i26;
                    }
                    c6 = c18 | (charAt7 << i43);
                    i42 = i26;
                }
                int i44 = i42 + 1;
                char charAt20 = d2.charAt(i42);
                if (charAt20 >= 55296) {
                    int i45 = 13;
                    int i46 = i44;
                    char c19 = charAt20 & 8191;
                    int i47 = i46;
                    while (true) {
                        i25 = i47 + 1;
                        charAt6 = d2.charAt(i47);
                        if (charAt6 < 55296) {
                            break;
                        }
                        c19 |= (charAt6 & 8191) << i45;
                        i45 += 13;
                        i47 = i25;
                    }
                    charAt20 = c19 | (charAt6 << i45);
                    i23 = i25;
                } else {
                    i23 = i44;
                }
                int i48 = i23 + 1;
                c10 = d2.charAt(i23);
                if (c10 >= 55296) {
                    int i49 = 13;
                    int i50 = i48;
                    char c20 = c10 & 8191;
                    int i51 = i50;
                    while (true) {
                        i24 = i51 + 1;
                        charAt5 = d2.charAt(i51);
                        if (charAt5 < 55296) {
                            break;
                        }
                        c20 |= (charAt5 & 8191) << i49;
                        i49 += 13;
                        i51 = i24;
                    }
                    c10 = c20 | (charAt5 << i49);
                    i48 = i24;
                }
                iArr = new int[(c10 + c6 + charAt20)];
                i4 = (charAt17 << 1) + charAt18;
                int i52 = i48;
                c2 = charAt17;
                c5 = charAt19;
                i3 = i52;
            }
            Unsafe unsafe = s;
            Object[] e2 = la1.e();
            Class<?> cls3 = la1.a().getClass();
            int i53 = i4;
            int[] iArr2 = new int[(c3 * 3)];
            Object[] objArr = new Object[(c3 << 1)];
            int i54 = c10 + c6;
            char c21 = c10;
            int i55 = i54;
            int i56 = 0;
            int i57 = 0;
            while (i3 < length) {
                int i58 = i3 + 1;
                char charAt21 = d2.charAt(i3);
                char c22 = 55296;
                if (charAt21 >= 55296) {
                    int i59 = 13;
                    int i60 = i58;
                    char c23 = charAt21 & 8191;
                    int i61 = i60;
                    while (true) {
                        i18 = i61 + 1;
                        charAt4 = d2.charAt(i61);
                        if (charAt4 < c22) {
                            break;
                        }
                        c23 |= (charAt4 & 8191) << i59;
                        i59 += 13;
                        i61 = i18;
                        c22 = 55296;
                    }
                    charAt21 = c23 | (charAt4 << i59);
                    i5 = i18;
                } else {
                    i5 = i58;
                }
                int i62 = i5 + 1;
                char charAt22 = d2.charAt(i5);
                int i63 = length;
                char c24 = 55296;
                if (charAt22 >= 55296) {
                    int i64 = 13;
                    int i65 = i62;
                    char c25 = charAt22 & 8191;
                    int i66 = i65;
                    while (true) {
                        i17 = i66 + 1;
                        charAt3 = d2.charAt(i66);
                        if (charAt3 < c24) {
                            break;
                        }
                        c25 |= (charAt3 & 8191) << i64;
                        i64 += 13;
                        i66 = i17;
                        c24 = 55296;
                    }
                    charAt22 = c25 | (charAt3 << i64);
                    i6 = i17;
                } else {
                    i6 = i62;
                }
                char c26 = c10;
                char c27 = charAt22 & 255;
                boolean z2 = z;
                if ((charAt22 & 1024) != 0) {
                    iArr[i56] = i57;
                    i56++;
                }
                int i67 = i56;
                if (c27 >= '3') {
                    int i68 = i6 + 1;
                    char charAt23 = d2.charAt(i6);
                    char c28 = 55296;
                    if (charAt23 >= 55296) {
                        char c29 = charAt23 & 8191;
                        int i69 = 13;
                        while (true) {
                            i16 = i68 + 1;
                            charAt2 = d2.charAt(i68);
                            if (charAt2 < c28) {
                                break;
                            }
                            c29 |= (charAt2 & 8191) << i69;
                            i69 += 13;
                            i68 = i16;
                            c28 = 55296;
                        }
                        charAt23 = c29 | (charAt2 << i69);
                        i68 = i16;
                    }
                    int i70 = c27 - '3';
                    int i71 = i68;
                    if (i70 == 9 || i70 == 17) {
                        c9 = 1;
                        objArr[((i57 / 3) << 1) + 1] = e2[i53];
                        i53++;
                    } else {
                        if (i70 == 12 && (charAt15 & 1) == 1) {
                            objArr[((i57 / 3) << 1) + 1] = e2[i53];
                            i53++;
                        }
                        c9 = 1;
                    }
                    int i72 = charAt23 << c9;
                    Object obj = e2[i72];
                    if (obj instanceof Field) {
                        field2 = (Field) obj;
                    } else {
                        field2 = a(cls3, (String) obj);
                        e2[i72] = field2;
                    }
                    char c30 = c5;
                    int objectFieldOffset = (int) unsafe.objectFieldOffset(field2);
                    int i73 = i72 + 1;
                    Object obj2 = e2[i73];
                    int i74 = objectFieldOffset;
                    if (obj2 instanceof Field) {
                        field3 = (Field) obj2;
                    } else {
                        field3 = a(cls3, (String) obj2);
                        e2[i73] = field3;
                    }
                    str = d2;
                    i10 = (int) unsafe.objectFieldOffset(field3);
                    cls2 = cls3;
                    i7 = i53;
                    i9 = i74;
                    i11 = 0;
                    c7 = c30;
                    c8 = c4;
                    i8 = charAt21;
                    i13 = i71;
                } else {
                    char c31 = c5;
                    int i75 = i53 + 1;
                    Field a2 = a(cls3, (String) e2[i53]);
                    c8 = c4;
                    if (c27 == 9 || c27 == 17) {
                        c7 = c31;
                        objArr[((i57 / 3) << 1) + 1] = a2.getType();
                    } else {
                        if (c27 == 27 || c27 == '1') {
                            c7 = c31;
                            i15 = i75 + 1;
                            objArr[((i57 / 3) << 1) + 1] = e2[i75];
                        } else if (c27 == 12 || c27 == 30 || c27 == ',') {
                            c7 = c31;
                            if ((charAt15 & 1) == 1) {
                                i15 = i75 + 1;
                                objArr[((i57 / 3) << 1) + 1] = e2[i75];
                            }
                        } else if (c27 == '2') {
                            int i76 = c21 + 1;
                            iArr[c21] = i57;
                            int i77 = (i57 / 3) << 1;
                            int i78 = i75 + 1;
                            objArr[i77] = e2[i75];
                            if ((charAt22 & 2048) != 0) {
                                i75 = i78 + 1;
                                objArr[i77 + 1] = e2[i78];
                                c7 = c31;
                                c21 = i76;
                            } else {
                                c21 = i76;
                                i75 = i78;
                                c7 = c31;
                            }
                        } else {
                            c7 = c31;
                        }
                        i8 = charAt21;
                        i75 = i15;
                        i9 = (int) unsafe.objectFieldOffset(a2);
                        if ((charAt15 & 1) == 1 || c27 > 17) {
                            str = d2;
                            cls2 = cls3;
                            i7 = i75;
                            i12 = i6;
                            i11 = 0;
                            i10 = 0;
                        } else {
                            i12 = i6 + 1;
                            char charAt24 = d2.charAt(i6);
                            if (charAt24 >= 55296) {
                                char c32 = charAt24 & 8191;
                                int i79 = 13;
                                while (true) {
                                    i14 = i12 + 1;
                                    charAt = d2.charAt(i12);
                                    if (charAt < 55296) {
                                        break;
                                    }
                                    c32 |= (charAt & 8191) << i79;
                                    i79 += 13;
                                    i12 = i14;
                                }
                                charAt24 = c32 | (charAt << i79);
                                i12 = i14;
                            }
                            int i80 = (c2 << 1) + (charAt24 / ' ');
                            Object obj3 = e2[i80];
                            str = d2;
                            if (obj3 instanceof Field) {
                                field = (Field) obj3;
                            } else {
                                field = a(cls3, (String) obj3);
                                e2[i80] = field;
                            }
                            cls2 = cls3;
                            i7 = i75;
                            i10 = (int) unsafe.objectFieldOffset(field);
                            i11 = charAt24 % ' ';
                        }
                        if (c27 >= 18 && c27 <= '1') {
                            iArr[i55] = i9;
                            i55++;
                        }
                        i13 = i12;
                    }
                    i8 = charAt21;
                    i9 = (int) unsafe.objectFieldOffset(a2);
                    if ((charAt15 & 1) == 1) {
                    }
                    str = d2;
                    cls2 = cls3;
                    i7 = i75;
                    i12 = i6;
                    i11 = 0;
                    i10 = 0;
                    iArr[i55] = i9;
                    i55++;
                    i13 = i12;
                }
                int i81 = i57 + 1;
                iArr2[i57] = i8;
                int i82 = i81 + 1;
                iArr2[i81] = (c27 << 20) | ((charAt22 & 256) != 0 ? 268435456 : 0) | ((charAt22 & 512) != 0 ? 536870912 : 0) | i9;
                i57 = i82 + 1;
                iArr2[i82] = (i11 << 20) | i10;
                cls3 = cls2;
                c4 = c8;
                c10 = c26;
                i53 = i7;
                length = i63;
                z = z2;
                c5 = c7;
                i56 = i67;
                d2 = str;
            }
            boolean z3 = z;
            return new aa1(iArr2, objArr, c5, c4, la1.a(), z, false, iArr, c10, i54, ea1, i91, eb1, j81, r91);
        }
        ((ab1) u912).c();
        throw null;
    }

    @DexIgnore
    public static boolean f(int i2) {
        return (i2 & 536870912) != 0;
    }

    @DexIgnore
    public static <T> boolean f(T t, long j2) {
        return ((Boolean) kb1.f(t, j2)).booleanValue();
    }

    @DexIgnore
    public final void b(T t, T t2) {
        if (t2 != null) {
            for (int i2 = 0; i2 < this.a.length; i2 += 3) {
                int d2 = d(i2);
                long j2 = (long) (1048575 & d2);
                int i3 = this.a[i2];
                switch ((d2 & 267386880) >>> 20) {
                    case 0:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.e(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 1:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.d(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 2:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.b(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 3:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.b(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 4:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.a((Object) t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 5:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.b(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 6:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.a((Object) t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 7:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.c(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 8:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.f(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 9:
                        a(t, t2, i2);
                        break;
                    case 10:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.f(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 11:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.a((Object) t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 12:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.a((Object) t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 13:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.a((Object) t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 14:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.b(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 15:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.a((Object) t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 16:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.b(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 17:
                        a(t, t2, i2);
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.n.a(t, t2, j2);
                        break;
                    case 50:
                        pa1.a(this.q, t, t2, j2);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (!a(t2, i3, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.f(t2, j2));
                            b(t, i3, i2);
                            break;
                        }
                    case 60:
                        b(t, t2, i2);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (!a(t2, i3, i2)) {
                            break;
                        } else {
                            kb1.a((Object) t, j2, kb1.f(t2, j2));
                            b(t, i3, i2);
                            break;
                        }
                    case 68:
                        b(t, t2, i2);
                        break;
                }
            }
            if (!this.h) {
                pa1.a(this.o, t, t2);
                if (this.f) {
                    pa1.a(this.p, t, t2);
                    return;
                }
                return;
            }
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final y81 c(int i2) {
        return (y81) this.b[((i2 / 3) << 1) + 1];
    }

    @DexIgnore
    public final void d(T t) {
        int i2;
        int i3 = this.k;
        while (true) {
            i2 = this.l;
            if (i3 >= i2) {
                break;
            }
            long d2 = (long) (d(this.j[i3]) & 1048575);
            Object f2 = kb1.f(t, d2);
            if (f2 != null) {
                this.q.a(f2);
                kb1.a((Object) t, d2, f2);
            }
            i3++;
        }
        int length = this.j.length;
        while (i2 < length) {
            this.n.b(t, (long) this.j[i2]);
            i2++;
        }
        this.o.f(t);
        if (this.f) {
            this.p.c(t);
        }
    }

    @DexIgnore
    public final int e(int i2) {
        return this.a[i2 + 2];
    }

    @DexIgnore
    public static <T> long e(T t, long j2) {
        return ((Long) kb1.f(t, j2)).longValue();
    }

    @DexIgnore
    public final boolean c(T t) {
        int i2;
        int i3 = 0;
        int i4 = -1;
        int i5 = 0;
        while (true) {
            boolean z = true;
            if (i3 >= this.k) {
                return !this.f || this.p.a((Object) t).d();
            }
            int i6 = this.j[i3];
            int i7 = this.a[i6];
            int d2 = d(i6);
            if (!this.h) {
                int i8 = this.a[i6 + 2];
                int i9 = i8 & 1048575;
                i2 = 1 << (i8 >>> 20);
                if (i9 != i4) {
                    i5 = s.getInt(t, (long) i9);
                    i4 = i9;
                }
            } else {
                i2 = 0;
            }
            if (((268435456 & d2) != 0) && !a(t, i6, i5, i2)) {
                return false;
            }
            int i10 = (267386880 & d2) >>> 20;
            if (i10 != 9 && i10 != 17) {
                if (i10 != 27) {
                    if (i10 == 60 || i10 == 68) {
                        if (a(t, i7, i6) && !a((Object) t, d2, a(i6))) {
                            return false;
                        }
                    } else if (i10 != 49) {
                        if (i10 == 50 && !this.q.e(kb1.f(t, (long) (d2 & 1048575))).isEmpty()) {
                            this.q.c(b(i6));
                            throw null;
                        }
                    }
                }
                List list = (List) kb1.f(t, (long) (d2 & 1048575));
                if (!list.isEmpty()) {
                    na1 a2 = a(i6);
                    int i11 = 0;
                    while (true) {
                        if (i11 >= list.size()) {
                            break;
                        } else if (!a2.c(list.get(i11))) {
                            z = false;
                            break;
                        } else {
                            i11++;
                        }
                    }
                }
                if (!z) {
                    return false;
                }
            } else if (a(t, i6, i5, i2) && !a((Object) t, d2, a(i6))) {
                return false;
            }
            i3++;
        }
    }

    @DexIgnore
    public final int d(int i2) {
        return this.a[i2 + 1];
    }

    @DexIgnore
    public static <T> int d(T t, long j2) {
        return ((Integer) kb1.f(t, j2)).intValue();
    }

    @DexIgnore
    public static <T> float c(T t, long j2) {
        return ((Float) kb1.f(t, j2)).floatValue();
    }

    @DexIgnore
    public final boolean c(T t, T t2, int i2) {
        return a(t, i2) == a(t2, i2);
    }

    @DexIgnore
    public final void b(T t, T t2, int i2) {
        int d2 = d(i2);
        int i3 = this.a[i2];
        long j2 = (long) (d2 & 1048575);
        if (a(t2, i3, i2)) {
            Object f2 = kb1.f(t, j2);
            Object f3 = kb1.f(t2, j2);
            if (f2 != null && f3 != null) {
                kb1.a((Object) t, j2, v81.a(f2, f3));
                b(t, i3, i2);
            } else if (f3 != null) {
                kb1.a((Object) t, j2, f3);
                b(t, i3, i2);
            }
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:398:0x0833, code lost:
        r9 = (r9 + r10) + r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:418:0x090c, code lost:
        r13 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:434:0x0954, code lost:
        r5 = r5 + r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:471:0x09fe, code lost:
        r5 = r5 + r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:480:0x0a20, code lost:
        r3 = r3 + 3;
        r9 = r13;
     */
    @DexIgnore
    public final int b(T t) {
        int i2;
        int i3;
        long j2;
        int i4;
        int b2;
        int i5;
        int i6;
        int i7;
        int i8;
        int b3;
        int i9;
        int i10;
        int i11;
        T t2 = t;
        int i12 = 267386880;
        if (this.h) {
            Unsafe unsafe = s;
            int i13 = 0;
            int i14 = 0;
            while (i13 < this.a.length) {
                int d2 = d(i13);
                int i15 = (d2 & i12) >>> 20;
                int i16 = this.a[i13];
                long j3 = (long) (d2 & 1048575);
                int i17 = (i15 < zzui.zzbww.id() || i15 > zzui.zzbxj.id()) ? 0 : this.a[i13 + 2] & 1048575;
                switch (i15) {
                    case 0:
                        if (a(t2, i13)) {
                            b3 = zztv.b(i16, 0.0d);
                        } else {
                            continue;
                        }
                    case 1:
                        if (a(t2, i13)) {
                            b3 = zztv.b(i16, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        } else {
                            continue;
                        }
                    case 2:
                        if (a(t2, i13)) {
                            b3 = zztv.d(i16, kb1.b(t2, j3));
                        } else {
                            continue;
                        }
                    case 3:
                        if (a(t2, i13)) {
                            b3 = zztv.e(i16, kb1.b(t2, j3));
                        } else {
                            continue;
                        }
                    case 4:
                        if (a(t2, i13)) {
                            b3 = zztv.f(i16, kb1.a((Object) t2, j3));
                        } else {
                            continue;
                        }
                    case 5:
                        if (a(t2, i13)) {
                            b3 = zztv.g(i16, 0);
                        } else {
                            continue;
                        }
                    case 6:
                        if (a(t2, i13)) {
                            b3 = zztv.i(i16, 0);
                        } else {
                            continue;
                        }
                    case 7:
                        if (a(t2, i13)) {
                            b3 = zztv.b(i16, true);
                        } else {
                            continue;
                        }
                    case 8:
                        if (a(t2, i13)) {
                            Object f2 = kb1.f(t2, j3);
                            if (f2 instanceof zzte) {
                                b3 = zztv.c(i16, (zzte) f2);
                            } else {
                                b3 = zztv.b(i16, (String) f2);
                            }
                        } else {
                            continue;
                        }
                    case 9:
                        if (a(t2, i13)) {
                            b3 = pa1.a(i16, kb1.f(t2, j3), a(i13));
                        } else {
                            continue;
                        }
                    case 10:
                        if (a(t2, i13)) {
                            b3 = zztv.c(i16, (zzte) kb1.f(t2, j3));
                        } else {
                            continue;
                        }
                    case 11:
                        if (a(t2, i13)) {
                            b3 = zztv.g(i16, kb1.a((Object) t2, j3));
                        } else {
                            continue;
                        }
                    case 12:
                        if (a(t2, i13)) {
                            b3 = zztv.k(i16, kb1.a((Object) t2, j3));
                        } else {
                            continue;
                        }
                    case 13:
                        if (a(t2, i13)) {
                            b3 = zztv.j(i16, 0);
                        } else {
                            continue;
                        }
                    case 14:
                        if (a(t2, i13)) {
                            b3 = zztv.h(i16, 0);
                        } else {
                            continue;
                        }
                    case 15:
                        if (a(t2, i13)) {
                            b3 = zztv.h(i16, kb1.a((Object) t2, j3));
                        } else {
                            continue;
                        }
                    case 16:
                        if (a(t2, i13)) {
                            b3 = zztv.f(i16, kb1.b(t2, j3));
                        } else {
                            continue;
                        }
                    case 17:
                        if (a(t2, i13)) {
                            b3 = zztv.c(i16, (w91) kb1.f(t2, j3), a(i13));
                        } else {
                            continue;
                        }
                    case 18:
                        b3 = pa1.i(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 19:
                        b3 = pa1.h(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 20:
                        b3 = pa1.a(i16, (List<Long>) a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 21:
                        b3 = pa1.b(i16, (List<Long>) a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 22:
                        b3 = pa1.e(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 23:
                        b3 = pa1.i(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 24:
                        b3 = pa1.h(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 25:
                        b3 = pa1.j(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 26:
                        b3 = pa1.a(i16, (List<?>) a((Object) t2, j3));
                        i14 += b3;
                        break;
                    case 27:
                        b3 = pa1.a(i16, (List<?>) a((Object) t2, j3), a(i13));
                        i14 += b3;
                        break;
                    case 28:
                        b3 = pa1.b(i16, a((Object) t2, j3));
                        i14 += b3;
                        break;
                    case 29:
                        b3 = pa1.f(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 30:
                        b3 = pa1.d(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 31:
                        b3 = pa1.h(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 32:
                        b3 = pa1.i(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 33:
                        b3 = pa1.g(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 34:
                        b3 = pa1.c(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 35:
                        i10 = pa1.g((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.i) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = zztv.e(i16);
                            i9 = zztv.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 36:
                        i10 = pa1.f((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.i) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = zztv.e(i16);
                            i9 = zztv.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 37:
                        i10 = pa1.i((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.i) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = zztv.e(i16);
                            i9 = zztv.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 38:
                        i10 = pa1.j((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.i) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = zztv.e(i16);
                            i9 = zztv.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 39:
                        i10 = pa1.c((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.i) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = zztv.e(i16);
                            i9 = zztv.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 40:
                        i10 = pa1.g((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.i) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = zztv.e(i16);
                            i9 = zztv.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 41:
                        i10 = pa1.f((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.i) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = zztv.e(i16);
                            i9 = zztv.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 42:
                        i10 = pa1.h((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.i) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = zztv.e(i16);
                            i9 = zztv.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 43:
                        i10 = pa1.d((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.i) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = zztv.e(i16);
                            i9 = zztv.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 44:
                        i10 = pa1.b((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.i) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = zztv.e(i16);
                            i9 = zztv.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 45:
                        i10 = pa1.f((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.i) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = zztv.e(i16);
                            i9 = zztv.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 46:
                        i10 = pa1.g((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.i) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = zztv.e(i16);
                            i9 = zztv.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 47:
                        i10 = pa1.e((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.i) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = zztv.e(i16);
                            i9 = zztv.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 48:
                        i10 = pa1.a((List<Long>) (List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.i) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = zztv.e(i16);
                            i9 = zztv.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 49:
                        b3 = pa1.b(i16, (List<w91>) a((Object) t2, j3), a(i13));
                        i14 += b3;
                        break;
                    case 50:
                        b3 = this.q.a(i16, kb1.f(t2, j3), b(i13));
                        i14 += b3;
                        break;
                    case 51:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.b(i16, 0.0d);
                        } else {
                            continue;
                        }
                    case 52:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.b(i16, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        } else {
                            continue;
                        }
                    case 53:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.d(i16, e(t2, j3));
                        } else {
                            continue;
                        }
                    case 54:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.e(i16, e(t2, j3));
                        } else {
                            continue;
                        }
                    case 55:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.f(i16, d(t2, j3));
                        } else {
                            continue;
                        }
                    case 56:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.g(i16, 0);
                        } else {
                            continue;
                        }
                    case 57:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.i(i16, 0);
                        } else {
                            continue;
                        }
                    case 58:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.b(i16, true);
                        } else {
                            continue;
                        }
                    case 59:
                        if (a(t2, i16, i13)) {
                            Object f3 = kb1.f(t2, j3);
                            if (f3 instanceof zzte) {
                                b3 = zztv.c(i16, (zzte) f3);
                            } else {
                                b3 = zztv.b(i16, (String) f3);
                            }
                        } else {
                            continue;
                        }
                    case 60:
                        if (a(t2, i16, i13)) {
                            b3 = pa1.a(i16, kb1.f(t2, j3), a(i13));
                        } else {
                            continue;
                        }
                    case 61:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.c(i16, (zzte) kb1.f(t2, j3));
                        } else {
                            continue;
                        }
                    case 62:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.g(i16, d(t2, j3));
                        } else {
                            continue;
                        }
                    case 63:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.k(i16, d(t2, j3));
                        } else {
                            continue;
                        }
                    case 64:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.j(i16, 0);
                        } else {
                            continue;
                        }
                    case 65:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.h(i16, 0);
                        } else {
                            continue;
                        }
                    case 66:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.h(i16, d(t2, j3));
                        } else {
                            continue;
                        }
                    case 67:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.f(i16, e(t2, j3));
                        } else {
                            continue;
                        }
                    case 68:
                        if (a(t2, i16, i13)) {
                            b3 = zztv.c(i16, (w91) kb1.f(t2, j3), a(i13));
                        } else {
                            continue;
                        }
                }
                b3 = i11 + i9 + i10;
                i14 += b3;
                i13 += 3;
                i12 = 267386880;
            }
            return i14 + a(this.o, t2);
        }
        Unsafe unsafe2 = s;
        int i18 = 0;
        int i19 = 0;
        int i20 = -1;
        int i21 = 0;
        while (i18 < this.a.length) {
            int d3 = d(i18);
            int[] iArr = this.a;
            int i22 = iArr[i18];
            int i23 = (d3 & 267386880) >>> 20;
            if (i23 <= 17) {
                i3 = iArr[i18 + 2];
                int i24 = i3 & 1048575;
                i2 = 1 << (i3 >>> 20);
                if (i24 != i20) {
                    i21 = unsafe2.getInt(t2, (long) i24);
                } else {
                    i24 = i20;
                }
                i20 = i24;
            } else {
                i3 = (!this.i || i23 < zzui.zzbww.id() || i23 > zzui.zzbxj.id()) ? 0 : this.a[i18 + 2] & 1048575;
                i2 = 0;
            }
            long j4 = (long) (d3 & 1048575);
            switch (i23) {
                case 0:
                    j2 = 0;
                    if ((i21 & i2) != 0) {
                        i19 += zztv.b(i22, 0.0d);
                        break;
                    }
                case 1:
                    j2 = 0;
                    if ((i21 & i2) != 0) {
                        i19 += zztv.b(i22, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        break;
                    }
                case 2:
                    j2 = 0;
                    if ((i21 & i2) != 0) {
                        i4 = zztv.d(i22, unsafe2.getLong(t2, j4));
                    }
                    break;
                case 3:
                    j2 = 0;
                    if ((i21 & i2) != 0) {
                        i4 = zztv.e(i22, unsafe2.getLong(t2, j4));
                    }
                    break;
                case 4:
                    j2 = 0;
                    if ((i21 & i2) != 0) {
                        i4 = zztv.f(i22, unsafe2.getInt(t2, j4));
                    }
                    break;
                case 5:
                    j2 = 0;
                    if ((i21 & i2) != 0) {
                        i4 = zztv.g(i22, 0);
                    }
                    break;
                case 6:
                    if ((i21 & i2) != 0) {
                        i19 += zztv.i(i22, 0);
                        break;
                    }
                case 7:
                    if ((i21 & i2) != 0) {
                        b2 = zztv.b(i22, true);
                    }
                    break;
                case 8:
                    if ((i21 & i2) != 0) {
                        Object object = unsafe2.getObject(t2, j4);
                        if (object instanceof zzte) {
                            b2 = zztv.c(i22, (zzte) object);
                        } else {
                            b2 = zztv.b(i22, (String) object);
                        }
                    }
                    break;
                case 9:
                    if ((i21 & i2) != 0) {
                        b2 = pa1.a(i22, unsafe2.getObject(t2, j4), a(i18));
                    }
                    break;
                case 10:
                    if ((i21 & i2) != 0) {
                        b2 = zztv.c(i22, (zzte) unsafe2.getObject(t2, j4));
                    }
                    break;
                case 11:
                    if ((i21 & i2) != 0) {
                        b2 = zztv.g(i22, unsafe2.getInt(t2, j4));
                    }
                    break;
                case 12:
                    if ((i21 & i2) != 0) {
                        b2 = zztv.k(i22, unsafe2.getInt(t2, j4));
                    }
                    break;
                case 13:
                    if ((i21 & i2) != 0) {
                        i5 = zztv.j(i22, 0);
                    }
                    break;
                case 14:
                    if ((i21 & i2) != 0) {
                        b2 = zztv.h(i22, 0);
                    }
                    break;
                case 15:
                    if ((i21 & i2) != 0) {
                        b2 = zztv.h(i22, unsafe2.getInt(t2, j4));
                    }
                    break;
                case 16:
                    if ((i21 & i2) != 0) {
                        b2 = zztv.f(i22, unsafe2.getLong(t2, j4));
                    }
                    break;
                case 17:
                    if ((i21 & i2) != 0) {
                        b2 = zztv.c(i22, (w91) unsafe2.getObject(t2, j4), a(i18));
                    }
                    break;
                case 18:
                    b2 = pa1.i(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 19:
                    b2 = pa1.h(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 20:
                    b2 = pa1.a(i22, (List<Long>) (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 21:
                    b2 = pa1.b(i22, (List<Long>) (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 22:
                    b2 = pa1.e(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 23:
                    b2 = pa1.i(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 24:
                    b2 = pa1.h(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 25:
                    b2 = pa1.j(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 26:
                    b2 = pa1.a(i22, (List<?>) (List) unsafe2.getObject(t2, j4));
                    i19 += b2;
                    break;
                case 27:
                    b2 = pa1.a(i22, (List<?>) (List) unsafe2.getObject(t2, j4), a(i18));
                    i19 += b2;
                    break;
                case 28:
                    b2 = pa1.b(i22, (List) unsafe2.getObject(t2, j4));
                    i19 += b2;
                    break;
                case 29:
                    b2 = pa1.f(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 30:
                    b2 = pa1.d(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 31:
                    b2 = pa1.h(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 32:
                    b2 = pa1.i(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 33:
                    b2 = pa1.g(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 34:
                    b2 = pa1.c(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 35:
                    i8 = pa1.g((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.i) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = zztv.e(i22);
                        i6 = zztv.g(i8);
                        break;
                    }
                    break;
                case 36:
                    i8 = pa1.f((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.i) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = zztv.e(i22);
                        i6 = zztv.g(i8);
                        break;
                    }
                    break;
                case 37:
                    i8 = pa1.i((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.i) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = zztv.e(i22);
                        i6 = zztv.g(i8);
                        break;
                    }
                    break;
                case 38:
                    i8 = pa1.j((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.i) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = zztv.e(i22);
                        i6 = zztv.g(i8);
                        break;
                    }
                    break;
                case 39:
                    i8 = pa1.c((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.i) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = zztv.e(i22);
                        i6 = zztv.g(i8);
                        break;
                    }
                    break;
                case 40:
                    i8 = pa1.g((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.i) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = zztv.e(i22);
                        i6 = zztv.g(i8);
                        break;
                    }
                    break;
                case 41:
                    i8 = pa1.f((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.i) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = zztv.e(i22);
                        i6 = zztv.g(i8);
                        break;
                    }
                    break;
                case 42:
                    i8 = pa1.h((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.i) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = zztv.e(i22);
                        i6 = zztv.g(i8);
                        break;
                    }
                    break;
                case 43:
                    i8 = pa1.d((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.i) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = zztv.e(i22);
                        i6 = zztv.g(i8);
                        break;
                    }
                    break;
                case 44:
                    i8 = pa1.b((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.i) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = zztv.e(i22);
                        i6 = zztv.g(i8);
                        break;
                    }
                    break;
                case 45:
                    i8 = pa1.f((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.i) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = zztv.e(i22);
                        i6 = zztv.g(i8);
                        break;
                    }
                    break;
                case 46:
                    i8 = pa1.g((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.i) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = zztv.e(i22);
                        i6 = zztv.g(i8);
                        break;
                    }
                    break;
                case 47:
                    i8 = pa1.e((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.i) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = zztv.e(i22);
                        i6 = zztv.g(i8);
                        break;
                    }
                    break;
                case 48:
                    i8 = pa1.a((List<Long>) (List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.i) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = zztv.e(i22);
                        i6 = zztv.g(i8);
                        break;
                    }
                    break;
                case 49:
                    b2 = pa1.b(i22, (List<w91>) (List) unsafe2.getObject(t2, j4), a(i18));
                    i19 += b2;
                    break;
                case 50:
                    b2 = this.q.a(i22, unsafe2.getObject(t2, j4), b(i18));
                    i19 += b2;
                    break;
                case 51:
                    if (a(t2, i22, i18)) {
                        b2 = zztv.b(i22, 0.0d);
                    }
                    break;
                case 52:
                    if (a(t2, i22, i18)) {
                        i5 = zztv.b(i22, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    }
                    break;
                case 53:
                    if (a(t2, i22, i18)) {
                        b2 = zztv.d(i22, e(t2, j4));
                    }
                    break;
                case 54:
                    if (a(t2, i22, i18)) {
                        b2 = zztv.e(i22, e(t2, j4));
                    }
                    break;
                case 55:
                    if (a(t2, i22, i18)) {
                        b2 = zztv.f(i22, d(t2, j4));
                    }
                    break;
                case 56:
                    if (a(t2, i22, i18)) {
                        b2 = zztv.g(i22, 0);
                    }
                    break;
                case 57:
                    if (a(t2, i22, i18)) {
                        i5 = zztv.i(i22, 0);
                    }
                    break;
                case 58:
                    if (a(t2, i22, i18)) {
                        b2 = zztv.b(i22, true);
                    }
                    break;
                case 59:
                    if (a(t2, i22, i18)) {
                        Object object2 = unsafe2.getObject(t2, j4);
                        if (object2 instanceof zzte) {
                            b2 = zztv.c(i22, (zzte) object2);
                        } else {
                            b2 = zztv.b(i22, (String) object2);
                        }
                    }
                    break;
                case 60:
                    if (a(t2, i22, i18)) {
                        b2 = pa1.a(i22, unsafe2.getObject(t2, j4), a(i18));
                    }
                    break;
                case 61:
                    if (a(t2, i22, i18)) {
                        b2 = zztv.c(i22, (zzte) unsafe2.getObject(t2, j4));
                    }
                    break;
                case 62:
                    if (a(t2, i22, i18)) {
                        b2 = zztv.g(i22, d(t2, j4));
                    }
                    break;
                case 63:
                    if (a(t2, i22, i18)) {
                        b2 = zztv.k(i22, d(t2, j4));
                    }
                    break;
                case 64:
                    if (a(t2, i22, i18)) {
                        i5 = zztv.j(i22, 0);
                    }
                    break;
                case 65:
                    if (a(t2, i22, i18)) {
                        b2 = zztv.h(i22, 0);
                    }
                    break;
                case 66:
                    if (a(t2, i22, i18)) {
                        b2 = zztv.h(i22, d(t2, j4));
                    }
                    break;
                case 67:
                    if (a(t2, i22, i18)) {
                        b2 = zztv.f(i22, e(t2, j4));
                    }
                    break;
                case 68:
                    if (a(t2, i22, i18)) {
                        b2 = zztv.c(i22, (w91) unsafe2.getObject(t2, j4), a(i18));
                    }
                    break;
            }
        }
        int a2 = i19 + a(this.o, t2);
        return this.f ? a2 + this.p.a((Object) t2).g() : a2;
    }

    @DexIgnore
    public static Field a(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    @DexIgnore
    public final T a() {
        return this.m.a(this.e);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (com.fossil.blesdk.obfuscated.pa1.a(com.fossil.blesdk.obfuscated.kb1.f(r10, r6), com.fossil.blesdk.obfuscated.kb1.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (com.fossil.blesdk.obfuscated.kb1.b(r10, r6) == com.fossil.blesdk.obfuscated.kb1.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r10, r6) == com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (com.fossil.blesdk.obfuscated.kb1.b(r10, r6) == com.fossil.blesdk.obfuscated.kb1.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r10, r6) == com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r10, r6) == com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r10, r6) == com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (com.fossil.blesdk.obfuscated.pa1.a(com.fossil.blesdk.obfuscated.kb1.f(r10, r6), com.fossil.blesdk.obfuscated.kb1.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (com.fossil.blesdk.obfuscated.pa1.a(com.fossil.blesdk.obfuscated.kb1.f(r10, r6), com.fossil.blesdk.obfuscated.kb1.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (com.fossil.blesdk.obfuscated.pa1.a(com.fossil.blesdk.obfuscated.kb1.f(r10, r6), com.fossil.blesdk.obfuscated.kb1.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (com.fossil.blesdk.obfuscated.kb1.c(r10, r6) == com.fossil.blesdk.obfuscated.kb1.c(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r10, r6) == com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (com.fossil.blesdk.obfuscated.kb1.b(r10, r6) == com.fossil.blesdk.obfuscated.kb1.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r10, r6) == com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (com.fossil.blesdk.obfuscated.kb1.b(r10, r6) == com.fossil.blesdk.obfuscated.kb1.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (com.fossil.blesdk.obfuscated.kb1.b(r10, r6) == com.fossil.blesdk.obfuscated.kb1.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.fossil.blesdk.obfuscated.kb1.d(r10, r6)) == java.lang.Float.floatToIntBits(com.fossil.blesdk.obfuscated.kb1.d(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.fossil.blesdk.obfuscated.kb1.e(r10, r6)) == java.lang.Double.doubleToLongBits(com.fossil.blesdk.obfuscated.kb1.e(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01c1, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.fossil.blesdk.obfuscated.pa1.a(com.fossil.blesdk.obfuscated.kb1.f(r10, r6), com.fossil.blesdk.obfuscated.kb1.f(r11, r6)) != false) goto L_0x01c2;
     */
    @DexIgnore
    public final boolean a(T t, T t2) {
        int length = this.a.length;
        int i2 = 0;
        while (true) {
            boolean z = true;
            if (i2 < length) {
                int d2 = d(i2);
                long j2 = (long) (d2 & 1048575);
                switch ((d2 & 267386880) >>> 20) {
                    case 0:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 1:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 2:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 3:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 4:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 5:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 6:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 7:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 8:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 9:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 10:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 11:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 12:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 13:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 14:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 15:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 16:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 17:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        z = pa1.a(kb1.f(t, j2), kb1.f(t2, j2));
                        break;
                    case 50:
                        z = pa1.a(kb1.f(t, j2), kb1.f(t2, j2));
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                    case 60:
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                    case 68:
                        long e2 = (long) (e(i2) & 1048575);
                        if (kb1.a((Object) t, e2) == kb1.a((Object) t2, e2)) {
                            break;
                        }
                }
                if (!z) {
                    return false;
                }
                i2 += 3;
            } else if (!this.o.c(t).equals(this.o.c(t2))) {
                return false;
            } else {
                if (this.f) {
                    return this.p.a((Object) t).equals(this.p.a((Object) t2));
                }
                return true;
            }
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01c3, code lost:
        r2 = (r2 * 53) + r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0227, code lost:
        r2 = r2 + r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0228, code lost:
        r1 = r1 + 3;
     */
    @DexIgnore
    public final int a(T t) {
        int i2;
        int i3;
        int length = this.a.length;
        int i4 = 0;
        int i5 = 0;
        while (i4 < length) {
            int d2 = d(i4);
            int i6 = this.a[i4];
            long j2 = (long) (1048575 & d2);
            int i7 = 37;
            switch ((d2 & 267386880) >>> 20) {
                case 0:
                    i3 = i5 * 53;
                    i2 = v81.a(Double.doubleToLongBits(kb1.e(t, j2)));
                case 1:
                    i3 = i5 * 53;
                    i2 = Float.floatToIntBits(kb1.d(t, j2));
                case 2:
                    i3 = i5 * 53;
                    i2 = v81.a(kb1.b(t, j2));
                case 3:
                    i3 = i5 * 53;
                    i2 = v81.a(kb1.b(t, j2));
                case 4:
                    i3 = i5 * 53;
                    i2 = kb1.a((Object) t, j2);
                case 5:
                    i3 = i5 * 53;
                    i2 = v81.a(kb1.b(t, j2));
                case 6:
                    i3 = i5 * 53;
                    i2 = kb1.a((Object) t, j2);
                case 7:
                    i3 = i5 * 53;
                    i2 = v81.a(kb1.c(t, j2));
                case 8:
                    i3 = i5 * 53;
                    i2 = ((String) kb1.f(t, j2)).hashCode();
                case 9:
                    Object f2 = kb1.f(t, j2);
                    if (f2 != null) {
                        i7 = f2.hashCode();
                        break;
                    }
                    break;
                case 10:
                    i3 = i5 * 53;
                    i2 = kb1.f(t, j2).hashCode();
                case 11:
                    i3 = i5 * 53;
                    i2 = kb1.a((Object) t, j2);
                case 12:
                    i3 = i5 * 53;
                    i2 = kb1.a((Object) t, j2);
                case 13:
                    i3 = i5 * 53;
                    i2 = kb1.a((Object) t, j2);
                case 14:
                    i3 = i5 * 53;
                    i2 = v81.a(kb1.b(t, j2));
                case 15:
                    i3 = i5 * 53;
                    i2 = kb1.a((Object) t, j2);
                case 16:
                    i3 = i5 * 53;
                    i2 = v81.a(kb1.b(t, j2));
                case 17:
                    Object f3 = kb1.f(t, j2);
                    if (f3 != null) {
                        i7 = f3.hashCode();
                        break;
                    }
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i3 = i5 * 53;
                    i2 = kb1.f(t, j2).hashCode();
                case 50:
                    i3 = i5 * 53;
                    i2 = kb1.f(t, j2).hashCode();
                case 51:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = v81.a(Double.doubleToLongBits(b(t, j2)));
                    }
                case 52:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = Float.floatToIntBits(c(t, j2));
                    }
                case 53:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = v81.a(e(t, j2));
                    }
                case 54:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = v81.a(e(t, j2));
                    }
                case 55:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = d(t, j2);
                    }
                case 56:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = v81.a(e(t, j2));
                    }
                case 57:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = d(t, j2);
                    }
                case 58:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = v81.a(f(t, j2));
                    }
                case 59:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = ((String) kb1.f(t, j2)).hashCode();
                    }
                case 60:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = kb1.f(t, j2).hashCode();
                    }
                case 61:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = kb1.f(t, j2).hashCode();
                    }
                case 62:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = d(t, j2);
                    }
                case 63:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = d(t, j2);
                    }
                case 64:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = d(t, j2);
                    }
                case 65:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = v81.a(e(t, j2));
                    }
                case 66:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = d(t, j2);
                    }
                case 67:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = v81.a(e(t, j2));
                    }
                case 68:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = kb1.f(t, j2).hashCode();
                    }
            }
        }
        int hashCode = (i5 * 53) + this.o.c(t).hashCode();
        return this.f ? (hashCode * 53) + this.p.a((Object) t).hashCode() : hashCode;
    }

    @DexIgnore
    public final void a(T t, T t2, int i2) {
        long d2 = (long) (d(i2) & 1048575);
        if (a(t2, i2)) {
            Object f2 = kb1.f(t, d2);
            Object f3 = kb1.f(t2, d2);
            if (f2 != null && f3 != null) {
                kb1.a((Object) t, d2, v81.a(f2, f3));
                b(t, i2);
            } else if (f3 != null) {
                kb1.a((Object) t, d2, f3);
                b(t, i2);
            }
        }
    }

    @DexIgnore
    public static <UT, UB> int a(eb1<UT, UB> eb1, T t) {
        return eb1.b(eb1.c(t));
    }

    @DexIgnore
    public static <E> List<E> a(Object obj, long j2) {
        return (List) kb1.f(obj, j2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x04ba A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x04bb  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x04e7  */
    /* JADX WARNING: Removed duplicated region for block: B:321:0x0969  */
    /* JADX WARNING: Removed duplicated region for block: B:323:0x096f  */
    public final void a(T t, sb1 sb1) throws IOException {
        Map.Entry entry;
        int length;
        int i2;
        Map.Entry entry2;
        int length2;
        if (sb1.a() == t81.e.l) {
            a(this.o, t, sb1);
            if (this.f) {
                m81<?> a2 = this.p.a((Object) t);
                if (!a2.b()) {
                    entry2 = a2.a().next();
                    length2 = this.a.length - 3;
                    while (length2 >= 0) {
                        int d2 = d(length2);
                        int[] iArr = this.a;
                        int i3 = iArr[length2];
                        if (entry2 == null) {
                            switch ((d2 & 267386880) >>> 20) {
                                case 0:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.zza(i3, kb1.e(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 1:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.zza(i3, kb1.d(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 2:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.b(i3, kb1.b(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 3:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.zza(i3, kb1.b(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 4:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.zzd(i3, kb1.a((Object) t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 5:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.zzc(i3, kb1.b(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 6:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.c(i3, kb1.a((Object) t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 7:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.a(i3, kb1.c(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 8:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        a(i3, kb1.f(t, (long) (d2 & 1048575)), sb1);
                                        break;
                                    }
                                case 9:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.a(i3, kb1.f(t, (long) (d2 & 1048575)), a(length2));
                                        break;
                                    }
                                case 10:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.a(i3, (zzte) kb1.f(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 11:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.zze(i3, kb1.a((Object) t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 12:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.b(i3, kb1.a((Object) t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 13:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.a(i3, kb1.a((Object) t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 14:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.a(i3, kb1.b(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 15:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.zzf(i3, kb1.a((Object) t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 16:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.zzb(i3, kb1.b(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 17:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        sb1.b(i3, kb1.f(t, (long) (d2 & 1048575)), a(length2));
                                        break;
                                    }
                                case 18:
                                    pa1.a(iArr[length2], (List<Double>) (List) kb1.f(t, (long) (d2 & 1048575)), sb1, false);
                                    break;
                                case 19:
                                    pa1.b(iArr[length2], (List<Float>) (List) kb1.f(t, (long) (d2 & 1048575)), sb1, false);
                                    break;
                                case 20:
                                    pa1.c(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, false);
                                    break;
                                case 21:
                                    pa1.d(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, false);
                                    break;
                                case 22:
                                    pa1.h(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, false);
                                    break;
                                case 23:
                                    pa1.f(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, false);
                                    break;
                                case 24:
                                    pa1.k(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, false);
                                    break;
                                case 25:
                                    pa1.n(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, false);
                                    break;
                                case 26:
                                    pa1.a(iArr[length2], (List<String>) (List) kb1.f(t, (long) (d2 & 1048575)), sb1);
                                    break;
                                case 27:
                                    pa1.a(iArr[length2], (List<?>) (List) kb1.f(t, (long) (d2 & 1048575)), sb1, a(length2));
                                    break;
                                case 28:
                                    pa1.b(iArr[length2], (List<zzte>) (List) kb1.f(t, (long) (d2 & 1048575)), sb1);
                                    break;
                                case 29:
                                    pa1.i(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, false);
                                    break;
                                case 30:
                                    pa1.m(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, false);
                                    break;
                                case 31:
                                    pa1.l(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, false);
                                    break;
                                case 32:
                                    pa1.g(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, false);
                                    break;
                                case 33:
                                    pa1.j(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, false);
                                    break;
                                case 34:
                                    pa1.e(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, false);
                                    break;
                                case 35:
                                    pa1.a(iArr[length2], (List<Double>) (List) kb1.f(t, (long) (d2 & 1048575)), sb1, true);
                                    break;
                                case 36:
                                    pa1.b(iArr[length2], (List<Float>) (List) kb1.f(t, (long) (d2 & 1048575)), sb1, true);
                                    break;
                                case 37:
                                    pa1.c(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, true);
                                    break;
                                case 38:
                                    pa1.d(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, true);
                                    break;
                                case 39:
                                    pa1.h(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, true);
                                    break;
                                case 40:
                                    pa1.f(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, true);
                                    break;
                                case 41:
                                    pa1.k(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, true);
                                    break;
                                case 42:
                                    pa1.n(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, true);
                                    break;
                                case 43:
                                    pa1.i(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, true);
                                    break;
                                case 44:
                                    pa1.m(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, true);
                                    break;
                                case 45:
                                    pa1.l(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, true);
                                    break;
                                case 46:
                                    pa1.g(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, true);
                                    break;
                                case 47:
                                    pa1.j(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, true);
                                    break;
                                case 48:
                                    pa1.e(iArr[length2], (List) kb1.f(t, (long) (d2 & 1048575)), sb1, true);
                                    break;
                                case 49:
                                    pa1.b(iArr[length2], (List<?>) (List) kb1.f(t, (long) (d2 & 1048575)), sb1, a(length2));
                                    break;
                                case 50:
                                    a(sb1, i3, kb1.f(t, (long) (d2 & 1048575)), length2);
                                    break;
                                case 51:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.zza(i3, b(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 52:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.zza(i3, c(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 53:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.b(i3, e(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 54:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.zza(i3, e(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 55:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.zzd(i3, d(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 56:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.zzc(i3, e(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 57:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.c(i3, d(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 58:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.a(i3, f(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 59:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        a(i3, kb1.f(t, (long) (d2 & 1048575)), sb1);
                                        break;
                                    }
                                case 60:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.a(i3, kb1.f(t, (long) (d2 & 1048575)), a(length2));
                                        break;
                                    }
                                case 61:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.a(i3, (zzte) kb1.f(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 62:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.zze(i3, d(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 63:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.b(i3, d(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 64:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.a(i3, d(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 65:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.a(i3, e(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 66:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.zzf(i3, d(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 67:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.zzb(i3, e(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 68:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        sb1.b(i3, kb1.f(t, (long) (d2 & 1048575)), a(length2));
                                        break;
                                    }
                            }
                            length2 -= 3;
                        } else {
                            this.p.a((Map.Entry<?, ?>) entry2);
                            throw null;
                        }
                    }
                    if (entry2 == null) {
                        this.p.a(sb1, entry2);
                        throw null;
                    }
                    return;
                }
            }
            entry2 = null;
            length2 = this.a.length - 3;
            while (length2 >= 0) {
            }
            if (entry2 == null) {
            }
        } else if (this.h) {
            if (this.f) {
                m81<?> a3 = this.p.a((Object) t);
                if (!a3.b()) {
                    entry = a3.e().next();
                    length = this.a.length;
                    i2 = 0;
                    while (i2 < length) {
                        int d3 = d(i2);
                        int[] iArr2 = this.a;
                        int i4 = iArr2[i2];
                        if (entry == null) {
                            switch ((d3 & 267386880) >>> 20) {
                                case 0:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.zza(i4, kb1.e(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 1:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.zza(i4, kb1.d(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 2:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.b(i4, kb1.b(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 3:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.zza(i4, kb1.b(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 4:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.zzd(i4, kb1.a((Object) t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 5:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.zzc(i4, kb1.b(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 6:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.c(i4, kb1.a((Object) t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 7:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.a(i4, kb1.c(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 8:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        a(i4, kb1.f(t, (long) (d3 & 1048575)), sb1);
                                        break;
                                    }
                                case 9:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.a(i4, kb1.f(t, (long) (d3 & 1048575)), a(i2));
                                        break;
                                    }
                                case 10:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.a(i4, (zzte) kb1.f(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 11:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.zze(i4, kb1.a((Object) t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 12:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.b(i4, kb1.a((Object) t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 13:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.a(i4, kb1.a((Object) t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 14:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.a(i4, kb1.b(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 15:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.zzf(i4, kb1.a((Object) t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 16:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.zzb(i4, kb1.b(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 17:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        sb1.b(i4, kb1.f(t, (long) (d3 & 1048575)), a(i2));
                                        break;
                                    }
                                case 18:
                                    pa1.a(iArr2[i2], (List<Double>) (List) kb1.f(t, (long) (d3 & 1048575)), sb1, false);
                                    break;
                                case 19:
                                    pa1.b(iArr2[i2], (List<Float>) (List) kb1.f(t, (long) (d3 & 1048575)), sb1, false);
                                    break;
                                case 20:
                                    pa1.c(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, false);
                                    break;
                                case 21:
                                    pa1.d(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, false);
                                    break;
                                case 22:
                                    pa1.h(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, false);
                                    break;
                                case 23:
                                    pa1.f(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, false);
                                    break;
                                case 24:
                                    pa1.k(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, false);
                                    break;
                                case 25:
                                    pa1.n(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, false);
                                    break;
                                case 26:
                                    pa1.a(iArr2[i2], (List<String>) (List) kb1.f(t, (long) (d3 & 1048575)), sb1);
                                    break;
                                case 27:
                                    pa1.a(iArr2[i2], (List<?>) (List) kb1.f(t, (long) (d3 & 1048575)), sb1, a(i2));
                                    break;
                                case 28:
                                    pa1.b(iArr2[i2], (List<zzte>) (List) kb1.f(t, (long) (d3 & 1048575)), sb1);
                                    break;
                                case 29:
                                    pa1.i(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, false);
                                    break;
                                case 30:
                                    pa1.m(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, false);
                                    break;
                                case 31:
                                    pa1.l(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, false);
                                    break;
                                case 32:
                                    pa1.g(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, false);
                                    break;
                                case 33:
                                    pa1.j(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, false);
                                    break;
                                case 34:
                                    pa1.e(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, false);
                                    break;
                                case 35:
                                    pa1.a(iArr2[i2], (List<Double>) (List) kb1.f(t, (long) (d3 & 1048575)), sb1, true);
                                    break;
                                case 36:
                                    pa1.b(iArr2[i2], (List<Float>) (List) kb1.f(t, (long) (d3 & 1048575)), sb1, true);
                                    break;
                                case 37:
                                    pa1.c(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, true);
                                    break;
                                case 38:
                                    pa1.d(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, true);
                                    break;
                                case 39:
                                    pa1.h(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, true);
                                    break;
                                case 40:
                                    pa1.f(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, true);
                                    break;
                                case 41:
                                    pa1.k(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, true);
                                    break;
                                case 42:
                                    pa1.n(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, true);
                                    break;
                                case 43:
                                    pa1.i(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, true);
                                    break;
                                case 44:
                                    pa1.m(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, true);
                                    break;
                                case 45:
                                    pa1.l(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, true);
                                    break;
                                case 46:
                                    pa1.g(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, true);
                                    break;
                                case 47:
                                    pa1.j(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, true);
                                    break;
                                case 48:
                                    pa1.e(iArr2[i2], (List) kb1.f(t, (long) (d3 & 1048575)), sb1, true);
                                    break;
                                case 49:
                                    pa1.b(iArr2[i2], (List<?>) (List) kb1.f(t, (long) (d3 & 1048575)), sb1, a(i2));
                                    break;
                                case 50:
                                    a(sb1, i4, kb1.f(t, (long) (d3 & 1048575)), i2);
                                    break;
                                case 51:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.zza(i4, b(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 52:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.zza(i4, c(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 53:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.b(i4, e(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 54:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.zza(i4, e(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 55:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.zzd(i4, d(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 56:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.zzc(i4, e(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 57:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.c(i4, d(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 58:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.a(i4, f(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 59:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        a(i4, kb1.f(t, (long) (d3 & 1048575)), sb1);
                                        break;
                                    }
                                case 60:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.a(i4, kb1.f(t, (long) (d3 & 1048575)), a(i2));
                                        break;
                                    }
                                case 61:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.a(i4, (zzte) kb1.f(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 62:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.zze(i4, d(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 63:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.b(i4, d(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 64:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.a(i4, d(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 65:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.a(i4, e(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 66:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.zzf(i4, d(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 67:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.zzb(i4, e(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 68:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        sb1.b(i4, kb1.f(t, (long) (d3 & 1048575)), a(i2));
                                        break;
                                    }
                            }
                            i2 += 3;
                        } else {
                            this.p.a((Map.Entry<?, ?>) entry);
                            throw null;
                        }
                    }
                    if (entry != null) {
                        a(this.o, t, sb1);
                        return;
                    } else {
                        this.p.a(sb1, entry);
                        throw null;
                    }
                }
            }
            entry = null;
            length = this.a.length;
            i2 = 0;
            while (i2 < length) {
            }
            if (entry != null) {
            }
        } else {
            b(t, sb1);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:167:0x0469  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x046f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    public final void b(T t, sb1 sb1) throws IOException {
        Map.Entry entry;
        int length;
        int i2;
        int i3;
        int i4;
        T t2 = t;
        sb1 sb12 = sb1;
        if (this.f) {
            m81<?> a2 = this.p.a((Object) t2);
            if (!a2.b()) {
                entry = a2.e().next();
                length = this.a.length;
                Unsafe unsafe = s;
                i2 = 0;
                int i5 = -1;
                int i6 = 0;
                while (i2 < length) {
                    int d2 = d(i2);
                    int[] iArr = this.a;
                    int i7 = iArr[i2];
                    int i8 = (267386880 & d2) >>> 20;
                    if (this.h || i8 > 17) {
                        i3 = i5;
                        i4 = 0;
                    } else {
                        int i9 = iArr[i2 + 2];
                        i3 = i9 & 1048575;
                        if (i3 != i5) {
                            i6 = unsafe.getInt(t2, (long) i3);
                        } else {
                            i3 = i5;
                        }
                        i4 = 1 << (i9 >>> 20);
                    }
                    if (entry == null) {
                        long j2 = (long) (d2 & 1048575);
                        switch (i8) {
                            case 0:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.zza(i7, kb1.e(t2, j2));
                                    break;
                                }
                            case 1:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.zza(i7, kb1.d(t2, j2));
                                    break;
                                }
                            case 2:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.b(i7, unsafe.getLong(t2, j2));
                                    break;
                                }
                            case 3:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.zza(i7, unsafe.getLong(t2, j2));
                                    break;
                                }
                            case 4:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.zzd(i7, unsafe.getInt(t2, j2));
                                    break;
                                }
                            case 5:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.zzc(i7, unsafe.getLong(t2, j2));
                                    break;
                                }
                            case 6:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.c(i7, unsafe.getInt(t2, j2));
                                    break;
                                }
                            case 7:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.a(i7, kb1.c(t2, j2));
                                    break;
                                }
                            case 8:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    a(i7, unsafe.getObject(t2, j2), sb12);
                                    break;
                                }
                            case 9:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.a(i7, unsafe.getObject(t2, j2), a(i2));
                                    break;
                                }
                            case 10:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.a(i7, (zzte) unsafe.getObject(t2, j2));
                                    break;
                                }
                            case 11:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.zze(i7, unsafe.getInt(t2, j2));
                                    break;
                                }
                            case 12:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.b(i7, unsafe.getInt(t2, j2));
                                    break;
                                }
                            case 13:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.a(i7, unsafe.getInt(t2, j2));
                                    break;
                                }
                            case 14:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.a(i7, unsafe.getLong(t2, j2));
                                    break;
                                }
                            case 15:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.zzf(i7, unsafe.getInt(t2, j2));
                                    break;
                                }
                            case 16:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.zzb(i7, unsafe.getLong(t2, j2));
                                    break;
                                }
                            case 17:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    sb12.b(i7, unsafe.getObject(t2, j2), a(i2));
                                    break;
                                }
                            case 18:
                                pa1.a(this.a[i2], (List<Double>) (List) unsafe.getObject(t2, j2), sb12, false);
                                break;
                            case 19:
                                pa1.b(this.a[i2], (List<Float>) (List) unsafe.getObject(t2, j2), sb12, false);
                                break;
                            case 20:
                                pa1.c(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, false);
                                break;
                            case 21:
                                pa1.d(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, false);
                                break;
                            case 22:
                                pa1.h(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, false);
                                break;
                            case 23:
                                pa1.f(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, false);
                                break;
                            case 24:
                                pa1.k(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, false);
                                break;
                            case 25:
                                pa1.n(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, false);
                                break;
                            case 26:
                                pa1.a(this.a[i2], (List<String>) (List) unsafe.getObject(t2, j2), sb12);
                                break;
                            case 27:
                                pa1.a(this.a[i2], (List<?>) (List) unsafe.getObject(t2, j2), sb12, a(i2));
                                break;
                            case 28:
                                pa1.b(this.a[i2], (List<zzte>) (List) unsafe.getObject(t2, j2), sb12);
                                break;
                            case 29:
                                pa1.i(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, false);
                                break;
                            case 30:
                                pa1.m(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, false);
                                break;
                            case 31:
                                pa1.l(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, false);
                                break;
                            case 32:
                                pa1.g(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, false);
                                break;
                            case 33:
                                pa1.j(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, false);
                                break;
                            case 34:
                                pa1.e(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, false);
                                break;
                            case 35:
                                pa1.a(this.a[i2], (List<Double>) (List) unsafe.getObject(t2, j2), sb12, true);
                                break;
                            case 36:
                                pa1.b(this.a[i2], (List<Float>) (List) unsafe.getObject(t2, j2), sb12, true);
                                break;
                            case 37:
                                pa1.c(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, true);
                                break;
                            case 38:
                                pa1.d(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, true);
                                break;
                            case 39:
                                pa1.h(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, true);
                                break;
                            case 40:
                                pa1.f(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, true);
                                break;
                            case 41:
                                pa1.k(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, true);
                                break;
                            case 42:
                                pa1.n(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, true);
                                break;
                            case 43:
                                pa1.i(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, true);
                                break;
                            case 44:
                                pa1.m(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, true);
                                break;
                            case 45:
                                pa1.l(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, true);
                                break;
                            case 46:
                                pa1.g(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, true);
                                break;
                            case 47:
                                pa1.j(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, true);
                                break;
                            case 48:
                                pa1.e(this.a[i2], (List) unsafe.getObject(t2, j2), sb12, true);
                                break;
                            case 49:
                                pa1.b(this.a[i2], (List<?>) (List) unsafe.getObject(t2, j2), sb12, a(i2));
                                break;
                            case 50:
                                a(sb12, i7, unsafe.getObject(t2, j2), i2);
                                break;
                            case 51:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.zza(i7, b(t2, j2));
                                    break;
                                }
                            case 52:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.zza(i7, c(t2, j2));
                                    break;
                                }
                            case 53:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.b(i7, e(t2, j2));
                                    break;
                                }
                            case 54:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.zza(i7, e(t2, j2));
                                    break;
                                }
                            case 55:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.zzd(i7, d(t2, j2));
                                    break;
                                }
                            case 56:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.zzc(i7, e(t2, j2));
                                    break;
                                }
                            case 57:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.c(i7, d(t2, j2));
                                    break;
                                }
                            case 58:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.a(i7, f(t2, j2));
                                    break;
                                }
                            case 59:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    a(i7, unsafe.getObject(t2, j2), sb12);
                                    break;
                                }
                            case 60:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.a(i7, unsafe.getObject(t2, j2), a(i2));
                                    break;
                                }
                            case 61:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.a(i7, (zzte) unsafe.getObject(t2, j2));
                                    break;
                                }
                            case 62:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.zze(i7, d(t2, j2));
                                    break;
                                }
                            case 63:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.b(i7, d(t2, j2));
                                    break;
                                }
                            case 64:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.a(i7, d(t2, j2));
                                    break;
                                }
                            case 65:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.a(i7, e(t2, j2));
                                    break;
                                }
                            case 66:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.zzf(i7, d(t2, j2));
                                    break;
                                }
                            case 67:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.zzb(i7, e(t2, j2));
                                    break;
                                }
                            case 68:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    sb12.b(i7, unsafe.getObject(t2, j2), a(i2));
                                    break;
                                }
                        }
                        i2 += 3;
                        i5 = i3;
                    } else {
                        this.p.a((Map.Entry<?, ?>) entry);
                        throw null;
                    }
                }
                if (entry != null) {
                    a(this.o, t2, sb12);
                    return;
                } else {
                    this.p.a(sb12, entry);
                    throw null;
                }
            }
        }
        entry = null;
        length = this.a.length;
        Unsafe unsafe2 = s;
        i2 = 0;
        int i52 = -1;
        int i62 = 0;
        while (i2 < length) {
        }
        if (entry != null) {
        }
    }

    @DexIgnore
    public final <K, V> void a(sb1 sb1, int i2, Object obj, int i3) throws IOException {
        if (obj != null) {
            this.q.c(b(i3));
            throw null;
        }
    }

    @DexIgnore
    public static <UT, UB> void a(eb1<UT, UB> eb1, T t, sb1 sb1) throws IOException {
        eb1.a(eb1.c(t), sb1);
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    @DexIgnore
    public final void a(T r13, com.fossil.blesdk.obfuscated.ma1 r14, com.fossil.blesdk.obfuscated.i81 r15) throws java.io.IOException {
        /*
            r12 = this;
            if (r15 == 0) goto L_0x05d0
            com.fossil.blesdk.obfuscated.eb1<?, ?> r7 = r12.o
            com.fossil.blesdk.obfuscated.j81<?> r0 = r12.p
            r8 = 0
            r9 = r8
        L_0x0008:
            int r1 = r14.j()     // Catch:{ all -> 0x05b9 }
            int r2 = r12.c     // Catch:{ all -> 0x05b9 }
            r3 = -1
            if (r1 < r2) goto L_0x0035
            int r2 = r12.d     // Catch:{ all -> 0x05b9 }
            if (r1 > r2) goto L_0x0035
            r2 = 0
            int[] r4 = r12.a     // Catch:{ all -> 0x05b9 }
            int r4 = r4.length     // Catch:{ all -> 0x05b9 }
            int r4 = r4 / 3
            int r4 = r4 + -1
        L_0x001d:
            if (r2 > r4) goto L_0x0035
            int r5 = r4 + r2
            int r5 = r5 >>> 1
            int r6 = r5 * 3
            int[] r10 = r12.a     // Catch:{ all -> 0x05b9 }
            r10 = r10[r6]     // Catch:{ all -> 0x05b9 }
            if (r1 != r10) goto L_0x002d
            r3 = r6
            goto L_0x0035
        L_0x002d:
            if (r1 >= r10) goto L_0x0032
            int r4 = r5 + -1
            goto L_0x001d
        L_0x0032:
            int r2 = r5 + 1
            goto L_0x001d
        L_0x0035:
            if (r3 >= 0) goto L_0x0092
            r2 = 2147483647(0x7fffffff, float:NaN)
            if (r1 != r2) goto L_0x0052
            int r14 = r12.k
        L_0x003e:
            int r15 = r12.l
            if (r14 >= r15) goto L_0x004c
            int[] r15 = r12.j
            r15 = r15[r14]
            r12.a((java.lang.Object) r13, (int) r15, r9, r7)
            int r14 = r14 + 1
            goto L_0x003e
        L_0x004c:
            if (r9 == 0) goto L_0x0051
            r7.b((java.lang.Object) r13, r9)
        L_0x0051:
            return
        L_0x0052:
            boolean r2 = r12.f     // Catch:{ all -> 0x05b9 }
            if (r2 != 0) goto L_0x0058
            r2 = r8
            goto L_0x005f
        L_0x0058:
            com.fossil.blesdk.obfuscated.w91 r2 = r12.e     // Catch:{ all -> 0x05b9 }
            java.lang.Object r1 = r0.a(r15, r2, r1)     // Catch:{ all -> 0x05b9 }
            r2 = r1
        L_0x005f:
            if (r2 == 0) goto L_0x006d
            com.fossil.blesdk.obfuscated.m81 r4 = r0.b(r13)     // Catch:{ all -> 0x05b9 }
            r1 = r14
            r3 = r15
            r5 = r9
            r6 = r7
            r0.a(r1, r2, r3, r4, r5, r6)     // Catch:{ all -> 0x05b9 }
            throw r8
        L_0x006d:
            r7.a((com.fossil.blesdk.obfuscated.ma1) r14)     // Catch:{ all -> 0x05b9 }
            if (r9 != 0) goto L_0x0076
            java.lang.Object r9 = r7.d(r13)     // Catch:{ all -> 0x05b9 }
        L_0x0076:
            boolean r1 = r7.a(r9, (com.fossil.blesdk.obfuscated.ma1) r14)     // Catch:{ all -> 0x05b9 }
            if (r1 != 0) goto L_0x0008
            int r14 = r12.k
        L_0x007e:
            int r15 = r12.l
            if (r14 >= r15) goto L_0x008c
            int[] r15 = r12.j
            r15 = r15[r14]
            r12.a((java.lang.Object) r13, (int) r15, r9, r7)
            int r14 = r14 + 1
            goto L_0x007e
        L_0x008c:
            if (r9 == 0) goto L_0x0091
            r7.b((java.lang.Object) r13, r9)
        L_0x0091:
            return
        L_0x0092:
            int r2 = r12.d((int) r3)     // Catch:{ all -> 0x05b9 }
            r4 = 267386880(0xff00000, float:2.3665827E-29)
            r4 = r4 & r2
            int r4 = r4 >>> 20
            r5 = 1048575(0xfffff, float:1.469367E-39)
            switch(r4) {
                case 0: goto L_0x0568;
                case 1: goto L_0x0559;
                case 2: goto L_0x054a;
                case 3: goto L_0x053b;
                case 4: goto L_0x052c;
                case 5: goto L_0x051d;
                case 6: goto L_0x050e;
                case 7: goto L_0x04ff;
                case 8: goto L_0x04f7;
                case 9: goto L_0x04c6;
                case 10: goto L_0x04b7;
                case 11: goto L_0x04a8;
                case 12: goto L_0x0486;
                case 13: goto L_0x0477;
                case 14: goto L_0x0468;
                case 15: goto L_0x0459;
                case 16: goto L_0x044a;
                case 17: goto L_0x0419;
                case 18: goto L_0x040c;
                case 19: goto L_0x03ff;
                case 20: goto L_0x03f2;
                case 21: goto L_0x03e5;
                case 22: goto L_0x03d8;
                case 23: goto L_0x03cb;
                case 24: goto L_0x03be;
                case 25: goto L_0x03b1;
                case 26: goto L_0x0391;
                case 27: goto L_0x0380;
                case 28: goto L_0x0373;
                case 29: goto L_0x0366;
                case 30: goto L_0x0351;
                case 31: goto L_0x0344;
                case 32: goto L_0x0337;
                case 33: goto L_0x032a;
                case 34: goto L_0x031d;
                case 35: goto L_0x0310;
                case 36: goto L_0x0303;
                case 37: goto L_0x02f6;
                case 38: goto L_0x02e9;
                case 39: goto L_0x02dc;
                case 40: goto L_0x02cf;
                case 41: goto L_0x02c2;
                case 42: goto L_0x02b5;
                case 43: goto L_0x02a8;
                case 44: goto L_0x0293;
                case 45: goto L_0x0286;
                case 46: goto L_0x0279;
                case 47: goto L_0x026c;
                case 48: goto L_0x025f;
                case 49: goto L_0x024d;
                case 50: goto L_0x0211;
                case 51: goto L_0x01ff;
                case 52: goto L_0x01ed;
                case 53: goto L_0x01db;
                case 54: goto L_0x01c9;
                case 55: goto L_0x01b7;
                case 56: goto L_0x01a5;
                case 57: goto L_0x0193;
                case 58: goto L_0x0181;
                case 59: goto L_0x0179;
                case 60: goto L_0x0148;
                case 61: goto L_0x013a;
                case 62: goto L_0x0128;
                case 63: goto L_0x0103;
                case 64: goto L_0x00f1;
                case 65: goto L_0x00df;
                case 66: goto L_0x00cd;
                case 67: goto L_0x00bb;
                case 68: goto L_0x00a9;
                default: goto L_0x00a1;
            }
        L_0x00a1:
            if (r9 != 0) goto L_0x0577
            java.lang.Object r9 = r7.a()     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0577
        L_0x00a9:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.na1 r2 = r12.a((int) r3)     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r2 = r14.a(r2, r15)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x00bb:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            long r10 = r14.i()     // Catch:{ zzuw -> 0x0593 }
            java.lang.Long r2 = java.lang.Long.valueOf(r10)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x00cd:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            int r2 = r14.g()     // Catch:{ zzuw -> 0x0593 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x00df:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            long r10 = r14.k()     // Catch:{ zzuw -> 0x0593 }
            java.lang.Long r2 = java.lang.Long.valueOf(r10)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x00f1:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            int r2 = r14.o()     // Catch:{ zzuw -> 0x0593 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0103:
            int r4 = r14.q()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.y81 r6 = r12.c((int) r3)     // Catch:{ zzuw -> 0x0593 }
            if (r6 == 0) goto L_0x011a
            boolean r6 = r6.zzb(r4)     // Catch:{ zzuw -> 0x0593 }
            if (r6 == 0) goto L_0x0114
            goto L_0x011a
        L_0x0114:
            java.lang.Object r9 = com.fossil.blesdk.obfuscated.pa1.a((int) r1, (int) r4, r9, r7)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x011a:
            r2 = r2 & r5
            long r5 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r4)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r5, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0128:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            int r2 = r14.b()     // Catch:{ zzuw -> 0x0593 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x013a:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            com.google.android.gms.internal.measurement.zzte r2 = r14.d()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0148:
            boolean r4 = r12.a(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            if (r4 == 0) goto L_0x0164
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r2 = com.fossil.blesdk.obfuscated.kb1.f(r13, r4)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.na1 r6 = r12.a((int) r3)     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r6 = r14.b(r6, r15)     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r2 = com.fossil.blesdk.obfuscated.v81.a((java.lang.Object) r2, (java.lang.Object) r6)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0174
        L_0x0164:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.na1 r2 = r12.a((int) r3)     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r2 = r14.b(r2, r15)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
        L_0x0174:
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0179:
            r12.a((java.lang.Object) r13, (int) r2, (com.fossil.blesdk.obfuscated.ma1) r14)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0181:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            boolean r2 = r14.f()     // Catch:{ zzuw -> 0x0593 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0193:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            int r2 = r14.h()     // Catch:{ zzuw -> 0x0593 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x01a5:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            long r10 = r14.m()     // Catch:{ zzuw -> 0x0593 }
            java.lang.Long r2 = java.lang.Long.valueOf(r10)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x01b7:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            int r2 = r14.p()     // Catch:{ zzuw -> 0x0593 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x01c9:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            long r10 = r14.c()     // Catch:{ zzuw -> 0x0593 }
            java.lang.Long r2 = java.lang.Long.valueOf(r10)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x01db:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            long r10 = r14.r()     // Catch:{ zzuw -> 0x0593 }
            java.lang.Long r2 = java.lang.Long.valueOf(r10)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x01ed:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            float r2 = r14.readFloat()     // Catch:{ zzuw -> 0x0593 }
            java.lang.Float r2 = java.lang.Float.valueOf(r2)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x01ff:
            r2 = r2 & r5
            long r4 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            double r10 = r14.readDouble()     // Catch:{ zzuw -> 0x0593 }
            java.lang.Double r2 = java.lang.Double.valueOf(r10)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r4, (java.lang.Object) r2)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r1, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0211:
            java.lang.Object r1 = r12.b((int) r3)     // Catch:{ zzuw -> 0x0593 }
            int r2 = r12.d((int) r3)     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r4 = com.fossil.blesdk.obfuscated.kb1.f(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            if (r4 == 0) goto L_0x0239
            com.fossil.blesdk.obfuscated.r91 r5 = r12.q     // Catch:{ zzuw -> 0x0593 }
            boolean r5 = r5.f(r4)     // Catch:{ zzuw -> 0x0593 }
            if (r5 == 0) goto L_0x0242
            com.fossil.blesdk.obfuscated.r91 r5 = r12.q     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r5 = r5.b(r1)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.r91 r6 = r12.q     // Catch:{ zzuw -> 0x0593 }
            r6.b(r5, r4)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r2, (java.lang.Object) r5)     // Catch:{ zzuw -> 0x0593 }
            r4 = r5
            goto L_0x0242
        L_0x0239:
            com.fossil.blesdk.obfuscated.r91 r4 = r12.q     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r4 = r4.b(r1)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r2, (java.lang.Object) r4)     // Catch:{ zzuw -> 0x0593 }
        L_0x0242:
            com.fossil.blesdk.obfuscated.r91 r2 = r12.q     // Catch:{ zzuw -> 0x0593 }
            r2.d(r4)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.r91 r2 = r12.q     // Catch:{ zzuw -> 0x0593 }
            r2.c(r1)     // Catch:{ zzuw -> 0x0593 }
            throw r8
        L_0x024d:
            r1 = r2 & r5
            long r1 = (long) r1
            com.fossil.blesdk.obfuscated.na1 r3 = r12.a((int) r3)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.i91 r4 = r12.n     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r4.a(r13, r1)     // Catch:{ zzuw -> 0x0593 }
            r14.a(r1, r3, r15)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x025f:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.c(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x026c:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.d(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0279:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.o(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0286:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.q(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0293:
            com.fossil.blesdk.obfuscated.i91 r4 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r5 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r2 = r4.a(r13, r5)     // Catch:{ zzuw -> 0x0593 }
            r14.j(r2)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.y81 r3 = r12.c((int) r3)     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r9 = com.fossil.blesdk.obfuscated.pa1.a(r1, r2, r3, r9, r7)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x02a8:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.l(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x02b5:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.a(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x02c2:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.b(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x02cf:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.n(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x02dc:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.p(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x02e9:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.k(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x02f6:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.i(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0303:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.e(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0310:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.g(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x031d:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.c(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x032a:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.d(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0337:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.o(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0344:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.q(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0351:
            com.fossil.blesdk.obfuscated.i91 r4 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r5 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r2 = r4.a(r13, r5)     // Catch:{ zzuw -> 0x0593 }
            r14.j(r2)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.y81 r3 = r12.c((int) r3)     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r9 = com.fossil.blesdk.obfuscated.pa1.a(r1, r2, r3, r9, r7)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0366:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.l(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0373:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.f(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0380:
            com.fossil.blesdk.obfuscated.na1 r1 = r12.a((int) r3)     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.i91 r4 = r12.n     // Catch:{ zzuw -> 0x0593 }
            java.util.List r2 = r4.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.b(r2, r1, r15)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0391:
            boolean r1 = f(r2)     // Catch:{ zzuw -> 0x0593 }
            if (r1 == 0) goto L_0x03a4
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.h(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x03a4:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.m(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x03b1:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.a(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x03be:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.b(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x03cb:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.n(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x03d8:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.p(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x03e5:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.k(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x03f2:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.i(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x03ff:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.e(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x040c:
            com.fossil.blesdk.obfuscated.i91 r1 = r12.n     // Catch:{ zzuw -> 0x0593 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzuw -> 0x0593 }
            java.util.List r1 = r1.a(r13, r2)     // Catch:{ zzuw -> 0x0593 }
            r14.g(r1)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0419:
            boolean r1 = r12.a(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            if (r1 == 0) goto L_0x0437
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r4 = com.fossil.blesdk.obfuscated.kb1.f(r13, r1)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.na1 r3 = r12.a((int) r3)     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r3 = r14.a(r3, r15)     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r3 = com.fossil.blesdk.obfuscated.v81.a((java.lang.Object) r4, (java.lang.Object) r3)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (java.lang.Object) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0437:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.na1 r4 = r12.a((int) r3)     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r4 = r14.a(r4, r15)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (java.lang.Object) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x044a:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            long r4 = r14.i()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (long) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0459:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            int r4 = r14.g()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (int) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0468:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            long r4 = r14.k()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (long) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0477:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            int r4 = r14.o()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (int) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0486:
            int r4 = r14.q()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.y81 r6 = r12.c((int) r3)     // Catch:{ zzuw -> 0x0593 }
            if (r6 == 0) goto L_0x049d
            boolean r6 = r6.zzb(r4)     // Catch:{ zzuw -> 0x0593 }
            if (r6 == 0) goto L_0x0497
            goto L_0x049d
        L_0x0497:
            java.lang.Object r9 = com.fossil.blesdk.obfuscated.pa1.a((int) r1, (int) r4, r9, r7)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x049d:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (int) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x04a8:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            int r4 = r14.b()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (int) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x04b7:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            com.google.android.gms.internal.measurement.zzte r4 = r14.d()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (java.lang.Object) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x04c6:
            boolean r1 = r12.a(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            if (r1 == 0) goto L_0x04e4
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r4 = com.fossil.blesdk.obfuscated.kb1.f(r13, r1)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.na1 r3 = r12.a((int) r3)     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r3 = r14.b(r3, r15)     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r3 = com.fossil.blesdk.obfuscated.v81.a((java.lang.Object) r4, (java.lang.Object) r3)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (java.lang.Object) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x04e4:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.na1 r4 = r12.a((int) r3)     // Catch:{ zzuw -> 0x0593 }
            java.lang.Object r4 = r14.b(r4, r15)     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (java.lang.Object) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x04f7:
            r12.a((java.lang.Object) r13, (int) r2, (com.fossil.blesdk.obfuscated.ma1) r14)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x04ff:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            boolean r4 = r14.f()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (boolean) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x050e:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            int r4 = r14.h()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (int) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x051d:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            long r4 = r14.m()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (long) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x052c:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            int r4 = r14.p()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (int) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x053b:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            long r4 = r14.c()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (long) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x054a:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            long r4 = r14.r()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (long) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0559:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            float r4 = r14.readFloat()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (float) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0568:
            r1 = r2 & r5
            long r1 = (long) r1     // Catch:{ zzuw -> 0x0593 }
            double r4 = r14.readDouble()     // Catch:{ zzuw -> 0x0593 }
            com.fossil.blesdk.obfuscated.kb1.a((java.lang.Object) r13, (long) r1, (double) r4)     // Catch:{ zzuw -> 0x0593 }
            r12.b(r13, (int) r3)     // Catch:{ zzuw -> 0x0593 }
            goto L_0x0008
        L_0x0577:
            boolean r1 = r7.a(r9, (com.fossil.blesdk.obfuscated.ma1) r14)     // Catch:{ zzuw -> 0x0593 }
            if (r1 != 0) goto L_0x0008
            int r14 = r12.k
        L_0x057f:
            int r15 = r12.l
            if (r14 >= r15) goto L_0x058d
            int[] r15 = r12.j
            r15 = r15[r14]
            r12.a((java.lang.Object) r13, (int) r15, r9, r7)
            int r14 = r14 + 1
            goto L_0x057f
        L_0x058d:
            if (r9 == 0) goto L_0x0592
            r7.b((java.lang.Object) r13, r9)
        L_0x0592:
            return
        L_0x0593:
            r7.a((com.fossil.blesdk.obfuscated.ma1) r14)     // Catch:{ all -> 0x05b9 }
            if (r9 != 0) goto L_0x059d
            java.lang.Object r1 = r7.d(r13)     // Catch:{ all -> 0x05b9 }
            r9 = r1
        L_0x059d:
            boolean r1 = r7.a(r9, (com.fossil.blesdk.obfuscated.ma1) r14)     // Catch:{ all -> 0x05b9 }
            if (r1 != 0) goto L_0x0008
            int r14 = r12.k
        L_0x05a5:
            int r15 = r12.l
            if (r14 >= r15) goto L_0x05b3
            int[] r15 = r12.j
            r15 = r15[r14]
            r12.a((java.lang.Object) r13, (int) r15, r9, r7)
            int r14 = r14 + 1
            goto L_0x05a5
        L_0x05b3:
            if (r9 == 0) goto L_0x05b8
            r7.b((java.lang.Object) r13, r9)
        L_0x05b8:
            return
        L_0x05b9:
            r14 = move-exception
            int r15 = r12.k
        L_0x05bc:
            int r0 = r12.l
            if (r15 >= r0) goto L_0x05ca
            int[] r0 = r12.j
            r0 = r0[r15]
            r12.a((java.lang.Object) r13, (int) r0, r9, r7)
            int r15 = r15 + 1
            goto L_0x05bc
        L_0x05ca:
            if (r9 == 0) goto L_0x05cf
            r7.b((java.lang.Object) r13, r9)
        L_0x05cf:
            throw r14
        L_0x05d0:
            java.lang.NullPointerException r13 = new java.lang.NullPointerException
            r13.<init>()
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.blesdk.obfuscated.aa1.a(java.lang.Object, com.fossil.blesdk.obfuscated.ma1, com.fossil.blesdk.obfuscated.i81):void");
    }

    @DexIgnore
    public final Object b(int i2) {
        return this.b[(i2 / 3) << 1];
    }

    @DexIgnore
    public static <T> double b(T t, long j2) {
        return ((Double) kb1.f(t, j2)).doubleValue();
    }

    @DexIgnore
    public final void b(T t, int i2) {
        if (!this.h) {
            int e2 = e(i2);
            long j2 = (long) (e2 & 1048575);
            kb1.a((Object) t, j2, kb1.a((Object) t, j2) | (1 << (e2 >>> 20)));
        }
    }

    @DexIgnore
    public final void b(T t, int i2, int i3) {
        kb1.a((Object) t, (long) (e(i3) & 1048575), i2);
    }

    @DexIgnore
    public final na1 a(int i2) {
        int i3 = (i2 / 3) << 1;
        na1 na1 = (na1) this.b[i3];
        if (na1 != null) {
            return na1;
        }
        na1 a2 = ja1.a().a((Class) this.b[i3 + 1]);
        this.b[i3] = a2;
        return a2;
    }

    @DexIgnore
    public final <UT, UB> UB a(Object obj, int i2, UB ub, eb1<UT, UB> eb1) {
        int i3 = this.a[i2];
        Object f2 = kb1.f(obj, (long) (d(i2) & 1048575));
        if (f2 == null) {
            return ub;
        }
        y81 c2 = c(i2);
        if (c2 == null) {
            return ub;
        }
        a(i2, i3, this.q.d(f2), c2, ub, eb1);
        throw null;
    }

    @DexIgnore
    public final <K, V, UT, UB> UB a(int i2, int i3, Map<K, V> map, y81 y81, UB ub, eb1<UT, UB> eb1) {
        this.q.c(b(i2));
        throw null;
    }

    @DexIgnore
    public static boolean a(Object obj, int i2, na1 na1) {
        return na1.c(kb1.f(obj, (long) (i2 & 1048575)));
    }

    @DexIgnore
    public static void a(int i2, Object obj, sb1 sb1) throws IOException {
        if (obj instanceof String) {
            sb1.a(i2, (String) obj);
        } else {
            sb1.a(i2, (zzte) obj);
        }
    }

    @DexIgnore
    public final void a(Object obj, int i2, ma1 ma1) throws IOException {
        if (f(i2)) {
            kb1.a(obj, (long) (i2 & 1048575), (Object) ma1.e());
        } else if (this.g) {
            kb1.a(obj, (long) (i2 & 1048575), (Object) ma1.l());
        } else {
            kb1.a(obj, (long) (i2 & 1048575), (Object) ma1.d());
        }
    }

    @DexIgnore
    public final boolean a(T t, int i2, int i3, int i4) {
        if (this.h) {
            return a(t, i2);
        }
        return (i3 & i4) != 0;
    }

    @DexIgnore
    public final boolean a(T t, int i2) {
        if (this.h) {
            int d2 = d(i2);
            long j2 = (long) (d2 & 1048575);
            switch ((d2 & 267386880) >>> 20) {
                case 0:
                    return kb1.e(t, j2) != 0.0d;
                case 1:
                    return kb1.d(t, j2) != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                case 2:
                    return kb1.b(t, j2) != 0;
                case 3:
                    return kb1.b(t, j2) != 0;
                case 4:
                    return kb1.a((Object) t, j2) != 0;
                case 5:
                    return kb1.b(t, j2) != 0;
                case 6:
                    return kb1.a((Object) t, j2) != 0;
                case 7:
                    return kb1.c(t, j2);
                case 8:
                    Object f2 = kb1.f(t, j2);
                    if (f2 instanceof String) {
                        return !((String) f2).isEmpty();
                    }
                    if (f2 instanceof zzte) {
                        return !zzte.zzbts.equals(f2);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return kb1.f(t, j2) != null;
                case 10:
                    return !zzte.zzbts.equals(kb1.f(t, j2));
                case 11:
                    return kb1.a((Object) t, j2) != 0;
                case 12:
                    return kb1.a((Object) t, j2) != 0;
                case 13:
                    return kb1.a((Object) t, j2) != 0;
                case 14:
                    return kb1.b(t, j2) != 0;
                case 15:
                    return kb1.a((Object) t, j2) != 0;
                case 16:
                    return kb1.b(t, j2) != 0;
                case 17:
                    return kb1.f(t, j2) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int e2 = e(i2);
            return (kb1.a((Object) t, (long) (e2 & 1048575)) & (1 << (e2 >>> 20))) != 0;
        }
    }

    @DexIgnore
    public final boolean a(T t, int i2, int i3) {
        return kb1.a((Object) t, (long) (e(i3) & 1048575)) == i2;
    }
}
