package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hv */
public class C1976hv {

    @DexIgnore
    /* renamed from: c */
    public static /* final */ com.fossil.blesdk.obfuscated.C3376yp<?, ?, ?> f5839c;

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1855g4<com.fossil.blesdk.obfuscated.C2925sw, com.fossil.blesdk.obfuscated.C3376yp<?, ?, ?>> f5840a; // = new com.fossil.blesdk.obfuscated.C1855g4<>();

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.concurrent.atomic.AtomicReference<com.fossil.blesdk.obfuscated.C2925sw> f5841b; // = new java.util.concurrent.atomic.AtomicReference<>();

    /*
    static {
        com.fossil.blesdk.obfuscated.C2582op opVar = new com.fossil.blesdk.obfuscated.C2582op(java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.util.Collections.emptyList(), new com.fossil.blesdk.obfuscated.C2075iu(), (com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>>) null);
        com.fossil.blesdk.obfuscated.C3376yp ypVar = new com.fossil.blesdk.obfuscated.C3376yp(java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.util.Collections.singletonList(opVar), (com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>>) null);
        f5839c = ypVar;
    }
    */

    @DexIgnore
    /* renamed from: a */
    public boolean mo11747a(com.fossil.blesdk.obfuscated.C3376yp<?, ?, ?> ypVar) {
        return f5839c.equals(ypVar);
    }

    @DexIgnore
    /* renamed from: b */
    public final com.fossil.blesdk.obfuscated.C2925sw mo11748b(java.lang.Class<?> cls, java.lang.Class<?> cls2, java.lang.Class<?> cls3) {
        com.fossil.blesdk.obfuscated.C2925sw andSet = this.f5841b.getAndSet((java.lang.Object) null);
        if (andSet == null) {
            andSet = new com.fossil.blesdk.obfuscated.C2925sw();
        }
        andSet.mo16203a(cls, cls2, cls3);
        return andSet;
    }

    @DexIgnore
    /* renamed from: a */
    public <Data, TResource, Transcode> com.fossil.blesdk.obfuscated.C3376yp<Data, TResource, Transcode> mo11745a(java.lang.Class<Data> cls, java.lang.Class<TResource> cls2, java.lang.Class<Transcode> cls3) {
        com.fossil.blesdk.obfuscated.C3376yp<Data, TResource, Transcode> ypVar;
        com.fossil.blesdk.obfuscated.C2925sw b = mo11748b(cls, cls2, cls3);
        synchronized (this.f5840a) {
            ypVar = this.f5840a.get(b);
        }
        this.f5841b.set(b);
        return ypVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11746a(java.lang.Class<?> cls, java.lang.Class<?> cls2, java.lang.Class<?> cls3, com.fossil.blesdk.obfuscated.C3376yp<?, ?, ?> ypVar) {
        synchronized (this.f5840a) {
            com.fossil.blesdk.obfuscated.C1855g4<com.fossil.blesdk.obfuscated.C2925sw, com.fossil.blesdk.obfuscated.C3376yp<?, ?, ?>> g4Var = this.f5840a;
            com.fossil.blesdk.obfuscated.C2925sw swVar = new com.fossil.blesdk.obfuscated.C2925sw(cls, cls2, cls3);
            if (ypVar == null) {
                ypVar = f5839c;
            }
            g4Var.put(swVar, ypVar);
        }
    }
}
