package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.usecase.RequestEmailOtp;
import com.portfolio.platform.usecase.VerifyEmailOtp;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uo3 implements MembersInjector<so3> {
    @DexIgnore
    public static void a(so3 so3, RequestEmailOtp requestEmailOtp) {
        so3.f = requestEmailOtp;
    }

    @DexIgnore
    public static void a(so3 so3, VerifyEmailOtp verifyEmailOtp) {
        so3.g = verifyEmailOtp;
    }

    @DexIgnore
    public static void a(so3 so3) {
        so3.m();
    }
}
