package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.b6 */
public final class C1465b6 {
    @DexIgnore
    /* renamed from: a */
    public static void m4791a(android.app.Activity activity, android.content.Intent intent) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            activity.navigateUpTo(intent);
            return;
        }
        intent.addFlags(67108864);
        activity.startActivity(intent);
        activity.finish();
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m4794b(android.app.Activity activity, android.content.Intent intent) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return activity.shouldUpRecreateTask(intent);
        }
        java.lang.String action = activity.getIntent().getAction();
        return action != null && !action.equals("android.intent.action.MAIN");
    }

    @DexIgnore
    /* renamed from: c */
    public static void m4795c(android.app.Activity activity) {
        android.content.Intent a = m4789a(activity);
        if (a != null) {
            m4791a(activity, a);
            return;
        }
        throw new java.lang.IllegalArgumentException("Activity " + activity.getClass().getSimpleName() + " does not have a parent activity name specified." + " (Did you forget to add the android.support.PARENT_ACTIVITY <meta-data> " + " element in your manifest?)");
    }

    @DexIgnore
    /* renamed from: b */
    public static java.lang.String m4792b(android.app.Activity activity) {
        try {
            return m4793b((android.content.Context) activity, activity.getComponentName());
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            throw new java.lang.IllegalArgumentException(e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.content.Intent m4789a(android.app.Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            android.content.Intent parentActivityIntent = activity.getParentActivityIntent();
            if (parentActivityIntent != null) {
                return parentActivityIntent;
            }
        }
        java.lang.String b = m4792b(activity);
        if (b == null) {
            return null;
        }
        android.content.ComponentName componentName = new android.content.ComponentName(activity, b);
        try {
            if (m4793b((android.content.Context) activity, componentName) == null) {
                return android.content.Intent.makeMainActivity(componentName);
            }
            return new android.content.Intent().setComponent(componentName);
        } catch (android.content.pm.PackageManager.NameNotFoundException unused) {
            android.util.Log.e("NavUtils", "getParentActivityIntent: bad parentActivityName '" + b + "' in manifest");
            return null;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static java.lang.String m4793b(android.content.Context context, android.content.ComponentName componentName) throws android.content.pm.PackageManager.NameNotFoundException {
        android.content.pm.ActivityInfo activityInfo = context.getPackageManager().getActivityInfo(componentName, 128);
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            java.lang.String str = activityInfo.parentActivityName;
            if (str != null) {
                return str;
            }
        }
        if (activityInfo.metaData == null) {
            return null;
        }
        java.lang.String string = activityInfo.metaData.getString("android.support.PARENT_ACTIVITY");
        if (string == null) {
            return null;
        }
        if (string.charAt(0) != '.') {
            return string;
        }
        return context.getPackageName() + string;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.content.Intent m4790a(android.content.Context context, android.content.ComponentName componentName) throws android.content.pm.PackageManager.NameNotFoundException {
        java.lang.String b = m4793b(context, componentName);
        if (b == null) {
            return null;
        }
        android.content.ComponentName componentName2 = new android.content.ComponentName(componentName.getPackageName(), b);
        if (m4793b(context, componentName2) == null) {
            return android.content.Intent.makeMainActivity(componentName2);
        }
        return new android.content.Intent().setComponent(componentName2);
    }
}
