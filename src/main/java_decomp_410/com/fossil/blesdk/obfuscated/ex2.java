package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ex2 implements Factory<dx2> {
    @DexIgnore
    public static /* final */ ex2 a; // = new ex2();

    @DexIgnore
    public static ex2 a() {
        return a;
    }

    @DexIgnore
    public static dx2 b() {
        return new dx2();
    }

    @DexIgnore
    public dx2 get() {
        return b();
    }
}
