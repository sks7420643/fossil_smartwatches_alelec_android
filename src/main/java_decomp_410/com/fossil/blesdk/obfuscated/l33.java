package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.r62;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.Ringtone;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l33 extends zr2 implements k33, r62.b {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((fd4) null);
    @DexIgnore
    public tr3<k82> j;
    @DexIgnore
    public r62 k;
    @DexIgnore
    public j33 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return l33.n;
        }

        @DexIgnore
        public final l33 b() {
            return new l33();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ l33 e;

        @DexIgnore
        public b(l33 l33) {
            this.e = l33;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    /*
    static {
        String simpleName = l33.class.getSimpleName();
        kd4.a((Object) simpleName, "SearchRingPhoneFragment::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return n;
    }

    @DexIgnore
    public boolean S0() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return false;
        }
        j33 j33 = this.l;
        if (j33 != null) {
            j33.i();
            Intent intent = new Intent();
            j33 j332 = this.l;
            if (j332 != null) {
                intent.putExtra("KEY_SELECTED_RINGPHONE", j332.h());
                activity.setResult(-1, intent);
                activity.finish();
                return false;
            }
            kd4.d("mPresenter");
            throw null;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new tr3<>(this, (k82) qa.a(layoutInflater, R.layout.activity_search_ringphone, viewGroup, false, O0()));
        tr3<k82> tr3 = this.j;
        if (tr3 != null) {
            k82 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        j33 j33 = this.l;
        if (j33 != null) {
            j33.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        j33 j33 = this.l;
        if (j33 != null) {
            j33.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<k82> tr3 = this.j;
        if (tr3 != null) {
            k82 a2 = tr3.a();
            if (a2 != null) {
                this.k = new r62(this);
                RecyclerView recyclerView = a2.r;
                kd4.a((Object) recyclerView, "it.rvRingphones");
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView2 = a2.r;
                kd4.a((Object) recyclerView2, "it.rvRingphones");
                r62 r62 = this.k;
                if (r62 != null) {
                    recyclerView2.setAdapter(r62);
                    a2.q.setOnClickListener(new b(this));
                    return;
                }
                kd4.d("mSearchRingPhoneAdapter");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void r(List<Ringtone> list) {
        kd4.b(list, "data");
        r62 r62 = this.k;
        if (r62 != null) {
            j33 j33 = this.l;
            if (j33 != null) {
                r62.a(list, j33.h());
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        } else {
            kd4.d("mSearchRingPhoneAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(Ringtone ringtone) {
        kd4.b(ringtone, Constants.RINGTONE);
        j33 j33 = this.l;
        if (j33 != null) {
            j33.a(ringtone);
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(j33 j33) {
        kd4.b(j33, "presenter");
        this.l = j33;
    }
}
