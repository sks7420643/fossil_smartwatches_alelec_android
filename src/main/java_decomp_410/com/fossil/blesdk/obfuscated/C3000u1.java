package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.u1 */
public final class C3000u1 extends com.fossil.blesdk.obfuscated.C2452n1 implements android.widget.PopupWindow.OnDismissListener, android.widget.AdapterView.OnItemClickListener, com.fossil.blesdk.obfuscated.C2618p1, android.view.View.OnKeyListener {

    @DexIgnore
    /* renamed from: z */
    public static /* final */ int f9807z; // = com.fossil.blesdk.obfuscated.C3250x.abc_popup_menu_item_layout;

    @DexIgnore
    /* renamed from: f */
    public /* final */ android.content.Context f9808f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C1915h1 f9809g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C1852g1 f9810h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ boolean f9811i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ int f9812j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ int f9813k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ int f9814l;

    @DexIgnore
    /* renamed from: m */
    public /* final */ com.fossil.blesdk.obfuscated.C2621p2 f9815m;

    @DexIgnore
    /* renamed from: n */
    public /* final */ android.view.ViewTreeObserver.OnGlobalLayoutListener f9816n; // = new com.fossil.blesdk.obfuscated.C3000u1.C3001a();

    @DexIgnore
    /* renamed from: o */
    public /* final */ android.view.View.OnAttachStateChangeListener f9817o; // = new com.fossil.blesdk.obfuscated.C3000u1.C3002b();

    @DexIgnore
    /* renamed from: p */
    public android.widget.PopupWindow.OnDismissListener f9818p;

    @DexIgnore
    /* renamed from: q */
    public android.view.View f9819q;

    @DexIgnore
    /* renamed from: r */
    public android.view.View f9820r;

    @DexIgnore
    /* renamed from: s */
    public com.fossil.blesdk.obfuscated.C2618p1.C2619a f9821s;

    @DexIgnore
    /* renamed from: t */
    public android.view.ViewTreeObserver f9822t;

    @DexIgnore
    /* renamed from: u */
    public boolean f9823u;

    @DexIgnore
    /* renamed from: v */
    public boolean f9824v;

    @DexIgnore
    /* renamed from: w */
    public int f9825w;

    @DexIgnore
    /* renamed from: x */
    public int f9826x; // = 0;

    @DexIgnore
    /* renamed from: y */
    public boolean f9827y;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.u1$a")
    /* renamed from: com.fossil.blesdk.obfuscated.u1$a */
    public class C3001a implements android.view.ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public C3001a() {
        }

        @DexIgnore
        public void onGlobalLayout() {
            if (com.fossil.blesdk.obfuscated.C3000u1.this.mo862d() && !com.fossil.blesdk.obfuscated.C3000u1.this.f9815m.mo876l()) {
                android.view.View view = com.fossil.blesdk.obfuscated.C3000u1.this.f9820r;
                if (view == null || !view.isShown()) {
                    com.fossil.blesdk.obfuscated.C3000u1.this.dismiss();
                } else {
                    com.fossil.blesdk.obfuscated.C3000u1.this.f9815m.mo724c();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.u1$b")
    /* renamed from: com.fossil.blesdk.obfuscated.u1$b */
    public class C3002b implements android.view.View.OnAttachStateChangeListener {
        @DexIgnore
        public C3002b() {
        }

        @DexIgnore
        public void onViewAttachedToWindow(android.view.View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(android.view.View view) {
            android.view.ViewTreeObserver viewTreeObserver = com.fossil.blesdk.obfuscated.C3000u1.this.f9822t;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    com.fossil.blesdk.obfuscated.C3000u1.this.f9822t = view.getViewTreeObserver();
                }
                com.fossil.blesdk.obfuscated.C3000u1 u1Var = com.fossil.blesdk.obfuscated.C3000u1.this;
                u1Var.f9822t.removeGlobalOnLayoutListener(u1Var.f9816n);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    @DexIgnore
    public C3000u1(android.content.Context context, com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.View view, int i, int i2, boolean z) {
        this.f9808f = context;
        this.f9809g = h1Var;
        this.f9811i = z;
        this.f9810h = new com.fossil.blesdk.obfuscated.C1852g1(h1Var, android.view.LayoutInflater.from(context), this.f9811i, f9807z);
        this.f9813k = i;
        this.f9814l = i2;
        android.content.res.Resources resources = context.getResources();
        this.f9812j = java.lang.Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(com.fossil.blesdk.obfuscated.C2998u.abc_config_prefDialogWidth));
        this.f9819q = view;
        this.f9815m = new com.fossil.blesdk.obfuscated.C2621p2(this.f9808f, (android.util.AttributeSet) null, this.f9813k, this.f9814l);
        h1Var.mo11462a((com.fossil.blesdk.obfuscated.C2618p1) this, context);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10258a(int i) {
        this.f9826x = i;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1159a(android.os.Parcelable parcelable) {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10261a(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo1162a() {
        return false;
    }

    @DexIgnore
    /* renamed from: b */
    public android.os.Parcelable mo1165b() {
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10263b(boolean z) {
        this.f9810h.mo11100a(z);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo724c() {
        if (!mo16632h()) {
            throw new java.lang.IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo862d() {
        return !this.f9823u && this.f9815m.mo862d();
    }

    @DexIgnore
    public void dismiss() {
        if (mo862d()) {
            this.f9815m.dismiss();
        }
    }

    @DexIgnore
    /* renamed from: e */
    public android.widget.ListView mo864e() {
        return this.f9815m.mo864e();
    }

    @DexIgnore
    /* renamed from: h */
    public final boolean mo16632h() {
        if (mo862d()) {
            return true;
        }
        if (!this.f9823u) {
            android.view.View view = this.f9819q;
            if (view != null) {
                this.f9820r = view;
                this.f9815m.mo854a((android.widget.PopupWindow.OnDismissListener) this);
                this.f9815m.mo853a((android.widget.AdapterView.OnItemClickListener) this);
                this.f9815m.mo855a(true);
                android.view.View view2 = this.f9820r;
                boolean z = this.f9822t == null;
                this.f9822t = view2.getViewTreeObserver();
                if (z) {
                    this.f9822t.addOnGlobalLayoutListener(this.f9816n);
                }
                view2.addOnAttachStateChangeListener(this.f9817o);
                this.f9815m.mo852a(view2);
                this.f9815m.mo859c(this.f9826x);
                if (!this.f9824v) {
                    this.f9825w = com.fossil.blesdk.obfuscated.C2452n1.m10959a(this.f9810h, (android.view.ViewGroup) null, this.f9808f, this.f9812j);
                    this.f9824v = true;
                }
                this.f9815m.mo857b(this.f9825w);
                this.f9815m.mo865e(2);
                this.f9815m.mo850a(mo13789g());
                this.f9815m.mo724c();
                android.widget.ListView e = this.f9815m.mo864e();
                e.setOnKeyListener(this);
                if (this.f9827y && this.f9809g.mo11507h() != null) {
                    android.widget.FrameLayout frameLayout = (android.widget.FrameLayout) android.view.LayoutInflater.from(this.f9808f).inflate(com.fossil.blesdk.obfuscated.C3250x.abc_popup_menu_header_item_layout, e, false);
                    android.widget.TextView textView = (android.widget.TextView) frameLayout.findViewById(16908310);
                    if (textView != null) {
                        textView.setText(this.f9809g.mo11507h());
                    }
                    frameLayout.setEnabled(false);
                    e.addHeaderView(frameLayout, (java.lang.Object) null, false);
                }
                this.f9815m.mo721a((android.widget.ListAdapter) this.f9810h);
                this.f9815m.mo724c();
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void onDismiss() {
        this.f9823u = true;
        this.f9809g.close();
        android.view.ViewTreeObserver viewTreeObserver = this.f9822t;
        if (viewTreeObserver != null) {
            if (!viewTreeObserver.isAlive()) {
                this.f9822t = this.f9820r.getViewTreeObserver();
            }
            this.f9822t.removeGlobalOnLayoutListener(this.f9816n);
            this.f9822t = null;
        }
        this.f9820r.removeOnAttachStateChangeListener(this.f9817o);
        android.widget.PopupWindow.OnDismissListener onDismissListener = this.f9818p;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    @DexIgnore
    public boolean onKey(android.view.View view, int i, android.view.KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1161a(boolean z) {
        this.f9824v = false;
        com.fossil.blesdk.obfuscated.C1852g1 g1Var = this.f9810h;
        if (g1Var != null) {
            g1Var.notifyDataSetChanged();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10262b(int i) {
        this.f9815m.mo861d(i);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo10265c(int i) {
        this.f9815m.mo871h(i);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo10266c(boolean z) {
        this.f9827y = z;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9013a(com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar) {
        this.f9821s = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo1164a(com.fossil.blesdk.obfuscated.C3078v1 v1Var) {
        if (v1Var.hasVisibleItems()) {
            com.fossil.blesdk.obfuscated.C2518o1 o1Var = new com.fossil.blesdk.obfuscated.C2518o1(this.f9808f, v1Var, this.f9820r, this.f9811i, this.f9813k, this.f9814l);
            o1Var.mo14236a(this.f9821s);
            o1Var.mo14237a(com.fossil.blesdk.obfuscated.C2452n1.m10961b((com.fossil.blesdk.obfuscated.C1915h1) v1Var));
            o1Var.mo14235a(this.f9818p);
            this.f9818p = null;
            this.f9809g.mo11464a(false);
            int h = this.f9815m.mo870h();
            int i = this.f9815m.mo872i();
            if ((android.view.Gravity.getAbsoluteGravity(this.f9826x, com.fossil.blesdk.obfuscated.C1776f9.m6845k(this.f9819q)) & 7) == 5) {
                h += this.f9819q.getWidth();
            }
            if (o1Var.mo14238a(h, i)) {
                com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar = this.f9821s;
                if (aVar == null) {
                    return true;
                }
                aVar.mo534a(v1Var);
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1160a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, boolean z) {
        if (h1Var == this.f9809g) {
            dismiss();
            com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar = this.f9821s;
            if (aVar != null) {
                aVar.mo533a(h1Var, z);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10259a(android.view.View view) {
        this.f9819q = view;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10260a(android.widget.PopupWindow.OnDismissListener onDismissListener) {
        this.f9818p = onDismissListener;
    }
}
