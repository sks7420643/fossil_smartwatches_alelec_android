package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.io */
public final class C2049io {
    @DexIgnore
    /* renamed from: a */
    public static com.bumptech.glide.load.ImageHeaderParser.ImageType m8572a(java.util.List<com.bumptech.glide.load.ImageHeaderParser> list, java.nio.ByteBuffer byteBuffer) throws java.io.IOException {
        if (byteBuffer == null) {
            return com.bumptech.glide.load.ImageHeaderParser.ImageType.UNKNOWN;
        }
        int size = list.size();
        for (int i = 0; i < size; i++) {
            com.bumptech.glide.load.ImageHeaderParser.ImageType a = list.get(i).mo3940a(byteBuffer);
            if (a != com.bumptech.glide.load.ImageHeaderParser.ImageType.UNKNOWN) {
                return a;
            }
        }
        return com.bumptech.glide.load.ImageHeaderParser.ImageType.UNKNOWN;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* renamed from: b */
    public static com.bumptech.glide.load.ImageHeaderParser.ImageType m8573b(java.util.List<com.bumptech.glide.load.ImageHeaderParser> list, java.io.InputStream inputStream, com.fossil.blesdk.obfuscated.C1885gq gqVar) throws java.io.IOException {
        if (inputStream == null) {
            return com.bumptech.glide.load.ImageHeaderParser.ImageType.UNKNOWN;
        }
        if (!inputStream.markSupported()) {
            inputStream = new com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream(inputStream, gqVar);
        }
        inputStream.mark(5242880);
        int i = 0;
        int size = list.size();
        while (i < size) {
            try {
                com.bumptech.glide.load.ImageHeaderParser.ImageType a = list.get(i).mo3939a(inputStream);
                if (a != com.bumptech.glide.load.ImageHeaderParser.ImageType.UNKNOWN) {
                    inputStream.reset();
                    return a;
                }
                inputStream.reset();
                i++;
            } catch (Throwable th) {
                inputStream.reset();
                throw th;
            }
        }
        return com.bumptech.glide.load.ImageHeaderParser.ImageType.UNKNOWN;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public static int m8571a(java.util.List<com.bumptech.glide.load.ImageHeaderParser> list, java.io.InputStream inputStream, com.fossil.blesdk.obfuscated.C1885gq gqVar) throws java.io.IOException {
        if (inputStream == null) {
            return -1;
        }
        if (!inputStream.markSupported()) {
            inputStream = new com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream(inputStream, gqVar);
        }
        inputStream.mark(5242880);
        int i = 0;
        int size = list.size();
        while (i < size) {
            try {
                int a = list.get(i).mo3938a(inputStream, gqVar);
                if (a != -1) {
                    inputStream.reset();
                    return a;
                }
                inputStream.reset();
                i++;
            } catch (Throwable th) {
                inputStream.reset();
                throw th;
            }
        }
        return -1;
    }
}
