package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ro */
public final class C2818ro extends java.io.OutputStream {

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.io.OutputStream f9057e;

    @DexIgnore
    /* renamed from: f */
    public byte[] f9058f;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C1885gq f9059g;

    @DexIgnore
    /* renamed from: h */
    public int f9060h;

    @DexIgnore
    public C2818ro(java.io.OutputStream outputStream, com.fossil.blesdk.obfuscated.C1885gq gqVar) {
        this(outputStream, gqVar, 65536);
    }

    @DexIgnore
    /* renamed from: A */
    public final void mo15671A() {
        byte[] bArr = this.f9058f;
        if (bArr != null) {
            this.f9059g.put(bArr);
            this.f9058f = null;
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public void close() throws java.io.IOException {
        try {
            flush();
            this.f9057e.close();
            mo15671A();
        } catch (Throwable th) {
            this.f9057e.close();
            throw th;
        }
    }

    @DexIgnore
    public void flush() throws java.io.IOException {
        mo15677y();
        this.f9057e.flush();
    }

    @DexIgnore
    public void write(int i) throws java.io.IOException {
        byte[] bArr = this.f9058f;
        int i2 = this.f9060h;
        this.f9060h = i2 + 1;
        bArr[i2] = (byte) i;
        mo15678z();
    }

    @DexIgnore
    /* renamed from: y */
    public final void mo15677y() throws java.io.IOException {
        int i = this.f9060h;
        if (i > 0) {
            this.f9057e.write(this.f9058f, 0, i);
            this.f9060h = 0;
        }
    }

    @DexIgnore
    /* renamed from: z */
    public final void mo15678z() throws java.io.IOException {
        if (this.f9060h == this.f9058f.length) {
            mo15677y();
        }
    }

    @DexIgnore
    public C2818ro(java.io.OutputStream outputStream, com.fossil.blesdk.obfuscated.C1885gq gqVar, int i) {
        this.f9057e = outputStream;
        this.f9059g = gqVar;
        this.f9058f = (byte[]) gqVar.mo11285b(i, byte[].class);
    }

    @DexIgnore
    public void write(byte[] bArr) throws java.io.IOException {
        write(bArr, 0, bArr.length);
    }

    @DexIgnore
    public void write(byte[] bArr, int i, int i2) throws java.io.IOException {
        int i3 = 0;
        do {
            int i4 = i2 - i3;
            int i5 = i + i3;
            if (this.f9060h != 0 || i4 < this.f9058f.length) {
                int min = java.lang.Math.min(i4, this.f9058f.length - this.f9060h);
                java.lang.System.arraycopy(bArr, i5, this.f9058f, this.f9060h, min);
                this.f9060h += min;
                i3 += min;
                mo15678z();
            } else {
                this.f9057e.write(bArr, i5, i4);
                return;
            }
        } while (i3 < i2);
    }
}
