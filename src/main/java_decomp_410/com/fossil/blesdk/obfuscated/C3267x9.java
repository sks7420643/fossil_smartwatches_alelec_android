package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.x9 */
public class C3267x9 {
    @DexIgnore
    /* renamed from: a */
    public static android.content.res.ColorStateList m16199a(android.widget.ImageView imageView) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return imageView.getImageTintList();
        }
        if (imageView instanceof com.fossil.blesdk.obfuscated.C1606da) {
            return ((com.fossil.blesdk.obfuscated.C1606da) imageView).getSupportImageTintList();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public static android.graphics.PorterDuff.Mode m16202b(android.widget.ImageView imageView) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return imageView.getImageTintMode();
        }
        if (imageView instanceof com.fossil.blesdk.obfuscated.C1606da) {
            return ((com.fossil.blesdk.obfuscated.C1606da) imageView).getSupportImageTintMode();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m16200a(android.widget.ImageView imageView, android.content.res.ColorStateList colorStateList) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            imageView.setImageTintList(colorStateList);
            if (android.os.Build.VERSION.SDK_INT == 21) {
                android.graphics.drawable.Drawable drawable = imageView.getDrawable();
                boolean z = (imageView.getImageTintList() == null || imageView.getImageTintMode() == null) ? false : true;
                if (drawable != null && z) {
                    if (drawable.isStateful()) {
                        drawable.setState(imageView.getDrawableState());
                    }
                    imageView.setImageDrawable(drawable);
                }
            }
        } else if (imageView instanceof com.fossil.blesdk.obfuscated.C1606da) {
            ((com.fossil.blesdk.obfuscated.C1606da) imageView).setSupportImageTintList(colorStateList);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m16201a(android.widget.ImageView imageView, android.graphics.PorterDuff.Mode mode) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            imageView.setImageTintMode(mode);
            if (android.os.Build.VERSION.SDK_INT == 21) {
                android.graphics.drawable.Drawable drawable = imageView.getDrawable();
                boolean z = (imageView.getImageTintList() == null || imageView.getImageTintMode() == null) ? false : true;
                if (drawable != null && z) {
                    if (drawable.isStateful()) {
                        drawable.setState(imageView.getDrawableState());
                    }
                    imageView.setImageDrawable(drawable);
                }
            }
        } else if (imageView instanceof com.fossil.blesdk.obfuscated.C1606da) {
            ((com.fossil.blesdk.obfuscated.C1606da) imageView).setSupportImageTintMode(mode);
        }
    }
}
