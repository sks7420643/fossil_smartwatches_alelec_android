package com.fossil.blesdk.obfuscated;

import android.content.res.AssetManager;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.so;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class qo<T> implements so<T> {
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ AssetManager f;
    @DexIgnore
    public T g;

    @DexIgnore
    public qo(AssetManager assetManager, String str) {
        this.f = assetManager;
        this.e = str;
    }

    @DexIgnore
    public abstract T a(AssetManager assetManager, String str) throws IOException;

    @DexIgnore
    public void a(Priority priority, so.a<? super T> aVar) {
        try {
            this.g = a(this.f, this.e);
            aVar.a(this.g);
        } catch (IOException e2) {
            if (Log.isLoggable("AssetPathFetcher", 3)) {
                Log.d("AssetPathFetcher", "Failed to load data from asset manager", e2);
            }
            aVar.a((Exception) e2);
        }
    }

    @DexIgnore
    public abstract void a(T t) throws IOException;

    @DexIgnore
    public DataSource b() {
        return DataSource.LOCAL;
    }

    @DexIgnore
    public void cancel() {
    }

    @DexIgnore
    public void a() {
        T t = this.g;
        if (t != null) {
            try {
                a(t);
            } catch (IOException unused) {
            }
        }
    }
}
