package com.fossil.blesdk.obfuscated;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zf0 extends zm1 {
    @DexIgnore
    public /* final */ WeakReference<sf0> e;

    @DexIgnore
    public zf0(sf0 sf0) {
        this.e = new WeakReference<>(sf0);
    }

    @DexIgnore
    public final void a(gn1 gn1) {
        sf0 sf0 = (sf0) this.e.get();
        if (sf0 != null) {
            sf0.a.a((og0) new ag0(this, sf0, sf0, gn1));
        }
    }
}
