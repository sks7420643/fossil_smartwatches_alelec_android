package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import com.portfolio.platform.service.notification.HybridMessageNotificationComponent;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ep2 implements MembersInjector<FossilNotificationListenerService> {
    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, DianaNotificationComponent dianaNotificationComponent) {
        fossilNotificationListenerService.l = dianaNotificationComponent;
    }

    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, HybridMessageNotificationComponent hybridMessageNotificationComponent) {
        fossilNotificationListenerService.m = hybridMessageNotificationComponent;
    }

    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, h42 h42) {
        fossilNotificationListenerService.n = h42;
    }

    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, en2 en2) {
        fossilNotificationListenerService.o = en2;
    }
}
