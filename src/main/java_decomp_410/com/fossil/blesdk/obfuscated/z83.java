package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.enums.Unit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface z83 extends v52<y83> {
    @DexIgnore
    void a(qd<ActivitySummary> qdVar);

    @DexIgnore
    void b(Unit unit);

    @DexIgnore
    void f();
}
