package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jo3 {
    @DexIgnore
    public /* final */ BaseActivity a;
    @DexIgnore
    public /* final */ ho3 b;

    @DexIgnore
    public jo3(BaseActivity baseActivity, ho3 ho3) {
        kd4.b(baseActivity, "mContext");
        kd4.b(ho3, "mView");
        this.a = baseActivity;
        this.b = ho3;
    }

    @DexIgnore
    public final BaseActivity a() {
        return this.a;
    }

    @DexIgnore
    public final ho3 b() {
        return this.b;
    }
}
