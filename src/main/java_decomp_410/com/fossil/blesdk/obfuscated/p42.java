package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.helper.AnalyticsHelper;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p42 implements Factory<AnalyticsHelper> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public p42(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static p42 a(n42 n42) {
        return new p42(n42);
    }

    @DexIgnore
    public static AnalyticsHelper b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static AnalyticsHelper c(n42 n42) {
        AnalyticsHelper a2 = n42.a();
        n44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public AnalyticsHelper get() {
        return b(this.a);
    }
}
