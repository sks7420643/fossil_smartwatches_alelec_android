package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.kj0;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ni0 implements bh0 {
    @DexIgnore
    public /* final */ Map<de0.c<?>, mi0<?>> e; // = new HashMap();
    @DexIgnore
    public /* final */ Map<de0.c<?>, mi0<?>> f; // = new HashMap();
    @DexIgnore
    public /* final */ Map<de0<?>, Boolean> g;
    @DexIgnore
    public /* final */ ve0 h;
    @DexIgnore
    public /* final */ eg0 i;
    @DexIgnore
    public /* final */ Lock j;
    @DexIgnore
    public /* final */ Looper k;
    @DexIgnore
    public /* final */ yd0 l;
    @DexIgnore
    public /* final */ Condition m;
    @DexIgnore
    public /* final */ kj0 n;
    @DexIgnore
    public /* final */ boolean o;
    @DexIgnore
    public /* final */ boolean p;
    @DexIgnore
    public /* final */ Queue<te0<?, ?>> q; // = new LinkedList();
    @DexIgnore
    public boolean r;
    @DexIgnore
    public Map<yh0<?>, ud0> s;
    @DexIgnore
    public Map<yh0<?>, ud0> t;
    @DexIgnore
    public if0 u;
    @DexIgnore
    public ud0 v;

    @DexIgnore
    public ni0(Context context, Lock lock, Looper looper, yd0 yd0, Map<de0.c<?>, de0.f> map, kj0 kj0, Map<de0<?>, Boolean> map2, de0.a<? extends ln1, vm1> aVar, ArrayList<gi0> arrayList, eg0 eg0, boolean z) {
        boolean z2;
        boolean z3;
        boolean z4;
        this.j = lock;
        this.k = looper;
        this.m = lock.newCondition();
        this.l = yd0;
        this.i = eg0;
        this.g = map2;
        this.n = kj0;
        this.o = z;
        HashMap hashMap = new HashMap();
        for (de0 next : map2.keySet()) {
            hashMap.put(next.a(), next);
        }
        HashMap hashMap2 = new HashMap();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            gi0 gi0 = arrayList.get(i2);
            i2++;
            gi0 gi02 = gi0;
            hashMap2.put(gi02.e, gi02);
        }
        boolean z5 = true;
        boolean z6 = false;
        boolean z7 = true;
        boolean z8 = false;
        for (Map.Entry next2 : map.entrySet()) {
            de0 de0 = (de0) hashMap.get(next2.getKey());
            de0.f fVar = (de0.f) next2.getValue();
            if (fVar.h()) {
                z3 = z7;
                z4 = !this.g.get(de0).booleanValue() ? true : z8;
                z2 = true;
            } else {
                z2 = z6;
                z4 = z8;
                z3 = false;
            }
            mi0 mi0 = r1;
            mi0 mi02 = new mi0(context, de0, looper, fVar, (gi0) hashMap2.get(de0), kj0, aVar);
            this.e.put((de0.c) next2.getKey(), mi0);
            if (fVar.l()) {
                this.f.put((de0.c) next2.getKey(), mi0);
            }
            z8 = z4;
            z7 = z3;
            z6 = z2;
        }
        this.p = (!z6 || z7 || z8) ? false : z5;
        this.h = ve0.e();
    }

    @DexIgnore
    public final <A extends de0.b, T extends te0<? extends me0, A>> T a(T t2) {
        de0.c i2 = t2.i();
        if (this.o && c(t2)) {
            return t2;
        }
        this.i.y.a(t2);
        this.e.get(i2).c(t2);
        return t2;
    }

    @DexIgnore
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    @DexIgnore
    public final <A extends de0.b, R extends me0, T extends te0<R, A>> T b(T t2) {
        if (this.o && c(t2)) {
            return t2;
        }
        if (!c()) {
            this.q.add(t2);
            return t2;
        }
        this.i.y.a(t2);
        this.e.get(t2.i()).b(t2);
        return t2;
    }

    @DexIgnore
    public final <T extends te0<? extends me0, ? extends de0.b>> boolean c(T t2) {
        de0.c i2 = t2.i();
        ud0 a = a((de0.c<?>) i2);
        if (a == null || a.H() != 4) {
            return false;
        }
        t2.c(new Status(4, (String) null, this.h.a((yh0<?>) this.e.get(i2).h(), System.identityHashCode(this.i))));
        return true;
    }

    @DexIgnore
    public final void d() {
    }

    @DexIgnore
    public final void e() {
        this.j.lock();
        try {
            this.h.a();
            if (this.u != null) {
                this.u.a();
                this.u = null;
            }
            if (this.t == null) {
                this.t = new g4(this.f.size());
            }
            ud0 ud0 = new ud0(4);
            for (mi0<?> h2 : this.f.values()) {
                this.t.put(h2.h(), ud0);
            }
            if (this.s != null) {
                this.s.putAll(this.t);
            }
        } finally {
            this.j.unlock();
        }
    }

    @DexIgnore
    public final ud0 f() {
        b();
        while (g()) {
            try {
                this.m.await();
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
                return new ud0(15, (PendingIntent) null);
            }
        }
        if (c()) {
            return ud0.i;
        }
        ud0 ud0 = this.v;
        if (ud0 != null) {
            return ud0;
        }
        return new ud0(13, (PendingIntent) null);
    }

    @DexIgnore
    public final boolean g() {
        this.j.lock();
        try {
            return this.s == null && this.r;
        } finally {
            this.j.unlock();
        }
    }

    @DexIgnore
    public final boolean h() {
        this.j.lock();
        try {
            if (this.r) {
                if (this.o) {
                    for (de0.c<?> a : this.f.keySet()) {
                        ud0 a2 = a(a);
                        if (a2 != null) {
                            if (!a2.L()) {
                            }
                        }
                        this.j.unlock();
                        return false;
                    }
                    this.j.unlock();
                    return true;
                }
            }
            return false;
        } finally {
            this.j.unlock();
        }
    }

    @DexIgnore
    public final void i() {
        kj0 kj0 = this.n;
        if (kj0 == null) {
            this.i.q = Collections.emptySet();
            return;
        }
        HashSet hashSet = new HashSet(kj0.i());
        Map<de0<?>, kj0.b> f2 = this.n.f();
        for (de0 next : f2.keySet()) {
            ud0 a = a((de0<?>) next);
            if (a != null && a.L()) {
                hashSet.addAll(f2.get(next).a);
            }
        }
        this.i.q = hashSet;
    }

    @DexIgnore
    public final void j() {
        while (!this.q.isEmpty()) {
            a(this.q.remove());
        }
        this.i.a((Bundle) null);
    }

    @DexIgnore
    public final ud0 k() {
        ud0 ud0 = null;
        ud0 ud02 = null;
        int i2 = 0;
        int i3 = 0;
        for (mi0 next : this.e.values()) {
            de0 c = next.c();
            ud0 ud03 = this.s.get(next.h());
            if (!ud03.L() && (!this.g.get(c).booleanValue() || ud03.K() || this.l.c(ud03.H()))) {
                if (ud03.H() != 4 || !this.o) {
                    int a = c.c().a();
                    if (ud0 == null || i2 > a) {
                        ud0 = ud03;
                        i2 = a;
                    }
                } else {
                    int a2 = c.c().a();
                    if (ud02 == null || i3 > a2) {
                        ud02 = ud03;
                        i3 = a2;
                    }
                }
            }
        }
        return (ud0 == null || ud02 == null || i2 <= i3) ? ud0 : ud02;
    }

    @DexIgnore
    public final void a() {
        this.j.lock();
        try {
            this.r = false;
            this.s = null;
            this.t = null;
            if (this.u != null) {
                this.u.a();
                this.u = null;
            }
            this.v = null;
            while (!this.q.isEmpty()) {
                te0 remove = this.q.remove();
                remove.a((sh0) null);
                remove.a();
            }
            this.m.signalAll();
        } finally {
            this.j.unlock();
        }
    }

    @DexIgnore
    public final void b() {
        this.j.lock();
        try {
            if (!this.r) {
                this.r = true;
                this.s = null;
                this.t = null;
                this.u = null;
                this.v = null;
                this.h.c();
                this.h.a((Iterable<? extends fe0<?>>) this.e.values()).a((Executor) new tm0(this.k), new pi0(this));
                this.j.unlock();
            }
        } finally {
            this.j.unlock();
        }
    }

    @DexIgnore
    public final boolean c() {
        this.j.lock();
        try {
            return this.s != null && this.v == null;
        } finally {
            this.j.unlock();
        }
    }

    @DexIgnore
    public final ud0 a(de0<?> de0) {
        return a(de0.a());
    }

    @DexIgnore
    public final ud0 a(de0.c<?> cVar) {
        this.j.lock();
        try {
            mi0 mi0 = this.e.get(cVar);
            if (this.s != null && mi0 != null) {
                return this.s.get(mi0.h());
            }
            this.j.unlock();
            return null;
        } finally {
            this.j.unlock();
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final boolean a(cf0 cf0) {
        this.j.lock();
        try {
            if (!this.r || h()) {
                this.j.unlock();
                return false;
            }
            this.h.c();
            this.u = new if0(this, cf0);
            this.h.a((Iterable<? extends fe0<?>>) this.f.values()).a((Executor) new tm0(this.k), this.u);
            this.j.unlock();
            return true;
        } catch (Throwable th) {
            this.j.unlock();
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(mi0<?> mi0, ud0 ud0) {
        return !ud0.L() && !ud0.K() && this.g.get(mi0.c()).booleanValue() && mi0.i().h() && this.l.c(ud0.H());
    }
}
