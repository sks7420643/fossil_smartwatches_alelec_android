package com.fossil.blesdk.obfuscated;

import com.crashlytics.android.core.Report;
import com.misfit.frameworks.buttonservice.log.FileLogWriter;
import com.misfit.frameworks.common.constants.Constants;
import com.zendesk.sdk.attachment.AttachmentHelper;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class oz extends e54 implements cz {
    @DexIgnore
    public oz(v44 v44, String str, String str2, z64 z64) {
        super(v44, str, str2, z64, HttpMethod.POST);
    }

    @DexIgnore
    public boolean a(bz bzVar) {
        HttpRequest a = a();
        a(a, bzVar.a);
        a(a, bzVar.b);
        y44 g = q44.g();
        g.d("CrashlyticsCore", "Sending report to: " + b());
        int g2 = a.g();
        y44 g3 = q44.g();
        g3.d("CrashlyticsCore", "Result was: " + g2);
        return w54.a(g2) == 0;
    }

    @DexIgnore
    public final HttpRequest a(HttpRequest httpRequest, String str) {
        httpRequest.c("User-Agent", "Crashlytics Android SDK/" + this.e.r());
        httpRequest.c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        httpRequest.c("X-CRASHLYTICS-API-CLIENT-VERSION", this.e.r());
        httpRequest.c("X-CRASHLYTICS-API-KEY", str);
        return httpRequest;
    }

    @DexIgnore
    public final HttpRequest a(HttpRequest httpRequest, Report report) {
        httpRequest.e("report_id", report.b());
        for (File file : report.d()) {
            if (file.getName().equals("minidump")) {
                httpRequest.a("minidump_file", file.getName(), AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals("metadata")) {
                httpRequest.a("crash_meta_file", file.getName(), AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals("binaryImages")) {
                httpRequest.a("binary_images_file", file.getName(), AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals(Constants.SESSION)) {
                httpRequest.a("session_meta_file", file.getName(), AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals("app")) {
                httpRequest.a("app_meta_file", file.getName(), AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals("device")) {
                httpRequest.a("device_meta_file", file.getName(), AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals("os")) {
                httpRequest.a("os_meta_file", file.getName(), AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals("user")) {
                httpRequest.a("user_meta_file", file.getName(), AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals(FileLogWriter.LOG_FOLDER)) {
                httpRequest.a("logs_file", file.getName(), AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals("keys")) {
                httpRequest.a("keys_file", file.getName(), AttachmentHelper.DEFAULT_MIMETYPE, file);
            }
        }
        return httpRequest;
    }
}
