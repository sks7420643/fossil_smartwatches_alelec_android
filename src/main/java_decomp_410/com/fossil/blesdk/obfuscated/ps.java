package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ps implements aq<Bitmap>, wp {
    @DexIgnore
    public /* final */ Bitmap e;
    @DexIgnore
    public /* final */ jq f;

    @DexIgnore
    public ps(Bitmap bitmap, jq jqVar) {
        tw.a(bitmap, "Bitmap must not be null");
        this.e = bitmap;
        tw.a(jqVar, "BitmapPool must not be null");
        this.f = jqVar;
    }

    @DexIgnore
    public static ps a(Bitmap bitmap, jq jqVar) {
        if (bitmap == null) {
            return null;
        }
        return new ps(bitmap, jqVar);
    }

    @DexIgnore
    public int b() {
        return uw.a(this.e);
    }

    @DexIgnore
    public Class<Bitmap> c() {
        return Bitmap.class;
    }

    @DexIgnore
    public void d() {
        this.e.prepareToDraw();
    }

    @DexIgnore
    public void a() {
        this.f.a(this.e);
    }

    @DexIgnore
    public Bitmap get() {
        return this.e;
    }
}
