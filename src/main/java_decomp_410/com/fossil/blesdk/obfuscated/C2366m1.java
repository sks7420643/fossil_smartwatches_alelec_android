package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.m1 */
public class C2366m1 extends com.fossil.blesdk.obfuscated.C2274l1 {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.m1$a")
    /* renamed from: com.fossil.blesdk.obfuscated.m1$a */
    public class C2367a extends com.fossil.blesdk.obfuscated.C2274l1.C2275a implements android.view.ActionProvider.VisibilityListener {

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C2382m8.C2384b f7382c;

        @DexIgnore
        public C2367a(com.fossil.blesdk.obfuscated.C2366m1 m1Var, android.content.Context context, android.view.ActionProvider actionProvider) {
            super(context, actionProvider);
        }

        @DexIgnore
        public boolean isVisible() {
            return this.f7088a.isVisible();
        }

        @DexIgnore
        public void onActionProviderVisibilityChanged(boolean z) {
            com.fossil.blesdk.obfuscated.C2382m8.C2384b bVar = this.f7382c;
            if (bVar != null) {
                bVar.onActionProviderVisibilityChanged(z);
            }
        }

        @DexIgnore
        public android.view.View onCreateActionView(android.view.MenuItem menuItem) {
            return this.f7088a.onCreateActionView(menuItem);
        }

        @DexIgnore
        public boolean overridesItemVisibility() {
            return this.f7088a.overridesItemVisibility();
        }

        @DexIgnore
        public void refreshVisibility() {
            this.f7088a.refreshVisibility();
        }

        @DexIgnore
        public void setVisibilityListener(com.fossil.blesdk.obfuscated.C2382m8.C2384b bVar) {
            this.f7382c = bVar;
            this.f7088a.setVisibilityListener(bVar != null ? this : null);
        }
    }

    @DexIgnore
    public C2366m1(android.content.Context context, com.fossil.blesdk.obfuscated.C2001i7 i7Var) {
        super(context, i7Var);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2274l1.C2275a mo13018a(android.view.ActionProvider actionProvider) {
        return new com.fossil.blesdk.obfuscated.C2366m1.C2367a(this, this.f3952b, actionProvider);
    }
}
