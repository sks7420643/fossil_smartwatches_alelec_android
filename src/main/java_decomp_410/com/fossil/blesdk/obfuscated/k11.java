package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k11 implements qo0 {
    @DexIgnore
    public final he0<Status> a(ge0 ge0) {
        return ge0.b(new l11(this, ge0));
    }
}
