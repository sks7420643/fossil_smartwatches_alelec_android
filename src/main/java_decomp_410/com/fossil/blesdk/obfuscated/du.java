package com.fossil.blesdk.obfuscated;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class du implements gu<Bitmap, BitmapDrawable> {
    @DexIgnore
    public /* final */ Resources a;

    @DexIgnore
    public du(Resources resources) {
        tw.a(resources);
        this.a = resources;
    }

    @DexIgnore
    public aq<BitmapDrawable> a(aq<Bitmap> aqVar, lo loVar) {
        return dt.a(this.a, aqVar);
    }
}
