package com.fossil.blesdk.obfuscated;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d91<K> implements Map.Entry<K, Object> {
    @DexIgnore
    public Map.Entry<K, a91> e;

    @DexIgnore
    public d91(Map.Entry<K, a91> entry) {
        this.e = entry;
    }

    @DexIgnore
    public final a91 a() {
        return this.e.getValue();
    }

    @DexIgnore
    public final K getKey() {
        return this.e.getKey();
    }

    @DexIgnore
    public final Object getValue() {
        if (this.e.getValue() == null) {
            return null;
        }
        a91.c();
        throw null;
    }

    @DexIgnore
    public final Object setValue(Object obj) {
        if (obj instanceof w91) {
            return this.e.getValue().b((w91) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
