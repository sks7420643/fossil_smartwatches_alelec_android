package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qq */
public interface C2762qq {
    @DexIgnore
    /* renamed from: a */
    android.graphics.Bitmap mo11706a();

    @DexIgnore
    /* renamed from: a */
    android.graphics.Bitmap mo11707a(int i, int i2, android.graphics.Bitmap.Config config);

    @DexIgnore
    /* renamed from: a */
    void mo11708a(android.graphics.Bitmap bitmap);

    @DexIgnore
    /* renamed from: b */
    int mo11709b(android.graphics.Bitmap bitmap);

    @DexIgnore
    /* renamed from: b */
    java.lang.String mo11710b(int i, int i2, android.graphics.Bitmap.Config config);

    @DexIgnore
    /* renamed from: c */
    java.lang.String mo11711c(android.graphics.Bitmap bitmap);
}
