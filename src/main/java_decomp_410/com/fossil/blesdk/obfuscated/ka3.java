package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ka3 {
    @DexIgnore
    public /* final */ fa3 a;
    @DexIgnore
    public /* final */ ua3 b;
    @DexIgnore
    public /* final */ pa3 c;

    @DexIgnore
    public ka3(fa3 fa3, ua3 ua3, pa3 pa3) {
        kd4.b(fa3, "mCaloriesOverviewDayView");
        kd4.b(ua3, "mCaloriesOverviewWeekView");
        kd4.b(pa3, "mCaloriesOverviewMonthView");
        this.a = fa3;
        this.b = ua3;
        this.c = pa3;
    }

    @DexIgnore
    public final fa3 a() {
        return this.a;
    }

    @DexIgnore
    public final pa3 b() {
        return this.c;
    }

    @DexIgnore
    public final ua3 c() {
        return this.b;
    }
}
