package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.kb */
public abstract class C2195kb implements android.view.animation.Interpolator {

    @DexIgnore
    /* renamed from: a */
    public /* final */ float[] f6755a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ float f6756b; // = (1.0f / ((float) (this.f6755a.length - 1)));

    @DexIgnore
    public C2195kb(float[] fArr) {
        this.f6755a = fArr;
    }

    @DexIgnore
    public float getInterpolation(float f) {
        if (f >= 1.0f) {
            return 1.0f;
        }
        if (f <= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        float[] fArr = this.f6755a;
        int min = java.lang.Math.min((int) (((float) (fArr.length - 1)) * f), fArr.length - 2);
        float f2 = this.f6756b;
        float f3 = (f - (((float) min) * f2)) / f2;
        float[] fArr2 = this.f6755a;
        return fArr2[min] + (f3 * (fArr2[min + 1] - fArr2[min]));
    }
}
