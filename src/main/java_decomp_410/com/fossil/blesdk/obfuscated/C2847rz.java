package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rz */
public interface C2847rz {
    @DexIgnore
    /* renamed from: a */
    java.lang.String mo15792a();

    @DexIgnore
    /* renamed from: b */
    java.io.InputStream mo15793b();

    @DexIgnore
    /* renamed from: c */
    java.lang.String[] mo15794c();
}
