package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ViewConfiguration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class s0 {
    @DexIgnore
    public Context a;

    @DexIgnore
    public s0(Context context) {
        this.a = context;
    }

    @DexIgnore
    public static s0 a(Context context) {
        return new s0(context);
    }

    @DexIgnore
    public int b() {
        return this.a.getResources().getDisplayMetrics().widthPixels / 2;
    }

    @DexIgnore
    public int c() {
        Configuration configuration = this.a.getResources().getConfiguration();
        int i = configuration.screenWidthDp;
        int i2 = configuration.screenHeightDp;
        if (configuration.smallestScreenWidthDp > 600 || i > 600) {
            return 5;
        }
        if (i > 960 && i2 > 720) {
            return 5;
        }
        if (i > 720 && i2 > 960) {
            return 5;
        }
        if (i >= 500) {
            return 4;
        }
        if (i > 640 && i2 > 480) {
            return 4;
        }
        if (i <= 480 || i2 <= 640) {
            return i >= 360 ? 3 : 2;
        }
        return 4;
    }

    @DexIgnore
    public int d() {
        return this.a.getResources().getDimensionPixelSize(u.abc_action_bar_stacked_tab_max_width);
    }

    @DexIgnore
    public int e() {
        TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes((AttributeSet) null, a0.ActionBar, r.actionBarStyle, 0);
        int layoutDimension = obtainStyledAttributes.getLayoutDimension(a0.ActionBar_height, 0);
        Resources resources = this.a.getResources();
        if (!f()) {
            layoutDimension = Math.min(layoutDimension, resources.getDimensionPixelSize(u.abc_action_bar_stacked_max_height));
        }
        obtainStyledAttributes.recycle();
        return layoutDimension;
    }

    @DexIgnore
    public boolean f() {
        return this.a.getResources().getBoolean(s.abc_action_bar_embed_tabs);
    }

    @DexIgnore
    public boolean g() {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return !ViewConfiguration.get(this.a).hasPermanentMenuKey();
    }

    @DexIgnore
    public boolean a() {
        return this.a.getApplicationInfo().targetSdkVersion < 14;
    }
}
