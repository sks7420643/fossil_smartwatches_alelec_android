package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ob0 {
    @DexIgnore
    public SharedPreferences a;

    @DexIgnore
    public ob0(Context context) {
        try {
            Context remoteContext = zd0.getRemoteContext(context);
            this.a = remoteContext == null ? null : remoteContext.getSharedPreferences("google_ads_flags", 0);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while getting SharedPreferences ", th);
            this.a = null;
        }
    }

    @DexIgnore
    public final float a(String str, float f) {
        try {
            return this.a == null ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : this.a.getFloat(str, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th);
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
    }

    @DexIgnore
    public final String a(String str, String str2) {
        try {
            return this.a == null ? str2 : this.a.getString(str, str2);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th);
            return str2;
        }
    }

    @DexIgnore
    public final boolean a(String str, boolean z) {
        try {
            if (this.a == null) {
                return false;
            }
            return this.a.getBoolean(str, false);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th);
            return false;
        }
    }
}
