package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class w34 extends Application implements c44, f44, g44, d44, e44 {
    @DexIgnore
    public b44<Activity> e;
    @DexIgnore
    public b44<BroadcastReceiver> f;
    @DexIgnore
    public b44<Service> g;
    @DexIgnore
    public b44<ContentProvider> h;
    @DexIgnore
    public volatile boolean i; // = true;

    @DexIgnore
    public u34<ContentProvider> c() {
        f();
        return this.h;
    }

    @DexIgnore
    public abstract u34<? extends w34> e();

    @DexIgnore
    public final void f() {
        if (this.i) {
            synchronized (this) {
                if (this.i) {
                    e().a(this);
                    if (this.i) {
                        throw new IllegalStateException("The AndroidInjector returned from applicationInjector() did not inject the DaggerApplication");
                    }
                }
            }
        }
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        f();
    }

    @DexIgnore
    public b44<BroadcastReceiver> a() {
        return this.f;
    }

    @DexIgnore
    public b44<Service> b() {
        return this.g;
    }

    @DexIgnore
    public b44<Activity> d() {
        return this.e;
    }
}
