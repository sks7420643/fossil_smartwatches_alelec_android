package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ld;
import com.fossil.blesdk.obfuscated.md;
import java.util.IdentityHashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xd<K, A, B> extends md<K, B> {
    @DexIgnore
    public /* final */ md<K, A> a;
    @DexIgnore
    public /* final */ m3<List<A>, List<B>> b;
    @DexIgnore
    public /* final */ IdentityHashMap<B, K> c; // = new IdentityHashMap<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends md.c<A> {
        @DexIgnore
        public /* final */ /* synthetic */ md.c a;

        @DexIgnore
        public a(md.c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public void a(List<A> list) {
            this.a.a(xd.this.a(list));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends md.a<A> {
        @DexIgnore
        public /* final */ /* synthetic */ md.a a;

        @DexIgnore
        public b(md.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public void a(List<A> list) {
            this.a.a(xd.this.a(list));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends md.a<A> {
        @DexIgnore
        public /* final */ /* synthetic */ md.a a;

        @DexIgnore
        public c(md.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public void a(List<A> list) {
            this.a.a(xd.this.a(list));
        }
    }

    @DexIgnore
    public xd(md<K, A> mdVar, m3<List<A>, List<B>> m3Var) {
        this.a = mdVar;
        this.b = m3Var;
    }

    @DexIgnore
    public List<B> a(List<A> list) {
        List<B> convert = ld.convert(this.b, list);
        synchronized (this.c) {
            for (int i = 0; i < convert.size(); i++) {
                this.c.put(convert.get(i), this.a.getKey(list.get(i)));
            }
        }
        return convert;
    }

    @DexIgnore
    public void addInvalidatedCallback(ld.c cVar) {
        this.a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    public K getKey(B b2) {
        K k;
        synchronized (this.c) {
            k = this.c.get(b2);
        }
        return k;
    }

    @DexIgnore
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    public void loadAfter(md.f<K> fVar, md.a<B> aVar) {
        this.a.loadAfter(fVar, new b(aVar));
    }

    @DexIgnore
    public void loadBefore(md.f<K> fVar, md.a<B> aVar) {
        this.a.loadBefore(fVar, new c(aVar));
    }

    @DexIgnore
    public void loadInitial(md.e<K> eVar, md.c<B> cVar) {
        this.a.loadInitial(eVar, new a(cVar));
    }

    @DexIgnore
    public void removeInvalidatedCallback(ld.c cVar) {
        this.a.removeInvalidatedCallback(cVar);
    }
}
