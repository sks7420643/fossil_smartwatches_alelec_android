package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import kotlin.Result;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w52 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.e<P, E> {
        @DexIgnore
        public /* final */ /* synthetic */ dg4 a;
        @DexIgnore
        public /* final */ /* synthetic */ CoroutineUseCase b;

        @DexIgnore
        public a(dg4 dg4, CoroutineUseCase coroutineUseCase, CoroutineUseCase.b bVar) {
            this.a = dg4;
            this.b = coroutineUseCase;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(P p) {
            kd4.b(p, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String c = this.b.c();
            local.d(c, "success continuation " + this.a.hashCode());
            if (this.a.isActive()) {
                dg4 dg4 = this.a;
                Result.a aVar = Result.Companion;
                dg4.resumeWith(Result.m3constructorimpl(p));
            }
        }

        @DexIgnore
        public void a(E e) {
            kd4.b(e, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String c = this.b.c();
            local.d(c, "fail continuation " + this.a.hashCode());
            if (this.a.isActive()) {
                dg4 dg4 = this.a;
                Result.a aVar = Result.Companion;
                dg4.resumeWith(Result.m3constructorimpl(e));
            }
        }
    }

    @DexIgnore
    public static final <R extends CoroutineUseCase.b, P extends CoroutineUseCase.d, E extends CoroutineUseCase.a> Object a(CoroutineUseCase<? super R, P, E> coroutineUseCase, R r, yb4<? super CoroutineUseCase.c> yb4) {
        eg4 eg4 = new eg4(IntrinsicsKt__IntrinsicsJvmKt.a(yb4), 1);
        coroutineUseCase.a(r, (CoroutineUseCase.e<? super P, ? super E>) new a(eg4, coroutineUseCase, r));
        Object e = eg4.e();
        if (e == cc4.a()) {
            ic4.c(yb4);
        }
        return e;
    }
}
