package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.calibration.HandMovingConfig;
import com.fossil.blesdk.device.data.calibration.HandMovingType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface v30 {
    @DexIgnore
    g90<qa4> a(HandMovingType handMovingType, HandMovingConfig[] handMovingConfigArr);
}
