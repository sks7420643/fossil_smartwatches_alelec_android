package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ps */
public class C2675ps implements com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.C3233wp {

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.graphics.Bitmap f8444e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C2149jq f8445f;

    @DexIgnore
    public C2675ps(android.graphics.Bitmap bitmap, com.fossil.blesdk.obfuscated.C2149jq jqVar) {
        com.fossil.blesdk.obfuscated.C2992tw.m14458a(bitmap, "Bitmap must not be null");
        this.f8444e = bitmap;
        com.fossil.blesdk.obfuscated.C2992tw.m14458a(jqVar, "BitmapPool must not be null");
        this.f8445f = jqVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2675ps m12395a(android.graphics.Bitmap bitmap, com.fossil.blesdk.obfuscated.C2149jq jqVar) {
        if (bitmap == null) {
            return null;
        }
        return new com.fossil.blesdk.obfuscated.C2675ps(bitmap, jqVar);
    }

    @DexIgnore
    /* renamed from: b */
    public int mo8888b() {
        return com.fossil.blesdk.obfuscated.C3066uw.m14922a(this.f8444e);
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.Class<android.graphics.Bitmap> mo8889c() {
        return android.graphics.Bitmap.class;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo10116d() {
        this.f8444e.prepareToDraw();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8887a() {
        this.f8445f.mo12446a(this.f8444e);
    }

    @DexIgnore
    public android.graphics.Bitmap get() {
        return this.f8444e;
    }
}
