package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xi */
public final class C3290xi {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.concurrent.Executor f10971a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.concurrent.Executor f10972b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C2329lj f10973c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C1558cj f10974d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ int f10975e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ int f10976f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ int f10977g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ int f10978h;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xi$a")
    /* renamed from: com.fossil.blesdk.obfuscated.xi$a */
    public static final class C3291a {

        @DexIgnore
        /* renamed from: a */
        public java.util.concurrent.Executor f10979a;

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C2329lj f10980b;

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C1558cj f10981c;

        @DexIgnore
        /* renamed from: d */
        public java.util.concurrent.Executor f10982d;

        @DexIgnore
        /* renamed from: e */
        public int f10983e; // = 4;

        @DexIgnore
        /* renamed from: f */
        public int f10984f; // = 0;

        @DexIgnore
        /* renamed from: g */
        public int f10985g; // = Integer.MAX_VALUE;

        @DexIgnore
        /* renamed from: h */
        public int f10986h; // = 20;

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3290xi.C3291a mo17754a(com.fossil.blesdk.obfuscated.C2329lj ljVar) {
            this.f10980b = ljVar;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3290xi mo17755a() {
            return new com.fossil.blesdk.obfuscated.C3290xi(this);
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.xi$b */
    public interface C3292b {
        @DexIgnore
        /* renamed from: a */
        com.fossil.blesdk.obfuscated.C3290xi mo17756a();
    }

    @DexIgnore
    public C3290xi(com.fossil.blesdk.obfuscated.C3290xi.C3291a aVar) {
        java.util.concurrent.Executor executor = aVar.f10979a;
        if (executor == null) {
            this.f10971a = mo17745a();
        } else {
            this.f10971a = executor;
        }
        java.util.concurrent.Executor executor2 = aVar.f10982d;
        if (executor2 == null) {
            this.f10972b = mo17745a();
        } else {
            this.f10972b = executor2;
        }
        com.fossil.blesdk.obfuscated.C2329lj ljVar = aVar.f10980b;
        if (ljVar == null) {
            this.f10973c = com.fossil.blesdk.obfuscated.C2329lj.m10192a();
        } else {
            this.f10973c = ljVar;
        }
        com.fossil.blesdk.obfuscated.C1558cj cjVar = aVar.f10981c;
        if (cjVar == null) {
            this.f10974d = com.fossil.blesdk.obfuscated.C1558cj.m5428a();
        } else {
            this.f10974d = cjVar;
        }
        this.f10975e = aVar.f10983e;
        this.f10976f = aVar.f10984f;
        this.f10977g = aVar.f10985g;
        this.f10978h = aVar.f10986h;
    }

    @DexIgnore
    /* renamed from: a */
    public final java.util.concurrent.Executor mo17745a() {
        return java.util.concurrent.Executors.newFixedThreadPool(java.lang.Math.max(2, java.lang.Math.min(java.lang.Runtime.getRuntime().availableProcessors() - 1, 4)));
    }

    @DexIgnore
    /* renamed from: b */
    public java.util.concurrent.Executor mo17746b() {
        return this.f10971a;
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C1558cj mo17747c() {
        return this.f10974d;
    }

    @DexIgnore
    /* renamed from: d */
    public int mo17748d() {
        return this.f10977g;
    }

    @DexIgnore
    /* renamed from: e */
    public int mo17749e() {
        if (android.os.Build.VERSION.SDK_INT == 23) {
            return this.f10978h / 2;
        }
        return this.f10978h;
    }

    @DexIgnore
    /* renamed from: f */
    public int mo17750f() {
        return this.f10976f;
    }

    @DexIgnore
    /* renamed from: g */
    public int mo17751g() {
        return this.f10975e;
    }

    @DexIgnore
    /* renamed from: h */
    public java.util.concurrent.Executor mo17752h() {
        return this.f10972b;
    }

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C2329lj mo17753i() {
        return this.f10973c;
    }
}
