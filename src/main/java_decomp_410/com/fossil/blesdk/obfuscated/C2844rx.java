package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rx */
public class C2844rx {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f9148a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2993tx f9149b;

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2773qx f9150c;

    @DexIgnore
    public C2844rx(android.content.Context context) {
        this(context, new com.fossil.blesdk.obfuscated.C2993tx());
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2773qx mo15784a() {
        if (this.f9150c == null) {
            this.f9150c = com.fossil.blesdk.obfuscated.C2258kx.m9879b(this.f9148a);
        }
        return this.f9150c;
    }

    @DexIgnore
    public C2844rx(android.content.Context context, com.fossil.blesdk.obfuscated.C2993tx txVar) {
        this.f9148a = context;
        this.f9149b = txVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15785a(com.crashlytics.android.answers.SessionEvent sessionEvent) {
        com.fossil.blesdk.obfuscated.C2773qx a = mo15784a();
        if (a == null) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("Answers", "Firebase analytics logging was enabled, but not available...");
            return;
        }
        com.fossil.blesdk.obfuscated.C2926sx a2 = this.f9149b.mo16586a(sessionEvent);
        if (a2 == null) {
            com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
            g.mo30060d("Answers", "Fabric event was not mappable to Firebase event: " + sessionEvent);
            return;
        }
        a.mo12972a(a2.mo16215a(), a2.mo16216b());
        if ("levelEnd".equals(sessionEvent.f2170g)) {
            a.mo12972a("post_score", a2.mo16216b());
        }
    }
}
