package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@TargetApi(14)
public final class nj1 implements Application.ActivityLifecycleCallbacks {
    @DexIgnore
    public /* final */ /* synthetic */ dj1 e;

    @DexIgnore
    public nj1(dj1 dj1) {
        this.e = dj1;
    }

    @DexIgnore
    public final void onActivityCreated(Activity activity, Bundle bundle) {
        String str;
        try {
            this.e.d().A().a("onActivityCreated");
            Intent intent = activity.getIntent();
            if (intent != null) {
                Uri data = intent.getData();
                if (data != null && data.isHierarchical()) {
                    if (bundle == null) {
                        Bundle a = this.e.j().a(data);
                        this.e.j();
                        if (nl1.a(intent)) {
                            str = "gs";
                        } else {
                            str = "auto";
                        }
                        if (a != null) {
                            this.e.b(str, "_cmp", a);
                        }
                    }
                    String queryParameter = data.getQueryParameter("referrer");
                    if (!TextUtils.isEmpty(queryParameter)) {
                        if (!(queryParameter.contains("gclid") && (queryParameter.contains("utm_campaign") || queryParameter.contains("utm_source") || queryParameter.contains("utm_medium") || queryParameter.contains("utm_term") || queryParameter.contains("utm_content")))) {
                            this.e.d().z().a("Activity created with data 'referrer' param without gclid and at least one utm field");
                            return;
                        }
                        this.e.d().z().a("Activity created with referrer", queryParameter);
                        if (!TextUtils.isEmpty(queryParameter)) {
                            this.e.a("auto", "_ldl", (Object) queryParameter, true);
                        }
                    } else {
                        return;
                    }
                }
            }
        } catch (Exception e2) {
            this.e.d().s().a("Throwable caught in onActivityCreated", e2);
        }
        this.e.r().a(activity, bundle);
    }

    @DexIgnore
    public final void onActivityDestroyed(Activity activity) {
        this.e.r().a(activity);
    }

    @DexIgnore
    public final void onActivityPaused(Activity activity) {
        this.e.r().b(activity);
        tk1 t = this.e.t();
        t.a().a((Runnable) new xk1(t, t.c().c()));
    }

    @DexIgnore
    public final void onActivityResumed(Activity activity) {
        this.e.r().c(activity);
        tk1 t = this.e.t();
        t.a().a((Runnable) new wk1(t, t.c().c()));
    }

    @DexIgnore
    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        this.e.r().b(activity, bundle);
    }

    @DexIgnore
    public final void onActivityStarted(Activity activity) {
    }

    @DexIgnore
    public final void onActivityStopped(Activity activity) {
    }

    @DexIgnore
    public /* synthetic */ nj1(dj1 dj1, ej1 ej1) {
        this(dj1);
    }
}
