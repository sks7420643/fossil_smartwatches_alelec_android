package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hi */
public class C1951hi implements com.fossil.blesdk.obfuscated.C2128ji {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.os.IBinder f5759a;

    @DexIgnore
    public C1951hi(android.os.IBinder iBinder) {
        this.f5759a = iBinder;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        return (obj instanceof com.fossil.blesdk.obfuscated.C1951hi) && ((com.fossil.blesdk.obfuscated.C1951hi) obj).f5759a.equals(this.f5759a);
    }

    @DexIgnore
    public int hashCode() {
        return this.f5759a.hashCode();
    }
}
