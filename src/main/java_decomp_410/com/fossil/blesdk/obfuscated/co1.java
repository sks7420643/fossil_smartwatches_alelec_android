package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class co1<TResult, TContinuationResult> implements qo1<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ pn1<TResult, TContinuationResult> b;
    @DexIgnore
    public /* final */ uo1<TContinuationResult> c;

    @DexIgnore
    public co1(Executor executor, pn1<TResult, TContinuationResult> pn1, uo1<TContinuationResult> uo1) {
        this.a = executor;
        this.b = pn1;
        this.c = uo1;
    }

    @DexIgnore
    public final void onComplete(wn1<TResult> wn1) {
        this.a.execute(new do1(this, wn1));
    }
}
