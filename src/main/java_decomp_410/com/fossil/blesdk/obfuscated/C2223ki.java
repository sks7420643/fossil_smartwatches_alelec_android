package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ki */
public class C2223ki {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ int[] f6867a; // = {16842755, 16843041, 16843093, 16843097, 16843551, 16843754, 16843771, 16843778, 16843779};

    @DexIgnore
    /* renamed from: b */
    public static /* final */ int[] f6868b; // = {16842755, 16843189, 16843190, 16843556, 16843557, 16843558, 16843866, 16843867};

    @DexIgnore
    /* renamed from: c */
    public static /* final */ int[] f6869c; // = {16842755, 16843780, 16843781, 16843782, 16843783, 16843784, 16843785, 16843786, 16843787, 16843788, 16843789, 16843979, 16843980, 16844062};

    @DexIgnore
    /* renamed from: d */
    public static /* final */ int[] f6870d; // = {16842755, 16843781};

    @DexIgnore
    /* renamed from: e */
    public static /* final */ int[] f6871e; // = {16843161};

    @DexIgnore
    /* renamed from: f */
    public static /* final */ int[] f6872f; // = {16842755, 16843213};

    @DexIgnore
    /* renamed from: g */
    public static /* final */ int[] f6873g; // = {16843073, 16843160, 16843198, 16843199, 16843200, 16843486, 16843487, 16843488};

    @DexIgnore
    /* renamed from: h */
    public static /* final */ int[] f6874h; // = {16843490};

    @DexIgnore
    /* renamed from: i */
    public static /* final */ int[] f6875i; // = {16843486, 16843487, 16843488, 16843489};

    @DexIgnore
    /* renamed from: j */
    public static /* final */ int[] f6876j; // = {16842788, 16843073, 16843488, 16843992};

    @DexIgnore
    /* renamed from: k */
    public static /* final */ int[] f6877k; // = {16843489, 16843781, 16843892, 16843893};

    @DexIgnore
    /* renamed from: l */
    public static /* final */ int[] f6878l; // = {16843772, 16843773, 16843774, 16843775, 16843781};
}
