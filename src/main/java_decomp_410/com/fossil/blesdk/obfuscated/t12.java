package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t12 {
    @DexIgnore
    public b22 a;

    @DexIgnore
    public b22 a() {
        return this.a;
    }

    @DexIgnore
    public void a(int i) {
    }

    @DexIgnore
    public void a(boolean z) {
    }

    @DexIgnore
    public void b(int i) {
    }

    @DexIgnore
    public void c(int i) {
    }

    @DexIgnore
    public void a(b22 b22) {
        this.a = b22;
    }
}
