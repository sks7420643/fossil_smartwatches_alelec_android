package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.yl4;
import java.io.IOException;
import java.net.Socket;
import javax.net.ssl.SSLSocket;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class hm4 {
    @DexIgnore
    public static hm4 a;

    @DexIgnore
    public abstract int a(Response.a aVar);

    @DexIgnore
    public abstract tm4 a(ol4 ol4, gl4 gl4, wm4 wm4, fm4 fm4);

    @DexIgnore
    public abstract um4 a(ol4 ol4);

    @DexIgnore
    public abstract IOException a(jl4 jl4, IOException iOException);

    @DexIgnore
    public abstract Socket a(ol4 ol4, gl4 gl4, wm4 wm4);

    @DexIgnore
    public abstract void a(pl4 pl4, SSLSocket sSLSocket, boolean z);

    @DexIgnore
    public abstract void a(yl4.a aVar, String str);

    @DexIgnore
    public abstract void a(yl4.a aVar, String str, String str2);

    @DexIgnore
    public abstract boolean a(gl4 gl4, gl4 gl42);

    @DexIgnore
    public abstract boolean a(ol4 ol4, tm4 tm4);

    @DexIgnore
    public abstract void b(ol4 ol4, tm4 tm4);
}
