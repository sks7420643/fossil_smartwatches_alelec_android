package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.v5 */
public class C3084v5 extends com.fossil.blesdk.obfuscated.C2185k6 {

    @DexIgnore
    /* renamed from: c */
    public static com.fossil.blesdk.obfuscated.C3084v5.C3087c f10147c;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.v5$a")
    /* renamed from: com.fossil.blesdk.obfuscated.v5$a */
    public static class C3085a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.lang.String[] f10148e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ android.app.Activity f10149f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ int f10150g;

        @DexIgnore
        public C3085a(java.lang.String[] strArr, android.app.Activity activity, int i) {
            this.f10148e = strArr;
            this.f10149f = activity;
            this.f10150g = i;
        }

        @DexIgnore
        public void run() {
            int[] iArr = new int[this.f10148e.length];
            android.content.pm.PackageManager packageManager = this.f10149f.getPackageManager();
            java.lang.String packageName = this.f10149f.getPackageName();
            int length = this.f10148e.length;
            for (int i = 0; i < length; i++) {
                iArr[i] = packageManager.checkPermission(this.f10148e[i], packageName);
            }
            ((com.fossil.blesdk.obfuscated.C3084v5.C3086b) this.f10149f).onRequestPermissionsResult(this.f10150g, this.f10148e, iArr);
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.v5$b */
    public interface C3086b {
        @DexIgnore
        void onRequestPermissionsResult(int i, java.lang.String[] strArr, int[] iArr);
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.v5$c */
    public interface C3087c {
        @DexIgnore
        /* renamed from: a */
        boolean mo16978a(android.app.Activity activity, int i, int i2, android.content.Intent intent);

        @DexIgnore
        /* renamed from: a */
        boolean mo16979a(android.app.Activity activity, java.lang.String[] strArr, int i);
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.v5$d */
    public interface C3088d {
        @DexIgnore
        void validateRequestPermissionsRequestCode(int i);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.v5$e")
    /* renamed from: com.fossil.blesdk.obfuscated.v5$e */
    public static class C3089e extends android.app.SharedElementCallback {

        @DexIgnore
        /* renamed from: a */
        public /* final */ androidx.core.app.SharedElementCallback f10151a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.v5$e$a")
        /* renamed from: com.fossil.blesdk.obfuscated.v5$e$a */
        public class C3090a implements androidx.core.app.SharedElementCallback.C0137a {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ android.app.SharedElementCallback.OnSharedElementsReadyListener f10152a;

            @DexIgnore
            public C3090a(com.fossil.blesdk.obfuscated.C3084v5.C3089e eVar, android.app.SharedElementCallback.OnSharedElementsReadyListener onSharedElementsReadyListener) {
                this.f10152a = onSharedElementsReadyListener;
            }

            @DexIgnore
            /* renamed from: a */
            public void mo1583a() {
                this.f10152a.onSharedElementsReady();
            }
        }

        @DexIgnore
        public C3089e(androidx.core.app.SharedElementCallback sharedElementCallback) {
            this.f10151a = sharedElementCallback;
        }

        @DexIgnore
        public android.os.Parcelable onCaptureSharedElementSnapshot(android.view.View view, android.graphics.Matrix matrix, android.graphics.RectF rectF) {
            return this.f10151a.mo1576a(view, matrix, rectF);
        }

        @DexIgnore
        public android.view.View onCreateSnapshotView(android.content.Context context, android.os.Parcelable parcelable) {
            return this.f10151a.mo1577a(context, parcelable);
        }

        @DexIgnore
        public void onMapSharedElements(java.util.List<java.lang.String> list, java.util.Map<java.lang.String, android.view.View> map) {
            this.f10151a.mo1581a(list, map);
        }

        @DexIgnore
        public void onRejectSharedElements(java.util.List<android.view.View> list) {
            this.f10151a.mo1578a(list);
        }

        @DexIgnore
        public void onSharedElementEnd(java.util.List<java.lang.String> list, java.util.List<android.view.View> list2, java.util.List<android.view.View> list3) {
            this.f10151a.mo1580a(list, list2, list3);
        }

        @DexIgnore
        public void onSharedElementStart(java.util.List<java.lang.String> list, java.util.List<android.view.View> list2, java.util.List<android.view.View> list3) {
            this.f10151a.mo1582b(list, list2, list3);
        }

        @DexIgnore
        public void onSharedElementsArrived(java.util.List<java.lang.String> list, java.util.List<android.view.View> list2, android.app.SharedElementCallback.OnSharedElementsReadyListener onSharedElementsReadyListener) {
            this.f10151a.mo1579a(list, list2, (androidx.core.app.SharedElementCallback.C0137a) new com.fossil.blesdk.obfuscated.C3084v5.C3089e.C3090a(this, onSharedElementsReadyListener));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3084v5.C3087c m15061a() {
        return f10147c;
    }

    @DexIgnore
    /* renamed from: b */
    public static void m15068b(android.app.Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            activity.finishAfterTransition();
        } else {
            activity.finish();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static void m15070c(android.app.Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            activity.postponeEnterTransition();
        }
    }

    @DexIgnore
    /* renamed from: d */
    public static void m15071d(android.app.Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            activity.startPostponedEnterTransition();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m15063a(android.app.Activity activity, android.content.Intent intent, int i, android.os.Bundle bundle) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            activity.startActivityForResult(intent, i, bundle);
        } else {
            activity.startActivityForResult(intent, i);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m15069b(android.app.Activity activity, androidx.core.app.SharedElementCallback sharedElementCallback) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            activity.setExitSharedElementCallback(sharedElementCallback != null ? new com.fossil.blesdk.obfuscated.C3084v5.C3089e(sharedElementCallback) : null);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m15064a(android.app.Activity activity, android.content.IntentSender intentSender, int i, android.content.Intent intent, int i2, int i3, int i4, android.os.Bundle bundle) throws android.content.IntentSender.SendIntentException {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            activity.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4, bundle);
        } else {
            activity.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m15062a(android.app.Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            activity.finishAffinity();
        } else {
            activity.finish();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m15065a(android.app.Activity activity, androidx.core.app.SharedElementCallback sharedElementCallback) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            activity.setEnterSharedElementCallback(sharedElementCallback != null ? new com.fossil.blesdk.obfuscated.C3084v5.C3089e(sharedElementCallback) : null);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m15066a(android.app.Activity activity, java.lang.String[] strArr, int i) {
        com.fossil.blesdk.obfuscated.C3084v5.C3087c cVar = f10147c;
        if (cVar != null && cVar.mo16979a(activity, strArr, i)) {
            return;
        }
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (activity instanceof com.fossil.blesdk.obfuscated.C3084v5.C3088d) {
                ((com.fossil.blesdk.obfuscated.C3084v5.C3088d) activity).validateRequestPermissionsRequestCode(i);
            }
            activity.requestPermissions(strArr, i);
        } else if (activity instanceof com.fossil.blesdk.obfuscated.C3084v5.C3086b) {
            new android.os.Handler(android.os.Looper.getMainLooper()).post(new com.fossil.blesdk.obfuscated.C3084v5.C3085a(strArr, activity, i));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m15067a(android.app.Activity activity, java.lang.String str) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return activity.shouldShowRequestPermissionRationale(str);
        }
        return false;
    }
}
