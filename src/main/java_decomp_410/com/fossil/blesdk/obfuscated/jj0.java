package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jj0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<jj0> CREATOR; // = new zk0();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ String f;

    @DexIgnore
    public jj0(int i, String str) {
        this.e = i;
        this.f = str;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof jj0)) {
            jj0 jj0 = (jj0) obj;
            return jj0.e == this.e && zj0.a(jj0.f, this.f);
        }
    }

    @DexIgnore
    public int hashCode() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        int i = this.e;
        String str = this.f;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 12);
        sb.append(i);
        sb.append(":");
        sb.append(str);
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e);
        kk0.a(parcel, 2, this.f, false);
        kk0.a(parcel, a);
    }
}
