package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hb1 extends AbstractList<String> implements h91, RandomAccess {
    @DexIgnore
    public /* final */ h91 e;

    @DexIgnore
    public hb1(h91 h91) {
        this.e = h91;
    }

    @DexIgnore
    public final h91 C() {
        return this;
    }

    @DexIgnore
    public final List<?> E() {
        return this.e.E();
    }

    @DexIgnore
    public final void a(zzte zzte) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final Object d(int i) {
        return this.e.d(i);
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        return (String) this.e.get(i);
    }

    @DexIgnore
    public final Iterator<String> iterator() {
        return new jb1(this);
    }

    @DexIgnore
    public final ListIterator<String> listIterator(int i) {
        return new ib1(this, i);
    }

    @DexIgnore
    public final int size() {
        return this.e.size();
    }
}
