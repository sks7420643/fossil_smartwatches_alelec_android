package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.remote.GuestApiService;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b52 implements Factory<GuestApiService> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public b52(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static b52 a(n42 n42) {
        return new b52(n42);
    }

    @DexIgnore
    public static GuestApiService b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static GuestApiService c(n42 n42) {
        GuestApiService h = n42.h();
        n44.a(h, "Cannot return null from a non-@Nullable @Provides method");
        return h;
    }

    @DexIgnore
    public GuestApiService get() {
        return b(this.a);
    }
}
