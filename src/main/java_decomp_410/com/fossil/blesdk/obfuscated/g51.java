package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface g51 extends IInterface {
    @DexIgnore
    boolean a(g51 g51) throws RemoteException;

    @DexIgnore
    int k() throws RemoteException;
}
