package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ie2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ LinearLayout A;
    @DexIgnore
    public /* final */ SwitchCompat B;
    @DexIgnore
    public /* final */ SwitchCompat C;
    @DexIgnore
    public /* final */ SwitchCompat D;
    @DexIgnore
    public /* final */ SwitchCompat E;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ ImageView x;
    @DexIgnore
    public /* final */ LinearLayout y;
    @DexIgnore
    public /* final */ LinearLayout z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ie2(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ConstraintLayout constraintLayout5, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, FlexibleTextView flexibleTextView11, FlexibleTextView flexibleTextView12, FlexibleTextView flexibleTextView13, FlexibleTextView flexibleTextView14, FlexibleTextView flexibleTextView15, ImageView imageView, LinearLayout linearLayout, LinearLayout linearLayout2, LinearLayout linearLayout3, ScrollView scrollView, SwitchCompat switchCompat, SwitchCompat switchCompat2, SwitchCompat switchCompat3, SwitchCompat switchCompat4) {
        super(obj, view, i);
        this.q = constraintLayout2;
        this.r = constraintLayout4;
        this.s = flexibleButton;
        this.t = flexibleTextView5;
        this.u = flexibleTextView7;
        this.v = flexibleTextView12;
        this.w = flexibleTextView15;
        this.x = imageView;
        this.y = linearLayout;
        this.z = linearLayout2;
        this.A = linearLayout3;
        this.B = switchCompat;
        this.C = switchCompat2;
        this.D = switchCompat3;
        this.E = switchCompat4;
    }
}
