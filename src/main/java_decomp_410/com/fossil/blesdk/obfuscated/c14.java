package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.StatReportStrategy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class c14 {
    @DexIgnore
    public static volatile long f;
    @DexIgnore
    public o04 a;
    @DexIgnore
    public StatReportStrategy b; // = null;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public Context d; // = null;
    @DexIgnore
    public long e; // = System.currentTimeMillis();

    @DexIgnore
    public c14(o04 o04) {
        this.a = o04;
        this.b = h04.o();
        this.c = o04.e();
        this.d = o04.d();
    }

    @DexIgnore
    public void a() {
        if (!d()) {
            if (h04.J > 0 && this.e >= f) {
                j04.f(this.d);
                f = this.e + h04.K;
                if (h04.q()) {
                    t14 f2 = j04.m;
                    f2.e("nextFlushTime=" + f);
                }
            }
            if (t04.a(this.d).f()) {
                if (h04.q()) {
                    t14 f3 = j04.m;
                    f3.e("sendFailedCount=" + j04.p);
                }
                if (!j04.a()) {
                    b();
                    return;
                }
                g14.b(this.d).a(this.a, (p24) null, this.c, false);
                if (this.e - j04.q > 1800000) {
                    j04.d(this.d);
                    return;
                }
                return;
            }
            g14.b(this.d).a(this.a, (p24) null, this.c, false);
        }
    }

    @DexIgnore
    public final void a(p24 p24) {
        q24.b(j04.r).a(this.a, p24);
    }

    @DexIgnore
    public final void b() {
        if (this.a.c() != null && this.a.c().e()) {
            this.b = StatReportStrategy.INSTANT;
        }
        if (h04.z && t04.a(j04.r).e()) {
            this.b = StatReportStrategy.INSTANT;
        }
        if (h04.q()) {
            t14 f2 = j04.m;
            f2.e("strategy=" + this.b.name());
        }
        switch (w04.a[this.b.ordinal()]) {
            case 1:
                c();
                return;
            case 2:
                g14.b(this.d).a(this.a, (p24) null, this.c, false);
                if (h04.q()) {
                    t14 f3 = j04.m;
                    f3.e("PERIOD currTime=" + this.e + ",nextPeriodSendTs=" + j04.s + ",difftime=" + (j04.s - this.e));
                }
                if (j04.s == 0) {
                    j04.s = i24.a(this.d, "last_period_ts", 0);
                    if (this.e > j04.s) {
                        j04.e(this.d);
                    }
                    long l = this.e + ((long) (h04.l() * 60 * 1000));
                    if (j04.s > l) {
                        j04.s = l;
                    }
                    l24.a(this.d).a();
                }
                if (h04.q()) {
                    t14 f4 = j04.m;
                    f4.e("PERIOD currTime=" + this.e + ",nextPeriodSendTs=" + j04.s + ",difftime=" + (j04.s - this.e));
                }
                if (this.e > j04.s) {
                    j04.e(this.d);
                    return;
                }
                return;
            case 3:
            case 4:
                g14.b(this.d).a(this.a, (p24) null, this.c, false);
                return;
            case 5:
                g14.b(this.d).a(this.a, (p24) new d14(this), this.c, true);
                return;
            case 6:
                if (t04.a(j04.r).c() == 1) {
                    c();
                    return;
                } else {
                    g14.b(this.d).a(this.a, (p24) null, this.c, false);
                    return;
                }
            case 7:
                if (e24.h(this.d)) {
                    a((p24) new e14(this));
                    return;
                }
                return;
            default:
                t14 f5 = j04.m;
                f5.d("Invalid stat strategy:" + h04.o());
                return;
        }
    }

    @DexIgnore
    public final void c() {
        if (g14.i().f <= 0 || !h04.I) {
            a((p24) new f14(this));
            return;
        }
        g14.i().a(this.a, (p24) null, this.c, true);
        g14.i().a(-1);
    }

    @DexIgnore
    public final boolean d() {
        if (h04.w <= 0) {
            return false;
        }
        if (this.e > j04.d) {
            j04.c.clear();
            long unused = j04.d = this.e + h04.x;
            if (h04.q()) {
                t14 f2 = j04.m;
                f2.e("clear methodsCalledLimitMap, nextLimitCallClearTime=" + j04.d);
            }
        }
        Integer valueOf = Integer.valueOf(this.a.a().a());
        Integer num = (Integer) j04.c.get(valueOf);
        if (num != null) {
            j04.c.put(valueOf, Integer.valueOf(num.intValue() + 1));
            if (num.intValue() <= h04.w) {
                return false;
            }
            if (h04.q()) {
                t14 f3 = j04.m;
                f3.c("event " + this.a.f() + " was discard, cause of called limit, current:" + num + ", limit:" + h04.w + ", period:" + h04.x + " ms");
            }
            return true;
        }
        j04.c.put(valueOf, 1);
        return false;
    }
}
