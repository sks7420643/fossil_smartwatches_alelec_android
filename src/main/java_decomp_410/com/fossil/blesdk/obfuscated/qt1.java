package com.fossil.blesdk.obfuscated;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qt1 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ a b;
        @DexIgnore
        public a c;
        @DexIgnore
        public boolean d;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public String a;
            @DexIgnore
            public Object b;
            @DexIgnore
            public a c;

            @DexIgnore
            public a() {
            }
        }

        @DexIgnore
        public b a(String str, Object obj) {
            b(str, obj);
            return this;
        }

        @DexIgnore
        public b b(Object obj) {
            a(obj);
            return this;
        }

        @DexIgnore
        public String toString() {
            boolean z = this.d;
            StringBuilder sb = new StringBuilder(32);
            sb.append(this.a);
            sb.append('{');
            String str = "";
            for (a aVar = this.b.c; aVar != null; aVar = aVar.c) {
                Object obj = aVar.b;
                if (!z || obj != null) {
                    sb.append(str);
                    String str2 = aVar.a;
                    if (str2 != null) {
                        sb.append(str2);
                        sb.append('=');
                    }
                    if (obj == null || !obj.getClass().isArray()) {
                        sb.append(obj);
                    } else {
                        String deepToString = Arrays.deepToString(new Object[]{obj});
                        sb.append(deepToString, 1, deepToString.length() - 1);
                    }
                    str = ", ";
                }
            }
            sb.append('}');
            return sb.toString();
        }

        @DexIgnore
        public b(String str) {
            this.b = new a();
            this.c = this.b;
            this.d = false;
            st1.a(str);
            this.a = str;
        }

        @DexIgnore
        public b a(String str, int i) {
            b(str, String.valueOf(i));
            return this;
        }

        @DexIgnore
        public final b b(String str, Object obj) {
            a a2 = a();
            a2.b = obj;
            st1.a(str);
            a2.a = str;
            return this;
        }

        @DexIgnore
        public final a a() {
            a aVar = new a();
            this.c.c = aVar;
            this.c = aVar;
            return aVar;
        }

        @DexIgnore
        public final b a(Object obj) {
            a().b = obj;
            return this;
        }
    }

    @DexIgnore
    public static <T> T a(T t, T t2) {
        if (t != null) {
            return t;
        }
        st1.a(t2);
        return t2;
    }

    @DexIgnore
    public static b a(Object obj) {
        return new b(obj.getClass().getSimpleName());
    }
}
