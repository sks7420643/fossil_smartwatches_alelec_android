package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vj3 implements Factory<uj3> {
    @DexIgnore
    public static uj3 a(yj3 yj3, UpdateUser updateUser, nr2 nr2) {
        return new uj3(yj3, updateUser, nr2);
    }
}
