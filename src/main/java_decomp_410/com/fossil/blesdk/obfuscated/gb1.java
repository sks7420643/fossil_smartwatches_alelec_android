package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gb1 extends eb1<fb1, fb1> {
    @DexIgnore
    public static void a(Object obj, fb1 fb1) {
        ((t81) obj).zzbyf = fb1;
    }

    @DexIgnore
    public final boolean a(ma1 ma1) {
        return false;
    }

    @DexIgnore
    public final /* synthetic */ int b(Object obj) {
        return ((fb1) obj).b();
    }

    @DexIgnore
    public final /* synthetic */ Object c(Object obj, Object obj2) {
        fb1 fb1 = (fb1) obj;
        fb1 fb12 = (fb1) obj2;
        if (fb12.equals(fb1.d())) {
            return fb1;
        }
        return fb1.a(fb1, fb12);
    }

    @DexIgnore
    public final /* synthetic */ Object d(Object obj) {
        fb1 fb1 = ((t81) obj).zzbyf;
        if (fb1 != fb1.d()) {
            return fb1;
        }
        fb1 e = fb1.e();
        a(obj, e);
        return e;
    }

    @DexIgnore
    public final /* synthetic */ int e(Object obj) {
        return ((fb1) obj).c();
    }

    @DexIgnore
    public final void f(Object obj) {
        ((t81) obj).zzbyf.a();
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, sb1 sb1) throws IOException {
        ((fb1) obj).b(sb1);
    }

    @DexIgnore
    public final /* synthetic */ void b(Object obj, sb1 sb1) throws IOException {
        ((fb1) obj).a(sb1);
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, Object obj2) {
        a(obj, (fb1) obj2);
    }

    @DexIgnore
    public final /* synthetic */ Object c(Object obj) {
        return ((t81) obj).zzbyf;
    }

    @DexIgnore
    public final /* synthetic */ Object a(Object obj) {
        fb1 fb1 = (fb1) obj;
        fb1.a();
        return fb1;
    }

    @DexIgnore
    public final /* synthetic */ void b(Object obj, Object obj2) {
        a(obj, (fb1) obj2);
    }

    @DexIgnore
    public final /* synthetic */ Object a() {
        return fb1.e();
    }

    @DexIgnore
    public final /* synthetic */ void b(Object obj, int i, long j) {
        ((fb1) obj).a((i << 3) | 1, (Object) Long.valueOf(j));
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, int i, Object obj2) {
        ((fb1) obj).a((i << 3) | 3, (Object) (fb1) obj2);
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, int i, zzte zzte) {
        ((fb1) obj).a((i << 3) | 2, (Object) zzte);
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, int i, int i2) {
        ((fb1) obj).a((i << 3) | 5, (Object) Integer.valueOf(i2));
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, int i, long j) {
        ((fb1) obj).a(i << 3, (Object) Long.valueOf(j));
    }
}
