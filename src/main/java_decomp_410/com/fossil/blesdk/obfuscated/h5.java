package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class h5 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public ArrayList<a> e; // = new ArrayList<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public ConstraintAnchor a;
        @DexIgnore
        public ConstraintAnchor b;
        @DexIgnore
        public int c;
        @DexIgnore
        public ConstraintAnchor.Strength d;
        @DexIgnore
        public int e;

        @DexIgnore
        public a(ConstraintAnchor constraintAnchor) {
            this.a = constraintAnchor;
            this.b = constraintAnchor.g();
            this.c = constraintAnchor.b();
            this.d = constraintAnchor.f();
            this.e = constraintAnchor.a();
        }

        @DexIgnore
        public void a(ConstraintWidget constraintWidget) {
            constraintWidget.a(this.a.h()).a(this.b, this.c, this.d, this.e);
        }

        @DexIgnore
        public void b(ConstraintWidget constraintWidget) {
            this.a = constraintWidget.a(this.a.h());
            ConstraintAnchor constraintAnchor = this.a;
            if (constraintAnchor != null) {
                this.b = constraintAnchor.g();
                this.c = this.a.b();
                this.d = this.a.f();
                this.e = this.a.a();
                return;
            }
            this.b = null;
            this.c = 0;
            this.d = ConstraintAnchor.Strength.STRONG;
            this.e = 0;
        }
    }

    @DexIgnore
    public h5(ConstraintWidget constraintWidget) {
        this.a = constraintWidget.w();
        this.b = constraintWidget.x();
        this.c = constraintWidget.t();
        this.d = constraintWidget.j();
        ArrayList<ConstraintAnchor> c2 = constraintWidget.c();
        int size = c2.size();
        for (int i = 0; i < size; i++) {
            this.e.add(new a(c2.get(i)));
        }
    }

    @DexIgnore
    public void a(ConstraintWidget constraintWidget) {
        constraintWidget.s(this.a);
        constraintWidget.t(this.b);
        constraintWidget.p(this.c);
        constraintWidget.h(this.d);
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).a(constraintWidget);
        }
    }

    @DexIgnore
    public void b(ConstraintWidget constraintWidget) {
        this.a = constraintWidget.w();
        this.b = constraintWidget.x();
        this.c = constraintWidget.t();
        this.d = constraintWidget.j();
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).b(constraintWidget);
        }
    }
}
