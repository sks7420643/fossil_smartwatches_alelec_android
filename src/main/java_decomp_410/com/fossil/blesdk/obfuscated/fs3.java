package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import android.provider.ContactsContract;
import android.widget.FilterQueryProvider;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fs3 {
    @DexIgnore
    public static /* final */ fs3 a; // = new fs3();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements FilterQueryProvider {
        @DexIgnore
        public static /* final */ a a; // = new a();

        @DexIgnore
        public final Cursor runQuery(CharSequence charSequence) {
            String str;
            kd4.a((Object) charSequence, "constraint");
            if (charSequence.length() == 0) {
                str = "has_phone_number!=0 AND mimetype=?";
            } else {
                str = "(display_name LIKE '%" + charSequence + "%'" + " OR " + "display_name" + " LIKE 'N%" + charSequence + "%'" + ") AND " + "has_phone_number" + "!=0 AND " + "mimetype" + "=?";
            }
            return PortfolioApp.W.c().getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"}, str, new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
        }
    }

    @DexIgnore
    public final FilterQueryProvider a() {
        return a.a;
    }
}
