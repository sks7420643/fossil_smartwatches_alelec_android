package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.dw */
public class C1663dw<R> implements com.fossil.blesdk.obfuscated.C1750ew<R> {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C1663dw<?> f4535a; // = new com.fossil.blesdk.obfuscated.C1663dw<>();

    @DexIgnore
    /* renamed from: b */
    public static /* final */ com.fossil.blesdk.obfuscated.C1836fw<?> f4536b; // = new com.fossil.blesdk.obfuscated.C1663dw.C1664a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dw$a")
    /* renamed from: com.fossil.blesdk.obfuscated.dw$a */
    public static class C1664a<R> implements com.fossil.blesdk.obfuscated.C1836fw<R> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1750ew<R> mo10142a(com.bumptech.glide.load.DataSource dataSource, boolean z) {
            return com.fossil.blesdk.obfuscated.C1663dw.f4535a;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static <R> com.fossil.blesdk.obfuscated.C1836fw<R> m6070a() {
        return f4536b;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo10141a(java.lang.Object obj, com.fossil.blesdk.obfuscated.C1750ew.C1751a aVar) {
        return false;
    }
}
