package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.fi4;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CompletionHandlerException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class eg4<T> extends mh4<T> implements dg4<T>, fc4 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater j; // = AtomicIntegerFieldUpdater.newUpdater(eg4.class, "_decision");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater k; // = AtomicReferenceFieldUpdater.newUpdater(eg4.class, Object.class, "_state");
    @DexIgnore
    public volatile int _decision; // = 0;
    @DexIgnore
    public volatile Object _state; // = vf4.e;
    @DexIgnore
    public /* final */ CoroutineContext h; // = this.i.getContext();
    @DexIgnore
    public /* final */ yb4<T> i;
    @DexIgnore
    public volatile oh4 parentHandle;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public eg4(yb4<? super T> yb4, int i2) {
        super(i2);
        kd4.b(yb4, "delegate");
        this.i = yb4;
    }

    @DexIgnore
    public void a(Object obj, Throwable th) {
        kd4.b(th, "cause");
        if (obj instanceof qg4) {
            try {
                ((qg4) obj).b.invoke(th);
            } catch (Throwable th2) {
                CoroutineContext context = getContext();
                wg4.a(context, (Throwable) new CompletionHandlerException("Exception in cancellation handler for " + this, th2));
            }
        }
    }

    @DexIgnore
    public final yb4<T> b() {
        return this.i;
    }

    @DexIgnore
    public Object c() {
        return f();
    }

    @DexIgnore
    public final void d(Object obj) {
        throw new IllegalStateException(("Already resumed, but proposed with update " + obj).toString());
    }

    @DexIgnore
    public final Object e() {
        g();
        if (k()) {
            return cc4.a();
        }
        Object f = f();
        if (!(f instanceof ng4)) {
            if (this.g == 1) {
                fi4 fi4 = (fi4) getContext().get(fi4.d);
                if (fi4 != null && !fi4.isActive()) {
                    CancellationException y = fi4.y();
                    a(f, (Throwable) y);
                    throw ck4.a(y, (yb4<?>) this);
                }
            }
            return c(f);
        }
        throw ck4.a(((ng4) f).a, (yb4<?>) this);
    }

    @DexIgnore
    public final Object f() {
        return this._state;
    }

    @DexIgnore
    public final void g() {
        if (!h()) {
            fi4 fi4 = (fi4) this.i.getContext().get(fi4.d);
            if (fi4 != null) {
                fi4.start();
                oh4 a = fi4.a.a(fi4, true, false, new hg4(fi4, this), 2, (Object) null);
                this.parentHandle = a;
                if (h()) {
                    a.dispose();
                    this.parentHandle = ri4.e;
                }
            }
        }
    }

    @DexIgnore
    public fc4 getCallerFrame() {
        yb4<T> yb4 = this.i;
        if (!(yb4 instanceof fc4)) {
            yb4 = null;
        }
        return (fc4) yb4;
    }

    @DexIgnore
    public CoroutineContext getContext() {
        return this.h;
    }

    @DexIgnore
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public boolean h() {
        return !(f() instanceof si4);
    }

    @DexIgnore
    public String i() {
        return "CancellableContinuation";
    }

    @DexIgnore
    public boolean isActive() {
        return f() instanceof si4;
    }

    @DexIgnore
    public final boolean j() {
        do {
            int i2 = this._decision;
            if (i2 != 0) {
                if (i2 == 1) {
                    return false;
                }
                throw new IllegalStateException("Already resumed".toString());
            }
        } while (!j.compareAndSet(this, 0, 2));
        return true;
    }

    @DexIgnore
    public final boolean k() {
        do {
            int i2 = this._decision;
            if (i2 != 0) {
                if (i2 == 2) {
                    return false;
                }
                throw new IllegalStateException("Already suspended".toString());
            }
        } while (!j.compareAndSet(this, 0, 1));
        return true;
    }

    @DexIgnore
    public void resumeWith(Object obj) {
        a(og4.a(obj), this.g);
    }

    @DexIgnore
    public String toString() {
        return i() + '(' + dh4.a((yb4<?>) this.i) + "){" + f() + "}@" + dh4.b(this);
    }

    @DexIgnore
    public void b(Object obj) {
        kd4.b(obj, "token");
        a(this.g);
    }

    @DexIgnore
    public <T> T c(Object obj) {
        if (obj instanceof pg4) {
            return ((pg4) obj).b;
        }
        return obj instanceof qg4 ? ((qg4) obj).a : obj;
    }

    @DexIgnore
    public final void d() {
        oh4 oh4 = this.parentHandle;
        if (oh4 != null) {
            oh4.dispose();
            this.parentHandle = ri4.e;
        }
    }

    @DexIgnore
    public void b(xc4<? super Throwable, qa4> xc4) {
        Object obj;
        kd4.b(xc4, "handler");
        Throwable th = null;
        bg4 bg4 = null;
        do {
            obj = this._state;
            if (obj instanceof vf4) {
                if (bg4 == null) {
                    bg4 = a(xc4);
                }
            } else if (obj instanceof bg4) {
                a(xc4, obj);
                throw null;
            } else if (!(obj instanceof gg4)) {
                return;
            } else {
                if (((gg4) obj).b()) {
                    try {
                        if (!(obj instanceof ng4)) {
                            obj = null;
                        }
                        ng4 ng4 = (ng4) obj;
                        if (ng4 != null) {
                            th = ng4.a;
                        }
                        xc4.invoke(th);
                        return;
                    } catch (Throwable th2) {
                        wg4.a(getContext(), (Throwable) new CompletionHandlerException("Exception in cancellation handler for " + this, th2));
                        return;
                    }
                } else {
                    a(xc4, obj);
                    throw null;
                }
            }
        } while (!k.compareAndSet(this, obj, bg4));
    }

    @DexIgnore
    public Throwable a(fi4 fi4) {
        kd4.b(fi4, "parent");
        return fi4.y();
    }

    @DexIgnore
    public final gg4 a(Throwable th, int i2) {
        kd4.b(th, "exception");
        return a((Object) new ng4(th, false, 2, (fd4) null), i2);
    }

    @DexIgnore
    public final void a(xc4<? super Throwable, qa4> xc4, Object obj) {
        throw new IllegalStateException(("It's prohibited to register multiple handlers, tried to register " + xc4 + ", already has " + obj).toString());
    }

    @DexIgnore
    public final bg4 a(xc4<? super Throwable, qa4> xc4) {
        return xc4 instanceof bg4 ? (bg4) xc4 : new ci4(xc4);
    }

    @DexIgnore
    public final void a(int i2) {
        if (!j()) {
            lh4.a(this, i2);
        }
    }

    @DexIgnore
    public void a(ug4 ug4, T t) {
        kd4.b(ug4, "$this$resumeUndispatched");
        yb4<T> yb4 = this.i;
        ug4 ug42 = null;
        if (!(yb4 instanceof jh4)) {
            yb4 = null;
        }
        jh4 jh4 = (jh4) yb4;
        if (jh4 != null) {
            ug42 = jh4.k;
        }
        a((Object) t, ug42 == ug4 ? 3 : this.g);
    }

    @DexIgnore
    public boolean a(Throwable th) {
        Object obj;
        boolean z;
        do {
            obj = this._state;
            if (!(obj instanceof si4)) {
                return false;
            }
            z = obj instanceof bg4;
        } while (!k.compareAndSet(this, obj, new gg4(this, th, z)));
        if (z) {
            try {
                ((bg4) obj).a(th);
            } catch (Throwable th2) {
                CoroutineContext context = getContext();
                wg4.a(context, (Throwable) new CompletionHandlerException("Exception in cancellation handler for " + this, th2));
            }
        }
        d();
        a(0);
        return true;
    }

    @DexIgnore
    public final gg4 a(Object obj, int i2) {
        Object obj2;
        do {
            obj2 = this._state;
            if (!(obj2 instanceof si4)) {
                if (obj2 instanceof gg4) {
                    gg4 gg4 = (gg4) obj2;
                    if (gg4.c()) {
                        return gg4;
                    }
                }
                d(obj);
                throw null;
            }
        } while (!k.compareAndSet(this, obj2, obj));
        d();
        a(i2);
        return null;
    }

    @DexIgnore
    public Object a(T t, Object obj) {
        Object obj2;
        T t2;
        do {
            obj2 = this._state;
            if (obj2 instanceof si4) {
                if (obj == null) {
                    t2 = t;
                } else {
                    t2 = new pg4(obj, t, (si4) obj2);
                }
            } else if (!(obj2 instanceof pg4)) {
                return null;
            } else {
                pg4 pg4 = (pg4) obj2;
                if (pg4.a != obj) {
                    return null;
                }
                if (ch4.a()) {
                    if (!(pg4.b == t)) {
                        throw new AssertionError();
                    }
                }
                return pg4.c;
            }
        } while (!k.compareAndSet(this, obj2, t2));
        d();
        return obj2;
    }
}
