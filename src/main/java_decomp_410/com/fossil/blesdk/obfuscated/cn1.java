package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface cn1 extends IInterface {
    @DexIgnore
    void a(en1 en1, an1 an1) throws RemoteException;

    @DexIgnore
    void a(tj0 tj0, int i, boolean z) throws RemoteException;

    @DexIgnore
    void c(int i) throws RemoteException;
}
