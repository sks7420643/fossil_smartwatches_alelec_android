package com.fossil.blesdk.obfuscated;

import android.util.SparseArray;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface qz2 extends v52<pz2> {
    @DexIgnore
    void a(SparseArray<List<BaseFeatureModel>> sparseArray);
}
