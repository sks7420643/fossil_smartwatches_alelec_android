package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xh */
public class C3289xh {

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Method f10935a;

    @DexIgnore
    /* renamed from: b */
    public static boolean f10936b;

    @DexIgnore
    /* renamed from: a */
    public static void m16298a(android.view.ViewGroup viewGroup, boolean z) {
        m16297a();
        java.lang.reflect.Method method = f10935a;
        if (method != null) {
            try {
                method.invoke(viewGroup, new java.lang.Object[]{java.lang.Boolean.valueOf(z)});
            } catch (java.lang.IllegalAccessException e) {
                android.util.Log.i("ViewUtilsApi18", "Failed to invoke suppressLayout method", e);
            } catch (java.lang.reflect.InvocationTargetException e2) {
                android.util.Log.i("ViewUtilsApi18", "Error invoking suppressLayout method", e2);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m16297a() {
        if (!f10936b) {
            try {
                f10935a = android.view.ViewGroup.class.getDeclaredMethod("suppressLayout", new java.lang.Class[]{java.lang.Boolean.TYPE});
                f10935a.setAccessible(true);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.i("ViewUtilsApi18", "Failed to retrieve suppressLayout method", e);
            }
            f10936b = true;
        }
    }
}
