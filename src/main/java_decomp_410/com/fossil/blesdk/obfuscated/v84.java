package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v84 {
    @DexIgnore
    public static volatile f94<Callable<r84>, r84> a;
    @DexIgnore
    public static volatile f94<r84, r84> b;

    @DexIgnore
    public static r84 a(r84 r84) {
        if (r84 != null) {
            f94<r84, r84> f94 = b;
            if (f94 == null) {
                return r84;
            }
            a(f94, r84);
            return r84;
        }
        throw new NullPointerException("scheduler == null");
    }

    @DexIgnore
    public static r84 b(Callable<r84> callable) {
        if (callable != null) {
            f94<Callable<r84>, r84> f94 = a;
            if (f94 == null) {
                return a(callable);
            }
            return a(f94, callable);
        }
        throw new NullPointerException("scheduler == null");
    }

    @DexIgnore
    public static r84 a(Callable<r84> callable) {
        try {
            r84 call = callable.call();
            if (call != null) {
                return call;
            }
            throw new NullPointerException("Scheduler Callable returned null");
        } catch (Throwable th) {
            a94.a(th);
            throw null;
        }
    }

    @DexIgnore
    public static r84 a(f94<Callable<r84>, r84> f94, Callable<r84> callable) {
        a(f94, callable);
        r84 r84 = (r84) callable;
        if (r84 != null) {
            return r84;
        }
        throw new NullPointerException("Scheduler Callable returned null");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [R, T, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public static <T, R> R a(f94<T, R> f94, T r1) {
        try {
            f94.apply(r1);
            return r1;
        } catch (Throwable th) {
            a94.a(th);
            throw null;
        }
    }
}
