package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.km */
public class C2230km {

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2037im<?> f6925a;

    @DexIgnore
    public C2230km(com.fossil.blesdk.obfuscated.C2037im<?> imVar) {
        this.f6925a = imVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12837a() {
        this.f6925a = null;
    }

    @DexIgnore
    public void finalize() throws java.lang.Throwable {
        try {
            com.fossil.blesdk.obfuscated.C2037im<?> imVar = this.f6925a;
            if (imVar != null) {
                com.fossil.blesdk.obfuscated.C2037im.C2045g j = com.fossil.blesdk.obfuscated.C2037im.m8544j();
                if (j != null) {
                    j.mo12050a(imVar, new bolts.UnobservedTaskException(imVar.mo12036a()));
                }
            }
        } finally {
            super.finalize();
        }
    }
}
