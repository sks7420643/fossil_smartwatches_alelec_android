package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.c9 */
public final class C1540c9 {

    @DexIgnore
    /* renamed from: a */
    public java.lang.Object f4018a;

    @DexIgnore
    public C1540c9(java.lang.Object obj) {
        this.f4018a = obj;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Object mo9433a() {
        return this.f4018a;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1540c9 m5324a(android.content.Context context, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return new com.fossil.blesdk.obfuscated.C1540c9(android.view.PointerIcon.getSystemIcon(context, i));
        }
        return new com.fossil.blesdk.obfuscated.C1540c9((java.lang.Object) null);
    }
}
