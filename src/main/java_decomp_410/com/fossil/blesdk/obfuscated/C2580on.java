package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.on */
public class C2580on {
    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2975tm m11847a(android.content.Context context, com.fossil.blesdk.obfuscated.C3445zm zmVar) {
        com.fossil.blesdk.obfuscated.C1432an anVar;
        java.lang.String str;
        if (zmVar != null) {
            anVar = new com.fossil.blesdk.obfuscated.C1432an(zmVar);
        } else if (android.os.Build.VERSION.SDK_INT >= 9) {
            anVar = new com.fossil.blesdk.obfuscated.C1432an((com.fossil.blesdk.obfuscated.C3445zm) new com.fossil.blesdk.obfuscated.C1960hn());
        } else {
            try {
                android.content.pm.PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                str = r3 + com.zendesk.sdk.network.impl.ZendeskConfig.SLASH + packageInfo.versionCode;
            } catch (android.content.pm.PackageManager.NameNotFoundException unused) {
                str = "volley/0";
            }
            anVar = new com.fossil.blesdk.obfuscated.C1432an((com.fossil.blesdk.obfuscated.C1882gn) new com.fossil.blesdk.obfuscated.C1644dn(android.net.http.AndroidHttpClient.newInstance(str)));
        }
        return m11846a(context, (com.fossil.blesdk.obfuscated.C2748qm) anVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2975tm m11846a(android.content.Context context, com.fossil.blesdk.obfuscated.C2748qm qmVar) {
        com.fossil.blesdk.obfuscated.C2975tm tmVar = new com.fossil.blesdk.obfuscated.C2975tm(new com.fossil.blesdk.obfuscated.C1567cn(new java.io.File(context.getCacheDir(), "volley")), qmVar);
        tmVar.mo16505b();
        return tmVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2975tm m11845a(android.content.Context context) {
        return m11847a(context, (com.fossil.blesdk.obfuscated.C3445zm) null);
    }
}
