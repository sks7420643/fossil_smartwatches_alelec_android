package com.fossil.blesdk.database;

import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.data.file.FileHandle;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.ee4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.j00;
import com.fossil.blesdk.obfuscated.k00;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.util.List;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SdkDatabaseWrapper {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ SdkDatabaseWrapper b; // = new SdkDatabaseWrapper();

    @DexIgnore
    public enum DatabaseEvent {
        INSERT,
        DELETE,
        QUERY,
        DELETE_ALL;
        
        @DexIgnore
        public /* final */ String logName;

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    /*
    static {
        String simpleName = SdkDatabaseWrapper.class.getSimpleName();
        kd4.a((Object) simpleName, "SdkDatabaseWrapper::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00fc A[ORIG_RETURN, RETURN, SYNTHETIC] */
    public final long a(DeviceFile deviceFile) {
        Long l;
        String str;
        boolean z;
        String str2;
        kd4.b(deviceFile, "deviceFile");
        SdkDatabase a2 = SdkDatabase.e.a();
        if (a2 != null) {
            k00 a3 = a2.a();
            if (a3 != null) {
                l = Long.valueOf(a3.a(deviceFile));
                if (!(l == null && ee4.a(Long.MIN_VALUE, 0).a(l.longValue()))) {
                    str2 = "Insert Fail";
                } else if (l == null) {
                    str2 = "Database instance is null";
                } else {
                    str = "";
                    z = true;
                    a(DatabaseEvent.INSERT, z, deviceFile.getDeviceMacAddress(), wa0.a(wa0.a(new JSONObject(), JSONKey.FILE, deviceFile.toJSONObject(true)), JSONKey.ROW_ID, l != null ? l : -1), str);
                    t90 t90 = t90.c;
                    String str3 = a;
                    t90.a(str3, "addFile: " + "macAddress=" + deviceFile.getDeviceMacAddress() + ", " + "fileType=" + deviceFile.getFileType() + ", " + "fileIndex=" + deviceFile.getFileIndex() + ", " + "fileCrc=" + deviceFile.getFileCrc() + ", " + "dataLength=" + deviceFile.getRawData().length + ", " + "fileLength=" + deviceFile.getFileLength() + ", " + "isCompleted=" + deviceFile.isCompleted() + ", " + "insertedRowId=" + l);
                    if (l != null) {
                        return l.longValue();
                    }
                    return -1;
                }
                str = str2;
                z = false;
                a(DatabaseEvent.INSERT, z, deviceFile.getDeviceMacAddress(), wa0.a(wa0.a(new JSONObject(), JSONKey.FILE, deviceFile.toJSONObject(true)), JSONKey.ROW_ID, l != null ? l : -1), str);
                t90 t902 = t90.c;
                String str32 = a;
                t902.a(str32, "addFile: " + "macAddress=" + deviceFile.getDeviceMacAddress() + ", " + "fileType=" + deviceFile.getFileType() + ", " + "fileIndex=" + deviceFile.getFileIndex() + ", " + "fileCrc=" + deviceFile.getFileCrc() + ", " + "dataLength=" + deviceFile.getRawData().length + ", " + "fileLength=" + deviceFile.getFileLength() + ", " + "isCompleted=" + deviceFile.isCompleted() + ", " + "insertedRowId=" + l);
                if (l != null) {
                }
            }
        }
        l = null;
        if (!(l == null && ee4.a(Long.MIN_VALUE, 0).a(l.longValue()))) {
        }
        str = str2;
        z = false;
        a(DatabaseEvent.INSERT, z, deviceFile.getDeviceMacAddress(), wa0.a(wa0.a(new JSONObject(), JSONKey.FILE, deviceFile.toJSONObject(true)), JSONKey.ROW_ID, l != null ? l : -1), str);
        t90 t9022 = t90.c;
        String str322 = a;
        t9022.a(str322, "addFile: " + "macAddress=" + deviceFile.getDeviceMacAddress() + ", " + "fileType=" + deviceFile.getFileType() + ", " + "fileIndex=" + deviceFile.getFileIndex() + ", " + "fileCrc=" + deviceFile.getFileCrc() + ", " + "dataLength=" + deviceFile.getRawData().length + ", " + "fileLength=" + deviceFile.getFileLength() + ", " + "isCompleted=" + deviceFile.isCompleted() + ", " + "insertedRowId=" + l);
        if (l != null) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0051, code lost:
        if (r2 != null) goto L_0x0061;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001d  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0020  */
    public final List<DeviceFile> b(String str, byte b2) {
        List<DeviceFile> list;
        JSONArray jSONArray;
        kd4.b(str, "deviceMacAddress");
        SdkDatabase a2 = SdkDatabase.e.a();
        Integer num = null;
        if (a2 != null) {
            k00 a3 = a2.a();
            if (a3 != null) {
                list = a3.b(str, b2);
                boolean z = list == null;
                String str2 = !z ? "" : "Database instance is null";
                JSONObject a4 = wa0.a(new FileHandle(b2, (byte) 255).toJSONObject(), JSONKey.IS_COMPLETED, JSONObject.NULL);
                JSONKey jSONKey = JSONKey.FILES;
                if (list != null) {
                    Object[] array = list.toArray(new DeviceFile[0]);
                    if (array != null) {
                        DeviceFile[] deviceFileArr = (DeviceFile[]) array;
                        if (deviceFileArr != null) {
                            jSONArray = j00.a(deviceFileArr);
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                jSONArray = new JSONArray();
                a(DatabaseEvent.QUERY, z, str, wa0.a(a4, jSONKey, jSONArray), str2);
                t90 t90 = t90.c;
                String str3 = a;
                StringBuilder sb = new StringBuilder();
                sb.append("getAllFilesByFileType: ");
                sb.append("deviceMacAddress=");
                sb.append(str);
                sb.append(", ");
                sb.append("fileType=");
                sb.append(b2);
                sb.append(", ");
                sb.append("numberOfRow=");
                if (list != null) {
                    num = Integer.valueOf(list.size());
                }
                sb.append(num);
                sb.append('.');
                t90.a(str3, sb.toString());
                return list == null ? list : cb4.a();
            }
        }
        list = null;
        if (list == null) {
        }
        String str22 = !z ? "" : "Database instance is null";
        JSONObject a42 = wa0.a(new FileHandle(b2, (byte) 255).toJSONObject(), JSONKey.IS_COMPLETED, JSONObject.NULL);
        JSONKey jSONKey2 = JSONKey.FILES;
        if (list != null) {
        }
        jSONArray = new JSONArray();
        a(DatabaseEvent.QUERY, z, str, wa0.a(a42, jSONKey2, jSONArray), str22);
        t90 t902 = t90.c;
        String str32 = a;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("getAllFilesByFileType: ");
        sb2.append("deviceMacAddress=");
        sb2.append(str);
        sb2.append(", ");
        sb2.append("fileType=");
        sb2.append(b2);
        sb2.append(", ");
        sb2.append("numberOfRow=");
        if (list != null) {
        }
        sb2.append(num);
        sb2.append('.');
        t902.a(str32, sb2.toString());
        if (list == null) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0053, code lost:
        if (r2 != null) goto L_0x0063;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001d  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0020  */
    public final List<DeviceFile> c(String str, byte b2) {
        List<DeviceFile> list;
        JSONArray jSONArray;
        kd4.b(str, "deviceMacAddress");
        SdkDatabase a2 = SdkDatabase.e.a();
        Integer num = null;
        if (a2 != null) {
            k00 a3 = a2.a();
            if (a3 != null) {
                list = a3.a(str, b2);
                boolean z = list == null;
                String str2 = !z ? "" : "Database instance is null";
                JSONObject a4 = wa0.a(new FileHandle(b2, (byte) 255).toJSONObject(), JSONKey.IS_COMPLETED, false);
                JSONKey jSONKey = JSONKey.FILES;
                if (list != null) {
                    Object[] array = list.toArray(new DeviceFile[0]);
                    if (array != null) {
                        DeviceFile[] deviceFileArr = (DeviceFile[]) array;
                        if (deviceFileArr != null) {
                            jSONArray = j00.a(deviceFileArr);
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                jSONArray = new JSONArray();
                a(DatabaseEvent.QUERY, z, str, wa0.a(a4, jSONKey, jSONArray), str2);
                t90 t90 = t90.c;
                String str3 = a;
                StringBuilder sb = new StringBuilder();
                sb.append("getIncompleteFilesByFileType: ");
                sb.append("deviceMacAddress=");
                sb.append(str);
                sb.append(", ");
                sb.append("fileType=");
                sb.append(b2);
                sb.append(", ");
                sb.append("numberOfRow=");
                if (list != null) {
                    num = Integer.valueOf(list.size());
                }
                sb.append(num);
                sb.append('.');
                t90.a(str3, sb.toString());
                return list == null ? list : cb4.a();
            }
        }
        list = null;
        if (list == null) {
        }
        String str22 = !z ? "" : "Database instance is null";
        JSONObject a42 = wa0.a(new FileHandle(b2, (byte) 255).toJSONObject(), JSONKey.IS_COMPLETED, false);
        JSONKey jSONKey2 = JSONKey.FILES;
        if (list != null) {
        }
        jSONArray = new JSONArray();
        a(DatabaseEvent.QUERY, z, str, wa0.a(a42, jSONKey2, jSONArray), str22);
        t90 t902 = t90.c;
        String str32 = a;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("getIncompleteFilesByFileType: ");
        sb2.append("deviceMacAddress=");
        sb2.append(str);
        sb2.append(", ");
        sb2.append("fileType=");
        sb2.append(b2);
        sb2.append(", ");
        sb2.append("numberOfRow=");
        if (list != null) {
        }
        sb2.append(num);
        sb2.append('.');
        t902.a(str32, sb2.toString());
        if (list == null) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0050, code lost:
        if (r2 != null) goto L_0x0060;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001d  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0020  */
    public final List<DeviceFile> b(String str, byte b2, byte b3) {
        List<DeviceFile> list;
        JSONArray jSONArray;
        kd4.b(str, "deviceMacAddress");
        SdkDatabase a2 = SdkDatabase.e.a();
        Integer num = null;
        if (a2 != null) {
            k00 a3 = a2.a();
            if (a3 != null) {
                list = a3.a(str, b2, b3);
                boolean z = list == null;
                String str2 = !z ? "" : "Database instance is null";
                JSONObject a4 = wa0.a(new FileHandle(b2, b3).toJSONObject(), JSONKey.IS_COMPLETED, false);
                JSONKey jSONKey = JSONKey.FILES;
                if (list != null) {
                    Object[] array = list.toArray(new DeviceFile[0]);
                    if (array != null) {
                        DeviceFile[] deviceFileArr = (DeviceFile[]) array;
                        if (deviceFileArr != null) {
                            jSONArray = j00.a(deviceFileArr);
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                jSONArray = new JSONArray();
                a(DatabaseEvent.QUERY, z, str, wa0.a(a4, jSONKey, jSONArray), str2);
                t90 t90 = t90.c;
                String str3 = a;
                StringBuilder sb = new StringBuilder();
                sb.append("findIncompleteFilesByFileHandle: ");
                sb.append("deviceMacAddress=");
                sb.append(str);
                sb.append(", ");
                sb.append("fileType=");
                sb.append(b2);
                sb.append(", ");
                sb.append("fileIndex=");
                sb.append(b3);
                sb.append(", ");
                sb.append("numberOfRow=");
                if (list != null) {
                    num = Integer.valueOf(list.size());
                }
                sb.append(num);
                sb.append('.');
                t90.a(str3, sb.toString());
                return list == null ? list : cb4.a();
            }
        }
        list = null;
        if (list == null) {
        }
        String str22 = !z ? "" : "Database instance is null";
        JSONObject a42 = wa0.a(new FileHandle(b2, b3).toJSONObject(), JSONKey.IS_COMPLETED, false);
        JSONKey jSONKey2 = JSONKey.FILES;
        if (list != null) {
        }
        jSONArray = new JSONArray();
        a(DatabaseEvent.QUERY, z, str, wa0.a(a42, jSONKey2, jSONArray), str22);
        t90 t902 = t90.c;
        String str32 = a;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("findIncompleteFilesByFileHandle: ");
        sb2.append("deviceMacAddress=");
        sb2.append(str);
        sb2.append(", ");
        sb2.append("fileType=");
        sb2.append(b2);
        sb2.append(", ");
        sb2.append("fileIndex=");
        sb2.append(b3);
        sb2.append(", ");
        sb2.append("numberOfRow=");
        if (list != null) {
        }
        sb2.append(num);
        sb2.append('.');
        t902.a(str32, sb2.toString());
        if (list == null) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0020  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0025  */
    public final int a(String str, byte b2, byte b3) {
        Integer num;
        String str2;
        boolean z;
        kd4.b(str, "deviceMacAddress");
        SdkDatabase a2 = SdkDatabase.e.a();
        if (a2 != null) {
            k00 a3 = a2.a();
            if (a3 != null) {
                num = Integer.valueOf(a3.b(str, b2, b3));
                if (num != null) {
                    str2 = "Database instance is null";
                    z = false;
                } else {
                    str2 = "";
                    z = true;
                }
                a(DatabaseEvent.DELETE, z, str, wa0.a(wa0.a(new FileHandle(b2, b3).toJSONObject(), JSONKey.IS_COMPLETED, false), JSONKey.NUMBER_OF_DELETED_ROW, num == null ? num : -1), str2);
                t90 t90 = t90.c;
                String str3 = a;
                t90.a(str3, "deleteIncompleteFileByFileHandle: " + "deviceMacAddress=" + str + ", " + "fileType=" + b2 + ", " + "fileIndex=" + b3 + ", " + "numberOfDeletedRow=" + num + '.');
                if (num == null) {
                    return num.intValue();
                }
                return 0;
            }
        }
        num = null;
        if (num != null) {
        }
        a(DatabaseEvent.DELETE, z, str, wa0.a(wa0.a(new FileHandle(b2, b3).toJSONObject(), JSONKey.IS_COMPLETED, false), JSONKey.NUMBER_OF_DELETED_ROW, num == null ? num : -1), str2);
        t90 t902 = t90.c;
        String str32 = a;
        t902.a(str32, "deleteIncompleteFileByFileHandle: " + "deviceMacAddress=" + str + ", " + "fileType=" + b2 + ", " + "fileIndex=" + b3 + ", " + "numberOfDeletedRow=" + num + '.');
        if (num == null) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0020  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0025  */
    public final int a(String str, byte b2) {
        Integer num;
        String str2;
        boolean z;
        kd4.b(str, "deviceMacAddress");
        SdkDatabase a2 = SdkDatabase.e.a();
        if (a2 != null) {
            k00 a3 = a2.a();
            if (a3 != null) {
                num = Integer.valueOf(a3.c(str, b2));
                if (num != null) {
                    str2 = "Database instance is null";
                    z = false;
                } else {
                    str2 = "";
                    z = true;
                }
                a(DatabaseEvent.DELETE, z, str, wa0.a(wa0.a(new FileHandle(b2, (byte) 255).toJSONObject(), JSONKey.IS_COMPLETED, JSONObject.NULL), JSONKey.NUMBER_OF_DELETED_ROW, num == null ? num : -1), str2);
                t90 t90 = t90.c;
                String str3 = a;
                t90.a(str3, "deleteFileByFileType: " + "deviceMacAddress=" + str + ", " + "fileType=" + b2 + ", " + "numberOfDeletedRow=" + num + '.');
                if (num == null) {
                    return num.intValue();
                }
                return 0;
            }
        }
        num = null;
        if (num != null) {
        }
        a(DatabaseEvent.DELETE, z, str, wa0.a(wa0.a(new FileHandle(b2, (byte) 255).toJSONObject(), JSONKey.IS_COMPLETED, JSONObject.NULL), JSONKey.NUMBER_OF_DELETED_ROW, num == null ? num : -1), str2);
        t90 t902 = t90.c;
        String str32 = a;
        t902.a(str32, "deleteFileByFileType: " + "deviceMacAddress=" + str + ", " + "fileType=" + b2 + ", " + "numberOfDeletedRow=" + num + '.');
        if (num == null) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0020  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0025  */
    public final int a(String str) {
        Integer num;
        String str2;
        boolean z;
        kd4.b(str, "deviceMacAddress");
        SdkDatabase a2 = SdkDatabase.e.a();
        if (a2 != null) {
            k00 a3 = a2.a();
            if (a3 != null) {
                num = Integer.valueOf(a3.a(str));
                if (num != null) {
                    str2 = "Database instance is null";
                    z = false;
                } else {
                    str2 = "";
                    z = true;
                }
                a(DatabaseEvent.DELETE_ALL, z, str, wa0.a(new JSONObject(), JSONKey.NUMBER_OF_DELETED_ROW, num == null ? num : -1), str2);
                t90 t90 = t90.c;
                String str3 = a;
                t90.a(str3, "deleteFileByDeviceMacAddress: " + "deviceMacAddress=" + str + ", " + "numberOfDeletedRow=" + num + '.');
                if (num == null) {
                    return num.intValue();
                }
                return 0;
            }
        }
        num = null;
        if (num != null) {
        }
        a(DatabaseEvent.DELETE_ALL, z, str, wa0.a(new JSONObject(), JSONKey.NUMBER_OF_DELETED_ROW, num == null ? num : -1), str2);
        t90 t902 = t90.c;
        String str32 = a;
        t902.a(str32, "deleteFileByDeviceMacAddress: " + "deviceMacAddress=" + str + ", " + "numberOfDeletedRow=" + num + '.');
        if (num == null) {
        }
    }

    @DexIgnore
    public final void a(DatabaseEvent databaseEvent, boolean z, String str, JSONObject jSONObject, String str2) {
        Object obj;
        JSONObject jSONObject2;
        da0 da0 = da0.l;
        String logName$blesdk_productionRelease = databaseEvent.getLogName$blesdk_productionRelease();
        EventType eventType = EventType.DATABASE;
        JSONKey jSONKey = JSONKey.MESSAGE;
        if (!qf4.a(str2)) {
            jSONObject2 = jSONObject;
            obj = str2;
        } else {
            obj = JSONObject.NULL;
            jSONObject2 = jSONObject;
        }
        da0.b(new SdkLogEntry(logName$blesdk_productionRelease, eventType, str, "", "", z, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(jSONObject2, jSONKey, obj), 448, (fd4) null));
    }
}
