package com.fossil.blesdk.database;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.k00;
import com.fossil.blesdk.obfuscated.l00;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SdkDatabase_Impl extends SdkDatabase {
    @DexIgnore
    public volatile k00 f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends tf.a {
        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `DeviceFile` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `deviceMacAddress` TEXT NOT NULL, `fileType` INTEGER NOT NULL, `fileIndex` INTEGER NOT NULL, `rawData` BLOB NOT NULL, `fileLength` INTEGER NOT NULL, `fileCrc` INTEGER NOT NULL, `createdTimeStamp` INTEGER NOT NULL, `isCompleted` INTEGER NOT NULL)");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'f0501e661379ccab31d0b3858cee8e83')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `DeviceFile`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (SdkDatabase_Impl.this.mCallbacks != null) {
                int size = SdkDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) SdkDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = SdkDatabase_Impl.this.mDatabase = ggVar;
            SdkDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (SdkDatabase_Impl.this.mCallbacks != null) {
                int size = SdkDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) SdkDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            HashMap hashMap = new HashMap(9);
            hashMap.put("id", new eg.a("id", "INTEGER", true, 1));
            hashMap.put("deviceMacAddress", new eg.a("deviceMacAddress", "TEXT", true, 0));
            hashMap.put("fileType", new eg.a("fileType", "INTEGER", true, 0));
            hashMap.put("fileIndex", new eg.a("fileIndex", "INTEGER", true, 0));
            hashMap.put("rawData", new eg.a("rawData", "BLOB", true, 0));
            hashMap.put("fileLength", new eg.a("fileLength", "INTEGER", true, 0));
            hashMap.put("fileCrc", new eg.a("fileCrc", "INTEGER", true, 0));
            hashMap.put("createdTimeStamp", new eg.a("createdTimeStamp", "INTEGER", true, 0));
            hashMap.put("isCompleted", new eg.a("isCompleted", "INTEGER", true, 0));
            eg egVar = new eg("DeviceFile", hashMap, new HashSet(0), new HashSet(0));
            eg a2 = eg.a(ggVar, "DeviceFile");
            if (!egVar.equals(a2)) {
                throw new IllegalStateException("Migration didn't properly handle DeviceFile(com.fossil.blesdk.database.entity.DeviceFile).\n Expected:\n" + egVar + "\n Found:\n" + a2);
            }
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a2 = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a2.b("DELETE FROM `DeviceFile`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a2.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a2.x()) {
                a2.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), "DeviceFile");
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new a(3), "f0501e661379ccab31d0b3858cee8e83", "05778a001d8750bf160c2cf361f4e16f");
        hg.b.a a2 = hg.b.a(jfVar.b);
        a2.a(jfVar.c);
        a2.a((hg.a) tfVar);
        return jfVar.a.a(a2.a());
    }

    @DexIgnore
    public k00 a() {
        k00 k00;
        if (this.f != null) {
            return this.f;
        }
        synchronized (this) {
            if (this.f == null) {
                this.f = new l00(this);
            }
            k00 = this.f;
        }
        return k00;
    }
}
