package com.fossil.blesdk.hid;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.mb0;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HIDProfile {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ b b; // = new b();
    @DexIgnore
    public static BluetoothProfile c;
    @DexIgnore
    public static /* final */ BroadcastReceiver d; // = new a();
    @DexIgnore
    public static /* final */ HIDProfile e; // = new HIDProfile();

    @DexIgnore
    public enum LogEventName {
        HID_STATE_CHANGED;
        
        @DexIgnore
        public /* final */ String logName;

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (context != null && intent != null) {
                String action = intent.getAction();
                if (action != null && action.hashCode() == -1021360715 && action.equals("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED")) {
                    BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                    int intExtra = intent.getIntExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", 0);
                    int intExtra2 = intent.getIntExtra("android.bluetooth.profile.extra.STATE", 0);
                    if (bluetoothDevice != null) {
                        HIDProfile.e.b(bluetoothDevice, intExtra, intExtra2);
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements BluetoothProfile.ServiceListener {
        @DexIgnore
        public void onServiceConnected(int i, BluetoothProfile bluetoothProfile) {
            kd4.b(bluetoothProfile, "bluetoothProfile");
            t90 t90 = t90.c;
            String a = HIDProfile.a;
            t90.a(a, "onServiceConnected: profile=" + i + ", " + "proxy=" + bluetoothProfile + '.');
            if (i == 4) {
                HIDProfile.c = bluetoothProfile;
            }
        }

        @DexIgnore
        public void onServiceDisconnected(int i) {
            t90 t90 = t90.c;
            String a = HIDProfile.a;
            t90.a(a, "onServiceDisconnected: profile=" + i + '.');
            if (i == 4) {
                HIDProfile.c = null;
            }
        }
    }

    /*
    static {
        String simpleName = HIDProfile.class.getSimpleName();
        kd4.a((Object) simpleName, "HIDProfile::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final String a(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? "UNKNOWN" : "DISCONNECTING" : "CONNECTED" : "CONNECTING" : "DISCONNECTED";
    }

    @DexIgnore
    public final void b(BluetoothDevice bluetoothDevice, int i, int i2) {
        int i3 = i;
        int i4 = i2;
        t90 t90 = t90.c;
        String str = a;
        t90.a(str, "onHIDDeviceStateChanged: device=" + bluetoothDevice.getAddress() + ", " + a(i3) + '(' + i3 + ") to " + a(i4) + '(' + i4 + ')');
        da0 da0 = da0.l;
        String logName$blesdk_productionRelease = LogEventName.HID_STATE_CHANGED.getLogName$blesdk_productionRelease();
        EventType eventType = EventType.DEVICE_EVENT;
        String address = bluetoothDevice.getAddress();
        String str2 = address != null ? address : "";
        JSONObject jSONObject = new JSONObject();
        JSONKey jSONKey = JSONKey.MAC_ADDRESS;
        String address2 = bluetoothDevice.getAddress();
        if (address2 == null) {
            address2 = "";
        }
        SdkLogEntry sdkLogEntry = r4;
        SdkLogEntry sdkLogEntry2 = new SdkLogEntry(logName$blesdk_productionRelease, eventType, str2, "", "", true, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(wa0.a(wa0.a(jSONObject, jSONKey, address2), JSONKey.PREV_STATE, a(i3)), JSONKey.NEW_STATE, a(i4)), 448, (fd4) null);
        da0.b(sdkLogEntry);
        a(bluetoothDevice, i, i2);
    }

    @DexIgnore
    public final int c(BluetoothDevice bluetoothDevice) {
        kd4.b(bluetoothDevice, "bluetoothDevice");
        BluetoothProfile bluetoothProfile = c;
        if (bluetoothProfile != null) {
            return bluetoothProfile.getConnectionState(bluetoothDevice);
        }
        return 0;
    }

    @DexIgnore
    public final void a(BluetoothDevice bluetoothDevice, int i, int i2) {
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.hid.HIDProfile.action.CONNECTION_STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.hid.HIDProfile.extra.BLUETOOTH_DEVICE", bluetoothDevice);
        intent.putExtra("com.fossil.blesdk.hid.HIDProfile.extra.PREVIOUS_STATE", i);
        intent.putExtra("com.fossil.blesdk.hid.HIDProfile.extra.NEW_STATE", i2);
        mb0.a.a(intent);
    }

    @DexIgnore
    public final int a(BluetoothProfile bluetoothProfile, BluetoothDevice bluetoothDevice) throws Exception {
        Method method = bluetoothProfile.getClass().getMethod("getPriority", new Class[]{BluetoothDevice.class});
        if (method == null) {
            t90.c.a(a, "getPriority: localMethod NOT found!");
            return -1;
        }
        Object invoke = method.invoke(bluetoothProfile, new Object[]{bluetoothDevice});
        if (invoke != null) {
            int intValue = ((Integer) invoke).intValue();
            t90 t90 = t90.c;
            String str = a;
            t90.a(str, "BluetoothHidHost.getPriority: " + "device=" + bluetoothDevice.getAddress() + ", " + "priority=" + intValue);
            return intValue;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Int");
    }

    @DexIgnore
    public final void b(Context context) {
        context.registerReceiver(d, new IntentFilter("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"));
    }

    @DexIgnore
    public final int b(BluetoothDevice bluetoothDevice) {
        kd4.b(bluetoothDevice, "bluetoothDevice");
        BluetoothProfile bluetoothProfile = c;
        if (bluetoothProfile == null) {
            return 256;
        }
        try {
            Method method = bluetoothProfile.getClass().getMethod("disconnect", new Class[]{BluetoothDevice.class});
            if (method == null) {
                t90.c.a(a, "disconnect: localMethod NOT found!");
                return 511;
            }
            Object invoke = method.invoke(bluetoothProfile, new Object[]{bluetoothDevice});
            if (invoke != null) {
                boolean booleanValue = ((Boolean) invoke).booleanValue();
                t90 t90 = t90.c;
                String str = a;
                t90.a(str, "disconnectDevice: success=" + booleanValue);
                if (booleanValue) {
                    return 0;
                }
                return 511;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Boolean");
        } catch (NoSuchMethodException e2) {
            t90 t902 = t90.c;
            String str2 = a;
            t902.a(str2, "disconnectDevice got exception: " + e2.getLocalizedMessage());
            da0.l.a(e2);
            return 257;
        } catch (Exception e3) {
            t90 t903 = t90.c;
            String str3 = a;
            t903.a(str3, "disconnectDevice got exception: " + e3.getLocalizedMessage());
            da0.l.a(e3);
            return 511;
        }
    }

    @DexIgnore
    public final void a(Context context) {
        kd4.b(context, "context");
        int profileConnectionState = BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(4);
        boolean profileProxy = BluetoothAdapter.getDefaultAdapter().getProfileProxy(context, b, 4);
        t90 t90 = t90.c;
        String str = a;
        t90.a(str, "setUp: profileConnectionState=" + profileConnectionState + ", " + "getProxy=" + profileProxy);
        b(context);
    }

    @DexIgnore
    public final int a(BluetoothDevice bluetoothDevice) {
        kd4.b(bluetoothDevice, "bluetoothDevice");
        BluetoothProfile bluetoothProfile = c;
        if (bluetoothProfile == null) {
            return 256;
        }
        try {
            int a2 = e.a(bluetoothProfile, bluetoothDevice);
            t90 t90 = t90.c;
            String str = a;
            t90.a(str, "getPriority: device=" + bluetoothDevice.getAddress() + ", " + "priority=" + a2);
            if (a2 == 0) {
                return 258;
            }
            Method method = bluetoothProfile.getClass().getMethod("connect", new Class[]{BluetoothDevice.class});
            if (method == null) {
                t90.c.a(a, "connect: localMethod NOT found");
                return 257;
            }
            Object invoke = method.invoke(bluetoothProfile, new Object[]{bluetoothDevice});
            if (invoke != null) {
                boolean booleanValue = ((Boolean) invoke).booleanValue();
                t90 t902 = t90.c;
                String str2 = a;
                t902.a(str2, "connectDevice: success=" + booleanValue);
                if (booleanValue) {
                    return 0;
                }
                return 511;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Boolean");
        } catch (NoSuchMethodException e2) {
            t90 t903 = t90.c;
            String str3 = a;
            t903.a(str3, "connectDevice got exception: " + e2.getLocalizedMessage());
            da0.l.a(e2);
        } catch (Exception e3) {
            t90 t904 = t90.c;
            String str4 = a;
            t904.a(str4, "connectDevice got exception: " + e3.getLocalizedMessage());
            e3.printStackTrace();
            da0.l.a(e3);
            return 511;
        }
    }

    @DexIgnore
    public final List<BluetoothDevice> a() {
        BluetoothProfile bluetoothProfile = c;
        if (bluetoothProfile != null) {
            List<BluetoothDevice> connectedDevices = bluetoothProfile.getConnectedDevices();
            if (connectedDevices != null) {
                return connectedDevices;
            }
        }
        return new ArrayList();
    }
}
