package com.fossil.blesdk.contract;

import com.fossil.blesdk.obfuscated.kd4;
import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class JSONAbleObject implements Serializable {
    @DexIgnore
    public static /* synthetic */ String toJSONString$default(JSONAbleObject jSONAbleObject, int i, int i2, Object obj) throws JSONException {
        if (obj == null) {
            if ((i2 & 1) != 0) {
                i = 2;
            }
            return jSONAbleObject.toJSONString(i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: toJSONString");
    }

    @DexIgnore
    public abstract JSONObject toJSONObject() throws JSONException;

    @DexIgnore
    public final String toJSONString(int i) throws JSONException {
        if (i == 0) {
            String jSONObject = toJSONObject().toString();
            kd4.a((Object) jSONObject, "toJSONObject().toString()");
            return jSONObject;
        }
        String jSONObject2 = toJSONObject().toString(i);
        kd4.a((Object) jSONObject2, "toJSONObject().toString(indentSpace)");
        return jSONObject2;
    }

    @DexIgnore
    public String toString() {
        try {
            return toJSONString(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        throw null;
    }
}
