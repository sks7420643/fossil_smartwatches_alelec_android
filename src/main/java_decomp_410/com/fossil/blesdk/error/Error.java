package com.fossil.blesdk.error;

import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class Error extends JSONAbleObject {
    @DexIgnore
    public /* final */ h90 errorCode;

    @DexIgnore
    public Error(h90 h90) {
        kd4.b(h90, "errorCode");
        this.errorCode = h90;
    }

    @DexIgnore
    public h90 getErrorCode() {
        return this.errorCode;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        try {
            wa0.a(jSONObject, JSONKey.ERROR_CODE, getErrorCode().getLogName());
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }
}
