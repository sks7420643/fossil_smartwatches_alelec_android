package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum SleepState {
    AWAKE,
    SLEEP,
    DEEP_SLEEP,
    UNKNOWN
}
