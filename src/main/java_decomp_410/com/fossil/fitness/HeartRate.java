package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRate implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<HeartRate> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ short mAverage;
    @DexIgnore
    public /* final */ int mResolutionInSecond;
    @DexIgnore
    public /* final */ ArrayList<Short> mValues;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<HeartRate> {
        @DexIgnore
        public HeartRate createFromParcel(Parcel parcel) {
            return new HeartRate(parcel);
        }

        @DexIgnore
        public HeartRate[] newArray(int i) {
            return new HeartRate[i];
        }
    }

    @DexIgnore
    public HeartRate(int i, ArrayList<Short> arrayList, short s) {
        this.mResolutionInSecond = i;
        this.mValues = arrayList;
        this.mAverage = s;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof HeartRate)) {
            return false;
        }
        HeartRate heartRate = (HeartRate) obj;
        if (this.mResolutionInSecond == heartRate.mResolutionInSecond && this.mValues.equals(heartRate.mValues) && this.mAverage == heartRate.mAverage) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public short getAverage() {
        return this.mAverage;
    }

    @DexIgnore
    public int getResolutionInSecond() {
        return this.mResolutionInSecond;
    }

    @DexIgnore
    public ArrayList<Short> getValues() {
        return this.mValues;
    }

    @DexIgnore
    public int hashCode() {
        return ((((527 + this.mResolutionInSecond) * 31) + this.mValues.hashCode()) * 31) + this.mAverage;
    }

    @DexIgnore
    public String toString() {
        return "HeartRate{mResolutionInSecond=" + this.mResolutionInSecond + ",mValues=" + this.mValues + ",mAverage=" + this.mAverage + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mResolutionInSecond);
        parcel.writeList(this.mValues);
        parcel.writeInt(this.mAverage);
    }

    @DexIgnore
    public HeartRate(Parcel parcel) {
        this.mResolutionInSecond = parcel.readInt();
        this.mValues = new ArrayList<>();
        parcel.readList(this.mValues, HeartRate.class.getClassLoader());
        this.mAverage = (short) parcel.readInt();
    }
}
