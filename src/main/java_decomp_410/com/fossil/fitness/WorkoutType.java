package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum WorkoutType {
    UNKNOWN,
    RUNNING,
    CYCLING,
    TREADMILL,
    ELLIPTICAL,
    WEIGHTS,
    WORKOUT,
    YOGA
}
