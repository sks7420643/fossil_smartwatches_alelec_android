package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BinaryFile implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<BinaryFile> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ byte[] mData;
    @DexIgnore
    public /* final */ int mSynctime;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<BinaryFile> {
        @DexIgnore
        public BinaryFile createFromParcel(Parcel parcel) {
            return new BinaryFile(parcel);
        }

        @DexIgnore
        public BinaryFile[] newArray(int i) {
            return new BinaryFile[i];
        }
    }

    @DexIgnore
    public BinaryFile(byte[] bArr, int i) {
        this.mData = bArr;
        this.mSynctime = i;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof BinaryFile)) {
            return false;
        }
        BinaryFile binaryFile = (BinaryFile) obj;
        if (!Arrays.equals(this.mData, binaryFile.mData) || this.mSynctime != binaryFile.mSynctime) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public byte[] getData() {
        return this.mData;
    }

    @DexIgnore
    public int getSynctime() {
        return this.mSynctime;
    }

    @DexIgnore
    public int hashCode() {
        return ((527 + Arrays.hashCode(this.mData)) * 31) + this.mSynctime;
    }

    @DexIgnore
    public String toString() {
        return "BinaryFile{mData=" + this.mData + ",mSynctime=" + this.mSynctime + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByteArray(this.mData);
        parcel.writeInt(this.mSynctime);
    }

    @DexIgnore
    public BinaryFile(Parcel parcel) {
        this.mData = parcel.createByteArray();
        this.mSynctime = parcel.readInt();
    }
}
