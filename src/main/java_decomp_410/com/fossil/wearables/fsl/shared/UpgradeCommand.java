package com.fossil.wearables.fsl.shared;

import android.database.sqlite.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface UpgradeCommand {
    @DexIgnore
    void execute(SQLiteDatabase sQLiteDatabase);
}
