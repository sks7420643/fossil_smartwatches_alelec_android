package com.fossil.wearables.fsl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface BaseProvider {
    @DexIgnore
    String getDbPath();
}
