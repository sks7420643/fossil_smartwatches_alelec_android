package com.fossil.wearables.fsl.keyvalue;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface KeyValueProvider {
    @DexIgnore
    List<KeyValue> getAllKeyValues();

    @DexIgnore
    KeyValue getKeyValueById(int i);

    @DexIgnore
    String getValueByKey(String str);

    @DexIgnore
    void removeAllKeyValues();

    @DexIgnore
    void removeKeyValue(KeyValue keyValue);

    @DexIgnore
    void saveKeyValue(KeyValue keyValue);
}
