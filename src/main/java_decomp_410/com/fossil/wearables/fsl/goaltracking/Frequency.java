package com.fossil.wearables.fsl.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum Frequency {
    UNKNOWN(-1),
    DAILY(0),
    WEEKLY(1);
    
    @DexIgnore
    public int value;

    @DexIgnore
    Frequency(int i) {
        this.value = i;
    }

    @DexIgnore
    public static Frequency fromInt(int i) {
        for (Frequency frequency : values()) {
            if (frequency.getValue() == i) {
                return frequency;
            }
        }
        return UNKNOWN;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
