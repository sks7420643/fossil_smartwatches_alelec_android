package com.fossil.wearables.fsl.sleep;

import com.fossil.wearables.fsl.BaseProvider;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface MFSleepSessionProvider extends BaseProvider {
    @DexIgnore
    boolean addJustSleepSession(MFSleepSession mFSleepSession);

    @DexIgnore
    boolean addSleepDay(MFSleepDay mFSleepDay);

    @DexIgnore
    boolean addSleepSession(MFSleepSession mFSleepSession);

    @DexIgnore
    void deleteAllSleepDays();

    @DexIgnore
    void deleteAllSleepSessions();

    @DexIgnore
    boolean deleteSleepSession(long j);

    @DexIgnore
    boolean deleteSleepSession(MFSleepSession mFSleepSession);

    @DexIgnore
    boolean editSleepSession(MFSleepSession mFSleepSession);

    @DexIgnore
    MFSleepGoal getDailySleepGoal(String str);

    @DexIgnore
    int getLastSleepGoal();

    @DexIgnore
    int getLastSleepGoalFromDate(String str);

    @DexIgnore
    List<MFSleepSession> getPendingSleepSessions();

    @DexIgnore
    MFSleepDay getSleepDay(String str);

    @DexIgnore
    List<MFSleepDay> getSleepDayInRange(String str, String str2);

    @DexIgnore
    List<MFSleepDay> getSleepDays(long j, long j2);

    @DexIgnore
    MFSleepSession getSleepSession(long j);

    @DexIgnore
    int getSleepSessionNumberFromSleepDay(long j);

    @DexIgnore
    List<MFSleepSession> getSleepSessions(long j);

    @DexIgnore
    List<MFSleepSession> getSleepSessions(long j, long j2);

    @DexIgnore
    List<MFSleepSession> getSleepSessions(String str);

    @DexIgnore
    int getTodaySleepGoal();

    @DexIgnore
    MFSleepGoal updateDailySleepGoal(MFSleepGoal mFSleepGoal);

    @DexIgnore
    void updateSleepDay(MFSleepDay mFSleepDay);

    @DexIgnore
    void updateSleepSessionPinType(MFSleepSession mFSleepSession, int i);
}
