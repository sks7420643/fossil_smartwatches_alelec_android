package com.fossil.wearables.fsl.location;

import com.fossil.wearables.fsl.BaseProvider;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface LocationProvider extends BaseProvider {
    @DexIgnore
    List<DeviceLocation> getAllDevicesLocation();

    @DexIgnore
    List<LocationGroup> getAllLocationGroups();

    @DexIgnore
    DeviceLocation getDeviceLocation(String str);

    @DexIgnore
    Location getLocation(int i);

    @DexIgnore
    LocationGroup getLocationGroup(int i);

    @DexIgnore
    List<Location> getLocations(int i);

    @DexIgnore
    List<Location> getLocations(int i, boolean z);

    @DexIgnore
    List<Location> getLocations(List<Location> list, boolean z);

    @DexIgnore
    void removeDeviceLocation(DeviceLocation deviceLocation);

    @DexIgnore
    void removeDeviceLocation(String str);

    @DexIgnore
    void removeLocation(Location location);

    @DexIgnore
    void removeLocationGroup(LocationGroup locationGroup);

    @DexIgnore
    void saveDeviceLocation(DeviceLocation deviceLocation);

    @DexIgnore
    void saveLocation(Location location);

    @DexIgnore
    void saveLocationGroup(LocationGroup locationGroup);
}
