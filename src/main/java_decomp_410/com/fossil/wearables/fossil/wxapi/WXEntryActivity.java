package com.fossil.wearables.fossil.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.blesdk.obfuscated.hz3;
import com.fossil.blesdk.obfuscated.iz3;
import com.fossil.blesdk.obfuscated.us3;
import com.fossil.blesdk.obfuscated.xz3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class WXEntryActivity extends Activity implements xz3 {
    /*
    static {
        Class<WXEntryActivity> cls = WXEntryActivity.class;
    }
    */

    @DexIgnore
    public void a(hz3 hz3) {
        us3.a().a(hz3);
        finish();
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        us3.a().a(getIntent(), this);
    }

    @DexIgnore
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        us3.a().a(getIntent(), this);
    }

    @DexIgnore
    public void a(iz3 iz3) {
        us3.a().a(iz3);
        finish();
    }
}
