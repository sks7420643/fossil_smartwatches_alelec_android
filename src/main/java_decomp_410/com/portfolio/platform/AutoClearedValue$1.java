package com.portfolio.platform;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AutoClearedValue$1 implements com.fossil.blesdk.obfuscated.C3179wb {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.t52 f20900a;

    @DexIgnore
    @com.fossil.blesdk.obfuscated.C1613dc(androidx.lifecycle.Lifecycle.Event.ON_DESTROY)
    public final void onDestroy() {
        this.f20900a.mo31128a(null);
        throw null;
    }
}
