package com.portfolio.platform.util;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.ServerError;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$Anon1", f = "NetworkBoundResource.kt", l = {62, 69, 72, 92, 102, 115}, m = "invokeSuspend")
public final class NetworkBoundResource$fetchFromNetwork$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ LiveData $dbSource;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NetworkBoundResource this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$Anon1$Anon1", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NetworkBoundResource$fetchFromNetwork$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements cc<S> {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 a;

            @DexIgnore
            public a(Anon1 anon1) {
                this.a = anon1;
            }

            @DexIgnore
            public final void a(ResultType resulttype) {
                this.a.this$Anon0.this$Anon0.setValue(os3.e.b(resulttype));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(NetworkBoundResource$fetchFromNetwork$Anon1 networkBoundResource$fetchFromNetwork$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = networkBoundResource$fetchFromNetwork$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.this$Anon0.result.a(this.this$Anon0.$dbSource, new a(this));
                this.this$Anon0.this$Anon0.result.a(this.this$Anon0.$dbSource);
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$Anon1$Anon2", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NetworkBoundResource$fetchFromNetwork$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements cc<S> {
            @DexIgnore
            public /* final */ /* synthetic */ Anon2 a;

            @DexIgnore
            public a(Anon2 anon2) {
                this.a = anon2;
            }

            @DexIgnore
            public final void a(ResultType resulttype) {
                this.a.this$Anon0.this$Anon0.setValue(os3.e.c(resulttype));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(NetworkBoundResource$fetchFromNetwork$Anon1 networkBoundResource$fetchFromNetwork$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = networkBoundResource$fetchFromNetwork$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, yb4);
            anon2.p$ = (zg4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                FLogger.INSTANCE.getLocal().d("NetworkBoundResource", "set value dbSource fetched from network response null");
                this.this$Anon0.this$Anon0.result.a(this.this$Anon0.this$Anon0.loadFromDb(), new a(this));
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$Anon1$Anon3", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon3 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NetworkBoundResource$fetchFromNetwork$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements cc<S> {
            @DexIgnore
            public /* final */ /* synthetic */ Anon3 a;

            @DexIgnore
            public a(Anon3 anon3) {
                this.a = anon3;
            }

            @DexIgnore
            public final void a(ResultType resulttype) {
                this.a.this$Anon0.this$Anon0.setValue(os3.e.c(resulttype));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3(NetworkBoundResource$fetchFromNetwork$Anon1 networkBoundResource$fetchFromNetwork$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = networkBoundResource$fetchFromNetwork$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon3 anon3 = new Anon3(this.this$Anon0, yb4);
            anon3.p$ = (zg4) obj;
            return anon3;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon3) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                FLogger.INSTANCE.getLocal().d("NetworkBoundResource", "set value dbSource fetched from network success");
                this.this$Anon0.this$Anon0.result.a(this.this$Anon0.this$Anon0.loadFromDb(), new a(this));
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$Anon1$Anon4", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon4 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NetworkBoundResource$fetchFromNetwork$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements cc<S> {
            @DexIgnore
            public /* final */ /* synthetic */ Anon4 a;

            @DexIgnore
            public a(Anon4 anon4) {
                this.a = anon4;
            }

            @DexIgnore
            public final void a(ResultType resulttype) {
                this.a.this$Anon0.this$Anon0.setValue(os3.e.b(resulttype));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon4(NetworkBoundResource$fetchFromNetwork$Anon1 networkBoundResource$fetchFromNetwork$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = networkBoundResource$fetchFromNetwork$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon4 anon4 = new Anon4(this.this$Anon0, yb4);
            anon4.p$ = (zg4) obj;
            return anon4;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon4) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.this$Anon0.result.a(this.this$Anon0.this$Anon0.loadFromDb(), new a(this));
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$Anon1$Anon5", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon5 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ qo2 $response;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NetworkBoundResource$fetchFromNetwork$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements cc<S> {
            @DexIgnore
            public /* final */ /* synthetic */ Anon5 a;

            @DexIgnore
            public a(Anon5 anon5) {
                this.a = anon5;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:3:0x0020, code lost:
                if (r3 != null) goto L_0x0035;
             */
            @DexIgnore
            public final void a(ResultType resulttype) {
                String str;
                Anon5 anon5 = this.a;
                NetworkBoundResource networkBoundResource = anon5.this$Anon0.this$Anon0;
                os3.a aVar = os3.e;
                int a2 = ((po2) anon5.$response).a();
                ServerError c = ((po2) this.a.$response).c();
                if (c != null) {
                    str = c.getUserMessage();
                }
                ServerError c2 = ((po2) this.a.$response).c();
                str = c2 != null ? c2.getMessage() : null;
                if (str == null) {
                    str = "";
                }
                networkBoundResource.setValue(aVar.a(a2, str, resulttype));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon5(NetworkBoundResource$fetchFromNetwork$Anon1 networkBoundResource$fetchFromNetwork$Anon1, qo2 qo2, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = networkBoundResource$fetchFromNetwork$Anon1;
            this.$response = qo2;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon5 anon5 = new Anon5(this.this$Anon0, this.$response, yb4);
            anon5.p$ = (zg4) obj;
            return anon5;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon5) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.this$Anon0.result.a(this.this$Anon0.$dbSource, new a(this));
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NetworkBoundResource$fetchFromNetwork$Anon1(NetworkBoundResource networkBoundResource, LiveData liveData, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = networkBoundResource;
        this.$dbSource = liveData;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NetworkBoundResource$fetchFromNetwork$Anon1 networkBoundResource$fetchFromNetwork$Anon1 = new NetworkBoundResource$fetchFromNetwork$Anon1(this.this$Anon0, this.$dbSource, yb4);
        networkBoundResource$fetchFromNetwork$Anon1.p$ = (zg4) obj;
        return networkBoundResource$fetchFromNetwork$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NetworkBoundResource$fetchFromNetwork$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x004a, code lost:
        r7 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$Anon1$response$Anon1(r6, (com.fossil.blesdk.obfuscated.yb4) null);
        r6.L$Anon0 = r1;
        r6.label = 2;
        r7 = com.portfolio.platform.response.ResponseKt.a(r7, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0058, code lost:
        if (r7 != r0) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005a, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x005b, code lost:
        r7 = (com.fossil.blesdk.obfuscated.qo2) r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005f, code lost:
        if ((r7 instanceof com.fossil.blesdk.obfuscated.ro2) == false) goto L_0x010c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0061, code lost:
        r3 = (com.fossil.blesdk.obfuscated.ro2) r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0068, code lost:
        if (r3.a() != null) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006a, code lost:
        r3 = com.fossil.blesdk.obfuscated.nh4.c();
        r4 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$Anon1.Anon2(r6, (com.fossil.blesdk.obfuscated.yb4) null);
        r6.L$Anon0 = r1;
        r6.L$Anon1 = r7;
        r6.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x007e, code lost:
        if (com.fossil.blesdk.obfuscated.yf4.a(r3, r4, r6) != r0) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0080, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0081, code lost:
        r4 = r6.this$Anon0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x008b, code lost:
        if (r4.processContinueFetching(r4.processResponse(r3)) == false) goto L_0x00b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0093, code lost:
        if (r6.this$Anon0.isFromCache == false) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0095, code lost:
        r6.this$Anon0.isFromCache = r3.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a2, code lost:
        if (r3.b() != false) goto L_0x00ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a4, code lost:
        r7 = r6.this$Anon0;
        r7.saveCallResult(r7.processResponse(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ad, code lost:
        r6.this$Anon0.fetchFromNetwork(r6.$dbSource);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00bc, code lost:
        if (r6.this$Anon0.isFromCache == false) goto L_0x00c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00be, code lost:
        r6.this$Anon0.isFromCache = r3.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00cb, code lost:
        if (r3.b() != false) goto L_0x00d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00cd, code lost:
        r4 = r6.this$Anon0;
        r4.saveCallResult(r4.processResponse(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00dc, code lost:
        if (r6.this$Anon0.isFromCache != false) goto L_0x00f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00de, code lost:
        r3 = com.fossil.blesdk.obfuscated.nh4.c();
        r4 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$Anon1.Anon3(r6, (com.fossil.blesdk.obfuscated.yb4) null);
        r6.L$Anon0 = r1;
        r6.L$Anon1 = r7;
        r6.label = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00f2, code lost:
        if (com.fossil.blesdk.obfuscated.yf4.a(r3, r4, r6) != r0) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00f4, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00f5, code lost:
        r3 = com.fossil.blesdk.obfuscated.nh4.c();
        r4 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$Anon1.Anon4(r6, (com.fossil.blesdk.obfuscated.yb4) null);
        r6.L$Anon0 = r1;
        r6.L$Anon1 = r7;
        r6.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0109, code lost:
        if (com.fossil.blesdk.obfuscated.yf4.a(r3, r4, r6) != r0) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x010b, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x010e, code lost:
        if ((r7 instanceof com.fossil.blesdk.obfuscated.po2) == false) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0110, code lost:
        r6.this$Anon0.onFetchFailed(((com.fossil.blesdk.obfuscated.po2) r7).d());
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d("NetworkBoundResource", "set value dbSource fetched from network failed");
        r3 = com.fossil.blesdk.obfuscated.nh4.c();
        r4 = new com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$Anon1.Anon5(r6, r7, (com.fossil.blesdk.obfuscated.yb4) null);
        r6.L$Anon0 = r1;
        r6.L$Anon1 = r7;
        r6.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x013d, code lost:
        if (com.fossil.blesdk.obfuscated.yf4.a(r3, r4, r6) != r0) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x013f, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0142, code lost:
        return com.fossil.blesdk.obfuscated.qa4.a;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        Object a = cc4.a();
        switch (this.label) {
            case 0:
                na4.a(obj);
                zg4 zg42 = this.p$;
                pi4 c = nh4.c();
                Anon1 anon1 = new Anon1(this, (yb4) null);
                this.L$Anon0 = zg42;
                this.label = 1;
                if (yf4.a(c, anon1, this) != a) {
                    zg4 = zg42;
                    break;
                } else {
                    return a;
                }
            case 1:
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
                break;
            case 2:
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
                break;
            case 3:
            case 4:
            case 5:
            case 6:
                qo2 qo2 = (qo2) this.L$Anon1;
                zg4 zg43 = (zg4) this.L$Anon0;
                na4.a(obj);
                break;
            default:
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
