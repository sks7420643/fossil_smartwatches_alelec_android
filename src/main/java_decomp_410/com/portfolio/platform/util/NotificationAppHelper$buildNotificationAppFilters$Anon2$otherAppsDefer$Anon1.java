package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ls3;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.vy2;
import com.fossil.blesdk.obfuscated.w52;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.portfolio.platform.CoroutineUseCase;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1", f = "NotificationAppHelper.kt", l = {145}, m = "invokeSuspend")
public final class NotificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<AppNotificationFilter>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isAllAppToggleEnabled;
    @DexIgnore
    public /* final */ /* synthetic */ List $notificationAllFilterList;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationAppHelper$buildNotificationAppFilters$Anon2 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1(NotificationAppHelper$buildNotificationAppFilters$Anon2 notificationAppHelper$buildNotificationAppFilters$Anon2, List list, boolean z, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = notificationAppHelper$buildNotificationAppFilters$Anon2;
        this.$notificationAllFilterList = list;
        this.$isAllAppToggleEnabled = z;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NotificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1 notificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1 = new NotificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1(this.this$Anon0, this.$notificationAllFilterList, this.$isAllAppToggleEnabled, yb4);
        notificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1.p$ = (zg4) obj;
        return notificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        List list;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ArrayList arrayList = new ArrayList();
            vy2 vy2 = this.this$Anon0.$getApps;
            this.L$Anon0 = zg4;
            this.L$Anon1 = arrayList;
            this.label = 1;
            obj = w52.a(vy2, null, this);
            if (obj == a) {
                return a;
            }
            list = arrayList;
        } else if (i == 1) {
            list = (List) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        CoroutineUseCase.c cVar = (CoroutineUseCase.c) obj;
        if (cVar instanceof vy2.a) {
            this.$notificationAllFilterList.addAll(ls3.a(((vy2.a) cVar).a(), this.$isAllAppToggleEnabled));
        }
        return list;
    }
}
