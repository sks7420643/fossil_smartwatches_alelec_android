package com.portfolio.platform.util;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ac;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class NetworkBoundResource<ResultType, RequestType> {
    @DexIgnore
    public boolean isFromCache; // = true;
    @DexIgnore
    public /* final */ ac<os3<ResultType>> result; // = new ac<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1<T> implements cc<S> {
        @DexIgnore
        public /* final */ /* synthetic */ NetworkBoundResource a;
        @DexIgnore
        public /* final */ /* synthetic */ LiveData b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.NetworkBoundResource$Anon1$Anon1")
        @gc4(c = "com.portfolio.platform.util.NetworkBoundResource$Anon1$Anon1", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.util.NetworkBoundResource$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0158Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.NetworkBoundResource$Anon1$Anon1$a")
            /* renamed from: com.portfolio.platform.util.NetworkBoundResource$Anon1$Anon1$a */
            public static final class a<T> implements cc<S> {
                @DexIgnore
                public /* final */ /* synthetic */ C0158Anon1 a;

                @DexIgnore
                public a(C0158Anon1 anon1) {
                    this.a = anon1;
                }

                @DexIgnore
                public final void a(ResultType resulttype) {
                    this.a.this$Anon0.a.setValue(os3.e.c(resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0158Anon1(Anon1 anon1, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0158Anon1 anon1 = new C0158Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0158Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                cc4.a();
                if (this.label == 0) {
                    na4.a(obj);
                    this.this$Anon0.a.result.a(this.this$Anon0.b, new a(this));
                    return qa4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public Anon1(NetworkBoundResource networkBoundResource, LiveData liveData) {
            this.a = networkBoundResource;
            this.b = liveData;
        }

        @DexIgnore
        public final void a(ResultType resulttype) {
            this.a.result.a(this.b);
            if (this.a.shouldFetch(resulttype)) {
                this.a.fetchFromNetwork(this.b);
            } else {
                fi4 unused = ag4.b(ah4.a(nh4.c()), (CoroutineContext) null, (CoroutineStart) null, new C0158Anon1(this, (yb4) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    public NetworkBoundResource() {
        this.result.b(os3.e.a(null));
        this.isFromCache = true;
        LiveData loadFromDb = loadFromDb();
        this.result.a(loadFromDb, new Anon1(this, loadFromDb));
    }

    @DexIgnore
    private final void fetchFromNetwork(LiveData<ResultType> liveData) {
        fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new NetworkBoundResource$fetchFromNetwork$Anon1(this, liveData, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    private final void setValue(os3<? extends ResultType> os3) {
        if (!kd4.a((Object) this.result.a(), (Object) os3)) {
            this.result.b(os3);
        }
    }

    @DexIgnore
    public final LiveData<os3<ResultType>> asLiveData() {
        ac<os3<ResultType>> acVar = this.result;
        if (acVar != null) {
            return acVar;
        }
        throw new TypeCastException("null cannot be cast to non-null type androidx.lifecycle.LiveData<com.portfolio.platform.util.Resource<ResultType>>");
    }

    @DexIgnore
    public abstract Object createCall(yb4<? super qr4<RequestType>> yb4);

    @DexIgnore
    public abstract LiveData<ResultType> loadFromDb();

    @DexIgnore
    public abstract void onFetchFailed(Throwable th);

    @DexIgnore
    public boolean processContinueFetching(RequestType requesttype) {
        return false;
    }

    @DexIgnore
    public RequestType processResponse(ro2<RequestType> ro2) {
        kd4.b(ro2, "response");
        RequestType a = ro2.a();
        if (a != null) {
            return a;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public abstract void saveCallResult(RequestType requesttype);

    @DexIgnore
    public abstract boolean shouldFetch(ResultType resulttype);
}
