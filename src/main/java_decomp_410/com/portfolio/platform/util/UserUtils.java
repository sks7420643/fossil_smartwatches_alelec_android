package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.la4;
import com.fossil.blesdk.obfuscated.le4;
import com.fossil.blesdk.obfuscated.ma4;
import com.fossil.blesdk.obfuscated.md4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.internal.PropertyReference1;
import kotlin.jvm.internal.PropertyReference1Impl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UserUtils {
    @DexIgnore
    public static /* final */ la4 b; // = ma4.a(UserUtils$Companion$INSTANCE$Anon2.INSTANCE);
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ a d; // = new a((fd4) null);
    @DexIgnore
    public en2 a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ /* synthetic */ le4[] a;

        /*
        static {
            PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(md4.a(a.class), "INSTANCE", "getINSTANCE()Lcom/portfolio/platform/util/UserUtils;");
            md4.a((PropertyReference1) propertyReference1Impl);
            a = new le4[]{propertyReference1Impl};
        }
        */

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final UserUtils a() {
            la4 b = UserUtils.b;
            a aVar = UserUtils.d;
            le4 le4 = a[0];
            return (UserUtils) b.getValue();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ UserUtils a; // = new UserUtils();
        @DexIgnore
        public static /* final */ b b; // = new b();

        @DexIgnore
        public final UserUtils a() {
            return a;
        }
    }

    /*
    static {
        String simpleName = UserUtils.class.getSimpleName();
        kd4.a((Object) simpleName, "UserUtils::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public UserUtils() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final boolean a(int i, int i2) {
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        long currentTimeMillis = System.currentTimeMillis();
        en2 en2 = this.a;
        if (en2 != null) {
            int days = (int) timeUnit.toDays(currentTimeMillis - en2.n());
            FLogger.INSTANCE.getLocal().d(c, "Inside .isHappyUser");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = c;
            local.d(str, "crashThreshold = " + i + ", " + "successfulSyncThreshold = " + i2 + ", delta in Day = " + days);
            if (days >= i) {
                en2 en22 = this.a;
                if (en22 == null) {
                    kd4.d("mSharePrefs");
                    throw null;
                } else if (en22.e() >= i2) {
                    return true;
                }
            }
            return false;
        }
        kd4.d("mSharePrefs");
        throw null;
    }

    @DexIgnore
    public final boolean a() {
        en2 en2 = this.a;
        if (en2 != null) {
            return en2.K();
        }
        kd4.d("mSharePrefs");
        throw null;
    }
}
