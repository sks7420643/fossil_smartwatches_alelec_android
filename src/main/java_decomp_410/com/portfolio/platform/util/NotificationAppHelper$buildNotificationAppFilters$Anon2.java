package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.vy2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$IntRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$Anon2", f = "NotificationAppHelper.kt", l = {152, 153}, m = "invokeSuspend")
public final class NotificationAppHelper$buildNotificationAppFilters$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super List<AppNotificationFilter>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ px2 $getAllContactGroup;
    @DexIgnore
    public /* final */ /* synthetic */ vy2 $getApps;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationSettingsDatabase $notificationSettingsDatabase;
    @DexIgnore
    public /* final */ /* synthetic */ en2 $sharedPrefs;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public Object L$Anon8;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationAppHelper$buildNotificationAppFilters$Anon2(en2 en2, NotificationSettingsDatabase notificationSettingsDatabase, px2 px2, vy2 vy2, yb4 yb4) {
        super(2, yb4);
        this.$sharedPrefs = en2;
        this.$notificationSettingsDatabase = notificationSettingsDatabase;
        this.$getAllContactGroup = px2;
        this.$getApps = vy2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NotificationAppHelper$buildNotificationAppFilters$Anon2 notificationAppHelper$buildNotificationAppFilters$Anon2 = new NotificationAppHelper$buildNotificationAppFilters$Anon2(this.$sharedPrefs, this.$notificationSettingsDatabase, this.$getAllContactGroup, this.$getApps, yb4);
        notificationAppHelper$buildNotificationAppFilters$Anon2.p$ = (zg4) obj;
        return notificationAppHelper$buildNotificationAppFilters$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationAppHelper$buildNotificationAppFilters$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        List list;
        Object obj2;
        List list2;
        gh4 gh4;
        gh4 gh42;
        Ref$IntRef ref$IntRef;
        Ref$IntRef ref$IntRef2;
        NotificationSettingsModel notificationSettingsModel;
        boolean z;
        NotificationSettingsModel notificationSettingsModel2;
        zg4 zg4;
        Object obj3;
        List list3;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg42 = this.p$;
            ref$IntRef = new Ref$IntRef();
            ref$IntRef.element = 0;
            ref$IntRef2 = new Ref$IntRef();
            ref$IntRef2.element = 0;
            list = new ArrayList();
            z = this.$sharedPrefs.A();
            notificationSettingsModel2 = this.$notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(true);
            notificationSettingsModel = this.$notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationAppHelper.a;
            local.d(a2, "buildNotificationAppFilters(), callSettingsTypeModel = " + notificationSettingsModel2 + ", messageSettingsTypeModel = " + notificationSettingsModel + ", isAllAppToggleEnabled = " + z);
            if (notificationSettingsModel2 != null) {
                ref$IntRef.element = notificationSettingsModel2.getSettingsType();
            } else {
                this.$notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowCallsFrom", 0, true));
            }
            if (notificationSettingsModel != null) {
                ref$IntRef2.element = notificationSettingsModel.getSettingsType();
            } else {
                this.$notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowMessagesFrom", 0, false));
            }
            gh4 a3 = ag4.a(zg42, (CoroutineContext) null, (CoroutineStart) null, new NotificationAppHelper$buildNotificationAppFilters$Anon2$contactMessageDefer$Anon1(this, ref$IntRef, ref$IntRef2, (yb4) null), 3, (Object) null);
            gh42 = ag4.a(zg42, (CoroutineContext) null, (CoroutineStart) null, new NotificationAppHelper$buildNotificationAppFilters$Anon2$otherAppsDefer$Anon1(this, list, z, (yb4) null), 3, (Object) null);
            this.L$Anon0 = zg42;
            this.L$Anon1 = ref$IntRef;
            this.L$Anon2 = ref$IntRef2;
            this.L$Anon3 = list;
            this.Z$Anon0 = z;
            this.L$Anon4 = notificationSettingsModel2;
            this.L$Anon5 = notificationSettingsModel;
            this.L$Anon6 = a3;
            this.L$Anon7 = gh42;
            this.L$Anon8 = list;
            this.label = 1;
            obj3 = a3.a(this);
            if (obj3 == a) {
                return a;
            }
            gh4 = a3;
            zg4 = zg42;
            list3 = list;
        } else if (i == 1) {
            list3 = (List) this.L$Anon8;
            notificationSettingsModel2 = (NotificationSettingsModel) this.L$Anon4;
            z = this.Z$Anon0;
            ref$IntRef2 = (Ref$IntRef) this.L$Anon2;
            ref$IntRef = (Ref$IntRef) this.L$Anon1;
            na4.a(obj);
            gh4 = (gh4) this.L$Anon6;
            zg4 = (zg4) this.L$Anon0;
            gh42 = (gh4) this.L$Anon7;
            obj3 = obj;
            List list4 = (List) this.L$Anon3;
            notificationSettingsModel = (NotificationSettingsModel) this.L$Anon5;
            list = list4;
        } else if (i == 2) {
            list2 = (List) this.L$Anon8;
            gh4 gh43 = (gh4) this.L$Anon7;
            gh4 gh44 = (gh4) this.L$Anon6;
            NotificationSettingsModel notificationSettingsModel3 = (NotificationSettingsModel) this.L$Anon5;
            NotificationSettingsModel notificationSettingsModel4 = (NotificationSettingsModel) this.L$Anon4;
            Ref$IntRef ref$IntRef3 = (Ref$IntRef) this.L$Anon2;
            Ref$IntRef ref$IntRef4 = (Ref$IntRef) this.L$Anon1;
            zg4 zg43 = (zg4) this.L$Anon0;
            na4.a(obj);
            list = (List) this.L$Anon3;
            obj2 = obj;
            list2.addAll((Collection) obj2);
            return list;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        list3.addAll((Collection) obj3);
        this.L$Anon0 = zg4;
        this.L$Anon1 = ref$IntRef;
        this.L$Anon2 = ref$IntRef2;
        this.L$Anon3 = list;
        this.Z$Anon0 = z;
        this.L$Anon4 = notificationSettingsModel2;
        this.L$Anon5 = notificationSettingsModel;
        this.L$Anon6 = gh4;
        this.L$Anon7 = gh42;
        this.L$Anon8 = list;
        this.label = 2;
        obj2 = gh42.a(this);
        if (obj2 == a) {
            return a;
        }
        list2 = list;
        list2.addAll((Collection) obj2);
        return list;
    }
}
