package com.portfolio.platform.util;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UserUtils$Companion$INSTANCE$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.portfolio.platform.util.UserUtils> {
    @DexIgnore
    public static /* final */ com.portfolio.platform.util.UserUtils$Companion$INSTANCE$2 INSTANCE; // = new com.portfolio.platform.util.UserUtils$Companion$INSTANCE$2();

    @DexIgnore
    public UserUtils$Companion$INSTANCE$2() {
        super(0);
    }

    @DexIgnore
    public final com.portfolio.platform.util.UserUtils invoke() {
        return com.portfolio.platform.util.UserUtils.C6947b.f24444b.mo41933a();
    }
}
