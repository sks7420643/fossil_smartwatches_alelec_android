package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$Anon1$response$Anon1", f = "NetworkBoundResource.kt", l = {69}, m = "invokeSuspend")
public final class NetworkBoundResource$fetchFromNetwork$Anon1$response$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<RequestType>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ NetworkBoundResource$fetchFromNetwork$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NetworkBoundResource$fetchFromNetwork$Anon1$response$Anon1(NetworkBoundResource$fetchFromNetwork$Anon1 networkBoundResource$fetchFromNetwork$Anon1, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = networkBoundResource$fetchFromNetwork$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new NetworkBoundResource$fetchFromNetwork$Anon1$response$Anon1(this.this$Anon0, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((NetworkBoundResource$fetchFromNetwork$Anon1$response$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            NetworkBoundResource networkBoundResource = this.this$Anon0.this$Anon0;
            this.label = 1;
            obj = networkBoundResource.createCall(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
