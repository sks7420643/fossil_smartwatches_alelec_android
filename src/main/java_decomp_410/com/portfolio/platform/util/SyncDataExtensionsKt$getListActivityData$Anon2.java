package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SyncDataExtensionsKt$getListActivityData$Anon2 extends Lambda implements xc4<FitnessDataWrapper, Long> {
    @DexIgnore
    public static /* final */ SyncDataExtensionsKt$getListActivityData$Anon2 INSTANCE; // = new SyncDataExtensionsKt$getListActivityData$Anon2();

    @DexIgnore
    public SyncDataExtensionsKt$getListActivityData$Anon2() {
        super(1);
    }

    @DexIgnore
    public final long invoke(FitnessDataWrapper fitnessDataWrapper) {
        kd4.b(fitnessDataWrapper, "it");
        return fitnessDataWrapper.getStartLongTime();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Long.valueOf(invoke((FitnessDataWrapper) obj));
    }
}
