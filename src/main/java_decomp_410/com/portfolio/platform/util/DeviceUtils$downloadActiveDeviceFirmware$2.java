package com.portfolio.platform.util;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2", mo27670f = "DeviceUtils.kt", mo27671l = {97}, mo27672m = "invokeSuspend")
public final class DeviceUtils$downloadActiveDeviceFirmware$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.qo2 $repoResponse;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public java.lang.Object L$7;
    @DexIgnore
    public java.lang.Object L$8;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24411p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.util.DeviceUtils this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2$2")
    /* renamed from: com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2$2 */
    public static final class C69252 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.portfolio.platform.data.model.Firmware, com.fossil.blesdk.obfuscated.gh4<? extends java.lang.Boolean>> {
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.zg4 $this_withContext;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2$2$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2$2$1", mo27670f = "DeviceUtils.kt", mo27671l = {93}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2$2$1 */
        public static final class C69261 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Boolean>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.data.model.Firmware $it;
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f24412p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2.C69252 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C69261(com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2.C69252 r1, com.portfolio.platform.data.model.Firmware firmware, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$it = firmware;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2.C69252.C69261 r0 = new com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2.C69252.C69261(this.this$0, this.$it, yb4);
                r0.f24412p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2.C69252.C69261) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24412p$;
                    com.portfolio.platform.util.DeviceUtils deviceUtils = this.this$0.this$0.this$0;
                    com.portfolio.platform.data.model.Firmware firmware = this.$it;
                    this.L$0 = zg4;
                    this.label = 1;
                    obj = deviceUtils.mo41905a(firmware, (com.fossil.blesdk.obfuscated.yb4<? super java.lang.Boolean>) this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C69252(com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2 deviceUtils$downloadActiveDeviceFirmware$2, com.fossil.blesdk.obfuscated.zg4 zg4) {
            super(1);
            this.this$0 = deviceUtils$downloadActiveDeviceFirmware$2;
            this.$this_withContext = zg4;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.gh4<java.lang.Boolean> invoke(com.portfolio.platform.data.model.Firmware firmware) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(firmware, "it");
            return com.fossil.blesdk.obfuscated.ag4.m19841a(this.$this_withContext, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2.C69252.C69261(this, firmware, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceUtils$downloadActiveDeviceFirmware$2(com.portfolio.platform.util.DeviceUtils deviceUtils, com.fossil.blesdk.obfuscated.qo2 qo2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = deviceUtils;
        this.$repoResponse = qo2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2 deviceUtils$downloadActiveDeviceFirmware$2 = new com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2(this.this$0, this.$repoResponse, yb4);
        deviceUtils$downloadActiveDeviceFirmware$2.f24411p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return deviceUtils$downloadActiveDeviceFirmware$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d6  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2 deviceUtils$downloadActiveDeviceFirmware$2;
        boolean z;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.util.List list;
        com.fossil.blesdk.obfuscated.un2 un2;
        kotlin.jvm.internal.Ref$BooleanRef ref$BooleanRef;
        com.fossil.blesdk.obfuscated.re4 re4;
        java.util.Iterator it;
        kotlin.jvm.internal.Ref$BooleanRef ref$BooleanRef2;
        java.lang.Object obj2;
        com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2 deviceUtils$downloadActiveDeviceFirmware$22;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f24411p$;
            com.portfolio.platform.data.source.remote.ApiResponse apiResponse = (com.portfolio.platform.data.source.remote.ApiResponse) ((com.fossil.blesdk.obfuscated.ro2) this.$repoResponse).mo30673a();
            java.util.List<com.portfolio.platform.data.model.Firmware> list2 = apiResponse != null ? apiResponse.get_items() : null;
            if (list2 != null) {
                com.fossil.blesdk.obfuscated.un2 e = com.fossil.blesdk.obfuscated.dn2.f14174p.mo26576a().mo26565e();
                kotlin.jvm.internal.Ref$BooleanRef ref$BooleanRef3 = new kotlin.jvm.internal.Ref$BooleanRef();
                ref$BooleanRef3.element = true;
                for (com.portfolio.platform.data.model.Firmware a2 : list2) {
                    e.mo31583a(a2);
                }
                com.fossil.blesdk.obfuscated.re4 c = kotlin.sequences.SequencesKt___SequencesKt.m37033c(com.fossil.blesdk.obfuscated.kb4.m24372b(list2), new com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2.C69252(this, zg42));
                zg4 = zg42;
                list = list2;
                ref$BooleanRef2 = ref$BooleanRef3;
                re4 = c;
                deviceUtils$downloadActiveDeviceFirmware$2 = this;
                un2 = e;
                it = c.iterator();
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            ref$BooleanRef2 = (kotlin.jvm.internal.Ref$BooleanRef) this.L$8;
            com.fossil.blesdk.obfuscated.gh4 gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$7;
            it = (java.util.Iterator) this.L$5;
            re4 = (com.fossil.blesdk.obfuscated.re4) this.L$4;
            ref$BooleanRef = (kotlin.jvm.internal.Ref$BooleanRef) this.L$3;
            un2 = (com.fossil.blesdk.obfuscated.un2) this.L$2;
            list = (java.util.List) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            obj2 = a;
            deviceUtils$downloadActiveDeviceFirmware$22 = this;
            if (!((java.lang.Boolean) obj).booleanValue()) {
                deviceUtils$downloadActiveDeviceFirmware$2 = deviceUtils$downloadActiveDeviceFirmware$22;
                a = obj2;
                z = true;
                ref$BooleanRef2.element = z;
                ref$BooleanRef2 = ref$BooleanRef;
            }
            deviceUtils$downloadActiveDeviceFirmware$2 = deviceUtils$downloadActiveDeviceFirmware$22;
            a = obj2;
            z = false;
            ref$BooleanRef2.element = z;
            ref$BooleanRef2 = ref$BooleanRef;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (!it.hasNext()) {
            java.lang.Object next = it.next();
            com.fossil.blesdk.obfuscated.gh4 gh42 = (com.fossil.blesdk.obfuscated.gh4) next;
            if (ref$BooleanRef2.element) {
                deviceUtils$downloadActiveDeviceFirmware$2.L$0 = zg4;
                deviceUtils$downloadActiveDeviceFirmware$2.L$1 = list;
                deviceUtils$downloadActiveDeviceFirmware$2.L$2 = un2;
                deviceUtils$downloadActiveDeviceFirmware$2.L$3 = ref$BooleanRef2;
                deviceUtils$downloadActiveDeviceFirmware$2.L$4 = re4;
                deviceUtils$downloadActiveDeviceFirmware$2.L$5 = it;
                deviceUtils$downloadActiveDeviceFirmware$2.L$6 = next;
                deviceUtils$downloadActiveDeviceFirmware$2.L$7 = gh42;
                deviceUtils$downloadActiveDeviceFirmware$2.L$8 = ref$BooleanRef2;
                deviceUtils$downloadActiveDeviceFirmware$2.label = 1;
                java.lang.Object a3 = gh42.mo27693a(deviceUtils$downloadActiveDeviceFirmware$2);
                if (a3 == a) {
                    return a;
                }
                obj2 = a;
                deviceUtils$downloadActiveDeviceFirmware$22 = deviceUtils$downloadActiveDeviceFirmware$2;
                obj = a3;
                ref$BooleanRef = ref$BooleanRef2;
                if (!((java.lang.Boolean) obj).booleanValue()) {
                    deviceUtils$downloadActiveDeviceFirmware$2 = deviceUtils$downloadActiveDeviceFirmware$22;
                    a = obj2;
                    z = false;
                    ref$BooleanRef2.element = z;
                    ref$BooleanRef2 = ref$BooleanRef;
                    if (!it.hasNext()) {
                    }
                }
                deviceUtils$downloadActiveDeviceFirmware$2 = deviceUtils$downloadActiveDeviceFirmware$22;
                a = obj2;
                z = true;
                ref$BooleanRef2.element = z;
                ref$BooleanRef2 = ref$BooleanRef;
                if (!it.hasNext()) {
                }
                return a;
            }
            ref$BooleanRef = ref$BooleanRef2;
            z = false;
            ref$BooleanRef2.element = z;
            ref$BooleanRef2 = ref$BooleanRef;
            if (!it.hasNext()) {
            }
        }
        if (!ref$BooleanRef2.element) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(com.portfolio.platform.util.DeviceUtils.f24405f, "downloadActiveDeviceFirmware - download detail fw FAILED in one or more parts. Retry later.");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
