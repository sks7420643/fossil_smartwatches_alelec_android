package com.portfolio.platform.util;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$otherAppsDefer$1", f = "NotificationAppHelper.kt", l = {145}, m = "invokeSuspend")
public final class NotificationAppHelper$buildNotificationAppFilters$2$otherAppsDefer$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isAllAppToggleEnabled;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $notificationAllFilterList;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationAppHelper$buildNotificationAppFilters$2$otherAppsDefer$1(com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2 notificationAppHelper$buildNotificationAppFilters$2, java.util.List list, boolean z, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = notificationAppHelper$buildNotificationAppFilters$2;
        this.$notificationAllFilterList = list;
        this.$isAllAppToggleEnabled = z;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$otherAppsDefer$1 notificationAppHelper$buildNotificationAppFilters$2$otherAppsDefer$1 = new com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$otherAppsDefer$1(this.this$0, this.$notificationAllFilterList, this.$isAllAppToggleEnabled, yb4);
        notificationAppHelper$buildNotificationAppFilters$2$otherAppsDefer$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationAppHelper$buildNotificationAppFilters$2$otherAppsDefer$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$otherAppsDefer$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.util.List list;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.p$;
            java.util.ArrayList arrayList = new java.util.ArrayList();
            com.fossil.blesdk.obfuscated.vy2 vy2 = this.this$0.$getApps;
            this.L$0 = zg4;
            this.L$1 = arrayList;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.w52.a(vy2, null, this);
            if (obj == a) {
                return a;
            }
            list = arrayList;
        } else if (i == 1) {
            list = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.CoroutineUseCase.c cVar = (com.portfolio.platform.CoroutineUseCase.c) obj;
        if (cVar instanceof com.fossil.blesdk.obfuscated.vy2.a) {
            this.$notificationAllFilterList.addAll(com.fossil.blesdk.obfuscated.ls3.a(((com.fossil.blesdk.obfuscated.vy2.a) cVar).a(), this.$isAllAppToggleEnabled));
        }
        return list;
    }
}
