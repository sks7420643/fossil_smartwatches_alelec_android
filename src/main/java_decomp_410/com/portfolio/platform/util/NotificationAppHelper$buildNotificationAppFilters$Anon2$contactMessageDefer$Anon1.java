package com.portfolio.platform.util;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.w52;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.CoroutineUseCase;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$IntRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$Anon2$contactMessageDefer$Anon1", f = "NotificationAppHelper.kt", l = {101}, m = "invokeSuspend")
public final class NotificationAppHelper$buildNotificationAppFilters$Anon2$contactMessageDefer$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<AppNotificationFilter>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Ref$IntRef $callSettingsType;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$IntRef $messageSettingsType;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationAppHelper$buildNotificationAppFilters$Anon2 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationAppHelper$buildNotificationAppFilters$Anon2$contactMessageDefer$Anon1(NotificationAppHelper$buildNotificationAppFilters$Anon2 notificationAppHelper$buildNotificationAppFilters$Anon2, Ref$IntRef ref$IntRef, Ref$IntRef ref$IntRef2, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = notificationAppHelper$buildNotificationAppFilters$Anon2;
        this.$callSettingsType = ref$IntRef;
        this.$messageSettingsType = ref$IntRef2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NotificationAppHelper$buildNotificationAppFilters$Anon2$contactMessageDefer$Anon1 notificationAppHelper$buildNotificationAppFilters$Anon2$contactMessageDefer$Anon1 = new NotificationAppHelper$buildNotificationAppFilters$Anon2$contactMessageDefer$Anon1(this.this$Anon0, this.$callSettingsType, this.$messageSettingsType, yb4);
        notificationAppHelper$buildNotificationAppFilters$Anon2$contactMessageDefer$Anon1.p$ = (zg4) obj;
        return notificationAppHelper$buildNotificationAppFilters$Anon2$contactMessageDefer$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationAppHelper$buildNotificationAppFilters$Anon2$contactMessageDefer$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        List list;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ArrayList arrayList = new ArrayList();
            px2 px2 = this.this$Anon0.$getAllContactGroup;
            this.L$Anon0 = zg4;
            this.L$Anon1 = arrayList;
            this.label = 1;
            obj2 = w52.a(px2, null, this);
            if (obj2 == a) {
                return a;
            }
            list = arrayList;
        } else if (i == 1) {
            list = (List) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            obj2 = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        CoroutineUseCase.c cVar = (CoroutineUseCase.c) obj2;
        if (cVar instanceof px2.d) {
            List<T> d = kb4.d(((px2.d) cVar).a());
            int i2 = this.$callSettingsType.element;
            if (i2 == 0) {
                DianaNotificationObj.AApplicationName aApplicationName = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                FNotification fNotification = r8;
                FNotification fNotification2 = new FNotification(aApplicationName.getAppName(), aApplicationName.getBundleCrc(), aApplicationName.getGroupId(), aApplicationName.getPackageName(), aApplicationName.getIconResourceId(), aApplicationName.getIconFwPath(), aApplicationName.getNotificationType());
                list.add(new AppNotificationFilter(fNotification));
            } else if (i2 == 1) {
                int size = d.size();
                for (int i3 = 0; i3 < size; i3++) {
                    ContactGroup contactGroup = (ContactGroup) d.get(i3);
                    DianaNotificationObj.AApplicationName aApplicationName2 = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                    FNotification fNotification3 = r10;
                    FNotification fNotification4 = new FNotification(aApplicationName2.getAppName(), aApplicationName2.getBundleCrc(), aApplicationName2.getGroupId(), aApplicationName2.getPackageName(), aApplicationName2.getIconResourceId(), aApplicationName2.getIconFwPath(), aApplicationName2.getNotificationType());
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification3);
                    List<Contact> contacts = contactGroup.getContacts();
                    kd4.a((Object) contacts, "item.contacts");
                    if (!contacts.isEmpty()) {
                        Contact contact = contactGroup.getContacts().get(0);
                        kd4.a((Object) contact, "item.contacts[0]");
                        appNotificationFilter.setSender(contact.getDisplayName());
                        list.add(appNotificationFilter);
                    }
                }
            }
            int i4 = this.$messageSettingsType.element;
            if (i4 == 0) {
                DianaNotificationObj.AApplicationName aApplicationName3 = DianaNotificationObj.AApplicationName.MESSAGES;
                list.add(new AppNotificationFilter(new FNotification(aApplicationName3.getAppName(), aApplicationName3.getBundleCrc(), aApplicationName3.getGroupId(), aApplicationName3.getPackageName(), aApplicationName3.getIconResourceId(), aApplicationName3.getIconFwPath(), aApplicationName3.getNotificationType())));
            } else if (i4 == 1) {
                int size2 = d.size();
                int i5 = 0;
                while (i5 < size2) {
                    ContactGroup contactGroup2 = (ContactGroup) d.get(i5);
                    DianaNotificationObj.AApplicationName aApplicationName4 = DianaNotificationObj.AApplicationName.MESSAGES;
                    FNotification fNotification5 = r9;
                    FNotification fNotification6 = new FNotification(aApplicationName4.getAppName(), aApplicationName4.getBundleCrc(), aApplicationName4.getGroupId(), aApplicationName4.getPackageName(), aApplicationName4.getIconResourceId(), aApplicationName4.getIconFwPath(), aApplicationName4.getNotificationType());
                    List<Contact> contacts2 = contactGroup2.getContacts();
                    kd4.a((Object) contacts2, "item.contacts");
                    if (!contacts2.isEmpty()) {
                        AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification5);
                        Contact contact2 = contactGroup2.getContacts().get(0);
                        kd4.a((Object) contact2, "item.contacts[0]");
                        appNotificationFilter2.setSender(contact2.getDisplayName());
                        list.add(appNotificationFilter2);
                    }
                    i5++;
                }
            }
        }
        return list;
    }
}
