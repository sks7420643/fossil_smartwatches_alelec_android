package com.portfolio.platform;

import android.content.Context;
import com.facebook.FacebookSdk;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.f62;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na0;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.google.android.libraries.places.api.Places;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.model.CloudLogConfig;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.PortfolioApp$onCreate$Anon1", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
public final class PortfolioApp$onCreate$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PortfolioApp this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PortfolioApp$onCreate$Anon1(PortfolioApp portfolioApp, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = portfolioApp;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        PortfolioApp$onCreate$Anon1 portfolioApp$onCreate$Anon1 = new PortfolioApp$onCreate$Anon1(this.this$Anon0, yb4);
        portfolioApp$onCreate$Anon1.p$ = (zg4) obj;
        return portfolioApp$onCreate$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PortfolioApp$onCreate$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004f, code lost:
        if (r2 != null) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0059, code lost:
        if (r4 != null) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0091, code lost:
        if (r1 != null) goto L_0x0095;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00df, code lost:
        if (r4 != null) goto L_0x00e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00e9, code lost:
        if (r15 != null) goto L_0x00ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0027, code lost:
        if (r4 != null) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0031, code lost:
        if (r5 != null) goto L_0x0035;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            Access a = new SoLibraryLoader().a((Context) this.this$Anon0);
            xa0 xa0 = xa0.c;
            String q = f62.x.q();
            if (a != null) {
                str = a.getL();
            }
            str = "";
            if (a != null) {
                str2 = a.getM();
            }
            str2 = "";
            xa0.a(new na0(q, str, str2));
            String f = f62.x.f();
            String c = f62.x.c();
            if (a != null) {
                str3 = a.getI();
            }
            str3 = "";
            if (a != null) {
                str4 = a.getK();
            }
            str4 = "";
            FLogger.INSTANCE.init("App", AppHelper.f.a().b(), AppHelper.f.a().a(), new CloudLogConfig(f, c, str3, str4), this.this$Anon0, true, "App");
            FLogger.INSTANCE.getRemote().flush();
            String str8 = "0000000000000";
            if (a != null) {
                str5 = a.getO();
            }
            str5 = str8;
            FacebookSdk.setApplicationId(str5);
            if (!FacebookSdk.isInitialized()) {
                FacebookSdk.sdkInitialize(this.this$Anon0);
            }
            if (!Places.isInitialized()) {
                PortfolioApp portfolioApp = this.this$Anon0;
                if (a != null) {
                    String n = a.getN();
                    if (n != null) {
                        str8 = n;
                    }
                }
                Places.initialize(portfolioApp, str8);
            }
            AnalyticsHelper.f.c().a(AnalyticsHelper.f.c().b());
            AppHelper.f.c();
            ZendeskConfig zendeskConfig = ZendeskConfig.INSTANCE;
            PortfolioApp portfolioApp2 = this.this$Anon0;
            String v = f62.x.v();
            if (a != null) {
                str6 = a.getG();
            }
            str6 = "";
            if (a != null) {
                str7 = a.getH();
            }
            str7 = "";
            zendeskConfig.init(portfolioApp2, v, str6, str7);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
