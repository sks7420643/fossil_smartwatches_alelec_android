package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.on3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.w52;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.MigrationManager$migrateFor4Dot0$Anon7", f = "MigrationManager.kt", l = {259}, m = "invokeSuspend")
public final class MigrationManager$migrateFor4Dot0$Anon7 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MigrationManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MigrationManager$migrateFor4Dot0$Anon7(MigrationManager migrationManager, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = migrationManager;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MigrationManager$migrateFor4Dot0$Anon7 migrationManager$migrateFor4Dot0$Anon7 = new MigrationManager$migrateFor4Dot0$Anon7(this.this$Anon0, yb4);
        migrationManager$migrateFor4Dot0$Anon7.p$ = (zg4) obj;
        return migrationManager$migrateFor4Dot0$Anon7;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MigrationManager$migrateFor4Dot0$Anon7) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            GetHybridDeviceSettingUseCase b = this.this$Anon0.d;
            on3 on3 = new on3(this.this$Anon0.e.e());
            this.L$Anon0 = zg4;
            this.label = 1;
            if (w52.a(b, on3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
