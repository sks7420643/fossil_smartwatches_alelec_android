package com.portfolio.platform.ui.view.chart.overview;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.wr3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class OverviewSleepDaySummary extends View {
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public float n;
    @DexIgnore
    public float o;
    @DexIgnore
    public RectF p;
    @DexIgnore
    public RectF q;
    @DexIgnore
    public RectF r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public RectF t;
    @DexIgnore
    public /* final */ PorterDuffXfermode u;
    @DexIgnore
    public /* final */ PorterDuffXfermode v;

    @DexIgnore
    public OverviewSleepDaySummary(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a(float f2, float f3, float f4) {
        this.h = f2;
        this.i = f3;
        this.j = f4;
        invalidate();
    }

    @DexIgnore
    public final String getTAG() {
        return this.e;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (canvas != null) {
            this.s.setColor(0);
            this.s.setXfermode(this.u);
            RectF rectF = this.t;
            float f2 = rectF.left;
            float f3 = rectF.top;
            float f4 = rectF.right;
            float f5 = rectF.bottom;
            float f6 = this.n;
            wr3.a(canvas, f2, f3, f4, f5, f6, f6, true, true, true, true, this.s);
            a(canvas, this.p, this.m);
            a(canvas, this.q, this.l);
            a(canvas, this.r, this.k);
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int size = View.MeasureSpec.getSize(i2);
        int size2 = View.MeasureSpec.getSize(i3);
        setMeasuredDimension(size, size2);
        float f2 = (float) size;
        float f3 = this.o;
        float f4 = f2 - (((float) 2) * f3);
        this.f = this.h * f4;
        this.g = this.i * f4;
        float f5 = (float) size2;
        this.t.set(f3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2 - f3, f5);
        RectF rectF = this.p;
        float f6 = this.o;
        rectF.set(f6, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.f + f6, f5);
        RectF rectF2 = this.q;
        float f7 = this.o;
        float f8 = this.f;
        rectF2.set(f7 + f8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f7 + f8 + this.g, f5);
        RectF rectF3 = this.r;
        float f9 = this.o;
        rectF3.set(this.f + f9 + this.g, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2 - f9, f5);
    }

    @DexIgnore
    public OverviewSleepDaySummary(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewSleepDaySummary(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, 0);
    }

    @DexIgnore
    public OverviewSleepDaySummary(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        this.e = "OverviewSleepDaySummary";
        this.p = new RectF();
        this.q = new RectF();
        this.r = new RectF();
        this.s = new Paint(1);
        this.t = new RectF();
        this.u = new PorterDuffXfermode(PorterDuff.Mode.CLEAR);
        this.v = new PorterDuffXfermode(PorterDuff.Mode.DST_OVER);
        if (!(attributeSet == null || context == null)) {
            Resources.Theme theme = context.getTheme();
            if (theme != null) {
                TypedArray obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, h62.OverviewSleepDaySummary, 0, 0);
                if (obtainStyledAttributes != null) {
                    try {
                        this.k = obtainStyledAttributes.getColor(3, 0);
                        this.l = obtainStyledAttributes.getColor(4, 0);
                        this.m = obtainStyledAttributes.getColor(0, 0);
                        this.n = (float) obtainStyledAttributes.getDimensionPixelSize(2, 5);
                        this.o = (float) obtainStyledAttributes.getDimensionPixelSize(1, 30);
                    } catch (Exception e2) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = this.e;
                        local.d(str, "constructor - e=" + e2);
                    } catch (Throwable th) {
                        obtainStyledAttributes.recycle();
                        throw th;
                    }
                    obtainStyledAttributes.recycle();
                }
            }
        }
        a();
    }

    @DexIgnore
    public final void a() {
        this.s.setAntiAlias(true);
        this.s.setStyle(Paint.Style.FILL);
    }

    @DexIgnore
    public final void a(Canvas canvas, RectF rectF, int i2) {
        this.s.setColor(i2);
        this.s.setXfermode(this.v);
        canvas.drawRect(rectF, this.s);
    }
}
