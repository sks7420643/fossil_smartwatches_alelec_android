package com.portfolio.platform.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.Keep;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DashBar extends View {
    @DexIgnore
    public Paint e; // = new Paint(1);
    @DexIgnore
    public Paint f; // = new Paint(1);
    @DexIgnore
    public float g; // = 10.0f;
    @DexIgnore
    public float h; // = 6.0f;
    @DexIgnore
    public int i; // = 4;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m; // = 50;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public DashBar(Context context) {
        super(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a(float f2) {
        this.e.setDither(true);
        this.e.setStyle(Paint.Style.STROKE);
        this.e.setStrokeWidth(f2);
        this.e.setAntiAlias(true);
        this.e.setStrokeCap(Paint.Cap.ROUND);
        this.e.setStrokeJoin(Paint.Join.ROUND);
    }

    @DexIgnore
    public final void b(float f2) {
        this.e.setShader(new LinearGradient(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.j, this.k, Shader.TileMode.CLAMP));
        Paint paint = this.e;
        int i2 = this.i;
        float f3 = this.h;
        paint.setPathEffect(new DashPathEffect(new float[]{((f2 - 20.0f) - (((float) (i2 - 1)) * f3)) / ((float) i2), f3}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.f = new Paint(this.e);
        Paint paint2 = this.f;
        int i3 = this.l;
        paint2.setShader(new LinearGradient(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, i3, i3, Shader.TileMode.CLAMP));
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        b((float) getWidth());
        if (canvas != null) {
            float height = ((float) canvas.getHeight()) / 2.0f;
            float width = (float) canvas.getWidth();
            float f2 = height;
            float f3 = height;
            canvas.drawLine(10.0f, f2, width - 10.0f, f3, this.f);
            canvas.drawLine(10.0f, f2, ((width * ((float) this.m)) / ((float) 100)) - 10.0f, f3, this.e);
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (!(mode == Integer.MIN_VALUE || mode == 0 || mode == 1073741824)) {
            size = 0;
        }
        if (mode2 != Integer.MIN_VALUE) {
            if (mode2 == 0) {
                size2 = ((int) this.g) * 2;
            } else if (mode2 != 1073741824) {
                size2 = 0;
            }
        }
        setMeasuredDimension(size, size2);
    }

    @DexIgnore
    @Keep
    public final void setLength(int i2) {
        this.i = i2;
        invalidate();
    }

    @DexIgnore
    @Keep
    public final void setProgress(int i2) {
        this.m = i2;
        invalidate();
    }

    @DexIgnore
    public DashBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context != null ? context.obtainStyledAttributes(attributeSet, h62.DashBar) : null;
        if (obtainStyledAttributes != null) {
            this.g = obtainStyledAttributes.getDimension(6, 10.0f);
            this.h = obtainStyledAttributes.getDimension(3, 6.0f);
            this.i = obtainStyledAttributes.getInteger(4, 4);
            this.j = obtainStyledAttributes.getColor(2, 0);
            this.k = obtainStyledAttributes.getColor(1, 0);
            this.l = obtainStyledAttributes.getColor(0, 0);
            this.m = obtainStyledAttributes.getInt(5, 0);
        }
        a(this.g);
        setLayerType(1, (Paint) null);
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.recycle();
        }
    }
}
