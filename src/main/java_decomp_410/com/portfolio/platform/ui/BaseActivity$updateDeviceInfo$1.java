package com.portfolio.platform.ui;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.BaseActivity$updateDeviceInfo$1", f = "BaseActivity.kt", l = {696}, m = "invokeSuspend")
public final class BaseActivity$updateDeviceInfo$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.BaseActivity this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseActivity$updateDeviceInfo$1(com.portfolio.platform.ui.BaseActivity baseActivity, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = baseActivity;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.ui.BaseActivity$updateDeviceInfo$1 baseActivity$updateDeviceInfo$1 = new com.portfolio.platform.ui.BaseActivity$updateDeviceInfo$1(this.this$0, yb4);
        baseActivity$updateDeviceInfo$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return baseActivity$updateDeviceInfo$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.ui.BaseActivity$updateDeviceInfo$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.p$;
            com.portfolio.platform.data.source.DeviceRepository b = this.this$0.b();
            java.lang.String a2 = this.this$0.u;
            if (a2 != null) {
                com.portfolio.platform.data.model.Device deviceBySerial = b.getDeviceBySerial(a2);
                if (deviceBySerial != null) {
                    com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.c();
                    com.portfolio.platform.ui.BaseActivity$updateDeviceInfo$1$invokeSuspend$$inlined$let$lambda$1 baseActivity$updateDeviceInfo$1$invokeSuspend$$inlined$let$lambda$1 = new com.portfolio.platform.ui.BaseActivity$updateDeviceInfo$1$invokeSuspend$$inlined$let$lambda$1((com.fossil.blesdk.obfuscated.yb4) null, this, deviceBySerial);
                    this.L$0 = zg4;
                    this.L$1 = deviceBySerial;
                    this.L$2 = deviceBySerial;
                    this.label = 1;
                    if (com.fossil.blesdk.obfuscated.yf4.a(c, baseActivity$updateDeviceInfo$1$invokeSuspend$$inlined$let$lambda$1, this) == a) {
                        return a;
                    }
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.a();
                throw null;
            }
        } else if (i == 1) {
            com.portfolio.platform.data.model.Device device = (com.portfolio.platform.data.model.Device) this.L$2;
            com.portfolio.platform.data.model.Device device2 = (com.portfolio.platform.data.model.Device) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.model.MFUser b2 = com.fossil.blesdk.obfuscated.dn2.p.a().n().b();
        java.lang.String accessTokenExpiresAt = b2 != null ? b2.getAccessTokenExpiresAt() : null;
        android.widget.TextView c2 = this.this$0.n;
        if (c2 != null) {
            if (accessTokenExpiresAt == null) {
                accessTokenExpiresAt = "";
            }
            c2.setText(accessTokenExpiresAt);
            return com.fossil.blesdk.obfuscated.qa4.a;
        }
        com.fossil.blesdk.obfuscated.kd4.a();
        throw null;
    }
}
