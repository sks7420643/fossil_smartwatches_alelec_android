package com.portfolio.platform.ui;

import android.widget.TextView;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.DeviceRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.BaseActivity$updateDeviceInfo$Anon1", f = "BaseActivity.kt", l = {696}, m = "invokeSuspend")
public final class BaseActivity$updateDeviceInfo$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BaseActivity this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseActivity$updateDeviceInfo$Anon1(BaseActivity baseActivity, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = baseActivity;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        BaseActivity$updateDeviceInfo$Anon1 baseActivity$updateDeviceInfo$Anon1 = new BaseActivity$updateDeviceInfo$Anon1(this.this$Anon0, yb4);
        baseActivity$updateDeviceInfo$Anon1.p$ = (zg4) obj;
        return baseActivity$updateDeviceInfo$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BaseActivity$updateDeviceInfo$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            DeviceRepository b = this.this$Anon0.b();
            String a2 = this.this$Anon0.u;
            if (a2 != null) {
                Device deviceBySerial = b.getDeviceBySerial(a2);
                if (deviceBySerial != null) {
                    pi4 c = nh4.c();
                    BaseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 baseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new BaseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1((yb4) null, this, deviceBySerial);
                    this.L$Anon0 = zg4;
                    this.L$Anon1 = deviceBySerial;
                    this.L$Anon2 = deviceBySerial;
                    this.label = 1;
                    if (yf4.a(c, baseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1, this) == a) {
                        return a;
                    }
                }
            } else {
                kd4.a();
                throw null;
            }
        } else if (i == 1) {
            Device device = (Device) this.L$Anon2;
            Device device2 = (Device) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser b2 = dn2.p.a().n().b();
        String accessTokenExpiresAt = b2 != null ? b2.getAccessTokenExpiresAt() : null;
        TextView c2 = this.this$Anon0.n;
        if (c2 != null) {
            if (accessTokenExpiresAt == null) {
                accessTokenExpiresAt = "";
            }
            c2.setText(accessTokenExpiresAt);
            return qa4.a;
        }
        kd4.a();
        throw null;
    }
}
