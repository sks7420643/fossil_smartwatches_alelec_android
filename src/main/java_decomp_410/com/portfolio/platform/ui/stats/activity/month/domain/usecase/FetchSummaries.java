package com.portfolio.platform.ui.stats.activity.month.domain.usecase;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.ft3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FetchSummaries extends CoroutineUseCase<FetchSummaries.b, CoroutineUseCase.d, CoroutineUseCase.a> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ SummariesRepository d;
    @DexIgnore
    public /* final */ FitnessDataRepository e;
    @DexIgnore
    public /* final */ UserRepository f;
    @DexIgnore
    public /* final */ ActivitiesRepository g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public b(Date date) {
            kd4.b(date, "date");
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = FetchSummaries.class.getSimpleName();
        kd4.a((Object) simpleName, "FetchSummaries::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public FetchSummaries(SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, ActivitiesRepository activitiesRepository) {
        kd4.b(summariesRepository, "mSummariesRepository");
        kd4.b(fitnessDataRepository, "mFitnessDataRepository");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(activitiesRepository, "mActivitiesRepository");
        this.d = summariesRepository;
        this.e = fitnessDataRepository;
        this.f = userRepository;
        this.g = activitiesRepository;
    }

    @DexIgnore
    public String c() {
        return h;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(b bVar, yb4<Object> yb4) {
        FetchSummaries$run$Anon1 fetchSummaries$run$Anon1;
        int i;
        Date date;
        Date date2;
        if (yb4 instanceof FetchSummaries$run$Anon1) {
            fetchSummaries$run$Anon1 = (FetchSummaries$run$Anon1) yb4;
            int i2 = fetchSummaries$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fetchSummaries$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = fetchSummaries$run$Anon1.result;
                Object a2 = cc4.a();
                i = fetchSummaries$run$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    if (bVar == null) {
                        return qa4.a;
                    }
                    Date a3 = bVar.a();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = h;
                    local.d(str, "executeUseCase - date=" + ft3.a(a3));
                    MFUser currentUser = this.f.getCurrentUser();
                    if (currentUser == null || TextUtils.isEmpty(currentUser.getCreatedAt())) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = h;
                        local2.d(str2, "executeUseCase - FAILED!!! with user=" + currentUser);
                        return qa4.a;
                    }
                    Date d2 = rk2.d(currentUser.getCreatedAt());
                    Calendar instance = Calendar.getInstance();
                    kd4.a((Object) instance, "calendar");
                    instance.setTime(a3);
                    instance.set(5, 1);
                    long timeInMillis = instance.getTimeInMillis();
                    kd4.a((Object) d2, "createdDate");
                    if (rk2.a(timeInMillis, d2.getTime())) {
                        date = d2;
                    } else {
                        Calendar e2 = rk2.e(instance);
                        kd4.a((Object) e2, "DateHelper.getStartOfMonth(calendar)");
                        date = e2.getTime();
                        kd4.a((Object) date, "DateHelper.getStartOfMonth(calendar).time");
                        if (rk2.b(d2, date)) {
                            return qa4.a;
                        }
                    }
                    Boolean r = rk2.r(date);
                    kd4.a((Object) r, "DateHelper.isThisMonth(startDate)");
                    if (r.booleanValue()) {
                        date2 = new Date();
                    } else {
                        Calendar j = rk2.j(date);
                        kd4.a((Object) j, "DateHelper.getEndOfMonth(startDate)");
                        date2 = j.getTime();
                        kd4.a((Object) date2, "DateHelper.getEndOfMonth(startDate).time");
                    }
                    List<SampleRaw> pendingActivities = this.g.getPendingActivities(date, date2);
                    List<FitnessDataWrapper> fitnessData = this.e.getFitnessData(date, date2);
                    if (pendingActivities.isEmpty() && fitnessData.isEmpty()) {
                        SummariesRepository summariesRepository = this.d;
                        fetchSummaries$run$Anon1.L$Anon0 = this;
                        fetchSummaries$run$Anon1.L$Anon1 = bVar;
                        fetchSummaries$run$Anon1.L$Anon2 = a3;
                        fetchSummaries$run$Anon1.L$Anon3 = currentUser;
                        fetchSummaries$run$Anon1.L$Anon4 = d2;
                        fetchSummaries$run$Anon1.L$Anon5 = instance;
                        fetchSummaries$run$Anon1.L$Anon6 = date2;
                        fetchSummaries$run$Anon1.L$Anon7 = date;
                        fetchSummaries$run$Anon1.L$Anon8 = pendingActivities;
                        fetchSummaries$run$Anon1.L$Anon9 = fitnessData;
                        fetchSummaries$run$Anon1.label = 1;
                        if (summariesRepository.loadSummaries(date, date2, fetchSummaries$run$Anon1) == a2) {
                            return a2;
                        }
                    }
                } else if (i == 1) {
                    List list = (List) fetchSummaries$run$Anon1.L$Anon9;
                    List list2 = (List) fetchSummaries$run$Anon1.L$Anon8;
                    Date date3 = (Date) fetchSummaries$run$Anon1.L$Anon7;
                    Date date4 = (Date) fetchSummaries$run$Anon1.L$Anon6;
                    Calendar calendar = (Calendar) fetchSummaries$run$Anon1.L$Anon5;
                    Date date5 = (Date) fetchSummaries$run$Anon1.L$Anon4;
                    MFUser mFUser = (MFUser) fetchSummaries$run$Anon1.L$Anon3;
                    Date date6 = (Date) fetchSummaries$run$Anon1.L$Anon2;
                    b bVar2 = (b) fetchSummaries$run$Anon1.L$Anon1;
                    FetchSummaries fetchSummaries = (FetchSummaries) fetchSummaries$run$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return qa4.a;
            }
        }
        fetchSummaries$run$Anon1 = new FetchSummaries$run$Anon1(this, yb4);
        Object obj2 = fetchSummaries$run$Anon1.result;
        Object a22 = cc4.a();
        i = fetchSummaries$run$Anon1.label;
        if (i != 0) {
        }
        return qa4.a;
    }
}
