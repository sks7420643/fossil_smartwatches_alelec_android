package com.portfolio.platform.ui.stats.sleep.month.domain.usecase;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries", f = "FetchSleepSummaries.kt", l = {61}, m = "run")
public final class FetchSleepSummaries$run$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public Object L$Anon8;
    @DexIgnore
    public Object L$Anon9;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ FetchSleepSummaries this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FetchSleepSummaries$run$Anon1(FetchSleepSummaries fetchSleepSummaries, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = fetchSleepSummaries;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((FetchSleepSummaries.b) null, (yb4<? super qa4>) this);
    }
}
