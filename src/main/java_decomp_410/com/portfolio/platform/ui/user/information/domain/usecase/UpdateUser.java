package com.portfolio.platform.ui.user.information.domain.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.UserRepository;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UpdateUser extends CoroutineUseCase<UpdateUser.b, UpdateUser.d, UpdateUser.c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ MFUser a;

        @DexIgnore
        public b(MFUser mFUser) {
            kd4.b(mFUser, "mfUser");
            this.a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ MFUser a;

        @DexIgnore
        public d(MFUser mFUser) {
            this.a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.a;
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = UpdateUser.class.getSimpleName();
        kd4.a((Object) simpleName, "UpdateUser::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public UpdateUser(UserRepository userRepository) {
        kd4.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public Object a(b bVar, yb4<Object> yb4) {
        UpdateUser$run$Anon1 updateUser$run$Anon1;
        int i;
        qo2 qo2;
        String str;
        if (yb4 instanceof UpdateUser$run$Anon1) {
            updateUser$run$Anon1 = (UpdateUser$run$Anon1) yb4;
            int i2 = updateUser$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                updateUser$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = updateUser$run$Anon1.result;
                Object a2 = cc4.a();
                i = updateUser$run$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d(e, "running UseCase");
                    if (bVar == null) {
                        return new c(600, "");
                    }
                    UserRepository userRepository = this.d;
                    MFUser a3 = bVar.a();
                    updateUser$run$Anon1.L$Anon0 = this;
                    updateUser$run$Anon1.L$Anon1 = bVar;
                    updateUser$run$Anon1.label = 1;
                    obj = userRepository.updateUser(a3, true, updateUser$run$Anon1);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    b bVar2 = (b) updateUser$run$Anon1.L$Anon1;
                    UpdateUser updateUser = (UpdateUser) updateUser$run$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    PortfolioApp c2 = PortfolioApp.W.c();
                    UserProfile j = PortfolioApp.W.c().j();
                    if (j != null) {
                        c2.a(j);
                        return new d((MFUser) ((ro2) qo2).a());
                    }
                    kd4.a();
                    throw null;
                } else if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    int a4 = po2.a();
                    ServerError c3 = po2.c();
                    if (c3 != null) {
                        String userMessage = c3.getUserMessage();
                        if (userMessage != null) {
                            str = userMessage;
                            if (str == null) {
                                str = "";
                            }
                            return new c(a4, str);
                        }
                    }
                    ServerError c4 = po2.c();
                    str = c4 != null ? c4.getMessage() : null;
                    if (str == null) {
                    }
                    return new c(a4, str);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        updateUser$run$Anon1 = new UpdateUser$run$Anon1(this, yb4);
        Object obj2 = updateUser$run$Anon1.result;
        Object a22 = cc4.a();
        i = updateUser$run$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }
}
