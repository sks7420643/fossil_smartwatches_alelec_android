package com.portfolio.platform.ui.user.usecase;

import android.app.Activity;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gr3;
import com.fossil.blesdk.obfuscated.jn2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.zk2;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import com.portfolio.platform.helper.AnalyticsHelper;
import java.lang.ref.WeakReference;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeleteLogoutUserUseCase extends CoroutineUseCase<DeleteLogoutUserUseCase.b, DeleteLogoutUserUseCase.d, DeleteLogoutUserUseCase.c> {
    @DexIgnore
    public static /* final */ String H;
    @DexIgnore
    public static /* final */ a I; // = new a((fd4) null);
    @DexIgnore
    public /* final */ FitnessDataRepository A;
    @DexIgnore
    public /* final */ HeartRateSampleRepository B;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository C;
    @DexIgnore
    public /* final */ WorkoutSessionRepository D;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase E;
    @DexIgnore
    public /* final */ ThirdPartyDatabase F;
    @DexIgnore
    public /* final */ PortfolioApp G;
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ AlarmsRepository e;
    @DexIgnore
    public /* final */ en2 f;
    @DexIgnore
    public /* final */ HybridPresetRepository g;
    @DexIgnore
    public /* final */ ActivitiesRepository h;
    @DexIgnore
    public /* final */ SummariesRepository i;
    @DexIgnore
    public /* final */ MicroAppSettingRepository j;
    @DexIgnore
    public /* final */ NotificationsRepository k;
    @DexIgnore
    public /* final */ DeviceRepository l;
    @DexIgnore
    public /* final */ SleepSessionsRepository m;
    @DexIgnore
    public /* final */ GoalTrackingRepository n;
    @DexIgnore
    public /* final */ jn2 o;
    @DexIgnore
    public /* final */ zk2 p;
    @DexIgnore
    public /* final */ SleepSummariesRepository q;
    @DexIgnore
    public /* final */ DianaPresetRepository r;
    @DexIgnore
    public /* final */ WatchAppRepository s;
    @DexIgnore
    public /* final */ ComplicationRepository t;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase u;
    @DexIgnore
    public /* final */ DNDSettingsDatabase v;
    @DexIgnore
    public /* final */ MicroAppRepository w;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository x;
    @DexIgnore
    public /* final */ ComplicationLastSettingRepository y;
    @DexIgnore
    public /* final */ WatchAppLastSettingRepository z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DeleteLogoutUserUseCase.H;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ WeakReference<Activity> b;

        @DexIgnore
        public b(int i, WeakReference<Activity> weakReference) {
            this.a = i;
            this.b = weakReference;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final WeakReference<Activity> b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements gr3.c {
        @DexIgnore
        public void a() {
            FLogger.INSTANCE.getLocal().d(DeleteLogoutUserUseCase.I.a(), "Logout UA success");
        }
    }

    /*
    static {
        String simpleName = DeleteLogoutUserUseCase.class.getSimpleName();
        kd4.a((Object) simpleName, "DeleteLogoutUserUseCase::class.java.simpleName");
        H = simpleName;
    }
    */

    @DexIgnore
    public DeleteLogoutUserUseCase(UserRepository userRepository, AlarmsRepository alarmsRepository, en2 en2, HybridPresetRepository hybridPresetRepository, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, MicroAppSettingRepository microAppSettingRepository, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, SleepSessionsRepository sleepSessionsRepository, GoalTrackingRepository goalTrackingRepository, jn2 jn2, zk2 zk2, SleepSummariesRepository sleepSummariesRepository, DianaPresetRepository dianaPresetRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, NotificationSettingsDatabase notificationSettingsDatabase, DNDSettingsDatabase dNDSettingsDatabase, MicroAppRepository microAppRepository, MicroAppLastSettingRepository microAppLastSettingRepository, ComplicationLastSettingRepository complicationLastSettingRepository, WatchAppLastSettingRepository watchAppLastSettingRepository, FitnessDataRepository fitnessDataRepository, HeartRateSampleRepository heartRateSampleRepository, HeartRateSummaryRepository heartRateSummaryRepository, WorkoutSessionRepository workoutSessionRepository, RemindersSettingsDatabase remindersSettingsDatabase, ThirdPartyDatabase thirdPartyDatabase, PortfolioApp portfolioApp) {
        UserRepository userRepository2 = userRepository;
        AlarmsRepository alarmsRepository2 = alarmsRepository;
        en2 en22 = en2;
        HybridPresetRepository hybridPresetRepository2 = hybridPresetRepository;
        ActivitiesRepository activitiesRepository2 = activitiesRepository;
        SummariesRepository summariesRepository2 = summariesRepository;
        MicroAppSettingRepository microAppSettingRepository2 = microAppSettingRepository;
        NotificationsRepository notificationsRepository2 = notificationsRepository;
        DeviceRepository deviceRepository2 = deviceRepository;
        SleepSessionsRepository sleepSessionsRepository2 = sleepSessionsRepository;
        GoalTrackingRepository goalTrackingRepository2 = goalTrackingRepository;
        jn2 jn22 = jn2;
        zk2 zk22 = zk2;
        SleepSummariesRepository sleepSummariesRepository2 = sleepSummariesRepository;
        WatchAppRepository watchAppRepository2 = watchAppRepository;
        kd4.b(userRepository2, "mUserRepository");
        kd4.b(alarmsRepository2, "mAlarmRepository");
        kd4.b(en22, "mSharedPreferences");
        kd4.b(hybridPresetRepository2, "mPresetRepository");
        kd4.b(activitiesRepository2, "mActivitiesRepository");
        kd4.b(summariesRepository2, "mSummariesRepository");
        kd4.b(microAppSettingRepository2, "mMicroAppSettingRepository");
        kd4.b(notificationsRepository2, "mNotificationRepository");
        kd4.b(deviceRepository2, "mDeviceRepository");
        kd4.b(sleepSessionsRepository2, "mSleepSessionsRepository");
        kd4.b(goalTrackingRepository2, "mGoalTrackingRepository");
        kd4.b(jn22, "mLoginGoogleManager");
        kd4.b(zk22, "mGoogleFitHelper");
        kd4.b(sleepSummariesRepository2, "mSleepSummariesRepository");
        kd4.b(dianaPresetRepository, "mDianaPresetRepository");
        kd4.b(watchAppRepository, "mWatchAppRepository");
        kd4.b(complicationRepository, "mComplicationRepository");
        kd4.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        kd4.b(dNDSettingsDatabase, "mDNDSettingsDatabase");
        kd4.b(microAppRepository, "mMicroAppRepository");
        kd4.b(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        kd4.b(complicationLastSettingRepository, "mComplicationLastSettingRepository");
        kd4.b(watchAppLastSettingRepository, "mWatchAppLastSettingRepository");
        kd4.b(fitnessDataRepository, "mFitnessDataRepository");
        kd4.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        kd4.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        kd4.b(workoutSessionRepository, "mWorkoutSessionRepository");
        kd4.b(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        kd4.b(thirdPartyDatabase, "mThirdPartyDatabase");
        kd4.b(portfolioApp, "mApp");
        this.d = userRepository2;
        this.e = alarmsRepository2;
        this.f = en22;
        this.g = hybridPresetRepository2;
        this.h = activitiesRepository2;
        this.i = summariesRepository2;
        this.j = microAppSettingRepository2;
        this.k = notificationsRepository2;
        this.l = deviceRepository2;
        this.m = sleepSessionsRepository2;
        this.n = goalTrackingRepository2;
        this.o = jn22;
        this.p = zk22;
        this.q = sleepSummariesRepository2;
        this.r = dianaPresetRepository;
        this.s = watchAppRepository;
        this.t = complicationRepository;
        this.u = notificationSettingsDatabase;
        this.v = dNDSettingsDatabase;
        this.w = microAppRepository;
        this.x = microAppLastSettingRepository;
        this.y = complicationLastSettingRepository;
        this.z = watchAppLastSettingRepository;
        this.A = fitnessDataRepository;
        this.B = heartRateSampleRepository;
        this.C = heartRateSummaryRepository;
        this.D = workoutSessionRepository;
        this.E = remindersSettingsDatabase;
        this.F = thirdPartyDatabase;
        this.G = portfolioApp;
    }

    @DexIgnore
    public String c() {
        return H;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    public Object a(b bVar, yb4<Object> yb4) {
        DeleteLogoutUserUseCase$run$Anon1 deleteLogoutUserUseCase$run$Anon1;
        int i2;
        DeleteLogoutUserUseCase deleteLogoutUserUseCase;
        int intValue;
        DeleteLogoutUserUseCase deleteLogoutUserUseCase2;
        int intValue2;
        if (yb4 instanceof DeleteLogoutUserUseCase$run$Anon1) {
            deleteLogoutUserUseCase$run$Anon1 = (DeleteLogoutUserUseCase$run$Anon1) yb4;
            int i3 = deleteLogoutUserUseCase$run$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                deleteLogoutUserUseCase$run$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = deleteLogoutUserUseCase$run$Anon1.result;
                Object a2 = cc4.a();
                i2 = deleteLogoutUserUseCase$run$Anon1.label;
                if (i2 != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = H;
                    StringBuilder sb = new StringBuilder();
                    sb.append("running UseCase with mode=");
                    sb.append(bVar != null ? dc4.a(bVar.a()) : null);
                    local.d(str, sb.toString());
                    if (bVar != null && bVar.a() == 0) {
                        MFUser currentUser = this.d.getCurrentUser();
                        if (currentUser != null) {
                            UserRepository userRepository = this.d;
                            deleteLogoutUserUseCase$run$Anon1.L$Anon0 = this;
                            deleteLogoutUserUseCase$run$Anon1.L$Anon1 = bVar;
                            deleteLogoutUserUseCase$run$Anon1.L$Anon2 = currentUser;
                            deleteLogoutUserUseCase$run$Anon1.label = 1;
                            obj = userRepository.deleteUser(currentUser, deleteLogoutUserUseCase$run$Anon1);
                            if (obj == a2) {
                                return a2;
                            }
                            deleteLogoutUserUseCase2 = this;
                        }
                        return new Object();
                    } else if (bVar == null || bVar.a() != 1) {
                        if (bVar != null && bVar.a() == 2) {
                            if (this.d.getCurrentUser() != null) {
                                a(bVar);
                            } else {
                                a(new c(600));
                            }
                        }
                        return new Object();
                    } else {
                        UserRepository userRepository2 = this.d;
                        deleteLogoutUserUseCase$run$Anon1.L$Anon0 = this;
                        deleteLogoutUserUseCase$run$Anon1.L$Anon1 = bVar;
                        deleteLogoutUserUseCase$run$Anon1.label = 2;
                        obj = userRepository2.logoutUser(deleteLogoutUserUseCase$run$Anon1);
                        if (obj == a2) {
                            return a2;
                        }
                        deleteLogoutUserUseCase = this;
                        intValue = ((Number) obj).intValue();
                        if (intValue != 200) {
                        }
                        return new Object();
                    }
                } else if (i2 == 1) {
                    MFUser mFUser = (MFUser) deleteLogoutUserUseCase$run$Anon1.L$Anon2;
                    bVar = (b) deleteLogoutUserUseCase$run$Anon1.L$Anon1;
                    deleteLogoutUserUseCase2 = (DeleteLogoutUserUseCase) deleteLogoutUserUseCase$run$Anon1.L$Anon0;
                    na4.a(obj);
                } else if (i2 == 2) {
                    bVar = (b) deleteLogoutUserUseCase$run$Anon1.L$Anon1;
                    deleteLogoutUserUseCase = (DeleteLogoutUserUseCase) deleteLogoutUserUseCase$run$Anon1.L$Anon0;
                    na4.a(obj);
                    intValue = ((Number) obj).intValue();
                    if (intValue != 200) {
                        deleteLogoutUserUseCase.a(bVar);
                    } else {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = H;
                        local2.d(str2, "Inside .run failed with http code=" + intValue);
                        if (intValue == 401 || intValue == 404) {
                            deleteLogoutUserUseCase.a(bVar);
                        } else {
                            deleteLogoutUserUseCase.a(new c(intValue));
                        }
                    }
                    return new Object();
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                intValue2 = ((Number) obj).intValue();
                if (intValue2 != 200) {
                    AnalyticsHelper.f.c().b("remove_user", Constants.RESULT, "");
                    deleteLogoutUserUseCase2.a(bVar);
                } else {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = H;
                    local3.d(str3, "Inside .run failed with http code=" + intValue2);
                    AnalyticsHelper.f.c().b("remove_user", Constants.RESULT, String.valueOf(intValue2));
                    if (intValue2 == 401 || intValue2 == 404) {
                        deleteLogoutUserUseCase2.a(bVar);
                    } else {
                        deleteLogoutUserUseCase2.a(new c(intValue2));
                    }
                }
                return new Object();
            }
        }
        deleteLogoutUserUseCase$run$Anon1 = new DeleteLogoutUserUseCase$run$Anon1(this, yb4);
        Object obj2 = deleteLogoutUserUseCase$run$Anon1.result;
        Object a22 = cc4.a();
        i2 = deleteLogoutUserUseCase$run$Anon1.label;
        if (i2 != 0) {
        }
        intValue2 = ((Number) obj2).intValue();
        if (intValue2 != 200) {
        }
        return new Object();
    }

    @DexIgnore
    public final void a(b bVar) {
        throw null;
        // this.o.a(bVar.b());
        // try {
        //     IButtonConnectivity b2 = PortfolioApp.W.b();
        //     if (b2 != null) {
        //         b2.deviceUnlink(PortfolioApp.W.c().e());
        //     }
        // } catch (Exception e2) {
        //     e2.printStackTrace();
        // }
        // fi4 unused = ag4.b(b(), (CoroutineContext) null, (CoroutineStart) null, new DeleteLogoutUserUseCase$clearUserData$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final /* synthetic */ Object a(yb4<? super qa4> yb4) {
        DeleteLogoutUserUseCase$removeAllConnectedApps$Anon1 deleteLogoutUserUseCase$removeAllConnectedApps$Anon1;
        int i2;
        DeleteLogoutUserUseCase deleteLogoutUserUseCase = null;
        if (yb4 instanceof DeleteLogoutUserUseCase$removeAllConnectedApps$Anon1) {
            deleteLogoutUserUseCase$removeAllConnectedApps$Anon1 = (DeleteLogoutUserUseCase$removeAllConnectedApps$Anon1) yb4;
            int i3 = deleteLogoutUserUseCase$removeAllConnectedApps$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                deleteLogoutUserUseCase$removeAllConnectedApps$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = deleteLogoutUserUseCase$removeAllConnectedApps$Anon1.result;
                Object a2 = cc4.a();
                i2 = deleteLogoutUserUseCase$removeAllConnectedApps$Anon1.label;
                if (i2 != 0) {
                    na4.a(obj);
                    if (this.G.r().b()) {
                        gr3 r2 = this.G.r();
                        e eVar = new e();
                        deleteLogoutUserUseCase$removeAllConnectedApps$Anon1.L$Anon0 = this;
                        deleteLogoutUserUseCase$removeAllConnectedApps$Anon1.label = 1;
                        if (r2.a((gr3.c) eVar, (yb4<? super qa4>) deleteLogoutUserUseCase$removeAllConnectedApps$Anon1) == a2) {
                            return a2;
                        }
                    }
                    deleteLogoutUserUseCase = this;
                } else if (i2 == 1) {
                    deleteLogoutUserUseCase = (DeleteLogoutUserUseCase) deleteLogoutUserUseCase$removeAllConnectedApps$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (deleteLogoutUserUseCase.p.e()) {
                    deleteLogoutUserUseCase.p.h();
                }
                return qa4.a;
            }
        }
        deleteLogoutUserUseCase$removeAllConnectedApps$Anon1 = new DeleteLogoutUserUseCase$removeAllConnectedApps$Anon1(this, yb4);
        Object obj2 = deleteLogoutUserUseCase$removeAllConnectedApps$Anon1.result;
        Object a22 = cc4.a();
        i2 = deleteLogoutUserUseCase$removeAllConnectedApps$Anon1.label;
        if (i2 != 0) {
        }
        if (deleteLogoutUserUseCase.p.e()) {
        }
        return qa4.a;
    }
}
