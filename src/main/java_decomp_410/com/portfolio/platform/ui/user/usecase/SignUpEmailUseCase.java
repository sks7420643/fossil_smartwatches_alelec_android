package com.portfolio.platform.ui.user.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SignUpEmailUseCase extends CoroutineUseCase<SignUpEmailUseCase.a, SignUpEmailUseCase.c, SignUpEmailUseCase.b> {
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ en2 e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ SignUpEmailAuth a;

        @DexIgnore
        public a(SignUpEmailAuth signUpEmailAuth) {
            kd4.b(signUpEmailAuth, "emailAuth");
            this.a = signUpEmailAuth;
        }

        @DexIgnore
        public final SignUpEmailAuth a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(int i, String str) {
            kd4.b(str, "errorMesagge");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.d {
    }

    @DexIgnore
    public SignUpEmailUseCase(UserRepository userRepository, en2 en2) {
        kd4.b(userRepository, "mUserRepository");
        kd4.b(en2, "mSharedPreferencesManager");
        this.d = userRepository;
        this.e = en2;
    }

    @DexIgnore
    public String c() {
        return "SignUpEmailUseCase";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00de, code lost:
        if (r9 != null) goto L_0x00e2;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    public Object a(a aVar, yb4<Object> yb4) {
        SignUpEmailUseCase$run$Anon1 signUpEmailUseCase$run$Anon1;
        int i;
        SignUpEmailUseCase signUpEmailUseCase;
        qo2 qo2;
        String str;
        if (yb4 instanceof SignUpEmailUseCase$run$Anon1) {
            signUpEmailUseCase$run$Anon1 = (SignUpEmailUseCase$run$Anon1) yb4;
            int i2 = signUpEmailUseCase$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                signUpEmailUseCase$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = signUpEmailUseCase$run$Anon1.result;
                Object a2 = cc4.a();
                i = signUpEmailUseCase$run$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d("SignUpEmailUseCase", "running UseCase");
                    if (aVar == null) {
                        return new b(600, "");
                    }
                    if (this.d.getCurrentUser() != null) {
                        return new c();
                    }
                    UserRepository userRepository = this.d;
                    SignUpEmailAuth a3 = aVar.a();
                    signUpEmailUseCase$run$Anon1.L$Anon0 = this;
                    signUpEmailUseCase$run$Anon1.L$Anon1 = aVar;
                    signUpEmailUseCase$run$Anon1.label = 1;
                    obj = userRepository.signUpEmail(a3, signUpEmailUseCase$run$Anon1);
                    if (obj == a2) {
                        return a2;
                    }
                    signUpEmailUseCase = this;
                } else if (i == 1) {
                    a aVar2 = (a) signUpEmailUseCase$run$Anon1.L$Anon1;
                    signUpEmailUseCase = (SignUpEmailUseCase) signUpEmailUseCase$run$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                String str2 = null;
                if (!(qo2 instanceof ro2)) {
                    FLogger.INSTANCE.getLocal().d("SignUpEmailUseCase", "signUpEmail success");
                    en2 en2 = signUpEmailUseCase.e;
                    Auth auth = (Auth) ((ro2) qo2).a();
                    if (auth != null) {
                        str2 = auth.getAccessToken();
                    }
                    en2.w(str2);
                    return new c();
                } else if (!(qo2 instanceof po2)) {
                    return new b(600, "");
                } else {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("signUpEmail failed code=");
                    po2 po2 = (po2) qo2;
                    ServerError c2 = po2.c();
                    if (c2 != null) {
                        str2 = c2.getMessage();
                    }
                    sb.append(str2);
                    local.d("SignUpEmailUseCase", sb.toString());
                    int a4 = po2.a();
                    ServerError c3 = po2.c();
                    if (c3 != null) {
                        str = c3.getMessage();
                    }
                    str = "";
                    return new b(a4, str);
                }
            }
        }
        signUpEmailUseCase$run$Anon1 = new SignUpEmailUseCase$run$Anon1(this, yb4);
        Object obj2 = signUpEmailUseCase$run$Anon1.result;
        Object a22 = cc4.a();
        i = signUpEmailUseCase$run$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        String str22 = null;
        if (!(qo2 instanceof ro2)) {
        }
        throw null;
    }
}
