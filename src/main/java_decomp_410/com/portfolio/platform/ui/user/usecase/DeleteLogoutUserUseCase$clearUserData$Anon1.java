package com.portfolio.platform.ui.user.usecase;

import com.fossil.blesdk.obfuscated.ap2;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.is3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$Anon1", f = "DeleteLogoutUserUseCase.kt", l = {148}, m = "invokeSuspend")
public final class DeleteLogoutUserUseCase$clearUserData$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeleteLogoutUserUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeleteLogoutUserUseCase$clearUserData$Anon1(DeleteLogoutUserUseCase deleteLogoutUserUseCase, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = deleteLogoutUserUseCase;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DeleteLogoutUserUseCase$clearUserData$Anon1 deleteLogoutUserUseCase$clearUserData$Anon1 = new DeleteLogoutUserUseCase$clearUserData$Anon1(this.this$Anon0, yb4);
        deleteLogoutUserUseCase$clearUserData$Anon1.p$ = (zg4) obj;
        return deleteLogoutUserUseCase$clearUserData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DeleteLogoutUserUseCase$clearUserData$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            FLogger.INSTANCE.getRemote().flush();
            try {
                is3.b.f(ButtonService.DEVICE_SECRET_KEY);
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = DeleteLogoutUserUseCase.I.a();
                local.e(a2, "exception when remove alias in keystore " + e);
            }
            this.this$Anon0.k.clearAllNotificationSetting();
            this.this$Anon0.m.cleanUp();
            this.this$Anon0.q.cleanUp();
            this.this$Anon0.A.cleanUp();
            this.this$Anon0.f.V();
            this.this$Anon0.d.clearAllUser();
            PortfolioApp.W.c().S();
            DeleteLogoutUserUseCase deleteLogoutUserUseCase = this.this$Anon0;
            this.L$Anon0 = zg4;
            this.label = 1;
            if (deleteLogoutUserUseCase.a((yb4<? super qa4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.x.cleanUp();
        this.this$Anon0.y.cleanUp();
        this.this$Anon0.z.cleanUp();
        this.this$Anon0.g.cleanUp();
        this.this$Anon0.w.cleanUp();
        this.this$Anon0.h.cleanUp();
        this.this$Anon0.i.cleanUp();
        this.this$Anon0.n.cleanUp();
        this.this$Anon0.r.cleanUp();
        this.this$Anon0.s.cleanUp();
        this.this$Anon0.t.cleanUp();
        this.this$Anon0.j.clearData();
        this.this$Anon0.e.cleanUp();
        dn2.p.a().o();
        ap2.g.a();
        PortfolioApp.W.c().c();
        PortfolioApp.W.c().F();
        this.this$Anon0.u.getNotificationSettingsDao().delete();
        this.this$Anon0.v.getDNDScheduledTimeDao().delete();
        this.this$Anon0.E.getInactivityNudgeTimeDao().delete();
        this.this$Anon0.E.getRemindTimeDao().delete();
        this.this$Anon0.B.cleanUp();
        this.this$Anon0.C.cleanUp();
        this.this$Anon0.D.cleanUp();
        ThirdPartyDatabase w = this.this$Anon0.F;
        w.getGFitSampleDao().clearAll();
        w.getGFitActiveTimeDao().clearAll();
        w.getGFitWorkoutSessionDao().clearAll();
        w.getUASampleDao().clearAll();
        this.this$Anon0.a(new DeleteLogoutUserUseCase.d());
        this.this$Anon0.l.cleanUp();
        return qa4.a;
    }
}
