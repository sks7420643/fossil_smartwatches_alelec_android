package com.portfolio.platform.ui.user.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase", f = "DownloadUserInfoUseCase.kt", l = {22}, m = "run")
public final class DownloadUserInfoUseCase$run$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DownloadUserInfoUseCase$run$1(com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase downloadUserInfoUseCase, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = downloadUserInfoUseCase;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.a((com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase.c) null, (com.fossil.blesdk.obfuscated.yb4<java.lang.Object>) this);
    }
}
