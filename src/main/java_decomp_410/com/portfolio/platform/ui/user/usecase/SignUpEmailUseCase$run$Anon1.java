package com.portfolio.platform.ui.user.usecase;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase", f = "SignUpEmailUseCase.kt", l = {29}, m = "run")
public final class SignUpEmailUseCase$run$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ SignUpEmailUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SignUpEmailUseCase$run$Anon1(SignUpEmailUseCase signUpEmailUseCase, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = signUpEmailUseCase;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((SignUpEmailUseCase.a) null, (yb4<Object>) this);
    }
}
