package com.portfolio.platform.ui.user.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.response.ResponseKt;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ResetPasswordUseCase extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ AuthApiGuestService d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            kd4.b(str, "email");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = ResetPasswordUseCase.class.getSimpleName();
        kd4.a((Object) simpleName, "ResetPasswordUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public ResetPasswordUseCase(AuthApiGuestService authApiGuestService) {
        kd4.b(authApiGuestService, "mApiGuestService");
        this.d = authApiGuestService;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ec, code lost:
        if (r10 != null) goto L_0x00f0;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public Object a(b bVar, yb4<Object> yb4) {
        ResetPasswordUseCase$run$Anon1 resetPasswordUseCase$run$Anon1;
        int i;
        qo2 qo2;
        int i2;
        ServerError c2;
        String str;
        if (yb4 instanceof ResetPasswordUseCase$run$Anon1) {
            resetPasswordUseCase$run$Anon1 = (ResetPasswordUseCase$run$Anon1) yb4;
            int i3 = resetPasswordUseCase$run$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                resetPasswordUseCase$run$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = resetPasswordUseCase$run$Anon1.result;
                Object a2 = cc4.a();
                i = resetPasswordUseCase$run$Anon1.label;
                Integer num = null;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = e;
                    StringBuilder sb = new StringBuilder();
                    sb.append("running UseCase with email=");
                    sb.append(bVar != null ? bVar.a() : null);
                    local.d(str2, sb.toString());
                    if (bVar == null) {
                        return new c(600, "");
                    }
                    xz1 xz1 = new xz1();
                    xz1.a("email", bVar.a());
                    ResetPasswordUseCase$run$response$Anon1 resetPasswordUseCase$run$response$Anon1 = new ResetPasswordUseCase$run$response$Anon1(this, xz1, (yb4) null);
                    resetPasswordUseCase$run$Anon1.L$Anon0 = this;
                    resetPasswordUseCase$run$Anon1.L$Anon1 = bVar;
                    resetPasswordUseCase$run$Anon1.L$Anon2 = xz1;
                    resetPasswordUseCase$run$Anon1.label = 1;
                    obj = ResponseKt.a(resetPasswordUseCase$run$response$Anon1, resetPasswordUseCase$run$Anon1);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    xz1 xz12 = (xz1) resetPasswordUseCase$run$Anon1.L$Anon2;
                    b bVar2 = (b) resetPasswordUseCase$run$Anon1.L$Anon1;
                    ResetPasswordUseCase resetPasswordUseCase = (ResetPasswordUseCase) resetPasswordUseCase$run$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    return new d();
                }
                if (qo2 instanceof po2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = e;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Inside .run failed with error=");
                    po2 po2 = (po2) qo2;
                    ServerError c3 = po2.c();
                    if (c3 != null) {
                        num = c3.getCode();
                    }
                    sb2.append(num);
                    local2.d(str3, sb2.toString());
                    ServerError c4 = po2.c();
                    if (c4 != null) {
                        Integer code = c4.getCode();
                        if (code != null) {
                            i2 = code.intValue();
                            c2 = po2.c();
                            if (c2 != null) {
                                str = c2.getMessage();
                            }
                            str = "";
                            return new c(i2, str);
                        }
                    }
                    i2 = po2.a();
                    c2 = po2.c();
                    if (c2 != null) {
                    }
                    str = "";
                    return new c(i2, str);
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        resetPasswordUseCase$run$Anon1 = new ResetPasswordUseCase$run$Anon1(this, yb4);
        Object obj2 = resetPasswordUseCase$run$Anon1.result;
        Object a22 = cc4.a();
        i = resetPasswordUseCase$run$Anon1.label;
        Integer num2 = null;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }
}
