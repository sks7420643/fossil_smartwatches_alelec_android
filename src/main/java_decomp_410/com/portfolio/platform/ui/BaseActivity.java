package com.portfolio.platform.ui;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.b6;
import com.fossil.blesdk.obfuscated.bb;
import com.fossil.blesdk.obfuscated.bt3;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.gr2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pq4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.ts3;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.service.MFDeviceService;
import java.lang.reflect.Method;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@SuppressLint({"Registered"})
public class BaseActivity extends AppCompatActivity implements ws3.g, pq4.a {
    @DexIgnore
    public static /* final */ a A; // = new a((fd4) null);
    @DexIgnore
    public static IButtonConnectivity z;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ Handler h; // = new Handler();
    @DexIgnore
    public View i;
    @DexIgnore
    public TextView j;
    @DexIgnore
    public TextView k;
    @DexIgnore
    public TextView l;
    @DexIgnore
    public TextView m;
    @DexIgnore
    public TextView n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public UserRepository p;
    @DexIgnore
    public en2 q;
    @DexIgnore
    public DeviceRepository r;
    @DexIgnore
    public MigrationManager s;
    @DexIgnore
    public gr2 t;
    @DexIgnore
    public String u;
    @DexIgnore
    public bt3 v;
    @DexIgnore
    public /* final */ BaseActivity$mButtonServiceConnection$Anon1 w; // = new BaseActivity$mButtonServiceConnection$Anon1(this);
    @DexIgnore
    public /* final */ d x; // = new d(this);
    @DexIgnore
    public /* final */ Runnable y; // = new b(this);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final IButtonConnectivity a() {
            return BaseActivity.z;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final void a(IButtonConnectivity iButtonConnectivity) {
            BaseActivity.z = iButtonConnectivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements CoroutineUseCase.e<gr2.e, gr2.b> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(gr2.e eVar) {
                kd4.b(eVar, "responseValue");
                this.a.e.b(String.valueOf(eVar.a()));
            }

            @DexIgnore
            public void a(gr2.b bVar) {
                kd4.b(bVar, "errorValue");
                this.a.e.b("Disconnected");
            }
        }

        @DexIgnore
        public b(BaseActivity baseActivity) {
            this.e = baseActivity;
        }

        @DexIgnore
        public final void run() {
            if (!TextUtils.isEmpty(this.e.u)) {
                gr2 c = this.e.c();
                String a2 = this.e.u;
                if (a2 != null) {
                    c.a(new gr2.d(a2), new a(this));
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity e;

        @DexIgnore
        public c(BaseActivity baseActivity) {
            this.e = baseActivity;
        }

        @DexIgnore
        public final void run() {
            try {
                if (this.e.v == null) {
                    Fragment a = this.e.getSupportFragmentManager().a("ProgressDialogFragment");
                    if (a != null) {
                        this.e.v = (bt3) a;
                    }
                }
                if (this.e.v != null) {
                    FLogger.INSTANCE.getLocal().d("ProgressDialogFragment", "hideLoadingDialog dismissAllowingStateLoss");
                    bt3 b = this.e.v;
                    if (b != null) {
                        b.dismissAllowingStateLoss();
                        this.e.v = null;
                        return;
                    }
                    kd4.a();
                    throw null;
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String f = this.e.f();
                local.d(f, "Exception when dismiss progress dialog=" + e2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ServiceConnection {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity a;

        @DexIgnore
        public d(BaseActivity baseActivity) {
            this.a = baseActivity;
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            kd4.b(componentName, "name");
            kd4.b(iBinder, Constants.SERVICE);
            FLogger.INSTANCE.getLocal().d(this.a.f(), "Misfit service connected");
            MFDeviceService.b bVar = (MFDeviceService.b) iBinder;
            this.a.a(bVar.a());
            PortfolioApp.W.b(bVar);
            this.a.c(true);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            kd4.b(componentName, "name");
            this.a.c(false);
            this.a.a((MFDeviceService) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity e;

        @DexIgnore
        public e(BaseActivity baseActivity) {
            this.e = baseActivity;
        }

        @DexIgnore
        public final void run() {
            FragmentManager supportFragmentManager = this.e.getSupportFragmentManager();
            kd4.a((Object) supportFragmentManager, "supportFragmentManager");
            if (supportFragmentManager.c() > 1) {
                this.e.getSupportFragmentManager().f();
            } else {
                this.e.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity e;
        @DexIgnore
        public /* final */ /* synthetic */ String f;
        @DexIgnore
        public /* final */ /* synthetic */ boolean g;

        @DexIgnore
        public f(BaseActivity baseActivity, String str, boolean z) {
            this.e = baseActivity;
            this.f = str;
            this.g = z;
        }

        @DexIgnore
        public final void run() {
            try {
                if (!this.e.isDestroyed()) {
                    if (!this.e.isFinishing()) {
                        this.e.v = bt3.g.a(this.f);
                        bt3 b = this.e.v;
                        if (b != null) {
                            b.setCancelable(this.g);
                            bb a = this.e.getSupportFragmentManager().a();
                            kd4.a((Object) a, "supportFragmentManager.beginTransaction()");
                            bt3 b2 = this.e.v;
                            if (b2 != null) {
                                a.a((Fragment) b2, "ProgressDialogFragment");
                                a.b();
                                return;
                            }
                            kd4.a();
                            throw null;
                        }
                        kd4.a();
                        throw null;
                    }
                }
                FLogger.INSTANCE.getLocal().d(this.e.f(), "Activity is destroy or finishing, no need to show dialog");
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String f2 = this.e.f();
                local.d(f2, "Exception when showing progress dialog=" + e2);
            }
        }
    }

    @DexIgnore
    public BaseActivity() {
        String simpleName = getClass().getSimpleName();
        kd4.a((Object) simpleName, "this.javaClass.simpleName");
        this.e = simpleName;
    }

    @DexIgnore
    public void a(int i2, List<String> list) {
        kd4.b(list, "perms");
    }

    @DexIgnore
    public final void a(MFDeviceService mFDeviceService) {
    }

    @DexIgnore
    public void attachBaseContext(Context context) {
        kd4.b(context, "newBase");
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    @DexIgnore
    public void b(int i2, List<String> list) {
        kd4.b(list, "perms");
    }

    @DexIgnore
    public void finish() {
        super.finish();
        if (i()) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    @DexIgnore
    public final int g() {
        try {
            Method method = Context.class.getMethod("getThemeResId", new Class[0]);
            kd4.a((Object) method, "currentClass.getMethod(\"getThemeResId\")");
            method.setAccessible(true);
            Object invoke = method.invoke(this, new Object[0]);
            if (invoke != null) {
                return ((Integer) invoke).intValue();
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Int");
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d(this.e, "Failed to get theme resource ID");
            return 0;
        }
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d("ProgressDialogFragment", "hideLoadingDialog");
        this.h.post(new c(this));
    }

    @DexIgnore
    public final boolean i() {
        return g() == 2131886094;
    }

    @DexIgnore
    public final void j() {
        try {
            b6.c(this);
        } catch (IllegalArgumentException unused) {
            finish();
        }
    }

    @DexIgnore
    public final void k() {
        startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
    }

    @DexIgnore
    public final void l() {
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, getPackageName(), (String) null));
        startActivity(intent);
    }

    @DexIgnore
    public final void m() {
        startActivity(new Intent("android.settings.SETTINGS"));
    }

    @DexIgnore
    public final synchronized void n() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.e;
        StringBuilder sb = new StringBuilder();
        sb.append("needToUpdateBLEWhenUpgradeLegacy - isNeedToUpdateBLE=");
        en2 en2 = this.q;
        if (en2 != null) {
            sb.append(en2.L());
            local.d(str, sb.toString());
            en2 en22 = this.q;
            if (en22 == null) {
                kd4.d("mSharePrefs");
                throw null;
            } else if (en22.L()) {
                try {
                    MigrationManager migrationManager = this.s;
                    if (migrationManager != null) {
                        migrationManager.c();
                    } else {
                        kd4.d("mMigrationManager");
                        throw null;
                    }
                } catch (Exception e2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = this.e;
                    local2.e(str2, "needToUpdateBLEWhenUpgradeLegacy - e=" + e2);
                }
            }
        } else {
            kd4.d("mSharePrefs");
            throw null;
        }
        return;
    }

    @DexIgnore
    public final void o() {
        View view = this.i;
        if (view != null && this.o) {
            if (view != null) {
                ViewParent parent = view.getParent();
                if (parent != null) {
                    ((ViewGroup) parent).removeView(this.i);
                    this.o = false;
                    this.h.removeCallbacks(this.y);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
                }
            } else {
                kd4.a();
                throw null;
            }
        }
        try {
            gr2 gr2 = this.t;
            if (gr2 != null) {
                gr2.g();
            } else {
                kd4.d("mGetRssi");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public void onBackPressed() {
        runOnUiThread(new e(this));
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        PortfolioApp.W.c().g().a(this);
        super.onCreate(bundle);
        if (i()) {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
        fn2.o.a().b();
        Window window = getWindow();
        kd4.a((Object) window, "window");
        View decorView = window.getDecorView();
        kd4.a((Object) decorView, "window.decorView");
        decorView.setSystemUiVisibility(3328);
        AnalyticsHelper.f.c();
    }

    @DexIgnore
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        kd4.b(menuItem, "item");
        if (menuItem.getItemId() == 16908332) {
            j();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        try {
            PortfolioApp.W.c(this);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.e;
            local.e(str, "Inside " + this.e + ".onPause - exception=" + e2);
        }
        if (!PortfolioApp.W.c().D()) {
            en2 en2 = this.q;
            if (en2 == null) {
                kd4.d("mSharePrefs");
                throw null;
            } else if (en2.J()) {
                o();
            }
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        PortfolioApp.W.b((Object) this);
        a(false);
        if (PortfolioApp.W.f()) {
            fn2.o.a().b();
        }
        if (!PortfolioApp.W.c().D()) {
            en2 en2 = this.q;
            if (en2 == null) {
                kd4.d("mSharePrefs");
                throw null;
            } else if (en2.J()) {
                a();
            }
        }
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        FLogger.INSTANCE.getLocal().d(this.e, "onStart()");
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d(this.e, "onStop()");
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        super.onTrimMemory(i2);
        System.runFinalization();
    }

    @DexIgnore
    public final void p() {
        a(this, false, (String) null, 2, (Object) null);
    }

    @DexIgnore
    public final void q() {
        this.u = PortfolioApp.W.c().e();
        if (!TextUtils.isEmpty(this.u)) {
            fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new BaseActivity$updateDeviceInfo$Anon1(this, (yb4) null), 3, (Object) null);
            b("Disconnected");
        }
    }

    @DexIgnore
    public void setContentView(int i2) {
        super.setContentView(i2);
    }

    @DexIgnore
    public void startActivityForResult(Intent intent, int i2) {
        if (intent == null) {
            intent = new Intent();
        }
        super.startActivityForResult(intent, i2);
    }

    @DexIgnore
    public final void b(boolean z2) {
        this.g = z2;
    }

    @DexIgnore
    public final void c(boolean z2) {
        this.f = z2;
    }

    @DexIgnore
    public final UserRepository d() {
        UserRepository userRepository = this.p;
        if (userRepository != null) {
            return userRepository;
        }
        kd4.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final int e() {
        Resources resources = PortfolioApp.W.c().getResources();
        int identifier = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (identifier > 0) {
            return resources.getDimensionPixelSize(identifier);
        }
        return 0;
    }

    @DexIgnore
    public final String f() {
        return this.e;
    }

    @DexIgnore
    public final DeviceRepository b() {
        DeviceRepository deviceRepository = this.r;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        kd4.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final gr2 c() {
        gr2 gr2 = this.t;
        if (gr2 != null) {
            return gr2;
        }
        kd4.d("mGetRssi");
        throw null;
    }

    @DexIgnore
    public final <T extends Service> void b(Class<? extends T>... clsArr) {
        kd4.b(clsArr, "services");
        FLogger.INSTANCE.getLocal().d(this.e, "unbindServices()");
        for (Class<? extends T> cls : clsArr) {
            if (kd4.a((Object) cls, (Object) MFDeviceService.class)) {
                if (this.f) {
                    FLogger.INSTANCE.getLocal().d(this.e, "Unbinding from mIsMisfitServiceBound");
                    ps3.a.a((Context) this, (ServiceConnection) this.x);
                    this.f = false;
                }
            } else if (kd4.a((Object) cls, (Object) ButtonService.class) && this.g) {
                FLogger.INSTANCE.getLocal().d(this.e, "Unbinding from mButtonServiceBound");
                ps3.a.a((Context) this, (ServiceConnection) this.w);
                this.g = false;
            }
        }
    }

    @DexIgnore
    public <T extends Service> void a(Class<? extends T>... clsArr) {
        kd4.b(clsArr, "services");
        FLogger.INSTANCE.getLocal().d(this.e, "executeServices()");
        for (Class<? extends T> cls : clsArr) {
            if (kd4.a((Object) cls, (Object) MFDeviceService.class)) {
                ps3.a.b(this, MFDeviceService.class, this.x, 0);
            } else if (kd4.a((Object) cls, (Object) ButtonService.class)) {
                ps3.a.b(this, ButtonService.class, this.w, 1);
            }
        }
    }

    @DexIgnore
    public final void a() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.e;
        local.d(str, "displayDeviceInfoView() called: isDeviceInfoViewAdded = " + this.o);
        if (!this.o) {
            Object systemService = getSystemService("layout_inflater");
            if (systemService != null) {
                this.i = ((LayoutInflater) systemService).inflate(R.layout.view_device_info, (ViewGroup) null);
                View view = this.i;
                if (view != null) {
                    view.setY((float) e());
                    View view2 = this.i;
                    if (view2 != null) {
                        this.j = (TextView) view2.findViewById(R.id.tv_device_serial);
                        View view3 = this.i;
                        if (view3 != null) {
                            this.k = (TextView) view3.findViewById(R.id.tv_device_battery);
                            View view4 = this.i;
                            if (view4 != null) {
                                this.l = (TextView) view4.findViewById(R.id.tv_device_rssi);
                                View view5 = this.i;
                                if (view5 != null) {
                                    this.m = (TextView) view5.findViewById(R.id.tv_device_firmware);
                                    View view6 = this.i;
                                    if (view6 != null) {
                                        TextView textView = (TextView) view6.findViewById(R.id.tv_device_top_button);
                                        View view7 = this.i;
                                        if (view7 != null) {
                                            TextView textView2 = (TextView) view7.findViewById(R.id.tv_device_middle_button);
                                            View view8 = this.i;
                                            if (view8 != null) {
                                                TextView textView3 = (TextView) view8.findViewById(R.id.tv_device_bottom_button);
                                                View view9 = this.i;
                                                if (view9 != null) {
                                                    this.n = (TextView) view9.findViewById(R.id.tv_access_token_expired_at);
                                                    getWindow().addContentView(this.i, new ConstraintLayout.LayoutParams((int) ts3.a(170, (Context) this), (int) ts3.a(130, (Context) this)));
                                                    this.o = true;
                                                    q();
                                                } else {
                                                    kd4.a();
                                                    throw null;
                                                }
                                            } else {
                                                kd4.a();
                                                throw null;
                                            }
                                        } else {
                                            kd4.a();
                                            throw null;
                                        }
                                    } else {
                                        kd4.a();
                                        throw null;
                                    }
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type android.view.LayoutInflater");
            }
        }
        gr2 gr2 = this.t;
        if (gr2 != null) {
            gr2.f();
        } else {
            kd4.d("mGetRssi");
            throw null;
        }
    }

    @DexIgnore
    public final void b(String str) {
        kd4.b(str, "rssiInfo");
        TextView textView = this.l;
        if (textView != null) {
            textView.setText(str);
            this.h.postDelayed(this.y, 1000);
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(String str) {
        kd4.b(str, "title");
        a(false, str);
    }

    @DexIgnore
    public static /* synthetic */ void a(BaseActivity baseActivity, boolean z2, String str, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                str = "";
            }
            baseActivity.a(z2, str);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: showLoadingDialog");
    }

    @DexIgnore
    public final void a(boolean z2, String str) {
        kd4.b(str, "title");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProgressDialogFragment", "showLoadingDialog: cancelable = " + z2);
        h();
        this.h.post(new f(this, str, z2));
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        kd4.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.e;
        local.d(str2, "Inside .onDialogFragmentResult tag=" + str);
        if (str.hashCode() == 2009556792 && str.equals(InAppPermission.NOTIFICATION_ACCESS) && i2 == R.id.ftv_go_to_setting) {
            k();
        }
    }

    @DexIgnore
    @TargetApi(23)
    public final void a(boolean z2) {
        if (Build.VERSION.SDK_INT >= 23) {
            Window window = getWindow();
            window.clearFlags(67108864);
            window.addFlags(Integer.MIN_VALUE);
            kd4.a((Object) window, "window");
            View decorView = window.getDecorView();
            kd4.a((Object) decorView, "window.decorView");
            int systemUiVisibility = decorView.getSystemUiVisibility();
            if (z2) {
                decorView.setSystemUiVisibility(systemUiVisibility & -8193);
            } else {
                decorView.setSystemUiVisibility(systemUiVisibility | 8192);
            }
        }
    }

    @DexIgnore
    public final void a(Fragment fragment, int i2) {
        kd4.b(fragment, "fragment");
        a(fragment, (String) null, i2);
    }

    @DexIgnore
    public final void a(Fragment fragment, String str, int i2) {
        kd4.b(fragment, "fragment");
        bb a2 = getSupportFragmentManager().a();
        a2.b(i2, fragment, str);
        a2.a(str);
        a2.b();
    }
}
