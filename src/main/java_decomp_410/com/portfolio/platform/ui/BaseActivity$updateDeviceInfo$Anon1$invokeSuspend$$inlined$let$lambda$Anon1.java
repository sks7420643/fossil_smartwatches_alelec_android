package com.portfolio.platform.ui;

import android.widget.TextView;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.Device;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BaseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Device $activeDevice$inlined;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BaseActivity$updateDeviceInfo$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(yb4 yb4, BaseActivity$updateDeviceInfo$Anon1 baseActivity$updateDeviceInfo$Anon1, Device device) {
        super(2, yb4);
        this.this$Anon0 = baseActivity$updateDeviceInfo$Anon1;
        this.$activeDevice$inlined = device;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        BaseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 baseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new BaseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(yb4, this.this$Anon0, this.$activeDevice$inlined);
        baseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return baseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BaseActivity$updateDeviceInfo$Anon1$invokeSuspend$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            TextView f = this.this$Anon0.this$Anon0.j;
            if (f != null) {
                f.setText(this.$activeDevice$inlined.getDeviceId());
                TextView d = this.this$Anon0.this$Anon0.k;
                if (d != null) {
                    d.setText(String.valueOf(this.$activeDevice$inlined.getBatteryLevel()));
                    TextView e = this.this$Anon0.this$Anon0.m;
                    if (e != null) {
                        e.setText(this.$activeDevice$inlined.getFirmwareRevision());
                        this.this$Anon0.this$Anon0.u = this.$activeDevice$inlined.getDeviceId();
                        return qa4.a;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
