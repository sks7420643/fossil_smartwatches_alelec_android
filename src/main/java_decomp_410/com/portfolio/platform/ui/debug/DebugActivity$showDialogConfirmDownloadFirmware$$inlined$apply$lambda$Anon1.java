package com.portfolio.platform.ui.debug;

import android.content.DialogInterface;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.data.model.DebugFirmwareData;
import com.portfolio.platform.data.model.Firmware;
import java.io.File;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1 implements DialogInterface.OnClickListener {
    @DexIgnore
    public /* final */ /* synthetic */ Firmware e;
    @DexIgnore
    public /* final */ /* synthetic */ DebugActivity f;
    @DexIgnore
    public /* final */ /* synthetic */ DebugFirmwareData g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0129Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super File>, Object> {
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0129Anon1(Anon1 anon1, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0129Anon1 anon1 = new C0129Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0129Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 zg4 = this.p$;
                    FirmwareFileRepository x = this.this$Anon0.this$Anon0.f.x();
                    String versionNumber = this.this$Anon0.this$Anon0.e.getVersionNumber();
                    kd4.a((Object) versionNumber, "firmware.versionNumber");
                    String downloadUrl = this.this$Anon0.this$Anon0.e.getDownloadUrl();
                    kd4.a((Object) downloadUrl, "firmware.downloadUrl");
                    String checksum = this.this$Anon0.this$Anon0.e.getChecksum();
                    kd4.a((Object) checksum, "firmware.checksum");
                    this.L$Anon0 = zg4;
                    this.label = 1;
                    obj = x.downloadFirmware(versionNumber, downloadUrl, checksum, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1 debugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = debugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            DebugFirmwareData debugFirmwareData;
            int i;
            Object a = cc4.a();
            int i2 = this.label;
            if (i2 == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                ug4 a2 = nh4.a();
                C0129Anon1 anon1 = new C0129Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = yf4.a(a2, anon1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i2 == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((File) obj) != null) {
                debugFirmwareData = this.this$Anon0.g;
                i = 2;
            } else {
                debugFirmwareData = this.this$Anon0.g;
                i = 0;
            }
            debugFirmwareData.setState(i);
            DebugActivity.b(this.this$Anon0.f).f();
            return qa4.a;
        }
    }

    @DexIgnore
    public DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$Anon1(Firmware firmware, DebugActivity debugActivity, DebugFirmwareData debugFirmwareData) {
        this.e = firmware;
        this.f = debugActivity;
        this.g = debugFirmwareData;
    }

    @DexIgnore
    public final void onClick(DialogInterface dialogInterface, int i) {
        fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (yb4) null), 3, (Object) null);
        this.g.setState(1);
        DebugActivity.b(this.f).f();
    }
}
