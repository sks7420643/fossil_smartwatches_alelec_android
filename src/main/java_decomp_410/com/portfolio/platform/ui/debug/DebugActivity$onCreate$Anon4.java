package com.portfolio.platform.ui.debug;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.DebugFirmwareData;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Firmware;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.debug.DebugActivity$onCreate$Anon4", f = "DebugActivity.kt", l = {}, m = "invokeSuspend")
public final class DebugActivity$onCreate$Anon4 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DebugActivity this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DebugActivity$onCreate$Anon4(DebugActivity debugActivity, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = debugActivity;
        this.$activeDeviceSerial = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DebugActivity$onCreate$Anon4 debugActivity$onCreate$Anon4 = new DebugActivity$onCreate$Anon4(this.this$Anon0, this.$activeDeviceSerial, yb4);
        debugActivity$onCreate$Anon4.p$ = (zg4) obj;
        return debugActivity$onCreate$Anon4;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DebugActivity$onCreate$Anon4) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            Device deviceBySerial = this.this$Anon0.b().getDeviceBySerial(this.$activeDeviceSerial);
            if (deviceBySerial != null) {
                DebugActivity debugActivity = this.this$Anon0;
                String sku = deviceBySerial.getSku();
                if (sku == null) {
                    sku = "";
                }
                debugActivity.L = sku;
                Firmware a = this.this$Anon0.C().a(this.this$Anon0.L);
                if (a != null) {
                    DebugActivity.b(this.this$Anon0).a(a);
                }
                if (!DebugActivity.b(this.this$Anon0).e()) {
                    DebugActivity.b(this.this$Anon0).a((xc4<? super yb4<? super List<DebugFirmwareData>>, ? extends Object>) new DebugActivity$onCreate$Anon4$invokeSuspend$$inlined$let$lambda$Anon1((yb4) null, this));
                }
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
