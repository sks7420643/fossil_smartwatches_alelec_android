package com.portfolio.platform.ui.debug;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum DebugViewType {
    CHILD,
    CHILD_ITEM_WITH_SWITCH,
    CHILD_ITEM_WITH_TEXT,
    CHILD_ITEM_WITH_SPINNER
}
