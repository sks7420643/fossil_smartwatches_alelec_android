package com.portfolio.platform.ui.debug;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.debug.DebugActivity", f = "DebugActivity.kt", l = {263}, m = "loadFirmware")
public final class DebugActivity$loadFirmware$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.debug.DebugActivity this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DebugActivity$loadFirmware$1(com.portfolio.platform.ui.debug.DebugActivity debugActivity, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = debugActivity;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.a((java.lang.String) null, (com.fossil.blesdk.obfuscated.yb4<? super java.util.List<com.portfolio.platform.data.model.DebugFirmwareData>>) this);
    }
}
