package com.portfolio.platform.ui.debug;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class DebugClearDataWarningActivity extends AppCompatActivity {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugClearDataWarningActivity$a$a")
        /* renamed from: com.portfolio.platform.ui.debug.DebugClearDataWarningActivity$a$a  reason: collision with other inner class name */
        public class C0130a implements Runnable {
            @DexIgnore
            public C0130a(a aVar) {
            }

            @DexIgnore
            public void run() {
                PortfolioApp.R.a();
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            Toast.makeText(DebugClearDataWarningActivity.this, "The app is being restarted...", 1).show();
            new Handler().postDelayed(new C0130a(this), 1500);
        }
    }

    @DexIgnore
    public static void a(Context context) {
        Intent intent = new Intent(context, DebugClearDataWarningActivity.class);
        intent.addFlags(268435456);
        context.startActivity(intent);
    }

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_debug_clear_data_warning);
        ((Button) findViewById(R.id.btn_got_it)).setOnClickListener(new a());
    }
}
