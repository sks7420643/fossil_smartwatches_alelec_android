package com.portfolio.platform.ui;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BaseActivity$mButtonServiceConnection$1 implements android.content.ServiceConnection {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.BaseActivity a;

    @DexIgnore
    public BaseActivity$mButtonServiceConnection$1(com.portfolio.platform.ui.BaseActivity baseActivity) {
        this.a = baseActivity;
    }

    @DexIgnore
    public void onServiceConnected(android.content.ComponentName componentName, android.os.IBinder iBinder) {
        com.fossil.blesdk.obfuscated.kd4.b(componentName, "name");
        com.fossil.blesdk.obfuscated.kd4.b(iBinder, com.misfit.frameworks.common.constants.Constants.SERVICE);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(this.a.f(), "Button service connected");
        com.portfolio.platform.ui.BaseActivity.A.a(com.misfit.frameworks.buttonservice.IButtonConnectivity.Stub.asInterface(iBinder));
        this.a.b(true);
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.b(com.fossil.blesdk.obfuscated.ah4.a(com.fossil.blesdk.obfuscated.nh4.b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.ui.BaseActivity$mButtonServiceConnection$1$onServiceConnected$1(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    public void onServiceDisconnected(android.content.ComponentName componentName) {
        com.fossil.blesdk.obfuscated.kd4.b(componentName, "name");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(this.a.f(), "Button service disconnected");
        this.a.b(false);
        com.portfolio.platform.ui.BaseActivity.A.a((com.misfit.frameworks.buttonservice.IButtonConnectivity) null);
    }
}
