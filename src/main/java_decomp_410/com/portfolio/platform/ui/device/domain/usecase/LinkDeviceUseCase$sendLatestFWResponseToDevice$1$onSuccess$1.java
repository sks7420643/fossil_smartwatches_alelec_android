package com.portfolio.platform.ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1", f = "LinkDeviceUseCase.kt", l = {}, m = "invokeSuspend")
public final class LinkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.d $responseValue;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1(com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$1 linkDeviceUseCase$sendLatestFWResponseToDevice$1, com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.d dVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = linkDeviceUseCase$sendLatestFWResponseToDevice$1;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1 linkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1 = new com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1(this.this$0, this.$responseValue, yb4);
        linkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return linkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            java.lang.String a = this.$responseValue.a();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.q.a();
            local.d(a2, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + a);
            com.misfit.frameworks.buttonservice.model.FirmwareData a3 = com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase.g.a(this.this$0.a.n, this.this$0.a.f());
            if (a3 == null) {
                a3 = new com.misfit.frameworks.buttonservice.model.EmptyFirmwareData();
            }
            com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.W.c();
            java.lang.String h = this.this$0.a.h();
            if (h != null) {
                c.a(h, (com.misfit.frameworks.buttonservice.model.pairing.PairingResponse) com.misfit.frameworks.buttonservice.model.pairing.PairingResponse.CREATOR.buildPairingUpdateFWResponse(a3));
                return com.fossil.blesdk.obfuscated.qa4.a;
            }
            com.fossil.blesdk.obfuscated.kd4.a();
            throw null;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
