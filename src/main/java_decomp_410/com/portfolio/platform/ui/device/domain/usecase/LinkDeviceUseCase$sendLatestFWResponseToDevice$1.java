package com.portfolio.platform.ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LinkDeviceUseCase$sendLatestFWResponseToDevice$1 implements com.portfolio.platform.CoroutineUseCase.e<com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.d, com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.c> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase a;

    @DexIgnore
    public LinkDeviceUseCase$sendLatestFWResponseToDevice$1(com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase) {
        this.a = linkDeviceUseCase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ void a(java.lang.Object obj) {
        a((com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.c) obj);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ void onSuccess(java.lang.Object obj) {
        a((com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.d) obj);
    }

    @DexIgnore
    public void a(com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.d dVar) {
        com.fossil.blesdk.obfuscated.kd4.b(dVar, "responseValue");
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.b(this.a.b(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1(this, dVar, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    public void a(com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.c cVar) {
        com.fossil.blesdk.obfuscated.kd4.b(cVar, "errorValue");
        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
        com.misfit.frameworks.buttonservice.log.FLogger.Component component = com.misfit.frameworks.buttonservice.log.FLogger.Component.API;
        com.misfit.frameworks.buttonservice.log.FLogger.Session session = com.misfit.frameworks.buttonservice.log.FLogger.Session.PAIR;
        java.lang.String h = this.a.h();
        if (h != null) {
            java.lang.String a2 = com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.q.a();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append(" downloadFw FAILED!!!, latestFwVersion=");
            java.lang.String h2 = this.a.h();
            if (h2 != null) {
                sb.append(h2);
                sb.append(" but device is DianaEV1!!!");
                remote.e(component, session, h, a2, sb.toString());
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().e(com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.q.a(), "checkFirmware - downloadFw FAILED!!!");
                com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase = this.a;
                java.lang.String h3 = linkDeviceUseCase.h();
                if (h3 != null) {
                    linkDeviceUseCase.a(new com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.d(h3, ""));
                    com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.W.c();
                    java.lang.String h4 = this.a.h();
                    if (h4 != null) {
                        c.a(h4);
                        this.a.a(false);
                        return;
                    }
                    com.fossil.blesdk.obfuscated.kd4.a();
                    throw null;
                }
                com.fossil.blesdk.obfuscated.kd4.a();
                throw null;
            }
            com.fossil.blesdk.obfuscated.kd4.a();
            throw null;
        }
        com.fossil.blesdk.obfuscated.kd4.a();
        throw null;
    }
}
