package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.vj2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import java.util.ArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UnlinkDeviceUseCase extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a((fd4) null);
    @DexIgnore
    public b d;
    @DexIgnore
    public /* final */ DeviceRepository e;
    @DexIgnore
    public /* final */ PortfolioApp f;
    @DexIgnore
    public /* final */ WatchFaceRepository g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return UnlinkDeviceUseCase.h;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            kd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public c(String str, ArrayList<Integer> arrayList, String str2) {
            kd4.b(str, "lastErrorCode");
            this.a = str;
            this.b = arrayList;
            this.c = str2;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> c() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        String simpleName = UnlinkDeviceUseCase.class.getSimpleName();
        kd4.a((Object) simpleName, "UnlinkDeviceUseCase::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public UnlinkDeviceUseCase(DeviceRepository deviceRepository, vj2 vj2, PortfolioApp portfolioApp, WatchFaceRepository watchFaceRepository) {
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(vj2, "mDeviceSettingFactory");
        kd4.b(portfolioApp, "mApp");
        kd4.b(watchFaceRepository, "watchFaceRepository");
        this.e = deviceRepository;
        this.f = portfolioApp;
        this.g = watchFaceRepository;
    }

    @DexIgnore
    public final fi4 d() {
        return ag4.b(b(), (CoroutineContext) null, (CoroutineStart) null, new UnlinkDeviceUseCase$doRemoveDevice$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public String c() {
        return h;
    }

    @DexIgnore
    public Object a(b bVar, yb4<Object> yb4) {
        FLogger.INSTANCE.getLocal().d(h, "running UseCase");
        if (bVar == null) {
            return new c("", (ArrayList<Integer>) null, "");
        }
        this.d = bVar;
        String a2 = bVar.a();
        String e2 = this.f.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "remove device " + a2 + " currentActive " + e2);
        d();
        return new Object();
    }
}
