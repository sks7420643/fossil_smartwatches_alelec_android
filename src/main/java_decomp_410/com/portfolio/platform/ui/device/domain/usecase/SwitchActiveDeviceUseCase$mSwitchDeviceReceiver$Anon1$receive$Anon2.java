package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yk2;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2", f = "SwitchActiveDeviceUseCase.kt", l = {95}, m = "invokeSuspend")
public final class SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public int I$Anon0;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2(SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1 switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1, MisfitDeviceProfile misfitDeviceProfile, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1;
        this.$currentDeviceProfile = misfitDeviceProfile;
        this.$serial = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2 switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2 = new SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2(this.this$Anon0, this.$currentDeviceProfile, this.$serial, yb4);
        switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2.p$ = (zg4) obj;
        return switchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$Anon1$receive$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            int b = yk2.b(this.$currentDeviceProfile.getVibrationStrength().getVibrationStrengthLevel());
            DeviceRepository b2 = this.this$Anon0.a.i;
            String str = this.$serial;
            kd4.a((Object) str, "serial");
            Device deviceBySerial = b2.getDeviceBySerial(str);
            if (deviceBySerial != null) {
                Integer vibrationStrength = deviceBySerial.getVibrationStrength();
                if (vibrationStrength == null || vibrationStrength.intValue() != b) {
                    deviceBySerial.setVibrationStrength(dc4.a(b));
                    DeviceRepository b3 = this.this$Anon0.a.i;
                    this.L$Anon0 = zg4;
                    this.I$Anon0 = b;
                    this.L$Anon1 = deviceBySerial;
                    this.label = 1;
                    if (b3.updateDevice(deviceBySerial, false, this) == a) {
                        return a;
                    }
                }
            }
        } else if (i == 1) {
            Device device = (Device) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
