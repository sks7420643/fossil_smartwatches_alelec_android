package com.portfolio.platform.ui.device.domain.usecase;

import android.util.SparseArray;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.util.NotificationAppHelper;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase$saveNotificationSettingToDevice$Anon1", f = "HybridSyncUseCase.kt", l = {}, m = "invokeSuspend")
public final class HybridSyncUseCase$saveNotificationSettingToDevice$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ SparseArray $data;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isMovemberModel;
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HybridSyncUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridSyncUseCase$saveNotificationSettingToDevice$Anon1(HybridSyncUseCase hybridSyncUseCase, SparseArray sparseArray, boolean z, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = hybridSyncUseCase;
        this.$data = sparseArray;
        this.$isMovemberModel = z;
        this.$serialNumber = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // HybridSyncUseCase$saveNotificationSettingToDevice$Anon1 hybridSyncUseCase$saveNotificationSettingToDevice$Anon1 = new HybridSyncUseCase$saveNotificationSettingToDevice$Anon1(this.this$Anon0, this.$data, this.$isMovemberModel, this.$serialNumber, yb4);
        // hybridSyncUseCase$saveNotificationSettingToDevice$Anon1.p$ = (zg4) obj;
        // return hybridSyncUseCase$saveNotificationSettingToDevice$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((HybridSyncUseCase$saveNotificationSettingToDevice$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            AppNotificationFilterSettings a = NotificationAppHelper.b.a((SparseArray<List<BaseFeatureModel>>) this.$data, this.$isMovemberModel);
            PortfolioApp.W.c().a(a, this.$serialNumber);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String c = this.this$Anon0.c();
            local.d(c, "saveNotificationSettingToDevice, total: " + a.getNotificationFilters().size() + " items");
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
