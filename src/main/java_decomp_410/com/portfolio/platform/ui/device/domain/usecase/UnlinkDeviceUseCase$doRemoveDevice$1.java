package com.portfolio.platform.ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$1", f = "UnlinkDeviceUseCase.kt", l = {57}, m = "invokeSuspend")
public final class UnlinkDeviceUseCase$doRemoveDevice$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UnlinkDeviceUseCase$doRemoveDevice$1(com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase unlinkDeviceUseCase, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = unlinkDeviceUseCase;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$1 unlinkDeviceUseCase$doRemoveDevice$1 = new com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$1(this.this$0, yb4);
        unlinkDeviceUseCase$doRemoveDevice$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return unlinkDeviceUseCase$doRemoveDevice$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d3, code lost:
        if (r12 != null) goto L_0x00d7;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.data.model.Device device;
        java.lang.String str;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.p$;
            com.portfolio.platform.data.source.DeviceRepository a2 = this.this$0.e;
            com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase.b b = this.this$0.d;
            java.lang.String str2 = null;
            java.lang.String a3 = b != null ? b.a() : null;
            if (a3 != null) {
                com.portfolio.platform.data.model.Device deviceBySerial = a2.getDeviceBySerial(a3);
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a4 = com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase.i.a();
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append("doRemoveDevice ");
                if (deviceBySerial != null) {
                    str2 = deviceBySerial.getDeviceId();
                }
                sb.append(str2);
                local.d(a4, sb.toString());
                if (deviceBySerial != null) {
                    com.portfolio.platform.data.source.DeviceRepository a5 = this.this$0.e;
                    this.L$0 = zg4;
                    this.L$1 = deviceBySerial;
                    this.label = 1;
                    obj = a5.removeDevice(deviceBySerial, this);
                    if (obj == a) {
                        return a;
                    }
                    device = deviceBySerial;
                } else {
                    this.this$0.a(new com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase.c("UNLINK_FAIL_ON_SERVER", com.fossil.blesdk.obfuscated.cb4.a((T[]) new java.lang.Integer[]{com.fossil.blesdk.obfuscated.dc4.a(600)}), ""));
                    return com.fossil.blesdk.obfuscated.qa4.a;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.a();
                throw null;
            }
        } else if (i == 1) {
            device = (com.portfolio.platform.data.model.Device) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
        if (qo2 instanceof com.fossil.blesdk.obfuscated.ro2) {
            java.lang.String deviceId = device.getDeviceId();
            com.portfolio.platform.PortfolioApp.W.c().r(deviceId);
            if (com.portfolio.platform.helper.DeviceHelper.o.f(deviceId)) {
                this.this$0.g.deleteWatchFacesWithSerial(deviceId);
            }
            this.this$0.a(new com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase.d());
        } else if (qo2 instanceof com.fossil.blesdk.obfuscated.po2) {
            com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase unlinkDeviceUseCase = this.this$0;
            com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
            java.util.ArrayList a6 = com.fossil.blesdk.obfuscated.cb4.a((T[]) new java.lang.Integer[]{com.fossil.blesdk.obfuscated.dc4.a(po2.a())});
            com.portfolio.platform.data.model.ServerError c = po2.c();
            if (c != null) {
                str = c.getUserMessage();
            }
            str = "";
            unlinkDeviceUseCase.a(new com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase.c("UNLINK_FAIL_ON_SERVER", a6, str));
        }
        return com.fossil.blesdk.obfuscated.qa4.a;
    }
}
