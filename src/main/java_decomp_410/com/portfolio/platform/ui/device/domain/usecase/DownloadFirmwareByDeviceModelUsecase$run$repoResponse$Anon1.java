package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$run$repoResponse$Anon1", f = "DownloadFirmwareByDeviceModelUsecase.kt", l = {37}, m = "invokeSuspend")
public final class DownloadFirmwareByDeviceModelUsecase$run$repoResponse$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<ApiResponse<Firmware>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceModel;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ DownloadFirmwareByDeviceModelUsecase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DownloadFirmwareByDeviceModelUsecase$run$repoResponse$Anon1(DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase, String str, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = downloadFirmwareByDeviceModelUsecase;
        this.$deviceModel = str;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new DownloadFirmwareByDeviceModelUsecase$run$repoResponse$Anon1(this.this$Anon0, this.$deviceModel, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((DownloadFirmwareByDeviceModelUsecase$run$repoResponse$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            GuestApiService b = this.this$Anon0.e;
            String h = this.this$Anon0.d.h();
            String str = this.$deviceModel;
            this.label = 1;
            obj = b.getFirmwares(h, str, "android", this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
