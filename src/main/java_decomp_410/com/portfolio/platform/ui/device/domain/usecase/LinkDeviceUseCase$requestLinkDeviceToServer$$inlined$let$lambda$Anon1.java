package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LinkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Device $deviceModel;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LinkDeviceUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$Anon1(Device device, yb4 yb4, LinkDeviceUseCase linkDeviceUseCase) {
        super(2, yb4);
        this.$deviceModel = device;
        this.this$Anon0 = linkDeviceUseCase;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        LinkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$Anon1 linkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$Anon1 = new LinkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$Anon1(this.$deviceModel, yb4, this.this$Anon0);
        linkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return linkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LinkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.API;
            FLogger.Session session = FLogger.Session.PAIR;
            String deviceId = this.$deviceModel.getDeviceId();
            String a2 = LinkDeviceUseCase.q.a();
            remote.i(component, session, deviceId, a2, "Steal a device with serial " + this.$deviceModel.getDeviceId());
            PortfolioApp c = PortfolioApp.W.c();
            CommunicateMode communicateMode = CommunicateMode.LINK;
            String deviceId2 = this.$deviceModel.getDeviceId();
            c.a(communicateMode, deviceId2, "Force link a device with serial " + this.$deviceModel.getDeviceId());
            DeviceRepository a3 = this.this$Anon0.l;
            Device device = this.$deviceModel;
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = a3.forceLinkDevice(device, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo2 = (qo2) obj;
        if (qo2 instanceof ro2) {
            IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
            FLogger.Component component2 = FLogger.Component.API;
            FLogger.Session session2 = FLogger.Session.PAIR;
            String deviceId3 = this.$deviceModel.getDeviceId();
            String a4 = LinkDeviceUseCase.q.a();
            remote2.i(component2, session2, deviceId3, a4, "Steal a device with serial " + this.$deviceModel.getDeviceId() + " success");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a5 = LinkDeviceUseCase.q.a();
            local.d(a5, "forceLinkDevice onSuccess device=" + this.$deviceModel.getDeviceId());
            PortfolioApp.W.c().a(this.$deviceModel.getDeviceId(), (PairingResponse) PairingResponse.CREATOR.buildPairingLinkServerResponse(true, 0));
        } else if (qo2 instanceof po2) {
            IRemoteFLogger remote3 = FLogger.INSTANCE.getRemote();
            FLogger.Component component3 = FLogger.Component.API;
            FLogger.Session session3 = FLogger.Session.PAIR;
            String deviceId4 = this.$deviceModel.getDeviceId();
            String a6 = LinkDeviceUseCase.q.a();
            StringBuilder sb = new StringBuilder();
            sb.append("Steal a device with serial ");
            sb.append(this.$deviceModel.getDeviceId());
            sb.append(", server error=");
            po2 po2 = (po2) qo2;
            sb.append(po2.a());
            sb.append(", error = ");
            sb.append(ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.LINK_DEVICE, ErrorCodeBuilder.Component.APP, ErrorCodeBuilder.AppError.NETWORK_ERROR));
            remote3.e(component3, session3, deviceId4, a6, sb.toString());
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a7 = LinkDeviceUseCase.q.a();
            local2.d(a7, "forceLinkDevice onFail errorCode=" + po2.a());
            this.this$Anon0.a(new LinkDeviceUseCase.j(po2.a(), this.$deviceModel.getDeviceId(), ""));
            PortfolioApp.W.c().a(this.$deviceModel.getDeviceId(), (PairingResponse) PairingResponse.CREATOR.buildPairingLinkServerResponse(false, po2.a()));
        }
        return qa4.a;
    }
}
