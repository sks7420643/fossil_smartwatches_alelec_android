package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1", f = "SwitchActiveDeviceUseCase.kt", l = {180}, m = "invokeSuspend")
public final class SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $newActiveDeviceSerial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SwitchActiveDeviceUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1(SwitchActiveDeviceUseCase switchActiveDeviceUseCase, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = switchActiveDeviceUseCase;
        this.$newActiveDeviceSerial = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1 switchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1 = new SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1(this.this$Anon0, this.$newActiveDeviceSerial, yb4);
        switchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1.p$ = (zg4) obj;
        return switchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        String str;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SwitchActiveDeviceUseCase.n.a();
            local.d(a2, "could not erase new data file " + this.$newActiveDeviceSerial + ", force switch to it");
            String e = this.this$Anon0.k.e();
            SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.this$Anon0;
            switchActiveDeviceUseCase.a(switchActiveDeviceUseCase.i.getDeviceBySerial(this.$newActiveDeviceSerial));
            boolean e2 = PortfolioApp.W.c().e(this.$newActiveDeviceSerial);
            if (e2) {
                SwitchActiveDeviceUseCase switchActiveDeviceUseCase2 = this.this$Anon0;
                String str2 = this.$newActiveDeviceSerial;
                this.L$Anon0 = zg4;
                this.L$Anon1 = e;
                this.Z$Anon0 = e2;
                this.label = 1;
                obj = switchActiveDeviceUseCase2.a(str2, (MisfitDeviceProfile) null, this);
                if (obj == a) {
                    return a;
                }
                str = e;
            } else {
                this.this$Anon0.a(new SwitchActiveDeviceUseCase.c(116, (ArrayList<Integer>) null, ""));
                return qa4.a;
            }
        } else if (i == 1) {
            str = (String) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (((Boolean) ((Pair) obj).component1()).booleanValue()) {
            this.this$Anon0.b(this.$newActiveDeviceSerial);
            SwitchActiveDeviceUseCase switchActiveDeviceUseCase3 = this.this$Anon0;
            switchActiveDeviceUseCase3.a(new SwitchActiveDeviceUseCase.d(switchActiveDeviceUseCase3.e()));
        } else {
            PortfolioApp.W.c().e(str);
            SwitchActiveDeviceUseCase.c g = this.this$Anon0.g();
            if (g == null || this.this$Anon0.a(g) == null) {
                this.this$Anon0.a(new SwitchActiveDeviceUseCase.c(116, (ArrayList<Integer>) null, ""));
            }
        }
        return qa4.a;
    }
}
