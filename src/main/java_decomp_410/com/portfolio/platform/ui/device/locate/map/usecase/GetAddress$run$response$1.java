package com.portfolio.platform.ui.device.locate.map.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$run$response$1", f = "GetAddress.kt", l = {19}, m = "invokeSuspend")
public final class GetAddress$run$response$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.fossil.blesdk.obfuscated.xz1>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.device.locate.map.usecase.GetAddress.b $requestValues;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.device.locate.map.usecase.GetAddress this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetAddress$run$response$1(com.portfolio.platform.ui.device.locate.map.usecase.GetAddress getAddress, com.portfolio.platform.ui.device.locate.map.usecase.GetAddress.b bVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(1, yb4);
        this.this$0 = getAddress;
        this.$requestValues = bVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        return new com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$run$response$1(this.this$0, this.$requestValues, yb4);
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj) {
        return ((com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$run$response$1) create((com.fossil.blesdk.obfuscated.yb4) obj)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.portfolio.platform.data.source.remote.GoogleApiService a2 = this.this$0.d;
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            com.portfolio.platform.ui.device.locate.map.usecase.GetAddress.b bVar = this.$requestValues;
            java.lang.Double d = null;
            sb.append(bVar != null ? com.fossil.blesdk.obfuscated.dc4.a(bVar.a()) : null);
            sb.append(',');
            com.portfolio.platform.ui.device.locate.map.usecase.GetAddress.b bVar2 = this.$requestValues;
            if (bVar2 != null) {
                d = com.fossil.blesdk.obfuscated.dc4.a(bVar2.b());
            }
            sb.append(d);
            java.lang.String sb2 = sb.toString();
            this.label = 1;
            obj = a2.getAddress(sb2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
