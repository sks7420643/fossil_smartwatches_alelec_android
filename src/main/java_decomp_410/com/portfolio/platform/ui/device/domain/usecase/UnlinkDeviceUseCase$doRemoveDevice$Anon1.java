package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase;
import java.util.ArrayList;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$Anon1", f = "UnlinkDeviceUseCase.kt", l = {57}, m = "invokeSuspend")
public final class UnlinkDeviceUseCase$doRemoveDevice$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UnlinkDeviceUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UnlinkDeviceUseCase$doRemoveDevice$Anon1(UnlinkDeviceUseCase unlinkDeviceUseCase, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = unlinkDeviceUseCase;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        UnlinkDeviceUseCase$doRemoveDevice$Anon1 unlinkDeviceUseCase$doRemoveDevice$Anon1 = new UnlinkDeviceUseCase$doRemoveDevice$Anon1(this.this$Anon0, yb4);
        unlinkDeviceUseCase$doRemoveDevice$Anon1.p$ = (zg4) obj;
        return unlinkDeviceUseCase$doRemoveDevice$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((UnlinkDeviceUseCase$doRemoveDevice$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d3, code lost:
        if (r12 != null) goto L_0x00d7;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Device device;
        String str;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            DeviceRepository a2 = this.this$Anon0.e;
            UnlinkDeviceUseCase.b b = this.this$Anon0.d;
            String str2 = null;
            String a3 = b != null ? b.a() : null;
            if (a3 != null) {
                Device deviceBySerial = a2.getDeviceBySerial(a3);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a4 = UnlinkDeviceUseCase.i.a();
                StringBuilder sb = new StringBuilder();
                sb.append("doRemoveDevice ");
                if (deviceBySerial != null) {
                    str2 = deviceBySerial.getDeviceId();
                }
                sb.append(str2);
                local.d(a4, sb.toString());
                if (deviceBySerial != null) {
                    DeviceRepository a5 = this.this$Anon0.e;
                    this.L$Anon0 = zg4;
                    this.L$Anon1 = deviceBySerial;
                    this.label = 1;
                    obj = a5.removeDevice(deviceBySerial, this);
                    if (obj == a) {
                        return a;
                    }
                    device = deviceBySerial;
                } else {
                    this.this$Anon0.a(new UnlinkDeviceUseCase.c("UNLINK_FAIL_ON_SERVER", cb4.a((T[]) new Integer[]{dc4.a(600)}), ""));
                    return qa4.a;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else if (i == 1) {
            device = (Device) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo2 = (qo2) obj;
        if (qo2 instanceof ro2) {
            String deviceId = device.getDeviceId();
            PortfolioApp.W.c().r(deviceId);
            if (DeviceHelper.o.f(deviceId)) {
                this.this$Anon0.g.deleteWatchFacesWithSerial(deviceId);
            }
            this.this$Anon0.a(new UnlinkDeviceUseCase.d());
        } else if (qo2 instanceof po2) {
            UnlinkDeviceUseCase unlinkDeviceUseCase = this.this$Anon0;
            po2 po2 = (po2) qo2;
            ArrayList a6 = cb4.a((T[]) new Integer[]{dc4.a(po2.a())});
            ServerError c = po2.c();
            if (c != null) {
                str = c.getUserMessage();
            }
            str = "";
            unlinkDeviceUseCase.a(new UnlinkDeviceUseCase.c("UNLINK_FAIL_ON_SERVER", a6, str));
        }
        return qa4.a;
    }
}
