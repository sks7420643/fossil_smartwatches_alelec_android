package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$Anon1", f = "SetVibrationStrengthUseCase.kt", l = {49}, m = "invokeSuspend")
public final class SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SetVibrationStrengthUseCase.SetVibrationStrengthBroadcastReceiver this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$Anon1(SetVibrationStrengthUseCase.SetVibrationStrengthBroadcastReceiver setVibrationStrengthBroadcastReceiver, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = setVibrationStrengthBroadcastReceiver;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$Anon1 setVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$Anon1 = new SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$Anon1(this.this$Anon0, yb4);
        setVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$Anon1.p$ = (zg4) obj;
        return setVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            Device deviceBySerial = SetVibrationStrengthUseCase.this.h.getDeviceBySerial(SetVibrationStrengthUseCase.this.e());
            if (deviceBySerial != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = SetVibrationStrengthUseCase.j.a();
                local.d(a2, "Update vibration stregnth " + SetVibrationStrengthUseCase.this.d() + " to db");
                deviceBySerial.setVibrationStrength(dc4.a(SetVibrationStrengthUseCase.this.d()));
                DeviceRepository a3 = SetVibrationStrengthUseCase.this.h;
                this.L$Anon0 = zg4;
                this.L$Anon1 = deviceBySerial;
                this.L$Anon2 = deviceBySerial;
                this.label = 1;
                obj = a3.updateDevice(deviceBySerial, false, this);
                if (obj == a) {
                    return a;
                }
            }
            FLogger.INSTANCE.getLocal().d(SetVibrationStrengthUseCase.j.a(), "onReceive #getDeviceBySerial success");
            SetVibrationStrengthUseCase.this.a(new SetVibrationStrengthUseCase.d());
            return qa4.a;
        } else if (i == 1) {
            Device device = (Device) this.L$Anon2;
            Device device2 = (Device) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo2 = (qo2) obj;
        FLogger.INSTANCE.getLocal().d(SetVibrationStrengthUseCase.j.a(), "onReceive #getDeviceBySerial success");
        SetVibrationStrengthUseCase.this.a(new SetVibrationStrengthUseCase.d());
        return qa4.a;
    }
}
