package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1 implements CoroutineUseCase.e<DownloadFirmwareByDeviceModelUsecase.d, DownloadFirmwareByDeviceModelUsecase.c> {
    @DexIgnore
    public /* final */ /* synthetic */ LinkDeviceUseCase a;

    @DexIgnore
    public LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1(LinkDeviceUseCase linkDeviceUseCase) {
        this.a = linkDeviceUseCase;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(DownloadFirmwareByDeviceModelUsecase.d dVar) {
        kd4.b(dVar, "responseValue");
        fi4 unused = ag4.b(this.a.b(), (CoroutineContext) null, (CoroutineStart) null, new LinkDeviceUseCase$sendLatestFWResponseToDevice$Anon1$onSuccess$Anon1(this, dVar, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(DownloadFirmwareByDeviceModelUsecase.c cVar) {
        kd4.b(cVar, "errorValue");
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.API;
        FLogger.Session session = FLogger.Session.PAIR;
        String h = this.a.h();
        if (h != null) {
            String a2 = LinkDeviceUseCase.q.a();
            StringBuilder sb = new StringBuilder();
            sb.append(" downloadFw FAILED!!!, latestFwVersion=");
            String h2 = this.a.h();
            if (h2 != null) {
                sb.append(h2);
                sb.append(" but device is DianaEV1!!!");
                remote.e(component, session, h, a2, sb.toString());
                FLogger.INSTANCE.getLocal().e(LinkDeviceUseCase.q.a(), "checkFirmware - downloadFw FAILED!!!");
                LinkDeviceUseCase linkDeviceUseCase = this.a;
                String h3 = linkDeviceUseCase.h();
                if (h3 != null) {
                    linkDeviceUseCase.a(new LinkDeviceUseCase.d(h3, ""));
                    PortfolioApp c = PortfolioApp.W.c();
                    String h4 = this.a.h();
                    if (h4 != null) {
                        c.a(h4);
                        this.a.a(false);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }
}
