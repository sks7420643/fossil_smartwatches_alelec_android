package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1", f = "DianaSyncUseCase.kt", l = {}, m = "invokeSuspend")
public final class DianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaSyncUseCase$verifySecretKey$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1(DianaSyncUseCase$verifySecretKey$Anon1 dianaSyncUseCase$verifySecretKey$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = dianaSyncUseCase$verifySecretKey$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // DianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1 dianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1 = new DianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1(this.this$Anon0, yb4);
        // dianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1.p$ = (zg4) obj;
        // return dianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((DianaSyncUseCase$verifySecretKey$Anon1$onSuccess$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            DianaSyncUseCase$verifySecretKey$Anon1 dianaSyncUseCase$verifySecretKey$Anon1 = this.this$Anon0;
            dianaSyncUseCase$verifySecretKey$Anon1.a.a(dianaSyncUseCase$verifySecretKey$Anon1.c, dianaSyncUseCase$verifySecretKey$Anon1.d, dianaSyncUseCase$verifySecretKey$Anon1.e, dianaSyncUseCase$verifySecretKey$Anon1.b);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
