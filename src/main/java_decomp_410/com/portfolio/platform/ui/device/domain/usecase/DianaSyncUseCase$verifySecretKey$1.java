package com.portfolio.platform.ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaSyncUseCase$verifySecretKey$1 implements com.portfolio.platform.CoroutineUseCase.e<com.portfolio.platform.usecase.VerifySecretKeyUseCase.d, com.portfolio.platform.usecase.VerifySecretKeyUseCase.c> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase a;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String b;
    @DexIgnore
    public /* final */ /* synthetic */ int c;
    @DexIgnore
    public /* final */ /* synthetic */ boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.model.UserProfile e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements com.portfolio.platform.CoroutineUseCase.e<com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase.d, com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase.c> {
        @DexIgnore
        public void a(com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase.c cVar) {
            com.fossil.blesdk.obfuscated.kd4.b(cVar, "errorValue");
        }

        @DexIgnore
        public void a(com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase.d dVar) {
            com.fossil.blesdk.obfuscated.kd4.b(dVar, "responseValue");
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ void a(java.lang.Object obj) {
            a((com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase.c) obj);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ void onSuccess(java.lang.Object obj) {
            a((com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase.d) obj);
        }
    }

    @DexIgnore
    public DianaSyncUseCase$verifySecretKey$1(com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase dianaSyncUseCase, java.lang.String str, int i, boolean z, com.misfit.frameworks.buttonservice.model.UserProfile userProfile) {
        this.a = dianaSyncUseCase;
        this.b = str;
        this.c = i;
        this.d = z;
        this.e = userProfile;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ void a(java.lang.Object obj) {
        a((com.portfolio.platform.usecase.VerifySecretKeyUseCase.c) obj);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ void onSuccess(java.lang.Object obj) {
        a((com.portfolio.platform.usecase.VerifySecretKeyUseCase.d) obj);
    }

    @DexIgnore
    public void a(com.portfolio.platform.usecase.VerifySecretKeyUseCase.d dVar) {
        com.fossil.blesdk.obfuscated.kd4.b(dVar, "responseValue");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a2 = com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase.r.a();
        local.d(a2, "secret key " + dVar.a() + ", start sync");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER, this.b, com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase.r.a(), "[Sync] Verify secret key success, start sync");
        com.portfolio.platform.PortfolioApp.W.c().e(dVar.a(), this.b);
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.b(this.a.b(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1$onSuccess$1(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    public void a(com.portfolio.platform.usecase.VerifySecretKeyUseCase.c cVar) {
        com.fossil.blesdk.obfuscated.kd4.b(cVar, "errorValue");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a2 = com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase.r.a();
        local.d(a2, "verify secret key fail, stop sync " + cVar.a());
        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
        com.misfit.frameworks.buttonservice.log.FLogger.Component component = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
        com.misfit.frameworks.buttonservice.log.FLogger.Session session = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER;
        java.lang.String str = this.b;
        java.lang.String a3 = com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase.r.a();
        remote.i(component, session, str, a3, "[Sync] Verify secret key failed by " + cVar.a());
        if (cVar.a() == com.fossil.blesdk.device.FeatureErrorCode.REQUEST_UNSUPPORTED.getCode() && this.e.getOriginalSyncMode() == 12) {
            this.a.m.a(new com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase.b(this.b, true), new com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1.a());
        }
        this.a.a(this.e.getOriginalSyncMode(), this.b, 2);
    }
}
