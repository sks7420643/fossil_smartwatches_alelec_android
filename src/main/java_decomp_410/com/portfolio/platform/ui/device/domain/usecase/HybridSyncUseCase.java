package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.ar2;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.nj2;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.sj2;
import com.fossil.blesdk.obfuscated.ul2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yq2;
import com.fossil.blesdk.obfuscated.zq2;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.enums.SyncResponseCode;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.util.NotificationAppHelper;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridSyncUseCase extends CoroutineUseCase<zq2, ar2, yq2> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public /* final */ b d; // = new b(this);
    @DexIgnore
    public /* final */ MicroAppRepository e;
    @DexIgnore
    public /* final */ en2 f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ PortfolioApp h;
    @DexIgnore
    public /* final */ HybridPresetRepository i;
    @DexIgnore
    public /* final */ NotificationsRepository j;
    @DexIgnore
    public /* final */ AnalyticsHelper k;
    @DexIgnore
    public /* final */ AlarmsRepository l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ HybridSyncUseCase a;

        @DexIgnore
        public b(HybridSyncUseCase hybridSyncUseCase) {
            this.a = hybridSyncUseCase;
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            kd4.b(communicateMode, "communicateMode");
            kd4.b(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && qf4.b(stringExtra, this.a.h.e(), true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                if (intExtra == 1) {
                    FLogger.INSTANCE.getLocal().d(HybridSyncUseCase.m, "sync success, remove device now");
                    BleCommandResultManager.d.b((BleCommandResultManager.b) this, CommunicateMode.SYNC);
                    this.a.a(new ar2());
                } else if (intExtra == 2 || intExtra == 4) {
                    BleCommandResultManager.d.b((BleCommandResultManager.b) this, CommunicateMode.SYNC);
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>();
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String d = HybridSyncUseCase.m;
                    local.d(d, "sync fail due to " + intExtra2);
                    if (intExtra2 == 1101 || intExtra2 == 1112 || intExtra2 == 1113) {
                        this.a.a(new yq2(SyncResponseCode.FAIL_DUE_TO_LACK_PERMISSION, integerArrayListExtra));
                    } else {
                        this.a.a(new yq2(SyncResponseCode.FAIL_DUE_TO_SYNC_FAIL, (ArrayList<Integer>) null));
                    }
                }
            }
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = HybridSyncUseCase.class.getSimpleName();
        kd4.a((Object) simpleName, "HybridSyncUseCase::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public HybridSyncUseCase(MicroAppRepository microAppRepository, en2 en2, DeviceRepository deviceRepository, PortfolioApp portfolioApp, HybridPresetRepository hybridPresetRepository, NotificationsRepository notificationsRepository, AnalyticsHelper analyticsHelper, AlarmsRepository alarmsRepository) {
        kd4.b(microAppRepository, "mMicroAppRepository");
        kd4.b(en2, "mSharedPrefs");
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(portfolioApp, "mApp");
        kd4.b(hybridPresetRepository, "mPresetRepository");
        kd4.b(notificationsRepository, "mNotificationsRepository");
        kd4.b(analyticsHelper, "mAnalyticsHelper");
        kd4.b(alarmsRepository, "mAlarmsRepository");
        this.e = microAppRepository;
        this.f = en2;
        this.g = deviceRepository;
        this.h = portfolioApp;
        this.i = hybridPresetRepository;
        this.j = notificationsRepository;
        this.k = analyticsHelper;
        this.l = alarmsRepository;
    }

    @DexIgnore
    public String c() {
        return "HybridSyncUseCase";
    }

    @DexIgnore
    public Object a(zq2 zq2, yb4<Object> yb4) {
        if (zq2 == null) {
            return new yq2(SyncResponseCode.FAIL_DUE_TO_INVALID_REQUEST, (ArrayList<Integer>) null);
        }
        int b2 = zq2.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        StringBuilder sb = new StringBuilder();
        sb.append("start on thread=");
        Thread currentThread = Thread.currentThread();
        kd4.a((Object) currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        local.d(str, sb.toString());
        UserProfile j2 = PortfolioApp.W.c().j();
        if (j2 == null) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = m;
            local2.e(str2, "Error inside " + m + ".startDeviceSync - user is null");
            a(zq2.a());
            return new yq2(SyncResponseCode.FAIL_DUE_TO_INVALID_REQUEST, (ArrayList<Integer>) null);
        } else if (TextUtils.isEmpty(zq2.a())) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = m;
            local3.e(str3, "Error inside " + m + ".startDeviceSync - serial is null");
            a(zq2.a());
            return new yq2(SyncResponseCode.FAIL_DUE_TO_INVALID_REQUEST, (ArrayList<Integer>) null);
        } else {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = m;
            local4.d(str4, "Inside " + m + ".startDeviceSync - serial=" + zq2.a() + "," + " weightInKg=" + j2.getUserBiometricData().getWeightInKilogram() + ", heightInMeter=" + j2.getUserBiometricData().getHeightInMeter() + ", goal=" + j2.getGoalSteps() + ", isNewDevice=" + zq2.c() + ", SyncMode=" + b2);
            j2.setOriginalSyncMode(b2);
            if (!this.f.k(zq2.a()) || b2 == 13) {
                b2 = 13;
            }
            if (b2 == 13 || !PortfolioApp.W.c().h(zq2.a())) {
                if (b2 == 13) {
                    try {
                        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                        String str5 = m;
                        local5.d(str5, "Inside " + m + ".startDeviceSync - Start full-sync");
                        a(this.j.getAllNotificationsByHour(zq2.a(), MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()), zq2.a(), NotificationAppHelper.b.a(this.g.getSkuModelBySerialPrefix(DeviceHelper.o.b(zq2.a())), zq2.a()));
                        List activeAlarms = this.l.getActiveAlarms();
                        if (activeAlarms == null) {
                            activeAlarms = new ArrayList();
                        }
                        PortfolioApp.W.c().a((List<? extends Alarm>) nj2.a(activeAlarms));
                        HybridPreset activePresetBySerial = this.i.getActivePresetBySerial(zq2.a());
                        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                        String c = c();
                        local6.d(c, "startDeviceSync activePreset=" + activePresetBySerial);
                        if (activePresetBySerial != null) {
                            for (HybridPresetAppSetting hybridPresetAppSetting : activePresetBySerial.getButtons()) {
                                if (kd4.a((Object) hybridPresetAppSetting.getAppId(), (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                                    String settings = hybridPresetAppSetting.getSettings();
                                    if (!TextUtils.isEmpty(settings)) {
                                        try {
                                            SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) new Gson().a(settings, SecondTimezoneSetting.class);
                                            if (secondTimezoneSetting != null && !TextUtils.isEmpty(secondTimezoneSetting.getTimeZoneId())) {
                                                PortfolioApp.W.c().n(secondTimezoneSetting.getTimeZoneId());
                                            }
                                        } catch (Exception e2) {
                                            ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
                                            String str6 = m;
                                            local7.e(str6, "Inside " + m + ".startDeviceSync - parse secondTimezone, ex=" + e2);
                                            e2.printStackTrace();
                                        }
                                    }
                                }
                            }
                            List<MicroAppMapping> a2 = sj2.a(activePresetBySerial, zq2.a(), this.g, this.e);
                            if (!a2.isEmpty()) {
                                PortfolioApp.W.c().b(zq2.a(), (List<? extends BLEMapping>) a2);
                            }
                        }
                    } catch (Exception e3) {
                        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                        FLogger.Component component = FLogger.Component.APP;
                        FLogger.Session session = FLogger.Session.OTHER;
                        String a3 = zq2.a();
                        String str7 = m;
                        remote.i(component, session, a3, str7, "[Sync] Exception when prepare settings " + e3);
                    }
                }
                a(b2, zq2.c(), j2, zq2.a());
                return new Object();
            }
            FLogger.INSTANCE.getLocal().e(m, "Device is syncing, returning...");
            return new ar2();
        }
    }

    @DexIgnore
    public final fi4 a(SparseArray<List<BaseFeatureModel>> sparseArray, String str, boolean z) {
        throw null;
        // return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new HybridSyncUseCase$saveNotificationSettingToDevice$Anon1(this, sparseArray, z, str, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(int i2, boolean z, UserProfile userProfile, String str) {
        if (a() != null) {
            BleCommandResultManager.d.a((BleCommandResultManager.b) this.d, CommunicateMode.SYNC);
        }
        userProfile.setNewDevice(z);
        userProfile.setSyncMode(i2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, ".startDeviceSync - syncMode=" + i2);
        if (!this.f.U()) {
            SKUModel skuModelBySerialPrefix = this.g.getSkuModelBySerialPrefix(DeviceHelper.o.b(str));
            this.f.a((Boolean) true);
            this.k.a(i2, skuModelBySerialPrefix);
            ul2 b2 = AnalyticsHelper.f.b("sync_session");
            AnalyticsHelper.f.a("sync_session", b2);
            b2.d();
        }
        if (!PortfolioApp.W.c().a(str, userProfile)) {
            a(str);
        } else if (i2 == 13) {
            this.f.a(str, System.currentTimeMillis(), false);
        }
    }

    @DexIgnore
    public final void a(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, "broadcastSyncFail serial=" + str);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", 2);
        intent.putExtra("SERIAL", str);
    }
}
