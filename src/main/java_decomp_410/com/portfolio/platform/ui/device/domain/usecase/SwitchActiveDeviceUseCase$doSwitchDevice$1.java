package com.portfolio.platform.ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$1", f = "SwitchActiveDeviceUseCase.kt", l = {}, m = "invokeSuspend")
public final class SwitchActiveDeviceUseCase$doSwitchDevice$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$doSwitchDevice$1(com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = switchActiveDeviceUseCase;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$1 switchActiveDeviceUseCase$doSwitchDevice$1 = new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$1(this.this$0, yb4);
        switchActiveDeviceUseCase$doSwitchDevice$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return switchActiveDeviceUseCase$doSwitchDevice$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a = com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.n.a();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("doSwitchDevice serial ");
            com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.b f = this.this$0.f();
            if (f != null) {
                sb.append(f.b());
                local.d(a, sb.toString());
                com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.W.c();
                com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.b f2 = this.this$0.f();
                if (f2 != null) {
                    if (!c.q(f2.b())) {
                        this.this$0.i();
                        this.this$0.a(new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.c(116, (java.util.ArrayList<java.lang.Integer>) null, ""));
                    }
                    return com.fossil.blesdk.obfuscated.qa4.a;
                }
                com.fossil.blesdk.obfuscated.kd4.a();
                throw null;
            }
            com.fossil.blesdk.obfuscated.kd4.a();
            throw null;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
