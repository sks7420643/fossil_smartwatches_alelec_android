package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.zq2;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase", f = "DianaSyncUseCase.kt", l = {142}, m = "run")
public final class DianaSyncUseCase$run$Anon1 extends ContinuationImpl {
    @DexIgnore
    public int I$Anon0;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ DianaSyncUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaSyncUseCase$run$Anon1(DianaSyncUseCase dianaSyncUseCase, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = dianaSyncUseCase;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((zq2) null, (yb4<Object>) this);
    }
}
