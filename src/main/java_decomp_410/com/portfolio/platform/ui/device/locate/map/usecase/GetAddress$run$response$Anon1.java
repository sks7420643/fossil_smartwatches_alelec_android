package com.portfolio.platform.ui.device.locate.map.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$run$response$Anon1", f = "GetAddress.kt", l = {19}, m = "invokeSuspend")
public final class GetAddress$run$response$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<xz1>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GetAddress.b $requestValues;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ GetAddress this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetAddress$run$response$Anon1(GetAddress getAddress, GetAddress.b bVar, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = getAddress;
        this.$requestValues = bVar;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new GetAddress$run$response$Anon1(this.this$Anon0, this.$requestValues, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((GetAddress$run$response$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            GoogleApiService a2 = this.this$Anon0.d;
            StringBuilder sb = new StringBuilder();
            GetAddress.b bVar = this.$requestValues;
            Double d = null;
            sb.append(bVar != null ? dc4.a(bVar.a()) : null);
            sb.append(',');
            GetAddress.b bVar2 = this.$requestValues;
            if (bVar2 != null) {
                d = dc4.a(bVar2.b());
            }
            sb.append(d);
            String sb2 = sb.toString();
            this.label = 1;
            obj = a2.getAddress(sb2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
