package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yk2;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$linkServer$Anon2", f = "SwitchActiveDeviceUseCase.kt", l = {232, 239}, m = "invokeSuspend")
public final class SwitchActiveDeviceUseCase$linkServer$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super Pair<? extends Boolean, ? extends Integer>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SwitchActiveDeviceUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$linkServer$Anon2(SwitchActiveDeviceUseCase switchActiveDeviceUseCase, MisfitDeviceProfile misfitDeviceProfile, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = switchActiveDeviceUseCase;
        this.$currentDeviceProfile = misfitDeviceProfile;
        this.$serial = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SwitchActiveDeviceUseCase$linkServer$Anon2 switchActiveDeviceUseCase$linkServer$Anon2 = new SwitchActiveDeviceUseCase$linkServer$Anon2(this.this$Anon0, this.$currentDeviceProfile, this.$serial, yb4);
        switchActiveDeviceUseCase$linkServer$Anon2.p$ = (zg4) obj;
        return switchActiveDeviceUseCase$linkServer$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SwitchActiveDeviceUseCase$linkServer$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00bd, code lost:
        if (r22 == null) goto L_0x00c0;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        Object obj3;
        zg4 zg4;
        Device device;
        Object a = cc4.a();
        int i = this.label;
        String str = null;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.this$Anon0;
            MisfitDeviceProfile misfitDeviceProfile = this.$currentDeviceProfile;
            if (misfitDeviceProfile != null) {
                int batteryLevel = misfitDeviceProfile.getBatteryLevel();
                if (batteryLevel < 0 || 100 < batteryLevel) {
                    batteryLevel = batteryLevel < 0 ? 0 : 100;
                }
                int b = yk2.b(misfitDeviceProfile.getVibrationStrength().getVibrationStrengthLevel());
                Device deviceBySerial = this.this$Anon0.i.getDeviceBySerial(this.$serial);
                if (deviceBySerial != null) {
                    deviceBySerial.setBatteryLevel(batteryLevel);
                    deviceBySerial.setFirmwareRevision(misfitDeviceProfile.getFirmwareVersion());
                    deviceBySerial.setMacAddress(misfitDeviceProfile.getAddress());
                    deviceBySerial.setMajor(misfitDeviceProfile.getMicroAppMajorVersion());
                    deviceBySerial.setMinor(misfitDeviceProfile.getMicroAppMinorVersion());
                    if (deviceBySerial != null) {
                        device = deviceBySerial;
                    }
                }
                device = new Device(misfitDeviceProfile.getDeviceSerial(), misfitDeviceProfile.getAddress(), misfitDeviceProfile.getDeviceModel(), misfitDeviceProfile.getFirmwareVersion(), batteryLevel, dc4.a(b), false, 64, (fd4) null);
                qa4 qa4 = qa4.a;
            }
            device = this.this$Anon0.i.getDeviceBySerial(this.$serial);
            switchActiveDeviceUseCase.a(device);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SwitchActiveDeviceUseCase.n.a();
            local.d(a2, "doSwitchDevice device " + this.this$Anon0.e());
            if (this.this$Anon0.e() == null) {
                this.this$Anon0.a(new SwitchActiveDeviceUseCase.c(116, cb4.a((T[]) new Integer[]{dc4.a(-1)}), "No device data"));
                return new Pair(dc4.a(false), dc4.a(-1));
            }
            DeviceRepository b2 = this.this$Anon0.i;
            Device e = this.this$Anon0.e();
            if (e != null) {
                this.L$Anon0 = zg4;
                this.label = 1;
                obj3 = b2.forceLinkDevice(e, this);
                if (obj3 == a) {
                    return a;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            obj3 = obj;
        } else if (i == 2) {
            MFUser mFUser = (MFUser) this.L$Anon3;
            MFUser mFUser2 = (MFUser) this.L$Anon2;
            qo2 qo2 = (qo2) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            obj2 = obj;
            qo2 qo22 = (qo2) obj2;
            return new Pair(dc4.a(true), dc4.a(-1));
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo23 = (qo2) obj3;
        if (qo23 instanceof ro2) {
            FLogger.INSTANCE.getLocal().d(SwitchActiveDeviceUseCase.n.a(), "doSwitchDevice success");
            MFUser currentUser = this.this$Anon0.h.getCurrentUser();
            if (currentUser != null) {
                Device e2 = this.this$Anon0.e();
                if (e2 != null) {
                    currentUser.setActiveDeviceId(e2.getDeviceId());
                    UserRepository d = this.this$Anon0.h;
                    this.L$Anon0 = zg4;
                    this.L$Anon1 = qo23;
                    this.L$Anon2 = currentUser;
                    this.L$Anon3 = currentUser;
                    this.label = 2;
                    obj2 = d.updateUser(currentUser, false, this);
                    if (obj2 == a) {
                        return a;
                    }
                    qo2 qo222 = (qo2) obj2;
                } else {
                    kd4.a();
                    throw null;
                }
            }
            return new Pair(dc4.a(true), dc4.a(-1));
        } else if (!(qo23 instanceof po2)) {
            return new Pair(dc4.a(false), dc4.a(-1));
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = SwitchActiveDeviceUseCase.n.a();
            StringBuilder sb = new StringBuilder();
            sb.append("doSwitchDevice fail ");
            po2 po2 = (po2) qo23;
            sb.append(po2.a());
            local2.d(a3, sb.toString());
            SwitchActiveDeviceUseCase switchActiveDeviceUseCase2 = this.this$Anon0;
            ArrayList a4 = cb4.a((T[]) new Integer[]{dc4.a(po2.a())});
            ServerError c = po2.c();
            if (c != null) {
                str = c.getMessage();
            }
            switchActiveDeviceUseCase2.a(new SwitchActiveDeviceUseCase.c(114, a4, str));
            PortfolioApp.W.c().a(this.$serial, false, po2.a());
            return new Pair(dc4.a(false), dc4.a(po2.a()));
        }
    }
}
