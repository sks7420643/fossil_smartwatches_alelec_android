package com.portfolio.platform.ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1 implements com.portfolio.platform.service.BleCommandResultManager.b {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase a;

    @DexIgnore
    public SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1(com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase) {
        this.a = switchActiveDeviceUseCase;
    }

    @DexIgnore
    public void a(com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode, android.content.Intent intent) {
        com.fossil.blesdk.obfuscated.kd4.b(communicateMode, "communicateMode");
        com.fossil.blesdk.obfuscated.kd4.b(intent, "intent");
        int intExtra = intent.getIntExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
        java.lang.String stringExtra = intent.getStringExtra(com.misfit.frameworks.common.constants.Constants.SERIAL_NUMBER);
        if (communicateMode == com.misfit.frameworks.buttonservice.communite.CommunicateMode.SWITCH_DEVICE) {
            com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.b f = this.a.f();
            if (com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) stringExtra, (java.lang.Object) f != null ? f.b() : null)) {
                boolean z = true;
                if (intExtra == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.ASK_FOR_LINK_SERVER.ordinal()) {
                    if (intent.getExtras() == null) {
                        z = false;
                    }
                    if (!com.fossil.blesdk.obfuscated.ra4.a || z) {
                        android.os.Bundle extras = intent.getExtras();
                        if (extras != null) {
                            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.b(com.fossil.blesdk.obfuscated.ah4.a(com.fossil.blesdk.obfuscated.nh4.b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1(this, stringExtra, (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) extras.getParcelable("device"), (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.a();
                            throw null;
                        }
                    } else {
                        throw new java.lang.AssertionError("Assertion failed");
                    }
                } else if (intExtra == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.a.i();
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.n.a(), "Switch device  success");
                    if (intent.getExtras() == null) {
                        z = false;
                    }
                    if (!com.fossil.blesdk.obfuscated.ra4.a || z) {
                        android.os.Bundle extras2 = intent.getExtras();
                        if (extras2 != null) {
                            com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile misfitDeviceProfile = (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) extras2.getParcelable("device");
                            if (misfitDeviceProfile != null) {
                                if (misfitDeviceProfile.getHeartRateMode() != com.misfit.frameworks.buttonservice.enums.HeartRateMode.NONE) {
                                    this.a.l.a(misfitDeviceProfile.getHeartRateMode());
                                }
                                com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.a;
                                com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) stringExtra, "serial");
                                switchActiveDeviceUseCase.b(stringExtra);
                                com.fossil.blesdk.obfuscated.fi4 unused2 = com.fossil.blesdk.obfuscated.ag4.b(com.fossil.blesdk.obfuscated.ah4.a(com.fossil.blesdk.obfuscated.nh4.b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2(this, misfitDeviceProfile, stringExtra, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                                com.portfolio.platform.PortfolioApp.W.c().j(stringExtra);
                                com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase2 = this.a;
                                switchActiveDeviceUseCase2.a(new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.d(switchActiveDeviceUseCase2.e()));
                                return;
                            }
                            com.fossil.blesdk.obfuscated.kd4.a();
                            throw null;
                        }
                        com.fossil.blesdk.obfuscated.kd4.a();
                        throw null;
                    }
                    throw new java.lang.AssertionError("Assertion failed");
                } else if (intExtra == com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED.ordinal()) {
                    this.a.i();
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    java.util.ArrayList<java.lang.Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new java.util.ArrayList<>();
                    }
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String a2 = com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.n.a();
                    local.d(a2, "stop current workout fail due to " + intExtra2);
                    if (intExtra2 != 1101) {
                        if (intExtra2 == 1928) {
                            com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.c g = this.a.g();
                            if (g == null || this.a.a(g) == null) {
                                this.a.a(new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.c(116, (java.util.ArrayList<java.lang.Integer>) null, ""));
                                return;
                            }
                            return;
                        } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                            this.a.a(new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.c(117, (java.util.ArrayList<java.lang.Integer>) null, ""));
                            return;
                        }
                    }
                    this.a.a(new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.c(113, integerArrayListExtra, ""));
                }
            }
        }
    }
}
