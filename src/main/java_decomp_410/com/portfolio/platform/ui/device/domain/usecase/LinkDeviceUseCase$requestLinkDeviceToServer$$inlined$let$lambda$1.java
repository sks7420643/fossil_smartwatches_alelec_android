package com.portfolio.platform.ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LinkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.Device $deviceModel;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$1(com.portfolio.platform.data.model.Device device, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase) {
        super(2, yb4);
        this.$deviceModel = device;
        this.this$0 = linkDeviceUseCase;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$1 linkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$1 = new com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$1(this.$deviceModel, yb4, this.this$0);
        linkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return linkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$requestLinkDeviceToServer$$inlined$let$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.p$;
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
            com.misfit.frameworks.buttonservice.log.FLogger.Component component = com.misfit.frameworks.buttonservice.log.FLogger.Component.API;
            com.misfit.frameworks.buttonservice.log.FLogger.Session session = com.misfit.frameworks.buttonservice.log.FLogger.Session.PAIR;
            java.lang.String deviceId = this.$deviceModel.getDeviceId();
            java.lang.String a2 = com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.q.a();
            remote.i(component, session, deviceId, a2, "Steal a device with serial " + this.$deviceModel.getDeviceId());
            com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.W.c();
            com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode = com.misfit.frameworks.buttonservice.communite.CommunicateMode.LINK;
            java.lang.String deviceId2 = this.$deviceModel.getDeviceId();
            c.a(communicateMode, deviceId2, "Force link a device with serial " + this.$deviceModel.getDeviceId());
            com.portfolio.platform.data.source.DeviceRepository a3 = this.this$0.l;
            com.portfolio.platform.data.model.Device device = this.$deviceModel;
            this.L$0 = zg4;
            this.label = 1;
            obj = a3.forceLinkDevice(device, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
        if (qo2 instanceof com.fossil.blesdk.obfuscated.ro2) {
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
            com.misfit.frameworks.buttonservice.log.FLogger.Component component2 = com.misfit.frameworks.buttonservice.log.FLogger.Component.API;
            com.misfit.frameworks.buttonservice.log.FLogger.Session session2 = com.misfit.frameworks.buttonservice.log.FLogger.Session.PAIR;
            java.lang.String deviceId3 = this.$deviceModel.getDeviceId();
            java.lang.String a4 = com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.q.a();
            remote2.i(component2, session2, deviceId3, a4, "Steal a device with serial " + this.$deviceModel.getDeviceId() + " success");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a5 = com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.q.a();
            local.d(a5, "forceLinkDevice onSuccess device=" + this.$deviceModel.getDeviceId());
            com.portfolio.platform.PortfolioApp.W.c().a(this.$deviceModel.getDeviceId(), (com.misfit.frameworks.buttonservice.model.pairing.PairingResponse) com.misfit.frameworks.buttonservice.model.pairing.PairingResponse.CREATOR.buildPairingLinkServerResponse(true, 0));
        } else if (qo2 instanceof com.fossil.blesdk.obfuscated.po2) {
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
            com.misfit.frameworks.buttonservice.log.FLogger.Component component3 = com.misfit.frameworks.buttonservice.log.FLogger.Component.API;
            com.misfit.frameworks.buttonservice.log.FLogger.Session session3 = com.misfit.frameworks.buttonservice.log.FLogger.Session.PAIR;
            java.lang.String deviceId4 = this.$deviceModel.getDeviceId();
            java.lang.String a6 = com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.q.a();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("Steal a device with serial ");
            sb.append(this.$deviceModel.getDeviceId());
            sb.append(", server error=");
            com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
            sb.append(po2.a());
            sb.append(", error = ");
            sb.append(com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.INSTANCE.build(com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.Step.LINK_DEVICE, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.Component.APP, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.AppError.NETWORK_ERROR));
            remote3.e(component3, session3, deviceId4, a6, sb.toString());
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a7 = com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.q.a();
            local2.d(a7, "forceLinkDevice onFail errorCode=" + po2.a());
            this.this$0.a(new com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.j(po2.a(), this.$deviceModel.getDeviceId(), ""));
            com.portfolio.platform.PortfolioApp.W.c().a(this.$deviceModel.getDeviceId(), (com.misfit.frameworks.buttonservice.model.pairing.PairingResponse) com.misfit.frameworks.buttonservice.model.pairing.PairingResponse.CREATOR.buildPairingLinkServerResponse(false, po2.a()));
        }
        return com.fossil.blesdk.obfuscated.qa4.a;
    }
}
