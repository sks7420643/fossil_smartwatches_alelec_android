package com.portfolio.platform;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.PortfolioApp$updateActiveDeviceInfoLog$1", mo27670f = "PortfolioApp.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class PortfolioApp$updateActiveDeviceInfoLog$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20988p$;

    @DexIgnore
    public PortfolioApp$updateActiveDeviceInfoLog$1(com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.PortfolioApp$updateActiveDeviceInfoLog$1 portfolioApp$updateActiveDeviceInfoLog$1 = new com.portfolio.platform.PortfolioApp$updateActiveDeviceInfoLog$1(yb4);
        portfolioApp$updateActiveDeviceInfoLog$1.f20988p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return portfolioApp$updateActiveDeviceInfoLog$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.PortfolioApp$updateActiveDeviceInfoLog$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo a = com.portfolio.platform.helper.AppHelper.f21170f.mo39525a().mo39522a();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String d = com.portfolio.platform.PortfolioApp.f20941W.mo34591d();
            local.mo33255d(d, ".updateActiveDeviceInfoLog(), " + new com.google.gson.Gson().mo23096a((java.lang.Object) a));
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().updateActiveDeviceInfo(a);
            try {
                com.misfit.frameworks.buttonservice.IButtonConnectivity b = com.portfolio.platform.PortfolioApp.f20941W.mo34585b();
                if (b != null) {
                    b.updateActiveDeviceInfoLog(a);
                }
            } catch (java.lang.Exception e) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String d2 = com.portfolio.platform.PortfolioApp.f20941W.mo34591d();
                local2.mo33256e(d2, ".updateActiveDeviceInfoToButtonService(), error=" + e);
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
