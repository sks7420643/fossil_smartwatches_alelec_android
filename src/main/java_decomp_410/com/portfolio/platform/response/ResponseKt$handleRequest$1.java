package com.portfolio.platform.response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.response.ResponseKt", mo27670f = "Response.kt", mo27671l = {15}, mo27672m = "handleRequest")
public final class ResponseKt$handleRequest$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;

    @DexIgnore
    public ResponseKt$handleRequest$1(com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return com.portfolio.platform.response.ResponseKt.m32232a((com.fossil.blesdk.obfuscated.xc4) null, this);
    }
}
