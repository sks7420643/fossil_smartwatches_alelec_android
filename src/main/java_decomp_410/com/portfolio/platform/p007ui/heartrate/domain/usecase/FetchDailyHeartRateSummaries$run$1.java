package com.portfolio.platform.p007ui.heartrate.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries", mo27670f = "FetchDailyHeartRateSummaries.kt", mo27671l = {57}, mo27672m = "run")
/* renamed from: com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries$run$1 */
public final class FetchDailyHeartRateSummaries$run$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public java.lang.Object L$7;
    @DexIgnore
    public java.lang.Object L$8;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FetchDailyHeartRateSummaries$run$1(com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = fetchDailyHeartRateSummaries;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.mo26310a((com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries.C6183b) null, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this);
    }
}
