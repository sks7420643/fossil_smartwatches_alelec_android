package com.portfolio.platform.p007ui.heartrate.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples */
public final class FetchHeartRateSamples extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchHeartRateSamples.C6185b, com.portfolio.platform.CoroutineUseCase.C5605d, com.portfolio.platform.CoroutineUseCase.C5602a> {

    @DexIgnore
    /* renamed from: g */
    public static /* final */ java.lang.String f21931g;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.HeartRateSampleRepository f21932d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.portfolio.platform.data.source.FitnessDataRepository f21933e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.portfolio.platform.data.source.UserRepository f21934f;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples$a")
    /* renamed from: com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples$a */
    public static final class C6184a {
        @DexIgnore
        public C6184a() {
        }

        @DexIgnore
        public /* synthetic */ C6184a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples$b")
    /* renamed from: com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples$b */
    public static final class C6185b implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.Date f21935a;

        @DexIgnore
        public C6185b(java.util.Date date) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(date, "date");
            this.f21935a = date;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.util.Date mo40386a() {
            return this.f21935a;
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchHeartRateSamples.C6184a((com.fossil.blesdk.obfuscated.fd4) null);
        java.lang.String simpleName = com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchHeartRateSamples.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "FetchHeartRateSamples::class.java.simpleName");
        f21931g = simpleName;
    }
    */

    @DexIgnore
    public FetchHeartRateSamples(com.portfolio.platform.data.source.HeartRateSampleRepository heartRateSampleRepository, com.portfolio.platform.data.source.FitnessDataRepository fitnessDataRepository, com.portfolio.platform.data.source.UserRepository userRepository) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(heartRateSampleRepository, "mHeartRateSampleRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(fitnessDataRepository, "mFitnessDataRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(userRepository, "mUserRepository");
        this.f21932d = heartRateSampleRepository;
        this.f21933e = fitnessDataRepository;
        this.f21934f = userRepository;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f21931g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchHeartRateSamples.C6185b bVar, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4> yb4) {
        com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchHeartRateSamples$run$1 fetchHeartRateSamples$run$1;
        int i;
        if (yb4 instanceof com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchHeartRateSamples$run$1) {
            fetchHeartRateSamples$run$1 = (com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchHeartRateSamples$run$1) yb4;
            int i2 = fetchHeartRateSamples$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fetchHeartRateSamples$run$1.label = i2 - Integer.MIN_VALUE;
                com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchHeartRateSamples$run$1 fetchHeartRateSamples$run$12 = fetchHeartRateSamples$run$1;
                java.lang.Object obj = fetchHeartRateSamples$run$12.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = fetchHeartRateSamples$run$12.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    if (bVar == null) {
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    java.util.Date a2 = bVar.mo40386a();
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String str = f21931g;
                    local.mo33255d(str, "executeUseCase - date=" + com.fossil.blesdk.obfuscated.ft3.m22452a(a2));
                    com.portfolio.platform.data.model.MFUser currentUser = this.f21934f.getCurrentUser();
                    if (currentUser == null || android.text.TextUtils.isEmpty(currentUser.getCreatedAt())) {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String str2 = f21931g;
                        local2.mo33255d(str2, "executeUseCase - FAILED!!! with user=" + currentUser);
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    java.util.Date d = com.fossil.blesdk.obfuscated.rk2.m27394d(currentUser.getCreatedAt());
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String str3 = f21931g;
                    java.lang.StringBuilder sb = new java.lang.StringBuilder();
                    sb.append("executeUseCase - createdDate=");
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) d, "createdDate");
                    sb.append(com.fossil.blesdk.obfuscated.ft3.m22452a(d));
                    local3.mo33255d(str3, sb.toString());
                    if (com.fossil.blesdk.obfuscated.rk2.m27383b(d, a2) || com.fossil.blesdk.obfuscated.rk2.m27383b(a2, new java.util.Date())) {
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    java.util.Calendar p = com.fossil.blesdk.obfuscated.rk2.m27411p(a2);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) p, "DateHelper.getStartOfWeek(date)");
                    java.util.Date time = p.getTime();
                    if (com.fossil.blesdk.obfuscated.rk2.m27391c(d, time)) {
                        time = d;
                    }
                    com.portfolio.platform.data.source.FitnessDataRepository fitnessDataRepository = this.f21933e;
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) time, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_START_DATE);
                    java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> fitnessData = fitnessDataRepository.getFitnessData(time, a2);
                    if (fitnessData.isEmpty()) {
                        com.portfolio.platform.data.source.HeartRateSampleRepository heartRateSampleRepository = this.f21932d;
                        fetchHeartRateSamples$run$12.L$0 = this;
                        fetchHeartRateSamples$run$12.L$1 = bVar;
                        fetchHeartRateSamples$run$12.L$2 = a2;
                        fetchHeartRateSamples$run$12.L$3 = currentUser;
                        fetchHeartRateSamples$run$12.L$4 = d;
                        fetchHeartRateSamples$run$12.L$5 = time;
                        fetchHeartRateSamples$run$12.L$6 = fitnessData;
                        fetchHeartRateSamples$run$12.label = 1;
                        if (com.portfolio.platform.data.source.HeartRateSampleRepository.fetchHeartRateSamples$default(heartRateSampleRepository, time, a2, 0, 0, fetchHeartRateSamples$run$12, 12, (java.lang.Object) null) == a) {
                            return a;
                        }
                    }
                } else if (i == 1) {
                    java.util.List list = (java.util.List) fetchHeartRateSamples$run$12.L$6;
                    java.util.Date date = (java.util.Date) fetchHeartRateSamples$run$12.L$5;
                    java.util.Date date2 = (java.util.Date) fetchHeartRateSamples$run$12.L$4;
                    com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) fetchHeartRateSamples$run$12.L$3;
                    java.util.Date date3 = (java.util.Date) fetchHeartRateSamples$run$12.L$2;
                    com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchHeartRateSamples.C6185b bVar2 = (com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchHeartRateSamples.C6185b) fetchHeartRateSamples$run$12.L$1;
                    com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchHeartRateSamples fetchHeartRateSamples = (com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchHeartRateSamples) fetchHeartRateSamples$run$12.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }
        fetchHeartRateSamples$run$1 = new com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchHeartRateSamples$run$1(this, yb4);
        com.portfolio.platform.p007ui.heartrate.domain.usecase.FetchHeartRateSamples$run$1 fetchHeartRateSamples$run$122 = fetchHeartRateSamples$run$1;
        java.lang.Object obj2 = fetchHeartRateSamples$run$122.result;
        java.lang.Object a3 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = fetchHeartRateSamples$run$122.label;
        if (i != 0) {
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
