package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$run$repoResponse$1", mo27670f = "DownloadFirmwareByDeviceModelUsecase.kt", mo27671l = {37}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$run$repoResponse$1 */
public final class DownloadFirmwareByDeviceModelUsecase$run$repoResponse$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.Firmware>>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $deviceModel;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DownloadFirmwareByDeviceModelUsecase$run$repoResponse$1(com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(1, yb4);
        this.this$0 = downloadFirmwareByDeviceModelUsecase;
        this.$deviceModel = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        return new com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$run$repoResponse$1(this.this$0, this.$deviceModel, yb4);
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj) {
        return ((com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$run$repoResponse$1) create((com.fossil.blesdk.obfuscated.yb4) obj)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.source.remote.GuestApiService b = this.this$0.f21816e;
            java.lang.String h = this.this$0.f21815d.mo34539h();
            java.lang.String str = this.$deviceModel;
            this.label = 1;
            obj = b.getFirmwares(h, str, "android", this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
