package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase */
public final class DownloadFirmwareByDeviceModelUsecase extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6137b, com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6139d, com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6138c> {

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.PortfolioApp f21815d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.portfolio.platform.data.source.remote.GuestApiService f21816e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$a")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$a */
    public static final class C6136a {
        @DexIgnore
        public C6136a() {
        }

        @DexIgnore
        public /* synthetic */ C6136a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$b")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$b */
    public static final class C6137b implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f21817a;

        @DexIgnore
        public C6137b(java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "deviceModel");
            this.f21817a = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40289a() {
            return this.f21817a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$c")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$c */
    public static final class C6138c implements com.portfolio.platform.CoroutineUseCase.C5602a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$d")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$d */
    public static final class C6139d implements com.portfolio.platform.CoroutineUseCase.C5605d {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f21818a;

        @DexIgnore
        public C6139d(java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "latestFwVersion");
            this.f21818a = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40290a() {
            return this.f21818a;
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6136a((com.fossil.blesdk.obfuscated.fd4) null);
    }
    */

    @DexIgnore
    public DownloadFirmwareByDeviceModelUsecase(com.portfolio.platform.PortfolioApp portfolioApp, com.portfolio.platform.data.source.remote.GuestApiService guestApiService) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(portfolioApp, "mApp");
        com.fossil.blesdk.obfuscated.kd4.m24411b(guestApiService, "mGuestApiService");
        this.f21815d = portfolioApp;
        this.f21816e = guestApiService;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return "DownloadFirmwareByDeviceModelUsecase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002f  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6137b bVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$run$1 downloadFirmwareByDeviceModelUsecase$run$1;
        int i;
        com.portfolio.platform.data.model.Firmware firmware;
        com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase;
        com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6137b bVar2;
        java.lang.String str;
        com.fossil.blesdk.obfuscated.qo2 qo2;
        com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6137b bVar3 = bVar;
        com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb42 = yb4;
        if (yb42 instanceof com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$run$1) {
            downloadFirmwareByDeviceModelUsecase$run$1 = (com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$run$1) yb42;
            int i2 = downloadFirmwareByDeviceModelUsecase$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                downloadFirmwareByDeviceModelUsecase$run$1.label = i2 - Integer.MIN_VALUE;
                java.lang.Object obj = downloadFirmwareByDeviceModelUsecase$run$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = downloadFirmwareByDeviceModelUsecase$run$1.label;
                java.lang.String str2 = null;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("DownloadFirmwareByDeviceModelUsecase", "run");
                    if (bVar3 == null) {
                        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e("DownloadFirmwareByDeviceModelUsecase", "run - requestValues is NULL!!!");
                        return new com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6138c();
                    }
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("DownloadFirmwareByDeviceModelUsecase", "run - getFirmwares of deviceModel=" + bVar.mo40289a());
                    java.lang.String a2 = bVar.mo40289a();
                    com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$run$repoResponse$1 downloadFirmwareByDeviceModelUsecase$run$repoResponse$1 = new com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$run$repoResponse$1(this, a2, (com.fossil.blesdk.obfuscated.yb4) null);
                    downloadFirmwareByDeviceModelUsecase$run$1.L$0 = this;
                    downloadFirmwareByDeviceModelUsecase$run$1.L$1 = bVar3;
                    downloadFirmwareByDeviceModelUsecase$run$1.L$2 = a2;
                    downloadFirmwareByDeviceModelUsecase$run$1.label = 1;
                    java.lang.Object a3 = com.portfolio.platform.response.ResponseKt.m32232a(downloadFirmwareByDeviceModelUsecase$run$repoResponse$1, downloadFirmwareByDeviceModelUsecase$run$1);
                    if (a3 == a) {
                        return a;
                    }
                    downloadFirmwareByDeviceModelUsecase = this;
                    java.lang.Object obj2 = a3;
                    bVar2 = bVar3;
                    str = a2;
                    obj = obj2;
                } else if (i == 1) {
                    str = (java.lang.String) downloadFirmwareByDeviceModelUsecase$run$1.L$2;
                    bVar2 = (com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6137b) downloadFirmwareByDeviceModelUsecase$run$1.L$1;
                    downloadFirmwareByDeviceModelUsecase = (com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase) downloadFirmwareByDeviceModelUsecase$run$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else if (i == 2) {
                    java.util.List list = (java.util.List) downloadFirmwareByDeviceModelUsecase$run$1.L$4;
                    com.fossil.blesdk.obfuscated.qo2 qo22 = (com.fossil.blesdk.obfuscated.qo2) downloadFirmwareByDeviceModelUsecase$run$1.L$3;
                    java.lang.String str3 = (java.lang.String) downloadFirmwareByDeviceModelUsecase$run$1.L$2;
                    com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6137b bVar4 = (com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6137b) downloadFirmwareByDeviceModelUsecase$run$1.L$1;
                    com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase2 = (com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase) downloadFirmwareByDeviceModelUsecase$run$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    firmware = (com.portfolio.platform.data.model.Firmware) downloadFirmwareByDeviceModelUsecase$run$1.L$5;
                    if (((java.lang.Boolean) obj).booleanValue()) {
                        return new com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6138c();
                    }
                    java.lang.String versionNumber = firmware.getVersionNumber();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) versionNumber, "latestFirmware.versionNumber");
                    return new com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6139d(versionNumber);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
                if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
                    com.portfolio.platform.data.source.remote.ApiResponse apiResponse = (com.portfolio.platform.data.source.remote.ApiResponse) ((com.fossil.blesdk.obfuscated.ro2) qo2).mo30673a();
                    java.util.List<com.portfolio.platform.data.model.Firmware> list2 = apiResponse != null ? apiResponse.get_items() : null;
                    if (list2 != null) {
                        firmware = null;
                        for (com.portfolio.platform.data.model.Firmware firmware2 : list2) {
                            if (!android.text.TextUtils.isEmpty(firmware2.getDeviceModel())) {
                                java.lang.String deviceModel = firmware2.getDeviceModel();
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) deviceModel, "firmware.deviceModel");
                                if (kotlin.text.StringsKt__StringsKt.m37073a((java.lang.CharSequence) deviceModel, (java.lang.CharSequence) str, false, 2, (java.lang.Object) null)) {
                                    firmware = firmware2;
                                }
                            }
                        }
                    } else {
                        firmware = null;
                    }
                    if (firmware != null) {
                        com.portfolio.platform.util.DeviceUtils a4 = com.portfolio.platform.util.DeviceUtils.f24406g.mo41910a();
                        downloadFirmwareByDeviceModelUsecase$run$1.L$0 = downloadFirmwareByDeviceModelUsecase;
                        downloadFirmwareByDeviceModelUsecase$run$1.L$1 = bVar2;
                        downloadFirmwareByDeviceModelUsecase$run$1.L$2 = str;
                        downloadFirmwareByDeviceModelUsecase$run$1.L$3 = qo2;
                        downloadFirmwareByDeviceModelUsecase$run$1.L$4 = list2;
                        downloadFirmwareByDeviceModelUsecase$run$1.L$5 = firmware;
                        downloadFirmwareByDeviceModelUsecase$run$1.label = 2;
                        obj = a4.mo41905a(firmware, (com.fossil.blesdk.obfuscated.yb4<? super java.lang.Boolean>) downloadFirmwareByDeviceModelUsecase$run$1);
                        if (obj == a) {
                            return a;
                        }
                        if (((java.lang.Boolean) obj).booleanValue()) {
                        }
                    } else {
                        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e("DownloadFirmwareByDeviceModelUsecase", "run - getFirmwares of deviceModel=" + str + " not found on server");
                        return new com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6138c();
                    }
                } else if (qo2 instanceof com.fossil.blesdk.obfuscated.po2) {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.StringBuilder sb = new java.lang.StringBuilder();
                    sb.append("run - getFirmwares of deviceModel=");
                    sb.append(str);
                    sb.append(" not found on server code=");
                    com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
                    com.portfolio.platform.data.model.ServerError c = po2.mo30178c();
                    sb.append(c != null ? c.getCode() : null);
                    sb.append(", message=");
                    com.portfolio.platform.data.model.ServerError c2 = po2.mo30178c();
                    if (c2 != null) {
                        str2 = c2.getMessage();
                    }
                    sb.append(str2);
                    local.mo33256e("DownloadFirmwareByDeviceModelUsecase", sb.toString());
                    return new com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6138c();
                } else {
                    throw new kotlin.NoWhenBranchMatchedException();
                }
            }
        }
        downloadFirmwareByDeviceModelUsecase$run$1 = new com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$run$1(this, yb42);
        java.lang.Object obj3 = downloadFirmwareByDeviceModelUsecase$run$1.result;
        java.lang.Object a5 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = downloadFirmwareByDeviceModelUsecase$run$1.label;
        java.lang.String str22 = null;
        if (i != 0) {
        }
        qo2 = (com.fossil.blesdk.obfuscated.qo2) obj3;
        if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
        }
    }
}
