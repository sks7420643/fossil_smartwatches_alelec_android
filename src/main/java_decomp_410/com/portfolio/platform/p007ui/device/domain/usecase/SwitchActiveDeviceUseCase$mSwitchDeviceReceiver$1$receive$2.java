package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2", mo27670f = "SwitchActiveDeviceUseCase.kt", mo27671l = {95}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2 */
public final class SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile $currentDeviceProfile;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serial;
    @DexIgnore
    public int I$0;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21896p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2(com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1 switchActiveDeviceUseCase$mSwitchDeviceReceiver$1, com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile misfitDeviceProfile, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = switchActiveDeviceUseCase$mSwitchDeviceReceiver$1;
        this.$currentDeviceProfile = misfitDeviceProfile;
        this.$serial = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2 switchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2 = new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2(this.this$0, this.$currentDeviceProfile, this.$serial, yb4);
        switchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2.f21896p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return switchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21896p$;
            int b = com.fossil.blesdk.obfuscated.yk2.m31035b(this.$currentDeviceProfile.getVibrationStrength().getVibrationStrengthLevel());
            com.portfolio.platform.data.source.DeviceRepository b2 = this.this$0.f21894a.f21881i;
            java.lang.String str = this.$serial;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str, "serial");
            com.portfolio.platform.data.model.Device deviceBySerial = b2.getDeviceBySerial(str);
            if (deviceBySerial != null) {
                java.lang.Integer vibrationStrength = deviceBySerial.getVibrationStrength();
                if (vibrationStrength == null || vibrationStrength.intValue() != b) {
                    deviceBySerial.setVibrationStrength(com.fossil.blesdk.obfuscated.dc4.m20843a(b));
                    com.portfolio.platform.data.source.DeviceRepository b3 = this.this$0.f21894a.f21881i;
                    this.L$0 = zg4;
                    this.I$0 = b;
                    this.L$1 = deviceBySerial;
                    this.label = 1;
                    if (b3.updateDevice(deviceBySerial, false, this) == a) {
                        return a;
                    }
                }
            }
        } else if (i == 1) {
            com.portfolio.platform.data.model.Device device = (com.portfolio.platform.data.model.Device) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
