package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase */
public final class DianaSyncUseCase extends com.portfolio.platform.CoroutineUseCase<com.fossil.blesdk.obfuscated.zq2, com.fossil.blesdk.obfuscated.ar2, com.fossil.blesdk.obfuscated.yq2> {

    @DexIgnore
    /* renamed from: q */
    public static /* final */ java.lang.String f21793q;

    @DexIgnore
    /* renamed from: r */
    public static /* final */ com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase.C6133a f21794r; // = new com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase.C6133a((com.fossil.blesdk.obfuscated.fd4) null);

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase.C6134b f21795d; // = new com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase.C6134b(this);

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.portfolio.platform.data.source.DianaPresetRepository f21796e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.en2 f21797f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.vy2 f21798g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.portfolio.platform.PortfolioApp f21799h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.portfolio.platform.data.source.DeviceRepository f21800i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase f21801j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ com.fossil.blesdk.obfuscated.px2 f21802k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ com.portfolio.platform.usecase.VerifySecretKeyUseCase f21803l;

    @DexIgnore
    /* renamed from: m */
    public /* final */ com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase f21804m;

    @DexIgnore
    /* renamed from: n */
    public /* final */ com.portfolio.platform.helper.AnalyticsHelper f21805n;

    @DexIgnore
    /* renamed from: o */
    public /* final */ com.portfolio.platform.data.source.WatchFaceRepository f21806o;

    @DexIgnore
    /* renamed from: p */
    public /* final */ com.portfolio.platform.data.source.AlarmsRepository f21807p;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$a")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$a */
    public static final class C6133a {
        @DexIgnore
        public C6133a() {
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40283a() {
            return com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase.f21793q;
        }

        @DexIgnore
        public /* synthetic */ C6133a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$b")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$b */
    public static final class C6134b implements com.portfolio.platform.service.BleCommandResultManager.C5999b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase f21808a;

        @DexIgnore
        public C6134b(com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase dianaSyncUseCase) {
            this.f21808a = dianaSyncUseCase;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo27136a(com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode, android.content.Intent intent) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(communicateMode, "communicateMode");
            com.fossil.blesdk.obfuscated.kd4.m24411b(intent, "intent");
            java.lang.String stringExtra = intent.getStringExtra("SERIAL");
            if (!android.text.TextUtils.isEmpty(stringExtra) && com.fossil.blesdk.obfuscated.qf4.m26954b(stringExtra, this.f21808a.f21799h.mo34532e(), true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                if (intExtra == 1) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase.f21794r.mo40283a(), "sync success, remove device now");
                    com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39775b((com.portfolio.platform.service.BleCommandResultManager.C5999b) this, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC);
                    this.f21808a.mo34436a(new com.fossil.blesdk.obfuscated.ar2());
                } else if (intExtra == 2 || intExtra == 4) {
                    com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39775b((com.portfolio.platform.service.BleCommandResultManager.C5999b) this, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC);
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    java.util.ArrayList<java.lang.Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new java.util.ArrayList<>();
                    }
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String a = com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase.f21794r.mo40283a();
                    local.mo33255d(a, "sync fail due to " + intExtra2);
                    if (intExtra2 != 1101) {
                        if (intExtra2 == 1603) {
                            this.f21808a.mo34434a(new com.fossil.blesdk.obfuscated.yq2(com.portfolio.platform.enums.SyncResponseCode.FAIL_DUE_TO_USER_DENY_STOP_WORKOUT, (java.util.ArrayList<java.lang.Integer>) null));
                            return;
                        } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                            this.f21808a.mo34434a(new com.fossil.blesdk.obfuscated.yq2(com.portfolio.platform.enums.SyncResponseCode.FAIL_DUE_TO_SYNC_FAIL, (java.util.ArrayList<java.lang.Integer>) null));
                            return;
                        }
                    }
                    this.f21808a.mo34434a(new com.fossil.blesdk.obfuscated.yq2(com.portfolio.platform.enums.SyncResponseCode.FAIL_DUE_TO_LACK_PERMISSION, integerArrayListExtra));
                } else if (intExtra == 5) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase.f21794r.mo40283a(), "sync pending due to workout");
                    this.f21808a.mo34434a(new com.fossil.blesdk.obfuscated.yq2(com.portfolio.platform.enums.SyncResponseCode.FAIL_DUE_TO_PENDING_WORKOUT, (java.util.ArrayList<java.lang.Integer>) null));
                }
            }
        }
    }

    /*
    static {
        java.lang.String simpleName = com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "DianaSyncUseCase::class.java.simpleName");
        f21793q = simpleName;
    }
    */

    @DexIgnore
    public DianaSyncUseCase(com.portfolio.platform.data.source.DianaPresetRepository dianaPresetRepository, com.fossil.blesdk.obfuscated.en2 en2, com.fossil.blesdk.obfuscated.vy2 vy2, com.portfolio.platform.PortfolioApp portfolioApp, com.portfolio.platform.data.source.DeviceRepository deviceRepository, com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase notificationSettingsDatabase, com.fossil.blesdk.obfuscated.px2 px2, com.portfolio.platform.usecase.VerifySecretKeyUseCase verifySecretKeyUseCase, com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase updateFirmwareUsecase, com.portfolio.platform.helper.AnalyticsHelper analyticsHelper, com.portfolio.platform.data.source.WatchFaceRepository watchFaceRepository, com.portfolio.platform.data.source.AlarmsRepository alarmsRepository) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(dianaPresetRepository, "mDianaPresetRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(en2, "mSharedPrefs");
        com.fossil.blesdk.obfuscated.kd4.m24411b(vy2, "mGetApps");
        com.fossil.blesdk.obfuscated.kd4.m24411b(portfolioApp, "mApp");
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceRepository, "mDeviceRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        com.fossil.blesdk.obfuscated.kd4.m24411b(px2, "mGetAllContactGroup");
        com.fossil.blesdk.obfuscated.kd4.m24411b(verifySecretKeyUseCase, "mVerifySecretKeyUseCase");
        com.fossil.blesdk.obfuscated.kd4.m24411b(updateFirmwareUsecase, "mUpdateFirmwareUseCase");
        com.fossil.blesdk.obfuscated.kd4.m24411b(analyticsHelper, "mAnalyticsHelper");
        com.fossil.blesdk.obfuscated.kd4.m24411b(watchFaceRepository, "watchFaceRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(alarmsRepository, "mAlarmsRepository");
        this.f21796e = dianaPresetRepository;
        this.f21797f = en2;
        this.f21798g = vy2;
        this.f21799h = portfolioApp;
        this.f21800i = deviceRepository;
        this.f21801j = notificationSettingsDatabase;
        this.f21802k = px2;
        this.f21803l = verifySecretKeyUseCase;
        this.f21804m = updateFirmwareUsecase;
        this.f21805n = analyticsHelper;
        this.f21806o = watchFaceRepository;
        this.f21807p = alarmsRepository;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo40282b(int i, boolean z, com.misfit.frameworks.buttonservice.model.UserProfile userProfile, java.lang.String str) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str2 = f21793q;
        local.mo33255d(str2, "verifySecretKey " + str);
        mo40280a(userProfile.getOriginalSyncMode(), str, 0);
        com.portfolio.platform.usecase.VerifySecretKeyUseCase verifySecretKeyUseCase = this.f21803l;
        com.portfolio.platform.usecase.VerifySecretKeyUseCase.C6921b bVar = new com.portfolio.platform.usecase.VerifySecretKeyUseCase.C6921b(str);
        com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1 dianaSyncUseCase$verifySecretKey$1 = new com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1(this, str, i, z, userProfile);
        verifySecretKeyUseCase.mo34435a(bVar, dianaSyncUseCase$verifySecretKey$1);
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f21793q;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0222 A[Catch:{ Exception -> 0x02b6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x025e A[Catch:{ Exception -> 0x02b6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.fossil.blesdk.obfuscated.zq2 zq2, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$run$1 dianaSyncUseCase$run$1;
        int i;
        com.misfit.frameworks.buttonservice.model.UserProfile userProfile;
        int i2;
        com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase dianaSyncUseCase;
        java.util.List activeAlarms;
        com.portfolio.platform.data.model.diana.preset.DianaPreset activePresetBySerial;
        if (yb4 instanceof com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$run$1) {
            dianaSyncUseCase$run$1 = (com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$run$1) yb4;
            int i3 = dianaSyncUseCase$run$1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                dianaSyncUseCase$run$1.label = i3 - Integer.MIN_VALUE;
                java.lang.Object obj = dianaSyncUseCase$run$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = dianaSyncUseCase$run$1.label;
                com.misfit.frameworks.buttonservice.model.background.BackgroundConfig backgroundConfig = null;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    if (zq2 == null) {
                        return new com.fossil.blesdk.obfuscated.yq2(com.portfolio.platform.enums.SyncResponseCode.FAIL_DUE_TO_INVALID_REQUEST, (java.util.ArrayList<java.lang.Integer>) null);
                    }
                    int b = zq2.mo33110b();
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String c = mo26311c();
                    java.lang.StringBuilder sb = new java.lang.StringBuilder();
                    sb.append("start on thread=");
                    java.lang.Thread currentThread = java.lang.Thread.currentThread();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) currentThread, "Thread.currentThread()");
                    sb.append(currentThread.getName());
                    local.mo33255d(c, sb.toString());
                    userProfile = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34543j();
                    if (userProfile == null) {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String c2 = mo26311c();
                        local2.mo33256e(c2, "Error inside " + mo26311c() + ".startDeviceSync - user is null");
                        mo40280a(b, zq2.mo33109a(), 2);
                        return new com.fossil.blesdk.obfuscated.yq2(com.portfolio.platform.enums.SyncResponseCode.FAIL_DUE_TO_INVALID_REQUEST, (java.util.ArrayList<java.lang.Integer>) null);
                    } else if (android.text.TextUtils.isEmpty(zq2.mo33109a())) {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String c3 = mo26311c();
                        local3.mo33256e(c3, "Error inside " + mo26311c() + ".startDeviceSync - serial is null");
                        mo40280a(b, zq2.mo33109a(), 2);
                        return new com.fossil.blesdk.obfuscated.yq2(com.portfolio.platform.enums.SyncResponseCode.FAIL_DUE_TO_INVALID_REQUEST, (java.util.ArrayList<java.lang.Integer>) null);
                    } else {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String c4 = mo26311c();
                        local4.mo33255d(c4, "Inside " + mo26311c() + ".startDeviceSync - serial=" + zq2.mo33109a() + "," + " weightInKg=" + userProfile.getUserBiometricData().getWeightInKilogram() + ", heightInMeter=" + userProfile.getUserBiometricData().getHeightInMeter() + ", goal=" + userProfile.getGoalSteps() + ", isNewDevice=" + zq2.mo33111c() + ", SyncMode=" + b);
                        userProfile.setOriginalSyncMode(b);
                        boolean k = this.f21797f.mo27053k(zq2.mo33109a());
                        if (!k || b == 13) {
                            b = 13;
                        }
                        if (b == 13 || !com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34540h(zq2.mo33109a())) {
                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(f21793q, "start set localization");
                            com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34468O();
                            if (b == 13) {
                                try {
                                    this.f21797f.mo26999a(zq2.mo33109a(), java.lang.System.currentTimeMillis(), false);
                                    dianaSyncUseCase$run$1.L$0 = this;
                                    dianaSyncUseCase$run$1.L$1 = zq2;
                                    dianaSyncUseCase$run$1.I$0 = b;
                                    dianaSyncUseCase$run$1.L$2 = userProfile;
                                    dianaSyncUseCase$run$1.Z$0 = k;
                                    dianaSyncUseCase$run$1.label = 1;
                                    if (mo40278a((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) dianaSyncUseCase$run$1) == a) {
                                        return a;
                                    }
                                    dianaSyncUseCase = this;
                                    i2 = b;
                                } catch (java.lang.Exception e) {
                                    i2 = b;
                                    e = e;
                                    dianaSyncUseCase = this;
                                    com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                                    com.misfit.frameworks.buttonservice.log.FLogger.Component component = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                                    com.misfit.frameworks.buttonservice.log.FLogger.Session session = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER;
                                    java.lang.String a2 = zq2.mo33109a();
                                    java.lang.String str = f21793q;
                                    remote.mo33266i(component, session, a2, str, "[Sync] Exception when prepare settings " + e);
                                    dianaSyncUseCase.mo40282b(i2, zq2.mo33111c(), userProfile, zq2.mo33109a());
                                    return new java.lang.Object();
                                }
                            } else {
                                dianaSyncUseCase = this;
                                i2 = b;
                                dianaSyncUseCase.mo40282b(i2, zq2.mo33111c(), userProfile, zq2.mo33109a());
                                return new java.lang.Object();
                            }
                        } else {
                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(mo26311c(), "Device is syncing, returning...");
                            return new com.fossil.blesdk.obfuscated.yq2(com.portfolio.platform.enums.SyncResponseCode.SYNC_IS_IN_PROGRESS, (java.util.ArrayList<java.lang.Integer>) null);
                        }
                    }
                } else if (i == 1) {
                    boolean z = dianaSyncUseCase$run$1.Z$0;
                    com.misfit.frameworks.buttonservice.model.UserProfile userProfile2 = (com.misfit.frameworks.buttonservice.model.UserProfile) dianaSyncUseCase$run$1.L$2;
                    i2 = dianaSyncUseCase$run$1.I$0;
                    com.fossil.blesdk.obfuscated.zq2 zq22 = (com.fossil.blesdk.obfuscated.zq2) dianaSyncUseCase$run$1.L$1;
                    dianaSyncUseCase = (com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase) dianaSyncUseCase$run$1.L$0;
                    try {
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        com.fossil.blesdk.obfuscated.zq2 zq23 = zq22;
                        userProfile = userProfile2;
                        zq2 = zq23;
                    } catch (java.lang.Exception e2) {
                        e = e2;
                        com.fossil.blesdk.obfuscated.zq2 zq24 = zq22;
                        userProfile = userProfile2;
                        zq2 = zq24;
                        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                        com.misfit.frameworks.buttonservice.log.FLogger.Component component2 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                        com.misfit.frameworks.buttonservice.log.FLogger.Session session2 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER;
                        java.lang.String a22 = zq2.mo33109a();
                        java.lang.String str2 = f21793q;
                        remote2.mo33266i(component2, session2, a22, str2, "[Sync] Exception when prepare settings " + e);
                        dianaSyncUseCase.mo40282b(i2, zq2.mo33111c(), userProfile, zq2.mo33109a());
                        return new java.lang.Object();
                    }
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String c5 = dianaSyncUseCase.mo26311c();
                local5.mo33255d(c5, "Inside " + dianaSyncUseCase.mo26311c() + ".startDeviceSync - Start full-sync");
                activeAlarms = dianaSyncUseCase.f21807p.getActiveAlarms();
                if (activeAlarms == null) {
                    activeAlarms = new java.util.ArrayList();
                }
                com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34508a((java.util.List<? extends com.misfit.frameworks.buttonservice.model.Alarm>) com.fossil.blesdk.obfuscated.nj2.m25699a(activeAlarms));
                activePresetBySerial = dianaSyncUseCase.f21796e.getActivePresetBySerial(zq2.mo33109a());
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String c6 = dianaSyncUseCase.mo26311c();
                local6.mo33255d(c6, "startDeviceSync activePreset=" + activePresetBySerial);
                if (activePresetBySerial != null) {
                    com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34493a(com.fossil.blesdk.obfuscated.sj2.m27752a(activePresetBySerial.getComplications(), new com.google.gson.Gson()), zq2.mo33109a());
                    com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34495a(com.fossil.blesdk.obfuscated.sj2.m27762b(activePresetBySerial.getWatchapps(), new com.google.gson.Gson()), zq2.mo33109a());
                    com.portfolio.platform.data.model.diana.preset.WatchFace watchFaceWithId = dianaSyncUseCase.f21806o.getWatchFaceWithId(activePresetBySerial.getWatchFaceId());
                    if (watchFaceWithId != null) {
                        backgroundConfig = com.fossil.blesdk.obfuscated.tj2.m28283a(watchFaceWithId, (java.util.List<com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting>) activePresetBySerial.getComplications());
                    }
                    if (backgroundConfig != null) {
                        com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34491a(backgroundConfig, zq2.mo33109a());
                    }
                }
                dianaSyncUseCase.mo40282b(i2, zq2.mo33111c(), userProfile, zq2.mo33109a());
                return new java.lang.Object();
            }
        }
        dianaSyncUseCase$run$1 = new com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$run$1(this, yb4);
        java.lang.Object obj2 = dianaSyncUseCase$run$1.result;
        java.lang.Object a3 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = dianaSyncUseCase$run$1.label;
        com.misfit.frameworks.buttonservice.model.background.BackgroundConfig backgroundConfig2 = null;
        if (i != 0) {
        }
        try {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local52 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String c52 = dianaSyncUseCase.mo26311c();
            local52.mo33255d(c52, "Inside " + dianaSyncUseCase.mo26311c() + ".startDeviceSync - Start full-sync");
            activeAlarms = dianaSyncUseCase.f21807p.getActiveAlarms();
            if (activeAlarms == null) {
            }
            com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34508a((java.util.List<? extends com.misfit.frameworks.buttonservice.model.Alarm>) com.fossil.blesdk.obfuscated.nj2.m25699a(activeAlarms));
            activePresetBySerial = dianaSyncUseCase.f21796e.getActivePresetBySerial(zq2.mo33109a());
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local62 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String c62 = dianaSyncUseCase.mo26311c();
            local62.mo33255d(c62, "startDeviceSync activePreset=" + activePresetBySerial);
            if (activePresetBySerial != null) {
            }
        } catch (java.lang.Exception e3) {
            e = e3;
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote22 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
            com.misfit.frameworks.buttonservice.log.FLogger.Component component22 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
            com.misfit.frameworks.buttonservice.log.FLogger.Session session22 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER;
            java.lang.String a222 = zq2.mo33109a();
            java.lang.String str22 = f21793q;
            remote22.mo33266i(component22, session22, a222, str22, "[Sync] Exception when prepare settings " + e);
            dianaSyncUseCase.mo40282b(i2, zq2.mo33111c(), userProfile, zq2.mo33109a());
            return new java.lang.Object();
        }
        dianaSyncUseCase.mo40282b(i2, zq2.mo33111c(), userProfile, zq2.mo33109a());
        return new java.lang.Object();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* renamed from: a */
    public final /* synthetic */ java.lang.Object mo40278a(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4> yb4) {
        com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$setRuleNotificationFilterToDevice$1 dianaSyncUseCase$setRuleNotificationFilterToDevice$1;
        int i;
        if (yb4 instanceof com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$setRuleNotificationFilterToDevice$1) {
            dianaSyncUseCase$setRuleNotificationFilterToDevice$1 = (com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$setRuleNotificationFilterToDevice$1) yb4;
            int i2 = dianaSyncUseCase$setRuleNotificationFilterToDevice$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaSyncUseCase$setRuleNotificationFilterToDevice$1.label = i2 - Integer.MIN_VALUE;
                com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$setRuleNotificationFilterToDevice$1 dianaSyncUseCase$setRuleNotificationFilterToDevice$12 = dianaSyncUseCase$setRuleNotificationFilterToDevice$1;
                java.lang.Object obj = dianaSyncUseCase$setRuleNotificationFilterToDevice$12.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = dianaSyncUseCase$setRuleNotificationFilterToDevice$12.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.portfolio.platform.util.NotificationAppHelper notificationAppHelper = com.portfolio.platform.util.NotificationAppHelper.f24430b;
                    com.fossil.blesdk.obfuscated.vy2 vy2 = this.f21798g;
                    com.fossil.blesdk.obfuscated.px2 px2 = this.f21802k;
                    com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase notificationSettingsDatabase = this.f21801j;
                    com.fossil.blesdk.obfuscated.en2 en2 = this.f21797f;
                    dianaSyncUseCase$setRuleNotificationFilterToDevice$12.L$0 = this;
                    dianaSyncUseCase$setRuleNotificationFilterToDevice$12.label = 1;
                    obj = notificationAppHelper.mo41918a(vy2, px2, notificationSettingsDatabase, en2, dianaSyncUseCase$setRuleNotificationFilterToDevice$12);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase dianaSyncUseCase = (com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase) dianaSyncUseCase$setRuleNotificationFilterToDevice$12.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34494a(new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings((java.util.List) obj, java.lang.System.currentTimeMillis()), com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e());
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }
        dianaSyncUseCase$setRuleNotificationFilterToDevice$1 = new com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$setRuleNotificationFilterToDevice$1(this, yb4);
        com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$setRuleNotificationFilterToDevice$1 dianaSyncUseCase$setRuleNotificationFilterToDevice$122 = dianaSyncUseCase$setRuleNotificationFilterToDevice$1;
        java.lang.Object obj2 = dianaSyncUseCase$setRuleNotificationFilterToDevice$122.result;
        java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = dianaSyncUseCase$setRuleNotificationFilterToDevice$122.label;
        if (i != 0) {
        }
        com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34494a(new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings((java.util.List) obj2, java.lang.System.currentTimeMillis()), com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40280a(int i, java.lang.String str, int i2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String c = mo26311c();
        local.mo33255d(c, "broadcastSyncStatus serial=" + str + ' ' + i2);
        android.content.Intent intent = new android.content.Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", i2);
        intent.putExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getORIGINAL_SYNC_MODE(), i);
        intent.putExtra("SERIAL", str);
        com.portfolio.platform.service.BleCommandResultManager bleCommandResultManager = com.portfolio.platform.service.BleCommandResultManager.f21345d;
        com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC;
        bleCommandResultManager.mo39769a(communicateMode, new com.portfolio.platform.service.BleCommandResultManager.C5998a(communicateMode, str, intent));
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40281a(int i, boolean z, com.misfit.frameworks.buttonservice.model.UserProfile userProfile, java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(userProfile, "userProfile");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        if (mo34437a() != null) {
            com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39771a((com.portfolio.platform.service.BleCommandResultManager.C5999b) this.f21795d, com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC);
        }
        userProfile.setNewDevice(z);
        userProfile.setSyncMode(i);
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String c = mo26311c();
        local.mo33255d(c, ".startDeviceSync - syncMode=" + i);
        if (!this.f21797f.mo26983U()) {
            com.portfolio.platform.data.model.SKUModel skuModelBySerialPrefix = this.f21800i.getSkuModelBySerialPrefix(com.portfolio.platform.helper.DeviceHelper.f21188o.mo39559b(str));
            this.f21797f.mo26997a((java.lang.Boolean) true);
            this.f21805n.mo39493a(i, skuModelBySerialPrefix);
            com.fossil.blesdk.obfuscated.ul2 b = com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39514b("sync_session");
            com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39513a("sync_session", b);
            b.mo31559d();
        }
        if (!com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34512a(str, userProfile)) {
            mo40280a(userProfile.getOriginalSyncMode(), str, 2);
        }
    }
}
