package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$1", mo27670f = "UnlinkDeviceUseCase.kt", mo27671l = {57}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$1 */
public final class UnlinkDeviceUseCase$doRemoveDevice$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21907p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UnlinkDeviceUseCase$doRemoveDevice$1(com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase unlinkDeviceUseCase, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = unlinkDeviceUseCase;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$1 unlinkDeviceUseCase$doRemoveDevice$1 = new com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$1(this.this$0, yb4);
        unlinkDeviceUseCase$doRemoveDevice$1.f21907p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return unlinkDeviceUseCase$doRemoveDevice$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d3, code lost:
        if (r12 != null) goto L_0x00d7;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.data.model.Device device;
        java.lang.String str;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21907p$;
            com.portfolio.platform.data.source.DeviceRepository a2 = this.this$0.f21900e;
            com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.C6167b b = this.this$0.f21899d;
            java.lang.String str2 = null;
            java.lang.String a3 = b != null ? b.mo40366a() : null;
            if (a3 != null) {
                com.portfolio.platform.data.model.Device deviceBySerial = a2.getDeviceBySerial(a3);
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a4 = com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.f21898i.mo40365a();
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append("doRemoveDevice ");
                if (deviceBySerial != null) {
                    str2 = deviceBySerial.getDeviceId();
                }
                sb.append(str2);
                local.mo33255d(a4, sb.toString());
                if (deviceBySerial != null) {
                    com.portfolio.platform.data.source.DeviceRepository a5 = this.this$0.f21900e;
                    this.L$0 = zg4;
                    this.L$1 = deviceBySerial;
                    this.label = 1;
                    obj = a5.removeDevice(deviceBySerial, this);
                    if (obj == a) {
                        return a;
                    }
                    device = deviceBySerial;
                } else {
                    this.this$0.mo34434a(new com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.C6168c("UNLINK_FAIL_ON_SERVER", com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new java.lang.Integer[]{com.fossil.blesdk.obfuscated.dc4.m20843a(600)}), ""));
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        } else if (i == 1) {
            device = (com.portfolio.platform.data.model.Device) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
        if (qo2 instanceof com.fossil.blesdk.obfuscated.ro2) {
            java.lang.String deviceId = device.getDeviceId();
            com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34571r(deviceId);
            if (com.portfolio.platform.helper.DeviceHelper.f21188o.mo39568f(deviceId)) {
                this.this$0.f21902g.deleteWatchFacesWithSerial(deviceId);
            }
            this.this$0.mo34436a(new com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.C6169d());
        } else if (qo2 instanceof com.fossil.blesdk.obfuscated.po2) {
            com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase unlinkDeviceUseCase = this.this$0;
            com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
            java.util.ArrayList a6 = com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new java.lang.Integer[]{com.fossil.blesdk.obfuscated.dc4.m20843a(po2.mo30176a())});
            com.portfolio.platform.data.model.ServerError c = po2.mo30178c();
            if (c != null) {
                str = c.getUserMessage();
            }
            str = "";
            unlinkDeviceUseCase.mo34434a(new com.portfolio.platform.p007ui.device.domain.usecase.UnlinkDeviceUseCase.C6168c("UNLINK_FAIL_ON_SERVER", a6, str));
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
