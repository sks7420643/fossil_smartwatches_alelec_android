package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase */
public final class LinkDeviceUseCase extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6148g, com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6150i, com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6149h> {

    @DexIgnore
    /* renamed from: p */
    public static /* final */ java.lang.String f21831p;

    @DexIgnore
    /* renamed from: q */
    public static /* final */ com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6143b f21832q; // = new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6143b((com.fossil.blesdk.obfuscated.fd4) null);

    @DexIgnore
    /* renamed from: d */
    public java.lang.String f21833d; // = "";

    @DexIgnore
    /* renamed from: e */
    public java.lang.String f21834e; // = "";

    @DexIgnore
    /* renamed from: f */
    public com.portfolio.platform.data.model.Device f21835f;

    @DexIgnore
    /* renamed from: g */
    public java.lang.String f21836g; // = "";

    @DexIgnore
    /* renamed from: h */
    public boolean f21837h;

    @DexIgnore
    /* renamed from: i */
    public boolean f21838i;

    @DexIgnore
    /* renamed from: j */
    public com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6151j f21839j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6147f f21840k; // = new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6147f();

    @DexIgnore
    /* renamed from: l */
    public /* final */ com.portfolio.platform.data.source.DeviceRepository f21841l;

    @DexIgnore
    /* renamed from: m */
    public /* final */ com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase f21842m;

    @DexIgnore
    /* renamed from: n */
    public /* final */ com.fossil.blesdk.obfuscated.en2 f21843n;

    @DexIgnore
    /* renamed from: o */
    public /* final */ com.fossil.blesdk.obfuscated.vj2 f21844o;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$a")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$a */
    public static final class C6142a extends com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6150i {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f21845a;

        @DexIgnore
        public C6142a(java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
            this.f21845a = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40314a() {
            return this.f21845a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$b")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$b */
    public static final class C6143b {
        @DexIgnore
        public C6143b() {
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40315a() {
            return com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.f21831p;
        }

        @DexIgnore
        public /* synthetic */ C6143b(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$c")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$c */
    public static final class C6144c extends com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6149h {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C6144c(int i, java.lang.String str, java.lang.String str2) {
            super(i, str, str2);
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "deviceId");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$d")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$d */
    public static final class C6145d extends com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6149h {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C6145d(java.lang.String str, java.lang.String str2) {
            super(-1, str, str2);
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "deviceId");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$e")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$e */
    public static final class C6146e extends com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6150i {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f21846a;

        @DexIgnore
        public C6146e(java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
            this.f21846a = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40316a() {
            return this.f21846a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$f")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$f */
    public final class C6147f implements com.portfolio.platform.service.BleCommandResultManager.C5999b {
        @DexIgnore
        public C6147f() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo27136a(com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode, android.content.Intent intent) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(communicateMode, "communicateMode");
            com.fossil.blesdk.obfuscated.kd4.m24411b(intent, "intent");
            java.lang.String stringExtra = intent.getStringExtra(com.misfit.frameworks.common.constants.Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), com.misfit.frameworks.buttonservice.log.FailureCode.UNKNOWN_ERROR);
            com.misfit.frameworks.buttonservice.enums.ServiceActionResult serviceActionResult = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.values()[intent.getIntExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getSERVICE_ACTION_RESULT(), com.misfit.frameworks.buttonservice.enums.ServiceActionResult.UNALLOWED_ACTION.ordinal())];
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.f21832q.mo40315a();
            local.mo33255d(a, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40309j() + ", isSuccess=" + intent.getIntExtra(com.misfit.frameworks.buttonservice.ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1));
            boolean z = true;
            if (com.fossil.blesdk.obfuscated.qf4.m26954b(stringExtra, com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40307h(), true) && communicateMode == com.misfit.frameworks.buttonservice.communite.CommunicateMode.LINK && com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40309j()) {
                boolean z2 = false;
                switch (com.fossil.blesdk.obfuscated.sq2.f18633a[serviceActionResult.ordinal()]) {
                    case 1:
                        if (intent.getExtras() == null) {
                            z = false;
                        }
                        if (!com.fossil.blesdk.obfuscated.ra4.f18216a || z) {
                            com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this;
                            android.os.Bundle extras = intent.getExtras();
                            if (extras != null) {
                                java.lang.String string = extras.getString("device_model", "");
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) string, "intent.extras!!.getStrin\u2026nstants.DEVICE_MODEL, \"\")");
                                linkDeviceUseCase.mo40299a(string);
                                com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase2 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this;
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) stringExtra, "serial");
                                linkDeviceUseCase2.mo34436a(new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6142a(stringExtra));
                                com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40312m();
                                return;
                            }
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                        throw new java.lang.AssertionError("Assertion failed");
                    case 2:
                        com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase3 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this;
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) stringExtra, "serial");
                        linkDeviceUseCase3.mo34436a(new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6146e(stringExtra));
                        return;
                    case 3:
                        com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase4 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this;
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) stringExtra, "serial");
                        linkDeviceUseCase4.mo34436a(new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6153l(stringExtra, true));
                        return;
                    case 4:
                        com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase5 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this;
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) stringExtra, "serial");
                        linkDeviceUseCase5.mo34436a(new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6153l(stringExtra, false));
                        return;
                    case 5:
                        boolean z3 = intent.getExtras() != null;
                        if (!com.fossil.blesdk.obfuscated.ra4.f18216a || z3) {
                            android.os.Bundle extras2 = intent.getExtras();
                            if (extras2 != null) {
                                z2 = extras2.getBoolean(com.misfit.frameworks.buttonservice.utils.Constants.IS_JUST_OTA, false);
                            }
                            android.os.Bundle extras3 = intent.getExtras();
                            if (extras3 != null) {
                                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile misfitDeviceProfile = (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) extras3.getParcelable("device");
                                if (z2) {
                                    com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase6 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this;
                                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) stringExtra, "serial");
                                    linkDeviceUseCase6.mo34436a(new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6153l(stringExtra, true));
                                } else {
                                    com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase7 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this;
                                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) stringExtra, "serial");
                                    linkDeviceUseCase7.mo34436a(new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6146e(stringExtra));
                                }
                                if (misfitDeviceProfile != null) {
                                    com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40302c(misfitDeviceProfile.getDeviceSerial());
                                    com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40301b(misfitDeviceProfile.getAddress());
                                    com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase8 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this;
                                    com.portfolio.platform.data.model.Device device = new com.portfolio.platform.data.model.Device(misfitDeviceProfile.getDeviceSerial(), misfitDeviceProfile.getAddress(), misfitDeviceProfile.getDeviceModel(), misfitDeviceProfile.getFirmwareVersion(), misfitDeviceProfile.getBatteryLevel(), 50, false, 64, (com.fossil.blesdk.obfuscated.fd4) null);
                                    linkDeviceUseCase8.mo40297a(device);
                                    com.portfolio.platform.data.model.Device e = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40304e();
                                    if (e != null) {
                                        e.appendAdditionalInfo(misfitDeviceProfile);
                                    }
                                    com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40303d();
                                    return;
                                }
                                com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase9 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this;
                                java.lang.String h = linkDeviceUseCase9.mo40307h();
                                if (h != null) {
                                    linkDeviceUseCase9.mo34434a(new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6144c(com.misfit.frameworks.buttonservice.log.FailureCode.UNKNOWN_ERROR, h, "No device profile"));
                                    com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
                                    java.lang.String h2 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40307h();
                                    if (h2 != null) {
                                        c.mo34499a(h2);
                                        return;
                                    } else {
                                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                                        throw null;
                                    }
                                } else {
                                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                                    throw null;
                                }
                            } else {
                                com.fossil.blesdk.obfuscated.kd4.m24405a();
                                throw null;
                            }
                        } else {
                            throw new java.lang.AssertionError("Assertion failed");
                        }
                    case 6:
                        com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40300a(false);
                        com.portfolio.platform.PortfolioApp c2 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) stringExtra, "serial");
                        c2.mo34527c(stringExtra, com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40306g());
                        com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase10 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this;
                        com.portfolio.platform.data.model.Device e2 = linkDeviceUseCase10.mo40304e();
                        if (e2 != null) {
                            linkDeviceUseCase10.mo34436a(new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6152k(e2));
                            return;
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                    case 7:
                        com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40300a(false);
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a2 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.f21832q.mo40315a();
                        local2.mo33255d(a2, "Pair device failed due to " + intExtra + ", remove this device on button service");
                        try {
                            com.portfolio.platform.PortfolioApp c3 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
                            java.lang.String h3 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40307h();
                            if (h3 != null) {
                                c3.mo34550m(h3);
                                com.portfolio.platform.PortfolioApp c4 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
                                java.lang.String h4 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40307h();
                                if (h4 != null) {
                                    c4.mo34548l(h4);
                                    if (intExtra > 1000) {
                                        com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase11 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this;
                                        java.lang.String h5 = linkDeviceUseCase11.mo40307h();
                                        if (h5 != null) {
                                            linkDeviceUseCase11.mo34434a(new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6144c(intExtra, h5, ""));
                                            return;
                                        } else {
                                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                                            throw null;
                                        }
                                    } else {
                                        com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase12 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this;
                                        java.lang.String h6 = linkDeviceUseCase12.mo40307h();
                                        if (h6 != null) {
                                            linkDeviceUseCase12.mo40298a(new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6151j(intExtra, h6, ""));
                                            com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase13 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this;
                                            com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6151j i = linkDeviceUseCase13.mo40308i();
                                            if (i != null) {
                                                linkDeviceUseCase13.mo34434a(i);
                                                com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.this.mo40298a((com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6151j) null);
                                                return;
                                            }
                                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                                            throw null;
                                        }
                                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                                        throw null;
                                    }
                                } else {
                                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                                    throw null;
                                }
                            } else {
                                com.fossil.blesdk.obfuscated.kd4.m24405a();
                                throw null;
                            }
                        } catch (java.lang.Exception e3) {
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String a3 = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.f21832q.mo40315a();
                            local3.mo33255d(a3, "Pair device failed, remove this device on button service exception=" + e3.getMessage());
                        }
                    default:
                        return;
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$g")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$g */
    public static final class C6148g implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f21848a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.String f21849b;

        @DexIgnore
        public C6148g(java.lang.String str, java.lang.String str2) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "device");
            com.fossil.blesdk.obfuscated.kd4.m24411b(str2, "macAddress");
            this.f21848a = str;
            this.f21849b = str2;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40317a() {
            return this.f21848a;
        }

        @DexIgnore
        /* renamed from: b */
        public final java.lang.String mo40318b() {
            return this.f21849b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$h")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$h */
    public static class C6149h implements com.portfolio.platform.CoroutineUseCase.C5602a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f21850a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.String f21851b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ java.lang.String f21852c;

        @DexIgnore
        public C6149h(int i, java.lang.String str, java.lang.String str2) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "deviceId");
            this.f21850a = i;
            this.f21851b = str;
            this.f21852c = str2;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40319a() {
            return this.f21851b;
        }

        @DexIgnore
        /* renamed from: b */
        public final int mo40320b() {
            return this.f21850a;
        }

        @DexIgnore
        /* renamed from: c */
        public final java.lang.String mo40321c() {
            return this.f21852c;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$i")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$i */
    public static abstract class C6150i implements com.portfolio.platform.CoroutineUseCase.C5605d {
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$j")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$j */
    public static final class C6151j extends com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6149h {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C6151j(int i, java.lang.String str, java.lang.String str2) {
            super(i, str, str2);
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "deviceId");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$k")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$k */
    public static final class C6152k extends com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6150i {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.portfolio.platform.data.model.Device f21853a;

        @DexIgnore
        public C6152k(com.portfolio.platform.data.model.Device device) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(device, "device");
            this.f21853a = device;
        }

        @DexIgnore
        /* renamed from: a */
        public final com.portfolio.platform.data.model.Device mo40322a() {
            return this.f21853a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$l")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$l */
    public static final class C6153l extends com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6150i {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f21854a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ boolean f21855b;

        @DexIgnore
        public C6153l(java.lang.String str, boolean z) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
            this.f21854a = str;
            this.f21855b = z;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40323a() {
            return this.f21854a;
        }

        @DexIgnore
        /* renamed from: b */
        public final boolean mo40324b() {
            return this.f21855b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$m")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$m */
    public static final class C6154m implements com.portfolio.platform.CoroutineUseCase.C5606e<com.fossil.blesdk.obfuscated.pn3, com.fossil.blesdk.obfuscated.nn3> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase f21856a;

        @DexIgnore
        public C6154m(com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase) {
            this.f21856a = linkDeviceUseCase;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(com.fossil.blesdk.obfuscated.pn3 pn3) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(pn3, "responseValue");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.f21832q.mo40315a(), " get device setting success");
            this.f21856a.mo40311l();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo29641a(com.fossil.blesdk.obfuscated.nn3 nn3) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(nn3, "errorValue");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.f21832q.mo40315a(), " get device setting fail!!");
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
            com.misfit.frameworks.buttonservice.log.FLogger.Component component = com.misfit.frameworks.buttonservice.log.FLogger.Component.API;
            com.misfit.frameworks.buttonservice.log.FLogger.Session session = com.misfit.frameworks.buttonservice.log.FLogger.Session.PAIR;
            java.lang.String h = this.f21856a.mo40307h();
            if (h != null) {
                java.lang.String a = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.f21832q.mo40315a();
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append("Get device setting of ");
                java.lang.String h2 = this.f21856a.mo40307h();
                if (h2 != null) {
                    sb.append(h2);
                    sb.append(", server error=");
                    sb.append(nn3.mo29643a());
                    sb.append(", error = ");
                    sb.append(com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.INSTANCE.build(com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.Step.LINK_DEVICE, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.Component.APP, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.AppError.NETWORK_ERROR));
                    remote.mo33262e(component, session, h, a, sb.toString());
                    com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase = this.f21856a;
                    int a2 = nn3.mo29643a();
                    java.lang.String h3 = this.f21856a.mo40307h();
                    if (h3 != null) {
                        linkDeviceUseCase.mo40298a(new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6151j(a2, h3, ""));
                        com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
                        java.lang.String h4 = this.f21856a.mo40307h();
                        if (h4 != null) {
                            c.mo34513a(h4, (com.misfit.frameworks.buttonservice.model.pairing.PairingResponse) com.misfit.frameworks.buttonservice.model.pairing.PairingResponse.CREATOR.buildPairingLinkServerResponse(false, nn3.mo29643a()));
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
    }

    /*
    static {
        java.lang.String simpleName = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "LinkDeviceUseCase::class.java.simpleName");
        f21831p = simpleName;
    }
    */

    @DexIgnore
    public LinkDeviceUseCase(com.portfolio.platform.data.source.DeviceRepository deviceRepository, com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase, com.fossil.blesdk.obfuscated.en2 en2, com.fossil.blesdk.obfuscated.vj2 vj2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceRepository, "mDeviceRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(downloadFirmwareByDeviceModelUsecase, "mDownloadFwByDeviceModel");
        com.fossil.blesdk.obfuscated.kd4.m24411b(en2, "mSharedPreferencesManager");
        com.fossil.blesdk.obfuscated.kd4.m24411b(vj2, "mDeviceSettingFactory");
        this.f21841l = deviceRepository;
        this.f21842m = downloadFirmwareByDeviceModelUsecase;
        this.f21843n = en2;
        this.f21844o = vj2;
    }

    @DexIgnore
    /* renamed from: d */
    public final synchronized void mo40303d() {
        com.fossil.blesdk.obfuscated.vj2 vj2 = this.f21844o;
        java.lang.String str = this.f21833d;
        if (str != null) {
            com.portfolio.platform.CoroutineUseCase<com.fossil.blesdk.obfuscated.on3, com.fossil.blesdk.obfuscated.pn3, com.fossil.blesdk.obfuscated.nn3> a = vj2.mo31816a(str);
            java.lang.String str2 = this.f21833d;
            if (str2 != null) {
                a.mo34435a(new com.fossil.blesdk.obfuscated.on3(str2), (com.portfolio.platform.CoroutineUseCase.C5606e<? super com.fossil.blesdk.obfuscated.pn3, ? super com.fossil.blesdk.obfuscated.nn3>) new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6154m(this));
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        } else {
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: e */
    public final com.portfolio.platform.data.model.Device mo40304e() {
        return this.f21835f;
    }

    @DexIgnore
    /* renamed from: f */
    public final java.lang.String mo40305f() {
        return this.f21836g;
    }

    @DexIgnore
    /* renamed from: g */
    public final java.lang.String mo40306g() {
        return this.f21834e;
    }

    @DexIgnore
    /* renamed from: h */
    public final java.lang.String mo40307h() {
        return this.f21833d;
    }

    @DexIgnore
    /* renamed from: i */
    public final com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6151j mo40308i() {
        return this.f21839j;
    }

    @DexIgnore
    /* renamed from: j */
    public final boolean mo40309j() {
        return this.f21837h;
    }

    @DexIgnore
    /* renamed from: k */
    public final void mo40310k() {
        com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39771a((com.portfolio.platform.service.BleCommandResultManager.C5999b) this.f21840k, com.misfit.frameworks.buttonservice.communite.CommunicateMode.LINK);
    }

    @DexIgnore
    /* renamed from: l */
    public final synchronized void mo40311l() {
        if (!this.f21838i) {
            this.f21838i = true;
        } else {
            com.portfolio.platform.data.model.Device device = this.f21835f;
            if (device != null) {
                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(mo34439b(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.device.domain.usecase.C6155x722855f6(device, (com.fossil.blesdk.obfuscated.yb4) null, this), 3, (java.lang.Object) null);
            }
        }
    }

    @DexIgnore
    /* renamed from: m */
    public final void mo40312m() {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str = f21831p;
        local.mo33255d(str, "sendLatestFWResponseToDevice(), deviceModel=" + this.f21836g + ", isSkipOTA=" + this.f21843n.mo26982T());
        if (com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34457D() || !this.f21843n.mo26982T()) {
            this.f21842m.mo34435a(new com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6137b(this.f21836g), new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$1(this));
            return;
        }
        com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
        java.lang.String str2 = this.f21833d;
        if (str2 != null) {
            c.mo34513a(str2, (com.misfit.frameworks.buttonservice.model.pairing.PairingResponse) com.misfit.frameworks.buttonservice.model.pairing.PairingResponse.CREATOR.buildPairingUpdateFWResponse(new com.misfit.frameworks.buttonservice.model.SkipFirmwareData()));
        } else {
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: n */
    public final void mo40313n() {
        com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39775b((com.portfolio.platform.service.BleCommandResultManager.C5999b) this.f21840k, com.misfit.frameworks.buttonservice.communite.CommunicateMode.LINK);
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo40301b(java.lang.String str) {
        this.f21834e = str;
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo40302c(java.lang.String str) {
        this.f21833d = str;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40297a(com.portfolio.platform.data.model.Device device) {
        this.f21835f = device;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f21831p;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40299a(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "<set-?>");
        this.f21836g = str;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40300a(boolean z) {
        this.f21837h = z;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40298a(com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6151j jVar) {
        this.f21839j = jVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40296a(com.misfit.frameworks.buttonservice.model.ShineDevice shineDevice, com.portfolio.platform.CoroutineUseCase.C5606e<? super com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6150i, ? super com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6149h> eVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(shineDevice, "closestDevice");
        com.fossil.blesdk.obfuscated.kd4.m24411b(eVar, "caseCallback");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(f21831p, "onRetrieveLinkAction");
        this.f21837h = true;
        this.f21833d = shineDevice.getSerial();
        this.f21834e = shineDevice.getMacAddress();
        mo34438a(eVar);
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6148g gVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(f21831p, "running UseCase");
        if (gVar == null) {
            return new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6149h(600, "", "");
        }
        this.f21837h = true;
        this.f21833d = gVar.mo40317a();
        this.f21834e = "";
        gVar.mo40318b();
        this.f21834e = gVar.mo40318b();
        try {
            com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
            java.lang.String str = this.f21833d;
            if (str != null) {
                java.lang.String str2 = this.f21834e;
                if (str2 != null) {
                    c.mo34504a(str, str2);
                    return new java.lang.Object();
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str3 = f21831p;
            local.mo33256e(str3, "Error inside " + f21831p + ".connectDevice - e=" + e);
        }
    }
}
