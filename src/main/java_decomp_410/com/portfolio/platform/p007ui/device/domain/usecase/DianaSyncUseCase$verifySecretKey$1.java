package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1 */
public final class DianaSyncUseCase$verifySecretKey$1 implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.usecase.VerifySecretKeyUseCase.C6923d, com.portfolio.platform.usecase.VerifySecretKeyUseCase.C6922c> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase f21809a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ /* synthetic */ java.lang.String f21810b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ /* synthetic */ int f21811c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ /* synthetic */ boolean f21812d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.model.UserProfile f21813e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1$a")
    /* renamed from: com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1$a */
    public static final class C6135a implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6173d, com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6172c> {
        @DexIgnore
        /* renamed from: a */
        public void mo29641a(com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6172c cVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(cVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6173d dVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
        }
    }

    @DexIgnore
    public DianaSyncUseCase$verifySecretKey$1(com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase dianaSyncUseCase, java.lang.String str, int i, boolean z, com.misfit.frameworks.buttonservice.model.UserProfile userProfile) {
        this.f21809a = dianaSyncUseCase;
        this.f21810b = str;
        this.f21811c = i;
        this.f21812d = z;
        this.f21813e = userProfile;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(com.portfolio.platform.usecase.VerifySecretKeyUseCase.C6923d dVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a = com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase.f21794r.mo40283a();
        local.mo33255d(a, "secret key " + dVar.mo41902a() + ", start sync");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().mo33266i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER, this.f21810b, com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase.f21794r.mo40283a(), "[Sync] Verify secret key success, start sync");
        com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34533e(dVar.mo41902a(), this.f21810b);
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f21809a.mo34439b(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1$onSuccess$1(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo29641a(com.portfolio.platform.usecase.VerifySecretKeyUseCase.C6922c cVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(cVar, "errorValue");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a = com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase.f21794r.mo40283a();
        local.mo33255d(a, "verify secret key fail, stop sync " + cVar.mo41901a());
        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
        com.misfit.frameworks.buttonservice.log.FLogger.Component component = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
        com.misfit.frameworks.buttonservice.log.FLogger.Session session = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER;
        java.lang.String str = this.f21810b;
        java.lang.String a2 = com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase.f21794r.mo40283a();
        remote.mo33266i(component, session, str, a2, "[Sync] Verify secret key failed by " + cVar.mo41901a());
        if (cVar.mo41901a() == com.fossil.blesdk.device.FeatureErrorCode.REQUEST_UNSUPPORTED.getCode() && this.f21813e.getOriginalSyncMode() == 12) {
            this.f21809a.f21804m.mo34435a(new com.portfolio.platform.p007ui.device.domain.usecase.UpdateFirmwareUsecase.C6171b(this.f21810b, true), new com.portfolio.platform.p007ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1.C6135a());
        }
        this.f21809a.mo40280a(this.f21813e.getOriginalSyncMode(), this.f21810b, 2);
    }
}
