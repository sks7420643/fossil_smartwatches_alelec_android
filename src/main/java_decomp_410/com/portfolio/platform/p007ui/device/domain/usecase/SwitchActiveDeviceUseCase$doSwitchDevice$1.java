package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$1", mo27670f = "SwitchActiveDeviceUseCase.kt", mo27671l = {}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$1 */
public final class SwitchActiveDeviceUseCase$doSwitchDevice$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21891p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$doSwitchDevice$1(com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = switchActiveDeviceUseCase;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$1 switchActiveDeviceUseCase$doSwitchDevice$1 = new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$1(this.this$0, yb4);
        switchActiveDeviceUseCase$doSwitchDevice$1.f21891p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return switchActiveDeviceUseCase$doSwitchDevice$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a = com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.f21875n.mo40354a();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("doSwitchDevice serial ");
            com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6162b f = this.this$0.mo40350f();
            if (f != null) {
                sb.append(f.mo40356b());
                local.mo33255d(a, sb.toString());
                com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
                com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6162b f2 = this.this$0.mo40350f();
                if (f2 != null) {
                    if (!c.mo34569q(f2.mo40356b())) {
                        this.this$0.mo40353i();
                        this.this$0.mo34434a(new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6163c(116, (java.util.ArrayList<java.lang.Integer>) null, ""));
                    }
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
