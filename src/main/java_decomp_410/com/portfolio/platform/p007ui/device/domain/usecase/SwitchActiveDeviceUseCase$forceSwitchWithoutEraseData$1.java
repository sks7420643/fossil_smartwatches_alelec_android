package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1", mo27670f = "SwitchActiveDeviceUseCase.kt", mo27671l = {180}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1 */
public final class SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $newActiveDeviceSerial;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21892p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1(com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = switchActiveDeviceUseCase;
        this.$newActiveDeviceSerial = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1 switchActiveDeviceUseCase$forceSwitchWithoutEraseData$1 = new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1(this.this$0, this.$newActiveDeviceSerial, yb4);
        switchActiveDeviceUseCase$forceSwitchWithoutEraseData$1.f21892p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return switchActiveDeviceUseCase$forceSwitchWithoutEraseData$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.String str;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21892p$;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.f21875n.mo40354a();
            local.mo33255d(a2, "could not erase new data file " + this.$newActiveDeviceSerial + ", force switch to it");
            java.lang.String e = this.this$0.f21883k.mo34532e();
            com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.this$0;
            switchActiveDeviceUseCase.mo40345a(switchActiveDeviceUseCase.f21881i.getDeviceBySerial(this.$newActiveDeviceSerial));
            boolean e2 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34534e(this.$newActiveDeviceSerial);
            if (e2) {
                com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase2 = this.this$0;
                java.lang.String str2 = this.$newActiveDeviceSerial;
                this.L$0 = zg4;
                this.L$1 = e;
                this.Z$0 = e2;
                this.label = 1;
                obj = switchActiveDeviceUseCase2.mo40344a(str2, (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) null, this);
                if (obj == a) {
                    return a;
                }
                str = e;
            } else {
                this.this$0.mo34434a(new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6163c(116, (java.util.ArrayList<java.lang.Integer>) null, ""));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        } else if (i == 1) {
            str = (java.lang.String) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (((java.lang.Boolean) ((kotlin.Pair) obj).component1()).booleanValue()) {
            this.this$0.mo40347b(this.$newActiveDeviceSerial);
            com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase switchActiveDeviceUseCase3 = this.this$0;
            switchActiveDeviceUseCase3.mo34436a(new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6164d(switchActiveDeviceUseCase3.mo40349e()));
        } else {
            com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34534e(str);
            com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6163c g = this.this$0.mo40351g();
            if (g == null || this.this$0.mo34434a(g) == null) {
                this.this$0.mo34434a(new com.portfolio.platform.p007ui.device.domain.usecase.SwitchActiveDeviceUseCase.C6163c(116, (java.util.ArrayList<java.lang.Integer>) null, ""));
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
