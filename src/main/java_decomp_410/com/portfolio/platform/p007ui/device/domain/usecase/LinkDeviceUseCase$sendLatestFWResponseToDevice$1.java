package com.portfolio.platform.p007ui.device.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$1 */
public final class LinkDeviceUseCase$sendLatestFWResponseToDevice$1 implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6139d, com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6138c> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase f21858a;

    @DexIgnore
    public LinkDeviceUseCase$sendLatestFWResponseToDevice$1(com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase) {
        this.f21858a = linkDeviceUseCase;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6139d dVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f21858a.mo34439b(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1(this, dVar, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo29641a(com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6138c cVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(cVar, "errorValue");
        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
        com.misfit.frameworks.buttonservice.log.FLogger.Component component = com.misfit.frameworks.buttonservice.log.FLogger.Component.API;
        com.misfit.frameworks.buttonservice.log.FLogger.Session session = com.misfit.frameworks.buttonservice.log.FLogger.Session.PAIR;
        java.lang.String h = this.f21858a.mo40307h();
        if (h != null) {
            java.lang.String a = com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.f21832q.mo40315a();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append(" downloadFw FAILED!!!, latestFwVersion=");
            java.lang.String h2 = this.f21858a.mo40307h();
            if (h2 != null) {
                sb.append(h2);
                sb.append(" but device is DianaEV1!!!");
                remote.mo33262e(component, session, h, a, sb.toString());
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.f21832q.mo40315a(), "checkFirmware - downloadFw FAILED!!!");
                com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase linkDeviceUseCase = this.f21858a;
                java.lang.String h3 = linkDeviceUseCase.mo40307h();
                if (h3 != null) {
                    linkDeviceUseCase.mo34434a(new com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6145d(h3, ""));
                    com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
                    java.lang.String h4 = this.f21858a.mo40307h();
                    if (h4 != null) {
                        c.mo34499a(h4);
                        this.f21858a.mo40300a(false);
                        return;
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        }
        com.fossil.blesdk.obfuscated.kd4.m24405a();
        throw null;
    }
}
