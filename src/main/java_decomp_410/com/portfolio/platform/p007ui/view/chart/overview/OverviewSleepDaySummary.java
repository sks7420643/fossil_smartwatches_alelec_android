package com.portfolio.platform.p007ui.view.chart.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewSleepDaySummary */
public final class OverviewSleepDaySummary extends android.view.View {

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.String f22140e;

    @DexIgnore
    /* renamed from: f */
    public float f22141f;

    @DexIgnore
    /* renamed from: g */
    public float f22142g;

    @DexIgnore
    /* renamed from: h */
    public float f22143h;

    @DexIgnore
    /* renamed from: i */
    public float f22144i;

    @DexIgnore
    /* renamed from: j */
    public float f22145j;

    @DexIgnore
    /* renamed from: k */
    public int f22146k;

    @DexIgnore
    /* renamed from: l */
    public int f22147l;

    @DexIgnore
    /* renamed from: m */
    public int f22148m;

    @DexIgnore
    /* renamed from: n */
    public float f22149n;

    @DexIgnore
    /* renamed from: o */
    public float f22150o;

    @DexIgnore
    /* renamed from: p */
    public android.graphics.RectF f22151p;

    @DexIgnore
    /* renamed from: q */
    public android.graphics.RectF f22152q;

    @DexIgnore
    /* renamed from: r */
    public android.graphics.RectF f22153r;

    @DexIgnore
    /* renamed from: s */
    public /* final */ android.graphics.Paint f22154s;

    @DexIgnore
    /* renamed from: t */
    public android.graphics.RectF f22155t;

    @DexIgnore
    /* renamed from: u */
    public /* final */ android.graphics.PorterDuffXfermode f22156u;

    @DexIgnore
    /* renamed from: v */
    public /* final */ android.graphics.PorterDuffXfermode f22157v;

    @DexIgnore
    public OverviewSleepDaySummary(android.content.Context context) {
        this(context, (android.util.AttributeSet) null);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40647a(float f, float f2, float f3) {
        this.f22143h = f;
        this.f22144i = f2;
        this.f22145j = f3;
        invalidate();
    }

    @DexIgnore
    public final java.lang.String getTAG() {
        return this.f22140e;
    }

    @DexIgnore
    public void onDraw(android.graphics.Canvas canvas) {
        if (canvas != null) {
            this.f22154s.setColor(0);
            this.f22154s.setXfermode(this.f22156u);
            android.graphics.RectF rectF = this.f22155t;
            float f = rectF.left;
            float f2 = rectF.top;
            float f3 = rectF.right;
            float f4 = rectF.bottom;
            float f5 = this.f22149n;
            com.fossil.blesdk.obfuscated.wr3.m29743a(canvas, f, f2, f3, f4, f5, f5, true, true, true, true, this.f22154s);
            mo40648a(canvas, this.f22151p, this.f22148m);
            mo40648a(canvas, this.f22152q, this.f22147l);
            mo40648a(canvas, this.f22153r, this.f22146k);
        }
    }

    @DexIgnore
    public void onMeasure(int i, int i2) {
        int size = android.view.View.MeasureSpec.getSize(i);
        int size2 = android.view.View.MeasureSpec.getSize(i2);
        setMeasuredDimension(size, size2);
        float f = (float) size;
        float f2 = this.f22150o;
        float f3 = f - (((float) 2) * f2);
        this.f22141f = this.f22143h * f3;
        this.f22142g = this.f22144i * f3;
        float f4 = (float) size2;
        this.f22155t.set(f2, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f - f2, f4);
        android.graphics.RectF rectF = this.f22151p;
        float f5 = this.f22150o;
        rectF.set(f5, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.f22141f + f5, f4);
        android.graphics.RectF rectF2 = this.f22152q;
        float f6 = this.f22150o;
        float f7 = this.f22141f;
        rectF2.set(f6 + f7, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f6 + f7 + this.f22142g, f4);
        android.graphics.RectF rectF3 = this.f22153r;
        float f8 = this.f22150o;
        rectF3.set(this.f22141f + f8 + this.f22142g, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f - f8, f4);
    }

    @DexIgnore
    public OverviewSleepDaySummary(android.content.Context context, android.util.AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewSleepDaySummary(android.content.Context context, android.util.AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewSleepDaySummary(android.content.Context context, android.util.AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.f22140e = "OverviewSleepDaySummary";
        this.f22151p = new android.graphics.RectF();
        this.f22152q = new android.graphics.RectF();
        this.f22153r = new android.graphics.RectF();
        this.f22154s = new android.graphics.Paint(1);
        this.f22155t = new android.graphics.RectF();
        this.f22156u = new android.graphics.PorterDuffXfermode(android.graphics.PorterDuff.Mode.CLEAR);
        this.f22157v = new android.graphics.PorterDuffXfermode(android.graphics.PorterDuff.Mode.DST_OVER);
        if (!(attributeSet == null || context == null)) {
            android.content.res.Resources.Theme theme = context.getTheme();
            if (theme != null) {
                android.content.res.TypedArray obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, com.fossil.blesdk.obfuscated.h62.OverviewSleepDaySummary, 0, 0);
                if (obtainStyledAttributes != null) {
                    try {
                        this.f22146k = obtainStyledAttributes.getColor(3, 0);
                        this.f22147l = obtainStyledAttributes.getColor(4, 0);
                        this.f22148m = obtainStyledAttributes.getColor(0, 0);
                        this.f22149n = (float) obtainStyledAttributes.getDimensionPixelSize(2, 5);
                        this.f22150o = (float) obtainStyledAttributes.getDimensionPixelSize(1, 30);
                    } catch (java.lang.Exception e) {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String str = this.f22140e;
                        local.mo33255d(str, "constructor - e=" + e);
                    } catch (Throwable th) {
                        obtainStyledAttributes.recycle();
                        throw th;
                    }
                    obtainStyledAttributes.recycle();
                }
            }
        }
        mo40646a();
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40646a() {
        this.f22154s.setAntiAlias(true);
        this.f22154s.setStyle(android.graphics.Paint.Style.FILL);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40648a(android.graphics.Canvas canvas, android.graphics.RectF rectF, int i) {
        this.f22154s.setColor(i);
        this.f22154s.setXfermode(this.f22157v);
        canvas.drawRect(rectF, this.f22154s);
    }
}
