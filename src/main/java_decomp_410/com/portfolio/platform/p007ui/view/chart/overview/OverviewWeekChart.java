package com.portfolio.platform.p007ui.view.chart.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart */
public class OverviewWeekChart extends com.portfolio.platform.p007ui.view.chart.base.BarChart {

    @DexIgnore
    /* renamed from: u0 */
    public android.graphics.PointF f22158u0;

    @DexIgnore
    /* renamed from: v0 */
    public android.animation.ObjectAnimator f22159v0;

    @DexIgnore
    /* renamed from: w0 */
    public android.animation.ObjectAnimator f22160w0;

    @DexIgnore
    /* renamed from: x0 */
    public com.fossil.blesdk.obfuscated.wr2 f22161x0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart$a")
    /* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart$a */
    public static final class C6250a {
        @DexIgnore
        public C6250a() {
        }

        @DexIgnore
        public /* synthetic */ C6250a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart$b")
    /* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart$b */
    public static final class C6251b implements android.animation.Animator.AnimatorListener {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.view.chart.overview.OverviewWeekChart f22162a;

        @DexIgnore
        public C6251b(com.portfolio.platform.p007ui.view.chart.overview.OverviewWeekChart overviewWeekChart) {
            this.f22162a = overviewWeekChart;
        }

        @DexIgnore
        public void onAnimationCancel(android.animation.Animator animator) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f22162a.getTAG(), "changeModel - onAnimationCancel");
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tag = this.f22162a.getTAG();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("changeModel - onAnimationEnd -- isRunning=");
            android.animation.ObjectAnimator b = this.f22162a.f22159v0;
            sb.append(b != null ? java.lang.Boolean.valueOf(b.isRunning()) : null);
            local.mo33255d(tag, sb.toString());
            com.portfolio.platform.p007ui.view.chart.overview.OverviewWeekChart overviewWeekChart = this.f22162a;
            com.fossil.blesdk.obfuscated.wr2 c = overviewWeekChart.f22161x0;
            if (c != null) {
                overviewWeekChart.mo40456b(c);
                android.animation.ObjectAnimator a = this.f22162a.f22160w0;
                if (a != null) {
                    a.start();
                    return;
                }
                return;
            }
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        }

        @DexIgnore
        public void onAnimationRepeat(android.animation.Animator animator) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f22162a.getTAG(), "changeModel - onAnimationRepeat");
        }

        @DexIgnore
        public void onAnimationStart(android.animation.Animator animator) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f22162a.getTAG(), "changeModel - onAnimationStart");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart$c")
    /* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart$c */
    public static final class C6252c<T> implements java.util.Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return com.fossil.blesdk.obfuscated.wb4.m29575a(((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t).mo40563c(), ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t2).mo40563c());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart$d")
    /* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart$d */
    public static final class C6253d<T> implements java.util.Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return com.fossil.blesdk.obfuscated.wb4.m29575a(((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t).mo40563c(), ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t2).mo40563c());
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.view.chart.overview.OverviewWeekChart.C6250a((com.fossil.blesdk.obfuscated.fd4) null);
    }
    */

    @DexIgnore
    public OverviewWeekChart(android.content.Context context) {
        this(context, (android.util.AttributeSet) null);
    }

    @DexIgnore
    /* renamed from: e */
    public void mo40460e(android.graphics.Canvas canvas) {
        int i;
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        java.util.Iterator<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> it = getMChartModel().mo40570a().iterator();
        while (it.hasNext()) {
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a next = it.next();
            java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList = next.mo40552c().get(0);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList, "item.mListOfBarPoints[0]");
            java.util.Iterator it2 = com.fossil.blesdk.obfuscated.kb4.m24368a(arrayList, new com.portfolio.platform.p007ui.view.chart.overview.OverviewWeekChart.C6252c()).iterator();
            while (true) {
                if (it2.hasNext()) {
                    com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) it2.next();
                    if (bVar.mo40566e() != 0) {
                        android.graphics.Paint mGraphPaint = getMGraphPaint();
                        if (next.mo40549a() <= 0 || bVar.mo40566e() < next.mo40549a()) {
                            i = getMInActiveColor();
                        } else {
                            int i2 = com.fossil.blesdk.obfuscated.yr2.f20538a[bVar.mo40563c().ordinal()];
                            if (i2 == 1) {
                                i = getMLowestColor();
                            } else if (i2 == 2) {
                                i = getMDefaultColor();
                            } else if (i2 == 3) {
                                i = getMHighestColor();
                            } else {
                                throw new kotlin.NoWhenBranchMatchedException();
                            }
                        }
                        mGraphPaint.setColor(i);
                        canvas.drawRoundRect(bVar.mo40557a(), getMBarRadius(), getMBarRadius(), getMGraphPaint());
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: f */
    public final android.animation.ObjectAnimator mo40654f(int i, int i2, int i3, int i4) {
        android.animation.ObjectAnimator ofPropertyValuesHolder = android.animation.ObjectAnimator.ofPropertyValuesHolder(this, new android.animation.PropertyValuesHolder[]{android.animation.PropertyValuesHolder.ofInt("maxValue", new int[]{i, i4 * i}), android.animation.PropertyValuesHolder.ofInt("barAlpha", new int[]{i2, i3})});
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026s, outMaxValue, outAlpha)");
        ofPropertyValuesHolder.setDuration(200);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    /* renamed from: g */
    public void mo40464g(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        float width = (float) canvas.getWidth();
        if (getMGoalIconShow()) {
            canvas.drawPath(getMGoalLinePath(), getMGraphGoalLinePaint());
            int size = getMChartModel().mo40570a().size();
            if (size > 0) {
                android.graphics.Rect rect = new android.graphics.Rect();
                java.lang.String a = mo40652a(getMChartModel().mo40570a().get(size - 1).mo40549a());
                getMLegendPaint().getTextBounds(a, 0, a.length(), rect);
                getMLegendPaint().setColor(getMActiveColor());
                canvas.drawText(a, (width - getMGraphLegendMargin()) - ((float) rect.width()), this.f22158u0.y + getMGraphLegendMargin() + ((float) rect.height()), getMLegendPaint());
                getMLegendPaint().setColor(getMTextColor());
            }
        }
    }

    @DexIgnore
    /* renamed from: h */
    public void mo40502h(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        float width = (float) canvas.getWidth();
        java.util.Iterator<kotlin.Pair<java.lang.Integer, android.graphics.PointF>> it = getMGraphLegendPoint().iterator();
        while (it.hasNext()) {
            kotlin.Pair next = it.next();
            android.graphics.Rect rect = new android.graphics.Rect();
            java.lang.String a = mo40652a(((java.lang.Number) next.getFirst()).intValue());
            getMLegendPaint().getTextBounds(a, 0, a.length(), rect);
            float f = ((android.graphics.PointF) next.getSecond()).y;
            canvas.drawLine(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f, width, f, getMLegendLinePaint());
            canvas.drawText(a, (width - getMGraphLegendMargin()) - ((float) rect.width()), f + getMGraphLegendMargin() + ((float) rect.height()), getMLegendPaint());
        }
    }

    @DexIgnore
    /* renamed from: i */
    public void mo40503i(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
    }

    @DexIgnore
    public OverviewWeekChart(android.content.Context context, android.util.AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo40449a() {
        super.mo40449a();
        setMNumberBar(7);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo40455b(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        mo40457d();
        mo40459e();
        mo40502h(canvas);
        mo40460e(canvas);
    }

    @DexIgnore
    public OverviewWeekChart(android.content.Context context, android.util.AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewWeekChart(android.content.Context context, android.util.AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.f22158u0 = new android.graphics.PointF();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo40584a(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        super.mo40584a(canvas);
        java.util.Iterator<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> it = getMChartModel().mo40570a().iterator();
        while (it.hasNext()) {
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a next = it.next();
            java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList = next.mo40552c().get(0);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList, "item.mListOfBarPoints[0]");
            java.util.List a = com.fossil.blesdk.obfuscated.kb4.m24368a(arrayList, new com.portfolio.platform.p007ui.view.chart.overview.OverviewWeekChart.C6253d());
            if ((!a.isEmpty()) && next.mo40553d()) {
                android.graphics.Bitmap a2 = com.portfolio.platform.p007ui.view.chart.base.BarChart.m33049a((com.portfolio.platform.p007ui.view.chart.base.BarChart) this, getMLegendIconRes(), 0, 2, (java.lang.Object) null);
                if (a2 != null) {
                    android.graphics.RectF a3 = ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) a.get(0)).mo40557a();
                    canvas.drawBitmap(a2, a3.left + ((a3.width() - ((float) a2.getWidth())) * 0.5f), a3.bottom + ((float) getMTextMargin()), new android.graphics.Paint(1));
                    a2.recycle();
                }
            }
        }
        mo40462f(canvas);
    }

    @DexIgnore
    /* renamed from: b */
    public static /* synthetic */ android.animation.ObjectAnimator m33142b(com.portfolio.platform.p007ui.view.chart.overview.OverviewWeekChart overviewWeekChart, int i, int i2, int i3, int i4, int i5, java.lang.Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewWeekChart.mo40654f(i, i2, i3, i4);
        }
        throw new java.lang.UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createOutAnim");
    }

    @DexIgnore
    /* renamed from: e */
    public void mo40459e() {
        T t;
        java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> a = getMChartModel().mo40570a();
        android.graphics.RectF rectF = new android.graphics.RectF(getMBarMargin(), getMSafeAreaHeight(), ((float) getMGraphWidth()) - getMBarMarginEnd(), (float) getMGraphHeight());
        float height = rectF.height();
        float f = rectF.left;
        kotlin.jvm.internal.Ref$FloatRef ref$FloatRef = new kotlin.jvm.internal.Ref$FloatRef();
        kotlin.jvm.internal.Ref$FloatRef ref$FloatRef2 = new kotlin.jvm.internal.Ref$FloatRef();
        kotlin.jvm.internal.Ref$FloatRef ref$FloatRef3 = new kotlin.jvm.internal.Ref$FloatRef();
        java.util.Iterator<T> it = a.iterator();
        int i = 0;
        if (!it.hasNext()) {
            t = null;
        } else {
            t = it.next();
            if (it.hasNext()) {
                java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList = ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a) t).mo40552c().get(0);
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList, "it.mListOfBarPoints[0]");
                int i2 = 0;
                for (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b e : arrayList) {
                    i2 += e.mo40566e();
                }
                do {
                    T next = it.next();
                    java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList2 = ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a) next).mo40552c().get(0);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList2, "it.mListOfBarPoints[0]");
                    int i3 = 0;
                    for (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b e2 : arrayList2) {
                        i3 += e2.mo40566e();
                    }
                    if (i2 < i3) {
                        i2 = i3;
                        t = next;
                    }
                } while (it.hasNext());
            }
        }
        com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a aVar = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a) t;
        int size = a.size() - 1;
        java.util.Iterator<T> it2 = a.iterator();
        float f2 = f;
        int i4 = 0;
        boolean z = false;
        float f3 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f4 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (it2.hasNext()) {
            T next2 = it2.next();
            int i5 = i4 + 1;
            if (i4 >= 0) {
                com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a aVar2 = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a) next2;
                java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList3 = aVar2.mo40552c().get(i);
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList3, "item.mListOfBarPoints[0]");
                java.util.Iterator<T> it3 = it2;
                float a2 = (float) aVar2.mo40549a();
                java.lang.String str = "item.mListOfBarPoints[0]";
                ref$FloatRef2.element = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                ref$FloatRef.element = getMBarWidth() + f2;
                java.util.Iterator it4 = arrayList3.iterator();
                while (it4.hasNext()) {
                    java.util.Iterator it5 = it4;
                    ref$FloatRef2.element += (float) ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) it4.next()).mo40566e();
                    float mMaxValue = (ref$FloatRef2.element * height) / ((float) getMMaxValue());
                    if (mMaxValue != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && mMaxValue < getMBarRadius()) {
                        mMaxValue = getMBarRadius();
                    }
                    f4 = ((float) getMGraphHeight()) - mMaxValue;
                    it4 = it5;
                }
                setMGoalIconShow(true);
                float mGraphHeight = ((float) getMGraphHeight()) - ((a2 * height) / ((float) getMMaxValue()));
                ref$FloatRef3.element = ref$FloatRef.element + (getMBarSpace() * 0.5f);
                float f5 = ref$FloatRef3.element;
                float f6 = rectF.right;
                if (f5 > f6) {
                    ref$FloatRef3.element = f6 + (getMBarMargin() * 0.5f);
                }
                if (!z) {
                    getMGoalLinePath().moveTo(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, mGraphHeight);
                    getMGoalLinePath().lineTo(ref$FloatRef3.element, mGraphHeight);
                    z = true;
                } else {
                    getMGoalLinePath().lineTo(f3, mGraphHeight);
                    getMGoalLinePath().lineTo(ref$FloatRef3.element, mGraphHeight);
                    getMGoalIconPoint().set(ref$FloatRef3.element, mGraphHeight - (((float) getMGoalIconSize()) * 0.5f));
                }
                if (i4 == size) {
                    getMGoalLinePath().lineTo((float) getMGraphWidth(), mGraphHeight);
                    this.f22158u0 = new android.graphics.PointF((float) getMGraphWidth(), mGraphHeight);
                }
                f3 = ref$FloatRef3.element;
                if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) aVar, (java.lang.Object) aVar2)) {
                    java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList4 = aVar2.mo40552c().get(0);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList4, str);
                    int i6 = 0;
                    for (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b e3 : arrayList4) {
                        i6 += e3.mo40566e();
                    }
                    if (i6 >= aVar2.mo40549a()) {
                        getMStarIconPoint().add(new android.graphics.PointF(f2 - ((float) getMStarIconSize()), f4 - (((float) getMStarIconSize()) * 1.25f)));
                        float mGraphHeight2 = f4 + ((((float) getMGraphHeight()) - f4) * 0.5f);
                        getMStarIconPoint().add(new android.graphics.PointF(ref$FloatRef.element + (((float) getMStarIconSize()) * 0.5f), mGraphHeight2 - (((float) getMStarIconSize()) * 2.0f)));
                        getMStarIconPoint().add(new android.graphics.PointF(f2 - ((float) getMStarIconSize()), mGraphHeight2));
                    }
                }
                f2 = ref$FloatRef.element + getMBarSpace();
                i4 = i5;
                it2 = it3;
                i = 0;
            } else {
                com.fossil.blesdk.obfuscated.cb4.m20543c();
                throw null;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo40450a(float f, float f2) {
        android.graphics.Rect rect = new android.graphics.Rect();
        getMLegendPaint().getTextBounds("gh", 0, 2, rect);
        float mLegendHeight = ((float) (getMLegendHeight() + rect.height())) * 0.5f;
        int size = getMLegendTexts().size();
        float f3 = f;
        for (int i = 0; i < 7; i++) {
            if (i < size) {
                java.lang.String str = getMLegendTexts().get(i);
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str, "mLegendTexts[i]");
                java.lang.String str2 = str;
                getMLegendPaint().getTextBounds(str2, 0, str2.length(), rect);
                float mBarWidth = getMBarWidth() + f3;
                getMTextPoint().add(new kotlin.Pair(str2, new android.graphics.PointF(((f3 + mBarWidth) - ((float) rect.width())) * 0.5f, mLegendHeight)));
                f3 = mBarWidth + getMBarSpace();
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo40652a(int i) {
        float f = (float) i;
        java.lang.String valueOf = java.lang.String.valueOf((int) f);
        float f2 = (float) 1000;
        if (f < f2) {
            return valueOf;
        }
        return com.fossil.blesdk.obfuscated.il2.m23576a(f / f2, 1) + com.fossil.blesdk.obfuscated.sm2.m27795a(getContext(), (int) com.fossil.wearables.fossil.R.string.DashboardDiana_Main_StepsToday_Label__K);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0202, code lost:
        return;
     */
    @DexIgnore
    /* renamed from: a */
    public synchronized void mo40452a(com.fossil.blesdk.obfuscated.wr2 wr2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(wr2, com.facebook.devicerequests.internal.DeviceRequestsHelper.DEVICE_INFO_MODEL);
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String tag = getTAG();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("changeModel - model=");
        sb.append(wr2);
        sb.append(", mOutAnim.isRunning=");
        android.animation.ObjectAnimator objectAnimator = this.f22159v0;
        sb.append(objectAnimator != null ? java.lang.Boolean.valueOf(objectAnimator.isRunning()) : null);
        sb.append(", mInAnim.isRunning=");
        android.animation.ObjectAnimator objectAnimator2 = this.f22160w0;
        sb.append(objectAnimator2 != null ? java.lang.Boolean.valueOf(objectAnimator2.isRunning()) : null);
        local.mo33255d(tag, sb.toString());
        android.animation.ObjectAnimator objectAnimator3 = this.f22159v0;
        java.lang.Boolean valueOf = objectAnimator3 != null ? java.lang.Boolean.valueOf(objectAnimator3.isRunning()) : null;
        android.animation.ObjectAnimator objectAnimator4 = this.f22160w0;
        java.lang.Boolean valueOf2 = objectAnimator4 != null ? java.lang.Boolean.valueOf(objectAnimator4.isRunning()) : null;
        if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) valueOf, (java.lang.Object) true)) {
            if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) valueOf2, (java.lang.Object) true)) {
                if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) getMChartModel(), (java.lang.Object) wr2)) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(getTAG(), "changeModel - mChartModel == model");
                    return;
                }
                this.f22161x0 = wr2;
                this.f22159v0 = m33142b(this, getMMaxValue(), 255, 0, 0, 8, (java.lang.Object) null);
                com.fossil.blesdk.obfuscated.wr2 wr22 = this.f22161x0;
                if (wr22 != null) {
                    this.f22160w0 = m33140a(this, ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c) wr22).mo40575c(), 0, 255, 0, 8, (java.lang.Object) null);
                    android.animation.ObjectAnimator objectAnimator5 = this.f22159v0;
                    if (objectAnimator5 != null) {
                        objectAnimator5.addListener(new com.portfolio.platform.p007ui.view.chart.overview.OverviewWeekChart.C6251b(this));
                    }
                    android.animation.ObjectAnimator objectAnimator6 = this.f22159v0;
                    if (objectAnimator6 != null) {
                        objectAnimator6.start();
                    }
                } else {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                }
            }
        }
        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) wr2, (java.lang.Object) this.f22161x0)) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(getTAG(), "changeModel - model == mTempModel");
            return;
        }
        this.f22161x0 = wr2;
        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) valueOf, (java.lang.Object) true)) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(getTAG(), "changeModel - outRunning == true");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tag2 = getTAG();
            java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
            sb2.append("changeModel - outRunning == true - mMaxValue=");
            com.fossil.blesdk.obfuscated.wr2 wr23 = this.f22161x0;
            if (wr23 != null) {
                sb2.append(((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c) wr23).mo40575c());
                local2.mo33255d(tag2, sb2.toString());
                com.fossil.blesdk.obfuscated.wr2 wr24 = this.f22161x0;
                if (wr24 != null) {
                    this.f22160w0 = m33140a(this, ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c) wr24).mo40575c(), 0, 255, 0, 8, (java.lang.Object) null);
                } else {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                }
            } else {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
            }
        } else {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(getTAG(), "changeModel - inRunning == true");
            android.animation.ObjectAnimator objectAnimator7 = this.f22160w0;
            if (objectAnimator7 != null) {
                objectAnimator7.cancel();
            }
            int mMaxValue = getMMaxValue();
            int c = getMChartModel().mo40575c();
            int mBarAlpha = getMBarAlpha();
            if (c <= 0) {
                c = 1;
            }
            int i = mMaxValue / c;
            com.fossil.blesdk.obfuscated.wr2 wr25 = this.f22161x0;
            if (wr25 != null) {
                mo40456b(wr25);
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String tag3 = getTAG();
                local3.mo33255d(tag3, "changeModel - inRunning == true -- tempStartMaxValue=" + mMaxValue + ", " + "tempEndMaxValue=" + mMaxValue + ", tempAlpha=" + mMaxValue + ", tempMaxRate=" + mMaxValue + ", newMaxValue=" + getMChartModel().mo40575c());
                this.f22160w0 = mo40653e(getMChartModel().mo40575c(), mBarAlpha, 255, i);
                android.animation.ObjectAnimator objectAnimator8 = this.f22160w0;
                if (objectAnimator8 != null) {
                    objectAnimator8.start();
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static /* synthetic */ android.animation.ObjectAnimator m33140a(com.portfolio.platform.p007ui.view.chart.overview.OverviewWeekChart overviewWeekChart, int i, int i2, int i3, int i4, int i5, java.lang.Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewWeekChart.mo40653e(i, i2, i3, i4);
        }
        throw new java.lang.UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createInAnim");
    }

    @DexIgnore
    /* renamed from: e */
    public final android.animation.ObjectAnimator mo40653e(int i, int i2, int i3, int i4) {
        android.animation.ObjectAnimator ofPropertyValuesHolder = android.animation.ObjectAnimator.ofPropertyValuesHolder(this, new android.animation.PropertyValuesHolder[]{android.animation.PropertyValuesHolder.ofInt("maxValue", new int[]{i4 * i, i}), android.animation.PropertyValuesHolder.ofInt("barAlpha", new int[]{i2, i3})});
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026his, inMaxValue, inAlpha)");
        ofPropertyValuesHolder.setDuration(200);
        return ofPropertyValuesHolder;
    }
}
