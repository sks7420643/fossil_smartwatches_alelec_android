package com.portfolio.platform.p007ui.view.chart.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart */
public final class OverviewDayGoalChart extends com.portfolio.platform.p007ui.view.chart.overview.OverviewDayChart {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart$a")
    /* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart$a */
    public static final class C6247a<T> implements java.util.Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return com.fossil.blesdk.obfuscated.wb4.m29575a(((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t).mo40563c(), ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t2).mo40563c());
        }
    }

    @DexIgnore
    public OverviewDayGoalChart(android.content.Context context) {
        this(context, (android.util.AttributeSet) null);
    }

    @DexIgnore
    /* renamed from: e */
    public void mo40460e(android.graphics.Canvas canvas) {
        java.lang.Object obj;
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        getMGraphPaint().setColor(getMDefaultColor());
        java.util.Iterator<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> it = getMChartModel().mo40570a().iterator();
        while (it.hasNext()) {
            java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList = it.next().mo40552c().get(0);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList, "item.mListOfBarPoints[0]");
            java.util.Iterator it2 = com.fossil.blesdk.obfuscated.kb4.m24368a(arrayList, new com.portfolio.platform.p007ui.view.chart.overview.OverviewDayGoalChart.C6247a()).iterator();
            if (!it2.hasNext()) {
                obj = null;
            } else {
                obj = it2.next();
                if (it2.hasNext()) {
                    int e = ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) obj).mo40566e();
                    do {
                        java.lang.Object next = it2.next();
                        int e2 = ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next).mo40566e();
                        if (e < e2) {
                            obj = next;
                            e = e2;
                        }
                    } while (it2.hasNext());
                }
            }
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) obj;
            if (bVar != null) {
                canvas.drawRoundRect(bVar.mo40557a(), getMBarRadius(), getMBarRadius(), getMGraphPaint());
            }
        }
    }

    @DexIgnore
    public OverviewDayGoalChart(android.content.Context context, android.util.AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewDayGoalChart(android.content.Context context, android.util.AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewDayGoalChart(android.content.Context context, android.util.AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }
}
