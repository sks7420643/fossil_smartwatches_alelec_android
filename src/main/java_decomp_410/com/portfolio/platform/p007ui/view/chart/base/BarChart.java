package com.portfolio.platform.p007ui.view.chart.base;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.view.chart.base.BarChart */
public abstract class BarChart extends com.portfolio.platform.p007ui.view.chart.base.BaseChart {

    @DexIgnore
    /* renamed from: A */
    public int f22050A;

    @DexIgnore
    /* renamed from: B */
    public int f22051B;

    @DexIgnore
    /* renamed from: C */
    public android.graphics.PointF f22052C;

    @DexIgnore
    /* renamed from: D */
    public boolean f22053D;

    @DexIgnore
    /* renamed from: E */
    public int f22054E;

    @DexIgnore
    /* renamed from: F */
    public int f22055F;

    @DexIgnore
    /* renamed from: G */
    public java.util.ArrayList<android.graphics.PointF> f22056G;

    @DexIgnore
    /* renamed from: H */
    public boolean f22057H;

    @DexIgnore
    /* renamed from: I */
    public float f22058I;

    @DexIgnore
    /* renamed from: J */
    public int f22059J;

    @DexIgnore
    /* renamed from: K */
    public float f22060K;

    @DexIgnore
    /* renamed from: L */
    public float f22061L;

    @DexIgnore
    /* renamed from: M */
    public java.lang.String f22062M;

    @DexIgnore
    /* renamed from: N */
    public android.graphics.Path f22063N;

    @DexIgnore
    /* renamed from: O */
    public int f22064O;

    @DexIgnore
    /* renamed from: P */
    public float f22065P;

    @DexIgnore
    /* renamed from: Q */
    public java.util.ArrayList<java.lang.String> f22066Q;

    @DexIgnore
    /* renamed from: R */
    public int f22067R;

    @DexIgnore
    /* renamed from: S */
    public float f22068S;

    @DexIgnore
    /* renamed from: T */
    public int f22069T;

    @DexIgnore
    /* renamed from: U */
    public int f22070U;

    @DexIgnore
    /* renamed from: V */
    public android.graphics.Typeface f22071V;

    @DexIgnore
    /* renamed from: W */
    public java.util.ArrayList<kotlin.Pair<java.lang.String, android.graphics.PointF>> f22072W;

    @DexIgnore
    /* renamed from: a0 */
    public float f22073a0;

    @DexIgnore
    /* renamed from: b0 */
    public java.util.ArrayList<kotlin.Pair<java.lang.Integer, android.graphics.PointF>> f22074b0;

    @DexIgnore
    /* renamed from: c0 */
    public float f22075c0;

    @DexIgnore
    /* renamed from: d0 */
    public float f22076d0;

    @DexIgnore
    /* renamed from: e0 */
    public float f22077e0;

    @DexIgnore
    /* renamed from: f0 */
    public float f22078f0;

    @DexIgnore
    /* renamed from: g0 */
    public float f22079g0;

    @DexIgnore
    /* renamed from: h0 */
    public float f22080h0;

    @DexIgnore
    /* renamed from: i0 */
    public float f22081i0;

    @DexIgnore
    /* renamed from: j0 */
    public int f22082j0;

    @DexIgnore
    /* renamed from: k0 */
    public int f22083k0;

    @DexIgnore
    /* renamed from: l0 */
    public int f22084l0;

    @DexIgnore
    /* renamed from: m0 */
    public boolean f22085m0;

    @DexIgnore
    /* renamed from: n0 */
    public com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c f22086n0;

    @DexIgnore
    /* renamed from: o0 */
    public com.portfolio.platform.enums.GoalType f22087o0;

    @DexIgnore
    /* renamed from: p0 */
    public android.graphics.Paint f22088p0;

    @DexIgnore
    /* renamed from: q0 */
    public android.graphics.Paint f22089q0;

    @DexIgnore
    /* renamed from: r0 */
    public android.graphics.Paint f22090r0;

    @DexIgnore
    /* renamed from: s0 */
    public android.graphics.Paint f22091s0;

    @DexIgnore
    /* renamed from: t0 */
    public android.graphics.Paint f22092t0;

    @DexIgnore
    /* renamed from: u */
    public com.portfolio.platform.p007ui.view.chart.base.BarChart.C6234e f22093u;

    @DexIgnore
    /* renamed from: v */
    public int f22094v;

    @DexIgnore
    /* renamed from: w */
    public int f22095w;

    @DexIgnore
    /* renamed from: x */
    public int f22096x;

    @DexIgnore
    /* renamed from: y */
    public int f22097y;

    @DexIgnore
    /* renamed from: z */
    public int f22098z;

    @DexIgnore
    /* renamed from: com.portfolio.platform.ui.view.chart.base.BarChart$State */
    public enum State {
        LOWEST(0),
        DEFAULT(1),
        HIGHEST(2);
        
        @DexIgnore
        public static /* final */ com.portfolio.platform.p007ui.view.chart.base.BarChart.State.C6229a Companion; // = null;
        @DexIgnore
        public int mValue;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.base.BarChart$State$a")
        /* renamed from: com.portfolio.platform.ui.view.chart.base.BarChart$State$a */
        public static final class C6229a {
            @DexIgnore
            public C6229a() {
            }

            @DexIgnore
            public /* synthetic */ C6229a(com.fossil.blesdk.obfuscated.fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new com.portfolio.platform.p007ui.view.chart.base.BarChart.State.C6229a((com.fossil.blesdk.obfuscated.fd4) null);
        }
        */

        @DexIgnore
        public State(int i) {
            this.mValue = i;
        }

        @DexIgnore
        public final int getMValue() {
            return this.mValue;
        }

        @DexIgnore
        public final void setMValue(int i) {
            this.mValue = i;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.base.BarChart$a")
    /* renamed from: com.portfolio.platform.ui.view.chart.base.BarChart$a */
    public static final class C6230a {

        @DexIgnore
        /* renamed from: a */
        public int f22100a;

        @DexIgnore
        /* renamed from: b */
        public java.util.ArrayList<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> f22101b;

        @DexIgnore
        /* renamed from: c */
        public long f22102c;

        @DexIgnore
        /* renamed from: d */
        public boolean f22103d;

        @DexIgnore
        public C6230a() {
            this(0, (java.util.ArrayList) null, 0, false, 15, (com.fossil.blesdk.obfuscated.fd4) null);
        }

        @DexIgnore
        public C6230a(int i, java.util.ArrayList<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> arrayList, long j, boolean z) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(arrayList, "mListOfBarPoints");
            this.f22100a = i;
            this.f22101b = arrayList;
            this.f22102c = j;
            this.f22103d = z;
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo40549a() {
            return this.f22100a;
        }

        @DexIgnore
        /* renamed from: b */
        public final long mo40551b() {
            return this.f22102c;
        }

        @DexIgnore
        /* renamed from: c */
        public final java.util.ArrayList<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> mo40552c() {
            return this.f22101b;
        }

        @DexIgnore
        /* renamed from: d */
        public final boolean mo40553d() {
            return this.f22103d;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (!(obj instanceof com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a)) {
                return false;
            }
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a aVar = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a) obj;
            if (this.f22100a == aVar.f22100a && this.f22102c == aVar.f22102c && this.f22103d == aVar.f22103d && mo40550a(aVar.f22101b)) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((this.f22100a * 31) + this.f22101b.hashCode()) * 31) + java.lang.Long.valueOf(this.f22102c).hashCode()) * 31) + java.lang.Boolean.valueOf(this.f22103d).hashCode();
        }

        @DexIgnore
        public java.lang.String toString() {
            return "{goal=" + this.f22100a + ", index=" + this.f22102c + ", points=" + this.f22101b + '}';
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ C6230a(int i, java.util.ArrayList arrayList, long j, boolean z, int i2, com.fossil.blesdk.obfuscated.fd4 fd4) {
            this(r0, r1, (i2 & 4) != 0 ? -1 : j, (i2 & 8) == 0 ? z : r2);
            java.util.ArrayList arrayList2;
            int i3 = (i2 & 1) != 0 ? -1 : i;
            boolean z2 = false;
            if ((i2 & 2) != 0) {
                com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(0, (com.portfolio.platform.p007ui.view.chart.base.BarChart.State) null, 0, 0, (android.graphics.RectF) null, 23, (com.fossil.blesdk.obfuscated.fd4) null);
                arrayList2 = com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new java.util.ArrayList[]{com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b[]{bVar})});
            } else {
                arrayList2 = arrayList;
            }
        }

        @DexIgnore
        /* renamed from: a */
        public final boolean mo40550a(java.util.ArrayList<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> arrayList) {
            if (this.f22101b.size() != arrayList.size()) {
                return false;
            }
            int i = 0;
            for (T next : this.f22101b) {
                int i2 = i + 1;
                if (i >= 0) {
                    java.util.ArrayList arrayList2 = (java.util.ArrayList) next;
                    if (arrayList2.size() != arrayList.get(i).size()) {
                        return false;
                    }
                    int i3 = 0;
                    for (java.lang.Object next2 : arrayList2) {
                        int i4 = i3 + 1;
                        if (i3 < 0) {
                            com.fossil.blesdk.obfuscated.cb4.m20543c();
                            throw null;
                        } else if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2, (java.lang.Object) (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) arrayList.get(i).get(i3))) {
                            return false;
                        } else {
                            i3 = i4;
                        }
                    }
                    i = i2;
                } else {
                    com.fossil.blesdk.obfuscated.cb4.m20543c();
                    throw null;
                }
            }
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.base.BarChart$b")
    /* renamed from: com.portfolio.platform.ui.view.chart.base.BarChart$b */
    public static final class C6231b {

        @DexIgnore
        /* renamed from: a */
        public int f22104a;

        @DexIgnore
        /* renamed from: b */
        public com.portfolio.platform.p007ui.view.chart.base.BarChart.State f22105b;

        @DexIgnore
        /* renamed from: c */
        public int f22106c;

        @DexIgnore
        /* renamed from: d */
        public int f22107d;

        @DexIgnore
        /* renamed from: e */
        public android.graphics.RectF f22108e;

        @DexIgnore
        public C6231b() {
            this(0, (com.portfolio.platform.p007ui.view.chart.base.BarChart.State) null, 0, 0, (android.graphics.RectF) null, 31, (com.fossil.blesdk.obfuscated.fd4) null);
        }

        @DexIgnore
        public C6231b(int i, com.portfolio.platform.p007ui.view.chart.base.BarChart.State state, int i2, int i3, android.graphics.RectF rectF) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(state, "mState");
            com.fossil.blesdk.obfuscated.kd4.m24411b(rectF, "mBound");
            this.f22104a = i;
            this.f22105b = state;
            this.f22106c = i2;
            this.f22107d = i3;
            this.f22108e = rectF;
        }

        @DexIgnore
        /* renamed from: a */
        public final android.graphics.RectF mo40557a() {
            return this.f22108e;
        }

        @DexIgnore
        /* renamed from: b */
        public final int mo40561b() {
            return this.f22104a;
        }

        @DexIgnore
        /* renamed from: c */
        public final com.portfolio.platform.p007ui.view.chart.base.BarChart.State mo40563c() {
            return this.f22105b;
        }

        @DexIgnore
        /* renamed from: d */
        public final int mo40565d() {
            return this.f22106c;
        }

        @DexIgnore
        /* renamed from: e */
        public final int mo40566e() {
            return this.f22107d;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (!(obj instanceof com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b)) {
                return false;
            }
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) obj;
            if (this.f22107d == bVar.f22107d && this.f22106c == bVar.f22106c && this.f22104a == bVar.f22104a && this.f22105b.getMValue() == bVar.f22105b.getMValue()) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((((this.f22104a * 31) + this.f22105b.hashCode()) * 31) + this.f22106c) * 31) + this.f22107d) * 31) + this.f22108e.hashCode();
        }

        @DexIgnore
        public java.lang.String toString() {
            return "{index=" + this.f22104a + ", state=" + this.f22105b + ", mTotal=" + this.f22106c + ", value=" + this.f22107d + ", bound=" + this.f22108e + '}';
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ C6231b(int i, com.portfolio.platform.p007ui.view.chart.base.BarChart.State state, int i2, int i3, android.graphics.RectF rectF, int i4, com.fossil.blesdk.obfuscated.fd4 fd4) {
            this(i, r10, (i4 & 4) != 0 ? 0 : i2, (i4 & 8) == 0 ? i3 : r0, (i4 & 16) != 0 ? new android.graphics.RectF() : rectF);
            i = (i4 & 1) != 0 ? -1 : i;
            com.portfolio.platform.p007ui.view.chart.base.BarChart.State state2 = (i4 & 2) != 0 ? com.portfolio.platform.p007ui.view.chart.base.BarChart.State.DEFAULT : state;
            int i5 = 0;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo40559a(android.graphics.RectF rectF) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(rectF, "<set-?>");
            this.f22108e = rectF;
        }

        @DexIgnore
        /* renamed from: b */
        public final void mo40562b(int i) {
            this.f22106c = i;
        }

        @DexIgnore
        /* renamed from: c */
        public final void mo40564c(int i) {
            this.f22107d = i;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo40558a(int i) {
            this.f22104a = i;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo40560a(com.portfolio.platform.p007ui.view.chart.base.BarChart.State state) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(state, "<set-?>");
            this.f22105b = state;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.base.BarChart$c")
    /* renamed from: com.portfolio.platform.ui.view.chart.base.BarChart$c */
    public static final class C6232c extends com.fossil.blesdk.obfuscated.wr2 {

        @DexIgnore
        /* renamed from: b */
        public int f22109b;

        @DexIgnore
        /* renamed from: c */
        public int f22110c;

        @DexIgnore
        /* renamed from: d */
        public java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> f22111d;

        @DexIgnore
        public C6232c() {
            this(0, 0, (java.util.ArrayList) null, 7, (com.fossil.blesdk.obfuscated.fd4) null);
        }

        @DexIgnore
        public C6232c(int i, int i2, java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> arrayList) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(arrayList, "mData");
            this.f22109b = i;
            this.f22110c = i2;
            this.f22111d = arrayList;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> mo40570a() {
            return this.f22111d;
        }

        @DexIgnore
        /* renamed from: b */
        public final int mo40573b() {
            return this.f22110c;
        }

        @DexIgnore
        /* renamed from: c */
        public final int mo40575c() {
            return this.f22109b;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (!(obj instanceof com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c)) {
                return false;
            }
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c cVar = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c) obj;
            if (this.f22109b == cVar.f22109b && this.f22110c == cVar.f22110c && mo40572a(cVar.f22111d)) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((this.f22109b * 31) + this.f22110c) * 31) + this.f22111d.hashCode();
        }

        @DexIgnore
        public java.lang.String toString() {
            return "ChartModel:{max=" + this.f22109b + ", goal=" + this.f22110c + ", data=" + this.f22111d + '}';
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ C6232c(int i, int i2, java.util.ArrayList arrayList, int i3, com.fossil.blesdk.obfuscated.fd4 fd4) {
            this((i3 & 1) != 0 ? -1 : i, (i3 & 2) != 0 ? -1 : i2, (i3 & 4) != 0 ? new java.util.ArrayList() : arrayList);
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo40571a(int i) {
            this.f22110c = i;
        }

        @DexIgnore
        /* renamed from: b */
        public final void mo40574b(int i) {
            this.f22109b = i;
        }

        @DexIgnore
        /* renamed from: a */
        public final boolean mo40572a(java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> arrayList) {
            if (this.f22111d.size() != arrayList.size()) {
                return false;
            }
            int i = 0;
            for (T next : this.f22111d) {
                int i2 = i + 1;
                if (i < 0) {
                    com.fossil.blesdk.obfuscated.cb4.m20543c();
                    throw null;
                } else if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a) next, (java.lang.Object) arrayList.get(i))) {
                    return false;
                } else {
                    i = i2;
                }
            }
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.base.BarChart$d")
    /* renamed from: com.portfolio.platform.ui.view.chart.base.BarChart$d */
    public static final class C6233d {
        @DexIgnore
        public C6233d() {
        }

        @DexIgnore
        public /* synthetic */ C6233d(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    /* renamed from: com.portfolio.platform.ui.view.chart.base.BarChart$e */
    public interface C6234e {
        @DexIgnore
        /* renamed from: a */
        void mo40579a(long j);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.base.BarChart$f")
    /* renamed from: com.portfolio.platform.ui.view.chart.base.BarChart$f */
    public static final class C6235f<T> implements java.util.Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return com.fossil.blesdk.obfuscated.wb4.m29575a(((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t2).mo40563c(), ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t).mo40563c());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.base.BarChart$g")
    /* renamed from: com.portfolio.platform.ui.view.chart.base.BarChart$g */
    public static final class C6236g<T> implements java.util.Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return com.fossil.blesdk.obfuscated.wb4.m29575a(((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t2).mo40563c(), ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t).mo40563c());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.base.BarChart$h")
    /* renamed from: com.portfolio.platform.ui.view.chart.base.BarChart$h */
    public static final class C6237h<T> implements java.util.Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return com.fossil.blesdk.obfuscated.wb4.m29575a(((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t).mo40563c(), ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) t2).mo40563c());
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6233d((com.fossil.blesdk.obfuscated.fd4) null);
    }
    */

    @DexIgnore
    public BarChart(android.content.Context context) {
        this(context, (android.util.AttributeSet) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo40449a() {
        super.mo40449a();
        mo40463g();
        if (isInEditMode()) {
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.DEFAULT, 0, 40, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar2 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.LOWEST, 0, 40, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar3 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.HIGHEST, 0, 40, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a aVar = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a(200, com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new java.util.ArrayList[]{com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b[]{bVar, bVar2, bVar3})}), 0, false, 12, (com.fossil.blesdk.obfuscated.fd4) null);
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar4 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.DEFAULT, 0, 40, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar5 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.LOWEST, 0, 80, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar6 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.HIGHEST, 0, 50, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a aVar2 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a(200, com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new java.util.ArrayList[]{com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b[]{bVar4, bVar5, bVar6})}), 0, false, 12, (com.fossil.blesdk.obfuscated.fd4) null);
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar7 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.DEFAULT, 0, 60, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar8 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.LOWEST, 0, 100, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar9 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.HIGHEST, 0, 40, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a aVar3 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a(150, com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new java.util.ArrayList[]{com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b[]{bVar7, bVar8, bVar9})}), 0, false, 12, (com.fossil.blesdk.obfuscated.fd4) null);
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar10 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.DEFAULT, 0, 80, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar11 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.LOWEST, 0, 30, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar12 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.HIGHEST, 0, 50, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a aVar4 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a(150, com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new java.util.ArrayList[]{com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b[]{bVar10, bVar11, bVar12})}), 0, false, 12, (com.fossil.blesdk.obfuscated.fd4) null);
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar13 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.DEFAULT, 0, 40, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar14 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.LOWEST, 0, 90, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar15 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.HIGHEST, 0, 20, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a aVar5 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a(150, com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new java.util.ArrayList[]{com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b[]{bVar13, bVar14, bVar15})}), 0, false, 12, (com.fossil.blesdk.obfuscated.fd4) null);
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar16 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.DEFAULT, 0, 40, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar17 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.LOWEST, 0, 40, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar18 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.HIGHEST, 0, 40, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a aVar6 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a(com.fossil.blesdk.device.data.background.BackgroundImageConfig.BOTTOM_BACKGROUND_ANGLE, com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new java.util.ArrayList[]{com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b[]{bVar16, bVar17, bVar18})}), 0, false, 12, (com.fossil.blesdk.obfuscated.fd4) null);
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar19 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.DEFAULT, 0, 70, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar20 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.LOWEST, 0, 30, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar21 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b(-1, com.portfolio.platform.p007ui.view.chart.base.BarChart.State.HIGHEST, 0, 50, new android.graphics.RectF());
            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a aVar7 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a(com.fossil.blesdk.device.data.background.BackgroundImageConfig.BOTTOM_BACKGROUND_ANGLE, com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new java.util.ArrayList[]{com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b[]{bVar19, bVar20, bVar21})}), 0, false, 12, (com.fossil.blesdk.obfuscated.fd4) null);
            this.f22086n0 = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c(200, 200, com.fossil.blesdk.obfuscated.cb4.m20537a((T[]) new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a[]{aVar, aVar2, aVar3, aVar4, aVar5, aVar6, aVar7}));
            return;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo40455b(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        super.mo40455b(canvas);
        mo40457d();
        mo40459e();
        mo40462f(canvas);
        mo40460e(canvas);
        mo40502h(canvas);
    }

    @DexIgnore
    /* renamed from: d */
    public void mo40458d(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        super.mo40458d(canvas);
        mo40461f();
        mo40503i(canvas);
        mo40504j(canvas);
    }

    @DexIgnore
    /* renamed from: e */
    public void mo40460e(android.graphics.Canvas canvas) {
        int i;
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        java.util.Iterator<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> it = this.f22086n0.mo40570a().iterator();
        while (it.hasNext()) {
            java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList = it.next().mo40552c().get(0);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList, "item.mListOfBarPoints[0]");
            java.util.Iterator it2 = com.fossil.blesdk.obfuscated.kb4.m24368a(arrayList, new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6237h()).iterator();
            while (true) {
                if (it2.hasNext()) {
                    com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) it2.next();
                    if (bVar.mo40566e() != 0) {
                        android.graphics.Paint paint = this.f22088p0;
                        if (paint != null) {
                            int i2 = com.fossil.blesdk.obfuscated.vr2.f19507a[bVar.mo40563c().ordinal()];
                            if (i2 == 1) {
                                i = this.f22096x;
                            } else if (i2 == 2) {
                                i = this.f22097y;
                            } else if (i2 == 3) {
                                i = this.f22098z;
                            } else {
                                throw new kotlin.NoWhenBranchMatchedException();
                            }
                            paint.setColor(i);
                            float f = bVar.mo40557a().left;
                            float f2 = bVar.mo40557a().top;
                            float f3 = bVar.mo40557a().right;
                            float f4 = bVar.mo40557a().bottom;
                            float f5 = this.f22078f0;
                            android.graphics.Paint paint2 = this.f22088p0;
                            if (paint2 != null) {
                                com.fossil.blesdk.obfuscated.wr3.m29743a(canvas, f, f2, f3, f4, f5, f5, true, true, false, false, paint2);
                            } else {
                                com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphPaint");
                                throw null;
                            }
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphPaint");
                            throw null;
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: f */
    public final void mo40462f(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        mo40464g(canvas);
        mo40505k(canvas);
    }

    @DexIgnore
    /* renamed from: g */
    public final void mo40463g() {
        this.f22088p0 = new android.graphics.Paint(1);
        android.graphics.Paint paint = this.f22088p0;
        if (paint != null) {
            paint.setAntiAlias(true);
            android.graphics.Paint paint2 = this.f22088p0;
            if (paint2 != null) {
                paint2.setStyle(android.graphics.Paint.Style.FILL);
                android.graphics.Paint paint3 = this.f22088p0;
                if (paint3 != null) {
                    this.f22089q0 = new android.graphics.Paint(paint3);
                    android.graphics.Paint paint4 = this.f22089q0;
                    if (paint4 != null) {
                        paint4.setStrokeWidth(this.f22058I);
                        android.graphics.Paint paint5 = this.f22089q0;
                        if (paint5 != null) {
                            paint5.setColor(this.f22059J);
                            android.graphics.Paint paint6 = this.f22089q0;
                            if (paint6 != null) {
                                paint6.setStyle(android.graphics.Paint.Style.STROKE);
                                android.graphics.Paint paint7 = this.f22089q0;
                                if (paint7 != null) {
                                    paint7.setStrokeCap(android.graphics.Paint.Cap.ROUND);
                                    android.graphics.Paint paint8 = this.f22089q0;
                                    if (paint8 != null) {
                                        paint8.setStrokeJoin(android.graphics.Paint.Join.ROUND);
                                        android.graphics.Paint paint9 = this.f22089q0;
                                        if (paint9 != null) {
                                            paint9.setPathEffect(new android.graphics.DashPathEffect(new float[]{this.f22060K, this.f22061L}, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                                            this.f22090r0 = new android.graphics.Paint(1);
                                            android.graphics.Paint paint10 = this.f22090r0;
                                            if (paint10 != null) {
                                                paint10.setColorFilter(new android.graphics.PorterDuffColorFilter(this.f22094v, android.graphics.PorterDuff.Mode.SRC_IN));
                                                this.f22091s0 = new android.graphics.Paint(1);
                                                android.graphics.Paint paint11 = this.f22091s0;
                                                if (paint11 != null) {
                                                    paint11.setColor(this.f22069T);
                                                    android.graphics.Paint paint12 = this.f22091s0;
                                                    if (paint12 != null) {
                                                        paint12.setAntiAlias(true);
                                                        android.graphics.Paint paint13 = this.f22091s0;
                                                        if (paint13 != null) {
                                                            paint13.setStyle(android.graphics.Paint.Style.FILL);
                                                            android.graphics.Paint paint14 = this.f22091s0;
                                                            if (paint14 != null) {
                                                                paint14.setStrokeWidth(this.f22065P);
                                                                android.graphics.Paint paint15 = this.f22091s0;
                                                                if (paint15 != null) {
                                                                    paint15.setTextSize(this.f22068S);
                                                                    android.graphics.Typeface typeface = this.f22071V;
                                                                    if (typeface != null) {
                                                                        android.graphics.Paint paint16 = this.f22091s0;
                                                                        if (paint16 != null) {
                                                                            paint16.setTypeface(typeface);
                                                                        } else {
                                                                            com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendPaint");
                                                                            throw null;
                                                                        }
                                                                    }
                                                                    android.graphics.Paint paint17 = this.f22091s0;
                                                                    if (paint17 != null) {
                                                                        this.f22092t0 = new android.graphics.Paint(paint17);
                                                                        android.graphics.Paint paint18 = this.f22092t0;
                                                                        if (paint18 != null) {
                                                                            paint18.setColor(this.f22064O);
                                                                        } else {
                                                                            com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendLinePaint");
                                                                            throw null;
                                                                        }
                                                                    } else {
                                                                        com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendPaint");
                                                                        throw null;
                                                                    }
                                                                } else {
                                                                    com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendPaint");
                                                                    throw null;
                                                                }
                                                            } else {
                                                                com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendPaint");
                                                                throw null;
                                                            }
                                                        } else {
                                                            com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendPaint");
                                                            throw null;
                                                        }
                                                    } else {
                                                        com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendPaint");
                                                        throw null;
                                                    }
                                                } else {
                                                    com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendPaint");
                                                    throw null;
                                                }
                                            } else {
                                                com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphIconPaint");
                                                throw null;
                                            }
                                        } else {
                                            com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphGoalLinePaint");
                                            throw null;
                                        }
                                    } else {
                                        com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphGoalLinePaint");
                                        throw null;
                                    }
                                } else {
                                    com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphGoalLinePaint");
                                    throw null;
                                }
                            } else {
                                com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphGoalLinePaint");
                                throw null;
                            }
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphGoalLinePaint");
                            throw null;
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphGoalLinePaint");
                        throw null;
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphPaint");
                    throw null;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphPaint");
                throw null;
            }
        } else {
            com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphPaint");
            throw null;
        }
    }

    @DexIgnore
    public final int getMActiveColor() {
        return this.f22094v;
    }

    @DexIgnore
    public final int getMBarAlpha() {
        return this.f22084l0;
    }

    @DexIgnore
    public final float getMBarMargin() {
        return this.f22080h0;
    }

    @DexIgnore
    public final float getMBarMarginEnd() {
        return this.f22081i0;
    }

    @DexIgnore
    public final float getMBarRadius() {
        return this.f22078f0;
    }

    @DexIgnore
    public final float getMBarSpace() {
        return this.f22079g0;
    }

    @DexIgnore
    public final com.portfolio.platform.p007ui.view.chart.base.BarChart.C6234e getMBarTouchListener() {
        return this.f22093u;
    }

    @DexIgnore
    public final float getMBarWidth() {
        return this.f22077e0;
    }

    @DexIgnore
    public final com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c getMChartModel() {
        return this.f22086n0;
    }

    @DexIgnore
    public final int getMDefaultColor() {
        return this.f22097y;
    }

    @DexIgnore
    public final android.graphics.PointF getMGoalIconPoint() {
        return this.f22052C;
    }

    @DexIgnore
    public final boolean getMGoalIconShow() {
        return this.f22053D;
    }

    @DexIgnore
    public final int getMGoalIconSize() {
        return this.f22051B;
    }

    @DexIgnore
    public final android.graphics.Path getMGoalLinePath() {
        return this.f22063N;
    }

    @DexIgnore
    public final com.portfolio.platform.enums.GoalType getMGoalType() {
        return this.f22087o0;
    }

    @DexIgnore
    public final android.graphics.Paint getMGraphGoalLinePaint() {
        android.graphics.Paint paint = this.f22089q0;
        if (paint != null) {
            return paint;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphGoalLinePaint");
        throw null;
    }

    @DexIgnore
    public final float getMGraphLegendMargin() {
        return this.f22073a0;
    }

    @DexIgnore
    public final java.util.ArrayList<kotlin.Pair<java.lang.Integer, android.graphics.PointF>> getMGraphLegendPoint() {
        return this.f22074b0;
    }

    @DexIgnore
    public final android.graphics.Paint getMGraphPaint() {
        android.graphics.Paint paint = this.f22088p0;
        if (paint != null) {
            return paint;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphPaint");
        throw null;
    }

    @DexIgnore
    public final int getMHighestColor() {
        return this.f22098z;
    }

    @DexIgnore
    public final int getMInActiveColor() {
        return this.f22095w;
    }

    @DexIgnore
    public final boolean getMIsFlexibleSize() {
        return this.f22085m0;
    }

    @DexIgnore
    public final int getMLegendIconRes() {
        return this.f22067R;
    }

    @DexIgnore
    public final android.graphics.Paint getMLegendLinePaint() {
        android.graphics.Paint paint = this.f22092t0;
        if (paint != null) {
            return paint;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendLinePaint");
        throw null;
    }

    @DexIgnore
    public final android.graphics.Paint getMLegendPaint() {
        android.graphics.Paint paint = this.f22091s0;
        if (paint != null) {
            return paint;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendPaint");
        throw null;
    }

    @DexIgnore
    public final java.util.ArrayList<java.lang.String> getMLegendTexts() {
        return this.f22066Q;
    }

    @DexIgnore
    public final int getMLowestColor() {
        return this.f22096x;
    }

    @DexIgnore
    public final int getMMaxValue() {
        return this.f22083k0;
    }

    @DexIgnore
    public final int getMNumberBar() {
        return this.f22082j0;
    }

    @DexIgnore
    public final float getMSafeAreaHeight() {
        return this.f22075c0;
    }

    @DexIgnore
    public final java.util.ArrayList<android.graphics.PointF> getMStarIconPoint() {
        return this.f22056G;
    }

    @DexIgnore
    public final boolean getMStarIconShow() {
        return this.f22057H;
    }

    @DexIgnore
    public final int getMStarIconSize() {
        return this.f22055F;
    }

    @DexIgnore
    public final int getMTextColor() {
        return this.f22069T;
    }

    @DexIgnore
    public final int getMTextMargin() {
        return this.f22070U;
    }

    @DexIgnore
    public final java.util.ArrayList<kotlin.Pair<java.lang.String, android.graphics.PointF>> getMTextPoint() {
        return this.f22072W;
    }

    @DexIgnore
    public final float getMTextSize() {
        return this.f22068S;
    }

    @DexIgnore
    /* renamed from: h */
    public void mo40502h(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        java.util.Iterator<kotlin.Pair<java.lang.Integer, android.graphics.PointF>> it = this.f22074b0.iterator();
        while (it.hasNext()) {
            kotlin.Pair next = it.next();
            java.lang.String valueOf = java.lang.String.valueOf(((java.lang.Number) next.getFirst()).intValue());
            float f = ((android.graphics.PointF) next.getSecond()).x;
            float f2 = ((android.graphics.PointF) next.getSecond()).y;
            android.graphics.Paint paint = this.f22091s0;
            if (paint != null) {
                canvas.drawText(valueOf, f, f2, paint);
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendPaint");
                throw null;
            }
        }
    }

    @DexIgnore
    /* renamed from: i */
    public void mo40503i(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        float f = this.f22065P * 0.5f;
        float width = (float) canvas.getWidth();
        float f2 = this.f22065P * 0.5f;
        android.graphics.Paint paint = this.f22092t0;
        if (paint != null) {
            canvas.drawLine(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f, width, f2, paint);
        } else {
            com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendLinePaint");
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: j */
    public void mo40504j(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        java.util.Iterator<kotlin.Pair<java.lang.String, android.graphics.PointF>> it = this.f22072W.iterator();
        while (it.hasNext()) {
            kotlin.Pair next = it.next();
            java.lang.String str = (java.lang.String) next.getFirst();
            float f = ((android.graphics.PointF) next.getSecond()).x;
            float f2 = ((android.graphics.PointF) next.getSecond()).y;
            android.graphics.Paint paint = this.f22091s0;
            if (paint != null) {
                canvas.drawText(str, f, f2, paint);
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendPaint");
                throw null;
            }
        }
    }

    @DexIgnore
    /* renamed from: k */
    public void mo40505k(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        if (this.f22057H) {
            android.graphics.Bitmap a = mo40447a(this.f22054E, this.f22055F);
            if (a != null) {
                for (android.graphics.PointF pointF : this.f22056G) {
                    float f = pointF.x;
                    float f2 = pointF.y;
                    android.graphics.Paint paint = this.f22090r0;
                    if (paint != null) {
                        canvas.drawBitmap(a, f, f2, paint);
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphIconPaint");
                        throw null;
                    }
                }
                a.recycle();
            }
        }
    }

    @DexIgnore
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    @androidx.annotation.Keep
    public final void setBarAlpha(int i) {
        this.f22084l0 = i;
        android.graphics.Paint paint = this.f22088p0;
        if (paint != null) {
            paint.setAlpha(this.f22084l0);
            android.graphics.Paint paint2 = this.f22089q0;
            if (paint2 != null) {
                paint2.setAlpha(this.f22084l0);
                android.graphics.Paint paint3 = this.f22090r0;
                if (paint3 != null) {
                    paint3.setAlpha(this.f22084l0);
                    mo40587c();
                    return;
                }
                com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphIconPaint");
                throw null;
            }
            com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphGoalLinePaint");
            throw null;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphPaint");
        throw null;
    }

    @DexIgnore
    public final void setMActiveColor(int i) {
        this.f22094v = i;
    }

    @DexIgnore
    public final void setMBarAlpha(int i) {
        this.f22084l0 = i;
    }

    @DexIgnore
    public final void setMBarMargin(float f) {
        this.f22080h0 = f;
    }

    @DexIgnore
    public final void setMBarMarginEnd(float f) {
        this.f22081i0 = f;
    }

    @DexIgnore
    public final void setMBarRadius(float f) {
        this.f22078f0 = f;
    }

    @DexIgnore
    public final void setMBarSpace(float f) {
        this.f22079g0 = f;
    }

    @DexIgnore
    public final void setMBarTouchListener(com.portfolio.platform.p007ui.view.chart.base.BarChart.C6234e eVar) {
        this.f22093u = eVar;
    }

    @DexIgnore
    public final void setMBarWidth(float f) {
        this.f22077e0 = f;
    }

    @DexIgnore
    public final void setMChartModel(com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c cVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(cVar, "<set-?>");
        this.f22086n0 = cVar;
    }

    @DexIgnore
    public final void setMDefaultColor(int i) {
        this.f22097y = i;
    }

    @DexIgnore
    public final void setMGoalIconPoint(android.graphics.PointF pointF) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(pointF, "<set-?>");
        this.f22052C = pointF;
    }

    @DexIgnore
    public final void setMGoalIconShow(boolean z) {
        this.f22053D = z;
    }

    @DexIgnore
    public final void setMGoalIconSize(int i) {
        this.f22051B = i;
    }

    @DexIgnore
    public final void setMGoalLinePath(android.graphics.Path path) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(path, "<set-?>");
        this.f22063N = path;
    }

    @DexIgnore
    public final void setMGoalType(com.portfolio.platform.enums.GoalType goalType) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(goalType, "<set-?>");
        this.f22087o0 = goalType;
    }

    @DexIgnore
    public final void setMGraphGoalLinePaint(android.graphics.Paint paint) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(paint, "<set-?>");
        this.f22089q0 = paint;
    }

    @DexIgnore
    public final void setMGraphLegendMargin(float f) {
        this.f22073a0 = f;
    }

    @DexIgnore
    public final void setMGraphLegendPoint(java.util.ArrayList<kotlin.Pair<java.lang.Integer, android.graphics.PointF>> arrayList) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(arrayList, "<set-?>");
        this.f22074b0 = arrayList;
    }

    @DexIgnore
    public final void setMGraphPaint(android.graphics.Paint paint) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(paint, "<set-?>");
        this.f22088p0 = paint;
    }

    @DexIgnore
    public final void setMHighestColor(int i) {
        this.f22098z = i;
    }

    @DexIgnore
    public final void setMInActiveColor(int i) {
        this.f22095w = i;
    }

    @DexIgnore
    public final void setMIsFlexibleSize(boolean z) {
        this.f22085m0 = z;
    }

    @DexIgnore
    public final void setMLegendIconRes(int i) {
        this.f22067R = i;
    }

    @DexIgnore
    public final void setMLegendLinePaint(android.graphics.Paint paint) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(paint, "<set-?>");
        this.f22092t0 = paint;
    }

    @DexIgnore
    public final void setMLegendPaint(android.graphics.Paint paint) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(paint, "<set-?>");
        this.f22091s0 = paint;
    }

    @DexIgnore
    public final void setMLegendTexts(java.util.ArrayList<java.lang.String> arrayList) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(arrayList, "<set-?>");
        this.f22066Q = arrayList;
    }

    @DexIgnore
    public final void setMLowestColor(int i) {
        this.f22096x = i;
    }

    @DexIgnore
    public final void setMMaxValue(int i) {
        this.f22083k0 = i;
    }

    @DexIgnore
    public final void setMNumberBar(int i) {
        this.f22082j0 = i;
    }

    @DexIgnore
    public final void setMSafeAreaHeight(float f) {
        this.f22075c0 = f;
    }

    @DexIgnore
    public final void setMStarIconPoint(java.util.ArrayList<android.graphics.PointF> arrayList) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(arrayList, "<set-?>");
        this.f22056G = arrayList;
    }

    @DexIgnore
    public final void setMStarIconShow(boolean z) {
        this.f22057H = z;
    }

    @DexIgnore
    public final void setMStarIconSize(int i) {
        this.f22055F = i;
    }

    @DexIgnore
    public final void setMTextColor(int i) {
        this.f22069T = i;
    }

    @DexIgnore
    public final void setMTextMargin(int i) {
        this.f22070U = i;
    }

    @DexIgnore
    public final void setMTextPoint(java.util.ArrayList<kotlin.Pair<java.lang.String, android.graphics.PointF>> arrayList) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(arrayList, "<set-?>");
        this.f22072W = arrayList;
    }

    @DexIgnore
    public final void setMTextSize(float f) {
        this.f22068S = f;
    }

    @DexIgnore
    @androidx.annotation.Keep
    public final void setMaxValue(int i) {
        this.f22083k0 = i;
        mo40587c();
    }

    @DexIgnore
    public final void setOnTouchListener(com.portfolio.platform.p007ui.view.chart.base.BarChart.C6234e eVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(eVar, "barTouchListener");
        this.f22093u = eVar;
    }

    @DexIgnore
    public BarChart(android.content.Context context, android.util.AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public BarChart(android.content.Context context, android.util.AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    /* renamed from: f */
    public final void mo40461f() {
        android.graphics.RectF rectF = new android.graphics.RectF(this.f22080h0, this.f22075c0, ((float) getMGraphWidth()) - this.f22081i0, (float) getMGraphHeight());
        this.f22072W.clear();
        mo40450a(rectF.left, rectF.right);
    }

    @DexIgnore
    public BarChart(android.content.Context context, android.util.AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        java.lang.String[] strArr;
        this.f22050A = -1;
        this.f22051B = 10;
        this.f22052C = new android.graphics.PointF();
        this.f22054E = -1;
        this.f22055F = 10;
        this.f22056G = new java.util.ArrayList<>();
        this.f22058I = 2.0f;
        this.f22060K = 2.0f;
        this.f22061L = 2.0f;
        this.f22062M = "";
        this.f22063N = new android.graphics.Path();
        this.f22065P = 2.0f;
        this.f22066Q = new java.util.ArrayList<>();
        this.f22067R = -1;
        this.f22068S = 14.0f;
        this.f22070U = 10;
        this.f22072W = new java.util.ArrayList<>();
        this.f22074b0 = new java.util.ArrayList<>();
        this.f22075c0 = 50.0f;
        this.f22077e0 = 10.0f;
        this.f22078f0 = 5.0f;
        this.f22079g0 = 30.0f;
        this.f22080h0 = 30.0f;
        this.f22081i0 = 30.0f;
        this.f22082j0 = -1;
        this.f22084l0 = 255;
        com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c cVar = new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c(0, 0, (java.util.ArrayList) null, 7, (com.fossil.blesdk.obfuscated.fd4) null);
        this.f22086n0 = cVar;
        this.f22087o0 = com.portfolio.platform.enums.GoalType.ACTIVE_TIME;
        if (!(attributeSet == null || context == null)) {
            android.content.res.Resources.Theme theme = context.getTheme();
            if (theme != null) {
                android.content.res.TypedArray obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, com.fossil.blesdk.obfuscated.h62.BaseChart, 0, 0);
                if (obtainStyledAttributes != null) {
                    try {
                        this.f22094v = obtainStyledAttributes.getColor(0, 0);
                        this.f22095w = obtainStyledAttributes.getColor(13, 0);
                        this.f22096x = obtainStyledAttributes.getColor(19, 0);
                        this.f22097y = obtainStyledAttributes.getColor(1, 0);
                        this.f22098z = obtainStyledAttributes.getColor(12, 0);
                        this.f22050A = obtainStyledAttributes.getResourceId(4, 10);
                        this.f22051B = obtainStyledAttributes.getDimensionPixelSize(5, 10);
                        this.f22054E = obtainStyledAttributes.getResourceId(26, 10);
                        this.f22055F = obtainStyledAttributes.getDimensionPixelSize(27, 10);
                        this.f22058I = (float) obtainStyledAttributes.getDimensionPixelSize(9, 2);
                        this.f22059J = obtainStyledAttributes.getColor(6, 0);
                        this.f22060K = (float) obtainStyledAttributes.getDimensionPixelSize(8, 2);
                        this.f22061L = (float) obtainStyledAttributes.getDimensionPixelSize(7, 2);
                        java.lang.String string = obtainStyledAttributes.getString(10);
                        if (string == null) {
                            string = "";
                        }
                        this.f22062M = string;
                        this.f22068S = (float) obtainStyledAttributes.getDimensionPixelSize(30, 14);
                        this.f22069T = obtainStyledAttributes.getColor(28, 0);
                        this.f22070U = obtainStyledAttributes.getDimensionPixelSize(29, 10);
                        this.f22067R = obtainStyledAttributes.getResourceId(15, -1);
                        this.f22064O = obtainStyledAttributes.getColor(16, this.f22069T);
                        this.f22065P = (float) obtainStyledAttributes.getDimensionPixelSize(17, 2);
                        this.f22073a0 = (float) obtainStyledAttributes.getDimensionPixelSize(11, 0);
                        obtainStyledAttributes.getDimensionPixelSize(24, -1);
                        this.f22075c0 = (float) obtainStyledAttributes.getDimensionPixelSize(23, 50);
                        this.f22076d0 = (float) obtainStyledAttributes.getDimensionPixelSize(31, 0);
                        this.f22077e0 = (float) obtainStyledAttributes.getDimensionPixelSize(32, 10);
                        this.f22078f0 = (float) obtainStyledAttributes.getDimensionPixelSize(22, 5);
                        this.f22080h0 = (float) obtainStyledAttributes.getDimensionPixelSize(20, 30);
                        this.f22081i0 = (float) obtainStyledAttributes.getDimensionPixelSize(21, (int) this.f22080h0);
                        this.f22079g0 = (float) obtainStyledAttributes.getDimensionPixelSize(25, 30);
                        setMLegendHeight(obtainStyledAttributes.getDimensionPixelSize(14, 30));
                        this.f22085m0 = obtainStyledAttributes.getBoolean(2, false);
                        int resourceId = obtainStyledAttributes.getResourceId(3, -1);
                        if (resourceId != -1) {
                            this.f22071V = com.fossil.blesdk.obfuscated.C2785r6.m13071a(getContext(), resourceId);
                        }
                        int resourceId2 = obtainStyledAttributes.getResourceId(18, -1);
                        if (resourceId2 != -1) {
                            android.content.res.Resources resources = getResources();
                            if (resources != null) {
                                strArr = resources.getStringArray(resourceId2);
                                if (strArr != null) {
                                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) strArr, "(resources?.getStringArr\u2026         ?: emptyArray())");
                                    java.util.ArrayList<java.lang.String> arrayList = new java.util.ArrayList<>();
                                    com.fossil.blesdk.obfuscated.za4.m31306a(strArr, arrayList);
                                    this.f22066Q = arrayList;
                                }
                            }
                            strArr = new java.lang.String[0];
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) strArr, "(resources?.getStringArr\u2026         ?: emptyArray())");
                            java.util.ArrayList<java.lang.String> arrayList2 = new java.util.ArrayList<>();
                            com.fossil.blesdk.obfuscated.za4.m31306a(strArr, arrayList2);
                            this.f22066Q = arrayList2;
                        }
                    } catch (java.lang.Exception e) {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String tag = getTAG();
                        local.mo33255d(tag, "constructor - e=" + e);
                    } catch (Throwable th) {
                        obtainStyledAttributes.recycle();
                        throw th;
                    }
                    obtainStyledAttributes.recycle();
                }
            }
        }
        mo40449a();
    }

    @DexIgnore
    /* renamed from: d */
    public void mo40457d() {
        float f;
        java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> a = this.f22086n0.mo40570a();
        android.graphics.RectF rectF = new android.graphics.RectF(this.f22080h0, this.f22075c0, ((float) getMGraphWidth()) - this.f22081i0, (float) getMGraphHeight());
        float height = rectF.height();
        float width = rectF.width();
        if (this.f22085m0) {
            float f2 = this.f22079g0;
            int i = this.f22082j0;
            this.f22077e0 = (width - (f2 * ((float) (i - 1)))) / ((float) i);
        } else {
            this.f22079g0 = mo40446a(width);
        }
        float f3 = this.f22076d0;
        float f4 = this.f22079g0;
        if (f3 > f4 * 0.5f) {
            this.f22076d0 = f4 * 0.5f;
        }
        float f5 = rectF.left;
        float f6 = (float) 10;
        android.graphics.PointF pointF = new android.graphics.PointF(rectF.right + f6, rectF.top);
        android.graphics.PointF pointF2 = new android.graphics.PointF(rectF.right + f6, (rectF.top + rectF.bottom) * 0.5f);
        this.f22063N = new android.graphics.Path();
        this.f22052C = new android.graphics.PointF();
        int i2 = 0;
        this.f22053D = false;
        this.f22056G.clear();
        this.f22057H = false;
        this.f22074b0.clear();
        java.util.Iterator<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> it = a.iterator();
        boolean z = true;
        boolean z2 = true;
        float f7 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (it.hasNext()) {
            java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList = it.next().mo40552c().get(i2);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList, "item.mListOfBarPoints[0]");
            java.util.List<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> a2 = com.fossil.blesdk.obfuscated.kb4.m24368a(arrayList, new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6235f());
            float f8 = this.f22077e0 + f5;
            float f9 = f7;
            float f10 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            for (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar : a2) {
                f10 += (float) bVar.mo40566e();
                float f11 = (f10 * height) / ((float) this.f22083k0);
                if (f11 != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    f = this.f22078f0;
                    if (f11 < f) {
                        float mGraphHeight = ((float) getMGraphHeight()) - f;
                        bVar.mo40559a(new android.graphics.RectF(f5, mGraphHeight, f8, (float) getMGraphHeight()));
                        f9 = mGraphHeight;
                    }
                }
                f = f11;
                float mGraphHeight2 = ((float) getMGraphHeight()) - f;
                bVar.mo40559a(new android.graphics.RectF(f5, mGraphHeight2, f8, (float) getMGraphHeight()));
                f9 = mGraphHeight2;
            }
            if (f8 > pointF2.x && f9 < pointF2.y) {
                z = false;
            }
            if (f8 > pointF.x && f9 < pointF.y) {
                z2 = false;
            }
            f5 = f8 + this.f22079g0;
            f7 = f9;
            i2 = 0;
        }
        if (z) {
            this.f22074b0.add(new kotlin.Pair(java.lang.Integer.valueOf(this.f22083k0 / 2), pointF2));
        }
        if (z2) {
            this.f22074b0.add(new kotlin.Pair(java.lang.Integer.valueOf(this.f22083k0), pointF));
            return;
        }
        pointF.set(pointF.x, this.f22075c0 - 10.0f);
        this.f22074b0.add(new kotlin.Pair(java.lang.Integer.valueOf(this.f22083k0), pointF));
    }

    @DexIgnore
    /* renamed from: b */
    public void mo40456b(com.fossil.blesdk.obfuscated.wr2 wr2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(wr2, com.facebook.devicerequests.internal.DeviceRequestsHelper.DEVICE_INFO_MODEL);
        this.f22086n0 = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c) wr2;
        this.f22083k0 = this.f22086n0.mo40575c();
        if (this.f22082j0 == -1) {
            this.f22082j0 = this.f22086n0.mo40570a().size();
        }
    }

    @DexIgnore
    /* renamed from: e */
    public void mo40459e() {
        java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> a = this.f22086n0.mo40570a();
        int b = this.f22086n0.mo40573b();
        android.graphics.RectF rectF = new android.graphics.RectF(this.f22080h0, this.f22075c0, ((float) getMGraphWidth()) - this.f22081i0, (float) getMGraphHeight());
        float height = rectF.height();
        float f = rectF.left;
        java.util.Iterator<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> it = a.iterator();
        float f2 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        float f3 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (it.hasNext()) {
            java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> arrayList = it.next().mo40552c().get(0);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList, "item.mListOfBarPoints[0]");
            java.util.List<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b> a2 = com.fossil.blesdk.obfuscated.kb4.m24368a(arrayList, new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6236g());
            float f4 = this.f22077e0 + f;
            float f5 = f3;
            float f6 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            for (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar : a2) {
                f6 += (float) bVar.mo40566e();
                float f7 = (f6 * height) / ((float) this.f22083k0);
                if (f7 != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    float f8 = this.f22078f0;
                    if (f7 < f8) {
                        f7 = f8;
                    }
                }
                float mGraphHeight = ((float) getMGraphHeight()) - f7;
                bVar.mo40559a(new android.graphics.RectF(f, mGraphHeight, f4, (float) getMGraphHeight()));
                f5 = mGraphHeight;
            }
            f2 += f6;
            if (f2 >= ((float) b) && !z) {
                float f9 = (f + f4) * 0.5f;
                this.f22063N.moveTo(f9, this.f22075c0 - 10.0f);
                this.f22063N.lineTo(f9, f5);
                this.f22052C.set(f9 - (((float) this.f22051B) * 0.5f), (this.f22075c0 * 0.5f) - 20.0f);
                this.f22053D = true;
                z = true;
            }
            f = this.f22079g0 + f4;
            f3 = f5;
        }
    }

    @DexIgnore
    /* renamed from: g */
    public void mo40464g(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
        if (this.f22053D) {
            android.graphics.Path path = this.f22063N;
            android.graphics.Paint paint = this.f22089q0;
            if (paint != null) {
                canvas.drawPath(path, paint);
                android.graphics.Bitmap a = mo40447a(this.f22050A, this.f22051B);
                if (a != null) {
                    android.graphics.PointF pointF = this.f22052C;
                    float f = pointF.x;
                    float f2 = pointF.y;
                    android.graphics.Paint paint2 = this.f22090r0;
                    if (paint2 != null) {
                        canvas.drawBitmap(a, f, f2, paint2);
                        a.recycle();
                        return;
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphIconPaint");
                    throw null;
                }
                return;
            }
            com.fossil.blesdk.obfuscated.kd4.m24414d("mGraphGoalLinePaint");
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static /* synthetic */ android.graphics.Bitmap m33049a(com.portfolio.platform.p007ui.view.chart.base.BarChart barChart, int i, int i2, int i3, java.lang.Object obj) {
        if (obj == null) {
            if ((i3 & 2) != 0) {
                i2 = -1;
            }
            return barChart.mo40447a(i, i2);
        }
        throw new java.lang.UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createBitmapByRes");
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Bitmap mo40447a(int i, int i2) {
        int i3;
        if (i == -1) {
            return null;
        }
        try {
            android.graphics.Bitmap decodeResource = android.graphics.BitmapFactory.decodeResource(getResources(), i);
            if (decodeResource == null) {
                return decodeResource;
            }
            if (i2 == -1) {
                i2 = decodeResource.getWidth();
                i3 = decodeResource.getHeight();
            } else {
                i3 = i2;
            }
            return android.graphics.Bitmap.createScaledBitmap(decodeResource, i2, i3, false);
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tag = getTAG();
            local.mo33255d(tag, "createBitmapByRes - e=" + e);
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo40454a(android.view.MotionEvent motionEvent) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(motionEvent, com.misfit.frameworks.common.constants.Constants.EVENT);
        android.graphics.PointF pointF = new android.graphics.PointF(motionEvent.getX(), motionEvent.getY());
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String tag = getTAG();
        local.mo33255d(tag, "onGraphOverlayTouchEvent - pointF=" + pointF);
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                return false;
            }
            java.util.Iterator<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> it = this.f22086n0.mo40570a().iterator();
            while (it.hasNext()) {
                com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a next = it.next();
                java.util.Iterator<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> it2 = next.mo40552c().iterator();
                while (it2.hasNext()) {
                    java.util.ArrayList next2 = it2.next();
                    if (this.f22087o0 == com.portfolio.platform.enums.GoalType.TOTAL_SLEEP) {
                        if (com.portfolio.platform.p007ui.view.chart.base.BaseChart.f22112t.mo40624a(pointF, mo40448a(new android.graphics.RectF(((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(0)).mo40557a().left, ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(next2.size() - 1)).mo40557a().top, ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(0)).mo40557a().right, ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(0)).mo40557a().bottom), this.f22076d0))) {
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String tag2 = getTAG();
                            local2.mo33255d(tag2, "onGraphOverlayTouchEvent for total sleep - ACTION_UP = TRUE, index=" + next.mo40551b());
                            com.portfolio.platform.p007ui.view.chart.base.BarChart.C6234e eVar = this.f22093u;
                            if (eVar != null) {
                                eVar.mo40579a(next.mo40551b());
                            }
                        }
                    } else {
                        java.util.Iterator it3 = next2.iterator();
                        while (it3.hasNext()) {
                            if (com.portfolio.platform.p007ui.view.chart.base.BaseChart.f22112t.mo40624a(pointF, mo40448a(((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) it3.next()).mo40557a(), this.f22076d0))) {
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                java.lang.String tag3 = getTAG();
                                local3.mo33255d(tag3, "onGraphOverlayTouchEvent for normal chart item- ACTION_UP = TRUE, index=" + next.mo40551b());
                                com.portfolio.platform.p007ui.view.chart.base.BarChart.C6234e eVar2 = this.f22093u;
                                if (eVar2 != null) {
                                    eVar2.mo40579a(next.mo40551b());
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public float mo40446a(float f) {
        float f2 = this.f22077e0;
        int i = this.f22082j0;
        return (f - (f2 * ((float) i))) / ((float) (i - 1));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo40450a(float f, float f2) {
        android.graphics.Rect rect = new android.graphics.Rect();
        android.graphics.Paint paint = this.f22091s0;
        if (paint != null) {
            paint.getTextBounds("12 am", 0, 5, rect);
            float height = ((float) this.f22070U) + ((float) rect.height());
            this.f22072W.add(new kotlin.Pair("12 am", new android.graphics.PointF(f, height)));
            this.f22072W.add(new kotlin.Pair("12 am", new android.graphics.PointF(f2 - ((float) rect.width()), height)));
            android.graphics.Paint paint2 = this.f22091s0;
            if (paint2 != null) {
                paint2.getTextBounds("12 pm", 0, 5, rect);
                this.f22072W.add(new kotlin.Pair("12 pm", new android.graphics.PointF(((f + f2) - ((float) rect.width())) * 0.5f, height)));
                return;
            }
            com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendPaint");
            throw null;
        }
        com.fossil.blesdk.obfuscated.kd4.m24414d("mLegendPaint");
        throw null;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo40452a(com.fossil.blesdk.obfuscated.wr2 wr2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(wr2, com.facebook.devicerequests.internal.DeviceRequestsHelper.DEVICE_INFO_MODEL);
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String tag = getTAG();
        local.mo33255d(tag, "changeModel - model=" + wr2);
        if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.f22086n0, (java.lang.Object) wr2)) {
            mo40456b(wr2);
            mo40587c();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static /* synthetic */ void m33050a(com.portfolio.platform.p007ui.view.chart.base.BarChart barChart, int i, int i2, int i3, int i4, java.lang.String str, com.portfolio.platform.enums.GoalType goalType, int i5, java.lang.Object obj) {
        if (obj == null) {
            if ((i5 & 1) != 0) {
                i = -1;
            }
            if ((i5 & 2) != 0) {
                i2 = -1;
            }
            if ((i5 & 4) != 0) {
                i3 = -1;
            }
            if ((i5 & 8) != 0) {
                i4 = -1;
            }
            if ((i5 & 16) != 0) {
                str = null;
            }
            if ((i5 & 32) != 0) {
                goalType = com.portfolio.platform.enums.GoalType.ACTIVE_TIME;
            }
            barChart.mo40451a(i, i2, i3, i4, str, goalType);
            return;
        }
        throw new java.lang.UnsupportedOperationException("Super calls with default arguments not supported in this target, function: setDataRes");
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40451a(int i, int i2, int i3, int i4, java.lang.String str, com.portfolio.platform.enums.GoalType goalType) {
        java.lang.String str2;
        com.fossil.blesdk.obfuscated.kd4.m24411b(goalType, "goalType");
        if (i == -1) {
            i = this.f22094v;
        }
        this.f22094v = i;
        if (i2 == -1) {
            i2 = this.f22096x;
        }
        this.f22096x = i2;
        if (i3 == -1) {
            i3 = this.f22097y;
        }
        this.f22097y = i3;
        if (i4 == -1) {
            i4 = this.f22098z;
        }
        this.f22098z = i4;
        if (str != null) {
            str2 = str;
        } else {
            str2 = this.f22062M;
        }
        this.f22062M = str2;
        if (goalType == com.portfolio.platform.enums.GoalType.ACTIVE_TIME) {
            goalType = this.f22087o0;
        }
        this.f22087o0 = goalType;
        if (str == null) {
            str = this.f22062M;
        }
        this.f22062M = str;
    }

    @DexIgnore
    /* renamed from: a */
    public static /* synthetic */ void m33051a(com.portfolio.platform.p007ui.view.chart.base.BarChart barChart, java.util.ArrayList arrayList, boolean z, int i, java.lang.Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                z = false;
            }
            barChart.mo40453a((java.util.ArrayList<java.lang.String>) arrayList, z);
            return;
        }
        throw new java.lang.UnsupportedOperationException("Super calls with default arguments not supported in this target, function: setLegendTexts");
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40453a(java.util.ArrayList<java.lang.String> arrayList, boolean z) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(arrayList, "array");
        this.f22066Q.clear();
        this.f22066Q.addAll(arrayList);
        if (z) {
            getMLegend().invalidate();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final android.graphics.RectF mo40448a(android.graphics.RectF rectF, float f) {
        return new android.graphics.RectF(rectF.left - f, rectF.top - f, rectF.right + f, rectF.bottom + f);
    }
}
