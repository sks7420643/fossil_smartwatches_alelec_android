package com.portfolio.platform.p007ui.view.chart.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewSleepWeekChart */
public final class OverviewSleepWeekChart extends com.portfolio.platform.p007ui.view.chart.overview.OverviewWeekChart {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.overview.OverviewSleepWeekChart$a")
    /* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewSleepWeekChart$a */
    public static final class C6249a {
        @DexIgnore
        public C6249a() {
        }

        @DexIgnore
        public /* synthetic */ C6249a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.view.chart.overview.OverviewSleepWeekChart.C6249a((com.fossil.blesdk.obfuscated.fd4) null);
    }
    */

    @DexIgnore
    public OverviewSleepWeekChart(android.content.Context context) {
        this(context, (android.util.AttributeSet) null);
    }

    @DexIgnore
    /* renamed from: a */
    public float mo40446a(float f) {
        return ((f - (getMBarWidth() * ((float) getMNumberBar()))) - ((float) 20)) / ((float) (getMNumberBar() - 1));
    }

    @DexIgnore
    public OverviewSleepWeekChart(android.content.Context context, android.util.AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo40652a(int i) {
        com.fossil.blesdk.obfuscated.pd4 pd4 = com.fossil.blesdk.obfuscated.pd4.f17594a;
        java.lang.String a = com.fossil.blesdk.obfuscated.sm2.m27795a(getContext(), (int) com.fossil.wearables.fossil.R.string.DashboardDiana_Main_Sleep7days_Label__NumberHr);
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "LanguageHelper.getString\u2026eep7days_Label__NumberHr)");
        java.lang.Object[] objArr = {com.fossil.blesdk.obfuscated.il2.m23576a(((float) i) / ((float) 60), 1).toString()};
        java.lang.String format = java.lang.String.format(a, java.util.Arrays.copyOf(objArr, objArr.length));
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public OverviewSleepWeekChart(android.content.Context context, android.util.AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewSleepWeekChart(android.content.Context context, android.util.AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }
}
