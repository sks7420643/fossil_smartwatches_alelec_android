package com.portfolio.platform.p007ui.view.chart.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewSleepDayChart */
public final class OverviewSleepDayChart extends com.portfolio.platform.p007ui.view.chart.base.BarChart {

    @DexIgnore
    /* renamed from: u0 */
    public float f22136u0;

    @DexIgnore
    /* renamed from: v0 */
    public float f22137v0;

    @DexIgnore
    /* renamed from: w0 */
    public float f22138w0;

    @DexIgnore
    /* renamed from: x0 */
    public int f22139x0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.view.chart.overview.OverviewSleepDayChart$a")
    /* renamed from: com.portfolio.platform.ui.view.chart.overview.OverviewSleepDayChart$a */
    public static final class C6248a {
        @DexIgnore
        public C6248a() {
        }

        @DexIgnore
        public /* synthetic */ C6248a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.view.chart.overview.OverviewSleepDayChart.C6248a((com.fossil.blesdk.obfuscated.fd4) null);
    }
    */

    @DexIgnore
    public OverviewSleepDayChart(android.content.Context context) {
        this(context, (android.util.AttributeSet) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo40450a(float f, float f2) {
        if (getMChartModel().mo40570a().size() != 0) {
            java.util.ArrayList<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> c = getMChartModel().mo40570a().get(0).mo40552c();
            java.util.Iterator<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> it = c.iterator();
            while (it.hasNext()) {
                java.util.ArrayList next = it.next();
                java.lang.Object obj = next.get(0);
                com.fossil.blesdk.obfuscated.kd4.m24407a(obj, "session[0]");
                com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) obj;
                java.lang.Object obj2 = next.get(next.size() - 1);
                com.fossil.blesdk.obfuscated.kd4.m24407a(obj2, "session[session.size - 1]");
                com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar2 = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) obj2;
                long j = (long) 1000;
                long e = ((long) bVar.mo40566e()) * j;
                long d = ((((long) bVar2.mo40565d()) - 1) * ((long) 60) * j) + e;
                android.graphics.Rect rect = new android.graphics.Rect();
                int i = this.f22139x0;
                java.lang.String a = i != -1 ? com.fossil.blesdk.obfuscated.rk2.m27357a(e, i) : com.fossil.blesdk.obfuscated.rk2.m27374b(e);
                int i2 = this.f22139x0;
                java.lang.String a2 = i2 != -1 ? com.fossil.blesdk.obfuscated.rk2.m27357a(d, i2) : com.fossil.blesdk.obfuscated.rk2.m27374b(d);
                com.fossil.blesdk.obfuscated.ll2 ll2 = com.fossil.blesdk.obfuscated.ll2.f16535b;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "startTimeString");
                java.lang.String b = ll2.mo29220b(a);
                com.fossil.blesdk.obfuscated.ll2 ll22 = com.fossil.blesdk.obfuscated.ll2.f16535b;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a2, "endTimeString");
                java.lang.String b2 = ll22.mo29220b(a2);
                getMLegendPaint().getTextBounds(b, 0, kotlin.text.StringsKt__StringsKt.m37088c(b), rect);
                float mTextMargin = ((float) getMTextMargin()) + ((float) rect.height());
                if (bVar2.mo40557a().right - bVar.mo40557a().left > ((float) (rect.right * 2))) {
                    getMTextPoint().add(new kotlin.Pair(b, new android.graphics.PointF(bVar.mo40557a().left, mTextMargin)));
                    getMTextPoint().add(new kotlin.Pair(b2, new android.graphics.PointF((bVar2.mo40557a().right - f) - ((float) rect.right), mTextMargin)));
                } else if (c.indexOf(next) == c.size() - 1) {
                    getMTextPoint().add(new kotlin.Pair(b2, new android.graphics.PointF((bVar2.mo40557a().right - f) - ((float) rect.right), mTextMargin)));
                } else {
                    getMTextPoint().add(new kotlin.Pair(b, new android.graphics.PointF(bVar.mo40557a().left, mTextMargin)));
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo40457d() {
        float f;
        float f2;
        float f3;
        float f4;
        float f5;
        float f6;
        float f7;
        float f8;
        float f9;
        float f10;
        java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> a = getMChartModel().mo40570a();
        if (a.size() != 0) {
            setMGoalLinePath(new android.graphics.Path());
            setMGoalIconPoint(new android.graphics.PointF());
            setMGoalIconShow(false);
            android.graphics.RectF rectF = new android.graphics.RectF(getMBarMargin(), getMSafeAreaHeight(), ((float) getMGraphWidth()) - getMBarMargin(), (float) getMGraphHeight());
            getMGraphHeight();
            float f11 = rectF.left;
            float f12 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            java.util.ArrayList<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> c = a.get(0).mo40552c();
            java.util.Iterator<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> it = c.iterator();
            int i = 0;
            while (it.hasNext()) {
                java.util.ArrayList next = it.next();
                i += ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next.get(0)).mo40565d();
                if (c.indexOf(next) != 0) {
                    i += 10;
                }
            }
            float f13 = (rectF.right - rectF.left) / ((float) i);
            java.util.Iterator<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> it2 = c.iterator();
            while (it2.hasNext()) {
                java.util.ArrayList next2 = it2.next();
                float mGraphHeight = ((float) getMGraphHeight()) * this.f22136u0;
                int size = next2.size() - 1;
                if (size >= 0) {
                    float f14 = f12;
                    float f15 = f11;
                    int i2 = 0;
                    while (true) {
                        if (i2 < next2.size() - 1) {
                            f14 = (((float) (((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(i2 + 1)).mo40561b() - ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(i2)).mo40561b())) * f13) + f15;
                            int i3 = com.fossil.blesdk.obfuscated.xr2.f20322b[((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(i2)).mo40563c().ordinal()];
                            if (i3 != 1) {
                                if (i3 == 2) {
                                    f10 = (float) getMGraphHeight();
                                    f9 = (float) getMGraphHeight();
                                    f8 = this.f22137v0;
                                } else if (i3 == 3) {
                                    f10 = (float) getMGraphHeight();
                                    f9 = (float) getMGraphHeight();
                                    f8 = this.f22138w0;
                                } else {
                                    throw new kotlin.NoWhenBranchMatchedException();
                                }
                                f6 = (f10 - (f9 * f8)) - mGraphHeight;
                            } else {
                                f6 = ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.f22136u0);
                            }
                            int i4 = com.fossil.blesdk.obfuscated.xr2.f20323c[((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(i2)).mo40563c().ordinal()];
                            if (i4 == 1) {
                                f7 = (float) getMGraphHeight();
                            } else if (i4 == 2 || i4 == 3) {
                                f7 = ((float) getMGraphHeight()) - mGraphHeight;
                            } else {
                                throw new kotlin.NoWhenBranchMatchedException();
                            }
                            ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(i2)).mo40559a(new android.graphics.RectF(f15, f6, f14, f7));
                            f15 = f14;
                        } else if (i2 == next2.size() - 1) {
                            f14 = (((float) (((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(i2)).mo40565d() - ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(i2)).mo40561b())) * f13) + f15;
                            int i5 = com.fossil.blesdk.obfuscated.xr2.f20324d[((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(i2)).mo40563c().ordinal()];
                            if (i5 != 1) {
                                if (i5 == 2) {
                                    f5 = (float) getMGraphHeight();
                                    f4 = (float) getMGraphHeight();
                                    f3 = this.f22137v0;
                                } else if (i5 == 3) {
                                    f5 = (float) getMGraphHeight();
                                    f4 = (float) getMGraphHeight();
                                    f3 = this.f22138w0;
                                } else {
                                    throw new kotlin.NoWhenBranchMatchedException();
                                }
                                f = (f5 - (f4 * f3)) - mGraphHeight;
                            } else {
                                f = ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.f22136u0);
                            }
                            int i6 = com.fossil.blesdk.obfuscated.xr2.f20325e[((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(i2)).mo40563c().ordinal()];
                            if (i6 == 1) {
                                f2 = (float) getMGraphHeight();
                            } else if (i6 == 2 || i6 == 3) {
                                f2 = ((float) getMGraphHeight()) - mGraphHeight;
                            } else {
                                throw new kotlin.NoWhenBranchMatchedException();
                            }
                            ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(next2.size() - 1)).mo40559a(new android.graphics.RectF(f15, f, f14, f2));
                        }
                        if (i2 == size) {
                            f12 = f14;
                            break;
                        }
                        i2++;
                    }
                }
                f11 = f12 + (((float) 10) * f13);
                f12 = f11;
            }
        }
    }

    @DexIgnore
    /* renamed from: e */
    public void mo40460e(android.graphics.Canvas canvas) {
        android.graphics.Canvas canvas2 = canvas;
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas2, "canvas");
        canvas2.drawRect(new android.graphics.RectF(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.f22136u0), (float) getMGraphWidth(), (((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.f22136u0)) + ((float) 2)), getMLegendLinePaint());
        java.util.Iterator<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> it = getMChartModel().mo40570a().iterator();
        while (it.hasNext()) {
            java.util.Iterator<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> it2 = it.next().mo40552c().iterator();
            while (it2.hasNext()) {
                java.util.Iterator it3 = it2.next().iterator();
                while (it3.hasNext()) {
                    com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) it3.next();
                    int i = com.fossil.blesdk.obfuscated.xr2.f20321a[bVar.mo40563c().ordinal()];
                    if (i == 1) {
                        getMGraphPaint().setColor(getMLowestColor());
                        com.fossil.blesdk.obfuscated.wr3.m29743a(canvas, bVar.mo40557a().left, bVar.mo40557a().top, bVar.mo40557a().right, bVar.mo40557a().bottom, getMBarRadius(), getMBarRadius(), false, false, true, true, getMGraphPaint());
                    } else if (i == 2) {
                        getMGraphPaint().setColor(getMDefaultColor());
                        com.fossil.blesdk.obfuscated.wr3.m29743a(canvas, bVar.mo40557a().left, bVar.mo40557a().top, bVar.mo40557a().right, bVar.mo40557a().bottom, getMBarRadius(), getMBarRadius(), true, true, false, false, getMGraphPaint());
                    } else if (i == 3) {
                        getMGraphPaint().setColor(getMHighestColor());
                        com.fossil.blesdk.obfuscated.wr3.m29743a(canvas, bVar.mo40557a().left, bVar.mo40557a().top, bVar.mo40557a().right, bVar.mo40557a().bottom, getMBarRadius(), getMBarRadius(), true, true, false, false, getMGraphPaint());
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: i */
    public void mo40503i(android.graphics.Canvas canvas) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(canvas, "canvas");
    }

    @DexIgnore
    public final void setTimeZoneOffsetInSecond(int i) {
        this.f22139x0 = i;
    }

    @DexIgnore
    public OverviewSleepDayChart(android.content.Context context, android.util.AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewSleepDayChart(android.content.Context context, android.util.AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewSleepDayChart(android.content.Context context, android.util.AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.f22139x0 = -1;
        if (attributeSet != null && context != null) {
            android.content.res.Resources.Theme theme = context.getTheme();
            if (theme != null) {
                android.content.res.TypedArray obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, com.fossil.blesdk.obfuscated.h62.OverviewSleepDayChart, 0, 0);
                if (obtainStyledAttributes != null) {
                    try {
                        this.f22136u0 = obtainStyledAttributes.getFloat(0, 0.1f);
                        this.f22137v0 = obtainStyledAttributes.getFloat(2, 0.33f);
                        this.f22138w0 = obtainStyledAttributes.getFloat(1, 0.6f);
                    } catch (java.lang.Exception e) {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String tag = getTAG();
                        local.mo33255d(tag, "constructor - e=" + e);
                    } catch (Throwable th) {
                        obtainStyledAttributes.recycle();
                        throw th;
                    }
                    obtainStyledAttributes.recycle();
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: e */
    public void mo40459e() {
        if (getMGoalType() == com.portfolio.platform.enums.GoalType.TOTAL_SLEEP && getMChartModel().mo40570a().size() != 0) {
            android.graphics.RectF rectF = new android.graphics.RectF(getMBarMargin(), getMSafeAreaHeight(), ((float) getMGraphWidth()) - getMBarMargin(), (float) getMGraphHeight());
            java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6230a> a = getMChartModel().mo40570a();
            int b = getMChartModel().mo40573b();
            java.util.ArrayList<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> c = a.get(0).mo40552c();
            java.util.Iterator<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> it = c.iterator();
            int i = 0;
            while (it.hasNext()) {
                java.util.ArrayList next = it.next();
                i += ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next.get(0)).mo40565d();
                if (c.indexOf(next) != 0) {
                    i += 10;
                    b += 10;
                }
            }
            float f = (((float) b) * (rectF.right - rectF.left)) / ((float) i);
            java.util.Iterator<java.util.ArrayList<com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b>> it2 = c.iterator();
            while (it2.hasNext()) {
                java.util.ArrayList next2 = it2.next();
                if (f <= ((com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) next2.get(next2.size() - 1)).mo40557a().right) {
                    java.util.Iterator it3 = next2.iterator();
                    while (it3.hasNext()) {
                        com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b bVar = (com.portfolio.platform.p007ui.view.chart.base.BarChart.C6231b) it3.next();
                        if (f >= bVar.mo40557a().left && f <= bVar.mo40557a().right) {
                            getMGoalLinePath().moveTo(f, getMSafeAreaHeight() - 10.0f);
                            getMGoalLinePath().lineTo(f, bVar.mo40557a().top);
                            getMGoalIconPoint().set(f - (((float) getMGoalIconSize()) * 0.5f), (getMSafeAreaHeight() * 0.5f) - 20.0f);
                            setMGoalIconShow(false);
                            return;
                        }
                    }
                    continue;
                }
            }
        }
    }
}
