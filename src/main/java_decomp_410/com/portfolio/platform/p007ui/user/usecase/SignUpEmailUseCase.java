package com.portfolio.platform.p007ui.user.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase */
public final class SignUpEmailUseCase extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase.C6221a, com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase.C6223c, com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase.C6222b> {

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.UserRepository f22024d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.en2 f22025e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase$a")
    /* renamed from: com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase$a */
    public static final class C6221a implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.portfolio.platform.data.SignUpEmailAuth f22026a;

        @DexIgnore
        public C6221a(com.portfolio.platform.data.SignUpEmailAuth signUpEmailAuth) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(signUpEmailAuth, "emailAuth");
            this.f22026a = signUpEmailAuth;
        }

        @DexIgnore
        /* renamed from: a */
        public final com.portfolio.platform.data.SignUpEmailAuth mo40428a() {
            return this.f22026a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase$b")
    /* renamed from: com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase$b */
    public static final class C6222b implements com.portfolio.platform.CoroutineUseCase.C5602a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f22027a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.String f22028b;

        @DexIgnore
        public C6222b(int i, java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "errorMesagge");
            this.f22027a = i;
            this.f22028b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo40429a() {
            return this.f22027a;
        }

        @DexIgnore
        /* renamed from: b */
        public final java.lang.String mo40430b() {
            return this.f22028b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase$c")
    /* renamed from: com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase$c */
    public static final class C6223c implements com.portfolio.platform.CoroutineUseCase.C5605d {
    }

    @DexIgnore
    public SignUpEmailUseCase(com.portfolio.platform.data.source.UserRepository userRepository, com.fossil.blesdk.obfuscated.en2 en2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(userRepository, "mUserRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(en2, "mSharedPreferencesManager");
        this.f22024d = userRepository;
        this.f22025e = en2;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return "SignUpEmailUseCase";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00de, code lost:
        if (r9 != null) goto L_0x00e2;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase.C6221a aVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase$run$1 signUpEmailUseCase$run$1;
        int i;
        com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase signUpEmailUseCase;
        com.fossil.blesdk.obfuscated.qo2 qo2;
        java.lang.String str;
        if (yb4 instanceof com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase$run$1) {
            signUpEmailUseCase$run$1 = (com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase$run$1) yb4;
            int i2 = signUpEmailUseCase$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                signUpEmailUseCase$run$1.label = i2 - Integer.MIN_VALUE;
                java.lang.Object obj = signUpEmailUseCase$run$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = signUpEmailUseCase$run$1.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("SignUpEmailUseCase", "running UseCase");
                    if (aVar == null) {
                        return new com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase.C6222b(600, "");
                    }
                    if (this.f22024d.getCurrentUser() != null) {
                        return new com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase.C6223c();
                    }
                    com.portfolio.platform.data.source.UserRepository userRepository = this.f22024d;
                    com.portfolio.platform.data.SignUpEmailAuth a2 = aVar.mo40428a();
                    signUpEmailUseCase$run$1.L$0 = this;
                    signUpEmailUseCase$run$1.L$1 = aVar;
                    signUpEmailUseCase$run$1.label = 1;
                    obj = userRepository.signUpEmail(a2, signUpEmailUseCase$run$1);
                    if (obj == a) {
                        return a;
                    }
                    signUpEmailUseCase = this;
                } else if (i == 1) {
                    com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase.C6221a aVar2 = (com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase.C6221a) signUpEmailUseCase$run$1.L$1;
                    signUpEmailUseCase = (com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase) signUpEmailUseCase$run$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
                java.lang.String str2 = null;
                if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("SignUpEmailUseCase", "signUpEmail success");
                    com.fossil.blesdk.obfuscated.en2 en2 = signUpEmailUseCase.f22025e;
                    com.portfolio.platform.data.Auth auth = (com.portfolio.platform.data.Auth) ((com.fossil.blesdk.obfuscated.ro2) qo2).mo30673a();
                    if (auth != null) {
                        str2 = auth.getAccessToken();
                    }
                    en2.mo27088w(str2);
                    return new com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase.C6223c();
                } else if (!(qo2 instanceof com.fossil.blesdk.obfuscated.po2)) {
                    return new com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase.C6222b(600, "");
                } else {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.StringBuilder sb = new java.lang.StringBuilder();
                    sb.append("signUpEmail failed code=");
                    com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
                    com.portfolio.platform.data.model.ServerError c = po2.mo30178c();
                    if (c != null) {
                        str2 = c.getMessage();
                    }
                    sb.append(str2);
                    local.mo33255d("SignUpEmailUseCase", sb.toString());
                    int a3 = po2.mo30176a();
                    com.portfolio.platform.data.model.ServerError c2 = po2.mo30178c();
                    if (c2 != null) {
                        str = c2.getMessage();
                    }
                    str = "";
                    return new com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase.C6222b(a3, str);
                }
            }
        }
        signUpEmailUseCase$run$1 = new com.portfolio.platform.p007ui.user.usecase.SignUpEmailUseCase$run$1(this, yb4);
        java.lang.Object obj2 = signUpEmailUseCase$run$1.result;
        java.lang.Object a4 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = signUpEmailUseCase$run$1.label;
        if (i != 0) {
        }
        qo2 = (com.fossil.blesdk.obfuscated.qo2) obj2;
        java.lang.String str22 = null;
        if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
        }
    }
}
