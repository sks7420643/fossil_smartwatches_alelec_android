package com.portfolio.platform.p007ui.user.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase */
public final class DeleteLogoutUserUseCase extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6201b, com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6203d, com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6202c> {

    @DexIgnore
    /* renamed from: H */
    public static /* final */ java.lang.String f21966H;

    @DexIgnore
    /* renamed from: I */
    public static /* final */ com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6200a f21967I; // = new com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6200a((com.fossil.blesdk.obfuscated.fd4) null);

    @DexIgnore
    /* renamed from: A */
    public /* final */ com.portfolio.platform.data.source.FitnessDataRepository f21968A;

    @DexIgnore
    /* renamed from: B */
    public /* final */ com.portfolio.platform.data.source.HeartRateSampleRepository f21969B;

    @DexIgnore
    /* renamed from: C */
    public /* final */ com.portfolio.platform.data.source.HeartRateSummaryRepository f21970C;

    @DexIgnore
    /* renamed from: D */
    public /* final */ com.portfolio.platform.data.source.WorkoutSessionRepository f21971D;

    @DexIgnore
    /* renamed from: E */
    public /* final */ com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase f21972E;

    @DexIgnore
    /* renamed from: F */
    public /* final */ com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase f21973F;

    @DexIgnore
    /* renamed from: G */
    public /* final */ com.portfolio.platform.PortfolioApp f21974G;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.UserRepository f21975d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.portfolio.platform.data.source.AlarmsRepository f21976e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.en2 f21977f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.portfolio.platform.data.source.HybridPresetRepository f21978g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.portfolio.platform.data.source.ActivitiesRepository f21979h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.portfolio.platform.data.source.SummariesRepository f21980i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository f21981j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ com.portfolio.platform.data.source.NotificationsRepository f21982k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ com.portfolio.platform.data.source.DeviceRepository f21983l;

    @DexIgnore
    /* renamed from: m */
    public /* final */ com.portfolio.platform.data.source.SleepSessionsRepository f21984m;

    @DexIgnore
    /* renamed from: n */
    public /* final */ com.portfolio.platform.data.source.GoalTrackingRepository f21985n;

    @DexIgnore
    /* renamed from: o */
    public /* final */ com.fossil.blesdk.obfuscated.jn2 f21986o;

    @DexIgnore
    /* renamed from: p */
    public /* final */ com.fossil.blesdk.obfuscated.zk2 f21987p;

    @DexIgnore
    /* renamed from: q */
    public /* final */ com.portfolio.platform.data.source.SleepSummariesRepository f21988q;

    @DexIgnore
    /* renamed from: r */
    public /* final */ com.portfolio.platform.data.source.DianaPresetRepository f21989r;

    @DexIgnore
    /* renamed from: s */
    public /* final */ com.portfolio.platform.data.source.WatchAppRepository f21990s;

    @DexIgnore
    /* renamed from: t */
    public /* final */ com.portfolio.platform.data.source.ComplicationRepository f21991t;

    @DexIgnore
    /* renamed from: u */
    public /* final */ com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase f21992u;

    @DexIgnore
    /* renamed from: v */
    public /* final */ com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase f21993v;

    @DexIgnore
    /* renamed from: w */
    public /* final */ com.portfolio.platform.data.source.MicroAppRepository f21994w;

    @DexIgnore
    /* renamed from: x */
    public /* final */ com.portfolio.platform.data.source.MicroAppLastSettingRepository f21995x;

    @DexIgnore
    /* renamed from: y */
    public /* final */ com.portfolio.platform.data.source.ComplicationLastSettingRepository f21996y;

    @DexIgnore
    /* renamed from: z */
    public /* final */ com.portfolio.platform.data.source.WatchAppLastSettingRepository f21997z;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$a")
    /* renamed from: com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$a */
    public static final class C6200a {
        @DexIgnore
        public C6200a() {
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40405a() {
            return com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.f21966H;
        }

        @DexIgnore
        public /* synthetic */ C6200a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$b")
    /* renamed from: com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$b */
    public static final class C6201b implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f21998a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.ref.WeakReference<android.app.Activity> f21999b;

        @DexIgnore
        public C6201b(int i, java.lang.ref.WeakReference<android.app.Activity> weakReference) {
            this.f21998a = i;
            this.f21999b = weakReference;
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo40406a() {
            return this.f21998a;
        }

        @DexIgnore
        /* renamed from: b */
        public final java.lang.ref.WeakReference<android.app.Activity> mo40407b() {
            return this.f21999b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$c")
    /* renamed from: com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$c */
    public static final class C6202c implements com.portfolio.platform.CoroutineUseCase.C5602a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f22000a;

        @DexIgnore
        public C6202c(int i) {
            this.f22000a = i;
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo40408a() {
            return this.f22000a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$d")
    /* renamed from: com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$d */
    public static final class C6203d implements com.portfolio.platform.CoroutineUseCase.C5605d {
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$e")
    /* renamed from: com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$e */
    public static final class C6204e implements com.fossil.blesdk.obfuscated.gr3.C4352c {
        @DexIgnore
        /* renamed from: a */
        public void mo27800a() {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.f21967I.mo40405a(), "Logout UA success");
        }
    }

    /*
    static {
        java.lang.String simpleName = com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "DeleteLogoutUserUseCase::class.java.simpleName");
        f21966H = simpleName;
    }
    */

    @DexIgnore
    public DeleteLogoutUserUseCase(com.portfolio.platform.data.source.UserRepository userRepository, com.portfolio.platform.data.source.AlarmsRepository alarmsRepository, com.fossil.blesdk.obfuscated.en2 en2, com.portfolio.platform.data.source.HybridPresetRepository hybridPresetRepository, com.portfolio.platform.data.source.ActivitiesRepository activitiesRepository, com.portfolio.platform.data.source.SummariesRepository summariesRepository, com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository microAppSettingRepository, com.portfolio.platform.data.source.NotificationsRepository notificationsRepository, com.portfolio.platform.data.source.DeviceRepository deviceRepository, com.portfolio.platform.data.source.SleepSessionsRepository sleepSessionsRepository, com.portfolio.platform.data.source.GoalTrackingRepository goalTrackingRepository, com.fossil.blesdk.obfuscated.jn2 jn2, com.fossil.blesdk.obfuscated.zk2 zk2, com.portfolio.platform.data.source.SleepSummariesRepository sleepSummariesRepository, com.portfolio.platform.data.source.DianaPresetRepository dianaPresetRepository, com.portfolio.platform.data.source.WatchAppRepository watchAppRepository, com.portfolio.platform.data.source.ComplicationRepository complicationRepository, com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase notificationSettingsDatabase, com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase dNDSettingsDatabase, com.portfolio.platform.data.source.MicroAppRepository microAppRepository, com.portfolio.platform.data.source.MicroAppLastSettingRepository microAppLastSettingRepository, com.portfolio.platform.data.source.ComplicationLastSettingRepository complicationLastSettingRepository, com.portfolio.platform.data.source.WatchAppLastSettingRepository watchAppLastSettingRepository, com.portfolio.platform.data.source.FitnessDataRepository fitnessDataRepository, com.portfolio.platform.data.source.HeartRateSampleRepository heartRateSampleRepository, com.portfolio.platform.data.source.HeartRateSummaryRepository heartRateSummaryRepository, com.portfolio.platform.data.source.WorkoutSessionRepository workoutSessionRepository, com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase remindersSettingsDatabase, com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase thirdPartyDatabase, com.portfolio.platform.PortfolioApp portfolioApp) {
        com.portfolio.platform.data.source.UserRepository userRepository2 = userRepository;
        com.portfolio.platform.data.source.AlarmsRepository alarmsRepository2 = alarmsRepository;
        com.fossil.blesdk.obfuscated.en2 en22 = en2;
        com.portfolio.platform.data.source.HybridPresetRepository hybridPresetRepository2 = hybridPresetRepository;
        com.portfolio.platform.data.source.ActivitiesRepository activitiesRepository2 = activitiesRepository;
        com.portfolio.platform.data.source.SummariesRepository summariesRepository2 = summariesRepository;
        com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository microAppSettingRepository2 = microAppSettingRepository;
        com.portfolio.platform.data.source.NotificationsRepository notificationsRepository2 = notificationsRepository;
        com.portfolio.platform.data.source.DeviceRepository deviceRepository2 = deviceRepository;
        com.portfolio.platform.data.source.SleepSessionsRepository sleepSessionsRepository2 = sleepSessionsRepository;
        com.portfolio.platform.data.source.GoalTrackingRepository goalTrackingRepository2 = goalTrackingRepository;
        com.fossil.blesdk.obfuscated.jn2 jn22 = jn2;
        com.fossil.blesdk.obfuscated.zk2 zk22 = zk2;
        com.portfolio.platform.data.source.SleepSummariesRepository sleepSummariesRepository2 = sleepSummariesRepository;
        com.portfolio.platform.data.source.WatchAppRepository watchAppRepository2 = watchAppRepository;
        com.fossil.blesdk.obfuscated.kd4.m24411b(userRepository2, "mUserRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(alarmsRepository2, "mAlarmRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(en22, "mSharedPreferences");
        com.fossil.blesdk.obfuscated.kd4.m24411b(hybridPresetRepository2, "mPresetRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(activitiesRepository2, "mActivitiesRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(summariesRepository2, "mSummariesRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(microAppSettingRepository2, "mMicroAppSettingRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(notificationsRepository2, "mNotificationRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceRepository2, "mDeviceRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(sleepSessionsRepository2, "mSleepSessionsRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(goalTrackingRepository2, "mGoalTrackingRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(jn22, "mLoginGoogleManager");
        com.fossil.blesdk.obfuscated.kd4.m24411b(zk22, "mGoogleFitHelper");
        com.fossil.blesdk.obfuscated.kd4.m24411b(sleepSummariesRepository2, "mSleepSummariesRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(dianaPresetRepository, "mDianaPresetRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(watchAppRepository, "mWatchAppRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(complicationRepository, "mComplicationRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        com.fossil.blesdk.obfuscated.kd4.m24411b(dNDSettingsDatabase, "mDNDSettingsDatabase");
        com.fossil.blesdk.obfuscated.kd4.m24411b(microAppRepository, "mMicroAppRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(complicationLastSettingRepository, "mComplicationLastSettingRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(watchAppLastSettingRepository, "mWatchAppLastSettingRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(fitnessDataRepository, "mFitnessDataRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(heartRateSampleRepository, "mHeartRateSampleRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(workoutSessionRepository, "mWorkoutSessionRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        com.fossil.blesdk.obfuscated.kd4.m24411b(thirdPartyDatabase, "mThirdPartyDatabase");
        com.fossil.blesdk.obfuscated.kd4.m24411b(portfolioApp, "mApp");
        this.f21975d = userRepository2;
        this.f21976e = alarmsRepository2;
        this.f21977f = en22;
        this.f21978g = hybridPresetRepository2;
        this.f21979h = activitiesRepository2;
        this.f21980i = summariesRepository2;
        this.f21981j = microAppSettingRepository2;
        this.f21982k = notificationsRepository2;
        this.f21983l = deviceRepository2;
        this.f21984m = sleepSessionsRepository2;
        this.f21985n = goalTrackingRepository2;
        this.f21986o = jn22;
        this.f21987p = zk22;
        this.f21988q = sleepSummariesRepository2;
        this.f21989r = dianaPresetRepository;
        this.f21990s = watchAppRepository;
        this.f21991t = complicationRepository;
        this.f21992u = notificationSettingsDatabase;
        this.f21993v = dNDSettingsDatabase;
        this.f21994w = microAppRepository;
        this.f21995x = microAppLastSettingRepository;
        this.f21996y = complicationLastSettingRepository;
        this.f21997z = watchAppLastSettingRepository;
        this.f21968A = fitnessDataRepository;
        this.f21969B = heartRateSampleRepository;
        this.f21970C = heartRateSummaryRepository;
        this.f21971D = workoutSessionRepository;
        this.f21972E = remindersSettingsDatabase;
        this.f21973F = thirdPartyDatabase;
        this.f21974G = portfolioApp;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f21966H;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6201b bVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase$run$1 deleteLogoutUserUseCase$run$1;
        int i;
        com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase deleteLogoutUserUseCase;
        int intValue;
        com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase deleteLogoutUserUseCase2;
        int intValue2;
        if (yb4 instanceof com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase$run$1) {
            deleteLogoutUserUseCase$run$1 = (com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase$run$1) yb4;
            int i2 = deleteLogoutUserUseCase$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deleteLogoutUserUseCase$run$1.label = i2 - Integer.MIN_VALUE;
                java.lang.Object obj = deleteLogoutUserUseCase$run$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = deleteLogoutUserUseCase$run$1.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String str = f21966H;
                    java.lang.StringBuilder sb = new java.lang.StringBuilder();
                    sb.append("running UseCase with mode=");
                    sb.append(bVar != null ? com.fossil.blesdk.obfuscated.dc4.m20843a(bVar.mo40406a()) : null);
                    local.mo33255d(str, sb.toString());
                    if (bVar != null && bVar.mo40406a() == 0) {
                        com.portfolio.platform.data.model.MFUser currentUser = this.f21975d.getCurrentUser();
                        if (currentUser != null) {
                            com.portfolio.platform.data.source.UserRepository userRepository = this.f21975d;
                            deleteLogoutUserUseCase$run$1.L$0 = this;
                            deleteLogoutUserUseCase$run$1.L$1 = bVar;
                            deleteLogoutUserUseCase$run$1.L$2 = currentUser;
                            deleteLogoutUserUseCase$run$1.label = 1;
                            obj = userRepository.deleteUser(currentUser, deleteLogoutUserUseCase$run$1);
                            if (obj == a) {
                                return a;
                            }
                            deleteLogoutUserUseCase2 = this;
                        }
                        return new java.lang.Object();
                    } else if (bVar == null || bVar.mo40406a() != 1) {
                        if (bVar != null && bVar.mo40406a() == 2) {
                            if (this.f21975d.getCurrentUser() != null) {
                                mo40404a(bVar);
                            } else {
                                mo34434a(new com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6202c(600));
                            }
                        }
                        return new java.lang.Object();
                    } else {
                        com.portfolio.platform.data.source.UserRepository userRepository2 = this.f21975d;
                        deleteLogoutUserUseCase$run$1.L$0 = this;
                        deleteLogoutUserUseCase$run$1.L$1 = bVar;
                        deleteLogoutUserUseCase$run$1.label = 2;
                        obj = userRepository2.logoutUser(deleteLogoutUserUseCase$run$1);
                        if (obj == a) {
                            return a;
                        }
                        deleteLogoutUserUseCase = this;
                        intValue = ((java.lang.Number) obj).intValue();
                        if (intValue != 200) {
                        }
                        return new java.lang.Object();
                    }
                } else if (i == 1) {
                    com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) deleteLogoutUserUseCase$run$1.L$2;
                    bVar = (com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6201b) deleteLogoutUserUseCase$run$1.L$1;
                    deleteLogoutUserUseCase2 = (com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase) deleteLogoutUserUseCase$run$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else if (i == 2) {
                    bVar = (com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6201b) deleteLogoutUserUseCase$run$1.L$1;
                    deleteLogoutUserUseCase = (com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase) deleteLogoutUserUseCase$run$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    intValue = ((java.lang.Number) obj).intValue();
                    if (intValue != 200) {
                        deleteLogoutUserUseCase.mo40404a(bVar);
                    } else {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String str2 = f21966H;
                        local2.mo33255d(str2, "Inside .run failed with http code=" + intValue);
                        if (intValue == 401 || intValue == 404) {
                            deleteLogoutUserUseCase.mo40404a(bVar);
                        } else {
                            deleteLogoutUserUseCase.mo34434a(new com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6202c(intValue));
                        }
                    }
                    return new java.lang.Object();
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                intValue2 = ((java.lang.Number) obj).intValue();
                if (intValue2 != 200) {
                    com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39517c().mo39504b("remove_user", com.misfit.frameworks.common.constants.Constants.RESULT, "");
                    deleteLogoutUserUseCase2.mo40404a(bVar);
                } else {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String str3 = f21966H;
                    local3.mo33255d(str3, "Inside .run failed with http code=" + intValue2);
                    com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39517c().mo39504b("remove_user", com.misfit.frameworks.common.constants.Constants.RESULT, java.lang.String.valueOf(intValue2));
                    if (intValue2 == 401 || intValue2 == 404) {
                        deleteLogoutUserUseCase2.mo40404a(bVar);
                    } else {
                        deleteLogoutUserUseCase2.mo34434a(new com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6202c(intValue2));
                    }
                }
                return new java.lang.Object();
            }
        }
        deleteLogoutUserUseCase$run$1 = new com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase$run$1(this, yb4);
        java.lang.Object obj2 = deleteLogoutUserUseCase$run$1.result;
        java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = deleteLogoutUserUseCase$run$1.label;
        if (i != 0) {
        }
        intValue2 = ((java.lang.Number) obj2).intValue();
        if (intValue2 != 200) {
        }
        return new java.lang.Object();
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo40404a(com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6201b bVar) {
        this.f21986o.mo28552a(bVar.mo40407b());
        try {
            com.misfit.frameworks.buttonservice.IButtonConnectivity b = com.portfolio.platform.PortfolioApp.f20941W.mo34585b();
            if (b != null) {
                b.deviceUnlink(com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e());
            }
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(mo34439b(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$1(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* renamed from: a */
    public final /* synthetic */ java.lang.Object mo40402a(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4> yb4) {
        com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase$removeAllConnectedApps$1 deleteLogoutUserUseCase$removeAllConnectedApps$1;
        int i;
        com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase deleteLogoutUserUseCase;
        if (yb4 instanceof com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase$removeAllConnectedApps$1) {
            deleteLogoutUserUseCase$removeAllConnectedApps$1 = (com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase$removeAllConnectedApps$1) yb4;
            int i2 = deleteLogoutUserUseCase$removeAllConnectedApps$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deleteLogoutUserUseCase$removeAllConnectedApps$1.label = i2 - Integer.MIN_VALUE;
                java.lang.Object obj = deleteLogoutUserUseCase$removeAllConnectedApps$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = deleteLogoutUserUseCase$removeAllConnectedApps$1.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    if (this.f21974G.mo34570r().mo27797b()) {
                        com.fossil.blesdk.obfuscated.gr3 r = this.f21974G.mo34570r();
                        com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6204e eVar = new com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase.C6204e();
                        deleteLogoutUserUseCase$removeAllConnectedApps$1.L$0 = this;
                        deleteLogoutUserUseCase$removeAllConnectedApps$1.label = 1;
                        if (r.mo27791a((com.fossil.blesdk.obfuscated.gr3.C4352c) eVar, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) deleteLogoutUserUseCase$removeAllConnectedApps$1) == a) {
                            return a;
                        }
                    }
                    deleteLogoutUserUseCase = this;
                } else if (i == 1) {
                    deleteLogoutUserUseCase = (com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase) deleteLogoutUserUseCase$removeAllConnectedApps$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (deleteLogoutUserUseCase.f21987p.mo33047e()) {
                    deleteLogoutUserUseCase.f21987p.mo33050h();
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }
        deleteLogoutUserUseCase$removeAllConnectedApps$1 = new com.portfolio.platform.p007ui.user.usecase.DeleteLogoutUserUseCase$removeAllConnectedApps$1(this, yb4);
        java.lang.Object obj2 = deleteLogoutUserUseCase$removeAllConnectedApps$1.result;
        java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = deleteLogoutUserUseCase$removeAllConnectedApps$1.label;
        if (i != 0) {
        }
        if (deleteLogoutUserUseCase.f21987p.mo33047e()) {
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
