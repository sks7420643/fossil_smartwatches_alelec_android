package com.portfolio.platform.p007ui.user.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.user.usecase.LoginSocialUseCase */
public final class LoginSocialUseCase extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase.C6215c, com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase.C6216d, com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase.C6214b> {

    @DexIgnore
    /* renamed from: e */
    public static /* final */ java.lang.String f22013e;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.UserRepository f22014d;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$a")
    /* renamed from: com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$a */
    public static final class C6213a {
        @DexIgnore
        public C6213a() {
        }

        @DexIgnore
        public /* synthetic */ C6213a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$b")
    /* renamed from: com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$b */
    public static final class C6214b implements com.portfolio.platform.CoroutineUseCase.C5602a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f22015a;

        @DexIgnore
        public C6214b(int i, java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "errorMessage");
            this.f22015a = i;
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo40419a() {
            return this.f22015a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$c")
    /* renamed from: com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$c */
    public static final class C6215c implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f22016a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.String f22017b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ java.lang.String f22018c;

        @DexIgnore
        public C6215c(java.lang.String str, java.lang.String str2, java.lang.String str3) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, com.misfit.frameworks.common.constants.Constants.SERVICE);
            com.fossil.blesdk.obfuscated.kd4.m24411b(str2, "token");
            com.fossil.blesdk.obfuscated.kd4.m24411b(str3, "clientId");
            this.f22016a = str;
            this.f22017b = str2;
            this.f22018c = str3;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40420a() {
            return this.f22018c;
        }

        @DexIgnore
        /* renamed from: b */
        public final java.lang.String mo40421b() {
            return this.f22016a;
        }

        @DexIgnore
        /* renamed from: c */
        public final java.lang.String mo40422c() {
            return this.f22017b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$d")
    /* renamed from: com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$d */
    public static final class C6216d implements com.portfolio.platform.CoroutineUseCase.C5605d {
        @DexIgnore
        public C6216d(com.portfolio.platform.data.Auth auth) {
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase.C6213a((com.fossil.blesdk.obfuscated.fd4) null);
        java.lang.String simpleName = com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "LoginSocialUseCase::class.java.simpleName");
        f22013e = simpleName;
    }
    */

    @DexIgnore
    public LoginSocialUseCase(com.portfolio.platform.data.source.UserRepository userRepository) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(userRepository, "mUserRepository");
        this.f22014d = userRepository;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f22013e;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ba, code lost:
        if (r10 != null) goto L_0x00be;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase.C6215c cVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase$run$1 loginSocialUseCase$run$1;
        int i;
        com.fossil.blesdk.obfuscated.qo2 qo2;
        java.lang.String str;
        if (yb4 instanceof com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase$run$1) {
            loginSocialUseCase$run$1 = (com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase$run$1) yb4;
            int i2 = loginSocialUseCase$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                loginSocialUseCase$run$1.label = i2 - Integer.MIN_VALUE;
                java.lang.Object obj = loginSocialUseCase$run$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = loginSocialUseCase$run$1.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(f22013e, "running UseCase");
                    if (cVar == null) {
                        return new com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase.C6214b(600, "");
                    }
                    com.portfolio.platform.data.source.UserRepository userRepository = this.f22014d;
                    java.lang.String b = cVar.mo40421b();
                    java.lang.String c = cVar.mo40422c();
                    java.lang.String a2 = cVar.mo40420a();
                    loginSocialUseCase$run$1.L$0 = this;
                    loginSocialUseCase$run$1.L$1 = cVar;
                    loginSocialUseCase$run$1.label = 1;
                    obj = userRepository.loginWithSocial(b, c, a2, loginSocialUseCase$run$1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase.C6215c cVar2 = (com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase.C6215c) loginSocialUseCase$run$1.L$1;
                    com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase loginSocialUseCase = (com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase) loginSocialUseCase$run$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
                if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
                    return new com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase.C6216d((com.portfolio.platform.data.Auth) ((com.fossil.blesdk.obfuscated.ro2) qo2).mo30673a());
                }
                if (!(qo2 instanceof com.fossil.blesdk.obfuscated.po2)) {
                    return new com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase.C6214b(600, "");
                }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String str2 = f22013e;
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append("Inside .run failed with http code=");
                com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
                sb.append(po2.mo30176a());
                local.mo33255d(str2, sb.toString());
                int a3 = po2.mo30176a();
                com.portfolio.platform.data.model.ServerError c2 = po2.mo30178c();
                if (c2 != null) {
                    str = c2.getMessage();
                }
                str = "";
                return new com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase.C6214b(a3, str);
            }
        }
        loginSocialUseCase$run$1 = new com.portfolio.platform.p007ui.user.usecase.LoginSocialUseCase$run$1(this, yb4);
        java.lang.Object obj2 = loginSocialUseCase$run$1.result;
        java.lang.Object a4 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = loginSocialUseCase$run$1.label;
        if (i != 0) {
        }
        qo2 = (com.fossil.blesdk.obfuscated.qo2) obj2;
        if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
        }
    }
}
