package com.portfolio.platform.p007ui.user.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.user.usecase.LoginEmailUseCase */
public final class LoginEmailUseCase extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase.C6211c, com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase.C6212d, com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase.C6210b> {

    @DexIgnore
    /* renamed from: e */
    public static /* final */ java.lang.String f22007e;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.UserRepository f22008d;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.LoginEmailUseCase$a")
    /* renamed from: com.portfolio.platform.ui.user.usecase.LoginEmailUseCase$a */
    public static final class C6209a {
        @DexIgnore
        public C6209a() {
        }

        @DexIgnore
        public /* synthetic */ C6209a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.LoginEmailUseCase$b")
    /* renamed from: com.portfolio.platform.ui.user.usecase.LoginEmailUseCase$b */
    public static final class C6210b implements com.portfolio.platform.CoroutineUseCase.C5602a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f22009a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.String f22010b;

        @DexIgnore
        public C6210b(int i, java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "errorMessage");
            this.f22009a = i;
            this.f22010b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo40414a() {
            return this.f22009a;
        }

        @DexIgnore
        /* renamed from: b */
        public final java.lang.String mo40415b() {
            return this.f22010b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.LoginEmailUseCase$c")
    /* renamed from: com.portfolio.platform.ui.user.usecase.LoginEmailUseCase$c */
    public static final class C6211c implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f22011a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.String f22012b;

        @DexIgnore
        public C6211c(java.lang.String str, java.lang.String str2) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "email");
            com.fossil.blesdk.obfuscated.kd4.m24411b(str2, "password");
            this.f22011a = str;
            this.f22012b = str2;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40416a() {
            return this.f22011a;
        }

        @DexIgnore
        /* renamed from: b */
        public final java.lang.String mo40417b() {
            return this.f22012b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.LoginEmailUseCase$d")
    /* renamed from: com.portfolio.platform.ui.user.usecase.LoginEmailUseCase$d */
    public static final class C6212d implements com.portfolio.platform.CoroutineUseCase.C5605d {
        @DexIgnore
        public C6212d(com.portfolio.platform.data.Auth auth) {
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase.C6209a((com.fossil.blesdk.obfuscated.fd4) null);
        java.lang.String simpleName = com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "LoginEmailUseCase::class.java.simpleName");
        f22007e = simpleName;
    }
    */

    @DexIgnore
    public LoginEmailUseCase(com.portfolio.platform.data.source.UserRepository userRepository) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(userRepository, "mUserRepository");
        this.f22008d = userRepository;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f22007e;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00c7, code lost:
        if (r9 != null) goto L_0x00cb;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase.C6211c cVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase$run$1 loginEmailUseCase$run$1;
        int i;
        com.fossil.blesdk.obfuscated.qo2 qo2;
        java.lang.String str;
        if (yb4 instanceof com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase$run$1) {
            loginEmailUseCase$run$1 = (com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase$run$1) yb4;
            int i2 = loginEmailUseCase$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                loginEmailUseCase$run$1.label = i2 - Integer.MIN_VALUE;
                java.lang.Object obj = loginEmailUseCase$run$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = loginEmailUseCase$run$1.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(f22007e, "running UseCase");
                    if (cVar == null) {
                        return new com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase.C6210b(600, "");
                    }
                    java.lang.String a2 = cVar.mo40416a();
                    if (a2 != null) {
                        java.lang.String lowerCase = a2.toLowerCase();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                        com.portfolio.platform.data.source.UserRepository userRepository = this.f22008d;
                        java.lang.String b = cVar.mo40417b();
                        loginEmailUseCase$run$1.L$0 = this;
                        loginEmailUseCase$run$1.L$1 = cVar;
                        loginEmailUseCase$run$1.L$2 = lowerCase;
                        loginEmailUseCase$run$1.label = 1;
                        obj = userRepository.loginEmail(lowerCase, b, loginEmailUseCase$run$1);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                } else if (i == 1) {
                    java.lang.String str2 = (java.lang.String) loginEmailUseCase$run$1.L$2;
                    com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase.C6211c cVar2 = (com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase.C6211c) loginEmailUseCase$run$1.L$1;
                    com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase loginEmailUseCase = (com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase) loginEmailUseCase$run$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
                if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
                    return new com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase.C6212d((com.portfolio.platform.data.Auth) ((com.fossil.blesdk.obfuscated.ro2) qo2).mo30673a());
                }
                if (!(qo2 instanceof com.fossil.blesdk.obfuscated.po2)) {
                    return new com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase.C6210b(600, "");
                }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String str3 = f22007e;
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append("Inside .run failed with http code=");
                com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
                sb.append(po2.mo30176a());
                local.mo33255d(str3, sb.toString());
                int a3 = po2.mo30176a();
                com.portfolio.platform.data.model.ServerError c = po2.mo30178c();
                if (c != null) {
                    str = c.getMessage();
                }
                str = "";
                return new com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase.C6210b(a3, str);
            }
        }
        loginEmailUseCase$run$1 = new com.portfolio.platform.p007ui.user.usecase.LoginEmailUseCase$run$1(this, yb4);
        java.lang.Object obj2 = loginEmailUseCase$run$1.result;
        java.lang.Object a4 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = loginEmailUseCase$run$1.label;
        if (i != 0) {
        }
        qo2 = (com.fossil.blesdk.obfuscated.qo2) obj2;
        if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
        }
    }
}
