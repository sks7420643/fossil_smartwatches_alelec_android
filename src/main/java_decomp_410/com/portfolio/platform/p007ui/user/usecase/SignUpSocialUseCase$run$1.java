package com.portfolio.platform.p007ui.user.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase", mo27670f = "SignUpSocialUseCase.kt", mo27671l = {26}, mo27672m = "run")
/* renamed from: com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase$run$1 */
public final class SignUpSocialUseCase$run$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SignUpSocialUseCase$run$1(com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase signUpSocialUseCase, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = signUpSocialUseCase;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.mo26310a((com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase.C6224a) null, (com.fossil.blesdk.obfuscated.yb4<java.lang.Object>) this);
    }
}
