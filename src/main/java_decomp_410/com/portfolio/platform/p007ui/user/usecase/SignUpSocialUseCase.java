package com.portfolio.platform.p007ui.user.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase */
public final class SignUpSocialUseCase extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase.C6224a, com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase.C6226c, com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase.C6225b> {

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.UserRepository f22029d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.en2 f22030e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase$a")
    /* renamed from: com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase$a */
    public static final class C6224a implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.portfolio.platform.data.SignUpSocialAuth f22031a;

        @DexIgnore
        public C6224a(com.portfolio.platform.data.SignUpSocialAuth signUpSocialAuth) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(signUpSocialAuth, "socialAuth");
            this.f22031a = signUpSocialAuth;
        }

        @DexIgnore
        /* renamed from: a */
        public final com.portfolio.platform.data.SignUpSocialAuth mo40432a() {
            return this.f22031a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase$b")
    /* renamed from: com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase$b */
    public static final class C6225b implements com.portfolio.platform.CoroutineUseCase.C5602a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f22032a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.String f22033b;

        @DexIgnore
        public C6225b(int i, java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "errorMesagge");
            this.f22032a = i;
            this.f22033b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo40433a() {
            return this.f22032a;
        }

        @DexIgnore
        /* renamed from: b */
        public final java.lang.String mo40434b() {
            return this.f22033b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase$c")
    /* renamed from: com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase$c */
    public static final class C6226c implements com.portfolio.platform.CoroutineUseCase.C5605d {
    }

    @DexIgnore
    public SignUpSocialUseCase(com.portfolio.platform.data.source.UserRepository userRepository, com.fossil.blesdk.obfuscated.en2 en2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(userRepository, "mUserRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(en2, "mSharedPreferencesManager");
        this.f22029d = userRepository;
        this.f22030e = en2;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return "SignUpSocialUseCase";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00de, code lost:
        if (r9 != null) goto L_0x00e2;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase.C6224a aVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase$run$1 signUpSocialUseCase$run$1;
        int i;
        com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase signUpSocialUseCase;
        com.fossil.blesdk.obfuscated.qo2 qo2;
        java.lang.String str;
        if (yb4 instanceof com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase$run$1) {
            signUpSocialUseCase$run$1 = (com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase$run$1) yb4;
            int i2 = signUpSocialUseCase$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                signUpSocialUseCase$run$1.label = i2 - Integer.MIN_VALUE;
                java.lang.Object obj = signUpSocialUseCase$run$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = signUpSocialUseCase$run$1.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("SignUpSocialUseCase", "running UseCase");
                    if (aVar == null) {
                        return new com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase.C6225b(600, "");
                    }
                    if (this.f22029d.getCurrentUser() != null) {
                        return new com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase.C6226c();
                    }
                    com.portfolio.platform.data.source.UserRepository userRepository = this.f22029d;
                    com.portfolio.platform.data.SignUpSocialAuth a2 = aVar.mo40432a();
                    signUpSocialUseCase$run$1.L$0 = this;
                    signUpSocialUseCase$run$1.L$1 = aVar;
                    signUpSocialUseCase$run$1.label = 1;
                    obj = userRepository.signUpSocial(a2, signUpSocialUseCase$run$1);
                    if (obj == a) {
                        return a;
                    }
                    signUpSocialUseCase = this;
                } else if (i == 1) {
                    com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase.C6224a aVar2 = (com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase.C6224a) signUpSocialUseCase$run$1.L$1;
                    signUpSocialUseCase = (com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase) signUpSocialUseCase$run$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
                java.lang.String str2 = null;
                if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("SignUpSocialUseCase", "signUpEmail success");
                    com.fossil.blesdk.obfuscated.en2 en2 = signUpSocialUseCase.f22030e;
                    com.portfolio.platform.data.Auth auth = (com.portfolio.platform.data.Auth) ((com.fossil.blesdk.obfuscated.ro2) qo2).mo30673a();
                    if (auth != null) {
                        str2 = auth.getAccessToken();
                    }
                    en2.mo27088w(str2);
                    return new com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase.C6226c();
                } else if (!(qo2 instanceof com.fossil.blesdk.obfuscated.po2)) {
                    return new com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase.C6225b(600, "");
                } else {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.StringBuilder sb = new java.lang.StringBuilder();
                    sb.append("signUpEmail failed code=");
                    com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
                    com.portfolio.platform.data.model.ServerError c = po2.mo30178c();
                    if (c != null) {
                        str2 = c.getMessage();
                    }
                    sb.append(str2);
                    local.mo33255d("SignUpSocialUseCase", sb.toString());
                    int a3 = po2.mo30176a();
                    com.portfolio.platform.data.model.ServerError c2 = po2.mo30178c();
                    if (c2 != null) {
                        str = c2.getMessage();
                    }
                    str = "";
                    return new com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase.C6225b(a3, str);
                }
            }
        }
        signUpSocialUseCase$run$1 = new com.portfolio.platform.p007ui.user.usecase.SignUpSocialUseCase$run$1(this, yb4);
        java.lang.Object obj2 = signUpSocialUseCase$run$1.result;
        java.lang.Object a4 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = signUpSocialUseCase$run$1.label;
        if (i != 0) {
        }
        qo2 = (com.fossil.blesdk.obfuscated.qo2) obj2;
        java.lang.String str22 = null;
        if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
        }
    }
}
