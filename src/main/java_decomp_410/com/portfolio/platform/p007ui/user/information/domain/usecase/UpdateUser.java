package com.portfolio.platform.p007ui.user.information.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser */
public final class UpdateUser extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6197b, com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6199d, com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6198c> {

    @DexIgnore
    /* renamed from: e */
    public static /* final */ java.lang.String f21960e;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.UserRepository f21961d;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser$a")
    /* renamed from: com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser$a */
    public static final class C6196a {
        @DexIgnore
        public C6196a() {
        }

        @DexIgnore
        public /* synthetic */ C6196a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser$b")
    /* renamed from: com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser$b */
    public static final class C6197b implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.portfolio.platform.data.model.MFUser f21962a;

        @DexIgnore
        public C6197b(com.portfolio.platform.data.model.MFUser mFUser) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(mFUser, "mfUser");
            this.f21962a = mFUser;
        }

        @DexIgnore
        /* renamed from: a */
        public final com.portfolio.platform.data.model.MFUser mo40398a() {
            return this.f21962a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser$c")
    /* renamed from: com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser$c */
    public static final class C6198c implements com.portfolio.platform.CoroutineUseCase.C5602a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f21963a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.String f21964b;

        @DexIgnore
        public C6198c(int i, java.lang.String str) {
            this.f21963a = i;
            this.f21964b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo40399a() {
            return this.f21963a;
        }

        @DexIgnore
        /* renamed from: b */
        public final java.lang.String mo40400b() {
            return this.f21964b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser$d")
    /* renamed from: com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser$d */
    public static final class C6199d implements com.portfolio.platform.CoroutineUseCase.C5605d {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.portfolio.platform.data.model.MFUser f21965a;

        @DexIgnore
        public C6199d(com.portfolio.platform.data.model.MFUser mFUser) {
            this.f21965a = mFUser;
        }

        @DexIgnore
        /* renamed from: a */
        public final com.portfolio.platform.data.model.MFUser mo40401a() {
            return this.f21965a;
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6196a((com.fossil.blesdk.obfuscated.fd4) null);
        java.lang.String simpleName = com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "UpdateUser::class.java.simpleName");
        f21960e = simpleName;
    }
    */

    @DexIgnore
    public UpdateUser(com.portfolio.platform.data.source.UserRepository userRepository) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(userRepository, "mUserRepository");
        this.f21961d = userRepository;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f21960e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6197b bVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser$run$1 updateUser$run$1;
        int i;
        com.fossil.blesdk.obfuscated.qo2 qo2;
        java.lang.String str;
        if (yb4 instanceof com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser$run$1) {
            updateUser$run$1 = (com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser$run$1) yb4;
            int i2 = updateUser$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                updateUser$run$1.label = i2 - Integer.MIN_VALUE;
                java.lang.Object obj = updateUser$run$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = updateUser$run$1.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(f21960e, "running UseCase");
                    if (bVar == null) {
                        return new com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6198c(600, "");
                    }
                    com.portfolio.platform.data.source.UserRepository userRepository = this.f21961d;
                    com.portfolio.platform.data.model.MFUser a2 = bVar.mo40398a();
                    updateUser$run$1.L$0 = this;
                    updateUser$run$1.L$1 = bVar;
                    updateUser$run$1.label = 1;
                    obj = userRepository.updateUser(a2, true, updateUser$run$1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6197b bVar2 = (com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6197b) updateUser$run$1.L$1;
                    com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser updateUser = (com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser) updateUser$run$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
                if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
                    com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
                    com.misfit.frameworks.buttonservice.model.UserProfile j = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34543j();
                    if (j != null) {
                        c.mo34490a(j);
                        return new com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6199d((com.portfolio.platform.data.model.MFUser) ((com.fossil.blesdk.obfuscated.ro2) qo2).mo30673a());
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                } else if (qo2 instanceof com.fossil.blesdk.obfuscated.po2) {
                    com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
                    int a3 = po2.mo30176a();
                    com.portfolio.platform.data.model.ServerError c2 = po2.mo30178c();
                    if (c2 != null) {
                        java.lang.String userMessage = c2.getUserMessage();
                        if (userMessage != null) {
                            str = userMessage;
                            if (str == null) {
                                str = "";
                            }
                            return new com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6198c(a3, str);
                        }
                    }
                    com.portfolio.platform.data.model.ServerError c3 = po2.mo30178c();
                    str = c3 != null ? c3.getMessage() : null;
                    if (str == null) {
                    }
                    return new com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6198c(a3, str);
                } else {
                    throw new kotlin.NoWhenBranchMatchedException();
                }
            }
        }
        updateUser$run$1 = new com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser$run$1(this, yb4);
        java.lang.Object obj2 = updateUser$run$1.result;
        java.lang.Object a4 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = updateUser$run$1.label;
        if (i != 0) {
        }
        qo2 = (com.fossil.blesdk.obfuscated.qo2) obj2;
        if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
        }
    }
}
