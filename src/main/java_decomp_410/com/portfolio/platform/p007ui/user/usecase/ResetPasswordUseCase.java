package com.portfolio.platform.p007ui.user.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase */
public final class ResetPasswordUseCase extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase.C6218b, com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase.C6220d, com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase.C6219c> {

    @DexIgnore
    /* renamed from: e */
    public static /* final */ java.lang.String f22019e;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.remote.AuthApiGuestService f22020d;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase$a")
    /* renamed from: com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase$a */
    public static final class C6217a {
        @DexIgnore
        public C6217a() {
        }

        @DexIgnore
        public /* synthetic */ C6217a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase$b")
    /* renamed from: com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase$b */
    public static final class C6218b implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f22021a;

        @DexIgnore
        public C6218b(java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "email");
            this.f22021a = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.String mo40424a() {
            return this.f22021a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase$c")
    /* renamed from: com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase$c */
    public static final class C6219c implements com.portfolio.platform.CoroutineUseCase.C5602a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f22022a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.String f22023b;

        @DexIgnore
        public C6219c(int i, java.lang.String str) {
            this.f22022a = i;
            this.f22023b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo40425a() {
            return this.f22022a;
        }

        @DexIgnore
        /* renamed from: b */
        public final java.lang.String mo40426b() {
            return this.f22023b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase$d")
    /* renamed from: com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase$d */
    public static final class C6220d implements com.portfolio.platform.CoroutineUseCase.C5605d {
    }

    /*
    static {
        new com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase.C6217a((com.fossil.blesdk.obfuscated.fd4) null);
        java.lang.String simpleName = com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "ResetPasswordUseCase::class.java.simpleName");
        f22019e = simpleName;
    }
    */

    @DexIgnore
    public ResetPasswordUseCase(com.portfolio.platform.data.source.remote.AuthApiGuestService authApiGuestService) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(authApiGuestService, "mApiGuestService");
        this.f22020d = authApiGuestService;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f22019e;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ec, code lost:
        if (r10 != null) goto L_0x00f0;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase.C6218b bVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase$run$1 resetPasswordUseCase$run$1;
        int i;
        com.fossil.blesdk.obfuscated.qo2 qo2;
        int i2;
        com.portfolio.platform.data.model.ServerError c;
        java.lang.String str;
        if (yb4 instanceof com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase$run$1) {
            resetPasswordUseCase$run$1 = (com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase$run$1) yb4;
            int i3 = resetPasswordUseCase$run$1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                resetPasswordUseCase$run$1.label = i3 - Integer.MIN_VALUE;
                java.lang.Object obj = resetPasswordUseCase$run$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = resetPasswordUseCase$run$1.label;
                java.lang.Integer num = null;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String str2 = f22019e;
                    java.lang.StringBuilder sb = new java.lang.StringBuilder();
                    sb.append("running UseCase with email=");
                    sb.append(bVar != null ? bVar.mo40424a() : null);
                    local.mo33255d(str2, sb.toString());
                    if (bVar == null) {
                        return new com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase.C6219c(600, "");
                    }
                    com.fossil.blesdk.obfuscated.xz1 xz1 = new com.fossil.blesdk.obfuscated.xz1();
                    xz1.mo17940a("email", bVar.mo40424a());
                    com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase$run$response$1 resetPasswordUseCase$run$response$1 = new com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase$run$response$1(this, xz1, (com.fossil.blesdk.obfuscated.yb4) null);
                    resetPasswordUseCase$run$1.L$0 = this;
                    resetPasswordUseCase$run$1.L$1 = bVar;
                    resetPasswordUseCase$run$1.L$2 = xz1;
                    resetPasswordUseCase$run$1.label = 1;
                    obj = com.portfolio.platform.response.ResponseKt.m32232a(resetPasswordUseCase$run$response$1, resetPasswordUseCase$run$1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.fossil.blesdk.obfuscated.xz1 xz12 = (com.fossil.blesdk.obfuscated.xz1) resetPasswordUseCase$run$1.L$2;
                    com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase.C6218b bVar2 = (com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase.C6218b) resetPasswordUseCase$run$1.L$1;
                    com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase resetPasswordUseCase = (com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase) resetPasswordUseCase$run$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
                if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
                    return new com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase.C6220d();
                }
                if (qo2 instanceof com.fossil.blesdk.obfuscated.po2) {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String str3 = f22019e;
                    java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
                    sb2.append("Inside .run failed with error=");
                    com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
                    com.portfolio.platform.data.model.ServerError c2 = po2.mo30178c();
                    if (c2 != null) {
                        num = c2.getCode();
                    }
                    sb2.append(num);
                    local2.mo33255d(str3, sb2.toString());
                    com.portfolio.platform.data.model.ServerError c3 = po2.mo30178c();
                    if (c3 != null) {
                        java.lang.Integer code = c3.getCode();
                        if (code != null) {
                            i2 = code.intValue();
                            c = po2.mo30178c();
                            if (c != null) {
                                str = c.getMessage();
                            }
                            str = "";
                            return new com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase.C6219c(i2, str);
                        }
                    }
                    i2 = po2.mo30176a();
                    c = po2.mo30178c();
                    if (c != null) {
                    }
                    str = "";
                    return new com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase.C6219c(i2, str);
                }
                throw new kotlin.NoWhenBranchMatchedException();
            }
        }
        resetPasswordUseCase$run$1 = new com.portfolio.platform.p007ui.user.usecase.ResetPasswordUseCase$run$1(this, yb4);
        java.lang.Object obj2 = resetPasswordUseCase$run$1.result;
        java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = resetPasswordUseCase$run$1.label;
        java.lang.Integer num2 = null;
        if (i != 0) {
        }
        qo2 = (com.fossil.blesdk.obfuscated.qo2) obj2;
        if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
        }
    }
}
