package com.portfolio.platform.p007ui.user.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase */
public final class DownloadUserInfoUseCase extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase.C6207c, com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase.C6208d, com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase.C6206b> {

    @DexIgnore
    /* renamed from: e */
    public static /* final */ java.lang.String f22002e;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.UserRepository f22003d;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$a")
    /* renamed from: com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$a */
    public static final class C6205a {
        @DexIgnore
        public C6205a() {
        }

        @DexIgnore
        public /* synthetic */ C6205a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$b")
    /* renamed from: com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$b */
    public static final class C6206b implements com.portfolio.platform.CoroutineUseCase.C5602a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f22004a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.String f22005b;

        @DexIgnore
        public C6206b(int i, java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, "errorMessage");
            this.f22004a = i;
            this.f22005b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo40410a() {
            return this.f22004a;
        }

        @DexIgnore
        /* renamed from: b */
        public final java.lang.String mo40411b() {
            return this.f22005b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$c")
    /* renamed from: com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$c */
    public static final class C6207c implements com.portfolio.platform.CoroutineUseCase.C5603b {
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$d")
    /* renamed from: com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$d */
    public static final class C6208d implements com.portfolio.platform.CoroutineUseCase.C5605d {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.portfolio.platform.data.model.MFUser f22006a;

        @DexIgnore
        public C6208d(com.portfolio.platform.data.model.MFUser mFUser) {
            this.f22006a = mFUser;
        }

        @DexIgnore
        /* renamed from: a */
        public final com.portfolio.platform.data.model.MFUser mo40412a() {
            return this.f22006a;
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase.C6205a((com.fossil.blesdk.obfuscated.fd4) null);
        java.lang.String simpleName = com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "DownloadUserInfoUseCase::class.java.simpleName");
        f22002e = simpleName;
    }
    */

    @DexIgnore
    public DownloadUserInfoUseCase(com.portfolio.platform.data.source.UserRepository userRepository) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(userRepository, "userRepository");
        this.f22003d = userRepository;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f22002e;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0082, code lost:
        if (r7 != null) goto L_0x0087;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase.C6207c cVar, com.fossil.blesdk.obfuscated.yb4<java.lang.Object> yb4) {
        com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase$run$1 downloadUserInfoUseCase$run$1;
        int i;
        com.fossil.blesdk.obfuscated.qo2 qo2;
        java.lang.String str;
        if (yb4 instanceof com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase$run$1) {
            downloadUserInfoUseCase$run$1 = (com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase$run$1) yb4;
            int i2 = downloadUserInfoUseCase$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                downloadUserInfoUseCase$run$1.label = i2 - Integer.MIN_VALUE;
                java.lang.Object obj = downloadUserInfoUseCase$run$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = downloadUserInfoUseCase$run$1.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(f22002e, "running UseCase");
                    com.portfolio.platform.data.source.UserRepository userRepository = this.f22003d;
                    downloadUserInfoUseCase$run$1.L$0 = this;
                    downloadUserInfoUseCase$run$1.L$1 = cVar;
                    downloadUserInfoUseCase$run$1.label = 1;
                    obj = userRepository.loadUserInfo(downloadUserInfoUseCase$run$1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase.C6207c cVar2 = (com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase.C6207c) downloadUserInfoUseCase$run$1.L$1;
                    com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase downloadUserInfoUseCase = (com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase) downloadUserInfoUseCase$run$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
                if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
                    return new com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase.C6208d((com.portfolio.platform.data.model.MFUser) ((com.fossil.blesdk.obfuscated.ro2) qo2).mo30673a());
                }
                if (!(qo2 instanceof com.fossil.blesdk.obfuscated.po2)) {
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
                int a2 = po2.mo30176a();
                com.portfolio.platform.data.model.ServerError c = po2.mo30178c();
                if (c != null) {
                    str = c.getMessage();
                }
                str = "";
                return new com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase.C6206b(a2, str);
            }
        }
        downloadUserInfoUseCase$run$1 = new com.portfolio.platform.p007ui.user.usecase.DownloadUserInfoUseCase$run$1(this, yb4);
        java.lang.Object obj2 = downloadUserInfoUseCase$run$1.result;
        java.lang.Object a3 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = downloadUserInfoUseCase$run$1.label;
        if (i != 0) {
        }
        qo2 = (com.fossil.blesdk.obfuscated.qo2) obj2;
        if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
        }
    }
}
