package com.portfolio.platform.p007ui.debug;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.debug.DebugClearDataWarningActivity */
public class DebugClearDataWarningActivity extends androidx.appcompat.app.AppCompatActivity {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugClearDataWarningActivity$a")
    /* renamed from: com.portfolio.platform.ui.debug.DebugClearDataWarningActivity$a */
    public class C6126a implements android.view.View.OnClickListener {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugClearDataWarningActivity$a$a")
        /* renamed from: com.portfolio.platform.ui.debug.DebugClearDataWarningActivity$a$a */
        public class C6127a implements java.lang.Runnable {
            @DexIgnore
            public C6127a(com.portfolio.platform.p007ui.debug.DebugClearDataWarningActivity.C6126a aVar) {
            }

            @DexIgnore
            public void run() {
                com.portfolio.platform.PortfolioApp.f20936R.mo34482a();
            }
        }

        @DexIgnore
        public C6126a() {
        }

        @DexIgnore
        public void onClick(android.view.View view) {
            android.widget.Toast.makeText(com.portfolio.platform.p007ui.debug.DebugClearDataWarningActivity.this, "The app is being restarted...", 1).show();
            new android.os.Handler().postDelayed(new com.portfolio.platform.p007ui.debug.DebugClearDataWarningActivity.C6126a.C6127a(this), 1500);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m32762a(android.content.Context context) {
        android.content.Intent intent = new android.content.Intent(context, com.portfolio.platform.p007ui.debug.DebugClearDataWarningActivity.class);
        intent.addFlags(268435456);
        context.startActivity(intent);
    }

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    public void onCreate(android.os.Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) com.fossil.wearables.fossil.R.layout.activity_debug_clear_data_warning);
        ((android.widget.Button) findViewById(com.fossil.wearables.fossil.R.id.btn_got_it)).setOnClickListener(new com.portfolio.platform.p007ui.debug.DebugClearDataWarningActivity.C6126a());
    }
}
