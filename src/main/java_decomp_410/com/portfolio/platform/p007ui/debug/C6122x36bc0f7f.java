package com.portfolio.platform.p007ui.debug;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.debug.DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$1 */
public final class C6122x36bc0f7f implements android.content.DialogInterface.OnClickListener {

    @DexIgnore
    /* renamed from: e */
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.Firmware f21771e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.debug.DebugActivity f21772f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.DebugFirmwareData f21773g;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$1$1")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$1$1 */
    public static final class C61231 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21774p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.debug.C6122x36bc0f7f this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$1$1$1")
        /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$1$1$1 */
        public static final class C61241 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.io.File>, java.lang.Object> {
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f21775p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.p007ui.debug.C6122x36bc0f7f.C61231 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C61241(com.portfolio.platform.p007ui.debug.C6122x36bc0f7f.C61231 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.p007ui.debug.C6122x36bc0f7f.C61231.C61241 r0 = new com.portfolio.platform.p007ui.debug.C6122x36bc0f7f.C61231.C61241(this.this$0, yb4);
                r0.f21775p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.p007ui.debug.C6122x36bc0f7f.C61231.C61241) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21775p$;
                    com.misfit.frameworks.buttonservice.source.FirmwareFileRepository x = this.this$0.this$0.f21772f.mo40246x();
                    java.lang.String versionNumber = this.this$0.this$0.f21771e.getVersionNumber();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) versionNumber, "firmware.versionNumber");
                    java.lang.String downloadUrl = this.this$0.this$0.f21771e.getDownloadUrl();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) downloadUrl, "firmware.downloadUrl");
                    java.lang.String checksum = this.this$0.this$0.f21771e.getChecksum();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) checksum, "firmware.checksum");
                    this.L$0 = zg4;
                    this.label = 1;
                    obj = x.downloadFirmware(versionNumber, downloadUrl, checksum, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C61231(com.portfolio.platform.p007ui.debug.C6122x36bc0f7f debugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = debugActivity$showDialogConfirmDownloadFirmware$$inlined$apply$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.p007ui.debug.C6122x36bc0f7f.C61231 r0 = new com.portfolio.platform.p007ui.debug.C6122x36bc0f7f.C61231(this.this$0, yb4);
            r0.f21774p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.p007ui.debug.C6122x36bc0f7f.C61231) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.portfolio.platform.data.model.DebugFirmwareData debugFirmwareData;
            int i;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i2 = this.label;
            if (i2 == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21774p$;
                com.fossil.blesdk.obfuscated.ug4 a2 = com.fossil.blesdk.obfuscated.nh4.m25691a();
                com.portfolio.platform.p007ui.debug.C6122x36bc0f7f.C61231.C61241 r3 = new com.portfolio.platform.p007ui.debug.C6122x36bc0f7f.C61231.C61241(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r3, this);
                if (obj == a) {
                    return a;
                }
            } else if (i2 == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((java.io.File) obj) != null) {
                debugFirmwareData = this.this$0.f21773g;
                i = 2;
            } else {
                debugFirmwareData = this.this$0.f21773g;
                i = 0;
            }
            debugFirmwareData.setState(i);
            com.portfolio.platform.p007ui.debug.DebugActivity.m32731b(this.this$0.f21772f).mo42932f();
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public C6122x36bc0f7f(com.portfolio.platform.data.model.Firmware firmware, com.portfolio.platform.p007ui.debug.DebugActivity debugActivity, com.portfolio.platform.data.model.DebugFirmwareData debugFirmwareData) {
        this.f21771e = firmware;
        this.f21772f = debugActivity;
        this.f21773g = debugFirmwareData;
    }

    @DexIgnore
    public final void onClick(android.content.DialogInterface dialogInterface, int i) {
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.debug.C6122x36bc0f7f.C61231(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        this.f21773g.setState(1);
        com.portfolio.platform.p007ui.debug.DebugActivity.m32731b(this.f21772f).mo42932f();
    }
}
