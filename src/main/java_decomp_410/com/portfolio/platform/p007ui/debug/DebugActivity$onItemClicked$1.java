package com.portfolio.platform.p007ui.debug;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$1", mo27670f = "DebugActivity.kt", mo27671l = {294, 295, 296}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$1 */
public final class DebugActivity$onItemClicked$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21777p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.debug.DebugActivity this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$1$1", mo27670f = "DebugActivity.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$1$1 */
    public static final class C61251 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21778p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.p007ui.debug.DebugActivity$onItemClicked$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C61251(com.portfolio.platform.p007ui.debug.DebugActivity$onItemClicked$1 debugActivity$onItemClicked$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = debugActivity$onItemClicked$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.p007ui.debug.DebugActivity$onItemClicked$1.C61251 r0 = new com.portfolio.platform.p007ui.debug.DebugActivity$onItemClicked$1.C61251(this.this$0, yb4);
            r0.f21778p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.p007ui.debug.DebugActivity$onItemClicked$1.C61251) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                android.widget.Toast.makeText(this.this$0.this$0, "Log will be sent after 1 second", 1).show();
                this.this$0.this$0.finish();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DebugActivity$onItemClicked$1(com.portfolio.platform.p007ui.debug.DebugActivity debugActivity, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = debugActivity;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.p007ui.debug.DebugActivity$onItemClicked$1 debugActivity$onItemClicked$1 = new com.portfolio.platform.p007ui.debug.DebugActivity$onItemClicked$1(this.this$0, yb4);
        debugActivity$onItemClicked$1.f21777p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return debugActivity$onItemClicked$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.p007ui.debug.DebugActivity$onItemClicked$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006b A[RETURN] */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        com.fossil.blesdk.obfuscated.pi4 c;
        com.portfolio.platform.p007ui.debug.DebugActivity$onItemClicked$1.C61251 r3;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f21777p$;
            com.portfolio.platform.service.ShakeFeedbackService B = this.this$0.mo40230B();
            com.portfolio.platform.p007ui.debug.DebugActivity debugActivity = this.this$0;
            this.L$0 = zg42;
            this.label = 1;
            if (B.mo39880a((android.content.Context) debugActivity, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                return a;
            }
            zg4 = zg42;
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            c = com.fossil.blesdk.obfuscated.nh4.m25693c();
            r3 = new com.portfolio.platform.p007ui.debug.DebugActivity$onItemClicked$1.C61251(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 3;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r3, this) == a) {
                return a;
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 3) {
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.L$0 = zg4;
        this.label = 2;
        if (com.fossil.blesdk.obfuscated.ih4.m23524a(1000, this) == a) {
            return a;
        }
        c = com.fossil.blesdk.obfuscated.nh4.m25693c();
        r3 = new com.portfolio.platform.p007ui.debug.DebugActivity$onItemClicked$1.C61251(this, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.label = 3;
        if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r3, this) == a) {
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
