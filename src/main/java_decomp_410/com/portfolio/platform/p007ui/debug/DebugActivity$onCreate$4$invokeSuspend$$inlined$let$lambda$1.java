package com.portfolio.platform.p007ui.debug;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.debug.DebugActivity$onCreate$4$invokeSuspend$$inlined$let$lambda$1 */
public final class DebugActivity$onCreate$4$invokeSuspend$$inlined$let$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.yb4<? super java.util.List<? extends com.portfolio.platform.data.model.DebugFirmwareData>>, java.lang.Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.debug.DebugActivity$onCreate$4 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DebugActivity$onCreate$4$invokeSuspend$$inlined$let$lambda$1(com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.p007ui.debug.DebugActivity$onCreate$4 debugActivity$onCreate$4) {
        super(1, yb4);
        this.this$0 = debugActivity$onCreate$4;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        return new com.portfolio.platform.p007ui.debug.DebugActivity$onCreate$4$invokeSuspend$$inlined$let$lambda$1(yb4, this.this$0);
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj) {
        return ((com.portfolio.platform.p007ui.debug.DebugActivity$onCreate$4$invokeSuspend$$inlined$let$lambda$1) create((com.fossil.blesdk.obfuscated.yb4) obj)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.p007ui.debug.DebugActivity debugActivity = this.this$0.this$0;
            java.lang.String a2 = debugActivity.f21746L;
            this.label = 1;
            obj = debugActivity.mo40232a(a2, (com.fossil.blesdk.obfuscated.yb4<? super java.util.List<com.portfolio.platform.data.model.DebugFirmwareData>>) this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
