package com.portfolio.platform.p007ui;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.BaseActivity$mButtonServiceConnection$1 */
public final class BaseActivity$mButtonServiceConnection$1 implements android.content.ServiceConnection {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.BaseActivity f21732a;

    @DexIgnore
    public BaseActivity$mButtonServiceConnection$1(com.portfolio.platform.p007ui.BaseActivity baseActivity) {
        this.f21732a = baseActivity;
    }

    @DexIgnore
    public void onServiceConnected(android.content.ComponentName componentName, android.os.IBinder iBinder) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(componentName, "name");
        com.fossil.blesdk.obfuscated.kd4.m24411b(iBinder, com.misfit.frameworks.common.constants.Constants.SERVICE);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f21732a.mo40202f(), "Button service connected");
        com.portfolio.platform.p007ui.BaseActivity.f21700A.mo40218a(com.misfit.frameworks.buttonservice.IButtonConnectivity.Stub.asInterface(iBinder));
        this.f21732a.mo40196b(true);
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.p007ui.BaseActivity$mButtonServiceConnection$1$onServiceConnected$1(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    public void onServiceDisconnected(android.content.ComponentName componentName) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(componentName, "name");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.f21732a.mo40202f(), "Button service disconnected");
        this.f21732a.mo40196b(false);
        com.portfolio.platform.p007ui.BaseActivity.f21700A.mo40218a((com.misfit.frameworks.buttonservice.IButtonConnectivity) null);
    }
}
