package com.portfolio.platform.p007ui.goaltracking.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData */
public final class FetchGoalTrackingData extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchGoalTrackingData.C6181b, com.portfolio.platform.CoroutineUseCase.C5605d, com.portfolio.platform.CoroutineUseCase.C5602a> {

    @DexIgnore
    /* renamed from: f */
    public static /* final */ java.lang.String f21922f;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.GoalTrackingRepository f21923d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.portfolio.platform.data.source.UserRepository f21924e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData$a")
    /* renamed from: com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData$a */
    public static final class C6180a {
        @DexIgnore
        public C6180a() {
        }

        @DexIgnore
        public /* synthetic */ C6180a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData$b")
    /* renamed from: com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData$b */
    public static final class C6181b implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.Date f21925a;

        @DexIgnore
        public C6181b(java.util.Date date) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(date, "date");
            this.f21925a = date;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.util.Date mo40382a() {
            return this.f21925a;
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchGoalTrackingData.C6180a((com.fossil.blesdk.obfuscated.fd4) null);
        java.lang.String simpleName = com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchGoalTrackingData.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "FetchGoalTrackingData::class.java.simpleName");
        f21922f = simpleName;
    }
    */

    @DexIgnore
    public FetchGoalTrackingData(com.portfolio.platform.data.source.GoalTrackingRepository goalTrackingRepository, com.portfolio.platform.data.source.UserRepository userRepository) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(goalTrackingRepository, "mGoalTrackingRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(userRepository, "mUserRepository");
        this.f21923d = goalTrackingRepository;
        this.f21924e = userRepository;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f21922f;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchGoalTrackingData.C6181b bVar, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4> yb4) {
        com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchGoalTrackingData$run$1 fetchGoalTrackingData$run$1;
        int i;
        if (yb4 instanceof com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchGoalTrackingData$run$1) {
            fetchGoalTrackingData$run$1 = (com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchGoalTrackingData$run$1) yb4;
            int i2 = fetchGoalTrackingData$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fetchGoalTrackingData$run$1.label = i2 - Integer.MIN_VALUE;
                com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchGoalTrackingData$run$1 fetchGoalTrackingData$run$12 = fetchGoalTrackingData$run$1;
                java.lang.Object obj = fetchGoalTrackingData$run$12.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = fetchGoalTrackingData$run$12.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    if (bVar == null) {
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    java.util.Date a2 = bVar.mo40382a();
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String str = f21922f;
                    local.mo33255d(str, "executeUseCase - date=" + com.fossil.blesdk.obfuscated.ft3.m22452a(a2));
                    com.portfolio.platform.data.model.MFUser currentUser = this.f21924e.getCurrentUser();
                    if (currentUser == null || android.text.TextUtils.isEmpty(currentUser.getCreatedAt())) {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String str2 = f21922f;
                        local2.mo33255d(str2, "executeUseCase - FAILED!!! with user=" + currentUser);
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    java.util.Date d = com.fossil.blesdk.obfuscated.rk2.m27394d(currentUser.getCreatedAt());
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String str3 = f21922f;
                    java.lang.StringBuilder sb = new java.lang.StringBuilder();
                    sb.append("executeUseCase - createdDate=");
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) d, "createdDate");
                    sb.append(com.fossil.blesdk.obfuscated.ft3.m22452a(d));
                    local3.mo33255d(str3, sb.toString());
                    if (com.fossil.blesdk.obfuscated.rk2.m27383b(d, a2) || com.fossil.blesdk.obfuscated.rk2.m27383b(a2, new java.util.Date())) {
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    java.util.Calendar p = com.fossil.blesdk.obfuscated.rk2.m27411p(a2);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) p, "DateHelper.getStartOfWeek(date)");
                    java.util.Date time = p.getTime();
                    if (com.fossil.blesdk.obfuscated.rk2.m27391c(d, time)) {
                        time = d;
                    }
                    com.portfolio.platform.data.source.GoalTrackingRepository goalTrackingRepository = this.f21923d;
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) time, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_START_DATE);
                    fetchGoalTrackingData$run$12.L$0 = this;
                    fetchGoalTrackingData$run$12.L$1 = bVar;
                    fetchGoalTrackingData$run$12.L$2 = a2;
                    fetchGoalTrackingData$run$12.L$3 = currentUser;
                    fetchGoalTrackingData$run$12.L$4 = d;
                    fetchGoalTrackingData$run$12.L$5 = time;
                    fetchGoalTrackingData$run$12.label = 1;
                    if (com.portfolio.platform.data.source.GoalTrackingRepository.loadGoalTrackingDataList$default(goalTrackingRepository, time, a2, 0, 0, fetchGoalTrackingData$run$12, 12, (java.lang.Object) null) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    java.util.Date date = (java.util.Date) fetchGoalTrackingData$run$12.L$5;
                    java.util.Date date2 = (java.util.Date) fetchGoalTrackingData$run$12.L$4;
                    com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) fetchGoalTrackingData$run$12.L$3;
                    java.util.Date date3 = (java.util.Date) fetchGoalTrackingData$run$12.L$2;
                    com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchGoalTrackingData.C6181b bVar2 = (com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchGoalTrackingData.C6181b) fetchGoalTrackingData$run$12.L$1;
                    com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchGoalTrackingData fetchGoalTrackingData = (com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchGoalTrackingData) fetchGoalTrackingData$run$12.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }
        fetchGoalTrackingData$run$1 = new com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchGoalTrackingData$run$1(this, yb4);
        com.portfolio.platform.p007ui.goaltracking.domain.usecase.FetchGoalTrackingData$run$1 fetchGoalTrackingData$run$122 = fetchGoalTrackingData$run$1;
        java.lang.Object obj2 = fetchGoalTrackingData$run$122.result;
        java.lang.Object a3 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = fetchGoalTrackingData$run$122.label;
        if (i != 0) {
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
