package com.portfolio.platform.p007ui.stats.sleep.month.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries", mo27670f = "FetchSleepSummaries.kt", mo27671l = {61}, mo27672m = "run")
/* renamed from: com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries$run$1 */
public final class FetchSleepSummaries$run$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public java.lang.Object L$7;
    @DexIgnore
    public java.lang.Object L$8;
    @DexIgnore
    public java.lang.Object L$9;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.stats.sleep.month.domain.usecase.FetchSleepSummaries this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FetchSleepSummaries$run$1(com.portfolio.platform.p007ui.stats.sleep.month.domain.usecase.FetchSleepSummaries fetchSleepSummaries, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = fetchSleepSummaries;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.mo26310a((com.portfolio.platform.p007ui.stats.sleep.month.domain.usecase.FetchSleepSummaries.C6195b) null, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this);
    }
}
