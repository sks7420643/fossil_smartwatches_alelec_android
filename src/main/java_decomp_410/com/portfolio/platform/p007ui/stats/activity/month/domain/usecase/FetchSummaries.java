package com.portfolio.platform.p007ui.stats.activity.month.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries */
public final class FetchSummaries extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.stats.activity.month.domain.usecase.FetchSummaries.C6191b, com.portfolio.platform.CoroutineUseCase.C5605d, com.portfolio.platform.CoroutineUseCase.C5602a> {

    @DexIgnore
    /* renamed from: h */
    public static /* final */ java.lang.String f21943h;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.SummariesRepository f21944d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.portfolio.platform.data.source.FitnessDataRepository f21945e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.portfolio.platform.data.source.UserRepository f21946f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.portfolio.platform.data.source.ActivitiesRepository f21947g;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries$a")
    /* renamed from: com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries$a */
    public static final class C6190a {
        @DexIgnore
        public C6190a() {
        }

        @DexIgnore
        public /* synthetic */ C6190a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries$b")
    /* renamed from: com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries$b */
    public static final class C6191b implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.Date f21948a;

        @DexIgnore
        public C6191b(java.util.Date date) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(date, "date");
            this.f21948a = date;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.util.Date mo40392a() {
            return this.f21948a;
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.stats.activity.month.domain.usecase.FetchSummaries.C6190a((com.fossil.blesdk.obfuscated.fd4) null);
        java.lang.String simpleName = com.portfolio.platform.p007ui.stats.activity.month.domain.usecase.FetchSummaries.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "FetchSummaries::class.java.simpleName");
        f21943h = simpleName;
    }
    */

    @DexIgnore
    public FetchSummaries(com.portfolio.platform.data.source.SummariesRepository summariesRepository, com.portfolio.platform.data.source.FitnessDataRepository fitnessDataRepository, com.portfolio.platform.data.source.UserRepository userRepository, com.portfolio.platform.data.source.ActivitiesRepository activitiesRepository) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(summariesRepository, "mSummariesRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(fitnessDataRepository, "mFitnessDataRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(userRepository, "mUserRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(activitiesRepository, "mActivitiesRepository");
        this.f21944d = summariesRepository;
        this.f21945e = fitnessDataRepository;
        this.f21946f = userRepository;
        this.f21947g = activitiesRepository;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f21943h;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.stats.activity.month.domain.usecase.FetchSummaries.C6191b bVar, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4> yb4) {
        com.portfolio.platform.p007ui.stats.activity.month.domain.usecase.FetchSummaries$run$1 fetchSummaries$run$1;
        int i;
        java.util.Date date;
        java.util.Date date2;
        if (yb4 instanceof com.portfolio.platform.p007ui.stats.activity.month.domain.usecase.FetchSummaries$run$1) {
            fetchSummaries$run$1 = (com.portfolio.platform.p007ui.stats.activity.month.domain.usecase.FetchSummaries$run$1) yb4;
            int i2 = fetchSummaries$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fetchSummaries$run$1.label = i2 - Integer.MIN_VALUE;
                java.lang.Object obj = fetchSummaries$run$1.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = fetchSummaries$run$1.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    if (bVar == null) {
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    java.util.Date a2 = bVar.mo40392a();
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String str = f21943h;
                    local.mo33255d(str, "executeUseCase - date=" + com.fossil.blesdk.obfuscated.ft3.m22452a(a2));
                    com.portfolio.platform.data.model.MFUser currentUser = this.f21946f.getCurrentUser();
                    if (currentUser == null || android.text.TextUtils.isEmpty(currentUser.getCreatedAt())) {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String str2 = f21943h;
                        local2.mo33255d(str2, "executeUseCase - FAILED!!! with user=" + currentUser);
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    java.util.Date d = com.fossil.blesdk.obfuscated.rk2.m27394d(currentUser.getCreatedAt());
                    java.util.Calendar instance = java.util.Calendar.getInstance();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "calendar");
                    instance.setTime(a2);
                    instance.set(5, 1);
                    long timeInMillis = instance.getTimeInMillis();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) d, "createdDate");
                    if (com.fossil.blesdk.obfuscated.rk2.m27370a(timeInMillis, d.getTime())) {
                        date = d;
                    } else {
                        java.util.Calendar e = com.fossil.blesdk.obfuscated.rk2.m27398e(instance);
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.getStartOfMonth(calendar)");
                        date = e.getTime();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date, "DateHelper.getStartOfMonth(calendar).time");
                        if (com.fossil.blesdk.obfuscated.rk2.m27383b(d, date)) {
                            return com.fossil.blesdk.obfuscated.qa4.f17909a;
                        }
                    }
                    java.lang.Boolean r = com.fossil.blesdk.obfuscated.rk2.m27413r(date);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) r, "DateHelper.isThisMonth(startDate)");
                    if (r.booleanValue()) {
                        date2 = new java.util.Date();
                    } else {
                        java.util.Calendar j = com.fossil.blesdk.obfuscated.rk2.m27405j(date);
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) j, "DateHelper.getEndOfMonth(startDate)");
                        date2 = j.getTime();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date2, "DateHelper.getEndOfMonth(startDate).time");
                    }
                    java.util.List<com.portfolio.platform.data.model.room.fitness.SampleRaw> pendingActivities = this.f21947g.getPendingActivities(date, date2);
                    java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> fitnessData = this.f21945e.getFitnessData(date, date2);
                    if (pendingActivities.isEmpty() && fitnessData.isEmpty()) {
                        com.portfolio.platform.data.source.SummariesRepository summariesRepository = this.f21944d;
                        fetchSummaries$run$1.L$0 = this;
                        fetchSummaries$run$1.L$1 = bVar;
                        fetchSummaries$run$1.L$2 = a2;
                        fetchSummaries$run$1.L$3 = currentUser;
                        fetchSummaries$run$1.L$4 = d;
                        fetchSummaries$run$1.L$5 = instance;
                        fetchSummaries$run$1.L$6 = date2;
                        fetchSummaries$run$1.L$7 = date;
                        fetchSummaries$run$1.L$8 = pendingActivities;
                        fetchSummaries$run$1.L$9 = fitnessData;
                        fetchSummaries$run$1.label = 1;
                        if (summariesRepository.loadSummaries(date, date2, fetchSummaries$run$1) == a) {
                            return a;
                        }
                    }
                } else if (i == 1) {
                    java.util.List list = (java.util.List) fetchSummaries$run$1.L$9;
                    java.util.List list2 = (java.util.List) fetchSummaries$run$1.L$8;
                    java.util.Date date3 = (java.util.Date) fetchSummaries$run$1.L$7;
                    java.util.Date date4 = (java.util.Date) fetchSummaries$run$1.L$6;
                    java.util.Calendar calendar = (java.util.Calendar) fetchSummaries$run$1.L$5;
                    java.util.Date date5 = (java.util.Date) fetchSummaries$run$1.L$4;
                    com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) fetchSummaries$run$1.L$3;
                    java.util.Date date6 = (java.util.Date) fetchSummaries$run$1.L$2;
                    com.portfolio.platform.p007ui.stats.activity.month.domain.usecase.FetchSummaries.C6191b bVar2 = (com.portfolio.platform.p007ui.stats.activity.month.domain.usecase.FetchSummaries.C6191b) fetchSummaries$run$1.L$1;
                    com.portfolio.platform.p007ui.stats.activity.month.domain.usecase.FetchSummaries fetchSummaries = (com.portfolio.platform.p007ui.stats.activity.month.domain.usecase.FetchSummaries) fetchSummaries$run$1.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }
        fetchSummaries$run$1 = new com.portfolio.platform.p007ui.stats.activity.month.domain.usecase.FetchSummaries$run$1(this, yb4);
        java.lang.Object obj2 = fetchSummaries$run$1.result;
        java.lang.Object a3 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = fetchSummaries$run$1.label;
        if (i != 0) {
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
