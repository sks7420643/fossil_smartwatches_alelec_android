package com.portfolio.platform.p007ui.stats.activity.day.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities */
public final class FetchActivities extends com.portfolio.platform.CoroutineUseCase<com.portfolio.platform.p007ui.stats.activity.day.domain.usecase.FetchActivities.C6189b, com.portfolio.platform.CoroutineUseCase.C5605d, com.portfolio.platform.CoroutineUseCase.C5602a> {

    @DexIgnore
    /* renamed from: g */
    public static /* final */ java.lang.String f21938g;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.portfolio.platform.data.source.ActivitiesRepository f21939d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.portfolio.platform.data.source.UserRepository f21940e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.portfolio.platform.data.source.FitnessDataRepository f21941f;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities$a")
    /* renamed from: com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities$a */
    public static final class C6188a {
        @DexIgnore
        public C6188a() {
        }

        @DexIgnore
        public /* synthetic */ C6188a(com.fossil.blesdk.obfuscated.fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities$b")
    /* renamed from: com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities$b */
    public static final class C6189b implements com.portfolio.platform.CoroutineUseCase.C5603b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.Date f21942a;

        @DexIgnore
        public C6189b(java.util.Date date) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(date, "date");
            this.f21942a = date;
        }

        @DexIgnore
        /* renamed from: a */
        public final java.util.Date mo40390a() {
            return this.f21942a;
        }
    }

    /*
    static {
        new com.portfolio.platform.p007ui.stats.activity.day.domain.usecase.FetchActivities.C6188a((com.fossil.blesdk.obfuscated.fd4) null);
        java.lang.String simpleName = com.portfolio.platform.p007ui.stats.activity.day.domain.usecase.FetchActivities.class.getSimpleName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) simpleName, "FetchActivities::class.java.simpleName");
        f21938g = simpleName;
    }
    */

    @DexIgnore
    public FetchActivities(com.portfolio.platform.data.source.ActivitiesRepository activitiesRepository, com.portfolio.platform.data.source.UserRepository userRepository, com.portfolio.platform.data.source.FitnessDataRepository fitnessDataRepository) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(activitiesRepository, "mActivitiesRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(userRepository, "mUserRepository");
        com.fossil.blesdk.obfuscated.kd4.m24411b(fitnessDataRepository, "mFitnessDataRepository");
        this.f21939d = activitiesRepository;
        this.f21940e = userRepository;
        this.f21941f = fitnessDataRepository;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo26311c() {
        return f21938g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* renamed from: a */
    public java.lang.Object mo26310a(com.portfolio.platform.p007ui.stats.activity.day.domain.usecase.FetchActivities.C6189b bVar, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4> yb4) {
        com.portfolio.platform.p007ui.stats.activity.day.domain.usecase.FetchActivities$run$1 fetchActivities$run$1;
        int i;
        if (yb4 instanceof com.portfolio.platform.p007ui.stats.activity.day.domain.usecase.FetchActivities$run$1) {
            fetchActivities$run$1 = (com.portfolio.platform.p007ui.stats.activity.day.domain.usecase.FetchActivities$run$1) yb4;
            int i2 = fetchActivities$run$1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fetchActivities$run$1.label = i2 - Integer.MIN_VALUE;
                com.portfolio.platform.p007ui.stats.activity.day.domain.usecase.FetchActivities$run$1 fetchActivities$run$12 = fetchActivities$run$1;
                java.lang.Object obj = fetchActivities$run$12.result;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                i = fetchActivities$run$12.label;
                if (i != 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    if (bVar == null) {
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    java.util.Date a2 = bVar.mo40390a();
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String str = f21938g;
                    local.mo33255d(str, "executeUseCase - date=" + com.fossil.blesdk.obfuscated.ft3.m22452a(a2));
                    com.portfolio.platform.data.model.MFUser currentUser = this.f21940e.getCurrentUser();
                    if (currentUser == null || android.text.TextUtils.isEmpty(currentUser.getCreatedAt())) {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String str2 = f21938g;
                        local2.mo33255d(str2, "executeUseCase - FAILED!!! with user=" + currentUser);
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    java.util.Date d = com.fossil.blesdk.obfuscated.rk2.m27394d(currentUser.getCreatedAt());
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String str3 = f21938g;
                    java.lang.StringBuilder sb = new java.lang.StringBuilder();
                    sb.append("executeUseCase - createdDate=");
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) d, "createdDate");
                    sb.append(com.fossil.blesdk.obfuscated.ft3.m22452a(d));
                    local3.mo33255d(str3, sb.toString());
                    if (com.fossil.blesdk.obfuscated.rk2.m27383b(d, a2) || com.fossil.blesdk.obfuscated.rk2.m27383b(a2, new java.util.Date())) {
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    java.util.Calendar p = com.fossil.blesdk.obfuscated.rk2.m27411p(a2);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) p, "DateHelper.getStartOfWeek(date)");
                    java.util.Date time = p.getTime();
                    if (com.fossil.blesdk.obfuscated.rk2.m27391c(d, time)) {
                        time = d;
                    }
                    com.portfolio.platform.data.source.FitnessDataRepository fitnessDataRepository = this.f21941f;
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) time, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_START_DATE);
                    java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> fitnessData = fitnessDataRepository.getFitnessData(time, a2);
                    if (fitnessData.isEmpty()) {
                        com.portfolio.platform.data.source.ActivitiesRepository activitiesRepository = this.f21939d;
                        fetchActivities$run$12.L$0 = this;
                        fetchActivities$run$12.L$1 = bVar;
                        fetchActivities$run$12.L$2 = a2;
                        fetchActivities$run$12.L$3 = currentUser;
                        fetchActivities$run$12.L$4 = d;
                        fetchActivities$run$12.L$5 = time;
                        fetchActivities$run$12.L$6 = fitnessData;
                        fetchActivities$run$12.label = 1;
                        if (com.portfolio.platform.data.source.ActivitiesRepository.fetchActivitySamples$default(activitiesRepository, time, a2, 0, 0, fetchActivities$run$12, 12, (java.lang.Object) null) == a) {
                            return a;
                        }
                    }
                } else if (i == 1) {
                    java.util.List list = (java.util.List) fetchActivities$run$12.L$6;
                    java.util.Date date = (java.util.Date) fetchActivities$run$12.L$5;
                    java.util.Date date2 = (java.util.Date) fetchActivities$run$12.L$4;
                    com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) fetchActivities$run$12.L$3;
                    java.util.Date date3 = (java.util.Date) fetchActivities$run$12.L$2;
                    com.portfolio.platform.p007ui.stats.activity.day.domain.usecase.FetchActivities.C6189b bVar2 = (com.portfolio.platform.p007ui.stats.activity.day.domain.usecase.FetchActivities.C6189b) fetchActivities$run$12.L$1;
                    com.portfolio.platform.p007ui.stats.activity.day.domain.usecase.FetchActivities fetchActivities = (com.portfolio.platform.p007ui.stats.activity.day.domain.usecase.FetchActivities) fetchActivities$run$12.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }
        fetchActivities$run$1 = new com.portfolio.platform.p007ui.stats.activity.day.domain.usecase.FetchActivities$run$1(this, yb4);
        com.portfolio.platform.p007ui.stats.activity.day.domain.usecase.FetchActivities$run$1 fetchActivities$run$122 = fetchActivities$run$1;
        java.lang.Object obj2 = fetchActivities$run$122.result;
        java.lang.Object a3 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        i = fetchActivities$run$122.label;
        if (i != 0) {
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
