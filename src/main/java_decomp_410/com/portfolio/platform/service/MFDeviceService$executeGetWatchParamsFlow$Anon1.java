package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.WatchParamHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.MFDeviceService$executeGetWatchParamsFlow$Anon1", f = "MFDeviceService.kt", l = {668, 670, 672}, m = "invokeSuspend")
public final class MFDeviceService$executeGetWatchParamsFlow$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ float $currentWPVersion;
    @DexIgnore
    public /* final */ /* synthetic */ int $majorVersion;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$executeGetWatchParamsFlow$Anon1(MFDeviceService mFDeviceService, String str, int i, float f, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = mFDeviceService;
        this.$serial = str;
        this.$majorVersion = i;
        this.$currentWPVersion = f;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MFDeviceService$executeGetWatchParamsFlow$Anon1 mFDeviceService$executeGetWatchParamsFlow$Anon1 = new MFDeviceService$executeGetWatchParamsFlow$Anon1(this.this$Anon0, this.$serial, this.$majorVersion, this.$currentWPVersion, yb4);
        mFDeviceService$executeGetWatchParamsFlow$Anon1.p$ = (zg4) obj;
        return mFDeviceService$executeGetWatchParamsFlow$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFDeviceService$executeGetWatchParamsFlow$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            DeviceRepository d = this.this$Anon0.d();
            String str = this.$serial;
            int i2 = this.$majorVersion;
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = d.getLatestWatchParamFromServer(str, i2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2 || i == 3) {
            WatchParameterResponse watchParameterResponse = (WatchParameterResponse) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        WatchParameterResponse watchParameterResponse2 = (WatchParameterResponse) obj;
        if (watchParameterResponse2 != null) {
            WatchParamHelper t = this.this$Anon0.t();
            String str2 = this.$serial;
            float f = this.$currentWPVersion;
            this.L$Anon0 = zg4;
            this.L$Anon1 = watchParameterResponse2;
            this.label = 2;
            if (t.a(str2, f, watchParameterResponse2, this) == a) {
                return a;
            }
        } else {
            WatchParamHelper t2 = this.this$Anon0.t();
            String str3 = this.$serial;
            float f2 = this.$currentWPVersion;
            this.L$Anon0 = zg4;
            this.L$Anon1 = watchParameterResponse2;
            this.label = 3;
            if (t2.a(str3, f2, this) == a) {
                return a;
            }
        }
        return qa4.a;
    }
}
