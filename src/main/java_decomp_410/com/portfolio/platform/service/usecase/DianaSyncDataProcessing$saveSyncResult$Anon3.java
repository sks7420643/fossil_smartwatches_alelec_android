package com.portfolio.platform.service.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.util.SyncDataExtensionsKt;
import java.util.Date;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$Anon3", f = "DianaSyncDataProcessing.kt", l = {238, 243}, m = "invokeSuspend")
public final class DianaSyncDataProcessing$saveSyncResult$Anon3 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository $activityRepository;
    @DexIgnore
    public /* final */ /* synthetic */ FitnessDataRepository $fitnessDataRepository;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSampleRepository $heartRateSampleRepository;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryRepository $heartRateSummaryRepository;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository $sleepSessionsRepository;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository $sleepSummariesRepository;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository $summaryRepository;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository $workoutSessionRepository;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$Anon3$Anon1", f = "DianaSyncDataProcessing.kt", l = {255}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super ActivityStatistic>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public Object L$Anon2;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncDataProcessing$saveSyncResult$Anon3 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$Anon3$Anon1$Anon1")
        @gc4(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$Anon3$Anon1$Anon1", f = "DianaSyncDataProcessing.kt", l = {256, 257, 259, 260, 262, 263, 265, 266}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$Anon3$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0127Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super ActivityStatistic>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Ref$ObjectRef $endDate;
            @DexIgnore
            public /* final */ /* synthetic */ Ref$ObjectRef $startDate;
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0127Anon1(Anon1 anon1, Ref$ObjectRef ref$ObjectRef, Ref$ObjectRef ref$ObjectRef2, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
                this.$startDate = ref$ObjectRef;
                this.$endDate = ref$ObjectRef2;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0127Anon1 anon1 = new C0127Anon1(this.this$Anon0, this.$startDate, this.$endDate, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0127Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            /* JADX WARNING: Code restructure failed: missing block: B:15:0x0088, code lost:
                r12 = r11.this$Anon0.this$Anon0.$summaryRepository;
                r3 = com.portfolio.platform.util.SyncDataExtensionsKt.a((org.joda.time.DateTime) r11.$startDate.element, true);
                r4 = com.portfolio.platform.util.SyncDataExtensionsKt.a((org.joda.time.DateTime) r11.$endDate.element, true);
                r11.L$Anon0 = r1;
                r11.label = 2;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:16:0x00ab, code lost:
                if (r12.loadSummaries(r3, r4, r11) != r0) goto L_0x00ae;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:17:0x00ad, code lost:
                return r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:18:0x00ae, code lost:
                r3 = r11.this$Anon0.this$Anon0.$sleepSessionsRepository;
                r4 = com.portfolio.platform.util.SyncDataExtensionsKt.a((org.joda.time.DateTime) r11.$startDate.element, true);
                r5 = com.portfolio.platform.util.SyncDataExtensionsKt.a((org.joda.time.DateTime) r11.$endDate.element, true);
                r11.L$Anon0 = r1;
                r11.label = 3;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:19:0x00d7, code lost:
                if (com.portfolio.platform.data.source.SleepSessionsRepository.fetchSleepSessions$default(r3, r4, r5, 0, 0, r11, 12, (java.lang.Object) null) != r0) goto L_0x00da;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:20:0x00d9, code lost:
                return r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:21:0x00da, code lost:
                r12 = r11.this$Anon0.this$Anon0.$sleepSummariesRepository;
                r3 = com.portfolio.platform.util.SyncDataExtensionsKt.a((org.joda.time.DateTime) r11.$startDate.element, true);
                r4 = com.portfolio.platform.util.SyncDataExtensionsKt.a((org.joda.time.DateTime) r11.$endDate.element, true);
                r11.L$Anon0 = r1;
                r11.label = 4;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:22:0x00fd, code lost:
                if (r12.fetchSleepSummaries(r3, r4, r11) != r0) goto L_0x0100;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:23:0x00ff, code lost:
                return r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:24:0x0100, code lost:
                r3 = r11.this$Anon0.this$Anon0.$heartRateSampleRepository;
                r4 = com.portfolio.platform.util.SyncDataExtensionsKt.a((org.joda.time.DateTime) r11.$startDate.element, true);
                r5 = com.portfolio.platform.util.SyncDataExtensionsKt.a((org.joda.time.DateTime) r11.$endDate.element, true);
                r11.L$Anon0 = r1;
                r11.label = 5;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:25:0x0129, code lost:
                if (com.portfolio.platform.data.source.HeartRateSampleRepository.fetchHeartRateSamples$default(r3, r4, r5, 0, 0, r11, 12, (java.lang.Object) null) != r0) goto L_0x012c;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:26:0x012b, code lost:
                return r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:27:0x012c, code lost:
                r12 = r11.this$Anon0.this$Anon0.$heartRateSummaryRepository;
                r3 = com.portfolio.platform.util.SyncDataExtensionsKt.a((org.joda.time.DateTime) r11.$startDate.element, true);
                r4 = com.portfolio.platform.util.SyncDataExtensionsKt.a((org.joda.time.DateTime) r11.$endDate.element, true);
                r11.L$Anon0 = r1;
                r11.label = 6;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:28:0x014f, code lost:
                if (r12.loadSummaries(r3, r4, r11) != r0) goto L_0x0152;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:29:0x0151, code lost:
                return r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:30:0x0152, code lost:
                r3 = r11.this$Anon0.this$Anon0.$workoutSessionRepository;
                r4 = com.portfolio.platform.util.SyncDataExtensionsKt.a((org.joda.time.DateTime) r11.$startDate.element, true);
                r5 = com.portfolio.platform.util.SyncDataExtensionsKt.a((org.joda.time.DateTime) r11.$endDate.element, true);
                r11.L$Anon0 = r1;
                r11.label = 7;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:31:0x017b, code lost:
                if (com.portfolio.platform.data.source.WorkoutSessionRepository.fetchWorkoutSessions$default(r3, r4, r5, 0, 0, r11, 12, (java.lang.Object) null) != r0) goto L_0x017e;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:32:0x017d, code lost:
                return r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:33:0x017e, code lost:
                r12 = r11.this$Anon0.this$Anon0.$summaryRepository;
                r11.L$Anon0 = r1;
                r11.label = 8;
                r12 = r12.fetchActivityStatistic(r11);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:34:0x018e, code lost:
                if (r12 != r0) goto L_0x0191;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:35:0x0190, code lost:
                return r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:36:0x0191, code lost:
                return r12;
             */
            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                zg4 zg4;
                Object a = cc4.a();
                switch (this.label) {
                    case 0:
                        na4.a(obj);
                        zg4 = this.p$;
                        ActivitiesRepository activitiesRepository = this.this$Anon0.this$Anon0.$activityRepository;
                        Date a2 = SyncDataExtensionsKt.a((DateTime) this.$startDate.element, true);
                        Date a3 = SyncDataExtensionsKt.a((DateTime) this.$endDate.element, true);
                        this.L$Anon0 = zg4;
                        this.label = 1;
                        if (ActivitiesRepository.fetchActivitySamples$default(activitiesRepository, a2, a3, 0, 0, this, 12, (Object) null) == a) {
                            return a;
                        }
                        break;
                    case 1:
                        zg4 = (zg4) this.L$Anon0;
                        na4.a(obj);
                        break;
                    case 2:
                        zg4 = (zg4) this.L$Anon0;
                        na4.a(obj);
                        break;
                    case 3:
                        zg4 = (zg4) this.L$Anon0;
                        na4.a(obj);
                        break;
                    case 4:
                        zg4 = (zg4) this.L$Anon0;
                        na4.a(obj);
                        break;
                    case 5:
                        zg4 = (zg4) this.L$Anon0;
                        na4.a(obj);
                        break;
                    case 6:
                        zg4 = (zg4) this.L$Anon0;
                        na4.a(obj);
                        break;
                    case 7:
                        zg4 = (zg4) this.L$Anon0;
                        na4.a(obj);
                        break;
                    case 8:
                        zg4 zg42 = (zg4) this.L$Anon0;
                        na4.a(obj);
                        break;
                    default:
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(DianaSyncDataProcessing$saveSyncResult$Anon3 dianaSyncDataProcessing$saveSyncResult$Anon3, List list, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = dianaSyncDataProcessing$saveSyncResult$Anon3;
            this.$fitnessDataList = list;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$fitnessDataList, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                ref$ObjectRef.element = ((FitnessDataWrapper) this.$fitnessDataList.get(0)).getStartTimeTZ();
                Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
                ref$ObjectRef2.element = ((FitnessDataWrapper) this.$fitnessDataList.get(0)).getEndTimeTZ();
                for (FitnessDataWrapper fitnessDataWrapper : this.$fitnessDataList) {
                    if (fitnessDataWrapper.getStartLongTime() < ((DateTime) ref$ObjectRef.element).getMillis()) {
                        ref$ObjectRef.element = fitnessDataWrapper.getStartTimeTZ();
                    }
                    if (fitnessDataWrapper.getEndLongTime() > ((DateTime) ref$ObjectRef2.element).getMillis()) {
                        ref$ObjectRef2.element = fitnessDataWrapper.getEndTimeTZ();
                    }
                }
                ug4 b = nh4.b();
                C0127Anon1 anon1 = new C0127Anon1(this, ref$ObjectRef, ref$ObjectRef2, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = ref$ObjectRef;
                this.L$Anon2 = ref$ObjectRef2;
                this.label = 1;
                obj = yf4.a(b, anon1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                Ref$ObjectRef ref$ObjectRef3 = (Ref$ObjectRef) this.L$Anon2;
                Ref$ObjectRef ref$ObjectRef4 = (Ref$ObjectRef) this.L$Anon1;
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaSyncDataProcessing$saveSyncResult$Anon3(FitnessDataRepository fitnessDataRepository, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, SleepSessionsRepository sleepSessionsRepository, SleepSummariesRepository sleepSummariesRepository, HeartRateSampleRepository heartRateSampleRepository, HeartRateSummaryRepository heartRateSummaryRepository, WorkoutSessionRepository workoutSessionRepository, yb4 yb4) {
        super(2, yb4);
        this.$fitnessDataRepository = fitnessDataRepository;
        this.$activityRepository = activitiesRepository;
        this.$summaryRepository = summariesRepository;
        this.$sleepSessionsRepository = sleepSessionsRepository;
        this.$sleepSummariesRepository = sleepSummariesRepository;
        this.$heartRateSampleRepository = heartRateSampleRepository;
        this.$heartRateSummaryRepository = heartRateSummaryRepository;
        this.$workoutSessionRepository = workoutSessionRepository;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DianaSyncDataProcessing$saveSyncResult$Anon3 dianaSyncDataProcessing$saveSyncResult$Anon3 = new DianaSyncDataProcessing$saveSyncResult$Anon3(this.$fitnessDataRepository, this.$activityRepository, this.$summaryRepository, this.$sleepSessionsRepository, this.$sleepSummariesRepository, this.$heartRateSampleRepository, this.$heartRateSummaryRepository, this.$workoutSessionRepository, yb4);
        dianaSyncDataProcessing$saveSyncResult$Anon3.p$ = (zg4) obj;
        return dianaSyncDataProcessing$saveSyncResult$Anon3;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DianaSyncDataProcessing$saveSyncResult$Anon3) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            FitnessDataRepository fitnessDataRepository = this.$fitnessDataRepository;
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = fitnessDataRepository.pushPendingFitnessData(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            List list = (List) this.L$Anon2;
            qo2 qo2 = (qo2) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo22 = (qo2) obj;
        if (qo22 instanceof ro2) {
            List list2 = (List) ((ro2) qo22).a();
            if (list2 != null && (true ^ list2.isEmpty())) {
                ug4 a2 = nh4.a();
                Anon1 anon1 = new Anon1(this, list2, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = qo22;
                this.L$Anon2 = list2;
                this.label = 2;
                if (yf4.a(a2, anon1, this) == a) {
                    return a;
                }
            }
        } else {
            boolean z = qo22 instanceof po2;
        }
        return qa4.a;
    }
}
