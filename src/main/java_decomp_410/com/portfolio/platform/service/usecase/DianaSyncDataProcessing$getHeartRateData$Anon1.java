package com.portfolio.platform.service.usecase;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaSyncDataProcessing$getHeartRateData$Anon1 extends Lambda implements xc4<FitnessDataWrapper, Integer> {
    @DexIgnore
    public static /* final */ DianaSyncDataProcessing$getHeartRateData$Anon1 INSTANCE; // = new DianaSyncDataProcessing$getHeartRateData$Anon1();

    @DexIgnore
    public DianaSyncDataProcessing$getHeartRateData$Anon1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Integer.valueOf(invoke((FitnessDataWrapper) obj));
    }

    @DexIgnore
    public final int invoke(FitnessDataWrapper fitnessDataWrapper) {
        kd4.b(fitnessDataWrapper, "it");
        return fitnessDataWrapper.getTimezoneOffsetInSecond();
    }
}
