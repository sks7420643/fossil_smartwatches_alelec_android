package com.portfolio.platform.service.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3", mo27670f = "HybridSyncDataProcessing.kt", mo27671l = {188, 195}, mo27672m = "invokeSuspend")
public final class HybridSyncDataProcessing$saveSyncResult$3 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ActivitiesRepository $activityRepository;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.FitnessDataRepository $fitnessDataRepository;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSessionsRepository $sleepSessionsRepository;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSummariesRepository $sleepSummariesRepository;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SummariesRepository $summaryRepository;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21674p$;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3$1", mo27670f = "HybridSyncDataProcessing.kt", mo27671l = {204}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3$1 */
    public static final class C60971 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.ActivityStatistic>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $startDate;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21675p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3$1$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3$1$1", mo27670f = "HybridSyncDataProcessing.kt", mo27671l = {205, 206, 208, 209, 210}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3$1$1 */
        public static final class C60981 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.ActivityStatistic>, java.lang.Object> {
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f21676p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C60981(com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971.C60981 r0 = new com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971.C60981(this.this$0, yb4);
                r0.f21676p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971.C60981) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:21:0x00c7 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x012e A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:31:0x013f A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                com.fossil.blesdk.obfuscated.zg4 zg4;
                com.portfolio.platform.data.source.SleepSummariesRepository sleepSummariesRepository;
                java.util.Date date;
                java.util.Date date2;
                com.fossil.blesdk.obfuscated.zg4 zg42;
                com.portfolio.platform.data.source.SummariesRepository summariesRepository;
                java.util.Date date3;
                java.util.Date date4;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg43 = this.f21676p$;
                    com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971 r0 = this.this$0;
                    com.portfolio.platform.data.source.ActivitiesRepository activitiesRepository = r0.this$0.$activityRepository;
                    java.util.Date date5 = ((org.joda.time.DateTime) r0.$startDate.element).toDate();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date5, "startDate.toDate()");
                    java.util.Date date6 = ((org.joda.time.DateTime) this.this$0.$endDate.element).toDate();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date6, "endDate.toDate()");
                    this.L$0 = zg43;
                    this.label = 1;
                    com.fossil.blesdk.obfuscated.zg4 zg44 = zg43;
                    if (com.portfolio.platform.data.source.ActivitiesRepository.fetchActivitySamples$default(activitiesRepository, date5, date6, 0, 0, this, 12, (java.lang.Object) null) == a) {
                        return a;
                    }
                    zg42 = zg44;
                    com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971 r1 = this.this$0;
                    summariesRepository = r1.this$0.$summaryRepository;
                    date3 = ((org.joda.time.DateTime) r1.$startDate.element).toDate();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date3, "startDate.toDate()");
                    date4 = ((org.joda.time.DateTime) this.this$0.$endDate.element).toDate();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date4, "endDate.toDate()");
                    this.L$0 = zg42;
                    this.label = 2;
                    if (summariesRepository.loadSummaries(date3, date4, this) == a) {
                    }
                } else if (i == 1) {
                    zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971 r12 = this.this$0;
                    summariesRepository = r12.this$0.$summaryRepository;
                    date3 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date3, "startDate.toDate()");
                    date4 = ((org.joda.time.DateTime) this.this$0.$endDate.element).toDate();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date4, "endDate.toDate()");
                    this.L$0 = zg42;
                    this.label = 2;
                    if (summariesRepository.loadSummaries(date3, date4, this) == a) {
                        return a;
                    }
                } else if (i == 2) {
                    zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else if (i == 3) {
                    zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971 r13 = this.this$0;
                    sleepSummariesRepository = r13.this$0.$sleepSummariesRepository;
                    date = ((org.joda.time.DateTime) r13.$startDate.element).toDate();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date, "startDate.toDate()");
                    date2 = ((org.joda.time.DateTime) this.this$0.$endDate.element).toDate();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date2, "endDate.toDate()");
                    this.L$0 = zg4;
                    this.label = 4;
                    if (sleepSummariesRepository.fetchSleepSummaries(date, date2, this) == a) {
                        return a;
                    }
                    com.portfolio.platform.data.source.SummariesRepository summariesRepository2 = this.this$0.this$0.$summaryRepository;
                    this.L$0 = zg4;
                    this.label = 5;
                    java.lang.Object fetchActivityStatistic = summariesRepository2.fetchActivityStatistic(this);
                    if (fetchActivityStatistic == a) {
                    }
                } else if (i == 4) {
                    zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.portfolio.platform.data.source.SummariesRepository summariesRepository22 = this.this$0.this$0.$summaryRepository;
                    this.L$0 = zg4;
                    this.label = 5;
                    java.lang.Object fetchActivityStatistic2 = summariesRepository22.fetchActivityStatistic(this);
                    return fetchActivityStatistic2 == a ? a : fetchActivityStatistic2;
                } else if (i == 5) {
                    com.fossil.blesdk.obfuscated.zg4 zg45 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    return obj;
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                com.fossil.blesdk.obfuscated.zg4 zg46 = zg42;
                com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971 r02 = this.this$0;
                com.portfolio.platform.data.source.SleepSessionsRepository sleepSessionsRepository = r02.this$0.$sleepSessionsRepository;
                java.util.Date date7 = ((org.joda.time.DateTime) r02.$startDate.element).toDate();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date7, "startDate.toDate()");
                java.util.Date date8 = ((org.joda.time.DateTime) this.this$0.$endDate.element).toDate();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date8, "endDate.toDate()");
                this.L$0 = zg46;
                this.label = 3;
                if (com.portfolio.platform.data.source.SleepSessionsRepository.fetchSleepSessions$default(sleepSessionsRepository, date7, date8, 0, 0, this, 12, (java.lang.Object) null) == a) {
                    return a;
                }
                zg4 = zg46;
                com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971 r132 = this.this$0;
                sleepSummariesRepository = r132.this$0.$sleepSummariesRepository;
                date = ((org.joda.time.DateTime) r132.$startDate.element).toDate();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date, "startDate.toDate()");
                date2 = ((org.joda.time.DateTime) this.this$0.$endDate.element).toDate();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date2, "endDate.toDate()");
                this.L$0 = zg4;
                this.label = 4;
                if (sleepSummariesRepository.fetchSleepSummaries(date, date2, this) == a) {
                }
                com.portfolio.platform.data.source.SummariesRepository summariesRepository222 = this.this$0.this$0.$summaryRepository;
                this.L$0 = zg4;
                this.label = 5;
                java.lang.Object fetchActivityStatistic22 = summariesRepository222.fetchActivityStatistic(this);
                if (fetchActivityStatistic22 == a) {
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C60971(com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3 hybridSyncDataProcessing$saveSyncResult$3, java.util.List list, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = hybridSyncDataProcessing$saveSyncResult$3;
            this.$fitnessDataList = list;
            this.$startDate = ref$ObjectRef;
            this.$endDate = ref$ObjectRef2;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971 r1 = new com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971(this.this$0, this.$fitnessDataList, this.$startDate, this.$endDate, yb4);
            r1.f21675p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r1;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21675p$;
                for (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper fitnessDataWrapper : this.$fitnessDataList) {
                    if (fitnessDataWrapper.getStartLongTime() < ((org.joda.time.DateTime) this.$startDate.element).getMillis()) {
                        this.$startDate.element = fitnessDataWrapper.getStartTimeTZ();
                    }
                    if (fitnessDataWrapper.getEndLongTime() > ((org.joda.time.DateTime) this.$endDate.element).getMillis()) {
                        this.$endDate.element = fitnessDataWrapper.getEndTimeTZ();
                    }
                }
                com.fossil.blesdk.obfuscated.ug4 b = com.fossil.blesdk.obfuscated.nh4.m25692b();
                com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971.C60981 r3 = new com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971.C60981(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b, r3, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridSyncDataProcessing$saveSyncResult$3(com.portfolio.platform.data.source.FitnessDataRepository fitnessDataRepository, com.portfolio.platform.data.source.ActivitiesRepository activitiesRepository, com.portfolio.platform.data.source.SummariesRepository summariesRepository, com.portfolio.platform.data.source.SleepSessionsRepository sleepSessionsRepository, com.portfolio.platform.data.source.SleepSummariesRepository sleepSummariesRepository, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$fitnessDataRepository = fitnessDataRepository;
        this.$activityRepository = activitiesRepository;
        this.$summaryRepository = summariesRepository;
        this.$sleepSessionsRepository = sleepSessionsRepository;
        this.$sleepSummariesRepository = sleepSummariesRepository;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3 hybridSyncDataProcessing$saveSyncResult$3 = new com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3(this.$fitnessDataRepository, this.$activityRepository, this.$summaryRepository, this.$sleepSessionsRepository, this.$sleepSummariesRepository, yb4);
        hybridSyncDataProcessing$saveSyncResult$3.f21674p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return hybridSyncDataProcessing$saveSyncResult$3;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f21674p$;
            com.portfolio.platform.data.source.FitnessDataRepository fitnessDataRepository = this.$fitnessDataRepository;
            this.L$0 = zg4;
            this.label = 1;
            obj = fitnessDataRepository.pushPendingFitnessData(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef = (kotlin.jvm.internal.Ref$ObjectRef) this.L$4;
            kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2 = (kotlin.jvm.internal.Ref$ObjectRef) this.L$3;
            java.util.List list = (java.util.List) this.L$2;
            com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo22 = (com.fossil.blesdk.obfuscated.qo2) obj;
        if (qo22 instanceof com.fossil.blesdk.obfuscated.ro2) {
            java.util.List list2 = (java.util.List) ((com.fossil.blesdk.obfuscated.ro2) qo22).mo30673a();
            if (list2 != null && (true ^ list2.isEmpty())) {
                kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef3 = new kotlin.jvm.internal.Ref$ObjectRef();
                ref$ObjectRef3.element = ((com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) list2.get(0)).getStartTimeTZ();
                kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef4 = new kotlin.jvm.internal.Ref$ObjectRef();
                ref$ObjectRef4.element = ((com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) list2.get(0)).getEndTimeTZ();
                com.fossil.blesdk.obfuscated.ug4 a2 = com.fossil.blesdk.obfuscated.nh4.m25691a();
                com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971 r5 = new com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3.C60971(this, list2, ref$ObjectRef3, ref$ObjectRef4, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = qo22;
                this.L$2 = list2;
                this.L$3 = ref$ObjectRef3;
                this.L$4 = ref$ObjectRef4;
                this.label = 2;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r5, this) == a) {
                    return a;
                }
            }
        } else {
            boolean z = qo22 instanceof com.fossil.blesdk.obfuscated.po2;
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
