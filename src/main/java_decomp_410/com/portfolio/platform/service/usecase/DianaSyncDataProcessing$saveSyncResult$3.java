package com.portfolio.platform.service.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3", mo27670f = "DianaSyncDataProcessing.kt", mo27671l = {238, 243}, mo27672m = "invokeSuspend")
public final class DianaSyncDataProcessing$saveSyncResult$3 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ActivitiesRepository $activityRepository;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.FitnessDataRepository $fitnessDataRepository;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.HeartRateSampleRepository $heartRateSampleRepository;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.HeartRateSummaryRepository $heartRateSummaryRepository;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSessionsRepository $sleepSessionsRepository;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSummariesRepository $sleepSummariesRepository;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SummariesRepository $summaryRepository;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.WorkoutSessionRepository $workoutSessionRepository;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21661p$;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3$1", mo27670f = "DianaSyncDataProcessing.kt", mo27671l = {255}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3$1 */
    public static final class C60931 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.ActivityStatistic>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $fitnessDataList;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public java.lang.Object L$1;
        @DexIgnore
        public java.lang.Object L$2;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21662p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3$1$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3$1$1", mo27670f = "DianaSyncDataProcessing.kt", mo27671l = {256, 257, 259, 260, 262, 263, 265, 266}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3$1$1 */
        public static final class C60941 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.ActivityStatistic>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $endDate;
            @DexIgnore
            public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $startDate;
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f21663p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3.C60931 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C60941(com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3.C60931 r1, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$startDate = ref$ObjectRef;
                this.$endDate = ref$ObjectRef2;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3.C60931.C60941 r0 = new com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3.C60931.C60941(this.this$0, this.$startDate, this.$endDate, yb4);
                r0.f21663p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3.C60931.C60941) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            /* JADX WARNING: Code restructure failed: missing block: B:15:0x0088, code lost:
                r12 = r11.this$0.this$0.$summaryRepository;
                r3 = com.portfolio.platform.util.SyncDataExtensionsKt.m36150a((org.joda.time.DateTime) r11.$startDate.element, true);
                r4 = com.portfolio.platform.util.SyncDataExtensionsKt.m36150a((org.joda.time.DateTime) r11.$endDate.element, true);
                r11.L$0 = r1;
                r11.label = 2;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:16:0x00ab, code lost:
                if (r12.loadSummaries(r3, r4, r11) != r0) goto L_0x00ae;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:17:0x00ad, code lost:
                return r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:18:0x00ae, code lost:
                r3 = r11.this$0.this$0.$sleepSessionsRepository;
                r4 = com.portfolio.platform.util.SyncDataExtensionsKt.m36150a((org.joda.time.DateTime) r11.$startDate.element, true);
                r5 = com.portfolio.platform.util.SyncDataExtensionsKt.m36150a((org.joda.time.DateTime) r11.$endDate.element, true);
                r11.L$0 = r1;
                r11.label = 3;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:19:0x00d7, code lost:
                if (com.portfolio.platform.data.source.SleepSessionsRepository.fetchSleepSessions$default(r3, r4, r5, 0, 0, r11, 12, (java.lang.Object) null) != r0) goto L_0x00da;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:20:0x00d9, code lost:
                return r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:21:0x00da, code lost:
                r12 = r11.this$0.this$0.$sleepSummariesRepository;
                r3 = com.portfolio.platform.util.SyncDataExtensionsKt.m36150a((org.joda.time.DateTime) r11.$startDate.element, true);
                r4 = com.portfolio.platform.util.SyncDataExtensionsKt.m36150a((org.joda.time.DateTime) r11.$endDate.element, true);
                r11.L$0 = r1;
                r11.label = 4;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:22:0x00fd, code lost:
                if (r12.fetchSleepSummaries(r3, r4, r11) != r0) goto L_0x0100;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:23:0x00ff, code lost:
                return r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:24:0x0100, code lost:
                r3 = r11.this$0.this$0.$heartRateSampleRepository;
                r4 = com.portfolio.platform.util.SyncDataExtensionsKt.m36150a((org.joda.time.DateTime) r11.$startDate.element, true);
                r5 = com.portfolio.platform.util.SyncDataExtensionsKt.m36150a((org.joda.time.DateTime) r11.$endDate.element, true);
                r11.L$0 = r1;
                r11.label = 5;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:25:0x0129, code lost:
                if (com.portfolio.platform.data.source.HeartRateSampleRepository.fetchHeartRateSamples$default(r3, r4, r5, 0, 0, r11, 12, (java.lang.Object) null) != r0) goto L_0x012c;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:26:0x012b, code lost:
                return r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:27:0x012c, code lost:
                r12 = r11.this$0.this$0.$heartRateSummaryRepository;
                r3 = com.portfolio.platform.util.SyncDataExtensionsKt.m36150a((org.joda.time.DateTime) r11.$startDate.element, true);
                r4 = com.portfolio.platform.util.SyncDataExtensionsKt.m36150a((org.joda.time.DateTime) r11.$endDate.element, true);
                r11.L$0 = r1;
                r11.label = 6;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:28:0x014f, code lost:
                if (r12.loadSummaries(r3, r4, r11) != r0) goto L_0x0152;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:29:0x0151, code lost:
                return r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:30:0x0152, code lost:
                r3 = r11.this$0.this$0.$workoutSessionRepository;
                r4 = com.portfolio.platform.util.SyncDataExtensionsKt.m36150a((org.joda.time.DateTime) r11.$startDate.element, true);
                r5 = com.portfolio.platform.util.SyncDataExtensionsKt.m36150a((org.joda.time.DateTime) r11.$endDate.element, true);
                r11.L$0 = r1;
                r11.label = 7;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:31:0x017b, code lost:
                if (com.portfolio.platform.data.source.WorkoutSessionRepository.fetchWorkoutSessions$default(r3, r4, r5, 0, 0, r11, 12, (java.lang.Object) null) != r0) goto L_0x017e;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:32:0x017d, code lost:
                return r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:33:0x017e, code lost:
                r12 = r11.this$0.this$0.$summaryRepository;
                r11.L$0 = r1;
                r11.label = 8;
                r12 = r12.fetchActivityStatistic(r11);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:34:0x018e, code lost:
                if (r12 != r0) goto L_0x0191;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:35:0x0190, code lost:
                return r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:36:0x0191, code lost:
                return r12;
             */
            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                com.fossil.blesdk.obfuscated.zg4 zg4;
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                switch (this.label) {
                    case 0:
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        zg4 = this.f21663p$;
                        com.portfolio.platform.data.source.ActivitiesRepository activitiesRepository = this.this$0.this$0.$activityRepository;
                        java.util.Date a2 = com.portfolio.platform.util.SyncDataExtensionsKt.m36150a((org.joda.time.DateTime) this.$startDate.element, true);
                        java.util.Date a3 = com.portfolio.platform.util.SyncDataExtensionsKt.m36150a((org.joda.time.DateTime) this.$endDate.element, true);
                        this.L$0 = zg4;
                        this.label = 1;
                        if (com.portfolio.platform.data.source.ActivitiesRepository.fetchActivitySamples$default(activitiesRepository, a2, a3, 0, 0, this, 12, (java.lang.Object) null) == a) {
                            return a;
                        }
                        break;
                    case 1:
                        zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        break;
                    case 2:
                        zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        break;
                    case 3:
                        zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        break;
                    case 4:
                        zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        break;
                    case 5:
                        zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        break;
                    case 6:
                        zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        break;
                    case 7:
                        zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        break;
                    case 8:
                        com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        break;
                    default:
                        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C60931(com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3 dianaSyncDataProcessing$saveSyncResult$3, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = dianaSyncDataProcessing$saveSyncResult$3;
            this.$fitnessDataList = list;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3.C60931 r0 = new com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3.C60931(this.this$0, this.$fitnessDataList, yb4);
            r0.f21662p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3.C60931) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21662p$;
                kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef = new kotlin.jvm.internal.Ref$ObjectRef();
                ref$ObjectRef.element = ((com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) this.$fitnessDataList.get(0)).getStartTimeTZ();
                kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2 = new kotlin.jvm.internal.Ref$ObjectRef();
                ref$ObjectRef2.element = ((com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) this.$fitnessDataList.get(0)).getEndTimeTZ();
                for (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper fitnessDataWrapper : this.$fitnessDataList) {
                    if (fitnessDataWrapper.getStartLongTime() < ((org.joda.time.DateTime) ref$ObjectRef.element).getMillis()) {
                        ref$ObjectRef.element = fitnessDataWrapper.getStartTimeTZ();
                    }
                    if (fitnessDataWrapper.getEndLongTime() > ((org.joda.time.DateTime) ref$ObjectRef2.element).getMillis()) {
                        ref$ObjectRef2.element = fitnessDataWrapper.getEndTimeTZ();
                    }
                }
                com.fossil.blesdk.obfuscated.ug4 b = com.fossil.blesdk.obfuscated.nh4.m25692b();
                com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3.C60931.C60941 r5 = new com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3.C60931.C60941(this, ref$ObjectRef, ref$ObjectRef2, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = ref$ObjectRef;
                this.L$2 = ref$ObjectRef2;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b, r5, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef3 = (kotlin.jvm.internal.Ref$ObjectRef) this.L$2;
                kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef4 = (kotlin.jvm.internal.Ref$ObjectRef) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaSyncDataProcessing$saveSyncResult$3(com.portfolio.platform.data.source.FitnessDataRepository fitnessDataRepository, com.portfolio.platform.data.source.ActivitiesRepository activitiesRepository, com.portfolio.platform.data.source.SummariesRepository summariesRepository, com.portfolio.platform.data.source.SleepSessionsRepository sleepSessionsRepository, com.portfolio.platform.data.source.SleepSummariesRepository sleepSummariesRepository, com.portfolio.platform.data.source.HeartRateSampleRepository heartRateSampleRepository, com.portfolio.platform.data.source.HeartRateSummaryRepository heartRateSummaryRepository, com.portfolio.platform.data.source.WorkoutSessionRepository workoutSessionRepository, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$fitnessDataRepository = fitnessDataRepository;
        this.$activityRepository = activitiesRepository;
        this.$summaryRepository = summariesRepository;
        this.$sleepSessionsRepository = sleepSessionsRepository;
        this.$sleepSummariesRepository = sleepSummariesRepository;
        this.$heartRateSampleRepository = heartRateSampleRepository;
        this.$heartRateSummaryRepository = heartRateSummaryRepository;
        this.$workoutSessionRepository = workoutSessionRepository;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3 dianaSyncDataProcessing$saveSyncResult$3 = new com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3(this.$fitnessDataRepository, this.$activityRepository, this.$summaryRepository, this.$sleepSessionsRepository, this.$sleepSummariesRepository, this.$heartRateSampleRepository, this.$heartRateSummaryRepository, this.$workoutSessionRepository, yb4);
        dianaSyncDataProcessing$saveSyncResult$3.f21661p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dianaSyncDataProcessing$saveSyncResult$3;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f21661p$;
            com.portfolio.platform.data.source.FitnessDataRepository fitnessDataRepository = this.$fitnessDataRepository;
            this.L$0 = zg4;
            this.label = 1;
            obj = fitnessDataRepository.pushPendingFitnessData(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            java.util.List list = (java.util.List) this.L$2;
            com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo22 = (com.fossil.blesdk.obfuscated.qo2) obj;
        if (qo22 instanceof com.fossil.blesdk.obfuscated.ro2) {
            java.util.List list2 = (java.util.List) ((com.fossil.blesdk.obfuscated.ro2) qo22).mo30673a();
            if (list2 != null && (true ^ list2.isEmpty())) {
                com.fossil.blesdk.obfuscated.ug4 a2 = com.fossil.blesdk.obfuscated.nh4.m25691a();
                com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3.C60931 r5 = new com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3.C60931(this, list2, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = qo22;
                this.L$2 = list2;
                this.label = 2;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r5, this) == a) {
                    return a;
                }
            }
        } else {
            boolean z = qo22 instanceof com.fossil.blesdk.obfuscated.po2;
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
