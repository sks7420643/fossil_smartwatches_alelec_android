package com.portfolio.platform.service;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.aj2;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.fj2;
import com.fossil.blesdk.obfuscated.fp2;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.mr3;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.sl2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.ul2;
import com.fossil.blesdk.obfuscated.w52;
import com.fossil.blesdk.obfuscated.xk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.fitness.FitnessData;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.db.DataFile;
import com.misfit.frameworks.buttonservice.db.DataFileProvider;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.service.usecase.DianaSyncDataProcessing;
import com.portfolio.platform.service.usecase.HybridSyncDataProcessing;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1", f = "MFDeviceService.kt", l = {275, 286, 379, 418, 463, 481}, m = "invokeSuspend")
public final class MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Intent $intent;
    @DexIgnore
    public int I$Anon0;
    @DexIgnore
    public int I$Anon1;
    @DexIgnore
    public int I$Anon2;
    @DexIgnore
    public int I$Anon3;
    @DexIgnore
    public int I$Anon4;
    @DexIgnore
    public int I$Anon5;
    @DexIgnore
    public int I$Anon6;
    @DexIgnore
    public long J$Anon0;
    @DexIgnore
    public long J$Anon1;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon10;
    @DexIgnore
    public Object L$Anon11;
    @DexIgnore
    public Object L$Anon12;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public Object L$Anon8;
    @DexIgnore
    public Object L$Anon9;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService$bleActionServiceReceiver$Anon1 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$Anon7", f = "MFDeviceService.kt", l = {519, 520}, m = "invokeSuspend")
    public static final class Anon7 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $secretKey;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public Object L$Anon2;
        @DexIgnore
        public Object L$Anon3;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon7(MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1, String str, String str2, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1;
            this.$serial = str;
            this.$secretKey = str2;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon7 anon7 = new Anon7(this.this$Anon0, this.$serial, this.$secretKey, yb4);
            anon7.p$ = (zg4) obj;
            return anon7;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon7) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            MFUser mFUser;
            zg4 zg4;
            MFUser mFUser2;
            String str;
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 = this.p$;
                mFUser = this.this$Anon0.this$Anon0.a.r().getCurrentUser();
                if (mFUser != null) {
                    str = mFUser.getUserId() + ':' + this.$serial;
                    mr3 e = this.this$Anon0.this$Anon0.a.e();
                    String str2 = this.$secretKey;
                    if (str2 != null) {
                        mr3.b bVar = new mr3.b(str, str2);
                        this.L$Anon0 = zg4;
                        this.L$Anon1 = mFUser;
                        this.L$Anon2 = mFUser;
                        this.L$Anon3 = str;
                        this.label = 1;
                        if (w52.a(e, bVar, this) == a) {
                            return a;
                        }
                        mFUser2 = mFUser;
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                return qa4.a;
            } else if (i == 1) {
                str = (String) this.L$Anon3;
                mFUser2 = (MFUser) this.L$Anon2;
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
                mFUser = (MFUser) this.L$Anon1;
            } else if (i == 2) {
                String str3 = (String) this.L$Anon3;
                MFUser mFUser3 = (MFUser) this.L$Anon2;
                MFUser mFUser4 = (MFUser) this.L$Anon1;
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
                return qa4.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            DeviceRepository d = this.this$Anon0.this$Anon0.a.d();
            String str4 = this.$serial;
            kd4.a((Object) str4, "serial");
            String str5 = this.$secretKey;
            this.L$Anon0 = zg4;
            this.L$Anon1 = mFUser;
            this.L$Anon2 = mFUser2;
            this.L$Anon3 = str;
            this.label = 2;
            if (d.updateDeviceSecretKey(str4, str5, this) == a) {
                return a;
            }
            return qa4.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.e<VerifySecretKeyUseCase.d, VerifySecretKeyUseCase.c> {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public a(MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1, String str) {
            this.a = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(VerifySecretKeyUseCase.d dVar) {
            kd4.b(dVar, "responseValue");
            PortfolioApp c = this.a.this$Anon0.a.c();
            String str = this.b;
            kd4.a((Object) str, "serial");
            c.b(str, dVar.a());
        }

        @DexIgnore
        public void a(VerifySecretKeyUseCase.c cVar) {
            kd4.b(cVar, "errorValue");
            PortfolioApp c = this.a.this$Anon0.a.c();
            String str = this.b;
            kd4.a((Object) str, "serial");
            c.b(str, (String) null);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1(MFDeviceService$bleActionServiceReceiver$Anon1 mFDeviceService$bleActionServiceReceiver$Anon1, Intent intent, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = mFDeviceService$bleActionServiceReceiver$Anon1;
        this.$intent = intent;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 = new MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1(this.this$Anon0, this.$intent, yb4);
        mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.p$ = (zg4) obj;
        return mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v55, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v21, resolved type: java.lang.String} */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x05e6, code lost:
        r0 = (com.fossil.blesdk.obfuscated.qo2) r0;
        r2 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x05fa, code lost:
        r6 = r6;
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x0610, code lost:
        r0.putExtra(r17, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x0613, code lost:
        if (r6 == null) goto L_0x061e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0615, code lost:
        r15.this$Anon0.a.a("ota_success", (java.util.Map<java.lang.String, java.lang.String>) r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x061e, code lost:
        if (r2 == null) goto L_0x066c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x0620, code lost:
        r2.a(r23);
        r1 = com.fossil.blesdk.obfuscated.qa4.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x066c, code lost:
        com.portfolio.platform.helper.AnalyticsHelper.f.e(r18);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.service.MFDeviceService.U.b(), "Inside .OTA receiver, broadcast local in app");
        r1 = com.portfolio.platform.service.BleCommandResultManager.d;
        r2 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.OTA;
        r1.a(r2, new com.portfolio.platform.service.BleCommandResultManager.a(r2, r9, r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0144, code lost:
        r42 = "Inside ";
        r44 = "sync_session";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x070e, code lost:
        r0 = com.fossil.blesdk.obfuscated.qa4.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x0710, code lost:
        r8 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:0x0b8a, code lost:
        r7 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:271:0x0d04, code lost:
        if (r0 == 0) goto L_0x0d3f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:272:0x0d06, code lost:
        r8 = r53;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:274:?, code lost:
        r8.this$Anon0.a.c().a(com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC, r7, "Process data OK.");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC, r7, com.portfolio.platform.service.MFDeviceService.U.b(), "Process data OK.");
        r8.this$Anon0.a.c().c(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:275:0x0d3b, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:276:0x0d3c, code lost:
        r12 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:277:0x0d3f, code lost:
        r8 = r53;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:279:?, code lost:
        r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r1 = com.portfolio.platform.service.MFDeviceService.U.b();
        r0.d(r1, r42 + com.portfolio.platform.service.MFDeviceService.U.b() + ".processBleResult - data is empty.");
        r8.this$Anon0.a.c().a(com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC, r7, "data is empty.");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC, r7, com.portfolio.platform.service.MFDeviceService.U.b(), "data is empty.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:280:0x0d99, code lost:
        r5 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:282:?, code lost:
        r8.this$Anon0.a.c().c(true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:283:0x0d9f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:284:0x0da1, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:285:0x0da2, code lost:
        r5 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:286:0x0da3, code lost:
        r12 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x02bd, code lost:
        r0 = (com.fossil.blesdk.obfuscated.qo2) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x02c1, code lost:
        if ((r0 instanceof com.fossil.blesdk.obfuscated.ro2) == false) goto L_0x02ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x02c3, code lost:
        r3 = ((com.fossil.blesdk.obfuscated.ro2) r0).a();
        r11 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x02d0, code lost:
        if ((r0 instanceof com.fossil.blesdk.obfuscated.po2) == false) goto L_0x02ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x02d2, code lost:
        r11 = ((com.fossil.blesdk.obfuscated.po2) r0).a();
        r3 = r23;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x02da, code lost:
        r0 = r15.this$Anon0.a.c();
        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) r9, "serial");
        r0.a(r9, r3, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x02ef, code lost:
        throw new kotlin.NoWhenBranchMatchedException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0343, code lost:
        r0 = (com.fossil.blesdk.obfuscated.qo2) r0;
        r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r2 = com.portfolio.platform.service.MFDeviceService.U.b();
        r1.d(r2, "swap pairing key " + r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0367, code lost:
        if ((r0 instanceof com.fossil.blesdk.obfuscated.ro2) == false) goto L_0x0389;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0369, code lost:
        r1 = r15.this$Anon0.a.c();
        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) r9, "serial");
        r0 = ((com.fossil.blesdk.obfuscated.ro2) r0).a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x037a, code lost:
        if (r0 == null) goto L_0x0384;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x037c, code lost:
        r1.b(r9, (java.lang.String) r0, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0384, code lost:
        com.fossil.blesdk.obfuscated.kd4.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0388, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x038b, code lost:
        if ((r0 instanceof com.fossil.blesdk.obfuscated.po2) == false) goto L_0x0710;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x038d, code lost:
        r1 = r15.this$Anon0.a.c();
        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) r9, "serial");
        r1.b(r9, r23, ((com.fossil.blesdk.obfuscated.po2) r0).a());
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    public final Object invokeSuspend(Object obj) {
        String str;
        MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1;
        String str2;
        boolean z;
        String str3;
        int i;
        String str4;
        String str5;
        String str6;
        Intent intent;
        String str7;
        String str8;
        ul2 ul2;
        HashMap hashMap;
        Intent intent2;
        ul2 ul22;
        Object obj2;
        String str9;
        Object obj3;
        Object obj4;
        String str10;
        String str11;
        String str12;
        int i2;
        String str13;
        int i3;
        ArrayList<Integer> arrayList;
        List<DataFile> list;
        FitnessData fitnessData;
        ILocalFLogger local;
        String b;
        int i4;
        ul2 ul23;
        HashMap hashMap2;
        String str14;
        HashMap hashMap3;
        String str15;
        String str16;
        DeviceRepository deviceRepository;
        String str17;
        Object a2 = cc4.a();
        switch (this.label) {
            case 0:
                na4.a(obj);
                zg4 zg4 = this.p$;
                int intExtra = this.$intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
                CommunicateMode communicateMode = CommunicateMode.values()[intExtra];
                str6 = this.$intent.getStringExtra(Constants.SERIAL_NUMBER);
                int intExtra2 = this.$intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
                int intExtra3 = this.$intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), FailureCode.UNKNOWN_ERROR);
                ArrayList<Integer> integerArrayListExtra = this.$intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>();
                }
                ArrayList<Integer> arrayList2 = integerArrayListExtra;
                Bundle extras = this.$intent.getExtras();
                if (extras == null) {
                    extras = new Bundle();
                }
                String str18 = "Inside ";
                Bundle bundle = extras;
                kd4.a((Object) bundle, "intent.extras ?: Bundle()");
                String str19 = "sync_session";
                SKUModel skuModelBySerialPrefix = this.this$Anon0.a.d().getSkuModelBySerialPrefix(DeviceHelper.o.b(str6));
                str5 = "ota_session";
                str8 = "OTA_RESULT";
                int i5 = bundle.getInt(ButtonService.Companion.getSYNC_MODE(), -1);
                int i6 = bundle.getInt(ButtonService.Companion.getORIGINAL_SYNC_MODE(), 10);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                str9 = "";
                String b2 = MFDeviceService.U.b();
                Object obj5 = a2;
                StringBuilder sb = new StringBuilder();
                int i7 = i6;
                sb.append("Inside .bleActionServiceReceiver phase ");
                sb.append(communicateMode);
                sb.append(" result ");
                sb.append(intExtra2);
                sb.append(" extra info ");
                sb.append(bundle);
                local2.d(b2, sb.toString());
                int i8 = bundle.getInt(ButtonService.Companion.getLOG_ID(), -1);
                switch (fp2.a[communicateMode.ordinal()]) {
                    case 1:
                        this.this$Anon0.a.m().b(System.currentTimeMillis());
                        break;
                    case 2:
                        String str20 = "error_code";
                        int i9 = i8;
                        int i10 = i7;
                        int i11 = i5;
                        String str21 = "device";
                        ul2 c = AnalyticsHelper.f.c(str19);
                        int i12 = intExtra;
                        CommunicateMode communicateMode2 = communicateMode;
                        this.this$Anon0.a.m().b(System.currentTimeMillis(), str6);
                        if (intExtra2 == ServiceActionResult.ASK_FOR_STOP_WORKOUT.ordinal()) {
                            FLogger.INSTANCE.getLocal().d(MFDeviceService.U.b(), "sync is pending because workout is working");
                            this.this$Anon0.a.a(str6, 5, intExtra3, arrayList2, dc4.a(i10));
                        } else if (intExtra2 == ServiceActionResult.PROCESSING.ordinal()) {
                            FLogger.INSTANCE.getLocal().d(MFDeviceService.U.b(), "sync status is SYNCING");
                            MFDeviceService.a(this.this$Anon0.a, str6, 0, 0, (ArrayList) null, (Integer) null, 24, (Object) null);
                        } else if (intExtra2 == ServiceActionResult.UNALLOWED_ACTION.ordinal()) {
                            this.this$Anon0.a.m().b(0);
                            this.this$Anon0.a.a(str6, 4, intExtra3, arrayList2, dc4.a(i10));
                        } else if (intExtra2 == ServiceActionResult.FAILED.ordinal()) {
                            this.this$Anon0.a.m().b(0);
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String b3 = MFDeviceService.U.b();
                            local3.d(b3, "sync status is FAILED - current sync success number = " + this.this$Anon0.a.m().e());
                            this.this$Anon0.a.a(str6, 2, intExtra3, arrayList2, dc4.a(i10));
                            this.this$Anon0.a.a(skuModelBySerialPrefix, i11, 2);
                            sl2 a3 = AnalyticsHelper.f.a("sync_session_optional_error");
                            a3.a(str20, String.valueOf(intExtra3));
                            if (c != null) {
                                c.a(a3);
                                qa4 qa4 = qa4.a;
                            }
                            if (c != null) {
                                c.a(String.valueOf(intExtra3));
                                qa4 qa42 = qa4.a;
                            }
                            FossilNotificationBar.c.a(this.this$Anon0.a);
                        } else if (intExtra2 == ServiceActionResult.RECEIVED_DATA.ordinal()) {
                            if (!bundle.isEmpty()) {
                                if (!(str6 == null || qf4.a(str6))) {
                                    String str22 = str6;
                                    long j = bundle.getLong(Constants.SYNC_RESULT, -1);
                                    long j2 = bundle.getLong(ButtonService.Companion.getREALTIME_STEPS(), -1);
                                    UserProfile j3 = this.this$Anon0.a.c().j();
                                    MFUser currentUser = this.this$Anon0.a.r().getCurrentUser();
                                    PortfolioApp c2 = this.this$Anon0.a.c();
                                    ul2 ul24 = c;
                                    CommunicateMode communicateMode3 = CommunicateMode.SYNC;
                                    int i13 = i9;
                                    StringBuilder sb2 = new StringBuilder();
                                    int i14 = i10;
                                    sb2.append("Sync time=");
                                    Bundle bundle2 = bundle;
                                    SKUModel sKUModel = skuModelBySerialPrefix;
                                    long j4 = j;
                                    sb2.append(j4);
                                    int i15 = i11;
                                    String str23 = str22;
                                    c2.a(communicateMode3, str23, sb2.toString());
                                    if (j4 <= ((long) -1) || j3 == null || currentUser == null) {
                                        String str24 = str23;
                                        mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 = this;
                                        String str25 = str18;
                                        str = str19;
                                        if (j4 < 0) {
                                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                                            String b4 = MFDeviceService.U.b();
                                            local4.e(b4, str25 + MFDeviceService.U.b() + ".processBleResult - syncTime wrong: " + j4);
                                            PortfolioApp c3 = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c();
                                            CommunicateMode communicateMode4 = CommunicateMode.SYNC;
                                            str10 = str24;
                                            c3.a(communicateMode4, str10, "syncTime value wrong: " + j4);
                                            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                                            FLogger.Component component = FLogger.Component.APP;
                                            FLogger.Session session = FLogger.Session.SYNC;
                                            String b5 = MFDeviceService.U.b();
                                            remote.i(component, session, str10, b5, "syncTime value wrong: " + j4);
                                        } else {
                                            str10 = str24;
                                        }
                                        if (j3 == null) {
                                            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                                            String b6 = MFDeviceService.U.b();
                                            local5.e(b6, str25 + MFDeviceService.U.b() + ".processBleResult - userProfile empty");
                                            mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c().a(CommunicateMode.SYNC, str10, "userProfile is empty.");
                                            FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str10, MFDeviceService.U.b(), "userProfile is empty.");
                                        }
                                        if (currentUser == null) {
                                            ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                                            String b7 = MFDeviceService.U.b();
                                            local6.e(b7, str25 + MFDeviceService.U.b() + ".processBleResult - user data empty");
                                            mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c().a(CommunicateMode.SYNC, str10, "user data is empty.");
                                            FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str10, MFDeviceService.U.b(), "user data is empty.");
                                        }
                                        mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c().c(true);
                                    } else {
                                        try {
                                            this.this$Anon0.a.c().a(CommunicateMode.SYNC, str23, "Save sync result to DB");
                                            ArrayList arrayList3 = new ArrayList();
                                            List<DataFile> allDataFiles = DataFileProvider.getInstance(this.this$Anon0.a).getAllDataFiles(j4, str23);
                                            IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                                            FLogger.Component component2 = FLogger.Component.APP;
                                            FLogger.Session session2 = FLogger.Session.SYNC;
                                            String b8 = MFDeviceService.U.b();
                                            StringBuilder sb3 = new StringBuilder();
                                            Bundle bundle3 = bundle2;
                                            sb3.append("[SYNC] Process sync data from BS dataSize ");
                                            sb3.append(allDataFiles.size());
                                            remote2.i(component2, session2, str23, b8, sb3.toString());
                                            kd4.a((Object) allDataFiles, "cacheData");
                                            Iterator<T> it = allDataFiles.iterator();
                                            while (it.hasNext()) {
                                                try {
                                                    DataFile dataFile = (DataFile) it.next();
                                                    Iterator<T> it2 = it;
                                                    try {
                                                        Gson a4 = this.this$Anon0.a.F;
                                                        list = allDataFiles;
                                                        try {
                                                            kd4.a((Object) dataFile, "it");
                                                            fitnessData = (FitnessData) a4.a(dataFile.getDataFile(), FitnessData.class);
                                                            local = FLogger.INSTANCE.getLocal();
                                                            b = MFDeviceService.U.b();
                                                            arrayList = arrayList2;
                                                        } catch (Exception e) {
                                                            e = e;
                                                            i3 = intExtra3;
                                                            arrayList = arrayList2;
                                                            ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
                                                            String b9 = MFDeviceService.U.b();
                                                            local7.d(b9, "Parse fitnessData. exception=" + e);
                                                            IRemoteFLogger remote3 = FLogger.INSTANCE.getRemote();
                                                            FLogger.Component component3 = FLogger.Component.APP;
                                                            FLogger.Session session3 = FLogger.Session.SYNC;
                                                            String b10 = MFDeviceService.U.b();
                                                            remote3.i(component3, session3, str23, b10, "[Sync Data Received] Exception when parse data " + e);
                                                            it = it2;
                                                            allDataFiles = list;
                                                            arrayList2 = arrayList;
                                                            intExtra3 = i3;
                                                        }
                                                        try {
                                                            StringBuilder sb4 = new StringBuilder();
                                                            i3 = intExtra3;
                                                            try {
                                                                sb4.append("[SYNC] Add fitness data ");
                                                                sb4.append(fitnessData);
                                                                sb4.append(" to sync data");
                                                                local.d(b, sb4.toString());
                                                                kd4.a((Object) fitnessData, "fitnessData");
                                                                arrayList3.add(fitnessData);
                                                            } catch (Exception e2) {
                                                                e = e2;
                                                            }
                                                        } catch (Exception e3) {
                                                            e = e3;
                                                            i3 = intExtra3;
                                                            ILocalFLogger local72 = FLogger.INSTANCE.getLocal();
                                                            String b92 = MFDeviceService.U.b();
                                                            local72.d(b92, "Parse fitnessData. exception=" + e);
                                                            IRemoteFLogger remote32 = FLogger.INSTANCE.getRemote();
                                                            FLogger.Component component32 = FLogger.Component.APP;
                                                            FLogger.Session session32 = FLogger.Session.SYNC;
                                                            String b102 = MFDeviceService.U.b();
                                                            remote32.i(component32, session32, str23, b102, "[Sync Data Received] Exception when parse data " + e);
                                                            it = it2;
                                                            allDataFiles = list;
                                                            arrayList2 = arrayList;
                                                            intExtra3 = i3;
                                                        }
                                                    } catch (Exception e4) {
                                                        e = e4;
                                                        list = allDataFiles;
                                                        i3 = intExtra3;
                                                        arrayList = arrayList2;
                                                        ILocalFLogger local722 = FLogger.INSTANCE.getLocal();
                                                        String b922 = MFDeviceService.U.b();
                                                        local722.d(b922, "Parse fitnessData. exception=" + e);
                                                        IRemoteFLogger remote322 = FLogger.INSTANCE.getRemote();
                                                        FLogger.Component component322 = FLogger.Component.APP;
                                                        FLogger.Session session322 = FLogger.Session.SYNC;
                                                        String b1022 = MFDeviceService.U.b();
                                                        remote322.i(component322, session322, str23, b1022, "[Sync Data Received] Exception when parse data " + e);
                                                        it = it2;
                                                        allDataFiles = list;
                                                        arrayList2 = arrayList;
                                                        intExtra3 = i3;
                                                    }
                                                    it = it2;
                                                    allDataFiles = list;
                                                    arrayList2 = arrayList;
                                                    intExtra3 = i3;
                                                } catch (Exception e5) {
                                                    e = e5;
                                                    str2 = str23;
                                                    mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 = this;
                                                    str = str19;
                                                    break;
                                                }
                                            }
                                            List<DataFile> list2 = allDataFiles;
                                            int i16 = intExtra3;
                                            ArrayList<Integer> arrayList4 = arrayList2;
                                            ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
                                            String b11 = MFDeviceService.U.b();
                                            local8.d(b11, "[SYNC] Process data done,syncData size " + arrayList3.size());
                                            IRemoteFLogger remote4 = FLogger.INSTANCE.getRemote();
                                            FLogger.Component component4 = FLogger.Component.APP;
                                            FLogger.Session session4 = FLogger.Session.SYNC;
                                            String b12 = MFDeviceService.U.b();
                                            remote4.i(component4, session4, str23, b12, "[Sync Data Received] After get data, size " + arrayList3.size());
                                            i = !arrayList3.isEmpty() ? 1 : 0;
                                            ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
                                            String b13 = MFDeviceService.U.b();
                                            local9.d(b13, ".onReadDataFilesSuccess(), sdk = " + this.this$Anon0.a.F.a((Object) arrayList3));
                                            if (!FossilDeviceSerialPatternUtil.isDianaDevice(str23)) {
                                                String str26 = str23;
                                                Bundle bundle4 = bundle3;
                                                str3 = str18;
                                                str = str19;
                                                Object obj6 = obj5;
                                                int i17 = i12;
                                                CommunicateMode communicateMode5 = communicateMode2;
                                                int i18 = i15;
                                                List<DataFile> list3 = list2;
                                                ul2 ul25 = ul24;
                                                int i19 = i13;
                                                int i20 = i14;
                                                SKUModel sKUModel2 = sKUModel;
                                                ArrayList<Integer> arrayList5 = arrayList4;
                                                int i21 = i16;
                                                if (i != 0) {
                                                    Bundle bundle5 = bundle4;
                                                    SKUModel sKUModel3 = sKUModel2;
                                                    try {
                                                        int i22 = i;
                                                        ArrayList<Integer> arrayList6 = arrayList5;
                                                        String str27 = str26;
                                                        try {
                                                            List<FitnessDataWrapper> a5 = this.this$Anon0.a.a((List<FitnessData>) arrayList3, str27, new DateTime());
                                                            this.this$Anon0.a.f().saveFitnessData(a5);
                                                            HybridSyncDataProcessing.a a6 = HybridSyncDataProcessing.b.a(str27, a5, currentUser, j3, j4, j2, this.this$Anon0.a.c());
                                                            HybridSyncDataProcessing hybridSyncDataProcessing = HybridSyncDataProcessing.b;
                                                            HybridSyncDataProcessing.a aVar = a6;
                                                            SleepSessionsRepository n = this.this$Anon0.a.n();
                                                            SummariesRepository p = this.this$Anon0.a.p();
                                                            SleepSummariesRepository o = this.this$Anon0.a.o();
                                                            FitnessDataRepository f = this.this$Anon0.a.f();
                                                            ActivitiesRepository b14 = this.this$Anon0.a.b();
                                                            HybridPresetRepository l = this.this$Anon0.a.l();
                                                            GoalTrackingRepository h = this.this$Anon0.a.h();
                                                            ThirdPartyRepository q = this.this$Anon0.a.q();
                                                            UserRepository r = this.this$Anon0.a.r();
                                                            PortfolioApp c4 = this.this$Anon0.a.c();
                                                            xk2 g = this.this$Anon0.a.g();
                                                            this.L$Anon0 = zg4;
                                                            this.I$Anon0 = i17;
                                                            this.L$Anon1 = communicateMode5;
                                                            this.L$Anon2 = str27;
                                                            this.I$Anon1 = intExtra2;
                                                            this.I$Anon2 = i21;
                                                            this.L$Anon3 = arrayList6;
                                                            this.L$Anon4 = bundle5;
                                                            this.L$Anon5 = sKUModel3;
                                                            this.I$Anon3 = i18;
                                                            this.I$Anon4 = i20;
                                                            this.I$Anon5 = i19;
                                                            this.L$Anon6 = ul25;
                                                            this.J$Anon0 = j4;
                                                            this.J$Anon1 = j2;
                                                            this.L$Anon7 = j3;
                                                            this.L$Anon8 = currentUser;
                                                            this.L$Anon9 = arrayList3;
                                                            this.L$Anon10 = list3;
                                                            int i23 = i22;
                                                            this.I$Anon6 = i23;
                                                            this.L$Anon11 = a5;
                                                            HybridSyncDataProcessing.a aVar2 = aVar;
                                                            this.L$Anon12 = aVar2;
                                                            this.label = 2;
                                                            str11 = str27;
                                                            int i24 = i23;
                                                            try {
                                                                Object obj7 = obj6;
                                                                if (hybridSyncDataProcessing.a(aVar2, str27, n, p, o, f, b14, l, h, q, r, c4, g, this) != obj7) {
                                                                    i = i24;
                                                                    str4 = str11;
                                                                    break;
                                                                } else {
                                                                    return obj7;
                                                                }
                                                            } catch (Exception e6) {
                                                                e = e6;
                                                                z = true;
                                                                mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 = this;
                                                                str2 = str11;
                                                                ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
                                                                String b15 = MFDeviceService.U.b();
                                                                StringBuilder sb5 = new StringBuilder();
                                                                sb5.append("Error while processing sync data - e=");
                                                                e.printStackTrace();
                                                                sb5.append(qa4.a);
                                                                local10.e(b15, sb5.toString());
                                                                e.printStackTrace();
                                                                PortfolioApp c5 = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c();
                                                                CommunicateMode communicateMode6 = CommunicateMode.SYNC;
                                                                c5.a(communicateMode6, str2, "Sync data result OK but has exception error e=" + e);
                                                                IRemoteFLogger remote5 = FLogger.INSTANCE.getRemote();
                                                                FLogger.Component component5 = FLogger.Component.APP;
                                                                FLogger.Session session5 = FLogger.Session.SYNC;
                                                                String b16 = MFDeviceService.U.b();
                                                                remote5.i(component5, session5, str2, b16, "Sync data result OK but has exception error e=" + e);
                                                                mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c().c(z);
                                                                AnalyticsHelper.f.e(str);
                                                                FossilNotificationBar.c.a(mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a);
                                                                return qa4.a;
                                                            }
                                                        } catch (Exception e7) {
                                                            e = e7;
                                                            str11 = str27;
                                                            z = true;
                                                            mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 = this;
                                                            str2 = str11;
                                                            ILocalFLogger local102 = FLogger.INSTANCE.getLocal();
                                                            String b152 = MFDeviceService.U.b();
                                                            StringBuilder sb52 = new StringBuilder();
                                                            sb52.append("Error while processing sync data - e=");
                                                            e.printStackTrace();
                                                            sb52.append(qa4.a);
                                                            local102.e(b152, sb52.toString());
                                                            e.printStackTrace();
                                                            PortfolioApp c52 = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c();
                                                            CommunicateMode communicateMode62 = CommunicateMode.SYNC;
                                                            c52.a(communicateMode62, str2, "Sync data result OK but has exception error e=" + e);
                                                            IRemoteFLogger remote52 = FLogger.INSTANCE.getRemote();
                                                            FLogger.Component component52 = FLogger.Component.APP;
                                                            FLogger.Session session52 = FLogger.Session.SYNC;
                                                            String b162 = MFDeviceService.U.b();
                                                            remote52.i(component52, session52, str2, b162, "Sync data result OK but has exception error e=" + e);
                                                            mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c().c(z);
                                                            AnalyticsHelper.f.e(str);
                                                            FossilNotificationBar.c.a(mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a);
                                                            return qa4.a;
                                                        }
                                                    } catch (Exception e8) {
                                                        e = e8;
                                                        str11 = str26;
                                                        z = true;
                                                        mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 = this;
                                                        str2 = str11;
                                                        ILocalFLogger local1022 = FLogger.INSTANCE.getLocal();
                                                        String b1522 = MFDeviceService.U.b();
                                                        StringBuilder sb522 = new StringBuilder();
                                                        sb522.append("Error while processing sync data - e=");
                                                        e.printStackTrace();
                                                        sb522.append(qa4.a);
                                                        local1022.e(b1522, sb522.toString());
                                                        e.printStackTrace();
                                                        PortfolioApp c522 = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c();
                                                        CommunicateMode communicateMode622 = CommunicateMode.SYNC;
                                                        c522.a(communicateMode622, str2, "Sync data result OK but has exception error e=" + e);
                                                        IRemoteFLogger remote522 = FLogger.INSTANCE.getRemote();
                                                        FLogger.Component component522 = FLogger.Component.APP;
                                                        FLogger.Session session522 = FLogger.Session.SYNC;
                                                        String b1622 = MFDeviceService.U.b();
                                                        remote522.i(component522, session522, str2, b1622, "Sync data result OK but has exception error e=" + e);
                                                        mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c().c(z);
                                                        AnalyticsHelper.f.e(str);
                                                        FossilNotificationBar.c.a(mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a);
                                                        return qa4.a;
                                                    }
                                                } else {
                                                    i2 = i;
                                                    str12 = str26;
                                                }
                                            } else if (i != 0) {
                                                try {
                                                    List<FitnessDataWrapper> a7 = this.this$Anon0.a.a((List<FitnessData>) arrayList3, str23, new DateTime());
                                                    this.this$Anon0.a.f().saveFitnessData(a7);
                                                    DianaSyncDataProcessing.a a8 = DianaSyncDataProcessing.b.a(str23, a7, currentUser, j3, j4, j2, this.this$Anon0.a.c());
                                                    DianaSyncDataProcessing dianaSyncDataProcessing = DianaSyncDataProcessing.b;
                                                    SleepSessionsRepository n2 = this.this$Anon0.a.n();
                                                    SummariesRepository p2 = this.this$Anon0.a.p();
                                                    SleepSummariesRepository o2 = this.this$Anon0.a.o();
                                                    HeartRateSampleRepository i25 = this.this$Anon0.a.i();
                                                    HeartRateSummaryRepository j5 = this.this$Anon0.a.j();
                                                    WorkoutSessionRepository u = this.this$Anon0.a.u();
                                                    FitnessDataRepository f2 = this.this$Anon0.a.f();
                                                    ActivitiesRepository b17 = this.this$Anon0.a.b();
                                                    ThirdPartyRepository q2 = this.this$Anon0.a.q();
                                                    PortfolioApp c6 = this.this$Anon0.a.c();
                                                    xk2 g2 = this.this$Anon0.a.g();
                                                    this.L$Anon0 = zg4;
                                                    this.I$Anon0 = i12;
                                                    this.L$Anon1 = communicateMode2;
                                                    this.L$Anon2 = str23;
                                                    this.I$Anon1 = intExtra2;
                                                    this.I$Anon2 = i16;
                                                    this.L$Anon3 = arrayList4;
                                                    this.L$Anon4 = bundle3;
                                                    this.L$Anon5 = sKUModel;
                                                    this.I$Anon3 = i15;
                                                    this.I$Anon4 = i14;
                                                    this.I$Anon5 = i13;
                                                    this.L$Anon6 = ul24;
                                                    this.J$Anon0 = j4;
                                                    this.J$Anon1 = j2;
                                                    this.L$Anon7 = j3;
                                                    this.L$Anon8 = currentUser;
                                                    this.L$Anon9 = arrayList3;
                                                    this.L$Anon10 = list2;
                                                    this.I$Anon6 = i;
                                                    this.L$Anon11 = a7;
                                                    this.L$Anon12 = a8;
                                                    this.label = 1;
                                                    str3 = str18;
                                                    str13 = str23;
                                                    str = str19;
                                                    Object obj8 = obj5;
                                                    try {
                                                        Object a9 = dianaSyncDataProcessing.a(a8, str23, n2, p2, o2, i25, j5, u, f2, b17, q2, c6, g2, this);
                                                        Object obj9 = obj8;
                                                        if (a9 != obj9) {
                                                            str4 = str13;
                                                            break;
                                                        } else {
                                                            return obj9;
                                                        }
                                                    } catch (Exception e9) {
                                                        e = e9;
                                                        z = true;
                                                        mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 = this;
                                                        str2 = str13;
                                                        ILocalFLogger local10222 = FLogger.INSTANCE.getLocal();
                                                        String b15222 = MFDeviceService.U.b();
                                                        StringBuilder sb5222 = new StringBuilder();
                                                        sb5222.append("Error while processing sync data - e=");
                                                        e.printStackTrace();
                                                        sb5222.append(qa4.a);
                                                        local10222.e(b15222, sb5222.toString());
                                                        e.printStackTrace();
                                                        PortfolioApp c5222 = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c();
                                                        CommunicateMode communicateMode6222 = CommunicateMode.SYNC;
                                                        c5222.a(communicateMode6222, str2, "Sync data result OK but has exception error e=" + e);
                                                        IRemoteFLogger remote5222 = FLogger.INSTANCE.getRemote();
                                                        FLogger.Component component5222 = FLogger.Component.APP;
                                                        FLogger.Session session5222 = FLogger.Session.SYNC;
                                                        String b16222 = MFDeviceService.U.b();
                                                        remote5222.i(component5222, session5222, str2, b16222, "Sync data result OK but has exception error e=" + e);
                                                        mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c().c(z);
                                                        AnalyticsHelper.f.e(str);
                                                        FossilNotificationBar.c.a(mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a);
                                                        return qa4.a;
                                                    }
                                                } catch (Exception e10) {
                                                    e = e10;
                                                    str13 = str23;
                                                    str = str19;
                                                    z = true;
                                                    mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 = this;
                                                    str2 = str13;
                                                    ILocalFLogger local102222 = FLogger.INSTANCE.getLocal();
                                                    String b152222 = MFDeviceService.U.b();
                                                    StringBuilder sb52222 = new StringBuilder();
                                                    sb52222.append("Error while processing sync data - e=");
                                                    e.printStackTrace();
                                                    sb52222.append(qa4.a);
                                                    local102222.e(b152222, sb52222.toString());
                                                    e.printStackTrace();
                                                    PortfolioApp c52222 = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c();
                                                    CommunicateMode communicateMode62222 = CommunicateMode.SYNC;
                                                    c52222.a(communicateMode62222, str2, "Sync data result OK but has exception error e=" + e);
                                                    IRemoteFLogger remote52222 = FLogger.INSTANCE.getRemote();
                                                    FLogger.Component component52222 = FLogger.Component.APP;
                                                    FLogger.Session session52222 = FLogger.Session.SYNC;
                                                    String b162222 = MFDeviceService.U.b();
                                                    remote52222.i(component52222, session52222, str2, b162222, "Sync data result OK but has exception error e=" + e);
                                                    mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c().c(z);
                                                    AnalyticsHelper.f.e(str);
                                                    FossilNotificationBar.c.a(mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a);
                                                    return qa4.a;
                                                }
                                            } else {
                                                str3 = str18;
                                                str = str19;
                                                i2 = i;
                                                str12 = str23;
                                            }
                                            i = i2;
                                            String str28 = str12;
                                            break;
                                        } catch (Exception e11) {
                                            e = e11;
                                            str11 = str23;
                                            mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 = this;
                                            str = str19;
                                            z = true;
                                            str2 = str11;
                                            ILocalFLogger local1022222 = FLogger.INSTANCE.getLocal();
                                            String b1522222 = MFDeviceService.U.b();
                                            StringBuilder sb522222 = new StringBuilder();
                                            sb522222.append("Error while processing sync data - e=");
                                            e.printStackTrace();
                                            sb522222.append(qa4.a);
                                            local1022222.e(b1522222, sb522222.toString());
                                            e.printStackTrace();
                                            PortfolioApp c522222 = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c();
                                            CommunicateMode communicateMode622222 = CommunicateMode.SYNC;
                                            c522222.a(communicateMode622222, str2, "Sync data result OK but has exception error e=" + e);
                                            IRemoteFLogger remote522222 = FLogger.INSTANCE.getRemote();
                                            FLogger.Component component522222 = FLogger.Component.APP;
                                            FLogger.Session session522222 = FLogger.Session.SYNC;
                                            String b1622222 = MFDeviceService.U.b();
                                            remote522222.i(component522222, session522222, str2, b1622222, "Sync data result OK but has exception error e=" + e);
                                            mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c().c(z);
                                            AnalyticsHelper.f.e(str);
                                            FossilNotificationBar.c.a(mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a);
                                            return qa4.a;
                                        }
                                    }
                                }
                            }
                            String str29 = str6;
                            mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 = this;
                            str = str19;
                            if (bundle.isEmpty()) {
                                PortfolioApp c7 = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c();
                                CommunicateMode communicateMode7 = CommunicateMode.SYNC;
                                if (str29 != null) {
                                    c7.a(communicateMode7, str29, "Sync data result OK but extra info is EMPTY");
                                    FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str29, MFDeviceService.U.b(), "Sync data result OK but extra info is EMPTY");
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                PortfolioApp c8 = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c();
                                CommunicateMode communicateMode8 = CommunicateMode.SYNC;
                                if (str29 != null) {
                                    c8.a(communicateMode8, str29, "Sync data result OK but serial is " + str29);
                                    IRemoteFLogger remote6 = FLogger.INSTANCE.getRemote();
                                    FLogger.Component component6 = FLogger.Component.APP;
                                    FLogger.Session session6 = FLogger.Session.SYNC;
                                    String b18 = MFDeviceService.U.b();
                                    remote6.i(component6, session6, str29, b18, "Sync data result OK but serial is " + str29);
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            }
                            mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c().c(true);
                        } else {
                            ul2 ul26 = c;
                            String str30 = str6;
                            Bundle bundle6 = bundle;
                            SKUModel sKUModel4 = skuModelBySerialPrefix;
                            mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 = this;
                            str = str19;
                            en2 m = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.m();
                            m.b(m.e() + 1);
                            ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
                            String b19 = MFDeviceService.U.b();
                            local11.d(b19, "sync status is successful - current sync success number = " + mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.m().e());
                            mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.m().c(System.currentTimeMillis(), str30);
                            if (!bundle6.isEmpty() && str30 != null) {
                                mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.a((MisfitDeviceProfile) bundle6.getParcelable(str21));
                                MFDeviceService mFDeviceService = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a;
                                mFDeviceService.a(mFDeviceService.a(), str30, false);
                                if (i11 == 13 && !mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.m().k(str30)) {
                                    mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.m().a(str30, bundle6.getLong(Constants.SYNC_ID), true);
                                }
                            }
                            MFDeviceService.a(mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a, str30, 1, 0, (ArrayList) null, (Integer) null, 24, (Object) null);
                            mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.a(sKUModel4, i11, 1);
                            if (ul26 != null) {
                                ul26.a(str9);
                                qa4 qa43 = qa4.a;
                            }
                            FossilNotificationBar.c.a(mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a);
                            mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.x();
                        }
                        mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 = this;
                        str = str19;
                        break;
                    case 3:
                        Object obj10 = obj5;
                        int i26 = i7;
                        int i27 = i8;
                        int i28 = i5;
                        int i29 = i27;
                        if (intExtra2 == ServiceActionResult.SUCCEEDED.ordinal() && !bundle.isEmpty()) {
                            int i30 = bundle.getInt(Constants.BATTERY);
                            Object obj11 = obj10;
                            DeviceRepository d = this.this$Anon0.a.d();
                            kd4.a((Object) str6, "serial");
                            Device deviceBySerial = d.getDeviceBySerial(str6);
                            if (deviceBySerial != null) {
                                if (i30 > 0) {
                                    deviceBySerial.setBatteryLevel(i30);
                                    int i31 = i29;
                                    if (deviceBySerial.getBatteryLevel() > 100) {
                                        deviceBySerial.setBatteryLevel(100);
                                    }
                                    ug4 b20 = nh4.b();
                                    MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1((yb4) null, this, i30, deviceBySerial);
                                    this.L$Anon0 = zg4;
                                    this.I$Anon0 = intExtra;
                                    this.L$Anon1 = communicateMode;
                                    this.L$Anon2 = str6;
                                    this.I$Anon1 = intExtra2;
                                    this.I$Anon2 = intExtra3;
                                    this.L$Anon3 = arrayList2;
                                    this.L$Anon4 = bundle;
                                    this.L$Anon5 = skuModelBySerialPrefix;
                                    this.I$Anon3 = i28;
                                    this.I$Anon4 = i26;
                                    this.I$Anon5 = i31;
                                    this.I$Anon6 = i30;
                                    this.L$Anon6 = deviceBySerial;
                                    this.L$Anon7 = deviceBySerial;
                                    this.label = 3;
                                    Object obj12 = obj11;
                                    if (yf4.a(b20, mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1, this) == obj12) {
                                        return obj12;
                                    }
                                }
                            }
                        }
                        break;
                    case 4:
                        String str31 = "error_code";
                        int i32 = i8;
                        String str32 = str9;
                        Object obj13 = obj5;
                        int i33 = i7;
                        int i34 = i32;
                        Intent intent3 = new Intent("BROADCAST_OTA_COMPLETE");
                        ul2 c9 = AnalyticsHelper.f.c(str5);
                        if (skuModelBySerialPrefix != null) {
                            hashMap2 = new HashMap();
                            String sku = skuModelBySerialPrefix.getSku();
                            if (sku != null) {
                                i4 = i33;
                                String str33 = sku;
                                ul23 = c9;
                                str16 = str33;
                            } else {
                                ul23 = c9;
                                i4 = i33;
                                str16 = str32;
                            }
                            hashMap2.put("Style_Number", str16);
                            String deviceName = skuModelBySerialPrefix.getDeviceName();
                            if (deviceName == null) {
                                deviceName = str32;
                            }
                            hashMap2.put("Device_Name", deviceName);
                        } else {
                            ul23 = c9;
                            i4 = i33;
                            hashMap2 = null;
                        }
                        if (intExtra2 != ServiceActionResult.SUCCEEDED.ordinal()) {
                            Intent intent4 = intent3;
                            HashMap hashMap4 = hashMap2;
                            String str34 = str8;
                            ul2 ul27 = ul23;
                            if (hashMap4 != null) {
                                str14 = str31;
                                hashMap4.put(str14, String.valueOf(intExtra3));
                                this.this$Anon0.a.a("ota_fail", (Map<String, String>) hashMap4);
                            } else {
                                str14 = str31;
                            }
                            sl2 a10 = AnalyticsHelper.f.a("ota_session_optional_error");
                            a10.a(str14, String.valueOf(intExtra3));
                            if (ul27 != null) {
                                ul27.a(a10);
                                qa4 qa44 = qa4.a;
                            }
                            if (ul27 != null) {
                                ul27.a(String.valueOf(intExtra3));
                                qa4 qa45 = qa4.a;
                            }
                            intent4.putExtra(str34, false);
                            intent = intent4;
                            break;
                        } else {
                            if (!bundle.isEmpty()) {
                                this.this$Anon0.a.a((MisfitDeviceProfile) bundle.getParcelable("device"));
                            }
                            if (this.this$Anon0.a.a() != null) {
                                ILocalFLogger local12 = FLogger.INSTANCE.getLocal();
                                String b21 = MFDeviceService.U.b();
                                StringBuilder sb6 = new StringBuilder();
                                hashMap3 = hashMap2;
                                sb6.append("OTA complete, update fw version from device=");
                                sb6.append(str32);
                                local12.d(b21, sb6.toString());
                                MisfitDeviceProfile a11 = this.this$Anon0.a.a();
                                if (a11 != null) {
                                    str15 = a11.getFirmwareVersion();
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                hashMap3 = hashMap2;
                                ILocalFLogger local13 = FLogger.INSTANCE.getLocal();
                                String b22 = MFDeviceService.U.b();
                                local13.d(b22, "OTA complete, update fw version from temp=" + str32);
                                str15 = this.this$Anon0.a.m().i(str6);
                            }
                            if (TextUtils.isEmpty(str15)) {
                                str7 = str32;
                                HashMap hashMap5 = hashMap3;
                                intent = intent3;
                                ul2 = ul23;
                                break;
                            } else {
                                DeviceRepository d2 = this.this$Anon0.a.d();
                                if (str6 != null) {
                                    Device deviceBySerial2 = d2.getDeviceBySerial(str6);
                                    if (deviceBySerial2 == null) {
                                        intent2 = intent3;
                                        str7 = str32;
                                        hashMap = hashMap3;
                                        ul2 = ul23;
                                        break;
                                    } else {
                                        ILocalFLogger local14 = FLogger.INSTANCE.getLocal();
                                        String b23 = MFDeviceService.U.b();
                                        str7 = str32;
                                        StringBuilder sb7 = new StringBuilder();
                                        int i35 = i5;
                                        sb7.append("OTA complete, update fw version=");
                                        if (str15 != null) {
                                            sb7.append(str15);
                                            local14.d(b23, sb7.toString());
                                            deviceBySerial2.setFirmwareRevision(str15);
                                            DeviceRepository d3 = this.this$Anon0.a.d();
                                            this.L$Anon0 = zg4;
                                            this.I$Anon0 = intExtra;
                                            this.L$Anon1 = communicateMode;
                                            this.L$Anon2 = str6;
                                            this.I$Anon1 = intExtra2;
                                            this.I$Anon2 = intExtra3;
                                            this.L$Anon3 = arrayList2;
                                            this.L$Anon4 = bundle;
                                            this.L$Anon5 = skuModelBySerialPrefix;
                                            this.I$Anon3 = i35;
                                            this.I$Anon4 = i4;
                                            this.I$Anon5 = i34;
                                            intent2 = intent3;
                                            this.L$Anon6 = intent2;
                                            ul2 ul28 = ul23;
                                            this.L$Anon7 = ul28;
                                            hashMap = hashMap3;
                                            this.L$Anon8 = hashMap;
                                            this.L$Anon9 = str15;
                                            this.L$Anon10 = str15;
                                            this.L$Anon11 = deviceBySerial2;
                                            this.L$Anon12 = deviceBySerial2;
                                            this.label = 4;
                                            obj2 = d3.updateDevice(deviceBySerial2, false, this);
                                            Object obj14 = obj13;
                                            if (obj2 != obj14) {
                                                ul22 = ul28;
                                                break;
                                            } else {
                                                return obj14;
                                            }
                                        } else {
                                            kd4.a();
                                            throw null;
                                        }
                                    }
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            }
                        }
                    case 5:
                        PortfolioApp.W.a((Object) new fj2(str6, intExtra2 == ServiceActionResult.SUCCEEDED.ordinal()));
                        break;
                    case 6:
                        if (intExtra2 != ServiceActionResult.SUCCEEDED.ordinal()) {
                            PortfolioApp.W.a((Object) new aj2(str6, false));
                            break;
                        } else {
                            PortfolioApp.W.a((Object) new aj2(str6, true));
                            break;
                        }
                    case 7:
                        if (!bundle.isEmpty()) {
                            this.this$Anon0.a.a((MisfitDeviceProfile) bundle.getParcelable("device"));
                            MFDeviceService mFDeviceService2 = this.this$Anon0.a;
                            MisfitDeviceProfile a12 = mFDeviceService2.a();
                            kd4.a((Object) str6, "serial");
                            mFDeviceService2.a(a12, str6);
                            break;
                        }
                        break;
                    case 8:
                        ILocalFLogger local15 = FLogger.INSTANCE.getLocal();
                        String b24 = MFDeviceService.U.b();
                        StringBuilder sb8 = new StringBuilder();
                        int i36 = i8;
                        sb8.append("onExchangeSecretKey result ");
                        sb8.append(intExtra2);
                        local15.d(b24, sb8.toString());
                        if (intExtra2 != ServiceActionResult.ASK_FOR_RANDOM_KEY.ordinal()) {
                            Object obj15 = obj5;
                            int i37 = i7;
                            int i38 = i36;
                            if (intExtra2 != ServiceActionResult.ASK_FOR_SERVER_SECRET_KEY.ordinal()) {
                                if (intExtra2 != ServiceActionResult.ASK_FOR_CURRENT_SECRET_KEY.ordinal()) {
                                    if (intExtra2 == ServiceActionResult.SUCCEEDED.ordinal() && !bundle.isEmpty()) {
                                        String string = bundle.getString(ButtonService.DEVICE_SECRET_KEY);
                                        if (!TextUtils.isEmpty(string)) {
                                            ILocalFLogger local16 = FLogger.INSTANCE.getLocal();
                                            String b25 = MFDeviceService.U.b();
                                            local16.d(b25, "encrypt secret key " + string + " for " + str6);
                                            fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new Anon7(this, str6, string, (yb4) null), 3, (Object) null);
                                            break;
                                        }
                                    }
                                } else {
                                    PortfolioApp c10 = this.this$Anon0.a.c();
                                    kd4.a((Object) str6, "serial");
                                    c10.i(str6);
                                    this.this$Anon0.a.s().a(new VerifySecretKeyUseCase.b(str6), new a(this, str6));
                                    break;
                                }
                            } else {
                                String string2 = bundle.getString(ButtonService.DEVICE_RANDOM_KEY);
                                Object obj16 = obj15;
                                DeviceRepository d4 = this.this$Anon0.a.d();
                                if (string2 != null) {
                                    deviceRepository = d4;
                                    str17 = string2;
                                } else {
                                    deviceRepository = d4;
                                    str17 = str9;
                                }
                                kd4.a((Object) str6, "serial");
                                this.L$Anon0 = zg4;
                                this.I$Anon0 = intExtra;
                                this.L$Anon1 = communicateMode;
                                this.L$Anon2 = str6;
                                this.I$Anon1 = intExtra2;
                                this.I$Anon2 = intExtra3;
                                this.L$Anon3 = arrayList2;
                                this.L$Anon4 = bundle;
                                this.L$Anon5 = skuModelBySerialPrefix;
                                this.I$Anon3 = i5;
                                this.I$Anon4 = i37;
                                this.I$Anon5 = i38;
                                this.L$Anon6 = string2;
                                this.label = 6;
                                obj3 = deviceRepository.swapPairingKey(str17, str6, this);
                                Object obj17 = obj16;
                                if (obj3 == obj17) {
                                    return obj17;
                                }
                            }
                        } else {
                            DeviceRepository d5 = this.this$Anon0.a.d();
                            kd4.a((Object) str6, "serial");
                            this.L$Anon0 = zg4;
                            this.I$Anon0 = intExtra;
                            this.L$Anon1 = communicateMode;
                            this.L$Anon2 = str6;
                            this.I$Anon1 = intExtra2;
                            this.I$Anon2 = intExtra3;
                            this.L$Anon3 = arrayList2;
                            this.L$Anon4 = bundle;
                            this.L$Anon5 = skuModelBySerialPrefix;
                            this.I$Anon3 = i5;
                            this.I$Anon4 = i7;
                            this.I$Anon5 = i36;
                            this.label = 5;
                            obj4 = d5.generatePairingKey(str6, this);
                            Object obj18 = obj5;
                            if (obj4 == obj18) {
                                return obj18;
                            }
                        }
                        break;
                    case 9:
                        if (intExtra2 == ServiceActionResult.ASK_FOR_WATCH_PARAMS.ordinal()) {
                            MFDeviceService mFDeviceService3 = this.this$Anon0.a;
                            kd4.a((Object) str6, "serial");
                            mFDeviceService3.a(str6, bundle);
                            break;
                        }
                        break;
                    default:
                        CommunicateMode communicateMode9 = communicateMode;
                        BleCommandResultManager.d.a(communicateMode9, new BleCommandResultManager.a(communicateMode9, str6, this.$intent));
                        break;
                }
            case 1:
                DianaSyncDataProcessing.a aVar3 = (DianaSyncDataProcessing.a) this.L$Anon12;
                List list4 = (List) this.L$Anon11;
                i = this.I$Anon6;
                List list5 = (List) this.L$Anon10;
                List list6 = (List) this.L$Anon9;
                MFUser mFUser = (MFUser) this.L$Anon8;
                UserProfile userProfile = (UserProfile) this.L$Anon7;
                ul2 ul29 = (ul2) this.L$Anon6;
                SKUModel sKUModel5 = (SKUModel) this.L$Anon5;
                Bundle bundle7 = (Bundle) this.L$Anon4;
                ArrayList arrayList7 = (ArrayList) this.L$Anon3;
                str4 = (String) this.L$Anon2;
                CommunicateMode communicateMode10 = (CommunicateMode) this.L$Anon1;
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
                break;
            case 2:
                HybridSyncDataProcessing.a aVar4 = (HybridSyncDataProcessing.a) this.L$Anon12;
                List list7 = (List) this.L$Anon11;
                i = this.I$Anon6;
                List list8 = (List) this.L$Anon10;
                List list9 = (List) this.L$Anon9;
                MFUser mFUser2 = (MFUser) this.L$Anon8;
                UserProfile userProfile2 = (UserProfile) this.L$Anon7;
                ul2 ul210 = (ul2) this.L$Anon6;
                SKUModel sKUModel6 = (SKUModel) this.L$Anon5;
                Bundle bundle8 = (Bundle) this.L$Anon4;
                ArrayList arrayList8 = (ArrayList) this.L$Anon3;
                str4 = (String) this.L$Anon2;
                CommunicateMode communicateMode11 = (CommunicateMode) this.L$Anon1;
                zg4 zg43 = (zg4) this.L$Anon0;
                try {
                    na4.a(obj);
                    break;
                } catch (Exception e12) {
                    e = e12;
                    str2 = str4;
                    str = "sync_session";
                    mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 = this;
                    break;
                }
            case 3:
                Device device = (Device) this.L$Anon7;
                Device device2 = (Device) this.L$Anon6;
                SKUModel sKUModel7 = (SKUModel) this.L$Anon5;
                Bundle bundle9 = (Bundle) this.L$Anon4;
                ArrayList arrayList9 = (ArrayList) this.L$Anon3;
                String str35 = (String) this.L$Anon2;
                CommunicateMode communicateMode12 = (CommunicateMode) this.L$Anon1;
                zg4 zg44 = (zg4) this.L$Anon0;
                na4.a(obj);
            case 4:
                Device device3 = (Device) this.L$Anon12;
                Device device4 = (Device) this.L$Anon11;
                String str36 = (String) this.L$Anon10;
                String str37 = (String) this.L$Anon9;
                ul22 = (ul2) this.L$Anon7;
                SKUModel sKUModel8 = (SKUModel) this.L$Anon5;
                Bundle bundle10 = (Bundle) this.L$Anon4;
                ArrayList arrayList10 = (ArrayList) this.L$Anon3;
                CommunicateMode communicateMode13 = (CommunicateMode) this.L$Anon1;
                zg4 zg45 = (zg4) this.L$Anon0;
                na4.a(obj);
                str5 = "ota_session";
                str8 = "OTA_RESULT";
                str7 = "";
                intent2 = (Intent) this.L$Anon6;
                str6 = (String) this.L$Anon2;
                hashMap = (HashMap) this.L$Anon8;
                obj2 = obj;
                break;
            case 5:
                SKUModel sKUModel9 = (SKUModel) this.L$Anon5;
                Bundle bundle11 = (Bundle) this.L$Anon4;
                ArrayList arrayList11 = (ArrayList) this.L$Anon3;
                CommunicateMode communicateMode14 = (CommunicateMode) this.L$Anon1;
                zg4 zg46 = (zg4) this.L$Anon0;
                na4.a(obj);
                str6 = (String) this.L$Anon2;
                str9 = "";
                obj4 = obj;
                break;
            case 6:
                String str38 = (String) this.L$Anon6;
                SKUModel sKUModel10 = (SKUModel) this.L$Anon5;
                Bundle bundle12 = (Bundle) this.L$Anon4;
                ArrayList arrayList12 = (ArrayList) this.L$Anon3;
                CommunicateMode communicateMode15 = (CommunicateMode) this.L$Anon1;
                zg4 zg47 = (zg4) this.L$Anon0;
                na4.a(obj);
                str6 = (String) this.L$Anon2;
                str9 = "";
                obj3 = obj;
                break;
            default:
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        z = true;
        ILocalFLogger local10222222 = FLogger.INSTANCE.getLocal();
        String b15222222 = MFDeviceService.U.b();
        StringBuilder sb5222222 = new StringBuilder();
        sb5222222.append("Error while processing sync data - e=");
        e.printStackTrace();
        sb5222222.append(qa4.a);
        local10222222.e(b15222222, sb5222222.toString());
        e.printStackTrace();
        PortfolioApp c5222222 = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c();
        CommunicateMode communicateMode6222222 = CommunicateMode.SYNC;
        c5222222.a(communicateMode6222222, str2, "Sync data result OK but has exception error e=" + e);
        IRemoteFLogger remote5222222 = FLogger.INSTANCE.getRemote();
        FLogger.Component component5222222 = FLogger.Component.APP;
        FLogger.Session session5222222 = FLogger.Session.SYNC;
        String b16222222 = MFDeviceService.U.b();
        remote5222222.i(component5222222, session5222222, str2, b16222222, "Sync data result OK but has exception error e=" + e);
        mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a.c().c(z);
        AnalyticsHelper.f.e(str);
        FossilNotificationBar.c.a(mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1.this$Anon0.a);
        return qa4.a;
    }
}
