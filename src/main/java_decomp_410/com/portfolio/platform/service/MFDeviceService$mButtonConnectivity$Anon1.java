package com.portfolio.platform.service;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFDeviceService$mButtonConnectivity$Anon1 implements ServiceConnection {
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService a;

    @DexIgnore
    public MFDeviceService$mButtonConnectivity$Anon1(MFDeviceService mFDeviceService) {
        this.a = mFDeviceService;
    }

    @DexIgnore
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        kd4.b(componentName, "name");
        kd4.b(iBinder, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(MFDeviceService.U.b(), "Connection to the BLE has been established");
        this.a.a(true);
        fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new MFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1(iBinder, (yb4) null), 3, (Object) null);
        this.a.A();
        this.a.z();
        FossilNotificationBar.c.a(this.a);
    }

    @DexIgnore
    public void onServiceDisconnected(ComponentName componentName) {
        kd4.b(componentName, "name");
        this.a.a(false);
    }
}
