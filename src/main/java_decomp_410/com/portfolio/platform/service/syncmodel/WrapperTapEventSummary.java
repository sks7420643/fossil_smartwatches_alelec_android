package com.portfolio.platform.service.syncmodel;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@Parcel
public class WrapperTapEventSummary {
    @DexIgnore
    public int goalId;
    @DexIgnore
    public int startTime;
    @DexIgnore
    public int timezoneOffsetInSecond;

    @DexIgnore
    @ParcelConstructor
    public WrapperTapEventSummary(int i, int i2, int i3) {
        this.startTime = i;
        this.timezoneOffsetInSecond = i2;
        this.goalId = i3;
    }
}
