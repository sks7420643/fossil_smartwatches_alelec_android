package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.portfolio.platform.service.BleCommandResultManager;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1", f = "BleCommandResultManager.kt", l = {}, m = "invokeSuspend")
public final class BleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ BleCommandResultManager.a $bleResult;
    @DexIgnore
    public /* final */ /* synthetic */ CommunicateMode $communicateMode;
    @DexIgnore
    public /* final */ /* synthetic */ BleCommandResultManager.d $observerList;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1(BleCommandResultManager.d dVar, CommunicateMode communicateMode, BleCommandResultManager.a aVar, yb4 yb4) {
        super(2, yb4);
        this.$observerList = dVar;
        this.$communicateMode = communicateMode;
        this.$bleResult = aVar;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // BleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1 bleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1 = new BleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1(this.$observerList, this.$communicateMode, this.$bleResult, yb4);
        // bleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1.p$ = (zg4) obj;
        // return bleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((BleCommandResultManager$retrieveBleResult$Anon1$Anon1$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        throw null;
        // cc4.a();
        // if (this.label == 0) {
        //     na4.a(obj);
        //     for (BleCommandResultManager.b a : this.$observerList.a()) {
        //         a.a(this.$communicateMode, this.$bleResult.a());
        //     }
        //     return qa4.a;
        // }
        // throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
