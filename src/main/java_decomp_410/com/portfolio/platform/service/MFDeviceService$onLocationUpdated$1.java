package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.MFDeviceService$onLocationUpdated$1", mo27670f = "MFDeviceService.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class MFDeviceService$onLocationUpdated$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.wearables.fsl.location.DeviceLocation $deviceLocation;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21435p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$onLocationUpdated$1(com.fossil.wearables.fsl.location.DeviceLocation deviceLocation, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$deviceLocation = deviceLocation;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.MFDeviceService$onLocationUpdated$1 mFDeviceService$onLocationUpdated$1 = new com.portfolio.platform.service.MFDeviceService$onLocationUpdated$1(this.$deviceLocation, yb4);
        mFDeviceService$onLocationUpdated$1.f21435p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return mFDeviceService$onLocationUpdated$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.MFDeviceService$onLocationUpdated$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.dn2.f14174p.mo26576a().mo26567g().saveDeviceLocation(this.$deviceLocation);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
