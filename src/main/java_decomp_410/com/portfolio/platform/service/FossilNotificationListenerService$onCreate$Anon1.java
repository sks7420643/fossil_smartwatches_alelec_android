package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.ih4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.FossilNotificationListenerService$onCreate$Anon1", f = "FossilNotificationListenerService.kt", l = {83, 86}, m = "invokeSuspend")
public final class FossilNotificationListenerService$onCreate$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ FossilNotificationListenerService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FossilNotificationListenerService$onCreate$Anon1(FossilNotificationListenerService fossilNotificationListenerService, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = fossilNotificationListenerService;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // FossilNotificationListenerService$onCreate$Anon1 fossilNotificationListenerService$onCreate$Anon1 = new FossilNotificationListenerService$onCreate$Anon1(this.this$Anon0, yb4);
        // fossilNotificationListenerService$onCreate$Anon1.p$ = (zg4) obj;
        // return fossilNotificationListenerService$onCreate$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((FossilNotificationListenerService$onCreate$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            this.L$Anon0 = zg4;
            this.label = 1;
            if (ih4.a(5000, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (PortfolioApp.W.c().C()) {
            FLogger.INSTANCE.getLocal().d(FossilNotificationListenerService.s.a(), "onCreate() - enable Music Control Component.");
            MusicControlComponent b = this.this$Anon0.k;
            FossilNotificationListenerService fossilNotificationListenerService = this.this$Anon0;
            this.L$Anon0 = zg4;
            this.label = 2;
            if (b.a(fossilNotificationListenerService, (yb4<? super qa4>) this) == a) {
                return a;
            }
        }
        return qa4.a;
    }
}
