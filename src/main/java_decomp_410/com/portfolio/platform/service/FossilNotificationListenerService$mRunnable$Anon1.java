package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.wc4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FossilNotificationListenerService$mRunnable$Anon1 extends Lambda implements wc4<qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ FossilNotificationListenerService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FossilNotificationListenerService$mRunnable$Anon1(FossilNotificationListenerService fossilNotificationListenerService) {
        super(0);
        this.this$Anon0 = fossilNotificationListenerService;
    }

    @DexIgnore
    public final void invoke() {
        FLogger.INSTANCE.getLocal().d(FossilNotificationListenerService.s.a(), "Running runnable");
        this.this$Anon0.b();
    }
}
