package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yk2;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.MFDeviceService$updateDeviceData$Anon1", f = "MFDeviceService.kt", l = {859, 868}, m = "invokeSuspend")
public final class MFDeviceService$updateDeviceData$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MisfitDeviceProfile $deviceProfile;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isNeedUpdateRemote;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$updateDeviceData$Anon1(MFDeviceService mFDeviceService, String str, boolean z, MisfitDeviceProfile misfitDeviceProfile, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = mFDeviceService;
        this.$serial = str;
        this.$isNeedUpdateRemote = z;
        this.$deviceProfile = misfitDeviceProfile;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MFDeviceService$updateDeviceData$Anon1 mFDeviceService$updateDeviceData$Anon1 = new MFDeviceService$updateDeviceData$Anon1(this.this$Anon0, this.$serial, this.$isNeedUpdateRemote, this.$deviceProfile, yb4);
        mFDeviceService$updateDeviceData$Anon1.p$ = (zg4) obj;
        return mFDeviceService$updateDeviceData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFDeviceService$updateDeviceData$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0225 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0230  */
    public final Object invokeSuspend(Object obj) {
        Device device;
        zg4 zg4;
        Device device2;
        MisfitDeviceProfile misfitDeviceProfile;
        VibrationStrengthObj vibrationStrengthObj;
        DeviceRepository d;
        boolean z;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            device2 = this.this$Anon0.d().getDeviceBySerial(this.$serial);
            if (device2 != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = MFDeviceService.U.b();
                local.d(b, "updateDeviceData localDevice " + device2 + " forceUpdateRemote " + this.$isNeedUpdateRemote);
                MisfitDeviceProfile misfitDeviceProfile2 = this.$deviceProfile;
                if (misfitDeviceProfile2 != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String b2 = MFDeviceService.U.b();
                    local2.d(b2, "updateDeviceData batteryLevel " + this.$deviceProfile.getBatteryLevel() + " fwVersion " + this.$deviceProfile.getFirmwareVersion() + " major " + this.$deviceProfile.getMicroAppMajorVersion() + " minor " + this.$deviceProfile.getMicroAppMinorVersion());
                    vibrationStrengthObj = this.$deviceProfile.getVibrationStrength();
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String b3 = MFDeviceService.U.b();
                    StringBuilder sb = new StringBuilder();
                    sb.append("updateDeviceData - isDefaultValue: ");
                    sb.append(vibrationStrengthObj.isDefaultValue());
                    local3.d(b3, sb.toString());
                    if (!vibrationStrengthObj.isDefaultValue()) {
                        int b4 = yk2.b(vibrationStrengthObj.getVibrationStrengthLevel());
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String b5 = MFDeviceService.U.b();
                        local4.d(b5, "updateDeviceData - newVibrationLvl: " + b4 + " - device: " + device2);
                        device2.setVibrationStrength(dc4.a(b4));
                    }
                    if (this.$deviceProfile.getBatteryLevel() > 0) {
                        device2.setBatteryLevel(this.$deviceProfile.getBatteryLevel());
                        if (device2.getBatteryLevel() > 100) {
                            device2.setBatteryLevel(100);
                        }
                    }
                    device2.setFirmwareRevision(this.$deviceProfile.getFirmwareVersion());
                    device2.setSku(this.$deviceProfile.getDeviceModel());
                    device2.setMacAddress(this.$deviceProfile.getAddress());
                    if ((device2.getMajor() == this.$deviceProfile.getMicroAppMajorVersion() && device2.getMinor() == this.$deviceProfile.getMicroAppMinorVersion()) ? false : true) {
                        MicroAppRepository k = this.this$Anon0.k();
                        String str = this.$serial;
                        String valueOf = String.valueOf(this.$deviceProfile.getMicroAppMajorVersion());
                        String valueOf2 = String.valueOf(this.$deviceProfile.getMicroAppMinorVersion());
                        this.L$Anon0 = zg4;
                        this.L$Anon1 = device2;
                        this.L$Anon2 = device2;
                        this.L$Anon3 = misfitDeviceProfile2;
                        this.L$Anon4 = vibrationStrengthObj;
                        this.label = 1;
                        if (k.downloadMicroAppVariant(str, valueOf, valueOf2, this) == a) {
                            return a;
                        }
                        misfitDeviceProfile = misfitDeviceProfile2;
                        device = device2;
                    } else {
                        misfitDeviceProfile = misfitDeviceProfile2;
                        device = device2;
                        device2.setMajor(this.$deviceProfile.getMicroAppMajorVersion());
                        device2.setMinor(this.$deviceProfile.getMicroAppMinorVersion());
                        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                        String b6 = MFDeviceService.U.b();
                        local5.d(b6, "update device data " + device2);
                        d = this.this$Anon0.d();
                        z = this.$isNeedUpdateRemote;
                        this.L$Anon0 = zg4;
                        this.L$Anon1 = device2;
                        this.L$Anon2 = device;
                        this.L$Anon3 = misfitDeviceProfile;
                        this.L$Anon4 = vibrationStrengthObj;
                        this.label = 2;
                        if (d.updateDevice(device2, z, this) == a) {
                            return a;
                        }
                        if (this.$deviceProfile.getHeartRateMode() != HeartRateMode.NONE) {
                        }
                    }
                }
            }
            return qa4.a;
        } else if (i == 1) {
            vibrationStrengthObj = (VibrationStrengthObj) this.L$Anon4;
            misfitDeviceProfile = (MisfitDeviceProfile) this.L$Anon3;
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            device = (Device) this.L$Anon2;
            device2 = (Device) this.L$Anon1;
        } else if (i == 2) {
            VibrationStrengthObj vibrationStrengthObj2 = (VibrationStrengthObj) this.L$Anon4;
            MisfitDeviceProfile misfitDeviceProfile3 = (MisfitDeviceProfile) this.L$Anon3;
            Device device3 = (Device) this.L$Anon2;
            Device device4 = (Device) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            if (this.$deviceProfile.getHeartRateMode() != HeartRateMode.NONE) {
                this.this$Anon0.m().a(this.$deviceProfile.getHeartRateMode());
            }
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.m().a(this.$serial, 0, false);
        device2.setMajor(this.$deviceProfile.getMicroAppMajorVersion());
        device2.setMinor(this.$deviceProfile.getMicroAppMinorVersion());
        ILocalFLogger local52 = FLogger.INSTANCE.getLocal();
        String b62 = MFDeviceService.U.b();
        local52.d(b62, "update device data " + device2);
        d = this.this$Anon0.d();
        z = this.$isNeedUpdateRemote;
        this.L$Anon0 = zg4;
        this.L$Anon1 = device2;
        this.L$Anon2 = device;
        this.L$Anon3 = misfitDeviceProfile;
        this.L$Anon4 = vibrationStrengthObj;
        this.label = 2;
        if (d.updateDevice(device2, z, this) == a) {
        }
        if (this.$deviceProfile.getHeartRateMode() != HeartRateMode.NONE) {
        }
        return qa4.a;
    }
}
