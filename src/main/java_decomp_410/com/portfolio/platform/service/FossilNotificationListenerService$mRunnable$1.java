package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FossilNotificationListenerService$mRunnable$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.FossilNotificationListenerService this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FossilNotificationListenerService$mRunnable$1(com.portfolio.platform.service.FossilNotificationListenerService fossilNotificationListenerService) {
        super(0);
        this.this$0 = fossilNotificationListenerService;
    }

    @DexIgnore
    public final void invoke() {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.service.FossilNotificationListenerService.f21356s.mo39806a(), "Running runnable");
        this.this$0.mo39791b();
    }
}
