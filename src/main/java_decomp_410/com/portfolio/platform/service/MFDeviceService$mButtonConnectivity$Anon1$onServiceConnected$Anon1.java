package com.portfolio.platform.service;

import android.os.IBinder;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.portfolio.platform.PortfolioApp;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.MFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1", f = "MFDeviceService.kt", l = {}, m = "invokeSuspend")
public final class MFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ IBinder $service;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1(IBinder iBinder, yb4 yb4) {
        super(2, yb4);
        this.$service = iBinder;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1 mFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1 = new MFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1(this.$service, yb4);
        mFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1.p$ = (zg4) obj;
        return mFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFDeviceService$mButtonConnectivity$Anon1$onServiceConnected$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            PortfolioApp.a aVar = PortfolioApp.W;
            IButtonConnectivity asInterface = IButtonConnectivity.Stub.asInterface(this.$service);
            kd4.a((Object) asInterface, "IButtonConnectivity.Stub.asInterface(service)");
            aVar.b(asInterface);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
