package com.portfolio.platform.service.complicationapp.weather;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$getWeatherBaseOnLocation$1", mo27670f = "ComplicationWeatherService.kt", mo27671l = {118}, mo27672m = "invokeSuspend")
public final class ComplicationWeatherService$getWeatherBaseOnLocation$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21483p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ComplicationWeatherService$getWeatherBaseOnLocation$1(com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService complicationWeatherService, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = complicationWeatherService;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$getWeatherBaseOnLocation$1 complicationWeatherService$getWeatherBaseOnLocation$1 = new com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$getWeatherBaseOnLocation$1(this.this$0, yb4);
        complicationWeatherService$getWeatherBaseOnLocation$1.f21483p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return complicationWeatherService$getWeatherBaseOnLocation$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$getWeatherBaseOnLocation$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21483p$;
            if (this.this$0.mo39910k() != null) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a2 = com.fossil.blesdk.obfuscated.jp2.f16071j.mo28642a();
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append("getWeatherBaseOnLocation - location=");
                com.portfolio.platform.data.model.microapp.weather.WeatherSettings k = this.this$0.mo39910k();
                if (k != null) {
                    sb.append(k.getLocation());
                    local.mo33255d(a2, sb.toString());
                    com.portfolio.platform.data.model.microapp.weather.WeatherSettings k2 = this.this$0.mo39910k();
                    if (k2 == null) {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    } else if (!k2.isUseCurrentLocation()) {
                        this.this$0.mo39911l();
                    } else {
                        java.util.Calendar instance = java.util.Calendar.getInstance();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "Calendar.getInstance()");
                        long timeInMillis = instance.getTimeInMillis();
                        com.portfolio.platform.data.model.microapp.weather.WeatherSettings k3 = this.this$0.mo39910k();
                        if (k3 == null) {
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        } else if (timeInMillis - k3.getUpdatedAt() < ((long) this.this$0.f21469k)) {
                            com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService complicationWeatherService = this.this$0;
                            com.portfolio.platform.data.model.microapp.weather.WeatherSettings k4 = complicationWeatherService.mo39910k();
                            if (k4 != null) {
                                complicationWeatherService.mo39901a(k4, true);
                                this.this$0.mo30197a();
                            } else {
                                com.fossil.blesdk.obfuscated.kd4.m24405a();
                                throw null;
                            }
                        } else {
                            com.portfolio.platform.data.LocationSource e = this.this$0.mo28641e();
                            com.portfolio.platform.PortfolioApp h = this.this$0.mo39907h();
                            this.L$0 = zg4;
                            this.label = 1;
                            obj = e.getLocation(h, false, this);
                            if (obj == a) {
                                return a;
                            }
                        }
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.LocationSource.Result result = (com.portfolio.platform.data.LocationSource.Result) obj;
        if (result.getErrorState() == com.portfolio.platform.data.LocationSource.ErrorState.SUCCESS) {
            android.location.Location location = result.getLocation();
            com.portfolio.platform.data.model.microapp.weather.WeatherSettings k5 = this.this$0.mo39910k();
            if (k5 == null) {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            } else if (location != null) {
                k5.setLatLng(new com.google.android.gms.maps.model.LatLng(location.getLatitude(), location.getLongitude()));
                this.this$0.mo39911l();
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        } else {
            this.this$0.mo28640a(result.getErrorState());
            this.this$0.mo39905c(result.getErrorState());
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.fossil.blesdk.obfuscated.jp2.f16071j.mo28642a(), "getWeatherBaseOnLocation - using current location but location is null, stop service.");
            this.this$0.mo30197a();
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
