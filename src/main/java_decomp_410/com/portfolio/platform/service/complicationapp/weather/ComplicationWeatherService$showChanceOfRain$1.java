package com.portfolio.platform.service.complicationapp.weather;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$showChanceOfRain$1", mo27670f = "ComplicationWeatherService.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class ComplicationWeatherService$showChanceOfRain$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $rainProbabilityPercent;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21484p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ComplicationWeatherService$showChanceOfRain$1(com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService complicationWeatherService, int i, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = complicationWeatherService;
        this.$rainProbabilityPercent = i;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$showChanceOfRain$1 complicationWeatherService$showChanceOfRain$1 = new com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$showChanceOfRain$1(this.this$0, this.$rainProbabilityPercent, yb4);
        complicationWeatherService$showChanceOfRain$1.f21484p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return complicationWeatherService$showChanceOfRain$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$showChanceOfRain$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            this.this$0.mo39906g().upsertCustomizeRealData(new com.portfolio.platform.data.model.CustomizeRealData("chance_of_rain", java.lang.String.valueOf(this.$rainProbabilityPercent)));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
