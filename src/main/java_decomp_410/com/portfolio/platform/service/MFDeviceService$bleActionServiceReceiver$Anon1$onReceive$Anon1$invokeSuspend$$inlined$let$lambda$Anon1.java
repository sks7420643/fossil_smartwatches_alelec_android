package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qo2<Void>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $batteryLevel$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Device $device$inlined;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(yb4 yb4, MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1 mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1, int i, Device device) {
        super(2, yb4);
        this.this$Anon0 = mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1;
        this.$batteryLevel$inlined = i;
        this.$device$inlined = device;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(yb4, this.this$Anon0, this.$batteryLevel$inlined, this.$device$inlined);
        mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return mFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFDeviceService$bleActionServiceReceiver$Anon1$onReceive$Anon1$invokeSuspend$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            DeviceRepository d = this.this$Anon0.this$Anon0.a.d();
            Device device = this.$device$inlined;
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = d.updateDevice(device, true, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
