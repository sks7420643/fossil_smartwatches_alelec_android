package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile $this_run;
    @DexIgnore
    public int I$0;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.MFDeviceService this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$1(com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile misfitDeviceProfile, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.service.MFDeviceService mFDeviceService, java.lang.String str) {
        super(2, yb4);
        this.$this_run = misfitDeviceProfile;
        this.this$0 = mFDeviceService;
        this.$serial$inlined = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.service.MFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$1 mFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$1 = new com.portfolio.platform.service.MFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$1(this.$this_run, yb4, this.this$0, this.$serial$inlined);
        mFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return mFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.MFDeviceService$updateVibrationStrengthLevel$$inlined$run$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.p$;
            com.portfolio.platform.data.model.Device deviceBySerial = this.this$0.d().getDeviceBySerial(this.$serial$inlined);
            int b = com.fossil.blesdk.obfuscated.yk2.b(this.$this_run.getVibrationStrength().getVibrationStrengthLevel());
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String b2 = com.portfolio.platform.service.MFDeviceService.U.b();
            local.d(b2, "newVibrationLvl: " + b + " - device: " + deviceBySerial);
            if (deviceBySerial != null) {
                java.lang.Integer vibrationStrength = deviceBySerial.getVibrationStrength();
                if (vibrationStrength == null || vibrationStrength.intValue() != b) {
                    deviceBySerial.setVibrationStrength(com.fossil.blesdk.obfuscated.dc4.a(b));
                    com.portfolio.platform.data.source.DeviceRepository d = this.this$0.d();
                    this.L$0 = zg4;
                    this.L$1 = deviceBySerial;
                    this.I$0 = b;
                    this.label = 1;
                    if (d.updateDevice(deviceBySerial, false, this) == a) {
                        return a;
                    }
                }
            }
        } else if (i == 1) {
            com.portfolio.platform.data.model.Device device = (com.portfolio.platform.data.model.Device) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.a;
    }
}
