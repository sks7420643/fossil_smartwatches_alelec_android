package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFDeviceService$mButtonConnectivity$1 implements android.content.ServiceConnection {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.service.MFDeviceService f21433a;

    @DexIgnore
    public MFDeviceService$mButtonConnectivity$1(com.portfolio.platform.service.MFDeviceService mFDeviceService) {
        this.f21433a = mFDeviceService;
    }

    @DexIgnore
    public void onServiceConnected(android.content.ComponentName componentName, android.os.IBinder iBinder) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(componentName, "name");
        com.fossil.blesdk.obfuscated.kd4.m24411b(iBinder, com.misfit.frameworks.common.constants.Constants.SERVICE);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b(), "Connection to the BLE has been established");
        this.f21433a.mo39828a(true);
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25691a()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.service.MFDeviceService$mButtonConnectivity$1$onServiceConnected$1(iBinder, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        this.f21433a.mo39815A();
        this.f21433a.mo39860z();
        com.portfolio.platform.news.notifications.FossilNotificationBar.f21301c.mo39716a(this.f21433a);
    }

    @DexIgnore
    public void onServiceDisconnected(android.content.ComponentName componentName) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(componentName, "name");
        this.f21433a.mo39828a(false);
    }
}
