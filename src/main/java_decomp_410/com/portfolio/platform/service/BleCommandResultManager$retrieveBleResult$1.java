package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1", mo27670f = "BleCommandResultManager.kt", mo27671l = {69}, mo27672m = "invokeSuspend")
public final class BleCommandResultManager$retrieveBleResult$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $communicateModes;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21351p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleCommandResultManager$retrieveBleResult$1(java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$communicateModes = list;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1 bleCommandResultManager$retrieveBleResult$1 = new com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1(this.$communicateModes, yb4);
        bleCommandResultManager$retrieveBleResult$1.f21351p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return bleCommandResultManager$retrieveBleResult$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:9:0x004b  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Iterable iterable;
        java.util.Iterator it;
        java.lang.Object obj2;
        com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1 bleCommandResultManager$retrieveBleResult$1;
        com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode;
        java.lang.Object obj3;
        com.portfolio.platform.service.BleCommandResultManager.C5998a aVar;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f21351p$;
            java.util.List list = this.$communicateModes;
            it = list.iterator();
            zg4 = zg42;
            iterable = list;
            obj2 = a;
            bleCommandResultManager$retrieveBleResult$1 = this;
        } else if (i == 1) {
            com.portfolio.platform.service.BleCommandResultManager.C6001d dVar = (com.portfolio.platform.service.BleCommandResultManager.C6001d) this.L$6;
            aVar = (com.portfolio.platform.service.BleCommandResultManager.C5998a) this.L$5;
            it = (java.util.Iterator) this.L$2;
            iterable = (java.lang.Iterable) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            obj3 = a;
            communicateMode = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) this.L$4;
            bleCommandResultManager$retrieveBleResult$1 = this;
            com.portfolio.platform.service.BleCommandResultManager.f21343b.mo39778a(communicateMode, aVar);
            obj2 = obj3;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (it.hasNext()) {
            java.lang.Object next = it.next();
            communicateMode = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) next;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String c = com.portfolio.platform.service.BleCommandResultManager.f21342a;
            local.mo33255d(c, "retrieve mode=" + communicateMode);
            com.portfolio.platform.service.BleCommandResultManager.C5998a aVar2 = (com.portfolio.platform.service.BleCommandResultManager.C5998a) com.portfolio.platform.service.BleCommandResultManager.f21343b.mo39777a(communicateMode);
            if (aVar2 != null) {
                com.portfolio.platform.service.BleCommandResultManager.C6001d dVar2 = (com.portfolio.platform.service.BleCommandResultManager.C6001d) com.portfolio.platform.service.BleCommandResultManager.f21344c.get(communicateMode);
                if (dVar2 != null && dVar2.mo39783b() > 0) {
                    if (communicateMode == com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC) {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String c2 = com.portfolio.platform.service.BleCommandResultManager.f21342a;
                        local2.mo33255d(c2, "SyncMode=" + aVar2.mo39776a().getIntExtra("sync_result", 3));
                    }
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String c3 = com.portfolio.platform.service.BleCommandResultManager.f21342a;
                    local3.mo33255d(c3, "retrieve mode success=" + dVar2.mo39783b());
                    com.fossil.blesdk.obfuscated.pi4 c4 = com.fossil.blesdk.obfuscated.nh4.m25693c();
                    com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1$1$1 bleCommandResultManager$retrieveBleResult$1$1$1 = new com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1$1$1(dVar2, communicateMode, aVar2, (com.fossil.blesdk.obfuscated.yb4) null);
                    bleCommandResultManager$retrieveBleResult$1.L$0 = zg4;
                    bleCommandResultManager$retrieveBleResult$1.L$1 = iterable;
                    bleCommandResultManager$retrieveBleResult$1.L$2 = it;
                    bleCommandResultManager$retrieveBleResult$1.L$3 = next;
                    bleCommandResultManager$retrieveBleResult$1.L$4 = communicateMode;
                    bleCommandResultManager$retrieveBleResult$1.L$5 = aVar2;
                    bleCommandResultManager$retrieveBleResult$1.L$6 = dVar2;
                    bleCommandResultManager$retrieveBleResult$1.label = 1;
                    if (com.fossil.blesdk.obfuscated.yf4.m30997a(c4, bleCommandResultManager$retrieveBleResult$1$1$1, bleCommandResultManager$retrieveBleResult$1) == obj2) {
                        return obj2;
                    }
                    obj3 = obj2;
                    aVar = aVar2;
                    com.portfolio.platform.service.BleCommandResultManager.f21343b.mo39778a(communicateMode, aVar);
                    obj2 = obj3;
                    if (it.hasNext()) {
                    }
                    return obj2;
                }
            }
            if (it.hasNext()) {
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
