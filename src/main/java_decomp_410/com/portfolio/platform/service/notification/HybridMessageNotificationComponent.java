package com.portfolio.platform.service.notification;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.ContactsContract;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.j256.ormlite.field.FieldType;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.model.NotificationInfo;
import com.portfolio.platform.manager.LightAndHapticsManager;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import kotlin.coroutines.CoroutineContext;
import kotlin.text.StringsKt__StringsKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridMessageNotificationComponent {
    @DexIgnore
    public /* final */ SparseArray<c> a; // = new SparseArray<>();
    @DexIgnore
    public /* final */ Handler b; // = new Handler();
    @DexIgnore
    public b c; // = new b(0, (String) null, 0, 7, (fd4) null);
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ Handler e; // = new Handler();
    @DexIgnore
    public /* final */ ConcurrentLinkedQueue<c> f; // = new ConcurrentLinkedQueue<>();
    @DexIgnore
    public /* final */ String[] g;
    @DexIgnore
    public String h; // = "date DESC LIMIT 1";
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ Runnable j; // = new d(this);
    @DexIgnore
    public /* final */ Runnable k; // = new e(this);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ long c;

        @DexIgnore
        public b() {
            this(0, (String) null, 0, 7, (fd4) null);
        }

        @DexIgnore
        public b(long j, String str, long j2) {
            kd4.b(str, RemoteFLogger.MESSAGE_SENDER_KEY);
            this.a = j;
            this.b = str;
            this.c = j2;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final long b() {
            return this.c;
        }

        @DexIgnore
        public final long c() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if ((this.a == bVar.a) && kd4.a((Object) this.b, (Object) bVar.b)) {
                        if (this.c == bVar.c) {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            long j = this.a;
            int i = ((int) (j ^ (j >>> 32))) * 31;
            String str = this.b;
            int hashCode = str != null ? str.hashCode() : 0;
            long j2 = this.c;
            return ((i + hashCode) * 31) + ((int) (j2 ^ (j2 >>> 32)));
        }

        @DexIgnore
        public String toString() {
            return "InboxMessage(id=" + this.a + ", sender=" + this.b + ", dateSent=" + this.c + ")";
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(long j, String str, long j2, int i, fd4 fd4) {
            throw null;
            // this(r2, (i & 2) != 0 ? "" : str, (i & 4) != 0 ? 0 : j2);
            // long j3 = (i & 1) != 0 ? 0 : j;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public c(String str, String str2, boolean z) {
            kd4.b(str, "packageName");
            kd4.b(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
            this.a = str;
            this.b = str2;
            this.c = z;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final boolean c() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof c) {
                    c cVar = (c) obj;
                    if (kd4.a((Object) this.a, (Object) cVar.a) && kd4.a((Object) this.b, (Object) cVar.b)) {
                        if (this.c == cVar.c) {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.b;
            if (str2 != null) {
                i = str2.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z = this.c;
            if (z) {
                z = true;
            }
            return i2 + (z ? 1 : 0);
        }

        @DexIgnore
        public String toString() {
            return "MessageNotification(packageName=" + this.a + ", sender=" + this.b + ", shouldCheckAppAndAllText=" + this.c + ")";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ HybridMessageNotificationComponent e;

        @DexIgnore
        public d(HybridMessageNotificationComponent hybridMessageNotificationComponent) {
            this.e = hybridMessageNotificationComponent;
        }

        @DexIgnore
        public final void run() {
            if (this.e.a.size() == 0) {
                this.e.d();
                return;
            }
            this.e.a.clear();
            this.e.b();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ HybridMessageNotificationComponent e;

        @DexIgnore
        public e(HybridMessageNotificationComponent hybridMessageNotificationComponent) {
            this.e = hybridMessageNotificationComponent;
        }

        @DexIgnore
        public final void run() {
            if (this.e.f.isEmpty()) {
                this.e.e();
                return;
            }
            this.e.a();
            this.e.c();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public HybridMessageNotificationComponent() {
        this.d = TextUtils.equals(Build.MANUFACTURER, "samsung") ? "display_recipient_ids" : "recipient_ids";
        this.g = new String[]{FieldType.FOREIGN_ID_FIELD_SUFFIX, "date", this.d};
    }

    @DexIgnore
    public final void b() {
        d();
        this.b.postDelayed(this.j, ButtonService.CONNECT_TIMEOUT);
    }

    @DexIgnore
    public final void c() {
        this.i = true;
        this.e.postDelayed(this.k, 500);
    }

    @DexIgnore
    public final void d() {
        this.b.removeCallbacksAndMessages((Object) null);
    }

    @DexIgnore
    public final void e() {
        this.i = false;
        this.e.removeCallbacksAndMessages((Object) null);
    }

    @DexIgnore
    public final boolean b(StatusBarNotification statusBarNotification) {
        String tag = statusBarNotification.getTag();
        if (tag == null || (!StringsKt__StringsKt.a((CharSequence) tag, (CharSequence) "sms", true) && !StringsKt__StringsKt.a((CharSequence) tag, (CharSequence) "mms", true) && !StringsKt__StringsKt.a((CharSequence) tag, (CharSequence) "rcs", true))) {
            return TextUtils.equals(statusBarNotification.getNotification().category, "msg");
        }
        return true;
    }

    @DexIgnore
    public final fi4 a(StatusBarNotification statusBarNotification) {
        throw null;
        // kd4.b(statusBarNotification, "sbn");
        // return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new HybridMessageNotificationComponent$handleNotificationPosted$Anon1(this, statusBarNotification, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a() {
        FLogger.INSTANCE.getLocal().d("HybridMessageNotificationComponent", "processMessage()");
        c poll = this.f.poll();
        if (poll != null) {
            LightAndHapticsManager.i.a().a(new NotificationInfo(NotificationSource.TEXT, poll.b(), "", poll.a()), poll.c());
            this.a.put(poll.hashCode(), poll);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0056, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        com.fossil.blesdk.obfuscated.qc4.a(r8, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005a, code lost:
        throw r1;
     */
    @DexIgnore
    public final b b(Context context, String str) {
        throw null;
        // try {
        //     Cursor query = context.getContentResolver().query(Uri.parse("content://mms-sms/conversations?simple=true"), this.g, (String) null, (String[]) null, this.h);
        //     if (query != null) {
        //         if (query.moveToFirst()) {
        //             String string = query.getString(query.getColumnIndex(str));
        //             long j2 = query.getLong(query.getColumnIndex(FieldType.FOREIGN_ID_FIELD_SUFFIX));
        //             long j3 = query.getLong(query.getColumnIndex("date"));
        //             query.close();
        //             kd4.a((Object) string, "recipientIds");
        //             b bVar = new b(j2, string, j3);
        //             qc4.a(query, (Throwable) null);
        //             return bVar;
        //         }
        //         query.close();
        //         qa4 qa4 = qa4.a;
        //         qc4.a(query, (Throwable) null);
        //     }
        // } catch (Exception e2) {
        //     FLogger.INSTANCE.getLocal().e("HybridMessageNotificationComponent", e2.getMessage());
        // }
        // return null;
    }

    @DexIgnore
    public final void a(String str, String str2, boolean z) {
        if (this.a.size() == 0) {
            b();
        }
        c cVar = new c(str2, str, z);
        if (this.a.indexOfKey(cVar.hashCode()) < 0) {
            this.f.offer(cVar);
            if (!this.i) {
                c();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0049, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        com.fossil.blesdk.obfuscated.qc4.a(r11, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004d, code lost:
        throw r12;
     */
    @DexIgnore
    public final List<String> a(Context context, long j2) {
        throw null;
        // ArrayList arrayList = new ArrayList();
        // try {
        //     Cursor query = context.getContentResolver().query(ContentUris.withAppendedId(Uri.parse("content://mms-sms/canonical-address"), j2), (String[]) null, (String) null, (String[]) null, (String) null);
        //     if (query != null) {
        //         if (query.moveToFirst()) {
        //             String string = query.getString(query.getColumnIndex("address"));
        //             if (!TextUtils.isEmpty(string)) {
        //                 kd4.a((Object) string, "address");
        //                 arrayList.addAll(a(context, string));
        //             }
        //             query.close();
        //         }
        //         qa4 qa4 = qa4.a;
        //         qc4.a(query, (Throwable) null);
        //     }
        // } catch (Exception e2) {
        //     FLogger.INSTANCE.getLocal().e("HybridMessageNotificationComponent", e2.getMessage());
        // }
        // return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        com.fossil.blesdk.obfuscated.qc4.a(r10, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005f, code lost:
        throw r1;
     */
    @DexIgnore
    public final List<String> a(Context context, String str) {
        throw null;
        // ArrayList arrayList = new ArrayList();
        // try {
        //     Cursor query = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(str)), new String[]{"display_name", "normalized_number"}, (String) null, (String[]) null, (String) null);
        //     if (query != null) {
        //         if (query.moveToFirst()) {
        //             do {
        //                 String string = query.getString(query.getColumnIndex("normalized_number"));
        //                 String string2 = query.getString(query.getColumnIndex("display_name"));
        //                 if (string2 != null) {
        //                     string = string2;
        //                 }
        //                 kd4.a((Object) string, "name");
        //                 arrayList.add(string);
        //             } while (query.moveToNext());
        //         } else {
        //             arrayList.add(str);
        //         }
        //         query.close();
        //         qa4 qa4 = qa4.a;
        //         qc4.a(query, (Throwable) null);
        //     }
        // } catch (Exception e2) {
        //     FLogger.INSTANCE.getLocal().e("HybridMessageNotificationComponent", e2.getMessage());
        // }
        // return arrayList;
    }
}
