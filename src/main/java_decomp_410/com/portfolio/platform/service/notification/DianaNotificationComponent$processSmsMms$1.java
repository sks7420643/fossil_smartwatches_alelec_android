package com.portfolio.platform.service.notification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processSmsMms$1", mo27670f = "DianaNotificationComponent.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class DianaNotificationComponent$processSmsMms$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $content;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $phoneNumber;
    @DexIgnore
    public /* final */ /* synthetic */ long $receivedTime;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21624p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.notification.DianaNotificationComponent this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaNotificationComponent$processSmsMms$1(com.portfolio.platform.service.notification.DianaNotificationComponent dianaNotificationComponent, java.lang.String str, java.lang.String str2, long j, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = dianaNotificationComponent;
        this.$phoneNumber = str;
        this.$content = str2;
        this.$receivedTime = j;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.notification.DianaNotificationComponent$processSmsMms$1 dianaNotificationComponent$processSmsMms$1 = new com.portfolio.platform.service.notification.DianaNotificationComponent$processSmsMms$1(this.this$0, this.$phoneNumber, this.$content, this.$receivedTime, yb4);
        dianaNotificationComponent$processSmsMms$1.f21624p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dianaNotificationComponent$processSmsMms$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.notification.DianaNotificationComponent$processSmsMms$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            if (!this.this$0.f21592g.mo26975M()) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.this$0.mo40045b(), "Process SMS/MMS by old solution");
                if (!this.this$0.mo40043a(this.$phoneNumber, false)) {
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                this.this$0.mo40050c((com.portfolio.platform.service.notification.DianaNotificationComponent.C6078d) new com.portfolio.platform.service.notification.DianaNotificationComponent.C6081f(this.this$0.mo40054d(this.$phoneNumber), this.$content, this.$receivedTime));
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
