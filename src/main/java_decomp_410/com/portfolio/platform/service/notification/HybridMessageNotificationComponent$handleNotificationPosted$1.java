package com.portfolio.platform.service.notification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.notification.HybridMessageNotificationComponent$handleNotificationPosted$1", mo27670f = "HybridMessageNotificationComponent.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class HybridMessageNotificationComponent$handleNotificationPosted$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.service.notification.StatusBarNotification $sbn;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21644p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.notification.HybridMessageNotificationComponent this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridMessageNotificationComponent$handleNotificationPosted$1(com.portfolio.platform.service.notification.HybridMessageNotificationComponent hybridMessageNotificationComponent, android.service.notification.StatusBarNotification statusBarNotification, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = hybridMessageNotificationComponent;
        this.$sbn = statusBarNotification;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.notification.HybridMessageNotificationComponent$handleNotificationPosted$1 hybridMessageNotificationComponent$handleNotificationPosted$1 = new com.portfolio.platform.service.notification.HybridMessageNotificationComponent$handleNotificationPosted$1(this.this$0, this.$sbn, yb4);
        hybridMessageNotificationComponent$handleNotificationPosted$1.f21644p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return hybridMessageNotificationComponent$handleNotificationPosted$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.notification.HybridMessageNotificationComponent$handleNotificationPosted$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ea  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        boolean z;
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            if (this.this$0.mo40109b(this.$sbn)) {
                com.portfolio.platform.service.notification.HybridMessageNotificationComponent hybridMessageNotificationComponent = this.this$0;
                android.content.Context applicationContext = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().getApplicationContext();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) applicationContext, "PortfolioApp.instance.applicationContext");
                com.portfolio.platform.service.notification.HybridMessageNotificationComponent.C6084b a = hybridMessageNotificationComponent.mo40107b(applicationContext, this.this$0.f21628d);
                if (a != null) {
                    java.lang.String a2 = a.mo40113a();
                    long b = a.mo40114b();
                    if (!android.text.TextUtils.isEmpty(a2) && this.this$0.f21627c.mo40115c() < b) {
                        z = false;
                        for (java.lang.String str : kotlin.text.StringsKt__StringsKt.m37069a((java.lang.CharSequence) a2, new java.lang.String[]{" "}, false, 0, 6, (java.lang.Object) null)) {
                            if (str != null) {
                                java.lang.Long a3 = com.fossil.blesdk.obfuscated.qv1.m12988a(kotlin.text.StringsKt__StringsKt.m37091d(str).toString());
                                if (a3 != null) {
                                    long longValue = a3.longValue();
                                    com.portfolio.platform.service.notification.HybridMessageNotificationComponent hybridMessageNotificationComponent2 = this.this$0;
                                    android.content.Context applicationContext2 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().getApplicationContext();
                                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) applicationContext2, "PortfolioApp.instance.applicationContext");
                                    java.util.List a4 = hybridMessageNotificationComponent2.mo40103a(applicationContext2, longValue);
                                    int i = 0;
                                    for (java.lang.Object next : a4) {
                                        int i2 = i + 1;
                                        if (i >= 0) {
                                            java.lang.String str2 = (java.lang.String) next;
                                            int intValue = com.fossil.blesdk.obfuscated.dc4.m20843a(i).intValue();
                                            if (!android.text.TextUtils.isEmpty(str2)) {
                                                this.this$0.f21627c = a;
                                                com.portfolio.platform.service.notification.HybridMessageNotificationComponent hybridMessageNotificationComponent3 = this.this$0;
                                                java.lang.String packageName = this.$sbn.getPackageName();
                                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) packageName, "sbn.packageName");
                                                hybridMessageNotificationComponent3.mo40106a(str2, packageName, intValue == a4.size() - 1);
                                                z = true;
                                            }
                                            i = i2;
                                        } else {
                                            com.fossil.blesdk.obfuscated.cb4.m20543c();
                                            throw null;
                                        }
                                    }
                                    continue;
                                }
                            } else {
                                throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                            }
                        }
                        if (!z) {
                            java.lang.String tag = this.$sbn.getTag();
                            if (tag != null && kotlin.text.StringsKt__StringsKt.m37072a((java.lang.CharSequence) tag, (java.lang.CharSequence) "one_to_one", true)) {
                                java.lang.String packageName2 = this.$sbn.getPackageName();
                                java.lang.CharSequence charSequence = this.$sbn.getNotification().tickerText;
                                if (charSequence != null) {
                                    try {
                                        int length = charSequence.length();
                                        int i3 = 0;
                                        while (true) {
                                            if (i3 >= length) {
                                                i3 = -1;
                                                break;
                                            } else if (com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) java.lang.String.valueOf(com.fossil.blesdk.obfuscated.dc4.m20840a(charSequence.charAt(i3)).charValue()), (java.lang.Object) ":")).booleanValue()) {
                                                break;
                                            } else {
                                                i3++;
                                            }
                                        }
                                        if (i3 != -1) {
                                            java.lang.String obj2 = charSequence.subSequence(0, i3).toString();
                                            if (obj2 != null) {
                                                packageName2 = kotlin.text.StringsKt__StringsKt.m37091d(obj2).toString();
                                            } else {
                                                throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                                            }
                                        }
                                    } catch (java.lang.Exception e) {
                                        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HybridMessageNotificationComponent", e.getMessage());
                                    }
                                }
                                com.portfolio.platform.service.notification.HybridMessageNotificationComponent hybridMessageNotificationComponent4 = this.this$0;
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) packageName2, "contact");
                                java.lang.String packageName3 = this.$sbn.getPackageName();
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) packageName3, "sbn.packageName");
                                hybridMessageNotificationComponent4.mo40106a(packageName2, packageName3, true);
                            }
                        }
                    }
                }
                z = false;
                if (!z) {
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
