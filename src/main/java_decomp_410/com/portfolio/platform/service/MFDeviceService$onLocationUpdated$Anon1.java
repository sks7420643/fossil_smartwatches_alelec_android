package com.portfolio.platform.service;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.MFDeviceService$onLocationUpdated$Anon1", f = "MFDeviceService.kt", l = {}, m = "invokeSuspend")
public final class MFDeviceService$onLocationUpdated$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ DeviceLocation $deviceLocation;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$onLocationUpdated$Anon1(DeviceLocation deviceLocation, yb4 yb4) {
        super(2, yb4);
        this.$deviceLocation = deviceLocation;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MFDeviceService$onLocationUpdated$Anon1 mFDeviceService$onLocationUpdated$Anon1 = new MFDeviceService$onLocationUpdated$Anon1(this.$deviceLocation, yb4);
        mFDeviceService$onLocationUpdated$Anon1.p$ = (zg4) obj;
        return mFDeviceService$onLocationUpdated$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFDeviceService$onLocationUpdated$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            dn2.p.a().g().saveDeviceLocation(this.$deviceLocation);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
