package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1$onReceive$1", mo27670f = "MFDeviceService.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class MFDeviceService$connectionStateChangeReceiver$1$onReceive$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.content.Context $context;
    @DexIgnore
    public /* final */ /* synthetic */ android.content.Intent $intent;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21429p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1 this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1$onReceive$1$a")
    /* renamed from: com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1$onReceive$1$a */
    public static final class C6018a implements com.fossil.blesdk.obfuscated.f42.C4239b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1$onReceive$1 f21430a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ java.lang.String f21431b;

        @DexIgnore
        public C6018a(com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1$onReceive$1 mFDeviceService$connectionStateChangeReceiver$1$onReceive$1, java.lang.String str) {
            this.f21430a = mFDeviceService$connectionStateChangeReceiver$1$onReceive$1;
            this.f21431b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo27322a(android.location.Location location, int i) {
            com.portfolio.platform.service.MFDeviceService mFDeviceService = this.f21430a.this$0.f21428a;
            java.lang.String str = this.f21431b;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str, "serial");
            mFDeviceService.mo39824a(str, location, i);
            if (location != null) {
                com.fossil.blesdk.obfuscated.f42.m22078a((android.content.Context) this.f21430a.this$0.f21428a.mo39831c()).mo27318b((com.fossil.blesdk.obfuscated.f42.C4239b) this);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$connectionStateChangeReceiver$1$onReceive$1(com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1 mFDeviceService$connectionStateChangeReceiver$1, android.content.Intent intent, android.content.Context context, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = mFDeviceService$connectionStateChangeReceiver$1;
        this.$intent = intent;
        this.$context = context;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1$onReceive$1 mFDeviceService$connectionStateChangeReceiver$1$onReceive$1 = new com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1$onReceive$1(this.this$0, this.$intent, this.$context, yb4);
        mFDeviceService$connectionStateChangeReceiver$1$onReceive$1.f21429p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return mFDeviceService$connectionStateChangeReceiver$1$onReceive$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1$onReceive$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.lang.String stringExtra = this.$intent.getStringExtra(com.misfit.frameworks.common.constants.Constants.SERIAL_NUMBER);
            int intExtra = this.$intent.getIntExtra(com.misfit.frameworks.common.constants.Constants.CONNECTION_STATE, -1);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String b = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
            local.mo33255d(b, "---Inside .connectionStateChangeReceiver " + stringExtra + " status " + intExtra);
            if (intExtra == com.misfit.frameworks.buttonservice.enums.ConnectionStateChange.GATT_ON.ordinal()) {
                com.portfolio.platform.helper.DeviceHelper e = com.portfolio.platform.helper.DeviceHelper.f21188o.mo39565e();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) stringExtra, "serial");
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile a = e.mo39545a(stringExtra);
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String b2 = com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b();
                local2.mo33255d(b2, "---Inside .connectionStateChangeReceiver " + stringExtra + " deviceProfile=" + a);
                this.this$0.f21428a.mo39816a(a, stringExtra, true);
                if (!android.text.TextUtils.isEmpty(stringExtra) && com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) stringExtra, (java.lang.Object) com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e())) {
                    this.this$0.f21428a.mo39858x();
                }
                this.this$0.f21428a.mo39860z();
                if (((com.portfolio.platform.service.musiccontrol.MusicControlComponent) com.portfolio.platform.service.musiccontrol.MusicControlComponent.f21517o.mo31282a(this.$context)).mo39962b(stringExtra)) {
                    ((com.portfolio.platform.service.musiccontrol.MusicControlComponent) com.portfolio.platform.service.musiccontrol.MusicControlComponent.f21517o.mo31282a(this.$context)).mo39961b();
                }
            } else if (intExtra == com.misfit.frameworks.buttonservice.enums.ConnectionStateChange.GATT_OFF.ordinal() && !android.text.TextUtils.isEmpty(stringExtra) && com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) stringExtra, (java.lang.Object) com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e())) {
                this.this$0.f21428a.mo39859y();
            }
            com.portfolio.platform.PortfolioApp.f20941W.mo34583a((java.lang.Object) new com.fossil.blesdk.obfuscated.ij2(stringExtra, intExtra));
            com.portfolio.platform.service.MFDeviceService mFDeviceService = this.this$0.f21428a;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) stringExtra, "serial");
            mFDeviceService.mo39832c(stringExtra);
            if (this.this$0.f21428a.mo39842m().mo26968F()) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.service.MFDeviceService.f21377U.mo39862b(), "Collect location data only user enable locate feature");
                com.fossil.blesdk.obfuscated.f42.m22078a((android.content.Context) this.this$0.f21428a).mo27316a((com.fossil.blesdk.obfuscated.f42.C4239b) new com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1$onReceive$1.C6018a(this, stringExtra));
            }
            com.portfolio.platform.news.notifications.FossilNotificationBar.f21301c.mo39716a(this.this$0.f21428a);
            com.fossil.blesdk.obfuscated.C2796rc.m13141a((android.content.Context) this.this$0.f21428a.mo39831c()).mo15485a(this.$intent);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
