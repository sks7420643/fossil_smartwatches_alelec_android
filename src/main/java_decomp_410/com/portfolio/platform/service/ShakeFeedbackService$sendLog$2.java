package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.ShakeFeedbackService$sendLog$2", mo27670f = "ShakeFeedbackService.kt", mo27671l = {82}, mo27672m = "invokeSuspend")
public final class ShakeFeedbackService$sendLog$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.graphics.Bitmap $bitmap;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21462p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.ShakeFeedbackService this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ShakeFeedbackService$sendLog$2(com.portfolio.platform.service.ShakeFeedbackService shakeFeedbackService, android.graphics.Bitmap bitmap, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = shakeFeedbackService;
        this.$bitmap = bitmap;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.ShakeFeedbackService$sendLog$2 shakeFeedbackService$sendLog$2 = new com.portfolio.platform.service.ShakeFeedbackService$sendLog$2(this.this$0, this.$bitmap, yb4);
        shakeFeedbackService$sendLog$2.f21462p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return shakeFeedbackService$sendLog$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.ShakeFeedbackService$sendLog$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21462p$;
            com.portfolio.platform.service.ShakeFeedbackService shakeFeedbackService = this.this$0;
            android.graphics.Bitmap bitmap = this.$bitmap;
            this.L$0 = zg4;
            this.label = 1;
            if (shakeFeedbackService.mo39881a(bitmap, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
