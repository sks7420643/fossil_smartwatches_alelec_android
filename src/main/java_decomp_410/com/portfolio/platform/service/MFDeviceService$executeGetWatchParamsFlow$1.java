package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.MFDeviceService$executeGetWatchParamsFlow$1", mo27670f = "MFDeviceService.kt", mo27671l = {668, 670, 672}, mo27672m = "invokeSuspend")
public final class MFDeviceService$executeGetWatchParamsFlow$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ float $currentWPVersion;
    @DexIgnore
    public /* final */ /* synthetic */ int $majorVersion;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serial;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21432p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.MFDeviceService this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$executeGetWatchParamsFlow$1(com.portfolio.platform.service.MFDeviceService mFDeviceService, java.lang.String str, int i, float f, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = mFDeviceService;
        this.$serial = str;
        this.$majorVersion = i;
        this.$currentWPVersion = f;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.MFDeviceService$executeGetWatchParamsFlow$1 mFDeviceService$executeGetWatchParamsFlow$1 = new com.portfolio.platform.service.MFDeviceService$executeGetWatchParamsFlow$1(this.this$0, this.$serial, this.$majorVersion, this.$currentWPVersion, yb4);
        mFDeviceService$executeGetWatchParamsFlow$1.f21432p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return mFDeviceService$executeGetWatchParamsFlow$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.MFDeviceService$executeGetWatchParamsFlow$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f21432p$;
            com.portfolio.platform.data.source.DeviceRepository d = this.this$0.mo39833d();
            java.lang.String str = this.$serial;
            int i2 = this.$majorVersion;
            this.L$0 = zg4;
            this.label = 1;
            obj = d.getLatestWatchParamFromServer(str, i2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2 || i == 3) {
            com.portfolio.platform.data.model.WatchParameterResponse watchParameterResponse = (com.portfolio.platform.data.model.WatchParameterResponse) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.model.WatchParameterResponse watchParameterResponse2 = (com.portfolio.platform.data.model.WatchParameterResponse) obj;
        if (watchParameterResponse2 != null) {
            com.portfolio.platform.helper.WatchParamHelper t = this.this$0.mo39854t();
            java.lang.String str2 = this.$serial;
            float f = this.$currentWPVersion;
            this.L$0 = zg4;
            this.L$1 = watchParameterResponse2;
            this.label = 2;
            if (t.mo39601a(str2, f, watchParameterResponse2, this) == a) {
                return a;
            }
        } else {
            com.portfolio.platform.helper.WatchParamHelper t2 = this.this$0.mo39854t();
            java.lang.String str3 = this.$serial;
            float f2 = this.$currentWPVersion;
            this.L$0 = zg4;
            this.L$1 = watchParameterResponse2;
            this.label = 3;
            if (t2.mo39600a(str3, f2, this) == a) {
                return a;
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
