package com.portfolio.platform.service;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.view.Window;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.ns3;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.sr1;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import java.lang.ref.WeakReference;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ShakeFeedbackService$showQuestionDialog$Anon2 implements View.OnClickListener {
    @DexIgnore
    public /* final */ /* synthetic */ ShakeFeedbackService e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$Anon2$Anon1", f = "ShakeFeedbackService.kt", l = {262, 263}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService$showQuestionDialog$Anon2 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$Anon2$Anon1$Anon1")
        @gc4(c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$Anon2$Anon1$Anon1", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$Anon2$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0125Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0125Anon1(Anon1 anon1, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0125Anon1 anon1 = new C0125Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0125Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                cc4.a();
                if (this.label == 0) {
                    na4.a(obj);
                    sr1 c = this.this$Anon0.this$Anon0.e.d;
                    if (c != null) {
                        c.dismiss();
                        return qa4.a;
                    }
                    kd4.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ShakeFeedbackService$showQuestionDialog$Anon2 shakeFeedbackService$showQuestionDialog$Anon2, Bitmap bitmap, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = shakeFeedbackService$showQuestionDialog$Anon2;
            this.$bitmap = bitmap;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$bitmap, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            zg4 zg4;
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 = this.p$;
                ShakeFeedbackService shakeFeedbackService = this.this$Anon0.e;
                Bitmap bitmap = this.$bitmap;
                this.L$Anon0 = zg4;
                this.label = 1;
                if (shakeFeedbackService.a(bitmap, (yb4<? super qa4>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else if (i == 2) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
                return qa4.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            pi4 c = nh4.c();
            C0125Anon1 anon1 = new C0125Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 2;
            if (yf4.a(c, anon1, this) == a) {
                return a;
            }
            return qa4.a;
        }
    }

    @DexIgnore
    public ShakeFeedbackService$showQuestionDialog$Anon2(ShakeFeedbackService shakeFeedbackService) {
        this.e = shakeFeedbackService;
    }

    @DexIgnore
    public final void onClick(View view) {
        ns3.a aVar = ns3.a;
        WeakReference b = this.e.a;
        if (b != null) {
            Object obj = b.get();
            if (obj == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
            } else if (aVar.c((Activity) obj, 123)) {
                WeakReference b2 = this.e.a;
                if (b2 != null) {
                    Object obj2 = b2.get();
                    if (obj2 != null) {
                        Window window = ((Activity) obj2).getWindow();
                        kd4.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                        View decorView = window.getDecorView();
                        kd4.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                        View rootView = decorView.getRootView();
                        kd4.a((Object) rootView, "v1");
                        rootView.setDrawingCacheEnabled(true);
                        Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                        kd4.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                        rootView.setDrawingCacheEnabled(false);
                        fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, createBitmap, (yb4) null), 3, (Object) null);
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
                }
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }
}
