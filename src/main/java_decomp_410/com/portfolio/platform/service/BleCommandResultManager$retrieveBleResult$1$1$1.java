package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1$1$1", mo27670f = "BleCommandResultManager.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class BleCommandResultManager$retrieveBleResult$1$1$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.BleCommandResultManager.C5998a $bleResult;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.CommunicateMode $communicateMode;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.BleCommandResultManager.C6001d $observerList;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21352p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleCommandResultManager$retrieveBleResult$1$1$1(com.portfolio.platform.service.BleCommandResultManager.C6001d dVar, com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode, com.portfolio.platform.service.BleCommandResultManager.C5998a aVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$observerList = dVar;
        this.$communicateMode = communicateMode;
        this.$bleResult = aVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1$1$1 bleCommandResultManager$retrieveBleResult$1$1$1 = new com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1$1$1(this.$observerList, this.$communicateMode, this.$bleResult, yb4);
        bleCommandResultManager$retrieveBleResult$1$1$1.f21352p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return bleCommandResultManager$retrieveBleResult$1$1$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1$1$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            for (com.portfolio.platform.service.BleCommandResultManager.C5999b a : this.$observerList.mo39781a()) {
                a.mo27136a(this.$communicateMode, this.$bleResult.mo39776a());
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
