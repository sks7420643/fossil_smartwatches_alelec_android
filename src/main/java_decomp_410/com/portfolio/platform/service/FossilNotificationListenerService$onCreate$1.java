package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.FossilNotificationListenerService$onCreate$1", mo27670f = "FossilNotificationListenerService.kt", mo27671l = {83, 86}, mo27672m = "invokeSuspend")
public final class FossilNotificationListenerService$onCreate$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21374p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.FossilNotificationListenerService this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FossilNotificationListenerService$onCreate$1(com.portfolio.platform.service.FossilNotificationListenerService fossilNotificationListenerService, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = fossilNotificationListenerService;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.FossilNotificationListenerService$onCreate$1 fossilNotificationListenerService$onCreate$1 = new com.portfolio.platform.service.FossilNotificationListenerService$onCreate$1(this.this$0, yb4);
        fossilNotificationListenerService$onCreate$1.f21374p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return fossilNotificationListenerService$onCreate$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.FossilNotificationListenerService$onCreate$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f21374p$;
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.ih4.m23524a(5000, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34456C()) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.service.FossilNotificationListenerService.f21356s.mo39806a(), "onCreate() - enable Music Control Component.");
            com.portfolio.platform.service.musiccontrol.MusicControlComponent b = this.this$0.f21363k;
            com.portfolio.platform.service.FossilNotificationListenerService fossilNotificationListenerService = this.this$0;
            this.L$0 = zg4;
            this.label = 2;
            if (b.mo39952a(fossilNotificationListenerService, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                return a;
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
