package com.portfolio.platform.service.watchapp.commute;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager", mo27670f = "WatchAppCommuteTimeManager.kt", mo27671l = {153, 171}, mo27672m = "getDurationTimeBaseOnLocation")
public final class WatchAppCommuteTimeManager$getDurationTimeBaseOnLocation$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppCommuteTimeManager$getDurationTimeBaseOnLocation$1(com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager watchAppCommuteTimeManager, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = watchAppCommuteTimeManager;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.mo40170a((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this);
    }
}
