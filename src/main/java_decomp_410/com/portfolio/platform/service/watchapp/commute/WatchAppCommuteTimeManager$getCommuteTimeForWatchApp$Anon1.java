package com.portfolio.platform.service.watchapp.commute;

import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.oj2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.source.DianaPresetRepository;
import java.util.Iterator;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.TimeoutKt;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1", f = "WatchAppCommuteTimeManager.kt", l = {118}, m = "invokeSuspend")
public final class WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $destination;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppCommuteTimeManager this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1$Anon1", f = "WatchAppCommuteTimeManager.kt", l = {144}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public Object L$Anon2;
        @DexIgnore
        public Object L$Anon3;
        @DexIgnore
        public Object L$Anon4;
        @DexIgnore
        public Object L$Anon5;
        @DexIgnore
        public Object L$Anon6;
        @DexIgnore
        public Object L$Anon7;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1 watchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = watchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            T t;
            Object a = cc4.a();
            int i = this.label;
            String str = null;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                DianaPresetRepository b = this.this$Anon0.this$Anon0.b();
                String b2 = this.this$Anon0.this$Anon0.i;
                if (b2 != null) {
                    DianaPreset activePresetBySerial = b.getActivePresetBySerial(b2);
                    if (activePresetBySerial == null) {
                        return null;
                    }
                    Iterator<T> it = activePresetBySerial.getWatchapps().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        if (dc4.a(kd4.a((Object) ((DianaPresetWatchAppSetting) t).getId(), (Object) "commute-time")).booleanValue()) {
                            break;
                        }
                    }
                    DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t;
                    FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "getCommuteTimeForWatchApp activePreset " + activePresetBySerial);
                    if (dianaPresetWatchAppSetting == null) {
                        return null;
                    }
                    FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "getCommuteTimeForWatchApp json=" + dianaPresetWatchAppSetting.getSettings());
                    if (!oj2.a(dianaPresetWatchAppSetting.getSettings())) {
                        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) new Gson().a(dianaPresetWatchAppSetting.getSettings(), CommuteTimeWatchAppSetting.class);
                        AddressWrapper addressByName = commuteTimeWatchAppSetting.getAddressByName(this.this$Anon0.$destination);
                        JSONObject jSONObject = new JSONObject();
                        String id = addressByName != null ? addressByName.getId() : null;
                        AddressWrapper a2 = this.this$Anon0.this$Anon0.n;
                        if (kd4.a((Object) id, (Object) a2 != null ? a2.getId() : null)) {
                            AddressWrapper a3 = this.this$Anon0.this$Anon0.n;
                            if (a3 != null) {
                                str = a3.getId();
                            }
                            jSONObject.put("des", str);
                            jSONObject.put("type", "refresh");
                        } else {
                            this.this$Anon0.this$Anon0.n = addressByName;
                            AddressWrapper a4 = this.this$Anon0.this$Anon0.n;
                            if (a4 != null) {
                                str = a4.getId();
                            }
                            jSONObject.put("des", str);
                            jSONObject.put("type", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                            FLogger.INSTANCE.getRemote().startSession(FLogger.Session.DIANA_COMMUTE_TIME, this.this$Anon0.$serial, "WatchAppCommuteTimeManager");
                        }
                        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                        FLogger.Component component = FLogger.Component.APP;
                        FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
                        String str2 = this.this$Anon0.$serial;
                        String jSONObject2 = jSONObject.toString();
                        kd4.a((Object) jSONObject2, "jsonObject.toString()");
                        remote.i(component, session, str2, "WatchAppCommuteTimeManager", jSONObject2);
                        WatchAppCommuteTimeManager watchAppCommuteTimeManager = this.this$Anon0.this$Anon0;
                        this.L$Anon0 = zg4;
                        this.L$Anon1 = activePresetBySerial;
                        this.L$Anon2 = activePresetBySerial;
                        this.L$Anon3 = dianaPresetWatchAppSetting;
                        this.L$Anon4 = commuteTimeWatchAppSetting;
                        this.L$Anon5 = addressByName;
                        this.L$Anon6 = jSONObject;
                        this.L$Anon7 = dianaPresetWatchAppSetting;
                        this.label = 1;
                        if (watchAppCommuteTimeManager.a((yb4<? super qa4>) this) == a) {
                            return a;
                        }
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else if (i == 1) {
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) this.L$Anon7;
                JSONObject jSONObject3 = (JSONObject) this.L$Anon6;
                AddressWrapper addressWrapper = (AddressWrapper) this.L$Anon5;
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = (CommuteTimeWatchAppSetting) this.L$Anon4;
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting3 = (DianaPresetWatchAppSetting) this.L$Anon3;
                DianaPreset dianaPreset = (DianaPreset) this.L$Anon2;
                DianaPreset dianaPreset2 = (DianaPreset) this.L$Anon1;
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return qa4.a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1(WatchAppCommuteTimeManager watchAppCommuteTimeManager, String str, String str2, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = watchAppCommuteTimeManager;
        this.$destination = str;
        this.$serial = str2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1 watchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1 = new WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1(this.this$Anon0, this.$destination, this.$serial, yb4);
        watchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1.p$ = (zg4) obj;
        return watchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            if (TimeoutKt.a(4000, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
