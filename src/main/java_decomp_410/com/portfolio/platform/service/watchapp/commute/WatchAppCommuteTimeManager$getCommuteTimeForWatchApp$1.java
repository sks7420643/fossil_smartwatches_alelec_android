package com.portfolio.platform.service.watchapp.commute;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1", mo27670f = "WatchAppCommuteTimeManager.kt", mo27671l = {118}, mo27672m = "invokeSuspend")
public final class WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $destination;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serial;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21698p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1$1", mo27670f = "WatchAppCommuteTimeManager.kt", mo27671l = {144}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1$1 */
    public static final class C61001 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public java.lang.Object L$1;
        @DexIgnore
        public java.lang.Object L$2;
        @DexIgnore
        public java.lang.Object L$3;
        @DexIgnore
        public java.lang.Object L$4;
        @DexIgnore
        public java.lang.Object L$5;
        @DexIgnore
        public java.lang.Object L$6;
        @DexIgnore
        public java.lang.Object L$7;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21699p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C61001(com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1 watchAppCommuteTimeManager$getCommuteTimeForWatchApp$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = watchAppCommuteTimeManager$getCommuteTimeForWatchApp$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1.C61001 r0 = new com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1.C61001(this.this$0, yb4);
            r0.f21699p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1.C61001) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            T t;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            java.lang.String str = null;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21699p$;
                com.portfolio.platform.data.source.DianaPresetRepository b = this.this$0.this$0.mo40176b();
                java.lang.String b2 = this.this$0.this$0.f21687i;
                if (b2 != null) {
                    com.portfolio.platform.data.model.diana.preset.DianaPreset activePresetBySerial = b.getActivePresetBySerial(b2);
                    if (activePresetBySerial == null) {
                        return null;
                    }
                    java.util.Iterator<T> it = activePresetBySerial.getWatchapps().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        if (com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) t).getId(), (java.lang.Object) "commute-time")).booleanValue()) {
                            break;
                        }
                    }
                    com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) t;
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("WatchAppCommuteTimeManager", "getCommuteTimeForWatchApp activePreset " + activePresetBySerial);
                    if (dianaPresetWatchAppSetting == null) {
                        return null;
                    }
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("WatchAppCommuteTimeManager", "getCommuteTimeForWatchApp json=" + dianaPresetWatchAppSetting.getSettings());
                    if (!com.fossil.blesdk.obfuscated.oj2.m26116a(dianaPresetWatchAppSetting.getSettings())) {
                        com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = (com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting) new com.google.gson.Gson().mo23093a(dianaPresetWatchAppSetting.getSettings(), com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting.class);
                        com.portfolio.platform.data.model.diana.commutetime.AddressWrapper addressByName = commuteTimeWatchAppSetting.getAddressByName(this.this$0.$destination);
                        org.json.JSONObject jSONObject = new org.json.JSONObject();
                        java.lang.String id = addressByName != null ? addressByName.getId() : null;
                        com.portfolio.platform.data.model.diana.commutetime.AddressWrapper a2 = this.this$0.this$0.f21692n;
                        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) id, (java.lang.Object) a2 != null ? a2.getId() : null)) {
                            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper a3 = this.this$0.this$0.f21692n;
                            if (a3 != null) {
                                str = a3.getId();
                            }
                            jSONObject.put("des", str);
                            jSONObject.put("type", "refresh");
                        } else {
                            this.this$0.this$0.f21692n = addressByName;
                            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper a4 = this.this$0.this$0.f21692n;
                            if (a4 != null) {
                                str = a4.getId();
                            }
                            jSONObject.put("des", str);
                            jSONObject.put("type", com.facebook.share.internal.VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().startSession(com.misfit.frameworks.buttonservice.log.FLogger.Session.DIANA_COMMUTE_TIME, this.this$0.$serial, "WatchAppCommuteTimeManager");
                        }
                        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
                        com.misfit.frameworks.buttonservice.log.FLogger.Component component = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
                        com.misfit.frameworks.buttonservice.log.FLogger.Session session = com.misfit.frameworks.buttonservice.log.FLogger.Session.DIANA_COMMUTE_TIME;
                        java.lang.String str2 = this.this$0.$serial;
                        java.lang.String jSONObject2 = jSONObject.toString();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) jSONObject2, "jsonObject.toString()");
                        remote.mo33266i(component, session, str2, "WatchAppCommuteTimeManager", jSONObject2);
                        com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager watchAppCommuteTimeManager = this.this$0.this$0;
                        this.L$0 = zg4;
                        this.L$1 = activePresetBySerial;
                        this.L$2 = activePresetBySerial;
                        this.L$3 = dianaPresetWatchAppSetting;
                        this.L$4 = commuteTimeWatchAppSetting;
                        this.L$5 = addressByName;
                        this.L$6 = jSONObject;
                        this.L$7 = dianaPresetWatchAppSetting;
                        this.label = 1;
                        if (watchAppCommuteTimeManager.mo40170a((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                            return a;
                        }
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            } else if (i == 1) {
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting2 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) this.L$7;
                org.json.JSONObject jSONObject3 = (org.json.JSONObject) this.L$6;
                com.portfolio.platform.data.model.diana.commutetime.AddressWrapper addressWrapper = (com.portfolio.platform.data.model.diana.commutetime.AddressWrapper) this.L$5;
                com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = (com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting) this.L$4;
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting3 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) this.L$3;
                com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset = (com.portfolio.platform.data.model.diana.preset.DianaPreset) this.L$2;
                com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset2 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1(com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager watchAppCommuteTimeManager, java.lang.String str, java.lang.String str2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = watchAppCommuteTimeManager;
        this.$destination = str;
        this.$serial = str2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1 watchAppCommuteTimeManager$getCommuteTimeForWatchApp$1 = new com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1(this.this$0, this.$destination, this.$serial, yb4);
        watchAppCommuteTimeManager$getCommuteTimeForWatchApp$1.f21698p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return watchAppCommuteTimeManager$getCommuteTimeForWatchApp$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21698p$;
            com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1.C61001 r1 = new com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1.C61001(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            if (kotlinx.coroutines.TimeoutKt.m37094a(4000, r1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
