package com.portfolio.platform.service.watchapp.commute;

import android.location.Location;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$Listener$onLocationResult$Anon1", f = "WatchAppCommuteTimeManager.kt", l = {}, m = "invokeSuspend")
public final class WatchAppCommuteTimeManager$Listener$onLocationResult$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Location $location;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppCommuteTimeManager.Listener this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppCommuteTimeManager$Listener$onLocationResult$Anon1(WatchAppCommuteTimeManager.Listener listener, Location location, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = listener;
        this.$location = location;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WatchAppCommuteTimeManager$Listener$onLocationResult$Anon1 watchAppCommuteTimeManager$Listener$onLocationResult$Anon1 = new WatchAppCommuteTimeManager$Listener$onLocationResult$Anon1(this.this$Anon0, this.$location, yb4);
        watchAppCommuteTimeManager$Listener$onLocationResult$Anon1.p$ = (zg4) obj;
        return watchAppCommuteTimeManager$Listener$onLocationResult$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WatchAppCommuteTimeManager$Listener$onLocationResult$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            WatchAppCommuteTimeManager.this.a(this.$location.getLatitude(), this.$location.getLongitude());
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
