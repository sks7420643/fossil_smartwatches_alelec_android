package com.portfolio.platform.service.musiccontrol;

import android.content.ComponentName;
import android.media.session.MediaSessionManager;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.service.FossilNotificationListenerService;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableNotificationListener$Anon3", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
public final class MusicControlComponent$enableNotificationListener$Anon3 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ FossilNotificationListenerService $fossilNotificationListenerService;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$enableNotificationListener$Anon3(MusicControlComponent musicControlComponent, FossilNotificationListenerService fossilNotificationListenerService, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = musicControlComponent;
        this.$fossilNotificationListenerService = fossilNotificationListenerService;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // MusicControlComponent$enableNotificationListener$Anon3 musicControlComponent$enableNotificationListener$Anon3 = new MusicControlComponent$enableNotificationListener$Anon3(this.this$Anon0, this.$fossilNotificationListenerService, yb4);
        // musicControlComponent$enableNotificationListener$Anon3.p$ = (zg4) obj;
        // return musicControlComponent$enableNotificationListener$Anon3;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((MusicControlComponent$enableNotificationListener$Anon3) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            MediaSessionManager.OnActiveSessionsChangedListener b = this.this$Anon0.f;
            if (b == null) {
                return null;
            }
            MediaSessionManager c = this.this$Anon0.a;
            if (c != null) {
                c.addOnActiveSessionsChangedListener(b, new ComponentName(this.$fossilNotificationListenerService, FossilNotificationListenerService.class));
                return qa4.a;
            }
            kd4.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
