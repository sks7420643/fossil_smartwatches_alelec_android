package com.portfolio.platform.service.musiccontrol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicControlComponent$OldVersionMusicController$callback$1 extends android.support.p000v4.media.session.MediaControllerCompat.C0029a {

    @DexIgnore
    /* renamed from: d */
    public /* final */ /* synthetic */ com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController f21574d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ /* synthetic */ java.lang.String f21575e;

    @DexIgnore
    public MusicControlComponent$OldVersionMusicController$callback$1(com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController oldVersionMusicController, java.lang.String str) {
        this.f21574d = oldVersionMusicController;
        this.f21575e = str;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo118a(android.support.p000v4.media.session.PlaybackStateCompat playbackStateCompat) {
        super.mo118a(playbackStateCompat);
        int a = this.f21574d.mo39986a(playbackStateCompat);
        if (a != this.f21574d.mo40000h()) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.service.musiccontrol.MusicControlComponent.f21517o.mo39970a();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append(".onPlaybackStateChanged, PlaybackStateCompat = ");
            sb.append(playbackStateCompat != null ? java.lang.Integer.valueOf(playbackStateCompat.getState()) : null);
            local.mo33255d(a2, sb.toString());
            int h = this.f21574d.mo40000h();
            this.f21574d.mo39988a(a);
            com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController oldVersionMusicController = this.f21574d;
            oldVersionMusicController.mo39989a(h, a, (com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6053a) oldVersionMusicController);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo123b() {
        super.mo123b();
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a = com.portfolio.platform.service.musiccontrol.MusicControlComponent.f21517o.mo39970a();
        local.mo33255d(a, ".onSessionReady, package=" + this.f21574d.mo40004c());
    }

    @DexIgnore
    /* renamed from: a */
    public void mo116a(android.support.p000v4.media.MediaMetadataCompat mediaMetadataCompat) {
        super.mo116a(mediaMetadataCompat);
        if (mediaMetadataCompat != null) {
            java.lang.String a = com.fossil.blesdk.obfuscated.qj2.m26975a(mediaMetadataCompat.mo82a("android.media.metadata.TITLE"), com.facebook.internal.AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            java.lang.String a2 = com.fossil.blesdk.obfuscated.qj2.m26975a(mediaMetadataCompat.mo82a("android.media.metadata.ARTIST"), com.facebook.internal.AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            java.lang.String a3 = com.fossil.blesdk.obfuscated.qj2.m26975a(mediaMetadataCompat.mo82a("android.media.metadata.ALBUM"), com.facebook.internal.AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a4 = com.portfolio.platform.service.musiccontrol.MusicControlComponent.f21517o.mo39970a();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append(".onMetadataChanged, title=");
            sb.append(a);
            sb.append(", artist=");
            sb.append(a2);
            sb.append(", album=");
            sb.append(a3);
            sb.append(", state=");
            android.support.p000v4.media.session.MediaControllerCompat j = this.f21574d.mo40002j();
            sb.append(j != null ? j.mo96b() : null);
            local.mo33255d(a4, sb.toString());
            com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6055b bVar = new com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6055b(this.f21574d.mo40004c(), this.f21575e, a, a2, a3);
            if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) bVar, (java.lang.Object) this.f21574d.mo39999g())) {
                com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6055b g = this.f21574d.mo39999g();
                this.f21574d.mo39994a(bVar);
                this.f21574d.mo39995a(g, bVar);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo111a() {
        super.mo111a();
        com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController oldVersionMusicController = this.f21574d;
        oldVersionMusicController.mo39993a((com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6053a) oldVersionMusicController);
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25691a()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.service.musiccontrol.C6070x2187dc4e(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }
}
