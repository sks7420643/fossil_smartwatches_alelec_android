package com.portfolio.platform.service.musiccontrol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableNotificationListener$3", mo27670f = "MusicControlComponent.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class MusicControlComponent$enableNotificationListener$3 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.FossilNotificationListenerService $fossilNotificationListenerService;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21582p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.musiccontrol.MusicControlComponent this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$enableNotificationListener$3(com.portfolio.platform.service.musiccontrol.MusicControlComponent musicControlComponent, com.portfolio.platform.service.FossilNotificationListenerService fossilNotificationListenerService, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = musicControlComponent;
        this.$fossilNotificationListenerService = fossilNotificationListenerService;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableNotificationListener$3 musicControlComponent$enableNotificationListener$3 = new com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableNotificationListener$3(this.this$0, this.$fossilNotificationListenerService, yb4);
        musicControlComponent$enableNotificationListener$3.f21582p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return musicControlComponent$enableNotificationListener$3;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableNotificationListener$3) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            android.media.session.MediaSessionManager.OnActiveSessionsChangedListener b = this.this$0.f21523f;
            if (b == null) {
                return null;
            }
            android.media.session.MediaSessionManager c = this.this$0.f21518a;
            if (c != null) {
                c.addOnActiveSessionsChangedListener(b, new android.content.ComponentName(this.$fossilNotificationListenerService, com.portfolio.platform.service.FossilNotificationListenerService.class));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
