package com.portfolio.platform.service.musiccontrol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$NewNotificationMusicController$metadata$2", mo27670f = "MusicControlComponent.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class MusicControlComponent$NewNotificationMusicController$metadata$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6055b $currentMetadata;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6055b $oldMetadata;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21572p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.musiccontrol.MusicControlComponent.NewNotificationMusicController this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$NewNotificationMusicController$metadata$2(com.portfolio.platform.service.musiccontrol.MusicControlComponent.NewNotificationMusicController newNotificationMusicController, com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6055b bVar, com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6055b bVar2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = newNotificationMusicController;
        this.$oldMetadata = bVar;
        this.$currentMetadata = bVar2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.musiccontrol.MusicControlComponent$NewNotificationMusicController$metadata$2 musicControlComponent$NewNotificationMusicController$metadata$2 = new com.portfolio.platform.service.musiccontrol.MusicControlComponent$NewNotificationMusicController$metadata$2(this.this$0, this.$oldMetadata, this.$currentMetadata, yb4);
        musicControlComponent$NewNotificationMusicController$metadata$2.f21572p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return musicControlComponent$NewNotificationMusicController$metadata$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.musiccontrol.MusicControlComponent$NewNotificationMusicController$metadata$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            this.this$0.mo39976a(this.$oldMetadata, this.$currentMetadata);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
