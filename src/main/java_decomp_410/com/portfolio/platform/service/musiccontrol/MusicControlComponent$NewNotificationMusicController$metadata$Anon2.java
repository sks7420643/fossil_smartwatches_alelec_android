package com.portfolio.platform.service.musiccontrol;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$NewNotificationMusicController$metadata$Anon2", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
public final class MusicControlComponent$NewNotificationMusicController$metadata$Anon2 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.b $currentMetadata;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.b $oldMetadata;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.NewNotificationMusicController this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$NewNotificationMusicController$metadata$Anon2(MusicControlComponent.NewNotificationMusicController newNotificationMusicController, MusicControlComponent.b bVar, MusicControlComponent.b bVar2, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = newNotificationMusicController;
        this.$oldMetadata = bVar;
        this.$currentMetadata = bVar2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // MusicControlComponent$NewNotificationMusicController$metadata$Anon2 musicControlComponent$NewNotificationMusicController$metadata$Anon2 = new MusicControlComponent$NewNotificationMusicController$metadata$Anon2(this.this$Anon0, this.$oldMetadata, this.$currentMetadata, yb4);
        // musicControlComponent$NewNotificationMusicController$metadata$Anon2.p$ = (zg4) obj;
        // return musicControlComponent$NewNotificationMusicController$metadata$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((MusicControlComponent$NewNotificationMusicController$metadata$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            this.this$Anon0.a(this.$oldMetadata, this.$currentMetadata);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
