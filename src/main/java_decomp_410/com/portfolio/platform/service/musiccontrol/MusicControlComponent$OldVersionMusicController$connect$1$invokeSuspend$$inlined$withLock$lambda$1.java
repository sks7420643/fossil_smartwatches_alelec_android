package com.portfolio.platform.service.musiccontrol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super android.support.v4.media.MediaBrowserCompat>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.zg4 $this_launch$inlined;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$1(com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1 musicControlComponent$OldVersionMusicController$connect$1, com.fossil.blesdk.obfuscated.zg4 zg4) {
        super(2, yb4);
        this.this$0 = musicControlComponent$OldVersionMusicController$connect$1;
        this.$this_launch$inlined = zg4;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$1 musicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$1 = new com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$1(yb4, this.this$0, this.$this_launch$inlined);
        musicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return musicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.p$;
            com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController oldVersionMusicController = this.this$0.this$0;
            android.content.Context b = oldVersionMusicController.h;
            com.portfolio.platform.data.model.MediaAppDetails c = this.this$0.this$0.i;
            this.L$0 = zg4;
            this.label = 1;
            obj = oldVersionMusicController.a(b, c, (com.fossil.blesdk.obfuscated.yb4<? super android.support.v4.media.MediaBrowserCompat>) this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
