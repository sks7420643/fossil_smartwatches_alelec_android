package com.portfolio.platform.service.musiccontrol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicControlComponent$enableNotificationListener$2 implements android.media.session.MediaSessionManager.OnActiveSessionsChangedListener {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.service.musiccontrol.MusicControlComponent f21580a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableNotificationListener$2$a")
    /* renamed from: com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableNotificationListener$2$a */
    public static final class C6071a extends com.portfolio.platform.service.musiccontrol.MusicControlComponent.NewNotificationMusicController {

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableNotificationListener$2 f21581g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C6071a(com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableNotificationListener$2 musicControlComponent$enableNotificationListener$2, android.media.session.MediaController mediaController, android.media.session.MediaController mediaController2, java.lang.String str) {
            super(mediaController2, str);
            this.f21581g = musicControlComponent$enableNotificationListener$2;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo39976a(com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6055b bVar, com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6055b bVar2) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(bVar, "oldMetadata");
            com.fossil.blesdk.obfuscated.kd4.m24411b(bVar2, "newMetadata");
            super.mo39976a(bVar, bVar2);
            this.f21581g.f21580a.mo39959a(mo39978b());
        }

        @DexIgnore
        /* renamed from: a */
        public void mo39973a(int i, int i2, com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6053a aVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(aVar, "controller");
            super.mo39973a(i, i2, aVar);
            if (i2 == 3) {
                this.f21581g.f21580a.mo39958a(aVar);
            } else if (this.f21581g.f21580a.mo39964d() == null) {
                this.f21581g.f21580a.mo39958a(aVar);
            }
            this.f21581g.f21580a.mo39955a(i2);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo39974a(com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6053a aVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(aVar, "controller");
            super.mo39974a(aVar);
            this.f21581g.f21580a.mo39965e().mo40028b(aVar);
            if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) aVar, (java.lang.Object) this.f21581g.f21580a.mo39964d())) {
                this.f21581g.f21580a.mo39958a((com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6053a) null);
            }
        }
    }

    @DexIgnore
    public MusicControlComponent$enableNotificationListener$2(com.portfolio.platform.service.musiccontrol.MusicControlComponent musicControlComponent) {
        this.f21580a = musicControlComponent;
    }

    @DexIgnore
    public final void onActiveSessionsChanged(java.util.List<android.media.session.MediaController> list) {
        if (list != null && (!list.isEmpty())) {
            for (android.media.session.MediaController next : list) {
                if (!(this.f21580a.mo39965e().mo40026a(new com.portfolio.platform.service.musiccontrol.C6072x160eb222(next)) != null)) {
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) next, "activeMediaController");
                    com.portfolio.platform.service.musiccontrol.MusicControlComponent musicControlComponent = this.f21580a;
                    java.lang.String packageName = next.getPackageName();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) packageName, "activeMediaController.packageName");
                    com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableNotificationListener$2.C6071a aVar = new com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableNotificationListener$2.C6071a(this, next, next, musicControlComponent.mo39953a(packageName));
                    if (this.f21580a.mo39965e().mo40027a(aVar)) {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a = com.portfolio.platform.service.musiccontrol.MusicControlComponent.f21517o.mo39970a();
                        local.mo33255d(a, ".NewNotificationMusicController is added to list controller, packageName=" + aVar.mo40004c());
                    }
                }
            }
        }
    }
}
