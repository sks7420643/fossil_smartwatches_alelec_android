package com.portfolio.platform.service.musiccontrol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1", mo27670f = "MusicControlComponent.kt", mo27671l = {872, 558, 565, 569}, mo27672m = "invokeSuspend")
public final class MusicControlComponent$OldVersionMusicController$connect$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21577p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$OldVersionMusicController$connect$1(com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController oldVersionMusicController, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = oldVersionMusicController;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1 musicControlComponent$OldVersionMusicController$connect$1 = new com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1(this.this$0, yb4);
        musicControlComponent$OldVersionMusicController$connect$1.f21577p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return musicControlComponent$OldVersionMusicController$connect$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e1 A[Catch:{ all -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0120 A[Catch:{ all -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0141 A[Catch:{ all -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0148 A[Catch:{ all -> 0x0065 }] */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.dl4 dl4;
        com.fossil.blesdk.obfuscated.dl4 dl42;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        com.fossil.blesdk.obfuscated.gh4 gh4;
        android.support.p000v4.media.MediaBrowserCompat mediaBrowserCompat;
        com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController oldVersionMusicController;
        com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController oldVersionMusicController2;
        android.support.p000v4.media.MediaBrowserCompat i;
        com.fossil.blesdk.obfuscated.zg4 zg42;
        com.fossil.blesdk.obfuscated.dl4 dl43;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i2 = this.label;
        if (i2 == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg42 = this.f21577p$;
            dl43 = ((com.portfolio.platform.service.musiccontrol.MusicControlComponent) com.portfolio.platform.service.musiccontrol.MusicControlComponent.f21517o.mo31282a(com.portfolio.platform.PortfolioApp.f20941W.mo34589c())).mo39966f();
            this.L$0 = zg42;
            this.L$1 = dl43;
            this.label = 1;
            if (dl43.mo26535a((java.lang.Object) null, this) == a) {
                return a;
            }
        } else if (i2 == 1) {
            dl43 = (com.fossil.blesdk.obfuscated.dl4) this.L$1;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
        } else if (i2 == 2) {
            oldVersionMusicController2 = (com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController) this.L$3;
            gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
            dl4 = (com.fossil.blesdk.obfuscated.dl4) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            oldVersionMusicController2.mo39990a((android.support.p000v4.media.MediaBrowserCompat) obj);
            i = this.this$0.mo40001i();
            if (i != null) {
                this.this$0.mo39992a((com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController) null);
                dl42 = dl4;
                com.fossil.blesdk.obfuscated.qa4 qa4 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                dl42.mo26536a((java.lang.Object) null);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else if (!i.mo4d()) {
                this.this$0.mo39992a((com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController) null);
                com.fossil.blesdk.obfuscated.qa4 qa42 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                dl4.mo26536a((java.lang.Object) null);
                return qa42;
            } else {
                oldVersionMusicController = this.this$0;
                com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
                com.portfolio.platform.service.musiccontrol.C6067xf2438f4b musicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$2 = new com.portfolio.platform.service.musiccontrol.C6067xf2438f4b(i, (com.fossil.blesdk.obfuscated.yb4) null, this, zg4);
                this.L$0 = zg4;
                this.L$1 = dl4;
                this.L$2 = gh4;
                this.L$3 = i;
                this.L$4 = oldVersionMusicController;
                this.label = 3;
                java.lang.Object a2 = com.fossil.blesdk.obfuscated.yf4.m30997a(c, musicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$2, this);
                if (a2 == a) {
                    return a;
                }
                java.lang.Object obj2 = a2;
                mediaBrowserCompat = i;
                obj = obj2;
                oldVersionMusicController.mo39991a((android.support.p000v4.media.session.MediaControllerCompat) obj);
                if (this.this$0.mo40002j() == null) {
                }
                dl42 = dl4;
                com.fossil.blesdk.obfuscated.qa4 qa43 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                dl42.mo26536a((java.lang.Object) null);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        } else if (i2 == 3) {
            oldVersionMusicController = (com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController) this.L$4;
            mediaBrowserCompat = (android.support.p000v4.media.MediaBrowserCompat) this.L$3;
            gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
            dl4 = (com.fossil.blesdk.obfuscated.dl4) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            try {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                oldVersionMusicController.mo39991a((android.support.p000v4.media.session.MediaControllerCompat) obj);
                if (this.this$0.mo40002j() == null) {
                    this.this$0.mo39992a(this.this$0);
                    com.fossil.blesdk.obfuscated.pi4 c2 = com.fossil.blesdk.obfuscated.nh4.m25693c();
                    com.portfolio.platform.service.musiccontrol.C6068xf2438f4c musicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$3 = new com.portfolio.platform.service.musiccontrol.C6068xf2438f4c((com.fossil.blesdk.obfuscated.yb4) null, this, zg4);
                    this.L$0 = zg4;
                    this.L$1 = dl4;
                    this.L$2 = gh4;
                    this.L$3 = mediaBrowserCompat;
                    this.label = 4;
                    if (com.fossil.blesdk.obfuscated.yf4.m30997a(c2, musicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$3, this) == a) {
                        return a;
                    }
                } else {
                    this.this$0.mo39992a((com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController) null);
                }
                dl42 = dl4;
                com.fossil.blesdk.obfuscated.qa4 qa432 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                dl42.mo26536a((java.lang.Object) null);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } catch (Throwable th) {
                th = th;
            }
        } else if (i2 == 4) {
            android.support.p000v4.media.MediaBrowserCompat mediaBrowserCompat2 = (android.support.p000v4.media.MediaBrowserCompat) this.L$3;
            com.fossil.blesdk.obfuscated.gh4 gh42 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
            dl42 = (com.fossil.blesdk.obfuscated.dl4) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            try {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                dl4 = dl42;
                dl42 = dl4;
                com.fossil.blesdk.obfuscated.qa4 qa4322 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                dl42.mo26536a((java.lang.Object) null);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } catch (Throwable th2) {
                th = th2;
                dl4 = dl42;
                dl4.mo26536a((java.lang.Object) null);
                throw th;
            }
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.service.musiccontrol.MusicControlComponent.f21517o.mo39970a(), "connecting");
            com.fossil.blesdk.obfuscated.gh4 a3 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, com.fossil.blesdk.obfuscated.nh4.m25693c(), (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.service.musiccontrol.C6066xf2438f4a((com.fossil.blesdk.obfuscated.yb4) null, this, zg42), 2, (java.lang.Object) null);
            com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController oldVersionMusicController3 = this.this$0;
            this.L$0 = zg42;
            this.L$1 = dl43;
            this.L$2 = a3;
            this.L$3 = oldVersionMusicController3;
            this.label = 2;
            java.lang.Object a4 = a3.mo27693a(this);
            if (a4 == a) {
                return a;
            }
            com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController oldVersionMusicController4 = oldVersionMusicController3;
            zg4 = zg42;
            obj = a4;
            gh4 = a3;
            dl4 = dl43;
            oldVersionMusicController2 = oldVersionMusicController4;
            oldVersionMusicController2.mo39990a((android.support.p000v4.media.MediaBrowserCompat) obj);
            i = this.this$0.mo40001i();
            if (i != null) {
            }
        } catch (Throwable th3) {
            th = th3;
            dl4 = dl43;
            dl4.mo26536a((java.lang.Object) null);
            throw th;
        }
    }
}
