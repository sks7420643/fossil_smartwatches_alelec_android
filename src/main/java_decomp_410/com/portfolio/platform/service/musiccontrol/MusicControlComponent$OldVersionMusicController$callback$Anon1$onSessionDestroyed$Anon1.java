package com.portfolio.platform.service.musiccontrol;

import android.support.v4.media.MediaBrowserCompat;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$callback$Anon1$onSessionDestroyed$Anon1", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
public final class MusicControlComponent$OldVersionMusicController$callback$Anon1$onSessionDestroyed$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent$OldVersionMusicController$callback$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$OldVersionMusicController$callback$Anon1$onSessionDestroyed$Anon1(MusicControlComponent$OldVersionMusicController$callback$Anon1 musicControlComponent$OldVersionMusicController$callback$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = musicControlComponent$OldVersionMusicController$callback$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MusicControlComponent$OldVersionMusicController$callback$Anon1$onSessionDestroyed$Anon1 musicControlComponent$OldVersionMusicController$callback$Anon1$onSessionDestroyed$Anon1 = new MusicControlComponent$OldVersionMusicController$callback$Anon1$onSessionDestroyed$Anon1(this.this$Anon0, yb4);
        musicControlComponent$OldVersionMusicController$callback$Anon1$onSessionDestroyed$Anon1.p$ = (zg4) obj;
        return musicControlComponent$OldVersionMusicController$callback$Anon1$onSessionDestroyed$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MusicControlComponent$OldVersionMusicController$callback$Anon1$onSessionDestroyed$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            MediaBrowserCompat i = this.this$Anon0.d.i();
            if (i != null) {
                i.b();
            }
            this.this$Anon0.d.e();
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
