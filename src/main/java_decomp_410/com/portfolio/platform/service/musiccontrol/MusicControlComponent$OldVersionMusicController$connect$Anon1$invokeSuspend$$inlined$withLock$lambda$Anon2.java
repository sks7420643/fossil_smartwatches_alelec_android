package com.portfolio.platform.service.musiccontrol;

import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaControllerCompat;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super MediaControllerCompat>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MediaBrowserCompat $it;
    @DexIgnore
    public /* final */ /* synthetic */ zg4 $this_launch$inlined;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent$OldVersionMusicController$connect$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2(MediaBrowserCompat mediaBrowserCompat, yb4 yb4, MusicControlComponent$OldVersionMusicController$connect$Anon1 musicControlComponent$OldVersionMusicController$connect$Anon1, zg4 zg4) {
        super(2, yb4);
        this.$it = mediaBrowserCompat;
        this.this$Anon0 = musicControlComponent$OldVersionMusicController$connect$Anon1;
        this.$this_launch$inlined = zg4;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2 musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2 = new MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2(this.$it, yb4, this.this$Anon0, this.$this_launch$inlined);
        musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2.p$ = (zg4) obj;
        return musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return this.this$Anon0.this$Anon0.b(this.$it);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
