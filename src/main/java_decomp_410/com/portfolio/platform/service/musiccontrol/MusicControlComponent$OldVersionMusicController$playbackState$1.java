package com.portfolio.platform.service.musiccontrol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$playbackState$1", mo27670f = "MusicControlComponent.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class MusicControlComponent$OldVersionMusicController$playbackState$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $currentState;
    @DexIgnore
    public /* final */ /* synthetic */ int $oldState;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21579p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$OldVersionMusicController$playbackState$1(com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController oldVersionMusicController, int i, int i2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = oldVersionMusicController;
        this.$oldState = i;
        this.$currentState = i2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$playbackState$1 musicControlComponent$OldVersionMusicController$playbackState$1 = new com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$playbackState$1(this.this$0, this.$oldState, this.$currentState, yb4);
        musicControlComponent$OldVersionMusicController$playbackState$1.f21579p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return musicControlComponent$OldVersionMusicController$playbackState$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$playbackState$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.service.musiccontrol.MusicControlComponent.OldVersionMusicController oldVersionMusicController = this.this$0;
            oldVersionMusicController.mo39989a(this.$oldState, this.$currentState, (com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6053a) oldVersionMusicController);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
