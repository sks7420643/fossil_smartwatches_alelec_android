package com.portfolio.platform.service.musiccontrol;

import android.content.Context;
import android.support.v4.media.MediaBrowserCompat;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.MediaAppDetails;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super MediaBrowserCompat>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ zg4 $this_launch$inlined;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent$OldVersionMusicController$connect$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(yb4 yb4, MusicControlComponent$OldVersionMusicController$connect$Anon1 musicControlComponent$OldVersionMusicController$connect$Anon1, zg4 zg4) {
        super(2, yb4);
        this.this$Anon0 = musicControlComponent$OldVersionMusicController$connect$Anon1;
        this.$this_launch$inlined = zg4;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 = new MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(yb4, this.this$Anon0, this.$this_launch$inlined);
        musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1.p$ = (zg4) obj;
        return musicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MusicControlComponent$OldVersionMusicController$connect$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            MusicControlComponent.OldVersionMusicController oldVersionMusicController = this.this$Anon0.this$Anon0;
            Context b = oldVersionMusicController.h;
            MediaAppDetails c = this.this$Anon0.this$Anon0.i;
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = oldVersionMusicController.a(b, c, (yb4<? super MediaBrowserCompat>) this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
