package com.portfolio.platform.service.musiccontrol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$3 */
public final class C6068xf2438f4c extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.zg4 $this_launch$inlined;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21571p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6068xf2438f4c(com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1 musicControlComponent$OldVersionMusicController$connect$1, com.fossil.blesdk.obfuscated.zg4 zg4) {
        super(2, yb4);
        this.this$0 = musicControlComponent$OldVersionMusicController$connect$1;
        this.$this_launch$inlined = zg4;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.musiccontrol.C6068xf2438f4c musicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$3 = new com.portfolio.platform.service.musiccontrol.C6068xf2438f4c(yb4, this.this$0, this.$this_launch$inlined);
        musicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$3.f21571p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return musicControlComponent$OldVersionMusicController$connect$1$invokeSuspend$$inlined$withLock$lambda$3;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.musiccontrol.C6068xf2438f4c) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            android.support.p000v4.media.session.MediaControllerCompat j = this.this$0.this$0.mo40002j();
            if (j != null) {
                j.mo93a(this.this$0.this$0.mo39998f());
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
