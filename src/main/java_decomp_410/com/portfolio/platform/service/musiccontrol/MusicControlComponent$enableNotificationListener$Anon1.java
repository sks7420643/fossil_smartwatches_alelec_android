package com.portfolio.platform.service.musiccontrol;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.service.FossilNotificationListenerService;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent", f = "MusicControlComponent.kt", l = {235}, m = "enableNotificationListener")
public final class MusicControlComponent$enableNotificationListener$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicControlComponent$enableNotificationListener$Anon1(MusicControlComponent musicControlComponent, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = musicControlComponent;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((FossilNotificationListenerService) null, (yb4<? super qa4>) this);
    }
}
