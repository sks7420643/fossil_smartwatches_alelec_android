package com.portfolio.platform.service.musiccontrol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableNotificationListener$2$isHasThisController$1 */
public final class C6072x160eb222 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6053a, java.lang.Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ android.media.session.MediaController $activeMediaController;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6072x160eb222(android.media.session.MediaController mediaController) {
        super(1);
        this.$activeMediaController = mediaController;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke((com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6053a) obj));
    }

    @DexIgnore
    public final boolean invoke(com.portfolio.platform.service.musiccontrol.MusicControlComponent.C6053a aVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(aVar, "musicController");
        java.lang.String c = aVar.mo40004c();
        android.media.session.MediaController mediaController = this.$activeMediaController;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) mediaController, "activeMediaController");
        return com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) c, (java.lang.Object) mediaController.getPackageName());
    }
}
