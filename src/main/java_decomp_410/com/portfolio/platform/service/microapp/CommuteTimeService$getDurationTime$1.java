package com.portfolio.platform.service.microapp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.microapp.CommuteTimeService$getDurationTime$1", mo27670f = "CommuteTimeService.kt", mo27671l = {280}, mo27672m = "invokeSuspend")
public final class CommuteTimeService$getDurationTime$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.location.Location $location;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21514p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.service.microapp.CommuteTimeService this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeService$getDurationTime$1(com.portfolio.platform.service.microapp.CommuteTimeService commuteTimeService, android.location.Location location, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = commuteTimeService;
        this.$location = location;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.microapp.CommuteTimeService$getDurationTime$1 commuteTimeService$getDurationTime$1 = new com.portfolio.platform.service.microapp.CommuteTimeService$getDurationTime$1(this.this$0, this.$location, yb4);
        commuteTimeService$getDurationTime$1.f21514p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return commuteTimeService$getDurationTime$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.microapp.CommuteTimeService$getDurationTime$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21514p$;
            if (this.this$0.f21498m != null) {
                com.portfolio.platform.data.model.setting.CommuteTimeSetting a2 = this.this$0.f21498m;
                if (a2 != null) {
                    if (!(a2.getAddress().length() == 0)) {
                        com.fossil.blesdk.obfuscated.lr2 h = this.this$0.mo39933h();
                        if (h != null) {
                            com.portfolio.platform.data.model.setting.CommuteTimeSetting a3 = this.this$0.f21498m;
                            if (a3 != null) {
                                java.lang.String address = a3.getAddress();
                                com.google.maps.model.TravelMode travelMode = com.google.maps.model.TravelMode.DRIVING;
                                com.portfolio.platform.data.model.setting.CommuteTimeSetting a4 = this.this$0.f21498m;
                                if (a4 != null) {
                                    boolean avoidTolls = a4.getAvoidTolls();
                                    double latitude = this.$location.getLatitude();
                                    double longitude = this.$location.getLongitude();
                                    this.L$0 = zg4;
                                    this.label = 1;
                                    obj = h.mo29266a(address, travelMode, avoidTolls, latitude, longitude, this);
                                    if (obj == a) {
                                        return a;
                                    }
                                } else {
                                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                                    throw null;
                                }
                            } else {
                                com.fossil.blesdk.obfuscated.kd4.m24405a();
                                throw null;
                            }
                        } else {
                            com.fossil.blesdk.obfuscated.kd4.m24405a();
                            throw null;
                        }
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        long longValue = ((java.lang.Number) obj).longValue();
        if (longValue != -1) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a5 = com.portfolio.platform.service.microapp.CommuteTimeService.f21488A.mo39947a();
            local.mo33255d(a5, "getDurationTime duration " + longValue);
            this.this$0.f21497l = longValue;
            if (this.this$0.f21497l < ((long) 60)) {
                this.this$0.f21497l = 60;
            }
            this.this$0.mo39936k();
        } else {
            this.this$0.mo30197a();
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
