package com.portfolio.platform.service.microapp;

import android.location.Location;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lr2;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.google.maps.model.TravelMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.service.microapp.CommuteTimeService$getDurationTime$Anon1", f = "CommuteTimeService.kt", l = {280}, m = "invokeSuspend")
public final class CommuteTimeService$getDurationTime$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Location $location;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeService$getDurationTime$Anon1(CommuteTimeService commuteTimeService, Location location, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = commuteTimeService;
        this.$location = location;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // CommuteTimeService$getDurationTime$Anon1 commuteTimeService$getDurationTime$Anon1 = new CommuteTimeService$getDurationTime$Anon1(this.this$Anon0, this.$location, yb4);
        // commuteTimeService$getDurationTime$Anon1.p$ = (zg4) obj;
        // return commuteTimeService$getDurationTime$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((CommuteTimeService$getDurationTime$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            if (this.this$Anon0.m != null) {
                CommuteTimeSetting a2 = this.this$Anon0.m;
                if (a2 != null) {
                    if (!(a2.getAddress().length() == 0)) {
                        lr2 h = this.this$Anon0.h();
                        if (h != null) {
                            CommuteTimeSetting a3 = this.this$Anon0.m;
                            if (a3 != null) {
                                String address = a3.getAddress();
                                TravelMode travelMode = TravelMode.DRIVING;
                                CommuteTimeSetting a4 = this.this$Anon0.m;
                                if (a4 != null) {
                                    boolean avoidTolls = a4.getAvoidTolls();
                                    double latitude = this.$location.getLatitude();
                                    double longitude = this.$location.getLongitude();
                                    this.L$Anon0 = zg4;
                                    this.label = 1;
                                    obj = h.a(address, travelMode, avoidTolls, latitude, longitude, this);
                                    if (obj == a) {
                                        return a;
                                    }
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
            return qa4.a;
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        long longValue = ((Number) obj).longValue();
        if (longValue != -1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a5 = CommuteTimeService.A.a();
            local.d(a5, "getDurationTime duration " + longValue);
            this.this$Anon0.l = longValue;
            if (this.this$Anon0.l < ((long) 60)) {
                this.this$Anon0.l = 60;
            }
            this.this$Anon0.k();
        } else {
            this.this$Anon0.a();
        }
        return qa4.a;
    }
}
