package com.portfolio.platform.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.service.MFDeviceService$mButtonConnectivity$1$onServiceConnected$1", mo27670f = "MFDeviceService.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class MFDeviceService$mButtonConnectivity$1$onServiceConnected$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.os.IBinder $service;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21434p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFDeviceService$mButtonConnectivity$1$onServiceConnected$1(android.os.IBinder iBinder, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$service = iBinder;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.service.MFDeviceService$mButtonConnectivity$1$onServiceConnected$1 mFDeviceService$mButtonConnectivity$1$onServiceConnected$1 = new com.portfolio.platform.service.MFDeviceService$mButtonConnectivity$1$onServiceConnected$1(this.$service, yb4);
        mFDeviceService$mButtonConnectivity$1$onServiceConnected$1.f21434p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return mFDeviceService$mButtonConnectivity$1$onServiceConnected$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.service.MFDeviceService$mButtonConnectivity$1$onServiceConnected$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.PortfolioApp.C5609a aVar = com.portfolio.platform.PortfolioApp.f20941W;
            com.misfit.frameworks.buttonservice.IButtonConnectivity asInterface = com.misfit.frameworks.buttonservice.IButtonConnectivity.Stub.asInterface(this.$service);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) asInterface, "IButtonConnectivity.Stub.asInterface(service)");
            aVar.mo34586b(asInterface);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
