package com.portfolio.platform.service.fcm;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FossilFirebaseInstanceIDService extends FirebaseInstanceIdService {
    @DexIgnore
    public en2 j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void a() {
        FirebaseInstanceId m = FirebaseInstanceId.m();
        kd4.a((Object) m, "FirebaseInstanceId.getInstance()");
        String c = m.c();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FossilFirebaseInstanceIDService", "Refreshed token: " + c);
        en2 en2 = this.j;
        if (en2 != null) {
            en2.r(c);
            a(c);
            return;
        }
        kd4.d("mSharedPrefs");
        throw null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        FLogger.INSTANCE.getLocal().d("FossilFirebaseInstanceIDService", "onCreate()");
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final fi4 a(String str) {
        throw null;
        // return ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new FossilFirebaseInstanceIDService$sendRegistrationToServer$Anon1((yb4) null), 3, (Object) null);
    }
}
