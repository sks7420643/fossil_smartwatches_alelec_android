package com.portfolio.platform.workers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AlarmHelper;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TimeChangeReceiver extends BroadcastReceiver {
    @DexIgnore
    public AlarmHelper a;
    @DexIgnore
    public en2 b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public TimeChangeReceiver() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final AlarmHelper a() {
        AlarmHelper alarmHelper = this.a;
        if (alarmHelper != null) {
            return alarmHelper;
        }
        kd4.d("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public final en2 b() {
        en2 en2 = this.b;
        if (en2 != null) {
            return en2;
        }
        kd4.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        throw null;
        // FLogger.INSTANCE.getLocal().d("TimeChangeReceiver", "onReceive()");
        // if (intent != null) {
        //     String action = intent.getAction();
        //     if (action == null) {
        //         return;
        //     }
        //     if (kd4.a((Object) action, (Object) "android.intent.action.TIME_SET") || kd4.a((Object) action, (Object) "android.intent.action.TIMEZONE_CHANGED") || kd4.a((Object) action, (Object) "android.intent.action.TIME_TICK")) {
        //         fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new TimeChangeReceiver$onReceive$$inlined$let$lambda$Anon1((yb4) null, this), 3, (Object) null);
        //     }
        // }
    }
}
