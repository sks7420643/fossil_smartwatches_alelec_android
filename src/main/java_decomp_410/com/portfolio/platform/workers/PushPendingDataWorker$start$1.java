package com.portfolio.platform.workers;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.workers.PushPendingDataWorker", mo27670f = "PushPendingDataWorker.kt", mo27671l = {263, 271, 279, 191, 225, 230, 233, 287}, mo27672m = "start")
public final class PushPendingDataWorker$start$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.workers.PushPendingDataWorker this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PushPendingDataWorker$start$1(com.portfolio.platform.workers.PushPendingDataWorker pushPendingDataWorker, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = pushPendingDataWorker;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.mo42933b((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this);
    }
}
