package com.portfolio.platform.workers;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TimeChangeReceiver$onReceive$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ TimeChangeReceiver this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TimeChangeReceiver$onReceive$$inlined$let$lambda$Anon1(yb4 yb4, TimeChangeReceiver timeChangeReceiver) {
        super(2, yb4);
        this.this$Anon0 = timeChangeReceiver;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        TimeChangeReceiver$onReceive$$inlined$let$lambda$Anon1 timeChangeReceiver$onReceive$$inlined$let$lambda$Anon1 = new TimeChangeReceiver$onReceive$$inlined$let$lambda$Anon1(yb4, this.this$Anon0);
        timeChangeReceiver$onReceive$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return timeChangeReceiver$onReceive$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((TimeChangeReceiver$onReceive$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            String e = PortfolioApp.W.c().e();
            if (e.length() == 0) {
                return qa4.a;
            }
            long g = this.this$Anon0.b().g(e);
            long e2 = this.this$Anon0.b().e(e);
            long currentTimeMillis = System.currentTimeMillis();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("TimeChangeReceiver", "currentTime = " + currentTimeMillis + "; lastSyncTimeSuccess = " + g + "; " + "lastReminderSyncTimeSuccess = " + e2);
            if (e2 > currentTimeMillis || g > currentTimeMillis) {
                en2 b = this.this$Anon0.b();
                long j = currentTimeMillis - ((long) CommuteTimeService.y);
                b.c(j, e);
                this.this$Anon0.b().a(j, e);
                return qa4.a;
            }
            this.this$Anon0.a().e();
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
