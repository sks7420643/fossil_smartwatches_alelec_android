package com.portfolio.platform.workers;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dg4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import java.util.Date;
import java.util.List;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ dg4 $cancellableContinuation;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PushPendingDataWorker this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements ActivitiesRepository.PushPendingActivitiesCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0164Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Ref$ObjectRef $endDate;
            @DexIgnore
            public /* final */ /* synthetic */ Ref$ObjectRef $startDate;
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0164Anon1(Anon1 anon1, Ref$ObjectRef ref$ObjectRef, Ref$ObjectRef ref$ObjectRef2, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
                this.$startDate = ref$ObjectRef;
                this.$endDate = ref$ObjectRef2;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0164Anon1 anon1 = new C0164Anon1(this.this$Anon0, this.$startDate, this.$endDate, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0164Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 zg4 = this.p$;
                    SummariesRepository g = this.this$Anon0.a.this$Anon0.l;
                    Date date = ((DateTime) this.$startDate.element).toLocalDateTime().toDate();
                    kd4.a((Object) date, "startDate.toLocalDateTime().toDate()");
                    Date date2 = ((DateTime) this.$endDate.element).toLocalDateTime().toDate();
                    kd4.a((Object) date2, "endDate.toLocalDateTime().toDate()");
                    this.L$Anon0 = zg4;
                    this.label = 1;
                    if (g.loadSummaries(date, date2, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return qa4.a;
            }
        }

        @DexIgnore
        public Anon1(PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1 pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1) {
            this.a = pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        public void onFail(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PushPendingDataWorker", "ActivitiesRepository.pushPendingActivities onFail, go to next, errorCode = " + i);
            if (this.a.$cancellableContinuation.isActive()) {
                dg4 dg4 = this.a.$cancellableContinuation;
                Result.a aVar = Result.Companion;
                dg4.resumeWith(Result.m3constructorimpl(false));
            }
        }

        @DexIgnore
        public void onSuccess(List<ActivitySample> list) {
            kd4.b(list, "activityList");
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "ActivitiesRepository.pushPendingActivities onSuccess, go to next");
            if (!list.isEmpty()) {
                Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                ref$ObjectRef.element = list.get(0).getStartTime();
                Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
                ref$ObjectRef2.element = list.get(0).getEndTime();
                for (ActivitySample next : list) {
                    if (next.getStartTime().getMillis() < ((DateTime) ref$ObjectRef.element).getMillis()) {
                        ref$ObjectRef.element = next.getStartTime();
                    }
                    if (next.getEndTime().getMillis() < ((DateTime) ref$ObjectRef2.element).getMillis()) {
                        ref$ObjectRef2.element = next.getEndTime();
                    }
                }
                fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new C0164Anon1(this, ref$ObjectRef, ref$ObjectRef2, (yb4) null), 3, (Object) null);
            }
            if (this.a.$cancellableContinuation.isActive()) {
                dg4 dg4 = this.a.$cancellableContinuation;
                Result.a aVar = Result.Companion;
                dg4.resumeWith(Result.m3constructorimpl(true));
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1(dg4 dg4, yb4 yb4, PushPendingDataWorker pushPendingDataWorker) {
        super(2, yb4);
        this.$cancellableContinuation = dg4;
        this.this$Anon0 = pushPendingDataWorker;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1 pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1 = new PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1(this.$cancellableContinuation, yb4, this.this$Anon0);
        pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1.p$ = (zg4) obj;
        return pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ActivitiesRepository a2 = this.this$Anon0.k;
            Anon1 anon1 = new Anon1(this);
            this.L$Anon0 = zg4;
            this.label = 1;
            if (a2.pushPendingActivities(anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
