package com.portfolio.platform.workers;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$1 */
public final class C7083xbddd89ac extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.dg4 $cancellableContinuation;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f25720p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.workers.PushPendingDataWorker this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$1$1")
    /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$1$1 */
    public static final class C70841 implements com.portfolio.platform.data.source.ActivitiesRepository.PushPendingActivitiesCallback {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.workers.C7083xbddd89ac f25721a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$1$1$1")
        /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$1$1$1 */
        public static final class C70851 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $endDate;
            @DexIgnore
            public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $startDate;
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f25722p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.workers.C7083xbddd89ac.C70841 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C70851(com.portfolio.platform.workers.C7083xbddd89ac.C70841 r1, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$startDate = ref$ObjectRef;
                this.$endDate = ref$ObjectRef2;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.workers.C7083xbddd89ac.C70841.C70851 r0 = new com.portfolio.platform.workers.C7083xbddd89ac.C70841.C70851(this.this$0, this.$startDate, this.$endDate, yb4);
                r0.f25722p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.workers.C7083xbddd89ac.C70841.C70851) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f25722p$;
                    com.portfolio.platform.data.source.SummariesRepository g = this.this$0.f25721a.this$0.f25692l;
                    java.util.Date date = ((org.joda.time.DateTime) this.$startDate.element).toLocalDateTime().toDate();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date, "startDate.toLocalDateTime().toDate()");
                    java.util.Date date2 = ((org.joda.time.DateTime) this.$endDate.element).toLocalDateTime().toDate();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date2, "endDate.toLocalDateTime().toDate()");
                    this.L$0 = zg4;
                    this.label = 1;
                    if (g.loadSummaries(date, date2, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        public C70841(com.portfolio.platform.workers.C7083xbddd89ac pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$1) {
            this.f25721a = pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$1;
        }

        @DexIgnore
        public void onFail(int i) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("PushPendingDataWorker", "ActivitiesRepository.pushPendingActivities onFail, go to next, errorCode = " + i);
            if (this.f25721a.$cancellableContinuation.isActive()) {
                com.fossil.blesdk.obfuscated.dg4 dg4 = this.f25721a.$cancellableContinuation;
                kotlin.Result.C7350a aVar = kotlin.Result.Companion;
                dg4.resumeWith(kotlin.Result.m37419constructorimpl(false));
            }
        }

        @DexIgnore
        public void onSuccess(java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample> list) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(list, "activityList");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("PushPendingDataWorker", "ActivitiesRepository.pushPendingActivities onSuccess, go to next");
            if (!list.isEmpty()) {
                kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef = new kotlin.jvm.internal.Ref$ObjectRef();
                ref$ObjectRef.element = list.get(0).getStartTime();
                kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2 = new kotlin.jvm.internal.Ref$ObjectRef();
                ref$ObjectRef2.element = list.get(0).getEndTime();
                for (com.portfolio.platform.data.model.room.fitness.ActivitySample next : list) {
                    if (next.getStartTime().getMillis() < ((org.joda.time.DateTime) ref$ObjectRef.element).getMillis()) {
                        ref$ObjectRef.element = next.getStartTime();
                    }
                    if (next.getEndTime().getMillis() < ((org.joda.time.DateTime) ref$ObjectRef2.element).getMillis()) {
                        ref$ObjectRef2.element = next.getEndTime();
                    }
                }
                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.workers.C7083xbddd89ac.C70841.C70851(this, ref$ObjectRef, ref$ObjectRef2, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            }
            if (this.f25721a.$cancellableContinuation.isActive()) {
                com.fossil.blesdk.obfuscated.dg4 dg4 = this.f25721a.$cancellableContinuation;
                kotlin.Result.C7350a aVar = kotlin.Result.Companion;
                dg4.resumeWith(kotlin.Result.m37419constructorimpl(true));
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C7083xbddd89ac(com.fossil.blesdk.obfuscated.dg4 dg4, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.workers.PushPendingDataWorker pushPendingDataWorker) {
        super(2, yb4);
        this.$cancellableContinuation = dg4;
        this.this$0 = pushPendingDataWorker;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.workers.C7083xbddd89ac pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$1 = new com.portfolio.platform.workers.C7083xbddd89ac(this.$cancellableContinuation, yb4, this.this$0);
        pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$1.f25720p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.workers.C7083xbddd89ac) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f25720p$;
            com.portfolio.platform.data.source.ActivitiesRepository a2 = this.this$0.f25691k;
            com.portfolio.platform.workers.C7083xbddd89ac.C70841 r3 = new com.portfolio.platform.workers.C7083xbddd89ac.C70841(this);
            this.L$0 = zg4;
            this.label = 1;
            if (a2.pushPendingActivities(r3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
