package com.portfolio.platform.workers;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TimeChangeReceiver$onReceive$$inlined$let$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f25734p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.workers.TimeChangeReceiver this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TimeChangeReceiver$onReceive$$inlined$let$lambda$1(com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.workers.TimeChangeReceiver timeChangeReceiver) {
        super(2, yb4);
        this.this$0 = timeChangeReceiver;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.workers.TimeChangeReceiver$onReceive$$inlined$let$lambda$1 timeChangeReceiver$onReceive$$inlined$let$lambda$1 = new com.portfolio.platform.workers.TimeChangeReceiver$onReceive$$inlined$let$lambda$1(yb4, this.this$0);
        timeChangeReceiver$onReceive$$inlined$let$lambda$1.f25734p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return timeChangeReceiver$onReceive$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.workers.TimeChangeReceiver$onReceive$$inlined$let$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.lang.String e = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e();
            if (e.length() == 0) {
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            long g = this.this$0.mo42936b().mo27039g(e);
            long e2 = this.this$0.mo42936b().mo27034e(e);
            long currentTimeMillis = java.lang.System.currentTimeMillis();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("TimeChangeReceiver", "currentTime = " + currentTimeMillis + "; lastSyncTimeSuccess = " + g + "; " + "lastReminderSyncTimeSuccess = " + e2);
            if (e2 > currentTimeMillis || g > currentTimeMillis) {
                com.fossil.blesdk.obfuscated.en2 b = this.this$0.mo42936b();
                long j = currentTimeMillis - ((long) com.portfolio.platform.service.microapp.CommuteTimeService.f21491y);
                b.mo27023c(j, e);
                this.this$0.mo42936b().mo26993a(j, e);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            this.this$0.mo42935a().mo39486e();
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
