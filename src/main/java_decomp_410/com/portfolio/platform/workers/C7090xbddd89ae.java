package com.portfolio.platform.workers;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$3 */
public final class C7090xbddd89ae extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.dg4 $cancellableContinuation;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f25727p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.workers.PushPendingDataWorker this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$3$1")
    /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$3$1 */
    public static final class C70911 implements com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.workers.C7090xbddd89ae f25728a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$3$1$1")
        /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$3$1$1 */
        public static final class C70921 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ java.util.List $goalTrackingList;
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public java.lang.Object L$1;
            @DexIgnore
            public java.lang.Object L$2;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f25729p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.workers.C7090xbddd89ae.C70911 this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$3$1$1$1")
            /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$3$1$1$1 */
            public static final class C70931 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qo2<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary>>>, java.lang.Object> {
                @DexIgnore
                public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $endDate;
                @DexIgnore
                public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $startDate;
                @DexIgnore
                public java.lang.Object L$0;
                @DexIgnore
                public int label;

                @DexIgnore
                /* renamed from: p$ */
                public com.fossil.blesdk.obfuscated.zg4 f25730p$;
                @DexIgnore
                public /* final */ /* synthetic */ com.portfolio.platform.workers.C7090xbddd89ae.C70911.C70921 this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C70931(com.portfolio.platform.workers.C7090xbddd89ae.C70911.C70921 r1, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2, com.fossil.blesdk.obfuscated.yb4 yb4) {
                    super(2, yb4);
                    this.this$0 = r1;
                    this.$startDate = ref$ObjectRef;
                    this.$endDate = ref$ObjectRef2;
                }

                @DexIgnore
                public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                    com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                    com.portfolio.platform.workers.C7090xbddd89ae.C70911.C70921.C70931 r0 = new com.portfolio.platform.workers.C7090xbddd89ae.C70911.C70921.C70931(this.this$0, this.$startDate, this.$endDate, yb4);
                    r0.f25730p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                    return r0;
                }

                @DexIgnore
                public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                    return ((com.portfolio.platform.workers.C7090xbddd89ae.C70911.C70921.C70931) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
                }

                @DexIgnore
                public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                    java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                    int i = this.label;
                    if (i == 0) {
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        this.L$0 = this.f25730p$;
                        this.label = 1;
                        obj = this.this$0.this$0.f25728a.this$0.f25695o.loadSummaries((java.util.Date) this.$startDate.element, (java.util.Date) this.$endDate.element, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        com.fossil.blesdk.obfuscated.zg4 zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    } else {
                        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C70921(com.portfolio.platform.workers.C7090xbddd89ae.C70911 r1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$goalTrackingList = list;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.workers.C7090xbddd89ae.C70911.C70921 r0 = new com.portfolio.platform.workers.C7090xbddd89ae.C70911.C70921(this.this$0, this.$goalTrackingList, yb4);
                r0.f25729p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.workers.C7090xbddd89ae.C70911.C70921) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f25729p$;
                    kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef = new kotlin.jvm.internal.Ref$ObjectRef();
                    ref$ObjectRef.element = ((com.portfolio.platform.data.model.goaltracking.GoalTrackingData) this.$goalTrackingList.get(0)).getDate();
                    kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2 = new kotlin.jvm.internal.Ref$ObjectRef();
                    ref$ObjectRef2.element = ((com.portfolio.platform.data.model.goaltracking.GoalTrackingData) this.$goalTrackingList.get(0)).getDate();
                    for (com.portfolio.platform.data.model.goaltracking.GoalTrackingData goalTrackingData : this.$goalTrackingList) {
                        if (goalTrackingData.getDate().getTime() < ((java.util.Date) ref$ObjectRef.element).getTime()) {
                            ref$ObjectRef.element = goalTrackingData.getDate();
                        }
                        if (goalTrackingData.getDate().getTime() > ((java.util.Date) ref$ObjectRef2.element).getTime()) {
                            ref$ObjectRef2.element = goalTrackingData.getDate();
                        }
                    }
                    com.fossil.blesdk.obfuscated.ug4 b = com.fossil.blesdk.obfuscated.nh4.m25692b();
                    com.portfolio.platform.workers.C7090xbddd89ae.C70911.C70921.C70931 r5 = new com.portfolio.platform.workers.C7090xbddd89ae.C70911.C70921.C70931(this, ref$ObjectRef, ref$ObjectRef2, (com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg4;
                    this.L$1 = ref$ObjectRef;
                    this.L$2 = ref$ObjectRef2;
                    this.label = 1;
                    if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r5, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef3 = (kotlin.jvm.internal.Ref$ObjectRef) this.L$2;
                    kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef4 = (kotlin.jvm.internal.Ref$ObjectRef) this.L$1;
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (this.this$0.f25728a.$cancellableContinuation.isActive()) {
                    com.fossil.blesdk.obfuscated.dg4 dg4 = this.this$0.f25728a.$cancellableContinuation;
                    java.lang.Boolean a2 = com.fossil.blesdk.obfuscated.dc4.m20839a(true);
                    kotlin.Result.C7350a aVar = kotlin.Result.Companion;
                    dg4.resumeWith(kotlin.Result.m37419constructorimpl(a2));
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        public C70911(com.portfolio.platform.workers.C7090xbddd89ae pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$3) {
            this.f25728a = pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$3;
        }

        @DexIgnore
        public void onFail(int i) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("PushPendingDataWorker", "GoalTrackingRepository.pushPendingGoalTrackingDataList onFail, go to next, errorCode = " + i);
            if (this.f25728a.$cancellableContinuation.isActive()) {
                com.fossil.blesdk.obfuscated.dg4 dg4 = this.f25728a.$cancellableContinuation;
                kotlin.Result.C7350a aVar = kotlin.Result.Companion;
                dg4.resumeWith(kotlin.Result.m37419constructorimpl(false));
            }
        }

        @DexIgnore
        public void onSuccess(java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> list) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(list, "goalTrackingList");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("PushPendingDataWorker", "GoalTrackingRepository.pushPendingGoalTrackingDataList onSuccess, go to next");
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25691a()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.workers.C7090xbddd89ae.C70911.C70921(this, list, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C7090xbddd89ae(com.fossil.blesdk.obfuscated.dg4 dg4, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.workers.PushPendingDataWorker pushPendingDataWorker) {
        super(2, yb4);
        this.$cancellableContinuation = dg4;
        this.this$0 = pushPendingDataWorker;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.workers.C7090xbddd89ae pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$3 = new com.portfolio.platform.workers.C7090xbddd89ae(this.$cancellableContinuation, yb4, this.this$0);
        pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$3.f25727p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return pushPendingDataWorker$start$$inlined$suspendCancellableCoroutine$lambda$3;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.workers.C7090xbddd89ae) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f25727p$;
            com.portfolio.platform.data.source.GoalTrackingRepository b = this.this$0.f25695o;
            com.portfolio.platform.workers.C7090xbddd89ae.C70911 r3 = new com.portfolio.platform.workers.C7090xbddd89ae.C70911(this);
            this.L$0 = zg4;
            this.label = 1;
            if (b.pushPendingGoalTrackingDataList(r3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
