package com.portfolio.platform.workers;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.workers.PushPendingDataWorker", f = "PushPendingDataWorker.kt", l = {263, 271, 279, 191, 225, 230, 233, 287}, m = "start")
public final class PushPendingDataWorker$start$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ PushPendingDataWorker this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PushPendingDataWorker$start$Anon1(PushPendingDataWorker pushPendingDataWorker, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = pushPendingDataWorker;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.b((yb4<? super qa4>) this);
    }
}
