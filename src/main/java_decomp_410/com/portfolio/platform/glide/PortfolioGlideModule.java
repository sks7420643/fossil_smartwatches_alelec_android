package com.portfolio.platform.glide;

import android.content.Context;
import com.bumptech.glide.Registry;
import com.fossil.blesdk.obfuscated.ak2;
import com.fossil.blesdk.obfuscated.bk2;
import com.fossil.blesdk.obfuscated.gk2;
import com.fossil.blesdk.obfuscated.hk2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rn;
import com.fossil.blesdk.obfuscated.yj2;
import com.fossil.blesdk.obfuscated.zj2;
import com.fossil.blesdk.obfuscated.zu;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioGlideModule extends zu {
    @DexIgnore
    public void a(Context context, rn rnVar, Registry registry) {
        kd4.b(context, "context");
        kd4.b(rnVar, "glide");
        kd4.b(registry, "registry");
        super.a(context, rnVar, registry);
        registry.a(zj2.class, InputStream.class, new yj2.b());
        registry.a(bk2.class, InputStream.class, new ak2.b());
        registry.a(hk2.class, InputStream.class, new gk2.b());
    }

    @DexIgnore
    public boolean a() {
        return false;
    }
}
