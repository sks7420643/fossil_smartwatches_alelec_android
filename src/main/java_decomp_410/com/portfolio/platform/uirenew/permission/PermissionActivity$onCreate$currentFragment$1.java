package com.portfolio.platform.uirenew.permission;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PermissionActivity$onCreate$currentFragment$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<java.lang.Integer, java.lang.Boolean> {
    @DexIgnore
    public static /* final */ com.portfolio.platform.uirenew.permission.PermissionActivity$onCreate$currentFragment$1 INSTANCE; // = new com.portfolio.platform.uirenew.permission.PermissionActivity$onCreate$currentFragment$1();

    @DexIgnore
    public PermissionActivity$onCreate$currentFragment$1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke((java.lang.Integer) obj));
    }

    @DexIgnore
    public final boolean invoke(java.lang.Integer num) {
        return num == null || num.intValue() != 10;
    }
}
