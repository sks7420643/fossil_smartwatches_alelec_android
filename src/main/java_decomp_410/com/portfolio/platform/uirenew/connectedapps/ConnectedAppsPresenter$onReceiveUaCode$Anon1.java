package com.portfolio.platform.uirenew.connectedapps;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.gr3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$Anon1", f = "ConnectedAppsPresenter.kt", l = {120}, m = "invokeSuspend")
public final class ConnectedAppsPresenter$onReceiveUaCode$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $code;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ConnectedAppsPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$Anon1$Anon1", f = "ConnectedAppsPresenter.kt", l = {121}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ConnectedAppsPresenter$onReceiveUaCode$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$Anon1$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0131Anon1 implements gr3.b {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 a;

            @DexIgnore
            public C0131Anon1(Anon1 anon1) {
                this.a = anon1;
            }

            @DexIgnore
            public void a() {
                fi4 unused = ag4.b(this.a.this$Anon0.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new ConnectedAppsPresenter$onReceiveUaCode$Anon1$Anon1$Anon1$onLoginSuccess$Anon1(this, (yb4) null), 3, (Object) null);
            }

            @DexIgnore
            public void a(String str) {
                FLogger.INSTANCE.getLocal().d(ConnectedAppsPresenter.m.a(), str);
                this.a.this$Anon0.this$Anon0.h.i();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ConnectedAppsPresenter$onReceiveUaCode$Anon1 connectedAppsPresenter$onReceiveUaCode$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = connectedAppsPresenter$onReceiveUaCode$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                gr3 r = this.this$Anon0.this$Anon0.j.r();
                String str = this.this$Anon0.$code;
                C0131Anon1 anon1 = new C0131Anon1(this);
                this.L$Anon0 = zg4;
                this.label = 1;
                if (r.a(str, (gr3.b) anon1, (yb4<? super qa4>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return qa4.a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ConnectedAppsPresenter$onReceiveUaCode$Anon1(ConnectedAppsPresenter connectedAppsPresenter, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = connectedAppsPresenter;
        this.$code = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ConnectedAppsPresenter$onReceiveUaCode$Anon1 connectedAppsPresenter$onReceiveUaCode$Anon1 = new ConnectedAppsPresenter$onReceiveUaCode$Anon1(this.this$Anon0, this.$code, yb4);
        connectedAppsPresenter$onReceiveUaCode$Anon1.p$ = (zg4) obj;
        return connectedAppsPresenter$onReceiveUaCode$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ConnectedAppsPresenter$onReceiveUaCode$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 a2 = this.this$Anon0.b();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            if (yf4.a(a2, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
