package com.portfolio.platform.uirenew.connectedapps;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1", mo27670f = "ConnectedAppsPresenter.kt", mo27671l = {46}, mo27672m = "invokeSuspend")
public final class ConnectedAppsPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22250p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1$1", mo27670f = "ConnectedAppsPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1$1 */
    public static final class C62811 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$BooleanRef $isUAAuthenticated;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22251p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C62811(com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1 connectedAppsPresenter$start$1, kotlin.jvm.internal.Ref$BooleanRef ref$BooleanRef, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = connectedAppsPresenter$start$1;
            this.$isUAAuthenticated = ref$BooleanRef;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1.C62811 r0 = new com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1.C62811(this.this$0, this.$isUAAuthenticated, yb4);
            r0.f22251p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1.C62811) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter connectedAppsPresenter = this.this$0.this$0;
                connectedAppsPresenter.f22231f = connectedAppsPresenter.f22234i.getCurrentUser();
                this.$isUAAuthenticated.element = this.this$0.this$0.f22235j.mo34570r().mo27797b();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ConnectedAppsPresenter$start$1(com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter connectedAppsPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = connectedAppsPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1 connectedAppsPresenter$start$1 = new com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1(this.this$0, yb4);
        connectedAppsPresenter$start$1.f22250p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return connectedAppsPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        kotlin.jvm.internal.Ref$BooleanRef ref$BooleanRef;
        boolean z;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22250p$;
            ref$BooleanRef = new kotlin.jvm.internal.Ref$BooleanRef();
            ref$BooleanRef.element = false;
            boolean e = this.this$0.f22236k.mo33047e();
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1.C62811 r5 = new com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1.C62811(this, ref$BooleanRef, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = ref$BooleanRef;
            this.Z$0 = e;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r5, this) == a) {
                return a;
            }
            z = e;
        } else if (i == 1) {
            z = this.Z$0;
            ref$BooleanRef = (kotlin.jvm.internal.Ref$BooleanRef) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a2 = com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter.f22230m.mo40722a();
        local.mo33255d(a2, "starts isUAConnected=" + ref$BooleanRef.element + " isGFConnected=" + this.this$0.f22236k.mo33047e());
        this.this$0.f22236k.mo33043a((com.fossil.blesdk.obfuscated.zk2.C5524d) this.this$0);
        this.this$0.f22233h.mo28711x(ref$BooleanRef.element);
        this.this$0.f22233h.mo28712y(z);
        if (!z && this.this$0.f22236k.mo33048f() && !this.this$0.f22232g) {
            this.this$0.f22236k.mo33051i();
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
