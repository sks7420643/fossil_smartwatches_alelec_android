package com.portfolio.platform.uirenew.connectedapps;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ku2;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.lg3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ConnectedAppsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a D; // = new a((fd4) null);
    @DexIgnore
    public lg3 B;
    @DexIgnore
    public ConnectedAppsPresenter C;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            kd4.b(context, "context");
            Intent intent = new Intent(context, ConnectedAppsActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        lg3 lg3 = this.B;
        if (lg3 != null) {
            lg3.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        this.B = (lg3) getSupportFragmentManager().a((int) R.id.content);
        if (this.B == null) {
            this.B = lg3.n.b();
            lg3 lg3 = this.B;
            if (lg3 != null) {
                a((Fragment) lg3, lg3.n.a(), (int) R.id.content);
            } else {
                kd4.a();
                throw null;
            }
        }
        l42 g = PortfolioApp.W.c().g();
        lg3 lg32 = this.B;
        if (lg32 != null) {
            g.a(new ku2(lg32)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.connectedapps.ConnectedAppsContract.View");
    }
}
