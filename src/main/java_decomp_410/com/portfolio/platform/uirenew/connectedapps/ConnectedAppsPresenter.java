package com.portfolio.platform.uirenew.connectedapps;

import android.app.Activity;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.he0;
import com.fossil.blesdk.obfuscated.iu2;
import com.fossil.blesdk.obfuscated.ju2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ke0;
import com.fossil.blesdk.obfuscated.lg3;
import com.fossil.blesdk.obfuscated.ne0;
import com.fossil.blesdk.obfuscated.ud0;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.zk2;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.enums.Integration;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ConnectedAppsPresenter extends iu2 implements zk2.d {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a((fd4) null);
    @DexIgnore
    public MFUser f;
    @DexIgnore
    public boolean g; // = true;
    @DexIgnore
    public /* final */ ju2 h;
    @DexIgnore
    public /* final */ UserRepository i;
    @DexIgnore
    public /* final */ PortfolioApp j;
    @DexIgnore
    public /* final */ zk2 k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ConnectedAppsPresenter.l;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ke0<Status> {
        @DexIgnore
        public /* final */ /* synthetic */ ConnectedAppsPresenter c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ConnectedAppsPresenter connectedAppsPresenter, Activity activity, int i) {
            super(activity, i);
            this.c = connectedAppsPresenter;
        }

        @DexIgnore
        public void b(Status status) {
            kd4.b(status, "status");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = ConnectedAppsPresenter.m.a();
            local.d(a, "onGFLogout.onUnresolvableFailure(): isSuccess = " + status.L() + ", statusMessage = " + status.J() + ", statusCode = " + status.I());
            if (status.I() == 17 || status.I() == 5010) {
                this.c.k();
                return;
            }
            this.c.h.y(true);
            this.c.h.o(status.I());
        }

        @DexIgnore
        /* renamed from: c */
        public void a(Status status) {
            kd4.b(status, "status");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = ConnectedAppsPresenter.m.a();
            local.d(a, "onGFLogout.onSuccess(): isSuccess = " + status.L() + ", statusMessage = " + status.J());
            if (status.L()) {
                this.c.k();
                return;
            }
            this.c.h.y(true);
            this.c.h.o(status.I());
        }
    }

    /*
    static {
        String simpleName = ConnectedAppsPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "ConnectedAppsPresenter::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public ConnectedAppsPresenter(ju2 ju2, UserRepository userRepository, PortfolioApp portfolioApp, zk2 zk2) {
        kd4.b(ju2, "mView");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(portfolioApp, "mApp");
        kd4.b(zk2, "mGoogleFitHelper");
        this.h = ju2;
        this.i = userRepository;
        this.j = portfolioApp;
        this.k = zk2;
    }

    @DexIgnore
    public void i() {
        this.k.a();
    }

    @DexIgnore
    public void j() {
        if (this.j.r().b()) {
            FLogger.INSTANCE.getLocal().d(l, "setUpUnderAmourRecord - Logging out of UA");
            fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ConnectedAppsPresenter$onUASelected$Anon1(this, (yb4) null), 3, (Object) null);
            return;
        }
        fi4 unused2 = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ConnectedAppsPresenter$onUASelected$Anon2(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void k() {
        this.k.b();
        String name = Integration.GoogleFit.getName();
        kd4.a((Object) name, "Integration.GoogleFit.getName()");
        c(name);
        this.h.y(false);
    }

    @DexIgnore
    public void l() {
        this.h.a(this);
    }

    @DexIgnore
    public final void b(String str) {
        kd4.b(str, "connectedAppName");
        MFUser mFUser = this.f;
        if (mFUser != null) {
            List<String> integrations = mFUser.getIntegrations();
            kd4.a((Object) integrations, "mUser!!.integrations");
            if (!integrations.contains(str)) {
                integrations.add(str);
            }
            MFUser mFUser2 = this.f;
            if (mFUser2 != null) {
                mFUser2.setIntegrations(integrations);
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void c(String str) {
        kd4.b(str, "connectedAppName");
        MFUser mFUser = this.f;
        if (mFUser != null) {
            List<String> integrations = mFUser.getIntegrations();
            kd4.a((Object) integrations, "mUser!!.integrations");
            integrations.remove(str);
            MFUser mFUser2 = this.f;
            if (mFUser2 != null) {
                mFUser2.setIntegrations(integrations);
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void f() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ConnectedAppsPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        Log.d(l, "stop");
        MFUser mFUser = this.f;
        if (mFUser != null) {
            fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ConnectedAppsPresenter$stop$$inlined$let$lambda$Anon1(mFUser, (yb4) null, this), 3, (Object) null);
        }
        this.k.a((zk2.d) null);
    }

    @DexIgnore
    public void h() {
        if (!PortfolioApp.W.c().w()) {
            this.h.o(601);
            this.h.y(this.k.e());
        } else if (this.k.e()) {
            this.k.g();
        } else if (this.k.d()) {
            this.k.a();
        } else {
            zk2 zk2 = this.k;
            ju2 ju2 = this.h;
            if (ju2 != null) {
                zk2.a((Fragment) (lg3) ju2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.ConnectedAppsFragment");
        }
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, "code");
        this.h.k();
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ConnectedAppsPresenter$onReceiveUaCode$Anon1(this, str, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a() {
        FLogger.INSTANCE.getLocal().d(l, "onGFConnectionSuccess");
        String name = Integration.GoogleFit.getName();
        kd4.a((Object) name, "Integration.GoogleFit.getName()");
        b(name);
        this.h.y(true);
    }

    @DexIgnore
    public void a(ud0 ud0) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "onGFConnectionFailed - Cause: " + ud0);
        if (ud0 == null) {
            return;
        }
        if (ud0.K()) {
            this.g = false;
            this.h.a(ud0);
            return;
        }
        this.h.o(ud0.H());
    }

    @DexIgnore
    public void a(he0<Status> he0) {
        if (he0 != null) {
            ju2 ju2 = this.h;
            if (ju2 != null) {
                FragmentActivity activity = ((lg3) ju2).getActivity();
                if (activity != null) {
                    he0.a((ne0<? super Status>) new b(this, activity, 3));
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.ConnectedAppsFragment");
            }
        }
    }
}
