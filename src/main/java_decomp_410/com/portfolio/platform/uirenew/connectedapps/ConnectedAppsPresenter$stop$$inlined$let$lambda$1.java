package com.portfolio.platform.uirenew.connectedapps;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ConnectedAppsPresenter$stop$$inlined$let$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.MFUser $it;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22238p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$stop$$inlined$let$lambda$1$1")
    /* renamed from: com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$stop$$inlined$let$lambda$1$1 */
    public static final class C62751 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qo2<com.portfolio.platform.data.model.MFUser>>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22239p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$stop$$inlined$let$lambda$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C62751(com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$stop$$inlined$let$lambda$1 connectedAppsPresenter$stop$$inlined$let$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = connectedAppsPresenter$stop$$inlined$let$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$stop$$inlined$let$lambda$1.C62751 r0 = new com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$stop$$inlined$let$lambda$1.C62751(this.this$0, yb4);
            r0.f22239p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$stop$$inlined$let$lambda$1.C62751) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22239p$;
                com.portfolio.platform.data.source.UserRepository e = this.this$0.this$0.f22234i;
                com.portfolio.platform.data.model.MFUser mFUser = this.this$0.$it;
                this.L$0 = zg4;
                this.label = 1;
                obj = e.updateUser(mFUser, true, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ConnectedAppsPresenter$stop$$inlined$let$lambda$1(com.portfolio.platform.data.model.MFUser mFUser, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter connectedAppsPresenter) {
        super(2, yb4);
        this.$it = mFUser;
        this.this$0 = connectedAppsPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$stop$$inlined$let$lambda$1 connectedAppsPresenter$stop$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$stop$$inlined$let$lambda$1(this.$it, yb4, this.this$0);
        connectedAppsPresenter$stop$$inlined$let$lambda$1.f22238p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return connectedAppsPresenter$stop$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$stop$$inlined$let$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22238p$;
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$stop$$inlined$let$lambda$1.C62751 r3 = new com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$stop$$inlined$let$lambda$1.C62751(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
