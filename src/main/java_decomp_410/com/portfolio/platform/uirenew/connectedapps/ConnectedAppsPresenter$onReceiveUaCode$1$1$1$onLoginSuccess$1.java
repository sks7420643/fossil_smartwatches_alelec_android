package com.portfolio.platform.uirenew.connectedapps;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1", mo27670f = "ConnectedAppsPresenter.kt", mo27671l = {125}, mo27672m = "invokeSuspend")
public final class ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22243p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1.C62761.C62771 this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1$1", mo27670f = "ConnectedAppsPresenter.kt", mo27671l = {125}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1$1 */
    public static final class C62781 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22244p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C62781(com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1 connectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = connectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1.C62781 r0 = new com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1.C62781(this.this$0, yb4);
            r0.f22244p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1.C62781) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22244p$;
                com.fossil.blesdk.obfuscated.gr3 r = this.this$0.this$0.f22242a.this$0.this$0.f22235j.mo34570r();
                this.L$0 = zg4;
                this.label = 1;
                if (r.mo27796b("fossil_q", this) == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1(com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1.C62761.C62771 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = r1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1 connectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1 = new com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1(this.this$0, yb4);
        connectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1.f22243p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return connectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22243p$;
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter.f22230m.mo40722a(), "updateUnderAmourRecord - Login completed!");
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.f22242a.this$0.this$0.mo31440b();
            com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1.C62781 r3 = new com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onReceiveUaCode$1$1$1$onLoginSuccess$1.C62781(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter connectedAppsPresenter = this.this$0.f22242a.this$0.this$0;
        java.lang.String name = com.portfolio.platform.enums.Integration.UnderArmour.getName();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) name, "Integration.UnderArmour.getName()");
        connectedAppsPresenter.mo40718b(name);
        this.this$0.f22242a.this$0.this$0.f22233h.mo28711x(true);
        this.this$0.f22242a.this$0.this$0.f22233h.mo28708i();
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
