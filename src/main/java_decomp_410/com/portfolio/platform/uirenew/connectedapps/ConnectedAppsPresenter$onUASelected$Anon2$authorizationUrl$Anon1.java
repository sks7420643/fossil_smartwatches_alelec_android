package com.portfolio.platform.uirenew.connectedapps;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.gr3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.underamour.UAWebViewActivity;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1", f = "ConnectedAppsPresenter.kt", l = {110}, m = "invokeSuspend")
public final class ConnectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super String>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    public ConnectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1(yb4 yb4) {
        super(2, yb4);
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ConnectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1 connectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1 = new ConnectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1(yb4);
        connectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1.p$ = (zg4) obj;
        return connectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ConnectedAppsPresenter$onUASelected$Anon2$authorizationUrl$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            gr3 r = PortfolioApp.W.c().r();
            String a2 = UAWebViewActivity.H.a();
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = r.a(a2, (yb4<? super String>) this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
