package com.portfolio.platform.uirenew.connectedapps;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.blesdk.obfuscated.zk2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$BooleanRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$Anon1", f = "ConnectedAppsPresenter.kt", l = {46}, m = "invokeSuspend")
public final class ConnectedAppsPresenter$start$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ConnectedAppsPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$Anon1$Anon1", f = "ConnectedAppsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ref$BooleanRef $isUAAuthenticated;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ConnectedAppsPresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ConnectedAppsPresenter$start$Anon1 connectedAppsPresenter$start$Anon1, Ref$BooleanRef ref$BooleanRef, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = connectedAppsPresenter$start$Anon1;
            this.$isUAAuthenticated = ref$BooleanRef;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$isUAAuthenticated, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                ConnectedAppsPresenter connectedAppsPresenter = this.this$Anon0.this$Anon0;
                connectedAppsPresenter.f = connectedAppsPresenter.i.getCurrentUser();
                this.$isUAAuthenticated.element = this.this$Anon0.this$Anon0.j.r().b();
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ConnectedAppsPresenter$start$Anon1(ConnectedAppsPresenter connectedAppsPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = connectedAppsPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ConnectedAppsPresenter$start$Anon1 connectedAppsPresenter$start$Anon1 = new ConnectedAppsPresenter$start$Anon1(this.this$Anon0, yb4);
        connectedAppsPresenter$start$Anon1.p$ = (zg4) obj;
        return connectedAppsPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ConnectedAppsPresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Ref$BooleanRef ref$BooleanRef;
        boolean z;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ref$BooleanRef = new Ref$BooleanRef();
            ref$BooleanRef.element = false;
            boolean e = this.this$Anon0.k.e();
            ug4 b = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(this, ref$BooleanRef, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = ref$BooleanRef;
            this.Z$Anon0 = e;
            this.label = 1;
            if (yf4.a(b, anon1, this) == a) {
                return a;
            }
            z = e;
        } else if (i == 1) {
            z = this.Z$Anon0;
            ref$BooleanRef = (Ref$BooleanRef) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = ConnectedAppsPresenter.m.a();
        local.d(a2, "starts isUAConnected=" + ref$BooleanRef.element + " isGFConnected=" + this.this$Anon0.k.e());
        this.this$Anon0.k.a((zk2.d) this.this$Anon0);
        this.this$Anon0.h.x(ref$BooleanRef.element);
        this.this$Anon0.h.y(z);
        if (!z && this.this$Anon0.k.f() && !this.this$Anon0.g) {
            this.this$Anon0.k.i();
        }
        return qa4.a;
    }
}
