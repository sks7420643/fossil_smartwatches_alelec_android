package com.portfolio.platform.uirenew.connectedapps;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onUASelected$2$authorizationUrl$1", mo27670f = "ConnectedAppsPresenter.kt", mo27671l = {110}, mo27672m = "invokeSuspend")
public final class ConnectedAppsPresenter$onUASelected$2$authorizationUrl$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.String>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22249p$;

    @DexIgnore
    public ConnectedAppsPresenter$onUASelected$2$authorizationUrl$1(com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onUASelected$2$authorizationUrl$1 connectedAppsPresenter$onUASelected$2$authorizationUrl$1 = new com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onUASelected$2$authorizationUrl$1(yb4);
        connectedAppsPresenter$onUASelected$2$authorizationUrl$1.f22249p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return connectedAppsPresenter$onUASelected$2$authorizationUrl$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$onUASelected$2$authorizationUrl$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22249p$;
            com.fossil.blesdk.obfuscated.gr3 r = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34570r();
            java.lang.String a2 = com.portfolio.platform.underamour.UAWebViewActivity.f24349H.mo41859a();
            this.L$0 = zg4;
            this.label = 1;
            obj = r.mo27794a(a2, (com.fossil.blesdk.obfuscated.yb4<? super java.lang.String>) this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
