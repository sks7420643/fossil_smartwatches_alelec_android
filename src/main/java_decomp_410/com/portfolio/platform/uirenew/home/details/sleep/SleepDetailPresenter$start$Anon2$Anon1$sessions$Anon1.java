package com.portfolio.platform.uirenew.home.details.sleep;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$Anon2;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$Anon2$Anon1$sessions$Anon1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class SleepDetailPresenter$start$Anon2$Anon1$sessions$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<MFSleepSession>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepDetailPresenter$start$Anon2.Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepDetailPresenter$start$Anon2$Anon1$sessions$Anon1(SleepDetailPresenter$start$Anon2.Anon1 anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SleepDetailPresenter$start$Anon2$Anon1$sessions$Anon1 sleepDetailPresenter$start$Anon2$Anon1$sessions$Anon1 = new SleepDetailPresenter$start$Anon2$Anon1$sessions$Anon1(this.this$Anon0, yb4);
        sleepDetailPresenter$start$Anon2$Anon1$sessions$Anon1.p$ = (zg4) obj;
        return sleepDetailPresenter$start$Anon2$Anon1$sessions$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepDetailPresenter$start$Anon2$Anon1$sessions$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            SleepDetailPresenter sleepDetailPresenter = this.this$Anon0.this$Anon0.a;
            return sleepDetailPresenter.a(sleepDetailPresenter.g, (List<MFSleepSession>) this.this$Anon0.this$Anon0.a.m);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
