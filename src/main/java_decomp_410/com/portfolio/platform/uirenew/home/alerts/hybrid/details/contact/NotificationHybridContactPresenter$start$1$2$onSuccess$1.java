package com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1", mo27670f = "NotificationHybridContactPresenter.kt", mo27671l = {100}, mo27672m = "invokeSuspend")
public final class NotificationHybridContactPresenter$start$1$2$onSuccess$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.t03.C5127d $successResponse;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22570p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1.C63842 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationHybridContactPresenter$start$1$2$onSuccess$1(com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1.C63842 r1, com.fossil.blesdk.obfuscated.t03.C5127d dVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = r1;
        this.$successResponse = dVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1 notificationHybridContactPresenter$start$1$2$onSuccess$1 = new com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1(this.this$0, this.$successResponse, yb4);
        notificationHybridContactPresenter$start$1$2$onSuccess$1.f22570p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationHybridContactPresenter$start$1$2$onSuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22570p$;
            com.fossil.blesdk.obfuscated.gh4 a2 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg4, this.this$0.f22569a.this$0.mo31440b(), (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.C6385x7dc29959(this, (com.fossil.blesdk.obfuscated.yb4) null), 2, (java.lang.Object) null);
            this.L$0 = zg4;
            this.L$1 = a2;
            this.label = 1;
            if (a2.mo27693a(this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.gh4 gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (!this.this$0.f22569a.this$0.f22563i.isEmpty()) {
            for (com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper add : this.this$0.f22569a.this$0.f22563i) {
                this.this$0.f22569a.this$0.mo40924j().add(add);
            }
        }
        this.this$0.f22569a.this$0.f22561g.mo28971a(this.this$0.f22569a.this$0.mo40924j(), com.fossil.blesdk.obfuscated.fs3.f14915a.mo27491a(), this.this$0.f22569a.this$0.f22562h);
        this.this$0.f22569a.this$0.f22564j.mo2302a(0, new android.os.Bundle(), this.this$0.f22569a.this$0);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
