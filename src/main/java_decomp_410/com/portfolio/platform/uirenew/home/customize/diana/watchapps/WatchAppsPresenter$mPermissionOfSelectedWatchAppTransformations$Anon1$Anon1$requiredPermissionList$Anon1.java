package com.portfolio.platform.uirenew.home.customize.diana.watchapps;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.pl2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
public final class WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<? extends String>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1.Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1(WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1.Anon1 anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1 watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1 = new WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1(this.this$Anon0, yb4);
        watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1.p$ = (zg4) obj;
        return watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$Anon1$Anon1$requiredPermissionList$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return pl2.d.b(this.this$Anon0.$it.getWatchappId());
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
