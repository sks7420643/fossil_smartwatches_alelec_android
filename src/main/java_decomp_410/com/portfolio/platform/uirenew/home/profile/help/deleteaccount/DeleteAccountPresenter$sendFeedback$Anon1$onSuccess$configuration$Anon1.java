package com.portfolio.platform.uirenew.home.profile.help.deleteaccount;

import com.fossil.blesdk.obfuscated.kr2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeleteAccountPresenter$sendFeedback$Anon1$onSuccess$configuration$Anon1 extends BaseZendeskFeedbackConfiguration {
    @DexIgnore
    public /* final */ /* synthetic */ kr2.d $responseValue;

    @DexIgnore
    public DeleteAccountPresenter$sendFeedback$Anon1$onSuccess$configuration$Anon1(kr2.d dVar) {
        this.$responseValue = dVar;
    }

    @DexIgnore
    public String getAdditionalInfo() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a = DeleteAccountPresenter.l.a();
        local.d(a, "Inside. getAdditionalInfo: \n" + this.$responseValue.a());
        return this.$responseValue.a();
    }

    @DexIgnore
    public String getRequestSubject() {
        FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.l.a(), "getRequestSubject");
        return this.$responseValue.d();
    }

    @DexIgnore
    public List<String> getTags() {
        FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.l.a(), "getTags");
        return this.$responseValue.e();
    }
}
