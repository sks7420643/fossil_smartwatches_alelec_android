package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateOverviewDayPresenter$start$1<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.diana.heartrate.HeartRateSample>>> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter f23410a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1", mo27670f = "HeartRateOverviewDayPresenter.kt", mo27671l = {50, 58, 65}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1 */
    public static final class C66411 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $data;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public long J$0;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public java.lang.Object L$1;
        @DexIgnore
        public java.lang.Object L$2;
        @DexIgnore
        public java.lang.Object L$3;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23411p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$1", mo27670f = "HeartRateOverviewDayPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$1 */
        public static final class C66421 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f23412p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411 this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$1$a")
            /* renamed from: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$1$a */
            public static final class C6643a<T> implements java.util.Comparator<T> {
                @DexIgnore
                public final int compare(T t, T t2) {
                    return com.fossil.blesdk.obfuscated.wb4.m29575a(java.lang.Long.valueOf(((com.portfolio.platform.data.model.diana.heartrate.HeartRateSample) t).getStartTimeId().getMillis()), java.lang.Long.valueOf(((com.portfolio.platform.data.model.diana.heartrate.HeartRateSample) t2).getStartTimeId().getMillis()));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C66421(com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411.C66421 r0 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411.C66421(this.this$0, yb4);
                r0.f23412p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411.C66421) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                com.fossil.blesdk.obfuscated.cc4.m20546a();
                if (this.label == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    java.util.List list = this.this$0.$data;
                    if (list == null) {
                        return null;
                    }
                    if (list.size() > 1) {
                        com.fossil.blesdk.obfuscated.gb4.m22644a(list, new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411.C66421.C6643a());
                    }
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$2")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$2", mo27670f = "HeartRateOverviewDayPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$2 */
        public static final class C66442 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ java.util.List $listTimeZoneChange;
            @DexIgnore
            public /* final */ /* synthetic */ java.util.List $listTodayHeartRateModel;
            @DexIgnore
            public /* final */ /* synthetic */ int $maxHR;
            @DexIgnore
            public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $previousTimeZoneOffset;
            @DexIgnore
            public /* final */ /* synthetic */ long $startOfDay;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f23413p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C66442(com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411 r1, long j, kotlin.jvm.internal.Ref$IntRef ref$IntRef, java.util.List list, java.util.List list2, int i, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$startOfDay = j;
                this.$previousTimeZoneOffset = ref$IntRef;
                this.$listTimeZoneChange = list;
                this.$listTodayHeartRateModel = list2;
                this.$maxHR = i;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411.C66442 r1 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411.C66442(this.this$0, this.$startOfDay, this.$previousTimeZoneOffset, this.$listTimeZoneChange, this.$listTodayHeartRateModel, this.$maxHR, yb4);
                r1.f23413p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r1;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411.C66442) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.util.Iterator it;
                char c;
                java.lang.StringBuilder sb;
                com.fossil.blesdk.obfuscated.cc4.m20546a();
                if (this.label == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    java.util.List list = this.this$0.$data;
                    if (list == null) {
                        return null;
                    }
                    for (java.util.Iterator it2 = list.iterator(); it2.hasNext(); it2 = it) {
                        com.portfolio.platform.data.model.diana.heartrate.HeartRateSample heartRateSample = (com.portfolio.platform.data.model.diana.heartrate.HeartRateSample) it2.next();
                        long millis = (heartRateSample.getStartTimeId().getMillis() - this.$startOfDay) / 60000;
                        long millis2 = (heartRateSample.getEndTime().getMillis() - this.$startOfDay) / 60000;
                        long j = (millis2 + millis) / ((long) 2);
                        if (heartRateSample.getTimezoneOffsetInSecond() != this.$previousTimeZoneOffset.element) {
                            int hourOfDay = heartRateSample.getStartTimeId().getHourOfDay();
                            java.lang.String a = com.fossil.blesdk.obfuscated.ml2.m25355a(hourOfDay);
                            com.fossil.blesdk.obfuscated.pd4 pd4 = com.fossil.blesdk.obfuscated.pd4.f17594a;
                            java.lang.Object[] objArr = {com.fossil.blesdk.obfuscated.dc4.m20843a(java.lang.Math.abs(heartRateSample.getTimezoneOffsetInSecond() / 3600)), com.fossil.blesdk.obfuscated.dc4.m20843a(java.lang.Math.abs((heartRateSample.getTimezoneOffsetInSecond() / 60) % 60))};
                            java.lang.String format = java.lang.String.format("%02d:%02d", java.util.Arrays.copyOf(objArr, objArr.length));
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) format, "java.lang.String.format(format, *args)");
                            java.util.List list2 = this.$listTimeZoneChange;
                            java.lang.Integer a2 = com.fossil.blesdk.obfuscated.dc4.m20843a((int) j);
                            it = it2;
                            kotlin.Pair pair = new kotlin.Pair(com.fossil.blesdk.obfuscated.dc4.m20843a(hourOfDay), com.fossil.blesdk.obfuscated.dc4.m20842a(((float) heartRateSample.getTimezoneOffsetInSecond()) / 3600.0f));
                            java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
                            sb2.append(a);
                            if (heartRateSample.getTimezoneOffsetInSecond() >= 0) {
                                sb = new java.lang.StringBuilder();
                                c = '+';
                            } else {
                                sb = new java.lang.StringBuilder();
                                c = '-';
                            }
                            sb.append(c);
                            sb.append(format);
                            sb2.append(sb.toString());
                            list2.add(new kotlin.Triple(a2, pair, sb2.toString()));
                            this.$previousTimeZoneOffset.element = heartRateSample.getTimezoneOffsetInSecond();
                        } else {
                            it = it2;
                        }
                        if (!this.$listTodayHeartRateModel.isEmpty()) {
                            com.fossil.blesdk.obfuscated.rt3 rt3 = (com.fossil.blesdk.obfuscated.rt3) com.fossil.blesdk.obfuscated.kb4.m24385f(this.$listTodayHeartRateModel);
                            if (millis - ((long) rt3.mo30751a()) > 1) {
                                com.fossil.blesdk.obfuscated.rt3 rt32 = new com.fossil.blesdk.obfuscated.rt3(0, 0, 0, rt3.mo30751a(), (int) millis, (int) j);
                                this.$listTodayHeartRateModel.add(rt32);
                            }
                        }
                        int average = (int) heartRateSample.getAverage();
                        if (heartRateSample.getMax() == this.$maxHR) {
                            average = heartRateSample.getMax();
                        }
                        com.fossil.blesdk.obfuscated.rt3 rt33 = new com.fossil.blesdk.obfuscated.rt3(average, heartRateSample.getMin(), heartRateSample.getMax(), (int) millis, (int) millis2, (int) j);
                        this.$listTodayHeartRateModel.add(rt33);
                    }
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C66411(com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1 heartRateOverviewDayPresenter$start$1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = heartRateOverviewDayPresenter$start$1;
            this.$data = list;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411 r0 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411(this.this$0, this.$data, yb4);
            r0.f23411p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x012c A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x012d  */
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.util.List list;
            java.util.List list2;
            int i;
            int i2;
            long j;
            java.util.List list3;
            com.fossil.blesdk.obfuscated.zg4 zg4;
            java.lang.Object obj2;
            com.fossil.blesdk.obfuscated.ug4 a;
            com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411.C66442 r19;
            java.lang.Object obj3;
            com.fossil.blesdk.obfuscated.zg4 zg42;
            java.util.List list4;
            long j2;
            java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i3 = this.label;
            if (i3 == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg43 = this.f23411p$;
                java.util.ArrayList arrayList = new java.util.ArrayList();
                com.fossil.blesdk.obfuscated.ug4 a3 = this.this$0.f23410a.mo31440b();
                com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411.C66421 r5 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411.C66421(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg43;
                this.L$1 = arrayList;
                this.label = 1;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(a3, r5, this) == a2) {
                    return a2;
                }
                java.util.ArrayList arrayList2 = arrayList;
                zg42 = zg43;
                list4 = arrayList2;
            } else if (i3 == 1) {
                list4 = (java.util.List) this.L$1;
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else if (i3 == 2) {
                long j3 = this.J$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                j = j3;
                list3 = (java.util.List) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                obj2 = obj;
                int intValue = ((java.lang.Number) obj2).intValue();
                kotlin.jvm.internal.Ref$IntRef ref$IntRef = new kotlin.jvm.internal.Ref$IntRef();
                ref$IntRef.element = -1;
                java.util.ArrayList arrayList3 = new java.util.ArrayList();
                a = this.this$0.f23410a.mo31440b();
                java.util.ArrayList arrayList4 = arrayList3;
                int i4 = intValue;
                r19 = r0;
                java.util.List list5 = list3;
                com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411.C66442 r0 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411.C66442(this, j, ref$IntRef, arrayList4, list3, i4, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = list5;
                this.J$0 = j;
                this.I$0 = i4;
                this.L$2 = ref$IntRef;
                list2 = arrayList4;
                this.L$3 = list2;
                this.label = 3;
                obj3 = a2;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(a, r19, this) != obj3) {
                    return obj3;
                }
                list = list5;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local.mo33255d("HeartRateOverviewDayPresenter", "listTimeZoneChange=" + com.fossil.blesdk.obfuscated.rj2.m27347a(list2));
                if (!list2.isEmpty()) {
                }
                i = 0;
                list2.clear();
                int i5 = i + org.joda.time.DateTimeConstants.MINUTES_PER_DAY;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local2.mo33255d("HeartRateOverviewDayPresenter", "minutesOfDay=" + i5);
                this.this$0.f23410a.f23406i.mo27367a(i5, list, list2);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else if (i3 == 3) {
                list2 = (java.util.List) this.L$3;
                kotlin.jvm.internal.Ref$IntRef ref$IntRef2 = (kotlin.jvm.internal.Ref$IntRef) this.L$2;
                list = (java.util.List) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg44 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local3.mo33255d("HeartRateOverviewDayPresenter", "listTimeZoneChange=" + com.fossil.blesdk.obfuscated.rj2.m27347a(list2));
                if ((!list2.isEmpty()) || list2.size() <= 1) {
                    i = 0;
                    list2.clear();
                } else {
                    float floatValue = ((java.lang.Number) ((kotlin.Pair) ((kotlin.Triple) list2.get(0)).getSecond()).getSecond()).floatValue();
                    float floatValue2 = ((java.lang.Number) ((kotlin.Pair) ((kotlin.Triple) list2.get(com.fossil.blesdk.obfuscated.cb4.m20535a(list2))).getSecond()).getSecond()).floatValue();
                    float f = (float) 0;
                    if ((floatValue >= f || floatValue2 >= f) && (floatValue < f || floatValue2 < f)) {
                        i2 = java.lang.Math.abs((int) ((floatValue - floatValue2) * ((float) 60)));
                        if (floatValue2 < f) {
                            i2 = -i2;
                        }
                    } else {
                        i2 = (int) ((floatValue - floatValue2) * ((float) 60));
                    }
                    int i6 = i2;
                    java.util.ArrayList<kotlin.Triple> arrayList5 = new java.util.ArrayList<>();
                    for (java.lang.Object next : list2) {
                        if (com.fossil.blesdk.obfuscated.dc4.m20839a(((java.lang.Number) ((kotlin.Pair) ((kotlin.Triple) next).getSecond()).getSecond()).floatValue() == floatValue).booleanValue()) {
                            arrayList5.add(next);
                        }
                    }
                    java.util.ArrayList<kotlin.Pair> arrayList6 = new java.util.ArrayList<>(com.fossil.blesdk.obfuscated.db4.m20831a(arrayList5, 10));
                    for (kotlin.Triple second : arrayList5) {
                        arrayList6.add((kotlin.Pair) second.getSecond());
                    }
                    java.util.ArrayList arrayList7 = new java.util.ArrayList(com.fossil.blesdk.obfuscated.db4.m20831a(arrayList6, 10));
                    for (kotlin.Pair first : arrayList6) {
                        arrayList7.add(com.fossil.blesdk.obfuscated.dc4.m20843a(((java.lang.Number) first.getFirst()).intValue()));
                    }
                    if (!arrayList7.contains(com.fossil.blesdk.obfuscated.dc4.m20843a(0))) {
                        java.lang.String a4 = this.this$0.f23410a.mo41309a(com.fossil.blesdk.obfuscated.dc4.m20842a(floatValue));
                        java.lang.Integer a5 = com.fossil.blesdk.obfuscated.dc4.m20843a(0);
                        kotlin.Pair pair = new kotlin.Pair(com.fossil.blesdk.obfuscated.dc4.m20843a(0), com.fossil.blesdk.obfuscated.dc4.m20842a(floatValue));
                        list2.add(0, new kotlin.Triple(a5, pair, "12a" + a4));
                    }
                    java.util.ArrayList<kotlin.Triple> arrayList8 = new java.util.ArrayList<>();
                    for (java.lang.Object next2 : list2) {
                        if (com.fossil.blesdk.obfuscated.dc4.m20839a(((java.lang.Number) ((kotlin.Pair) ((kotlin.Triple) next2).getSecond()).getSecond()).floatValue() == floatValue2).booleanValue()) {
                            arrayList8.add(next2);
                        }
                    }
                    java.util.ArrayList<kotlin.Pair> arrayList9 = new java.util.ArrayList<>(com.fossil.blesdk.obfuscated.db4.m20831a(arrayList8, 10));
                    for (kotlin.Triple second2 : arrayList8) {
                        arrayList9.add((kotlin.Pair) second2.getSecond());
                    }
                    java.util.ArrayList arrayList10 = new java.util.ArrayList(com.fossil.blesdk.obfuscated.db4.m20831a(arrayList9, 10));
                    for (kotlin.Pair first2 : arrayList9) {
                        arrayList10.add(com.fossil.blesdk.obfuscated.dc4.m20843a(((java.lang.Number) first2.getFirst()).intValue()));
                    }
                    if (!arrayList10.contains(com.fossil.blesdk.obfuscated.dc4.m20843a(24))) {
                        java.lang.String a6 = this.this$0.f23410a.mo41309a(com.fossil.blesdk.obfuscated.dc4.m20842a(floatValue2));
                        java.lang.Integer a7 = com.fossil.blesdk.obfuscated.dc4.m20843a(i6 + org.joda.time.DateTimeConstants.MINUTES_PER_DAY);
                        kotlin.Pair pair2 = new kotlin.Pair(com.fossil.blesdk.obfuscated.dc4.m20843a(24), com.fossil.blesdk.obfuscated.dc4.m20842a(floatValue2));
                        list2.add(new kotlin.Triple(a7, pair2, "12a" + a6));
                    }
                    i = i6;
                }
                int i52 = i + org.joda.time.DateTimeConstants.MINUTES_PER_DAY;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local22 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local22.mo33255d("HeartRateOverviewDayPresenter", "minutesOfDay=" + i52);
                this.this$0.f23410a.f23406i.mo27367a(i52, list, list2);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            java.util.List list6 = this.$data;
            if (list6 == null || !(!list6.isEmpty())) {
                java.util.Date n = com.fossil.blesdk.obfuscated.rk2.m27409n(com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter.m34843b(this.this$0.f23410a));
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) n, "DateHelper.getStartOfDay(mDate)");
                j2 = n.getTime();
            } else {
                org.joda.time.DateTime withTimeAtStartOfDay = ((com.portfolio.platform.data.model.diana.heartrate.HeartRateSample) this.$data.get(0)).getStartTimeId().withTimeAtStartOfDay();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) withTimeAtStartOfDay, "data[0].getStartTimeId().withTimeAtStartOfDay()");
                j2 = withTimeAtStartOfDay.getMillis();
            }
            com.fossil.blesdk.obfuscated.ug4 a8 = this.this$0.f23410a.mo31440b();
            com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$maxHR$1 heartRateOverviewDayPresenter$start$1$1$maxHR$1 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$maxHR$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg42;
            this.L$1 = list4;
            this.J$0 = j2;
            this.label = 2;
            obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(a8, heartRateOverviewDayPresenter$start$1$1$maxHR$1, this);
            if (obj2 == a2) {
                return a2;
            }
            list3 = list4;
            zg4 = zg42;
            j = j2;
            int intValue2 = ((java.lang.Number) obj2).intValue();
            kotlin.jvm.internal.Ref$IntRef ref$IntRef3 = new kotlin.jvm.internal.Ref$IntRef();
            ref$IntRef3.element = -1;
            java.util.ArrayList arrayList32 = new java.util.ArrayList();
            a = this.this$0.f23410a.mo31440b();
            java.util.ArrayList arrayList42 = arrayList32;
            int i42 = intValue2;
            r19 = r0;
            java.util.List list52 = list3;
            com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411.C66442 r02 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411.C66442(this, j, ref$IntRef3, arrayList42, list3, i42, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = list52;
            this.J$0 = j;
            this.I$0 = i42;
            this.L$2 = ref$IntRef3;
            list2 = arrayList42;
            this.L$3 = list2;
            this.label = 3;
            obj3 = a2;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(a, r19, this) != obj3) {
            }
        }
    }

    @DexIgnore
    public HeartRateOverviewDayPresenter$start$1(com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
        this.f23410a = heartRateOverviewDayPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.diana.heartrate.HeartRateSample>> os3) {
        com.portfolio.platform.enums.Status a = os3.mo29972a();
        java.util.List list = (java.util.List) os3.mo29973b();
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("start - mHeartRateSamples -- heartRateSamples=");
        sb.append(list != null ? java.lang.Integer.valueOf(list.size()) : null);
        sb.append(", status=");
        sb.append(a);
        local.mo33255d("HeartRateOverviewDayPresenter", sb.toString());
        if (a != com.portfolio.platform.enums.Status.DATABASE_LOADING) {
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23410a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1.C66411(this, list, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        }
    }
}
