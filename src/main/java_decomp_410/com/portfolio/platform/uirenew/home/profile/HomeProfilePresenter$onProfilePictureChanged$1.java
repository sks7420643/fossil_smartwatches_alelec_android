package com.portfolio.platform.uirenew.home.profile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1", mo27670f = "HomeProfilePresenter.kt", mo27671l = {335, 341}, mo27672m = "invokeSuspend")
public final class HomeProfilePresenter$onProfilePictureChanged$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.net.Uri $imageUri;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23810p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1$1", mo27670f = "HomeProfilePresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1$1 */
    public static final class C67471 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.String>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ android.graphics.Bitmap $bitmap;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23811p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C67471(android.graphics.Bitmap bitmap, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.$bitmap = bitmap;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1.C67471 r0 = new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1.C67471(this.$bitmap, yb4);
            r0.f23811p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1.C67471) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                android.graphics.Bitmap bitmap = this.$bitmap;
                if (bitmap != null) {
                    return com.fossil.blesdk.obfuscated.vr3.m29405a(bitmap);
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$onProfilePictureChanged$1(com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter homeProfilePresenter, android.net.Uri uri, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homeProfilePresenter;
        this.$imageUri = uri;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1 homeProfilePresenter$onProfilePictureChanged$1 = new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1(this.this$0, this.$imageUri, yb4);
        homeProfilePresenter$onProfilePictureChanged$1.f23810p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeProfilePresenter$onProfilePictureChanged$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.data.model.MFUser mFUser;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f23810p$;
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1$bitmap$1 homeProfilePresenter$onProfilePictureChanged$1$bitmap$1 = new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1$bitmap$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b, homeProfilePresenter$onProfilePictureChanged$1$bitmap$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            mFUser = (com.portfolio.platform.data.model.MFUser) this.L$2;
            android.graphics.Bitmap bitmap = (android.graphics.Bitmap) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            try {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                mFUser.setProfilePicture((java.lang.String) obj);
                this.this$0.mo41446b(this.this$0.mo41448k());
            } catch (java.lang.Exception e) {
                e.printStackTrace();
                this.this$0.mo41449l().mo27225o();
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        android.graphics.Bitmap bitmap2 = (android.graphics.Bitmap) obj;
        com.portfolio.platform.data.model.MFUser k = this.this$0.mo41448k();
        if (k != null) {
            com.fossil.blesdk.obfuscated.ug4 b2 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1.C67471 r6 = new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1.C67471(bitmap2, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = bitmap2;
            this.L$2 = k;
            this.label = 2;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b2, r6, this);
            if (obj == a) {
                return a;
            }
            mFUser = k;
            mFUser.setProfilePicture((java.lang.String) obj);
        }
        this.this$0.mo41446b(this.this$0.mo41448k());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
