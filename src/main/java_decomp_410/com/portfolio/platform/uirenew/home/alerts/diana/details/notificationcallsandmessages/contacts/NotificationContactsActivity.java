package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.xw2;
import com.fossil.blesdk.obfuscated.yw2;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationContactsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public NotificationContactsPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment) {
            kd4.b(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationContactsActivity.class);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        xw2 xw2 = (xw2) getSupportFragmentManager().a((int) R.id.content);
        if (xw2 == null) {
            xw2 = xw2.o.b();
            a((Fragment) xw2, xw2.o.a(), (int) R.id.content);
        }
        l42 g = PortfolioApp.W.c().g();
        if (xw2 != null) {
            LoaderManager supportLoaderManager = getSupportLoaderManager();
            kd4.a((Object) supportLoaderManager, "supportLoaderManager");
            g.a(new yw2(xw2, supportLoaderManager)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsContract.View");
    }
}
