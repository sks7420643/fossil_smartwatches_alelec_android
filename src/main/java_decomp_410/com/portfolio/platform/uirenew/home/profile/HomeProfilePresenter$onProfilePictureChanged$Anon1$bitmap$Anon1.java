package com.portfolio.platform.uirenew.home.profile;

import android.content.Context;
import android.graphics.Bitmap;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.ck2;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lv;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nk2;
import com.fossil.blesdk.obfuscated.oo;
import com.fossil.blesdk.obfuscated.pp;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rv;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.PortfolioApp;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
public final class HomeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Bitmap>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeProfilePresenter$onProfilePictureChanged$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1(HomeProfilePresenter$onProfilePictureChanged$Anon1 homeProfilePresenter$onProfilePictureChanged$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = homeProfilePresenter$onProfilePictureChanged$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1 homeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1 = new HomeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1(this.this$Anon0, yb4);
        homeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1.p$ = (zg4) obj;
        return homeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return ck2.a((Context) PortfolioApp.W.c()).e().a(this.this$Anon0.$imageUri).a((lv<?>) ((rv) ((rv) new rv().a(pp.a)).a(true)).a((oo<Bitmap>) new nk2())).c(200, 200).get();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
