package com.portfolio.platform.uirenew.home.profile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4$1$invokeSuspend$$inlined$withLock$lambda$1 */
public final class C6746x2b66fa75 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.String>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.Device $deviceModel;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23809p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4.C67481 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6746x2b66fa75(com.portfolio.platform.data.model.Device device, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4.C67481 r3) {
        super(2, yb4);
        this.$deviceModel = device;
        this.this$0 = r3;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.profile.C6746x2b66fa75 homeProfilePresenter$start$4$1$invokeSuspend$$inlined$withLock$lambda$1 = new com.portfolio.platform.uirenew.home.profile.C6746x2b66fa75(this.$deviceModel, yb4, this.this$0);
        homeProfilePresenter$start$4$1$invokeSuspend$$inlined$withLock$lambda$1.f23809p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeProfilePresenter$start$4$1$invokeSuspend$$inlined$withLock$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.profile.C6746x2b66fa75) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return this.this$0.this$0.f23815a.f23792w.getDeviceNameBySerial(this.$deviceModel.getDeviceId());
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
