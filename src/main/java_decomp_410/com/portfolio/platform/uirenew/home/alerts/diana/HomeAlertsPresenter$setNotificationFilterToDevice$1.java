package com.portfolio.platform.uirenew.home.alerts.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1", mo27670f = "HomeAlertsPresenter.kt", mo27671l = {432, 434}, mo27672m = "invokeSuspend")
public final class HomeAlertsPresenter$setNotificationFilterToDevice$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public long J$0;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22323p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeAlertsPresenter$setNotificationFilterToDevice$1(com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter homeAlertsPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homeAlertsPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1 homeAlertsPresenter$setNotificationFilterToDevice$1 = new com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1(this.this$0, yb4);
        homeAlertsPresenter$setNotificationFilterToDevice$1.f22323p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeAlertsPresenter$setNotificationFilterToDevice$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0106  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        long j;
        java.util.List list;
        java.lang.Object obj2;
        java.util.List list2;
        com.fossil.blesdk.obfuscated.gh4 gh4;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        com.fossil.blesdk.obfuscated.gh4 gh42;
        java.util.List list3;
        java.lang.Object obj3;
        java.util.List list4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f22323p$;
            list3 = new java.util.ArrayList();
            j = java.lang.System.currentTimeMillis();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a();
            local.mo33255d(a2, "filter notification, time start=" + j);
            list3.clear();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a3 = com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a();
            local2.mo33255d(a3, "mListAppWrapper.size = " + this.this$0.mo40764i().size());
            com.fossil.blesdk.obfuscated.gh4 a4 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, this.this$0.mo31440b(), (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.alerts.diana.C6306x5cbb617f(this, (com.fossil.blesdk.obfuscated.yb4) null), 2, (java.lang.Object) null);
            gh42 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, this.this$0.mo31440b(), (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.alerts.diana.C6301xb0ea01af(this, (com.fossil.blesdk.obfuscated.yb4) null), 2, (java.lang.Object) null);
            this.L$0 = zg42;
            this.L$1 = list3;
            this.J$0 = j;
            this.L$2 = a4;
            this.L$3 = gh42;
            this.L$4 = list3;
            this.label = 1;
            obj3 = a4.mo27693a(this);
            if (obj3 == a) {
                return a;
            }
            gh4 = a4;
            zg4 = zg42;
            list4 = list3;
        } else if (i == 1) {
            list4 = (java.util.List) this.L$4;
            j = this.J$0;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
            list3 = (java.util.List) this.L$1;
            gh42 = (com.fossil.blesdk.obfuscated.gh4) this.L$3;
            obj3 = obj;
        } else if (i == 2) {
            com.fossil.blesdk.obfuscated.gh4 gh43 = (com.fossil.blesdk.obfuscated.gh4) this.L$3;
            com.fossil.blesdk.obfuscated.gh4 gh44 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
            long j2 = this.J$0;
            list = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            j = j2;
            obj2 = obj;
            list2 = (java.util.List) obj2;
            if (list2 != null) {
                list.addAll(list2);
                long b = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34515b(new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings(list, java.lang.System.currentTimeMillis()), com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e());
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a5 = com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a();
                local3.mo33255d(a5, "filter notification, time end= " + b);
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a6 = com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter.f22300x.mo40769a();
                local4.mo33255d(a6, "delayTime =" + (b - j) + " milliseconds");
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        list4.addAll((java.util.Collection) obj3);
        this.L$0 = zg4;
        this.L$1 = list3;
        this.J$0 = j;
        this.L$2 = gh4;
        this.L$3 = gh42;
        this.label = 2;
        obj2 = gh42.mo27693a(this);
        if (obj2 == a) {
            return a;
        }
        list = list3;
        list2 = (java.util.List) obj2;
        if (list2 != null) {
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
