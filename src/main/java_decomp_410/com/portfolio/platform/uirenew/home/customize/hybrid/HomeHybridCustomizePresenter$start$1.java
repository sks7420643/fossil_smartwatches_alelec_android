package com.portfolio.platform.uirenew.home.customize.hybrid;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1", mo27670f = "HomeHybridCustomizePresenter.kt", mo27671l = {60}, mo27672m = "invokeSuspend")
public final class HomeHybridCustomizePresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22969p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1$1", mo27670f = "HomeHybridCustomizePresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1$1 */
    public static final class C65221 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<? extends com.portfolio.platform.data.model.room.microapp.MicroApp>>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22970p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C65221(com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1 homeHybridCustomizePresenter$start$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = homeHybridCustomizePresenter$start$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1.C65221 r0 = new com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1.C65221(this.this$0, yb4);
            r0.f22970p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1.C65221) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return this.this$0.this$0.f22953p.getAllMicroApp(this.this$0.this$0.f22951n.mo34532e());
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1$a")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1$a */
    public static final class C6523a<T> implements com.fossil.blesdk.obfuscated.C1548cc<java.lang.String> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1 f22971a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1$a$a")
        /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1$a$a */
        public static final class C6524a<T> implements com.fossil.blesdk.obfuscated.C1548cc<java.util.List<? extends com.portfolio.platform.data.model.room.microapp.HybridPreset>> {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1.C6523a f22972a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1$a$a$a")
            /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1$a$a$a */
            public static final class C6525a<T> implements java.util.Comparator<T> {
                @DexIgnore
                public final int compare(T t, T t2) {
                    return com.fossil.blesdk.obfuscated.wb4.m29575a(java.lang.Boolean.valueOf(((com.portfolio.platform.data.model.room.microapp.HybridPreset) t2).isActive()), java.lang.Boolean.valueOf(((com.portfolio.platform.data.model.room.microapp.HybridPreset) t).isActive()));
                }
            }

            @DexIgnore
            public C6524a(com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1.C6523a aVar) {
                this.f22972a = aVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void mo8689a(java.util.List<com.portfolio.platform.data.model.room.microapp.HybridPreset> list) {
                if (list != null) {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    local.mo33255d("HomeHybridCustomizePresenter", "onObserve on preset list change " + list);
                    java.util.List<T> a = com.fossil.blesdk.obfuscated.kb4.m24368a(list, new com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1.C6523a.C6524a.C6525a());
                    boolean a2 = com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) a, (java.lang.Object) this.f22972a.f22971a.this$0.f22945h) ^ true;
                    int itemCount = this.f22972a.f22971a.this$0.f22952o.getItemCount();
                    if (a2 || a.size() != itemCount - 1) {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        local2.mo33255d("HomeHybridCustomizePresenter", "process change - " + a2 + " - itemCount: " + itemCount + " - sortedPresetSize: " + a.size());
                        if (this.f22972a.f22971a.this$0.f22949l == 1) {
                            this.f22972a.f22971a.this$0.mo41132a((java.util.List<com.portfolio.platform.data.model.room.microapp.HybridPreset>) a);
                        } else {
                            this.f22972a.f22971a.this$0.f22950m = com.fossil.blesdk.obfuscated.oa4.m26076a(true, a);
                        }
                    }
                }
            }
        }

        @DexIgnore
        public C6523a(com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1 homeHybridCustomizePresenter$start$1) {
            this.f22971a = homeHybridCustomizePresenter$start$1;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(java.lang.String str) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("HomeHybridCustomizePresenter", "on active serial change " + str + " mCurrentHomeTab " + this.f22971a.this$0.f22949l);
            if (!android.text.TextUtils.isEmpty(str)) {
                com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.f22971a.this$0;
                com.portfolio.platform.data.source.HybridPresetRepository e = homeHybridCustomizePresenter.f22954q;
                if (str != null) {
                    homeHybridCustomizePresenter.f22943f = e.getPresetListAsLiveData(str);
                    this.f22971a.this$0.f22943f.mo2277a((androidx.lifecycle.LifecycleOwner) this.f22971a.this$0.f22952o, new com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1.C6523a.C6524a(this));
                    this.f22971a.this$0.f22952o.mo33002a(false);
                    return;
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
            this.f22971a.this$0.f22952o.mo33002a(true);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeHybridCustomizePresenter$start$1(com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter homeHybridCustomizePresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homeHybridCustomizePresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1 homeHybridCustomizePresenter$start$1 = new com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1(this.this$0, yb4);
        homeHybridCustomizePresenter$start$1.f22969p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeHybridCustomizePresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0085  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.z53 l;
        java.util.ArrayList arrayList;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22969p$;
            if (this.this$0.f22944g.isEmpty()) {
                java.util.ArrayList a2 = this.this$0.f22944g;
                com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
                com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1.C65221 r4 = new com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1.C65221(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = a2;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b, r4, this);
                if (obj == a) {
                    return a;
                }
                arrayList = a2;
            }
            androidx.lifecycle.MutableLiveData i2 = this.this$0.f22947j;
            l = this.this$0.f22952o;
            if (l == null) {
                i2.mo2277a((com.fossil.blesdk.obfuscated.zu2) l, new com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1.C6523a(this));
                this.this$0.f22955r.mo41124e();
                com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39773a(com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_LINK_MAPPING);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } else if (i == 1) {
            arrayList = (java.util.ArrayList) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        arrayList.addAll((java.util.Collection) obj);
        androidx.lifecycle.MutableLiveData i22 = this.this$0.f22947j;
        l = this.this$0.f22952o;
        if (l == null) {
        }
    }
}
