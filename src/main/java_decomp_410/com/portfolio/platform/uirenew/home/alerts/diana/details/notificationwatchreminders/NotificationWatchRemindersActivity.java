package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.cy2;
import com.fossil.blesdk.obfuscated.ey2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationWatchRemindersActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public NotificationWatchRemindersPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment) {
            kd4.b(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationWatchRemindersActivity.class);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        kd4.a((Object) NotificationWatchRemindersActivity.class.getSimpleName(), "NotificationWatchReminde\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        cy2 cy2 = (cy2) getSupportFragmentManager().a((int) R.id.content);
        if (cy2 == null) {
            cy2 = cy2.r.b();
            a((Fragment) cy2, cy2.r.a(), (int) R.id.content);
        }
        l42 g = PortfolioApp.W.c().g();
        if (cy2 != null) {
            g.a(new ey2(cy2)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersContract.View");
    }
}
