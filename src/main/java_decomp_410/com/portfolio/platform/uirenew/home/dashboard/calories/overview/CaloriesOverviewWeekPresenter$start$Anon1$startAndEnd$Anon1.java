package com.portfolio.platform.uirenew.home.dashboard.calories.overview;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import java.util.Date;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1", f = "CaloriesOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
public final class CaloriesOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Pair<? extends Date, ? extends Date>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CaloriesOverviewWeekPresenter$start$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CaloriesOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1(CaloriesOverviewWeekPresenter$start$Anon1 caloriesOverviewWeekPresenter$start$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = caloriesOverviewWeekPresenter$start$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CaloriesOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1 caloriesOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1 = new CaloriesOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1(this.this$Anon0, yb4);
        caloriesOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1.p$ = (zg4) obj;
        return caloriesOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CaloriesOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter = this.this$Anon0.this$Anon0;
            return caloriesOverviewWeekPresenter.a(CaloriesOverviewWeekPresenter.e(caloriesOverviewWeekPresenter));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
