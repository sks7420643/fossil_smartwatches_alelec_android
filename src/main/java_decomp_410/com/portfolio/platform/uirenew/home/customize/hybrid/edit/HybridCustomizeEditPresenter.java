package com.portfolio.platform.uirenew.home.customize.hybrid.edit;

import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.bn2;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.g13;
import com.fossil.blesdk.obfuscated.h13;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.k63;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l63;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.m63;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridCustomizeEditPresenter extends k63 {
    @DexIgnore
    public HybridCustomizeViewModel f;
    @DexIgnore
    public MutableLiveData<HybridPreset> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<g13> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Gson i; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<g13> j;
    @DexIgnore
    public /* final */ l63 k;
    @DexIgnore
    public /* final */ SetHybridPresetToWatchUseCase l;
    @DexIgnore
    public /* final */ en2 m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<HybridPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditPresenter a;

        @DexIgnore
        public b(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
            this.a = hybridCustomizeEditPresenter;
        }

        @DexIgnore
        public final void a(HybridPreset hybridPreset) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "start - observe current preset value=" + hybridPreset);
            MutableLiveData b = this.a.g;
            if (hybridPreset != null) {
                b.a(hybridPreset.clone());
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements cc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditPresenter a;

        @DexIgnore
        public c(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
            this.a = hybridCustomizeEditPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "start - observe selected microApp value=" + str);
            l63 h = this.a.k;
            if (str != null) {
                h.n(str);
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements cc<g13> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditPresenter a;

        @DexIgnore
        public d(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
            this.a = hybridCustomizeEditPresenter;
        }

        @DexIgnore
        public final void a(g13 g13) {
            if (g13 != null) {
                this.a.k.a(g13);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements cc<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditPresenter a;

        @DexIgnore
        public e(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
            this.a = hybridCustomizeEditPresenter;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "update preset status isChanged=" + bool);
            l63 h = this.a.k;
            if (bool != null) {
                h.f(bool.booleanValue());
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditPresenter a;

        @DexIgnore
        public f(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
            this.a = hybridCustomizeEditPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<g13> apply(HybridPreset hybridPreset) {
            String str;
            String component1 = hybridPreset.component1();
            String component2 = hybridPreset.component2();
            ArrayList<HybridPresetAppSetting> component4 = hybridPreset.component4();
            boolean component5 = hybridPreset.component5();
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(component4);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "wrapperPresetTransformations presetChanged microApps=" + arrayList);
            ArrayList arrayList2 = new ArrayList();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) it.next();
                String component12 = hybridPresetAppSetting.component1();
                MicroApp c = HybridCustomizeEditPresenter.e(this.a).c(hybridPresetAppSetting.component2());
                if (c != null) {
                    String id = c.getId();
                    String icon = c.getIcon();
                    if (icon != null) {
                        str = icon;
                    } else {
                        str = "";
                    }
                    h13 h13 = r9;
                    h13 h132 = new h13(id, str, sm2.a(PortfolioApp.W.c(), c.getNameKey(), c.getName()), component12, (String) null, 16, (fd4) null);
                    arrayList2.add(h13);
                }
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("HybridCustomizeEditPresenter", "wrapperPresetTransformations presetChanged microAppsDetail=" + arrayList2);
            MutableLiveData c2 = this.a.h;
            if (component2 == null) {
                component2 = "";
            }
            c2.a(new g13(component1, component2, arrayList2, component5));
            return this.a.h;
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public HybridCustomizeEditPresenter(l63 l63, SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, en2 en2) {
        kd4.b(l63, "mView");
        kd4.b(setHybridPresetToWatchUseCase, "mSetHybridPresetToWatchUseCase");
        kd4.b(en2, "mSharedPreferencesManager");
        this.k = l63;
        this.l = setHybridPresetToWatchUseCase;
        this.m = en2;
        LiveData<g13> b2 = hc.b(this.g, new f(this));
        kd4.a((Object) b2, "Transformations.switchMa\u2026urrentWrapperPreset\n    }");
        this.j = b2;
    }

    @DexIgnore
    public static final /* synthetic */ HybridCustomizeViewModel e(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
        HybridCustomizeViewModel hybridCustomizeViewModel = hybridCustomizeEditPresenter.f;
        if (hybridCustomizeViewModel != null) {
            return hybridCustomizeViewModel;
        }
        kd4.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void i() {
        HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
        if (hybridCustomizeViewModel != null) {
            HybridPreset a2 = hybridCustomizeViewModel.c().a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "setPresetToWatch currentPreset=" + a2);
            Set<Integer> a3 = bn2.d.a(a2);
            bn2 bn2 = bn2.d;
            l63 l63 = this.k;
            if (l63 == null) {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment");
            } else if (bn2.a(((m63) l63).getContext(), a3) && a2 != null) {
                fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1(a2, (yb4) null, this, a2), 3, (Object) null);
            }
        } else {
            kd4.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void j() {
        this.k.a(this);
    }

    @DexIgnore
    public void a(HybridCustomizeViewModel hybridCustomizeViewModel) {
        kd4.b(hybridCustomizeViewModel, "viewModel");
        this.f = hybridCustomizeViewModel;
    }

    @DexIgnore
    public void b(String str, String str2) {
        T t;
        kd4.b(str, "fromPosition");
        kd4.b(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "swapMicroApp - fromPosition=" + str + ", toPosition=" + str2);
        if (!kd4.a((Object) str, (Object) str2)) {
            HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
            T t2 = null;
            if (hybridCustomizeViewModel != null) {
                HybridPreset a2 = hybridCustomizeViewModel.c().a();
                if (a2 != null) {
                    HybridPreset clone = a2.clone();
                    Iterator<T> it = clone.getButtons().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        if (kd4.a((Object) ((HybridPresetAppSetting) t).getPosition(), (Object) str)) {
                            break;
                        }
                    }
                    HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t;
                    Iterator<T> it2 = clone.getButtons().iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        T next = it2.next();
                        if (kd4.a((Object) ((HybridPresetAppSetting) next).getPosition(), (Object) str2)) {
                            t2 = next;
                            break;
                        }
                    }
                    HybridPresetAppSetting hybridPresetAppSetting2 = (HybridPresetAppSetting) t2;
                    if (hybridPresetAppSetting != null) {
                        hybridPresetAppSetting.setPosition(str2);
                    }
                    if (hybridPresetAppSetting2 != null) {
                        hybridPresetAppSetting2.setPosition(str);
                    }
                    FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "swapMicroApp - update preset");
                    a(clone);
                    return;
                }
                return;
            }
            kd4.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void f() {
        if (FossilDeviceSerialPatternUtil.isSamDevice(PortfolioApp.W.c().e())) {
            this.k.D();
        }
        this.l.e();
        BleCommandResultManager.d.a(CommunicateMode.SET_LINK_MAPPING);
        HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
        if (hybridCustomizeViewModel != null) {
            MutableLiveData<HybridPreset> c2 = hybridCustomizeViewModel.c();
            l63 l63 = this.k;
            if (l63 != null) {
                c2.a((m63) l63, new b(this));
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.f;
                if (hybridCustomizeViewModel2 != null) {
                    hybridCustomizeViewModel2.g().a((LifecycleOwner) this.k, new c(this));
                    this.j.a((LifecycleOwner) this.k, new d(this));
                    HybridCustomizeViewModel hybridCustomizeViewModel3 = this.f;
                    if (hybridCustomizeViewModel3 != null) {
                        hybridCustomizeViewModel3.d().a((LifecycleOwner) this.k, new e(this));
                    } else {
                        kd4.d("mHybridCustomizeViewModel");
                        throw null;
                    }
                } else {
                    kd4.d("mHybridCustomizeViewModel");
                    throw null;
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment");
            }
        } else {
            kd4.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void g() {
        this.l.g();
        HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
        if (hybridCustomizeViewModel != null) {
            MutableLiveData<HybridPreset> c2 = hybridCustomizeViewModel.c();
            l63 l63 = this.k;
            if (l63 != null) {
                c2.a((LifecycleOwner) (m63) l63);
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.f;
                if (hybridCustomizeViewModel2 != null) {
                    hybridCustomizeViewModel2.g().a((LifecycleOwner) this.k);
                    this.h.a((LifecycleOwner) this.k);
                    return;
                }
                kd4.d("mHybridCustomizeViewModel");
                throw null;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment");
        }
        kd4.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void h() {
        HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
        if (hybridCustomizeViewModel != null) {
            boolean i2 = hybridCustomizeViewModel.i();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "onUserExit isPresetChanged " + i2);
            if (i2) {
                this.k.p();
            } else {
                this.k.g(false);
            }
        } else {
            kd4.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, "microAppPos");
        HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
        if (hybridCustomizeViewModel != null) {
            hybridCustomizeViewModel.f(str);
        } else {
            kd4.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        kd4.b(str, "microAppId");
        kd4.b(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HybridCustomizeEditPresenter", "dropMicroApp - microAppid=" + str + ", toPosition=" + str2);
        HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
        T t = null;
        if (hybridCustomizeViewModel == null) {
            kd4.d("mHybridCustomizeViewModel");
            throw null;
        } else if (!hybridCustomizeViewModel.e(str)) {
            HybridCustomizeViewModel hybridCustomizeViewModel2 = this.f;
            if (hybridCustomizeViewModel2 != null) {
                HybridPreset a2 = hybridCustomizeViewModel2.c().a();
                if (a2 != null) {
                    Iterator<T> it = a2.getButtons().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        T next = it.next();
                        if (kd4.a((Object) ((HybridPresetAppSetting) next).getPosition(), (Object) str2)) {
                            t = next;
                            break;
                        }
                    }
                    HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t;
                    if (hybridPresetAppSetting != null) {
                        hybridPresetAppSetting.setAppId(str);
                        hybridPresetAppSetting.setSettings("");
                    }
                    FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "dropMicroApp - update preset");
                    kd4.a((Object) a2, "currentPreset");
                    a(a2);
                    return;
                }
                return;
            }
            kd4.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void a(HybridPreset hybridPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HybridCustomizeEditPresenter", "updateCurrentPreset=" + hybridPreset);
        HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
        if (hybridCustomizeViewModel != null) {
            hybridCustomizeViewModel.a(hybridPreset);
        } else {
            kd4.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        HybridCustomizeViewModel hybridCustomizeViewModel = this.f;
        if (hybridCustomizeViewModel != null) {
            HybridPreset a2 = hybridCustomizeViewModel.c().a();
            if (!(a2 == null || bundle == null)) {
                bundle.putString("KEY_CURRENT_PRESET", this.i.a((Object) a2));
            }
            HybridCustomizeViewModel hybridCustomizeViewModel2 = this.f;
            if (hybridCustomizeViewModel2 != null) {
                HybridPreset e2 = hybridCustomizeViewModel2.e();
                if (!(e2 == null || bundle == null)) {
                    bundle.putString("KEY_ORIGINAL_PRESET", this.i.a((Object) e2));
                }
                HybridCustomizeViewModel hybridCustomizeViewModel3 = this.f;
                if (hybridCustomizeViewModel3 != null) {
                    String a3 = hybridCustomizeViewModel3.g().a();
                    if (!(a3 == null || bundle == null)) {
                        bundle.putString("KEY_PRESET_WATCH_APP_POS_SELECTED", a3);
                    }
                    return bundle;
                }
                kd4.d("mHybridCustomizeViewModel");
                throw null;
            }
            kd4.d("mHybridCustomizeViewModel");
            throw null;
        }
        kd4.d("mHybridCustomizeViewModel");
        throw null;
    }
}
