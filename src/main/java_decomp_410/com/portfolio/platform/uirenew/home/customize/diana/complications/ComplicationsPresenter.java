package com.portfolio.platform.uirenew.home.customize.diana.complications;

import android.content.Context;
import android.os.Parcelable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.h23;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.i23;
import com.fossil.blesdk.obfuscated.j23;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.ns3;
import com.fossil.blesdk.obfuscated.ok2;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fossil.R;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.setting.ComplicationActivitySetting;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationsPresenter extends h23 {
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public DianaCustomizeViewModel f;
    @DexIgnore
    public /* final */ MutableLiveData<Complication> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Complication>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Pair<String, Boolean>>> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Triple<String, Boolean, Parcelable>> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Gson l; // = new Gson();
    @DexIgnore
    public ArrayList<Category> m; // = new ArrayList<>();
    @DexIgnore
    public /* final */ LiveData<String> n;
    @DexIgnore
    public /* final */ cc<String> o;
    @DexIgnore
    public /* final */ LiveData<List<Complication>> p;
    @DexIgnore
    public /* final */ cc<List<Complication>> q;
    @DexIgnore
    public /* final */ LiveData<List<Pair<String, Boolean>>> r;
    @DexIgnore
    public /* final */ cc<List<Pair<String, Boolean>>> s;
    @DexIgnore
    public /* final */ cc<Triple<String, Boolean, Parcelable>> t;
    @DexIgnore
    public /* final */ i23 u;
    @DexIgnore
    public /* final */ CategoryRepository v;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsPresenter a;

        @DexIgnore
        public b(ComplicationsPresenter complicationsPresenter) {
            this.a = complicationsPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String l = ComplicationsPresenter.w;
            local.d(l, "onLiveDataChanged category=" + str);
            if (str != null) {
                this.a.u.e(str);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsPresenter a;

        @DexIgnore
        public c(ComplicationsPresenter complicationsPresenter) {
            this.a = complicationsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(Complication complication) {
            if (complication != null) {
                String str = (String) this.a.h.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String l = ComplicationsPresenter.w;
                local.d(l, "transform from selected complication to category currentCategory=" + str + " compsCategories=" + complication.getCategories());
                ArrayList<String> categories = complication.getCategories();
                if (str == null || !categories.contains(str)) {
                    this.a.h.a(categories.get(0));
                } else {
                    this.a.h.a(str);
                }
            }
            return this.a.h;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements cc<List<? extends Complication>> {
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsPresenter a;

        @DexIgnore
        public d(ComplicationsPresenter complicationsPresenter) {
            this.a = complicationsPresenter;
        }

        @DexIgnore
        public final void a(List<Complication> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String l = ComplicationsPresenter.w;
            StringBuilder sb = new StringBuilder();
            sb.append("onLiveDataChanged complications by category value=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d(l, sb.toString());
            if (list != null) {
                this.a.u.m(list);
                Complication a2 = ComplicationsPresenter.g(this.a).f().a();
                if (a2 != null) {
                    this.a.u.b(a2);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements cc<List<? extends Pair<? extends String, ? extends Boolean>>> {
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsPresenter a;

        @DexIgnore
        public e(ComplicationsPresenter complicationsPresenter) {
            this.a = complicationsPresenter;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x006e, code lost:
            if (r5.equals(com.portfolio.platform.data.InAppPermission.ACCESS_FINE_LOCATION) == false) goto L_0x00bf;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x008d, code lost:
            if (r5.equals(com.portfolio.platform.data.InAppPermission.LOCATION_SERVICE) != false) goto L_0x008f;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00da  */
        public final void a(List<Pair<String, Boolean>> list) {
            T t;
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (!((Boolean) ((Pair) t).getSecond()).booleanValue()) {
                        break;
                    }
                }
                Pair pair = (Pair) t;
                FLogger.INSTANCE.getLocal().d(ComplicationsPresenter.w, "onLiveDataChanged permissionsRequired " + pair + ' ');
                String str = "";
                int i = 0;
                if (pair != null) {
                    String str2 = (String) pair.getFirst();
                    int hashCode = str2.hashCode();
                    if (hashCode != 385352715) {
                        if (hashCode != 564039755) {
                            if (hashCode == 766697727) {
                            }
                        } else if (str2.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                            str = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.background_location_service_general_explain);
                        }
                        i23 l = this.a.u;
                        if (!(list instanceof Collection) || !list.isEmpty()) {
                            for (Pair second : list) {
                                if (((Boolean) second.getSecond()).booleanValue()) {
                                    i++;
                                    if (i < 0) {
                                        cb4.b();
                                        throw null;
                                    }
                                }
                            }
                        }
                        int size = list.size();
                        String a2 = ns3.a.a((String) pair.getFirst());
                        kd4.a((Object) str, "content");
                        l.a(i, size, a2, str);
                        return;
                    }
                    pd4 pd4 = pd4.a;
                    String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.___Text__TurnOnLocationServicesToAllow);
                    kd4.a((Object) a3, "LanguageHelper.getString\u2026nLocationServicesToAllow)");
                    Object[] objArr = {PortfolioApp.W.c().i()};
                    str = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                    kd4.a((Object) str, "java.lang.String.format(format, *args)");
                    i23 l2 = this.a.u;
                    while (r5.hasNext()) {
                    }
                    int size2 = list.size();
                    String a22 = ns3.a.a((String) pair.getFirst());
                    kd4.a((Object) str, "content");
                    l2.a(i, size2, a22, str);
                    return;
                }
                this.a.u.a(0, 0, str, str);
                Complication a4 = ComplicationsPresenter.g(this.a).f().a();
                if (a4 != null) {
                    i23 l3 = this.a.u;
                    String a5 = sm2.a(PortfolioApp.W.c(), a4.getDescriptionKey(), a4.getDescription());
                    kd4.a((Object) a5, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    l3.C(a5);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsPresenter a;

        @DexIgnore
        public f(ComplicationsPresenter complicationsPresenter) {
            this.a = complicationsPresenter;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x00b9, code lost:
            if (r1 <= 0) goto L_0x00bb;
         */
        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<Pair<String, Boolean>>> apply(Complication complication) {
            List<String> b = ok2.c.b(complication.getComplicationId());
            String[] a2 = ns3.a.a();
            ArrayList<Pair> arrayList = new ArrayList<>();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String l = ComplicationsPresenter.w;
            local.d(l, "checkPermissionOf complicationId=" + complication.getComplicationId() + ' ' + "grantedPermission " + a2.length + " requiredPermission " + b.size());
            if (!b.isEmpty()) {
                for (String next : b) {
                    arrayList.add(new Pair(next, Boolean.valueOf(za4.b((T[]) a2, next))));
                }
            }
            this.a.j.a(arrayList);
            if (!arrayList.isEmpty()) {
                int i = 0;
                if (!arrayList.isEmpty()) {
                    for (Pair second : arrayList) {
                        if (!((Boolean) second.getSecond()).booleanValue()) {
                            i++;
                            if (i < 0) {
                                cb4.b();
                                throw null;
                            }
                        }
                    }
                }
            }
            fi4 unused = this.a.c(complication.getComplicationId());
            return this.a.j;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements cc<Triple<? extends String, ? extends Boolean, ? extends Parcelable>> {
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsPresenter a;

        @DexIgnore
        public g(ComplicationsPresenter complicationsPresenter) {
            this.a = complicationsPresenter;
        }

        @DexIgnore
        public final void a(Triple<String, Boolean, ? extends Parcelable> triple) {
            if (triple != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String l = ComplicationsPresenter.w;
                local.d(l, "onLiveDataChanged setting of " + triple.getFirst() + " isSettingRequired " + triple.getSecond().booleanValue() + ' ');
                String str = "";
                if (triple.getSecond().booleanValue()) {
                    if (((Parcelable) triple.getThird()) == null) {
                        str = ok2.c.a(triple.getFirst());
                    }
                    this.a.u.a(true, triple.getFirst(), str, (Parcelable) triple.getThird());
                    return;
                }
                this.a.u.a(false, triple.getFirst(), str, (Parcelable) null);
                Complication a2 = ComplicationsPresenter.g(this.a).f().a();
                if (a2 != null) {
                    i23 l2 = this.a.u;
                    String a3 = sm2.a(PortfolioApp.W.c(), a2.getDescriptionKey(), a2.getDescription());
                    kd4.a((Object) a3, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    l2.C(a3);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements cc<Complication> {
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsPresenter a;

        @DexIgnore
        public h(ComplicationsPresenter complicationsPresenter) {
            this.a = complicationsPresenter;
        }

        @DexIgnore
        public final void a(Complication complication) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String l = ComplicationsPresenter.w;
            local.d(l, "onLiveDataChanged selectedComplication value=" + complication);
            if (complication != null) {
                this.a.g.a(complication);
            }
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = ComplicationsPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "ComplicationsPresenter::class.java.simpleName");
        w = simpleName;
    }
    */

    @DexIgnore
    public ComplicationsPresenter(i23 i23, CategoryRepository categoryRepository) {
        kd4.b(i23, "mView");
        kd4.b(categoryRepository, "mCategoryRepository");
        this.u = i23;
        this.v = categoryRepository;
        LiveData<String> b2 = hc.b(this.g, new c(this));
        kd4.a((Object) b2, "Transformations.switchMa\u2026omplicationLiveData\n    }");
        this.n = b2;
        this.o = new b(this);
        LiveData<List<Complication>> b3 = hc.b(this.n, new ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1(this));
        kd4.a((Object) b3, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.p = b3;
        this.q = new d(this);
        LiveData<List<Pair<String, Boolean>>> b4 = hc.b(this.g, new f(this));
        kd4.a((Object) b4, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.r = b4;
        this.s = new e(this);
        this.t = new g(this);
    }

    @DexIgnore
    public static final /* synthetic */ DianaCustomizeViewModel g(ComplicationsPresenter complicationsPresenter) {
        DianaCustomizeViewModel dianaCustomizeViewModel = complicationsPresenter.f;
        if (dianaCustomizeViewModel != null) {
            return dianaCustomizeViewModel;
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void b(String str) {
        T t2;
        kd4.b(str, "newComplicationId");
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            Complication c2 = dianaCustomizeViewModel.c(str);
            FLogger.INSTANCE.getLocal().d(w, "onUserChooseComplication " + c2);
            if (c2 != null) {
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
                if (dianaCustomizeViewModel2 != null) {
                    DianaPreset a2 = dianaCustomizeViewModel2.c().a();
                    if (a2 != null) {
                        DianaPreset clone = a2.clone();
                        ArrayList arrayList = new ArrayList();
                        DianaCustomizeViewModel dianaCustomizeViewModel3 = this.f;
                        if (dianaCustomizeViewModel3 != null) {
                            String a3 = dianaCustomizeViewModel3.g().a();
                            if (a3 != null) {
                                kd4.a((Object) a3, "mDianaCustomizeViewModel\u2026ComplicationPos().value!!");
                                String str2 = a3;
                                DianaCustomizeViewModel dianaCustomizeViewModel4 = this.f;
                                if (dianaCustomizeViewModel4 == null) {
                                    kd4.d("mDianaCustomizeViewModel");
                                    throw null;
                                } else if (!dianaCustomizeViewModel4.i(str)) {
                                    Iterator<DianaPresetComplicationSetting> it = clone.getComplications().iterator();
                                    while (it.hasNext()) {
                                        DianaPresetComplicationSetting next = it.next();
                                        if (kd4.a((Object) next.getPosition(), (Object) str2)) {
                                            arrayList.add(new DianaPresetComplicationSetting(str2, str, next.getLocalUpdateAt()));
                                        } else {
                                            arrayList.add(next);
                                        }
                                    }
                                    clone.getComplications().clear();
                                    clone.getComplications().addAll(arrayList);
                                    FLogger.INSTANCE.getLocal().d(w, "Update current preset=" + clone);
                                    DianaCustomizeViewModel dianaCustomizeViewModel5 = this.f;
                                    if (dianaCustomizeViewModel5 != null) {
                                        dianaCustomizeViewModel5.a(clone);
                                    } else {
                                        kd4.d("mDianaCustomizeViewModel");
                                        throw null;
                                    }
                                } else {
                                    Iterator<T> it2 = a2.getComplications().iterator();
                                    while (true) {
                                        if (!it2.hasNext()) {
                                            t2 = null;
                                            break;
                                        }
                                        t2 = it2.next();
                                        if (kd4.a((Object) ((DianaPresetComplicationSetting) t2).getId(), (Object) str)) {
                                            break;
                                        }
                                    }
                                    DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t2;
                                    if (dianaPresetComplicationSetting != null) {
                                        DianaCustomizeViewModel dianaCustomizeViewModel6 = this.f;
                                        if (dianaCustomizeViewModel6 != null) {
                                            dianaCustomizeViewModel6.k(dianaPresetComplicationSetting.getPosition());
                                        } else {
                                            kd4.d("mDianaCustomizeViewModel");
                                            throw null;
                                        }
                                    }
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.d("mDianaCustomizeViewModel");
                            throw null;
                        }
                    }
                } else {
                    kd4.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final fi4 c(String str) {
        return ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1(this, str, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(w, "onStart");
        if (this.m.isEmpty()) {
            fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ComplicationsPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
        } else {
            this.u.a(this.m);
        }
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            LiveData<Complication> f2 = dianaCustomizeViewModel.f();
            i23 i23 = this.u;
            if (i23 != null) {
                f2.a((j23) i23, new h(this));
                this.n.a(this.o);
                this.p.a(this.q);
                this.r.a(this.s);
                this.k.a(this.t);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsFragment");
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void g() {
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            LiveData<Complication> f2 = dianaCustomizeViewModel.f();
            i23 i23 = this.u;
            if (i23 != null) {
                f2.a((LifecycleOwner) (j23) i23);
                this.i.b(this.q);
                this.h.b(this.o);
                this.j.b(this.s);
                this.k.b(this.t);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsFragment");
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003e, code lost:
        if (r3 != null) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006d, code lost:
        if (r4 != null) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x009c, code lost:
        if (r5 != null) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00ca, code lost:
        if (r0 != null) goto L_0x00ce;
     */
    @DexIgnore
    public void h() {
        T t2;
        String str;
        T t3;
        String str2;
        T t4;
        String str3;
        String str4;
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        T t5 = null;
        if (dianaCustomizeViewModel != null) {
            DianaPreset a2 = dianaCustomizeViewModel.c().a();
            if (a2 != null) {
                Iterator<T> it = a2.getComplications().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (kd4.a((Object) ((DianaPresetComplicationSetting) t2).getPosition(), (Object) ViewHierarchy.DIMENSION_TOP_KEY)) {
                        break;
                    }
                }
                DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t2;
                if (dianaPresetComplicationSetting != null) {
                    str = dianaPresetComplicationSetting.getId();
                }
                str = "empty";
                Iterator<T> it2 = a2.getComplications().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t3 = null;
                        break;
                    }
                    t3 = it2.next();
                    if (kd4.a((Object) ((DianaPresetComplicationSetting) t3).getPosition(), (Object) "bottom")) {
                        break;
                    }
                }
                DianaPresetComplicationSetting dianaPresetComplicationSetting2 = (DianaPresetComplicationSetting) t3;
                if (dianaPresetComplicationSetting2 != null) {
                    str2 = dianaPresetComplicationSetting2.getId();
                }
                str2 = "empty";
                Iterator<T> it3 = a2.getComplications().iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        t4 = null;
                        break;
                    }
                    t4 = it3.next();
                    if (kd4.a((Object) ((DianaPresetComplicationSetting) t4).getPosition(), (Object) "right")) {
                        break;
                    }
                }
                DianaPresetComplicationSetting dianaPresetComplicationSetting3 = (DianaPresetComplicationSetting) t4;
                if (dianaPresetComplicationSetting3 != null) {
                    str3 = dianaPresetComplicationSetting3.getId();
                }
                str3 = "empty";
                Iterator<T> it4 = a2.getComplications().iterator();
                while (true) {
                    if (!it4.hasNext()) {
                        break;
                    }
                    T next = it4.next();
                    if (kd4.a((Object) ((DianaPresetComplicationSetting) next).getPosition(), (Object) ViewHierarchy.DIMENSION_LEFT_KEY)) {
                        t5 = next;
                        break;
                    }
                }
                DianaPresetComplicationSetting dianaPresetComplicationSetting4 = (DianaPresetComplicationSetting) t5;
                if (dianaPresetComplicationSetting4 != null) {
                    str4 = dianaPresetComplicationSetting4.getId();
                }
                str4 = "empty";
                this.u.a(str, str2, str3, str4);
                return;
            }
            this.u.a("empty", "empty", "empty", "empty");
            return;
        }
        kd4.d("mDianaCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0051, code lost:
        if (r1 != null) goto L_0x0056;
     */
    @DexIgnore
    public void i() {
        String str;
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        T t2 = null;
        if (dianaCustomizeViewModel != null) {
            Complication a2 = dianaCustomizeViewModel.f().a();
            if (a2 != null) {
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
                if (dianaCustomizeViewModel2 != null) {
                    DianaPreset a3 = dianaCustomizeViewModel2.c().a();
                    if (a3 != null) {
                        ArrayList<DianaPresetComplicationSetting> complications = a3.getComplications();
                        if (complications != null) {
                            Iterator<T> it = complications.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                T next = it.next();
                                if (kd4.a((Object) a2.getComplicationId(), (Object) ((DianaPresetComplicationSetting) next).getId())) {
                                    t2 = next;
                                    break;
                                }
                            }
                            DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t2;
                            if (dianaPresetComplicationSetting != null) {
                                str = dianaPresetComplicationSetting.getSettings();
                            }
                        }
                    }
                    str = "";
                    String complicationId = a2.getComplicationId();
                    int hashCode = complicationId.hashCode();
                    if (hashCode != -829740640) {
                        if (hashCode == 134170930 && complicationId.equals("second-timezone")) {
                            this.u.g(str);
                        }
                    } else if (complicationId.equals("commute-time")) {
                        this.u.b(str);
                    }
                } else {
                    kd4.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void j() {
        Object obj;
        List a2 = this.j.a();
        if (a2 != null) {
            Iterator it = a2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (!((Boolean) ((Pair) obj).getSecond()).booleanValue()) {
                    break;
                }
            }
            Pair pair = (Pair) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "processRequiredPermission " + pair);
            if (pair != null) {
                this.u.f((String) pair.getFirst());
            }
        }
    }

    @DexIgnore
    public void k() {
        this.u.a(this);
    }

    @DexIgnore
    public void a(DianaCustomizeViewModel dianaCustomizeViewModel) {
        kd4.b(dianaCustomizeViewModel, "viewModel");
        this.f = dianaCustomizeViewModel;
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, "category");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = w;
        local.d(str2, "category change " + str);
        this.h.a(str);
    }

    @DexIgnore
    public void a(String str, Parcelable parcelable) {
        T t2;
        kd4.b(str, "complicationId");
        kd4.b(parcelable, MicroAppSetting.SETTING);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            DianaPreset a2 = dianaCustomizeViewModel.c().a();
            if (a2 != null) {
                DianaPreset clone = a2.clone();
                Iterator<T> it = clone.getComplications().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (kd4.a((Object) ((DianaPresetComplicationSetting) t2).getId(), (Object) str)) {
                        break;
                    }
                }
                DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t2;
                if (dianaPresetComplicationSetting != null) {
                    dianaPresetComplicationSetting.setSettings(this.l.a((Object) parcelable));
                }
                FLogger.INSTANCE.getLocal().d(w, "update current preset with new setting " + parcelable + " of " + str);
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
                if (dianaCustomizeViewModel2 != null) {
                    dianaCustomizeViewModel2.a(clone);
                } else {
                    kd4.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(boolean z) {
        T t2;
        DianaCustomizeViewModel dianaCustomizeViewModel = this.f;
        if (dianaCustomizeViewModel != null) {
            DianaPreset a2 = dianaCustomizeViewModel.c().a();
            if (a2 != null) {
                DianaPreset clone = a2.clone();
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.f;
                if (dianaCustomizeViewModel2 != null) {
                    Complication a3 = dianaCustomizeViewModel2.f().a();
                    if (a3 != null) {
                        String component1 = a3.component1();
                        Iterator<T> it = clone.getComplications().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t2 = null;
                                break;
                            }
                            t2 = it.next();
                            if (kd4.a((Object) ((DianaPresetComplicationSetting) t2).getId(), (Object) component1)) {
                                break;
                            }
                        }
                        DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t2;
                        if (dianaPresetComplicationSetting != null) {
                            ComplicationActivitySetting complicationActivitySetting = (ComplicationActivitySetting) this.l.a(dianaPresetComplicationSetting.getSettings(), ComplicationActivitySetting.class);
                            if (complicationActivitySetting != null) {
                                complicationActivitySetting.setRingEnabled(z);
                                dianaPresetComplicationSetting.setSettings(this.l.a((Object) complicationActivitySetting));
                                DianaCustomizeViewModel dianaCustomizeViewModel3 = this.f;
                                if (dianaCustomizeViewModel3 != null) {
                                    dianaCustomizeViewModel3.a(clone);
                                } else {
                                    kd4.d("mDianaCustomizeViewModel");
                                    throw null;
                                }
                            }
                        }
                    }
                } else {
                    kd4.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            kd4.d("mDianaCustomizeViewModel");
            throw null;
        }
    }
}
