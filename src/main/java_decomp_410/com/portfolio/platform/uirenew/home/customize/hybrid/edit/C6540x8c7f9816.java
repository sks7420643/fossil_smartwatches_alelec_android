package com.portfolio.platform.uirenew.home.customize.hybrid.edit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1 */
public final class C6540x8c7f9816 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.room.microapp.HybridPreset $currentPreset$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.room.microapp.HybridPreset $it;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23017p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1$1")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1$1 */
    public static final class C65411 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super android.os.Parcelable>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting $buttonMapping;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23018p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.edit.C6540x8c7f9816 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C65411(com.portfolio.platform.uirenew.home.customize.hybrid.edit.C6540x8c7f9816 hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1, com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting hybridPresetAppSetting, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1;
            this.$buttonMapping = hybridPresetAppSetting;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.hybrid.edit.C6540x8c7f9816.C65411 r0 = new com.portfolio.platform.uirenew.home.customize.hybrid.edit.C6540x8c7f9816.C65411(this.this$0, this.$buttonMapping, yb4);
            r0.f23018p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.hybrid.edit.C6540x8c7f9816.C65411) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter.m34282e(this.this$0.this$0).mo41153d(this.$buttonMapping.getAppId());
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1$a")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1$a */
    public static final class C6542a implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase.C6512d, com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase.C6510b> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.edit.C6540x8c7f9816 f23019a;

        @DexIgnore
        public C6542a(com.portfolio.platform.uirenew.home.customize.hybrid.edit.C6540x8c7f9816 hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1) {
            this.f23019a = hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase.C6512d dVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HybridCustomizeEditPresenter", "setToWatch success");
            this.f23019a.this$0.f23009k.mo29104m();
            this.f23019a.this$0.f23009k.mo29102g(true);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo29641a(com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase.C6510b bVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(bVar, "errorValue");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HybridCustomizeEditPresenter", "setToWatch onError");
            this.f23019a.this$0.f23009k.mo29104m();
            int b = bVar.mo41128b();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("HybridCustomizeEditPresenter", "setPresetToWatch() - mSetHybridPresetToWatchUseCase - onError - lastErrorCode = " + b);
            if (b != 1101) {
                if (b == 8888) {
                    this.f23019a.this$0.f23009k.mo29100c();
                    return;
                } else if (!(b == 1112 || b == 1113)) {
                    this.f23019a.this$0.f23009k.mo29107q();
                    return;
                }
            }
            java.util.List<com.portfolio.platform.enums.PermissionCodes> convertBLEPermissionErrorCode = com.portfolio.platform.enums.PermissionCodes.convertBLEPermissionErrorCode(bVar.mo41127a());
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            com.fossil.blesdk.obfuscated.l63 h = this.f23019a.this$0.f23009k;
            java.lang.Object[] array = convertBLEPermissionErrorCode.toArray(new com.portfolio.platform.enums.PermissionCodes[0]);
            if (array != null) {
                com.portfolio.platform.enums.PermissionCodes[] permissionCodesArr = (com.portfolio.platform.enums.PermissionCodes[]) array;
                h.mo26045a((com.portfolio.platform.enums.PermissionCodes[]) java.util.Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                return;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6540x8c7f9816(com.portfolio.platform.data.model.room.microapp.HybridPreset hybridPreset, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter hybridCustomizeEditPresenter, com.portfolio.platform.data.model.room.microapp.HybridPreset hybridPreset2) {
        super(2, yb4);
        this.$it = hybridPreset;
        this.this$0 = hybridCustomizeEditPresenter;
        this.$currentPreset$inlined = hybridPreset2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.hybrid.edit.C6540x8c7f9816 hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.customize.hybrid.edit.C6540x8c7f9816(this.$it, yb4, this.this$0, this.$currentPreset$inlined);
        hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.f23017p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.hybrid.edit.C6540x8c7f9816) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v25, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v19, resolved type: java.util.Iterator} */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01f6, code lost:
        if (android.text.TextUtils.isEmpty(((com.portfolio.platform.data.model.setting.SecondTimezoneSetting) r2.this$0.f23007i.mo23093a(r0.getSettings(), com.portfolio.platform.data.model.setting.SecondTimezoneSetting.class)).getTimeZoneId()) != false) goto L_0x021a;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0129  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0193 A[Catch:{ Exception -> 0x01a1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0216  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0236  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x02ad  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.uirenew.home.customize.hybrid.edit.C6540x8c7f9816 hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1;
        com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting hybridPresetAppSetting;
        java.lang.String str;
        java.lang.Object obj2;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.util.List list;
        com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting hybridPresetAppSetting2;
        java.util.List list2;
        com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting hybridPresetAppSetting3;
        java.util.Iterator it;
        java.lang.Object obj3;
        android.os.Parcelable parcelable;
        com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting hybridPresetAppSetting4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        com.fossil.blesdk.obfuscated.yb4 yb4 = null;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f23017p$;
            java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting> buttons = this.$it.getButtons();
            java.util.List arrayList = new java.util.ArrayList();
            for (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting next : buttons) {
                if (com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.gl2.f15135c.mo27708c(next.getAppId())).booleanValue()) {
                    arrayList.add(next);
                }
            }
            if (!arrayList.isEmpty()) {
                java.util.Iterator it2 = arrayList.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    hybridPresetAppSetting4 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) it2.next();
                    if (!com.fossil.blesdk.obfuscated.gl2.f15135c.mo27710e(hybridPresetAppSetting4.getAppId())) {
                        break;
                    }
                }
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HybridCustomizeEditPresenter", "setPresetToWatch missingPermissionComp " + hybridPresetAppSetting4);
                if (hybridPresetAppSetting4 == null) {
                    this.this$0.f23011m.mo27071q(true);
                    this.this$0.f23009k.mo29098b(hybridPresetAppSetting4.getAppId(), hybridPresetAppSetting4.getPosition());
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting> buttons2 = this.$it.getButtons();
                java.util.List arrayList2 = new java.util.ArrayList();
                for (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting next2 : buttons2) {
                    if (com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.gl2.f15135c.mo27709d(next2.getAppId())).booleanValue()) {
                        arrayList2.add(next2);
                    }
                }
                if (!arrayList2.isEmpty()) {
                    it = arrayList2.iterator();
                    zg4 = zg42;
                    obj2 = a;
                    list = arrayList;
                    hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1 = this;
                    hybridPresetAppSetting3 = null;
                    java.util.List list3 = arrayList2;
                    hybridPresetAppSetting2 = hybridPresetAppSetting4;
                    list2 = list3;
                } else {
                    hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1 = this;
                    hybridPresetAppSetting = null;
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
                    if (hybridPresetAppSetting != null) {
                    }
                }
            }
            hybridPresetAppSetting4 = null;
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HybridCustomizeEditPresenter", "setPresetToWatch missingPermissionComp " + hybridPresetAppSetting4);
            if (hybridPresetAppSetting4 == null) {
            }
        } else if (i == 1) {
            it = this.L$6;
            hybridPresetAppSetting = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) this.L$5;
            hybridPresetAppSetting3 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) this.L$4;
            list2 = (java.util.List) this.L$3;
            hybridPresetAppSetting2 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) this.L$2;
            list = (java.util.List) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            try {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                obj3 = obj;
                obj2 = a;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1 = this;
            } catch (java.lang.Exception e) {
                e = e;
                obj2 = a;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1 = this;
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e("HybridCustomizeEditPresenter", "exception when parse setting from json " + e);
                yb4 = null;
                if (it.hasNext()) {
                }
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
                if (hybridPresetAppSetting != null) {
                }
            }
            parcelable = (android.os.Parcelable) obj3;
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HybridCustomizeEditPresenter", "last setting " + parcelable);
            if (parcelable != null) {
                hybridPresetAppSetting.setSettings(hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.this$0.f23007i.mo23096a((java.lang.Object) parcelable));
                yb4 = null;
            }
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
            if (hybridPresetAppSetting != null) {
                java.lang.String appId = hybridPresetAppSetting.getAppId();
                if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) appId, (java.lang.Object) com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                    str = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.Customization_Buttons_CommuteTimeError___ToDisplayCommuteTimePleaseEnter);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str, "LanguageHelper.getString\u2026ayCommuteTimePleaseEnter)");
                } else if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) appId, (java.lang.Object) com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                    str = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.Customization_Complications_SecondTimezoneError_Text__ToDisplayTimezoneOnYourWatch);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str, "LanguageHelper.getString\u2026splayTimezoneOnYourWatch)");
                } else if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) appId, (java.lang.Object) com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                    str = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.set_to_watch_fail_ring_phone_not_configured);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str, "LanguageHelper.getString\u2026ing_phone_not_configured)");
                } else {
                    str = "";
                }
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.this$0.f23009k.mo29099b(str, hybridPresetAppSetting.getAppId(), hybridPresetAppSetting.getPosition());
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.this$0.f23009k.mo29103l();
            hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.this$0.f23010l.mo34435a(new com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase.C6511c(hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.$currentPreset$inlined), new com.portfolio.platform.uirenew.home.customize.hybrid.edit.C6540x8c7f9816.C6542a(hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (it.hasNext()) {
            hybridPresetAppSetting = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) it.next();
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HybridCustomizeEditPresenter", "check setting of " + hybridPresetAppSetting);
            try {
            } catch (java.lang.Exception e2) {
                e = e2;
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e("HybridCustomizeEditPresenter", "exception when parse setting from json " + e);
                yb4 = null;
                if (it.hasNext()) {
                }
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
                if (hybridPresetAppSetting != null) {
                }
            }
            if (com.fossil.blesdk.obfuscated.oj2.m26116a(hybridPresetAppSetting.getSettings())) {
                com.fossil.blesdk.obfuscated.ug4 a2 = hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.customize.hybrid.edit.C6540x8c7f9816.C65411 r14 = new com.portfolio.platform.uirenew.home.customize.hybrid.edit.C6540x8c7f9816.C65411(hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1, hybridPresetAppSetting, yb4);
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.L$0 = zg4;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.L$1 = list;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.L$2 = hybridPresetAppSetting2;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.L$3 = list2;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.L$4 = hybridPresetAppSetting3;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.L$5 = hybridPresetAppSetting;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.L$6 = it;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.label = 1;
                obj3 = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r14, hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1);
                if (obj3 == obj2) {
                    return obj2;
                }
                parcelable = (android.os.Parcelable) obj3;
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HybridCustomizeEditPresenter", "last setting " + parcelable);
                if (parcelable != null) {
                }
            }
            java.lang.String appId2 = hybridPresetAppSetting.getAppId();
            if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) appId2, (java.lang.Object) com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) appId2, (java.lang.Object) com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                }
                yb4 = null;
                if (it.hasNext()) {
                    hybridPresetAppSetting = hybridPresetAppSetting3;
                }
            }
            if (android.text.TextUtils.isEmpty(((com.portfolio.platform.data.model.setting.CommuteTimeSetting) hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.this$0.f23007i.mo23093a(hybridPresetAppSetting.getSettings(), com.portfolio.platform.data.model.setting.CommuteTimeSetting.class)).getAddress())) {
            }
            yb4 = null;
            if (it.hasNext()) {
            }
        }
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
        if (hybridPresetAppSetting != null) {
        }
    }
}
