package com.portfolio.platform.uirenew.home.profile;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$Anon4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super String>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Device $deviceModel;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeProfilePresenter$start$Anon4.Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(Device device, yb4 yb4, HomeProfilePresenter$start$Anon4.Anon1 anon1) {
        super(2, yb4);
        this.$deviceModel = device;
        this.this$Anon0 = anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 homeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 = new HomeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(this.$deviceModel, yb4, this.this$Anon0);
        homeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1.p$ = (zg4) obj;
        return homeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return this.this$Anon0.this$Anon0.a.w.getDeviceNameBySerial(this.$deviceModel.getDeviceId());
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
