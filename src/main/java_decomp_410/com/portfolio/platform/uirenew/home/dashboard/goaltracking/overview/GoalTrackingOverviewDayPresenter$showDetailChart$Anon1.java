package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$Anon1", f = "GoalTrackingOverviewDayPresenter.kt", l = {99, 102}, m = "invokeSuspend")
public final class GoalTrackingOverviewDayPresenter$showDetailChart$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingOverviewDayPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingOverviewDayPresenter$showDetailChart$Anon1(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = goalTrackingOverviewDayPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        GoalTrackingOverviewDayPresenter$showDetailChart$Anon1 goalTrackingOverviewDayPresenter$showDetailChart$Anon1 = new GoalTrackingOverviewDayPresenter$showDetailChart$Anon1(this.this$Anon0, yb4);
        goalTrackingOverviewDayPresenter$showDetailChart$Anon1.p$ = (zg4) obj;
        return goalTrackingOverviewDayPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingOverviewDayPresenter$showDetailChart$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00c6  */
    public final Object invokeSuspend(Object obj) {
        Pair pair;
        ArrayList arrayList;
        os3 os3;
        int i;
        zg4 zg4;
        Object a = cc4.a();
        int i2 = this.label;
        if (i2 == 0) {
            na4.a(obj);
            zg4 = this.p$;
            ug4 a2 = this.this$Anon0.b();
            GoalTrackingOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1 goalTrackingOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1 = new GoalTrackingOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a2, goalTrackingOverviewDayPresenter$showDetailChart$Anon1$pair$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i2 == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i2 == 2) {
            arrayList = (ArrayList) this.L$Anon2;
            pair = (Pair) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            Integer num = (Integer) obj;
            os3 = (os3) this.this$Anon0.i.a();
            if (os3 != null) {
                GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) os3.d();
                if (goalTrackingSummary != null) {
                    Integer a3 = dc4.a(goalTrackingSummary.getGoalTarget() / 16);
                    if (a3 != null) {
                        i = a3.intValue();
                        this.this$Anon0.k.a(new BarChart.c(Math.max(num != null ? num.intValue() : 0, i / 16), i, arrayList), (ArrayList) pair.getSecond());
                        return qa4.a;
                    }
                }
            }
            i = 8;
            this.this$Anon0.k.a(new BarChart.c(Math.max(num != null ? num.intValue() : 0, i / 16), i, arrayList), (ArrayList) pair.getSecond());
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair2 = (Pair) obj;
        ArrayList arrayList2 = (ArrayList) pair2.getFirst();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewDayPresenter", "showDetailChart - data=" + arrayList2);
        ug4 a4 = this.this$Anon0.b();
        GoalTrackingOverviewDayPresenter$showDetailChart$Anon1$maxValue$Anon1 goalTrackingOverviewDayPresenter$showDetailChart$Anon1$maxValue$Anon1 = new GoalTrackingOverviewDayPresenter$showDetailChart$Anon1$maxValue$Anon1(arrayList2, (yb4) null);
        this.L$Anon0 = zg4;
        this.L$Anon1 = pair2;
        this.L$Anon2 = arrayList2;
        this.label = 2;
        Object a5 = yf4.a(a4, goalTrackingOverviewDayPresenter$showDetailChart$Anon1$maxValue$Anon1, this);
        if (a5 == a) {
            return a;
        }
        arrayList = arrayList2;
        Object obj2 = a5;
        pair = pair2;
        obj = obj2;
        Integer num2 = (Integer) obj;
        os3 = (os3) this.this$Anon0.i.a();
        if (os3 != null) {
        }
        i = 8;
        this.this$Anon0.k.a(new BarChart.c(Math.max(num2 != null ? num2.intValue() : 0, i / 16), i, arrayList), (ArrayList) pair.getSecond());
        return qa4.a;
    }
}
