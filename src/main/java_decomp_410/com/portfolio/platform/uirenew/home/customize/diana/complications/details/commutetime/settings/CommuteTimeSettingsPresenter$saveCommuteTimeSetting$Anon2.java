package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2", f = "CommuteTimeSettingsPresenter.kt", l = {161, 168}, m = "invokeSuspend")
public final class CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $address;
    @DexIgnore
    public /* final */ /* synthetic */ String $displayInfo;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeSettingsPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2$Anon1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef $searchedRecent;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2 commuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2, Ref$ObjectRef ref$ObjectRef, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = commuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2;
            this.$searchedRecent = ref$ObjectRef;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$searchedRecent, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.this$Anon0.m.a((List<String>) (List) this.$searchedRecent.element);
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2(CommuteTimeSettingsPresenter commuteTimeSettingsPresenter, String str, String str2, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = commuteTimeSettingsPresenter;
        this.$address = str;
        this.$displayInfo = str2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2 commuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2 = new CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2(this.this$Anon0, this.$address, this.$displayInfo, yb4);
        commuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2.p$ = (zg4) obj;
        return commuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(T t) {
        zg4 zg4;
        Ref$ObjectRef ref$ObjectRef;
        Ref$ObjectRef ref$ObjectRef2;
        T a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(t);
            zg4 = this.p$;
            ref$ObjectRef2 = new Ref$ObjectRef();
            ug4 a2 = this.this$Anon0.c();
            CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2$searchedRecent$Anon1 commuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2$searchedRecent$Anon1 = new CommuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2$searchedRecent$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = ref$ObjectRef2;
            this.L$Anon2 = ref$ObjectRef2;
            this.label = 1;
            t = yf4.a(a2, commuteTimeSettingsPresenter$saveCommuteTimeSetting$Anon2$searchedRecent$Anon1, this);
            if (t == a) {
                return a;
            }
            ref$ObjectRef = ref$ObjectRef2;
        } else if (i == 1) {
            ref$ObjectRef2 = (Ref$ObjectRef) this.L$Anon2;
            ref$ObjectRef = (Ref$ObjectRef) this.L$Anon1;
            zg4 = (zg4) this.L$Anon0;
            na4.a(t);
        } else if (i == 2) {
            Ref$ObjectRef ref$ObjectRef3 = (Ref$ObjectRef) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(t);
            this.this$Anon0.l.a();
            this.this$Anon0.l.r(true);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        kd4.a((Object) t, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
        ref$ObjectRef2.element = (List) t;
        if (!((List) ref$ObjectRef.element).contains(this.$address)) {
            if (this.$displayInfo.length() > 0) {
                if (this.$address.length() > 0) {
                    ((List) ref$ObjectRef.element).add(0, this.$address);
                    if (((List) ref$ObjectRef.element).size() > 5) {
                        ref$ObjectRef.element = ((List) ref$ObjectRef.element).subList(0, 5);
                    }
                    ug4 a3 = this.this$Anon0.c();
                    Anon1 anon1 = new Anon1(this, ref$ObjectRef, (yb4) null);
                    this.L$Anon0 = zg4;
                    this.L$Anon1 = ref$ObjectRef;
                    this.label = 2;
                    if (yf4.a(a3, anon1, this) == a) {
                        return a;
                    }
                }
            }
        }
        this.this$Anon0.l.a();
        this.this$Anon0.l.r(true);
        return qa4.a;
    }
}
