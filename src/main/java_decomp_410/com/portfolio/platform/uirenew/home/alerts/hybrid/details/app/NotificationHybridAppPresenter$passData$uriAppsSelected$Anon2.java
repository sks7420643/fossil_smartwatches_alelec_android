package com.portfolio.platform.uirenew.home.alerts.hybrid.details.app;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationHybridAppPresenter$passData$uriAppsSelected$Anon2 extends Lambda implements xc4<AppWrapper, String> {
    @DexIgnore
    public static /* final */ NotificationHybridAppPresenter$passData$uriAppsSelected$Anon2 INSTANCE; // = new NotificationHybridAppPresenter$passData$uriAppsSelected$Anon2();

    @DexIgnore
    public NotificationHybridAppPresenter$passData$uriAppsSelected$Anon2() {
        super(1);
    }

    @DexIgnore
    public final String invoke(AppWrapper appWrapper) {
        kd4.b(appWrapper, "it");
        return String.valueOf(appWrapper.getUri());
    }
}
