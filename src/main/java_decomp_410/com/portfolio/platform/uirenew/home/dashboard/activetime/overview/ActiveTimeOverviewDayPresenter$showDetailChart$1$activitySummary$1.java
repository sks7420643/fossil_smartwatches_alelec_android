package com.portfolio.platform.uirenew.home.dashboard.activetime.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$activitySummary$1", f = "ActiveTimeOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
public final class ActiveTimeOverviewDayPresenter$showDetailChart$1$activitySummary$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.room.fitness.ActivitySummary>, java.lang.Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActiveTimeOverviewDayPresenter$showDetailChart$1$activitySummary$1(com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1 activeTimeOverviewDayPresenter$showDetailChart$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = activeTimeOverviewDayPresenter$showDetailChart$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$activitySummary$1 activeTimeOverviewDayPresenter$showDetailChart$1$activitySummary$1 = new com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$activitySummary$1(this.this$0, yb4);
        activeTimeOverviewDayPresenter$showDetailChart$1$activitySummary$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return activeTimeOverviewDayPresenter$showDetailChart$1$activitySummary$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$activitySummary$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.os3 os3 = (com.fossil.blesdk.obfuscated.os3) this.this$0.this$0.i.a();
            com.portfolio.platform.data.model.room.fitness.ActivitySummary activitySummary = null;
            if (os3 == null) {
                return null;
            }
            java.util.List list = (java.util.List) os3.d();
            if (list == null) {
                return null;
            }
            java.util.Iterator it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                java.lang.Object next = it.next();
                if (com.fossil.blesdk.obfuscated.dc4.a(com.fossil.blesdk.obfuscated.rk2.d(next.getDate(), com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter.d(this.this$0.this$0))).booleanValue()) {
                    activitySummary = next;
                    break;
                }
            }
            return activitySummary;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
