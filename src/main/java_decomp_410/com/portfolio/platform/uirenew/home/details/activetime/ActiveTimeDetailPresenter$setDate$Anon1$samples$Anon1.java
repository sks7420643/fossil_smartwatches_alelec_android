package com.portfolio.platform.uirenew.home.details.activetime;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$setDate$Anon1$samples$Anon1", f = "ActiveTimeDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class ActiveTimeDetailPresenter$setDate$Anon1$samples$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<ActivitySample>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActiveTimeDetailPresenter$setDate$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActiveTimeDetailPresenter$setDate$Anon1$samples$Anon1(ActiveTimeDetailPresenter$setDate$Anon1 activeTimeDetailPresenter$setDate$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = activeTimeDetailPresenter$setDate$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ActiveTimeDetailPresenter$setDate$Anon1$samples$Anon1 activeTimeDetailPresenter$setDate$Anon1$samples$Anon1 = new ActiveTimeDetailPresenter$setDate$Anon1$samples$Anon1(this.this$Anon0, yb4);
        activeTimeDetailPresenter$setDate$Anon1$samples$Anon1.p$ = (zg4) obj;
        return activeTimeDetailPresenter$setDate$Anon1$samples$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActiveTimeDetailPresenter$setDate$Anon1$samples$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            ActiveTimeDetailPresenter activeTimeDetailPresenter = this.this$Anon0.this$Anon0;
            return activeTimeDetailPresenter.a(activeTimeDetailPresenter.g, (List<ActivitySample>) this.this$Anon0.this$Anon0.l);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
