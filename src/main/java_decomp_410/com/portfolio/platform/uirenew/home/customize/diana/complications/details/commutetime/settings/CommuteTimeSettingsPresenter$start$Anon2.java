package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.u23;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.MFUser;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$Anon2", f = "CommuteTimeSettingsPresenter.kt", l = {73, 74}, m = "invokeSuspend")
public final class CommuteTimeSettingsPresenter$start$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeSettingsPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$Anon2$Anon1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super MFUser>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsPresenter$start$Anon2 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(CommuteTimeSettingsPresenter$start$Anon2 commuteTimeSettingsPresenter$start$Anon2, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = commuteTimeSettingsPresenter$start$Anon2;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return this.this$Anon0.this$Anon0.n.getCurrentUser();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsPresenter$start$Anon2(CommuteTimeSettingsPresenter commuteTimeSettingsPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = commuteTimeSettingsPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CommuteTimeSettingsPresenter$start$Anon2 commuteTimeSettingsPresenter$start$Anon2 = new CommuteTimeSettingsPresenter$start$Anon2(this.this$Anon0, yb4);
        commuteTimeSettingsPresenter$start$Anon2.p$ = (zg4) obj;
        return commuteTimeSettingsPresenter$start$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CommuteTimeSettingsPresenter$start$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0087  */
    public final Object invokeSuspend(Object obj) {
        MFUser e;
        zg4 zg4;
        CommuteTimeSettingsPresenter commuteTimeSettingsPresenter;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg42 = this.p$;
            commuteTimeSettingsPresenter = this.this$Anon0;
            ug4 a2 = commuteTimeSettingsPresenter.c();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg42;
            this.L$Anon1 = commuteTimeSettingsPresenter;
            this.label = 1;
            Object a3 = yf4.a(a2, anon1, this);
            if (a3 == a) {
                return a;
            }
            Object obj2 = a3;
            zg4 = zg42;
            obj = obj2;
        } else if (i == 1) {
            commuteTimeSettingsPresenter = (CommuteTimeSettingsPresenter) this.L$Anon1;
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            zg4 zg43 = (zg4) this.L$Anon0;
            na4.a(obj);
            kd4.a(obj, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
            this.this$Anon0.i.clear();
            this.this$Anon0.i.addAll((List) obj);
            e = this.this$Anon0.j;
            if (e != null) {
                u23 g = this.this$Anon0.l;
                String home = e.getHome();
                if (home == null) {
                    home = "";
                }
                g.w(home);
                u23 g2 = this.this$Anon0.l;
                String work = e.getWork();
                if (work == null) {
                    work = "";
                }
                g2.D(work);
            }
            this.this$Anon0.l.j(this.this$Anon0.i);
            this.this$Anon0.l.a(this.this$Anon0.h);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        commuteTimeSettingsPresenter.j = (MFUser) obj;
        ug4 a4 = this.this$Anon0.c();
        CommuteTimeSettingsPresenter$start$Anon2$recentSearchedAddress$Anon1 commuteTimeSettingsPresenter$start$Anon2$recentSearchedAddress$Anon1 = new CommuteTimeSettingsPresenter$start$Anon2$recentSearchedAddress$Anon1(this, (yb4) null);
        this.L$Anon0 = zg4;
        this.label = 2;
        obj = yf4.a(a4, commuteTimeSettingsPresenter$start$Anon2$recentSearchedAddress$Anon1, this);
        if (obj == a) {
            return a;
        }
        kd4.a(obj, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
        this.this$Anon0.i.clear();
        this.this$Anon0.i.addAll((List) obj);
        e = this.this$Anon0.j;
        if (e != null) {
        }
        this.this$Anon0.l.j(this.this$Anon0.i);
        this.this$Anon0.l.a(this.this$Anon0.h);
        return qa4.a;
    }
}
