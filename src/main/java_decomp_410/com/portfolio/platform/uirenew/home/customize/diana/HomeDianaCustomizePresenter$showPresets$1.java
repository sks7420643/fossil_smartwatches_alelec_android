package com.portfolio.platform.uirenew.home.customize.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1", mo27670f = "HomeDianaCustomizePresenter.kt", mo27671l = {333}, mo27672m = "invokeSuspend")
public final class HomeDianaCustomizePresenter$showPresets$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22699p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$showPresets$1(com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter homeDianaCustomizePresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homeDianaCustomizePresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1 homeDianaCustomizePresenter$showPresets$1 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1(this.this$0, yb4);
        homeDianaCustomizePresenter$showPresets$1.f22699p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeDianaCustomizePresenter$showPresets$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22699p$;
            com.fossil.blesdk.obfuscated.ug4 c = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1$wrapperUIData$1 homeDianaCustomizePresenter$showPresets$1$wrapperUIData$1 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1$wrapperUIData$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(c, homeDianaCustomizePresenter$showPresets$1$wrapperUIData$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.f22678t.mo26425c((java.util.List<com.fossil.blesdk.obfuscated.f13>) (java.util.List) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
