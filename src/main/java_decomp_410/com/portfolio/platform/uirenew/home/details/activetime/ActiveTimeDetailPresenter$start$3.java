package com.portfolio.platform.uirenew.home.details.activetime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActiveTimeDetailPresenter$start$3<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>>> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter f23560a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$3$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$3$1", mo27670f = "ActiveTimeDetailPresenter.kt", mo27671l = {149}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$3$1 */
    public static final class C66821 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23561p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$3 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C66821(com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$3 activeTimeDetailPresenter$start$3, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = activeTimeDetailPresenter$start$3;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$3.C66821 r0 = new com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$3.C66821(this.this$0, yb4);
            r0.f23561p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$3.C66821) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23561p$;
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.f23560a.mo31440b();
                com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$3$1$samples$1 activeTimeDetailPresenter$start$3$1$samples$1 = new com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$3$1$samples$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, activeTimeDetailPresenter$start$3$1$samples$1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            java.util.List list = (java.util.List) obj;
            if (this.this$0.f23560a.f23533n == null || (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23560a.f23533n, (java.lang.Object) list))) {
                this.this$0.f23560a.f23533n = list;
                if (this.this$0.f23560a.f23528i && this.this$0.f23560a.f23529j) {
                    com.fossil.blesdk.obfuscated.fi4 unused = this.this$0.f23560a.mo41359l();
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public ActiveTimeDetailPresenter$start$3(com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter activeTimeDetailPresenter) {
        this.f23560a = activeTimeDetailPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>> os3) {
        com.portfolio.platform.enums.Status a = os3.mo29972a();
        java.util.List list = (java.util.List) os3.mo29973b();
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("start - sampleTransformations -- activitySamples=");
        sb.append(list != null ? java.lang.Integer.valueOf(list.size()) : null);
        sb.append(", status=");
        sb.append(a);
        local.mo33255d("ActiveTimeDetailPresenter", sb.toString());
        if (a == com.portfolio.platform.enums.Status.NETWORK_LOADING || a == com.portfolio.platform.enums.Status.SUCCESS) {
            this.f23560a.f23531l = list;
            this.f23560a.f23529j = true;
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23560a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$3.C66821(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        }
    }
}
