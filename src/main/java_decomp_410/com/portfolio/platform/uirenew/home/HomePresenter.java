package com.portfolio.platform.uirenew.home;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ak3;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kr2;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.qr2;
import com.fossil.blesdk.obfuscated.qu2;
import com.fossil.blesdk.obfuscated.ru2;
import com.fossil.blesdk.obfuscated.vj2;
import com.fossil.blesdk.obfuscated.wu2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.util.UserUtils;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomePresenter extends qu2 implements ak3.b {
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ a y; // = new a((fd4) null);
    @DexIgnore
    public int f;
    @DexIgnore
    public volatile boolean g;
    @DexIgnore
    public LiveData<List<InAppNotification>> h;
    @DexIgnore
    public /* final */ c i; // = new c(this);
    @DexIgnore
    public /* final */ e j; // = new e(this);
    @DexIgnore
    public /* final */ d k; // = new d(this);
    @DexIgnore
    public /* final */ ru2 l;
    @DexIgnore
    public /* final */ en2 m;
    @DexIgnore
    public /* final */ DeviceRepository n;
    @DexIgnore
    public /* final */ PortfolioApp o;
    @DexIgnore
    public /* final */ UpdateFirmwareUsecase p;
    @DexIgnore
    public /* final */ j62 q;
    @DexIgnore
    public /* final */ vj2 r;
    @DexIgnore
    public /* final */ qr2 s;
    @DexIgnore
    public /* final */ UserUtils t;
    @DexIgnore
    public /* final */ ServerSettingRepository u;
    @DexIgnore
    public /* final */ kr2 v;
    @DexIgnore
    public /* final */ ak3 w;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return HomePresenter.x;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements i62.d<qr2.a.c, qr2.a.C0102a> {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        public b(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(qr2.a.c cVar) {
            FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "GetServerSettingUseCase onSuccess");
            this.a.a(true);
        }

        @DexIgnore
        public void a(qr2.a.C0102a aVar) {
            FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "GetServerSettingUseCase onError");
            this.a.a(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        public c(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            kd4.b(communicateMode, "communicateMode");
            kd4.b(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && qf4.b(stringExtra, this.a.o.e(), true)) {
                this.a.l.a(intent);
                int intExtra = intent.getIntExtra("sync_result", 3);
                int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>();
                }
                int intExtra3 = intent.getIntExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), 10);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = HomePresenter.y.a();
                local.d(a2, "Inside .syncReceiver syncResult=" + intExtra + ", serial=" + stringExtra + ", " + "errorCode=" + intExtra2 + " permissionErrors " + integerArrayListExtra + " originalSyncMode " + intExtra3);
                if (intExtra == 1) {
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.OTHER;
                    kd4.a((Object) stringExtra, "serial");
                    remote.i(component, session, stringExtra, HomePresenter.y.a(), "[Sync OK] Check FW to OTA");
                    this.a.k();
                    this.a.l();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        public d(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            kd4.b(communicateMode, "communicateMode");
            kd4.b(intent, "intent");
            this.a.b(false);
            boolean booleanExtra = intent.getBooleanExtra("OTA_RESULT", false);
            boolean A = this.a.o.A();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomePresenter.y.a();
            local.d(a2, "ota complete isSuccess" + booleanExtra + " isDeviceStillOta " + A);
            if (!A || !booleanExtra) {
                this.a.l.b(booleanExtra);
            }
            if (booleanExtra) {
                this.a.m.a(this.a.o.e(), -1);
                PortfolioApp.W.c().a(this.a.r, false, 13);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        public e(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            kd4.b(context, "context");
            kd4.b(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomePresenter.y.a();
            local.d(a2, "---Inside .otaProgressReceiver ota progress " + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
            if (!TextUtils.isEmpty(otaEvent.getSerial())) {
                if (!this.a.n()) {
                    this.a.b(true);
                }
                if (qf4.b(otaEvent.getSerial(), this.a.o.e(), true)) {
                    this.a.l.b((int) otaEvent.getProcess());
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CoroutineUseCase.e<UpdateFirmwareUsecase.d, UpdateFirmwareUsecase.c> {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        public f(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        public void a(UpdateFirmwareUsecase.c cVar) {
            kd4.b(cVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateFirmwareUsecase.d dVar) {
            kd4.b(dVar, "responseValue");
            this.a.b(true);
            this.a.l.G();
        }
    }

    /*
    static {
        String simpleName = HomePresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "HomePresenter::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    public HomePresenter(ru2 ru2, en2 en2, DeviceRepository deviceRepository, PortfolioApp portfolioApp, UpdateFirmwareUsecase updateFirmwareUsecase, j62 j62, vj2 vj2, qr2 qr2, UserUtils userUtils, ServerSettingRepository serverSettingRepository, kr2 kr2, ak3 ak3) {
        kd4.b(ru2, "mView");
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(portfolioApp, "mApp");
        kd4.b(updateFirmwareUsecase, "mUpdateFwUseCase");
        kd4.b(j62, "mUseCaseHandler");
        kd4.b(vj2, "mDeviceSettingFactory");
        kd4.b(qr2, "mGetServerSettingUseCase");
        kd4.b(userUtils, "mUserUtils");
        kd4.b(serverSettingRepository, "mServerSettingRepository");
        kd4.b(kr2, "mGetZendeskInformation");
        kd4.b(ak3, "mInAppManager");
        this.l = ru2;
        this.m = en2;
        this.n = deviceRepository;
        this.o = portfolioApp;
        this.p = updateFirmwareUsecase;
        this.q = j62;
        this.r = vj2;
        this.s = qr2;
        this.t = userUtils;
        this.u = serverSettingRepository;
        this.v = kr2;
        this.w = ak3;
    }

    @DexIgnore
    public final void l() {
        this.q.a(this.s, new qr2.a.b(), new b(this));
    }

    @DexIgnore
    public final void m() {
        String e2 = this.o.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "Inside .checkLastOTASuccess, activeSerial=" + e2);
        if (!TextUtils.isEmpty(e2)) {
            String i2 = this.m.i(e2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = x;
            local2.d(str2, "Inside .checkLastOTASuccess, lastTempFw=" + i2);
            if (!TextUtils.isEmpty(i2)) {
                fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomePresenter$checkLastOTASuccess$Anon1(this, e2, i2, (yb4) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    public final boolean n() {
        return this.g;
    }

    @DexIgnore
    public void o() {
        this.l.a(this);
        this.w.a((ak3.b) this);
    }

    @DexIgnore
    public final void p() {
        FLogger.INSTANCE.getLocal().d(x, "Inside .updateFirmware");
        String e2 = this.o.e();
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, e2, x, "[Sync OK] Start update FW");
        if (!TextUtils.isEmpty(e2)) {
            this.p.a(new UpdateFirmwareUsecase.b(e2, false, 2, (fd4) null), new f(this));
        }
    }

    @DexIgnore
    public void a(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "setTab " + i2);
        this.f = i2;
    }

    @DexIgnore
    public final void b(boolean z) {
        this.g = z;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(x, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        PortfolioApp portfolioApp = this.o;
        e eVar = this.j;
        portfolioApp.registerReceiver(eVar, new IntentFilter(this.o.getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.k, CommunicateMode.OTA);
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.i, CommunicateMode.SYNC);
        BleCommandResultManager.d.a(CommunicateMode.SYNC, CommunicateMode.OTA);
        m();
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(x, "stop");
        try {
            BleCommandResultManager.d.b((BleCommandResultManager.b) this.k, CommunicateMode.OTA);
            BleCommandResultManager.d.b((BleCommandResultManager.b) this.i, CommunicateMode.SYNC);
            this.o.unregisterReceiver(this.j);
            LiveData<List<InAppNotification>> liveData = this.h;
            if (liveData != null) {
                ru2 ru2 = this.l;
                if (ru2 != null) {
                    liveData.a((LifecycleOwner) (wu2) ru2);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeFragment");
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = x;
            local.d(str, "Exception when stop presenter=" + e2);
        }
    }

    @DexIgnore
    public int h() {
        return this.f;
    }

    @DexIgnore
    public boolean i() {
        return this.g || this.o.A();
    }

    @DexIgnore
    public void j() {
        this.w.a();
    }

    @DexIgnore
    public final void k() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "checkFirmware(), isSkipOTA=" + this.m.T());
        if (!PortfolioApp.W.e() || !this.m.T()) {
            String e2 = this.o.e();
            boolean A = this.o.A();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = x;
            local2.d(str2, "checkFirmware() isDeviceOtaing=" + A);
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String str3 = x;
            remote.i(component, session, e2, str3, "[Sync OK] Is device OTAing " + A);
            if (!A) {
                fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomePresenter$checkFirmware$Anon1(this, e2, (yb4) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    public void b(String str) {
        kd4.b(str, "subject");
        this.v.a(new kr2.c(str), new HomePresenter$startCollectingUserFeedback$Anon1(this));
    }

    @DexIgnore
    public void a(InAppNotification inAppNotification) {
        kd4.b(inAppNotification, "inAppNotification");
        this.l.a(inAppNotification);
    }

    @DexIgnore
    public void a(String str) {
        int i2;
        kd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        local.d(str2, "on active serial change serial " + str);
        if (TextUtils.isEmpty(this.o.u().x())) {
            i2 = 0;
        } else {
            i2 = (TextUtils.isEmpty(str) || !DeviceHelper.o.e(str)) ? 1 : 2;
        }
        if (!TextUtils.isEmpty(str)) {
            this.l.a(FossilDeviceSerialPatternUtil.getDeviceBySerial(str), i2);
        } else {
            this.l.a((FossilDeviceSerialPatternUtil.DEVICE) null, i2);
        }
    }

    @DexIgnore
    public void b(InAppNotification inAppNotification) {
        if (inAppNotification != null) {
            this.w.a(inAppNotification);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HomePresenter$checkIfHappyUser$Anon1(this, z, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(int i2, int i3, Intent intent) {
        this.l.onActivityResult(i2, i3, intent);
    }
}
