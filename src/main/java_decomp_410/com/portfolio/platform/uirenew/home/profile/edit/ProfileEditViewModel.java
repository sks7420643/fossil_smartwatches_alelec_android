package com.portfolio.platform.uirenew.home.profile.edit;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.facebook.internal.Utility;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.jc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nr2;
import com.fossil.blesdk.obfuscated.pk2;
import com.fossil.blesdk.obfuscated.qh3;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.Gender;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import java.io.PrintStream;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.text.StringsKt__StringsKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ProfileEditViewModel extends ic {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a((fd4) null);
    @DexIgnore
    public MFUser c;
    @DexIgnore
    public MFUser d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public volatile boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public MutableLiveData<b> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ UpdateUser i;
    @DexIgnore
    public /* final */ nr2 j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ProfileEditViewModel.k;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public MFUser a;
        @DexIgnore
        public Uri b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public Pair<Integer, String> d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public MFUser f;
        @DexIgnore
        public boolean g;

        @DexIgnore
        public b(MFUser mFUser, Uri uri, Boolean bool, Pair<Integer, String> pair, boolean z, MFUser mFUser2, boolean z2) {
            this.a = mFUser;
            this.b = uri;
            this.c = bool;
            this.d = pair;
            this.e = z;
            this.f = mFUser2;
            this.g = z2;
        }

        @DexIgnore
        public final Uri a() {
            return this.b;
        }

        @DexIgnore
        public final boolean b() {
            return this.e;
        }

        @DexIgnore
        public final boolean c() {
            return this.g;
        }

        @DexIgnore
        public final Pair<Integer, String> d() {
            return this.d;
        }

        @DexIgnore
        public final MFUser e() {
            return this.f;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (kd4.a((Object) this.a, (Object) bVar.a) && kd4.a((Object) this.b, (Object) bVar.b) && kd4.a((Object) this.c, (Object) bVar.c) && kd4.a((Object) this.d, (Object) bVar.d)) {
                        if ((this.e == bVar.e) && kd4.a((Object) this.f, (Object) bVar.f)) {
                            if (this.g == bVar.g) {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final MFUser f() {
            return this.a;
        }

        @DexIgnore
        public final Boolean g() {
            return this.c;
        }

        @DexIgnore
        public int hashCode() {
            MFUser mFUser = this.a;
            int i = 0;
            int hashCode = (mFUser != null ? mFUser.hashCode() : 0) * 31;
            Uri uri = this.b;
            int hashCode2 = (hashCode + (uri != null ? uri.hashCode() : 0)) * 31;
            Boolean bool = this.c;
            int hashCode3 = (hashCode2 + (bool != null ? bool.hashCode() : 0)) * 31;
            Pair<Integer, String> pair = this.d;
            int hashCode4 = (hashCode3 + (pair != null ? pair.hashCode() : 0)) * 31;
            boolean z = this.e;
            if (z) {
                z = true;
            }
            int i2 = (hashCode4 + (z ? 1 : 0)) * 31;
            MFUser mFUser2 = this.f;
            if (mFUser2 != null) {
                i = mFUser2.hashCode();
            }
            int i3 = (i2 + i) * 31;
            boolean z2 = this.g;
            if (z2) {
                z2 = true;
            }
            return i3 + (z2 ? 1 : 0);
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(user=" + this.a + ", imageUri=" + this.b + ", isUserChanged=" + this.c + ", showServerError=" + this.d + ", showGeneralError=" + this.e + ", showSuccess=" + this.f + ", showLoading=" + this.g + ")";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.e<nr2.a, CoroutineUseCase.a> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditViewModel a;

        @DexIgnore
        public c(ProfileEditViewModel profileEditViewModel) {
            this.a = profileEditViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(nr2.a aVar) {
            kd4.b(aVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(ProfileEditViewModel.l.a(), ".Inside mGetUser onSuccess");
            ProfileEditViewModel.a(this.a, (MFUser) null, (Uri) null, (Boolean) null, (Pair) null, false, (MFUser) null, false, 63, (Object) null);
            if (aVar.a() != null) {
                this.a.d = aVar.a();
                this.a.c = new MFUser(aVar.a());
                ProfileEditViewModel profileEditViewModel = this.a;
                ProfileEditViewModel.a(profileEditViewModel, profileEditViewModel.d, (Uri) null, (Boolean) null, (Pair) null, false, (MFUser) null, false, 126, (Object) null);
            } else {
                FLogger.INSTANCE.getLocal().d(ProfileEditViewModel.l.a(), "loadUserFirstTime user is null");
            }
            this.a.g = true;
        }

        @DexIgnore
        public void a(CoroutineUseCase.a aVar) {
            kd4.b(aVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(ProfileEditViewModel.l.a(), ".Inside mGetUser onError");
            ProfileEditViewModel.a(this.a, (MFUser) null, (Uri) null, (Boolean) null, (Pair) null, false, (MFUser) null, false, 63, (Object) null);
            this.a.g = true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.e<UpdateUser.d, UpdateUser.c> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditViewModel a;

        @DexIgnore
        public d(ProfileEditViewModel profileEditViewModel) {
            this.a = profileEditViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            kd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(ProfileEditViewModel.l.a(), ".Inside updateUser onSuccess");
            ProfileEditViewModel.a(this.a, (MFUser) null, (Uri) null, (Boolean) null, (Pair) null, false, dVar.a(), false, 31, (Object) null);
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            kd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ProfileEditViewModel.l.a();
            local.d(a2, ".Inside updateUser onError, errorCode=" + cVar.a());
            ProfileEditViewModel.a(this.a, (MFUser) null, (Uri) null, (Boolean) null, new Pair(Integer.valueOf(cVar.a()), ""), false, (MFUser) null, false, 55, (Object) null);
        }
    }

    /*
    static {
        String simpleName = ProfileEditViewModel.class.getSimpleName();
        kd4.a((Object) simpleName, "ProfileEditViewModel::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public ProfileEditViewModel(UpdateUser updateUser, nr2 nr2) {
        kd4.b(updateUser, "mUpdateUser");
        kd4.b(nr2, "mGetUser");
        this.i = updateUser;
        this.j = nr2;
    }

    @DexIgnore
    public final MutableLiveData<b> c() {
        return this.h;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00b3, code lost:
        if (r5 != (r0 + r4.intValue())) goto L_0x00d4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00d2, code lost:
        if (r0 != r4.getHeightInCentimeters()) goto L_0x00d4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d6, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0115, code lost:
        if (java.lang.Math.round(r4 * r5) != java.lang.Math.round(com.fossil.blesdk.obfuscated.pk2.f((float) r6.getWeightInGrams()) * r5)) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0119, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0146, code lost:
        if (java.lang.Math.round(r4 * r5) != java.lang.Math.round((((float) r7.getWeightInGrams()) / 1000.0f) * r5)) goto L_0x0117;
     */
    @DexIgnore
    public final boolean d() {
        MFUser mFUser = this.c;
        if (mFUser == null || this.d == null) {
            return false;
        }
        if (mFUser != null) {
            Unit heightUnit = mFUser.getHeightUnit();
            if (heightUnit != null && qh3.a[heightUnit.ordinal()] == 1) {
                MFUser mFUser2 = this.c;
                if (mFUser2 != null) {
                    int heightInCentimeters = mFUser2.getHeightInCentimeters();
                    MFUser mFUser3 = this.d;
                    if (mFUser3 == null) {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                PrintStream printStream = System.out;
                StringBuilder sb = new StringBuilder();
                sb.append("isUserChangeSomething ");
                MFUser mFUser4 = this.c;
                if (mFUser4 != null) {
                    sb.append((float) mFUser4.getHeightInCentimeters());
                    printStream.println(sb.toString());
                    MFUser mFUser5 = this.c;
                    if (mFUser5 != null) {
                        Pair<Integer, Integer> b2 = pk2.b((float) mFUser5.getHeightInCentimeters());
                        MFUser mFUser6 = this.d;
                        if (mFUser6 != null) {
                            Pair<Integer, Integer> b3 = pk2.b((float) mFUser6.getHeightInCentimeters());
                            PrintStream printStream2 = System.out;
                            printStream2.println("isUserChangeSomething " + b2);
                            Integer first = b2.getFirst();
                            kd4.a((Object) first, "defaultHeightInFeetAndInches.first");
                            int a2 = pk2.a(first.intValue());
                            Integer second = b2.getSecond();
                            kd4.a((Object) second, "defaultHeightInFeetAndInches.second");
                            int intValue = a2 + second.intValue();
                            Integer first2 = b3.getFirst();
                            kd4.a((Object) first2, "currentHeightInFeetAndInches.first");
                            int a3 = pk2.a(first2.intValue());
                            Integer second2 = b3.getSecond();
                            kd4.a((Object) second2, "currentHeightInFeetAndInches.second");
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
            boolean z = true;
            MFUser mFUser7 = this.c;
            if (mFUser7 != null) {
                Unit weightUnit = mFUser7.getWeightUnit();
                if (weightUnit != null && qh3.b[weightUnit.ordinal()] == 1) {
                    MFUser mFUser8 = this.c;
                    if (mFUser8 != null) {
                        float weightInGrams = ((float) mFUser8.getWeightInGrams()) / 1000.0f;
                        MFUser mFUser9 = this.d;
                        if (mFUser9 != null) {
                            float f2 = (float) 10;
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    MFUser mFUser10 = this.c;
                    if (mFUser10 != null) {
                        float f3 = pk2.f((float) mFUser10.getWeightInGrams());
                        MFUser mFUser11 = this.d;
                        if (mFUser11 != null) {
                            float f4 = (float) 10;
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                boolean z2 = true;
                if (z || z2) {
                    return true;
                }
                return false;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final boolean e() {
        MFUser mFUser = this.c;
        if (mFUser == null || this.d == null) {
            return false;
        }
        if (mFUser != null) {
            String firstName = mFUser.getFirstName();
            kd4.a((Object) firstName, "mDefaultUser!!.firstName");
            if (firstName != null) {
                String obj = StringsKt__StringsKt.d(firstName).toString();
                MFUser mFUser2 = this.d;
                if (mFUser2 != null) {
                    if (!(!kd4.a((Object) obj, (Object) mFUser2.getFirstName()))) {
                        MFUser mFUser3 = this.c;
                        if (mFUser3 != null) {
                            String lastName = mFUser3.getLastName();
                            kd4.a((Object) lastName, "mDefaultUser!!.lastName");
                            if (lastName != null) {
                                String obj2 = StringsKt__StringsKt.d(lastName).toString();
                                MFUser mFUser4 = this.d;
                                if (mFUser4 == null) {
                                    kd4.a();
                                    throw null;
                                } else if (!(!kd4.a((Object) obj2, (Object) mFUser4.getLastName())) && !d()) {
                                    MFUser mFUser5 = this.c;
                                    if (mFUser5 != null) {
                                        String gender = mFUser5.getGender().toString();
                                        MFUser mFUser6 = this.d;
                                        if (mFUser6 == null) {
                                            kd4.a();
                                            throw null;
                                        } else if ((!kd4.a((Object) gender, (Object) mFUser6.getGender().toString())) || this.e) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    } else {
                                        kd4.a();
                                        throw null;
                                    }
                                }
                            } else {
                                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    return true;
                }
                kd4.a();
                throw null;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void f() {
        if (!this.g) {
            a(this, (MFUser) null, (Uri) null, (Boolean) null, (Pair) null, false, (MFUser) null, true, 63, (Object) null);
            this.j.a(null, new c(this));
        }
    }

    @DexIgnore
    public final void g() {
        if (this.d != null) {
            a(this, (MFUser) null, (Uri) null, (Boolean) null, (Pair) null, false, (MFUser) null, true, 63, (Object) null);
            MFUser mFUser = this.d;
            if (mFUser != null) {
                if (!TextUtils.isEmpty(mFUser.getProfilePicture())) {
                    MFUser mFUser2 = this.d;
                    if (mFUser2 != null) {
                        String profilePicture = mFUser2.getProfilePicture();
                        kd4.a((Object) profilePicture, "mUser!!.profilePicture");
                        if (StringsKt__StringsKt.a((CharSequence) profilePicture, (CharSequence) Utility.URL_SCHEME, false, 2, (Object) null)) {
                            MFUser mFUser3 = this.d;
                            if (mFUser3 != null) {
                                mFUser3.setProfilePicture("");
                            } else {
                                kd4.a();
                                throw null;
                            }
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                MFUser mFUser4 = this.d;
                if (mFUser4 != null) {
                    if (mFUser4.isUseDefaultBiometric() && this.f) {
                        MFUser mFUser5 = this.d;
                        if (mFUser5 != null) {
                            mFUser5.setUseDefaultBiometric(false);
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    UpdateUser updateUser = this.i;
                    MFUser mFUser6 = this.d;
                    if (mFUser6 != null) {
                        updateUser.a(new UpdateUser.b(mFUser6), new d(this));
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void c(String str) {
        kd4.b(str, "name");
        MFUser mFUser = this.d;
        if (mFUser != null) {
            mFUser.setLastName(str);
        }
        a(this, (MFUser) null, (Uri) null, Boolean.valueOf(e()), (Pair) null, false, (MFUser) null, false, 123, (Object) null);
    }

    @DexIgnore
    public final void b(String str) {
        kd4.b(str, "name");
        MFUser mFUser = this.d;
        if (mFUser != null) {
            mFUser.setFirstName(str);
        }
        a(this, (MFUser) null, (Uri) null, Boolean.valueOf(e()), (Pair) null, false, (MFUser) null, false, 123, (Object) null);
    }

    @DexIgnore
    public final void a(int i2) {
        this.f = true;
        MFUser mFUser = this.d;
        if (mFUser != null) {
            mFUser.setHeightInCentimeters(i2);
        }
        a(this, (MFUser) null, (Uri) null, Boolean.valueOf(e()), (Pair) null, false, (MFUser) null, false, 123, (Object) null);
    }

    @DexIgnore
    public final void b(int i2) {
        this.f = true;
        MFUser mFUser = this.d;
        if (mFUser != null) {
            mFUser.setWeightInGrams(i2);
        }
        a(this, (MFUser) null, (Uri) null, Boolean.valueOf(e()), (Pair) null, false, (MFUser) null, false, 123, (Object) null);
    }

    @DexIgnore
    public final void a(Gender gender) {
        kd4.b(gender, "gender");
        MFUser mFUser = this.d;
        if (mFUser != null) {
            mFUser.setGender(gender.toString());
        }
        a(this, (MFUser) null, (Uri) null, Boolean.valueOf(e()), (Pair) null, false, (MFUser) null, false, 123, (Object) null);
    }

    @DexIgnore
    public final void a(Intent intent) {
        fi4 unused = ag4.b(jc.a(this), (CoroutineContext) null, (CoroutineStart) null, new ProfileEditViewModel$onProfilePictureChanged$Anon1(this, intent, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public static /* synthetic */ void a(ProfileEditViewModel profileEditViewModel, MFUser mFUser, Uri uri, Boolean bool, Pair pair, boolean z, MFUser mFUser2, boolean z2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            mFUser = null;
        }
        if ((i2 & 2) != 0) {
            uri = null;
        }
        if ((i2 & 4) != 0) {
            bool = null;
        }
        if ((i2 & 8) != 0) {
            pair = null;
        }
        if ((i2 & 16) != 0) {
            z = false;
        }
        if ((i2 & 32) != 0) {
            mFUser2 = null;
        }
        if ((i2 & 64) != 0) {
            z2 = false;
        }
        profileEditViewModel.a(mFUser, uri, bool, pair, z, mFUser2, z2);
    }

    @DexIgnore
    public final void a(MFUser mFUser, Uri uri, Boolean bool, Pair<Integer, String> pair, boolean z, MFUser mFUser2, boolean z2) {
        this.h.a(new b(mFUser, uri, bool, pair, z, mFUser2, z2));
    }
}
