package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1$populateContact$Anon1", f = "NotificationContactsAndAppsAssignedPresenter.kt", l = {}, m = "invokeSuspend")
public final class NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1$populateContact$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $contactWrapperList;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1$populateContact$Anon1(NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1 notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1, List list, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1;
        this.$contactWrapperList = list;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1$populateContact$Anon1 notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1$populateContact$Anon1 = new NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1$populateContact$Anon1(this.this$Anon0, this.$contactWrapperList, yb4);
        notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1$populateContact$Anon1.p$ = (zg4) obj;
        return notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1$populateContact$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1$populateContact$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            for (ContactGroup next : this.this$Anon0.$responseValue.a()) {
                for (Contact next2 : next.getContacts()) {
                    if (next.getHour() == this.this$Anon0.this$Anon0.a.q) {
                        ContactWrapper contactWrapper = new ContactWrapper(next2, "");
                        contactWrapper.setAdded(true);
                        kd4.a((Object) next2, "contact");
                        ContactGroup contactGroup = next2.getContactGroup();
                        kd4.a((Object) contactGroup, "contact.contactGroup");
                        contactWrapper.setCurrentHandGroup(contactGroup.getHour());
                        Contact contact = contactWrapper.getContact();
                        if (contact != null) {
                            contact.setDbRowId(next2.getDbRowId());
                            contact.setUseSms(next2.isUseSms());
                            contact.setUseCall(next2.isUseCall());
                        }
                        List<PhoneNumber> phoneNumbers = next2.getPhoneNumbers();
                        kd4.a((Object) phoneNumbers, "contact.phoneNumbers");
                        if (!phoneNumbers.isEmpty()) {
                            PhoneNumber phoneNumber = next2.getPhoneNumbers().get(0);
                            kd4.a((Object) phoneNumber, "contact.phoneNumbers[0]");
                            String number = phoneNumber.getNumber();
                            if (!TextUtils.isEmpty(number)) {
                                contactWrapper.setHasPhoneNumber(true);
                                contactWrapper.setPhoneNumber(number);
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String a = NotificationContactsAndAppsAssignedPresenter.x.a();
                                local.d(a, "mGetAllHybridContactGroups filter selected contact, phoneNumber=" + number);
                            }
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a2 = NotificationContactsAndAppsAssignedPresenter.x.a();
                        StringBuilder sb = new StringBuilder();
                        sb.append("mGetAllHybridContactGroups filter selected contact, hand=");
                        ContactGroup contactGroup2 = next2.getContactGroup();
                        kd4.a((Object) contactGroup2, "contact.contactGroup");
                        sb.append(contactGroup2.getHour());
                        sb.append(" ,rowId=");
                        sb.append(next2.getDbRowId());
                        sb.append(" ,isUseText=");
                        sb.append(next2.isUseSms());
                        sb.append(" ,isUseCall=");
                        sb.append(next2.isUseCall());
                        local2.d(a2, sb.toString());
                        this.$contactWrapperList.add(contactWrapper);
                        Contact contact2 = contactWrapper.getContact();
                        if (contact2 == null || contact2.getContactId() != -100) {
                            Contact contact3 = contactWrapper.getContact();
                            if (contact3 != null) {
                                if (contact3.getContactId() != -200) {
                                }
                            }
                        }
                        this.this$Anon0.this$Anon0.a.r().add(contactWrapper);
                    }
                }
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
