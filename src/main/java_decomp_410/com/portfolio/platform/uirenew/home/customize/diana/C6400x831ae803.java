package com.portfolio.platform.uirenew.home.customize.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1 */
public final class C6400x831ae803 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.diana.preset.DianaPreset $currentPreset$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.diana.preset.DianaPreset $it;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$10;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public java.lang.Object L$7;
    @DexIgnore
    public java.lang.Object L$8;
    @DexIgnore
    public java.lang.Object L$9;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22612p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1$1")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1$1 */
    public static final class C64011 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super android.os.Parcelable>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting $complication;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22613p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64011(com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1, com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting dianaPresetComplicationSetting, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1;
            this.$complication = dianaPresetComplicationSetting;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803.C64011 r0 = new com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803.C64011(this.this$0, this.$complication, yb4);
            r0.f22613p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803.C64011) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter.m33743g(this.this$0.this$0).mo40972g(this.$complication.getId());
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1$2")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1$2 */
    public static final class C64022 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super android.os.Parcelable>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting $watchApp;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22614p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64022(com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1, com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1;
            this.$watchApp = dianaPresetWatchAppSetting;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803.C64022 r0 = new com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803.C64022(this.this$0, this.$watchApp, yb4);
            r0.f22614p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803.C64022) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter.m33743g(this.this$0.this$0).mo40974h(this.$watchApp.getId());
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1$a")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1$a */
    public static final class C6403a implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase.C6507d, com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase.C6505b> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803 f22615a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.ul2 f22616b;

        @DexIgnore
        public C6403a(com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1, com.fossil.blesdk.obfuscated.ul2 ul2) {
            this.f22615a = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1;
            this.f22616b = ul2;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase.C6507d dVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("DianaCustomizeEditPresenter", "setToWatch success");
            this.f22616b.mo31555a("");
            this.f22615a.this$0.mo40947l();
            this.f22615a.this$0.f22599n.mo31100m();
            this.f22615a.this$0.f22599n.mo31098g(true);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo29641a(com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase.C6505b bVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(bVar, "errorValue");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("DianaCustomizeEditPresenter", "setToWatch onError");
            this.f22615a.this$0.mo40947l();
            this.f22615a.this$0.f22599n.mo31100m();
            int b = bVar.mo41118b();
            if (b != 1101) {
                if (b == 8888) {
                    this.f22615a.this$0.f22599n.mo31094c();
                } else if (!(b == 1112 || b == 1113)) {
                    this.f22615a.this$0.f22599n.mo31103q();
                }
                java.lang.String arrayList = bVar.mo41117a().toString();
                com.fossil.blesdk.obfuscated.ul2 ul2 = this.f22616b;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList, "errorCode");
                ul2.mo31555a(arrayList);
                return;
            }
            java.util.List<com.portfolio.platform.enums.PermissionCodes> convertBLEPermissionErrorCode = com.portfolio.platform.enums.PermissionCodes.convertBLEPermissionErrorCode(bVar.mo41117a());
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            com.fossil.blesdk.obfuscated.t13 k = this.f22615a.this$0.f22599n;
            java.lang.Object[] array = convertBLEPermissionErrorCode.toArray(new com.portfolio.platform.enums.PermissionCodes[0]);
            if (array != null) {
                com.portfolio.platform.enums.PermissionCodes[] permissionCodesArr = (com.portfolio.platform.enums.PermissionCodes[]) array;
                k.mo26045a((com.portfolio.platform.enums.PermissionCodes[]) java.util.Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                java.lang.String arrayList2 = bVar.mo41117a().toString();
                com.fossil.blesdk.obfuscated.ul2 ul22 = this.f22616b;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) arrayList2, "errorCode");
                ul22.mo31555a(arrayList2);
                return;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6400x831ae803(com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter dianaCustomizeEditPresenter, com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset2) {
        super(2, yb4);
        this.$it = dianaPreset;
        this.this$0 = dianaCustomizeEditPresenter;
        this.$currentPreset$inlined = dianaPreset2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803(this.$it, yb4, this.this$0, this.$currentPreset$inlined);
        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1.f22612p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v5, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v6, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v3, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v10, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v13, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v16, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v25, resolved type: com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v125, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v36, resolved type: java.util.Iterator} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v131, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v21, resolved type: java.util.Iterator} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v135, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r19v11, resolved type: java.util.List} */
    /* JADX WARNING: Can't wrap try/catch for region: R(10:4|(3:5|6|7)|(2:150|151)|152|153|154|(7:156|157|204|135|(14:137|138|139|(8:141|142|143|144|145|146|(1:148)(7:149|150|151|152|153|154|(1:158)(0))|148)|167|168|169|170|(4:184|185|186|(9:188|189|190|191|(1:193)|196|204|135|(1:205)(0)))(2:172|(1:174)(5:176|177|178|(1:180)|181))|175|196|204|135|(0)(0))(0)|207|(5:209|(2:211|(3:215|220|221))(2:216|(3:218|220|221))|219|220|221)(4:222|(1:224)(1:225)|226|227))|158|207|(0)(0)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(14:137|(1:138)|139|(8:141|142|143|144|145|146|(1:148)(7:149|150|151|152|153|154|(1:158)(0))|148)|167|168|169|170|(4:184|185|186|(9:188|189|190|191|(1:193)|196|204|135|(1:205)(0)))(2:172|(1:174)(5:176|177|178|(1:180)|181))|175|196|204|135|(0)(0)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:53|54|55|56|(1:58)(5:59|60|61|(1:65)(0)|65)|58) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:(2:141|142)|143|144|145|146|(1:148)(7:149|150|151|152|153|154|(1:158)(0))|148) */
    /* JADX WARNING: Can't wrap try/catch for region: R(9:188|189|190|191|(1:193)|196|204|135|(1:205)(0)) */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x054f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x0554, code lost:
        r15 = r3;
        r3 = r7;
        r7 = r8;
        r8 = r11;
        r10 = r16;
        r9 = r17;
        r2 = r20;
        r17 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x0562, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x0567, code lost:
        r17 = r2;
        r9 = r21;
        r10 = r16;
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x05e3, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x05e4, code lost:
        r9 = r11;
        r1 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x05f2, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x05f3, code lost:
        r17 = r2;
        r10 = r16;
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x02d9, code lost:
        if (android.text.TextUtils.isEmpty(((com.portfolio.platform.data.model.setting.CommuteTimeSetting) r1.this$0.f22595j.mo23093a(r0.getSettings(), com.portfolio.platform.data.model.setting.CommuteTimeSetting.class)).getAddress()) != false) goto L_0x02db;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0396  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x040c  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0453  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x04a4  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x052b A[Catch:{ Exception -> 0x054f }] */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x0632  */
    /* JADX WARNING: Removed duplicated region for block: B:209:0x0659  */
    /* JADX WARNING: Removed duplicated region for block: B:222:0x06b5  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0153  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01f2  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0266 A[Catch:{ Exception -> 0x027d }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0303  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x033a  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.String str;
        com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting;
        java.lang.String str2;
        java.lang.String str3;
        com.fossil.blesdk.obfuscated.ul2 ul2;
        java.lang.String str4;
        java.lang.String str5;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting2;
        com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting dianaPresetComplicationSetting;
        com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting dianaPresetComplicationSetting2;
        java.util.List list;
        java.util.List list2;
        java.lang.Object obj2;
        com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting3;
        java.util.List list3;
        java.util.List list4;
        java.util.Iterator it;
        java.lang.String str6;
        java.lang.String str7;
        com.fossil.blesdk.obfuscated.zg4 zg42;
        com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting4;
        java.lang.String str8;
        java.lang.String str9;
        java.lang.Object obj3;
        java.lang.String str10;
        com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting5;
        java.lang.Object obj4;
        java.util.List list5;
        com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting6;
        com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting dianaPresetComplicationSetting3;
        com.fossil.blesdk.obfuscated.zg4 zg43;
        android.os.Parcelable parcelable;
        java.lang.Object obj5;
        com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1;
        com.fossil.blesdk.obfuscated.ug4 a;
        java.lang.String str11;
        java.util.List list6;
        java.lang.Object obj6;
        com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting7;
        java.lang.String str12;
        java.util.List list7;
        com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting dianaPresetComplicationSetting4;
        java.util.Iterator it2;
        com.fossil.blesdk.obfuscated.zg4 zg44;
        java.lang.Object obj7;
        java.util.List list8;
        android.os.Parcelable parcelable2;
        com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting dianaPresetComplicationSetting5;
        com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12 = this;
        java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.label;
        java.lang.String str13 = "weather";
        java.lang.String str14 = "check setting of ";
        java.lang.String str15 = "java.lang.String.format(format, *args)";
        java.lang.String str16 = "exception when parse setting from json ";
        java.lang.String str17 = "commute-time";
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg45 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.f22612p$;
            java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting> complications = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.$it.getComplications();
            list2 = new java.util.ArrayList();
            for (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting next : complications) {
                if (com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.ok2.f17359c.mo29849c(next.getId())).booleanValue()) {
                    list2.add(next);
                }
            }
            if (!list2.isEmpty()) {
                java.util.Iterator it3 = list2.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    }
                    dianaPresetComplicationSetting5 = it3.next();
                    if (!com.fossil.blesdk.obfuscated.ok2.f17359c.mo29851e(dianaPresetComplicationSetting5.getId())) {
                        break;
                    }
                }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingPermissionComp " + dianaPresetComplicationSetting5);
                if (dianaPresetComplicationSetting5 == null) {
                    com.fossil.blesdk.obfuscated.pd4 pd4 = com.fossil.blesdk.obfuscated.pd4.f17594a;
                    java.lang.String a3 = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.set_to_watch_fail_complication_permission);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a3, "LanguageHelper.getString\u2026_complication_permission)");
                    java.lang.Object[] objArr = {dianaPresetComplicationSetting5.getId(), dianaPresetComplicationSetting5.getPosition()};
                    java.lang.String format = java.lang.String.format(a3, java.util.Arrays.copyOf(objArr, objArr.length));
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) format, str15);
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22599n.mo31092a(format, dianaPresetComplicationSetting5.getId(), dianaPresetComplicationSetting5.getPosition(), true);
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting> complications2 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.$it.getComplications();
                list6 = new java.util.ArrayList();
                for (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting next2 : complications2) {
                    com.fossil.blesdk.obfuscated.zg4 zg46 = zg45;
                    if (com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.ok2.f17359c.mo29850d(next2.getId())).booleanValue()) {
                        list6.add(next2);
                    }
                    zg45 = zg46;
                }
                com.fossil.blesdk.obfuscated.zg4 zg47 = zg45;
                if (!list6.isEmpty()) {
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12 = this;
                    it2 = list6.iterator();
                    str = "";
                    dianaPresetComplicationSetting2 = dianaPresetComplicationSetting5;
                    obj6 = a2;
                    list7 = list2;
                    zg42 = zg47;
                    dianaPresetComplicationSetting4 = null;
                } else {
                    str7 = str13;
                    str = "";
                    str5 = str14;
                    str11 = str15;
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12 = this;
                    obj6 = a2;
                    dianaPresetComplicationSetting2 = dianaPresetComplicationSetting5;
                    zg42 = zg47;
                    dianaPresetComplicationSetting = null;
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    local2.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingComplication " + dianaPresetComplicationSetting);
                    if (dianaPresetComplicationSetting != null) {
                    }
                }
            }
            dianaPresetComplicationSetting5 = null;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local3.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingPermissionComp " + dianaPresetComplicationSetting5);
            if (dianaPresetComplicationSetting5 == null) {
            }
        } else if (i == 1) {
            it2 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$6;
            dianaPresetComplicationSetting4 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$4;
            list6 = (java.util.List) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$3;
            dianaPresetComplicationSetting2 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$2;
            list7 = (java.util.List) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$1;
            com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting dianaPresetComplicationSetting6 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$5;
            zg44 = (com.fossil.blesdk.obfuscated.zg4) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$0;
            try {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                str7 = str13;
                str5 = str14;
                str11 = str15;
                obj7 = obj;
                obj6 = a2;
                list8 = list7;
                dianaPresetComplicationSetting = dianaPresetComplicationSetting6;
                str = "";
            } catch (java.lang.Exception e) {
                e = e;
                str7 = str13;
                str = "";
                str5 = str14;
                str11 = str15;
                obj6 = a2;
                zg42 = zg44;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local4.mo33256e("DianaCustomizeEditPresenter", str16 + e);
                str13 = str7;
                str15 = str11;
                str14 = str5;
                if (!it2.hasNext()) {
                }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local22 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local22.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingComplication " + dianaPresetComplicationSetting);
                if (dianaPresetComplicationSetting != null) {
                }
            }
            try {
            } catch (java.lang.Exception e2) {
                e = e2;
                list7 = list8;
                zg42 = zg44;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local42 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local42.mo33256e("DianaCustomizeEditPresenter", str16 + e);
                str13 = str7;
                str15 = str11;
                str14 = str5;
                if (!it2.hasNext()) {
                }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local222.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingComplication " + dianaPresetComplicationSetting);
                if (dianaPresetComplicationSetting != null) {
                }
            }
            parcelable2 = (android.os.Parcelable) obj7;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local5.mo33255d("DianaCustomizeEditPresenter", "last setting " + parcelable2);
            if (parcelable2 == null) {
                dianaPresetComplicationSetting.setSettings(dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22595j.mo23096a((java.lang.Object) parcelable2));
                list7 = list8;
                zg42 = zg44;
                str13 = str7;
                str15 = str11;
                str14 = str5;
            }
            list2 = list8;
            zg42 = zg44;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local2222.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingComplication " + dianaPresetComplicationSetting);
            if (dianaPresetComplicationSetting != null) {
                java.lang.String id = dianaPresetComplicationSetting.getId();
                int hashCode = id.hashCode();
                if (hashCode != -829740640) {
                    if (hashCode == 134170930 && id.equals("second-timezone")) {
                        str12 = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.Customization_Complications_SecondTimezoneError_Text__ToDisplayTimezoneOnYourWatch);
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str12, "LanguageHelper.getString\u2026splayTimezoneOnYourWatch)");
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22599n.mo31093b(str12, dianaPresetComplicationSetting.getId(), dianaPresetComplicationSetting.getPosition(), true);
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                } else if (id.equals(str17)) {
                    str12 = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.Customization_Buttons_CommuteTimeError___ToDisplayCommuteTimePleaseEnter);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str12, "LanguageHelper.getString\u2026ayCommuteTimePleaseEnter)");
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22599n.mo31093b(str12, dianaPresetComplicationSetting.getId(), dianaPresetComplicationSetting.getPosition(), true);
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                str12 = str;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22599n.mo31093b(str12, dianaPresetComplicationSetting.getId(), dianaPresetComplicationSetting.getPosition(), true);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting> watchapps = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.$it.getWatchapps();
            java.util.List arrayList = new java.util.ArrayList();
            for (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting next3 : watchapps) {
                if (com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.pl2.f17631d.mo30134f(next3.getId())).booleanValue()) {
                    arrayList.add(next3);
                }
            }
            if (!arrayList.isEmpty()) {
                java.util.Iterator it4 = arrayList.iterator();
                while (true) {
                    if (!it4.hasNext()) {
                        break;
                    }
                    dianaPresetWatchAppSetting7 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) it4.next();
                    if (!com.fossil.blesdk.obfuscated.pl2.f17631d.mo30132d(dianaPresetWatchAppSetting7.getId())) {
                        break;
                    }
                }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local6.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingPermissionWatchApp " + dianaPresetWatchAppSetting7);
                if (dianaPresetWatchAppSetting7 == null) {
                    com.fossil.blesdk.obfuscated.pd4 pd42 = com.fossil.blesdk.obfuscated.pd4.f17594a;
                    java.lang.String a4 = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.set_to_watch_fail_app_permission);
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a4, "LanguageHelper.getString\u2026atch_fail_app_permission)");
                    java.lang.Object[] objArr2 = {dianaPresetWatchAppSetting7.getId(), dianaPresetWatchAppSetting7.getPosition()};
                    java.lang.String format2 = java.lang.String.format(a4, java.util.Arrays.copyOf(objArr2, objArr2.length));
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) format2, str11);
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22599n.mo31092a(format2, dianaPresetWatchAppSetting7.getId(), dianaPresetWatchAppSetting7.getPosition(), false);
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting> watchapps2 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.$it.getWatchapps();
                java.util.List arrayList2 = new java.util.ArrayList();
                java.util.Iterator<T> it5 = watchapps2.iterator();
                while (it5.hasNext()) {
                    T next4 = it5.next();
                    java.util.Iterator<T> it6 = it5;
                    if (com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.pl2.f17631d.mo30135g(((com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) next4).getId())).booleanValue()) {
                        arrayList2.add(next4);
                    }
                    it5 = it6;
                }
                if (!arrayList2.isEmpty()) {
                    java.util.Iterator it7 = arrayList2.iterator();
                    obj2 = obj6;
                    dianaPresetWatchAppSetting3 = null;
                    list3 = arrayList2;
                    list4 = list6;
                    list = arrayList;
                    dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting7;
                    it = it7;
                    if (!it.hasNext()) {
                    }
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    local7.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                    if (dianaPresetWatchAppSetting == null) {
                    }
                } else {
                    str2 = str17;
                    str3 = str7;
                    dianaPresetWatchAppSetting = null;
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local72 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    local72.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                    if (dianaPresetWatchAppSetting == null) {
                    }
                }
            }
            dianaPresetWatchAppSetting7 = null;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local62 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local62.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingPermissionWatchApp " + dianaPresetWatchAppSetting7);
            if (dianaPresetWatchAppSetting7 == null) {
            }
        } else if (i == 2) {
            it = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$10;
            com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting8 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$8;
            java.util.List list9 = (java.util.List) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$7;
            com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting9 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$6;
            list = (java.util.List) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$5;
            com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting dianaPresetComplicationSetting7 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$4;
            list4 = (java.util.List) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$3;
            com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting10 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$9;
            com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting dianaPresetComplicationSetting8 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$2;
            java.util.List list10 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg48 = (com.fossil.blesdk.obfuscated.zg4) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$0;
            try {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                obj3 = a2;
                str8 = str14;
                list5 = list9;
                list2 = list10;
                zg42 = zg48;
                str7 = str13;
                str9 = str16;
                dianaPresetComplicationSetting3 = dianaPresetComplicationSetting7;
                dianaPresetComplicationSetting2 = dianaPresetComplicationSetting8;
                obj4 = obj;
                str = "";
                dianaPresetWatchAppSetting6 = dianaPresetWatchAppSetting9;
                dianaPresetWatchAppSetting = dianaPresetWatchAppSetting10;
                str10 = str17;
                dianaPresetWatchAppSetting5 = dianaPresetWatchAppSetting8;
            } catch (java.lang.Exception e3) {
                e = e3;
                str8 = str14;
                dianaPresetWatchAppSetting3 = dianaPresetWatchAppSetting8;
                zg4 = zg48;
                obj2 = a2;
                str3 = str13;
                dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting9;
                dianaPresetComplicationSetting = dianaPresetComplicationSetting7;
                dianaPresetComplicationSetting2 = dianaPresetComplicationSetting8;
                str = "";
                list3 = list9;
                list2 = list10;
                str9 = str16;
                str2 = str17;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$13 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12;
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting4;
                str6 = str9;
                sb.append(str6);
                sb.append(e);
                local8.mo33256e("DianaCustomizeEditPresenter", sb.toString());
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$13;
                str7 = str3;
                str17 = str2;
                zg42 = zg4;
                str16 = str6;
                dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting2;
                if (!it.hasNext()) {
                }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local722 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local722.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                if (dianaPresetWatchAppSetting == null) {
                }
            }
            try {
                parcelable = (android.os.Parcelable) obj4;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
            } catch (java.lang.Exception e4) {
                e = e4;
                zg43 = zg42;
            }
            zg43 = zg42;
            sb2.append("last setting ");
            sb2.append(parcelable);
            local9.mo33255d("DianaCustomizeEditPresenter", sb2.toString());
            if (parcelable == null) {
                dianaPresetWatchAppSetting.setSettings(dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22595j.mo23096a((java.lang.Object) parcelable));
                dianaPresetComplicationSetting = dianaPresetComplicationSetting3;
                str2 = str10;
                obj2 = obj3;
                str6 = str9;
                str3 = str7;
                zg4 = zg43;
                dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting6;
                list3 = list5;
                dianaPresetWatchAppSetting3 = dianaPresetWatchAppSetting5;
                str7 = str3;
                str17 = str2;
                zg42 = zg4;
                str16 = str6;
                dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting2;
                if (!it.hasNext()) {
                    com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting11 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) it.next();
                    str9 = str16;
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    str10 = str17;
                    java.lang.StringBuilder sb3 = new java.lang.StringBuilder();
                    java.lang.Object obj8 = obj2;
                    java.lang.String str18 = str5;
                    sb3.append(str18);
                    sb3.append(dianaPresetWatchAppSetting11);
                    local10.mo33255d("DianaCustomizeEditPresenter", sb3.toString());
                    try {
                    } catch (java.lang.Exception e5) {
                        e = e5;
                        zg4 = zg42;
                        str8 = str18;
                        str2 = str10;
                        str3 = str7;
                        obj5 = obj8;
                    }
                    if (com.fossil.blesdk.obfuscated.oj2.m26116a(dianaPresetWatchAppSetting11.getSettings())) {
                        try {
                            a = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.mo31440b();
                        } catch (java.lang.Exception e6) {
                            e = e6;
                            str8 = str18;
                        }
                        str8 = str18;
                        com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803.C64022 r11 = new com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803.C64022(dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12, dianaPresetWatchAppSetting11, (com.fossil.blesdk.obfuscated.yb4) null);
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$0 = zg42;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$1 = list2;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$2 = dianaPresetComplicationSetting2;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$3 = list4;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$4 = dianaPresetComplicationSetting;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$5 = list;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$6 = dianaPresetWatchAppSetting4;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$7 = list3;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$8 = dianaPresetWatchAppSetting3;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$9 = dianaPresetWatchAppSetting11;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$10 = it;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.label = 2;
                        obj4 = com.fossil.blesdk.obfuscated.yf4.m30997a(a, r11, dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12);
                        java.lang.Object obj9 = obj8;
                        if (obj4 == obj9) {
                            return obj9;
                        }
                        obj3 = obj9;
                        dianaPresetWatchAppSetting5 = dianaPresetWatchAppSetting3;
                        list5 = list3;
                        dianaPresetWatchAppSetting6 = dianaPresetWatchAppSetting4;
                        dianaPresetComplicationSetting3 = dianaPresetComplicationSetting;
                        dianaPresetWatchAppSetting = dianaPresetWatchAppSetting11;
                        parcelable = (android.os.Parcelable) obj4;
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local92 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.StringBuilder sb22 = new java.lang.StringBuilder();
                        zg43 = zg42;
                        sb22.append("last setting ");
                        sb22.append(parcelable);
                        local92.mo33255d("DianaCustomizeEditPresenter", sb22.toString());
                        if (parcelable == null) {
                            str2 = str10;
                            str3 = str7;
                        }
                        return obj9;
                    }
                    str8 = str18;
                    obj5 = obj8;
                    java.lang.String id2 = dianaPresetWatchAppSetting11.getId();
                    int hashCode2 = id2.hashCode();
                    zg4 = zg42;
                    if (hashCode2 == -829740640) {
                        str3 = str7;
                        str2 = str10;
                        try {
                        } catch (java.lang.Exception e7) {
                            e = e7;
                            com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$14 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12;
                            obj2 = obj5;
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local82 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$132 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12;
                            java.lang.StringBuilder sb4 = new java.lang.StringBuilder();
                            dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting4;
                            str6 = str9;
                            sb4.append(str6);
                            sb4.append(e);
                            local82.mo33256e("DianaCustomizeEditPresenter", sb4.toString());
                            dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$132;
                            str7 = str3;
                            str17 = str2;
                            zg42 = zg4;
                            str16 = str6;
                            dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting2;
                            if (!it.hasNext()) {
                            }
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local7222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            local7222.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                            if (dianaPresetWatchAppSetting == null) {
                            }
                        }
                        if (id2.equals(str2)) {
                            dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12;
                            com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting dianaPresetWatchAppSetting12 = dianaPresetWatchAppSetting11;
                            if (((com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22595j.mo23093a(dianaPresetWatchAppSetting11.getSettings(), com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting.class)).getAddresses().isEmpty()) {
                                dianaPresetWatchAppSetting = dianaPresetWatchAppSetting12;
                                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1;
                            }
                            obj2 = obj5;
                            dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1;
                            dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting4;
                            str6 = str9;
                            str7 = str3;
                            str17 = str2;
                            zg42 = zg4;
                            str16 = str6;
                            dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting2;
                            if (!it.hasNext()) {
                                com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$15 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12;
                                str2 = str17;
                                str3 = str7;
                                dianaPresetWatchAppSetting = dianaPresetWatchAppSetting3;
                            }
                        }
                    } else if (hashCode2 != 1223440372) {
                        str2 = str10;
                        str3 = str7;
                    } else {
                        str3 = str7;
                        try {
                        } catch (java.lang.Exception e8) {
                            e = e8;
                            obj2 = obj5;
                            str2 = str10;
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local822 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1322 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12;
                            java.lang.StringBuilder sb42 = new java.lang.StringBuilder();
                            dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting4;
                            str6 = str9;
                            sb42.append(str6);
                            sb42.append(e);
                            local822.mo33256e("DianaCustomizeEditPresenter", sb42.toString());
                            dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1322;
                            str7 = str3;
                            str17 = str2;
                            zg42 = zg4;
                            str16 = str6;
                            dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting2;
                            if (!it.hasNext()) {
                            }
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local72222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            local72222.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                            if (dianaPresetWatchAppSetting == null) {
                            }
                        }
                        if (id2.equals(str3)) {
                            com.portfolio.platform.data.model.setting.WeatherWatchAppSetting weatherWatchAppSetting = (com.portfolio.platform.data.model.setting.WeatherWatchAppSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22595j.mo23093a(dianaPresetWatchAppSetting11.getSettings(), com.portfolio.platform.data.model.setting.WeatherWatchAppSetting.class);
                        }
                        str2 = str10;
                    }
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12;
                    obj2 = obj5;
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$1;
                    dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting4;
                    str6 = str9;
                    str7 = str3;
                    str17 = str2;
                    zg42 = zg4;
                    str16 = str6;
                    dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting2;
                    if (!it.hasNext()) {
                    }
                }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local722222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local722222.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                if (dianaPresetWatchAppSetting == null) {
                    java.lang.String id3 = dianaPresetWatchAppSetting.getId();
                    int hashCode3 = id3.hashCode();
                    if (hashCode3 != -829740640) {
                        if (hashCode3 == 1223440372 && id3.equals(str3)) {
                            str4 = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.set_to_watch_fail_weather_app_not_configured);
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str4, "LanguageHelper.getString\u2026ather_app_not_configured)");
                            dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22599n.mo31093b(str4, dianaPresetWatchAppSetting.getId(), dianaPresetWatchAppSetting.getPosition(), false);
                            return com.fossil.blesdk.obfuscated.qa4.f17909a;
                        }
                    } else if (id3.equals(str2)) {
                        str4 = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.Customization_Buttons_CommuteTimeError___ToDisplayCommuteTimePleaseEnter);
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str4, "LanguageHelper.getString\u2026ayCommuteTimePleaseEnter)");
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22599n.mo31093b(str4, dianaPresetWatchAppSetting.getId(), dianaPresetWatchAppSetting.getPosition(), false);
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    str4 = str;
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22599n.mo31093b(str4, dianaPresetWatchAppSetting.getId(), dianaPresetWatchAppSetting.getPosition(), false);
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22599n.mo31099l();
                if (dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.mo40946k() == 1) {
                    ul2 = com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39514b("set_complication");
                } else {
                    ul2 = com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39514b("set_watch_apps");
                }
                ul2.mo31559d();
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22602q.mo34435a(new com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase.C6506c(dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.$currentPreset$inlined), new com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803.C6403a(dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12, ul2));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            str2 = str10;
            str3 = str7;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local7222222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local7222222.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
            if (dianaPresetWatchAppSetting == null) {
            }
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (!it2.hasNext()) {
            com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting dianaPresetComplicationSetting9 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) it2.next();
            str7 = str13;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            str11 = str15;
            local11.mo33255d("DianaCustomizeEditPresenter", str14 + dianaPresetComplicationSetting9);
            try {
            } catch (java.lang.Exception e9) {
                e = e9;
                str5 = str14;
            }
            if (com.fossil.blesdk.obfuscated.oj2.m26116a(dianaPresetComplicationSetting9.getSettings())) {
                com.fossil.blesdk.obfuscated.ug4 a5 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.mo31440b();
                str5 = str14;
                com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803.C64011 r9 = new com.portfolio.platform.uirenew.home.customize.diana.C6400x831ae803.C64011(dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12, dianaPresetComplicationSetting9, (com.fossil.blesdk.obfuscated.yb4) null);
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$0 = zg42;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$1 = list7;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$2 = dianaPresetComplicationSetting2;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$3 = list6;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$4 = dianaPresetComplicationSetting4;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$5 = dianaPresetComplicationSetting9;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.L$6 = it2;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.label = 1;
                obj7 = com.fossil.blesdk.obfuscated.yf4.m30997a(a5, r9, dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12);
                if (obj7 == obj6) {
                    return obj6;
                }
                zg44 = zg42;
                list8 = list7;
                dianaPresetComplicationSetting = dianaPresetComplicationSetting9;
                parcelable2 = (android.os.Parcelable) obj7;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local52 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local52.mo33255d("DianaCustomizeEditPresenter", "last setting " + parcelable2);
                if (parcelable2 == null) {
                    list2 = list8;
                    zg42 = zg44;
                }
                list2 = list8;
                zg42 = zg44;
                return obj6;
            }
            str5 = str14;
            java.lang.String id4 = dianaPresetComplicationSetting9.getId();
            int hashCode4 = id4.hashCode();
            if (hashCode4 == -829740640) {
                if (id4.equals(str17)) {
                }
                str13 = str7;
                str15 = str11;
                str14 = str5;
                if (!it2.hasNext()) {
                    str7 = str13;
                    str5 = str14;
                    str11 = str15;
                    java.util.List list11 = list7;
                    dianaPresetComplicationSetting = dianaPresetComplicationSetting4;
                    list2 = list11;
                }
            } else {
                if (hashCode4 == 134170930) {
                    if (id4.equals("second-timezone") && android.text.TextUtils.isEmpty(((com.portfolio.platform.data.model.setting.SecondTimezoneSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$12.this$0.f22595j.mo23093a(dianaPresetComplicationSetting9.getSettings(), com.portfolio.platform.data.model.setting.SecondTimezoneSetting.class)).getTimeZoneId())) {
                    }
                }
                str13 = str7;
                str15 = str11;
                str14 = str5;
                if (!it2.hasNext()) {
                }
            }
            list2 = list7;
            dianaPresetComplicationSetting = dianaPresetComplicationSetting9;
            e = e;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local422 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local422.mo33256e("DianaCustomizeEditPresenter", str16 + e);
            str13 = str7;
            str15 = str11;
            str14 = str5;
            if (!it2.hasNext()) {
            }
        }
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local22222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local22222.mo33255d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingComplication " + dianaPresetComplicationSetting);
        if (dianaPresetComplicationSetting != null) {
        }
    }
}
