package com.portfolio.platform.uirenew.home.profile;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.util.DeviceUtils;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1", f = "HomeProfilePresenter.kt", l = {185}, m = "invokeSuspend")
public final class HomeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Boolean>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeSerial;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1(String str, yb4 yb4) {
        super(2, yb4);
        this.$activeSerial = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1 homeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1 = new HomeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1(this.$activeSerial, yb4);
        homeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1.p$ = (zg4) obj;
        return homeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            DeviceUtils a2 = DeviceUtils.g.a();
            String str = this.$activeSerial;
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = a2.a(str, (Device) null, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
