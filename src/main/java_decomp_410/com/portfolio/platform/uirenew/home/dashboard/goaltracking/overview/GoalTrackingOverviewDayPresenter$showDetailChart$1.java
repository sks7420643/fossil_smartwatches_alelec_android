package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1", mo27670f = "GoalTrackingOverviewDayPresenter.kt", mo27671l = {99, 102}, mo27672m = "invokeSuspend")
public final class GoalTrackingOverviewDayPresenter$showDetailChart$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23345p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingOverviewDayPresenter$showDetailChart$1(com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = goalTrackingOverviewDayPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1 goalTrackingOverviewDayPresenter$showDetailChart$1 = new com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1(this.this$0, yb4);
        goalTrackingOverviewDayPresenter$showDetailChart$1.f23345p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return goalTrackingOverviewDayPresenter$showDetailChart$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00c6  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        kotlin.Pair pair;
        java.util.ArrayList arrayList;
        com.fossil.blesdk.obfuscated.os3 os3;
        int i;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i2 = this.label;
        if (i2 == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f23345p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1$pair$1 goalTrackingOverviewDayPresenter$showDetailChart$1$pair$1 = new com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1$pair$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, goalTrackingOverviewDayPresenter$showDetailChart$1$pair$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i2 == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i2 == 2) {
            arrayList = (java.util.ArrayList) this.L$2;
            pair = (kotlin.Pair) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.lang.Integer num = (java.lang.Integer) obj;
            os3 = (com.fossil.blesdk.obfuscated.os3) this.this$0.f23338i.mo2275a();
            if (os3 != null) {
                com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary goalTrackingSummary = (com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary) os3.mo29975d();
                if (goalTrackingSummary != null) {
                    java.lang.Integer a3 = com.fossil.blesdk.obfuscated.dc4.m20843a(goalTrackingSummary.getGoalTarget() / 16);
                    if (a3 != null) {
                        i = a3.intValue();
                        this.this$0.f23340k.mo27365a(new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c(java.lang.Math.max(num != null ? num.intValue() : 0, i / 16), i, arrayList), (java.util.ArrayList) pair.getSecond());
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                }
            }
            i = 8;
            this.this$0.f23340k.mo27365a(new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c(java.lang.Math.max(num != null ? num.intValue() : 0, i / 16), i, arrayList), (java.util.ArrayList) pair.getSecond());
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        kotlin.Pair pair2 = (kotlin.Pair) obj;
        java.util.ArrayList arrayList2 = (java.util.ArrayList) pair2.getFirst();
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d("GoalTrackingOverviewDayPresenter", "showDetailChart - data=" + arrayList2);
        com.fossil.blesdk.obfuscated.ug4 a4 = this.this$0.mo31440b();
        com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1$maxValue$1 goalTrackingOverviewDayPresenter$showDetailChart$1$maxValue$1 = new com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1$maxValue$1(arrayList2, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.L$1 = pair2;
        this.L$2 = arrayList2;
        this.label = 2;
        java.lang.Object a5 = com.fossil.blesdk.obfuscated.yf4.m30997a(a4, goalTrackingOverviewDayPresenter$showDetailChart$1$maxValue$1, this);
        if (a5 == a) {
            return a;
        }
        arrayList = arrayList2;
        java.lang.Object obj2 = a5;
        pair = pair2;
        obj = obj2;
        java.lang.Integer num2 = (java.lang.Integer) obj;
        os3 = (com.fossil.blesdk.obfuscated.os3) this.this$0.f23338i.mo2275a();
        if (os3 != null) {
        }
        i = 8;
        this.this$0.f23340k.mo27365a(new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c(java.lang.Math.max(num2 != null ? num2.intValue() : 0, i / 16), i, arrayList), (java.util.ArrayList) pair.getSecond());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
