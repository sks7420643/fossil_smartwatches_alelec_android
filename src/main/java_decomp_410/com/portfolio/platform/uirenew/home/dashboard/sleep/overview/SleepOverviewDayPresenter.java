package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import android.graphics.RectF;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.dg3;
import com.fossil.blesdk.obfuscated.ed3;
import com.fossil.blesdk.obfuscated.fd3;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gd3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.xk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.enums.Status;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepOverviewDayPresenter extends ed3 {
    @DexIgnore
    public Date f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public LiveData<os3<List<MFSleepDay>>> i; // = new MutableLiveData();
    @DexIgnore
    public LiveData<os3<List<MFSleepSession>>> j; // = new MutableLiveData();
    @DexIgnore
    public /* final */ fd3 k;
    @DexIgnore
    public /* final */ SleepSummariesRepository l;
    @DexIgnore
    public /* final */ SleepSessionsRepository m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<os3<? extends List<MFSleepDay>>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewDayPresenter a;

        @DexIgnore
        public b(SleepOverviewDayPresenter sleepOverviewDayPresenter) {
            this.a = sleepOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(os3<? extends List<MFSleepDay>> os3) {
            Status a2 = os3.a();
            List list = (List) os3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mSleepSummaries -- sleepSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("SleepOverviewDayPresenter", sb.toString());
            if (a2 != Status.DATABASE_LOADING) {
                this.a.g = true;
                if (this.a.g && this.a.h) {
                    fi4 unused = this.a.j();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements cc<os3<? extends List<MFSleepSession>>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewDayPresenter a;

        @DexIgnore
        public c(SleepOverviewDayPresenter sleepOverviewDayPresenter) {
            this.a = sleepOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(os3<? extends List<MFSleepSession>> os3) {
            Status a2 = os3.a();
            List list = (List) os3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mSleepSessions -- sleepSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            sb.append('\"');
            local.d("SleepOverviewDayPresenter", sb.toString());
            if (a2 != Status.DATABASE_LOADING) {
                this.a.h = true;
                if (this.a.g && this.a.h) {
                    fi4 unused = this.a.j();
                }
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public SleepOverviewDayPresenter(fd3 fd3, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        kd4.b(fd3, "mView");
        kd4.b(sleepSummariesRepository, "mSummariesRepository");
        kd4.b(sleepSessionsRepository, "mSessionsRepository");
        this.k = fd3;
        this.l = sleepSummariesRepository;
        this.m = sleepSessionsRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Date b(SleepOverviewDayPresenter sleepOverviewDayPresenter) {
        Date date = sleepOverviewDayPresenter.f;
        if (date != null) {
            return date;
        }
        kd4.d("mDate");
        throw null;
    }

    @DexIgnore
    public void h() {
        Date date = this.f;
        if (date != null) {
            if (date == null) {
                kd4.d("mDate");
                throw null;
            } else if (rk2.s(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.f;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("SleepOverviewDayPresenter", sb.toString());
                    return;
                }
                kd4.d("mDate");
                throw null;
            }
        }
        this.g = false;
        this.h = false;
        this.f = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.f;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("SleepOverviewDayPresenter", sb2.toString());
            SleepSummariesRepository sleepSummariesRepository = this.l;
            Date date4 = this.f;
            if (date4 == null) {
                kd4.d("mDate");
                throw null;
            } else if (date4 != null) {
                this.i = sleepSummariesRepository.getSleepSummaries(date4, date4, false);
                SleepSessionsRepository sleepSessionsRepository = this.m;
                Date date5 = this.f;
                if (date5 == null) {
                    kd4.d("mDate");
                    throw null;
                } else if (date5 != null) {
                    this.j = sleepSessionsRepository.getSleepSessionList(date5, date5, false);
                } else {
                    kd4.d("mDate");
                    throw null;
                }
            } else {
                kd4.d("mDate");
                throw null;
            }
        } else {
            kd4.d("mDate");
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        this.k.a(this);
    }

    @DexIgnore
    public final fi4 j() {
        return ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SleepOverviewDayPresenter$showDetailChart$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        h();
        LiveData<os3<List<MFSleepDay>>> liveData = this.i;
        fd3 fd3 = this.k;
        if (fd3 != null) {
            liveData.a((gd3) fd3, new b(this));
            this.j.a((LifecycleOwner) this.k, new c(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayPresenter", "stop");
        try {
            LiveData<os3<List<MFSleepDay>>> liveData = this.i;
            fd3 fd3 = this.k;
            if (fd3 != null) {
                liveData.a((LifecycleOwner) (gd3) fd3);
                this.j.a((LifecycleOwner) this.k);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayFragment");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayPresenter", "stop - e=" + e);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: com.portfolio.platform.data.model.room.sleep.MFSleepDay} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: com.portfolio.platform.data.model.room.sleep.MFSleepDay} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: com.portfolio.platform.data.model.room.sleep.MFSleepDay} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v10, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v8, resolved type: com.portfolio.platform.data.model.room.sleep.MFSleepDay} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: com.portfolio.platform.data.model.room.sleep.MFSleepDay} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final List<dg3.b> a(Date date, List<MFSleepSession> list) {
        ArrayList arrayList;
        ArrayList arrayList2 = new ArrayList();
        for (MFSleepSession next : list) {
            if (rk2.a(Long.valueOf(date.getTime()), Long.valueOf(next.getDate()))) {
                arrayList2.add(next);
            }
        }
        os3 a2 = this.i.a();
        MFSleepDay mFSleepDay = null;
        if (a2 != null) {
            List list2 = (List) a2.d();
            if (list2 != null) {
                Iterator it = list2.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    Object next2 = it.next();
                    if (rk2.d(date, next2.getDate())) {
                        mFSleepDay = next2;
                        break;
                    }
                }
                mFSleepDay = mFSleepDay;
            }
        }
        int a3 = xk2.d.a(mFSleepDay);
        ArrayList arrayList3 = new ArrayList();
        Iterator it2 = arrayList2.iterator();
        while (it2.hasNext()) {
            MFSleepSession mFSleepSession = (MFSleepSession) it2.next();
            BarChart.c cVar = new BarChart.c(0, 0, (ArrayList) null, 7, (fd4) null);
            ArrayList arrayList4 = new ArrayList();
            SleepDistribution realSleepStateDistInMinute = mFSleepSession.getRealSleepStateDistInMinute();
            List<WrapperSleepStateChange> sleepStateChange = mFSleepSession.getSleepStateChange();
            ArrayList arrayList5 = new ArrayList();
            int realStartTime = mFSleepSession.getRealStartTime();
            int totalMinuteBySleepDistribution = realSleepStateDistInMinute.getTotalMinuteBySleepDistribution();
            if (sleepStateChange != null) {
                for (WrapperSleepStateChange next3 : sleepStateChange) {
                    BarChart.b bVar = new BarChart.b(0, (BarChart.State) null, 0, 0, (RectF) null, 31, (fd4) null);
                    MFSleepSession mFSleepSession2 = mFSleepSession;
                    bVar.a((int) next3.index);
                    bVar.c(realStartTime);
                    bVar.b(totalMinuteBySleepDistribution);
                    int i2 = next3.state;
                    if (i2 == 0) {
                        bVar.a(BarChart.State.LOWEST);
                    } else if (i2 == 1) {
                        bVar.a(BarChart.State.DEFAULT);
                    } else if (i2 == 2) {
                        bVar.a(BarChart.State.HIGHEST);
                    }
                    arrayList5.add(bVar);
                    mFSleepSession = mFSleepSession2;
                }
            }
            MFSleepSession mFSleepSession3 = mFSleepSession;
            arrayList4.add(arrayList5);
            if (arrayList4.size() != 0) {
                arrayList = arrayList4;
            } else {
                arrayList = cb4.a((T[]) new ArrayList[]{cb4.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.State) null, 0, 0, (RectF) null, 23, (fd4) null)})});
            }
            ArrayList<BarChart.a> a4 = cVar.a();
            BarChart.a aVar = r4;
            Iterator it3 = it2;
            BarChart.a aVar2 = new BarChart.a(a3, arrayList, 0, false, 12, (fd4) null);
            a4.add(aVar);
            cVar.b(a3);
            cVar.a(a3);
            int awake = realSleepStateDistInMinute.getAwake();
            int light = realSleepStateDistInMinute.getLight();
            int deep = realSleepStateDistInMinute.getDeep();
            if (totalMinuteBySleepDistribution > 0) {
                float f2 = (float) totalMinuteBySleepDistribution;
                float f3 = ((float) awake) / f2;
                float f4 = ((float) light) / f2;
                arrayList3.add(new dg3.b(cVar, f3, f4, ((float) 1) - (f3 + f4), awake, light, deep, mFSleepSession3.getTimezoneOffset()));
            }
            it2 = it3;
        }
        return arrayList3;
    }
}
