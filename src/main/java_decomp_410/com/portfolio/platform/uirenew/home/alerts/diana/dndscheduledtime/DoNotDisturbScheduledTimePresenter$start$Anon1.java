package com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.sy2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$start$Anon1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {35}, m = "invokeSuspend")
public final class DoNotDisturbScheduledTimePresenter$start$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DoNotDisturbScheduledTimePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DoNotDisturbScheduledTimePresenter$start$Anon1(DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = doNotDisturbScheduledTimePresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DoNotDisturbScheduledTimePresenter$start$Anon1 doNotDisturbScheduledTimePresenter$start$Anon1 = new DoNotDisturbScheduledTimePresenter$start$Anon1(this.this$Anon0, yb4);
        doNotDisturbScheduledTimePresenter$start$Anon1.p$ = (zg4) obj;
        return doNotDisturbScheduledTimePresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DoNotDisturbScheduledTimePresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 a2 = this.this$Anon0.c();
            DoNotDisturbScheduledTimePresenter$start$Anon1$listDNDScheduledTimeModel$Anon1 doNotDisturbScheduledTimePresenter$start$Anon1$listDNDScheduledTimeModel$Anon1 = new DoNotDisturbScheduledTimePresenter$start$Anon1$listDNDScheduledTimeModel$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a2, doNotDisturbScheduledTimePresenter$start$Anon1$listDNDScheduledTimeModel$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        for (DNDScheduledTimeModel dNDScheduledTimeModel : (List) obj) {
            if (dNDScheduledTimeModel.getScheduledTimeType() == this.this$Anon0.g) {
                sy2 g = this.this$Anon0.j;
                DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter = this.this$Anon0;
                g.d(doNotDisturbScheduledTimePresenter.b(doNotDisturbScheduledTimePresenter.g));
                this.this$Anon0.j.a(dNDScheduledTimeModel.getMinutes());
            }
            if (dNDScheduledTimeModel.getScheduledTimeType() == 0) {
                this.this$Anon0.h = dNDScheduledTimeModel;
            } else {
                this.this$Anon0.i = dNDScheduledTimeModel;
            }
        }
        return qa4.a;
    }
}
