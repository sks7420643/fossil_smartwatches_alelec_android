package com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fs3;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.t03;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$Anon1;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1", f = "NotificationHybridContactPresenter.kt", l = {100}, m = "invokeSuspend")
public final class NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ t03.d $successResponse;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationHybridContactPresenter$start$Anon1.Anon2 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1(NotificationHybridContactPresenter$start$Anon1.Anon2 anon2, t03.d dVar, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = anon2;
        this.$successResponse = dVar;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1 notificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1 = new NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1(this.this$Anon0, this.$successResponse, yb4);
        notificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1.p$ = (zg4) obj;
        return notificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            gh4 a2 = ag4.a(zg4, this.this$Anon0.a.this$Anon0.b(), (CoroutineStart) null, new NotificationHybridContactPresenter$start$Anon1$Anon2$onSuccess$Anon1$populateContacts$Anon1(this, (yb4) null), 2, (Object) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = a2;
            this.label = 1;
            if (a2.a(this) == a) {
                return a;
            }
        } else if (i == 1) {
            gh4 gh4 = (gh4) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (!this.this$Anon0.a.this$Anon0.i.isEmpty()) {
            for (ContactWrapper add : this.this$Anon0.a.this$Anon0.i) {
                this.this$Anon0.a.this$Anon0.j().add(add);
            }
        }
        this.this$Anon0.a.this$Anon0.g.a(this.this$Anon0.a.this$Anon0.j(), fs3.a.a(), this.this$Anon0.a.this$Anon0.h);
        this.this$Anon0.a.this$Anon0.j.a(0, new Bundle(), this.this$Anon0.a.this$Anon0);
        return qa4.a;
    }
}
