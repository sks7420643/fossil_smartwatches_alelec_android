package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$Anon1", f = "NotificationContactsPresenter.kt", l = {49}, m = "invokeSuspend")
public final class NotificationContactsPresenter$start$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationContactsPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$Anon1$Anon1", f = "NotificationContactsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;

        @DexIgnore
        public Anon1(yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                PortfolioApp.W.c().J();
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 implements CoroutineUseCase.e<px2.d, px2.b> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsPresenter$start$Anon1 a;

        @DexIgnore
        public Anon2(NotificationContactsPresenter$start$Anon1 notificationContactsPresenter$start$Anon1) {
            this.a = notificationContactsPresenter$start$Anon1;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(px2.d dVar) {
            kd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.n.a(), "GetAllContactGroup onSuccess");
            fi4 unused = ag4.b(this.a.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationContactsPresenter$start$Anon1$Anon2$onSuccess$Anon1(this, dVar, (yb4) null), 3, (Object) null);
        }

        @DexIgnore
        public void a(px2.b bVar) {
            kd4.b(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.n.a(), "GetAllContactGroup onError");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationContactsPresenter$start$Anon1(NotificationContactsPresenter notificationContactsPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = notificationContactsPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NotificationContactsPresenter$start$Anon1 notificationContactsPresenter$start$Anon1 = new NotificationContactsPresenter$start$Anon1(this.this$Anon0, yb4);
        notificationContactsPresenter$start$Anon1.p$ = (zg4) obj;
        return notificationContactsPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationContactsPresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            if (!PortfolioApp.W.c().u().N()) {
                ug4 a2 = this.this$Anon0.b();
                Anon1 anon1 = new Anon1((yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                if (yf4.a(a2, anon1, this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (this.this$Anon0.j().isEmpty()) {
            this.this$Anon0.j.a(null, new Anon2(this));
        }
        return qa4.a;
    }
}
