package com.portfolio.platform.uirenew.home.profile.goal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$checkUserUsingDefaultGoal$1$invokeSuspend$$inlined$let$lambda$1 */
public final class C6762x30490a84 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qo2<com.portfolio.platform.data.model.MFUser>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.MFUser $it;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23871p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$checkUserUsingDefaultGoal$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6762x30490a84(com.portfolio.platform.data.model.MFUser mFUser, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$checkUserUsingDefaultGoal$1 profileGoalEditPresenter$checkUserUsingDefaultGoal$1) {
        super(2, yb4);
        this.$it = mFUser;
        this.this$0 = profileGoalEditPresenter$checkUserUsingDefaultGoal$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.profile.goal.C6762x30490a84 profileGoalEditPresenter$checkUserUsingDefaultGoal$1$invokeSuspend$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.profile.goal.C6762x30490a84(this.$it, yb4, this.this$0);
        profileGoalEditPresenter$checkUserUsingDefaultGoal$1$invokeSuspend$$inlined$let$lambda$1.f23871p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return profileGoalEditPresenter$checkUserUsingDefaultGoal$1$invokeSuspend$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.profile.goal.C6762x30490a84) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23871p$;
            com.portfolio.platform.data.source.UserRepository h = this.this$0.this$0.f23865t;
            com.portfolio.platform.data.model.MFUser mFUser = this.$it;
            this.L$0 = zg4;
            this.label = 1;
            obj = h.updateUser(mFUser, true, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
