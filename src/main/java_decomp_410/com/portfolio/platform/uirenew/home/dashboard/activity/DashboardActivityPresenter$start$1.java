package com.portfolio.platform.uirenew.home.dashboard.activity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$start$1", mo27670f = "DashboardActivityPresenter.kt", mo27671l = {48}, mo27672m = "invokeSuspend")
public final class DashboardActivityPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23193p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardActivityPresenter$start$1(com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter dashboardActivityPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = dashboardActivityPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$start$1 dashboardActivityPresenter$start$1 = new com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$start$1(this.this$0, yb4);
        dashboardActivityPresenter$start$1.f23193p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dashboardActivityPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23193p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$start$1$currentUser$1 dashboardActivityPresenter$start$1$currentUser$1 = new com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$start$1$currentUser$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, dashboardActivityPresenter$start$1$currentUser$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) obj;
        if (mFUser != null) {
            if (this.this$0.f23181h == null) {
                this.this$0.f23181h = mFUser.getDistanceUnit();
            } else {
                com.portfolio.platform.enums.Unit distanceUnit = mFUser.getDistanceUnit();
                if (!(distanceUnit == null || this.this$0.f23181h == distanceUnit)) {
                    this.this$0.f23181h = distanceUnit;
                    this.this$0.f23182i.mo25667b(distanceUnit);
                }
            }
        }
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("DashboardActivityPresenter", com.facebook.share.internal.VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!com.fossil.blesdk.obfuscated.rk2.m27414s(this.this$0.f23179f).booleanValue()) {
            this.this$0.f23179f = new java.util.Date();
            com.portfolio.platform.data.Listing b = this.this$0.f23180g;
            if (b != null) {
                b.getRefresh();
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
