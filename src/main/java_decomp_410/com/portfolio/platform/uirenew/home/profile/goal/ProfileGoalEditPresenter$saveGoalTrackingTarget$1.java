package com.portfolio.platform.uirenew.home.profile.goal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1", mo27670f = "ProfileGoalEditPresenter.kt", mo27671l = {58}, mo27672m = "invokeSuspend")
public final class ProfileGoalEditPresenter$saveGoalTrackingTarget$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23874p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1$1", mo27670f = "ProfileGoalEditPresenter.kt", mo27671l = {59}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1$1 */
    public static final class C67641 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23875p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1$1$a")
        /* renamed from: com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1$1$a */
        public static final class C6765a implements com.portfolio.platform.data.source.GoalTrackingRepository.UpdateGoalSettingCallback {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1.C67641 f23876a;

            @DexIgnore
            public C6765a(com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1.C67641 r1) {
                this.f23876a = r1;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:3:0x001d, code lost:
                if (r3 != null) goto L_0x0022;
             */
            @DexIgnore
            public void onFail(com.fossil.blesdk.obfuscated.po2<com.portfolio.platform.data.model.GoalSetting> po2) {
                java.lang.String str;
                com.fossil.blesdk.obfuscated.kd4.m24411b(po2, "error");
                com.fossil.blesdk.obfuscated.vh3 m = this.f23876a.this$0.this$0.mo41511m();
                int a = po2.mo30176a();
                com.portfolio.platform.data.model.ServerError c = po2.mo30178c();
                if (c != null) {
                    str = c.getMessage();
                }
                str = "";
                m.mo31810d(a, str);
                this.f23876a.this$0.this$0.mo41513o();
            }

            @DexIgnore
            public void onSuccess(com.fossil.blesdk.obfuscated.ro2<com.portfolio.platform.data.model.GoalSetting> ro2) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(ro2, "success");
                this.f23876a.this$0.this$0.mo41515q();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C67641(com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1 profileGoalEditPresenter$saveGoalTrackingTarget$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = profileGoalEditPresenter$saveGoalTrackingTarget$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1.C67641 r0 = new com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1.C67641(this.this$0, yb4);
            r0.f23875p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1.C67641) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23875p$;
                com.portfolio.platform.data.source.GoalTrackingRepository c = this.this$0.this$0.f23864s;
                com.portfolio.platform.data.model.GoalSetting goalSetting = new com.portfolio.platform.data.model.GoalSetting(this.this$0.this$0.f23857l);
                com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1.C67641.C6765a aVar = new com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1.C67641.C6765a(this);
                this.L$0 = zg4;
                this.label = 1;
                if (c.updateGoalSetting(goalSetting, aVar, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileGoalEditPresenter$saveGoalTrackingTarget$1(com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter profileGoalEditPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = profileGoalEditPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1 profileGoalEditPresenter$saveGoalTrackingTarget$1 = new com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1(this.this$0, yb4);
        profileGoalEditPresenter$saveGoalTrackingTarget$1.f23874p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return profileGoalEditPresenter$saveGoalTrackingTarget$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23874p$;
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1.C67641 r3 = new com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1.C67641(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
