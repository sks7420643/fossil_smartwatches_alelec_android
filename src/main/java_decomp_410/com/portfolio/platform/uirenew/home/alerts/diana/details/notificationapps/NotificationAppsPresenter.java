package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps;

import android.content.Context;
import android.content.Intent;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.bn2;
import com.fossil.blesdk.obfuscated.bw2;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cw2;
import com.fossil.blesdk.obfuscated.dw2;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.iw2;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ls3;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.vy2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationAppsPresenter extends bw2 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ a u; // = new a((fd4) null);
    @DexIgnore
    public boolean f; // = true;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public List<InstalledApp> h; // = new ArrayList();
    @DexIgnore
    public List<AppWrapper> i; // = new ArrayList();
    @DexIgnore
    public List<AppNotificationFilter> j; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> k; // = new ArrayList();
    @DexIgnore
    public /* final */ b l; // = new b();
    @DexIgnore
    public /* final */ cw2 m;
    @DexIgnore
    public /* final */ j62 n;
    @DexIgnore
    public /* final */ vy2 o;
    @DexIgnore
    public /* final */ px2 p;
    @DexIgnore
    public /* final */ iw2 q;
    @DexIgnore
    public /* final */ en2 r;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase s;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationAppsPresenter.t;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements BleCommandResultManager.b {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public final class a implements i62.d<iw2.c, i62.a> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(iw2.c cVar) {
                FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.u.a(), ".Inside mSaveAppsNotification onSuccess");
                NotificationAppsPresenter.this.m.a();
                NotificationAppsPresenter.this.m.close();
            }

            @DexIgnore
            public void a(i62.a aVar) {
                FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.u.a(), ".Inside mSaveAppsNotification onError");
                NotificationAppsPresenter.this.m.a();
                NotificationAppsPresenter.this.m.close();
            }
        }

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            kd4.b(communicateMode, "communicateMode");
            kd4.b(intent, "intent");
            FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.u.a(), "SetNotificationFilterReceiver");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationAppsPresenter.u.a();
            local.d(a2, "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
            if (communicateMode != CommunicateMode.SET_NOTIFICATION_FILTERS) {
                return;
            }
            if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.u.a(), "onReceive - success");
                NotificationAppsPresenter.this.r.a(NotificationAppsPresenter.this.k());
                NotificationAppsPresenter.this.n.a(NotificationAppsPresenter.this.q, new iw2.b(NotificationAppsPresenter.this.l()), new a(this));
                return;
            }
            FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.u.a(), "onReceive - failed");
            NotificationAppsPresenter.this.r.a(NotificationAppsPresenter.this.j());
            int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
            if (integerArrayListExtra == null) {
                integerArrayListExtra = new ArrayList<>(intExtra2);
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = NotificationAppsPresenter.u.a();
            local2.d(a3, "permissionErrorCodes=" + integerArrayListExtra + " , size=" + integerArrayListExtra.size());
            int size = integerArrayListExtra.size();
            for (int i = 0; i < size; i++) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = NotificationAppsPresenter.u.a();
                local3.d(a4, "error code " + i + " =" + integerArrayListExtra.get(i));
            }
            if (intExtra2 != 1101) {
                if (intExtra2 == 1924) {
                    NotificationAppsPresenter.this.m.j();
                    return;
                } else if (intExtra2 == 8888) {
                    NotificationAppsPresenter.this.m.c();
                    return;
                } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                    return;
                }
            }
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(integerArrayListExtra);
            kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
            cw2 l = NotificationAppsPresenter.this.m;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                l.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    /*
    static {
        String simpleName = NotificationAppsPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationAppsPresenter::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public NotificationAppsPresenter(cw2 cw2, j62 j62, vy2 vy2, px2 px2, iw2 iw2, en2 en2, NotificationSettingsDatabase notificationSettingsDatabase) {
        kd4.b(cw2, "mView");
        kd4.b(j62, "mUseCaseHandler");
        kd4.b(vy2, "mGetApps");
        kd4.b(px2, "mGetAllContactGroup");
        kd4.b(iw2, "mSaveAppsNotification");
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.m = cw2;
        this.n = j62;
        this.o = vy2;
        this.p = px2;
        this.q = iw2;
        this.r = en2;
        this.s = notificationSettingsDatabase;
    }

    @DexIgnore
    public final boolean m() {
        throw null;
        // T t2;
        // ArrayList<InstalledApp> arrayList = new ArrayList<>();
        // for (AppWrapper installedApp : this.i) {
        //     InstalledApp installedApp2 = installedApp.getInstalledApp();
        //     if (installedApp2 != null) {
        //         Boolean isSelected = installedApp2.isSelected();
        //         kd4.a((Object) isSelected, "it.isSelected");
        //         if (isSelected.booleanValue()) {
        //             arrayList.add(installedApp2);
        //         }
        //     }
        // }
        // if (arrayList.size() != this.h.size()) {
        //     return true;
        // }
        // for (InstalledApp installedApp3 : arrayList) {
        //     Iterator<T> it = this.h.iterator();
        //     while (true) {
        //         if (!it.hasNext()) {
        //             t2 = null;
        //             break;
        //         }
        //         t2 = it.next();
        //         if (kd4.a((Object) ((InstalledApp) t2).getIdentifier(), (Object) installedApp3.getIdentifier())) {
        //             break;
        //         }
        //     }
        //     InstalledApp installedApp4 = (InstalledApp) t2;
        //     if (installedApp4 != null) {
        //         if (!kd4.a((Object) installedApp4.isSelected(), (Object) installedApp3.isSelected())) {
        //         }
        //     }
        //     return true;
        // }
        // return false;
    }

    @DexIgnore
    public final void n() {
        FLogger.INSTANCE.getLocal().d(t, "registerBroadcastReceiver");
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.l, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public void o() {
        this.m.a(this);
    }

    @DexIgnore
    public final void p() {
        FLogger.INSTANCE.getLocal().d(t, "unregisterBroadcastReceiver");
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.l, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public final boolean b(boolean z) {
        int i2;
        int i3;
        List<AppWrapper> list = this.i;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            i2 = 0;
            for (AppWrapper installedApp : list) {
                InstalledApp installedApp2 = installedApp.getInstalledApp();
                Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
                if (isSelected != null) {
                    if (isSelected.booleanValue() == z) {
                        i2++;
                        if (i2 < 0) {
                            cb4.b();
                            throw null;
                        }
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
        } else {
            i2 = 0;
        }
        List<AppWrapper> list2 = this.i;
        if (!(list2 instanceof Collection) || !list2.isEmpty()) {
            i3 = 0;
            for (AppWrapper uri : list2) {
                if (uri.getUri() != null) {
                    i3++;
                    if (i3 < 0) {
                        cb4.b();
                        throw null;
                    }
                }
            }
        } else {
            i3 = 0;
        }
        if (i2 == i3) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void c(boolean z) {
        for (AppWrapper next : this.i) {
            if (next.getUri() != null) {
                InstalledApp installedApp = next.getInstalledApp();
                if (installedApp != null) {
                    installedApp.setSelected(z);
                }
            }
        }
    }

    @DexIgnore
    public void f() {
        throw null;
        // FLogger.INSTANCE.getLocal().d(t, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        // n();
        // this.m.b();
        // this.f = this.r.A();
        // this.g = this.r.A();
        // fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationAppsPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(t, "stop");
        p();
        this.m.a();
    }

    @DexIgnore
    public void h() {
        throw null;
        // if (!m()) {
        //     FLogger.INSTANCE.getLocal().d(t, "closeAndSave, nothing changed");
        //     this.m.close();
        //     return;
        // }
        // bn2 bn2 = bn2.d;
        // cw2 cw2 = this.m;
        // if (cw2 == null) {
        //     throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
        // } else if (bn2.a(bn2, ((dw2) cw2).getContext(), "SET_NOTIFICATION", false, 4, (Object) null)) {
        //     FLogger.INSTANCE.getLocal().d(t, "setRuleToDevice, showProgressDialog");
        //     this.m.b();
        //     long currentTimeMillis = System.currentTimeMillis();
        //     ILocalFLogger local = FLogger.INSTANCE.getLocal();
        //     String str = t;
        //     local.d(str, "filter notification, time start=" + currentTimeMillis);
        //     ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        //     String str2 = t;
        //     local2.d(str2, "mListAppWrapper.size = " + this.i.size());
        //     this.j.addAll(ls3.a(this.i, false, 1, (Object) null));
        //     long b2 = PortfolioApp.W.c().b(new AppNotificationFilterSettings(this.j, System.currentTimeMillis()), PortfolioApp.W.c().e());
        //     ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        //     String str3 = t;
        //     local3.d(str3, "filter notification, time end= " + b2);
        //     ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        //     String str4 = t;
        //     local4.d(str4, "delayTime =" + (b2 - currentTimeMillis) + " mili seconds");
        // }
    }

    @DexIgnore
    public void i() {
        this.m.a();
        this.m.close();
    }

    @DexIgnore
    public final boolean j() {
        return this.g;
    }

    @DexIgnore
    public final boolean k() {
        return this.f;
    }

    @DexIgnore
    public final List<AppWrapper> l() {
        return this.i;
    }

    @DexIgnore
    public final String a(int i2) {
        if (i2 == 0) {
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__Everyone);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return a2;
        } else if (i2 != 1) {
            String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__NoOne);
            kd4.a((Object) a3, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return a3;
        } else {
            String a4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__FavoriteContacts);
            kd4.a((Object) a4, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return a4;
        }
    }

    @DexIgnore
    public void a(boolean z) {
        throw null;
        // ILocalFLogger local = FLogger.INSTANCE.getLocal();
        // String str = t;
        // local.d(str, "setEnableAllAppsToggle: enable = " + z);
        // this.f = z;
        // boolean z2 = true;
        // if (!z && b(true)) {
        //     c(false);
        // } else if (z) {
        //     c(true);
        // } else {
        //     z2 = false;
        // }
        // if (z2) {
        //     this.m.g(this.i);
        // }
        // if (this.f) {
        //     bn2 bn2 = bn2.d;
        //     cw2 cw2 = this.m;
        //     if (cw2 != null) {
        //         bn2.a(bn2, ((dw2) cw2).getContext(), "NOTIFICATION_APPS", false, 4, (Object) null);
        //         return;
        //     }
        //     throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
        // }
    }

    @DexIgnore
    public void a(AppWrapper appWrapper, boolean z) {
        throw null;
        // T t2;
        // kd4.b(appWrapper, "appWrapper");
        // ILocalFLogger local = FLogger.INSTANCE.getLocal();
        // String str = t;
        // StringBuilder sb = new StringBuilder();
        // sb.append("setAppState: appName = ");
        // InstalledApp installedApp = appWrapper.getInstalledApp();
        // sb.append(installedApp != null ? installedApp.getTitle() : null);
        // sb.append(", selected = ");
        // sb.append(z);
        // local.d(str, sb.toString());
        // Iterator<T> it = this.i.iterator();
        // while (true) {
        //     if (!it.hasNext()) {
        //         t2 = null;
        //         break;
        //     }
        //     t2 = it.next();
        //     if (kd4.a((Object) ((AppWrapper) t2).getUri(), (Object) appWrapper.getUri())) {
        //         break;
        //     }
        // }
        // AppWrapper appWrapper2 = (AppWrapper) t2;
        // if (appWrapper2 != null) {
        //     InstalledApp installedApp2 = appWrapper2.getInstalledApp();
        //     if (installedApp2 != null) {
        //         installedApp2.setSelected(z);
        //     }
        // }
        // boolean z2 = false;
        // if (b(z) && this.f != z) {
        //     this.f = z;
        //     this.m.h(z);
        // } else if (!z && this.f) {
        //     this.f = false;
        //     this.m.h(false);
        // }
        // if (!this.f) {
        //     List<AppWrapper> list = this.i;
        //     if (!(list instanceof Collection) || !list.isEmpty()) {
        //         Iterator<T> it2 = list.iterator();
        //         while (true) {
        //             if (!it2.hasNext()) {
        //                 break;
        //             }
        //             InstalledApp installedApp3 = ((AppWrapper) it2.next()).getInstalledApp();
        //             if (installedApp3 != null) {
        //                 Boolean isSelected = installedApp3.isSelected();
        //                 kd4.a((Object) isSelected, "it.installedApp!!.isSelected");
        //                 if (isSelected.booleanValue()) {
        //                     z2 = true;
        //                     break;
        //                 }
        //             } else {
        //                 kd4.a();
        //                 throw null;
        //             }
        //         }
        //     }
        //     if (!z2) {
        //         return;
        //     }
        // }
        // cw2 cw2 = this.m;
        // if (cw2 != null) {
        //     Context context = ((dw2) cw2).getContext();
        //     if (context != null) {
        //         bn2.a(bn2.d, context, "NOTIFICATION_APPS", false, 4, (Object) null);
        //         return;
        //     }
        //     return;
        // }
        // throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
    }
}
