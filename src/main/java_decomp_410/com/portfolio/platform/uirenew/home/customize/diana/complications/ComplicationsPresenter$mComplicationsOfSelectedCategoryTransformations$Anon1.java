package com.portfolio.platform.uirenew.home.customize.diana.complications;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ ComplicationsPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1$Anon1", f = "ComplicationsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $complicationByCategories;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $currentPreset;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList $filteredComplicationByCategories;
        @DexIgnore
        public /* final */ /* synthetic */ String $selectedComplicationId;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1 complicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1, DianaPreset dianaPreset, List list, String str, ArrayList arrayList, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = complicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1;
            this.$currentPreset = dianaPreset;
            this.$complicationByCategories = list;
            this.$selectedComplicationId = str;
            this.$filteredComplicationByCategories = arrayList;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$currentPreset, this.$complicationByCategories, this.$selectedComplicationId, this.$filteredComplicationByCategories, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            T t;
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                if (this.$currentPreset != null) {
                    for (Complication complication : this.$complicationByCategories) {
                        Iterator<T> it = this.$currentPreset.getComplications().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it.next();
                            DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t;
                            boolean z = true;
                            if (!kd4.a((Object) dianaPresetComplicationSetting.getId(), (Object) complication.getComplicationId()) || !(!kd4.a((Object) dianaPresetComplicationSetting.getId(), (Object) this.$selectedComplicationId))) {
                                z = false;
                            }
                            if (dc4.a(z).booleanValue()) {
                                break;
                            }
                        }
                        if (((DianaPresetComplicationSetting) t) == null || kd4.a((Object) complication.getComplicationId(), (Object) "empty")) {
                            this.$filteredComplicationByCategories.add(complication);
                        }
                    }
                }
                this.this$Anon0.a.i.a(this.$filteredComplicationByCategories);
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$Anon1(ComplicationsPresenter complicationsPresenter) {
        this.a = complicationsPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final MutableLiveData<List<Complication>> apply(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String l = ComplicationsPresenter.w;
        local.d(l, "transform from category to list complication with category=" + str);
        DianaCustomizeViewModel g = ComplicationsPresenter.g(this.a);
        kd4.a((Object) str, "category");
        List<Complication> b = g.b(str);
        ArrayList arrayList = new ArrayList();
        DianaPreset a2 = ComplicationsPresenter.g(this.a).c().a();
        Complication a3 = ComplicationsPresenter.g(this.a).f().a();
        if (a3 != null) {
            String complicationId = a3.getComplicationId();
            fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, a2, b, complicationId, arrayList, (yb4) null), 3, (Object) null);
            return this.a.i;
        }
        kd4.a();
        throw null;
    }
}
