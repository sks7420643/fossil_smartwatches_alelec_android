package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.w52;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.CoroutineUseCase;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1", f = "NotificationCallsAndMessagesPresenter.kt", l = {310}, m = "invokeSuspend")
public final class NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<? extends AppNotificationFilter>>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1(NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1 notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1 notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1 = new NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1(this.this$Anon0, yb4);
        notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.p$ = (zg4) obj;
        return notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v50, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: java.util.List} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1 notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1 = this;
        Object a = cc4.a();
        int i = notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.label;
        List list = null;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.p$;
            px2 e = notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.this$Anon0.this$Anon0.v;
            notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.L$Anon0 = zg4;
            notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.L$Anon1 = null;
            notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.label = 1;
            obj2 = w52.a(e, null, notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1);
            if (obj2 == a) {
                return a;
            }
        } else if (i == 1) {
            list = notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.L$Anon1;
            zg4 zg42 = (zg4) notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.L$Anon0;
            na4.a(obj);
            obj2 = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        CoroutineUseCase.c cVar = (CoroutineUseCase.c) obj2;
        if (cVar instanceof px2.d) {
            list = new ArrayList();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationCallsAndMessagesPresenter.C.a();
            StringBuilder sb = new StringBuilder();
            sb.append("GetAllContactGroup onSuccess, size = ");
            px2.d dVar = (px2.d) cVar;
            sb.append(dVar.a().size());
            local.d(a2, sb.toString());
            notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.this$Anon0.this$Anon0.h = kb4.d(dVar.a());
            if (notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.this$Anon0.this$Anon0.t.E() == 0) {
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), "call everyone");
                DianaNotificationObj.AApplicationName aApplicationName = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                FNotification fNotification = r8;
                FNotification fNotification2 = new FNotification(aApplicationName.getAppName(), aApplicationName.getBundleCrc(), aApplicationName.getGroupId(), aApplicationName.getPackageName(), aApplicationName.getIconResourceId(), aApplicationName.getIconFwPath(), aApplicationName.getNotificationType());
                list.add(new AppNotificationFilter(fNotification));
            } else if (notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.this$Anon0.this$Anon0.t.E() == 1) {
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), "call favorite");
                int size = notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.this$Anon0.this$Anon0.h.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ContactGroup contactGroup = (ContactGroup) notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.this$Anon0.this$Anon0.h.get(i2);
                    DianaNotificationObj.AApplicationName aApplicationName2 = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                    FNotification fNotification3 = r10;
                    FNotification fNotification4 = new FNotification(aApplicationName2.getAppName(), aApplicationName2.getBundleCrc(), aApplicationName2.getGroupId(), aApplicationName2.getPackageName(), aApplicationName2.getIconResourceId(), aApplicationName2.getIconFwPath(), aApplicationName2.getNotificationType());
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification3);
                    List<Contact> contacts = contactGroup.getContacts();
                    kd4.a((Object) contacts, "item.contacts");
                    if (!contacts.isEmpty()) {
                        Contact contact = contactGroup.getContacts().get(0);
                        kd4.a((Object) contact, "item.contacts[0]");
                        appNotificationFilter.setSender(contact.getDisplayName());
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a3 = NotificationCallsAndMessagesPresenter.C.a();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("getTypeAllowFromCall - item ");
                        sb2.append(i2);
                        sb2.append(" name = ");
                        Contact contact2 = contactGroup.getContacts().get(0);
                        kd4.a((Object) contact2, "item.contacts[0]");
                        sb2.append(contact2.getDisplayName());
                        local2.d(a3, sb2.toString());
                        list.add(appNotificationFilter);
                    }
                }
            } else {
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), "call no one");
            }
            if (notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.this$Anon0.this$Anon0.t.J() == 0) {
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), "message everyone");
                DianaNotificationObj.AApplicationName aApplicationName3 = DianaNotificationObj.AApplicationName.MESSAGES;
                list.add(new AppNotificationFilter(new FNotification(aApplicationName3.getAppName(), aApplicationName3.getBundleCrc(), aApplicationName3.getGroupId(), aApplicationName3.getPackageName(), aApplicationName3.getIconResourceId(), aApplicationName3.getIconFwPath(), aApplicationName3.getNotificationType())));
            } else if (notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.this$Anon0.this$Anon0.t.J() == 1) {
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), "message favorite");
                int size2 = notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.this$Anon0.this$Anon0.h.size();
                int i3 = 0;
                while (i3 < size2) {
                    ContactGroup contactGroup2 = (ContactGroup) notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1.this$Anon0.this$Anon0.h.get(i3);
                    DianaNotificationObj.AApplicationName aApplicationName4 = DianaNotificationObj.AApplicationName.MESSAGES;
                    FNotification fNotification5 = r9;
                    FNotification fNotification6 = new FNotification(aApplicationName4.getAppName(), aApplicationName4.getBundleCrc(), aApplicationName4.getGroupId(), aApplicationName4.getPackageName(), aApplicationName4.getIconResourceId(), aApplicationName4.getIconFwPath(), aApplicationName4.getNotificationType());
                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification5);
                    List<Contact> contacts2 = contactGroup2.getContacts();
                    kd4.a((Object) contacts2, "item.contacts");
                    if (!contacts2.isEmpty()) {
                        Contact contact3 = contactGroup2.getContacts().get(0);
                        kd4.a((Object) contact3, "item.contacts[0]");
                        appNotificationFilter2.setSender(contact3.getDisplayName());
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String a4 = NotificationCallsAndMessagesPresenter.C.a();
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("getTypeAllowFromMessage - item ");
                        sb3.append(i3);
                        sb3.append(" name = ");
                        Contact contact4 = contactGroup2.getContacts().get(0);
                        kd4.a((Object) contact4, "item.contacts[0]");
                        sb3.append(contact4.getDisplayName());
                        local3.d(a4, sb3.toString());
                        list.add(appNotificationFilter2);
                    }
                    i3++;
                    notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1 = this;
                }
            } else {
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), "message no one");
            }
        } else if (cVar instanceof px2.b) {
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), "GetAllContactGroup onError");
        }
        return list;
    }
}
