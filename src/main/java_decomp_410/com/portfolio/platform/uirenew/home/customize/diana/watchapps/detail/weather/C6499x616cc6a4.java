package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$addLocation$1$$special$$inlined$let$lambda$1 */
public final class C6499x616cc6a4 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.google.android.gms.maps.model.LatLng $it;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22896p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$addLocation$1 this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$addLocation$1$$special$$inlined$let$lambda$1$1")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$addLocation$1$$special$$inlined$let$lambda$1$1 */
    public static final class C65001 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22897p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6499x616cc6a4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C65001(com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6499x616cc6a4 weatherSettingPresenter$addLocation$1$$special$$inlined$let$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = weatherSettingPresenter$addLocation$1$$special$$inlined$let$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6499x616cc6a4.C65001 r0 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6499x616cc6a4.C65001(this.this$0, yb4);
            r0.f22897p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6499x616cc6a4.C65001) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.f22899a.f22894i.mo25623o(this.this$0.this$0.f22899a.f22893h);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6499x616cc6a4(com.google.android.gms.maps.model.LatLng latLng, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$addLocation$1 weatherSettingPresenter$addLocation$1) {
        super(2, yb4);
        this.$it = latLng;
        this.this$0 = weatherSettingPresenter$addLocation$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6499x616cc6a4 weatherSettingPresenter$addLocation$1$$special$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6499x616cc6a4(this.$it, yb4, this.this$0);
        weatherSettingPresenter$addLocation$1$$special$$inlined$let$lambda$1.f22896p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return weatherSettingPresenter$addLocation$1$$special$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6499x616cc6a4) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22896p$;
            java.lang.String a2 = com.fossil.blesdk.obfuscated.ts3.m28439a(this.this$0.f22900b);
            java.util.List b = this.this$0.f22899a.f22893h;
            java.lang.String str = this.this$0.f22901c;
            com.google.android.gms.maps.model.LatLng latLng = this.$it;
            double d = latLng.f12162e;
            double d2 = latLng.f12163f;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a2, "name");
            com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper weatherLocationWrapper = r5;
            com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper weatherLocationWrapper2 = new com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper(str, d, d2, a2, this.this$0.f22900b, false, true, 32, (com.fossil.blesdk.obfuscated.fd4) null);
            b.add(weatherLocationWrapper);
            com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
            com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6499x616cc6a4.C65001 r5 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6499x616cc6a4.C65001(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = a2;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r5, this) == a) {
                return a;
            }
        } else if (i == 1) {
            java.lang.String str2 = (java.lang.String) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
