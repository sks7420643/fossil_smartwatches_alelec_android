package com.portfolio.platform.uirenew.home.customize.diana.complications;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$start$1", mo27670f = "ComplicationsPresenter.kt", mo27671l = {212}, mo27672m = "invokeSuspend")
public final class ComplicationsPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22735p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ComplicationsPresenter$start$1(com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter complicationsPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = complicationsPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$start$1 complicationsPresenter$start$1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$start$1(this.this$0, yb4);
        complicationsPresenter$start$1.f22735p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return complicationsPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22735p$;
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$start$1$allCategory$1 complicationsPresenter$start$1$allCategory$1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$start$1$allCategory$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b, complicationsPresenter$start$1$allCategory$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        java.util.ArrayList arrayList = new java.util.ArrayList();
        for (com.portfolio.platform.data.model.Category category : (java.util.List) obj) {
            if (!com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter.m33908g(this.this$0).mo40963b(category.getId()).isEmpty()) {
                arrayList.add(category);
            }
        }
        this.this$0.f22714m.addAll(arrayList);
        this.this$0.f22722u.mo28228a(this.this$0.f22714m);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
