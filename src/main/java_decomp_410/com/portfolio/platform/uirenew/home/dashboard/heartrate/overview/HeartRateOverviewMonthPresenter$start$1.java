package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateOverviewMonthPresenter$start$1<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>>> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter f23441a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1$1", mo27670f = "HeartRateOverviewMonthPresenter.kt", mo27671l = {63}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1$1 */
    public static final class C66511 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $data;
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.os3 $it;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public java.lang.Object L$1;
        @DexIgnore
        public java.lang.Object L$2;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23442p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1$1$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1$1$1", mo27670f = "HeartRateOverviewMonthPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1$1$1 */
        public static final class C66521 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ java.util.Calendar $calendar;
            @DexIgnore
            public /* final */ /* synthetic */ java.util.TreeMap $map;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f23443p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1.C66511 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C66521(com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1.C66511 r1, java.util.Calendar calendar, java.util.TreeMap treeMap, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$calendar = calendar;
                this.$map = treeMap;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1.C66511.C66521 r0 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1.C66511.C66521(this.this$0, this.$calendar, this.$map, yb4);
                r0.f23443p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1.C66511.C66521) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                com.fossil.blesdk.obfuscated.cc4.m20546a();
                if (this.label == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    for (com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary dailyHeartRateSummary : (java.util.List) this.this$0.$it.mo29975d()) {
                        java.util.Calendar calendar = this.$calendar;
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) calendar, "calendar");
                        calendar.setTime(dailyHeartRateSummary.getDate());
                        int i = 0;
                        this.$calendar.set(14, 0);
                        java.util.TreeMap treeMap = this.$map;
                        java.util.Calendar calendar2 = this.$calendar;
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) calendar2, "calendar");
                        java.lang.Long a = com.fossil.blesdk.obfuscated.dc4.m20844a(calendar2.getTimeInMillis());
                        com.portfolio.platform.data.model.diana.heartrate.Resting resting = dailyHeartRateSummary.getResting();
                        if (resting != null) {
                            java.lang.Integer a2 = com.fossil.blesdk.obfuscated.dc4.m20843a(resting.getValue());
                            if (a2 != null) {
                                i = a2.intValue();
                            }
                        }
                        treeMap.put(a, com.fossil.blesdk.obfuscated.dc4.m20843a(i));
                    }
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C66511(com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1 heartRateOverviewMonthPresenter$start$1, java.util.List list, com.fossil.blesdk.obfuscated.os3 os3, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = heartRateOverviewMonthPresenter$start$1;
            this.$data = list;
            this.$it = os3;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1.C66511 r0 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1.C66511(this.this$0, this.$data, this.$it, yb4);
            r0.f23442p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1.C66511) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.util.TreeMap treeMap;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23442p$;
                this.this$0.f23441a.f23433l = this.$data;
                java.util.TreeMap treeMap2 = new java.util.TreeMap();
                java.util.Calendar instance = java.util.Calendar.getInstance();
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.f23441a.mo31440b();
                com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1.C66511.C66521 r5 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1.C66511.C66521(this, instance, treeMap2, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = treeMap2;
                this.L$2 = instance;
                this.label = 1;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r5, this) == a) {
                    return a;
                }
                treeMap = treeMap2;
            } else if (i == 1) {
                java.util.Calendar calendar = (java.util.Calendar) this.L$2;
                treeMap = (java.util.TreeMap) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.f23441a.f23435n.mo30106a(treeMap);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public HeartRateOverviewMonthPresenter$start$1(com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
        this.f23441a = heartRateOverviewMonthPresenter;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>> os3) {
        java.lang.Integer num;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("mDateTransformations - status=");
        sb.append(os3 != null ? os3.mo29978f() : null);
        sb.append(" -- data.size=");
        if (os3 != null) {
            java.util.List list = (java.util.List) os3.mo29975d();
            if (list != null) {
                num = java.lang.Integer.valueOf(list.size());
                sb.append(num);
                local.mo33255d("HeartRateOverviewMonthPresenter", sb.toString());
                if ((os3 == null ? os3.mo29978f() : null) == com.portfolio.platform.enums.Status.DATABASE_LOADING) {
                    java.util.List list2 = os3 != null ? (java.util.List) os3.mo29975d() : null;
                    if (list2 != null && (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.f23441a.f23433l, (java.lang.Object) list2))) {
                        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23441a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1.C66511(this, list2, os3, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                        return;
                    }
                    return;
                }
                return;
            }
        }
        num = null;
        sb.append(num);
        local.mo33255d("HeartRateOverviewMonthPresenter", sb.toString());
        if ((os3 == null ? os3.mo29978f() : null) == com.portfolio.platform.enums.Status.DATABASE_LOADING) {
        }
    }
}
