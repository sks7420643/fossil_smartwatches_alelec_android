package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.d23;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import java.util.Arrays;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1 implements CoroutineUseCase.e<SetDianaPresetToWatchUseCase.d, SetDianaPresetToWatchUseCase.b> {
    @DexIgnore
    public /* final */ /* synthetic */ DianaPreset a;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0138Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0138Anon1(Anon1 anon1, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0138Anon1 anon1 = new C0138Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0138Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 zg4 = this.p$;
                    DianaPresetRepository j = this.this$Anon0.this$Anon0.b.w;
                    DianaPreset dianaPreset = this.this$Anon0.this$Anon0.a;
                    if (dianaPreset != null) {
                        String id = dianaPreset.getId();
                        this.L$Anon0 = zg4;
                        this.label = 1;
                        if (j.deletePresetById(id, this) == a) {
                            return a;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else if (i == 1) {
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return qa4.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HomeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1 homeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = homeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("set new preset to watch success, delete current active ");
                DianaPreset dianaPreset = this.this$Anon0.a;
                sb.append(dianaPreset != null ? dianaPreset.getName() : null);
                local.d("HomeDianaCustomizePresenter", sb.toString());
                ug4 d = this.this$Anon0.b.c();
                C0138Anon1 anon1 = new C0138Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                if (yf4.a(d, anon1, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$Anon0.b.t.m();
            this.this$Anon0.b.t.c(this.this$Anon0.b.k() - 1);
            return qa4.a;
        }
    }

    @DexIgnore
    public HomeDianaCustomizePresenter$deleteActivePreset$$inlined$let$lambda$Anon1(DianaPreset dianaPreset, HomeDianaCustomizePresenter homeDianaCustomizePresenter, String str) {
        this.a = dianaPreset;
        this.b = homeDianaCustomizePresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(SetDianaPresetToWatchUseCase.d dVar) {
        kd4.b(dVar, "responseValue");
        fi4 unused = ag4.b(this.b.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(SetDianaPresetToWatchUseCase.b bVar) {
        kd4.b(bVar, "errorValue");
        this.b.t.m();
        int b2 = bVar.b();
        if (b2 == 1101 || b2 == 1112 || b2 == 1113) {
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(bVar.a());
            kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            d23 r = this.b.t;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                r.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        this.b.t.j();
    }
}
