package com.portfolio.platform.uirenew.home.details.goaltracking;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.eg3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Pair<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDetailPresenter$showDetailChart$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(yb4 yb4, GoalTrackingDetailPresenter$showDetailChart$Anon1 goalTrackingDetailPresenter$showDetailChart$Anon1) {
        super(2, yb4);
        this.this$Anon0 = goalTrackingDetailPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        GoalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 goalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 = new GoalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(yb4, this.this$Anon0);
        goalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1.p$ = (zg4) obj;
        return goalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return eg3.a.a(this.this$Anon0.this$Anon0.g, this.this$Anon0.this$Anon0.n);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
