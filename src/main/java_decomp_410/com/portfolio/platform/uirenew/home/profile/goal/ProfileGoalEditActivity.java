package com.portfolio.platform.uirenew.home.profile.goal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.wh3;
import com.fossil.blesdk.obfuscated.yh3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ProfileGoalEditActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a D; // = new a((fd4) null);
    @DexIgnore
    public wh3 B;
    @DexIgnore
    public ProfileGoalEditPresenter C;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            kd4.b(context, "context");
            Intent intent = new Intent(context, ProfileGoalEditActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        wh3 wh3 = this.B;
        if (wh3 != null) {
            wh3.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        this.B = (wh3) getSupportFragmentManager().a((int) R.id.content);
        if (this.B == null) {
            this.B = wh3.p.b();
            wh3 wh3 = this.B;
            if (wh3 != null) {
                a((Fragment) wh3, wh3.p.a(), (int) R.id.content);
            } else {
                kd4.a();
                throw null;
            }
        }
        l42 g = PortfolioApp.W.c().g();
        wh3 wh32 = this.B;
        if (wh32 != null) {
            g.a(new yh3(wh32)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditContract.View");
    }
}
