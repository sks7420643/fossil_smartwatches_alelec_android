package com.portfolio.platform.uirenew.home.dashboard.goaltracking;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ab3;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.obfuscated.ya3;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.za3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DashboardGoalTrackingPresenter extends ya3 implements PagingRequestHelper.a {
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public Listing<GoalTrackingSummary> g;
    @DexIgnore
    public /* final */ za3 h;
    @DexIgnore
    public /* final */ GoalTrackingRepository i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ GoalTrackingDao k;
    @DexIgnore
    public /* final */ GoalTrackingDatabase l;
    @DexIgnore
    public /* final */ h42 m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public DashboardGoalTrackingPresenter(za3 za3, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, h42 h42) {
        kd4.b(za3, "mView");
        kd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(goalTrackingDao, "mGoalTrackingDao");
        kd4.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        kd4.b(h42, "mAppExecutors");
        this.h = za3;
        this.i = goalTrackingRepository;
        this.j = userRepository;
        this.k = goalTrackingDao;
        this.l = goalTrackingDatabase;
        this.m = h42;
    }

    @DexIgnore
    public void h() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DashboardGoalTrackingPresenter$initDataSource$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        try {
            Listing<GoalTrackingSummary> listing = this.g;
            if (listing != null) {
                LiveData<qd<GoalTrackingSummary>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    za3 za3 = this.h;
                    if (za3 != null) {
                        pagedList.a((LifecycleOwner) (ab3) za3);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment");
                    }
                }
            }
            this.i.removePagingListener();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(qa4.a);
            local.e("DashboardGoalTrackingPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void j() {
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", "retry all failed request");
        Listing<GoalTrackingSummary> listing = this.g;
        if (listing != null) {
            wc4<qa4> retry = listing.getRetry();
            if (retry != null) {
                qa4 invoke = retry.invoke();
            }
        }
    }

    @DexIgnore
    public final za3 k() {
        return this.h;
    }

    @DexIgnore
    public void l() {
        this.h.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!rk2.s(this.f).booleanValue()) {
            this.f = new Date();
            Listing<GoalTrackingSummary> listing = this.g;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", "stop");
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        kd4.b(eVar, "report");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardGoalTrackingPresenter", "onStatusChange status=" + eVar);
        if (eVar.a()) {
            this.h.f();
        }
    }
}
