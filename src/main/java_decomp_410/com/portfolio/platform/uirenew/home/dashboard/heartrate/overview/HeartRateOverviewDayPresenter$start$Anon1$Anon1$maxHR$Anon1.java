package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$Anon1;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$Anon1$Anon1$maxHR$Anon1", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
public final class HeartRateOverviewDayPresenter$start$Anon1$Anon1$maxHR$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Integer>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateOverviewDayPresenter$start$Anon1.Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateOverviewDayPresenter$start$Anon1$Anon1$maxHR$Anon1(HeartRateOverviewDayPresenter$start$Anon1.Anon1 anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HeartRateOverviewDayPresenter$start$Anon1$Anon1$maxHR$Anon1 heartRateOverviewDayPresenter$start$Anon1$Anon1$maxHR$Anon1 = new HeartRateOverviewDayPresenter$start$Anon1$Anon1$maxHR$Anon1(this.this$Anon0, yb4);
        heartRateOverviewDayPresenter$start$Anon1$Anon1$maxHR$Anon1.p$ = (zg4) obj;
        return heartRateOverviewDayPresenter$start$Anon1$Anon1$maxHR$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HeartRateOverviewDayPresenter$start$Anon1$Anon1$maxHR$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        int i;
        Object obj2;
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            List list = this.this$Anon0.$data;
            if (list != null) {
                Iterator it = list.iterator();
                if (!it.hasNext()) {
                    obj2 = null;
                } else {
                    obj2 = it.next();
                    if (it.hasNext()) {
                        Integer a = dc4.a(((HeartRateSample) obj2).getMax());
                        do {
                            Object next = it.next();
                            Integer a2 = dc4.a(((HeartRateSample) next).getMax());
                            if (a.compareTo(a2) < 0) {
                                obj2 = next;
                                a = a2;
                            }
                        } while (it.hasNext());
                    }
                }
                HeartRateSample heartRateSample = (HeartRateSample) obj2;
                if (heartRateSample != null) {
                    Integer a3 = dc4.a(heartRateSample.getMax());
                    if (a3 != null) {
                        i = a3.intValue();
                        return dc4.a(i);
                    }
                }
            }
            i = 0;
            return dc4.a(i);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
