package com.portfolio.platform.uirenew.home.customize.diana.complications.search;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1", mo27670f = "ComplicationSearchPresenter.kt", mo27671l = {75}, mo27672m = "invokeSuspend")
public final class ComplicationSearchPresenter$search$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $query;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $results;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22805p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1$1", mo27670f = "ComplicationSearchPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1$1 */
    public static final class C64631 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<com.portfolio.platform.data.model.diana.Complication>>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22806p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64631(com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1 complicationSearchPresenter$search$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = complicationSearchPresenter$search$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1.C64631 r0 = new com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1.C64631(this.this$0, yb4);
            r0.f22806p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1.C64631) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return com.fossil.blesdk.obfuscated.kb4.m24381d(this.this$0.this$0.f22803n.queryComplicationByName(this.this$0.$query));
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ComplicationSearchPresenter$search$1(com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter complicationSearchPresenter, java.lang.String str, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = complicationSearchPresenter;
        this.$query = str;
        this.$results = ref$ObjectRef;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1 complicationSearchPresenter$search$1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1(this.this$0, this.$query, this.$results, yb4);
        complicationSearchPresenter$search$1.f22805p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return complicationSearchPresenter$search$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(T t) {
        kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef;
        T a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(t);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22805p$;
            if (this.$query.length() > 0) {
                kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2 = this.$results;
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31441c();
                com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1.C64631 r4 = new com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1.C64631(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = ref$ObjectRef2;
                this.label = 1;
                t = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r4, this);
                if (t == a) {
                    return a;
                }
                ref$ObjectRef = ref$ObjectRef2;
            }
            this.this$0.mo41044i().mo25939b(this.this$0.mo41042a((java.util.List<com.portfolio.platform.data.model.diana.Complication>) (java.util.List) this.$results.element));
            this.this$0.f22799j = this.$query;
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            ref$ObjectRef = (kotlin.jvm.internal.Ref$ObjectRef) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(t);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ref$ObjectRef.element = (java.util.List) t;
        this.this$0.mo41044i().mo25939b(this.this$0.mo41042a((java.util.List<com.portfolio.platform.data.model.diana.Complication>) (java.util.List) this.$results.element));
        this.this$0.f22799j = this.$query;
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
