package com.portfolio.platform.uirenew.home.details.activetime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$1", mo27670f = "ActiveTimeDetailPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class ActiveTimeDetailPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23556p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActiveTimeDetailPresenter$start$1(com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter activeTimeDetailPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = activeTimeDetailPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$1 activeTimeDetailPresenter$start$1 = new com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$1(this.this$0, yb4);
        activeTimeDetailPresenter$start$1.f23556p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return activeTimeDetailPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001a, code lost:
        if (r0 != null) goto L_0x001f;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.enums.Unit unit;
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter activeTimeDetailPresenter = this.this$0;
            com.portfolio.platform.data.model.MFUser currentUser = activeTimeDetailPresenter.f23541v.getCurrentUser();
            if (currentUser != null) {
                unit = currentUser.getDistanceUnit();
            }
            unit = com.portfolio.platform.enums.Unit.METRIC;
            activeTimeDetailPresenter.f23534o = unit;
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
