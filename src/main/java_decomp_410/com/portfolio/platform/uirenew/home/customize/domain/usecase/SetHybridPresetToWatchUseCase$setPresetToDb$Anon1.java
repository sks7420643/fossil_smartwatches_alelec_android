package com.portfolio.platform.uirenew.home.customize.domain.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.oj2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import java.util.Calendar;
import java.util.Iterator;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$setPresetToDb$Anon1", f = "SetHybridPresetToWatchUseCase.kt", l = {80}, m = "invokeSuspend")
public final class SetHybridPresetToWatchUseCase$setPresetToDb$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SetHybridPresetToWatchUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetHybridPresetToWatchUseCase$setPresetToDb$Anon1(SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = setHybridPresetToWatchUseCase;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SetHybridPresetToWatchUseCase$setPresetToDb$Anon1 setHybridPresetToWatchUseCase$setPresetToDb$Anon1 = new SetHybridPresetToWatchUseCase$setPresetToDb$Anon1(this.this$Anon0, yb4);
        setHybridPresetToWatchUseCase$setPresetToDb$Anon1.p$ = (zg4) obj;
        return setHybridPresetToWatchUseCase$setPresetToDb$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SetHybridPresetToWatchUseCase$setPresetToDb$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        HybridPreset hybridPreset;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            HybridPreset a2 = SetHybridPresetToWatchUseCase.c(this.this$Anon0).a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetHybridPresetToWatchUseCase", "set to watch success, set preset " + a2 + " to db");
            a2.setActive(true);
            HybridPresetRepository a3 = this.this$Anon0.h;
            this.L$Anon0 = zg4;
            this.L$Anon1 = a2;
            this.label = 1;
            if (a3.upsertHybridPreset(a2, this) == a) {
                return a;
            }
            hybridPreset = a2;
        } else if (i == 1) {
            hybridPreset = (HybridPreset) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        String t = rk2.t(instance.getTime());
        Iterator<HybridPresetAppSetting> it = hybridPreset.getButtons().iterator();
        while (it.hasNext()) {
            HybridPresetAppSetting next = it.next();
            if (!oj2.a(next.getSettings())) {
                SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase = this.this$Anon0;
                String appId = next.getAppId();
                String settings = next.getSettings();
                if (settings != null) {
                    setHybridPresetToWatchUseCase.a(appId, settings);
                    MicroAppLastSettingRepository b = this.this$Anon0.j;
                    String appId2 = next.getAppId();
                    kd4.a((Object) t, "updatedAt");
                    String settings2 = next.getSettings();
                    if (settings2 != null) {
                        b.upsertMicroAppLastSetting(new MicroAppLastSetting(appId2, t, settings2));
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }
        this.this$Anon0.a(new SetHybridPresetToWatchUseCase.d());
        return qa4.a;
    }
}
