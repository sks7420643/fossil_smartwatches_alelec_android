package com.portfolio.platform.uirenew.home.customize.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$1 */
public final class C6508x96405059 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $errorCode$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.diana.preset.DianaPreset $it;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.ArrayList $permissionErrorCodes$inlined;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22930p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase.SetPresetBroadcastReceiver this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6508x96405059(com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase.SetPresetBroadcastReceiver setPresetBroadcastReceiver, int i, java.util.ArrayList arrayList) {
        super(2, yb4);
        this.$it = dianaPreset;
        this.this$0 = setPresetBroadcastReceiver;
        this.$errorCode$inlined = i;
        this.$permissionErrorCodes$inlined = arrayList;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.domain.usecase.C6508x96405059 setDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.customize.domain.usecase.C6508x96405059(this.$it, yb4, this.this$0, this.$errorCode$inlined, this.$permissionErrorCodes$inlined);
        setDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$1.f22930p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return setDianaPresetToWatchUseCase$SetPresetBroadcastReceiver$receive$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.domain.usecase.C6508x96405059) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22930p$;
            com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase = com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase.this;
            com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset = this.$it;
            this.L$0 = zg4;
            this.label = 1;
            if (setDianaPresetToWatchUseCase.mo41110a(dianaPreset, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase.this.mo34434a(new com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase.C6505b(this.$errorCode$inlined, this.$permissionErrorCodes$inlined));
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
