package com.portfolio.platform.uirenew.home.details.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.C2723qd<com.portfolio.platform.data.model.goaltracking.GoalTrackingData>> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter f23691a;

    @DexIgnore
    public GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1(com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
        this.f23691a = goalTrackingDetailPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo8689a(com.fossil.blesdk.obfuscated.C2723qd<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> qdVar) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("getGoalTrackingDataPaging observer size=");
        sb.append(qdVar != null ? java.lang.Integer.valueOf(qdVar.size()) : null);
        local.mo33255d("GoalTrackingDetailPresenter", sb.toString());
        if (qdVar != null) {
            this.f23691a.f23671j = true;
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23691a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.details.goaltracking.C6711xa002175d(qdVar, (com.fossil.blesdk.obfuscated.yb4) null, this), 3, (java.lang.Object) null);
        }
    }
}
