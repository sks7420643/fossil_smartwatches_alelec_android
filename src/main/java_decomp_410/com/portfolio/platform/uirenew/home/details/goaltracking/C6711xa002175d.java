package com.portfolio.platform.uirenew.home.details.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1$$special$$inlined$let$lambda$1 */
public final class C6711xa002175d extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2723qd $dataList;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23684p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6711xa002175d(com.fossil.blesdk.obfuscated.C2723qd qdVar, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1 goalTrackingDetailPresenter$observeGoalTrackingDataPaging$1) {
        super(2, yb4);
        this.$dataList = qdVar;
        this.this$0 = goalTrackingDetailPresenter$observeGoalTrackingDataPaging$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.goaltracking.C6711xa002175d goalTrackingDetailPresenter$observeGoalTrackingDataPaging$1$$special$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.details.goaltracking.C6711xa002175d(this.$dataList, yb4, this.this$0);
        goalTrackingDetailPresenter$observeGoalTrackingDataPaging$1$$special$$inlined$let$lambda$1.f23684p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return goalTrackingDetailPresenter$observeGoalTrackingDataPaging$1$$special$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.goaltracking.C6711xa002175d) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.dl4 dl4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23684p$;
            com.fossil.blesdk.obfuscated.dl4 h = this.this$0.f23691a.f23672k;
            this.L$0 = zg4;
            this.L$1 = h;
            this.label = 1;
            if (h.mo26535a((java.lang.Object) null, this) == a) {
                return a;
            }
            dl4 = h;
        } else if (i == 1) {
            dl4 = (com.fossil.blesdk.obfuscated.dl4) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            if (this.this$0.f23691a.f23675n == null || (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23691a.f23675n, (java.lang.Object) com.fossil.blesdk.obfuscated.kb4.m24381d(this.$dataList)))) {
                this.this$0.f23691a.f23675n = com.fossil.blesdk.obfuscated.kb4.m24381d(this.$dataList);
            }
            this.this$0.f23691a.f23678q.mo26884c(this.$dataList);
            if (this.this$0.f23691a.f23670i && this.this$0.f23691a.f23671j) {
                com.fossil.blesdk.obfuscated.fi4 unused = this.this$0.f23691a.mo41410m();
            }
            com.fossil.blesdk.obfuscated.qa4 qa4 = com.fossil.blesdk.obfuscated.qa4.f17909a;
            dl4.mo26536a((java.lang.Object) null);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } catch (Throwable th) {
            dl4.mo26536a((java.lang.Object) null);
            throw th;
        }
    }
}
