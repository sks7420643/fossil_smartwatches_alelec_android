package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1", f = "NotificationCallsAndMessagesPresenter.kt", l = {371, 373}, m = "invokeSuspend")
public final class NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public long J$Anon0;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = notificationCallsAndMessagesPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1 notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1 = new NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1(this.this$Anon0, yb4);
        notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1.p$ = (zg4) obj;
        return notificationCallsAndMessagesPresenter$setRuleToDevice$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x017f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x01f2  */
    public final Object invokeSuspend(Object obj) {
        long j;
        Object obj2;
        List list;
        gh4 gh4;
        zg4 zg4;
        List list2;
        gh4 gh42;
        Object obj3;
        List list3;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg42 = this.p$;
            this.this$Anon0.t.b();
            this.this$Anon0.t.T();
            list2 = new ArrayList();
            if (this.this$Anon0.t.E() != this.this$Anon0.k()) {
                list2.add(new NotificationSettingsModel("AllowCallsFrom", this.this$Anon0.t.E(), true));
            }
            if (this.this$Anon0.t.J() != this.this$Anon0.j()) {
                list2.add(new NotificationSettingsModel("AllowMessagesFrom", this.this$Anon0.t.J(), false));
            }
            if (!list2.isEmpty()) {
                this.this$Anon0.b((List<NotificationSettingsModel>) list2);
            }
            j = System.currentTimeMillis();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationCallsAndMessagesPresenter.C.a();
            local.d(a2, "filter notification, time start=" + j);
            this.this$Anon0.f.clear();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = NotificationCallsAndMessagesPresenter.C.a();
            local2.d(a3, "mListAppWrapper.size = " + this.this$Anon0.m().size());
            gh4 a4 = ag4.a(zg42, this.this$Anon0.b(), (CoroutineStart) null, new NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$otherAppDeffer$Anon1(this, (yb4) null), 2, (Object) null);
            gh42 = ag4.a(zg42, this.this$Anon0.b(), (CoroutineStart) null, new NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1$contactAppDeffer$Anon1(this, (yb4) null), 2, (Object) null);
            List h = this.this$Anon0.f;
            this.L$Anon0 = zg42;
            this.L$Anon1 = list2;
            this.J$Anon0 = j;
            this.L$Anon2 = a4;
            this.L$Anon3 = gh42;
            this.L$Anon4 = h;
            this.label = 1;
            obj3 = a4.a(this);
            if (obj3 == a) {
                return a;
            }
            gh4 gh43 = a4;
            zg4 = zg42;
            list3 = h;
            gh4 = gh43;
        } else if (i == 1) {
            list3 = (List) this.L$Anon4;
            long j2 = this.J$Anon0;
            list2 = (List) this.L$Anon1;
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            gh4 = (gh4) this.L$Anon2;
            j = j2;
            gh42 = (gh4) this.L$Anon3;
            obj3 = obj;
        } else if (i == 2) {
            gh4 gh44 = (gh4) this.L$Anon3;
            gh4 gh45 = (gh4) this.L$Anon2;
            long j3 = this.J$Anon0;
            List list4 = (List) this.L$Anon1;
            zg4 zg43 = (zg4) this.L$Anon0;
            na4.a(obj);
            j = j3;
            obj2 = obj;
            list = (List) obj2;
            if (list == null) {
                this.this$Anon0.f.addAll(list);
                long b = PortfolioApp.W.c().b(new AppNotificationFilterSettings(this.this$Anon0.f, System.currentTimeMillis()), PortfolioApp.W.c().e());
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a5 = NotificationCallsAndMessagesPresenter.C.a();
                local3.d(a5, "filter notification, time end= " + b);
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String a6 = NotificationCallsAndMessagesPresenter.C.a();
                local4.d(a6, "delayTime =" + (b - j) + " mili seconds");
            } else {
                NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter = this.this$Anon0;
                notificationCallsAndMessagesPresenter.a(notificationCallsAndMessagesPresenter.n());
                this.this$Anon0.t.a();
                this.this$Anon0.t.N();
                this.this$Anon0.t.close();
            }
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        list3.addAll((Collection) obj3);
        this.L$Anon0 = zg4;
        this.L$Anon1 = list2;
        this.J$Anon0 = j;
        this.L$Anon2 = gh4;
        this.L$Anon3 = gh42;
        this.label = 2;
        obj2 = gh42.a(this);
        if (obj2 == a) {
            return a;
        }
        list = (List) obj2;
        if (list == null) {
        }
        return qa4.a;
    }
}
