package com.portfolio.platform.uirenew.home.alerts.diana;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.w52;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1", f = "HomeAlertsPresenter.kt", l = {353, 359, 368, 376}, m = "invokeSuspend")
public final class HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<? extends AppNotificationFilter>>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeAlertsPresenter$setNotificationFilterToDevice$Anon1 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1$Anon1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $notificationSettings;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 e;

            @DexIgnore
            public a(Anon1 anon1) {
                this.e = anon1;
            }

            @DexIgnore
            public final void run() {
                this.e.this$Anon0.this$Anon0.this$Anon0.r.getNotificationSettingsDao().insertListNotificationSettings(this.e.$notificationSettings);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1 homeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1, List list, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = homeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1;
            this.$notificationSettings = list;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$notificationSettings, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.this$Anon0.this$Anon0.r.runInTransaction((Runnable) new a(this));
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1$Anon2", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef $contactMessageAppFilters;
        @DexIgnore
        public /* final */ /* synthetic */ List $listNotificationSettings;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1 homeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1, List list, Ref$ObjectRef ref$ObjectRef, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = homeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1;
            this.$listNotificationSettings = list;
            this.$contactMessageAppFilters = ref$ObjectRef;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, this.$listNotificationSettings, this.$contactMessageAppFilters, yb4);
            anon2.p$ = (zg4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                for (NotificationSettingsModel notificationSettingsModel : this.$listNotificationSettings) {
                    int component2 = notificationSettingsModel.component2();
                    int i = 0;
                    if (notificationSettingsModel.component3()) {
                        String a = this.this$Anon0.this$Anon0.this$Anon0.a(component2);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = HomeAlertsPresenter.x.a();
                        local.d(a2, "CALL settingsTypeName=" + a);
                        if (component2 == 0) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String a3 = HomeAlertsPresenter.x.a();
                            local2.d(a3, "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL);
                            DianaNotificationObj.AApplicationName aApplicationName = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                            ((List) this.$contactMessageAppFilters.element).add(new AppNotificationFilter(new FNotification(aApplicationName.getAppName(), aApplicationName.getBundleCrc(), aApplicationName.getGroupId(), aApplicationName.getPackageName(), aApplicationName.getIconResourceId(), aApplicationName.getIconFwPath(), aApplicationName.getNotificationType())));
                        } else if (component2 == 1) {
                            int size = this.this$Anon0.this$Anon0.this$Anon0.k.size();
                            int i2 = 0;
                            while (i2 < size) {
                                ContactGroup contactGroup = (ContactGroup) this.this$Anon0.this$Anon0.this$Anon0.k.get(i2);
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String a4 = HomeAlertsPresenter.x.a();
                                StringBuilder sb = new StringBuilder();
                                sb.append("mListAppNotificationFilter add PHONE item - ");
                                sb.append(i2);
                                sb.append(" name = ");
                                Contact contact = contactGroup.getContacts().get(i);
                                kd4.a((Object) contact, "item.contacts[0]");
                                sb.append(contact.getDisplayName());
                                local3.d(a4, sb.toString());
                                DianaNotificationObj.AApplicationName aApplicationName2 = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                                FNotification fNotification = r11;
                                FNotification fNotification2 = new FNotification(aApplicationName2.getAppName(), aApplicationName2.getBundleCrc(), aApplicationName2.getGroupId(), aApplicationName2.getPackageName(), aApplicationName2.getIconResourceId(), aApplicationName2.getIconFwPath(), aApplicationName2.getNotificationType());
                                List<Contact> contacts = contactGroup.getContacts();
                                kd4.a((Object) contacts, "item.contacts");
                                if (!contacts.isEmpty()) {
                                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification);
                                    Contact contact2 = contactGroup.getContacts().get(0);
                                    kd4.a((Object) contact2, "item.contacts[0]");
                                    appNotificationFilter.setSender(contact2.getDisplayName());
                                    ((List) this.$contactMessageAppFilters.element).add(appNotificationFilter);
                                }
                                i2++;
                                i = 0;
                            }
                        }
                    } else {
                        String a5 = this.this$Anon0.this$Anon0.this$Anon0.a(component2);
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String a6 = HomeAlertsPresenter.x.a();
                        local4.d(a6, "MESSAGE settingsTypeName=" + a5);
                        if (component2 == 0) {
                            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                            String a7 = HomeAlertsPresenter.x.a();
                            local5.d(a7, "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.MESSAGES);
                            DianaNotificationObj.AApplicationName aApplicationName3 = DianaNotificationObj.AApplicationName.MESSAGES;
                            ((List) this.$contactMessageAppFilters.element).add(new AppNotificationFilter(new FNotification(aApplicationName3.getAppName(), aApplicationName3.getBundleCrc(), aApplicationName3.getGroupId(), aApplicationName3.getPackageName(), aApplicationName3.getIconResourceId(), aApplicationName3.getIconFwPath(), aApplicationName3.getNotificationType())));
                        } else if (component2 == 1) {
                            int size2 = this.this$Anon0.this$Anon0.this$Anon0.k.size();
                            for (int i3 = 0; i3 < size2; i3++) {
                                ContactGroup contactGroup2 = (ContactGroup) this.this$Anon0.this$Anon0.this$Anon0.k.get(i3);
                                ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                                String a8 = HomeAlertsPresenter.x.a();
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("mListAppNotificationFilter add MESSAGE item - ");
                                sb2.append(i3);
                                sb2.append(" name = ");
                                Contact contact3 = contactGroup2.getContacts().get(0);
                                kd4.a((Object) contact3, "item.contacts[0]");
                                sb2.append(contact3.getDisplayName());
                                local6.d(a8, sb2.toString());
                                DianaNotificationObj.AApplicationName aApplicationName4 = DianaNotificationObj.AApplicationName.MESSAGES;
                                FNotification fNotification3 = r10;
                                FNotification fNotification4 = new FNotification(aApplicationName4.getAppName(), aApplicationName4.getBundleCrc(), aApplicationName4.getGroupId(), aApplicationName4.getPackageName(), aApplicationName4.getIconResourceId(), aApplicationName4.getIconFwPath(), aApplicationName4.getNotificationType());
                                List<Contact> contacts2 = contactGroup2.getContacts();
                                kd4.a((Object) contacts2, "item.contacts");
                                if (!contacts2.isEmpty()) {
                                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification3);
                                    Contact contact4 = contactGroup2.getContacts().get(0);
                                    kd4.a((Object) contact4, "item.contacts[0]");
                                    appNotificationFilter2.setSender(contact4.getDisplayName());
                                    ((List) this.$contactMessageAppFilters.element).add(appNotificationFilter2);
                                }
                            }
                        }
                    }
                }
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1(HomeAlertsPresenter$setNotificationFilterToDevice$Anon1 homeAlertsPresenter$setNotificationFilterToDevice$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = homeAlertsPresenter$setNotificationFilterToDevice$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1 homeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1 = new HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1(this.this$Anon0, yb4);
        homeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1.p$ = (zg4) obj;
        return homeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x013b  */
    public final Object invokeSuspend(Object obj) {
        Ref$ObjectRef ref$ObjectRef;
        zg4 zg4;
        CoroutineUseCase.c cVar;
        List list;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg42 = this.p$;
            ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            px2 g = this.this$Anon0.this$Anon0.p;
            this.L$Anon0 = zg42;
            this.L$Anon1 = ref$ObjectRef;
            this.label = 1;
            Object a2 = w52.a(g, null, this);
            if (a2 == a) {
                return a;
            }
            Object obj2 = a2;
            zg4 = zg42;
            obj = obj2;
        } else if (i == 1) {
            ref$ObjectRef = (Ref$ObjectRef) this.L$Anon1;
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i != 2) {
            if (i == 3) {
                NotificationSettingsModel notificationSettingsModel = (NotificationSettingsModel) this.L$Anon6;
                NotificationSettingsModel notificationSettingsModel2 = (NotificationSettingsModel) this.L$Anon5;
                List list2 = (List) this.L$Anon4;
            } else if (i != 4) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list3 = (List) this.L$Anon3;
            CoroutineUseCase.c cVar2 = (CoroutineUseCase.c) this.L$Anon2;
            zg4 zg43 = (zg4) this.L$Anon0;
            na4.a(obj);
            ref$ObjectRef = (Ref$ObjectRef) this.L$Anon1;
            return (List) ref$ObjectRef.element;
        } else {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            Ref$ObjectRef ref$ObjectRef2 = (Ref$ObjectRef) this.L$Anon1;
            cVar = (CoroutineUseCase.c) this.L$Anon2;
            ref$ObjectRef = ref$ObjectRef2;
            list = (List) obj;
            if (!list.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                NotificationSettingsModel notificationSettingsModel3 = new NotificationSettingsModel("AllowCallsFrom", 0, true);
                NotificationSettingsModel notificationSettingsModel4 = new NotificationSettingsModel("AllowMessagesFrom", 0, false);
                arrayList.add(notificationSettingsModel3);
                arrayList.add(notificationSettingsModel4);
                ug4 c = this.this$Anon0.this$Anon0.c();
                Anon1 anon1 = new Anon1(this, arrayList, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = ref$ObjectRef;
                this.L$Anon2 = cVar;
                this.L$Anon3 = list;
                this.L$Anon4 = arrayList;
                this.L$Anon5 = notificationSettingsModel3;
                this.L$Anon6 = notificationSettingsModel4;
                this.label = 3;
                if (yf4.a(c, anon1, this) == a) {
                    return a;
                }
            } else {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = HomeAlertsPresenter.x.a();
                local.d(a3, "listNotificationSettings.size = " + list.size());
                ug4 a4 = this.this$Anon0.this$Anon0.b();
                Anon2 anon2 = new Anon2(this, list, ref$ObjectRef, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = ref$ObjectRef;
                this.L$Anon2 = cVar;
                this.L$Anon3 = list;
                this.label = 4;
                if (yf4.a(a4, anon2, this) == a) {
                    return a;
                }
            }
            return (List) ref$ObjectRef.element;
        }
        CoroutineUseCase.c cVar3 = (CoroutineUseCase.c) obj;
        if (cVar3 instanceof px2.d) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a5 = HomeAlertsPresenter.x.a();
            StringBuilder sb = new StringBuilder();
            sb.append("GetAllContactGroup onSuccess, size = ");
            px2.d dVar = (px2.d) cVar3;
            sb.append(dVar.a().size());
            local2.d(a5, sb.toString());
            ref$ObjectRef.element = new ArrayList();
            this.this$Anon0.this$Anon0.k.clear();
            this.this$Anon0.this$Anon0.k = kb4.d(dVar.a());
            ug4 c2 = this.this$Anon0.this$Anon0.c();
            HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1$listNotificationSettings$Anon1 homeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1$listNotificationSettings$Anon1 = new HomeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1$listNotificationSettings$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = ref$ObjectRef;
            this.L$Anon2 = cVar3;
            this.label = 2;
            Object a6 = yf4.a(c2, homeAlertsPresenter$setNotificationFilterToDevice$Anon1$contactAppDeffer$Anon1$listNotificationSettings$Anon1, this);
            if (a6 == a) {
                return a;
            }
            Object obj3 = a6;
            cVar = cVar3;
            obj = obj3;
            list = (List) obj;
            if (!list.isEmpty()) {
            }
            return (List) ref$ObjectRef.element;
        }
        if (cVar3 instanceof px2.b) {
            FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "GetAllContactGroup onError");
        }
        return (List) ref$ObjectRef.element;
    }
}
