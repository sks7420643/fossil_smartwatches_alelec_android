package com.portfolio.platform.uirenew.home.profile.help;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gi3;
import com.fossil.blesdk.obfuscated.hi3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kr2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HelpPresenter extends gi3 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public /* final */ hi3 f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ kr2 h;
    @DexIgnore
    public /* final */ AnalyticsHelper i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = HelpPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "HelpPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public HelpPresenter(hi3 hi3, DeviceRepository deviceRepository, kr2 kr2, AnalyticsHelper analyticsHelper) {
        kd4.b(hi3, "mView");
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(kr2, "mGetZendeskInformation");
        kd4.b(analyticsHelper, "mAnalyticsHelper");
        this.f = hi3;
        this.g = deviceRepository;
        this.h = kr2;
        this.i = analyticsHelper;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(j, "presenter start");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(j, "presenter stop");
    }

    @DexIgnore
    public void h() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HelpPresenter$logSendFeedbackSuccessfullyEvent$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HelpPresenter$logSendOpenFeedbackEvent$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void j() {
        this.f.a(this);
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, "subject");
        this.h.a(new kr2.c(str), new HelpPresenter$sendFeedback$Anon1(this));
    }
}
