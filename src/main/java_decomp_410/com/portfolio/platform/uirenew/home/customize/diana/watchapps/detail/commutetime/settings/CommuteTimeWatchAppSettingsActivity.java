package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.p23;
import com.fossil.blesdk.obfuscated.y43;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeWatchAppSettingsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            kd4.b(fragment, "fragment");
            kd4.b(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), CommuteTimeWatchAppSettingsActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 106);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView(R.layout.activity_base);
        if (((y43) getSupportFragmentManager().a((int) R.id.content)) == null) {
            if (getIntent() != null) {
                str = getIntent().getStringExtra(Constants.USER_SETTING);
                kd4.a((Object) str, "intent.getStringExtra(Constants.JSON_KEY_SETTINGS)");
            } else {
                str = "";
            }
            a((Fragment) y43.n.a(str), p23.q.b(), (int) R.id.content);
        }
    }
}
