package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1", f = "NotificationCallsAndMessagesPresenter.kt", l = {433}, m = "invokeSuspend")
public final class NotificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $settings;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1$Anon1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 e;

            @DexIgnore
            public a(Anon1 anon1) {
                this.e = anon1;
            }

            @DexIgnore
            public final void run() {
                this.e.this$Anon0.this$Anon0.A.getNotificationSettingsDao().insertListNotificationSettings(this.e.this$Anon0.$settings);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(NotificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1 notificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = notificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.this$Anon0.A.runInTransaction((Runnable) new a(this));
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, List list, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = notificationCallsAndMessagesPresenter;
        this.$settings = list;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NotificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1 notificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1 = new NotificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1(this.this$Anon0, this.$settings, yb4);
        notificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1.p$ = (zg4) obj;
        return notificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 b = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            if (yf4.a(b, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
