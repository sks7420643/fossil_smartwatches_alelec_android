package com.portfolio.platform.uirenew.home.customize.hybrid.microapp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$1$1$requiredPermissionList$1", mo27670f = "MicroAppPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$1$1$requiredPermissionList$1 */
public final class C6554xef5abb8b extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<? extends java.lang.String>>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23049p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$1.C65521 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6554xef5abb8b(com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$1.C65521 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = r1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.hybrid.microapp.C6554xef5abb8b microAppPresenter$mPermissionOfSelectedMicroAppTransformations$1$1$requiredPermissionList$1 = new com.portfolio.platform.uirenew.home.customize.hybrid.microapp.C6554xef5abb8b(this.this$0, yb4);
        microAppPresenter$mPermissionOfSelectedMicroAppTransformations$1$1$requiredPermissionList$1.f23049p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return microAppPresenter$mPermissionOfSelectedMicroAppTransformations$1$1$requiredPermissionList$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.hybrid.microapp.C6554xef5abb8b) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.gl2.f15135c.mo27707b(this.this$0.$it.getId());
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
