package com.portfolio.platform.uirenew.home.details.activity;

import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ke3;
import com.fossil.blesdk.obfuscated.le3;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.me3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.re4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityDetailPresenter extends ke3 implements PagingRequestHelper.a {
    @DexIgnore
    public /* final */ h42 A;
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public MutableLiveData<Pair<Date, Date>> h; // = new MutableLiveData<>();
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public List<ActivitySummary> k; // = new ArrayList();
    @DexIgnore
    public List<ActivitySample> l; // = new ArrayList();
    @DexIgnore
    public ActivitySummary m;
    @DexIgnore
    public List<ActivitySample> n;
    @DexIgnore
    public Unit o; // = Unit.METRIC;
    @DexIgnore
    public LiveData<os3<List<ActivitySummary>>> p;
    @DexIgnore
    public LiveData<os3<List<ActivitySample>>> q;
    @DexIgnore
    public Listing<WorkoutSession> r;
    @DexIgnore
    public /* final */ le3 s;
    @DexIgnore
    public /* final */ SummariesRepository t;
    @DexIgnore
    public /* final */ ActivitiesRepository u;
    @DexIgnore
    public /* final */ UserRepository v;
    @DexIgnore
    public /* final */ WorkoutSessionRepository w;
    @DexIgnore
    public /* final */ FitnessDataDao x;
    @DexIgnore
    public /* final */ WorkoutDao y;
    @DexIgnore
    public /* final */ FitnessDatabase z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<qd<WorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter a;

        @DexIgnore
        public b(ActivityDetailPresenter activityDetailPresenter) {
            this.a = activityDetailPresenter;
        }

        @DexIgnore
        public final void a(qd<WorkoutSession> qdVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActivityDetailPresenter", "getWorkoutSessionsPaging observed size = " + qdVar.size());
            if (DeviceHelper.o.g(PortfolioApp.W.c().e())) {
                kd4.a((Object) qdVar, "pageList");
                if (kb4.d(qdVar).isEmpty()) {
                    this.a.s.a(false, this.a.o, qdVar);
                    return;
                }
            }
            le3 o = this.a.s;
            Unit h = this.a.o;
            kd4.a((Object) qdVar, "pageList");
            o.a(true, h, qdVar);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter a;

        @DexIgnore
        public c(ActivityDetailPresenter activityDetailPresenter) {
            this.a = activityDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<os3<List<ActivitySample>>> apply(Pair<? extends Date, ? extends Date> pair) {
            return this.a.u.getActivityList((Date) pair.component1(), (Date) pair.component2(), true);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter a;

        @DexIgnore
        public d(ActivityDetailPresenter activityDetailPresenter) {
            this.a = activityDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<os3<List<ActivitySummary>>> apply(Pair<? extends Date, ? extends Date> pair) {
            return this.a.t.getSummaries((Date) pair.component1(), (Date) pair.component2(), true);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public ActivityDetailPresenter(le3 le3, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FitnessDataDao fitnessDataDao, WorkoutDao workoutDao, FitnessDatabase fitnessDatabase, h42 h42) {
        kd4.b(le3, "mView");
        kd4.b(summariesRepository, "mSummariesRepository");
        kd4.b(activitiesRepository, "mActivitiesRepository");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(workoutSessionRepository, "mWorkoutSessionRepository");
        kd4.b(fitnessDataDao, "mFitnessDataDao");
        kd4.b(workoutDao, "mWorkoutDao");
        kd4.b(fitnessDatabase, "mWorkoutDatabase");
        kd4.b(h42, "appExecutors");
        this.s = le3;
        this.t = summariesRepository;
        this.u = activitiesRepository;
        this.v = userRepository;
        this.w = workoutSessionRepository;
        this.x = fitnessDataDao;
        this.y = workoutDao;
        this.z = fitnessDatabase;
        this.A = h42;
        LiveData<os3<List<ActivitySummary>>> b2 = hc.b(this.h, new d(this));
        kd4.a((Object) b2, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.p = b2;
        LiveData<os3<List<ActivitySample>>> b3 = hc.b(this.h, new c(this));
        kd4.a((Object) b3, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.q = b3;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("ActivityDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ActivityDetailPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("ActivityDetailPresenter", "stop");
        LiveData<os3<List<ActivitySummary>>> liveData = this.p;
        le3 le3 = this.s;
        if (le3 != null) {
            liveData.a((LifecycleOwner) (me3) le3);
            this.q.a((LifecycleOwner) this.s);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
    }

    @DexIgnore
    public void h() {
        try {
            Listing<WorkoutSession> listing = this.r;
            if (listing != null) {
                LiveData<qd<WorkoutSession>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    le3 le3 = this.s;
                    if (le3 != null) {
                        pagedList.a((LifecycleOwner) (me3) le3);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
                    }
                }
            }
            this.w.removePagingListener();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(qa4.a);
            local.e("ActivityDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void i() {
        Date l2 = rk2.l(this.g);
        kd4.a((Object) l2, "DateHelper.getNextDate(mDate)");
        b(l2);
    }

    @DexIgnore
    public void j() {
        Date m2 = rk2.m(this.g);
        kd4.a((Object) m2, "DateHelper.getPrevDate(mDate)");
        b(m2);
    }

    @DexIgnore
    public void k() {
        this.s.a(this);
    }

    @DexIgnore
    public final fi4 l() {
        return ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ActivityDetailPresenter$showDetailChart$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void c(Date date) {
        h();
        WorkoutSessionRepository workoutSessionRepository = this.w;
        this.r = workoutSessionRepository.getWorkoutSessionsPaging(date, workoutSessionRepository, this.x, this.y, this.z, this.A, this);
        Listing<WorkoutSession> listing = this.r;
        if (listing != null) {
            LiveData<qd<WorkoutSession>> pagedList = listing.getPagedList();
            le3 le3 = this.s;
            if (le3 != null) {
                pagedList.a((me3) le3, new b(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void b(Date date) {
        kd4.b(date, "date");
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ActivityDetailPresenter$setDate$Anon1(this, date, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final ActivitySummary b(Date date, List<ActivitySummary> list) {
        T t2 = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (rk2.d(((ActivitySummary) next).getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return (ActivitySummary) t2;
    }

    @DexIgnore
    public void a(Date date) {
        kd4.b(date, "date");
        c(date);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        kd4.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.g.getTime());
    }

    @DexIgnore
    public final List<ActivitySample> a(Date date, List<ActivitySample> list) {
        if (list != null) {
            re4<T> b2 = kb4.b(list);
            if (b2 != null) {
                re4<T> a2 = SequencesKt___SequencesKt.a(b2, new ActivityDetailPresenter$findActivitySamples$Anon1(date));
                if (a2 != null) {
                    return SequencesKt___SequencesKt.g(a2);
                }
            }
        }
        return null;
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        kd4.b(eVar, "report");
        FLogger.INSTANCE.getLocal().d("ActivityDetailPresenter", "retry all failed request");
        Listing<WorkoutSession> listing = this.r;
        if (listing != null) {
            wc4<qa4> retry = listing.getRetry();
            if (retry != null) {
                qa4 invoke = retry.invoke();
            }
        }
    }
}
