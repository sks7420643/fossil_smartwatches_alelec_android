package com.portfolio.platform.uirenew.home.profile;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeProfilePresenter$updateUser$Anon1 implements CoroutineUseCase.e<UpdateUser.d, UpdateUser.c> {
    @DexIgnore
    public /* final */ /* synthetic */ HomeProfilePresenter a;

    @DexIgnore
    public HomeProfilePresenter$updateUser$Anon1(HomeProfilePresenter homeProfilePresenter) {
        this.a = homeProfilePresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(UpdateUser.d dVar) {
        kd4.b(dVar, "responseValue");
        FLogger.INSTANCE.getLocal().d(HomeProfilePresenter.C.a(), ".Inside updateUser onSuccess");
        if (this.a.l().isActive()) {
            this.a.l().d();
            fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new HomeProfilePresenter$updateUser$Anon1$onSuccess$Anon1(this, (yb4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void a(UpdateUser.c cVar) {
        kd4.b(cVar, "errorValue");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = HomeProfilePresenter.C.a();
        local.d(a2, ".Inside updateUser onError errorCode=" + cVar.a());
        if (this.a.l().isActive()) {
            this.a.l().d();
            this.a.l().a(cVar.a(), "");
        }
    }
}
