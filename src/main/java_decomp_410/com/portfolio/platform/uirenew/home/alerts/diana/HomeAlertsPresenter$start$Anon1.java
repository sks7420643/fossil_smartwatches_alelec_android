package com.portfolio.platform.uirenew.home.alerts.diana;

import android.content.Context;
import androidx.lifecycle.LifecycleOwner;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.bn2;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gb4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.sv2;
import com.fossil.blesdk.obfuscated.tv2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.vy2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeAlertsPresenter$start$Anon1<T> implements cc<String> {
    @DexIgnore
    public /* final */ /* synthetic */ HomeAlertsPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$Anon1$Anon1", f = "HomeAlertsPresenter.kt", l = {83}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter$start$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon3<T> implements cc<List<? extends DNDScheduledTimeModel>> {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$Anon1$Anon1$Anon3$Anon1")
            @gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$Anon1$Anon1$Anon3$Anon1", f = "HomeAlertsPresenter.kt", l = {167}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$Anon1$Anon1$Anon3$Anon1  reason: collision with other inner class name */
            public static final class C0133Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $lDndScheduledTimeModel;
                @DexIgnore
                public Object L$Anon0;
                @DexIgnore
                public int label;
                @DexIgnore
                public zg4 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Anon3 this$Anon0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$Anon1$Anon1$Anon3$Anon1$Anon1")
                @gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$Anon1$Anon1$Anon3$Anon1$Anon1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
                /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$Anon1$Anon1$Anon3$Anon1$Anon1  reason: collision with other inner class name */
                public static final class C0134Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public zg4 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0133Anon1 this$Anon0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0134Anon1(C0133Anon1 anon1, yb4 yb4) {
                        super(2, yb4);
                        this.this$Anon0 = anon1;
                    }

                    @DexIgnore
                    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                        kd4.b(yb4, "completion");
                        C0134Anon1 anon1 = new C0134Anon1(this.this$Anon0, yb4);
                        anon1.p$ = (zg4) obj;
                        return anon1;
                    }

                    @DexIgnore
                    public final Object invoke(Object obj, Object obj2) {
                        return ((C0134Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
                    }

                    @DexIgnore
                    public final Object invokeSuspend(Object obj) {
                        cc4.a();
                        if (this.label == 0) {
                            na4.a(obj);
                            this.this$Anon0.this$Anon0.a.this$Anon0.a.v.getDNDScheduledTimeDao().insertListDNDScheduledTime(this.this$Anon0.$lDndScheduledTimeModel);
                            return qa4.a;
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0133Anon1(Anon3 anon3, List list, yb4 yb4) {
                    super(2, yb4);
                    this.this$Anon0 = anon3;
                    this.$lDndScheduledTimeModel = list;
                }

                @DexIgnore
                public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                    kd4.b(yb4, "completion");
                    C0133Anon1 anon1 = new C0133Anon1(this.this$Anon0, this.$lDndScheduledTimeModel, yb4);
                    anon1.p$ = (zg4) obj;
                    return anon1;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0133Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    Object a = cc4.a();
                    int i = this.label;
                    if (i == 0) {
                        na4.a(obj);
                        zg4 zg4 = this.p$;
                        ug4 c = this.this$Anon0.a.this$Anon0.a.c();
                        C0134Anon1 anon1 = new C0134Anon1(this, (yb4) null);
                        this.L$Anon0 = zg4;
                        this.label = 1;
                        if (yf4.a(c, anon1, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        zg4 zg42 = (zg4) this.L$Anon0;
                        na4.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return qa4.a;
                }
            }

            @DexIgnore
            public Anon3(Anon1 anon1) {
                this.a = anon1;
            }

            @DexIgnore
            public final void a(List<DNDScheduledTimeModel> list) {
                if (list == null || list.isEmpty()) {
                    ArrayList arrayList = new ArrayList();
                    DNDScheduledTimeModel dNDScheduledTimeModel = new DNDScheduledTimeModel("Start", 1380, 0);
                    DNDScheduledTimeModel dNDScheduledTimeModel2 = new DNDScheduledTimeModel("End", 1140, 1);
                    arrayList.add(dNDScheduledTimeModel);
                    arrayList.add(dNDScheduledTimeModel2);
                    fi4 unused = ag4.b(this.a.this$Anon0.a.e(), (CoroutineContext) null, (CoroutineStart) null, new C0133Anon1(this, arrayList, (yb4) null), 3, (Object) null);
                    return;
                }
                for (DNDScheduledTimeModel dNDScheduledTimeModel3 : list) {
                    if (dNDScheduledTimeModel3.getScheduledTimeType() == 0) {
                        this.a.this$Anon0.a.l.b(this.a.this$Anon0.a.b(dNDScheduledTimeModel3.getMinutes()));
                    } else {
                        this.a.this$Anon0.a.l.a(this.a.this$Anon0.a.b(dNDScheduledTimeModel3.getMinutes()));
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements CoroutineUseCase.e<vy2.a, CoroutineUseCase.a> {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 a;

            @DexIgnore
            public a(Anon1 anon1) {
                this.a = anon1;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(vy2.a aVar) {
                String str;
                kd4.b(aVar, "responseValue");
                FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "GetApps onSuccess");
                ArrayList<AppWrapper> arrayList = new ArrayList<>();
                for (AppWrapper next : aVar.a()) {
                    DianaNotificationObj.AApplicationName.Companion companion = DianaNotificationObj.AApplicationName.Companion;
                    InstalledApp installedApp = next.getInstalledApp();
                    if (installedApp != null) {
                        String identifier = installedApp.getIdentifier();
                        kd4.a((Object) identifier, "app.installedApp!!.identifier");
                        if (companion.isPackageNameSupportedBySDK(identifier)) {
                            arrayList.add(next);
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                if (this.a.this$Anon0.a.u.A()) {
                    ArrayList arrayList2 = new ArrayList();
                    for (AppWrapper appWrapper : arrayList) {
                        InstalledApp installedApp2 = appWrapper.getInstalledApp();
                        Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
                        if (isSelected == null) {
                            kd4.a();
                            throw null;
                        } else if (!isSelected.booleanValue()) {
                            arrayList2.add(appWrapper);
                        }
                    }
                    this.a.this$Anon0.a.i().clear();
                    this.a.this$Anon0.a.i().addAll(arrayList);
                    if (!arrayList2.isEmpty()) {
                        this.a.this$Anon0.a.a((List<AppWrapper>) arrayList2);
                        return;
                    }
                    String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_Elements_Label__All);
                    sv2 m = this.a.this$Anon0.a.l;
                    kd4.a((Object) a2, "notificationAppOverView");
                    m.h(a2);
                    if (bn2.a(bn2.d, ((tv2) this.a.this$Anon0.a.l).getContext(), "NOTIFICATION_APPS", false, 4, (Object) null)) {
                        HomeAlertsPresenter homeAlertsPresenter = this.a.this$Anon0.a;
                        if (homeAlertsPresenter.a(homeAlertsPresenter.i(), (List<AppWrapper>) arrayList)) {
                            this.a.this$Anon0.a.k();
                            return;
                        }
                        return;
                    }
                    return;
                }
                this.a.this$Anon0.a.i().clear();
                int i = 0;
                for (AppWrapper appWrapper2 : arrayList) {
                    InstalledApp installedApp3 = appWrapper2.getInstalledApp();
                    Boolean isSelected2 = installedApp3 != null ? installedApp3.isSelected() : null;
                    if (isSelected2 == null) {
                        kd4.a();
                        throw null;
                    } else if (isSelected2.booleanValue()) {
                        this.a.this$Anon0.a.i().add(appWrapper2);
                        i++;
                    }
                }
                if (i == aVar.a().size()) {
                    str = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_Elements_Label__All);
                } else if (i == 0) {
                    str = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Sections_without_Device_Profile_Without_Watch_None);
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append(i);
                    sb.append('/');
                    sb.append(arrayList.size());
                    str = sb.toString();
                }
                sv2 m2 = this.a.this$Anon0.a.l;
                kd4.a((Object) str, "notificationAppOverView");
                m2.h(str);
                if (i > 0) {
                    bn2.a(bn2.d, ((tv2) this.a.this$Anon0.a.l).getContext(), "NOTIFICATION_APPS", false, 4, (Object) null);
                }
            }

            @DexIgnore
            public void a(CoroutineUseCase.a aVar) {
                kd4.b(aVar, "errorValue");
                FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "GetApps onError");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements Comparator<T> {
            @DexIgnore
            public final int compare(T t, T t2) {
                return wb4.a(Integer.valueOf(((Alarm) t).getTotalMinutes()), Integer.valueOf(((Alarm) t2).getTotalMinutes()));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HomeAlertsPresenter$start$Anon1 homeAlertsPresenter$start$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = homeAlertsPresenter$start$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object a2 = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                ug4 c = this.this$Anon0.a.c();
                HomeAlertsPresenter$start$Anon1$Anon1$allAlarms$Anon1 homeAlertsPresenter$start$Anon1$Anon1$allAlarms$Anon1 = new HomeAlertsPresenter$start$Anon1$Anon1$allAlarms$Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                obj2 = yf4.a(c, homeAlertsPresenter$start$Anon1$Anon1$allAlarms$Anon1, this);
                if (obj2 == a2) {
                    return a2;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
                obj2 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List<Alarm> list = (List) obj2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = HomeAlertsPresenter.x.a();
            local.d(a3, "GetAlarms onSuccess: size = " + list.size());
            this.this$Anon0.a.h.clear();
            for (Alarm copy$default : list) {
                this.this$Anon0.a.h.add(Alarm.copy$default(copy$default, (String) null, (String) null, (String) null, 0, 0, (int[]) null, false, false, (String) null, (String) null, 0, 2047, (Object) null));
            }
            ArrayList e = this.this$Anon0.a.h;
            if (e.size() > 1) {
                gb4.a(e, new b());
            }
            this.this$Anon0.a.l.d(this.this$Anon0.a.h);
            this.this$Anon0.a.o.a(null, new a(this));
            HomeAlertsPresenter homeAlertsPresenter = this.this$Anon0.a;
            homeAlertsPresenter.i = homeAlertsPresenter.u.E();
            this.this$Anon0.a.l.m(this.this$Anon0.a.i);
            this.this$Anon0.a.g.a((LifecycleOwner) this.this$Anon0.a.l, new Anon3(this));
            return qa4.a;
        }
    }

    @DexIgnore
    public HomeAlertsPresenter$start$Anon1(HomeAlertsPresenter homeAlertsPresenter) {
        this.a = homeAlertsPresenter;
    }

    @DexIgnore
    public final void a(String str) {
        if (str == null || str.length() == 0) {
            this.a.l.a(true);
        } else {
            fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (yb4) null), 3, (Object) null);
        }
    }
}
