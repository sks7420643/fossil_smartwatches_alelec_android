package com.portfolio.platform.uirenew.home;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$Anon1", f = "HomePresenter.kt", l = {236}, m = "invokeSuspend")
public final class HomePresenter$checkLastOTASuccess$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeSerial;
    @DexIgnore
    public /* final */ /* synthetic */ String $lastFwTemp;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomePresenter$checkLastOTASuccess$Anon1(HomePresenter homePresenter, String str, String str2, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = homePresenter;
        this.$activeSerial = str;
        this.$lastFwTemp = str2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomePresenter$checkLastOTASuccess$Anon1 homePresenter$checkLastOTASuccess$Anon1 = new HomePresenter$checkLastOTASuccess$Anon1(this.this$Anon0, this.$activeSerial, this.$lastFwTemp, yb4);
        homePresenter$checkLastOTASuccess$Anon1.p$ = (zg4) obj;
        return homePresenter$checkLastOTASuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomePresenter$checkLastOTASuccess$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        String str = null;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 b = this.this$Anon0.c();
            HomePresenter$checkLastOTASuccess$Anon1$activeDevice$Anon1 homePresenter$checkLastOTASuccess$Anon1$activeDevice$Anon1 = new HomePresenter$checkLastOTASuccess$Anon1$activeDevice$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(b, homePresenter$checkLastOTASuccess$Anon1$activeDevice$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Device device = (Device) obj;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = HomePresenter.y.a();
        StringBuilder sb = new StringBuilder();
        sb.append("Inside .checkLastOTASuccess, getDeviceBySerial success currentDeviceFw ");
        if (device != null) {
            str = device.getFirmwareRevision();
        }
        sb.append(str);
        local.d(a2, sb.toString());
        if (device != null && !qf4.b(this.$lastFwTemp, device.getFirmwareRevision(), true)) {
            FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "Handle OTA complete on check last OTA success");
            this.this$Anon0.m.n(this.$activeSerial);
            this.this$Anon0.m.a(this.this$Anon0.o.e(), -1);
            this.this$Anon0.l.b(true);
        }
        return qa4.a;
    }
}
