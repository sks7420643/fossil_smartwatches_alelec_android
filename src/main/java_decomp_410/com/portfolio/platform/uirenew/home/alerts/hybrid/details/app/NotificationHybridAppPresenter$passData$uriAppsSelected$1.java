package com.portfolio.platform.uirenew.home.alerts.hybrid.details.app;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationHybridAppPresenter$passData$uriAppsSelected$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper, java.lang.Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationHybridAppPresenter$passData$uriAppsSelected$1(com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppPresenter notificationHybridAppPresenter) {
        super(1);
        this.this$0 = notificationHybridAppPresenter;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke((com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper) obj));
    }

    @DexIgnore
    public final boolean invoke(com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper appWrapper) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(appWrapper, "it");
        com.portfolio.platform.data.model.InstalledApp installedApp = appWrapper.getInstalledApp();
        java.lang.Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
        if (isSelected != null) {
            return isSelected.booleanValue() && appWrapper.getCurrentHandGroup() == this.this$0.f22551h;
        }
        com.fossil.blesdk.obfuscated.kd4.m24405a();
        throw null;
    }
}
