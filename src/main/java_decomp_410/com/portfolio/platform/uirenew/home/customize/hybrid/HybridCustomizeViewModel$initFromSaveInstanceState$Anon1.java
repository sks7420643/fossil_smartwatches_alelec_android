package com.portfolio.platform.uirenew.home.customize.hybrid;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initFromSaveInstanceState$Anon1", f = "HybridCustomizeViewModel.kt", l = {122, 129}, m = "invokeSuspend")
public final class HybridCustomizeViewModel$initFromSaveInstanceState$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $microAppPos;
    @DexIgnore
    public /* final */ /* synthetic */ String $presetId;
    @DexIgnore
    public /* final */ /* synthetic */ HybridPreset $savedCurrentPreset;
    @DexIgnore
    public /* final */ /* synthetic */ HybridPreset $savedOriginalPreset;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HybridCustomizeViewModel this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initFromSaveInstanceState$Anon1$Anon1", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel$initFromSaveInstanceState$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HybridCustomizeViewModel$initFromSaveInstanceState$Anon1 hybridCustomizeViewModel$initFromSaveInstanceState$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = hybridCustomizeViewModel$initFromSaveInstanceState$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.this$Anon0.d.a(this.this$Anon0.this$Anon0.n);
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridCustomizeViewModel$initFromSaveInstanceState$Anon1(HybridCustomizeViewModel hybridCustomizeViewModel, String str, HybridPreset hybridPreset, HybridPreset hybridPreset2, String str2, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = hybridCustomizeViewModel;
        this.$presetId = str;
        this.$savedOriginalPreset = hybridPreset;
        this.$savedCurrentPreset = hybridPreset2;
        this.$microAppPos = str2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HybridCustomizeViewModel$initFromSaveInstanceState$Anon1 hybridCustomizeViewModel$initFromSaveInstanceState$Anon1 = new HybridCustomizeViewModel$initFromSaveInstanceState$Anon1(this.this$Anon0, this.$presetId, this.$savedOriginalPreset, this.$savedCurrentPreset, this.$microAppPos, yb4);
        hybridCustomizeViewModel$initFromSaveInstanceState$Anon1.p$ = (zg4) obj;
        return hybridCustomizeViewModel$initFromSaveInstanceState$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HybridCustomizeViewModel$initFromSaveInstanceState$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00b9  */
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HybridCustomizeViewModel.s.a();
            local.d(a2, "initFromSaveInstanceState presetId " + this.$presetId + " savedOriginalPreset=" + this.$savedOriginalPreset + ',' + " savedCurrentPreset=" + this.$savedCurrentPreset + " microAppPos=" + this.$microAppPos + " currentPreset");
            if (this.$savedOriginalPreset == null) {
                HybridCustomizeViewModel hybridCustomizeViewModel = this.this$Anon0;
                String str = this.$presetId;
                this.L$Anon0 = zg4;
                this.label = 1;
                if (hybridCustomizeViewModel.a(str, (yb4<? super qa4>) this) == a) {
                    return a;
                }
            } else {
                this.this$Anon0.h();
                this.this$Anon0.c = this.$savedOriginalPreset;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            if (this.$savedCurrentPreset != null) {
                this.this$Anon0.d.a(this.$savedCurrentPreset);
            }
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.g.a(this.$microAppPos);
        pi4 c = nh4.c();
        Anon1 anon1 = new Anon1(this, (yb4) null);
        this.L$Anon0 = zg4;
        this.label = 2;
        if (yf4.a(c, anon1, this) == a) {
            return a;
        }
        if (this.$savedCurrentPreset != null) {
        }
        return qa4.a;
    }
}
