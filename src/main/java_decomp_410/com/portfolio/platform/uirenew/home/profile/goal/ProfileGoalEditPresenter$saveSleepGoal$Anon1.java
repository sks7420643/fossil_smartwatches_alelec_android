package com.portfolio.platform.uirenew.home.profile.goal;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.vh3;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.blesdk.obfuscated.zr2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.enums.Status;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveSleepGoal$Anon1", f = "ProfileGoalEditPresenter.kt", l = {}, m = "invokeSuspend")
public final class ProfileGoalEditPresenter$saveSleepGoal$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileGoalEditPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements cc<os3<? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter$saveSleepGoal$Anon1 a;
        @DexIgnore
        public /* final */ /* synthetic */ LiveData b;

        @DexIgnore
        public a(ProfileGoalEditPresenter$saveSleepGoal$Anon1 profileGoalEditPresenter$saveSleepGoal$Anon1, LiveData liveData) {
            this.a = profileGoalEditPresenter$saveSleepGoal$Anon1;
            this.b = liveData;
        }

        @DexIgnore
        public final void a(os3<Integer> os3) {
            FLogger.INSTANCE.getLocal().d(ProfileGoalEditPresenter.u, "saveSleepGoal observe");
            if ((os3 != null ? os3.f() : null) != Status.SUCCESS || os3.d() == null) {
                if ((os3 != null ? os3.f() : null) == Status.ERROR) {
                    vh3 m = this.a.this$Anon0.m();
                    Integer c = os3.c();
                    if (c != null) {
                        int intValue = c.intValue();
                        String e = os3.e();
                        if (e != null) {
                            m.d(intValue, e);
                            this.b.a((LifecycleOwner) this.a.this$Anon0.m());
                            this.a.this$Anon0.o();
                            return;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
                return;
            }
            this.a.this$Anon0.p();
            this.b.a((LifecycleOwner) this.a.this$Anon0.m());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileGoalEditPresenter$saveSleepGoal$Anon1(ProfileGoalEditPresenter profileGoalEditPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = profileGoalEditPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ProfileGoalEditPresenter$saveSleepGoal$Anon1 profileGoalEditPresenter$saveSleepGoal$Anon1 = new ProfileGoalEditPresenter$saveSleepGoal$Anon1(this.this$Anon0, yb4);
        profileGoalEditPresenter$saveSleepGoal$Anon1.p$ = (zg4) obj;
        return profileGoalEditPresenter$saveSleepGoal$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileGoalEditPresenter$saveSleepGoal$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            LiveData<os3<Integer>> updateLastSleepGoal = this.this$Anon0.r.updateLastSleepGoal(this.this$Anon0.j);
            vh3 m = this.this$Anon0.m();
            if (m != null) {
                updateLastSleepGoal.a((zr2) m, new a(this, updateLastSleepGoal));
                return qa4.a;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
