package com.portfolio.platform.uirenew.home.details.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1", mo27670f = "GoalTrackingDetailPresenter.kt", mo27671l = {262, 229, 232}, mo27672m = "invokeSuspend")
public final class GoalTrackingDetailPresenter$showDetailChart$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23694p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDetailPresenter$showDetailChart$1(com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter goalTrackingDetailPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = goalTrackingDetailPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1 goalTrackingDetailPresenter$showDetailChart$1 = new com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1(this.this$0, yb4);
        goalTrackingDetailPresenter$showDetailChart$1.f23694p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return goalTrackingDetailPresenter$showDetailChart$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00cf A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00df A[Catch:{ all -> 0x0027 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fc A[Catch:{ all -> 0x0027 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0101 A[Catch:{ all -> 0x0027 }] */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.dl4 dl4;
        com.fossil.blesdk.obfuscated.dl4 dl42;
        kotlin.Pair pair;
        java.util.ArrayList arrayList;
        com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary g;
        int i;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a;
        com.fossil.blesdk.obfuscated.zg4 zg42;
        java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i2 = this.label;
        if (i2 == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg42 = this.f23694p$;
            dl4 = this.this$0.f23672k;
            this.L$0 = zg42;
            this.L$1 = dl4;
            this.label = 1;
            if (dl4.mo26535a((java.lang.Object) null, this) == a2) {
                return a2;
            }
        } else if (i2 == 1) {
            dl4 = (com.fossil.blesdk.obfuscated.dl4) this.L$1;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
        } else if (i2 == 2) {
            dl4 = (com.fossil.blesdk.obfuscated.dl4) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            try {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                kotlin.Pair pair2 = (kotlin.Pair) obj;
                java.util.ArrayList arrayList2 = (java.util.ArrayList) pair2.getFirst();
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local.mo33255d("GoalTrackingDetailPresenter", "showDetailChart - date=" + this.this$0.f23668g + ", data=" + arrayList2);
                com.fossil.blesdk.obfuscated.ug4 a3 = this.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1$1$maxValue$1 goalTrackingDetailPresenter$showDetailChart$1$1$maxValue$1 = new com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1$1$maxValue$1(arrayList2, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = dl4;
                this.L$2 = pair2;
                this.L$3 = arrayList2;
                this.label = 3;
                a = com.fossil.blesdk.obfuscated.yf4.m30997a(a3, goalTrackingDetailPresenter$showDetailChart$1$1$maxValue$1, this);
                if (a != a2) {
                    return a2;
                }
                arrayList = arrayList2;
                com.fossil.blesdk.obfuscated.dl4 dl43 = dl4;
                pair = pair2;
                obj = a;
                dl42 = dl43;
                java.lang.Integer num = (java.lang.Integer) obj;
                g = this.this$0.f23674m;
                if (g != null) {
                }
                i = 8;
                this.this$0.f23678q.mo26881a(new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c(java.lang.Math.max(num != null ? num.intValue() : 0, i / 16), i, arrayList), (java.util.ArrayList) pair.getSecond());
                com.fossil.blesdk.obfuscated.qa4 qa4 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                dl42.mo26536a((java.lang.Object) null);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } catch (Throwable th) {
                th = th;
            }
        } else if (i2 == 3) {
            arrayList = (java.util.ArrayList) this.L$3;
            pair = (kotlin.Pair) this.L$2;
            dl42 = (com.fossil.blesdk.obfuscated.dl4) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            try {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                java.lang.Integer num2 = (java.lang.Integer) obj;
                g = this.this$0.f23674m;
                if (g != null) {
                    java.lang.Integer a4 = com.fossil.blesdk.obfuscated.dc4.m20843a(g.getGoalTarget() / 16);
                    if (a4 != null) {
                        i = a4.intValue();
                        this.this$0.f23678q.mo26881a(new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c(java.lang.Math.max(num2 != null ? num2.intValue() : 0, i / 16), i, arrayList), (java.util.ArrayList) pair.getSecond());
                        com.fossil.blesdk.obfuscated.qa4 qa42 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                        dl42.mo26536a((java.lang.Object) null);
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                }
                i = 8;
                this.this$0.f23678q.mo26881a(new com.portfolio.platform.p007ui.view.chart.base.BarChart.C6232c(java.lang.Math.max(num2 != null ? num2.intValue() : 0, i / 16), i, arrayList), (java.util.ArrayList) pair.getSecond());
                com.fossil.blesdk.obfuscated.qa4 qa422 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                dl42.mo26536a((java.lang.Object) null);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } catch (Throwable th2) {
                th = th2;
                dl4 = dl42;
            }
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.ug4 a5 = this.this$0.mo31440b();
        com.portfolio.platform.uirenew.home.details.goaltracking.C6713x357cb28 goalTrackingDetailPresenter$showDetailChart$1$invokeSuspend$$inlined$withLock$lambda$1 = new com.portfolio.platform.uirenew.home.details.goaltracking.C6713x357cb28((com.fossil.blesdk.obfuscated.yb4) null, this);
        this.L$0 = zg42;
        this.L$1 = dl4;
        this.label = 2;
        java.lang.Object a6 = com.fossil.blesdk.obfuscated.yf4.m30997a(a5, goalTrackingDetailPresenter$showDetailChart$1$invokeSuspend$$inlined$withLock$lambda$1, this);
        if (a6 == a2) {
            return a2;
        }
        java.lang.Object obj2 = a6;
        zg4 = zg42;
        obj = obj2;
        kotlin.Pair pair22 = (kotlin.Pair) obj;
        java.util.ArrayList arrayList22 = (java.util.ArrayList) pair22.getFirst();
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local2.mo33255d("GoalTrackingDetailPresenter", "showDetailChart - date=" + this.this$0.f23668g + ", data=" + arrayList22);
        com.fossil.blesdk.obfuscated.ug4 a32 = this.this$0.mo31440b();
        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1$1$maxValue$1 goalTrackingDetailPresenter$showDetailChart$1$1$maxValue$12 = new com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1$1$maxValue$1(arrayList22, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.L$1 = dl4;
        this.L$2 = pair22;
        this.L$3 = arrayList22;
        this.label = 3;
        a = com.fossil.blesdk.obfuscated.yf4.m30997a(a32, goalTrackingDetailPresenter$showDetailChart$1$1$maxValue$12, this);
        if (a != a2) {
        }
        dl4.mo26536a((java.lang.Object) null);
        throw th;
    }
}
