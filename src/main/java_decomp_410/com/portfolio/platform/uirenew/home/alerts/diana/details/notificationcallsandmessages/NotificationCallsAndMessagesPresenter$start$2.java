package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2", mo27670f = "NotificationCallsAndMessagesPresenter.kt", mo27671l = {143, 150}, mo27672m = "invokeSuspend")
public final class NotificationCallsAndMessagesPresenter$start$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22401p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$1", mo27670f = "NotificationCallsAndMessagesPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$1 */
    public static final class C63311 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22402p$;

        @DexIgnore
        public C63311(com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2.C63311 r0 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2.C63311(yb4);
            r0.f22402p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2.C63311) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34463J();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$a")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$a */
    public static final class C6332a implements com.portfolio.platform.CoroutineUseCase.C5606e<com.fossil.blesdk.obfuscated.px2.C4879d, com.fossil.blesdk.obfuscated.px2.C4877b> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2 f22403a;

        @DexIgnore
        public C6332a(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2 notificationCallsAndMessagesPresenter$start$2) {
            this.f22403a = notificationCallsAndMessagesPresenter$start$2;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(com.fossil.blesdk.obfuscated.px2.C4879d dVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a();
            local.mo33255d(a, "GetAllContactGroup onSuccess, size = " + dVar.mo30277a().size());
            this.f22403a.this$0.f22384t.mo29530l(com.fossil.blesdk.obfuscated.kb4.m24381d(dVar.mo30277a()));
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a();
            local2.mo33255d(a2, "mFlagGetListFavoriteFirstLoad=" + this.f22403a.this$0.mo40832l());
            if (this.f22403a.this$0.mo40832l()) {
                this.f22403a.this$0.mo40829c((java.util.List<com.fossil.wearables.fsl.contact.ContactGroup>) com.fossil.blesdk.obfuscated.kb4.m24381d(dVar.mo30277a()));
                for (com.fossil.wearables.fsl.contact.ContactGroup contactGroup : dVar.mo30277a()) {
                    java.util.List<com.fossil.wearables.fsl.contact.Contact> contacts = contactGroup.getContacts();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contacts, "contactGroup.contacts");
                    if (!contacts.isEmpty()) {
                        com.fossil.wearables.fsl.contact.Contact contact = contactGroup.getContacts().get(0);
                        com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper contactWrapper = new com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper(contact, (java.lang.String) null, 2, (com.fossil.blesdk.obfuscated.fd4) null);
                        contactWrapper.setAdded(true);
                        com.fossil.wearables.fsl.contact.Contact contact2 = contactWrapper.getContact();
                        if (contact2 != null) {
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact, "contact");
                            contact2.setDbRowId(contact.getDbRowId());
                        }
                        com.fossil.wearables.fsl.contact.Contact contact3 = contactWrapper.getContact();
                        if (contact3 != null) {
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact, "contact");
                            contact3.setUseSms(contact.isUseSms());
                        }
                        com.fossil.wearables.fsl.contact.Contact contact4 = contactWrapper.getContact();
                        if (contact4 != null) {
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact, "contact");
                            contact4.setUseCall(contact.isUseCall());
                        }
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact, "contact");
                        java.util.List<com.fossil.wearables.fsl.contact.PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) phoneNumbers, "contact.phoneNumbers");
                        if (!phoneNumbers.isEmpty()) {
                            com.fossil.wearables.fsl.contact.PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) phoneNumber, "contact.phoneNumbers[0]");
                            if (!android.text.TextUtils.isEmpty(phoneNumber.getNumber())) {
                                contactWrapper.setHasPhoneNumber(true);
                                com.fossil.wearables.fsl.contact.PhoneNumber phoneNumber2 = contact.getPhoneNumbers().get(0);
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) phoneNumber2, "contact.phoneNumbers[0]");
                                contactWrapper.setPhoneNumber(phoneNumber2.getNumber());
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                java.lang.String a3 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a();
                                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                                sb.append(".Inside loadContactData filter selected contact, phoneNumber=");
                                com.fossil.wearables.fsl.contact.PhoneNumber phoneNumber3 = contact.getPhoneNumbers().get(0);
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) phoneNumber3, "contact.phoneNumbers[0]");
                                sb.append(phoneNumber3.getNumber());
                                local3.mo33255d(a3, sb.toString());
                            }
                        }
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a4 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a();
                        local4.mo33255d(a4, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                        this.f22403a.this$0.mo40834n().add(contactWrapper);
                    }
                }
                this.f22403a.this$0.mo40827b(false);
            }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a5 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a();
            local5.mo33255d(a5, "start, mListFavoriteContactWrapperFirstLoad=" + this.f22403a.this$0.mo40834n() + " size=" + this.f22403a.this$0.mo40834n().size());
        }

        @DexIgnore
        /* renamed from: a */
        public void mo29641a(com.fossil.blesdk.obfuscated.px2.C4877b bVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(bVar, "errorValue");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a(), "GetAllContactGroup onError");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$b")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$b */
    public static final class C6333b implements com.portfolio.platform.CoroutineUseCase.C5606e<com.fossil.blesdk.obfuscated.vy2.C5306a, com.portfolio.platform.CoroutineUseCase.C5602a> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2 f22404a;

        @DexIgnore
        public C6333b(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2 notificationCallsAndMessagesPresenter$start$2) {
            this.f22404a = notificationCallsAndMessagesPresenter$start$2;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(com.fossil.blesdk.obfuscated.vy2.C5306a aVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(aVar, "responseValue");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a(), "GetApps onSuccess");
            this.f22404a.this$0.mo40833m().clear();
            this.f22404a.this$0.mo40833m().addAll(aVar.mo31940a());
        }

        @DexIgnore
        /* renamed from: a */
        public void mo29641a(com.portfolio.platform.CoroutineUseCase.C5602a aVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(aVar, "errorValue");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a(), "GetApps onError");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationCallsAndMessagesPresenter$start$2(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = notificationCallsAndMessagesPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2 notificationCallsAndMessagesPresenter$start$2 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2(this.this$0, yb4);
        notificationCallsAndMessagesPresenter$start$2.f22401p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationCallsAndMessagesPresenter$start$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b6  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.util.List<com.portfolio.platform.data.model.NotificationSettingsModel> list;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f22401p$;
            if (!com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34574u().mo26976N()) {
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2.C63311 r6 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2.C63311((com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r6, this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            list = (java.util.List) obj;
            if (!list.isEmpty()) {
                java.util.ArrayList arrayList = new java.util.ArrayList();
                com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel = new com.portfolio.platform.data.model.NotificationSettingsModel("AllowCallsFrom", 0, true);
                com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel2 = new com.portfolio.platform.data.model.NotificationSettingsModel("AllowMessagesFrom", 0, false);
                arrayList.add(notificationSettingsModel);
                arrayList.add(notificationSettingsModel2);
                this.this$0.mo40826b((java.util.List<com.portfolio.platform.data.model.NotificationSettingsModel>) arrayList);
                java.lang.String a3 = this.this$0.mo40823a(0);
                this.this$0.f22384t.mo29531m(a3);
                this.this$0.f22384t.mo29529i(a3);
            } else {
                for (com.portfolio.platform.data.model.NotificationSettingsModel notificationSettingsModel3 : list) {
                    int component2 = notificationSettingsModel3.component2();
                    if (notificationSettingsModel3.component3()) {
                        java.lang.String a4 = this.this$0.mo40823a(component2);
                        if (this.this$0.f22375k) {
                            this.this$0.mo40828c(component2);
                            this.this$0.f22375k = false;
                        }
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a5 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a();
                        local.mo33255d(a5, "start, mAlowCallsFromFirstLoad=" + this.this$0.mo40831k());
                        this.this$0.f22384t.mo29531m(a4);
                    } else {
                        java.lang.String a6 = this.this$0.mo40823a(component2);
                        if (this.this$0.f22377m) {
                            this.this$0.mo40825b(component2);
                            this.this$0.f22377m = false;
                        }
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a7 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a();
                        local2.mo33255d(a7, "start, mAllowMessagesFromFirsLoad=" + this.this$0.mo40830j());
                        this.this$0.f22384t.mo29529i(a6);
                    }
                }
            }
            this.this$0.f22386v.mo34435a(null, new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2.C6332a(this));
            this.this$0.f22389y.mo34435a(null, new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2.C6333b(this));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (this.this$0.f22382r) {
            this.this$0.f22382r = false;
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$settings$1 notificationCallsAndMessagesPresenter$start$2$settings$1 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$settings$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 2;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b, notificationCallsAndMessagesPresenter$start$2$settings$1, this);
            if (obj == a) {
                return a;
            }
            list = (java.util.List) obj;
            if (!list.isEmpty()) {
            }
        }
        this.this$0.f22386v.mo34435a(null, new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2.C6332a(this));
        this.this$0.f22389y.mo34435a(null, new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2.C6333b(this));
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
