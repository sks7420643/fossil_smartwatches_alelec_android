package com.portfolio.platform.uirenew.home.dashboard.activity;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.a93;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.obfuscated.xk2;
import com.fossil.blesdk.obfuscated.y83;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.z83;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DashboardActivityPresenter extends y83 implements PagingRequestHelper.a {
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public Listing<ActivitySummary> g;
    @DexIgnore
    public Unit h;
    @DexIgnore
    public /* final */ z83 i;
    @DexIgnore
    public /* final */ SummariesRepository j;
    @DexIgnore
    public /* final */ FitnessDataRepository k;
    @DexIgnore
    public /* final */ ActivitySummaryDao l;
    @DexIgnore
    public /* final */ FitnessDatabase m;
    @DexIgnore
    public /* final */ UserRepository n;
    @DexIgnore
    public /* final */ h42 o;
    @DexIgnore
    public /* final */ xk2 p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public DashboardActivityPresenter(z83 z83, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, UserRepository userRepository, h42 h42, xk2 xk2) {
        kd4.b(z83, "mView");
        kd4.b(summariesRepository, "mSummariesRepository");
        kd4.b(fitnessDataRepository, "mFitnessDataRepository");
        kd4.b(activitySummaryDao, "mActivitySummaryDao");
        kd4.b(fitnessDatabase, "mFitnessDatabase");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(h42, "mAppExecutors");
        kd4.b(xk2, "mFitnessHelper");
        this.i = z83;
        this.j = summariesRepository;
        this.k = fitnessDataRepository;
        this.l = activitySummaryDao;
        this.m = fitnessDatabase;
        this.n = userRepository;
        this.o = h42;
        this.p = xk2;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.W.c().e());
    }

    @DexIgnore
    public void f() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DashboardActivityPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardActivityPresenter", "stop");
    }

    @DexIgnore
    public void h() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DashboardActivityPresenter$initDataSource$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        try {
            Listing<ActivitySummary> listing = this.g;
            if (listing != null) {
                LiveData<qd<ActivitySummary>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    z83 z83 = this.i;
                    if (z83 != null) {
                        pagedList.a((LifecycleOwner) (a93) z83);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment");
                    }
                }
            }
            this.j.removePagingListener();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(qa4.a);
            local.e("DashboardActivityPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void j() {
        FLogger.INSTANCE.getLocal().d("DashboardActivityPresenter", "retry all failed request");
        Listing<ActivitySummary> listing = this.g;
        if (listing != null) {
            wc4<qa4> retry = listing.getRetry();
            if (retry != null) {
                qa4 invoke = retry.invoke();
            }
        }
    }

    @DexIgnore
    public void k() {
        this.i.a(this);
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        kd4.b(eVar, "report");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActivityPresenter", "onStatusChange status=" + eVar);
        if (eVar.a()) {
            this.i.f();
        }
    }
}
