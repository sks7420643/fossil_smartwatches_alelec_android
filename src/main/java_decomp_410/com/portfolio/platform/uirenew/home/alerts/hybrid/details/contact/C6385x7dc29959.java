package com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1$populateContacts$1", mo27670f = "NotificationHybridContactPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1$populateContacts$1 */
public final class C6385x7dc29959 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22571p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6385x7dc29959(com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1 notificationHybridContactPresenter$start$1$2$onSuccess$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = notificationHybridContactPresenter$start$1$2$onSuccess$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.C6385x7dc29959 notificationHybridContactPresenter$start$1$2$onSuccess$1$populateContacts$1 = new com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.C6385x7dc29959(this.this$0, yb4);
        notificationHybridContactPresenter$start$1$2$onSuccess$1$populateContacts$1.f22571p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationHybridContactPresenter$start$1$2$onSuccess$1$populateContacts$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.C6385x7dc29959) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            for (com.fossil.wearables.fsl.contact.ContactGroup contactGroup : this.this$0.$successResponse.mo31075a()) {
                for (com.fossil.wearables.fsl.contact.Contact next : contactGroup.getContacts()) {
                    com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper contactWrapper = new com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper(next, (java.lang.String) null, 2, (com.fossil.blesdk.obfuscated.fd4) null);
                    contactWrapper.setAdded(true);
                    com.fossil.wearables.fsl.contact.Contact contact = contactWrapper.getContact();
                    if (contact != null) {
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) next, "contact");
                        contact.setDbRowId(next.getDbRowId());
                        contact.setUseSms(next.isUseSms());
                        contact.setUseCall(next.isUseCall());
                    }
                    contactWrapper.setCurrentHandGroup(contactGroup.getHour());
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) next, "contact");
                    java.util.List<com.fossil.wearables.fsl.contact.PhoneNumber> phoneNumbers = next.getPhoneNumbers();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) phoneNumbers, "contact.phoneNumbers");
                    if (!phoneNumbers.isEmpty()) {
                        com.fossil.wearables.fsl.contact.PhoneNumber phoneNumber = next.getPhoneNumbers().get(0);
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) phoneNumber, "contact.phoneNumbers[0]");
                        java.lang.String number = phoneNumber.getNumber();
                        if (!android.text.TextUtils.isEmpty(number)) {
                            contactWrapper.setHasPhoneNumber(true);
                            contactWrapper.setPhoneNumber(number);
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String a = com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter.f22559n.mo40926a();
                            local.mo33255d(a, " filter selected contact, phoneNumber=" + number);
                        }
                    }
                    java.util.Iterator it = this.this$0.this$0.f22569a.this$0.f22563i.iterator();
                    int i = 0;
                    while (true) {
                        if (!it.hasNext()) {
                            i = -1;
                            break;
                        }
                        com.fossil.wearables.fsl.contact.Contact contact2 = ((com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper) it.next()).getContact();
                        if (com.fossil.blesdk.obfuscated.dc4.m20839a(contact2 != null && contact2.getContactId() == next.getContactId()).booleanValue()) {
                            break;
                        }
                        i++;
                    }
                    if (i != -1) {
                        contactWrapper.setCurrentHandGroup(this.this$0.this$0.f22569a.this$0.f22562h);
                        com.fossil.blesdk.obfuscated.kd4.m24407a(this.this$0.this$0.f22569a.this$0.f22563i.remove(i), "mContactWrappersSelected\u2026moveAt(indexContactFound)");
                    } else if (contactWrapper.getCurrentHandGroup() == this.this$0.this$0.f22569a.this$0.f22562h) {
                    }
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String a2 = com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter.f22559n.mo40926a();
                    local2.mo33255d(a2, ".Inside loadContactData filter selected contact, rowId = " + next.getDbRowId() + ", isUseText = " + next.isUseSms() + ", isUseCall = " + next.isUseCall());
                    this.this$0.this$0.f22569a.this$0.mo40924j().add(contactWrapper);
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
