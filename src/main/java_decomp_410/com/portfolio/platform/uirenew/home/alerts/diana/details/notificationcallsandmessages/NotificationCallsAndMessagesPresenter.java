package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages;

import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.bn2;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.dx2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lw2;
import com.fossil.blesdk.obfuscated.mw2;
import com.fossil.blesdk.obfuscated.nw2;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.rx2;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.sx2;
import com.fossil.blesdk.obfuscated.vy2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationCallsAndMessagesPresenter extends lw2 {
    @DexIgnore
    public static /* final */ String B;
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public /* final */ NotificationSettingsDatabase A;
    @DexIgnore
    public List<AppNotificationFilter> f; // = new ArrayList();
    @DexIgnore
    public List<AppWrapper> g; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> h; // = new ArrayList();
    @DexIgnore
    public /* final */ b i; // = new b();
    @DexIgnore
    public int j;
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean m; // = true;
    @DexIgnore
    public boolean n; // = true;
    @DexIgnore
    public /* final */ List<ContactWrapper> o; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> p; // = new ArrayList();
    @DexIgnore
    public /* final */ LiveData<List<NotificationSettingsModel>> q; // = this.z.getListNotificationSettings();
    @DexIgnore
    public boolean r; // = true;
    @DexIgnore
    public dx2 s;
    @DexIgnore
    public /* final */ mw2 t;
    @DexIgnore
    public /* final */ j62 u;
    @DexIgnore
    public /* final */ px2 v;
    @DexIgnore
    public /* final */ rx2 w;
    @DexIgnore
    public /* final */ sx2 x;
    @DexIgnore
    public /* final */ vy2 y;
    @DexIgnore
    public /* final */ NotificationSettingsDao z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationCallsAndMessagesPresenter.B;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements BleCommandResultManager.b {
        @DexIgnore
        public b() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:0x012f, code lost:
            if (r8 != 1113) goto L_0x015f;
         */
        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            kd4.b(communicateMode, "communicateMode");
            kd4.b(intent, "intent");
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), "SetNotificationFilterReceiver");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationCallsAndMessagesPresenter.C.a();
            local.d(a2, "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
            if (communicateMode != CommunicateMode.SET_NOTIFICATION_FILTERS) {
                return;
            }
            if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), "onReceive - success");
                NotificationCallsAndMessagesPresenter.this.t.a();
                NotificationCallsAndMessagesPresenter.this.t.close();
                return;
            }
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), "onReceive - failed");
            int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
            if (integerArrayListExtra == null) {
                integerArrayListExtra = new ArrayList<>(intExtra2);
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = NotificationCallsAndMessagesPresenter.C.a();
            local2.d(a3, "permissionErrorCodes=" + integerArrayListExtra + " , size=" + integerArrayListExtra.size());
            int size = integerArrayListExtra.size();
            for (int i = 0; i < size; i++) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = NotificationCallsAndMessagesPresenter.C.a();
                local3.d(a4, "error code " + i + " =" + integerArrayListExtra.get(i));
            }
            if (intExtra2 != 1101) {
                if (intExtra2 == 8888) {
                    NotificationCallsAndMessagesPresenter.this.t.c();
                } else if (intExtra2 != 1112) {
                }
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String a5 = NotificationCallsAndMessagesPresenter.C.a();
                local4.d(a5, "mAllowMessagesFromFirsLoad=" + NotificationCallsAndMessagesPresenter.this.j() + " mAlowCallsFromFirstLoad=" + NotificationCallsAndMessagesPresenter.this.k() + " mListFavoriteContactFirstLoad=" + NotificationCallsAndMessagesPresenter.this.n() + " size=" + NotificationCallsAndMessagesPresenter.this.n().size());
                ArrayList arrayList = new ArrayList();
                NotificationSettingsModel notificationSettingsModel = new NotificationSettingsModel("AllowCallsFrom", NotificationCallsAndMessagesPresenter.this.k(), true);
                NotificationSettingsModel notificationSettingsModel2 = new NotificationSettingsModel("AllowMessagesFrom", NotificationCallsAndMessagesPresenter.this.j(), false);
                arrayList.add(notificationSettingsModel);
                arrayList.add(notificationSettingsModel2);
                NotificationCallsAndMessagesPresenter.this.b((List<NotificationSettingsModel>) arrayList);
                NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter = NotificationCallsAndMessagesPresenter.this;
                notificationCallsAndMessagesPresenter.a(notificationCallsAndMessagesPresenter.n());
                return;
            }
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(integerArrayListExtra);
            kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
            mw2 l = NotificationCallsAndMessagesPresenter.this.t;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                l.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                ILocalFLogger local42 = FLogger.INSTANCE.getLocal();
                String a52 = NotificationCallsAndMessagesPresenter.C.a();
                local42.d(a52, "mAllowMessagesFromFirsLoad=" + NotificationCallsAndMessagesPresenter.this.j() + " mAlowCallsFromFirstLoad=" + NotificationCallsAndMessagesPresenter.this.k() + " mListFavoriteContactFirstLoad=" + NotificationCallsAndMessagesPresenter.this.n() + " size=" + NotificationCallsAndMessagesPresenter.this.n().size());
                ArrayList arrayList2 = new ArrayList();
                NotificationSettingsModel notificationSettingsModel3 = new NotificationSettingsModel("AllowCallsFrom", NotificationCallsAndMessagesPresenter.this.k(), true);
                NotificationSettingsModel notificationSettingsModel22 = new NotificationSettingsModel("AllowMessagesFrom", NotificationCallsAndMessagesPresenter.this.j(), false);
                arrayList2.add(notificationSettingsModel3);
                arrayList2.add(notificationSettingsModel22);
                NotificationCallsAndMessagesPresenter.this.b((List<NotificationSettingsModel>) arrayList2);
                NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter2 = NotificationCallsAndMessagesPresenter.this;
                notificationCallsAndMessagesPresenter2.a(notificationCallsAndMessagesPresenter2.n());
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements i62.d<rx2.c, i62.a> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ ContactGroup b;

        @DexIgnore
        public c(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, ContactGroup contactGroup) {
            this.a = notificationCallsAndMessagesPresenter;
            this.b = contactGroup;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(rx2.c cVar) {
            kd4.b(cVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), ".Inside mRemoveContactGroup onSuccess");
            this.a.t.a(this.b);
        }

        @DexIgnore
        public void a(i62.a aVar) {
            kd4.b(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), ".Inside mRemoveContactGroup onError");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements i62.d<sx2.c, i62.a> {
        @DexIgnore
        /* renamed from: a */
        public void onSuccess(sx2.c cVar) {
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), ".Inside mSaveContactGroupsNotification onSuccess");
        }

        @DexIgnore
        public void a(i62.a aVar) {
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.C.a(), ".Inside mSaveContactGroupsNotification onError");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements cc<dx2.a> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter a;

        @DexIgnore
        public e(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter) {
            this.a = notificationCallsAndMessagesPresenter;
        }

        @DexIgnore
        public final void a(dx2.a aVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationCallsAndMessagesPresenter.C.a();
            local.d(a2, "NotificationSettingChanged value = " + aVar);
            String a3 = this.a.a(aVar.a());
            if (aVar.b()) {
                this.a.t.m(a3);
            } else {
                this.a.t.i(a3);
            }
        }
    }

    /*
    static {
        String simpleName = NotificationCallsAndMessagesPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationCallsAndMess\u2026er::class.java.simpleName");
        B = simpleName;
    }
    */

    @DexIgnore
    public NotificationCallsAndMessagesPresenter(mw2 mw2, j62 j62, px2 px2, rx2 rx2, sx2 sx2, vy2 vy2, NotificationSettingsDao notificationSettingsDao, NotificationSettingsDatabase notificationSettingsDatabase) {
        kd4.b(mw2, "mView");
        kd4.b(j62, "mUseCaseHandler");
        kd4.b(px2, "mGetAllContactGroup");
        kd4.b(rx2, "mRemoveContactGroup");
        kd4.b(sx2, "mSaveContactGroupsNotification");
        kd4.b(vy2, "mGetApps");
        kd4.b(notificationSettingsDao, "mNotificationSettingDao");
        kd4.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.t = mw2;
        this.u = j62;
        this.v = px2;
        this.w = rx2;
        this.x = sx2;
        this.y = vy2;
        this.z = notificationSettingsDao;
        this.A = notificationSettingsDatabase;
    }

    @DexIgnore
    public final List<AppWrapper> m() {
        return this.g;
    }

    @DexIgnore
    public final List<ContactWrapper> n() {
        return this.o;
    }

    @DexIgnore
    public final boolean o() {
        return (this.t.E() == this.j && this.t.J() == this.l && !(kd4.a((Object) this.p, (Object) this.t.Q()) ^ true)) ? false : true;
    }

    @DexIgnore
    public final void p() {
        FLogger.INSTANCE.getLocal().d(B, "registerBroadcastReceiver");
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.i, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public void q() {
        this.t.a(this);
    }

    @DexIgnore
    public final void r() {
        FLogger.INSTANCE.getLocal().d(B, "unregisterBroadcastReceiver");
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.i, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(B, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        p();
        mw2 mw2 = this.t;
        if (mw2 != null) {
            FragmentActivity activity = ((nw2) mw2).getActivity();
            dx2 dx2 = this.s;
            if (dx2 != null) {
                dx2.c().a((LifecycleOwner) this.t, new e(this));
                bn2 bn2 = bn2.d;
                if (activity == null) {
                    kd4.a();
                    throw null;
                } else if (bn2.a(bn2, activity, "NOTIFICATION_CONTACTS", false, 4, (Object) null)) {
                    fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationCallsAndMessagesPresenter$start$Anon2(this, (yb4) null), 3, (Object) null);
                }
            } else {
                kd4.d("mNotificationSettingViewModel");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
        }
    }

    @DexIgnore
    public void g() {
        r();
        LiveData<List<NotificationSettingsModel>> liveData = this.q;
        mw2 mw2 = this.t;
        if (mw2 != null) {
            liveData.a((LifecycleOwner) (nw2) mw2);
            dx2 dx2 = this.s;
            if (dx2 != null) {
                dx2.c().a((LifecycleOwner) this.t);
                FLogger.INSTANCE.getLocal().d(B, "stop");
                return;
            }
            kd4.d("mNotificationSettingViewModel");
            throw null;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
    }

    @DexIgnore
    public void h() {
        this.t.a();
        this.t.close();
    }

    @DexIgnore
    public void i() {
        if (!o()) {
            FLogger.INSTANCE.getLocal().d(B, "setRuleToDevice, nothing changed");
            this.t.close();
            return;
        }
        bn2 bn2 = bn2.d;
        mw2 mw2 = this.t;
        if (mw2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
        } else if (bn2.a(bn2, ((nw2) mw2).getContext(), "SET_NOTIFICATION", false, 4, (Object) null)) {
            fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationCallsAndMessagesPresenter$setRuleToDevice$Anon1(this, (yb4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public final int j() {
        return this.l;
    }

    @DexIgnore
    public final int k() {
        return this.j;
    }

    @DexIgnore
    public final boolean l() {
        return this.n;
    }

    @DexIgnore
    public final void c(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public final void b(int i2) {
        this.l = i2;
    }

    @DexIgnore
    public final void c(List<ContactGroup> list) {
        kd4.b(list, "<set-?>");
        this.p = list;
    }

    @DexIgnore
    public final String a(int i2) {
        if (i2 == 0) {
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__Everyone);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return a2;
        } else if (i2 != 1) {
            String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__NoOne);
            kd4.a((Object) a3, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return a3;
        } else {
            String a4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__FavoriteContacts);
            kd4.a((Object) a4, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return a4;
        }
    }

    @DexIgnore
    public final void b(boolean z2) {
        this.n = z2;
    }

    @DexIgnore
    public final void b(List<NotificationSettingsModel> list) {
        FLogger.INSTANCE.getLocal().d(B, "saveNotificationSettings");
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationCallsAndMessagesPresenter$saveNotificationSettings$Anon1(this, list, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(List<ContactWrapper> list) {
        kd4.b(list, "mListFavoriteContactWrapperFirstLoad");
        FLogger.INSTANCE.getLocal().d(B, "revertListContactFavorite");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = B;
        local.d(str, "mListFavoriteContactWrapperFirstLoad=" + list + " size=" + list.size());
        int size = this.h.size();
        for (int i2 = 0; i2 < size; i2++) {
            ContactGroup contactGroup = this.h.get(i2);
            List<Contact> contacts = contactGroup.getContacts();
            kd4.a((Object) contacts, "item.contacts");
            if (!contacts.isEmpty()) {
                a(contactGroup);
            }
        }
        this.u.a(this.x, new sx2.b(new ArrayList(), list), new d());
    }

    @DexIgnore
    public void a(ContactGroup contactGroup) {
        kd4.b(contactGroup, "contactGroup");
        this.u.a(this.w, new rx2.b(contactGroup), new c(this, contactGroup));
    }

    @DexIgnore
    public void a(dx2 dx2) {
        kd4.b(dx2, "viewModel");
        this.s = dx2;
    }

    @DexIgnore
    public void a(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = B;
        local.d(str, "updateNotificationSetting isCall=" + z2);
        mw2 mw2 = this.t;
        dx2.a aVar = new dx2.a(z2 ? mw2.E() : mw2.J(), z2);
        dx2 dx2 = this.s;
        if (dx2 != null) {
            dx2.c().a(aVar);
            this.t.P();
            return;
        }
        kd4.d("mNotificationSettingViewModel");
        throw null;
    }
}
