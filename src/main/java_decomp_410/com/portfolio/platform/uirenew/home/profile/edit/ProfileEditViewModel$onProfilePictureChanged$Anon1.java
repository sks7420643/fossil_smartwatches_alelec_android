package com.portfolio.platform.uirenew.home.profile.edit;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.al2;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.vr3;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$Anon1", f = "ProfileEditViewModel.kt", l = {184}, m = "invokeSuspend")
public final class ProfileEditViewModel$onProfilePictureChanged$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Intent $data;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileEditViewModel this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileEditViewModel$onProfilePictureChanged$Anon1(ProfileEditViewModel profileEditViewModel, Intent intent, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = profileEditViewModel;
        this.$data = intent;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ProfileEditViewModel$onProfilePictureChanged$Anon1 profileEditViewModel$onProfilePictureChanged$Anon1 = new ProfileEditViewModel$onProfilePictureChanged$Anon1(this.this$Anon0, this.$data, yb4);
        profileEditViewModel$onProfilePictureChanged$Anon1.p$ = (zg4) obj;
        return profileEditViewModel$onProfilePictureChanged$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileEditViewModel$onProfilePictureChanged$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            Uri a2 = al2.a(this.$data, PortfolioApp.W.c());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = ProfileEditViewModel.l.a();
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .onActivityResult imageUri=");
            if (a2 != null) {
                sb.append(a2);
                local.d(a3, sb.toString());
                if (!PortfolioApp.W.c().a(this.$data, a2)) {
                    return qa4.a;
                }
                ProfileEditViewModel.a(this.this$Anon0, (MFUser) null, a2, (Boolean) null, (Pair) null, false, (MFUser) null, false, 125, (Object) null);
                ug4 b = nh4.b();
                ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1 profileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1 = new ProfileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1(a2, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = a2;
                this.label = 1;
                obj2 = yf4.a(b, profileEditViewModel$onProfilePictureChanged$Anon1$bitmap$Anon1, this);
                if (obj2 == a) {
                    return a;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else if (i == 1) {
            Uri uri = (Uri) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            try {
                na4.a(obj);
                obj2 = obj;
            } catch (Exception e) {
                e.printStackTrace();
                ProfileEditViewModel.a(this.this$Anon0, (MFUser) null, (Uri) null, (Boolean) null, (Pair) null, true, (MFUser) null, false, 111, (Object) null);
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Bitmap bitmap = (Bitmap) obj2;
        MFUser a4 = this.this$Anon0.d;
        if (a4 != null) {
            if (bitmap != null) {
                a4.setProfilePicture(vr3.a(bitmap));
            } else {
                kd4.a();
                throw null;
            }
        }
        this.this$Anon0.e = true;
        ProfileEditViewModel.a(this.this$Anon0, (MFUser) null, (Uri) null, dc4.a(this.this$Anon0.e()), (Pair) null, false, (MFUser) null, false, 123, (Object) null);
        return qa4.a;
    }
}
