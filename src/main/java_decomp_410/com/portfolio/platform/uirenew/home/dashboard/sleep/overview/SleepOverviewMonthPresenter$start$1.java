package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepOverviewMonthPresenter$start$1<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepDay>>> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter f23507a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1$1", mo27670f = "SleepOverviewMonthPresenter.kt", mo27671l = {60}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1$1 */
    public static final class C66691 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $data;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public java.lang.Object L$1;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23508p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1$1$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1$1$1", mo27670f = "SleepOverviewMonthPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1$1$1 */
        public static final class C66701 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.TreeMap<java.lang.Long, java.lang.Float>>, java.lang.Object> {
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f23509p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1.C66691 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C66701(com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1.C66691 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1.C66691.C66701 r0 = new com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1.C66691.C66701(this.this$0, yb4);
                r0.f23509p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1.C66691.C66701) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                com.fossil.blesdk.obfuscated.cc4.m20546a();
                if (this.label == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter sleepOverviewMonthPresenter = this.this$0.this$0.f23507a;
                    java.lang.Object a = sleepOverviewMonthPresenter.f23492f.mo2275a();
                    if (a != null) {
                        com.fossil.blesdk.obfuscated.kd4.m24407a(a, "mDate.value!!");
                        return sleepOverviewMonthPresenter.mo41343a((java.util.Date) a, (java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepDay>) this.this$0.$data);
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C66691(com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1 sleepOverviewMonthPresenter$start$1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = sleepOverviewMonthPresenter$start$1;
            this.$data = list;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1.C66691 r0 = new com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1.C66691(this.this$0, this.$data, yb4);
            r0.f23508p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1.C66691) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter sleepOverviewMonthPresenter;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23508p$;
                this.this$0.f23507a.f23498l = this.$data;
                com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter sleepOverviewMonthPresenter2 = this.this$0.f23507a;
                com.fossil.blesdk.obfuscated.ug4 a2 = sleepOverviewMonthPresenter2.mo31440b();
                com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1.C66691.C66701 r4 = new com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1.C66691.C66701(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = sleepOverviewMonthPresenter2;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r4, this);
                if (obj == a) {
                    return a;
                }
                sleepOverviewMonthPresenter = sleepOverviewMonthPresenter2;
            } else if (i == 1) {
                sleepOverviewMonthPresenter = (com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            sleepOverviewMonthPresenter.f23500n = (java.util.TreeMap) obj;
            com.fossil.blesdk.obfuscated.pd3 m = this.this$0.f23507a.f23501o;
            java.util.TreeMap d = this.this$0.f23507a.f23500n;
            if (d == null) {
                d = new java.util.TreeMap();
            }
            m.mo30108a(d);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public SleepOverviewMonthPresenter$start$1(com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
        this.f23507a = sleepOverviewMonthPresenter;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepDay>> os3) {
        java.lang.Integer num;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("mDateTransformations - status=");
        sb.append(os3 != null ? os3.mo29978f() : null);
        sb.append(" -- data.size=");
        if (os3 != null) {
            java.util.List list = (java.util.List) os3.mo29975d();
            if (list != null) {
                num = java.lang.Integer.valueOf(list.size());
                sb.append(num);
                local.mo33255d("SleepOverviewMonthPresenter", sb.toString());
                if ((os3 == null ? os3.mo29978f() : null) == com.portfolio.platform.enums.Status.DATABASE_LOADING) {
                    java.util.List list2 = os3 != null ? (java.util.List) os3.mo29975d() : null;
                    if (list2 != null && (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.f23507a.f23498l, (java.lang.Object) list2))) {
                        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23507a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$start$1.C66691(this, list2, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                        return;
                    }
                    return;
                }
                return;
            }
        }
        num = null;
        sb.append(num);
        local.mo33255d("SleepOverviewMonthPresenter", sb.toString());
        if ((os3 == null ? os3.mo29978f() : null) == com.portfolio.platform.enums.Status.DATABASE_LOADING) {
        }
    }
}
