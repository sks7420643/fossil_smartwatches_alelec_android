package com.portfolio.platform.uirenew.home.customize.domain.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase", mo27670f = "SetDianaPresetToWatchUseCase.kt", mo27671l = {96}, mo27672m = "setPresetToDb")
public final class SetDianaPresetToWatchUseCase$setPresetToDb$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetDianaPresetToWatchUseCase$setPresetToDb$1(com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = setDianaPresetToWatchUseCase;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.mo41110a((com.portfolio.platform.data.model.diana.preset.DianaPreset) null, (com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this);
    }
}
