package com.portfolio.platform.uirenew.home.customize.diana.theme;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $selectedPos$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ androidx.recyclerview.widget.RecyclerView $this_with;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$1(androidx.recyclerview.widget.RecyclerView recyclerView, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment customizeThemeFragment, int i) {
        super(2, yb4);
        this.$this_with = recyclerView;
        this.this$0 = customizeThemeFragment;
        this.$selectedPos$inlined = i;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$1 customizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$1(this.$this_with, yb4, this.this$0, this.$selectedPos$inlined);
        customizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return customizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.p$;
            long a2 = this.this$0.m;
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.ih4.a(a2, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.m = 0;
        this.$this_with.j(this.$selectedPos$inlined);
        return com.fossil.blesdk.obfuscated.qa4.a;
    }
}
