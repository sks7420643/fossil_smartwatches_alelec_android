package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$showDetailChart$Anon1", f = "SleepOverviewDayPresenter.kt", l = {159}, m = "invokeSuspend")
public final class SleepOverviewDayPresenter$showDetailChart$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepOverviewDayPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepOverviewDayPresenter$showDetailChart$Anon1(SleepOverviewDayPresenter sleepOverviewDayPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = sleepOverviewDayPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SleepOverviewDayPresenter$showDetailChart$Anon1 sleepOverviewDayPresenter$showDetailChart$Anon1 = new SleepOverviewDayPresenter$showDetailChart$Anon1(this.this$Anon0, yb4);
        sleepOverviewDayPresenter$showDetailChart$Anon1.p$ = (zg4) obj;
        return sleepOverviewDayPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepOverviewDayPresenter$showDetailChart$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            os3 os3 = (os3) this.this$Anon0.j.a();
            if (os3 != null) {
                List list = (List) os3.d();
                if (list != null) {
                    ug4 a2 = this.this$Anon0.b();
                    SleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 sleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new SleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(list, (yb4) null, this);
                    this.L$Anon0 = zg4;
                    this.L$Anon1 = list;
                    this.label = 1;
                    obj = yf4.a(a2, sleepOverviewDayPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$let$lambda$Anon1, this);
                    if (obj == a) {
                        return a;
                    }
                }
            }
            this.this$Anon0.k.s(new ArrayList());
            return qa4.a;
        } else if (i == 1) {
            List list2 = (List) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List list3 = (List) obj;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepOverviewDayPresenter", "showDetailChart - data=" + list3);
        this.this$Anon0.k.s(list3);
        return qa4.a;
    }
}
