package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.data.RemindTimeModel;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$IntRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$Anon1", f = "NotificationWatchRemindersPresenter.kt", l = {151, 160}, m = "invokeSuspend")
public final class NotificationWatchRemindersPresenter$save$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationWatchRemindersPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationWatchRemindersPresenter$save$Anon1(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = notificationWatchRemindersPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NotificationWatchRemindersPresenter$save$Anon1 notificationWatchRemindersPresenter$save$Anon1 = new NotificationWatchRemindersPresenter$save$Anon1(this.this$Anon0, yb4);
        notificationWatchRemindersPresenter$save$Anon1.p$ = (zg4) obj;
        return notificationWatchRemindersPresenter$save$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationWatchRemindersPresenter$save$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00f3  */
    public final Object invokeSuspend(Object obj) {
        boolean z;
        Ref$IntRef ref$IntRef;
        Object obj2;
        Ref$IntRef ref$IntRef2;
        RemindTimeModel remindTimeModel;
        short s;
        Object obj3;
        zg4 zg4;
        boolean z2;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            ref$IntRef2 = new Ref$IntRef();
            ref$IntRef2.element = 0;
            ref$IntRef = new Ref$IntRef();
            ref$IntRef.element = 0;
            z2 = this.this$Anon0.j;
            ug4 a2 = this.this$Anon0.c();
            NotificationWatchRemindersPresenter$save$Anon1$inactivityNudgeList$Anon1 notificationWatchRemindersPresenter$save$Anon1$inactivityNudgeList$Anon1 = new NotificationWatchRemindersPresenter$save$Anon1$inactivityNudgeList$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = ref$IntRef2;
            this.L$Anon2 = ref$IntRef;
            this.Z$Anon0 = z2;
            this.label = 1;
            obj3 = yf4.a(a2, notificationWatchRemindersPresenter$save$Anon1$inactivityNudgeList$Anon1, this);
            if (obj3 == a) {
                return a;
            }
        } else if (i == 1) {
            boolean z3 = this.Z$Anon0;
            ref$IntRef = (Ref$IntRef) this.L$Anon2;
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            obj3 = obj;
            z2 = z3;
            ref$IntRef2 = (Ref$IntRef) this.L$Anon1;
        } else if (i == 2) {
            List list = (List) this.L$Anon3;
            boolean z4 = this.Z$Anon0;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            z = z4;
            ref$IntRef = (Ref$IntRef) this.L$Anon2;
            ref$IntRef2 = (Ref$IntRef) this.L$Anon1;
            obj2 = obj;
            remindTimeModel = (RemindTimeModel) obj2;
            PortfolioApp c = PortfolioApp.W.c();
            String e = PortfolioApp.W.c().e();
            int i2 = ref$IntRef2.element;
            byte b = (byte) (i2 / 60);
            byte b2 = (byte) (i2 % 60);
            int i3 = ref$IntRef.element;
            byte b3 = (byte) (i3 / 60);
            byte b4 = (byte) (i3 % 60);
            if (remindTimeModel != null) {
                Integer a3 = dc4.a(remindTimeModel.getMinutes());
                if (a3 != null) {
                    Short a4 = dc4.a((short) a3.intValue());
                    if (a4 != null) {
                        s = a4.shortValue();
                        c.a(e, new InactiveNudgeData(b, b2, b3, b4, s, z));
                        this.this$Anon0.q.u(this.this$Anon0.j);
                        this.this$Anon0.q.v(this.this$Anon0.k);
                        this.this$Anon0.q.w(this.this$Anon0.l);
                        this.this$Anon0.q.t(this.this$Anon0.m);
                        this.this$Anon0.p.close();
                        return qa4.a;
                    }
                }
            }
            s = 0;
            c.a(e, new InactiveNudgeData(b, b2, b3, b4, s, z));
            this.this$Anon0.q.u(this.this$Anon0.j);
            this.this$Anon0.q.v(this.this$Anon0.k);
            this.this$Anon0.q.w(this.this$Anon0.l);
            this.this$Anon0.q.t(this.this$Anon0.m);
            this.this$Anon0.p.close();
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List<InactivityNudgeTimeModel> list2 = (List) obj3;
        for (InactivityNudgeTimeModel inactivityNudgeTimeModel : list2) {
            if (inactivityNudgeTimeModel.getNudgeTimeType() == 0) {
                ref$IntRef2.element = inactivityNudgeTimeModel.getMinutes();
            } else if (inactivityNudgeTimeModel.getNudgeTimeType() == 1) {
                ref$IntRef.element = inactivityNudgeTimeModel.getMinutes();
            }
        }
        ug4 a5 = this.this$Anon0.c();
        NotificationWatchRemindersPresenter$save$Anon1$remindMinutes$Anon1 notificationWatchRemindersPresenter$save$Anon1$remindMinutes$Anon1 = new NotificationWatchRemindersPresenter$save$Anon1$remindMinutes$Anon1(this, (yb4) null);
        this.L$Anon0 = zg4;
        this.L$Anon1 = ref$IntRef2;
        this.L$Anon2 = ref$IntRef;
        this.Z$Anon0 = z2;
        this.L$Anon3 = list2;
        this.label = 2;
        obj2 = yf4.a(a5, notificationWatchRemindersPresenter$save$Anon1$remindMinutes$Anon1, this);
        if (obj2 == a) {
            return a;
        }
        z = z2;
        remindTimeModel = (RemindTimeModel) obj2;
        PortfolioApp c2 = PortfolioApp.W.c();
        String e2 = PortfolioApp.W.c().e();
        int i22 = ref$IntRef2.element;
        byte b5 = (byte) (i22 / 60);
        byte b22 = (byte) (i22 % 60);
        int i32 = ref$IntRef.element;
        byte b32 = (byte) (i32 / 60);
        byte b42 = (byte) (i32 % 60);
        if (remindTimeModel != null) {
        }
        s = 0;
        c2.a(e2, new InactiveNudgeData(b5, b22, b32, b42, s, z));
        this.this$Anon0.q.u(this.this$Anon0.j);
        this.this$Anon0.q.v(this.this$Anon0.k);
        this.this$Anon0.q.w(this.this$Anon0.l);
        this.this$Anon0.q.t(this.this$Anon0.m);
        this.this$Anon0.p.close();
        return qa4.a;
    }
}
