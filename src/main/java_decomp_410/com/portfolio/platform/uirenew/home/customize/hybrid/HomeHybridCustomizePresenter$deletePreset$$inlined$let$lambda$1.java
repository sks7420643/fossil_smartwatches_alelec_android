package com.portfolio.platform.uirenew.home.customize.hybrid;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $nextActivePresetId$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.room.microapp.HybridPreset $preset;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22964p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1$1")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1$1 */
    public static final class C65201 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22965p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C65201(com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1 homeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = homeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1.C65201 r0 = new com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1.C65201(this.this$0, yb4);
            r0.f22965p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1.C65201) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22965p$;
                com.portfolio.platform.data.source.HybridPresetRepository e = this.this$0.this$0.f22954q;
                java.lang.String id = this.this$0.$preset.getId();
                this.L$0 = zg4;
                this.label = 1;
                if (e.deletePresetById(id, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1(com.portfolio.platform.data.model.room.microapp.HybridPreset hybridPreset, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter homeHybridCustomizePresenter, java.lang.String str) {
        super(2, yb4);
        this.$preset = hybridPreset;
        this.this$0 = homeHybridCustomizePresenter;
        this.$nextActivePresetId$inlined = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1 homeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1(this.$preset, yb4, this.this$0, this.$nextActivePresetId$inlined);
        homeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1.f22964p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22964p$;
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1.C65201 r3 = new com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$deletePreset$$inlined$let$lambda$1.C65201(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.f22952o.mo33003c(this.this$0.mo41136j() - 1);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
