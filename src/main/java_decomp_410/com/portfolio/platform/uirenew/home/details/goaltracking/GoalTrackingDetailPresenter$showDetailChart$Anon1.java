package com.portfolio.platform.uirenew.home.details.goaltracking;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.dl4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$Anon1", f = "GoalTrackingDetailPresenter.kt", l = {262, 229, 232}, m = "invokeSuspend")
public final class GoalTrackingDetailPresenter$showDetailChart$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDetailPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDetailPresenter$showDetailChart$Anon1(GoalTrackingDetailPresenter goalTrackingDetailPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = goalTrackingDetailPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        GoalTrackingDetailPresenter$showDetailChart$Anon1 goalTrackingDetailPresenter$showDetailChart$Anon1 = new GoalTrackingDetailPresenter$showDetailChart$Anon1(this.this$Anon0, yb4);
        goalTrackingDetailPresenter$showDetailChart$Anon1.p$ = (zg4) obj;
        return goalTrackingDetailPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingDetailPresenter$showDetailChart$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00cf A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00df A[Catch:{ all -> 0x0027 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fc A[Catch:{ all -> 0x0027 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0101 A[Catch:{ all -> 0x0027 }] */
    public final Object invokeSuspend(Object obj) {
        dl4 dl4;
        dl4 dl42;
        Pair pair;
        ArrayList arrayList;
        GoalTrackingSummary g;
        int i;
        zg4 zg4;
        Object a;
        zg4 zg42;
        Object a2 = cc4.a();
        int i2 = this.label;
        if (i2 == 0) {
            na4.a(obj);
            zg42 = this.p$;
            dl4 = this.this$Anon0.k;
            this.L$Anon0 = zg42;
            this.L$Anon1 = dl4;
            this.label = 1;
            if (dl4.a((Object) null, this) == a2) {
                return a2;
            }
        } else if (i2 == 1) {
            dl4 = (dl4) this.L$Anon1;
            na4.a(obj);
            zg42 = (zg4) this.L$Anon0;
        } else if (i2 == 2) {
            dl4 = (dl4) this.L$Anon1;
            zg4 = (zg4) this.L$Anon0;
            try {
                na4.a(obj);
                Pair pair2 = (Pair) obj;
                ArrayList arrayList2 = (ArrayList) pair2.getFirst();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("GoalTrackingDetailPresenter", "showDetailChart - date=" + this.this$Anon0.g + ", data=" + arrayList2);
                ug4 a3 = this.this$Anon0.b();
                GoalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon1 goalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon1 = new GoalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon1(arrayList2, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = dl4;
                this.L$Anon2 = pair2;
                this.L$Anon3 = arrayList2;
                this.label = 3;
                a = yf4.a(a3, goalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon1, this);
                if (a != a2) {
                    return a2;
                }
                arrayList = arrayList2;
                dl4 dl43 = dl4;
                pair = pair2;
                obj = a;
                dl42 = dl43;
                Integer num = (Integer) obj;
                g = this.this$Anon0.m;
                if (g != null) {
                }
                i = 8;
                this.this$Anon0.q.a(new BarChart.c(Math.max(num != null ? num.intValue() : 0, i / 16), i, arrayList), (ArrayList) pair.getSecond());
                qa4 qa4 = qa4.a;
                dl42.a((Object) null);
                return qa4.a;
            } catch (Throwable th) {
                th = th;
            }
        } else if (i2 == 3) {
            arrayList = (ArrayList) this.L$Anon3;
            pair = (Pair) this.L$Anon2;
            dl42 = (dl4) this.L$Anon1;
            zg4 zg43 = (zg4) this.L$Anon0;
            try {
                na4.a(obj);
                Integer num2 = (Integer) obj;
                g = this.this$Anon0.m;
                if (g != null) {
                    Integer a4 = dc4.a(g.getGoalTarget() / 16);
                    if (a4 != null) {
                        i = a4.intValue();
                        this.this$Anon0.q.a(new BarChart.c(Math.max(num2 != null ? num2.intValue() : 0, i / 16), i, arrayList), (ArrayList) pair.getSecond());
                        qa4 qa42 = qa4.a;
                        dl42.a((Object) null);
                        return qa4.a;
                    }
                }
                i = 8;
                this.this$Anon0.q.a(new BarChart.c(Math.max(num2 != null ? num2.intValue() : 0, i / 16), i, arrayList), (ArrayList) pair.getSecond());
                qa4 qa422 = qa4.a;
                dl42.a((Object) null);
                return qa4.a;
            } catch (Throwable th2) {
                th = th2;
                dl4 = dl42;
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ug4 a5 = this.this$Anon0.b();
        GoalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 goalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 = new GoalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1((yb4) null, this);
        this.L$Anon0 = zg42;
        this.L$Anon1 = dl4;
        this.label = 2;
        Object a6 = yf4.a(a5, goalTrackingDetailPresenter$showDetailChart$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1, this);
        if (a6 == a2) {
            return a2;
        }
        Object obj2 = a6;
        zg4 = zg42;
        obj = obj2;
        Pair pair22 = (Pair) obj;
        ArrayList arrayList22 = (ArrayList) pair22.getFirst();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("GoalTrackingDetailPresenter", "showDetailChart - date=" + this.this$Anon0.g + ", data=" + arrayList22);
        ug4 a32 = this.this$Anon0.b();
        GoalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon1 goalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon12 = new GoalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon1(arrayList22, (yb4) null);
        this.L$Anon0 = zg4;
        this.L$Anon1 = dl4;
        this.L$Anon2 = pair22;
        this.L$Anon3 = arrayList22;
        this.label = 3;
        a = yf4.a(a32, goalTrackingDetailPresenter$showDetailChart$Anon1$Anon1$maxValue$Anon12, this);
        if (a != a2) {
        }
        dl4.a((Object) null);
        throw th;
    }
}
