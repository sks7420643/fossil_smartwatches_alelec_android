package com.portfolio.platform.uirenew.home.details.heartrate;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1", mo27670f = "HeartRateDetailPresenter.kt", mo27671l = {192}, mo27672m = "invokeSuspend")
public final class HeartRateDetailPresenter$showDayDetail$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23727p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$showDayDetail$1(com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter heartRateDetailPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = heartRateDetailPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1 heartRateDetailPresenter$showDayDetail$1 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1(this.this$0, yb4);
        heartRateDetailPresenter$showDayDetail$1.f23727p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return heartRateDetailPresenter$showDayDetail$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x007a  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        int i;
        com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary h;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i2 = this.label;
        if (i2 == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23727p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1$summary$1 heartRateDetailPresenter$showDayDetail$1$summary$1 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1$summary$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, heartRateDetailPresenter$showDayDetail$1$summary$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i2 == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary dailyHeartRateSummary = (com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary) obj;
        if (this.this$0.f23707k == null || (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23707k, (java.lang.Object) dailyHeartRateSummary))) {
            this.this$0.f23707k = dailyHeartRateSummary;
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary h2 = this.this$0.f23707k;
            int i3 = 0;
            if (h2 != null) {
                com.portfolio.platform.data.model.diana.heartrate.Resting resting = h2.getResting();
                if (resting != null) {
                    java.lang.Integer a3 = com.fossil.blesdk.obfuscated.dc4.m20843a(resting.getValue());
                    if (a3 != null) {
                        i = a3.intValue();
                        h = this.this$0.f23707k;
                        if (h != null) {
                            java.lang.Integer a4 = com.fossil.blesdk.obfuscated.dc4.m20843a(h.getMax());
                            if (a4 != null) {
                                i3 = a4.intValue();
                            }
                        }
                        this.this$0.f23713q.mo29611c(i, i3);
                    }
                }
            }
            i = 0;
            h = this.this$0.f23707k;
            if (h != null) {
            }
            this.this$0.f23713q.mo29611c(i, i3);
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
