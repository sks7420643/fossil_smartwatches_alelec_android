package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1", mo27670f = "NotificationWatchRemindersPresenter.kt", mo27671l = {151, 160}, mo27672m = "invokeSuspend")
public final class NotificationWatchRemindersPresenter$save$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22456p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationWatchRemindersPresenter$save$1(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter notificationWatchRemindersPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = notificationWatchRemindersPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1 notificationWatchRemindersPresenter$save$1 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1(this.this$0, yb4);
        notificationWatchRemindersPresenter$save$1.f22456p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationWatchRemindersPresenter$save$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00f3  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        boolean z;
        kotlin.jvm.internal.Ref$IntRef ref$IntRef;
        java.lang.Object obj2;
        kotlin.jvm.internal.Ref$IntRef ref$IntRef2;
        com.portfolio.platform.data.RemindTimeModel remindTimeModel;
        short s;
        java.lang.Object obj3;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        boolean z2;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f22456p$;
            ref$IntRef2 = new kotlin.jvm.internal.Ref$IntRef();
            ref$IntRef2.element = 0;
            ref$IntRef = new kotlin.jvm.internal.Ref$IntRef();
            ref$IntRef.element = 0;
            z2 = this.this$0.f22445j;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1$inactivityNudgeList$1 notificationWatchRemindersPresenter$save$1$inactivityNudgeList$1 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1$inactivityNudgeList$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = ref$IntRef2;
            this.L$2 = ref$IntRef;
            this.Z$0 = z2;
            this.label = 1;
            obj3 = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, notificationWatchRemindersPresenter$save$1$inactivityNudgeList$1, this);
            if (obj3 == a) {
                return a;
            }
        } else if (i == 1) {
            boolean z3 = this.Z$0;
            ref$IntRef = (kotlin.jvm.internal.Ref$IntRef) this.L$2;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            obj3 = obj;
            z2 = z3;
            ref$IntRef2 = (kotlin.jvm.internal.Ref$IntRef) this.L$1;
        } else if (i == 2) {
            java.util.List list = (java.util.List) this.L$3;
            boolean z4 = this.Z$0;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            z = z4;
            ref$IntRef = (kotlin.jvm.internal.Ref$IntRef) this.L$2;
            ref$IntRef2 = (kotlin.jvm.internal.Ref$IntRef) this.L$1;
            obj2 = obj;
            remindTimeModel = (com.portfolio.platform.data.RemindTimeModel) obj2;
            com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
            java.lang.String e = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e();
            int i2 = ref$IntRef2.element;
            byte b = (byte) (i2 / 60);
            byte b2 = (byte) (i2 % 60);
            int i3 = ref$IntRef.element;
            byte b3 = (byte) (i3 / 60);
            byte b4 = (byte) (i3 % 60);
            if (remindTimeModel != null) {
                java.lang.Integer a3 = com.fossil.blesdk.obfuscated.dc4.m20843a(remindTimeModel.getMinutes());
                if (a3 != null) {
                    java.lang.Short a4 = com.fossil.blesdk.obfuscated.dc4.m20845a((short) a3.intValue());
                    if (a4 != null) {
                        s = a4.shortValue();
                        com.misfit.frameworks.buttonservice.model.InactiveNudgeData inactiveNudgeData = new com.misfit.frameworks.buttonservice.model.InactiveNudgeData(b, b2, b3, b4, s, z);
                        c.mo34477a(e, inactiveNudgeData);
                        this.this$0.f22452q.mo27083u(this.this$0.f22445j);
                        this.this$0.f22452q.mo27086v(this.this$0.f22446k);
                        this.this$0.f22452q.mo27089w(this.this$0.f22447l);
                        this.this$0.f22452q.mo27080t(this.this$0.f22448m);
                        this.this$0.f22451p.close();
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                }
            }
            s = 0;
            com.misfit.frameworks.buttonservice.model.InactiveNudgeData inactiveNudgeData2 = new com.misfit.frameworks.buttonservice.model.InactiveNudgeData(b, b2, b3, b4, s, z);
            c.mo34477a(e, inactiveNudgeData2);
            this.this$0.f22452q.mo27083u(this.this$0.f22445j);
            this.this$0.f22452q.mo27086v(this.this$0.f22446k);
            this.this$0.f22452q.mo27089w(this.this$0.f22447l);
            this.this$0.f22452q.mo27080t(this.this$0.f22448m);
            this.this$0.f22451p.close();
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        java.util.List<com.portfolio.platform.data.InactivityNudgeTimeModel> list2 = (java.util.List) obj3;
        for (com.portfolio.platform.data.InactivityNudgeTimeModel inactivityNudgeTimeModel : list2) {
            if (inactivityNudgeTimeModel.getNudgeTimeType() == 0) {
                ref$IntRef2.element = inactivityNudgeTimeModel.getMinutes();
            } else if (inactivityNudgeTimeModel.getNudgeTimeType() == 1) {
                ref$IntRef.element = inactivityNudgeTimeModel.getMinutes();
            }
        }
        com.fossil.blesdk.obfuscated.ug4 a5 = this.this$0.mo31441c();
        com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1$remindMinutes$1 notificationWatchRemindersPresenter$save$1$remindMinutes$1 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1$remindMinutes$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.L$1 = ref$IntRef2;
        this.L$2 = ref$IntRef;
        this.Z$0 = z2;
        this.L$3 = list2;
        this.label = 2;
        obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(a5, notificationWatchRemindersPresenter$save$1$remindMinutes$1, this);
        if (obj2 == a) {
            return a;
        }
        z = z2;
        remindTimeModel = (com.portfolio.platform.data.RemindTimeModel) obj2;
        com.portfolio.platform.PortfolioApp c2 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
        java.lang.String e2 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e();
        int i22 = ref$IntRef2.element;
        byte b5 = (byte) (i22 / 60);
        byte b22 = (byte) (i22 % 60);
        int i32 = ref$IntRef.element;
        byte b32 = (byte) (i32 / 60);
        byte b42 = (byte) (i32 % 60);
        if (remindTimeModel != null) {
        }
        s = 0;
        com.misfit.frameworks.buttonservice.model.InactiveNudgeData inactiveNudgeData22 = new com.misfit.frameworks.buttonservice.model.InactiveNudgeData(b5, b22, b32, b42, s, z);
        c2.mo34477a(e2, inactiveNudgeData22);
        this.this$0.f22452q.mo27083u(this.this$0.f22445j);
        this.this$0.f22452q.mo27086v(this.this$0.f22446k);
        this.this$0.f22452q.mo27089w(this.this$0.f22447l);
        this.this$0.f22452q.mo27080t(this.this$0.f22448m);
        this.this$0.f22451p.close();
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
