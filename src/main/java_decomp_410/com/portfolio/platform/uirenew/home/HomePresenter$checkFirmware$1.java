package com.portfolio.platform.uirenew.home;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$1", mo27670f = "HomePresenter.kt", mo27671l = {265}, mo27672m = "invokeSuspend")
public final class HomePresenter$checkFirmware$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $activeDeviceSerial;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22291p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.HomePresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomePresenter$checkFirmware$1(com.portfolio.platform.uirenew.home.HomePresenter homePresenter, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homePresenter;
        this.$activeDeviceSerial = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$1 homePresenter$checkFirmware$1 = new com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$1(this.this$0, this.$activeDeviceSerial, yb4);
        homePresenter$checkFirmware$1.f22291p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homePresenter$checkFirmware$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22291p$;
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.HomePresenter.f22267y.mo40748a(), "checkFirmware() enter checkFirmware");
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$1$isLatestFw$1 homePresenter$checkFirmware$1$isLatestFw$1 = new com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$1$isLatestFw$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, homePresenter$checkFirmware$1$isLatestFw$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        boolean booleanValue = ((java.lang.Boolean) obj).booleanValue();
        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
        com.misfit.frameworks.buttonservice.log.FLogger.Component component = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP;
        com.misfit.frameworks.buttonservice.log.FLogger.Session session = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER;
        java.lang.String str = this.$activeDeviceSerial;
        java.lang.String a3 = com.portfolio.platform.uirenew.home.HomePresenter.f22267y.mo40748a();
        remote.mo33266i(component, session, str, a3, "[Sync OK] Is device has latest FW " + booleanValue);
        if (booleanValue) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.HomePresenter.f22267y.mo40748a(), "checkFirmware() fw is latest, skip OTA after sync success");
        } else {
            this.this$0.mo40747p();
        }
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.HomePresenter.f22267y.mo40748a(), "checkFirmware() exit checkFirmware");
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
