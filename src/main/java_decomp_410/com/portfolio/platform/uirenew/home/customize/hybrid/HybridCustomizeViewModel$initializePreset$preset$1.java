package com.portfolio.platform.uirenew.home.customize.hybrid;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initializePreset$preset$1", mo27670f = "HybridCustomizeViewModel.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class HybridCustomizeViewModel$initializePreset$preset$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.room.microapp.HybridPreset>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $presetId;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22999p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridCustomizeViewModel$initializePreset$preset$1(com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel hybridCustomizeViewModel, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = hybridCustomizeViewModel;
        this.$presetId = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initializePreset$preset$1 hybridCustomizeViewModel$initializePreset$preset$1 = new com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initializePreset$preset$1(this.this$0, this.$presetId, yb4);
        hybridCustomizeViewModel$initializePreset$preset$1.f22999p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return hybridCustomizeViewModel$initializePreset$preset$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initializePreset$preset$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return this.this$0.f22987o.getPresetById(this.$presetId);
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
