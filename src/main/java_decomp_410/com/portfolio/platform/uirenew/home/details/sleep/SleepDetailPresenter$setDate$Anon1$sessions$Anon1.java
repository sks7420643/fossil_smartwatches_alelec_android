package com.portfolio.platform.uirenew.home.details.sleep;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$Anon1$sessions$Anon1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class SleepDetailPresenter$setDate$Anon1$sessions$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<MFSleepSession>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepDetailPresenter$setDate$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepDetailPresenter$setDate$Anon1$sessions$Anon1(SleepDetailPresenter$setDate$Anon1 sleepDetailPresenter$setDate$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = sleepDetailPresenter$setDate$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SleepDetailPresenter$setDate$Anon1$sessions$Anon1 sleepDetailPresenter$setDate$Anon1$sessions$Anon1 = new SleepDetailPresenter$setDate$Anon1$sessions$Anon1(this.this$Anon0, yb4);
        sleepDetailPresenter$setDate$Anon1$sessions$Anon1.p$ = (zg4) obj;
        return sleepDetailPresenter$setDate$Anon1$sessions$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepDetailPresenter$setDate$Anon1$sessions$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            SleepDetailPresenter sleepDetailPresenter = this.this$Anon0.this$Anon0;
            return sleepDetailPresenter.a(sleepDetailPresenter.g, (List<MFSleepSession>) this.this$Anon0.this$Anon0.m);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
