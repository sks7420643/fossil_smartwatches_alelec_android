package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fp4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.hb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.t03;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1", f = "NotificationContactsAndAppsAssignedPresenter.kt", l = {129}, m = "invokeSuspend")
public final class NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ t03.d $responseValue;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1(NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1 notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1, t03.d dVar, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1 notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1 = new NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1(this.this$Anon0, this.$responseValue, yb4);
        notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1.p$ = (zg4) obj;
        return notificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        List list;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.x.a(), "mGetAllHybridContactGroups onSuccess");
            ArrayList arrayList = new ArrayList();
            gh4 a2 = ag4.a(zg4, this.this$Anon0.a.b(), (CoroutineStart) null, new NotificationContactsAndAppsAssignedPresenter$loadContactData$Anon1$onSuccess$Anon1$populateContact$Anon1(this, arrayList, (yb4) null), 2, (Object) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = arrayList;
            this.L$Anon2 = a2;
            this.label = 1;
            if (a2.a(this) == a) {
                return a;
            }
            list = arrayList;
        } else if (i == 1) {
            gh4 gh4 = (gh4) this.L$Anon2;
            list = (List) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.a.o().addAll(list);
        this.this$Anon0.a.q().addAll(list);
        List<Object> p = this.this$Anon0.a.p();
        Object[] array = list.toArray(new ContactWrapper[0]);
        if (array != null) {
            Serializable a3 = fp4.a((Serializable) array);
            kd4.a((Object) a3, "SerializationUtils.clone\u2026apperList.toTypedArray())");
            hb4.a(p, (T[]) (Object[]) a3);
            this.this$Anon0.a.t();
            return qa4.a;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
