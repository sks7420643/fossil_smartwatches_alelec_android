package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$2$1$listRestingDataPoint$1", mo27670f = "HeartRateOverviewWeekPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$2$1$listRestingDataPoint$1 */
public final class C6656x32e31cac extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<? extends java.lang.Integer>>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23453p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1.C66542.C66551 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6656x32e31cac(com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1.C66542.C66551 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = r1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.C6656x32e31cac heartRateOverviewWeekPresenter$start$1$2$1$listRestingDataPoint$1 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.C6656x32e31cac(this.this$0, yb4);
        heartRateOverviewWeekPresenter$start$1$2$1$listRestingDataPoint$1.f23453p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return heartRateOverviewWeekPresenter$start$1$2$1$listRestingDataPoint$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.C6656x32e31cac) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.util.List<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary> list = this.this$0.$data;
            if (list == null) {
                return null;
            }
            java.util.ArrayList arrayList = new java.util.ArrayList(com.fossil.blesdk.obfuscated.db4.m20831a(list, 10));
            for (com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary resting : list) {
                com.portfolio.platform.data.model.diana.heartrate.Resting resting2 = resting.getResting();
                arrayList.add(resting2 != null ? com.fossil.blesdk.obfuscated.dc4.m20843a(resting2.getValue()) : null);
            }
            return arrayList;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
