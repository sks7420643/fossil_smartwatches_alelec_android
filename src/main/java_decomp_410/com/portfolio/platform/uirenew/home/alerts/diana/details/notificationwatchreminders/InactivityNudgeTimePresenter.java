package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import android.content.Context;
import android.text.format.DateFormat;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.tx2;
import com.fossil.blesdk.obfuscated.ux2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class InactivityNudgeTimePresenter extends tx2 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public InactivityNudgeTimeModel h;
    @DexIgnore
    public InactivityNudgeTimeModel i;
    @DexIgnore
    public /* final */ ux2 j;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = InactivityNudgeTimePresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "InactivityNudgeTimePrese\u2026er::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public InactivityNudgeTimePresenter(ux2 ux2, RemindersSettingsDatabase remindersSettingsDatabase) {
        kd4.b(ux2, "mView");
        kd4.b(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        this.j = ux2;
        this.k = remindersSettingsDatabase;
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(l, "stop");
    }

    @DexIgnore
    public void h() {
        if (this.g == 0) {
            int i2 = this.f;
            InactivityNudgeTimeModel inactivityNudgeTimeModel = this.i;
            if (inactivityNudgeTimeModel == null) {
                kd4.a();
                throw null;
            } else if (i2 > inactivityNudgeTimeModel.getMinutes()) {
                String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_MoveAlerts_Error_Text__SelectAnEndTimeThatIs);
                ux2 ux2 = this.j;
                kd4.a((Object) a2, "des");
                ux2.u(a2);
            } else {
                InactivityNudgeTimeModel inactivityNudgeTimeModel2 = this.h;
                if (inactivityNudgeTimeModel2 != null) {
                    inactivityNudgeTimeModel2.setMinutes(this.f);
                    fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new InactivityNudgeTimePresenter$save$Anon1(this, (yb4) null), 3, (Object) null);
                    return;
                }
                kd4.a();
                throw null;
            }
        } else {
            int i3 = this.f;
            InactivityNudgeTimeModel inactivityNudgeTimeModel3 = this.h;
            if (inactivityNudgeTimeModel3 == null) {
                kd4.a();
                throw null;
            } else if (i3 < inactivityNudgeTimeModel3.getMinutes()) {
                String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_MoveAlerts_Error_Text__SelectAnEndTimeThatIs);
                ux2 ux22 = this.j;
                kd4.a((Object) a3, "des");
                ux22.u(a3);
            } else {
                InactivityNudgeTimeModel inactivityNudgeTimeModel4 = this.i;
                if (inactivityNudgeTimeModel4 != null) {
                    inactivityNudgeTimeModel4.setMinutes(this.f);
                    fi4 unused2 = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new InactivityNudgeTimePresenter$save$Anon2(this, (yb4) null), 3, (Object) null);
                    return;
                }
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void i() {
        this.j.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(l, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new InactivityNudgeTimePresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final String b(int i2) {
        if (i2 == 0) {
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_MoveAlerts_Main_Text__Start);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026eAlerts_Main_Text__Start)");
            return a2;
        }
        String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_MoveAlerts_Main_Text__End);
        kd4.a((Object) a3, "LanguageHelper.getString\u2026oveAlerts_Main_Text__End)");
        return a3;
    }

    @DexIgnore
    public void a(int i2) {
        this.g = i2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0085  */
    public void a(String str, String str2, boolean z) {
        int i2;
        int i3;
        kd4.b(str, "hourValue");
        kd4.b(str2, "minuteValue");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = l;
        local.d(str3, "updateTime: hourValue = " + str + ", minuteValue = " + str2 + ", isPM = " + z);
        int i4 = 0;
        try {
            Integer valueOf = Integer.valueOf(str);
            kd4.a((Object) valueOf, "Integer.valueOf(hourValue)");
            i3 = valueOf.intValue();
            try {
                Integer valueOf2 = Integer.valueOf(str2);
                kd4.a((Object) valueOf2, "Integer.valueOf(minuteValue)");
                i2 = valueOf2.intValue();
            } catch (Exception e) {
                e = e;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str4 = l;
                local2.e(str4, "Exception when parse time e=" + e);
                i2 = 0;
                if (DateFormat.is24HourFormat(PortfolioApp.W.c())) {
                }
            }
        } catch (Exception e2) {
            e = e2;
            i3 = 0;
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            String str42 = l;
            local22.e(str42, "Exception when parse time e=" + e);
            i2 = 0;
            if (DateFormat.is24HourFormat(PortfolioApp.W.c())) {
            }
        }
        if (DateFormat.is24HourFormat(PortfolioApp.W.c())) {
            this.f = (i3 * 60) + i2;
            return;
        }
        if (z) {
            i4 = i3 == 12 ? 12 : i3 + 12;
        } else if (i3 != 12) {
            i4 = i3;
        }
        this.f = (i4 * 60) + i2;
    }
}
