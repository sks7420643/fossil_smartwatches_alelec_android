package com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1", mo27670f = "NotificationHybridEveryonePresenter.kt", mo27671l = {42}, mo27672m = "invokeSuspend")
public final class NotificationHybridEveryonePresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22584p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1$1", mo27670f = "NotificationHybridEveryonePresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1$1 */
    public static final class C63891 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22585p$;

        @DexIgnore
        public C63891(com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1.C63891 r0 = new com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1.C63891(yb4);
            r0.f22585p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1.C63891) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34463J();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1$a")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1$a */
    public static final class C6390a implements com.fossil.blesdk.obfuscated.i62.C4439d<com.fossil.blesdk.obfuscated.t03.C5127d, com.fossil.blesdk.obfuscated.t03.C5125b> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1 f22586a;

        @DexIgnore
        public C6390a(com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1 notificationHybridEveryonePresenter$start$1) {
            this.f22586a = notificationHybridEveryonePresenter$start$1;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(com.fossil.blesdk.obfuscated.t03.C5127d dVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "successResponse");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter.f22575m.mo40933a(), "GetAllContactGroup onSuccess");
            for (com.fossil.wearables.fsl.contact.ContactGroup contactGroup : dVar.mo31075a()) {
                for (com.fossil.wearables.fsl.contact.Contact next : contactGroup.getContacts()) {
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) next, "contact");
                    if (next.getContactId() == -100 || next.getContactId() == -200) {
                        com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper contactWrapper = new com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper(next, (java.lang.String) null, 2, (com.fossil.blesdk.obfuscated.fd4) null);
                        contactWrapper.setAdded(true);
                        com.fossil.wearables.fsl.contact.Contact contact = contactWrapper.getContact();
                        if (contact != null) {
                            contact.setDbRowId(next.getDbRowId());
                            contact.setUseSms(next.isUseSms());
                            contact.setUseCall(next.isUseCall());
                        }
                        contactWrapper.setCurrentHandGroup(contactGroup.getHour());
                        java.util.List<com.fossil.wearables.fsl.contact.PhoneNumber> phoneNumbers = next.getPhoneNumbers();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) phoneNumbers, "contact.phoneNumbers");
                        if (!phoneNumbers.isEmpty()) {
                            com.fossil.wearables.fsl.contact.PhoneNumber phoneNumber = next.getPhoneNumbers().get(0);
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) phoneNumber, "contact.phoneNumbers[0]");
                            java.lang.String number = phoneNumber.getNumber();
                            if (!android.text.TextUtils.isEmpty(number)) {
                                contactWrapper.setHasPhoneNumber(true);
                                contactWrapper.setPhoneNumber(number);
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                java.lang.String a = com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter.f22575m.mo40933a();
                                local.mo33255d(a, " filter selected contact, phoneNumber=" + number);
                            }
                        }
                        java.util.Iterator it = this.f22586a.this$0.f22579i.iterator();
                        int i = 0;
                        while (true) {
                            if (!it.hasNext()) {
                                i = -1;
                                break;
                            }
                            com.fossil.wearables.fsl.contact.Contact contact2 = ((com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper) it.next()).getContact();
                            if (contact2 != null && contact2.getContactId() == next.getContactId()) {
                                break;
                            }
                            i++;
                        }
                        if (i != -1) {
                            contactWrapper.setCurrentHandGroup(this.f22586a.this$0.f22578h);
                            this.f22586a.this$0.f22579i.remove(i);
                        }
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a2 = com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter.f22575m.mo40933a();
                        local2.mo33255d(a2, ".Inside loadContactData filter selected contact, rowId = " + next.getDbRowId() + ", isUseText = " + next.isUseSms() + ", isUseCall = " + next.isUseCall());
                        this.f22586a.this$0.mo40931m().add(contactWrapper);
                    }
                }
            }
            if (!this.f22586a.this$0.f22579i.isEmpty()) {
                for (com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper add : this.f22586a.this$0.f22579i) {
                    this.f22586a.this$0.mo40931m().add(add);
                }
            }
            this.f22586a.this$0.f22577g.mo32196b(this.f22586a.this$0.mo40931m(), this.f22586a.this$0.f22578h);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo28250a(com.fossil.blesdk.obfuscated.t03.C5125b bVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(bVar, "errorResponse");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter.f22575m.mo40933a(), "GetAllContactGroup onError");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationHybridEveryonePresenter$start$1(com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter notificationHybridEveryonePresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = notificationHybridEveryonePresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1 notificationHybridEveryonePresenter$start$1 = new com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1(this.this$0, yb4);
        notificationHybridEveryonePresenter$start$1.f22584p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationHybridEveryonePresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22584p$;
            if (!com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34574u().mo26976N()) {
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1.C63891 r4 = new com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1.C63891((com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r4, this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (this.this$0.mo40931m().isEmpty()) {
            this.this$0.f22580j.mo28479a(this.this$0.f22581k, null, new com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1.C6390a(this));
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
