package com.portfolio.platform.uirenew.home.details.heartrate;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1", mo27670f = "HeartRateDetailPresenter.kt", mo27671l = {143}, mo27672m = "invokeSuspend")
public final class HeartRateDetailPresenter$setDate$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $date;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23725p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1$1", mo27670f = "HeartRateDetailPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1$1 */
    public static final class C67241 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.Date>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23726p$;

        @DexIgnore
        public C67241(com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1.C67241 r0 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1.C67241(yb4);
            r0.f23726p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1.C67241) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34546k();
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$setDate$1(com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter heartRateDetailPresenter, java.util.Date date, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = heartRateDetailPresenter;
        this.$date = date;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1 heartRateDetailPresenter$setDate$1 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1(this.this$0, this.$date, yb4);
        heartRateDetailPresenter$setDate$1.f23725p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return heartRateDetailPresenter$setDate$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        android.util.Pair<java.util.Date, java.util.Date> a;
        kotlin.Pair pair;
        com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter heartRateDetailPresenter;
        java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23725p$;
            if (this.this$0.f23702f == null) {
                com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter heartRateDetailPresenter2 = this.this$0;
                com.fossil.blesdk.obfuscated.ug4 a3 = heartRateDetailPresenter2.mo31440b();
                com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1.C67241 r4 = new com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1.C67241((com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = heartRateDetailPresenter2;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a3, r4, this);
                if (obj == a2) {
                    return a2;
                }
                heartRateDetailPresenter = heartRateDetailPresenter2;
            }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("HeartRateDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.f23702f);
            this.this$0.f23703g = this.$date;
            java.lang.Boolean s = com.fossil.blesdk.obfuscated.rk2.m27414s(this.$date);
            com.fossil.blesdk.obfuscated.nf3 m = this.this$0.f23713q;
            java.util.Date date = this.$date;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) s, "isToday");
            m.mo29609a(date, com.fossil.blesdk.obfuscated.rk2.m27391c(this.this$0.f23702f, this.$date), s.booleanValue(), !com.fossil.blesdk.obfuscated.rk2.m27391c(new java.util.Date(), this.$date));
            a = com.fossil.blesdk.obfuscated.rk2.m27355a(this.$date, this.this$0.f23702f);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            pair = (kotlin.Pair) this.this$0.f23704h.mo2275a();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local2.mo33255d("HeartRateDetailPresenter", "setDate - rangeDateValue=" + pair + ", newRange=" + new kotlin.Pair(a.first, a.second));
            if (pair != null || !com.fossil.blesdk.obfuscated.rk2.m27396d((java.util.Date) pair.getFirst(), (java.util.Date) a.first) || !com.fossil.blesdk.obfuscated.rk2.m27396d((java.util.Date) pair.getSecond(), (java.util.Date) a.second)) {
                this.this$0.f23704h.mo2280a(new kotlin.Pair(a.first, a.second));
            } else {
                com.fossil.blesdk.obfuscated.fi4 unused = this.this$0.mo41421l();
                com.fossil.blesdk.obfuscated.fi4 unused2 = this.this$0.mo41422m();
                com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter heartRateDetailPresenter3 = this.this$0;
                heartRateDetailPresenter3.mo41419c(heartRateDetailPresenter3.f23703g);
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            heartRateDetailPresenter = (com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        heartRateDetailPresenter.f23702f = (java.util.Date) obj;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local3.mo33255d("HeartRateDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.f23702f);
        this.this$0.f23703g = this.$date;
        java.lang.Boolean s2 = com.fossil.blesdk.obfuscated.rk2.m27414s(this.$date);
        com.fossil.blesdk.obfuscated.nf3 m2 = this.this$0.f23713q;
        java.util.Date date2 = this.$date;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) s2, "isToday");
        m2.mo29609a(date2, com.fossil.blesdk.obfuscated.rk2.m27391c(this.this$0.f23702f, this.$date), s2.booleanValue(), !com.fossil.blesdk.obfuscated.rk2.m27391c(new java.util.Date(), this.$date));
        a = com.fossil.blesdk.obfuscated.rk2.m27355a(this.$date, this.this$0.f23702f);
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
        pair = (kotlin.Pair) this.this$0.f23704h.mo2275a();
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local22 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local22.mo33255d("HeartRateDetailPresenter", "setDate - rangeDateValue=" + pair + ", newRange=" + new kotlin.Pair(a.first, a.second));
        if (pair != null) {
        }
        this.this$0.f23704h.mo2280a(new kotlin.Pair(a.first, a.second));
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
