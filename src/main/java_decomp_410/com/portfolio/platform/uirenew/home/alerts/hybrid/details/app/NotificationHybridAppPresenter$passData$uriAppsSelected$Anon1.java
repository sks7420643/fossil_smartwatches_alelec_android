package com.portfolio.platform.uirenew.home.alerts.hybrid.details.app;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationHybridAppPresenter$passData$uriAppsSelected$Anon1 extends Lambda implements xc4<AppWrapper, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationHybridAppPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationHybridAppPresenter$passData$uriAppsSelected$Anon1(NotificationHybridAppPresenter notificationHybridAppPresenter) {
        super(1);
        this.this$Anon0 = notificationHybridAppPresenter;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(invoke((AppWrapper) obj));
    }

    @DexIgnore
    public final boolean invoke(AppWrapper appWrapper) {
        kd4.b(appWrapper, "it");
        InstalledApp installedApp = appWrapper.getInstalledApp();
        Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
        if (isSelected != null) {
            return isSelected.booleanValue() && appWrapper.getCurrentHandGroup() == this.this$Anon0.h;
        }
        kd4.a();
        throw null;
    }
}
