package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.f13;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$Anon1", f = "HomeDianaCustomizePresenter.kt", l = {333}, m = "invokeSuspend")
public final class HomeDianaCustomizePresenter$showPresets$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$showPresets$Anon1(HomeDianaCustomizePresenter homeDianaCustomizePresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = homeDianaCustomizePresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeDianaCustomizePresenter$showPresets$Anon1 homeDianaCustomizePresenter$showPresets$Anon1 = new HomeDianaCustomizePresenter$showPresets$Anon1(this.this$Anon0, yb4);
        homeDianaCustomizePresenter$showPresets$Anon1.p$ = (zg4) obj;
        return homeDianaCustomizePresenter$showPresets$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDianaCustomizePresenter$showPresets$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 c = this.this$Anon0.b();
            HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1 homeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1 = new HomeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(c, homeDianaCustomizePresenter$showPresets$Anon1$wrapperUIData$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.t.c((List<f13>) (List) obj);
        return qa4.a;
    }
}
