package com.portfolio.platform.uirenew.home.details.sleep;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.xc4;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import java.util.Date;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepDetailPresenter$findSleepSessions$Anon1 extends Lambda implements xc4<MFSleepSession, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepDetailPresenter$findSleepSessions$Anon1(Date date) {
        super(1);
        this.$date = date;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(invoke((MFSleepSession) obj));
    }

    @DexIgnore
    public final boolean invoke(MFSleepSession mFSleepSession) {
        kd4.b(mFSleepSession, "it");
        return rk2.d(mFSleepSession.getDay(), this.$date);
    }
}
