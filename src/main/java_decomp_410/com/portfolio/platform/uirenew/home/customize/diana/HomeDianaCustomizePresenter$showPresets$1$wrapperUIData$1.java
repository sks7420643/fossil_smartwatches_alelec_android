package com.portfolio.platform.uirenew.home.customize.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1$wrapperUIData$1", mo27670f = "HomeDianaCustomizePresenter.kt", mo27671l = {394}, mo27672m = "invokeSuspend")
public final class HomeDianaCustomizePresenter$showPresets$1$wrapperUIData$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<? extends com.fossil.blesdk.obfuscated.f13>>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22700p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$showPresets$1$wrapperUIData$1(com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1 homeDianaCustomizePresenter$showPresets$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homeDianaCustomizePresenter$showPresets$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1$wrapperUIData$1 homeDianaCustomizePresenter$showPresets$1$wrapperUIData$1 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1$wrapperUIData$1(this.this$0, yb4);
        homeDianaCustomizePresenter$showPresets$1$wrapperUIData$1.f22700p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeDianaCustomizePresenter$showPresets$1$wrapperUIData$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1$wrapperUIData$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.dl4 dl4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22700p$;
            com.fossil.blesdk.obfuscated.dl4 l = this.this$0.this$0.f22675q;
            this.L$0 = zg4;
            this.L$1 = l;
            this.label = 1;
            if (l.mo26535a((java.lang.Object) null, this) == a) {
                return a;
            }
            dl4 = l;
        } else if (i == 1) {
            dl4 = (com.fossil.blesdk.obfuscated.dl4) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            java.util.concurrent.CopyOnWriteArrayList<com.portfolio.platform.data.model.diana.preset.DianaPreset> m = this.this$0.this$0.f22668j;
            java.util.ArrayList arrayList = new java.util.ArrayList(com.fossil.blesdk.obfuscated.db4.m20831a(m, 10));
            for (com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset : m) {
                com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.this$0.this$0;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) dianaPreset, "it");
                arrayList.add(homeDianaCustomizePresenter.mo40993a(dianaPreset));
            }
            return arrayList;
        } finally {
            dl4.mo26536a((java.lang.Object) null);
        }
    }
}
