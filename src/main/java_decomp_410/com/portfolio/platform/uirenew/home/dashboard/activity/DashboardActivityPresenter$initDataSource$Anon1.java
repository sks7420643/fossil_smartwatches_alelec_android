package com.portfolio.platform.uirenew.home.dashboard.activity;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.a93;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.xk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.z83;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.enums.Unit;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$initDataSource$Anon1", f = "DashboardActivityPresenter.kt", l = {89}, m = "invokeSuspend")
public final class DashboardActivityPresenter$initDataSource$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DashboardActivityPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements cc<qd<ActivitySummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ DashboardActivityPresenter$initDataSource$Anon1 a;

        @DexIgnore
        public a(DashboardActivityPresenter$initDataSource$Anon1 dashboardActivityPresenter$initDataSource$Anon1) {
            this.a = dashboardActivityPresenter$initDataSource$Anon1;
        }

        @DexIgnore
        public final void a(qd<ActivitySummary> qdVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("getSummariesPaging observer size=");
            sb.append(qdVar != null ? Integer.valueOf(qdVar.size()) : null);
            local.d("DashboardActivityPresenter", sb.toString());
            if (qdVar != null) {
                this.a.this$Anon0.i.a(qdVar);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardActivityPresenter$initDataSource$Anon1(DashboardActivityPresenter dashboardActivityPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = dashboardActivityPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DashboardActivityPresenter$initDataSource$Anon1 dashboardActivityPresenter$initDataSource$Anon1 = new DashboardActivityPresenter$initDataSource$Anon1(this.this$Anon0, yb4);
        dashboardActivityPresenter$initDataSource$Anon1.p$ = (zg4) obj;
        return dashboardActivityPresenter$initDataSource$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DashboardActivityPresenter$initDataSource$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 a3 = this.this$Anon0.c();
            DashboardActivityPresenter$initDataSource$Anon1$user$Anon1 dashboardActivityPresenter$initDataSource$Anon1$user$Anon1 = new DashboardActivityPresenter$initDataSource$Anon1$user$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a3, dashboardActivityPresenter$initDataSource$Anon1$user$Anon1, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser = (MFUser) obj;
        if (mFUser != null) {
            Date d = rk2.d(mFUser.getCreatedAt());
            DashboardActivityPresenter dashboardActivityPresenter = this.this$Anon0;
            SummariesRepository j = dashboardActivityPresenter.j;
            SummariesRepository j2 = this.this$Anon0.j;
            xk2 i2 = this.this$Anon0.p;
            FitnessDataRepository g = this.this$Anon0.k;
            ActivitySummaryDao c = this.this$Anon0.l;
            FitnessDatabase h = this.this$Anon0.m;
            kd4.a((Object) d, "createdDate");
            dashboardActivityPresenter.g = j.getSummariesPaging(j2, i2, g, c, h, d, this.this$Anon0.o, this.this$Anon0);
            Listing b = this.this$Anon0.g;
            if (b != null) {
                LiveData pagedList = b.getPagedList();
                if (pagedList != null) {
                    z83 l = this.this$Anon0.i;
                    if (l != null) {
                        pagedList.a((a93) l, new a(this));
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment");
                    }
                }
            }
            z83 l2 = this.this$Anon0.i;
            Unit distanceUnit = mFUser.getDistanceUnit();
            if (distanceUnit == null) {
                distanceUnit = Unit.METRIC;
            }
            l2.b(distanceUnit);
        }
        return qa4.a;
    }
}
