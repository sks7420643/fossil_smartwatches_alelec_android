package com.portfolio.platform.uirenew.home.customize.diana.watchapps;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$1 */
public final class C6483x1f25a914<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter f22855a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$1$1", mo27670f = "WatchAppsPresenter.kt", mo27671l = {135, 136}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$1$1 */
    public static final class C64841 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.model.diana.WatchApp $it;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public java.lang.Object L$1;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22856p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.watchapps.C6483x1f25a914 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64841(com.portfolio.platform.uirenew.home.customize.diana.watchapps.C6483x1f25a914 watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$1, com.portfolio.platform.data.model.diana.WatchApp watchApp, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$1;
            this.$it = watchApp;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.watchapps.C6483x1f25a914.C64841 r0 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.C6483x1f25a914.C64841(this.this$0, this.$it, yb4);
            r0.f22856p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.watchapps.C6483x1f25a914.C64841) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0118, code lost:
            if (r0 <= 0) goto L_0x011a;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00b0  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00e2  */
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.util.List<java.lang.String> list;
            java.util.ArrayList arrayList;
            com.fossil.blesdk.obfuscated.zg4 zg4;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                zg4 = this.f22856p$;
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.f22855a.mo31440b();
                com.portfolio.platform.uirenew.home.customize.diana.watchapps.C6486xa1ad94bc watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$1$1$requiredPermissionList$1 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.C6486xa1ad94bc(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$1$1$requiredPermissionList$1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else if (i == 2) {
                list = (java.util.List) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                java.lang.String[] strArr = (java.lang.String[]) obj;
                arrayList = new java.util.ArrayList();
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local.mo33255d("WatchAppsPresenter", "checkPermissionOf watchAppId=" + this.$it.getWatchappId() + ' ' + "grantedPermission " + strArr.length + " requiredPermission " + list.size());
                if (!list.isEmpty()) {
                    for (java.lang.String str : list) {
                        arrayList.add(new kotlin.Pair(str, com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.za4.m31309b((T[]) strArr, str))));
                    }
                }
                this.this$0.f22855a.f22833j.mo2280a(arrayList);
                if (!arrayList.isEmpty()) {
                    int i2 = 0;
                    if (!arrayList.isEmpty()) {
                        java.util.Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            if (com.fossil.blesdk.obfuscated.dc4.m20839a(!((java.lang.Boolean) ((kotlin.Pair) it.next()).getSecond()).booleanValue()).booleanValue()) {
                                i2++;
                                if (i2 < 0) {
                                    com.fossil.blesdk.obfuscated.cb4.m20541b();
                                    throw null;
                                }
                            }
                        }
                    }
                }
                com.fossil.blesdk.obfuscated.fi4 unused = this.this$0.f22855a.mo41052b(this.$it.getWatchappId());
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            java.util.List list2 = (java.util.List) obj;
            com.fossil.blesdk.obfuscated.ug4 a3 = this.this$0.f22855a.mo31440b();
            com.portfolio.platform.uirenew.home.customize.diana.watchapps.C6485x12088232 watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$1$1$grantedPermissionList$1 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.C6485x12088232((com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = list2;
            this.label = 2;
            java.lang.Object a4 = com.fossil.blesdk.obfuscated.yf4.m30997a(a3, watchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$1$1$grantedPermissionList$1, this);
            if (a4 == a) {
                return a;
            }
            list = list2;
            obj = a4;
            java.lang.String[] strArr2 = (java.lang.String[]) obj;
            arrayList = new java.util.ArrayList();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local2.mo33255d("WatchAppsPresenter", "checkPermissionOf watchAppId=" + this.$it.getWatchappId() + ' ' + "grantedPermission " + strArr2.length + " requiredPermission " + list.size());
            if (!list.isEmpty()) {
            }
            this.this$0.f22855a.f22833j.mo2280a(arrayList);
            if (!arrayList.isEmpty()) {
            }
            com.fossil.blesdk.obfuscated.fi4 unused2 = this.this$0.f22855a.mo41052b(this.$it.getWatchappId());
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public C6483x1f25a914(com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter watchAppsPresenter) {
        this.f22855a = watchAppsPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final androidx.lifecycle.MutableLiveData<java.util.List<kotlin.Pair<java.lang.String, java.lang.Boolean>>> apply(com.portfolio.platform.data.model.diana.WatchApp watchApp) {
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f22855a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.customize.diana.watchapps.C6483x1f25a914.C64841(this, watchApp, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        return this.f22855a.f22833j;
    }
}
