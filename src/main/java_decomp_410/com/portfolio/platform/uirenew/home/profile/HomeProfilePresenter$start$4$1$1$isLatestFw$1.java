package com.portfolio.platform.uirenew.home.profile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4$1$1$isLatestFw$1", mo27670f = "HomeProfilePresenter.kt", mo27671l = {185}, mo27672m = "invokeSuspend")
public final class HomeProfilePresenter$start$4$1$1$isLatestFw$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Boolean>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $activeSerial;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23817p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$start$4$1$1$isLatestFw$1(java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$activeSerial = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4$1$1$isLatestFw$1 homeProfilePresenter$start$4$1$1$isLatestFw$1 = new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4$1$1$isLatestFw$1(this.$activeSerial, yb4);
        homeProfilePresenter$start$4$1$1$isLatestFw$1.f23817p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeProfilePresenter$start$4$1$1$isLatestFw$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4$1$1$isLatestFw$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23817p$;
            com.portfolio.platform.util.DeviceUtils a2 = com.portfolio.platform.util.DeviceUtils.f24406g.mo41910a();
            java.lang.String str = this.$activeSerial;
            this.L$0 = zg4;
            this.label = 1;
            obj = a2.mo41906a(str, (com.portfolio.platform.data.model.Device) null, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
