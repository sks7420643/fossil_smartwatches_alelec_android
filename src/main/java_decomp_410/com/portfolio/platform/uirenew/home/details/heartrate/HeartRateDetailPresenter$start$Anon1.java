package com.portfolio.platform.uirenew.home.details.heartrate;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nf3;
import com.fossil.blesdk.obfuscated.of3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.enums.Status;
import com.portfolio.platform.enums.Unit;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$Anon1", f = "HeartRateDetailPresenter.kt", l = {110}, m = "invokeSuspend")
public final class HeartRateDetailPresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateDetailPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements cc<os3<? extends List<DailyHeartRateSummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter$start$Anon1 a;

        @DexIgnore
        public a(HeartRateDetailPresenter$start$Anon1 heartRateDetailPresenter$start$Anon1) {
            this.a = heartRateDetailPresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(os3<? extends List<DailyHeartRateSummary>> os3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - summaryTransformations -- status=");
            Status status = null;
            sb.append(os3 != null ? os3.f() : null);
            sb.append(", resource=");
            sb.append(os3 != null ? (List) os3.d() : null);
            sb.append(", status=");
            if (os3 != null) {
                status = os3.f();
            }
            sb.append(status);
            local.d("HeartRateDetailPresenter", sb.toString());
            if (os3 != null && os3.f() != Status.DATABASE_LOADING) {
                this.a.this$Anon0.i = (List) os3.d();
                fi4 unused = this.a.this$Anon0.l();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<os3<? extends List<HeartRateSample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter$start$Anon1 a;

        @DexIgnore
        public b(HeartRateDetailPresenter$start$Anon1 heartRateDetailPresenter$start$Anon1) {
            this.a = heartRateDetailPresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(os3<? extends List<HeartRateSample>> os3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - sampleTransformations -- status=");
            Status status = null;
            sb.append(os3 != null ? os3.f() : null);
            sb.append(", resource=");
            sb.append(os3 != null ? (List) os3.d() : null);
            sb.append(", status=");
            if (os3 != null) {
                status = os3.f();
            }
            sb.append(status);
            local.d("HeartRateDetailPresenter", sb.toString());
            if (os3 != null && os3.f() != Status.DATABASE_LOADING) {
                this.a.this$Anon0.j = (List) os3.d();
                fi4 unused = this.a.this$Anon0.m();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$start$Anon1(HeartRateDetailPresenter heartRateDetailPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = heartRateDetailPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HeartRateDetailPresenter$start$Anon1 heartRateDetailPresenter$start$Anon1 = new HeartRateDetailPresenter$start$Anon1(this.this$Anon0, yb4);
        heartRateDetailPresenter$start$Anon1.p$ = (zg4) obj;
        return heartRateDetailPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HeartRateDetailPresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0041, code lost:
        if (r6 != null) goto L_0x0046;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Unit unit;
        Object a2 = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 a3 = this.this$Anon0.b();
            HeartRateDetailPresenter$start$Anon1$currentUser$Anon1 heartRateDetailPresenter$start$Anon1$currentUser$Anon1 = new HeartRateDetailPresenter$start$Anon1$currentUser$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a3, heartRateDetailPresenter$start$Anon1$currentUser$Anon1, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser = (MFUser) obj;
        HeartRateDetailPresenter heartRateDetailPresenter = this.this$Anon0;
        if (mFUser != null) {
            unit = mFUser.getDistanceUnit();
        }
        unit = Unit.METRIC;
        heartRateDetailPresenter.m = unit;
        LiveData o = this.this$Anon0.n;
        nf3 m = this.this$Anon0.q;
        if (m != null) {
            o.a((of3) m, new a(this));
            this.this$Anon0.o.a((LifecycleOwner) this.this$Anon0.q, new b(this));
            return qa4.a;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
    }
}
