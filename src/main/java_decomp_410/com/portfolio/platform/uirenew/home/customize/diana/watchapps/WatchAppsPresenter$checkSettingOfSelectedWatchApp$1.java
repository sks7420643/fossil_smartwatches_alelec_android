package com.portfolio.platform.uirenew.home.customize.diana.watchapps;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1", mo27670f = "WatchAppsPresenter.kt", mo27671l = {284}, mo27672m = "invokeSuspend")
public final class WatchAppsPresenter$checkSettingOfSelectedWatchApp$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $id;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22853p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1$1", mo27670f = "WatchAppsPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1$1 */
    public static final class C64821 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super android.os.Parcelable>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22854p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64821(com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1 watchAppsPresenter$checkSettingOfSelectedWatchApp$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = watchAppsPresenter$checkSettingOfSelectedWatchApp$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1.C64821 r0 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1.C64821(this.this$0, yb4);
            r0.f22854p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1.C64821) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter.m34058f(this.this$0.this$0).mo40974h(this.this$0.$id);
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppsPresenter$checkSettingOfSelectedWatchApp$1(com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter watchAppsPresenter, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = watchAppsPresenter;
        this.$id = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1 watchAppsPresenter$checkSettingOfSelectedWatchApp$1 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1(this.this$0, this.$id, yb4);
        watchAppsPresenter$checkSettingOfSelectedWatchApp$1.f22853p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return watchAppsPresenter$checkSettingOfSelectedWatchApp$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        boolean z;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        android.os.Parcelable parcelable = null;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22853p$;
            z = com.fossil.blesdk.obfuscated.pl2.f17631d.mo30135g(this.$id);
            if (z) {
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1.C64821 r5 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1.C64821(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = null;
                this.Z$0 = z;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r5, this);
                if (obj == a) {
                    return a;
                }
            }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("WatchAppsPresenter", "checkSettingOfSelectedWatchApp id=" + this.$id + " settings=" + parcelable);
            this.this$0.f22834k.mo2280a(new kotlin.Triple(this.$id, com.fossil.blesdk.obfuscated.dc4.m20839a(z), parcelable));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            boolean z2 = this.Z$0;
            android.os.Parcelable parcelable2 = (android.os.Parcelable) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            z = z2;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        parcelable = (android.os.Parcelable) obj;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local2.mo33255d("WatchAppsPresenter", "checkSettingOfSelectedWatchApp id=" + this.$id + " settings=" + parcelable);
        this.this$0.f22834k.mo2280a(new kotlin.Triple(this.$id, com.fossil.blesdk.obfuscated.dc4.m20839a(z), parcelable));
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
