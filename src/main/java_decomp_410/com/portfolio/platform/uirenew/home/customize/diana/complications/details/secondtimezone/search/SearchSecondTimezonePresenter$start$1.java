package com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1", mo27670f = "SearchSecondTimezonePresenter.kt", mo27671l = {37, 38}, mo27672m = "invokeSuspend")
public final class SearchSecondTimezonePresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22789p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SearchSecondTimezonePresenter$start$1(com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter searchSecondTimezonePresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = searchSecondTimezonePresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1 searchSecondTimezonePresenter$start$1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1(this.this$0, yb4);
        searchSecondTimezonePresenter$start$1.f22789p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return searchSecondTimezonePresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f22789p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$rawDataList$1 searchSecondTimezonePresenter$start$1$rawDataList$1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$rawDataList$1((com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, searchSecondTimezonePresenter$start$1$rawDataList$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            java.util.ArrayList arrayList = (java.util.ArrayList) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            this.this$0.f22786g.addAll((java.util.ArrayList) obj);
            this.this$0.f22788i.mo30283q(this.this$0.f22786g);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        java.util.ArrayList arrayList2 = (java.util.ArrayList) obj;
        com.fossil.blesdk.obfuscated.ug4 a3 = this.this$0.mo31440b();
        com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1 searchSecondTimezonePresenter$start$1$sortedDataList$1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1(arrayList2, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.L$1 = arrayList2;
        this.label = 2;
        obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a3, searchSecondTimezonePresenter$start$1$sortedDataList$1, this);
        if (obj == a) {
            return a;
        }
        this.this$0.f22786g.addAll((java.util.ArrayList) obj);
        this.this$0.f22788i.mo30283q(this.this$0.f22786g);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
