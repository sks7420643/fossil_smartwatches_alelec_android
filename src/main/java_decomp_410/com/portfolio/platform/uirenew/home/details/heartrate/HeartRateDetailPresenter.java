package com.portfolio.platform.uirenew.home.details.heartrate;

import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.mf3;
import com.fossil.blesdk.obfuscated.nf3;
import com.fossil.blesdk.obfuscated.of3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.re4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateDetailPresenter extends mf3 implements PagingRequestHelper.a {
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public MutableLiveData<Pair<Date, Date>> h; // = new MutableLiveData<>();
    @DexIgnore
    public List<DailyHeartRateSummary> i; // = new ArrayList();
    @DexIgnore
    public List<HeartRateSample> j; // = new ArrayList();
    @DexIgnore
    public DailyHeartRateSummary k;
    @DexIgnore
    public List<HeartRateSample> l;
    @DexIgnore
    public Unit m; // = Unit.METRIC;
    @DexIgnore
    public LiveData<os3<List<DailyHeartRateSummary>>> n;
    @DexIgnore
    public LiveData<os3<List<HeartRateSample>>> o;
    @DexIgnore
    public Listing<WorkoutSession> p;
    @DexIgnore
    public /* final */ nf3 q;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository r;
    @DexIgnore
    public /* final */ HeartRateSampleRepository s;
    @DexIgnore
    public /* final */ UserRepository t;
    @DexIgnore
    public /* final */ WorkoutSessionRepository u;
    @DexIgnore
    public /* final */ FitnessDataDao v;
    @DexIgnore
    public /* final */ WorkoutDao w;
    @DexIgnore
    public /* final */ FitnessDatabase x;
    @DexIgnore
    public /* final */ h42 y;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<qd<WorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter a;

        @DexIgnore
        public b(HeartRateDetailPresenter heartRateDetailPresenter) {
            this.a = heartRateDetailPresenter;
        }

        @DexIgnore
        public final void a(qd<WorkoutSession> qdVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateDetailPresenter", "getWorkoutSessionsPaging observed size = " + qdVar.size());
            if (DeviceHelper.o.g(PortfolioApp.W.c().e())) {
                kd4.a((Object) qdVar, "pageList");
                if (kb4.d(qdVar).isEmpty()) {
                    this.a.q.a(false, this.a.m, qdVar);
                    return;
                }
            }
            nf3 m = this.a.q;
            Unit c = this.a.m;
            kd4.a((Object) qdVar, "pageList");
            m.a(true, c, qdVar);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter a;

        @DexIgnore
        public c(HeartRateDetailPresenter heartRateDetailPresenter) {
            this.a = heartRateDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<os3<List<HeartRateSample>>> apply(Pair<? extends Date, ? extends Date> pair) {
            return this.a.s.getHeartRateSamples((Date) pair.component1(), (Date) pair.component2(), true);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter a;

        @DexIgnore
        public d(HeartRateDetailPresenter heartRateDetailPresenter) {
            this.a = heartRateDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<os3<List<DailyHeartRateSummary>>> apply(Pair<? extends Date, ? extends Date> pair) {
            return this.a.r.getHeartRateSummaries((Date) pair.component1(), (Date) pair.component2(), true);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public HeartRateDetailPresenter(nf3 nf3, HeartRateSummaryRepository heartRateSummaryRepository, HeartRateSampleRepository heartRateSampleRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FitnessDataDao fitnessDataDao, WorkoutDao workoutDao, FitnessDatabase fitnessDatabase, h42 h42) {
        kd4.b(nf3, "mView");
        kd4.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        kd4.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(workoutSessionRepository, "mWorkoutSessionRepository");
        kd4.b(fitnessDataDao, "mFitnessDataDao");
        kd4.b(workoutDao, "mWorkoutDao");
        kd4.b(fitnessDatabase, "mWorkoutDatabase");
        kd4.b(h42, "appExecutors");
        this.q = nf3;
        this.r = heartRateSummaryRepository;
        this.s = heartRateSampleRepository;
        this.t = userRepository;
        this.u = workoutSessionRepository;
        this.v = fitnessDataDao;
        this.w = workoutDao;
        this.x = fitnessDatabase;
        this.y = h42;
        LiveData<os3<List<DailyHeartRateSummary>>> b2 = hc.b(this.h, new d(this));
        kd4.a((Object) b2, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.n = b2;
        LiveData<os3<List<HeartRateSample>>> b3 = hc.b(this.h, new c(this));
        kd4.a((Object) b3, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.o = b3;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HeartRateDetailPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", "stop");
        LiveData<os3<List<HeartRateSample>>> liveData = this.o;
        nf3 nf3 = this.q;
        if (nf3 != null) {
            liveData.a((LifecycleOwner) (of3) nf3);
            this.n.a((LifecycleOwner) this.q);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
    }

    @DexIgnore
    public void h() {
        try {
            Listing<WorkoutSession> listing = this.p;
            if (listing != null) {
                LiveData<qd<WorkoutSession>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    nf3 nf3 = this.q;
                    if (nf3 != null) {
                        pagedList.a((LifecycleOwner) (of3) nf3);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
                    }
                }
            }
            this.u.removePagingListener();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(qa4.a);
            local.e("HeartRateDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void i() {
        Date l2 = rk2.l(this.g);
        kd4.a((Object) l2, "DateHelper.getNextDate(mDate)");
        b(l2);
    }

    @DexIgnore
    public void j() {
        Date m2 = rk2.m(this.g);
        kd4.a((Object) m2, "DateHelper.getPrevDate(mDate)");
        b(m2);
    }

    @DexIgnore
    public void k() {
        this.q.a(this);
    }

    @DexIgnore
    public final fi4 l() {
        return ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HeartRateDetailPresenter$showDayDetail$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final fi4 m() {
        return ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HeartRateDetailPresenter$showDetailChart$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void c(Date date) {
        h();
        WorkoutSessionRepository workoutSessionRepository = this.u;
        this.p = workoutSessionRepository.getWorkoutSessionsPaging(date, workoutSessionRepository, this.v, this.w, this.x, this.y, this);
        Listing<WorkoutSession> listing = this.p;
        if (listing != null) {
            LiveData<qd<WorkoutSession>> pagedList = listing.getPagedList();
            nf3 nf3 = this.q;
            if (nf3 != null) {
                pagedList.a((of3) nf3, new b(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void b(Date date) {
        kd4.b(date, "date");
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new HeartRateDetailPresenter$setDate$Anon1(this, date, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final DailyHeartRateSummary b(Date date, List<DailyHeartRateSummary> list) {
        T t2 = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (rk2.d(((DailyHeartRateSummary) next).getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return (DailyHeartRateSummary) t2;
    }

    @DexIgnore
    public void a(Date date) {
        kd4.b(date, "date");
        c(date);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        kd4.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.g.getTime());
    }

    @DexIgnore
    public final List<HeartRateSample> a(Date date, List<HeartRateSample> list) {
        if (list != null) {
            re4<T> b2 = kb4.b(list);
            if (b2 != null) {
                re4<T> a2 = SequencesKt___SequencesKt.a(b2, new HeartRateDetailPresenter$findHeartRateSamples$Anon1(date));
                if (a2 != null) {
                    return SequencesKt___SequencesKt.g(a2);
                }
            }
        }
        return null;
    }

    @DexIgnore
    public final String a(Float f2) {
        if (f2 == null) {
            return "";
        }
        pd4 pd4 = pd4.a;
        Object[] objArr = {Integer.valueOf((int) Math.abs(f2.floatValue())), Integer.valueOf(((int) Math.abs(f2.floatValue() - (f2.floatValue() % ((float) 10)))) * 60)};
        String format = String.format("%02d:%02d", Arrays.copyOf(objArr, objArr.length));
        kd4.a((Object) format, "java.lang.String.format(format, *args)");
        if (f2.floatValue() >= ((float) 0)) {
            return '+' + format;
        }
        return '-' + format;
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        kd4.b(eVar, "report");
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", "retry all failed request");
        Listing<WorkoutSession> listing = this.p;
        if (listing != null) {
            wc4<qa4> retry = listing.getRetry();
            if (retry != null) {
                qa4 invoke = retry.invoke();
            }
        }
    }
}
