package com.portfolio.platform.uirenew.home.customize.tutorial;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.o13;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomizeTutorialActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str) {
            kd4.b(context, "context");
            kd4.b(str, "watchAppId");
            Intent intent = new Intent(context, CustomizeTutorialActivity.class);
            intent.putExtra("EXTRA_WATCHAPP_ID", str);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        if (((o13) getSupportFragmentManager().a((int) R.id.content)) == null) {
            String stringExtra = getIntent().getStringExtra("EXTRA_WATCHAPP_ID");
            o13.a aVar = o13.n;
            kd4.a((Object) stringExtra, "watchAppId");
            a((Fragment) aVar.a(stringExtra), "CustomizeTutorialFragment", (int) R.id.content);
        }
    }
}
