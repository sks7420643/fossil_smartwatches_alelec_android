package com.portfolio.platform.uirenew.home.details.heartrate;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.xc4;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import java.util.Date;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateDetailPresenter$findHeartRateSamples$Anon1 extends Lambda implements xc4<HeartRateSample, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$findHeartRateSamples$Anon1(Date date) {
        super(1);
        this.$date = date;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(invoke((HeartRateSample) obj));
    }

    @DexIgnore
    public final boolean invoke(HeartRateSample heartRateSample) {
        kd4.b(heartRateSample, "it");
        return rk2.d(heartRateSample.getDate(), this.$date);
    }
}
