package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.bn2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.hb4;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.pc;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.qc;
import com.fossil.blesdk.obfuscated.sx2;
import com.fossil.blesdk.obfuscated.vw2;
import com.fossil.blesdk.obfuscated.ww2;
import com.fossil.blesdk.obfuscated.xw2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationContactsPresenter extends vw2 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public /* final */ List<ContactWrapper> f; // = new ArrayList();
    @DexIgnore
    public /* final */ List<ContactWrapper> g; // = new ArrayList();
    @DexIgnore
    public /* final */ ww2 h;
    @DexIgnore
    public /* final */ j62 i;
    @DexIgnore
    public /* final */ px2 j;
    @DexIgnore
    public /* final */ sx2 k;
    @DexIgnore
    public /* final */ LoaderManager l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationContactsPresenter.m;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements i62.d<sx2.c, i62.a> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsPresenter a;

        @DexIgnore
        public b(NotificationContactsPresenter notificationContactsPresenter) {
            this.a = notificationContactsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(sx2.c cVar) {
            FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.n.a(), ".Inside mSaveContactGroupsNotification onSuccess");
            this.a.h.close();
        }

        @DexIgnore
        public void a(i62.a aVar) {
            FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.n.a(), ".Inside mSaveContactGroupsNotification onError");
            this.a.h.close();
        }
    }

    /*
    static {
        String simpleName = NotificationContactsPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationContactsPres\u2026er::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public NotificationContactsPresenter(ww2 ww2, j62 j62, px2 px2, sx2 sx2, LoaderManager loaderManager) {
        kd4.b(ww2, "mView");
        kd4.b(j62, "mUseCaseHandler");
        kd4.b(px2, "mGetAllContactGroup");
        kd4.b(sx2, "mSaveContactGroupsNotification");
        kd4.b(loaderManager, "mLoaderManager");
        this.h = ww2;
        this.i = j62;
        this.j = px2;
        this.k = sx2;
        this.l = loaderManager;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(m, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        bn2 bn2 = bn2.d;
        ww2 ww2 = this.h;
        if (ww2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsFragment");
        } else if (bn2.a(bn2, ((xw2) ww2).getContext(), "NOTIFICATION_CONTACTS", false, 4, (Object) null)) {
            fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationContactsPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(m, "stop");
    }

    @DexIgnore
    public void h() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        List<T> c = kb4.c(this.g, this.f);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Iterator<T> it = c.iterator();
        while (true) {
            Integer num = null;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            Contact contact = ((ContactWrapper) next).getContact();
            if (contact != null) {
                num = Integer.valueOf(contact.getContactId());
            }
            Object obj = linkedHashMap.get(num);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap.put(num, obj);
            }
            ((List) obj).add(next);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        Iterator it2 = linkedHashMap.entrySet().iterator();
        while (true) {
            boolean z = false;
            if (!it2.hasNext()) {
                break;
            }
            Map.Entry entry = (Map.Entry) it2.next();
            if (((List) entry.getValue()).size() == 1) {
                z = true;
            }
            if (z) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList<ContactWrapper> arrayList3 = new ArrayList<>();
        for (Map.Entry value : linkedHashMap2.entrySet()) {
            hb4.a(arrayList3, (List) value.getValue());
        }
        for (ContactWrapper contactWrapper : arrayList3) {
            boolean z2 = false;
            for (ContactWrapper contact2 : this.g) {
                Contact contact3 = contact2.getContact();
                Integer valueOf = contact3 != null ? Integer.valueOf(contact3.getContactId()) : null;
                Contact contact4 = contactWrapper.getContact();
                if (kd4.a((Object) valueOf, (Object) contact4 != null ? Integer.valueOf(contact4.getContactId()) : null)) {
                    arrayList.add(contactWrapper);
                    z2 = true;
                }
            }
            if (!z2) {
                arrayList2.add(contactWrapper);
            }
        }
        this.i.a(this.k, new sx2.b(arrayList, arrayList2), new b(this));
    }

    @DexIgnore
    public void i() {
        FLogger.INSTANCE.getLocal().d(m, "onListContactWrapperChanged");
        List<T> c = kb4.c(this.g, this.f);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (T next : c) {
            Contact contact = ((ContactWrapper) next).getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Object obj = linkedHashMap.get(valueOf);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap.put(valueOf, obj);
            }
            ((List) obj).add(next);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        Iterator it = linkedHashMap.entrySet().iterator();
        while (true) {
            boolean z = false;
            if (!it.hasNext()) {
                break;
            }
            Map.Entry entry = (Map.Entry) it.next();
            if (((List) entry.getValue()).size() == 1) {
                z = true;
            }
            if (z) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList arrayList = new ArrayList();
        for (Map.Entry value : linkedHashMap2.entrySet()) {
            hb4.a(arrayList, (List) value.getValue());
        }
        if (arrayList.isEmpty()) {
            this.h.K(false);
        } else {
            this.h.K(true);
        }
    }

    @DexIgnore
    public final List<ContactWrapper> j() {
        return this.f;
    }

    @DexIgnore
    public final List<ContactWrapper> k() {
        return this.g;
    }

    @DexIgnore
    public void l() {
        this.h.a(this);
    }

    @DexIgnore
    public qc<Cursor> a(int i2, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, ".Inside onCreateLoader, selection = " + "has_phone_number!=0 AND mimetype=?");
        String[] strArr = {"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"};
        return new pc(PortfolioApp.W.c(), ContactsContract.Data.CONTENT_URI, strArr, "has_phone_number!=0 AND mimetype=?", new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
    }

    @DexIgnore
    public void a(qc<Cursor> qcVar, Cursor cursor) {
        kd4.b(qcVar, "loader");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, ".Inside onLoadFinished cursor=" + cursor);
        this.h.a(cursor);
    }

    @DexIgnore
    public void a(qc<Cursor> qcVar) {
        kd4.b(qcVar, "loader");
        FLogger.INSTANCE.getLocal().d(m, ".Inside onLoaderReset");
        this.h.H();
    }
}
