package com.portfolio.platform.uirenew.home.customize.diana.watchapps.search;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1", mo27670f = "WatchAppSearchPresenter.kt", mo27671l = {73}, mo27672m = "invokeSuspend")
public final class WatchAppSearchPresenter$search$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $query;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22914p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1$1", mo27670f = "WatchAppSearchPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1$1 */
    public static final class C65031 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<com.portfolio.platform.data.model.diana.WatchApp>>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22915p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C65031(com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1 watchAppSearchPresenter$search$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = watchAppSearchPresenter$search$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1.C65031 r0 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1.C65031(this.this$0, yb4);
            r0.f22915p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1.C65031) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return com.fossil.blesdk.obfuscated.kb4.m24381d(this.this$0.this$0.f22912m.queryWatchAppByName(this.this$0.$query));
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppSearchPresenter$search$1(com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter watchAppSearchPresenter, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = watchAppSearchPresenter;
        this.$query = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1 watchAppSearchPresenter$search$1 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1(this.this$0, this.$query, yb4);
        watchAppSearchPresenter$search$1.f22914p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return watchAppSearchPresenter$search$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.util.List list;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22914p$;
            list = new java.util.ArrayList();
            if (this.$query.length() > 0) {
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31441c();
                com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1.C65031 r4 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1.C65031(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = list;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r4, this);
                if (obj == a) {
                    return a;
                }
            }
            this.this$0.f22911l.mo30820b(this.this$0.mo41106a((java.util.List<com.portfolio.platform.data.model.diana.WatchApp>) list));
            this.this$0.f22908i = this.$query;
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            java.util.List list2 = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        list = (java.util.List) obj;
        this.this$0.f22911l.mo30820b(this.this$0.mo41106a((java.util.List<com.portfolio.platform.data.model.diana.WatchApp>) list));
        this.this$0.f22908i = this.$query;
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
