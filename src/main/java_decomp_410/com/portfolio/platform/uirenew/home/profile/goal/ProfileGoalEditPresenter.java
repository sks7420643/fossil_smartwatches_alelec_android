package com.portfolio.platform.uirenew.home.profile.goal;

import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ai3;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.uh3;
import com.fossil.blesdk.obfuscated.vh3;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.zr2;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.enums.GoalType;
import com.portfolio.platform.enums.Status;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ProfileGoalEditPresenter extends uh3 {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public LiveData<os3<ActivitySettings>> f; // = new MutableLiveData();
    @DexIgnore
    public ActivitySettings g; // = new ActivitySettings(VideoUploader.RETRY_DELAY_UNIT_MS, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 30);
    @DexIgnore
    public ActivitySettings h; // = new ActivitySettings(VideoUploader.RETRY_DELAY_UNIT_MS, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 30);
    @DexIgnore
    public int i; // = 480;
    @DexIgnore
    public int j; // = 480;
    @DexIgnore
    public int k; // = 8;
    @DexIgnore
    public int l; // = 8;
    @DexIgnore
    public LiveData<os3<Integer>> m; // = new MutableLiveData();
    @DexIgnore
    public LiveData<os3<Integer>> n; // = new MutableLiveData();
    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE o; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.W.c().e());
    @DexIgnore
    public /* final */ vh3 p;
    @DexIgnore
    public /* final */ SummariesRepository q;
    @DexIgnore
    public /* final */ SleepSummariesRepository r;
    @DexIgnore
    public /* final */ GoalTrackingRepository s;
    @DexIgnore
    public /* final */ UserRepository t;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<os3<? extends ActivitySettings>> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ LiveData b;

        @DexIgnore
        public b(ProfileGoalEditPresenter profileGoalEditPresenter, LiveData liveData) {
            this.a = profileGoalEditPresenter;
            this.b = liveData;
        }

        @DexIgnore
        public final void a(os3<ActivitySettings> os3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String s = ProfileGoalEditPresenter.u;
            local.d(s, "saveActivityGoal status " + os3.f());
            if ((os3 != null ? os3.f() : null) != Status.SUCCESS || os3.d() == null) {
                if ((os3 != null ? os3.f() : null) == Status.ERROR) {
                    vh3 m = this.a.m();
                    Integer c = os3.c();
                    if (c != null) {
                        int intValue = c.intValue();
                        String e = os3.e();
                        if (e != null) {
                            m.d(intValue, e);
                            this.b.a((LifecycleOwner) this.a.m());
                            this.a.o();
                            return;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
                return;
            }
            ProfileGoalEditPresenter profileGoalEditPresenter = this.a;
            profileGoalEditPresenter.a(profileGoalEditPresenter.h);
            fi4 unused = this.a.l();
            this.b.a((LifecycleOwner) this.a.m());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements cc<os3<? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter a;

        @DexIgnore
        public c(ProfileGoalEditPresenter profileGoalEditPresenter) {
            this.a = profileGoalEditPresenter;
        }

        @DexIgnore
        public final void a(os3<Integer> os3) {
            Integer num = null;
            if ((os3 != null ? os3.f() : null) != Status.DATABASE_LOADING) {
                if (os3 != null) {
                    num = os3.d();
                }
                if (num != null) {
                    this.a.a(os3.d().intValue());
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements cc<os3<? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter a;

        @DexIgnore
        public d(ProfileGoalEditPresenter profileGoalEditPresenter) {
            this.a = profileGoalEditPresenter;
        }

        @DexIgnore
        public final void a(os3<Integer> os3) {
            Integer num = null;
            if ((os3 != null ? os3.f() : null) != Status.DATABASE_LOADING) {
                if (os3 != null) {
                    num = os3.d();
                }
                if (num != null) {
                    this.a.b(os3.d().intValue());
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements cc<os3<? extends ActivitySettings>> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter a;

        @DexIgnore
        public e(ProfileGoalEditPresenter profileGoalEditPresenter) {
            this.a = profileGoalEditPresenter;
        }

        @DexIgnore
        public final void a(os3<ActivitySettings> os3) {
            ActivitySettings activitySettings = null;
            if ((os3 != null ? os3.f() : null) != Status.DATABASE_LOADING) {
                if (os3 != null) {
                    activitySettings = os3.d();
                }
                if (activitySettings != null) {
                    this.a.b(os3.d());
                }
            }
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = ProfileGoalEditPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "ProfileGoalEditPresenter::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public ProfileGoalEditPresenter(vh3 vh3, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository) {
        kd4.b(vh3, "mView");
        kd4.b(summariesRepository, "mSummariesRepository");
        kd4.b(sleepSummariesRepository, "mSleepSummariesRepository");
        kd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        kd4.b(userRepository, "mUserRepository");
        this.p = vh3;
        this.q = summariesRepository;
        this.r = sleepSummariesRepository;
        this.s = goalTrackingRepository;
        this.t = userRepository;
    }

    @DexIgnore
    public void j() {
        this.p.i(this.i);
    }

    @DexIgnore
    public void k() {
        if (i() != FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
            fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ProfileGoalEditPresenter$saveGoalTrackingTarget$Anon1(this, (yb4) null), 3, (Object) null);
        } else {
            q();
        }
    }

    @DexIgnore
    public final fi4 l() {
        return ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final vh3 m() {
        return this.p;
    }

    @DexIgnore
    public final boolean n() {
        boolean z = (this.h.getCurrentStepGoal() == this.g.getCurrentStepGoal() && this.h.getCurrentCaloriesGoal() == this.g.getCurrentCaloriesGoal() && this.h.getCurrentActiveTimeGoal() == this.g.getCurrentActiveTimeGoal()) ? false : true;
        boolean z2 = this.j != this.i;
        boolean z3 = this.l != this.k;
        if (z || z2 || z3) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void o() {
        b(this.g);
        b(this.i);
        a(this.k);
    }

    @DexIgnore
    public final void p() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("XXX", "saveActivityGoal " + this.h);
        LiveData<os3<ActivitySettings>> updateActivitySettings = this.q.updateActivitySettings(this.h);
        vh3 vh3 = this.p;
        if (vh3 != null) {
            updateActivitySettings.a((zr2) vh3, new b(this, updateActivitySettings));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    public final fi4 q() {
        return ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ProfileGoalEditPresenter$saveSleepGoal$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void r() {
        this.p.a(this);
    }

    @DexIgnore
    public void f() {
        this.p.r0();
        this.n = this.s.getLastGoalSetting();
        LiveData<os3<Integer>> liveData = this.n;
        vh3 vh3 = this.p;
        if (vh3 != null) {
            liveData.a((zr2) vh3, new c(this));
            this.m = this.r.getLastSleepGoal();
            LiveData<os3<Integer>> liveData2 = this.m;
            vh3 vh32 = this.p;
            if (vh32 != null) {
                liveData2.a((zr2) vh32, new d(this));
                this.f = this.q.getActivitySettings();
                LiveData<os3<ActivitySettings>> liveData3 = this.f;
                vh3 vh33 = this.p;
                if (vh33 != null) {
                    liveData3.a((zr2) vh33, new e(this));
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(u, "stop");
        LiveData<os3<ActivitySettings>> liveData = this.f;
        vh3 vh3 = this.p;
        if (vh3 != null) {
            liveData.a((LifecycleOwner) (zr2) vh3);
            this.m.a((LifecycleOwner) this.p);
            this.n.a((LifecycleOwner) this.p);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    public void h() {
        if (n()) {
            this.p.w0();
        } else {
            this.p.I0();
        }
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE i() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.o;
        kd4.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public final void a(ActivitySettings activitySettings) {
        String e2 = PortfolioApp.W.c().e();
        if (!TextUtils.isEmpty(e2)) {
            PortfolioApp.W.c().a(e2, activitySettings.getCurrentStepGoal());
        }
        this.p.I0();
    }

    @DexIgnore
    public final void b(ActivitySettings activitySettings) {
        if (!(activitySettings.getCurrentStepGoal() == this.g.getCurrentStepGoal() && activitySettings.getCurrentCaloriesGoal() == this.g.getCurrentCaloriesGoal() && activitySettings.getCurrentActiveTimeGoal() == this.g.getCurrentActiveTimeGoal())) {
            this.g = activitySettings;
            this.h = new ActivitySettings(activitySettings.getCurrentStepGoal(), activitySettings.getCurrentCaloriesGoal(), activitySettings.getCurrentActiveTimeGoal());
        }
        this.p.a(this.h);
    }

    @DexIgnore
    public final void a(int i2) {
        if (i2 != this.k) {
            this.k = i2;
            this.l = this.k;
        }
        this.p.j(this.l);
    }

    @DexIgnore
    public final void b(int i2) {
        if (i2 != this.i) {
            this.i = i2;
            this.j = this.i;
        }
        this.p.l(this.j);
    }

    @DexIgnore
    public void a(int i2, GoalType goalType) {
        kd4.b(goalType, "type");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "setSettingValue value=" + i2 + " type=" + goalType);
        int i3 = ai3.a[goalType.ordinal()];
        if (i3 == 1) {
            this.h.setCurrentStepGoal(i2);
        } else if (i3 == 2) {
            this.h.setCurrentCaloriesGoal(i2);
        } else if (i3 == 3) {
            this.h.setCurrentActiveTimeGoal(i2);
        } else if (i3 == 4) {
            this.j = i2;
        } else if (i3 == 5) {
            this.l = i2;
        }
        this.p.a(i2, goalType);
    }
}
