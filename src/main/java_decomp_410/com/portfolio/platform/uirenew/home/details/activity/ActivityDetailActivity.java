package com.portfolio.platform.uirenew.home.details.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.me3;
import com.fossil.blesdk.obfuscated.ne3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityDetailActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a D; // = new a((fd4) null);
    @DexIgnore
    public ActivityDetailPresenter B;
    @DexIgnore
    public Date C; // = new Date();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Date date, Context context) {
            kd4.b(date, "date");
            kd4.b(context, "context");
            Intent intent = new Intent(context, ActivityDetailActivity.class);
            intent.putExtra("KEY_LONG_TIME", date.getTime());
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        me3 me3 = (me3) getSupportFragmentManager().a((int) R.id.content);
        Intent intent = getIntent();
        if (intent != null) {
            this.C = new Date(intent.getLongExtra("KEY_LONG_TIME", new Date().getTime()));
        }
        if (me3 == null) {
            me3 = me3.o.a(this.C);
            a((Fragment) me3, (int) R.id.content);
        }
        PortfolioApp.W.c().g().a(new ne3(me3)).a(this);
    }
}
