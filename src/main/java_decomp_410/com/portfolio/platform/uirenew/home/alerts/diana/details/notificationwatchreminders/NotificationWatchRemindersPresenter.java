package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ay2;
import com.fossil.blesdk.obfuscated.by2;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cy2;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.data.RemindTimeModel;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationWatchRemindersPresenter extends ay2 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public boolean f; // = this.q.P();
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j; // = this.f;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public /* final */ LiveData<List<InactivityNudgeTimeModel>> n; // = this.r.getInactivityNudgeTimeDao().getListInactivityNudgeTime();
    @DexIgnore
    public /* final */ LiveData<RemindTimeModel> o; // = this.r.getRemindTimeDao().getRemindTime();
    @DexIgnore
    public /* final */ by2 p;
    @DexIgnore
    public /* final */ en2 q;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<List<? extends InactivityNudgeTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter a;

        @DexIgnore
        public b(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            this.a = notificationWatchRemindersPresenter;
        }

        @DexIgnore
        public final void a(List<InactivityNudgeTimeModel> list) {
            by2 unused = this.a.p;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements cc<RemindTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter a;

        @DexIgnore
        public c(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            this.a = notificationWatchRemindersPresenter;
        }

        @DexIgnore
        public final void a(RemindTimeModel remindTimeModel) {
            by2 unused = this.a.p;
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = NotificationWatchRemindersPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationWatchReminde\u2026er::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public NotificationWatchRemindersPresenter(by2 by2, en2 en2, RemindersSettingsDatabase remindersSettingsDatabase) {
        kd4.b(by2, "mView");
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        this.p = by2;
        this.q = en2;
        this.r = remindersSettingsDatabase;
    }

    @DexIgnore
    public void j() {
        this.k = !this.k;
        if (m()) {
            this.p.e(true);
        } else {
            this.p.e(false);
        }
    }

    @DexIgnore
    public void k() {
        this.l = !this.l;
        if (m()) {
            this.p.e(true);
        } else {
            this.p.e(false);
        }
    }

    @DexIgnore
    public void l() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new NotificationWatchRemindersPresenter$save$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final boolean m() {
        return (this.j == this.f && this.k == this.g && this.l == this.h && this.m == this.i) ? false : true;
    }

    @DexIgnore
    public void n() {
        this.p.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(s, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        LiveData<List<InactivityNudgeTimeModel>> liveData = this.n;
        by2 by2 = this.p;
        if (by2 != null) {
            liveData.a((cy2) by2, new NotificationWatchRemindersPresenter$start$Anon1(this));
            this.o.a((LifecycleOwner) this.p, new NotificationWatchRemindersPresenter$start$Anon2(this));
            this.g = this.q.Q();
            this.k = this.g;
            this.h = this.q.R();
            this.l = this.h;
            this.i = this.q.O();
            this.m = this.i;
            this.p.D(this.j);
            this.p.w(this.k);
            this.p.C(this.l);
            this.p.u(this.m);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(s, "stop");
        this.n.b(new b(this));
        this.o.b(new c(this));
    }

    @DexIgnore
    public void h() {
        this.m = !this.m;
        if (m()) {
            this.p.e(true);
        } else {
            this.p.e(false);
        }
    }

    @DexIgnore
    public void i() {
        this.j = !this.j;
        if (m()) {
            this.p.e(true);
        } else {
            this.p.e(false);
        }
        this.p.D(this.j);
    }
}
