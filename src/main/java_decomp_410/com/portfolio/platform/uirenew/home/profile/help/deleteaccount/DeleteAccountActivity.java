package com.portfolio.platform.uirenew.home.profile.help.deleteaccount;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.di3;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ri3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeleteAccountActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public DeleteAccountPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            kd4.b(context, "context");
            context.startActivity(new Intent(context, DeleteAccountActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        di3 di3 = (di3) getSupportFragmentManager().a((int) R.id.content);
        if (di3 == null) {
            di3 = di3.n.a();
            a((Fragment) di3, (int) R.id.content);
        }
        PortfolioApp.W.c().g().a(new ri3(di3)).a(this);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        a(false);
    }
}
