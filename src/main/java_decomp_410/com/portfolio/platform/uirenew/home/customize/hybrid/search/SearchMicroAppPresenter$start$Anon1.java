package com.portfolio.platform.uirenew.home.customize.hybrid.search;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$start$Anon1", f = "SearchMicroAppPresenter.kt", l = {40, 44}, m = "invokeSuspend")
public final class SearchMicroAppPresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SearchMicroAppPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SearchMicroAppPresenter$start$Anon1(SearchMicroAppPresenter searchMicroAppPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = searchMicroAppPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SearchMicroAppPresenter$start$Anon1 searchMicroAppPresenter$start$Anon1 = new SearchMicroAppPresenter$start$Anon1(this.this$Anon0, yb4);
        searchMicroAppPresenter$start$Anon1.p$ = (zg4) obj;
        return searchMicroAppPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SearchMicroAppPresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            ug4 a2 = this.this$Anon0.c();
            SearchMicroAppPresenter$start$Anon1$allMicroApps$Anon1 searchMicroAppPresenter$start$Anon1$allMicroApps$Anon1 = new SearchMicroAppPresenter$start$Anon1$allMicroApps$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a2, searchMicroAppPresenter$start$Anon1$allMicroApps$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            List list = (List) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            this.this$Anon0.k.clear();
            this.this$Anon0.k.addAll((List) obj);
            this.this$Anon0.i();
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List list2 = (List) obj;
        this.this$Anon0.j.clear();
        this.this$Anon0.j.addAll(list2);
        ug4 a3 = this.this$Anon0.c();
        SearchMicroAppPresenter$start$Anon1$allSearchedMicroApps$Anon1 searchMicroAppPresenter$start$Anon1$allSearchedMicroApps$Anon1 = new SearchMicroAppPresenter$start$Anon1$allSearchedMicroApps$Anon1(this, (yb4) null);
        this.L$Anon0 = zg4;
        this.L$Anon1 = list2;
        this.label = 2;
        obj = yf4.a(a3, searchMicroAppPresenter$start$Anon1$allSearchedMicroApps$Anon1, this);
        if (obj == a) {
            return a;
        }
        this.this$Anon0.k.clear();
        this.this$Anon0.k.addAll((List) obj);
        this.this$Anon0.i();
        return qa4.a;
    }
}
