package com.portfolio.platform.uirenew.home.profile.help.deleteaccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeleteAccountPresenter$sendFeedback$1$onSuccess$configuration$1 extends com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.kr2.C4595d $responseValue;

    @DexIgnore
    public DeleteAccountPresenter$sendFeedback$1$onSuccess$configuration$1(com.fossil.blesdk.obfuscated.kr2.C4595d dVar) {
        this.$responseValue = dVar;
    }

    @DexIgnore
    public java.lang.String getAdditionalInfo() {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a = com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter.f23895l.mo41528a();
        local.mo33255d(a, "Inside. getAdditionalInfo: \n" + this.$responseValue.mo28906a());
        return this.$responseValue.mo28906a();
    }

    @DexIgnore
    public java.lang.String getRequestSubject() {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter.f23895l.mo41528a(), "getRequestSubject");
        return this.$responseValue.mo28909d();
    }

    @DexIgnore
    public java.util.List<java.lang.String> getTags() {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter.f23895l.mo41528a(), "getTags");
        return this.$responseValue.mo28910e();
    }
}
