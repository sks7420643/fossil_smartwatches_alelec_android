package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1 */
public final class C6446x1629acb7 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $address$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$BooleanRef $isAddressSaved$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$BooleanRef $isChanged$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $it;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22751p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1$1")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1$1 */
    public static final class C64471 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22752p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64471(com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64471 r0 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64471(this.this$0, yb4);
            r0.f22752p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64471) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.mo41024h().mo27002a((java.util.List<java.lang.String>) this.this$0.this$0.f22747k);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1$2")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1$2 */
    public static final class C64482 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.MFUser>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22753p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64482(com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64482 r0 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64482(this.this$0, yb4);
            r0.f22753p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64482) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return this.this$0.this$0.mo41025i().getCurrentUser();
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1$3")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1$3 */
    public static final class C64493 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qo2<com.portfolio.platform.data.model.MFUser>>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.model.MFUser $user;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22754p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64493(com.portfolio.platform.data.model.MFUser mFUser, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1) {
            super(2, yb4);
            this.$user = mFUser;
            this.this$0 = commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64493 r0 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64493(this.$user, yb4, this.this$0);
            r0.f22754p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64493) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22754p$;
                com.portfolio.platform.data.source.UserRepository i2 = this.this$0.this$0.mo41025i();
                com.portfolio.platform.data.model.MFUser mFUser = this.$user;
                this.L$0 = zg4;
                this.label = 1;
                obj = i2.updateUser(mFUser, true, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1$4")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1$4 */
    public static final class C64504 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qo2<com.portfolio.platform.data.model.MFUser>>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.model.MFUser $user;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22755p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64504(com.portfolio.platform.data.model.MFUser mFUser, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1) {
            super(2, yb4);
            this.$user = mFUser;
            this.this$0 = commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64504 r0 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64504(this.$user, yb4, this.this$0);
            r0.f22755p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64504) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22755p$;
                com.portfolio.platform.data.source.UserRepository i2 = this.this$0.this$0.mo41025i();
                com.portfolio.platform.data.model.MFUser mFUser = this.$user;
                this.L$0 = zg4;
                this.label = 1;
                obj = i2.updateUser(mFUser, false, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6446x1629acb7(java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter, java.lang.String str2, kotlin.jvm.internal.Ref$BooleanRef ref$BooleanRef, kotlin.jvm.internal.Ref$BooleanRef ref$BooleanRef2) {
        super(2, yb4);
        this.$it = str;
        this.this$0 = commuteTimeSettingsDefaultAddressPresenter;
        this.$address$inlined = str2;
        this.$isChanged$inlined = ref$BooleanRef;
        this.$isAddressSaved$inlined = ref$BooleanRef2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7(this.$it, yb4, this.this$0, this.$address$inlined, this.$isChanged$inlined, this.$isAddressSaved$inlined);
        commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1.f22751p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00d3 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0190  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01e6  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0215  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x025e  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x026a  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.data.model.MFUser mFUser;
        com.fossil.blesdk.obfuscated.qo2 qo2;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        com.portfolio.platform.data.model.MFUser mFUser2;
        com.portfolio.platform.data.model.MFUser mFUser3;
        com.fossil.blesdk.obfuscated.qo2 qo22;
        com.fossil.blesdk.obfuscated.zg4 zg42;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        boolean z = false;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg42 = this.f22751p$;
            if (!(this.$it.length() == 0) && !this.this$0.f22747k.contains(this.$it)) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String l = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter.f22741o;
                local.mo33255d(l, "add " + this.$address$inlined + " to recent search list");
                this.this$0.f22747k.add(0, this.$it);
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31441c();
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64471 r11 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64471(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg42;
                this.label = 1;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r11, this) == a) {
                    return a;
                }
            }
            com.fossil.blesdk.obfuscated.ug4 a3 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64482 r112 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64482(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg42;
            this.label = 2;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a3, r112, this);
            if (obj == a) {
            }
        } else if (i == 1) {
            zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.ug4 a32 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64482 r1122 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64482(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg42;
            this.label = 2;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a32, r1122, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 2) {
            zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 3) {
            mFUser3 = (com.portfolio.platform.data.model.MFUser) this.L$2;
            mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            qo22 = (com.fossil.blesdk.obfuscated.qo2) obj;
            if (!(qo22 instanceof com.fossil.blesdk.obfuscated.ro2)) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String l2 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter.f22741o;
                local2.mo33255d(l2, "update UserRepository to remote DB success - home = " + mFUser3.getHome() + " - work = " + mFUser3.getWork() + " successfully");
                this.$isAddressSaved$inlined.element = true;
            } else if (qo22 instanceof com.fossil.blesdk.obfuscated.po2) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String l3 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter.f22741o;
                local3.mo33255d(l3, "update UserRepository to remote DB success - home = " + mFUser3.getHome() + " - work = " + mFUser3.getWork() + " failed");
                com.fossil.blesdk.obfuscated.ug4 a4 = this.this$0.mo31441c();
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64504 r113 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64504(mFUser3, (com.fossil.blesdk.obfuscated.yb4) null, this);
                this.L$0 = zg4;
                this.L$1 = mFUser2;
                this.L$2 = mFUser3;
                this.label = 4;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a4, r113, this);
                if (obj == a) {
                    return a;
                }
                mFUser = mFUser3;
                qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
                kotlin.jvm.internal.Ref$BooleanRef ref$BooleanRef = this.$isAddressSaved$inlined;
                if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
                }
                ref$BooleanRef.element = z;
            }
            this.this$0.mo41026j().mo32767a();
            if (this.$isAddressSaved$inlined.element) {
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 4) {
            mFUser = (com.portfolio.platform.data.model.MFUser) this.L$2;
            com.portfolio.platform.data.model.MFUser mFUser4 = (com.portfolio.platform.data.model.MFUser) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
            kotlin.jvm.internal.Ref$BooleanRef ref$BooleanRef2 = this.$isAddressSaved$inlined;
            if (!(qo2 instanceof com.fossil.blesdk.obfuscated.ro2)) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String l4 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter.f22741o;
                local4.mo33255d(l4, "update UserRepository to local DB success - home = " + mFUser.getHome() + " - work = " + mFUser.getWork() + " successfully");
                z = true;
            } else if (qo2 instanceof com.fossil.blesdk.obfuscated.po2) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String l5 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter.f22741o;
                local5.mo33255d(l5, "update UserRepository to local DB success - home = " + mFUser.getHome() + " - work = " + mFUser.getWork() + " failed");
            } else {
                throw new kotlin.NoWhenBranchMatchedException();
            }
            ref$BooleanRef2.element = z;
            this.this$0.mo41026j().mo32767a();
            if (this.$isAddressSaved$inlined.element) {
                this.this$0.mo41026j().mo29784B(this.$address$inlined);
            } else {
                this.this$0.mo41026j().mo29784B((java.lang.String) null);
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        zg4 = zg42;
        com.portfolio.platform.data.model.MFUser mFUser5 = (com.portfolio.platform.data.model.MFUser) obj;
        if (mFUser5 != null) {
            java.lang.String b = this.this$0.f22744h;
            if (b != null) {
                int hashCode = b.hashCode();
                if (hashCode != 2255103) {
                    if (hashCode == 76517104 && b.equals("Other")) {
                        this.$isChanged$inlined.element = !android.text.TextUtils.equals(mFUser5.getWork(), this.$address$inlined);
                        mFUser5.setWork(this.$address$inlined);
                    }
                } else if (b.equals("Home")) {
                    this.$isChanged$inlined.element = !android.text.TextUtils.equals(mFUser5.getHome(), this.$address$inlined);
                    mFUser5.setHome(this.$address$inlined);
                }
            }
            if (this.$isChanged$inlined.element) {
                this.this$0.mo41026j().mo32768b();
                com.fossil.blesdk.obfuscated.ug4 a5 = this.this$0.mo31441c();
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64493 r114 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6446x1629acb7.C64493(mFUser5, (com.fossil.blesdk.obfuscated.yb4) null, this);
                this.L$0 = zg4;
                this.L$1 = mFUser5;
                this.L$2 = mFUser5;
                this.label = 3;
                java.lang.Object a6 = com.fossil.blesdk.obfuscated.yf4.m30997a(a5, r114, this);
                if (a6 == a) {
                    return a;
                }
                mFUser2 = mFUser5;
                obj = a6;
                mFUser3 = mFUser2;
                qo22 = (com.fossil.blesdk.obfuscated.qo2) obj;
                if (!(qo22 instanceof com.fossil.blesdk.obfuscated.ro2)) {
                }
                this.this$0.mo41026j().mo32767a();
            }
            if (this.$isAddressSaved$inlined.element) {
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
