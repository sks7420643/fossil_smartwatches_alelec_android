package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.cu3;
import com.fossil.blesdk.obfuscated.f9;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.gd3;
import com.fossil.blesdk.obfuscated.kd3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.qa;
import com.fossil.blesdk.obfuscated.qd3;
import com.fossil.blesdk.obfuscated.sf2;
import com.fossil.blesdk.obfuscated.tr3;
import com.fossil.blesdk.obfuscated.vd3;
import com.fossil.blesdk.obfuscated.xe;
import com.fossil.blesdk.obfuscated.zr2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepOverviewFragment extends zr2 {
    @DexIgnore
    public tr3<sf2> j;
    @DexIgnore
    public SleepOverviewDayPresenter k;
    @DexIgnore
    public SleepOverviewWeekPresenter l;
    @DexIgnore
    public SleepOverviewMonthPresenter m;
    @DexIgnore
    public gd3 n;
    @DexIgnore
    public vd3 o;
    @DexIgnore
    public qd3 p;
    @DexIgnore
    public int q; // = 7;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewFragment e;

        @DexIgnore
        public b(SleepOverviewFragment sleepOverviewFragment) {
            this.e = sleepOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SleepOverviewFragment sleepOverviewFragment = this.e;
            tr3 a = sleepOverviewFragment.j;
            sleepOverviewFragment.a(7, a != null ? (sf2) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewFragment e;

        @DexIgnore
        public c(SleepOverviewFragment sleepOverviewFragment) {
            this.e = sleepOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SleepOverviewFragment sleepOverviewFragment = this.e;
            tr3 a = sleepOverviewFragment.j;
            sleepOverviewFragment.a(4, a != null ? (sf2) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewFragment e;

        @DexIgnore
        public d(SleepOverviewFragment sleepOverviewFragment) {
            this.e = sleepOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SleepOverviewFragment sleepOverviewFragment = this.e;
            tr3 a = sleepOverviewFragment.j;
            sleepOverviewFragment.a(2, a != null ? (sf2) a.a() : null);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "SleepOverviewFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewFragment", "onCreateView");
        sf2 sf2 = (sf2) qa.a(layoutInflater, R.layout.fragment_sleep_overview, viewGroup, false, O0());
        f9.c((View) sf2.t, false);
        if (bundle != null) {
            this.q = bundle.getInt("CURRENT_TAB", 7);
        }
        kd4.a((Object) sf2, "binding");
        a(sf2);
        this.j = new tr3<>(this, sf2);
        tr3<sf2> tr3 = this.j;
        if (tr3 != null) {
            sf2 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        kd4.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.q);
    }

    @DexIgnore
    public final void a(sf2 sf2) {
        FLogger.INSTANCE.getLocal().d("SleepOverviewFragment", "initUI");
        this.n = (gd3) getChildFragmentManager().a("SleepOverviewDayFragment");
        this.o = (vd3) getChildFragmentManager().a("SleepOverviewWeekFragment");
        this.p = (qd3) getChildFragmentManager().a("SleepOverviewMonthFragment");
        if (this.n == null) {
            this.n = new gd3();
        }
        if (this.o == null) {
            this.o = new vd3();
        }
        if (this.p == null) {
            this.p = new qd3();
        }
        ArrayList arrayList = new ArrayList();
        gd3 gd3 = this.n;
        if (gd3 != null) {
            arrayList.add(gd3);
            vd3 vd3 = this.o;
            if (vd3 != null) {
                arrayList.add(vd3);
                qd3 qd3 = this.p;
                if (qd3 != null) {
                    arrayList.add(qd3);
                    RecyclerView recyclerView = sf2.t;
                    kd4.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new cu3(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new SleepOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new xe().a(recyclerView);
                    a(this.q, sf2);
                    l42 g = PortfolioApp.W.c().g();
                    gd3 gd32 = this.n;
                    if (gd32 != null) {
                        vd3 vd32 = this.o;
                        if (vd32 != null) {
                            qd3 qd32 = this.p;
                            if (qd32 != null) {
                                g.a(new kd3(gd32, vd32, qd32)).a(this);
                                sf2.s.setOnClickListener(new b(this));
                                sf2.q.setOnClickListener(new c(this));
                                sf2.r.setOnClickListener(new d(this));
                                return;
                            }
                            kd4.a();
                            throw null;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i, sf2 sf2) {
        if (sf2 != null) {
            FlexibleTextView flexibleTextView = sf2.s;
            kd4.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = sf2.q;
            kd4.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = sf2.r;
            kd4.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = sf2.s;
            kd4.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = sf2.q;
            kd4.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = sf2.r;
            kd4.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i == 2) {
                FlexibleTextView flexibleTextView7 = sf2.r;
                kd4.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = sf2.r;
                kd4.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = sf2.q;
                kd4.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                tr3<sf2> tr3 = this.j;
                if (tr3 != null) {
                    sf2 a2 = tr3.a();
                    if (a2 != null) {
                        RecyclerView recyclerView = a2.t;
                        if (recyclerView != null) {
                            recyclerView.i(2);
                        }
                    }
                }
            } else if (i == 4) {
                FlexibleTextView flexibleTextView10 = sf2.q;
                kd4.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = sf2.q;
                kd4.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = sf2.q;
                kd4.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                tr3<sf2> tr32 = this.j;
                if (tr32 != null) {
                    sf2 a3 = tr32.a();
                    if (a3 != null) {
                        RecyclerView recyclerView2 = a3.t;
                        if (recyclerView2 != null) {
                            recyclerView2.i(1);
                        }
                    }
                }
            } else if (i != 7) {
                FlexibleTextView flexibleTextView13 = sf2.s;
                kd4.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = sf2.s;
                kd4.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = sf2.q;
                kd4.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                tr3<sf2> tr33 = this.j;
                if (tr33 != null) {
                    sf2 a4 = tr33.a();
                    if (a4 != null) {
                        RecyclerView recyclerView3 = a4.t;
                        if (recyclerView3 != null) {
                            recyclerView3.i(0);
                        }
                    }
                }
            } else {
                FlexibleTextView flexibleTextView16 = sf2.s;
                kd4.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = sf2.s;
                kd4.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = sf2.q;
                kd4.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                tr3<sf2> tr34 = this.j;
                if (tr34 != null) {
                    sf2 a5 = tr34.a();
                    if (a5 != null) {
                        RecyclerView recyclerView4 = a5.t;
                        if (recyclerView4 != null) {
                            recyclerView4.i(0);
                        }
                    }
                }
            }
        }
    }
}
