package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.tj2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaCustomizeViewModel$watchFaceObserver$Anon1<T> implements cc<List<? extends WatchFace>> {
    @DexIgnore
    public /* final */ /* synthetic */ DianaCustomizeViewModel a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$Anon1$Anon1", f = "DianaCustomizeViewModel.kt", l = {505}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $it;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel$watchFaceObserver$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$Anon1$Anon1$Anon1")
        @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$Anon1$Anon1$Anon1", f = "DianaCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0137Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Boolean>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $list;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0137Anon1(Anon1 anon1, List list, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
                this.$list = list;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0137Anon1 anon1 = new C0137Anon1(this.this$Anon0, this.$list, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0137Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                cc4.a();
                if (this.label == 0) {
                    na4.a(obj);
                    this.this$Anon0.this$Anon0.a.h.clear();
                    return dc4.a(this.this$Anon0.this$Anon0.a.h.addAll(this.$list));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(DianaCustomizeViewModel$watchFaceObserver$Anon1 dianaCustomizeViewModel$watchFaceObserver$Anon1, List list, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = dianaCustomizeViewModel$watchFaceObserver$Anon1;
            this.$it = list;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$it, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                if (!kd4.a((Object) this.this$Anon0.a.q, (Object) this.$it)) {
                    this.this$Anon0.a.q = this.$it;
                    List list = this.$it;
                    kd4.a((Object) list, "it");
                    DianaPreset dianaPreset = (DianaPreset) this.this$Anon0.a.d.a();
                    List<WatchFaceWrapper> a2 = tj2.a((List<WatchFace>) list, (List<DianaPresetComplicationSetting>) dianaPreset != null ? dianaPreset.getComplications() : null);
                    pi4 c = nh4.c();
                    C0137Anon1 anon1 = new C0137Anon1(this, a2, (yb4) null);
                    this.L$Anon0 = zg4;
                    this.L$Anon1 = a2;
                    this.label = 1;
                    if (yf4.a(c, anon1, this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                List list2 = (List) this.L$Anon1;
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return qa4.a;
        }
    }

    @DexIgnore
    public DianaCustomizeViewModel$watchFaceObserver$Anon1(DianaCustomizeViewModel dianaCustomizeViewModel) {
        this.a = dianaCustomizeViewModel;
    }

    @DexIgnore
    public final void a(List<WatchFace> list) {
        fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, list, (yb4) null), 3, (Object) null);
    }
}
