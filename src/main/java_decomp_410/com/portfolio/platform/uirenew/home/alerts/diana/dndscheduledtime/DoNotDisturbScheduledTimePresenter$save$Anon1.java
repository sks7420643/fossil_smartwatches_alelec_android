package com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$save$Anon1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {98}, m = "invokeSuspend")
public final class DoNotDisturbScheduledTimePresenter$save$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DoNotDisturbScheduledTimePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$save$Anon1$Anon1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DoNotDisturbScheduledTimePresenter$save$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ DNDScheduledTimeModel e;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 f;

            @DexIgnore
            public a(DNDScheduledTimeModel dNDScheduledTimeModel, Anon1 anon1) {
                this.e = dNDScheduledTimeModel;
                this.f = anon1;
            }

            @DexIgnore
            public final void run() {
                this.f.this$Anon0.this$Anon0.k.getDNDScheduledTimeDao().insertDNDScheduledTime(this.e);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ DNDScheduledTimeModel e;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 f;

            @DexIgnore
            public b(DNDScheduledTimeModel dNDScheduledTimeModel, Anon1 anon1) {
                this.e = dNDScheduledTimeModel;
                this.f = anon1;
            }

            @DexIgnore
            public final void run() {
                this.f.this$Anon0.this$Anon0.k.getDNDScheduledTimeDao().insertDNDScheduledTime(this.e);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(DoNotDisturbScheduledTimePresenter$save$Anon1 doNotDisturbScheduledTimePresenter$save$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = doNotDisturbScheduledTimePresenter$save$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                if (this.this$Anon0.this$Anon0.g == 0) {
                    DNDScheduledTimeModel c = this.this$Anon0.this$Anon0.h;
                    if (c == null) {
                        return null;
                    }
                    c.setMinutes(this.this$Anon0.this$Anon0.f);
                    this.this$Anon0.this$Anon0.k.runInTransaction((Runnable) new a(c, this));
                    return qa4.a;
                }
                DNDScheduledTimeModel b2 = this.this$Anon0.this$Anon0.i;
                if (b2 == null) {
                    return null;
                }
                b2.setMinutes(this.this$Anon0.this$Anon0.f);
                this.this$Anon0.this$Anon0.k.runInTransaction((Runnable) new b(b2, this));
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DoNotDisturbScheduledTimePresenter$save$Anon1(DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = doNotDisturbScheduledTimePresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DoNotDisturbScheduledTimePresenter$save$Anon1 doNotDisturbScheduledTimePresenter$save$Anon1 = new DoNotDisturbScheduledTimePresenter$save$Anon1(this.this$Anon0, yb4);
        doNotDisturbScheduledTimePresenter$save$Anon1.p$ = (zg4) obj;
        return doNotDisturbScheduledTimePresenter$save$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DoNotDisturbScheduledTimePresenter$save$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 a2 = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            if (yf4.a(a2, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.j.close();
        return qa4.a;
    }
}
