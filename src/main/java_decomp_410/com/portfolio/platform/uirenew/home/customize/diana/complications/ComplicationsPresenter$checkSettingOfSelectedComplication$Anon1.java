package com.portfolio.platform.uirenew.home.customize.diana.complications;

import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.ok2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import java.util.Iterator;
import kotlin.Triple;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1", f = "ComplicationsPresenter.kt", l = {260}, m = "invokeSuspend")
public final class ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $complicationId;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public boolean Z$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ComplicationsPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1$Anon1", f = "ComplicationsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Parcelable>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1 complicationsPresenter$checkSettingOfSelectedComplication$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = complicationsPresenter$checkSettingOfSelectedComplication$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return ComplicationsPresenter.g(this.this$Anon0.this$Anon0).g(this.this$Anon0.$complicationId);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1(ComplicationsPresenter complicationsPresenter, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = complicationsPresenter;
        this.$complicationId = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1 complicationsPresenter$checkSettingOfSelectedComplication$Anon1 = new ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1(this.this$Anon0, this.$complicationId, yb4);
        complicationsPresenter$checkSettingOfSelectedComplication$Anon1.p$ = (zg4) obj;
        return complicationsPresenter$checkSettingOfSelectedComplication$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ComplicationsPresenter$checkSettingOfSelectedComplication$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0091  */
    public final Object invokeSuspend(T t) {
        boolean z;
        Ref$ObjectRef ref$ObjectRef;
        T t2;
        Ref$ObjectRef ref$ObjectRef2;
        T a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(t);
            zg4 zg4 = this.p$;
            ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            z = ok2.c.d(this.$complicationId);
            if (z) {
                ug4 a2 = this.this$Anon0.b();
                Anon1 anon1 = new Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = ref$ObjectRef;
                this.Z$Anon0 = z;
                this.L$Anon2 = ref$ObjectRef;
                this.label = 1;
                t = yf4.a(a2, anon1, this);
                if (t == a) {
                    return a;
                }
                ref$ObjectRef2 = ref$ObjectRef;
            }
            FLogger.INSTANCE.getLocal().d(ComplicationsPresenter.w, "checkSettingOfSelectedComplication complicationId=" + this.$complicationId + " settings=" + ((Parcelable) ref$ObjectRef.element));
            if (((Parcelable) ref$ObjectRef.element) != null) {
                DianaPreset a3 = ComplicationsPresenter.g(this.this$Anon0).c().a();
                if (a3 != null) {
                    DianaPreset clone = a3.clone();
                    Iterator<T> it = clone.getComplications().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t2 = null;
                            break;
                        }
                        t2 = it.next();
                        if (dc4.a(kd4.a((Object) ((DianaPresetComplicationSetting) t2).getId(), (Object) this.$complicationId)).booleanValue()) {
                            break;
                        }
                    }
                    DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t2;
                    if (dianaPresetComplicationSetting != null) {
                        dianaPresetComplicationSetting.setSettings(this.this$Anon0.l.a((Object) (Parcelable) ref$ObjectRef.element));
                        ComplicationsPresenter.g(this.this$Anon0).a(clone);
                    }
                }
            }
            this.this$Anon0.k.a(new Triple(this.$complicationId, dc4.a(z), (Parcelable) ref$ObjectRef.element));
            return qa4.a;
        } else if (i == 1) {
            ref$ObjectRef2 = (Ref$ObjectRef) this.L$Anon2;
            boolean z2 = this.Z$Anon0;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(t);
            z = z2;
            ref$ObjectRef = (Ref$ObjectRef) this.L$Anon1;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ref$ObjectRef2.element = (Parcelable) t;
        FLogger.INSTANCE.getLocal().d(ComplicationsPresenter.w, "checkSettingOfSelectedComplication complicationId=" + this.$complicationId + " settings=" + ((Parcelable) ref$ObjectRef.element));
        if (((Parcelable) ref$ObjectRef.element) != null) {
        }
        this.this$Anon0.k.a(new Triple(this.$complicationId, dc4.a(z), (Parcelable) ref$ObjectRef.element));
        return qa4.a;
    }
}
