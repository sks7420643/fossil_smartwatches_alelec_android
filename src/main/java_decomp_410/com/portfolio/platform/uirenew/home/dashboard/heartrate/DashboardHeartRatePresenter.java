package com.portfolio.platform.uirenew.home.dashboard.heartrate;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ac3;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.obfuscated.yb3;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.zb3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DashboardHeartRatePresenter extends yb3 implements PagingRequestHelper.a {
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public Listing<DailyHeartRateSummary> g;
    @DexIgnore
    public /* final */ zb3 h;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository i;
    @DexIgnore
    public /* final */ FitnessDataRepository j;
    @DexIgnore
    public /* final */ HeartRateDailySummaryDao k;
    @DexIgnore
    public /* final */ FitnessDatabase l;
    @DexIgnore
    public /* final */ UserRepository m;
    @DexIgnore
    public /* final */ h42 n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public DashboardHeartRatePresenter(zb3 zb3, HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, HeartRateDailySummaryDao heartRateDailySummaryDao, FitnessDatabase fitnessDatabase, UserRepository userRepository, h42 h42) {
        kd4.b(zb3, "mView");
        kd4.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        kd4.b(fitnessDataRepository, "mFitnessDataRepository");
        kd4.b(heartRateDailySummaryDao, "mHeartRateDailySummaryDao");
        kd4.b(fitnessDatabase, "mFitnessDatabase");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(h42, "mAppExecutors");
        this.h = zb3;
        this.i = heartRateSummaryRepository;
        this.j = fitnessDataRepository;
        this.k = heartRateDailySummaryDao;
        this.l = fitnessDatabase;
        this.m = userRepository;
        this.n = h42;
    }

    @DexIgnore
    public void j() {
        MFLogger.d("DashboardHeartRatePresenter", "retry all failed request");
        Listing<DailyHeartRateSummary> listing = this.g;
        if (listing != null) {
            wc4<qa4> retry = listing.getRetry();
            if (retry != null) {
                qa4 invoke = retry.invoke();
            }
        }
    }

    @DexIgnore
    public void k() {
        this.h.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("DashboardHeartRatePresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!rk2.s(this.f).booleanValue()) {
            this.f = new Date();
            Listing<DailyHeartRateSummary> listing = this.g;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardHeartRatePresenter", "stop");
    }

    @DexIgnore
    public void h() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DashboardHeartRatePresenter$initDataSource$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        try {
            Listing<DailyHeartRateSummary> listing = this.g;
            if (listing != null) {
                LiveData<qd<DailyHeartRateSummary>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    zb3 zb3 = this.h;
                    if (zb3 != null) {
                        pagedList.a((LifecycleOwner) (ac3) zb3);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRateFragment");
                    }
                }
            }
            this.i.removePagingListener();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(qa4.a);
            local.e("DashboardHeartRatePresenter", sb.toString());
        }
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        kd4.b(eVar, "report");
        MFLogger.d("DashboardHeartRatePresenter", "onStatusChange status=" + eVar);
        if (eVar.a()) {
            this.h.f();
        }
    }
}
