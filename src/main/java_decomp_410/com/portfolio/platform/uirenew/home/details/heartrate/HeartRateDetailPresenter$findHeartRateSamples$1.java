package com.portfolio.platform.uirenew.home.details.heartrate;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateDetailPresenter$findHeartRateSamples$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.portfolio.platform.data.model.diana.heartrate.HeartRateSample, java.lang.Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $date;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateDetailPresenter$findHeartRateSamples$1(java.util.Date date) {
        super(1);
        this.$date = date;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke((com.portfolio.platform.data.model.diana.heartrate.HeartRateSample) obj));
    }

    @DexIgnore
    public final boolean invoke(com.portfolio.platform.data.model.diana.heartrate.HeartRateSample heartRateSample) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(heartRateSample, "it");
        return com.fossil.blesdk.obfuscated.rk2.m27396d(heartRateSample.getDate(), this.$date);
    }
}
