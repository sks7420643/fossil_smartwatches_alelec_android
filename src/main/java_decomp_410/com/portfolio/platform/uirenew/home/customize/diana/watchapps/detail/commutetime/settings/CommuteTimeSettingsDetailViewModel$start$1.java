package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel$start$1", mo27670f = "CommuteTimeSettingsDetailViewModel.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class CommuteTimeSettingsDetailViewModel$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22877p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsDetailViewModel$start$1(com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = commuteTimeSettingsDetailViewModel;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel$start$1 commuteTimeSettingsDetailViewModel$start$1 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel$start$1(this.this$0, yb4);
        commuteTimeSettingsDetailViewModel$start$1.f22877p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return commuteTimeSettingsDetailViewModel$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            this.this$0.f22863c = com.google.android.libraries.places.api.Places.createClient(com.portfolio.platform.PortfolioApp.f20941W.mo34589c());
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper a = this.this$0.f22864d;
            java.lang.Boolean bool = null;
            boolean z = (a != null ? a.getType() : null) == com.portfolio.platform.data.model.diana.commutetime.AddressWrapper.AddressType.OTHER;
            com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.this$0;
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper a2 = commuteTimeSettingsDetailViewModel.f22864d;
            java.lang.String name = a2 != null ? a2.getName() : null;
            java.lang.Boolean a3 = com.fossil.blesdk.obfuscated.dc4.m20839a(z);
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper a4 = this.this$0.f22864d;
            java.lang.String address = a4 != null ? a4.getAddress() : null;
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper a5 = this.this$0.f22864d;
            if (a5 != null) {
                bool = com.fossil.blesdk.obfuscated.dc4.m20839a(a5.getAvoidTolls());
            }
            com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel.m34094a(commuteTimeSettingsDetailViewModel, name, address, bool, a3, (java.lang.Boolean) null, this.this$0.f22863c, (java.lang.Boolean) null, 80, (java.lang.Object) null);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
