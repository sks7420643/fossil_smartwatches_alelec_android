package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel$start$Anon1", f = "CommuteTimeWatchAppSettingsViewModel.kt", l = {69}, m = "invokeSuspend")
public final class CommuteTimeWatchAppSettingsViewModel$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeWatchAppSettingsViewModel this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel$start$Anon1$Anon1", f = "CommuteTimeWatchAppSettingsViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super MFUser>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeWatchAppSettingsViewModel$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(CommuteTimeWatchAppSettingsViewModel$start$Anon1 commuteTimeWatchAppSettingsViewModel$start$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = commuteTimeWatchAppSettingsViewModel$start$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return this.this$Anon0.this$Anon0.g.getCurrentUser();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppSettingsViewModel$start$Anon1(CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = commuteTimeWatchAppSettingsViewModel;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CommuteTimeWatchAppSettingsViewModel$start$Anon1 commuteTimeWatchAppSettingsViewModel$start$Anon1 = new CommuteTimeWatchAppSettingsViewModel$start$Anon1(this.this$Anon0, yb4);
        commuteTimeWatchAppSettingsViewModel$start$Anon1.p$ = (zg4) obj;
        return commuteTimeWatchAppSettingsViewModel$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CommuteTimeWatchAppSettingsViewModel$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel;
        Object a = cc4.a();
        int i = this.label;
        List<AddressWrapper> list = null;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel2 = this.this$Anon0;
            ug4 b = nh4.b();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = commuteTimeWatchAppSettingsViewModel2;
            this.label = 1;
            obj = yf4.a(b, anon1, this);
            if (obj == a) {
                return a;
            }
            commuteTimeWatchAppSettingsViewModel = commuteTimeWatchAppSettingsViewModel2;
        } else if (i == 1) {
            commuteTimeWatchAppSettingsViewModel = (CommuteTimeWatchAppSettingsViewModel) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        commuteTimeWatchAppSettingsViewModel.e = (MFUser) obj;
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel3 = this.this$Anon0;
        CommuteTimeWatchAppSetting a2 = commuteTimeWatchAppSettingsViewModel3.d;
        if (a2 != null) {
            list = a2.getAddresses();
        }
        commuteTimeWatchAppSettingsViewModel3.a(list);
        return qa4.a;
    }
}
