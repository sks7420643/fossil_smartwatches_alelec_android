package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.dw2;
import com.fossil.blesdk.obfuscated.ew2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationAppsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public NotificationAppsPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment) {
            kd4.b(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationAppsActivity.class);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        dw2 dw2 = (dw2) getSupportFragmentManager().a((int) R.id.content);
        if (dw2 == null) {
            dw2 = dw2.q.b();
            a((Fragment) dw2, dw2.q.a(), (int) R.id.content);
        }
        l42 g = PortfolioApp.W.c().g();
        if (dw2 != null) {
            g.a(new ew2(dw2)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsContract.View");
    }
}
