package com.portfolio.platform.uirenew.home.dashboard;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1", mo27670f = "HomeDashboardPresenter.kt", mo27671l = {102, 103, 104, 105}, mo27672m = "invokeSuspend")
public final class HomeDashboardPresenter$checkDeviceStatus$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23101p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDashboardPresenter$checkDeviceStatus$1(com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter homeDashboardPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homeDashboardPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1 homeDashboardPresenter$checkDeviceStatus$1 = new com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1(this.this$0, yb4);
        homeDashboardPresenter$checkDeviceStatus$1.f23101p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeDashboardPresenter$checkDeviceStatus$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b0 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00d1 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0156  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0207  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.data.SleepStatistic sleepStatistic;
        com.portfolio.platform.data.ActivityStatistic activityStatistic;
        java.util.List list;
        java.lang.Object obj2;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a;
        java.lang.Object a2;
        com.fossil.blesdk.obfuscated.zg4 zg42;
        java.lang.Object a3 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg42 = this.f23101p$;
            com.fossil.blesdk.obfuscated.ug4 a4 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$activityStatistic$1 homeDashboardPresenter$checkDeviceStatus$1$activityStatistic$1 = new com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$activityStatistic$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg42;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a4, homeDashboardPresenter$checkDeviceStatus$1$activityStatistic$1, this);
            if (obj == a3) {
                return a3;
            }
        } else if (i == 1) {
            zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            activityStatistic = (com.portfolio.platform.data.ActivityStatistic) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.SleepStatistic sleepStatistic2 = (com.portfolio.platform.data.SleepStatistic) obj;
            com.fossil.blesdk.obfuscated.ug4 a5 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$totalDevices$1 homeDashboardPresenter$checkDeviceStatus$1$totalDevices$1 = new com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$totalDevices$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = activityStatistic;
            this.L$2 = sleepStatistic2;
            this.label = 3;
            a2 = com.fossil.blesdk.obfuscated.yf4.m30997a(a5, homeDashboardPresenter$checkDeviceStatus$1$totalDevices$1, this);
            if (a2 != a3) {
                return a3;
            }
            java.lang.Object obj3 = a2;
            sleepStatistic = sleepStatistic2;
            obj = obj3;
            java.util.List list2 = (java.util.List) obj;
            com.fossil.blesdk.obfuscated.ug4 a6 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$deviceName$1 homeDashboardPresenter$checkDeviceStatus$1$deviceName$1 = new com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$deviceName$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = activityStatistic;
            this.L$2 = sleepStatistic;
            this.L$3 = list2;
            this.label = 4;
            a = com.fossil.blesdk.obfuscated.yf4.m30997a(a6, homeDashboardPresenter$checkDeviceStatus$1$deviceName$1, this);
            if (a != a3) {
            }
        } else if (i == 3) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.ActivityStatistic activityStatistic2 = (com.portfolio.platform.data.ActivityStatistic) this.L$1;
            sleepStatistic = (com.portfolio.platform.data.SleepStatistic) this.L$2;
            activityStatistic = activityStatistic2;
            java.util.List list22 = (java.util.List) obj;
            com.fossil.blesdk.obfuscated.ug4 a62 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$deviceName$1 homeDashboardPresenter$checkDeviceStatus$1$deviceName$12 = new com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$deviceName$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = activityStatistic;
            this.L$2 = sleepStatistic;
            this.L$3 = list22;
            this.label = 4;
            a = com.fossil.blesdk.obfuscated.yf4.m30997a(a62, homeDashboardPresenter$checkDeviceStatus$1$deviceName$12, this);
            if (a != a3) {
                return a3;
            }
            list = list22;
            obj = a;
            java.lang.String str = (java.lang.String) obj;
            if (com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34537g(this.this$0.f23077h) != 2) {
            }
            this.this$0.f23085p = (activityStatistic != null || activityStatistic.getTotalSteps() == 0) && (sleepStatistic == null || sleepStatistic.getTotalSleeps() == 0);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("checkDeviceStatus activity ");
            sb.append(activityStatistic == null ? com.fossil.blesdk.obfuscated.dc4.m20843a(activityStatistic.getTotalSteps()) : null);
            sb.append(" sleep ");
            sb.append(sleepStatistic == null ? com.fossil.blesdk.obfuscated.dc4.m20843a(sleepStatistic.getTotalSleeps()) : null);
            sb.append(" totalDevices ");
            sb.append(list.size());
            local.mo33255d("HomeDashboardPresenter", sb.toString());
            if (!(!list.isEmpty())) {
            }
            this.this$0.mo41197a("release");
            this.this$0.mo41204o();
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 4) {
            list = (java.util.List) this.L$3;
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            sleepStatistic = (com.portfolio.platform.data.SleepStatistic) this.L$2;
            activityStatistic = (com.portfolio.platform.data.ActivityStatistic) this.L$1;
            java.lang.String str2 = (java.lang.String) obj;
            boolean z = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34537g(this.this$0.f23077h) != 2;
            this.this$0.f23085p = (activityStatistic != null || activityStatistic.getTotalSteps() == 0) && (sleepStatistic == null || sleepStatistic.getTotalSleeps() == 0);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
            sb2.append("checkDeviceStatus activity ");
            sb2.append(activityStatistic == null ? com.fossil.blesdk.obfuscated.dc4.m20843a(activityStatistic.getTotalSteps()) : null);
            sb2.append(" sleep ");
            sb2.append(sleepStatistic == null ? com.fossil.blesdk.obfuscated.dc4.m20843a(sleepStatistic.getTotalSleeps()) : null);
            sb2.append(" totalDevices ");
            sb2.append(list.size());
            local2.mo33255d("HomeDashboardPresenter", sb2.toString());
            if (!(!list.isEmpty())) {
                java.util.Iterator it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    if (com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.portfolio.platform.data.model.Device) obj2).getDeviceId(), (java.lang.Object) this.this$0.f23077h)).booleanValue()) {
                        break;
                    }
                }
                com.portfolio.platform.data.model.Device device = (com.portfolio.platform.data.model.Device) obj2;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.StringBuilder sb3 = new java.lang.StringBuilder();
                sb3.append("checkDeviceStatus activeDevice ");
                sb3.append(device != null ? device.getDeviceId() : null);
                sb3.append(" currentSerial ");
                sb3.append(this.this$0.f23077h);
                local3.mo33255d("HomeDashboardPresenter", sb3.toString());
                if (device != null) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HomeDashboardPresenter", "checkDeviceStatus isActiveDeviceConnected " + z);
                    this.this$0.f23093x.mo30960a(this.this$0.f23077h, str2);
                    this.this$0.f23093x.mo30962a(true, true, this.this$0.f23085p);
                } else {
                    this.this$0.f23093x.mo30960a((java.lang.String) null, "");
                    this.this$0.f23093x.mo30962a(true, false, this.this$0.f23085p);
                }
            } else {
                this.this$0.f23093x.mo30960a((java.lang.String) null, "");
                this.this$0.f23093x.mo30962a(false, false, this.this$0.f23085p);
            }
            this.this$0.mo41197a("release");
            this.this$0.mo41204o();
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.ActivityStatistic activityStatistic3 = (com.portfolio.platform.data.ActivityStatistic) obj;
        com.fossil.blesdk.obfuscated.ug4 a7 = this.this$0.mo31441c();
        com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$sleepStatistic$1 homeDashboardPresenter$checkDeviceStatus$1$sleepStatistic$1 = new com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$sleepStatistic$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg42;
        this.L$1 = activityStatistic3;
        this.label = 2;
        java.lang.Object a8 = com.fossil.blesdk.obfuscated.yf4.m30997a(a7, homeDashboardPresenter$checkDeviceStatus$1$sleepStatistic$1, this);
        if (a8 == a3) {
            return a3;
        }
        com.fossil.blesdk.obfuscated.zg4 zg44 = zg42;
        activityStatistic = activityStatistic3;
        obj = a8;
        zg4 = zg44;
        com.portfolio.platform.data.SleepStatistic sleepStatistic22 = (com.portfolio.platform.data.SleepStatistic) obj;
        com.fossil.blesdk.obfuscated.ug4 a52 = this.this$0.mo31441c();
        com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$totalDevices$1 homeDashboardPresenter$checkDeviceStatus$1$totalDevices$12 = new com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$totalDevices$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.L$1 = activityStatistic;
        this.L$2 = sleepStatistic22;
        this.label = 3;
        a2 = com.fossil.blesdk.obfuscated.yf4.m30997a(a52, homeDashboardPresenter$checkDeviceStatus$1$totalDevices$12, this);
        if (a2 != a3) {
        }
    }
}
