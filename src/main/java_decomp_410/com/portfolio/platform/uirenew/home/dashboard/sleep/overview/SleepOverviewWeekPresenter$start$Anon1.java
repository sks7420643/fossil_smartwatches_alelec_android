package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ud3;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.vd3;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.enums.Status;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$start$Anon1", f = "SleepOverviewWeekPresenter.kt", l = {50}, m = "invokeSuspend")
public final class SleepOverviewWeekPresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepOverviewWeekPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2<T> implements cc<os3<? extends List<MFSleepDay>>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewWeekPresenter$start$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @gc4(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$start$Anon1$Anon2$Anon1", f = "SleepOverviewWeekPresenter.kt", l = {61}, m = "invokeSuspend")
        public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
            @DexIgnore
            public Object L$Anon0;
            @DexIgnore
            public Object L$Anon1;
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon2 this$Anon0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$start$Anon1$Anon2$Anon1$Anon1")
            @gc4(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$start$Anon1$Anon2$Anon1$Anon1", f = "SleepOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$start$Anon1$Anon2$Anon1$Anon1  reason: collision with other inner class name */
            public static final class C0157Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super BarChart.c>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public zg4 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Anon1 this$Anon0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0157Anon1(Anon1 anon1, yb4 yb4) {
                    super(2, yb4);
                    this.this$Anon0 = anon1;
                }

                @DexIgnore
                public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                    kd4.b(yb4, "completion");
                    C0157Anon1 anon1 = new C0157Anon1(this.this$Anon0, yb4);
                    anon1.p$ = (zg4) obj;
                    return anon1;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0157Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    cc4.a();
                    if (this.label == 0) {
                        na4.a(obj);
                        SleepOverviewWeekPresenter sleepOverviewWeekPresenter = this.this$Anon0.this$Anon0.a.this$Anon0;
                        Date d = SleepOverviewWeekPresenter.d(sleepOverviewWeekPresenter);
                        os3 os3 = (os3) this.this$Anon0.this$Anon0.a.this$Anon0.g.a();
                        return sleepOverviewWeekPresenter.a(d, (List<MFSleepDay>) os3 != null ? (List) os3.d() : null);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(Anon2 anon2, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon2;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                SleepOverviewWeekPresenter sleepOverviewWeekPresenter;
                Object a = cc4.a();
                int i = this.label;
                if (i == 0) {
                    na4.a(obj);
                    zg4 zg4 = this.p$;
                    SleepOverviewWeekPresenter sleepOverviewWeekPresenter2 = this.this$Anon0.a.this$Anon0;
                    ug4 a2 = sleepOverviewWeekPresenter2.b();
                    C0157Anon1 anon1 = new C0157Anon1(this, (yb4) null);
                    this.L$Anon0 = zg4;
                    this.L$Anon1 = sleepOverviewWeekPresenter2;
                    this.label = 1;
                    obj = yf4.a(a2, anon1, this);
                    if (obj == a) {
                        return a;
                    }
                    sleepOverviewWeekPresenter = sleepOverviewWeekPresenter2;
                } else if (i == 1) {
                    sleepOverviewWeekPresenter = (SleepOverviewWeekPresenter) this.L$Anon1;
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                sleepOverviewWeekPresenter.h = (BarChart.c) obj;
                this.this$Anon0.a.this$Anon0.i.a(this.this$Anon0.a.this$Anon0.h);
                return qa4.a;
            }
        }

        @DexIgnore
        public Anon2(SleepOverviewWeekPresenter$start$Anon1 sleepOverviewWeekPresenter$start$Anon1) {
            this.a = sleepOverviewWeekPresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(os3<? extends List<MFSleepDay>> os3) {
            Status a2 = os3.a();
            List list = (List) os3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mSleepSummaries -- sleepSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("SleepOverviewWeekPresenter", sb.toString());
            if (a2 != Status.DATABASE_LOADING) {
                fi4 unused = ag4.b(this.a.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (yb4) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepOverviewWeekPresenter$start$Anon1(SleepOverviewWeekPresenter sleepOverviewWeekPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = sleepOverviewWeekPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SleepOverviewWeekPresenter$start$Anon1 sleepOverviewWeekPresenter$start$Anon1 = new SleepOverviewWeekPresenter$start$Anon1(this.this$Anon0, yb4);
        sleepOverviewWeekPresenter$start$Anon1.p$ = (zg4) obj;
        return sleepOverviewWeekPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepOverviewWeekPresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0103  */
    public final Object invokeSuspend(Object obj) {
        ud3 g;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            this.this$Anon0.h();
            if (this.this$Anon0.f == null || !rk2.s(SleepOverviewWeekPresenter.d(this.this$Anon0)).booleanValue()) {
                this.this$Anon0.f = new Date();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SleepOverviewWeekPresenter", "start - mDate=" + SleepOverviewWeekPresenter.d(this.this$Anon0));
                ug4 a2 = this.this$Anon0.b();
                SleepOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1 sleepOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1 = new SleepOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = yf4.a(a2, sleepOverviewWeekPresenter$start$Anon1$startAndEnd$Anon1, this);
                if (obj == a) {
                    return a;
                }
            }
            LiveData e = this.this$Anon0.g;
            g = this.this$Anon0.i;
            if (g == null) {
                e.a((vd3) g, new Anon2(this));
                return qa4.a;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekFragment");
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair = (Pair) obj;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("SleepOverviewWeekPresenter", "start - startDate=" + ((Date) pair.getFirst()) + ", endDate=" + ((Date) pair.getSecond()));
        Date date = (Date) pair.getFirst();
        Date date2 = (Date) pair.getSecond();
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        local3.d("SleepOverviewWeekPresenter", "start - startDate=" + date + ", endDate=" + date2);
        SleepOverviewWeekPresenter sleepOverviewWeekPresenter = this.this$Anon0;
        sleepOverviewWeekPresenter.g = sleepOverviewWeekPresenter.k.getSleepSummaries(date, date2, false);
        LiveData e2 = this.this$Anon0.g;
        g = this.this$Anon0.i;
        if (g == null) {
        }
    }
}
