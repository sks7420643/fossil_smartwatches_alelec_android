package com.portfolio.platform.uirenew.home.details.goaltracking;

import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.df3;
import com.fossil.blesdk.obfuscated.dl4;
import com.fossil.blesdk.obfuscated.ef3;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.ff3;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.fl4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDetailPresenter extends df3 implements PagingRequestHelper.a {
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public MutableLiveData<Pair<Date, Date>> h; // = new MutableLiveData<>();
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ dl4 k; // = fl4.a(false, 1, (Object) null);
    @DexIgnore
    public List<GoalTrackingSummary> l; // = new ArrayList();
    @DexIgnore
    public GoalTrackingSummary m;
    @DexIgnore
    public List<GoalTrackingData> n;
    @DexIgnore
    public LiveData<os3<List<GoalTrackingSummary>>> o;
    @DexIgnore
    public Listing<GoalTrackingData> p;
    @DexIgnore
    public /* final */ ef3 q;
    @DexIgnore
    public /* final */ GoalTrackingRepository r;
    @DexIgnore
    public /* final */ GoalTrackingDao s;
    @DexIgnore
    public /* final */ GoalTrackingDatabase t;
    @DexIgnore
    public /* final */ h42 u;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements m3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter a;

        @DexIgnore
        public b(GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
            this.a = goalTrackingDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<os3<List<GoalTrackingSummary>>> apply(Pair<? extends Date, ? extends Date> pair) {
            return this.a.r.getSummaries((Date) pair.component1(), (Date) pair.component2(), true);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public GoalTrackingDetailPresenter(ef3 ef3, GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, h42 h42) {
        kd4.b(ef3, "mView");
        kd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        kd4.b(goalTrackingDao, "mGoalTrackingDao");
        kd4.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        kd4.b(h42, "mAppExecutors");
        this.q = ef3;
        this.r = goalTrackingRepository;
        this.s = goalTrackingDao;
        this.t = goalTrackingDatabase;
        this.u = h42;
        LiveData<os3<List<GoalTrackingSummary>>> b2 = hc.b(this.h, new b(this));
        kd4.a((Object) b2, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.o = b2;
    }

    @DexIgnore
    public final void d(Date date) {
        h();
        GoalTrackingRepository goalTrackingRepository = this.r;
        this.p = goalTrackingRepository.getGoalTrackingDataPaging(date, goalTrackingRepository, this.s, this.t, this.u, this);
        Listing<GoalTrackingData> listing = this.p;
        if (listing != null) {
            LiveData<qd<GoalTrackingData>> pagedList = listing.getPagedList();
            if (pagedList != null) {
                ef3 ef3 = this.q;
                if (ef3 != null) {
                    pagedList.a((ff3) ef3, new GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1(this));
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
            }
        }
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        n();
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", "stop");
        LiveData<os3<List<GoalTrackingSummary>>> liveData = this.o;
        ef3 ef3 = this.q;
        if (ef3 != null) {
            liveData.a((LifecycleOwner) (ff3) ef3);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
    }

    @DexIgnore
    public void h() {
        try {
            Listing<GoalTrackingData> listing = this.p;
            if (listing != null) {
                LiveData<qd<GoalTrackingData>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    ef3 ef3 = this.q;
                    if (ef3 != null) {
                        pagedList.a((LifecycleOwner) (ff3) ef3);
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
                }
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(qa4.a);
            local.e("GoalTrackingDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void i() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", "retry all failed request");
        Listing<GoalTrackingData> listing = this.p;
        if (listing != null) {
            wc4<qa4> retry = listing.getRetry();
            if (retry != null) {
                qa4 invoke = retry.invoke();
            }
        }
    }

    @DexIgnore
    public void j() {
        Date l2 = rk2.l(this.g);
        kd4.a((Object) l2, "DateHelper.getNextDate(mDate)");
        c(l2);
    }

    @DexIgnore
    public void k() {
        Date m2 = rk2.m(this.g);
        kd4.a((Object) m2, "DateHelper.getPrevDate(mDate)");
        c(m2);
    }

    @DexIgnore
    public void l() {
        this.q.a(this);
    }

    @DexIgnore
    public final fi4 m() {
        return ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingDetailPresenter$showDetailChart$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void n() {
        LiveData<os3<List<GoalTrackingSummary>>> liveData = this.o;
        ef3 ef3 = this.q;
        if (ef3 != null) {
            liveData.a((ff3) ef3, new GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
    }

    @DexIgnore
    public void c(Date date) {
        kd4.b(date, "date");
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingDetailPresenter$setDate$Anon1(this, date, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void b(Date date) {
        kd4.b(date, "date");
        d(date);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        kd4.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.g.getTime());
    }

    @DexIgnore
    public void a(Date date) {
        kd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailPresenter", "addRecord date=" + date);
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingDetailPresenter$addRecord$Anon1(this, date, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(GoalTrackingData goalTrackingData) {
        kd4.b(goalTrackingData, OrmLiteConfigUtil.RAW_DIR_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailPresenter", "deleteRecord GoalTrackingData=" + goalTrackingData);
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingDetailPresenter$deleteRecord$Anon1(this, goalTrackingData, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final GoalTrackingSummary a(Date date, List<GoalTrackingSummary> list) {
        T t2 = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (rk2.d(((GoalTrackingSummary) next).getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return (GoalTrackingSummary) t2;
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        kd4.b(eVar, "report");
        if (eVar.a()) {
            this.q.f();
        }
    }
}
