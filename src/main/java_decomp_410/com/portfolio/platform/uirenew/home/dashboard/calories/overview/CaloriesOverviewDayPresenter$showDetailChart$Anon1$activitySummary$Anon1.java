package com.portfolio.platform.uirenew.home.dashboard.calories.overview;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1", f = "CaloriesOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
public final class CaloriesOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super ActivitySummary>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CaloriesOverviewDayPresenter$showDetailChart$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CaloriesOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1(CaloriesOverviewDayPresenter$showDetailChart$Anon1 caloriesOverviewDayPresenter$showDetailChart$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = caloriesOverviewDayPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CaloriesOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1 caloriesOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1 = new CaloriesOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1(this.this$Anon0, yb4);
        caloriesOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1.p$ = (zg4) obj;
        return caloriesOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CaloriesOverviewDayPresenter$showDetailChart$Anon1$activitySummary$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            os3 os3 = (os3) this.this$Anon0.this$Anon0.i.a();
            ActivitySummary activitySummary = null;
            if (os3 == null) {
                return null;
            }
            List list = (List) os3.d();
            if (list == null) {
                return null;
            }
            Iterator it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                if (dc4.a(rk2.d(next.getDate(), CaloriesOverviewDayPresenter.d(this.this$Anon0.this$Anon0))).booleanValue()) {
                    activitySummary = next;
                    break;
                }
            }
            return activitySummary;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
