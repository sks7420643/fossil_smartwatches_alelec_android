package com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.j33;
import com.fossil.blesdk.obfuscated.k33;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Ringtone;
import java.util.ArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SearchRingPhonePresenter extends j33 {
    @DexIgnore
    public /* final */ ArrayList<Ringtone> f; // = new ArrayList<>();
    @DexIgnore
    public Ringtone g;
    @DexIgnore
    public /* final */ Gson h; // = new Gson();
    @DexIgnore
    public /* final */ k33 i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public SearchRingPhonePresenter(k33 k33) {
        kd4.b(k33, "mView");
        this.i = k33;
    }

    @DexIgnore
    public void f() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SearchRingPhonePresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public Ringtone h() {
        Ringtone ringtone = this.g;
        if (ringtone != null) {
            return ringtone;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void i() {
        fn2.o.a().b();
    }

    @DexIgnore
    public final k33 j() {
        return this.i;
    }

    @DexIgnore
    public void k() {
        this.i.a(this);
    }

    @DexIgnore
    public void b(Ringtone ringtone) {
        kd4.b(ringtone, Constants.RINGTONE);
        this.g = ringtone;
    }

    @DexIgnore
    public void a(Ringtone ringtone) {
        kd4.b(ringtone, Constants.RINGTONE);
        fn2.o.a().a(ringtone);
        this.g = ringtone;
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, MicroAppSetting.SETTING);
        try {
            this.g = (Ringtone) this.h.a(str, Ringtone.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SearchRingPhonePresenter", "exception when parse ringtone setting " + e);
        }
    }
}
