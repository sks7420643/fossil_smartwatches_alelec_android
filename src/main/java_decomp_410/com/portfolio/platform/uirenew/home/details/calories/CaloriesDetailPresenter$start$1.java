package com.portfolio.platform.uirenew.home.details.calories;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1", mo27670f = "CaloriesDetailPresenter.kt", mo27671l = {116}, mo27672m = "invokeSuspend")
public final class CaloriesDetailPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23656p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$1", mo27670f = "CaloriesDetailPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$1 */
    public static final class C67031 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.enums.Unit>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23657p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C67031(com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1 caloriesDetailPresenter$start$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = caloriesDetailPresenter$start$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67031 r0 = new com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67031(this.this$0, yb4);
            r0.f23657p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67031) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.portfolio.platform.data.model.MFUser currentUser = this.this$0.this$0.f23641v.getCurrentUser();
                if (currentUser != null) {
                    com.portfolio.platform.enums.Unit distanceUnit = currentUser.getDistanceUnit();
                    if (distanceUnit != null) {
                        return distanceUnit;
                    }
                }
                return com.portfolio.platform.enums.Unit.METRIC;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$2")
    /* renamed from: com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$2 */
    public static final class C67042<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1 f23658a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$2$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$2$1", mo27670f = "CaloriesDetailPresenter.kt", mo27671l = {130}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$2$1 */
        public static final class C67051 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f23659p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67042 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C67051(com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67042 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67042.C67051 r0 = new com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67042.C67051(this.this$0, yb4);
                r0.f23659p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67042.C67051) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23659p$;
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    local.mo33255d("XXX", "find todaySummary of " + this.this$0.f23658a.this$0.f23630k);
                    com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.f23658a.this$0.mo31440b();
                    com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$2$1$summary$1 caloriesDetailPresenter$start$1$2$1$summary$1 = new com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$2$1$summary$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg4;
                    this.label = 1;
                    obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, caloriesDetailPresenter$start$1$2$1$summary$1, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                com.portfolio.platform.data.model.room.fitness.ActivitySummary activitySummary = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) obj;
                if (this.this$0.f23658a.this$0.f23632m == null || (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23658a.this$0.f23632m, (java.lang.Object) activitySummary))) {
                    this.this$0.f23658a.this$0.f23632m = activitySummary;
                    this.this$0.f23658a.this$0.f23638s.mo31791a(this.this$0.f23658a.this$0.f23634o, this.this$0.f23658a.this$0.f23632m);
                    if (this.this$0.f23658a.this$0.f23628i && this.this$0.f23658a.this$0.f23629j) {
                        com.fossil.blesdk.obfuscated.fi4 unused = this.this$0.f23658a.this$0.mo41399l();
                    }
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        public C67042(com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1 caloriesDetailPresenter$start$1) {
            this.f23658a = caloriesDetailPresenter$start$1;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>> os3) {
            com.portfolio.platform.enums.Status a = os3.mo29972a();
            java.util.List list = (java.util.List) os3.mo29973b();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("start - summaryTransformations -- activitySummaries=");
            sb.append(list != null ? java.lang.Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a);
            local.mo33255d("CaloriesDetailPresenter", sb.toString());
            if (a == com.portfolio.platform.enums.Status.NETWORK_LOADING || a == com.portfolio.platform.enums.Status.SUCCESS) {
                this.f23658a.this$0.f23630k = list;
                this.f23658a.this$0.f23628i = true;
                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23658a.this$0.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67042.C67051(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$3")
    /* renamed from: com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$3 */
    public static final class C67063<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1 f23660a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$3$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$3$1", mo27670f = "CaloriesDetailPresenter.kt", mo27671l = {152}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$3$1 */
        public static final class C67071 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f23661p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67063 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C67071(com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67063 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67063.C67071 r0 = new com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67063.C67071(this.this$0, yb4);
                r0.f23661p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67063.C67071) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23661p$;
                    com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.f23660a.this$0.mo31440b();
                    com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$3$1$samples$1 caloriesDetailPresenter$start$1$3$1$samples$1 = new com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$3$1$samples$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg4;
                    this.label = 1;
                    obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, caloriesDetailPresenter$start$1$3$1$samples$1, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                java.util.List list = (java.util.List) obj;
                if (this.this$0.f23660a.this$0.f23633n == null || (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23660a.this$0.f23633n, (java.lang.Object) list))) {
                    this.this$0.f23660a.this$0.f23633n = list;
                    if (this.this$0.f23660a.this$0.f23628i && this.this$0.f23660a.this$0.f23629j) {
                        com.fossil.blesdk.obfuscated.fi4 unused = this.this$0.f23660a.this$0.mo41399l();
                    }
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        public C67063(com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1 caloriesDetailPresenter$start$1) {
            this.f23660a = caloriesDetailPresenter$start$1;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>> os3) {
            com.portfolio.platform.enums.Status a = os3.mo29972a();
            java.util.List list = (java.util.List) os3.mo29973b();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("start - sampleTransformations -- activitySamples=");
            sb.append(list != null ? java.lang.Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a);
            local.mo33255d("CaloriesDetailPresenter", sb.toString());
            if (a == com.portfolio.platform.enums.Status.NETWORK_LOADING || a == com.portfolio.platform.enums.Status.SUCCESS) {
                this.f23660a.this$0.f23631l = list;
                this.f23660a.this$0.f23629j = true;
                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23660a.this$0.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67063.C67071(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CaloriesDetailPresenter$start$1(com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter caloriesDetailPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = caloriesDetailPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1 caloriesDetailPresenter$start$1 = new com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1(this.this$0, yb4);
        caloriesDetailPresenter$start$1.f23656p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return caloriesDetailPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter caloriesDetailPresenter;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23656p$;
            com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter caloriesDetailPresenter2 = this.this$0;
            com.fossil.blesdk.obfuscated.ug4 a2 = caloriesDetailPresenter2.mo31440b();
            com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67031 r4 = new com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67031(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = caloriesDetailPresenter2;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r4, this);
            if (obj == a) {
                return a;
            }
            caloriesDetailPresenter = caloriesDetailPresenter2;
        } else if (i == 1) {
            caloriesDetailPresenter = (com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        caloriesDetailPresenter.f23634o = (com.portfolio.platform.enums.Unit) obj;
        androidx.lifecycle.LiveData q = this.this$0.f23635p;
        com.fossil.blesdk.obfuscated.ve3 o = this.this$0.f23638s;
        if (o != null) {
            q.mo2277a((com.fossil.blesdk.obfuscated.we3) o, new com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67042(this));
            this.this$0.f23636q.mo2277a((androidx.lifecycle.LifecycleOwner) this.this$0.f23638s, new com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1.C67063(this));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailFragment");
    }
}
