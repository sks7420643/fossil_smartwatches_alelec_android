package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {310}, m = "invokeSuspend")
public final class NotificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<? extends com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter>>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1 notificationCallsAndMessagesPresenter$setRuleToDevice$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = notificationCallsAndMessagesPresenter$setRuleToDevice$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1 notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1(this.this$0, yb4);
        notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v50, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: java.util.List} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object obj2;
        com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1 notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1 = this;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.label;
        java.util.List list = null;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.p$;
            com.fossil.blesdk.obfuscated.px2 e = notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.v;
            notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.L$0 = zg4;
            notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.L$1 = null;
            notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.label = 1;
            obj2 = com.fossil.blesdk.obfuscated.w52.a(e, null, notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1);
            if (obj2 == a) {
                return a;
            }
        } else if (i == 1) {
            list = notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
            obj2 = obj;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.CoroutineUseCase.c cVar = (com.portfolio.platform.CoroutineUseCase.c) obj2;
        if (cVar instanceof com.fossil.blesdk.obfuscated.px2.d) {
            list = new java.util.ArrayList();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.C.a();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("GetAllContactGroup onSuccess, size = ");
            com.fossil.blesdk.obfuscated.px2.d dVar = (com.fossil.blesdk.obfuscated.px2.d) cVar;
            sb.append(dVar.a().size());
            local.d(a2, sb.toString());
            notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.h = com.fossil.blesdk.obfuscated.kb4.d(dVar.a());
            if (notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.t.E() == 0) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.C.a(), "call everyone");
                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification = r8;
                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification2 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName.getAppName(), aApplicationName.getBundleCrc(), aApplicationName.getGroupId(), aApplicationName.getPackageName(), aApplicationName.getIconResourceId(), aApplicationName.getIconFwPath(), aApplicationName.getNotificationType());
                list.add(new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification));
            } else if (notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.t.E() == 1) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.C.a(), "call favorite");
                int size = notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.h.size();
                for (int i2 = 0; i2 < size; i2++) {
                    com.fossil.wearables.fsl.contact.ContactGroup contactGroup = (com.fossil.wearables.fsl.contact.ContactGroup) notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.h.get(i2);
                    com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName2 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                    com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification3 = r10;
                    com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification4 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName2.getAppName(), aApplicationName2.getBundleCrc(), aApplicationName2.getGroupId(), aApplicationName2.getPackageName(), aApplicationName2.getIconResourceId(), aApplicationName2.getIconFwPath(), aApplicationName2.getNotificationType());
                    com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter appNotificationFilter = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification3);
                    java.util.List<com.fossil.wearables.fsl.contact.Contact> contacts = contactGroup.getContacts();
                    com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) contacts, "item.contacts");
                    if (!contacts.isEmpty()) {
                        com.fossil.wearables.fsl.contact.Contact contact = contactGroup.getContacts().get(0);
                        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) contact, "item.contacts[0]");
                        appNotificationFilter.setSender(contact.getDisplayName());
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a3 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.C.a();
                        java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
                        sb2.append("getTypeAllowFromCall - item ");
                        sb2.append(i2);
                        sb2.append(" name = ");
                        com.fossil.wearables.fsl.contact.Contact contact2 = contactGroup.getContacts().get(0);
                        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) contact2, "item.contacts[0]");
                        sb2.append(contact2.getDisplayName());
                        local2.d(a3, sb2.toString());
                        list.add(appNotificationFilter);
                    }
                }
            } else {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.C.a(), "call no one");
            }
            if (notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.t.J() == 0) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.C.a(), "message everyone");
                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName3 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.MESSAGES;
                com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification5 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName3.getAppName(), aApplicationName3.getBundleCrc(), aApplicationName3.getGroupId(), aApplicationName3.getPackageName(), aApplicationName3.getIconResourceId(), aApplicationName3.getIconFwPath(), aApplicationName3.getNotificationType());
                list.add(new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification5));
            } else if (notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.t.J() == 1) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.C.a(), "message favorite");
                int size2 = notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.h.size();
                int i3 = 0;
                while (i3 < size2) {
                    com.fossil.wearables.fsl.contact.ContactGroup contactGroup2 = (com.fossil.wearables.fsl.contact.ContactGroup) notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1.this$0.this$0.h.get(i3);
                    com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName4 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.MESSAGES;
                    com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification6 = r9;
                    com.misfit.frameworks.buttonservice.model.notification.FNotification fNotification7 = new com.misfit.frameworks.buttonservice.model.notification.FNotification(aApplicationName4.getAppName(), aApplicationName4.getBundleCrc(), aApplicationName4.getGroupId(), aApplicationName4.getPackageName(), aApplicationName4.getIconResourceId(), aApplicationName4.getIconFwPath(), aApplicationName4.getNotificationType());
                    com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter appNotificationFilter2 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter(fNotification6);
                    java.util.List<com.fossil.wearables.fsl.contact.Contact> contacts2 = contactGroup2.getContacts();
                    com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) contacts2, "item.contacts");
                    if (!contacts2.isEmpty()) {
                        com.fossil.wearables.fsl.contact.Contact contact3 = contactGroup2.getContacts().get(0);
                        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) contact3, "item.contacts[0]");
                        appNotificationFilter2.setSender(contact3.getDisplayName());
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a4 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.C.a();
                        java.lang.StringBuilder sb3 = new java.lang.StringBuilder();
                        sb3.append("getTypeAllowFromMessage - item ");
                        sb3.append(i3);
                        sb3.append(" name = ");
                        com.fossil.wearables.fsl.contact.Contact contact4 = contactGroup2.getContacts().get(0);
                        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) contact4, "item.contacts[0]");
                        sb3.append(contact4.getDisplayName());
                        local3.d(a4, sb3.toString());
                        list.add(appNotificationFilter2);
                    }
                    i3++;
                    notificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1 = this;
                }
            } else {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.C.a(), "message no one");
            }
        } else if (cVar instanceof com.fossil.blesdk.obfuscated.px2.b) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.C.a(), "GetAllContactGroup onError");
        }
        return list;
    }
}
