package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.content.Context;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.x23;
import com.fossil.blesdk.obfuscated.y23;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Ref$BooleanRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeSettingsDefaultAddressPresenter extends x23 {
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public PlacesClient f;
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public String h;
    @DexIgnore
    public /* final */ String i; // = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_CommuteTimeDestinationSettings_Label__Home);
    @DexIgnore
    public /* final */ String j; // = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_CommuteTimeDestinationSettings_Label__Other);
    @DexIgnore
    public /* final */ ArrayList<String> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ y23 l;
    @DexIgnore
    public /* final */ en2 m;
    @DexIgnore
    public /* final */ UserRepository n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = CommuteTimeSettingsDefaultAddressPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "CommuteTimeSettingsDefau\u2026er::class.java.simpleName");
        o = simpleName;
    }
    */

    @DexIgnore
    public CommuteTimeSettingsDefaultAddressPresenter(y23 y23, en2 en2, UserRepository userRepository) {
        kd4.b(y23, "mView");
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(userRepository, "mUserRepository");
        this.l = y23;
        this.m = en2;
        this.n = userRepository;
    }

    @DexIgnore
    public final en2 h() {
        return this.m;
    }

    @DexIgnore
    public final UserRepository i() {
        return this.n;
    }

    @DexIgnore
    public final y23 j() {
        return this.l;
    }

    @DexIgnore
    public void k() {
        this.l.a(this);
    }

    @DexIgnore
    public void f() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new CommuteTimeSettingsDefaultAddressPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        this.f = null;
    }

    @DexIgnore
    public void a(String str, String str2) {
        kd4.b(str, "addressType");
        kd4.b(str2, "defaultPlace");
        this.g = str2;
        this.h = str;
    }

    @DexIgnore
    public void a(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = o;
        local.d(str2, "onUserExit with address " + str);
        Ref$BooleanRef ref$BooleanRef = new Ref$BooleanRef();
        ref$BooleanRef.element = false;
        Ref$BooleanRef ref$BooleanRef2 = new Ref$BooleanRef();
        ref$BooleanRef2.element = false;
        if (str == null || ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1(str, (yb4) null, this, str, ref$BooleanRef, ref$BooleanRef2), 3, (Object) null) == null) {
            this.l.B((String) null);
            qa4 qa4 = qa4.a;
        }
    }
}
