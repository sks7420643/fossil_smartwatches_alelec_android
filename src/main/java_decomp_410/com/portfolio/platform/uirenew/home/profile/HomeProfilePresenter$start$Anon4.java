package com.portfolio.platform.uirenew.home.profile;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dl4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeProfilePresenter$start$Anon4<T> implements cc<List<? extends Device>> {
    @DexIgnore
    public /* final */ /* synthetic */ HomeProfilePresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$Anon4$Anon1", f = "HomeProfilePresenter.kt", l = {381, 182, 185}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList $deviceWrappers;
        @DexIgnore
        public /* final */ /* synthetic */ List $devices;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public Object L$Anon2;
        @DexIgnore
        public Object L$Anon3;
        @DexIgnore
        public Object L$Anon4;
        @DexIgnore
        public Object L$Anon5;
        @DexIgnore
        public boolean Z$Anon0;
        @DexIgnore
        public boolean Z$Anon1;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter$start$Anon4 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HomeProfilePresenter$start$Anon4 homeProfilePresenter$start$Anon4, List list, ArrayList arrayList, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = homeProfilePresenter$start$Anon4;
            this.$devices = list;
            this.$deviceWrappers = arrayList;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$devices, this.$deviceWrappers, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v14, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v5, resolved type: com.fossil.blesdk.obfuscated.dl4} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00e2 A[Catch:{ all -> 0x006e }] */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x0135 A[Catch:{ all -> 0x006e }] */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x014b A[Catch:{ all -> 0x006e }] */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x014e A[Catch:{ all -> 0x006e }] */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x015e A[Catch:{ all -> 0x006e }] */
        /* JADX WARNING: Removed duplicated region for block: B:76:0x0264 A[Catch:{ all -> 0x006e }] */
        /* JADX WARNING: Removed duplicated region for block: B:80:0x0291 A[Catch:{ all -> 0x006e }] */
        /* JADX WARNING: Removed duplicated region for block: B:83:0x02fe A[Catch:{ all -> 0x006e }] */
        public final Object invokeSuspend(Object obj) {
            dl4 dl4;
            Iterator it;
            Device device;
            zg4 zg4;
            Object obj2;
            String str;
            Anon1 anon1;
            int i;
            Object obj3;
            Iterator it2;
            zg4 zg42;
            Object obj4;
            Object obj5;
            dl4 dl42;
            Object a = cc4.a();
            int i2 = this.label;
            if (i2 == 0) {
                na4.a(obj);
                zg4 = this.p$;
                dl42 = this.this$Anon0.a.o;
                this.L$Anon0 = zg4;
                this.L$Anon1 = dl42;
                this.label = 1;
                if (dl42.a((Object) null, this) == a) {
                    return a;
                }
            } else if (i2 == 1) {
                dl42 = (dl4) this.L$Anon1;
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else if (i2 == 2) {
                Device device2 = (Device) this.L$Anon4;
                Iterator it3 = (Iterator) this.L$Anon3;
                String str2 = (String) this.L$Anon2;
                dl4 = this.L$Anon1;
                zg4 zg43 = (zg4) this.L$Anon0;
                na4.a(obj);
                obj4 = obj;
                it = it3;
                zg42 = zg43;
                obj5 = a;
                device = device2;
                str = str2;
                anon1 = this;
                String str3 = (String) obj4;
                ug4 a2 = anon1.this$Anon0.a.b();
                HomeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1 homeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1 = new HomeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1(str, (yb4) null);
                anon1.L$Anon0 = zg42;
                anon1.L$Anon1 = dl4;
                anon1.L$Anon2 = str;
                anon1.L$Anon3 = it;
                anon1.L$Anon4 = device;
                anon1.L$Anon5 = str3;
                anon1.Z$Anon0 = false;
                anon1.Z$Anon1 = false;
                anon1.label = 3;
                obj2 = yf4.a(a2, homeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1, anon1);
                if (obj2 != obj5) {
                }
                return obj5;
            } else if (i2 == 3) {
                boolean z = this.Z$Anon1;
                boolean z2 = this.Z$Anon0;
                String str4 = (String) this.L$Anon5;
                device = (Device) this.L$Anon4;
                it = (Iterator) this.L$Anon3;
                String str5 = (String) this.L$Anon2;
                dl4 = (dl4) this.L$Anon1;
                zg4 zg44 = (zg4) this.L$Anon0;
                try {
                    na4.a(obj);
                    obj2 = obj;
                    boolean z3 = z;
                    boolean z4 = z2;
                    String str6 = str4;
                    str = str5;
                    zg4 zg45 = zg44;
                    Object obj6 = a;
                    anon1 = this;
                    boolean booleanValue = ((Boolean) obj2).booleanValue();
                    if (device.getBatteryLevel() <= 100) {
                        i = 100;
                    } else {
                        i = device.getBatteryLevel();
                    }
                    if (!kd4.a((Object) device.getDeviceId(), (Object) str)) {
                        anon1.$deviceWrappers.add(new HomeProfilePresenter.b(device.getDeviceId(), z4, str6, i, z3, booleanValue));
                    } else {
                        anon1.$deviceWrappers.add(0, new HomeProfilePresenter.b(device.getDeviceId(), PortfolioApp.W.c().g(device.getDeviceId()) == 2, str6, i, true, booleanValue));
                        if (!kd4.a((Object) "release", (Object) "debug")) {
                            if (!kd4.a((Object) "release", (Object) "staging")) {
                                if (!FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId())) {
                                    if (device.getBatteryLevel() >= 10 || device.getBatteryLevel() <= 0) {
                                        anon1.this$Anon0.a.l().a(false, false);
                                    } else {
                                        anon1.this$Anon0.a.l().a(true, false);
                                    }
                                } else if (device.getBatteryLevel() >= 25 || device.getBatteryLevel() <= 0) {
                                    anon1.this$Anon0.a.l().a(false, true);
                                } else {
                                    anon1.this$Anon0.a.l().a(true, true);
                                }
                            }
                        }
                        if (!FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId())) {
                            if (device.getBatteryLevel() >= PortfolioApp.W.c().u().w() || device.getBatteryLevel() <= 0) {
                                anon1.this$Anon0.a.l().a(false, false);
                            } else {
                                anon1.this$Anon0.a.l().a(true, false);
                            }
                        } else if (device.getBatteryLevel() >= PortfolioApp.W.c().u().w() || device.getBatteryLevel() <= 0) {
                            anon1.this$Anon0.a.l().a(false, true);
                        } else {
                            anon1.this$Anon0.a.l().a(true, true);
                        }
                    }
                    obj3 = obj6;
                    it2 = it;
                    if (it2.hasNext()) {
                        Device device3 = (Device) it2.next();
                        ug4 b = anon1.this$Anon0.a.c();
                        HomeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 homeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1 = new HomeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(device3, (yb4) null, anon1);
                        anon1.L$Anon0 = zg4;
                        anon1.L$Anon1 = dl4;
                        anon1.L$Anon2 = str;
                        anon1.L$Anon3 = it2;
                        anon1.L$Anon4 = device3;
                        anon1.label = 2;
                        obj4 = yf4.a(b, homeProfilePresenter$start$Anon4$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1, anon1);
                        if (obj4 == obj3) {
                            return obj3;
                        }
                        zg42 = zg4;
                        obj5 = obj3;
                        device = device3;
                        it = it2;
                        String str32 = (String) obj4;
                        ug4 a22 = anon1.this$Anon0.a.b();
                        HomeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1 homeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon12 = new HomeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon1(str, (yb4) null);
                        anon1.L$Anon0 = zg42;
                        anon1.L$Anon1 = dl4;
                        anon1.L$Anon2 = str;
                        anon1.L$Anon3 = it;
                        anon1.L$Anon4 = device;
                        anon1.L$Anon5 = str32;
                        anon1.Z$Anon0 = false;
                        anon1.Z$Anon1 = false;
                        anon1.label = 3;
                        obj2 = yf4.a(a22, homeProfilePresenter$start$Anon4$Anon1$Anon1$isLatestFw$Anon12, anon1);
                        if (obj2 != obj5) {
                            return obj5;
                        }
                        str6 = str32;
                        z4 = false;
                        z3 = false;
                        obj6 = obj5;
                        zg45 = zg42;
                        boolean booleanValue2 = ((Boolean) obj2).booleanValue();
                        if (device.getBatteryLevel() <= 100) {
                        }
                        if (!kd4.a((Object) device.getDeviceId(), (Object) str)) {
                        }
                        obj3 = obj6;
                        it2 = it;
                        if (it2.hasNext()) {
                        }
                        return obj5;
                        return obj3;
                    }
                    if (!kd4.a((Object) anon1.$deviceWrappers, (Object) anon1.this$Anon0.a.j())) {
                        anon1.this$Anon0.a.j().clear();
                        anon1.this$Anon0.a.j().addAll(anon1.$deviceWrappers);
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a3 = HomeProfilePresenter.C.a();
                    local.d(a3, "update new device list " + anon1.this$Anon0.a.j());
                    anon1.this$Anon0.a.l().b(anon1.this$Anon0.a.j());
                    if (System.currentTimeMillis() - PortfolioApp.W.c().u().g(str) < 60000) {
                        anon1.this$Anon0.a.m();
                    }
                    FLogger.INSTANCE.getLocal().d(HomeProfilePresenter.C.a(), "updatedDevices done");
                    qa4 qa4 = qa4.a;
                    dl4.a((Object) null);
                    return qa4.a;
                } catch (Throwable th) {
                    dl4.a((Object) null);
                    throw th;
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            dl4 = dl42;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a4 = HomeProfilePresenter.C.a();
            local2.d(a4, "updatedDevices " + this.$devices + " currentDevices " + this.this$Anon0.a.g);
            str = PortfolioApp.W.c().e();
            it2 = this.$devices.iterator();
            obj3 = a;
            anon1 = this;
            if (it2.hasNext()) {
            }
            if (!kd4.a((Object) anon1.$deviceWrappers, (Object) anon1.this$Anon0.a.j())) {
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String a32 = HomeProfilePresenter.C.a();
            local3.d(a32, "update new device list " + anon1.this$Anon0.a.j());
            anon1.this$Anon0.a.l().b(anon1.this$Anon0.a.j());
            if (System.currentTimeMillis() - PortfolioApp.W.c().u().g(str) < 60000) {
            }
            FLogger.INSTANCE.getLocal().d(HomeProfilePresenter.C.a(), "updatedDevices done");
            qa4 qa42 = qa4.a;
            dl4.a((Object) null);
            return qa4.a;
        }
    }

    @DexIgnore
    public HomeProfilePresenter$start$Anon4(HomeProfilePresenter homeProfilePresenter) {
        this.a = homeProfilePresenter;
    }

    @DexIgnore
    public final void a(List<Device> list) {
        ArrayList arrayList = new ArrayList();
        this.a.g.clear();
        ArrayList e = this.a.g;
        if (list != null) {
            e.addAll(list);
            fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, list, arrayList, (yb4) null), 3, (Object) null);
            return;
        }
        kd4.a();
        throw null;
    }
}
