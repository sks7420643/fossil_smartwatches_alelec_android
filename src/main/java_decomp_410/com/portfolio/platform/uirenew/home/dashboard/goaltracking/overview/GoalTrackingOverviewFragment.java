package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.cu3;
import com.fossil.blesdk.obfuscated.f9;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.gb3;
import com.fossil.blesdk.obfuscated.kb3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.qa;
import com.fossil.blesdk.obfuscated.qb3;
import com.fossil.blesdk.obfuscated.tr3;
import com.fossil.blesdk.obfuscated.vb3;
import com.fossil.blesdk.obfuscated.xe;
import com.fossil.blesdk.obfuscated.yb2;
import com.fossil.blesdk.obfuscated.zr2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingOverviewFragment extends zr2 {
    @DexIgnore
    public tr3<yb2> j;
    @DexIgnore
    public GoalTrackingOverviewDayPresenter k;
    @DexIgnore
    public GoalTrackingOverviewWeekPresenter l;
    @DexIgnore
    public GoalTrackingOverviewMonthPresenter m;
    @DexIgnore
    public gb3 n;
    @DexIgnore
    public vb3 o;
    @DexIgnore
    public qb3 p;
    @DexIgnore
    public int q; // = 7;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewFragment e;

        @DexIgnore
        public b(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            this.e = goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.e;
            tr3 a = goalTrackingOverviewFragment.j;
            goalTrackingOverviewFragment.a(7, a != null ? (yb2) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewFragment e;

        @DexIgnore
        public c(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            this.e = goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.e;
            tr3 a = goalTrackingOverviewFragment.j;
            goalTrackingOverviewFragment.a(4, a != null ? (yb2) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewFragment e;

        @DexIgnore
        public d(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            this.e = goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.e;
            tr3 a = goalTrackingOverviewFragment.j;
            goalTrackingOverviewFragment.a(2, a != null ? (yb2) a.a() : null);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "GoalTrackingOverviewFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewFragment", "onCreateView");
        yb2 yb2 = (yb2) qa.a(layoutInflater, R.layout.fragment_goal_tracking_overview, viewGroup, false, O0());
        f9.c((View) yb2.t, false);
        if (bundle != null) {
            this.q = bundle.getInt("CURRENT_TAB", 7);
        }
        kd4.a((Object) yb2, "binding");
        a(yb2);
        this.j = new tr3<>(this, yb2);
        tr3<yb2> tr3 = this.j;
        if (tr3 != null) {
            yb2 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        kd4.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.q);
    }

    @DexIgnore
    public final void a(yb2 yb2) {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewFragment", "initUI");
        this.n = (gb3) getChildFragmentManager().a("GoalTrackingOverviewDayFragment");
        this.o = (vb3) getChildFragmentManager().a("GoalTrackingOverviewWeekFragment");
        this.p = (qb3) getChildFragmentManager().a("GoalTrackingOverviewMonthFragment");
        if (this.n == null) {
            this.n = new gb3();
        }
        if (this.o == null) {
            this.o = new vb3();
        }
        if (this.p == null) {
            this.p = new qb3();
        }
        ArrayList arrayList = new ArrayList();
        gb3 gb3 = this.n;
        if (gb3 != null) {
            arrayList.add(gb3);
            vb3 vb3 = this.o;
            if (vb3 != null) {
                arrayList.add(vb3);
                qb3 qb3 = this.p;
                if (qb3 != null) {
                    arrayList.add(qb3);
                    RecyclerView recyclerView = yb2.t;
                    kd4.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new cu3(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new GoalTrackingOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new xe().a(recyclerView);
                    a(this.q, yb2);
                    l42 g = PortfolioApp.W.c().g();
                    gb3 gb32 = this.n;
                    if (gb32 != null) {
                        vb3 vb32 = this.o;
                        if (vb32 != null) {
                            qb3 qb32 = this.p;
                            if (qb32 != null) {
                                g.a(new kb3(gb32, vb32, qb32)).a(this);
                                yb2.s.setOnClickListener(new b(this));
                                yb2.q.setOnClickListener(new c(this));
                                yb2.r.setOnClickListener(new d(this));
                                return;
                            }
                            kd4.a();
                            throw null;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i, yb2 yb2) {
        if (yb2 != null) {
            FlexibleTextView flexibleTextView = yb2.s;
            kd4.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = yb2.q;
            kd4.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = yb2.r;
            kd4.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = yb2.s;
            kd4.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = yb2.q;
            kd4.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = yb2.r;
            kd4.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i == 2) {
                FlexibleTextView flexibleTextView7 = yb2.r;
                kd4.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = yb2.r;
                kd4.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = yb2.q;
                kd4.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                tr3<yb2> tr3 = this.j;
                if (tr3 != null) {
                    yb2 a2 = tr3.a();
                    if (a2 != null) {
                        RecyclerView recyclerView = a2.t;
                        if (recyclerView != null) {
                            recyclerView.i(2);
                        }
                    }
                }
            } else if (i == 4) {
                FlexibleTextView flexibleTextView10 = yb2.q;
                kd4.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = yb2.q;
                kd4.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = yb2.q;
                kd4.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                tr3<yb2> tr32 = this.j;
                if (tr32 != null) {
                    yb2 a3 = tr32.a();
                    if (a3 != null) {
                        RecyclerView recyclerView2 = a3.t;
                        if (recyclerView2 != null) {
                            recyclerView2.i(1);
                        }
                    }
                }
            } else if (i != 7) {
                FlexibleTextView flexibleTextView13 = yb2.s;
                kd4.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = yb2.s;
                kd4.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = yb2.q;
                kd4.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                tr3<yb2> tr33 = this.j;
                if (tr33 != null) {
                    yb2 a4 = tr33.a();
                    if (a4 != null) {
                        RecyclerView recyclerView3 = a4.t;
                        if (recyclerView3 != null) {
                            recyclerView3.i(0);
                        }
                    }
                }
            } else {
                FlexibleTextView flexibleTextView16 = yb2.s;
                kd4.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = yb2.s;
                kd4.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = yb2.q;
                kd4.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                tr3<yb2> tr34 = this.j;
                if (tr34 != null) {
                    yb2 a5 = tr34.a();
                    if (a5 != null) {
                        RecyclerView recyclerView4 = a5.t;
                        if (recyclerView4 != null) {
                            recyclerView4.i(0);
                        }
                    }
                }
            }
        }
    }
}
