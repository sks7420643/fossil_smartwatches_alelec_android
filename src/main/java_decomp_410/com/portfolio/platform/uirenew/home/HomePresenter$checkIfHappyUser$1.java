package com.portfolio.platform.uirenew.home;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1", mo27670f = "HomePresenter.kt", mo27671l = {315}, mo27672m = "invokeSuspend")
public final class HomePresenter$checkIfHappyUser$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $hasServerSettings;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22293p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.HomePresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1$1", mo27670f = "HomePresenter.kt", mo27671l = {335, 336}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1$1 */
    public static final class C62931 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public long J$0;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public java.lang.Object L$1;
        @DexIgnore
        public java.lang.Object L$2;
        @DexIgnore
        public java.lang.Object L$3;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22294p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1$1$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1$1$1", mo27670f = "HomePresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1$1$1 */
        public static final class C62941 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f22295p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1.C62931 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C62941(com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1.C62931 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1.C62931.C62941 r0 = new com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1.C62931.C62941(this.this$0, yb4);
                r0.f22295p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1.C62931.C62941) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                com.fossil.blesdk.obfuscated.cc4.m20546a();
                if (this.label == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    this.this$0.this$0.this$0.f22274l.mo30759M();
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C62931(com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1 homePresenter$checkIfHappyUser$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = homePresenter$checkIfHappyUser$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1.C62931 r0 = new com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1.C62931(this.this$0, yb4);
            r0.f22294p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1.C62931) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00dd  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x00f3  */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x0166 A[RETURN] */
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            long j;
            com.fossil.blesdk.obfuscated.zg4 zg4;
            com.portfolio.platform.data.model.ServerSetting serverSetting;
            com.portfolio.platform.data.model.ServerSetting serverSetting2;
            com.portfolio.platform.data.model.ServerSetting serverSetting3;
            int i;
            int i2;
            java.lang.String value;
            java.lang.String value2;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i3 = this.label;
            if (i3 == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                zg4 = this.f22294p$;
                com.portfolio.platform.data.model.ServerSetting generateSetting = this.this$0.this$0.f22283u.generateSetting("crash-threshold", java.lang.String.valueOf(3));
                com.portfolio.platform.data.model.ServerSetting generateSetting2 = this.this$0.this$0.f22283u.generateSetting("successful-sync-threshold", java.lang.String.valueOf(4));
                serverSetting3 = this.this$0.this$0.f22283u.generateSetting("post-sync-threshold", java.lang.String.valueOf(5));
                com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1 homePresenter$checkIfHappyUser$1 = this.this$0;
                if (homePresenter$checkIfHappyUser$1.$hasServerSettings) {
                    com.portfolio.platform.data.model.ServerSetting serverSettingByKey = homePresenter$checkIfHappyUser$1.this$0.f22283u.getServerSettingByKey("crash-threshold");
                    if (serverSettingByKey != null) {
                        generateSetting = serverSettingByKey;
                    }
                    com.portfolio.platform.data.model.ServerSetting serverSettingByKey2 = this.this$0.this$0.f22283u.getServerSettingByKey("successful-sync-threshold");
                    if (serverSettingByKey2 != null) {
                        generateSetting2 = serverSettingByKey2;
                    }
                    com.portfolio.platform.data.model.ServerSetting serverSettingByKey3 = this.this$0.this$0.f22283u.getServerSettingByKey("post-sync-threshold");
                    if (serverSettingByKey3 == null) {
                        serverSettingByKey3 = serverSetting3;
                    }
                    serverSetting = generateSetting;
                    serverSetting2 = generateSetting2;
                    serverSetting3 = serverSettingByKey3;
                } else {
                    serverSetting = generateSetting;
                    serverSetting2 = generateSetting2;
                }
                java.lang.String value3 = serverSetting.getValue();
                if (value3 != null) {
                    java.lang.Integer a2 = com.fossil.blesdk.obfuscated.dc4.m20843a(java.lang.Integer.parseInt(value3));
                    if (a2 != null) {
                        i2 = a2.intValue();
                        value = serverSetting2.getValue();
                        if (value != null) {
                            java.lang.Integer a3 = com.fossil.blesdk.obfuscated.dc4.m20843a(java.lang.Integer.parseInt(value));
                            if (a3 != null) {
                                i = a3.intValue();
                                value2 = serverSetting3.getValue();
                                if (value2 != null) {
                                    java.lang.Long a4 = com.fossil.blesdk.obfuscated.dc4.m20844a(java.lang.Long.parseLong(value2));
                                    if (a4 != null) {
                                        j = a4.longValue();
                                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                        java.lang.String a5 = com.portfolio.platform.uirenew.home.HomePresenter.f22267y.mo40748a();
                                        local.mo33255d(a5, "Crash threshold: " + serverSetting + ", sync success: " + serverSetting2 + ", post sync: " + serverSetting3);
                                        if (this.this$0.this$0.f22282t.mo41930a() && this.this$0.this$0.f22282t.mo41931a(i2, i)) {
                                            this.L$0 = zg4;
                                            this.L$1 = serverSetting;
                                            this.L$2 = serverSetting2;
                                            this.L$3 = serverSetting3;
                                            this.I$0 = i2;
                                            this.I$1 = i;
                                            this.J$0 = j;
                                            this.label = 1;
                                            if (com.fossil.blesdk.obfuscated.ih4.m23524a(j, this) == a) {
                                                return a;
                                            }
                                        }
                                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                                    }
                                }
                                j = 0;
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                java.lang.String a52 = com.portfolio.platform.uirenew.home.HomePresenter.f22267y.mo40748a();
                                local2.mo33255d(a52, "Crash threshold: " + serverSetting + ", sync success: " + serverSetting2 + ", post sync: " + serverSetting3);
                                this.L$0 = zg4;
                                this.L$1 = serverSetting;
                                this.L$2 = serverSetting2;
                                this.L$3 = serverSetting3;
                                this.I$0 = i2;
                                this.I$1 = i;
                                this.J$0 = j;
                                this.label = 1;
                                if (com.fossil.blesdk.obfuscated.ih4.m23524a(j, this) == a) {
                                }
                            }
                        }
                        i = 0;
                        value2 = serverSetting3.getValue();
                        if (value2 != null) {
                        }
                        j = 0;
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local22 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a522 = com.portfolio.platform.uirenew.home.HomePresenter.f22267y.mo40748a();
                        local22.mo33255d(a522, "Crash threshold: " + serverSetting + ", sync success: " + serverSetting2 + ", post sync: " + serverSetting3);
                        this.L$0 = zg4;
                        this.L$1 = serverSetting;
                        this.L$2 = serverSetting2;
                        this.L$3 = serverSetting3;
                        this.I$0 = i2;
                        this.I$1 = i;
                        this.J$0 = j;
                        this.label = 1;
                        if (com.fossil.blesdk.obfuscated.ih4.m23524a(j, this) == a) {
                        }
                    }
                }
                i2 = 0;
                value = serverSetting2.getValue();
                if (value != null) {
                }
                i = 0;
                value2 = serverSetting3.getValue();
                if (value2 != null) {
                }
                j = 0;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a5222 = com.portfolio.platform.uirenew.home.HomePresenter.f22267y.mo40748a();
                local222.mo33255d(a5222, "Crash threshold: " + serverSetting + ", sync success: " + serverSetting2 + ", post sync: " + serverSetting3);
                this.L$0 = zg4;
                this.L$1 = serverSetting;
                this.L$2 = serverSetting2;
                this.L$3 = serverSetting3;
                this.I$0 = i2;
                this.I$1 = i;
                this.J$0 = j;
                this.label = 1;
                if (com.fossil.blesdk.obfuscated.ih4.m23524a(j, this) == a) {
                }
            } else if (i3 == 1) {
                long j2 = this.J$0;
                int i4 = this.I$1;
                int i5 = this.I$0;
                serverSetting3 = (com.portfolio.platform.data.model.ServerSetting) this.L$3;
                serverSetting2 = (com.portfolio.platform.data.model.ServerSetting) this.L$2;
                serverSetting = (com.portfolio.platform.data.model.ServerSetting) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                j = j2;
                i = i4;
                i2 = i5;
            } else if (i3 == 2) {
                com.portfolio.platform.data.model.ServerSetting serverSetting4 = (com.portfolio.platform.data.model.ServerSetting) this.L$3;
                com.portfolio.platform.data.model.ServerSetting serverSetting5 = (com.portfolio.platform.data.model.ServerSetting) this.L$2;
                com.portfolio.platform.data.model.ServerSetting serverSetting6 = (com.portfolio.platform.data.model.ServerSetting) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.f22275m.mo27073r(false);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            com.fossil.blesdk.obfuscated.pi4 j3 = this.this$0.this$0.mo31442d();
            com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1.C62931.C62941 r5 = new com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1.C62931.C62941(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = serverSetting;
            this.L$2 = serverSetting2;
            this.L$3 = serverSetting3;
            this.I$0 = i2;
            this.I$1 = i;
            this.J$0 = j;
            this.label = 2;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(j3, r5, this) == a) {
                return a;
            }
            this.this$0.this$0.f22275m.mo27073r(false);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomePresenter$checkIfHappyUser$1(com.portfolio.platform.uirenew.home.HomePresenter homePresenter, boolean z, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homePresenter;
        this.$hasServerSettings = z;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1 homePresenter$checkIfHappyUser$1 = new com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1(this.this$0, this.$hasServerSettings, yb4);
        homePresenter$checkIfHappyUser$1.f22293p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homePresenter$checkIfHappyUser$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22293p$;
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1.C62931 r3 = new com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1.C62931(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
