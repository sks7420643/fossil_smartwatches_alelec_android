package com.portfolio.platform.uirenew.home.details.goaltracking;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.enums.Status;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1<T> implements cc<os3<? extends List<GoalTrackingSummary>>> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDetailPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1$Anon1", f = "GoalTrackingDetailPresenter.kt", l = {120}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1 goalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = goalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                ug4 a2 = this.this$Anon0.a.b();
                GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1$Anon1$summary$Anon1 goalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1$Anon1$summary$Anon1 = new GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1$Anon1$summary$Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = yf4.a(a2, goalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1$Anon1$summary$Anon1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) obj;
            if (this.this$Anon0.a.m == null || (!kd4.a((Object) this.this$Anon0.a.m, (Object) goalTrackingSummary))) {
                this.this$Anon0.a.m = goalTrackingSummary;
                this.this$Anon0.a.q.a(this.this$Anon0.a.m);
                if (this.this$Anon0.a.i && this.this$Anon0.a.j) {
                    fi4 unused = this.this$Anon0.a.m();
                }
            }
            return qa4.a;
        }
    }

    @DexIgnore
    public GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$Anon1(GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
        this.a = goalTrackingDetailPresenter;
    }

    @DexIgnore
    public final void a(os3<? extends List<GoalTrackingSummary>> os3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailPresenter", "start - mGoalTrackingSummary -- goalTrackingSummary=" + os3);
        if ((os3 != null ? os3.f() : null) != Status.DATABASE_LOADING) {
            this.a.l = os3 != null ? (List) os3.d() : null;
            this.a.i = true;
            fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (yb4) null), 3, (Object) null);
        }
    }
}
