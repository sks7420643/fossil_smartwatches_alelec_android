package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$Anon2", f = "InactivityNudgeTimePresenter.kt", l = {115}, m = "invokeSuspend")
public final class InactivityNudgeTimePresenter$save$Anon2 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ InactivityNudgeTimePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$Anon2$Anon1", f = "InactivityNudgeTimePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ InactivityNudgeTimePresenter$save$Anon2 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 e;

            @DexIgnore
            public a(Anon1 anon1) {
                this.e = anon1;
            }

            @DexIgnore
            public final void run() {
                InactivityNudgeTimeDao inactivityNudgeTimeDao = this.e.this$Anon0.this$Anon0.k.getInactivityNudgeTimeDao();
                InactivityNudgeTimeModel b = this.e.this$Anon0.this$Anon0.i;
                if (b != null) {
                    inactivityNudgeTimeDao.upsertInactivityNudgeTime(b);
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(InactivityNudgeTimePresenter$save$Anon2 inactivityNudgeTimePresenter$save$Anon2, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = inactivityNudgeTimePresenter$save$Anon2;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.this$Anon0.k.runInTransaction((Runnable) new a(this));
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public InactivityNudgeTimePresenter$save$Anon2(InactivityNudgeTimePresenter inactivityNudgeTimePresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = inactivityNudgeTimePresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        InactivityNudgeTimePresenter$save$Anon2 inactivityNudgeTimePresenter$save$Anon2 = new InactivityNudgeTimePresenter$save$Anon2(this.this$Anon0, yb4);
        inactivityNudgeTimePresenter$save$Anon2.p$ = (zg4) obj;
        return inactivityNudgeTimePresenter$save$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((InactivityNudgeTimePresenter$save$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 a2 = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            if (yf4.a(a2, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.j.close();
        return qa4.a;
    }
}
