package com.portfolio.platform.uirenew.home.customize.hybrid.edit;

import android.content.Context;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gl2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l63;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.oj2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ HybridPreset $currentPreset$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ HybridPreset $it;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HybridCustomizeEditPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Parcelable>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridPresetAppSetting $buttonMapping;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1, HybridPresetAppSetting hybridPresetAppSetting, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1;
            this.$buttonMapping = hybridPresetAppSetting;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$buttonMapping, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return HybridCustomizeEditPresenter.e(this.this$Anon0.this$Anon0).d(this.$buttonMapping.getAppId());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.e<SetHybridPresetToWatchUseCase.d, SetHybridPresetToWatchUseCase.b> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 a;

        @DexIgnore
        public a(HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1) {
            this.a = hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetHybridPresetToWatchUseCase.d dVar) {
            kd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setToWatch success");
            this.a.this$Anon0.k.m();
            this.a.this$Anon0.k.g(true);
        }

        @DexIgnore
        public void a(SetHybridPresetToWatchUseCase.b bVar) {
            kd4.b(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setToWatch onError");
            this.a.this$Anon0.k.m();
            int b = bVar.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "setPresetToWatch() - mSetHybridPresetToWatchUseCase - onError - lastErrorCode = " + b);
            if (b != 1101) {
                if (b == 8888) {
                    this.a.this$Anon0.k.c();
                    return;
                } else if (!(b == 1112 || b == 1113)) {
                    this.a.this$Anon0.k.q();
                    return;
                }
            }
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(bVar.a());
            kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            l63 h = this.a.this$Anon0.k;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                h.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1(HybridPreset hybridPreset, yb4 yb4, HybridCustomizeEditPresenter hybridCustomizeEditPresenter, HybridPreset hybridPreset2) {
        super(2, yb4);
        this.$it = hybridPreset;
        this.this$Anon0 = hybridCustomizeEditPresenter;
        this.$currentPreset$inlined = hybridPreset2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 = new HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1(this.$it, yb4, this.this$Anon0, this.$currentPreset$inlined);
        hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v25, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v19, resolved type: java.util.Iterator} */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01f6, code lost:
        if (android.text.TextUtils.isEmpty(((com.portfolio.platform.data.model.setting.SecondTimezoneSetting) r2.this$Anon0.i.a(r0.getSettings(), com.portfolio.platform.data.model.setting.SecondTimezoneSetting.class)).getTimeZoneId()) != false) goto L_0x021a;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0129  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0193 A[Catch:{ Exception -> 0x01a1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0216  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0236  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x02ad  */
    public final Object invokeSuspend(Object obj) {
        HybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1;
        HybridPresetAppSetting hybridPresetAppSetting;
        String str;
        Object obj2;
        zg4 zg4;
        List list;
        HybridPresetAppSetting hybridPresetAppSetting2;
        List list2;
        HybridPresetAppSetting hybridPresetAppSetting3;
        Iterator it;
        Object obj3;
        Parcelable parcelable;
        HybridPresetAppSetting hybridPresetAppSetting4;
        Object a2 = cc4.a();
        int i = this.label;
        yb4 yb4 = null;
        if (i == 0) {
            na4.a(obj);
            zg4 zg42 = this.p$;
            ArrayList<HybridPresetAppSetting> buttons = this.$it.getButtons();
            List arrayList = new ArrayList();
            for (HybridPresetAppSetting next : buttons) {
                if (dc4.a(gl2.c.c(next.getAppId())).booleanValue()) {
                    arrayList.add(next);
                }
            }
            if (!arrayList.isEmpty()) {
                Iterator it2 = arrayList.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    hybridPresetAppSetting4 = (HybridPresetAppSetting) it2.next();
                    if (!gl2.c.e(hybridPresetAppSetting4.getAppId())) {
                        break;
                    }
                }
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setPresetToWatch missingPermissionComp " + hybridPresetAppSetting4);
                if (hybridPresetAppSetting4 == null) {
                    this.this$Anon0.m.q(true);
                    this.this$Anon0.k.b(hybridPresetAppSetting4.getAppId(), hybridPresetAppSetting4.getPosition());
                    return qa4.a;
                }
                ArrayList<HybridPresetAppSetting> buttons2 = this.$it.getButtons();
                List arrayList2 = new ArrayList();
                for (HybridPresetAppSetting next2 : buttons2) {
                    if (dc4.a(gl2.c.d(next2.getAppId())).booleanValue()) {
                        arrayList2.add(next2);
                    }
                }
                if (!arrayList2.isEmpty()) {
                    it = arrayList2.iterator();
                    zg4 = zg42;
                    obj2 = a2;
                    list = arrayList;
                    hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 = this;
                    hybridPresetAppSetting3 = null;
                    List list3 = arrayList2;
                    hybridPresetAppSetting2 = hybridPresetAppSetting4;
                    list2 = list3;
                } else {
                    hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 = this;
                    hybridPresetAppSetting = null;
                    FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
                    if (hybridPresetAppSetting != null) {
                    }
                }
            }
            hybridPresetAppSetting4 = null;
            FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setPresetToWatch missingPermissionComp " + hybridPresetAppSetting4);
            if (hybridPresetAppSetting4 == null) {
            }
        } else if (i == 1) {
            it = this.L$Anon6;
            hybridPresetAppSetting = (HybridPresetAppSetting) this.L$Anon5;
            hybridPresetAppSetting3 = (HybridPresetAppSetting) this.L$Anon4;
            list2 = (List) this.L$Anon3;
            hybridPresetAppSetting2 = (HybridPresetAppSetting) this.L$Anon2;
            list = (List) this.L$Anon1;
            zg4 = (zg4) this.L$Anon0;
            try {
                na4.a(obj);
                obj3 = obj;
                obj2 = a2;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 = this;
            } catch (Exception e) {
                e = e;
                obj2 = a2;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 = this;
                FLogger.INSTANCE.getLocal().e("HybridCustomizeEditPresenter", "exception when parse setting from json " + e);
                yb4 = null;
                if (it.hasNext()) {
                }
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
                if (hybridPresetAppSetting != null) {
                }
            }
            parcelable = (Parcelable) obj3;
            FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "last setting " + parcelable);
            if (parcelable != null) {
                hybridPresetAppSetting.setSettings(hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.this$Anon0.i.a((Object) parcelable));
                yb4 = null;
            }
            FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
            if (hybridPresetAppSetting != null) {
                String appId = hybridPresetAppSetting.getAppId();
                if (kd4.a((Object) appId, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                    str = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_CommuteTimeError___ToDisplayCommuteTimePleaseEnter);
                    kd4.a((Object) str, "LanguageHelper.getString\u2026ayCommuteTimePleaseEnter)");
                } else if (kd4.a((Object) appId, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                    str = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_SecondTimezoneError_Text__ToDisplayTimezoneOnYourWatch);
                    kd4.a((Object) str, "LanguageHelper.getString\u2026splayTimezoneOnYourWatch)");
                } else if (kd4.a((Object) appId, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                    str = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.set_to_watch_fail_ring_phone_not_configured);
                    kd4.a((Object) str, "LanguageHelper.getString\u2026ing_phone_not_configured)");
                } else {
                    str = "";
                }
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.this$Anon0.k.b(str, hybridPresetAppSetting.getAppId(), hybridPresetAppSetting.getPosition());
                return qa4.a;
            }
            hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.this$Anon0.k.l();
            hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.this$Anon0.l.a(new SetHybridPresetToWatchUseCase.c(hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.$currentPreset$inlined), new a(hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1));
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (it.hasNext()) {
            hybridPresetAppSetting = (HybridPresetAppSetting) it.next();
            FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "check setting of " + hybridPresetAppSetting);
            try {
            } catch (Exception e2) {
                e = e2;
                FLogger.INSTANCE.getLocal().e("HybridCustomizeEditPresenter", "exception when parse setting from json " + e);
                yb4 = null;
                if (it.hasNext()) {
                }
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
                if (hybridPresetAppSetting != null) {
                }
            }
            if (oj2.a(hybridPresetAppSetting.getSettings())) {
                ug4 a3 = hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.this$Anon0.b();
                Anon1 anon1 = new Anon1(hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1, hybridPresetAppSetting, yb4);
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.L$Anon0 = zg4;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.L$Anon1 = list;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.L$Anon2 = hybridPresetAppSetting2;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.L$Anon3 = list2;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.L$Anon4 = hybridPresetAppSetting3;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.L$Anon5 = hybridPresetAppSetting;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.L$Anon6 = it;
                hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.label = 1;
                obj3 = yf4.a(a3, anon1, hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1);
                if (obj3 == obj2) {
                    return obj2;
                }
                parcelable = (Parcelable) obj3;
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "last setting " + parcelable);
                if (parcelable != null) {
                }
            }
            String appId2 = hybridPresetAppSetting.getAppId();
            if (!kd4.a((Object) appId2, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                if (kd4.a((Object) appId2, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                }
                yb4 = null;
                if (it.hasNext()) {
                    hybridPresetAppSetting = hybridPresetAppSetting3;
                }
            }
            if (TextUtils.isEmpty(((CommuteTimeSetting) hybridCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.this$Anon0.i.a(hybridPresetAppSetting.getSettings(), CommuteTimeSetting.class)).getAddress())) {
            }
            yb4 = null;
            if (it.hasNext()) {
            }
        }
        FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setPresetToWatch missingSettingMicroApp " + hybridPresetAppSetting);
        if (hybridPresetAppSetting != null) {
        }
    }
}
