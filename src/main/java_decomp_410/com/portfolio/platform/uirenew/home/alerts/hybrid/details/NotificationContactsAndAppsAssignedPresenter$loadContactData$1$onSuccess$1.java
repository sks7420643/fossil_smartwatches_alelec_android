package com.portfolio.platform.uirenew.home.alerts.hybrid.details;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1", f = "NotificationContactsAndAppsAssignedPresenter.kt", l = {129}, m = "invokeSuspend")
public final class NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.t03.d $responseValue;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1(com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1 notificationContactsAndAppsAssignedPresenter$loadContactData$1, com.fossil.blesdk.obfuscated.t03.d dVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = notificationContactsAndAppsAssignedPresenter$loadContactData$1;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1 notificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1 = new com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1(this.this$0, this.$responseValue, yb4);
        notificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.util.List list;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.p$;
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter.x.a(), "mGetAllHybridContactGroups onSuccess");
            java.util.ArrayList arrayList = new java.util.ArrayList();
            com.fossil.blesdk.obfuscated.gh4 a2 = com.fossil.blesdk.obfuscated.ag4.a(zg4, this.this$0.a.b(), (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1$populateContact$1(this, arrayList, (com.fossil.blesdk.obfuscated.yb4) null), 2, (java.lang.Object) null);
            this.L$0 = zg4;
            this.L$1 = arrayList;
            this.L$2 = a2;
            this.label = 1;
            if (a2.a(this) == a) {
                return a;
            }
            list = arrayList;
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.gh4 gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
            list = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.a.o().addAll(list);
        this.this$0.a.q().addAll(list);
        java.util.List<java.lang.Object> p = this.this$0.a.p();
        java.lang.Object[] array = list.toArray(new com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper[0]);
        if (array != null) {
            java.io.Serializable a3 = com.fossil.blesdk.obfuscated.fp4.a((java.io.Serializable) array);
            com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) a3, "SerializationUtils.clone\u2026apperList.toTypedArray())");
            com.fossil.blesdk.obfuscated.hb4.a(p, (T[]) (java.lang.Object[]) a3);
            this.this$0.a.t();
            return com.fossil.blesdk.obfuscated.qa4.a;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
