package com.portfolio.platform.uirenew.home.dashboard.sleep;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ad3;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zc3;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$Anon1", f = "DashboardSleepPresenter.kt", l = {65}, m = "invokeSuspend")
public final class DashboardSleepPresenter$initDataSource$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DashboardSleepPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements cc<qd<SleepSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ DashboardSleepPresenter$initDataSource$Anon1 a;

        @DexIgnore
        public a(DashboardSleepPresenter$initDataSource$Anon1 dashboardSleepPresenter$initDataSource$Anon1) {
            this.a = dashboardSleepPresenter$initDataSource$Anon1;
        }

        @DexIgnore
        public final void a(qd<SleepSummary> qdVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("getSummariesPaging observer size=");
            sb.append(qdVar != null ? Integer.valueOf(qdVar.size()) : null);
            local.d("DashboardSleepPresenter", sb.toString());
            if (qdVar != null) {
                this.a.this$Anon0.k().a(qdVar);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardSleepPresenter$initDataSource$Anon1(DashboardSleepPresenter dashboardSleepPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = dashboardSleepPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DashboardSleepPresenter$initDataSource$Anon1 dashboardSleepPresenter$initDataSource$Anon1 = new DashboardSleepPresenter$initDataSource$Anon1(this.this$Anon0, yb4);
        dashboardSleepPresenter$initDataSource$Anon1.p$ = (zg4) obj;
        return dashboardSleepPresenter$initDataSource$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DashboardSleepPresenter$initDataSource$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 a3 = this.this$Anon0.c();
            DashboardSleepPresenter$initDataSource$Anon1$user$Anon1 dashboardSleepPresenter$initDataSource$Anon1$user$Anon1 = new DashboardSleepPresenter$initDataSource$Anon1$user$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a3, dashboardSleepPresenter$initDataSource$Anon1$user$Anon1, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser = (MFUser) obj;
        if (mFUser != null) {
            Date d = rk2.d(mFUser.getCreatedAt());
            DashboardSleepPresenter dashboardSleepPresenter = this.this$Anon0;
            SleepSummariesRepository h = dashboardSleepPresenter.i;
            SleepSummariesRepository h2 = this.this$Anon0.i;
            SleepSessionsRepository f = this.this$Anon0.j;
            FitnessDataRepository c = this.this$Anon0.k;
            SleepDao d2 = this.this$Anon0.l;
            SleepDatabase e = this.this$Anon0.m;
            kd4.a((Object) d, "createdDate");
            dashboardSleepPresenter.g = h.getSummariesPaging(h2, f, c, d2, e, d, this.this$Anon0.o, this.this$Anon0);
            Listing g = this.this$Anon0.g;
            if (g != null) {
                LiveData pagedList = g.getPagedList();
                if (pagedList != null) {
                    zc3 k = this.this$Anon0.k();
                    if (k != null) {
                        pagedList.a((ad3) k, new a(this));
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment");
                    }
                }
            }
        }
        return qa4.a;
    }
}
