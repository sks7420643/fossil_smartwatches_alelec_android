package com.portfolio.platform.uirenew.home.customize.hybrid.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.cq2;
import com.fossil.blesdk.obfuscated.f8;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.j42;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.m63;
import com.fossil.blesdk.obfuscated.o63;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.v5;
import com.fossil.blesdk.obfuscated.x5;
import com.fossil.wearables.fossil.R;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.gson.HybridPresetDeserializer;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridCustomizeEditActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a E; // = new a((fd4) null);
    @DexIgnore
    public HybridCustomizeEditPresenter B;
    @DexIgnore
    public j42 C;
    @DexIgnore
    public HybridCustomizeViewModel D;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            kd4.b(context, "context");
            kd4.b(str, "presetId");
            kd4.b(str2, "microAppPos");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditActivity", "start - presetId=" + str + ", microAppPos=" + str2);
            Intent intent = new Intent(context, HybridCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str2);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final void a(FragmentActivity fragmentActivity, String str, ArrayList<f8<View, String>> arrayList, List<? extends f8<CustomizeWidget, String>> list, String str2) {
            kd4.b(fragmentActivity, "context");
            kd4.b(str, "presetId");
            kd4.b(arrayList, "views");
            kd4.b(list, "customizeWidgetViews");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditActivity", "startForResultAnimation() - presetId=" + str + ", microAppPos=" + str2);
            Intent intent = new Intent(fragmentActivity, HybridCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str2);
            for (f8 f8Var : list) {
                arrayList.add(new f8(f8Var.a, f8Var.b));
                Bundle bundle = new Bundle();
                cq2.a aVar = cq2.f;
                F f = f8Var.a;
                if (f != null) {
                    kd4.a((Object) f, "wcPair.first!!");
                    aVar.a((CustomizeWidget) f, bundle);
                    F f2 = f8Var.a;
                    if (f2 != null) {
                        kd4.a((Object) f2, "wcPair.first!!");
                        intent.putExtra(((CustomizeWidget) f2).getTransitionName(), bundle);
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
            Object[] array = arrayList.toArray(new f8[0]);
            if (array != null) {
                f8[] f8VarArr = (f8[]) array;
                x5 a = x5.a(fragmentActivity, (f8[]) Arrays.copyOf(f8VarArr, f8VarArr.length));
                kd4.a((Object) a, "ActivityOptionsCompat.ma\u2026context, *viewsTypeArray)");
                v5.a(fragmentActivity, intent, 100, a.a());
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0122  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x012e  */
    public void onCreate(Bundle bundle) {
        HybridPreset hybridPreset;
        HybridPreset hybridPreset2;
        HybridCustomizeViewModel hybridCustomizeViewModel;
        Class cls = HybridPreset.class;
        super.onCreate(bundle);
        setContentView(R.layout.activity_base);
        String stringExtra = getIntent().getStringExtra("KEY_PRESET_ID");
        String stringExtra2 = getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
        m63 m63 = (m63) getSupportFragmentManager().a((int) R.id.content);
        if (m63 == null) {
            m63 = new m63();
            a((Fragment) m63, "DianaCustomizeEditFragment", (int) R.id.content);
        }
        l42 g = PortfolioApp.W.c().g();
        kd4.a((Object) stringExtra, "presetId");
        kd4.a((Object) stringExtra2, "microAppPos");
        g.a(new o63(m63, stringExtra, stringExtra2)).a(this);
        j42 j42 = this.C;
        if (j42 != null) {
            ic a2 = lc.a((FragmentActivity) this, (kc.b) j42).a(HybridCustomizeViewModel.class);
            kd4.a((Object) a2, "ViewModelProviders.of(th\u2026izeViewModel::class.java)");
            this.D = (HybridCustomizeViewModel) a2;
            if (bundle == null) {
                FLogger.INSTANCE.getLocal().d(f(), "init from initialize state");
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.D;
                if (hybridCustomizeViewModel2 != null) {
                    hybridCustomizeViewModel2.a(stringExtra, stringExtra2);
                } else {
                    kd4.d("mHybridCustomizeViewModel");
                    throw null;
                }
            } else {
                FLogger.INSTANCE.getLocal().d(f(), "init from savedInstanceState");
                rz1 rz1 = new rz1();
                rz1.a(cls, new HybridPresetDeserializer());
                Gson a3 = rz1.a();
                try {
                    if (bundle.containsKey("KEY_CURRENT_PRESET")) {
                        FLogger.INSTANCE.getLocal().d(f(), "parse gson " + bundle.getString("KEY_CURRENT_PRESET"));
                        hybridPreset = (HybridPreset) a3.a(bundle.getString("KEY_CURRENT_PRESET"), cls);
                    } else {
                        hybridPreset = null;
                    }
                    try {
                        if (bundle.containsKey("KEY_ORIGINAL_PRESET")) {
                            hybridPreset2 = (HybridPreset) a3.a(bundle.getString("KEY_ORIGINAL_PRESET"), cls);
                            if (bundle.containsKey("KEY_PRESET_WATCH_APP_POS_SELECTED")) {
                                stringExtra2 = bundle.getString("KEY_PRESET_WATCH_APP_POS_SELECTED");
                            }
                            hybridCustomizeViewModel = this.D;
                            if (hybridCustomizeViewModel == null) {
                                hybridCustomizeViewModel.a(stringExtra, hybridPreset2, hybridPreset, stringExtra2);
                                return;
                            } else {
                                kd4.d("mHybridCustomizeViewModel");
                                throw null;
                            }
                        }
                    } catch (Exception e) {
                        e = e;
                        FLogger.INSTANCE.getLocal().d(f(), "exception when parse GSON when retrieve from saveInstanceState " + e);
                        hybridPreset2 = null;
                        if (bundle.containsKey("KEY_PRESET_WATCH_APP_POS_SELECTED")) {
                        }
                        hybridCustomizeViewModel = this.D;
                        if (hybridCustomizeViewModel == null) {
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    hybridPreset = null;
                    FLogger.INSTANCE.getLocal().d(f(), "exception when parse GSON when retrieve from saveInstanceState " + e);
                    hybridPreset2 = null;
                    if (bundle.containsKey("KEY_PRESET_WATCH_APP_POS_SELECTED")) {
                    }
                    hybridCustomizeViewModel = this.D;
                    if (hybridCustomizeViewModel == null) {
                    }
                }
                hybridPreset2 = null;
                if (bundle.containsKey("KEY_PRESET_WATCH_APP_POS_SELECTED")) {
                }
                hybridCustomizeViewModel = this.D;
                if (hybridCustomizeViewModel == null) {
                }
            }
        } else {
            kd4.d("viewModelFactory");
            throw null;
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        HybridCustomizeEditPresenter hybridCustomizeEditPresenter = this.B;
        if (hybridCustomizeEditPresenter != null) {
            hybridCustomizeEditPresenter.a(bundle);
            super.onSaveInstanceState(bundle);
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }
}
