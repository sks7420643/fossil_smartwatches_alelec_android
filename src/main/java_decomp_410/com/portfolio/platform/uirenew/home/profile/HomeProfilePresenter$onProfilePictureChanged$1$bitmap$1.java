package com.portfolio.platform.uirenew.home.profile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1$bitmap$1", mo27670f = "HomeProfilePresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class HomeProfilePresenter$onProfilePictureChanged$1$bitmap$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super android.graphics.Bitmap>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23812p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$onProfilePictureChanged$1$bitmap$1(com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1 homeProfilePresenter$onProfilePictureChanged$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homeProfilePresenter$onProfilePictureChanged$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1$bitmap$1 homeProfilePresenter$onProfilePictureChanged$1$bitmap$1 = new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1$bitmap$1(this.this$0, yb4);
        homeProfilePresenter$onProfilePictureChanged$1$bitmap$1.f23812p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeProfilePresenter$onProfilePictureChanged$1$bitmap$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1$bitmap$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.ck2.m20587a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c()).m22249e().m21590a(this.this$0.$imageUri).m21591a((com.fossil.blesdk.obfuscated.C2354lv<?>) ((com.fossil.blesdk.obfuscated.C2842rv) ((com.fossil.blesdk.obfuscated.C2842rv) new com.fossil.blesdk.obfuscated.C2842rv().mo13441a(com.fossil.blesdk.obfuscated.C2663pp.f8415a)).mo13444a(true)).mo13439a((com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap>) new com.fossil.blesdk.obfuscated.nk2())).mo17504c(200, 200).get();
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
