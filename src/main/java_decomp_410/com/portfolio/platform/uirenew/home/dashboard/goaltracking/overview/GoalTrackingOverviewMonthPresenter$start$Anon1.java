package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.pb3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.enums.Status;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingOverviewMonthPresenter$start$Anon1<T> implements cc<os3<? extends List<GoalTrackingSummary>>> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingOverviewMonthPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter$start$Anon1$Anon1", f = "GoalTrackingOverviewMonthPresenter.kt", l = {60}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $data;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewMonthPresenter$start$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter$start$Anon1$Anon1$Anon1")
        @gc4(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter$start$Anon1$Anon1$Anon1", f = "GoalTrackingOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter$start$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0152Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super TreeMap<Long, Float>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0152Anon1(Anon1 anon1, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0152Anon1 anon1 = new C0152Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0152Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                cc4.a();
                if (this.label == 0) {
                    na4.a(obj);
                    GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter = this.this$Anon0.this$Anon0.a;
                    Object a = goalTrackingOverviewMonthPresenter.f.a();
                    if (a != null) {
                        kd4.a(a, "mDate.value!!");
                        return goalTrackingOverviewMonthPresenter.a((Date) a, (List<GoalTrackingSummary>) this.this$Anon0.$data);
                    }
                    kd4.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingOverviewMonthPresenter$start$Anon1 goalTrackingOverviewMonthPresenter$start$Anon1, List list, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = goalTrackingOverviewMonthPresenter$start$Anon1;
            this.$data = list;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$data, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter;
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter2 = this.this$Anon0.a;
                ug4 a2 = goalTrackingOverviewMonthPresenter2.b();
                C0152Anon1 anon1 = new C0152Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = goalTrackingOverviewMonthPresenter2;
                this.label = 1;
                obj = yf4.a(a2, anon1, this);
                if (obj == a) {
                    return a;
                }
                goalTrackingOverviewMonthPresenter = goalTrackingOverviewMonthPresenter2;
            } else if (i == 1) {
                goalTrackingOverviewMonthPresenter = (GoalTrackingOverviewMonthPresenter) this.L$Anon1;
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            goalTrackingOverviewMonthPresenter.n = (TreeMap) obj;
            pb3 m = this.this$Anon0.a.o;
            TreeMap c = this.this$Anon0.a.n;
            if (c == null) {
                c = new TreeMap();
            }
            m.a(c);
            return qa4.a;
        }
    }

    @DexIgnore
    public GoalTrackingOverviewMonthPresenter$start$Anon1(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
        this.a = goalTrackingOverviewMonthPresenter;
    }

    @DexIgnore
    public final void a(os3<? extends List<GoalTrackingSummary>> os3) {
        Status a2 = os3.a();
        List list = (List) os3.b();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthPresenter", "mDateTransformations observer");
        if (a2 != Status.DATABASE_LOADING) {
            if (list != null && (!kd4.a((Object) this.a.k, (Object) list))) {
                this.a.k = list;
                fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, list, (yb4) null), 3, (Object) null);
            }
            this.a.o.c(!this.a.q.H());
        }
    }
}
