package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$Anon1;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1", f = "HeartRateOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
public final class HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<? extends Integer>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateOverviewWeekPresenter$start$Anon1.Anon2.Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1(HeartRateOverviewWeekPresenter$start$Anon1.Anon2.Anon1 anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1 heartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1 = new HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1(this.this$Anon0, yb4);
        heartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1.p$ = (zg4) obj;
        return heartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HeartRateOverviewWeekPresenter$start$Anon1$Anon2$Anon1$listRestingDataPoint$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            List<DailyHeartRateSummary> list = this.this$Anon0.$data;
            if (list == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList(db4.a(list, 10));
            for (DailyHeartRateSummary resting : list) {
                Resting resting2 = resting.getResting();
                arrayList.add(resting2 != null ? dc4.a(resting2.getValue()) : null);
            }
            return arrayList;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
