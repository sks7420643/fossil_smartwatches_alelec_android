package com.portfolio.platform.uirenew.home.customize.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1", mo27670f = "HomeDianaCustomizePresenter.kt", mo27671l = {87}, mo27672m = "invokeSuspend")
public final class HomeDianaCustomizePresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22701p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$1", mo27670f = "HomeDianaCustomizePresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$1 */
    public static final class C64271 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Boolean>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22702p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64271(com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1 homeDianaCustomizePresenter$start$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = homeDianaCustomizePresenter$start$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1.C64271 r0 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1.C64271(this.this$0, yb4);
            r0.f22702p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1.C64271) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.f22666h.clear();
                this.this$0.this$0.f22666h.addAll(this.this$0.this$0.f22680v.getAllComplicationRaw());
                this.this$0.this$0.f22667i.clear();
                this.this$0.this$0.f22667i.addAll(this.this$0.this$0.f22679u.getAllWatchAppRaw());
                this.this$0.this$0.f22672n.clear();
                return com.fossil.blesdk.obfuscated.dc4.m20839a(this.this$0.this$0.f22672n.addAll(this.this$0.this$0.f22684z.getAllRealDataRaw()));
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$2")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$2 */
    public static final class C64282<T> implements com.fossil.blesdk.obfuscated.C1548cc<java.util.List<? extends com.portfolio.platform.data.model.CustomizeRealData>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1 f22703a;

        @DexIgnore
        public C64282(com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1 homeDianaCustomizePresenter$start$1) {
            this.f22703a = homeDianaCustomizePresenter$start$1;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(java.util.List<com.portfolio.platform.data.model.CustomizeRealData> list) {
            if (list != null) {
                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f22703a.this$0.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.customize.diana.C6424xd92f2dc1(list, (com.fossil.blesdk.obfuscated.yb4) null, this), 3, (java.lang.Object) null);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$a")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$a */
    public static final class C6429a<T> implements com.fossil.blesdk.obfuscated.C1548cc<java.lang.String> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1 f22704a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$a$a")
        /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$a$a */
        public static final class C6430a<T> implements com.fossil.blesdk.obfuscated.C1548cc<java.util.List<? extends com.portfolio.platform.data.model.diana.preset.DianaPreset>> {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1.C6429a f22705a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$a$a$a")
            /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$a$a$a */
            public static final class C6431a<T> implements java.util.Comparator<T> {
                @DexIgnore
                public final int compare(T t, T t2) {
                    return com.fossil.blesdk.obfuscated.wb4.m29575a(java.lang.Boolean.valueOf(((com.portfolio.platform.data.model.diana.preset.DianaPreset) t2).isActive()), java.lang.Boolean.valueOf(((com.portfolio.platform.data.model.diana.preset.DianaPreset) t).isActive()));
                }
            }

            @DexIgnore
            public C6430a(com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1.C6429a aVar) {
                this.f22705a = aVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void mo8689a(java.util.List<com.portfolio.platform.data.model.diana.preset.DianaPreset> list) {
                if (list != null) {
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    local.mo33255d("HomeDianaCustomizePresenter", "onObserve on preset list change " + list);
                    java.util.List<T> a = com.fossil.blesdk.obfuscated.kb4.m24368a(list, new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1.C6429a.C6430a.C6431a());
                    boolean a2 = com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) a, (java.lang.Object) this.f22705a.f22704a.this$0.f22668j) ^ true;
                    int itemCount = this.f22705a.f22704a.this$0.f22678t.getItemCount();
                    if (a2 || a.size() != itemCount - 1) {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        local2.mo33256e("HomeDianaCustomizePresenter", "process change - " + a2 + " - itemCount: " + itemCount + " - sortedPresetSize: " + a.size());
                        if (this.f22705a.f22704a.this$0.f22673o == 1) {
                            this.f22705a.f22704a.this$0.mo40995a((java.util.List<com.portfolio.platform.data.model.diana.preset.DianaPreset>) a);
                        } else {
                            this.f22705a.f22704a.this$0.f22676r = com.fossil.blesdk.obfuscated.oa4.m26076a(true, a);
                        }
                    }
                }
            }
        }

        @DexIgnore
        public C6429a(com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1 homeDianaCustomizePresenter$start$1) {
            this.f22704a = homeDianaCustomizePresenter$start$1;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(java.lang.String str) {
            if (!android.text.TextUtils.isEmpty(str)) {
                com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.f22704a.this$0;
                com.portfolio.platform.data.source.DianaPresetRepository j = homeDianaCustomizePresenter.f22681w;
                if (str != null) {
                    homeDianaCustomizePresenter.f22664f = j.getPresetListAsLiveData(str);
                    this.f22704a.this$0.f22664f.mo2277a((androidx.lifecycle.LifecycleOwner) this.f22704a.this$0.f22678t, new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1.C6429a.C6430a(this));
                    this.f22704a.this$0.f22678t.mo26423a(false);
                    return;
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
            this.f22704a.this$0.f22678t.mo26423a(true);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$start$1(com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter homeDianaCustomizePresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homeDianaCustomizePresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1 homeDianaCustomizePresenter$start$1 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1(this.this$0, yb4);
        homeDianaCustomizePresenter$start$1.f22701p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homeDianaCustomizePresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22701p$;
            if (this.this$0.f22666h.isEmpty() || this.this$0.f22667i.isEmpty()) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("HomeDianaCustomizePresenter", "init comps, apps");
                com.fossil.blesdk.obfuscated.ug4 c = this.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1.C64271 r3 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1.C64271(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r3, this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        androidx.lifecycle.LiveData h = this.this$0.f22665g;
        com.fossil.blesdk.obfuscated.d23 r = this.this$0.f22678t;
        if (r != null) {
            h.mo2277a((com.fossil.blesdk.obfuscated.vu2) r, new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1.C64282(this));
            this.this$0.f22670l.mo2277a((androidx.lifecycle.LifecycleOwner) this.this$0.f22678t, new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1.C6429a(this));
            this.this$0.f22682x.mo41115f();
            com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39773a(com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_PRESET_APPS_DATA);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
    }
}
