package com.portfolio.platform.uirenew.home.details.sleep;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$sessions$1", mo27670f = "SleepDetailPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class SleepDetailPresenter$setDate$1$sessions$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession>>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23760p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepDetailPresenter$setDate$1$sessions$1(com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1 sleepDetailPresenter$setDate$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = sleepDetailPresenter$setDate$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$sessions$1 sleepDetailPresenter$setDate$1$sessions$1 = new com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$sessions$1(this.this$0, yb4);
        sleepDetailPresenter$setDate$1$sessions$1.f23760p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return sleepDetailPresenter$setDate$1$sessions$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$sessions$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter sleepDetailPresenter = this.this$0.this$0;
            return sleepDetailPresenter.mo41432a(sleepDetailPresenter.f23742g, (java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession>) this.this$0.this$0.f23748m);
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
