package com.portfolio.platform.uirenew.home.customize.diana;

import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.d23;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.oa4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.vu2;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.Comparator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$Anon1", f = "HomeDianaCustomizePresenter.kt", l = {87}, m = "invokeSuspend")
public final class HomeDianaCustomizePresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$Anon1$Anon1", f = "HomeDianaCustomizePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Boolean>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HomeDianaCustomizePresenter$start$Anon1 homeDianaCustomizePresenter$start$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = homeDianaCustomizePresenter$start$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.this$Anon0.h.clear();
                this.this$Anon0.this$Anon0.h.addAll(this.this$Anon0.this$Anon0.v.getAllComplicationRaw());
                this.this$Anon0.this$Anon0.i.clear();
                this.this$Anon0.this$Anon0.i.addAll(this.this$Anon0.this$Anon0.u.getAllWatchAppRaw());
                this.this$Anon0.this$Anon0.n.clear();
                return dc4.a(this.this$Anon0.this$Anon0.n.addAll(this.this$Anon0.this$Anon0.z.getAllRealDataRaw()));
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2<T> implements cc<List<? extends CustomizeRealData>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter$start$Anon1 a;

        @DexIgnore
        public Anon2(HomeDianaCustomizePresenter$start$Anon1 homeDianaCustomizePresenter$start$Anon1) {
            this.a = homeDianaCustomizePresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(List<CustomizeRealData> list) {
            if (list != null) {
                fi4 unused = ag4.b(this.a.this$Anon0.e(), (CoroutineContext) null, (CoroutineStart) null, new HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1(list, (yb4) null, this), 3, (Object) null);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements cc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter$start$Anon1 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$Anon1$a$a")
        /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$Anon1$a$a  reason: collision with other inner class name */
        public static final class C0139a<T> implements cc<List<? extends DianaPreset>> {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$Anon1$a$a$a")
            /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$Anon1$a$a$a  reason: collision with other inner class name */
            public static final class C0140a<T> implements Comparator<T> {
                @DexIgnore
                public final int compare(T t, T t2) {
                    return wb4.a(Boolean.valueOf(((DianaPreset) t2).isActive()), Boolean.valueOf(((DianaPreset) t).isActive()));
                }
            }

            @DexIgnore
            public C0139a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void a(List<DianaPreset> list) {
                if (list != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("HomeDianaCustomizePresenter", "onObserve on preset list change " + list);
                    List<T> a2 = kb4.a(list, new C0140a());
                    boolean a3 = kd4.a((Object) a2, (Object) this.a.a.this$Anon0.j) ^ true;
                    int itemCount = this.a.a.this$Anon0.t.getItemCount();
                    if (a3 || a2.size() != itemCount - 1) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.e("HomeDianaCustomizePresenter", "process change - " + a3 + " - itemCount: " + itemCount + " - sortedPresetSize: " + a2.size());
                        if (this.a.a.this$Anon0.o == 1) {
                            this.a.a.this$Anon0.a((List<DianaPreset>) a2);
                        } else {
                            this.a.a.this$Anon0.r = oa4.a(true, a2);
                        }
                    }
                }
            }
        }

        @DexIgnore
        public a(HomeDianaCustomizePresenter$start$Anon1 homeDianaCustomizePresenter$start$Anon1) {
            this.a = homeDianaCustomizePresenter$start$Anon1;
        }

        @DexIgnore
        public final void a(String str) {
            if (!TextUtils.isEmpty(str)) {
                HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.a.this$Anon0;
                DianaPresetRepository j = homeDianaCustomizePresenter.w;
                if (str != null) {
                    homeDianaCustomizePresenter.f = j.getPresetListAsLiveData(str);
                    this.a.this$Anon0.f.a((LifecycleOwner) this.a.this$Anon0.t, new C0139a(this));
                    this.a.this$Anon0.t.a(false);
                    return;
                }
                kd4.a();
                throw null;
            }
            this.a.this$Anon0.t.a(true);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$start$Anon1(HomeDianaCustomizePresenter homeDianaCustomizePresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = homeDianaCustomizePresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeDianaCustomizePresenter$start$Anon1 homeDianaCustomizePresenter$start$Anon1 = new HomeDianaCustomizePresenter$start$Anon1(this.this$Anon0, yb4);
        homeDianaCustomizePresenter$start$Anon1.p$ = (zg4) obj;
        return homeDianaCustomizePresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDianaCustomizePresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            if (this.this$Anon0.h.isEmpty() || this.this$Anon0.i.isEmpty()) {
                FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "init comps, apps");
                ug4 c = this.this$Anon0.b();
                Anon1 anon1 = new Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                if (yf4.a(c, anon1, this) == a2) {
                    return a2;
                }
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        LiveData h = this.this$Anon0.g;
        d23 r = this.this$Anon0.t;
        if (r != null) {
            h.a((vu2) r, new Anon2(this));
            this.this$Anon0.l.a((LifecycleOwner) this.this$Anon0.t, new a(this));
            this.this$Anon0.x.f();
            BleCommandResultManager.d.a(CommunicateMode.SET_PRESET_APPS_DATA);
            return qa4.a;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
    }
}
