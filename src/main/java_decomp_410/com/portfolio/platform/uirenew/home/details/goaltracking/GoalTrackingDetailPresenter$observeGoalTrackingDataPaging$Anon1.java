package com.portfolio.platform.uirenew.home.details.goaltracking;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1<T> implements cc<qd<GoalTrackingData>> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDetailPresenter a;

    @DexIgnore
    public GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1(GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
        this.a = goalTrackingDetailPresenter;
    }

    @DexIgnore
    public final void a(qd<GoalTrackingData> qdVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("getGoalTrackingDataPaging observer size=");
        sb.append(qdVar != null ? Integer.valueOf(qdVar.size()) : null);
        local.d("GoalTrackingDetailPresenter", sb.toString());
        if (qdVar != null) {
            this.a.j = true;
            fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1$$special$$inlined$let$lambda$Anon1(qdVar, (yb4) null, this), 3, (Object) null);
        }
    }
}
