package com.portfolio.platform.uirenew.home.dashboard.goaltracking;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ab3;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.za3;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$Anon1", f = "DashboardGoalTrackingPresenter.kt", l = {61}, m = "invokeSuspend")
public final class DashboardGoalTrackingPresenter$initDataSource$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DashboardGoalTrackingPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements cc<qd<GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ DashboardGoalTrackingPresenter$initDataSource$Anon1 a;

        @DexIgnore
        public a(DashboardGoalTrackingPresenter$initDataSource$Anon1 dashboardGoalTrackingPresenter$initDataSource$Anon1) {
            this.a = dashboardGoalTrackingPresenter$initDataSource$Anon1;
        }

        @DexIgnore
        public final void a(qd<GoalTrackingSummary> qdVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("getSummariesPaging observer size=");
            sb.append(qdVar != null ? Integer.valueOf(qdVar.size()) : null);
            local.d("DashboardGoalTrackingPresenter", sb.toString());
            if (qdVar != null) {
                this.a.this$Anon0.k().a(qdVar);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardGoalTrackingPresenter$initDataSource$Anon1(DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = dashboardGoalTrackingPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DashboardGoalTrackingPresenter$initDataSource$Anon1 dashboardGoalTrackingPresenter$initDataSource$Anon1 = new DashboardGoalTrackingPresenter$initDataSource$Anon1(this.this$Anon0, yb4);
        dashboardGoalTrackingPresenter$initDataSource$Anon1.p$ = (zg4) obj;
        return dashboardGoalTrackingPresenter$initDataSource$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DashboardGoalTrackingPresenter$initDataSource$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 a3 = this.this$Anon0.b();
            DashboardGoalTrackingPresenter$initDataSource$Anon1$user$Anon1 dashboardGoalTrackingPresenter$initDataSource$Anon1$user$Anon1 = new DashboardGoalTrackingPresenter$initDataSource$Anon1$user$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a3, dashboardGoalTrackingPresenter$initDataSource$Anon1$user$Anon1, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser = (MFUser) obj;
        if (mFUser != null) {
            Date d = rk2.d(mFUser.getCreatedAt());
            DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter = this.this$Anon0;
            GoalTrackingRepository e = dashboardGoalTrackingPresenter.i;
            GoalTrackingRepository e2 = this.this$Anon0.i;
            GoalTrackingDao c = this.this$Anon0.k;
            GoalTrackingDatabase d2 = this.this$Anon0.l;
            kd4.a((Object) d, "createdDate");
            dashboardGoalTrackingPresenter.g = e.getSummariesPaging(e2, c, d2, d, this.this$Anon0.m, this.this$Anon0);
            Listing f = this.this$Anon0.g;
            if (f != null) {
                LiveData pagedList = f.getPagedList();
                if (pagedList != null) {
                    za3 k = this.this$Anon0.k();
                    if (k != null) {
                        pagedList.a((ab3) k, new a(this));
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment");
                    }
                }
            }
        }
        return qa4.a;
    }
}
