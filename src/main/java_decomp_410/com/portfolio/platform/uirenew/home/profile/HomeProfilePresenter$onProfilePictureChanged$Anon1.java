package com.portfolio.platform.uirenew.home.profile;

import android.graphics.Bitmap;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.vr3;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.MFUser;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$Anon1", f = "HomeProfilePresenter.kt", l = {335, 341}, m = "invokeSuspend")
public final class HomeProfilePresenter$onProfilePictureChanged$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Uri $imageUri;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeProfilePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$Anon1$Anon1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super String>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(Bitmap bitmap, yb4 yb4) {
            super(2, yb4);
            this.$bitmap = bitmap;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.$bitmap, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                Bitmap bitmap = this.$bitmap;
                if (bitmap != null) {
                    return vr3.a(bitmap);
                }
                kd4.a();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$onProfilePictureChanged$Anon1(HomeProfilePresenter homeProfilePresenter, Uri uri, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = homeProfilePresenter;
        this.$imageUri = uri;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeProfilePresenter$onProfilePictureChanged$Anon1 homeProfilePresenter$onProfilePictureChanged$Anon1 = new HomeProfilePresenter$onProfilePictureChanged$Anon1(this.this$Anon0, this.$imageUri, yb4);
        homeProfilePresenter$onProfilePictureChanged$Anon1.p$ = (zg4) obj;
        return homeProfilePresenter$onProfilePictureChanged$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeProfilePresenter$onProfilePictureChanged$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        MFUser mFUser;
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            ug4 b = this.this$Anon0.c();
            HomeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1 homeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1 = new HomeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(b, homeProfilePresenter$onProfilePictureChanged$Anon1$bitmap$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            mFUser = (MFUser) this.L$Anon2;
            Bitmap bitmap = (Bitmap) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            try {
                na4.a(obj);
                mFUser.setProfilePicture((String) obj);
                this.this$Anon0.b(this.this$Anon0.k());
            } catch (Exception e) {
                e.printStackTrace();
                this.this$Anon0.l().o();
            }
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Bitmap bitmap2 = (Bitmap) obj;
        MFUser k = this.this$Anon0.k();
        if (k != null) {
            ug4 b2 = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(bitmap2, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = bitmap2;
            this.L$Anon2 = k;
            this.label = 2;
            obj = yf4.a(b2, anon1, this);
            if (obj == a) {
                return a;
            }
            mFUser = k;
            mFUser.setProfilePicture((String) obj);
        }
        this.this$Anon0.b(this.this$Anon0.k());
        return qa4.a;
    }
}
