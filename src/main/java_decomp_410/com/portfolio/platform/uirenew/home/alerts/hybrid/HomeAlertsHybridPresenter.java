package com.portfolio.platform.uirenew.home.alerts.hybrid;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.bj2;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ox3;
import com.fossil.blesdk.obfuscated.xy2;
import com.fossil.blesdk.obfuscated.yy2;
import com.fossil.blesdk.obfuscated.zy2;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeAlertsHybridPresenter extends xy2 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((fd4) null);
    @DexIgnore
    public LiveData<String> f; // = PortfolioApp.W.c().f();
    @DexIgnore
    public ArrayList<Alarm> g; // = new ArrayList<>();
    @DexIgnore
    public boolean h;
    @DexIgnore
    public /* final */ yy2 i;
    @DexIgnore
    public /* final */ AlarmHelper j;
    @DexIgnore
    public /* final */ SetAlarms k;
    @DexIgnore
    public /* final */ AlarmsRepository l;
    @DexIgnore
    public /* final */ en2 m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return HomeAlertsHybridPresenter.n;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<SetAlarms.d, SetAlarms.b> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ Alarm b;

        @DexIgnore
        public b(HomeAlertsHybridPresenter homeAlertsHybridPresenter, Alarm alarm) {
            this.a = homeAlertsHybridPresenter;
            this.b = alarm;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetAlarms.d dVar) {
            kd4.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsHybridPresenter.o.a();
            local.d(a2, "enableAlarm - onSuccess: alarmUri = " + dVar.a().getUri() + ", alarmId = " + dVar.a().getId());
            this.a.i.a();
            this.a.b(this.b, true);
        }

        @DexIgnore
        public void a(SetAlarms.b bVar) {
            kd4.b(bVar, "errorValue");
            this.a.i.a();
            int c = bVar.c();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsHybridPresenter.o.a();
            local.d(a2, "enableAlarm() - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.a.i.c();
                } else if (!(c == 1112 || c == 1113)) {
                    this.a.i.w();
                }
                this.a.b(bVar.a(), false);
                return;
            }
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(bVar.b());
            kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            yy2 f = this.a.i;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                f.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                this.a.b(bVar.a(), false);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements cc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridPresenter a;

        @DexIgnore
        public c(HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
            this.a = homeAlertsHybridPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            yy2 unused = this.a.i;
        }
    }

    /*
    static {
        String simpleName = HomeAlertsHybridPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "HomeAlertsHybridPresenter::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public HomeAlertsHybridPresenter(yy2 yy2, AlarmHelper alarmHelper, SetAlarms setAlarms, AlarmsRepository alarmsRepository, en2 en2) {
        kd4.b(yy2, "mView");
        kd4.b(alarmHelper, "mAlarmHelper");
        kd4.b(setAlarms, "mSetAlarms");
        kd4.b(alarmsRepository, "mAlarmRepository");
        kd4.b(en2, "mSharedPreferencesManager");
        this.i = yy2;
        this.j = alarmHelper;
        this.k = setAlarms;
        this.l = alarmsRepository;
        this.m = en2;
    }

    @DexIgnore
    public void h() {
        this.h = !this.h;
        this.m.d(this.h);
        this.i.i(this.h);
        this.i.n(this.h);
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d(n, "onSetAlarmsSuccess");
        this.j.c(PortfolioApp.W.c());
        PortfolioApp c2 = PortfolioApp.W.c();
        String a2 = this.f.a();
        if (a2 != null) {
            kd4.a((Object) a2, "mActiveSerial.value!!");
            c2.k(a2);
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void j() {
        this.i.a(this);
    }

    @DexIgnore
    @ox3
    public final void onSetAlarmEventEndComplete(bj2 bj2) {
        FLogger.INSTANCE.getLocal().d(n, "onSetAlarmEventEndComplete()");
        if (bj2 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = n;
            local.d(str, "onSetAlarmEventEndComplete() - event = " + bj2);
            if (bj2.b()) {
                String a2 = bj2.a();
                Iterator<Alarm> it = this.g.iterator();
                while (it.hasNext()) {
                    Alarm next = it.next();
                    if (kd4.a((Object) next.getUri(), (Object) a2)) {
                        next.setActive(false);
                    }
                }
                this.i.s();
            }
        }
    }

    @DexIgnore
    public final void b(Alarm alarm, boolean z) {
        kd4.b(alarm, "editAlarm");
        Iterator<Alarm> it = this.g.iterator();
        while (it.hasNext()) {
            Alarm next = it.next();
            if (kd4.a((Object) next.getUri(), (Object) alarm.getUri())) {
                ArrayList<Alarm> arrayList = this.g;
                ArrayList<Alarm> arrayList2 = arrayList;
                arrayList2.set(arrayList.indexOf(next), Alarm.copy$default(alarm, (String) null, (String) null, (String) null, 0, 0, (int[]) null, false, false, (String) null, (String) null, 0, 2047, (Object) null));
                if (!z) {
                    break;
                }
                i();
            }
            Alarm alarm2 = alarm;
        }
        this.i.d(this.g);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(n, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.k.f();
        PortfolioApp.W.b((Object) this);
        LiveData<String> liveData = this.f;
        yy2 yy2 = this.i;
        if (yy2 != null) {
            liveData.a((zy2) yy2, new HomeAlertsHybridPresenter$start$Anon1(this));
            BleCommandResultManager.d.a(CommunicateMode.SET_LIST_ALARM);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(n, "stop");
        this.f.b(new c(this));
        this.k.g();
        PortfolioApp.W.c(this);
    }

    @DexIgnore
    public void a(Alarm alarm) {
        CharSequence a2 = this.f.a();
        if (a2 == null || a2.length() == 0) {
            FLogger.INSTANCE.getLocal().d(n, "Current Active Device Serial Is Empty");
        } else if (alarm != null || this.g.size() < 32) {
            yy2 yy2 = this.i;
            String a3 = this.f.a();
            if (a3 != null) {
                kd4.a((Object) a3, "mActiveSerial.value!!");
                yy2.a(a3, this.g, alarm);
                return;
            }
            kd4.a();
            throw null;
        } else {
            this.i.r();
        }
    }

    @DexIgnore
    public void a(Alarm alarm, boolean z) {
        kd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        CharSequence a2 = this.f.a();
        if (!(a2 == null || a2.length() == 0)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = n;
            local.d(str, "enableAlarm - alarmTotalMinues: " + alarm.getTotalMinutes() + " - enable: " + z);
            alarm.setActive(z);
            this.i.b();
            SetAlarms setAlarms = this.k;
            String a3 = this.f.a();
            if (a3 != null) {
                kd4.a((Object) a3, "mActiveSerial.value!!");
                setAlarms.a(new SetAlarms.c(a3, this.g, alarm), new b(this, alarm));
                return;
            }
            kd4.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(n, "enableAlarm - Current Active Device Serial Is Empty");
    }
}
