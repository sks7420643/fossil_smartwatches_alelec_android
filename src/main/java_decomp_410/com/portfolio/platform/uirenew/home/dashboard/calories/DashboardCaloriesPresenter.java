package com.portfolio.platform.uirenew.home.dashboard.calories;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.aa3;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.obfuscated.xk2;
import com.fossil.blesdk.obfuscated.y93;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.z93;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DashboardCaloriesPresenter extends y93 implements PagingRequestHelper.a {
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public Listing<ActivitySummary> g;
    @DexIgnore
    public /* final */ z93 h;
    @DexIgnore
    public /* final */ SummariesRepository i;
    @DexIgnore
    public /* final */ FitnessDataRepository j;
    @DexIgnore
    public /* final */ ActivitySummaryDao k;
    @DexIgnore
    public /* final */ FitnessDatabase l;
    @DexIgnore
    public /* final */ UserRepository m;
    @DexIgnore
    public /* final */ h42 n;
    @DexIgnore
    public /* final */ xk2 o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public DashboardCaloriesPresenter(z93 z93, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, UserRepository userRepository, h42 h42, xk2 xk2) {
        kd4.b(z93, "mView");
        kd4.b(summariesRepository, "mSummariesRepository");
        kd4.b(fitnessDataRepository, "mFitnessDataRepository");
        kd4.b(activitySummaryDao, "mActivitySummaryDao");
        kd4.b(fitnessDatabase, "mFitnessDatabase");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(h42, "mAppExecutors");
        kd4.b(xk2, "mFitnessHelper");
        this.h = z93;
        this.i = summariesRepository;
        this.j = fitnessDataRepository;
        this.k = activitySummaryDao;
        this.l = fitnessDatabase;
        this.m = userRepository;
        this.n = h42;
        this.o = xk2;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.W.c().e());
    }

    @DexIgnore
    public void k() {
        this.h.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("DashboardCaloriesPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!rk2.s(this.f).booleanValue()) {
            this.f = new Date();
            Listing<ActivitySummary> listing = this.g;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardCaloriesPresenter", "stop");
    }

    @DexIgnore
    public void h() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DashboardCaloriesPresenter$initDataSource$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        try {
            Listing<ActivitySummary> listing = this.g;
            if (listing != null) {
                LiveData<qd<ActivitySummary>> pagedList = listing.getPagedList();
                if (pagedList != null) {
                    z93 z93 = this.h;
                    if (z93 != null) {
                        pagedList.a((LifecycleOwner) (aa3) z93);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesFragment");
                    }
                }
            }
            this.i.removePagingListener();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e.printStackTrace();
            sb.append(qa4.a);
            local.e("DashboardCaloriesPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void j() {
        FLogger.INSTANCE.getLocal().d("DashboardCaloriesPresenter", "retry all failed request");
        Listing<ActivitySummary> listing = this.g;
        if (listing != null) {
            wc4<qa4> retry = listing.getRetry();
            if (retry != null) {
                qa4 invoke = retry.invoke();
            }
        }
    }

    @DexIgnore
    public void a(PagingRequestHelper.e eVar) {
        kd4.b(eVar, "report");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardCaloriesPresenter", "onStatusChange status=" + eVar);
        if (eVar.a()) {
            this.h.f();
        }
    }
}
