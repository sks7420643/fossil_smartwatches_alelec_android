package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2", mo27670f = "CommuteTimeSettingsPresenter.kt", mo27671l = {161, 168}, mo27672m = "invokeSuspend")
public final class CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $address;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $displayInfo;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22768p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2$1", mo27670f = "CommuteTimeSettingsPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2$1 */
    public static final class C64531 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $searchedRecent;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22769p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64531(com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2 commuteTimeSettingsPresenter$saveCommuteTimeSetting$2, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = commuteTimeSettingsPresenter$saveCommuteTimeSetting$2;
            this.$searchedRecent = ref$ObjectRef;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2.C64531 r0 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2.C64531(this.this$0, this.$searchedRecent, yb4);
            r0.f22769p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2.C64531) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.f22766m.mo27002a((java.util.List<java.lang.String>) (java.util.List) this.$searchedRecent.element);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2(com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter commuteTimeSettingsPresenter, java.lang.String str, java.lang.String str2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = commuteTimeSettingsPresenter;
        this.$address = str;
        this.$displayInfo = str2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2 commuteTimeSettingsPresenter$saveCommuteTimeSetting$2 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2(this.this$0, this.$address, this.$displayInfo, yb4);
        commuteTimeSettingsPresenter$saveCommuteTimeSetting$2.f22768p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return commuteTimeSettingsPresenter$saveCommuteTimeSetting$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(T t) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef;
        kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2;
        T a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(t);
            zg4 = this.f22768p$;
            ref$ObjectRef2 = new kotlin.jvm.internal.Ref$ObjectRef();
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6454x2a95d3f9 commuteTimeSettingsPresenter$saveCommuteTimeSetting$2$searchedRecent$1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.C6454x2a95d3f9(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = ref$ObjectRef2;
            this.L$2 = ref$ObjectRef2;
            this.label = 1;
            t = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, commuteTimeSettingsPresenter$saveCommuteTimeSetting$2$searchedRecent$1, this);
            if (t == a) {
                return a;
            }
            ref$ObjectRef = ref$ObjectRef2;
        } else if (i == 1) {
            ref$ObjectRef2 = (kotlin.jvm.internal.Ref$ObjectRef) this.L$2;
            ref$ObjectRef = (kotlin.jvm.internal.Ref$ObjectRef) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(t);
        } else if (i == 2) {
            kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef3 = (kotlin.jvm.internal.Ref$ObjectRef) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(t);
            this.this$0.f22765l.mo31429a();
            this.this$0.f22765l.mo30027r(true);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) t, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
        ref$ObjectRef2.element = (java.util.List) t;
        if (!((java.util.List) ref$ObjectRef.element).contains(this.$address)) {
            if (this.$displayInfo.length() > 0) {
                if (this.$address.length() > 0) {
                    ((java.util.List) ref$ObjectRef.element).add(0, this.$address);
                    if (((java.util.List) ref$ObjectRef.element).size() > 5) {
                        ref$ObjectRef.element = ((java.util.List) ref$ObjectRef.element).subList(0, 5);
                    }
                    com.fossil.blesdk.obfuscated.ug4 a3 = this.this$0.mo31441c();
                    com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2.C64531 r1 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2.C64531(this, ref$ObjectRef, (com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg4;
                    this.L$1 = ref$ObjectRef;
                    this.label = 2;
                    if (com.fossil.blesdk.obfuscated.yf4.m30997a(a3, r1, this) == a) {
                        return a;
                    }
                }
            }
        }
        this.this$0.f22765l.mo31429a();
        this.this$0.f22765l.mo30027r(true);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
