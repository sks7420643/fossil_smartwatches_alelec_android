package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1", mo27670f = "NotificationContactsPresenter.kt", mo27671l = {60}, mo27672m = "invokeSuspend")
public final class NotificationContactsPresenter$start$1$2$onSuccess$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.px2.C4879d $responseValue;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22421p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1.C63382 this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1$1", mo27670f = "NotificationContactsPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1$1 */
    public static final class C63391 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Boolean>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22422p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C63391(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1 notificationContactsPresenter$start$1$2$onSuccess$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = notificationContactsPresenter$start$1$2$onSuccess$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1.C63391 r0 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1.C63391(this.this$0, yb4);
            r0.f22422p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1.C63391) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                for (com.fossil.wearables.fsl.contact.ContactGroup contactGroup : this.this$0.$responseValue.mo30277a()) {
                    java.util.List<com.fossil.wearables.fsl.contact.Contact> contacts = contactGroup.getContacts();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contacts, "it.contacts");
                    if (!contacts.isEmpty()) {
                        com.fossil.wearables.fsl.contact.Contact contact = contactGroup.getContacts().get(0);
                        com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper contactWrapper = new com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper(contact, (java.lang.String) null, 2, (com.fossil.blesdk.obfuscated.fd4) null);
                        contactWrapper.setAdded(true);
                        com.fossil.wearables.fsl.contact.Contact contact2 = contactWrapper.getContact();
                        if (contact2 != null) {
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact, "contact");
                            contact2.setDbRowId(contact.getDbRowId());
                        }
                        com.fossil.wearables.fsl.contact.Contact contact3 = contactWrapper.getContact();
                        if (contact3 != null) {
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact, "contact");
                            contact3.setUseSms(contact.isUseSms());
                        }
                        com.fossil.wearables.fsl.contact.Contact contact4 = contactWrapper.getContact();
                        if (contact4 != null) {
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact, "contact");
                            contact4.setUseCall(contact.isUseCall());
                        }
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) contact, "contact");
                        java.util.List<com.fossil.wearables.fsl.contact.PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) phoneNumbers, "contact.phoneNumbers");
                        if (!phoneNumbers.isEmpty()) {
                            com.fossil.wearables.fsl.contact.PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) phoneNumber, "contact.phoneNumbers[0]");
                            if (!android.text.TextUtils.isEmpty(phoneNumber.getNumber())) {
                                contactWrapper.setHasPhoneNumber(true);
                                com.fossil.wearables.fsl.contact.PhoneNumber phoneNumber2 = contact.getPhoneNumbers().get(0);
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) phoneNumber2, "contact.phoneNumbers[0]");
                                contactWrapper.setPhoneNumber(phoneNumber2.getNumber());
                                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                java.lang.String a = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter.f22409n.mo40855a();
                                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                                sb.append(".Inside loadContactData filter selected contact, phoneNumber=");
                                com.fossil.wearables.fsl.contact.PhoneNumber phoneNumber3 = contact.getPhoneNumbers().get(0);
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) phoneNumber3, "contact.phoneNumbers[0]");
                                sb.append(phoneNumber3.getNumber());
                                local.mo33255d(a, sb.toString());
                            }
                        }
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String a2 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter.f22409n.mo40855a();
                        local2.mo33255d(a2, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                        this.this$0.this$0.f22420a.this$0.mo40852j().add(contactWrapper);
                    }
                }
                return com.fossil.blesdk.obfuscated.dc4.m20839a(this.this$0.this$0.f22420a.this$0.mo40853k().addAll(com.fossil.blesdk.obfuscated.kb4.m24381d(this.this$0.this$0.f22420a.this$0.mo40852j())));
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationContactsPresenter$start$1$2$onSuccess$1(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1.C63382 r1, com.fossil.blesdk.obfuscated.px2.C4879d dVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = r1;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1 notificationContactsPresenter$start$1$2$onSuccess$1 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1(this.this$0, this.$responseValue, yb4);
        notificationContactsPresenter$start$1$2$onSuccess$1.f22421p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationContactsPresenter$start$1$2$onSuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22421p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.f22420a.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1.C63391 r3 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1.C63391(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.f22420a.this$0.f22412h.mo32190a(this.this$0.f22420a.this$0.mo40852j(), com.fossil.blesdk.obfuscated.fs3.f14915a.mo27491a());
        this.this$0.f22420a.this$0.f22416l.mo2302a(0, new android.os.Bundle(), this.this$0.f22420a.this$0);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
