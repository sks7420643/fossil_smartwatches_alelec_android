package com.portfolio.platform.uirenew.home.details.activetime;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.eg3;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$showDetailChart$Anon1$pairData$Anon1", f = "ActiveTimeDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class ActiveTimeDetailPresenter$showDetailChart$Anon1$pairData$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Pair<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActiveTimeDetailPresenter$showDetailChart$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActiveTimeDetailPresenter$showDetailChart$Anon1$pairData$Anon1(ActiveTimeDetailPresenter$showDetailChart$Anon1 activeTimeDetailPresenter$showDetailChart$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = activeTimeDetailPresenter$showDetailChart$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ActiveTimeDetailPresenter$showDetailChart$Anon1$pairData$Anon1 activeTimeDetailPresenter$showDetailChart$Anon1$pairData$Anon1 = new ActiveTimeDetailPresenter$showDetailChart$Anon1$pairData$Anon1(this.this$Anon0, yb4);
        activeTimeDetailPresenter$showDetailChart$Anon1$pairData$Anon1.p$ = (zg4) obj;
        return activeTimeDetailPresenter$showDetailChart$Anon1$pairData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActiveTimeDetailPresenter$showDetailChart$Anon1$pairData$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return eg3.a.a(this.this$Anon0.this$Anon0.g, (List<ActivitySample>) this.this$Anon0.this$Anon0.l, 1);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
