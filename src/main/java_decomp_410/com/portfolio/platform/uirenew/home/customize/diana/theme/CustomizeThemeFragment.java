package com.portfolio.platform.uirenew.home.customize.diana.theme;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.i43;
import com.fossil.blesdk.obfuscated.ic;
import com.fossil.blesdk.obfuscated.j42;
import com.fossil.blesdk.obfuscated.j43;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l43;
import com.fossil.blesdk.obfuscated.lc;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa;
import com.fossil.blesdk.obfuscated.qa2;
import com.fossil.blesdk.obfuscated.tr3;
import com.fossil.blesdk.obfuscated.ts2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.zr2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomizeThemeFragment extends zr2 implements j43 {
    @DexIgnore
    public tr3<qa2> j;
    @DexIgnore
    public i43 k;
    @DexIgnore
    public ts2 l;
    @DexIgnore
    public long m; // = 500;
    @DexIgnore
    public j42 n;
    @DexIgnore
    public DianaCustomizeViewModel o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ts2.c {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemeFragment a;

        @DexIgnore
        public b(CustomizeThemeFragment customizeThemeFragment) {
            this.a = customizeThemeFragment;
        }

        @DexIgnore
        public void a(WatchFaceWrapper watchFaceWrapper) {
            kd4.b(watchFaceWrapper, "watchFaceWrapper");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeThemeFragment", "onItemClicked backgroundWrapper=" + watchFaceWrapper);
            CustomizeThemeFragment.b(this.a).a(watchFaceWrapper);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ i43 b(CustomizeThemeFragment customizeThemeFragment) {
        i43 i43 = customizeThemeFragment.k;
        if (i43 != null) {
            return i43;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void n(List<WatchFaceWrapper> list) {
        kd4.b(list, "watchFaceWrappers");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemeFragment", "showWatchFaces watchFaceWrappers=" + list);
        ts2 ts2 = this.l;
        if (ts2 != null) {
            ts2.a(list);
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = (DianaCustomizeEditActivity) activity;
            j42 j42 = this.n;
            if (j42 != null) {
                ic a2 = lc.a((FragmentActivity) dianaCustomizeEditActivity, (kc.b) j42).a(DianaCustomizeViewModel.class);
                kd4.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.o = (DianaCustomizeViewModel) a2;
                i43 i43 = this.k;
                if (i43 != null) {
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.o;
                    if (dianaCustomizeViewModel != null) {
                        i43.a(dianaCustomizeViewModel);
                    } else {
                        kd4.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else {
                kd4.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        qa2 qa2 = (qa2) qa.a(layoutInflater, R.layout.fragment_customize_theme, viewGroup, false, O0());
        PortfolioApp.W.c().g().a(new l43(this)).a(this);
        this.j = new tr3<>(this, qa2);
        kd4.a((Object) qa2, "binding");
        return qa2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        i43 i43 = this.k;
        if (i43 != null) {
            i43.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        i43 i43 = this.k;
        if (i43 != null) {
            i43.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        ts2 ts2 = new ts2((ArrayList) null, (ts2.c) null, 3, (fd4) null);
        ts2.a((ts2.c) new b(this));
        this.l = ts2;
        tr3<qa2> tr3 = this.j;
        if (tr3 != null) {
            qa2 a2 = tr3.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.q;
                recyclerView.setHasFixedSize(true);
                recyclerView.setItemAnimator((RecyclerView.j) null);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                recyclerView.setAdapter(this.l);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void n(int i) {
        if (i >= 0) {
            ts2 ts2 = this.l;
            if (ts2 != null) {
                ts2.a(i);
            }
            tr3<qa2> tr3 = this.j;
            if (tr3 != null) {
                qa2 a2 = tr3.a();
                if (a2 != null) {
                    RecyclerView recyclerView = a2.q;
                    RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                    if (layoutManager != null) {
                        int I = ((LinearLayoutManager) layoutManager).I();
                        RecyclerView.m layoutManager2 = recyclerView.getLayoutManager();
                        if (layoutManager2 == null) {
                            throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                        } else if (i < ((LinearLayoutManager) layoutManager2).G() || i > I) {
                            fi4 unused = ag4.b(ah4.a(nh4.c()), (CoroutineContext) null, (CoroutineStart) null, new CustomizeThemeFragment$showCurrentWatchFace$$inlined$let$lambda$Anon1(recyclerView, (yb4) null, this, i), 3, (Object) null);
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                    }
                }
            } else {
                kd4.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(i43 i43) {
        kd4.b(i43, "presenter");
        this.k = i43;
    }
}
