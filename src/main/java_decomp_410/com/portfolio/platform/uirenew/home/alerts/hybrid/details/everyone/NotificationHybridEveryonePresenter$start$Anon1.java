package com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.t03;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$Anon1", f = "NotificationHybridEveryonePresenter.kt", l = {42}, m = "invokeSuspend")
public final class NotificationHybridEveryonePresenter$start$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationHybridEveryonePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$Anon1$Anon1", f = "NotificationHybridEveryonePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;

        @DexIgnore
        public Anon1(yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                PortfolioApp.W.c().J();
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements i62.d<t03.d, t03.b> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridEveryonePresenter$start$Anon1 a;

        @DexIgnore
        public a(NotificationHybridEveryonePresenter$start$Anon1 notificationHybridEveryonePresenter$start$Anon1) {
            this.a = notificationHybridEveryonePresenter$start$Anon1;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(t03.d dVar) {
            kd4.b(dVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(NotificationHybridEveryonePresenter.m.a(), "GetAllContactGroup onSuccess");
            for (ContactGroup contactGroup : dVar.a()) {
                for (Contact next : contactGroup.getContacts()) {
                    kd4.a((Object) next, "contact");
                    if (next.getContactId() == -100 || next.getContactId() == -200) {
                        ContactWrapper contactWrapper = new ContactWrapper(next, (String) null, 2, (fd4) null);
                        contactWrapper.setAdded(true);
                        Contact contact = contactWrapper.getContact();
                        if (contact != null) {
                            contact.setDbRowId(next.getDbRowId());
                            contact.setUseSms(next.isUseSms());
                            contact.setUseCall(next.isUseCall());
                        }
                        contactWrapper.setCurrentHandGroup(contactGroup.getHour());
                        List<PhoneNumber> phoneNumbers = next.getPhoneNumbers();
                        kd4.a((Object) phoneNumbers, "contact.phoneNumbers");
                        if (!phoneNumbers.isEmpty()) {
                            PhoneNumber phoneNumber = next.getPhoneNumbers().get(0);
                            kd4.a((Object) phoneNumber, "contact.phoneNumbers[0]");
                            String number = phoneNumber.getNumber();
                            if (!TextUtils.isEmpty(number)) {
                                contactWrapper.setHasPhoneNumber(true);
                                contactWrapper.setPhoneNumber(number);
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String a2 = NotificationHybridEveryonePresenter.m.a();
                                local.d(a2, " filter selected contact, phoneNumber=" + number);
                            }
                        }
                        Iterator it = this.a.this$Anon0.i.iterator();
                        int i = 0;
                        while (true) {
                            if (!it.hasNext()) {
                                i = -1;
                                break;
                            }
                            Contact contact2 = ((ContactWrapper) it.next()).getContact();
                            if (contact2 != null && contact2.getContactId() == next.getContactId()) {
                                break;
                            }
                            i++;
                        }
                        if (i != -1) {
                            contactWrapper.setCurrentHandGroup(this.a.this$Anon0.h);
                            this.a.this$Anon0.i.remove(i);
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a3 = NotificationHybridEveryonePresenter.m.a();
                        local2.d(a3, ".Inside loadContactData filter selected contact, rowId = " + next.getDbRowId() + ", isUseText = " + next.isUseSms() + ", isUseCall = " + next.isUseCall());
                        this.a.this$Anon0.m().add(contactWrapper);
                    }
                }
            }
            if (!this.a.this$Anon0.i.isEmpty()) {
                for (ContactWrapper add : this.a.this$Anon0.i) {
                    this.a.this$Anon0.m().add(add);
                }
            }
            this.a.this$Anon0.g.b(this.a.this$Anon0.m(), this.a.this$Anon0.h);
        }

        @DexIgnore
        public void a(t03.b bVar) {
            kd4.b(bVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(NotificationHybridEveryonePresenter.m.a(), "GetAllContactGroup onError");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationHybridEveryonePresenter$start$Anon1(NotificationHybridEveryonePresenter notificationHybridEveryonePresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = notificationHybridEveryonePresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        NotificationHybridEveryonePresenter$start$Anon1 notificationHybridEveryonePresenter$start$Anon1 = new NotificationHybridEveryonePresenter$start$Anon1(this.this$Anon0, yb4);
        notificationHybridEveryonePresenter$start$Anon1.p$ = (zg4) obj;
        return notificationHybridEveryonePresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((NotificationHybridEveryonePresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            if (!PortfolioApp.W.c().u().N()) {
                ug4 a3 = this.this$Anon0.b();
                Anon1 anon1 = new Anon1((yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                if (yf4.a(a3, anon1, this) == a2) {
                    return a2;
                }
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (this.this$Anon0.m().isEmpty()) {
            this.this$Anon0.j.a(this.this$Anon0.k, null, new a(this));
        }
        return qa4.a;
    }
}
