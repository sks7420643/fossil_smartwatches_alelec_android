package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import android.text.SpannableString;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.by2;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nl2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationWatchRemindersPresenter$start$Anon1<T> implements cc<List<? extends InactivityNudgeTimeModel>> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationWatchRemindersPresenter a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$Anon1$Anon1", f = "NotificationWatchRemindersPresenter.kt", l = {60}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $lInActivityNudgeTimeModel;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter$start$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$Anon1$Anon1$Anon1")
        @gc4(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$Anon1$Anon1$Anon1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0135Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public zg4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0135Anon1(Anon1 anon1, yb4 yb4) {
                super(2, yb4);
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                kd4.b(yb4, "completion");
                C0135Anon1 anon1 = new C0135Anon1(this.this$Anon0, yb4);
                anon1.p$ = (zg4) obj;
                return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0135Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                cc4.a();
                if (this.label == 0) {
                    na4.a(obj);
                    this.this$Anon0.this$Anon0.a.r.getInactivityNudgeTimeDao().upsertListInactivityNudgeTime(this.this$Anon0.$lInActivityNudgeTimeModel);
                    return qa4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(NotificationWatchRemindersPresenter$start$Anon1 notificationWatchRemindersPresenter$start$Anon1, List list, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = notificationWatchRemindersPresenter$start$Anon1;
            this.$lInActivityNudgeTimeModel = list;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$lInActivityNudgeTimeModel, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                ug4 a2 = this.this$Anon0.a.c();
                C0135Anon1 anon1 = new C0135Anon1(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.label = 1;
                if (yf4.a(a2, anon1, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return qa4.a;
        }
    }

    @DexIgnore
    public NotificationWatchRemindersPresenter$start$Anon1(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
        this.a = notificationWatchRemindersPresenter;
    }

    @DexIgnore
    public final void a(List<InactivityNudgeTimeModel> list) {
        if (list == null || list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            InactivityNudgeTimeModel inactivityNudgeTimeModel = new InactivityNudgeTimeModel("Start", 660, 0);
            InactivityNudgeTimeModel inactivityNudgeTimeModel2 = new InactivityNudgeTimeModel("End", 1260, 1);
            arrayList.add(inactivityNudgeTimeModel);
            arrayList.add(inactivityNudgeTimeModel2);
            fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, arrayList, (yb4) null), 3, (Object) null);
            return;
        }
        for (InactivityNudgeTimeModel inactivityNudgeTimeModel3 : list) {
            if (inactivityNudgeTimeModel3.getNudgeTimeType() == 0) {
                by2 h = this.a.p;
                SpannableString e = nl2.e(inactivityNudgeTimeModel3.getMinutes());
                kd4.a((Object) e, "TimeUtils.getTimeSpannab\u2026tyNudgeTimeModel.minutes)");
                h.c(e);
            } else {
                by2 h2 = this.a.p;
                SpannableString e2 = nl2.e(inactivityNudgeTimeModel3.getMinutes());
                kd4.a((Object) e2, "TimeUtils.getTimeSpannab\u2026tyNudgeTimeModel.minutes)");
                h2.d(e2);
            }
        }
    }
}
