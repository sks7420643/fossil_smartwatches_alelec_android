package com.portfolio.platform.uirenew.home.profile.opt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1", mo27670f = "ProfileOptInPresenter.kt", mo27671l = {31}, mo27672m = "invokeSuspend")
public final class ProfileOptInPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23918p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1$1", mo27670f = "ProfileOptInPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1$1 */
    public static final class C67761 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.MFUser>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23919p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C67761(com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1 profileOptInPresenter$start$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = profileOptInPresenter$start$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1.C67761 r0 = new com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1.C67761(this.this$0, yb4);
            r0.f23919p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1.C67761) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return this.this$0.this$0.mo41534h().getCurrentUser();
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileOptInPresenter$start$1(com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter profileOptInPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = profileOptInPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1 profileOptInPresenter$start$1 = new com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1(this.this$0, yb4);
        profileOptInPresenter$start$1.f23918p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return profileOptInPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter profileOptInPresenter;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23918p$;
            com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter profileOptInPresenter2 = this.this$0;
            com.fossil.blesdk.obfuscated.ug4 a2 = profileOptInPresenter2.mo31441c();
            com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1.C67761 r4 = new com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1.C67761(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = profileOptInPresenter2;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r4, this);
            if (obj == a) {
                return a;
            }
            profileOptInPresenter = profileOptInPresenter2;
        } else if (i == 1) {
            profileOptInPresenter = (com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        profileOptInPresenter.f23910g = (com.portfolio.platform.data.model.MFUser) obj;
        com.portfolio.platform.data.model.MFUser b = this.this$0.f23910g;
        if (b != null) {
            this.this$0.mo41535i().mo32834E(b.isDiagnosticEnabled());
            this.this$0.mo41535i().mo32836H(b.isEmailOptIn());
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
