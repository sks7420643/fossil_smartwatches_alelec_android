package com.portfolio.platform.uirenew.home.profile.goal;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qo2<MFUser>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MFUser $it;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(MFUser mFUser, yb4 yb4, ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1 profileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1) {
        super(2, yb4);
        this.$it = mFUser;
        this.this$Anon0 = profileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 profileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(this.$it, yb4, this.this$Anon0);
        profileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$invokeSuspend$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return profileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$invokeSuspend$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$invokeSuspend$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            UserRepository h = this.this$Anon0.this$Anon0.t;
            MFUser mFUser = this.$it;
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = h.updateUser(mFUser, true, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
