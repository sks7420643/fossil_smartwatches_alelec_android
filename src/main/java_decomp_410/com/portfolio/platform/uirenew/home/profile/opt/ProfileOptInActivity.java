package com.portfolio.platform.uirenew.home.profile.opt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.aj3;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.zi3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ProfileOptInActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public ProfileOptInPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            kd4.b(context, "context");
            Intent intent = new Intent(context, ProfileOptInActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        zi3 zi3 = (zi3) getSupportFragmentManager().a((int) R.id.content);
        if (zi3 == null) {
            zi3 = zi3.m.a();
            a((Fragment) zi3, (int) R.id.content);
        }
        PortfolioApp.W.c().g().a(new aj3(zi3)).a(this);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        a(false);
    }
}
