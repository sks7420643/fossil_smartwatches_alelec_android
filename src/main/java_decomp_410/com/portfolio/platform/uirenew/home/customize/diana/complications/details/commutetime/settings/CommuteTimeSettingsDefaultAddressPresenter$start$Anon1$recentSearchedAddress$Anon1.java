package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1", f = "CommuteTimeSettingsDefaultAddressPresenter.kt", l = {}, m = "invokeSuspend")
public final class CommuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<String>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter$start$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1(CommuteTimeSettingsDefaultAddressPresenter$start$Anon1 commuteTimeSettingsDefaultAddressPresenter$start$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = commuteTimeSettingsDefaultAddressPresenter$start$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CommuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1 commuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1 = new CommuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1(this.this$Anon0, yb4);
        commuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1.p$ = (zg4) obj;
        return commuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CommuteTimeSettingsDefaultAddressPresenter$start$Anon1$recentSearchedAddress$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return this.this$Anon0.this$Anon0.h().c();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
