package com.portfolio.platform.uirenew.home.profile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeProfilePresenter$updateUser$1 implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6199d, com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6198c> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter f23818a;

    @DexIgnore
    public HomeProfilePresenter$updateUser$1(com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter homeProfilePresenter) {
        this.f23818a = homeProfilePresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6199d dVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter.f23773C.mo41453a(), ".Inside updateUser onSuccess");
        if (this.f23818a.mo41449l().isActive()) {
            this.f23818a.mo41449l().mo27223d();
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23818a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$updateUser$1$onSuccess$1(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo29641a(com.portfolio.platform.p007ui.user.information.domain.usecase.UpdateUser.C6198c cVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(cVar, "errorValue");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a = com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter.f23773C.mo41453a();
        local.mo33255d(a, ".Inside updateUser onError errorCode=" + cVar.mo40399a());
        if (this.f23818a.mo41449l().isActive()) {
            this.f23818a.mo41449l().mo27223d();
            this.f23818a.mo41449l().mo27215a(cVar.mo40399a(), "");
        }
    }
}
