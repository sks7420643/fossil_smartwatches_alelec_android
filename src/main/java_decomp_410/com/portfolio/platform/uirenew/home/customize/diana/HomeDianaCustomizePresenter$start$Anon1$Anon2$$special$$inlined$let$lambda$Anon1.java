package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.dl4;
import com.fossil.blesdk.obfuscated.f13;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$Anon1;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $it;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter$start$Anon1.Anon2 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super MFUser>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1 homeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = homeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return this.this$Anon0.this$Anon0.a.this$Anon0.A.getCurrentUser();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super List<? extends f13>>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public Object L$Anon1;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1 homeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = homeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, yb4);
            anon2.p$ = (zg4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            dl4 dl4;
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                dl4 l = this.this$Anon0.this$Anon0.a.this$Anon0.q;
                this.L$Anon0 = zg4;
                this.L$Anon1 = l;
                this.label = 1;
                if (l.a((Object) null, this) == a) {
                    return a;
                }
                dl4 = l;
            } else if (i == 1) {
                dl4 = (dl4) this.L$Anon1;
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            try {
                CopyOnWriteArrayList<DianaPreset> m = this.this$Anon0.this$Anon0.a.this$Anon0.j;
                ArrayList arrayList = new ArrayList(db4.a(m, 10));
                for (DianaPreset dianaPreset : m) {
                    HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.this$Anon0.this$Anon0.a.this$Anon0;
                    kd4.a((Object) dianaPreset, "it");
                    arrayList.add(homeDianaCustomizePresenter.a(dianaPreset));
                }
                return arrayList;
            } finally {
                dl4.a((Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1(List list, yb4 yb4, HomeDianaCustomizePresenter$start$Anon1.Anon2 anon2) {
        super(2, yb4);
        this.$it = list;
        this.this$Anon0 = anon2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1 homeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1 = new HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1(this.$it, yb4, this.this$Anon0);
        homeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return homeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeDianaCustomizePresenter$start$Anon1$Anon2$$special$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            ug4 c = this.this$Anon0.a.this$Anon0.b();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(c, anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            MFUser mFUser = (MFUser) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            this.this$Anon0.a.this$Anon0.t.c((List<f13>) (List) obj);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser2 = (MFUser) obj;
        if ((!kd4.a((Object) this.this$Anon0.a.this$Anon0.n, (Object) this.$it)) || (!kd4.a((Object) this.this$Anon0.a.this$Anon0.p, (Object) mFUser2))) {
            this.this$Anon0.a.this$Anon0.p = mFUser2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizePresenter", "on real data change " + this.$it);
            this.this$Anon0.a.this$Anon0.n.clear();
            this.this$Anon0.a.this$Anon0.n.addAll(this.$it);
            if (true ^ this.this$Anon0.a.this$Anon0.j.isEmpty()) {
                ug4 c2 = this.this$Anon0.a.this$Anon0.b();
                Anon2 anon2 = new Anon2(this, (yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = mFUser2;
                this.label = 2;
                obj = yf4.a(c2, anon2, this);
                if (obj == a) {
                    return a;
                }
                this.this$Anon0.a.this$Anon0.t.c((List<f13>) (List) obj);
            }
        }
        return qa4.a;
    }
}
