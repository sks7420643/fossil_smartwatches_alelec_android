package com.portfolio.platform.uirenew.home.profile.goal;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.MFUser;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1", f = "ProfileGoalEditPresenter.kt", l = {119, 124}, m = "invokeSuspend")
public final class ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileGoalEditPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1(ProfileGoalEditPresenter profileGoalEditPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = profileGoalEditPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1 profileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1 = new ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1(this.this$Anon0, yb4);
        profileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1.p$ = (zg4) obj;
        return profileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            ug4 b = this.this$Anon0.c();
            ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$currentUser$Anon1 profileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$currentUser$Anon1 = new ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$currentUser$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(b, profileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$currentUser$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            MFUser mFUser = (MFUser) this.L$Anon2;
            MFUser mFUser2 = (MFUser) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFUser mFUser3 = (MFUser) obj;
        if (mFUser3 != null && mFUser3.isUseDefaultGoals()) {
            mFUser3.setUseDefaultGoals(false);
            ug4 b2 = this.this$Anon0.c();
            ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 profileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new ProfileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(mFUser3, (yb4) null, this);
            this.L$Anon0 = zg4;
            this.L$Anon1 = mFUser3;
            this.L$Anon2 = mFUser3;
            this.label = 2;
            if (yf4.a(b2, profileGoalEditPresenter$checkUserUsingDefaultGoal$Anon1$invokeSuspend$$inlined$let$lambda$Anon1, this) == a) {
                return a;
            }
        }
        return qa4.a;
    }
}
