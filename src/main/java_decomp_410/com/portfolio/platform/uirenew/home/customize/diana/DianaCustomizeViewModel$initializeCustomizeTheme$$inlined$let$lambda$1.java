package com.portfolio.platform.uirenew.home.customize.diana;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.yb4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.diana.preset.DianaPreset $currentPreset$inlined;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$1(com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel dianaCustomizeViewModel, com.portfolio.platform.data.model.diana.preset.DianaPreset dianaPreset, com.fossil.blesdk.obfuscated.yb4 yb42) {
        super(2, yb4);
        this.this$0 = dianaCustomizeViewModel;
        this.$currentPreset$inlined = dianaPreset;
        this.$continuation$inlined = yb42;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$1 dianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$1(yb4, this.this$0, this.$currentPreset$inlined, this.$continuation$inlined);
        dianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$initializeCustomizeTheme$$inlined$let$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            androidx.lifecycle.LiveData m = this.this$0.p;
            if (m == null) {
                return null;
            }
            m.a(this.this$0.y);
            return com.fossil.blesdk.obfuscated.qa4.a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
