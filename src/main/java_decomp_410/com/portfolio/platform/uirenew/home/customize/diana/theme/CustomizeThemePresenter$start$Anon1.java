package com.portfolio.platform.uirenew.home.customize.diana.theme;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.j43;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.tj2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$Anon1", f = "CustomizeThemePresenter.kt", l = {53}, m = "invokeSuspend")
public final class CustomizeThemePresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CustomizeThemePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$Anon1$Anon2", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemePresenter$start$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1<T> implements cc<List<? extends WatchFace>> {
            @DexIgnore
            public /* final */ /* synthetic */ Anon2 a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$Anon1$Anon2$Anon1$Anon1")
            @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$Anon1$Anon2$Anon1$Anon1", f = "CustomizeThemePresenter.kt", l = {60}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$Anon1$Anon2$Anon1$Anon1  reason: collision with other inner class name */
            public static final class C0141Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $it;
                @DexIgnore
                public Object L$Anon0;
                @DexIgnore
                public Object L$Anon1;
                @DexIgnore
                public int label;
                @DexIgnore
                public zg4 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Anon1 this$Anon0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$Anon1$Anon2$Anon1$Anon1$Anon1")
                @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$Anon1$Anon2$Anon1$Anon1$Anon1", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
                /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$Anon1$Anon2$Anon1$Anon1$Anon1  reason: collision with other inner class name */
                public static final class C0142Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ List $watchFaceWrappers;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public zg4 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0141Anon1 this$Anon0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0142Anon1(C0141Anon1 anon1, List list, yb4 yb4) {
                        super(2, yb4);
                        this.this$Anon0 = anon1;
                        this.$watchFaceWrappers = list;
                    }

                    @DexIgnore
                    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                        kd4.b(yb4, "completion");
                        C0142Anon1 anon1 = new C0142Anon1(this.this$Anon0, this.$watchFaceWrappers, yb4);
                        anon1.p$ = (zg4) obj;
                        return anon1;
                    }

                    @DexIgnore
                    public final Object invoke(Object obj, Object obj2) {
                        return ((C0142Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
                    }

                    @DexIgnore
                    public final Object invokeSuspend(Object obj) {
                        cc4.a();
                        if (this.label == 0) {
                            na4.a(obj);
                            this.this$Anon0.this$Anon0.a.this$Anon0.this$Anon0.i.n((List<WatchFaceWrapper>) this.$watchFaceWrappers);
                            return qa4.a;
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0141Anon1(Anon1 anon1, List list, yb4 yb4) {
                    super(2, yb4);
                    this.this$Anon0 = anon1;
                    this.$it = list;
                }

                @DexIgnore
                public final yb4<qa4> create(Object obj, yb4<?> yb4) {
                    kd4.b(yb4, "completion");
                    C0141Anon1 anon1 = new C0141Anon1(this.this$Anon0, this.$it, yb4);
                    anon1.p$ = (zg4) obj;
                    return anon1;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0141Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    Object a = cc4.a();
                    int i = this.label;
                    if (i == 0) {
                        na4.a(obj);
                        zg4 zg4 = this.p$;
                        if (!kd4.a((Object) this.this$Anon0.a.this$Anon0.this$Anon0.h, (Object) this.$it)) {
                            CustomizeThemePresenter customizeThemePresenter = this.this$Anon0.a.this$Anon0.this$Anon0;
                            List list = this.$it;
                            kd4.a((Object) list, "it");
                            customizeThemePresenter.h = list;
                            List list2 = this.$it;
                            kd4.a((Object) list2, "it");
                            DianaPreset a2 = CustomizeThemePresenter.b(this.this$Anon0.a.this$Anon0.this$Anon0).c().a();
                            List<WatchFaceWrapper> a3 = tj2.a((List<WatchFace>) list2, (List<DianaPresetComplicationSetting>) a2 != null ? a2.getComplications() : null);
                            pi4 c = nh4.c();
                            C0142Anon1 anon1 = new C0142Anon1(this, a3, (yb4) null);
                            this.L$Anon0 = zg4;
                            this.L$Anon1 = a3;
                            this.label = 1;
                            if (yf4.a(c, anon1, this) == a) {
                                return a;
                            }
                        }
                    } else if (i == 1) {
                        List list3 = (List) this.L$Anon1;
                        zg4 zg42 = (zg4) this.L$Anon0;
                        na4.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return qa4.a;
                }
            }

            @DexIgnore
            public Anon1(Anon2 anon2) {
                this.a = anon2;
            }

            @DexIgnore
            public final void a(List<WatchFace> list) {
                fi4 unused = ag4.b(this.a.this$Anon0.this$Anon0.e(), nh4.a(), (CoroutineStart) null, new C0141Anon1(this, list, (yb4) null), 2, (Object) null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(CustomizeThemePresenter$start$Anon1 customizeThemePresenter$start$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = customizeThemePresenter$start$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, yb4);
            anon2.p$ = (zg4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                LiveData e = this.this$Anon0.this$Anon0.g;
                if (e == null) {
                    return null;
                }
                j43 c = this.this$Anon0.this$Anon0.i;
                if (c != null) {
                    e.a((CustomizeThemeFragment) c, new Anon1(this));
                    return qa4.a;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment");
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomizeThemePresenter$start$Anon1(CustomizeThemePresenter customizeThemePresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = customizeThemePresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CustomizeThemePresenter$start$Anon1 customizeThemePresenter$start$Anon1 = new CustomizeThemePresenter$start$Anon1(this.this$Anon0, yb4);
        customizeThemePresenter$start$Anon1.p$ = (zg4) obj;
        return customizeThemePresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CustomizeThemePresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            if (this.this$Anon0.g == null) {
                CustomizeThemePresenter customizeThemePresenter = this.this$Anon0;
                customizeThemePresenter.g = customizeThemePresenter.j.getWatchFacesLiveDataWithSerial(PortfolioApp.W.c().e());
                qa4 qa4 = qa4.a;
            }
            pi4 c = nh4.c();
            Anon2 anon2 = new Anon2(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            if (yf4.a(c, anon2, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
