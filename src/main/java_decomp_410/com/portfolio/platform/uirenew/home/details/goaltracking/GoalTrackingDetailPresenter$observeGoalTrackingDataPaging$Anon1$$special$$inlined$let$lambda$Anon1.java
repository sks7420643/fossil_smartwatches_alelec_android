package com.portfolio.platform.uirenew.home.details.goaltracking;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dl4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1$$special$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ qd $dataList;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1$$special$$inlined$let$lambda$Anon1(qd qdVar, yb4 yb4, GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1 goalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1) {
        super(2, yb4);
        this.$dataList = qdVar;
        this.this$Anon0 = goalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1$$special$$inlined$let$lambda$Anon1 goalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1$$special$$inlined$let$lambda$Anon1 = new GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1$$special$$inlined$let$lambda$Anon1(this.$dataList, yb4, this.this$Anon0);
        goalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1$$special$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return goalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1$$special$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$Anon1$$special$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final Object invokeSuspend(Object obj) {
        dl4 dl4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            dl4 h = this.this$Anon0.a.k;
            this.L$Anon0 = zg4;
            this.L$Anon1 = h;
            this.label = 1;
            if (h.a((Object) null, this) == a) {
                return a;
            }
            dl4 = h;
        } else if (i == 1) {
            dl4 = (dl4) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            if (this.this$Anon0.a.n == null || (!kd4.a((Object) this.this$Anon0.a.n, (Object) kb4.d(this.$dataList)))) {
                this.this$Anon0.a.n = kb4.d(this.$dataList);
            }
            this.this$Anon0.a.q.c(this.$dataList);
            if (this.this$Anon0.a.i && this.this$Anon0.a.j) {
                fi4 unused = this.this$Anon0.a.m();
            }
            qa4 qa4 = qa4.a;
            dl4.a((Object) null);
            return qa4.a;
        } catch (Throwable th) {
            dl4.a((Object) null);
            throw th;
        }
    }
}
