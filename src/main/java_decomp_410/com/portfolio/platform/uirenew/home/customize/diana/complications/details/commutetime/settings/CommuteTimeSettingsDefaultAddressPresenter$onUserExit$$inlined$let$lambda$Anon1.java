package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$BooleanRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $address$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$BooleanRef $isAddressSaved$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$BooleanRef $isChanged$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ String $it;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.this$Anon0.h().a((List<String>) this.this$Anon0.this$Anon0.k);
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super MFUser>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, yb4);
            anon2.p$ = (zg4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return this.this$Anon0.this$Anon0.i().getCurrentUser();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3 extends SuspendLambda implements yc4<zg4, yb4<? super qo2<MFUser>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3(MFUser mFUser, yb4 yb4, CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1) {
            super(2, yb4);
            this.$user = mFUser;
            this.this$Anon0 = commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon3 anon3 = new Anon3(this.$user, yb4, this.this$Anon0);
            anon3.p$ = (zg4) obj;
            return anon3;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon3) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                UserRepository i2 = this.this$Anon0.this$Anon0.i();
                MFUser mFUser = this.$user;
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = i2.updateUser(mFUser, true, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon4 extends SuspendLambda implements yc4<zg4, yb4<? super qo2<MFUser>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user;
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon4(MFUser mFUser, yb4 yb4, CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1) {
            super(2, yb4);
            this.$user = mFUser;
            this.this$Anon0 = commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon4 anon4 = new Anon4(this.$user, yb4, this.this$Anon0);
            anon4.p$ = (zg4) obj;
            return anon4;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon4) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                UserRepository i2 = this.this$Anon0.this$Anon0.i();
                MFUser mFUser = this.$user;
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = i2.updateUser(mFUser, false, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1(String str, yb4 yb4, CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter, String str2, Ref$BooleanRef ref$BooleanRef, Ref$BooleanRef ref$BooleanRef2) {
        super(2, yb4);
        this.$it = str;
        this.this$Anon0 = commuteTimeSettingsDefaultAddressPresenter;
        this.$address$inlined = str2;
        this.$isChanged$inlined = ref$BooleanRef;
        this.$isAddressSaved$inlined = ref$BooleanRef2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1 = new CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1(this.$it, yb4, this.this$Anon0, this.$address$inlined, this.$isChanged$inlined, this.$isAddressSaved$inlined);
        commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return commuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CommuteTimeSettingsDefaultAddressPresenter$onUserExit$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00d3 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0190  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01e6  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0215  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x025e  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x026a  */
    public final Object invokeSuspend(Object obj) {
        MFUser mFUser;
        qo2 qo2;
        zg4 zg4;
        MFUser mFUser2;
        MFUser mFUser3;
        qo2 qo22;
        zg4 zg42;
        Object a = cc4.a();
        int i = this.label;
        boolean z = false;
        if (i == 0) {
            na4.a(obj);
            zg42 = this.p$;
            if (!(this.$it.length() == 0) && !this.this$Anon0.k.contains(this.$it)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String l = CommuteTimeSettingsDefaultAddressPresenter.o;
                local.d(l, "add " + this.$address$inlined + " to recent search list");
                this.this$Anon0.k.add(0, this.$it);
                ug4 a2 = this.this$Anon0.c();
                Anon1 anon1 = new Anon1(this, (yb4) null);
                this.L$Anon0 = zg42;
                this.label = 1;
                if (yf4.a(a2, anon1, this) == a) {
                    return a;
                }
            }
            ug4 a3 = this.this$Anon0.c();
            Anon2 anon2 = new Anon2(this, (yb4) null);
            this.L$Anon0 = zg42;
            this.label = 2;
            obj = yf4.a(a3, anon2, this);
            if (obj == a) {
            }
        } else if (i == 1) {
            zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            ug4 a32 = this.this$Anon0.c();
            Anon2 anon22 = new Anon2(this, (yb4) null);
            this.L$Anon0 = zg42;
            this.label = 2;
            obj = yf4.a(a32, anon22, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 2) {
            zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 3) {
            mFUser3 = (MFUser) this.L$Anon2;
            mFUser2 = (MFUser) this.L$Anon1;
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            qo22 = (qo2) obj;
            if (!(qo22 instanceof ro2)) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String l2 = CommuteTimeSettingsDefaultAddressPresenter.o;
                local2.d(l2, "update UserRepository to remote DB success - home = " + mFUser3.getHome() + " - work = " + mFUser3.getWork() + " successfully");
                this.$isAddressSaved$inlined.element = true;
            } else if (qo22 instanceof po2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String l3 = CommuteTimeSettingsDefaultAddressPresenter.o;
                local3.d(l3, "update UserRepository to remote DB success - home = " + mFUser3.getHome() + " - work = " + mFUser3.getWork() + " failed");
                ug4 a4 = this.this$Anon0.c();
                Anon4 anon4 = new Anon4(mFUser3, (yb4) null, this);
                this.L$Anon0 = zg4;
                this.L$Anon1 = mFUser2;
                this.L$Anon2 = mFUser3;
                this.label = 4;
                obj = yf4.a(a4, anon4, this);
                if (obj == a) {
                    return a;
                }
                mFUser = mFUser3;
                qo2 = (qo2) obj;
                Ref$BooleanRef ref$BooleanRef = this.$isAddressSaved$inlined;
                if (!(qo2 instanceof ro2)) {
                }
                ref$BooleanRef.element = z;
            }
            this.this$Anon0.j().a();
            if (this.$isAddressSaved$inlined.element) {
            }
            return qa4.a;
        } else if (i == 4) {
            mFUser = (MFUser) this.L$Anon2;
            MFUser mFUser4 = (MFUser) this.L$Anon1;
            zg4 zg43 = (zg4) this.L$Anon0;
            na4.a(obj);
            qo2 = (qo2) obj;
            Ref$BooleanRef ref$BooleanRef2 = this.$isAddressSaved$inlined;
            if (!(qo2 instanceof ro2)) {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String l4 = CommuteTimeSettingsDefaultAddressPresenter.o;
                local4.d(l4, "update UserRepository to local DB success - home = " + mFUser.getHome() + " - work = " + mFUser.getWork() + " successfully");
                z = true;
            } else if (qo2 instanceof po2) {
                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                String l5 = CommuteTimeSettingsDefaultAddressPresenter.o;
                local5.d(l5, "update UserRepository to local DB success - home = " + mFUser.getHome() + " - work = " + mFUser.getWork() + " failed");
            } else {
                throw new NoWhenBranchMatchedException();
            }
            ref$BooleanRef2.element = z;
            this.this$Anon0.j().a();
            if (this.$isAddressSaved$inlined.element) {
                this.this$Anon0.j().B(this.$address$inlined);
            } else {
                this.this$Anon0.j().B((String) null);
            }
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        zg4 = zg42;
        MFUser mFUser5 = (MFUser) obj;
        if (mFUser5 != null) {
            String b = this.this$Anon0.h;
            if (b != null) {
                int hashCode = b.hashCode();
                if (hashCode != 2255103) {
                    if (hashCode == 76517104 && b.equals("Other")) {
                        this.$isChanged$inlined.element = !TextUtils.equals(mFUser5.getWork(), this.$address$inlined);
                        mFUser5.setWork(this.$address$inlined);
                    }
                } else if (b.equals("Home")) {
                    this.$isChanged$inlined.element = !TextUtils.equals(mFUser5.getHome(), this.$address$inlined);
                    mFUser5.setHome(this.$address$inlined);
                }
            }
            if (this.$isChanged$inlined.element) {
                this.this$Anon0.j().b();
                ug4 a5 = this.this$Anon0.c();
                Anon3 anon3 = new Anon3(mFUser5, (yb4) null, this);
                this.L$Anon0 = zg4;
                this.L$Anon1 = mFUser5;
                this.L$Anon2 = mFUser5;
                this.label = 3;
                Object a6 = yf4.a(a5, anon3, this);
                if (a6 == a) {
                    return a;
                }
                mFUser2 = mFUser5;
                obj = a6;
                mFUser3 = mFUser2;
                qo22 = (qo2) obj;
                if (!(qo22 instanceof ro2)) {
                }
                this.this$Anon0.j().a();
            }
            if (this.$isAddressSaved$inlined.element) {
            }
        }
        return qa4.a;
    }
}
