package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1", f = "WeatherSettingPresenter.kt", l = {126}, m = "invokeSuspend")
public final class WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherSettingPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1(WeatherSettingPresenter weatherSettingPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = weatherSettingPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1 weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1 = new WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1(this.this$Anon0, yb4);
        weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1.p$ = (zg4) obj;
        return weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        WeatherWatchAppSetting weatherWatchAppSetting;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            WeatherWatchAppSetting d = this.this$Anon0.g;
            if (d != null) {
                ug4 a2 = this.this$Anon0.b();
                WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new WeatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1((yb4) null, this);
                this.L$Anon0 = zg4;
                this.L$Anon1 = d;
                this.label = 1;
                obj = yf4.a(a2, weatherSettingPresenter$saveWeatherWatchAppSetting$Anon1$invokeSuspend$$inlined$let$lambda$Anon1, this);
                if (obj == a) {
                    return a;
                }
                weatherWatchAppSetting = d;
            }
            this.this$Anon0.i.a();
            this.this$Anon0.i.r(true);
            return qa4.a;
        } else if (i == 1) {
            weatherWatchAppSetting = (WeatherWatchAppSetting) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        weatherWatchAppSetting.setLocations(kb4.d((List) obj));
        this.this$Anon0.i.a();
        this.this$Anon0.i.r(true);
        return qa4.a;
    }
}
