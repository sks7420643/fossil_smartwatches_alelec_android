package com.portfolio.platform.uirenew.home.details.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDetailPresenter$showDetailChart$1$invokeSuspend$$inlined$withLock$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super kotlin.Pair<? extends java.util.ArrayList<com.portfolio.platform.ui.view.chart.base.BarChart.a>, ? extends java.util.ArrayList<java.lang.String>>>, java.lang.Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDetailPresenter$showDetailChart$1$invokeSuspend$$inlined$withLock$lambda$1(com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1 goalTrackingDetailPresenter$showDetailChart$1) {
        super(2, yb4);
        this.this$0 = goalTrackingDetailPresenter$showDetailChart$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1$invokeSuspend$$inlined$withLock$lambda$1 goalTrackingDetailPresenter$showDetailChart$1$invokeSuspend$$inlined$withLock$lambda$1 = new com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1$invokeSuspend$$inlined$withLock$lambda$1(yb4, this.this$0);
        goalTrackingDetailPresenter$showDetailChart$1$invokeSuspend$$inlined$withLock$lambda$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return goalTrackingDetailPresenter$showDetailChart$1$invokeSuspend$$inlined$withLock$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1$invokeSuspend$$inlined$withLock$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            return com.fossil.blesdk.obfuscated.eg3.a.a(this.this$0.this$0.g, this.this$0.this$0.n);
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
