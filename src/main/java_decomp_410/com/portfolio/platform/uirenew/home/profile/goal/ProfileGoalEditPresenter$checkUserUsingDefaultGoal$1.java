package com.portfolio.platform.uirenew.home.profile.goal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$checkUserUsingDefaultGoal$1", mo27670f = "ProfileGoalEditPresenter.kt", mo27671l = {119, 124}, mo27672m = "invokeSuspend")
public final class ProfileGoalEditPresenter$checkUserUsingDefaultGoal$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23872p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileGoalEditPresenter$checkUserUsingDefaultGoal$1(com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter profileGoalEditPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = profileGoalEditPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$checkUserUsingDefaultGoal$1 profileGoalEditPresenter$checkUserUsingDefaultGoal$1 = new com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$checkUserUsingDefaultGoal$1(this.this$0, yb4);
        profileGoalEditPresenter$checkUserUsingDefaultGoal$1.f23872p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return profileGoalEditPresenter$checkUserUsingDefaultGoal$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$checkUserUsingDefaultGoal$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f23872p$;
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.profile.goal.C6763xb0bb8a3b profileGoalEditPresenter$checkUserUsingDefaultGoal$1$currentUser$1 = new com.portfolio.platform.uirenew.home.profile.goal.C6763xb0bb8a3b(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b, profileGoalEditPresenter$checkUserUsingDefaultGoal$1$currentUser$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) this.L$2;
            com.portfolio.platform.data.model.MFUser mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.model.MFUser mFUser3 = (com.portfolio.platform.data.model.MFUser) obj;
        if (mFUser3 != null && mFUser3.isUseDefaultGoals()) {
            mFUser3.setUseDefaultGoals(false);
            com.fossil.blesdk.obfuscated.ug4 b2 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.profile.goal.C6762x30490a84 profileGoalEditPresenter$checkUserUsingDefaultGoal$1$invokeSuspend$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.profile.goal.C6762x30490a84(mFUser3, (com.fossil.blesdk.obfuscated.yb4) null, this);
            this.L$0 = zg4;
            this.L$1 = mFUser3;
            this.L$2 = mFUser3;
            this.label = 2;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(b2, profileGoalEditPresenter$checkUserUsingDefaultGoal$1$invokeSuspend$$inlined$let$lambda$1, this) == a) {
                return a;
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
