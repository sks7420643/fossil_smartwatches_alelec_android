package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$saveWeatherWatchAppSetting$1$invokeSuspend$$inlined$let$lambda$1 */
public final class C6501xdb6c7842 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<? extends com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper>>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22898p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$saveWeatherWatchAppSetting$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6501xdb6c7842(com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$saveWeatherWatchAppSetting$1 weatherSettingPresenter$saveWeatherWatchAppSetting$1) {
        super(2, yb4);
        this.this$0 = weatherSettingPresenter$saveWeatherWatchAppSetting$1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6501xdb6c7842 weatherSettingPresenter$saveWeatherWatchAppSetting$1$invokeSuspend$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6501xdb6c7842(yb4, this.this$0);
        weatherSettingPresenter$saveWeatherWatchAppSetting$1$invokeSuspend$$inlined$let$lambda$1.f22898p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return weatherSettingPresenter$saveWeatherWatchAppSetting$1$invokeSuspend$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.C6501xdb6c7842) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.util.List b = this.this$0.this$0.f22893h;
            java.util.ArrayList arrayList = new java.util.ArrayList();
            for (java.lang.Object next : b) {
                if (com.fossil.blesdk.obfuscated.dc4.m20839a(((com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper) next).isEnableLocation()).booleanValue()) {
                    arrayList.add(next);
                }
            }
            return arrayList;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
