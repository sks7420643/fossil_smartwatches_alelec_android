package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ts3;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.google.android.gms.maps.model.LatLng;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ LatLng $it;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherSettingPresenter$addLocation$Anon1 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1 weatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = weatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.this$Anon0.a.i.o(this.this$Anon0.this$Anon0.a.h);
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1(LatLng latLng, yb4 yb4, WeatherSettingPresenter$addLocation$Anon1 weatherSettingPresenter$addLocation$Anon1) {
        super(2, yb4);
        this.$it = latLng;
        this.this$Anon0 = weatherSettingPresenter$addLocation$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1 weatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1 = new WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1(this.$it, yb4, this.this$Anon0);
        weatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return weatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            String a2 = ts3.a(this.this$Anon0.b);
            List b = this.this$Anon0.a.h;
            String str = this.this$Anon0.c;
            LatLng latLng = this.$it;
            double d = latLng.e;
            double d2 = latLng.f;
            kd4.a((Object) a2, "name");
            WeatherLocationWrapper weatherLocationWrapper = r5;
            WeatherLocationWrapper weatherLocationWrapper2 = new WeatherLocationWrapper(str, d, d2, a2, this.this$Anon0.b, false, true, 32, (fd4) null);
            b.add(weatherLocationWrapper);
            pi4 c = nh4.c();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = a2;
            this.label = 1;
            if (yf4.a(c, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            String str2 = (String) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
