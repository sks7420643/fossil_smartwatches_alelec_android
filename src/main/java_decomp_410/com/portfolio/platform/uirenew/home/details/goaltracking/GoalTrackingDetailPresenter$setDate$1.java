package com.portfolio.platform.uirenew.home.details.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1", mo27670f = "GoalTrackingDetailPresenter.kt", mo27671l = {144, 262, 169}, mo27672m = "invokeSuspend")
public final class GoalTrackingDetailPresenter$setDate$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $date;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23692p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1$1", mo27670f = "GoalTrackingDetailPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1$1 */
    public static final class C67161 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.Date>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23693p$;

        @DexIgnore
        public C67161(com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1.C67161 r0 = new com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1.C67161(yb4);
            r0.f23693p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1.C67161) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34546k();
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDetailPresenter$setDate$1(com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter goalTrackingDetailPresenter, java.util.Date date, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = goalTrackingDetailPresenter;
        this.$date = date;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1 goalTrackingDetailPresenter$setDate$1 = new com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1(this.this$0, this.$date, yb4);
        goalTrackingDetailPresenter$setDate$1.f23692p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return goalTrackingDetailPresenter$setDate$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v24, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v30, resolved type: com.fossil.blesdk.obfuscated.dl4} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x01a0 A[Catch:{ all -> 0x01eb }, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01b1 A[Catch:{ all -> 0x01eb }] */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.dl4 dl4;
        java.lang.Object obj2;
        com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary goalTrackingSummary;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        boolean z;
        java.lang.Boolean bool;
        kotlin.Pair pair;
        android.util.Pair<java.util.Date, java.util.Date> pair2;
        com.fossil.blesdk.obfuscated.zg4 zg42;
        kotlin.Pair pair3;
        java.lang.Object obj3;
        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter goalTrackingDetailPresenter;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg42 = this.f23692p$;
            if (this.this$0.f23667f == null) {
                goalTrackingDetailPresenter = this.this$0;
                com.fossil.blesdk.obfuscated.ug4 a2 = goalTrackingDetailPresenter.mo31440b();
                com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1.C67161 r9 = new com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1.C67161((com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg42;
                this.L$1 = goalTrackingDetailPresenter;
                this.label = 1;
                obj3 = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r9, this);
                if (obj3 == a) {
                    return a;
                }
            }
            zg4 = zg42;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("GoalTrackingDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.f23667f);
            this.this$0.f23668g = this.$date;
            z = com.fossil.blesdk.obfuscated.rk2.m27391c(this.this$0.f23667f, this.$date);
            bool = com.fossil.blesdk.obfuscated.rk2.m27414s(this.$date);
            com.fossil.blesdk.obfuscated.ef3 m = this.this$0.f23678q;
            java.util.Date date = this.$date;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) bool, "isToday");
            m.mo26883a(date, z, bool.booleanValue(), !com.fossil.blesdk.obfuscated.rk2.m27391c(new java.util.Date(), this.$date));
            pair2 = com.fossil.blesdk.obfuscated.rk2.m27355a(this.$date, this.this$0.f23667f);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) pair2, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            pair3 = (kotlin.Pair) this.this$0.f23669h.mo2275a();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local2.mo33255d("GoalTrackingDetailPresenter", "setDate - rangeDateValue=" + pair3 + ", newRange=" + new kotlin.Pair(pair2.first, pair2.second));
            if (pair3 != null || !com.fossil.blesdk.obfuscated.rk2.m27396d((java.util.Date) pair3.getFirst(), (java.util.Date) pair2.first) || !com.fossil.blesdk.obfuscated.rk2.m27396d((java.util.Date) pair3.getSecond(), (java.util.Date) pair2.second)) {
                this.this$0.f23670i = false;
                this.this$0.f23671j = false;
                com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter goalTrackingDetailPresenter2 = this.this$0;
                goalTrackingDetailPresenter2.mo41408d(goalTrackingDetailPresenter2.f23668g);
                this.this$0.f23669h.mo2280a(new kotlin.Pair(pair2.first, pair2.second));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            dl4 = this.this$0.f23672k;
            this.L$0 = zg4;
            this.Z$0 = z;
            this.L$1 = bool;
            this.L$2 = pair2;
            this.L$3 = pair3;
            this.L$4 = dl4;
            this.label = 2;
            if (dl4.mo26535a((java.lang.Object) null, this) == a) {
                return a;
            }
            pair = pair3;
            com.fossil.blesdk.obfuscated.ug4 a3 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.home.details.goaltracking.C6712x243e8dc8 goalTrackingDetailPresenter$setDate$1$invokeSuspend$$inlined$withLock$lambda$1 = new com.portfolio.platform.uirenew.home.details.goaltracking.C6712x243e8dc8((com.fossil.blesdk.obfuscated.yb4) null, this);
            this.L$0 = zg4;
            this.Z$0 = z;
            this.L$1 = bool;
            this.L$2 = pair2;
            this.L$3 = pair;
            this.L$4 = dl4;
            this.label = 3;
            obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(a3, goalTrackingDetailPresenter$setDate$1$invokeSuspend$$inlined$withLock$lambda$1, this);
            if (obj2 == a) {
            }
            goalTrackingSummary = (com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary) obj2;
            if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23674m, (java.lang.Object) goalTrackingSummary)) {
            }
            this.this$0.f23678q.mo26882a(this.this$0.f23674m);
            this.this$0.mo41408d(this.this$0.f23668g);
            com.fossil.blesdk.obfuscated.fi4 unused = this.this$0.mo41410m();
            com.fossil.blesdk.obfuscated.qa4 qa4 = com.fossil.blesdk.obfuscated.qa4.f17909a;
            dl4.mo26536a((java.lang.Object) null);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            goalTrackingDetailPresenter = (com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter) this.L$1;
            zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            obj3 = obj;
        } else if (i == 2) {
            pair = (kotlin.Pair) this.L$3;
            z = this.Z$0;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            dl4 = (com.fossil.blesdk.obfuscated.dl4) this.L$4;
            pair2 = (android.util.Pair) this.L$2;
            bool = (java.lang.Boolean) this.L$1;
            try {
                com.fossil.blesdk.obfuscated.ug4 a32 = this.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.details.goaltracking.C6712x243e8dc8 goalTrackingDetailPresenter$setDate$1$invokeSuspend$$inlined$withLock$lambda$12 = new com.portfolio.platform.uirenew.home.details.goaltracking.C6712x243e8dc8((com.fossil.blesdk.obfuscated.yb4) null, this);
                this.L$0 = zg4;
                this.Z$0 = z;
                this.L$1 = bool;
                this.L$2 = pair2;
                this.L$3 = pair;
                this.L$4 = dl4;
                this.label = 3;
                obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(a32, goalTrackingDetailPresenter$setDate$1$invokeSuspend$$inlined$withLock$lambda$12, this);
                if (obj2 == a) {
                    return a;
                }
                goalTrackingSummary = (com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary) obj2;
                if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23674m, (java.lang.Object) goalTrackingSummary)) {
                }
                this.this$0.f23678q.mo26882a(this.this$0.f23674m);
                this.this$0.mo41408d(this.this$0.f23668g);
                com.fossil.blesdk.obfuscated.fi4 unused2 = this.this$0.mo41410m();
                com.fossil.blesdk.obfuscated.qa4 qa42 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                dl4.mo26536a((java.lang.Object) null);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } catch (Throwable th) {
                th = th;
                dl4.mo26536a((java.lang.Object) null);
                throw th;
            }
        } else if (i == 3) {
            com.fossil.blesdk.obfuscated.dl4 dl42 = this.L$4;
            kotlin.Pair pair4 = (kotlin.Pair) this.L$3;
            android.util.Pair pair5 = (android.util.Pair) this.L$2;
            java.lang.Boolean bool2 = (java.lang.Boolean) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            try {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                dl4 = dl42;
                obj2 = obj;
                goalTrackingSummary = (com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary) obj2;
                if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23674m, (java.lang.Object) goalTrackingSummary)) {
                    this.this$0.f23674m = goalTrackingSummary;
                }
                this.this$0.f23678q.mo26882a(this.this$0.f23674m);
                this.this$0.mo41408d(this.this$0.f23668g);
                if (this.this$0.f23670i && this.this$0.f23671j) {
                    com.fossil.blesdk.obfuscated.fi4 unused3 = this.this$0.mo41410m();
                }
                com.fossil.blesdk.obfuscated.qa4 qa422 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                dl4.mo26536a((java.lang.Object) null);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } catch (Throwable th2) {
                th = th2;
                dl4 = dl42;
                dl4.mo26536a((java.lang.Object) null);
                throw th;
            }
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        goalTrackingDetailPresenter.f23667f = (java.util.Date) obj3;
        zg4 = zg42;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local3.mo33255d("GoalTrackingDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.f23667f);
        this.this$0.f23668g = this.$date;
        z = com.fossil.blesdk.obfuscated.rk2.m27391c(this.this$0.f23667f, this.$date);
        bool = com.fossil.blesdk.obfuscated.rk2.m27414s(this.$date);
        com.fossil.blesdk.obfuscated.ef3 m2 = this.this$0.f23678q;
        java.util.Date date2 = this.$date;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) bool, "isToday");
        m2.mo26883a(date2, z, bool.booleanValue(), !com.fossil.blesdk.obfuscated.rk2.m27391c(new java.util.Date(), this.$date));
        pair2 = com.fossil.blesdk.obfuscated.rk2.m27355a(this.$date, this.this$0.f23667f);
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) pair2, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
        pair3 = (kotlin.Pair) this.this$0.f23669h.mo2275a();
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local22 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local22.mo33255d("GoalTrackingDetailPresenter", "setDate - rangeDateValue=" + pair3 + ", newRange=" + new kotlin.Pair(pair2.first, pair2.second));
        if (pair3 != null) {
        }
        this.this$0.f23670i = false;
        this.this$0.f23671j = false;
        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter goalTrackingDetailPresenter22 = this.this$0;
        goalTrackingDetailPresenter22.mo41408d(goalTrackingDetailPresenter22.f23668g);
        this.this$0.f23669h.mo2280a(new kotlin.Pair(pair2.first, pair2.second));
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
