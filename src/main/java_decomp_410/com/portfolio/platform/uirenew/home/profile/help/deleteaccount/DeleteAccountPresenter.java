package com.portfolio.platform.uirenew.home.profile.help.deleteaccount;

import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.di3;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kr2;
import com.fossil.blesdk.obfuscated.pi3;
import com.fossil.blesdk.obfuscated.qi3;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import java.lang.ref.WeakReference;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeleteAccountPresenter extends pi3 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a((fd4) null);
    @DexIgnore
    public /* final */ qi3 f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ AnalyticsHelper h;
    @DexIgnore
    public /* final */ kr2 i;
    @DexIgnore
    public /* final */ DeleteLogoutUserUseCase j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DeleteAccountPresenter.k;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<DeleteLogoutUserUseCase.d, DeleteLogoutUserUseCase.c> {
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAccountPresenter a;

        @DexIgnore
        public b(DeleteAccountPresenter deleteAccountPresenter) {
            this.a = deleteAccountPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(DeleteLogoutUserUseCase.d dVar) {
            kd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.l.a(), "deleteUser - onSuccess");
            this.a.f.d();
            if (((di3) this.a.f).isActive()) {
                this.a.f.G0();
            }
        }

        @DexIgnore
        public void a(DeleteLogoutUserUseCase.c cVar) {
            kd4.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.l.a(), "deleteUser - onError");
            this.a.f.d();
            this.a.f.a(cVar.a(), "");
        }
    }

    /*
    static {
        String simpleName = DeleteAccountPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "DeleteAccountPresenter::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public DeleteAccountPresenter(qi3 qi3, DeviceRepository deviceRepository, AnalyticsHelper analyticsHelper, kr2 kr2, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        kd4.b(qi3, "mView");
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(analyticsHelper, "mAnalyticsHelper");
        kd4.b(kr2, "mGetZendeskInformation");
        kd4.b(deleteLogoutUserUseCase, "mDeleteLogoutUserUseCase");
        this.f = qi3;
        this.g = deviceRepository;
        this.h = analyticsHelper;
        this.i = kr2;
        this.j = deleteLogoutUserUseCase;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(k, "presenter start");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(k, "presenter stop");
    }

    @DexIgnore
    public void h() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new DeleteAccountPresenter$logSendOpenFeedbackEvent$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void j() {
        this.f.a(this);
    }

    @DexIgnore
    public void a(MFUser mFUser) {
        kd4.b(mFUser, "user");
        FLogger.INSTANCE.getLocal().d(k, "deleteUser");
        this.f.e();
        DeleteLogoutUserUseCase deleteLogoutUserUseCase = this.j;
        qi3 qi3 = this.f;
        if (qi3 != null) {
            FragmentActivity activity = ((di3) qi3).getActivity();
            if (activity != null) {
                deleteLogoutUserUseCase.a(new DeleteLogoutUserUseCase.b(0, new WeakReference(activity)), new b(this));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.help.DeleteAccountFragment");
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, "subject");
        this.i.a(new kr2.c(str), new DeleteAccountPresenter$sendFeedback$Anon1(this));
    }
}
