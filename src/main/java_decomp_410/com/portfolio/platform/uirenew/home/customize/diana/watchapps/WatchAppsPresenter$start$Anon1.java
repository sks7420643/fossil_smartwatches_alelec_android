package com.portfolio.platform.uirenew.home.customize.diana.watchapps;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.Category;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$start$Anon1", f = "WatchAppsPresenter.kt", l = {237}, m = "invokeSuspend")
public final class WatchAppsPresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppsPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppsPresenter$start$Anon1(WatchAppsPresenter watchAppsPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = watchAppsPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WatchAppsPresenter$start$Anon1 watchAppsPresenter$start$Anon1 = new WatchAppsPresenter$start$Anon1(this.this$Anon0, yb4);
        watchAppsPresenter$start$Anon1.p$ = (zg4) obj;
        return watchAppsPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WatchAppsPresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 b = this.this$Anon0.c();
            WatchAppsPresenter$start$Anon1$allCategory$Anon1 watchAppsPresenter$start$Anon1$allCategory$Anon1 = new WatchAppsPresenter$start$Anon1$allCategory$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(b, watchAppsPresenter$start$Anon1$allCategory$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ArrayList arrayList = new ArrayList();
        for (Category category : (List) obj) {
            if (!WatchAppsPresenter.f(this.this$Anon0).d(category.getId()).isEmpty()) {
                arrayList.add(category);
            }
        }
        this.this$Anon0.l.addAll(arrayList);
        this.this$Anon0.t.a(this.this$Anon0.l);
        return qa4.a;
    }
}
