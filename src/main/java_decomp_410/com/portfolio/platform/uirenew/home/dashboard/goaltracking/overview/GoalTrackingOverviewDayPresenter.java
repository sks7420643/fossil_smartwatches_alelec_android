package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.eb3;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fb3;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gb3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.enums.Status;
import java.util.Date;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingOverviewDayPresenter extends eb3 {
    @DexIgnore
    public Date f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public LiveData<os3<GoalTrackingSummary>> i; // = new MutableLiveData();
    @DexIgnore
    public LiveData<os3<List<GoalTrackingData>>> j; // = new MutableLiveData();
    @DexIgnore
    public /* final */ fb3 k;
    @DexIgnore
    public /* final */ en2 l;
    @DexIgnore
    public /* final */ GoalTrackingRepository m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<os3<? extends GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewDayPresenter a;

        @DexIgnore
        public b(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
            this.a = goalTrackingOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(os3<GoalTrackingSummary> os3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "start - mGoalTrackingSummary -- summary=" + os3);
            if ((os3 != null ? os3.f() : null) != Status.DATABASE_LOADING) {
                this.a.g = true;
                if (this.a.g && this.a.h) {
                    fi4 unused = this.a.j();
                }
                this.a.k.c(true ^ this.a.l.H());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements cc<os3<? extends List<GoalTrackingData>>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewDayPresenter a;

        @DexIgnore
        public c(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
            this.a = goalTrackingOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(os3<? extends List<GoalTrackingData>> os3) {
            Status a2 = os3.a();
            List list = (List) os3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mGoalTrackingData -- goalTrackingSamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("GoalTrackingOverviewDayPresenter", sb.toString());
            if (a2 != Status.DATABASE_LOADING) {
                this.a.h = true;
                if (this.a.g && this.a.h) {
                    fi4 unused = this.a.j();
                }
                this.a.k.c(true ^ this.a.l.H());
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public GoalTrackingOverviewDayPresenter(fb3 fb3, en2 en2, GoalTrackingRepository goalTrackingRepository) {
        kd4.b(fb3, "mView");
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        this.k = fb3;
        this.l = en2;
        this.m = goalTrackingRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Date b(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
        Date date = goalTrackingOverviewDayPresenter.f;
        if (date != null) {
            return date;
        }
        kd4.d("mDate");
        throw null;
    }

    @DexIgnore
    public final fi4 j() {
        return ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingOverviewDayPresenter$showDetailChart$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        h();
        LiveData<os3<GoalTrackingSummary>> liveData = this.i;
        fb3 fb3 = this.k;
        if (fb3 != null) {
            liveData.a((gb3) fb3, new b(this));
            this.j.a((LifecycleOwner) this.k, new c(this));
            this.k.c(!this.l.H());
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayPresenter", "stop");
        try {
            LiveData<os3<List<GoalTrackingData>>> liveData = this.j;
            fb3 fb3 = this.k;
            if (fb3 != null) {
                liveData.a((LifecycleOwner) (gb3) fb3);
                this.i.a((LifecycleOwner) this.k);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayFragment");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "stop - e=" + e);
        }
    }

    @DexIgnore
    public void h() {
        Date date = this.f;
        if (date != null) {
            if (date == null) {
                kd4.d("mDate");
                throw null;
            } else if (rk2.s(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.f;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("GoalTrackingOverviewDayPresenter", sb.toString());
                    return;
                }
                kd4.d("mDate");
                throw null;
            }
        }
        this.g = false;
        this.h = false;
        this.f = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.f;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("GoalTrackingOverviewDayPresenter", sb2.toString());
            GoalTrackingRepository goalTrackingRepository = this.m;
            Date date4 = this.f;
            if (date4 != null) {
                this.i = goalTrackingRepository.getSummary(date4);
                GoalTrackingRepository goalTrackingRepository2 = this.m;
                Date date5 = this.f;
                if (date5 == null) {
                    kd4.d("mDate");
                    throw null;
                } else if (date5 != null) {
                    this.j = goalTrackingRepository2.getGoalTrackingDataList(date5, date5, true);
                } else {
                    kd4.d("mDate");
                    throw null;
                }
            } else {
                kd4.d("mDate");
                throw null;
            }
        } else {
            kd4.d("mDate");
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        this.k.a(this);
    }
}
