package com.portfolio.platform.uirenew.home.dashboard.heartrate;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.MFUser;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter$initDataSource$Anon1$user$Anon1", f = "DashboardHeartRatePresenter.kt", l = {}, m = "invokeSuspend")
public final class DashboardHeartRatePresenter$initDataSource$Anon1$user$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super MFUser>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DashboardHeartRatePresenter$initDataSource$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardHeartRatePresenter$initDataSource$Anon1$user$Anon1(DashboardHeartRatePresenter$initDataSource$Anon1 dashboardHeartRatePresenter$initDataSource$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = dashboardHeartRatePresenter$initDataSource$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DashboardHeartRatePresenter$initDataSource$Anon1$user$Anon1 dashboardHeartRatePresenter$initDataSource$Anon1$user$Anon1 = new DashboardHeartRatePresenter$initDataSource$Anon1$user$Anon1(this.this$Anon0, yb4);
        dashboardHeartRatePresenter$initDataSource$Anon1$user$Anon1.p$ = (zg4) obj;
        return dashboardHeartRatePresenter$initDataSource$Anon1$user$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DashboardHeartRatePresenter$initDataSource$Anon1$user$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return this.this$Anon0.this$Anon0.m.getCurrentUser();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
