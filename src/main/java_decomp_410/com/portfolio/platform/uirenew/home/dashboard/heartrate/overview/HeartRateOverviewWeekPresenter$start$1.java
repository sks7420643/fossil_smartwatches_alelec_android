package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1", mo27670f = "HeartRateOverviewWeekPresenter.kt", mo27671l = {48}, mo27672m = "invokeSuspend")
public final class HeartRateOverviewWeekPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23450p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$2")
    /* renamed from: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$2 */
    public static final class C66542<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1 f23451a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$2$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$2$1", mo27670f = "HeartRateOverviewWeekPresenter.kt", mo27671l = {62}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$2$1 */
        public static final class C66551 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public /* final */ /* synthetic */ java.util.List $data;
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f23452p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1.C66542 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C66551(com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1.C66542 r1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
                this.$data = list;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1.C66542.C66551 r0 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1.C66542.C66551(this.this$0, this.$data, yb4);
                r0.f23452p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1.C66542.C66551) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23452p$;
                    com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.f23451a.this$0.mo31440b();
                    com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.C6656x32e31cac heartRateOverviewWeekPresenter$start$1$2$1$listRestingDataPoint$1 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.C6656x32e31cac(this, (com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg4;
                    this.label = 1;
                    obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, heartRateOverviewWeekPresenter$start$1$2$1$listRestingDataPoint$1, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                java.util.List list = (java.util.List) obj;
                if (list != null) {
                    this.this$0.f23451a.this$0.f23447i.mo31494a(com.fossil.blesdk.obfuscated.kb4.m24381d(list), this.this$0.f23451a.this$0.f23446h);
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        public C66542(com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1 heartRateOverviewWeekPresenter$start$1) {
            this.f23451a = heartRateOverviewWeekPresenter$start$1;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>> os3) {
            com.portfolio.platform.enums.Status a = os3.mo29972a();
            java.util.List list = (java.util.List) os3.mo29973b();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("start - mHeartRateSummaries -- heartRateSummaries=");
            sb.append(list != null ? java.lang.Integer.valueOf(list.size()) : null);
            local.mo33255d("HeartRateOverviewWeekPresenter", sb.toString());
            if (a != com.portfolio.platform.enums.Status.DATABASE_LOADING) {
                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23451a.this$0.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1.C66542.C66551(this, list, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateOverviewWeekPresenter$start$1(com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = heartRateOverviewWeekPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1 heartRateOverviewWeekPresenter$start$1 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1(this.this$0, yb4);
        heartRateOverviewWeekPresenter$start$1.f23450p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return heartRateOverviewWeekPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00b2  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        androidx.lifecycle.LiveData d;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23450p$;
            if (this.this$0.f23444f == null || !com.fossil.blesdk.obfuscated.rk2.m27414s(com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter.m34895c(this.this$0)).booleanValue()) {
                this.this$0.f23444f = new java.util.Date();
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
                com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$startAndEnd$1 heartRateOverviewWeekPresenter$start$1$startAndEnd$1 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$startAndEnd$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, heartRateOverviewWeekPresenter$start$1$startAndEnd$1, this);
                if (obj == a) {
                    return a;
                }
            }
            this.this$0.mo41326h();
            d = this.this$0.f23445g;
            if (d != null) {
                com.fossil.blesdk.obfuscated.uc3 g = this.this$0.f23447i;
                if (g != null) {
                    d.mo2277a((com.fossil.blesdk.obfuscated.vc3) g, new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1.C66542(this));
                } else {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekFragment");
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        kotlin.Pair pair = (kotlin.Pair) obj;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d("HeartRateOverviewWeekPresenter", "start - startDate=" + ((java.util.Date) pair.getFirst()) + ", endDate=" + ((java.util.Date) pair.getSecond()));
        com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter = this.this$0;
        heartRateOverviewWeekPresenter.f23445g = heartRateOverviewWeekPresenter.f23449k.getHeartRateSummaries((java.util.Date) pair.getFirst(), (java.util.Date) pair.getSecond(), false);
        this.this$0.mo41326h();
        d = this.this$0.f23445g;
        if (d != null) {
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
