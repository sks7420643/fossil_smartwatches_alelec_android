package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.e93;
import com.fossil.blesdk.obfuscated.f93;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.g93;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.enums.Status;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityOverviewDayPresenter extends e93 {
    @DexIgnore
    public Date f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public LiveData<os3<List<ActivitySummary>>> i; // = new MutableLiveData();
    @DexIgnore
    public LiveData<os3<List<ActivitySample>>> j; // = new MutableLiveData();
    @DexIgnore
    public LiveData<os3<List<WorkoutSession>>> k; // = new MutableLiveData();
    @DexIgnore
    public /* final */ f93 l;
    @DexIgnore
    public /* final */ SummariesRepository m;
    @DexIgnore
    public /* final */ ActivitiesRepository n;
    @DexIgnore
    public /* final */ WorkoutSessionRepository o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<os3<? extends List<ActivitySummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewDayPresenter a;

        @DexIgnore
        public b(ActivityOverviewDayPresenter activityOverviewDayPresenter) {
            this.a = activityOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(os3<? extends List<ActivitySummary>> os3) {
            Status a2 = os3.a();
            List list = (List) os3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mActivitySummaries -- activitySummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("ActivityOverviewDayPresenter", sb.toString());
            if (a2 != Status.DATABASE_LOADING) {
                this.a.g = true;
                if (this.a.g && this.a.h) {
                    fi4 unused = this.a.j();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements cc<os3<? extends List<ActivitySample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewDayPresenter a;

        @DexIgnore
        public c(ActivityOverviewDayPresenter activityOverviewDayPresenter) {
            this.a = activityOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(os3<? extends List<ActivitySample>> os3) {
            Status a2 = os3.a();
            List list = (List) os3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mActivitySamples -- activitySamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("ActivityOverviewDayPresenter", sb.toString());
            if (a2 != Status.DATABASE_LOADING) {
                this.a.h = true;
                if (this.a.g && this.a.h) {
                    fi4 unused = this.a.j();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements cc<os3<? extends List<WorkoutSession>>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewDayPresenter a;

        @DexIgnore
        public d(ActivityOverviewDayPresenter activityOverviewDayPresenter) {
            this.a = activityOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(os3<? extends List<WorkoutSession>> os3) {
            Status a2 = os3.a();
            List list = (List) os3.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mWorkoutSessions -- workoutSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("ActivityOverviewDayPresenter", sb.toString());
            if (a2 == Status.DATABASE_LOADING) {
                return;
            }
            if (list == null || list.isEmpty()) {
                this.a.l.a(false, new ArrayList());
            } else {
                this.a.l.a(true, list);
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public ActivityOverviewDayPresenter(f93 f93, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, WorkoutSessionRepository workoutSessionRepository) {
        kd4.b(f93, "mView");
        kd4.b(summariesRepository, "mSummariesRepository");
        kd4.b(activitiesRepository, "mActivitiesRepository");
        kd4.b(workoutSessionRepository, "mWorkoutSessionRepository");
        this.l = f93;
        this.m = summariesRepository;
        this.n = activitiesRepository;
        this.o = workoutSessionRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Date d(ActivityOverviewDayPresenter activityOverviewDayPresenter) {
        Date date = activityOverviewDayPresenter.f;
        if (date != null) {
            return date;
        }
        kd4.d("mDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        this.l.a(this);
    }

    @DexIgnore
    public final fi4 j() {
        return ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ActivityOverviewDayPresenter$showDetailChart$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        h();
        LiveData<os3<List<ActivitySummary>>> liveData = this.i;
        f93 f93 = this.l;
        if (f93 != null) {
            liveData.a((g93) f93, new b(this));
            this.j.a((LifecycleOwner) this.l, new c(this));
            this.k.a((LifecycleOwner) this.l, new d(this));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayPresenter", "stop");
        try {
            LiveData<os3<List<ActivitySample>>> liveData = this.j;
            f93 f93 = this.l;
            if (f93 != null) {
                liveData.a((LifecycleOwner) (g93) f93);
                this.i.a((LifecycleOwner) this.l);
                this.k.a((LifecycleOwner) this.l);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayFragment");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActivityOverviewDayPresenter", "stop - e=" + e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x001e, code lost:
        if (com.fossil.blesdk.obfuscated.rk2.s(r0).booleanValue() == false) goto L_0x0025;
     */
    @DexIgnore
    public void h() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayPresenter", "loadData");
        Date date = this.f;
        if (date != null) {
            if (date == null) {
                kd4.d("mDate");
                throw null;
            }
        }
        this.g = false;
        this.h = false;
        this.f = new Date();
        SummariesRepository summariesRepository = this.m;
        Date date2 = this.f;
        if (date2 == null) {
            kd4.d("mDate");
            throw null;
        } else if (date2 != null) {
            this.i = summariesRepository.getSummaries(date2, date2, false);
            ActivitiesRepository activitiesRepository = this.n;
            Date date3 = this.f;
            if (date3 == null) {
                kd4.d("mDate");
                throw null;
            } else if (date3 != null) {
                this.j = activitiesRepository.getActivityList(date3, date3, true);
                WorkoutSessionRepository workoutSessionRepository = this.o;
                Date date4 = this.f;
                if (date4 == null) {
                    kd4.d("mDate");
                    throw null;
                } else if (date4 != null) {
                    this.k = workoutSessionRepository.getWorkoutSessions(date4, date4, true);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("loadData - mDate=");
                    Date date5 = this.f;
                    if (date5 != null) {
                        sb.append(date5);
                        local.d("ActivityOverviewDayPresenter", sb.toString());
                        return;
                    }
                    kd4.d("mDate");
                    throw null;
                } else {
                    kd4.d("mDate");
                    throw null;
                }
            } else {
                kd4.d("mDate");
                throw null;
            }
        } else {
            kd4.d("mDate");
            throw null;
        }
    }
}
