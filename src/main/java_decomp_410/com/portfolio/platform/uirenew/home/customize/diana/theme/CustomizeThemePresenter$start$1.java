package com.portfolio.platform.uirenew.home.customize.diana.theme;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1", mo27670f = "CustomizeThemePresenter.kt", mo27671l = {53}, mo27672m = "invokeSuspend")
public final class CustomizeThemePresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22824p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1$2")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1$2", mo27670f = "CustomizeThemePresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1$2 */
    public static final class C64702 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22825p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1$2$1")
        /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1$2$1 */
        public static final class C64711<T> implements com.fossil.blesdk.obfuscated.C1548cc<java.util.List<? extends com.portfolio.platform.data.model.diana.preset.WatchFace>> {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702 f22826a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1$2$1$1")
            @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1$2$1$1", mo27670f = "CustomizeThemePresenter.kt", mo27671l = {60}, mo27672m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1$2$1$1 */
            public static final class C64721 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
                @DexIgnore
                public /* final */ /* synthetic */ java.util.List $it;
                @DexIgnore
                public java.lang.Object L$0;
                @DexIgnore
                public java.lang.Object L$1;
                @DexIgnore
                public int label;

                @DexIgnore
                /* renamed from: p$ */
                public com.fossil.blesdk.obfuscated.zg4 f22827p$;
                @DexIgnore
                public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702.C64711 this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1$2$1$1$1")
                @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1$2$1$1$1", mo27670f = "CustomizeThemePresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
                /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1$2$1$1$1 */
                public static final class C64731 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ java.util.List $watchFaceWrappers;
                    @DexIgnore
                    public int label;

                    @DexIgnore
                    /* renamed from: p$ */
                    public com.fossil.blesdk.obfuscated.zg4 f22828p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702.C64711.C64721 this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C64731(com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702.C64711.C64721 r1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
                        super(2, yb4);
                        this.this$0 = r1;
                        this.$watchFaceWrappers = list;
                    }

                    @DexIgnore
                    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                        com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702.C64711.C64721.C64731 r0 = new com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702.C64711.C64721.C64731(this.this$0, this.$watchFaceWrappers, yb4);
                        r0.f22828p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                        return r0;
                    }

                    @DexIgnore
                    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                        return ((com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702.C64711.C64721.C64731) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
                    }

                    @DexIgnore
                    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                        com.fossil.blesdk.obfuscated.cc4.m20546a();
                        if (this.label == 0) {
                            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                            this.this$0.this$0.f22826a.this$0.this$0.f22821i.mo28472n((java.util.List<com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper>) this.$watchFaceWrappers);
                            return com.fossil.blesdk.obfuscated.qa4.f17909a;
                        }
                        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C64721(com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702.C64711 r1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
                    super(2, yb4);
                    this.this$0 = r1;
                    this.$it = list;
                }

                @DexIgnore
                public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                    com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                    com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702.C64711.C64721 r0 = new com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702.C64711.C64721(this.this$0, this.$it, yb4);
                    r0.f22827p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                    return r0;
                }

                @DexIgnore
                public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                    return ((com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702.C64711.C64721) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
                }

                @DexIgnore
                public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                    java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                    int i = this.label;
                    if (i == 0) {
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                        com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22827p$;
                        if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f22826a.this$0.this$0.f22820h, (java.lang.Object) this.$it)) {
                            com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter customizeThemePresenter = this.this$0.f22826a.this$0.this$0;
                            java.util.List list = this.$it;
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) list, "it");
                            customizeThemePresenter.f22820h = list;
                            java.util.List list2 = this.$it;
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) list2, "it");
                            com.portfolio.platform.data.model.diana.preset.DianaPreset a2 = com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter.m34037b(this.this$0.f22826a.this$0.this$0).mo40964c().mo2275a();
                            java.util.List<com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper> a3 = com.fossil.blesdk.obfuscated.tj2.m28285a((java.util.List<com.portfolio.platform.data.model.diana.preset.WatchFace>) list2, (java.util.List<com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting>) a2 != null ? a2.getComplications() : null);
                            com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
                            com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702.C64711.C64721.C64731 r5 = new com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702.C64711.C64721.C64731(this, a3, (com.fossil.blesdk.obfuscated.yb4) null);
                            this.L$0 = zg4;
                            this.L$1 = a3;
                            this.label = 1;
                            if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r5, this) == a) {
                                return a;
                            }
                        }
                    } else if (i == 1) {
                        java.util.List list3 = (java.util.List) this.L$1;
                        com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                        com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    } else {
                        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
            }

            @DexIgnore
            public C64711(com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702 r1) {
                this.f22826a = r1;
            }

            @DexIgnore
            /* renamed from: a */
            public final void mo8689a(java.util.List<com.portfolio.platform.data.model.diana.preset.WatchFace> list) {
                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f22826a.this$0.this$0.mo31443e(), com.fossil.blesdk.obfuscated.nh4.m25691a(), (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702.C64711.C64721(this, list, (com.fossil.blesdk.obfuscated.yb4) null), 2, (java.lang.Object) null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C64702(com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1 customizeThemePresenter$start$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = customizeThemePresenter$start$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702 r0 = new com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702(this.this$0, yb4);
            r0.f22825p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                androidx.lifecycle.LiveData e = this.this$0.this$0.f22819g;
                if (e == null) {
                    return null;
                }
                com.fossil.blesdk.obfuscated.j43 c = this.this$0.this$0.f22821i;
                if (c != null) {
                    e.mo2277a((com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment) c, new com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702.C64711(this));
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment");
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomizeThemePresenter$start$1(com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter customizeThemePresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = customizeThemePresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1 customizeThemePresenter$start$1 = new com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1(this.this$0, yb4);
        customizeThemePresenter$start$1.f22824p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return customizeThemePresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22824p$;
            if (this.this$0.f22819g == null) {
                com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter customizeThemePresenter = this.this$0;
                customizeThemePresenter.f22819g = customizeThemePresenter.f22822j.getWatchFacesLiveDataWithSerial(com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e());
                com.fossil.blesdk.obfuscated.qa4 qa4 = com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
            com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702 r3 = new com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$start$1.C64702(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
