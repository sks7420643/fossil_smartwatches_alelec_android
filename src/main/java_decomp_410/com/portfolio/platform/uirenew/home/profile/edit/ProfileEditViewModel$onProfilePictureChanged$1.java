package com.portfolio.platform.uirenew.home.profile.edit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$1", mo27670f = "ProfileEditViewModel.kt", mo27671l = {184}, mo27672m = "invokeSuspend")
public final class ProfileEditViewModel$onProfilePictureChanged$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.content.Intent $data;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23845p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileEditViewModel$onProfilePictureChanged$1(com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel profileEditViewModel, android.content.Intent intent, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = profileEditViewModel;
        this.$data = intent;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$1 profileEditViewModel$onProfilePictureChanged$1 = new com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$1(this.this$0, this.$data, yb4);
        profileEditViewModel$onProfilePictureChanged$1.f23845p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return profileEditViewModel$onProfilePictureChanged$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object obj2;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23845p$;
            android.net.Uri a2 = com.fossil.blesdk.obfuscated.al2.m19880a(this.$data, com.portfolio.platform.PortfolioApp.f20941W.mo34589c());
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a3 = com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel.f23827l.mo41490a();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("Inside .onActivityResult imageUri=");
            if (a2 != null) {
                sb.append(a2);
                local.mo33255d(a3, sb.toString());
                if (!com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34510a(this.$data, a2)) {
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
                com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel.m35393a(this.this$0, (com.portfolio.platform.data.model.MFUser) null, a2, (java.lang.Boolean) null, (kotlin.Pair) null, false, (com.portfolio.platform.data.model.MFUser) null, false, 125, (java.lang.Object) null);
                com.fossil.blesdk.obfuscated.ug4 b = com.fossil.blesdk.obfuscated.nh4.m25692b();
                com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$1$bitmap$1 profileEditViewModel$onProfilePictureChanged$1$bitmap$1 = new com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$1$bitmap$1(a2, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = a2;
                this.label = 1;
                obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(b, profileEditViewModel$onProfilePictureChanged$1$bitmap$1, this);
                if (obj2 == a) {
                    return a;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        } else if (i == 1) {
            android.net.Uri uri = (android.net.Uri) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            try {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                obj2 = obj;
            } catch (java.lang.Exception e) {
                e.printStackTrace();
                com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel.m35393a(this.this$0, (com.portfolio.platform.data.model.MFUser) null, (android.net.Uri) null, (java.lang.Boolean) null, (kotlin.Pair) null, true, (com.portfolio.platform.data.model.MFUser) null, false, 111, (java.lang.Object) null);
            }
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        android.graphics.Bitmap bitmap = (android.graphics.Bitmap) obj2;
        com.portfolio.platform.data.model.MFUser a4 = this.this$0.f23829d;
        if (a4 != null) {
            if (bitmap != null) {
                a4.setProfilePicture(com.fossil.blesdk.obfuscated.vr3.m29405a(bitmap));
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
        this.this$0.f23830e = true;
        com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel.m35393a(this.this$0, (com.portfolio.platform.data.model.MFUser) null, (android.net.Uri) null, com.fossil.blesdk.obfuscated.dc4.m20839a(this.this$0.mo41487e()), (kotlin.Pair) null, false, (com.portfolio.platform.data.model.MFUser) null, false, 123, (java.lang.Object) null);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
