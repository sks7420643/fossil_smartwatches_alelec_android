package com.portfolio.platform.uirenew.home.customize.diana;

import android.content.Context;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.oj2;
import com.fossil.blesdk.obfuscated.ok2;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.pl2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.t13;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.ul2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ DianaPreset $currentPreset$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ DianaPreset $it;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon10;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public Object L$Anon8;
    @DexIgnore
    public Object L$Anon9;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaCustomizeEditPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Parcelable>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaPresetComplicationSetting $complication;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1, DianaPresetComplicationSetting dianaPresetComplicationSetting, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1;
            this.$complication = dianaPresetComplicationSetting;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$complication, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return DianaCustomizeEditPresenter.g(this.this$Anon0.this$Anon0).g(this.$complication.getId());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super Parcelable>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaPresetWatchAppSetting $watchApp;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1, DianaPresetWatchAppSetting dianaPresetWatchAppSetting, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1;
            this.$watchApp = dianaPresetWatchAppSetting;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, this.$watchApp, yb4);
            anon2.p$ = (zg4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return DianaCustomizeEditPresenter.g(this.this$Anon0.this$Anon0).h(this.$watchApp.getId());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.e<SetDianaPresetToWatchUseCase.d, SetDianaPresetToWatchUseCase.b> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 a;
        @DexIgnore
        public /* final */ /* synthetic */ ul2 b;

        @DexIgnore
        public a(DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1, ul2 ul2) {
            this.a = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1;
            this.b = ul2;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetDianaPresetToWatchUseCase.d dVar) {
            kd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "setToWatch success");
            this.b.a("");
            this.a.this$Anon0.l();
            this.a.this$Anon0.n.m();
            this.a.this$Anon0.n.g(true);
        }

        @DexIgnore
        public void a(SetDianaPresetToWatchUseCase.b bVar) {
            kd4.b(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "setToWatch onError");
            this.a.this$Anon0.l();
            this.a.this$Anon0.n.m();
            int b2 = bVar.b();
            if (b2 != 1101) {
                if (b2 == 8888) {
                    this.a.this$Anon0.n.c();
                } else if (!(b2 == 1112 || b2 == 1113)) {
                    this.a.this$Anon0.n.q();
                }
                String arrayList = bVar.a().toString();
                ul2 ul2 = this.b;
                kd4.a((Object) arrayList, "errorCode");
                ul2.a(arrayList);
                return;
            }
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(bVar.a());
            kd4.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            t13 k = this.a.this$Anon0.n;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                k.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                String arrayList2 = bVar.a().toString();
                ul2 ul22 = this.b;
                kd4.a((Object) arrayList2, "errorCode");
                ul22.a(arrayList2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1(DianaPreset dianaPreset, yb4 yb4, DianaCustomizeEditPresenter dianaCustomizeEditPresenter, DianaPreset dianaPreset2) {
        super(2, yb4);
        this.$it = dianaPreset;
        this.this$Anon0 = dianaCustomizeEditPresenter;
        this.$currentPreset$inlined = dianaPreset2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 = new DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1(this.$it, yb4, this.this$Anon0, this.$currentPreset$inlined);
        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v5, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v6, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v3, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v10, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v13, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v16, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v25, resolved type: com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v125, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v36, resolved type: java.util.Iterator} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v131, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v21, resolved type: java.util.Iterator} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v135, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r19v11, resolved type: java.util.List} */
    /* JADX WARNING: Can't wrap try/catch for region: R(10:4|(3:5|6|7)|(2:150|151)|152|153|154|(7:156|157|204|135|(14:137|138|139|(8:141|142|143|144|145|146|(1:148)(7:149|150|151|152|153|154|(1:158)(0))|148)|167|168|169|170|(4:184|185|186|(9:188|189|190|191|(1:193)|196|204|135|(1:205)(0)))(2:172|(1:174)(5:176|177|178|(1:180)|181))|175|196|204|135|(0)(0))(0)|207|(5:209|(2:211|(3:215|220|221))(2:216|(3:218|220|221))|219|220|221)(4:222|(1:224)(1:225)|226|227))|158|207|(0)(0)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(14:137|(1:138)|139|(8:141|142|143|144|145|146|(1:148)(7:149|150|151|152|153|154|(1:158)(0))|148)|167|168|169|170|(4:184|185|186|(9:188|189|190|191|(1:193)|196|204|135|(1:205)(0)))(2:172|(1:174)(5:176|177|178|(1:180)|181))|175|196|204|135|(0)(0)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:53|54|55|56|(1:58)(5:59|60|61|(1:65)(0)|65)|58) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:(2:141|142)|143|144|145|146|(1:148)(7:149|150|151|152|153|154|(1:158)(0))|148) */
    /* JADX WARNING: Can't wrap try/catch for region: R(9:188|189|190|191|(1:193)|196|204|135|(1:205)(0)) */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x054f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x0554, code lost:
        r15 = r3;
        r3 = r7;
        r7 = r8;
        r8 = r11;
        r10 = r16;
        r9 = r17;
        r2 = r20;
        r17 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x0562, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x0567, code lost:
        r17 = r2;
        r9 = r21;
        r10 = r16;
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x05e3, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x05e4, code lost:
        r9 = r11;
        r1 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x05f2, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x05f3, code lost:
        r17 = r2;
        r10 = r16;
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x02d9, code lost:
        if (android.text.TextUtils.isEmpty(((com.portfolio.platform.data.model.setting.CommuteTimeSetting) r1.this$Anon0.j.a(r0.getSettings(), com.portfolio.platform.data.model.setting.CommuteTimeSetting.class)).getAddress()) != false) goto L_0x02db;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0396  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x040c  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0453  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x04a4  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x052b A[Catch:{ Exception -> 0x054f }] */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x0632  */
    /* JADX WARNING: Removed duplicated region for block: B:209:0x0659  */
    /* JADX WARNING: Removed duplicated region for block: B:222:0x06b5  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0153  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01f2  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0266 A[Catch:{ Exception -> 0x027d }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0303  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x033a  */
    public final Object invokeSuspend(Object obj) {
        String str;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting;
        String str2;
        String str3;
        ul2 ul2;
        String str4;
        String str5;
        zg4 zg4;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting2;
        DianaPresetComplicationSetting dianaPresetComplicationSetting;
        DianaPresetComplicationSetting dianaPresetComplicationSetting2;
        List list;
        List list2;
        Object obj2;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting3;
        List list3;
        List list4;
        Iterator it;
        String str6;
        String str7;
        zg4 zg42;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting4;
        String str8;
        String str9;
        Object obj3;
        String str10;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting5;
        Object obj4;
        List list5;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting6;
        DianaPresetComplicationSetting dianaPresetComplicationSetting3;
        zg4 zg43;
        Parcelable parcelable;
        Object obj5;
        DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1;
        ug4 a2;
        String str11;
        List list6;
        Object obj6;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting7;
        String str12;
        List list7;
        DianaPresetComplicationSetting dianaPresetComplicationSetting4;
        Iterator it2;
        zg4 zg44;
        Object obj7;
        List list8;
        Parcelable parcelable2;
        DianaPresetComplicationSetting dianaPresetComplicationSetting5;
        DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12 = this;
        Object a3 = cc4.a();
        int i = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.label;
        String str13 = "weather";
        String str14 = "check setting of ";
        String str15 = "java.lang.String.format(format, *args)";
        String str16 = "exception when parse setting from json ";
        String str17 = "commute-time";
        if (i == 0) {
            na4.a(obj);
            zg4 zg45 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.p$;
            ArrayList<DianaPresetComplicationSetting> complications = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.$it.getComplications();
            list2 = new ArrayList();
            for (DianaPresetComplicationSetting next : complications) {
                if (dc4.a(ok2.c.c(next.getId())).booleanValue()) {
                    list2.add(next);
                }
            }
            if (!list2.isEmpty()) {
                Iterator it3 = list2.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    }
                    dianaPresetComplicationSetting5 = it3.next();
                    if (!ok2.c.e(dianaPresetComplicationSetting5.getId())) {
                        break;
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DianaCustomizeEditPresenter", "setPresetToWatch missingPermissionComp " + dianaPresetComplicationSetting5);
                if (dianaPresetComplicationSetting5 == null) {
                    pd4 pd4 = pd4.a;
                    String a4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.set_to_watch_fail_complication_permission);
                    kd4.a((Object) a4, "LanguageHelper.getString\u2026_complication_permission)");
                    Object[] objArr = {dianaPresetComplicationSetting5.getId(), dianaPresetComplicationSetting5.getPosition()};
                    String format = String.format(a4, Arrays.copyOf(objArr, objArr.length));
                    kd4.a((Object) format, str15);
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.n.a(format, dianaPresetComplicationSetting5.getId(), dianaPresetComplicationSetting5.getPosition(), true);
                    return qa4.a;
                }
                ArrayList<DianaPresetComplicationSetting> complications2 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.$it.getComplications();
                list6 = new ArrayList();
                for (DianaPresetComplicationSetting next2 : complications2) {
                    zg4 zg46 = zg45;
                    if (dc4.a(ok2.c.d(next2.getId())).booleanValue()) {
                        list6.add(next2);
                    }
                    zg45 = zg46;
                }
                zg4 zg47 = zg45;
                if (!list6.isEmpty()) {
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12 = this;
                    it2 = list6.iterator();
                    str = "";
                    dianaPresetComplicationSetting2 = dianaPresetComplicationSetting5;
                    obj6 = a3;
                    list7 = list2;
                    zg42 = zg47;
                    dianaPresetComplicationSetting4 = null;
                } else {
                    str7 = str13;
                    str = "";
                    str5 = str14;
                    str11 = str15;
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12 = this;
                    obj6 = a3;
                    dianaPresetComplicationSetting2 = dianaPresetComplicationSetting5;
                    zg42 = zg47;
                    dianaPresetComplicationSetting = null;
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingComplication " + dianaPresetComplicationSetting);
                    if (dianaPresetComplicationSetting != null) {
                    }
                }
            }
            dianaPresetComplicationSetting5 = null;
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("DianaCustomizeEditPresenter", "setPresetToWatch missingPermissionComp " + dianaPresetComplicationSetting5);
            if (dianaPresetComplicationSetting5 == null) {
            }
        } else if (i == 1) {
            it2 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon6;
            dianaPresetComplicationSetting4 = (DianaPresetComplicationSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon4;
            list6 = (List) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon3;
            dianaPresetComplicationSetting2 = (DianaPresetComplicationSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon2;
            list7 = (List) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon1;
            DianaPresetComplicationSetting dianaPresetComplicationSetting6 = (DianaPresetComplicationSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon5;
            zg44 = (zg4) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon0;
            try {
                na4.a(obj);
                str7 = str13;
                str5 = str14;
                str11 = str15;
                obj7 = obj;
                obj6 = a3;
                list8 = list7;
                dianaPresetComplicationSetting = dianaPresetComplicationSetting6;
                str = "";
            } catch (Exception e) {
                e = e;
                str7 = str13;
                str = "";
                str5 = str14;
                str11 = str15;
                obj6 = a3;
                zg42 = zg44;
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                local4.e("DianaCustomizeEditPresenter", str16 + e);
                str13 = str7;
                str15 = str11;
                str14 = str5;
                if (!it2.hasNext()) {
                }
                ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                local22.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingComplication " + dianaPresetComplicationSetting);
                if (dianaPresetComplicationSetting != null) {
                }
            }
            try {
            } catch (Exception e2) {
                e = e2;
                list7 = list8;
                zg42 = zg44;
                ILocalFLogger local42 = FLogger.INSTANCE.getLocal();
                local42.e("DianaCustomizeEditPresenter", str16 + e);
                str13 = str7;
                str15 = str11;
                str14 = str5;
                if (!it2.hasNext()) {
                }
                ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
                local222.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingComplication " + dianaPresetComplicationSetting);
                if (dianaPresetComplicationSetting != null) {
                }
            }
            parcelable2 = (Parcelable) obj7;
            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            local5.d("DianaCustomizeEditPresenter", "last setting " + parcelable2);
            if (parcelable2 == null) {
                dianaPresetComplicationSetting.setSettings(dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.j.a((Object) parcelable2));
                list7 = list8;
                zg42 = zg44;
                str13 = str7;
                str15 = str11;
                str14 = str5;
            }
            list2 = list8;
            zg42 = zg44;
            ILocalFLogger local2222 = FLogger.INSTANCE.getLocal();
            local2222.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingComplication " + dianaPresetComplicationSetting);
            if (dianaPresetComplicationSetting != null) {
                String id = dianaPresetComplicationSetting.getId();
                int hashCode = id.hashCode();
                if (hashCode != -829740640) {
                    if (hashCode == 134170930 && id.equals("second-timezone")) {
                        str12 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_SecondTimezoneError_Text__ToDisplayTimezoneOnYourWatch);
                        kd4.a((Object) str12, "LanguageHelper.getString\u2026splayTimezoneOnYourWatch)");
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.n.b(str12, dianaPresetComplicationSetting.getId(), dianaPresetComplicationSetting.getPosition(), true);
                        return qa4.a;
                    }
                } else if (id.equals(str17)) {
                    str12 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_CommuteTimeError___ToDisplayCommuteTimePleaseEnter);
                    kd4.a((Object) str12, "LanguageHelper.getString\u2026ayCommuteTimePleaseEnter)");
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.n.b(str12, dianaPresetComplicationSetting.getId(), dianaPresetComplicationSetting.getPosition(), true);
                    return qa4.a;
                }
                str12 = str;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.n.b(str12, dianaPresetComplicationSetting.getId(), dianaPresetComplicationSetting.getPosition(), true);
                return qa4.a;
            }
            ArrayList<DianaPresetWatchAppSetting> watchapps = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.$it.getWatchapps();
            List arrayList = new ArrayList();
            for (DianaPresetWatchAppSetting next3 : watchapps) {
                if (dc4.a(pl2.d.f(next3.getId())).booleanValue()) {
                    arrayList.add(next3);
                }
            }
            if (!arrayList.isEmpty()) {
                Iterator it4 = arrayList.iterator();
                while (true) {
                    if (!it4.hasNext()) {
                        break;
                    }
                    dianaPresetWatchAppSetting7 = (DianaPresetWatchAppSetting) it4.next();
                    if (!pl2.d.d(dianaPresetWatchAppSetting7.getId())) {
                        break;
                    }
                }
                ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                local6.d("DianaCustomizeEditPresenter", "setPresetToWatch missingPermissionWatchApp " + dianaPresetWatchAppSetting7);
                if (dianaPresetWatchAppSetting7 == null) {
                    pd4 pd42 = pd4.a;
                    String a5 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.set_to_watch_fail_app_permission);
                    kd4.a((Object) a5, "LanguageHelper.getString\u2026atch_fail_app_permission)");
                    Object[] objArr2 = {dianaPresetWatchAppSetting7.getId(), dianaPresetWatchAppSetting7.getPosition()};
                    String format2 = String.format(a5, Arrays.copyOf(objArr2, objArr2.length));
                    kd4.a((Object) format2, str11);
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.n.a(format2, dianaPresetWatchAppSetting7.getId(), dianaPresetWatchAppSetting7.getPosition(), false);
                    return qa4.a;
                }
                ArrayList<DianaPresetWatchAppSetting> watchapps2 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.$it.getWatchapps();
                List arrayList2 = new ArrayList();
                Iterator<T> it5 = watchapps2.iterator();
                while (it5.hasNext()) {
                    T next4 = it5.next();
                    Iterator<T> it6 = it5;
                    if (dc4.a(pl2.d.g(((DianaPresetWatchAppSetting) next4).getId())).booleanValue()) {
                        arrayList2.add(next4);
                    }
                    it5 = it6;
                }
                if (!arrayList2.isEmpty()) {
                    Iterator it7 = arrayList2.iterator();
                    obj2 = obj6;
                    dianaPresetWatchAppSetting3 = null;
                    list3 = arrayList2;
                    list4 = list6;
                    list = arrayList;
                    dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting7;
                    it = it7;
                    if (!it.hasNext()) {
                    }
                    ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
                    local7.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                    if (dianaPresetWatchAppSetting == null) {
                    }
                } else {
                    str2 = str17;
                    str3 = str7;
                    dianaPresetWatchAppSetting = null;
                    ILocalFLogger local72 = FLogger.INSTANCE.getLocal();
                    local72.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                    if (dianaPresetWatchAppSetting == null) {
                    }
                }
            }
            dianaPresetWatchAppSetting7 = null;
            ILocalFLogger local62 = FLogger.INSTANCE.getLocal();
            local62.d("DianaCustomizeEditPresenter", "setPresetToWatch missingPermissionWatchApp " + dianaPresetWatchAppSetting7);
            if (dianaPresetWatchAppSetting7 == null) {
            }
        } else if (i == 2) {
            it = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon10;
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting8 = (DianaPresetWatchAppSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon8;
            List list9 = (List) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon7;
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting9 = (DianaPresetWatchAppSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon6;
            list = (List) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon5;
            DianaPresetComplicationSetting dianaPresetComplicationSetting7 = (DianaPresetComplicationSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon4;
            list4 = (List) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon3;
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting10 = (DianaPresetWatchAppSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon9;
            DianaPresetComplicationSetting dianaPresetComplicationSetting8 = (DianaPresetComplicationSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon2;
            List list10 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon1;
            zg4 zg48 = (zg4) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon0;
            try {
                na4.a(obj);
                obj3 = a3;
                str8 = str14;
                list5 = list9;
                list2 = list10;
                zg42 = zg48;
                str7 = str13;
                str9 = str16;
                dianaPresetComplicationSetting3 = dianaPresetComplicationSetting7;
                dianaPresetComplicationSetting2 = dianaPresetComplicationSetting8;
                obj4 = obj;
                str = "";
                dianaPresetWatchAppSetting6 = dianaPresetWatchAppSetting9;
                dianaPresetWatchAppSetting = dianaPresetWatchAppSetting10;
                str10 = str17;
                dianaPresetWatchAppSetting5 = dianaPresetWatchAppSetting8;
            } catch (Exception e3) {
                e = e3;
                str8 = str14;
                dianaPresetWatchAppSetting3 = dianaPresetWatchAppSetting8;
                zg4 = zg48;
                obj2 = a3;
                str3 = str13;
                dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting9;
                dianaPresetComplicationSetting = dianaPresetComplicationSetting7;
                dianaPresetComplicationSetting2 = dianaPresetComplicationSetting8;
                str = "";
                list3 = list9;
                list2 = list10;
                str9 = str16;
                str2 = str17;
                ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
                DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon13 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12;
                StringBuilder sb = new StringBuilder();
                dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting4;
                str6 = str9;
                sb.append(str6);
                sb.append(e);
                local8.e("DianaCustomizeEditPresenter", sb.toString());
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon13;
                str7 = str3;
                str17 = str2;
                zg42 = zg4;
                str16 = str6;
                dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting2;
                if (!it.hasNext()) {
                }
                ILocalFLogger local722 = FLogger.INSTANCE.getLocal();
                local722.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                if (dianaPresetWatchAppSetting == null) {
                }
            }
            try {
                parcelable = (Parcelable) obj4;
                ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
                StringBuilder sb2 = new StringBuilder();
            } catch (Exception e4) {
                e = e4;
                zg43 = zg42;
            }
            zg43 = zg42;
            sb2.append("last setting ");
            sb2.append(parcelable);
            local9.d("DianaCustomizeEditPresenter", sb2.toString());
            if (parcelable == null) {
                dianaPresetWatchAppSetting.setSettings(dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.j.a((Object) parcelable));
                dianaPresetComplicationSetting = dianaPresetComplicationSetting3;
                str2 = str10;
                obj2 = obj3;
                str6 = str9;
                str3 = str7;
                zg4 = zg43;
                dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting6;
                list3 = list5;
                dianaPresetWatchAppSetting3 = dianaPresetWatchAppSetting5;
                str7 = str3;
                str17 = str2;
                zg42 = zg4;
                str16 = str6;
                dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting2;
                if (!it.hasNext()) {
                    DianaPresetWatchAppSetting dianaPresetWatchAppSetting11 = (DianaPresetWatchAppSetting) it.next();
                    str9 = str16;
                    ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
                    str10 = str17;
                    StringBuilder sb3 = new StringBuilder();
                    Object obj8 = obj2;
                    String str18 = str5;
                    sb3.append(str18);
                    sb3.append(dianaPresetWatchAppSetting11);
                    local10.d("DianaCustomizeEditPresenter", sb3.toString());
                    try {
                    } catch (Exception e5) {
                        e = e5;
                        zg4 = zg42;
                        str8 = str18;
                        str2 = str10;
                        str3 = str7;
                        obj5 = obj8;
                    }
                    if (oj2.a(dianaPresetWatchAppSetting11.getSettings())) {
                        try {
                            a2 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.b();
                        } catch (Exception e6) {
                            e = e6;
                            str8 = str18;
                        }
                        str8 = str18;
                        Anon2 anon2 = new Anon2(dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12, dianaPresetWatchAppSetting11, (yb4) null);
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon0 = zg42;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon1 = list2;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon2 = dianaPresetComplicationSetting2;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon3 = list4;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon4 = dianaPresetComplicationSetting;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon5 = list;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon6 = dianaPresetWatchAppSetting4;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon7 = list3;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon8 = dianaPresetWatchAppSetting3;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon9 = dianaPresetWatchAppSetting11;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon10 = it;
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.label = 2;
                        obj4 = yf4.a(a2, anon2, dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12);
                        Object obj9 = obj8;
                        if (obj4 == obj9) {
                            return obj9;
                        }
                        obj3 = obj9;
                        dianaPresetWatchAppSetting5 = dianaPresetWatchAppSetting3;
                        list5 = list3;
                        dianaPresetWatchAppSetting6 = dianaPresetWatchAppSetting4;
                        dianaPresetComplicationSetting3 = dianaPresetComplicationSetting;
                        dianaPresetWatchAppSetting = dianaPresetWatchAppSetting11;
                        parcelable = (Parcelable) obj4;
                        ILocalFLogger local92 = FLogger.INSTANCE.getLocal();
                        StringBuilder sb22 = new StringBuilder();
                        zg43 = zg42;
                        sb22.append("last setting ");
                        sb22.append(parcelable);
                        local92.d("DianaCustomizeEditPresenter", sb22.toString());
                        if (parcelable == null) {
                            str2 = str10;
                            str3 = str7;
                        }
                        return obj9;
                    }
                    str8 = str18;
                    obj5 = obj8;
                    String id2 = dianaPresetWatchAppSetting11.getId();
                    int hashCode2 = id2.hashCode();
                    zg4 = zg42;
                    if (hashCode2 == -829740640) {
                        str3 = str7;
                        str2 = str10;
                        try {
                        } catch (Exception e7) {
                            e = e7;
                            DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon14 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12;
                            obj2 = obj5;
                            ILocalFLogger local82 = FLogger.INSTANCE.getLocal();
                            DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon132 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12;
                            StringBuilder sb4 = new StringBuilder();
                            dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting4;
                            str6 = str9;
                            sb4.append(str6);
                            sb4.append(e);
                            local82.e("DianaCustomizeEditPresenter", sb4.toString());
                            dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon132;
                            str7 = str3;
                            str17 = str2;
                            zg42 = zg4;
                            str16 = str6;
                            dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting2;
                            if (!it.hasNext()) {
                            }
                            ILocalFLogger local7222 = FLogger.INSTANCE.getLocal();
                            local7222.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                            if (dianaPresetWatchAppSetting == null) {
                            }
                        }
                        if (id2.equals(str2)) {
                            dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12;
                            DianaPresetWatchAppSetting dianaPresetWatchAppSetting12 = dianaPresetWatchAppSetting11;
                            if (((CommuteTimeWatchAppSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.j.a(dianaPresetWatchAppSetting11.getSettings(), CommuteTimeWatchAppSetting.class)).getAddresses().isEmpty()) {
                                dianaPresetWatchAppSetting = dianaPresetWatchAppSetting12;
                                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1;
                            }
                            obj2 = obj5;
                            dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1;
                            dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting4;
                            str6 = str9;
                            str7 = str3;
                            str17 = str2;
                            zg42 = zg4;
                            str16 = str6;
                            dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting2;
                            if (!it.hasNext()) {
                                DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon15 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12;
                                str2 = str17;
                                str3 = str7;
                                dianaPresetWatchAppSetting = dianaPresetWatchAppSetting3;
                            }
                        }
                    } else if (hashCode2 != 1223440372) {
                        str2 = str10;
                        str3 = str7;
                    } else {
                        str3 = str7;
                        try {
                        } catch (Exception e8) {
                            e = e8;
                            obj2 = obj5;
                            str2 = str10;
                            ILocalFLogger local822 = FLogger.INSTANCE.getLocal();
                            DianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1322 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12;
                            StringBuilder sb42 = new StringBuilder();
                            dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting4;
                            str6 = str9;
                            sb42.append(str6);
                            sb42.append(e);
                            local822.e("DianaCustomizeEditPresenter", sb42.toString());
                            dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1322;
                            str7 = str3;
                            str17 = str2;
                            zg42 = zg4;
                            str16 = str6;
                            dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting2;
                            if (!it.hasNext()) {
                            }
                            ILocalFLogger local72222 = FLogger.INSTANCE.getLocal();
                            local72222.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                            if (dianaPresetWatchAppSetting == null) {
                            }
                        }
                        if (id2.equals(str3)) {
                            WeatherWatchAppSetting weatherWatchAppSetting = (WeatherWatchAppSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.j.a(dianaPresetWatchAppSetting11.getSettings(), WeatherWatchAppSetting.class);
                        }
                        str2 = str10;
                    }
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12;
                    obj2 = obj5;
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon1;
                    dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting4;
                    str6 = str9;
                    str7 = str3;
                    str17 = str2;
                    zg42 = zg4;
                    str16 = str6;
                    dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting2;
                    if (!it.hasNext()) {
                    }
                }
                ILocalFLogger local722222 = FLogger.INSTANCE.getLocal();
                local722222.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
                if (dianaPresetWatchAppSetting == null) {
                    String id3 = dianaPresetWatchAppSetting.getId();
                    int hashCode3 = id3.hashCode();
                    if (hashCode3 != -829740640) {
                        if (hashCode3 == 1223440372 && id3.equals(str3)) {
                            str4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.set_to_watch_fail_weather_app_not_configured);
                            kd4.a((Object) str4, "LanguageHelper.getString\u2026ather_app_not_configured)");
                            dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.n.b(str4, dianaPresetWatchAppSetting.getId(), dianaPresetWatchAppSetting.getPosition(), false);
                            return qa4.a;
                        }
                    } else if (id3.equals(str2)) {
                        str4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_CommuteTimeError___ToDisplayCommuteTimePleaseEnter);
                        kd4.a((Object) str4, "LanguageHelper.getString\u2026ayCommuteTimePleaseEnter)");
                        dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.n.b(str4, dianaPresetWatchAppSetting.getId(), dianaPresetWatchAppSetting.getPosition(), false);
                        return qa4.a;
                    }
                    str4 = str;
                    dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.n.b(str4, dianaPresetWatchAppSetting.getId(), dianaPresetWatchAppSetting.getPosition(), false);
                    return qa4.a;
                }
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.n.l();
                if (dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.k() == 1) {
                    ul2 = AnalyticsHelper.f.b("set_complication");
                } else {
                    ul2 = AnalyticsHelper.f.b("set_watch_apps");
                }
                ul2.d();
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.q.a(new SetDianaPresetToWatchUseCase.c(dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.$currentPreset$inlined), new a(dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12, ul2));
                return qa4.a;
            }
            str2 = str10;
            str3 = str7;
            ILocalFLogger local7222222 = FLogger.INSTANCE.getLocal();
            local7222222.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingWatchApp " + dianaPresetWatchAppSetting);
            if (dianaPresetWatchAppSetting == null) {
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (!it2.hasNext()) {
            DianaPresetComplicationSetting dianaPresetComplicationSetting9 = (DianaPresetComplicationSetting) it2.next();
            str7 = str13;
            ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
            str11 = str15;
            local11.d("DianaCustomizeEditPresenter", str14 + dianaPresetComplicationSetting9);
            try {
            } catch (Exception e9) {
                e = e9;
                str5 = str14;
            }
            if (oj2.a(dianaPresetComplicationSetting9.getSettings())) {
                ug4 a6 = dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.b();
                str5 = str14;
                Anon1 anon1 = new Anon1(dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12, dianaPresetComplicationSetting9, (yb4) null);
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon0 = zg42;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon1 = list7;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon2 = dianaPresetComplicationSetting2;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon3 = list6;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon4 = dianaPresetComplicationSetting4;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon5 = dianaPresetComplicationSetting9;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.L$Anon6 = it2;
                dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.label = 1;
                obj7 = yf4.a(a6, anon1, dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12);
                if (obj7 == obj6) {
                    return obj6;
                }
                zg44 = zg42;
                list8 = list7;
                dianaPresetComplicationSetting = dianaPresetComplicationSetting9;
                parcelable2 = (Parcelable) obj7;
                ILocalFLogger local52 = FLogger.INSTANCE.getLocal();
                local52.d("DianaCustomizeEditPresenter", "last setting " + parcelable2);
                if (parcelable2 == null) {
                    list2 = list8;
                    zg42 = zg44;
                }
                list2 = list8;
                zg42 = zg44;
                return obj6;
            }
            str5 = str14;
            String id4 = dianaPresetComplicationSetting9.getId();
            int hashCode4 = id4.hashCode();
            if (hashCode4 == -829740640) {
                if (id4.equals(str17)) {
                }
                str13 = str7;
                str15 = str11;
                str14 = str5;
                if (!it2.hasNext()) {
                    str7 = str13;
                    str5 = str14;
                    str11 = str15;
                    List list11 = list7;
                    dianaPresetComplicationSetting = dianaPresetComplicationSetting4;
                    list2 = list11;
                }
            } else {
                if (hashCode4 == 134170930) {
                    if (id4.equals("second-timezone") && TextUtils.isEmpty(((SecondTimezoneSetting) dianaCustomizeEditPresenter$setPresetToWatch$$inlined$let$lambda$Anon12.this$Anon0.j.a(dianaPresetComplicationSetting9.getSettings(), SecondTimezoneSetting.class)).getTimeZoneId())) {
                    }
                }
                str13 = str7;
                str15 = str11;
                str14 = str5;
                if (!it2.hasNext()) {
                }
            }
            list2 = list7;
            dianaPresetComplicationSetting = dianaPresetComplicationSetting9;
            e = e;
            ILocalFLogger local422 = FLogger.INSTANCE.getLocal();
            local422.e("DianaCustomizeEditPresenter", str16 + e);
            str13 = str7;
            str15 = str11;
            str14 = str5;
            if (!it2.hasNext()) {
            }
        }
        ILocalFLogger local22222 = FLogger.INSTANCE.getLocal();
        local22222.d("DianaCustomizeEditPresenter", "setPresetToWatch missingSettingComplication " + dianaPresetComplicationSetting);
        if (dianaPresetComplicationSetting != null) {
        }
    }
}
