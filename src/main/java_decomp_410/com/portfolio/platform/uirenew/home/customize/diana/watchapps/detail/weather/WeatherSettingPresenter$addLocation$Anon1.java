package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.tn1;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherSettingPresenter$addLocation$Anon1<TResult> implements tn1<FetchPlaceResponse> {
    @DexIgnore
    public /* final */ /* synthetic */ WeatherSettingPresenter a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;

    @DexIgnore
    public WeatherSettingPresenter$addLocation$Anon1(WeatherSettingPresenter weatherSettingPresenter, String str, String str2) {
        this.a = weatherSettingPresenter;
        this.b = str;
        this.c = str2;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
        this.a.i.a();
        kd4.a((Object) fetchPlaceResponse, "response");
        Place place = fetchPlaceResponse.getPlace();
        kd4.a((Object) place, "response.place");
        LatLng latLng = place.getLatLng();
        if (latLng != null) {
            fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new WeatherSettingPresenter$addLocation$Anon1$$special$$inlined$let$lambda$Anon1(latLng, (yb4) null, this), 3, (Object) null);
        }
    }
}
