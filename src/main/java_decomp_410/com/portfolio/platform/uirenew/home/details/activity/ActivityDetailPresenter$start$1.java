package com.portfolio.platform.uirenew.home.details.activity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1", mo27670f = "ActivityDetailPresenter.kt", mo27671l = {114}, mo27672m = "invokeSuspend")
public final class ActivityDetailPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23598p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$1", mo27670f = "ActivityDetailPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$1 */
    public static final class C66891 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.enums.Unit>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23599p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C66891(com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1 activityDetailPresenter$start$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = activityDetailPresenter$start$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66891 r0 = new com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66891(this.this$0, yb4);
            r0.f23599p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66891) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.portfolio.platform.data.model.MFUser currentUser = this.this$0.this$0.f23583v.getCurrentUser();
                if (currentUser != null) {
                    return currentUser.getDistanceUnit();
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$2")
    /* renamed from: com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$2 */
    public static final class C66902<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1 f23600a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$2$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$2$1", mo27670f = "ActivityDetailPresenter.kt", mo27671l = {125}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$2$1 */
        public static final class C66911 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f23601p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66902 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C66911(com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66902 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66902.C66911 r0 = new com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66902.C66911(this.this$0, yb4);
                r0.f23601p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66902.C66911) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23601p$;
                    com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.f23600a.this$0.mo31440b();
                    com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$2$1$summary$1 activityDetailPresenter$start$1$2$1$summary$1 = new com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$2$1$summary$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg4;
                    this.label = 1;
                    obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, activityDetailPresenter$start$1$2$1$summary$1, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                com.portfolio.platform.data.model.room.fitness.ActivitySummary activitySummary = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) obj;
                if (this.this$0.f23600a.this$0.f23574m == null || (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23600a.this$0.f23574m, (java.lang.Object) activitySummary))) {
                    this.this$0.f23600a.this$0.f23574m = activitySummary;
                    this.this$0.f23600a.this$0.f23580s.mo29128a(this.this$0.f23600a.this$0.f23576o, this.this$0.f23600a.this$0.f23574m);
                    if (this.this$0.f23600a.this$0.f23570i && this.this$0.f23600a.this$0.f23571j) {
                        com.fossil.blesdk.obfuscated.fi4 unused = this.this$0.f23600a.this$0.mo41371l();
                    }
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        public C66902(com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1 activityDetailPresenter$start$1) {
            this.f23600a = activityDetailPresenter$start$1;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>> os3) {
            com.portfolio.platform.enums.Status a = os3.mo29972a();
            java.util.List list = (java.util.List) os3.mo29973b();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("start - summaryTransformations -- activitySummaries=");
            sb.append(list != null ? java.lang.Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a);
            local.mo33255d("ActivityDetailPresenter", sb.toString());
            if (a == com.portfolio.platform.enums.Status.NETWORK_LOADING || a == com.portfolio.platform.enums.Status.SUCCESS) {
                this.f23600a.this$0.f23572k = list;
                this.f23600a.this$0.f23570i = true;
                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23600a.this$0.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66902.C66911(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$3")
    /* renamed from: com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$3 */
    public static final class C66923<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1 f23602a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$3$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$3$1", mo27670f = "ActivityDetailPresenter.kt", mo27671l = {148}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$3$1 */
        public static final class C66931 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
            @DexIgnore
            public java.lang.Object L$0;
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f23603p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66923 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C66931(com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66923 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66923.C66931 r0 = new com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66923.C66931(this.this$0, yb4);
                r0.f23603p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66923.C66931) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
                int i = this.label;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23603p$;
                    com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.f23602a.this$0.mo31440b();
                    com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$3$1$samples$1 activityDetailPresenter$start$1$3$1$samples$1 = new com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$3$1$samples$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg4;
                    this.label = 1;
                    obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, activityDetailPresenter$start$1$3$1$samples$1, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                java.util.List list = (java.util.List) obj;
                if (this.this$0.f23602a.this$0.f23575n == null || (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.f23602a.this$0.f23575n, (java.lang.Object) list))) {
                    this.this$0.f23602a.this$0.f23575n = list;
                    if (this.this$0.f23602a.this$0.f23570i && this.this$0.f23602a.this$0.f23571j) {
                        com.fossil.blesdk.obfuscated.fi4 unused = this.this$0.f23602a.this$0.mo41371l();
                    }
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        }

        @DexIgnore
        public C66923(com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1 activityDetailPresenter$start$1) {
            this.f23602a = activityDetailPresenter$start$1;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>> os3) {
            com.portfolio.platform.enums.Status a = os3.mo29972a();
            java.util.List list = (java.util.List) os3.mo29973b();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("start - sampleTransformations -- activitySamples=");
            sb.append(list != null ? java.lang.Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a);
            local.mo33255d("ActivityDetailPresenter", sb.toString());
            if (a == com.portfolio.platform.enums.Status.NETWORK_LOADING || a == com.portfolio.platform.enums.Status.SUCCESS) {
                this.f23602a.this$0.f23573l = list;
                this.f23602a.this$0.f23571j = true;
                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23602a.this$0.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66923.C66931(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityDetailPresenter$start$1(com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter activityDetailPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = activityDetailPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1 activityDetailPresenter$start$1 = new com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1(this.this$0, yb4);
        activityDetailPresenter$start$1.f23598p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return activityDetailPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter activityDetailPresenter;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23598p$;
            com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter activityDetailPresenter2 = this.this$0;
            com.fossil.blesdk.obfuscated.ug4 a2 = activityDetailPresenter2.mo31440b();
            com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66891 r4 = new com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66891(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = activityDetailPresenter2;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r4, this);
            if (obj == a) {
                return a;
            }
            activityDetailPresenter = activityDetailPresenter2;
        } else if (i == 1) {
            activityDetailPresenter = (com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.kd4.m24407a(obj, "withContext(commonPool) \u2026ntUser()!!.distanceUnit }");
        activityDetailPresenter.f23576o = (com.portfolio.platform.enums.Unit) obj;
        androidx.lifecycle.LiveData q = this.this$0.f23577p;
        com.fossil.blesdk.obfuscated.le3 o = this.this$0.f23580s;
        if (o != null) {
            q.mo2277a((com.fossil.blesdk.obfuscated.me3) o, new com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66902(this));
            this.this$0.f23578q.mo2277a((androidx.lifecycle.LifecycleOwner) this.this$0.f23580s, new com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1.C66923(this));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
    }
}
