package com.portfolio.platform.uirenew.home;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$1", mo27670f = "HomePresenter.kt", mo27671l = {236}, mo27672m = "invokeSuspend")
public final class HomePresenter$checkLastOTASuccess$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $activeSerial;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $lastFwTemp;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22296p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.HomePresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomePresenter$checkLastOTASuccess$1(com.portfolio.platform.uirenew.home.HomePresenter homePresenter, java.lang.String str, java.lang.String str2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = homePresenter;
        this.$activeSerial = str;
        this.$lastFwTemp = str2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$1 homePresenter$checkLastOTASuccess$1 = new com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$1(this.this$0, this.$activeSerial, this.$lastFwTemp, yb4);
        homePresenter$checkLastOTASuccess$1.f22296p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return homePresenter$checkLastOTASuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        java.lang.String str = null;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22296p$;
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$1$activeDevice$1 homePresenter$checkLastOTASuccess$1$activeDevice$1 = new com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$1$activeDevice$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b, homePresenter$checkLastOTASuccess$1$activeDevice$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.model.Device device = (com.portfolio.platform.data.model.Device) obj;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a2 = com.portfolio.platform.uirenew.home.HomePresenter.f22267y.mo40748a();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("Inside .checkLastOTASuccess, getDeviceBySerial success currentDeviceFw ");
        if (device != null) {
            str = device.getFirmwareRevision();
        }
        sb.append(str);
        local.mo33255d(a2, sb.toString());
        if (device != null && !com.fossil.blesdk.obfuscated.qf4.m26954b(this.$lastFwTemp, device.getFirmwareRevision(), true)) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.HomePresenter.f22267y.mo40748a(), "Handle OTA complete on check last OTA success");
            this.this$0.f22275m.mo27061n(this.$activeSerial);
            this.this$0.f22275m.mo26998a(this.this$0.f22277o.mo34532e(), -1);
            this.this$0.f22274l.mo30765b(true);
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
