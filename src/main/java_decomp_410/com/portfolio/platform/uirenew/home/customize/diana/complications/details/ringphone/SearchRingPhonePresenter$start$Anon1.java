package com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.helper.AppHelper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter$start$Anon1", f = "SearchRingPhonePresenter.kt", l = {29}, m = "invokeSuspend")
public final class SearchRingPhonePresenter$start$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SearchRingPhonePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter$start$Anon1$Anon1", f = "SearchRingPhonePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<? extends Ringtone>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;

        @DexIgnore
        public Anon1(yb4 yb4) {
            super(2, yb4);
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return AppHelper.f.c();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SearchRingPhonePresenter$start$Anon1(SearchRingPhonePresenter searchRingPhonePresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = searchRingPhonePresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SearchRingPhonePresenter$start$Anon1 searchRingPhonePresenter$start$Anon1 = new SearchRingPhonePresenter$start$Anon1(this.this$Anon0, yb4);
        searchRingPhonePresenter$start$Anon1.p$ = (zg4) obj;
        return searchRingPhonePresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SearchRingPhonePresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ArrayList arrayList;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            if (this.this$Anon0.f.isEmpty()) {
                ArrayList b = this.this$Anon0.f;
                ug4 a2 = this.this$Anon0.b();
                Anon1 anon1 = new Anon1((yb4) null);
                this.L$Anon0 = zg4;
                this.L$Anon1 = b;
                this.label = 1;
                obj = yf4.a(a2, anon1, this);
                if (obj == a) {
                    return a;
                }
                arrayList = b;
            }
            this.this$Anon0.j().r(this.this$Anon0.f);
            return qa4.a;
        } else if (i == 1) {
            arrayList = (ArrayList) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        arrayList.addAll((Collection) obj);
        if (this.this$Anon0.g == null) {
            SearchRingPhonePresenter searchRingPhonePresenter = this.this$Anon0;
            searchRingPhonePresenter.g = (Ringtone) searchRingPhonePresenter.f.get(0);
        }
        this.this$Anon0.j().r(this.this$Anon0.f);
        return qa4.a;
    }
}
