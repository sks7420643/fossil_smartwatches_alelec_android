package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hz2;
import com.fossil.blesdk.obfuscated.iz2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationContactsAndAppsAssignedActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public NotificationContactsAndAppsAssignedPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, int i) {
            kd4.b(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationContactsAndAppsAssignedActivity.class);
            intent.putExtra("EXTRA_HAND_NUMBER", i);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        hz2 hz2 = (hz2) getSupportFragmentManager().a((int) R.id.content);
        if (hz2 == null) {
            hz2 = hz2.q.b();
            a((Fragment) hz2, hz2.q.a(), (int) R.id.content);
        }
        l42 g = PortfolioApp.W.c().g();
        if (hz2 != null) {
            int intExtra = getIntent().getIntExtra("EXTRA_HAND_NUMBER", 0);
            LoaderManager supportLoaderManager = getSupportLoaderManager();
            kd4.a((Object) supportLoaderManager, "supportLoaderManager");
            g.a(new iz2(hz2, intExtra, supportLoaderManager)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedContract.View");
    }
}
