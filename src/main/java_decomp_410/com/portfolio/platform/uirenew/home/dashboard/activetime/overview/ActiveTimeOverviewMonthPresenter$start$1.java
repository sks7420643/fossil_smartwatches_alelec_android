package com.portfolio.platform.uirenew.home.dashboard.activetime.overview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActiveTimeOverviewMonthPresenter$start$1<T> implements com.fossil.blesdk.obfuscated.C1548cc<com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>>> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter f23165a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1$1", mo27670f = "ActiveTimeOverviewMonthPresenter.kt", mo27671l = {61}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1$1 */
    public static final class C65781 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $data;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public java.lang.Object L$1;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23166p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1$1$1")
        @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1$1$1", mo27670f = "ActiveTimeOverviewMonthPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
        /* renamed from: com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1$1$1 */
        public static final class C65791 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.TreeMap<java.lang.Long, java.lang.Float>>, java.lang.Object> {
            @DexIgnore
            public int label;

            @DexIgnore
            /* renamed from: p$ */
            public com.fossil.blesdk.obfuscated.zg4 f23167p$;
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1.C65781 this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C65791(com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1.C65781 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
                super(2, yb4);
                this.this$0 = r1;
            }

            @DexIgnore
            public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
                com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
                com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1.C65781.C65791 r0 = new com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1.C65781.C65791(this.this$0, yb4);
                r0.f23167p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
                return r0;
            }

            @DexIgnore
            public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
                return ((com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1.C65781.C65791) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
            }

            @DexIgnore
            public final java.lang.Object invokeSuspend(java.lang.Object obj) {
                com.fossil.blesdk.obfuscated.cc4.m20546a();
                if (this.label == 0) {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter activeTimeOverviewMonthPresenter = this.this$0.this$0.f23165a;
                    java.lang.Object a = activeTimeOverviewMonthPresenter.f23150f.mo2275a();
                    if (a != null) {
                        com.fossil.blesdk.obfuscated.kd4.m24407a(a, "mDate.value!!");
                        return activeTimeOverviewMonthPresenter.mo41224a((java.util.Date) a, (java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>) this.this$0.$data);
                    }
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C65781(com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1 activeTimeOverviewMonthPresenter$start$1, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = activeTimeOverviewMonthPresenter$start$1;
            this.$data = list;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1.C65781 r0 = new com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1.C65781(this.this$0, this.$data, yb4);
            r0.f23166p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1.C65781) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter activeTimeOverviewMonthPresenter;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f23166p$;
                com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter activeTimeOverviewMonthPresenter2 = this.this$0.f23165a;
                com.fossil.blesdk.obfuscated.ug4 b = activeTimeOverviewMonthPresenter2.mo31441c();
                com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1.C65781.C65791 r4 = new com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1.C65781.C65791(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = activeTimeOverviewMonthPresenter2;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b, r4, this);
                if (obj == a) {
                    return a;
                }
                activeTimeOverviewMonthPresenter = activeTimeOverviewMonthPresenter2;
            } else if (i == 1) {
                activeTimeOverviewMonthPresenter = (com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            activeTimeOverviewMonthPresenter.f23158n = (java.util.TreeMap) obj;
            com.fossil.blesdk.obfuscated.p83 m = this.this$0.f23165a.f23159o;
            java.util.TreeMap e = this.this$0.f23165a.f23158n;
            if (e == null) {
                e = new java.util.TreeMap();
            }
            m.mo30096a(e);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public ActiveTimeOverviewMonthPresenter$start$1(com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter activeTimeOverviewMonthPresenter) {
        this.f23165a = activeTimeOverviewMonthPresenter;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    public final void mo8689a(com.fossil.blesdk.obfuscated.os3<? extends java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>> os3) {
        java.lang.Integer num;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("mDateTransformations - status=");
        sb.append(os3 != null ? os3.mo29978f() : null);
        sb.append(" -- data.size=");
        if (os3 != null) {
            java.util.List list = (java.util.List) os3.mo29975d();
            if (list != null) {
                num = java.lang.Integer.valueOf(list.size());
                sb.append(num);
                local.mo33255d("ActiveTimeOverviewMonthPresenter", sb.toString());
                if ((os3 == null ? os3.mo29978f() : null) == com.portfolio.platform.enums.Status.DATABASE_LOADING) {
                    java.util.List list2 = os3 != null ? (java.util.List) os3.mo29975d() : null;
                    if (list2 != null && (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.f23165a.f23156l, (java.lang.Object) list2))) {
                        this.f23165a.f23156l = list2;
                        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23165a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1.C65781(this, list2, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                        return;
                    }
                    return;
                }
                return;
            }
        }
        num = null;
        sb.append(num);
        local.mo33255d("ActiveTimeOverviewMonthPresenter", sb.toString());
        if ((os3 == null ? os3.mo29978f() : null) == com.portfolio.platform.enums.Status.DATABASE_LOADING) {
        }
    }
}
