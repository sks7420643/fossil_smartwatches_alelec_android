package com.portfolio.platform.uirenew.home.customize.hybrid;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel", f = "HybridCustomizeViewModel.kt", l = {269, 270}, m = "initializePreset")
public final class HybridCustomizeViewModel$initializePreset$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ HybridCustomizeViewModel this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridCustomizeViewModel$initializePreset$Anon1(HybridCustomizeViewModel hybridCustomizeViewModel, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = hybridCustomizeViewModel;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((String) null, (yb4<? super qa4>) this);
    }
}
