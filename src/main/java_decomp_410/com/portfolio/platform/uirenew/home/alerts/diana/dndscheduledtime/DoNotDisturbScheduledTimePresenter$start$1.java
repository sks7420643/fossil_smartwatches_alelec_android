package com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$start$1", mo27670f = "DoNotDisturbScheduledTimePresenter.kt", mo27671l = {35}, mo27672m = "invokeSuspend")
public final class DoNotDisturbScheduledTimePresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22487p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DoNotDisturbScheduledTimePresenter$start$1(com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = doNotDisturbScheduledTimePresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$start$1 doNotDisturbScheduledTimePresenter$start$1 = new com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$start$1(this.this$0, yb4);
        doNotDisturbScheduledTimePresenter$start$1.f22487p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return doNotDisturbScheduledTimePresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22487p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.C6361x4ca48724 doNotDisturbScheduledTimePresenter$start$1$listDNDScheduledTimeModel$1 = new com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.C6361x4ca48724(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, doNotDisturbScheduledTimePresenter$start$1$listDNDScheduledTimeModel$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        for (com.portfolio.platform.data.model.DNDScheduledTimeModel dNDScheduledTimeModel : (java.util.List) obj) {
            if (dNDScheduledTimeModel.getScheduledTimeType() == this.this$0.f22476g) {
                com.fossil.blesdk.obfuscated.sy2 g = this.this$0.f22479j;
                com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter = this.this$0;
                g.mo30264d(doNotDisturbScheduledTimePresenter.mo40872b(doNotDisturbScheduledTimePresenter.f22476g));
                this.this$0.f22479j.mo30260a(dNDScheduledTimeModel.getMinutes());
            }
            if (dNDScheduledTimeModel.getScheduledTimeType() == 0) {
                this.this$0.f22477h = dNDScheduledTimeModel;
            } else {
                this.this$0.f22478i = dNDScheduledTimeModel;
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
