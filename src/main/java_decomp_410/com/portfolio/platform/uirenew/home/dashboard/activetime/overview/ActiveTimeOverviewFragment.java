package com.portfolio.platform.uirenew.home.dashboard.activetime.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.cu3;
import com.fossil.blesdk.obfuscated.f9;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.g83;
import com.fossil.blesdk.obfuscated.k83;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.q83;
import com.fossil.blesdk.obfuscated.qa;
import com.fossil.blesdk.obfuscated.s82;
import com.fossil.blesdk.obfuscated.tr3;
import com.fossil.blesdk.obfuscated.v83;
import com.fossil.blesdk.obfuscated.xe;
import com.fossil.blesdk.obfuscated.zr2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActiveTimeOverviewFragment extends zr2 {
    @DexIgnore
    public tr3<s82> j;
    @DexIgnore
    public ActiveTimeOverviewDayPresenter k;
    @DexIgnore
    public ActiveTimeOverviewWeekPresenter l;
    @DexIgnore
    public ActiveTimeOverviewMonthPresenter m;
    @DexIgnore
    public g83 n;
    @DexIgnore
    public v83 o;
    @DexIgnore
    public q83 p;
    @DexIgnore
    public int q; // = 7;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment e;

        @DexIgnore
        public b(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.e = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.e;
            tr3 a = activeTimeOverviewFragment.j;
            activeTimeOverviewFragment.a(7, a != null ? (s82) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment e;

        @DexIgnore
        public c(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.e = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.e;
            tr3 a = activeTimeOverviewFragment.j;
            activeTimeOverviewFragment.a(4, a != null ? (s82) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment e;

        @DexIgnore
        public d(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.e = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.e;
            tr3 a = activeTimeOverviewFragment.j;
            activeTimeOverviewFragment.a(2, a != null ? (s82) a.a() : null);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ActiveTimeOverviewFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "onCreateView");
        s82 s82 = (s82) qa.a(layoutInflater, R.layout.fragment_active_time_overview, viewGroup, false, O0());
        f9.c((View) s82.t, false);
        if (bundle != null) {
            this.q = bundle.getInt("CURRENT_TAB", 7);
        }
        kd4.a((Object) s82, "binding");
        a(s82);
        this.j = new tr3<>(this, s82);
        tr3<s82> tr3 = this.j;
        if (tr3 != null) {
            s82 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        kd4.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.q);
    }

    @DexIgnore
    public final void a(s82 s82) {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "initUI");
        this.n = (g83) getChildFragmentManager().a("ActiveTimeOverviewDayFragment");
        this.o = (v83) getChildFragmentManager().a("ActiveTimeOverviewWeekFragment");
        this.p = (q83) getChildFragmentManager().a("ActiveTimeOverviewMonthFragment");
        if (this.n == null) {
            this.n = new g83();
        }
        if (this.o == null) {
            this.o = new v83();
        }
        if (this.p == null) {
            this.p = new q83();
        }
        ArrayList arrayList = new ArrayList();
        g83 g83 = this.n;
        if (g83 != null) {
            arrayList.add(g83);
            v83 v83 = this.o;
            if (v83 != null) {
                arrayList.add(v83);
                q83 q83 = this.p;
                if (q83 != null) {
                    arrayList.add(q83);
                    RecyclerView recyclerView = s82.t;
                    kd4.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new cu3(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new ActiveTimeOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new xe().a(recyclerView);
                    a(this.q, s82);
                    l42 g = PortfolioApp.W.c().g();
                    g83 g832 = this.n;
                    if (g832 != null) {
                        v83 v832 = this.o;
                        if (v832 != null) {
                            q83 q832 = this.p;
                            if (q832 != null) {
                                g.a(new k83(g832, v832, q832)).a(this);
                                s82.s.setOnClickListener(new b(this));
                                s82.q.setOnClickListener(new c(this));
                                s82.r.setOnClickListener(new d(this));
                                return;
                            }
                            kd4.a();
                            throw null;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i, s82 s82) {
        if (s82 != null) {
            FlexibleTextView flexibleTextView = s82.s;
            kd4.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = s82.q;
            kd4.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = s82.r;
            kd4.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = s82.s;
            kd4.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = s82.q;
            kd4.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = s82.r;
            kd4.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i == 2) {
                FlexibleTextView flexibleTextView7 = s82.r;
                kd4.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = s82.r;
                kd4.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = s82.q;
                kd4.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                tr3<s82> tr3 = this.j;
                if (tr3 != null) {
                    s82 a2 = tr3.a();
                    if (a2 != null) {
                        RecyclerView recyclerView = a2.t;
                        if (recyclerView != null) {
                            recyclerView.i(2);
                        }
                    }
                }
            } else if (i == 4) {
                FlexibleTextView flexibleTextView10 = s82.q;
                kd4.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = s82.q;
                kd4.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = s82.q;
                kd4.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                tr3<s82> tr32 = this.j;
                if (tr32 != null) {
                    s82 a3 = tr32.a();
                    if (a3 != null) {
                        RecyclerView recyclerView2 = a3.t;
                        if (recyclerView2 != null) {
                            recyclerView2.i(1);
                        }
                    }
                }
            } else if (i != 7) {
                FlexibleTextView flexibleTextView13 = s82.s;
                kd4.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = s82.s;
                kd4.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = s82.q;
                kd4.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                tr3<s82> tr33 = this.j;
                if (tr33 != null) {
                    s82 a4 = tr33.a();
                    if (a4 != null) {
                        RecyclerView recyclerView3 = a4.t;
                        if (recyclerView3 != null) {
                            recyclerView3.i(0);
                        }
                    }
                }
            } else {
                FlexibleTextView flexibleTextView16 = s82.s;
                kd4.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = s82.s;
                kd4.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = s82.q;
                kd4.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                tr3<s82> tr34 = this.j;
                if (tr34 != null) {
                    s82 a5 = tr34.a();
                    if (a5 != null) {
                        RecyclerView recyclerView4 = a5.t;
                        if (recyclerView4 != null) {
                            recyclerView4.i(0);
                        }
                    }
                }
            }
        }
    }
}
