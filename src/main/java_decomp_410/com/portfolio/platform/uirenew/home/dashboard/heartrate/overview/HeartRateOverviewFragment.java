package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.cu3;
import com.fossil.blesdk.obfuscated.f9;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.gc3;
import com.fossil.blesdk.obfuscated.ic2;
import com.fossil.blesdk.obfuscated.kc3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.qa;
import com.fossil.blesdk.obfuscated.qc3;
import com.fossil.blesdk.obfuscated.tr3;
import com.fossil.blesdk.obfuscated.vc3;
import com.fossil.blesdk.obfuscated.xe;
import com.fossil.blesdk.obfuscated.zr2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateOverviewFragment extends zr2 {
    @DexIgnore
    public tr3<ic2> j;
    @DexIgnore
    public HeartRateOverviewDayPresenter k;
    @DexIgnore
    public HeartRateOverviewWeekPresenter l;
    @DexIgnore
    public HeartRateOverviewMonthPresenter m;
    @DexIgnore
    public gc3 n;
    @DexIgnore
    public vc3 o;
    @DexIgnore
    public qc3 p;
    @DexIgnore
    public int q; // = 7;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewFragment e;

        @DexIgnore
        public b(HeartRateOverviewFragment heartRateOverviewFragment) {
            this.e = heartRateOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HeartRateOverviewFragment heartRateOverviewFragment = this.e;
            tr3 a = heartRateOverviewFragment.j;
            heartRateOverviewFragment.a(7, a != null ? (ic2) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewFragment e;

        @DexIgnore
        public c(HeartRateOverviewFragment heartRateOverviewFragment) {
            this.e = heartRateOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HeartRateOverviewFragment heartRateOverviewFragment = this.e;
            tr3 a = heartRateOverviewFragment.j;
            heartRateOverviewFragment.a(4, a != null ? (ic2) a.a() : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewFragment e;

        @DexIgnore
        public d(HeartRateOverviewFragment heartRateOverviewFragment) {
            this.e = heartRateOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HeartRateOverviewFragment heartRateOverviewFragment = this.e;
            tr3 a = heartRateOverviewFragment.j;
            heartRateOverviewFragment.a(2, a != null ? (ic2) a.a() : null);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HeartRateOverviewFragment";
    }

    @DexIgnore
    public boolean S0() {
        MFLogger.d("HeartRateOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewFragment", "onCreateView");
        ic2 ic2 = (ic2) qa.a(layoutInflater, R.layout.fragment_heartrate_overview, viewGroup, false, O0());
        f9.c((View) ic2.t, false);
        if (bundle != null) {
            this.q = bundle.getInt("CURRENT_TAB", 7);
        }
        kd4.a((Object) ic2, "binding");
        a(ic2);
        this.j = new tr3<>(this, ic2);
        tr3<ic2> tr3 = this.j;
        if (tr3 != null) {
            ic2 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        kd4.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.q);
    }

    @DexIgnore
    public final void a(ic2 ic2) {
        MFLogger.d("HeartRateOverviewFragment", "initUI");
        this.n = (gc3) getChildFragmentManager().a("HeartRateOverviewDayFragment");
        this.o = (vc3) getChildFragmentManager().a("HeartRateOverviewWeekFragment");
        this.p = (qc3) getChildFragmentManager().a("HeartRateOverviewMonthFragment");
        if (this.n == null) {
            this.n = new gc3();
        }
        if (this.o == null) {
            this.o = new vc3();
        }
        if (this.p == null) {
            this.p = new qc3();
        }
        ArrayList arrayList = new ArrayList();
        gc3 gc3 = this.n;
        if (gc3 != null) {
            arrayList.add(gc3);
            vc3 vc3 = this.o;
            if (vc3 != null) {
                arrayList.add(vc3);
                qc3 qc3 = this.p;
                if (qc3 != null) {
                    arrayList.add(qc3);
                    RecyclerView recyclerView = ic2.t;
                    kd4.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new cu3(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new HeartRateOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new xe().a(recyclerView);
                    a(this.q, ic2);
                    l42 g = PortfolioApp.W.c().g();
                    gc3 gc32 = this.n;
                    if (gc32 != null) {
                        vc3 vc32 = this.o;
                        if (vc32 != null) {
                            qc3 qc32 = this.p;
                            if (qc32 != null) {
                                g.a(new kc3(gc32, vc32, qc32)).a(this);
                                ic2.s.setOnClickListener(new b(this));
                                ic2.q.setOnClickListener(new c(this));
                                ic2.r.setOnClickListener(new d(this));
                                return;
                            }
                            kd4.a();
                            throw null;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i, ic2 ic2) {
        if (ic2 != null) {
            FlexibleTextView flexibleTextView = ic2.s;
            kd4.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = ic2.q;
            kd4.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = ic2.r;
            kd4.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = ic2.s;
            kd4.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = ic2.q;
            kd4.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = ic2.r;
            kd4.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i == 2) {
                FlexibleTextView flexibleTextView7 = ic2.r;
                kd4.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = ic2.r;
                kd4.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = ic2.q;
                kd4.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                tr3<ic2> tr3 = this.j;
                if (tr3 != null) {
                    ic2 a2 = tr3.a();
                    if (a2 != null) {
                        RecyclerView recyclerView = a2.t;
                        if (recyclerView != null) {
                            recyclerView.i(2);
                        }
                    }
                }
            } else if (i == 4) {
                FlexibleTextView flexibleTextView10 = ic2.q;
                kd4.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = ic2.q;
                kd4.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = ic2.q;
                kd4.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                tr3<ic2> tr32 = this.j;
                if (tr32 != null) {
                    ic2 a3 = tr32.a();
                    if (a3 != null) {
                        RecyclerView recyclerView2 = a3.t;
                        if (recyclerView2 != null) {
                            recyclerView2.i(1);
                        }
                    }
                }
            } else if (i != 7) {
                FlexibleTextView flexibleTextView13 = ic2.s;
                kd4.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = ic2.s;
                kd4.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = ic2.q;
                kd4.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                tr3<ic2> tr33 = this.j;
                if (tr33 != null) {
                    ic2 a4 = tr33.a();
                    if (a4 != null) {
                        RecyclerView recyclerView3 = a4.t;
                        if (recyclerView3 != null) {
                            recyclerView3.i(0);
                        }
                    }
                }
            } else {
                FlexibleTextView flexibleTextView16 = ic2.s;
                kd4.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = ic2.s;
                kd4.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = ic2.q;
                kd4.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                tr3<ic2> tr34 = this.j;
                if (tr34 != null) {
                    ic2 a5 = tr34.a();
                    if (a5 != null) {
                        RecyclerView recyclerView4 = a5.t;
                        if (recyclerView4 != null) {
                            recyclerView4.i(0);
                        }
                    }
                }
            }
        }
    }
}
