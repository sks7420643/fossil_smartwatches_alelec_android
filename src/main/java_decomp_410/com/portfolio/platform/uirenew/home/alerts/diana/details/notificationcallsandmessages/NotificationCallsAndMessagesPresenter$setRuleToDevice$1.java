package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1", mo27670f = "NotificationCallsAndMessagesPresenter.kt", mo27671l = {371, 373}, mo27672m = "invokeSuspend")
public final class NotificationCallsAndMessagesPresenter$setRuleToDevice$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public long J$0;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22398p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationCallsAndMessagesPresenter$setRuleToDevice$1(com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = notificationCallsAndMessagesPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1 notificationCallsAndMessagesPresenter$setRuleToDevice$1 = new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1(this.this$0, yb4);
        notificationCallsAndMessagesPresenter$setRuleToDevice$1.f22398p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return notificationCallsAndMessagesPresenter$setRuleToDevice$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x017f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x01f2  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        long j;
        java.lang.Object obj2;
        java.util.List list;
        com.fossil.blesdk.obfuscated.gh4 gh4;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.util.List list2;
        com.fossil.blesdk.obfuscated.gh4 gh42;
        java.lang.Object obj3;
        java.util.List list3;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f22398p$;
            this.this$0.f22384t.mo29526b();
            this.this$0.f22384t.mo29523T();
            list2 = new java.util.ArrayList();
            if (this.this$0.f22384t.mo29518E() != this.this$0.mo40831k()) {
                list2.add(new com.portfolio.platform.data.model.NotificationSettingsModel("AllowCallsFrom", this.this$0.f22384t.mo29518E(), true));
            }
            if (this.this$0.f22384t.mo29519J() != this.this$0.mo40830j()) {
                list2.add(new com.portfolio.platform.data.model.NotificationSettingsModel("AllowMessagesFrom", this.this$0.f22384t.mo29519J(), false));
            }
            if (!list2.isEmpty()) {
                this.this$0.mo40826b((java.util.List<com.portfolio.platform.data.model.NotificationSettingsModel>) list2);
            }
            j = java.lang.System.currentTimeMillis();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a();
            local.mo33255d(a2, "filter notification, time start=" + j);
            this.this$0.f22370f.clear();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a3 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a();
            local2.mo33255d(a3, "mListAppWrapper.size = " + this.this$0.mo40833m().size());
            com.fossil.blesdk.obfuscated.gh4 a4 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, this.this$0.mo31440b(), (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.C6330x14ef5c89(this, (com.fossil.blesdk.obfuscated.yb4) null), 2, (java.lang.Object) null);
            gh42 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, this.this$0.mo31440b(), (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.C6329x2c0b6239(this, (com.fossil.blesdk.obfuscated.yb4) null), 2, (java.lang.Object) null);
            java.util.List h = this.this$0.f22370f;
            this.L$0 = zg42;
            this.L$1 = list2;
            this.J$0 = j;
            this.L$2 = a4;
            this.L$3 = gh42;
            this.L$4 = h;
            this.label = 1;
            obj3 = a4.mo27693a(this);
            if (obj3 == a) {
                return a;
            }
            com.fossil.blesdk.obfuscated.gh4 gh43 = a4;
            zg4 = zg42;
            list3 = h;
            gh4 = gh43;
        } else if (i == 1) {
            list3 = (java.util.List) this.L$4;
            long j2 = this.J$0;
            list2 = (java.util.List) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
            j = j2;
            gh42 = (com.fossil.blesdk.obfuscated.gh4) this.L$3;
            obj3 = obj;
        } else if (i == 2) {
            com.fossil.blesdk.obfuscated.gh4 gh44 = (com.fossil.blesdk.obfuscated.gh4) this.L$3;
            com.fossil.blesdk.obfuscated.gh4 gh45 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
            long j3 = this.J$0;
            java.util.List list4 = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            j = j3;
            obj2 = obj;
            list = (java.util.List) obj2;
            if (list == null) {
                this.this$0.f22370f.addAll(list);
                long b = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34515b(new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings(this.this$0.f22370f, java.lang.System.currentTimeMillis()), com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e());
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a5 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a();
                local3.mo33255d(a5, "filter notification, time end= " + b);
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a6 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.f22368C.mo40839a();
                local4.mo33255d(a6, "delayTime =" + (b - j) + " mili seconds");
            } else {
                com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter = this.this$0;
                notificationCallsAndMessagesPresenter.mo40824a(notificationCallsAndMessagesPresenter.mo40834n());
                this.this$0.f22384t.mo29524a();
                this.this$0.f22384t.mo29520N();
                this.this$0.f22384t.close();
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        list3.addAll((java.util.Collection) obj3);
        this.L$0 = zg4;
        this.L$1 = list2;
        this.J$0 = j;
        this.L$2 = gh4;
        this.L$3 = gh42;
        this.label = 2;
        obj2 = gh42.mo27693a(this);
        if (obj2 == a) {
            return a;
        }
        list = (java.util.List) obj2;
        if (list == null) {
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
