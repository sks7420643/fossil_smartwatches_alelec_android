package com.portfolio.platform.uirenew.home.profile;

import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.ev2;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.og3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$Anon1", f = "HomeProfilePresenter.kt", l = {355}, m = "invokeSuspend")
public final class HomeProfilePresenter$openCameraIntent$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeProfilePresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeProfilePresenter$openCameraIntent$Anon1(HomeProfilePresenter homeProfilePresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = homeProfilePresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        HomeProfilePresenter$openCameraIntent$Anon1 homeProfilePresenter$openCameraIntent$Anon1 = new HomeProfilePresenter$openCameraIntent$Anon1(this.this$Anon0, yb4);
        homeProfilePresenter$openCameraIntent$Anon1.p$ = (zg4) obj;
        return homeProfilePresenter$openCameraIntent$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((HomeProfilePresenter$openCameraIntent$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            og3 l = this.this$Anon0.l();
            if (l != null) {
                FragmentActivity activity = ((ev2) l).getActivity();
                if (activity != null) {
                    ug4 a2 = this.this$Anon0.b();
                    HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1 homeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1 = new HomeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1(activity, (yb4) null);
                    this.L$Anon0 = zg4;
                    this.L$Anon1 = activity;
                    this.L$Anon2 = activity;
                    this.label = 1;
                    obj = yf4.a(a2, homeProfilePresenter$openCameraIntent$Anon1$Anon1$pickerImageIntent$Anon1, this);
                    if (obj == a) {
                        return a;
                    }
                }
                return qa4.a;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
        } else if (i == 1) {
            FragmentActivity fragmentActivity = (FragmentActivity) this.L$Anon2;
            FragmentActivity fragmentActivity2 = (FragmentActivity) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Intent intent = (Intent) obj;
        if (intent != null) {
            ((ev2) this.this$Anon0.l()).startActivityForResult(intent, 1234);
        }
        return qa4.a;
    }
}
