package com.portfolio.platform.uirenew.home.customize.hybrid.microapp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$1$1$grantedPermissionList$1", mo27670f = "MicroAppPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
/* renamed from: com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mPermissionOfSelectedMicroAppTransformations$1$1$grantedPermissionList$1 */
public final class C6553xfbc3c583 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.String[]>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23048p$;

    @DexIgnore
    public C6553xfbc3c583(com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.home.customize.hybrid.microapp.C6553xfbc3c583 microAppPresenter$mPermissionOfSelectedMicroAppTransformations$1$1$grantedPermissionList$1 = new com.portfolio.platform.uirenew.home.customize.hybrid.microapp.C6553xfbc3c583(yb4);
        microAppPresenter$mPermissionOfSelectedMicroAppTransformations$1$1$grantedPermissionList$1.f23048p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return microAppPresenter$mPermissionOfSelectedMicroAppTransformations$1$1$grantedPermissionList$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.home.customize.hybrid.microapp.C6553xfbc3c583) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.ns3.f17135a.mo29712a();
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
