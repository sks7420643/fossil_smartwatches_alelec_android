package com.portfolio.platform.uirenew.home;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomePresenter$startCollectingUserFeedback$1 implements com.portfolio.platform.CoroutineUseCase.C5606e<com.fossil.blesdk.obfuscated.kr2.C4595d, com.fossil.blesdk.obfuscated.kr2.C4593b> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.HomePresenter f22298a;

    @DexIgnore
    public HomePresenter$startCollectingUserFeedback$1(com.portfolio.platform.uirenew.home.HomePresenter homePresenter) {
        this.f22298a = homePresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo29641a(com.fossil.blesdk.obfuscated.kr2.C4593b bVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bVar, "errorValue");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.HomePresenter.f22267y.mo40748a(), "startCollectingUserFeedback onError");
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(com.fossil.blesdk.obfuscated.kr2.C4595d dVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.HomePresenter.f22267y.mo40748a(), "startCollectingUserFeedback onSuccess");
        com.zendesk.sdk.network.impl.ZendeskConfig.INSTANCE.setIdentity(new com.zendesk.sdk.model.access.AnonymousIdentity.Builder().withNameIdentifier(dVar.mo28911f()).withEmailIdentifier(dVar.mo28908c()).build());
        com.zendesk.sdk.network.impl.ZendeskConfig.INSTANCE.setCustomFields(dVar.mo28907b());
        this.f22298a.f22274l.mo30763a((com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration) new com.portfolio.platform.uirenew.home.C6295xc54706fb(dVar));
    }
}
