package com.portfolio.platform.uirenew.home.profile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HomeProfilePresenter$start$4<T> implements com.fossil.blesdk.obfuscated.C1548cc<java.util.List<? extends com.portfolio.platform.data.model.Device>> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter f23815a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4$1", mo27670f = "HomeProfilePresenter.kt", mo27671l = {381, 182, 185}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4$1 */
    public static final class C67481 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.ArrayList $deviceWrappers;
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $devices;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public java.lang.Object L$1;
        @DexIgnore
        public java.lang.Object L$2;
        @DexIgnore
        public java.lang.Object L$3;
        @DexIgnore
        public java.lang.Object L$4;
        @DexIgnore
        public java.lang.Object L$5;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f23816p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C67481(com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4 homeProfilePresenter$start$4, java.util.List list, java.util.ArrayList arrayList, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = homeProfilePresenter$start$4;
            this.$devices = list;
            this.$deviceWrappers = arrayList;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4.C67481 r0 = new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4.C67481(this.this$0, this.$devices, this.$deviceWrappers, yb4);
            r0.f23816p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4.C67481) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v14, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v5, resolved type: com.fossil.blesdk.obfuscated.dl4} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00e2 A[Catch:{ all -> 0x006e }] */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x0135 A[Catch:{ all -> 0x006e }] */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x014b A[Catch:{ all -> 0x006e }] */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x014e A[Catch:{ all -> 0x006e }] */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x015e A[Catch:{ all -> 0x006e }] */
        /* JADX WARNING: Removed duplicated region for block: B:76:0x0264 A[Catch:{ all -> 0x006e }] */
        /* JADX WARNING: Removed duplicated region for block: B:80:0x0291 A[Catch:{ all -> 0x006e }] */
        /* JADX WARNING: Removed duplicated region for block: B:83:0x02fe A[Catch:{ all -> 0x006e }] */
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.dl4 dl4;
            java.util.Iterator it;
            com.portfolio.platform.data.model.Device device;
            com.fossil.blesdk.obfuscated.zg4 zg4;
            java.lang.Object obj2;
            java.lang.String str;
            com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4.C67481 r2;
            int i;
            java.lang.Object obj3;
            java.util.Iterator it2;
            com.fossil.blesdk.obfuscated.zg4 zg42;
            java.lang.Object obj4;
            java.lang.Object obj5;
            com.fossil.blesdk.obfuscated.dl4 dl42;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i2 = this.label;
            if (i2 == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                zg4 = this.f23816p$;
                dl42 = this.this$0.f23815a.f23784o;
                this.L$0 = zg4;
                this.L$1 = dl42;
                this.label = 1;
                if (dl42.mo26535a((java.lang.Object) null, this) == a) {
                    return a;
                }
            } else if (i2 == 1) {
                dl42 = (com.fossil.blesdk.obfuscated.dl4) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else if (i2 == 2) {
                com.portfolio.platform.data.model.Device device2 = (com.portfolio.platform.data.model.Device) this.L$4;
                java.util.Iterator it3 = (java.util.Iterator) this.L$3;
                java.lang.String str2 = (java.lang.String) this.L$2;
                dl4 = this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                obj4 = obj;
                it = it3;
                zg42 = zg43;
                obj5 = a;
                device = device2;
                str = str2;
                r2 = this;
                java.lang.String str3 = (java.lang.String) obj4;
                com.fossil.blesdk.obfuscated.ug4 a2 = r2.this$0.f23815a.mo31440b();
                com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4$1$1$isLatestFw$1 homeProfilePresenter$start$4$1$1$isLatestFw$1 = new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4$1$1$isLatestFw$1(str, (com.fossil.blesdk.obfuscated.yb4) null);
                r2.L$0 = zg42;
                r2.L$1 = dl4;
                r2.L$2 = str;
                r2.L$3 = it;
                r2.L$4 = device;
                r2.L$5 = str3;
                r2.Z$0 = false;
                r2.Z$1 = false;
                r2.label = 3;
                obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, homeProfilePresenter$start$4$1$1$isLatestFw$1, r2);
                if (obj2 != obj5) {
                }
                return obj5;
            } else if (i2 == 3) {
                boolean z = this.Z$1;
                boolean z2 = this.Z$0;
                java.lang.String str4 = (java.lang.String) this.L$5;
                device = (com.portfolio.platform.data.model.Device) this.L$4;
                it = (java.util.Iterator) this.L$3;
                java.lang.String str5 = (java.lang.String) this.L$2;
                dl4 = (com.fossil.blesdk.obfuscated.dl4) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg44 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                try {
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    obj2 = obj;
                    boolean z3 = z;
                    boolean z4 = z2;
                    java.lang.String str6 = str4;
                    str = str5;
                    com.fossil.blesdk.obfuscated.zg4 zg45 = zg44;
                    java.lang.Object obj6 = a;
                    r2 = this;
                    boolean booleanValue = ((java.lang.Boolean) obj2).booleanValue();
                    if (device.getBatteryLevel() <= 100) {
                        i = 100;
                    } else {
                        i = device.getBatteryLevel();
                    }
                    if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) device.getDeviceId(), (java.lang.Object) str)) {
                        java.util.ArrayList arrayList = r2.$deviceWrappers;
                        com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter.C6738b bVar = new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter.C6738b(device.getDeviceId(), z4, str6, i, z3, booleanValue);
                        arrayList.add(bVar);
                    } else {
                        boolean z5 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34537g(device.getDeviceId()) == 2;
                        java.util.ArrayList arrayList2 = r2.$deviceWrappers;
                        com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter.C6738b bVar2 = new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter.C6738b(device.getDeviceId(), z5, str6, i, true, booleanValue);
                        arrayList2.add(0, bVar2);
                        if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) "release", (java.lang.Object) "debug")) {
                            if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) "release", (java.lang.Object) "staging")) {
                                if (!com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId())) {
                                    if (device.getBatteryLevel() >= 10 || device.getBatteryLevel() <= 0) {
                                        r2.this$0.f23815a.mo41449l().mo27221a(false, false);
                                    } else {
                                        r2.this$0.f23815a.mo41449l().mo27221a(true, false);
                                    }
                                } else if (device.getBatteryLevel() >= 25 || device.getBatteryLevel() <= 0) {
                                    r2.this$0.f23815a.mo41449l().mo27221a(false, true);
                                } else {
                                    r2.this$0.f23815a.mo41449l().mo27221a(true, true);
                                }
                            }
                        }
                        if (!com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId())) {
                            if (device.getBatteryLevel() >= com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34574u().mo27087w() || device.getBatteryLevel() <= 0) {
                                r2.this$0.f23815a.mo41449l().mo27221a(false, false);
                            } else {
                                r2.this$0.f23815a.mo41449l().mo27221a(true, false);
                            }
                        } else if (device.getBatteryLevel() >= com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34574u().mo27087w() || device.getBatteryLevel() <= 0) {
                            r2.this$0.f23815a.mo41449l().mo27221a(false, true);
                        } else {
                            r2.this$0.f23815a.mo41449l().mo27221a(true, true);
                        }
                    }
                    obj3 = obj6;
                    it2 = it;
                    if (it2.hasNext()) {
                        com.portfolio.platform.data.model.Device device3 = (com.portfolio.platform.data.model.Device) it2.next();
                        com.fossil.blesdk.obfuscated.ug4 b = r2.this$0.f23815a.mo31441c();
                        com.portfolio.platform.uirenew.home.profile.C6746x2b66fa75 homeProfilePresenter$start$4$1$invokeSuspend$$inlined$withLock$lambda$1 = new com.portfolio.platform.uirenew.home.profile.C6746x2b66fa75(device3, (com.fossil.blesdk.obfuscated.yb4) null, r2);
                        r2.L$0 = zg4;
                        r2.L$1 = dl4;
                        r2.L$2 = str;
                        r2.L$3 = it2;
                        r2.L$4 = device3;
                        r2.label = 2;
                        obj4 = com.fossil.blesdk.obfuscated.yf4.m30997a(b, homeProfilePresenter$start$4$1$invokeSuspend$$inlined$withLock$lambda$1, r2);
                        if (obj4 == obj3) {
                            return obj3;
                        }
                        zg42 = zg4;
                        obj5 = obj3;
                        device = device3;
                        it = it2;
                        java.lang.String str32 = (java.lang.String) obj4;
                        com.fossil.blesdk.obfuscated.ug4 a22 = r2.this$0.f23815a.mo31440b();
                        com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4$1$1$isLatestFw$1 homeProfilePresenter$start$4$1$1$isLatestFw$12 = new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4$1$1$isLatestFw$1(str, (com.fossil.blesdk.obfuscated.yb4) null);
                        r2.L$0 = zg42;
                        r2.L$1 = dl4;
                        r2.L$2 = str;
                        r2.L$3 = it;
                        r2.L$4 = device;
                        r2.L$5 = str32;
                        r2.Z$0 = false;
                        r2.Z$1 = false;
                        r2.label = 3;
                        obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(a22, homeProfilePresenter$start$4$1$1$isLatestFw$12, r2);
                        if (obj2 != obj5) {
                            return obj5;
                        }
                        str6 = str32;
                        z4 = false;
                        z3 = false;
                        obj6 = obj5;
                        zg45 = zg42;
                        boolean booleanValue2 = ((java.lang.Boolean) obj2).booleanValue();
                        if (device.getBatteryLevel() <= 100) {
                        }
                        if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) device.getDeviceId(), (java.lang.Object) str)) {
                        }
                        obj3 = obj6;
                        it2 = it;
                        if (it2.hasNext()) {
                        }
                        return obj5;
                        return obj3;
                    }
                    if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) r2.$deviceWrappers, (java.lang.Object) r2.this$0.f23815a.mo41447j())) {
                        r2.this$0.f23815a.mo41447j().clear();
                        r2.this$0.f23815a.mo41447j().addAll(r2.$deviceWrappers);
                    }
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String a3 = com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter.f23773C.mo41453a();
                    local.mo33255d(a3, "update new device list " + r2.this$0.f23815a.mo41447j());
                    r2.this$0.f23815a.mo41449l().mo27222b(r2.this$0.f23815a.mo41447j());
                    if (java.lang.System.currentTimeMillis() - com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34574u().mo27039g(str) < 60000) {
                        r2.this$0.f23815a.mo41450m();
                    }
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter.f23773C.mo41453a(), "updatedDevices done");
                    com.fossil.blesdk.obfuscated.qa4 qa4 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                    dl4.mo26536a((java.lang.Object) null);
                    return com.fossil.blesdk.obfuscated.qa4.f17909a;
                } catch (Throwable th) {
                    dl4.mo26536a((java.lang.Object) null);
                    throw th;
                }
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            dl4 = dl42;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a4 = com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter.f23773C.mo41453a();
            local2.mo33255d(a4, "updatedDevices " + this.$devices + " currentDevices " + this.this$0.f23815a.f23776g);
            str = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e();
            it2 = this.$devices.iterator();
            obj3 = a;
            r2 = this;
            if (it2.hasNext()) {
            }
            if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) r2.$deviceWrappers, (java.lang.Object) r2.this$0.f23815a.mo41447j())) {
            }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a32 = com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter.f23773C.mo41453a();
            local3.mo33255d(a32, "update new device list " + r2.this$0.f23815a.mo41447j());
            r2.this$0.f23815a.mo41449l().mo27222b(r2.this$0.f23815a.mo41447j());
            if (java.lang.System.currentTimeMillis() - com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34574u().mo27039g(str) < 60000) {
            }
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter.f23773C.mo41453a(), "updatedDevices done");
            com.fossil.blesdk.obfuscated.qa4 qa42 = com.fossil.blesdk.obfuscated.qa4.f17909a;
            dl4.mo26536a((java.lang.Object) null);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public HomeProfilePresenter$start$4(com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter homeProfilePresenter) {
        this.f23815a = homeProfilePresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo8689a(java.util.List<com.portfolio.platform.data.model.Device> list) {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        this.f23815a.f23776g.clear();
        java.util.ArrayList e = this.f23815a.f23776g;
        if (list != null) {
            e.addAll(list);
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f23815a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4.C67481(this, list, arrayList, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            return;
        }
        com.fossil.blesdk.obfuscated.kd4.m24405a();
        throw null;
    }
}
