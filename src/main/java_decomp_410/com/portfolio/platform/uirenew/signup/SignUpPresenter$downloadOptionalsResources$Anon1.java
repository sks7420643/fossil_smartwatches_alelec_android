package com.portfolio.platform.uirenew.signup;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nj2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$Anon1", f = "SignUpPresenter.kt", l = {499, 509}, m = "invokeSuspend")
public final class SignUpPresenter$downloadOptionalsResources$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SignUpPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$Anon1$Anon1", f = "SignUpPresenter.kt", l = {500, 501, 502, 503, 504, 505}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter$downloadOptionalsResources$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(SignUpPresenter$downloadOptionalsResources$Anon1 signUpPresenter$downloadOptionalsResources$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = signUpPresenter$downloadOptionalsResources$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x005d, code lost:
            r6 = r5.this$Anon0.this$Anon0.x();
            r5.L$Anon0 = r1;
            r5.label = 2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x006e, code lost:
            if (r6.fetchActivitySettings(r5) != r0) goto L_0x0071;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0070, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0071, code lost:
            r6 = r5.this$Anon0.this$Anon0.w();
            r5.L$Anon0 = r1;
            r5.label = 3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0082, code lost:
            if (r6.fetchLastSleepGoal(r5) != r0) goto L_0x0085;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0084, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0085, code lost:
            r6 = r5.this$Anon0.this$Anon0.u();
            r5.L$Anon0 = r1;
            r5.label = 4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0096, code lost:
            if (r6.fetchGoalSetting(r5) != r0) goto L_0x0099;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0098, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0099, code lost:
            r6 = r5.this$Anon0.this$Anon0.p();
            r5.L$Anon0 = r1;
            r5.label = 5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00aa, code lost:
            if (r6.downloadAlarms(r5) != r0) goto L_0x00ad;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x00ac, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00ad, code lost:
            r6 = r5.this$Anon0.this$Anon0.r();
            r5.L$Anon0 = r1;
            r5.label = 6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x00bf, code lost:
            if (com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r6, 0, r5, 1, (java.lang.Object) null) != r0) goto L_0x00c2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x00c1, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00c4, code lost:
            return com.fossil.blesdk.obfuscated.qa4.a;
         */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            zg4 zg4;
            Object a = cc4.a();
            switch (this.label) {
                case 0:
                    na4.a(obj);
                    zg4 = this.p$;
                    WatchLocalizationRepository z = this.this$Anon0.this$Anon0.z();
                    this.L$Anon0 = zg4;
                    this.label = 1;
                    if (z.getWatchLocalizationFromServer(false, this) == a) {
                        return a;
                    }
                    break;
                case 1:
                    zg4 = (zg4) this.L$Anon0;
                    na4.a(obj);
                    break;
                case 2:
                    zg4 = (zg4) this.L$Anon0;
                    na4.a(obj);
                    break;
                case 3:
                    zg4 = (zg4) this.L$Anon0;
                    na4.a(obj);
                    break;
                case 4:
                    zg4 = (zg4) this.L$Anon0;
                    na4.a(obj);
                    break;
                case 5:
                    zg4 = (zg4) this.L$Anon0;
                    na4.a(obj);
                    break;
                case 6:
                    zg4 zg42 = (zg4) this.L$Anon0;
                    na4.a(obj);
                    break;
                default:
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SignUpPresenter$downloadOptionalsResources$Anon1(SignUpPresenter signUpPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = signUpPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SignUpPresenter$downloadOptionalsResources$Anon1 signUpPresenter$downloadOptionalsResources$Anon1 = new SignUpPresenter$downloadOptionalsResources$Anon1(this.this$Anon0, yb4);
        signUpPresenter$downloadOptionalsResources$Anon1.p$ = (zg4) obj;
        return signUpPresenter$downloadOptionalsResources$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SignUpPresenter$downloadOptionalsResources$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005c  */
    public final Object invokeSuspend(Object obj) {
        List list;
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            ug4 b = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            if (yf4.a(b, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            list = (List) obj;
            if (list == null) {
                list = new ArrayList();
            }
            PortfolioApp.W.c().a((List<? extends Alarm>) nj2.a(list));
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ug4 a2 = this.this$Anon0.b();
        SignUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1 signUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1 = new SignUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1(this, (yb4) null);
        this.L$Anon0 = zg4;
        this.label = 2;
        obj = yf4.a(a2, signUpPresenter$downloadOptionalsResources$Anon1$alarms$Anon1, this);
        if (obj == a) {
            return a;
        }
        list = (List) obj;
        if (list == null) {
        }
        PortfolioApp.W.c().a((List<? extends Alarm>) nj2.a(list));
        return qa4.a;
    }
}
