package com.portfolio.platform.uirenew.signup;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1", f = "SignUpPresenter.kt", l = {237, 238, 239}, m = "invokeSuspend")
public final class SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SignUpPresenter$signupEmail$Anon1 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1$Anon1", f = "SignUpPresenter.kt", l = {237}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qo2<ActivitySettings>>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1 signUpPresenter$signupEmail$Anon1$onSuccess$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = signUpPresenter$signupEmail$Anon1$onSuccess$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                SummariesRepository x = this.this$Anon0.this$Anon0.a.x();
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = x.fetchActivitySettings(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1$Anon2", f = "SignUpPresenter.kt", l = {238}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qo2<MFSleepSettings>>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1 signUpPresenter$signupEmail$Anon1$onSuccess$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = signUpPresenter$signupEmail$Anon1$onSuccess$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, yb4);
            anon2.p$ = (zg4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                SleepSummariesRepository w = this.this$Anon0.this$Anon0.a.w();
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = w.fetchLastSleepGoal(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1$Anon3", f = "SignUpPresenter.kt", l = {239}, m = "invokeSuspend")
    public static final class Anon3 extends SuspendLambda implements yc4<zg4, yb4<? super qo2<GoalSetting>>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3(SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1 signUpPresenter$signupEmail$Anon1$onSuccess$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = signUpPresenter$signupEmail$Anon1$onSuccess$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon3 anon3 = new Anon3(this.this$Anon0, yb4);
            anon3.p$ = (zg4) obj;
            return anon3;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon3) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                GoalTrackingRepository u = this.this$Anon0.this$Anon0.a.u();
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = u.fetchGoalSetting(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1(SignUpPresenter$signupEmail$Anon1 signUpPresenter$signupEmail$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = signUpPresenter$signupEmail$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1 signUpPresenter$signupEmail$Anon1$onSuccess$Anon1 = new SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1(this.this$Anon0, yb4);
        signUpPresenter$signupEmail$Anon1$onSuccess$Anon1.p$ = (zg4) obj;
        return signUpPresenter$signupEmail$Anon1$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SignUpPresenter$signupEmail$Anon1$onSuccess$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007f A[RETURN] */
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        ug4 b;
        Anon3 anon3;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg42 = this.p$;
            ug4 b2 = this.this$Anon0.a.c();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg42;
            this.label = 1;
            if (yf4.a(b2, anon1, this) == a) {
                return a;
            }
            zg4 = zg42;
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            b = this.this$Anon0.a.c();
            anon3 = new Anon3(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 3;
            if (yf4.a(b, anon3, this) == a) {
                return a;
            }
            return qa4.a;
        } else if (i == 3) {
            zg4 zg43 = (zg4) this.L$Anon0;
            na4.a(obj);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ug4 b3 = this.this$Anon0.a.c();
        Anon2 anon2 = new Anon2(this, (yb4) null);
        this.L$Anon0 = zg4;
        this.label = 2;
        if (yf4.a(b3, anon2, this) == a) {
            return a;
        }
        b = this.this$Anon0.a.c();
        anon3 = new Anon3(this, (yb4) null);
        this.L$Anon0 = zg4;
        this.label = 3;
        if (yf4.a(b, anon3, this) == a) {
        }
        return qa4.a;
    }
}
