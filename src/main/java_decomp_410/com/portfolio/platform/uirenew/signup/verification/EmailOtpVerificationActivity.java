package com.portfolio.platform.uirenew.signup.verification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.ro3;
import com.fossil.blesdk.obfuscated.so3;
import com.fossil.blesdk.obfuscated.wo3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class EmailOtpVerificationActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public so3 B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, SignUpEmailAuth signUpEmailAuth) {
            kd4.b(context, "context");
            kd4.b(signUpEmailAuth, "emailAuth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("EmailOtpVerificationActivity", "start verify Otp with Email Auth =" + signUpEmailAuth);
            Intent intent = new Intent(context, EmailOtpVerificationActivity.class);
            intent.putExtra("EMAIL_AUTH", signUpEmailAuth);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        ro3 ro3 = (ro3) getSupportFragmentManager().a((int) R.id.content);
        if (ro3 == null) {
            ro3 = ro3.u.a();
            a((Fragment) ro3, f(), (int) R.id.content);
        }
        l42 g = PortfolioApp.W.c().g();
        if (ro3 != null) {
            g.a(new wo3(this, ro3)).a(this);
            Intent intent = getIntent();
            if (intent != null && intent.hasExtra("EMAIL_AUTH")) {
                FLogger.INSTANCE.getLocal().d(f(), "Retrieve from intent emailAddress");
                so3 so3 = this.B;
                if (so3 != null) {
                    Parcelable parcelableExtra = intent.getParcelableExtra("EMAIL_AUTH");
                    kd4.a((Object) parcelableExtra, "it.getParcelableExtra(co\u2026ums.Constants.EMAIL_AUTH)");
                    so3.a((SignUpEmailAuth) parcelableExtra);
                    return;
                }
                kd4.d("mPresenter");
                throw null;
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationContract.View");
    }
}
