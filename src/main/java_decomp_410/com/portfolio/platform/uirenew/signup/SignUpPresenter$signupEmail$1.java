package com.portfolio.platform.uirenew.signup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SignUpPresenter$signupEmail$1 implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.usecase.CheckAuthenticationEmailExisting.C6892d, com.portfolio.platform.usecase.CheckAuthenticationEmailExisting.C6891c> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.signup.SignUpPresenter f24217a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ /* synthetic */ java.lang.String f24218b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ /* synthetic */ java.lang.String f24219c;

    @DexIgnore
    public SignUpPresenter$signupEmail$1(com.portfolio.platform.uirenew.signup.SignUpPresenter signUpPresenter, java.lang.String str, java.lang.String str2) {
        this.f24217a = signUpPresenter;
        this.f24218b = str;
        this.f24219c = str2;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(com.portfolio.platform.usecase.CheckAuthenticationEmailExisting.C6892d dVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
        boolean a = dVar.mo41866a();
        if (a) {
            this.f24217a.f24170N.mo28035i();
            com.fossil.blesdk.obfuscated.ho3 c = this.f24217a.f24170N;
            java.lang.String a2 = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.Onboarding_SignUp_EmailInUse_Text__ThisEmailIsAlreadyInUse);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a2, "LanguageHelper.getString\u2026_ThisEmailIsAlreadyInUse)");
            c.mo28026N(a2);
            this.f24217a.mo41696m();
        } else if (!a) {
            com.portfolio.platform.data.SignUpEmailAuth signUpEmailAuth = new com.portfolio.platform.data.SignUpEmailAuth();
            signUpEmailAuth.setEmail(this.f24218b);
            signUpEmailAuth.setPassword(this.f24219c);
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f24217a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            this.f24217a.mo41692a(signUpEmailAuth);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo29641a(com.portfolio.platform.usecase.CheckAuthenticationEmailExisting.C6891c cVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(cVar, "errorValue");
        this.f24217a.f24170N.mo28035i();
        this.f24217a.mo41691a(cVar.mo41865a(), "");
    }
}
