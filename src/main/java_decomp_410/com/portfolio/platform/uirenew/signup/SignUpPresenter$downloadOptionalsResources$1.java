package com.portfolio.platform.uirenew.signup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1", mo27670f = "SignUpPresenter.kt", mo27671l = {499, 509}, mo27672m = "invokeSuspend")
public final class SignUpPresenter$downloadOptionalsResources$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24205p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.signup.SignUpPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1$1", mo27670f = "SignUpPresenter.kt", mo27671l = {500, 501, 502, 503, 504, 505}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1$1 */
    public static final class C68421 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24206p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68421(com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1 signUpPresenter$downloadOptionalsResources$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = signUpPresenter$downloadOptionalsResources$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1.C68421 r0 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1.C68421(this.this$0, yb4);
            r0.f24206p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1.C68421) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x005d, code lost:
            r6 = r5.this$0.this$0.mo41707x();
            r5.L$0 = r1;
            r5.label = 2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x006e, code lost:
            if (r6.fetchActivitySettings(r5) != r0) goto L_0x0071;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0070, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0071, code lost:
            r6 = r5.this$0.this$0.mo41706w();
            r5.L$0 = r1;
            r5.label = 3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0082, code lost:
            if (r6.fetchLastSleepGoal(r5) != r0) goto L_0x0085;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0084, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0085, code lost:
            r6 = r5.this$0.this$0.mo41704u();
            r5.L$0 = r1;
            r5.label = 4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0096, code lost:
            if (r6.fetchGoalSetting(r5) != r0) goto L_0x0099;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0098, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0099, code lost:
            r6 = r5.this$0.this$0.mo41699p();
            r5.L$0 = r1;
            r5.label = 5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00aa, code lost:
            if (r6.downloadAlarms(r5) != r0) goto L_0x00ad;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x00ac, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00ad, code lost:
            r6 = r5.this$0.this$0.mo41701r();
            r5.L$0 = r1;
            r5.label = 6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x00bf, code lost:
            if (com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r6, 0, r5, 1, (java.lang.Object) null) != r0) goto L_0x00c2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x00c1, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00c4, code lost:
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
         */
        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.zg4 zg4;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            switch (this.label) {
                case 0:
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    zg4 = this.f24206p$;
                    com.portfolio.platform.data.source.WatchLocalizationRepository z = this.this$0.this$0.mo41709z();
                    this.L$0 = zg4;
                    this.label = 1;
                    if (z.getWatchLocalizationFromServer(false, this) == a) {
                        return a;
                    }
                    break;
                case 1:
                    zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    break;
                case 2:
                    zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    break;
                case 3:
                    zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    break;
                case 4:
                    zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    break;
                case 5:
                    zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    break;
                case 6:
                    com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                    com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                    break;
                default:
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SignUpPresenter$downloadOptionalsResources$1(com.portfolio.platform.uirenew.signup.SignUpPresenter signUpPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = signUpPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1 signUpPresenter$downloadOptionalsResources$1 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1(this.this$0, yb4);
        signUpPresenter$downloadOptionalsResources$1.f24205p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return signUpPresenter$downloadOptionalsResources$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005c  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.util.List list;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f24205p$;
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1.C68421 r5 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1.C68421(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r5, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            list = (java.util.List) obj;
            if (list == null) {
                list = new java.util.ArrayList();
            }
            com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34508a((java.util.List<? extends com.misfit.frameworks.buttonservice.model.Alarm>) com.fossil.blesdk.obfuscated.nj2.m25699a(list));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
        com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1$alarms$1 signUpPresenter$downloadOptionalsResources$1$alarms$1 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1$alarms$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.label = 2;
        obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, signUpPresenter$downloadOptionalsResources$1$alarms$1, this);
        if (obj == a) {
            return a;
        }
        list = (java.util.List) obj;
        if (list == null) {
        }
        com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34508a((java.util.List<? extends com.misfit.frameworks.buttonservice.model.Alarm>) com.fossil.blesdk.obfuscated.nj2.m25699a(list));
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
