package com.portfolio.platform.uirenew.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wb4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SecondTimeZoneSearchAdapter extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public List<SecondTimezoneSetting> a; // = new ArrayList();
    @DexIgnore
    public /* final */ ArrayList<SecondTimeZoneSearchModel> b; // = new ArrayList<>();
    @DexIgnore
    public b c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class SecondTimeZoneSearchModel {
        @DexIgnore
        public SecondTimezoneSetting a;
        @DexIgnore
        public String b;
        @DexIgnore
        public TYPE c; // = TYPE.TYPE_HEADER;

        @DexIgnore
        public enum TYPE {
            TYPE_HEADER,
            TYPE_VALUE
        }

        @DexIgnore
        public final void a(SecondTimezoneSetting secondTimezoneSetting) {
            this.a = secondTimezoneSetting;
        }

        @DexIgnore
        public final TYPE b() {
            return this.c;
        }

        @DexIgnore
        public final SecondTimezoneSetting c() {
            return this.a;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final void a(String str) {
            this.b = str;
        }

        @DexIgnore
        public final void a(TYPE type) {
            kd4.b(type, "<set-?>");
            this.c = type;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(SecondTimeZoneSearchAdapter secondTimeZoneSearchAdapter, View view) {
            super(view);
            kd4.b(view, "view");
            View findViewById = view.findViewById(R.id.ftv_text);
            if (findViewById != null) {
                this.a = (FlexibleTextView) findViewById;
            } else {
                kd4.a();
                throw null;
            }
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ b b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public a(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b a = this.e.b;
                if (a != null) {
                    a.a(this.e.getAdapterPosition());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(SecondTimeZoneSearchAdapter secondTimeZoneSearchAdapter, View view, b bVar) {
            super(view);
            kd4.b(view, "view");
            this.b = bVar;
            view.setOnClickListener(new a(this));
            View findViewById = view.findViewById(R.id.tv_second_timezone);
            if (findViewById != null) {
                this.a = (FlexibleTextView) findViewById;
            } else {
                kd4.a();
                throw null;
            }
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return wb4.a(((SecondTimezoneSetting) t).getTimeZoneName(), ((SecondTimezoneSetting) t2).getTimeZoneName());
        }
    }

    @DexIgnore
    public final void a(List<SecondTimezoneSetting> list) {
        throw null;
        // kd4.b(list, "secondTimeZoneList");
        // this.b.clear();
        // this.a = list;
        // List<T> a2 = kb4.a(list, new d());
        // int size = a2.size();
        // String str = "";
        // int i = 0;
        // while (i < size) {
        //     SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) a2.get(i);
        //     String timeZoneName = secondTimezoneSetting.getTimeZoneName();
        //     if (timeZoneName != null) {
        //         char[] charArray = timeZoneName.toCharArray();
        //         kd4.a((Object) charArray, "(this as java.lang.String).toCharArray()");
        //         String valueOf = String.valueOf(Character.toUpperCase(charArray[0]));
        //         if (!TextUtils.equals(str, valueOf)) {
        //             SecondTimeZoneSearchModel secondTimeZoneSearchModel = new SecondTimeZoneSearchModel();
        //             secondTimeZoneSearchModel.a(valueOf);
        //             secondTimeZoneSearchModel.a(SecondTimeZoneSearchModel.TYPE.TYPE_HEADER);
        //             this.b.add(secondTimeZoneSearchModel);
        //             str = valueOf;
        //         }
        //         SecondTimeZoneSearchModel secondTimeZoneSearchModel2 = new SecondTimeZoneSearchModel();
        //         secondTimeZoneSearchModel2.a(secondTimezoneSetting);
        //         secondTimeZoneSearchModel2.a(SecondTimeZoneSearchModel.TYPE.TYPE_VALUE);
        //         this.b.add(secondTimeZoneSearchModel2);
        //         i++;
        //     } else {
        //         throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        //     }
        // }
        // notifyDataSetChanged();
    }

    @DexIgnore
    public final List<SecondTimeZoneSearchModel> b() {
        return this.b;
    }

    @DexIgnore
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public int getItemViewType(int i) {
        return this.b.get(i).b().ordinal();
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        kd4.b(viewHolder, "holder");
        if (getItemViewType(i) == SecondTimeZoneSearchModel.TYPE.TYPE_HEADER.ordinal()) {
            FlexibleTextView a2 = ((a) viewHolder).a();
            String a3 = this.b.get(i).a();
            if (a3 != null) {
                a2.setText(a3);
            } else {
                kd4.a();
                throw null;
            }
        } else {
            SecondTimezoneSetting c2 = this.b.get(i).c();
            if (c2 != null) {
                ((c) viewHolder).a().setText(c2.getTimeZoneName());
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        if (i == SecondTimeZoneSearchModel.TYPE.TYPE_HEADER.ordinal()) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_search_second_timezone_header, viewGroup, false);
            kd4.a((Object) inflate, "v");
            return new a(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_timezone_item_adapter, viewGroup, false);
        kd4.a((Object) inflate2, "v");
        return new c(this, inflate2, this.c);
    }

    @DexIgnore
    public final int b(String str) {
        kd4.b(str, "letter");
        int i = 0;
        for (SecondTimeZoneSearchModel next : this.b) {
            if (next.b() == SecondTimeZoneSearchModel.TYPE.TYPE_HEADER && kd4.a((Object) next.a(), (Object) str)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public final void a(String str) {
        throw null;
        // kd4.b(str, "filterText");
        // if (TextUtils.isEmpty(str)) {
        //     a(this.a);
        // } else {
        //     this.b.clear();
        //     for (SecondTimezoneSetting next : this.a) {
        //         String timeZoneName = next.getTimeZoneName();
        //         if (timeZoneName != null) {
        //             String lowerCase = timeZoneName.toLowerCase();
        //             kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
        //             String lowerCase2 = str.toLowerCase();
        //             kd4.a((Object) lowerCase2, "(this as java.lang.String).toLowerCase()");
        //             if (StringsKt__StringsKt.a((CharSequence) lowerCase, (CharSequence) lowerCase2, false, 2, (Object) null)) {
        //                 SecondTimeZoneSearchModel secondTimeZoneSearchModel = new SecondTimeZoneSearchModel();
        //                 secondTimeZoneSearchModel.a(next);
        //                 secondTimeZoneSearchModel.a(SecondTimeZoneSearchModel.TYPE.TYPE_VALUE);
        //                 this.b.add(secondTimeZoneSearchModel);
        //             }
        //         } else {
        //             throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        //         }
        //     }
        // }
        // notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        kd4.b(bVar, "mItemClickListener");
        this.c = bVar;
    }
}
