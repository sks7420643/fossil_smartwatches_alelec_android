package com.portfolio.platform.uirenew.login;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nj2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$Anon1", f = "LoginPresenter.kt", l = {448, 457}, m = "invokeSuspend")
public final class LoginPresenter$downloadOptionalsResources$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LoginPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$Anon1$Anon1", f = "LoginPresenter.kt", l = {449, 450, 451, 452, 453}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter$downloadOptionalsResources$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(LoginPresenter$downloadOptionalsResources$Anon1 loginPresenter$downloadOptionalsResources$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = loginPresenter$downloadOptionalsResources$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0087 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x009a A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00ae A[RETURN] */
        public final Object invokeSuspend(Object obj) {
            zg4 zg4;
            DeviceRepository r;
            GoalTrackingRepository u;
            SleepSummariesRepository w;
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg42 = this.p$;
                WatchLocalizationRepository z = this.this$Anon0.this$Anon0.z();
                this.L$Anon0 = zg42;
                this.label = 1;
                if (z.getWatchLocalizationFromServer(false, this) == a) {
                    return a;
                }
                zg4 = zg42;
            } else if (i == 1) {
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else if (i == 2) {
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
                w = this.this$Anon0.this$Anon0.w();
                this.L$Anon0 = zg4;
                this.label = 3;
                if (w.fetchLastSleepGoal(this) == a) {
                    return a;
                }
                u = this.this$Anon0.this$Anon0.u();
                this.L$Anon0 = zg4;
                this.label = 4;
                if (u.fetchGoalSetting(this) == a) {
                }
                r = this.this$Anon0.this$Anon0.r();
                this.L$Anon0 = zg4;
                this.label = 5;
                if (DeviceRepository.downloadSupportedSku$default(r, 0, this, 1, (Object) null) == a) {
                }
                return qa4.a;
            } else if (i == 3) {
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
                u = this.this$Anon0.this$Anon0.u();
                this.L$Anon0 = zg4;
                this.label = 4;
                if (u.fetchGoalSetting(this) == a) {
                    return a;
                }
                r = this.this$Anon0.this$Anon0.r();
                this.L$Anon0 = zg4;
                this.label = 5;
                if (DeviceRepository.downloadSupportedSku$default(r, 0, this, 1, (Object) null) == a) {
                }
                return qa4.a;
            } else if (i == 4) {
                zg4 = (zg4) this.L$Anon0;
                na4.a(obj);
                r = this.this$Anon0.this$Anon0.r();
                this.L$Anon0 = zg4;
                this.label = 5;
                if (DeviceRepository.downloadSupportedSku$default(r, 0, this, 1, (Object) null) == a) {
                    return a;
                }
                return qa4.a;
            } else if (i == 5) {
                zg4 zg43 = (zg4) this.L$Anon0;
                na4.a(obj);
                return qa4.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SummariesRepository x = this.this$Anon0.this$Anon0.x();
            this.L$Anon0 = zg4;
            this.label = 2;
            if (x.fetchActivitySettings(this) == a) {
                return a;
            }
            w = this.this$Anon0.this$Anon0.w();
            this.L$Anon0 = zg4;
            this.label = 3;
            if (w.fetchLastSleepGoal(this) == a) {
            }
            u = this.this$Anon0.this$Anon0.u();
            this.L$Anon0 = zg4;
            this.label = 4;
            if (u.fetchGoalSetting(this) == a) {
            }
            r = this.this$Anon0.this$Anon0.r();
            this.L$Anon0 = zg4;
            this.label = 5;
            if (DeviceRepository.downloadSupportedSku$default(r, 0, this, 1, (Object) null) == a) {
            }
            return qa4.a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LoginPresenter$downloadOptionalsResources$Anon1(LoginPresenter loginPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = loginPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        LoginPresenter$downloadOptionalsResources$Anon1 loginPresenter$downloadOptionalsResources$Anon1 = new LoginPresenter$downloadOptionalsResources$Anon1(this.this$Anon0, yb4);
        loginPresenter$downloadOptionalsResources$Anon1.p$ = (zg4) obj;
        return loginPresenter$downloadOptionalsResources$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LoginPresenter$downloadOptionalsResources$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005c  */
    public final Object invokeSuspend(Object obj) {
        List list;
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            ug4 b = this.this$Anon0.c();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            if (yf4.a(b, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            list = (List) obj;
            if (list == null) {
                list = new ArrayList();
            }
            PortfolioApp.W.c().a((List<? extends Alarm>) nj2.a(list));
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ug4 b2 = this.this$Anon0.c();
        LoginPresenter$downloadOptionalsResources$Anon1$alarms$Anon1 loginPresenter$downloadOptionalsResources$Anon1$alarms$Anon1 = new LoginPresenter$downloadOptionalsResources$Anon1$alarms$Anon1(this, (yb4) null);
        this.L$Anon0 = zg4;
        this.label = 2;
        obj = yf4.a(b2, loginPresenter$downloadOptionalsResources$Anon1$alarms$Anon1, this);
        if (obj == a) {
            return a;
        }
        list = (List) obj;
        if (list == null) {
        }
        PortfolioApp.W.c().a((List<? extends Alarm>) nj2.a(list));
        return qa4.a;
    }
}
