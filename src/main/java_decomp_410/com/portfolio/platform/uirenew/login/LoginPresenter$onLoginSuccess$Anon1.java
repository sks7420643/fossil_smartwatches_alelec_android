package com.portfolio.platform.uirenew.login;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LoginPresenter$onLoginSuccess$Anon1 implements CoroutineUseCase.e<DownloadUserInfoUseCase.d, DownloadUserInfoUseCase.b> {
    @DexIgnore
    public /* final */ /* synthetic */ LoginPresenter a;

    @DexIgnore
    public LoginPresenter$onLoginSuccess$Anon1(LoginPresenter loginPresenter) {
        this.a = loginPresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(DownloadUserInfoUseCase.d dVar) {
        kd4.b(dVar, "responseValue");
        fi4 unused = ag4.b(this.a.e(), (CoroutineContext) null, (CoroutineStart) null, new LoginPresenter$onLoginSuccess$Anon1$onSuccess$Anon1(this, dVar.a(), (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(DownloadUserInfoUseCase.b bVar) {
        kd4.b(bVar, "errorValue");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = LoginPresenter.O.a();
        local.d(a2, "onLoginSuccess download userInfo failed " + bVar.a());
        this.a.y().clearAllUser();
        this.a.L.i();
        this.a.a(bVar.a(), bVar.b());
    }
}
