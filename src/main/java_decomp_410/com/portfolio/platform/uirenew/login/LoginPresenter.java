package com.portfolio.platform.uirenew.login;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.as3;
import com.fossil.blesdk.obfuscated.dk3;
import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.ek3;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.j62;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kn2;
import com.fossil.blesdk.obfuscated.ks3;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.rr2;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.sr2;
import com.fossil.blesdk.obfuscated.tq2;
import com.fossil.blesdk.obfuscated.tr2;
import com.fossil.blesdk.obfuscated.ur2;
import com.fossil.blesdk.obfuscated.vj2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginEmailUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.Locale;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LoginPresenter extends dk3 {
    @DexIgnore
    public static /* final */ String N;
    @DexIgnore
    public static /* final */ a O; // = new a((fd4) null);
    @DexIgnore
    public tr2 A;
    @DexIgnore
    public CheckAuthenticationSocialExisting B;
    @DexIgnore
    public AnalyticsHelper C;
    @DexIgnore
    public SummariesRepository D;
    @DexIgnore
    public SleepSummariesRepository E;
    @DexIgnore
    public GoalTrackingRepository F;
    @DexIgnore
    public FetchDailyGoalTrackingSummaries G;
    @DexIgnore
    public FetchGoalTrackingData H;
    @DexIgnore
    public GetSecretKeyUseCase I;
    @DexIgnore
    public WatchLocalizationRepository J;
    @DexIgnore
    public AlarmsRepository K;
    @DexIgnore
    public /* final */ ek3 L;
    @DexIgnore
    public /* final */ BaseActivity M;
    @DexIgnore
    public String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public LoginEmailUseCase h;
    @DexIgnore
    public LoginSocialUseCase i;
    @DexIgnore
    public DownloadUserInfoUseCase j;
    @DexIgnore
    public tq2 k;
    @DexIgnore
    public vj2 l;
    @DexIgnore
    public UserRepository m;
    @DexIgnore
    public DeviceRepository n;
    @DexIgnore
    public en2 o;
    @DexIgnore
    public FetchActivities p;
    @DexIgnore
    public FetchSummaries q;
    @DexIgnore
    public j62 r;
    @DexIgnore
    public FetchSleepSessions s;
    @DexIgnore
    public FetchSleepSummaries t;
    @DexIgnore
    public FetchHeartRateSamples u;
    @DexIgnore
    public FetchDailyHeartRateSummaries v;
    @DexIgnore
    public rr2 w;
    @DexIgnore
    public kn2 x;
    @DexIgnore
    public sr2 y;
    @DexIgnore
    public ur2 z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return LoginPresenter.N;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<CheckAuthenticationSocialExisting.d, CheckAuthenticationSocialExisting.c> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpSocialAuth b;

        @DexIgnore
        public b(LoginPresenter loginPresenter, SignUpSocialAuth signUpSocialAuth) {
            this.a = loginPresenter;
            this.b = signUpSocialAuth;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(CheckAuthenticationSocialExisting.d dVar) {
            kd4.b(dVar, "responseValue");
            boolean a2 = dVar.a();
            if (a2) {
                this.a.c(this.b);
            } else if (!a2) {
                dn2.p.a().o();
                this.a.L.c(this.b);
            }
        }

        @DexIgnore
        public void a(CheckAuthenticationSocialExisting.c cVar) {
            kd4.b(cVar, "errorValue");
            this.a.L.i();
            this.a.b(cVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.e<LoginSocialUseCase.d, LoginSocialUseCase.b> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        public c(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(LoginSocialUseCase.d dVar) {
            kd4.b(dVar, "responseValue");
            PortfolioApp.W.c().g().a(this.a);
            this.a.B();
        }

        @DexIgnore
        public void a(LoginSocialUseCase.b bVar) {
            kd4.b(bVar, "errorValue");
            this.a.L.i();
            this.a.a(bVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.e<LoginEmailUseCase.d, LoginEmailUseCase.b> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        public d(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(LoginEmailUseCase.d dVar) {
            kd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(LoginPresenter.O.a(), "Inside .loginEmail success ");
            PortfolioApp.W.c().g().a(this.a);
            this.a.B();
        }

        @DexIgnore
        public void a(LoginEmailUseCase.b bVar) {
            kd4.b(bVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.O.a();
            local.d(a2, "Inside .loginEmail failed with error=" + bVar.a());
            this.a.L.i();
            this.a.b(bVar.a(), bVar.b());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CoroutineUseCase.e<rr2.d, rr2.c> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        public e(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(rr2.d dVar) {
            kd4.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.O.a();
            local.d(a2, "Inside .loginFacebook success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(rr2.c cVar) {
            kd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.O.a();
            local.d(a2, "Inside .loginFacebook failed with error=" + cVar.a());
            this.a.L.i();
            if (2 != cVar.a()) {
                this.a.b(cVar.a(), "");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CoroutineUseCase.e<sr2.d, sr2.c> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        public f(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(sr2.d dVar) {
            kd4.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.O.a();
            local.d(a2, "Inside .loginGoogle success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(sr2.c cVar) {
            kd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.O.a();
            local.d(a2, "Inside .loginGoogle failed with error=" + cVar.a());
            this.a.L.i();
            if (2 != cVar.a()) {
                this.a.b(cVar.a(), "");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements CoroutineUseCase.e<tr2.d, tr2.c> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        public g(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(tr2.d dVar) {
            kd4.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.O.a();
            local.d(a2, "Inside .loginWechat success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(tr2.c cVar) {
            kd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.O.a();
            local.e(a2, "Inside .loginWechat failed with error=" + cVar.a());
            this.a.L.i();
            this.a.b(cVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements CoroutineUseCase.e<ur2.d, ur2.c> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        public h(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ur2.d dVar) {
            kd4.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.O.a();
            local.d(a2, "Inside .loginWeibo success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(ur2.c cVar) {
            kd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.O.a();
            local.d(a2, "Inside .loginWeibo failed with error=" + cVar.a());
            this.a.L.i();
            this.a.b(cVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements CoroutineUseCase.e<tq2.e, tq2.d> {
        @DexIgnore
        public void a(tq2.d dVar) {
            kd4.b(dVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(tq2.e eVar) {
            kd4.b(eVar, "responseValue");
        }
    }

    /*
    static {
        String simpleName = LoginPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "LoginPresenter::class.java.simpleName");
        N = simpleName;
    }
    */

    @DexIgnore
    public LoginPresenter(ek3 ek3, BaseActivity baseActivity) {
        kd4.b(ek3, "mView");
        kd4.b(baseActivity, "mContext");
        this.L = ek3;
        this.M = baseActivity;
    }

    @DexIgnore
    public final boolean A() {
        String str = this.f;
        if (str == null || str.length() == 0) {
            this.L.a(false, "");
            return false;
        } else if (!ks3.a(this.f)) {
            ek3 ek3 = this.L;
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_SignUp_InputError_Text__InvalidEmailAddress);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            ek3.a(true, a2);
            return false;
        } else {
            this.L.a(false, "");
            return true;
        }
    }

    @DexIgnore
    public final void B() {
        FLogger.INSTANCE.getLocal().d(N, "onLoginSuccess download user info");
        DownloadUserInfoUseCase downloadUserInfoUseCase = this.j;
        if (downloadUserInfoUseCase != null) {
            downloadUserInfoUseCase.a(new DownloadUserInfoUseCase.c(), new LoginPresenter$onLoginSuccess$Anon1(this));
        } else {
            kd4.d("mDownloadUserInfoUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void C() {
        this.L.a(this);
    }

    @DexIgnore
    public final void D() {
        Locale locale = Locale.getDefault();
        kd4.a((Object) locale, "Locale.getDefault()");
        if (!TextUtils.isEmpty(locale.getLanguage())) {
            Locale locale2 = Locale.getDefault();
            kd4.a((Object) locale2, "Locale.getDefault()");
            if (!TextUtils.isEmpty(locale2.getCountry())) {
                StringBuilder sb = new StringBuilder();
                Locale locale3 = Locale.getDefault();
                kd4.a((Object) locale3, "Locale.getDefault()");
                sb.append(locale3.getLanguage());
                sb.append("_");
                Locale locale4 = Locale.getDefault();
                kd4.a((Object) locale4, "Locale.getDefault()");
                sb.append(locale4.getCountry());
                String sb2 = sb.toString();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = N;
                local.d(str, "language: " + sb2);
                if (qf4.b(sb2, "zh_CN", true) || qf4.b(sb2, "zh_SG", true)) {
                    this.L.F(true);
                    return;
                } else {
                    this.L.F(false);
                    return;
                }
            }
        }
        this.L.F(false);
    }

    @DexIgnore
    public final void E() {
        if (!A() || TextUtils.isEmpty(this.g)) {
            this.L.Y();
        } else {
            this.L.q0();
        }
    }

    @DexIgnore
    public void f() {
        E();
        D();
    }

    @DexIgnore
    public void h() {
        if (PortfolioApp.W.c().w()) {
            this.L.F();
        } else {
            a(601, "");
        }
    }

    @DexIgnore
    public void i() {
        ek3 ek3 = this.L;
        String str = this.f;
        if (str == null) {
            str = "";
        }
        ek3.J(str);
    }

    @DexIgnore
    public void j() {
        this.L.k();
        rr2 rr2 = this.w;
        if (rr2 != null) {
            rr2.a(new rr2.b(new WeakReference(this.M)), new e(this));
        } else {
            kd4.d("mLoginFacebookUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void k() {
        this.L.k();
        sr2 sr2 = this.y;
        if (sr2 != null) {
            sr2.a(new sr2.b(new WeakReference(this.M)), new f(this));
        } else {
            kd4.d("mLoginGoogleUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void l() {
        if (!AppHelper.f.a(this.M, "com.tencent.mm")) {
            AppHelper.f.b(this.M, "com.tencent.mm");
            return;
        }
        this.L.k();
        tr2 tr2 = this.A;
        if (tr2 != null) {
            tr2.a(new tr2.b(new WeakReference(this.M)), new g(this));
        } else {
            kd4.d("mLoginWechatUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void m() {
        this.L.k();
        ur2 ur2 = this.z;
        if (ur2 != null) {
            ur2.a(new ur2.b(new WeakReference(this.M)), new h(this));
        } else {
            kd4.d("mLoginWeiboUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void n() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new LoginPresenter$checkOnboarding$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void o() {
        FLogger.INSTANCE.getLocal().d(N, "downloadOptionalsResources");
        Date date = new Date();
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new LoginPresenter$downloadOptionalsResources$Anon1(this, (yb4) null), 3, (Object) null);
        FetchSummaries fetchSummaries = this.q;
        if (fetchSummaries != null) {
            fetchSummaries.a(new FetchSummaries.b(date), (CoroutineUseCase.e) null);
            FetchActivities fetchActivities = this.p;
            if (fetchActivities != null) {
                fetchActivities.a(new FetchActivities.b(date), (CoroutineUseCase.e) null);
                FetchSleepSessions fetchSleepSessions = this.s;
                if (fetchSleepSessions != null) {
                    fetchSleepSessions.a(new FetchSleepSessions.b(date), (CoroutineUseCase.e) null);
                    FetchSleepSummaries fetchSleepSummaries = this.t;
                    if (fetchSleepSummaries != null) {
                        fetchSleepSummaries.a(new FetchSleepSummaries.b(date), (CoroutineUseCase.e) null);
                        FetchHeartRateSamples fetchHeartRateSamples = this.u;
                        if (fetchHeartRateSamples != null) {
                            fetchHeartRateSamples.a(new FetchHeartRateSamples.b(date), (CoroutineUseCase.e) null);
                            FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries = this.v;
                            if (fetchDailyHeartRateSummaries != null) {
                                fetchDailyHeartRateSummaries.a(new FetchDailyHeartRateSummaries.b(date), (CoroutineUseCase.e) null);
                                FetchGoalTrackingData fetchGoalTrackingData = this.H;
                                if (fetchGoalTrackingData != null) {
                                    fetchGoalTrackingData.a(new FetchGoalTrackingData.b(date), (CoroutineUseCase.e) null);
                                    FetchDailyGoalTrackingSummaries fetchDailyGoalTrackingSummaries = this.G;
                                    if (fetchDailyGoalTrackingSummaries != null) {
                                        fetchDailyGoalTrackingSummaries.a(new FetchDailyGoalTrackingSummaries.b(date), (CoroutineUseCase.e) null);
                                    } else {
                                        kd4.d("mFetchDailyGoalTrackingSummaries");
                                        throw null;
                                    }
                                } else {
                                    kd4.d("mFetchGoalTrackingData");
                                    throw null;
                                }
                            } else {
                                kd4.d("mFetchDailyHeartRateSummaries");
                                throw null;
                            }
                        } else {
                            kd4.d("mFetchHeartRateSamples");
                            throw null;
                        }
                    } else {
                        kd4.d("mFetchSleepSummaries");
                        throw null;
                    }
                } else {
                    kd4.d("mFetchSleepSessions");
                    throw null;
                }
            } else {
                kd4.d("mFetchActivities");
                throw null;
            }
        } else {
            kd4.d("mFetchSummaries");
            throw null;
        }
    }

    @DexIgnore
    public final AlarmsRepository p() {
        AlarmsRepository alarmsRepository = this.K;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        kd4.d("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final AnalyticsHelper q() {
        AnalyticsHelper analyticsHelper = this.C;
        if (analyticsHelper != null) {
            return analyticsHelper;
        }
        kd4.d("mAnalyticsHelper");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository r() {
        DeviceRepository deviceRepository = this.n;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        kd4.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final vj2 s() {
        vj2 vj2 = this.l;
        if (vj2 != null) {
            return vj2;
        }
        kd4.d("mDeviceSettingFactory");
        throw null;
    }

    @DexIgnore
    public final GetSecretKeyUseCase t() {
        GetSecretKeyUseCase getSecretKeyUseCase = this.I;
        if (getSecretKeyUseCase != null) {
            return getSecretKeyUseCase;
        }
        kd4.d("mGetSecretKeyUseCase");
        throw null;
    }

    @DexIgnore
    public final GoalTrackingRepository u() {
        GoalTrackingRepository goalTrackingRepository = this.F;
        if (goalTrackingRepository != null) {
            return goalTrackingRepository;
        }
        kd4.d("mGoalTrackingRepository");
        throw null;
    }

    @DexIgnore
    public final en2 v() {
        en2 en2 = this.o;
        if (en2 != null) {
            return en2;
        }
        kd4.d("mSharePrefs");
        throw null;
    }

    @DexIgnore
    public final SleepSummariesRepository w() {
        SleepSummariesRepository sleepSummariesRepository = this.E;
        if (sleepSummariesRepository != null) {
            return sleepSummariesRepository;
        }
        kd4.d("mSleepSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final SummariesRepository x() {
        SummariesRepository summariesRepository = this.D;
        if (summariesRepository != null) {
            return summariesRepository;
        }
        kd4.d("mSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository y() {
        UserRepository userRepository = this.m;
        if (userRepository != null) {
            return userRepository;
        }
        kd4.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final WatchLocalizationRepository z() {
        WatchLocalizationRepository watchLocalizationRepository = this.J;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        kd4.d("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public void a(boolean z2) {
        E();
    }

    @DexIgnore
    public void b(String str) {
        kd4.b(str, "password");
        this.g = str;
        E();
    }

    @DexIgnore
    public final void c(SignUpSocialAuth signUpSocialAuth) {
        kd4.b(signUpSocialAuth, "auth");
        LoginSocialUseCase loginSocialUseCase = this.i;
        if (loginSocialUseCase != null) {
            loginSocialUseCase.a(new LoginSocialUseCase.c(signUpSocialAuth.getService(), signUpSocialAuth.getToken(), signUpSocialAuth.getClientId()), new c(this));
        } else {
            kd4.d("mLoginSocialUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, "email");
        this.f = str;
    }

    @DexIgnore
    public void a(String str, String str2) {
        kd4.b(str, "email");
        kd4.b(str2, "password");
        if (A()) {
            this.L.k();
            LoginEmailUseCase loginEmailUseCase = this.h;
            if (loginEmailUseCase != null) {
                loginEmailUseCase.a(new LoginEmailUseCase.c(str, str2), new d(this));
            } else {
                kd4.d("mLoginEmailUseCase");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void b(SignUpSocialAuth signUpSocialAuth) {
        kd4.b(signUpSocialAuth, "auth");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = SignUpPresenter.R.a();
        local.d(a2, "checkSocialAccountIsExisted " + signUpSocialAuth);
        CheckAuthenticationSocialExisting checkAuthenticationSocialExisting = this.B;
        if (checkAuthenticationSocialExisting != null) {
            checkAuthenticationSocialExisting.a(new CheckAuthenticationSocialExisting.b(signUpSocialAuth.getService(), signUpSocialAuth.getToken()), new b(this, signUpSocialAuth));
        } else {
            kd4.d("mCheckAuthenticationSocialExisting");
            throw null;
        }
    }

    @DexIgnore
    public final void c(String str) {
        kd4.b(str, "activeSerial");
        tq2 tq2 = this.k;
        if (tq2 != null) {
            tq2.a(new tq2.c(str), new i());
        } else {
            kd4.d("mReconnectDeviceUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void b(int i2, String str) {
        kd4.b(str, "errorMessage");
        if ((400 <= i2 && 407 >= i2) || (407 <= i2 && 499 >= i2)) {
            this.L.i0();
        } else if (i2 != 408) {
            this.L.b(i2, str);
        } else if (!as3.b(PortfolioApp.W.c())) {
            this.L.b(601, "");
        } else {
            this.L.b(i2, "");
        }
    }

    @DexIgnore
    public final void a(int i2, String str) {
        kd4.b(str, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = SignUpPresenter.R.a();
        local.d(a2, "handleError errorCode=" + i2 + " message=" + str);
        if (i2 != 408) {
            this.L.b(i2, str);
        } else if (!as3.b(PortfolioApp.W.c())) {
            this.L.b(601, "");
        } else {
            this.L.b(i2, "");
        }
    }

    @DexIgnore
    public void a(SignUpSocialAuth signUpSocialAuth) {
        kd4.b(signUpSocialAuth, "auth");
        this.L.k();
        b(signUpSocialAuth);
    }
}
