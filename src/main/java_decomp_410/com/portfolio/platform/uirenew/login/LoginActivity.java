package com.portfolio.platform.uirenew.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fk3;
import com.fossil.blesdk.obfuscated.gk3;
import com.fossil.blesdk.obfuscated.in2;
import com.fossil.blesdk.obfuscated.jn2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kn2;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LoginActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a G; // = new a((fd4) null);
    @DexIgnore
    public in2 B;
    @DexIgnore
    public jn2 C;
    @DexIgnore
    public kn2 D;
    @DexIgnore
    public MFLoginWechatManager E;
    @DexIgnore
    public LoginPresenter F;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            kd4.b(context, "context");
            context.startActivity(new Intent(context, LoginActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null) {
            in2 in2 = this.B;
            if (in2 != null) {
                in2.a(i, i2, intent);
                jn2 jn2 = this.C;
                if (jn2 != null) {
                    jn2.a(i, i2, intent);
                    kn2 kn2 = this.D;
                    if (kn2 != null) {
                        kn2.a(i, i2, intent);
                        Fragment a2 = getSupportFragmentManager().a((int) R.id.content);
                        if (a2 != null) {
                            a2.onActivityResult(i, i2, intent);
                            return;
                        }
                        return;
                    }
                    kd4.d("mLoginWeiboManager");
                    throw null;
                }
                kd4.d("mLoginGoogleManager");
                throw null;
            }
            kd4.d("mLoginFacebookManager");
            throw null;
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        fk3 fk3 = (fk3) getSupportFragmentManager().a((int) R.id.content);
        if (fk3 == null) {
            fk3 = fk3.n.b();
            a((Fragment) fk3, fk3.n.a(), (int) R.id.content);
        }
        l42 g = PortfolioApp.W.c().g();
        if (fk3 != null) {
            g.a(new gk3(this, fk3)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.login.LoginContract.View");
    }

    @DexIgnore
    public void onNewIntent(Intent intent) {
        kd4.b(intent, "intent");
        super.onNewIntent(intent);
        MFLoginWechatManager mFLoginWechatManager = this.E;
        if (mFLoginWechatManager != null) {
            Intent intent2 = getIntent();
            kd4.a((Object) intent2, "getIntent()");
            mFLoginWechatManager.a(intent2);
            return;
        }
        kd4.d("mLoginWechatManager");
        throw null;
    }
}
