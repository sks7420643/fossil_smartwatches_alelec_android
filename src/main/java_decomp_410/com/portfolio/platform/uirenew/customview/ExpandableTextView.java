package com.portfolio.platform.uirenew.customview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import androidx.appcompat.widget.AppCompatTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ExpandableTextView extends AppCompatTextView {
    @DexIgnore
    public /* final */ List<c> h;
    @DexIgnore
    public TimeInterpolator i;
    @DexIgnore
    public TimeInterpolator j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public int n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            ExpandableTextView.this.setHeight(((Integer) valueAnimator.getAnimatedValue()).intValue());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends AnimatorListenerAdapter {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ExpandableTextView.this.g();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(ExpandableTextView expandableTextView);
    }

    @DexIgnore
    public ExpandableTextView(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public boolean e() {
        if (this.m || this.l || this.k < 0) {
            return false;
        }
        f();
        measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
        this.n = getMeasuredHeight();
        this.l = true;
        setMaxLines(Integer.MAX_VALUE);
        measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
        ValueAnimator ofInt = ValueAnimator.ofInt(new int[]{this.n, getMeasuredHeight()});
        ofInt.addUpdateListener(new a());
        ofInt.addListener(new b());
        ofInt.setInterpolator(this.i);
        ofInt.setDuration(750).start();
        return true;
    }

    @DexIgnore
    public final void f() {
        for (c a2 : this.h) {
            a2.a(this);
        }
    }

    @DexIgnore
    public void g() {
        setMaxHeight(Integer.MAX_VALUE);
        setMinHeight(0);
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.height = -2;
        setLayoutParams(layoutParams);
        this.m = true;
        this.l = false;
    }

    @DexIgnore
    public TimeInterpolator getCollapseInterpolator() {
        return this.j;
    }

    @DexIgnore
    public TimeInterpolator getExpandInterpolator() {
        return this.i;
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (this.k == 0 && !this.m && !this.l) {
            i3 = View.MeasureSpec.makeMeasureSpec(0, 1073741824);
        }
        super.onMeasure(i2, i3);
    }

    @DexIgnore
    public void setCollapseInterpolator(TimeInterpolator timeInterpolator) {
        this.j = timeInterpolator;
    }

    @DexIgnore
    public void setExpandInterpolator(TimeInterpolator timeInterpolator) {
        this.i = timeInterpolator;
    }

    @DexIgnore
    public void setInterpolator(TimeInterpolator timeInterpolator) {
        this.i = timeInterpolator;
        this.j = timeInterpolator;
    }

    @DexIgnore
    public ExpandableTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public ExpandableTextView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.k = getMaxLines();
        this.h = new ArrayList();
        this.i = new AccelerateDecelerateInterpolator();
        this.j = new AccelerateDecelerateInterpolator();
    }
}
