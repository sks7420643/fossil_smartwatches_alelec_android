package com.portfolio.platform.uirenew.inappnotification;

import android.app.Activity;
import android.widget.Toast;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.la4;
import com.fossil.blesdk.obfuscated.le4;
import com.fossil.blesdk.obfuscated.ma4;
import com.fossil.blesdk.obfuscated.md4;
import com.portfolio.platform.data.InAppNotification;
import kotlin.jvm.internal.PropertyReference1;
import kotlin.jvm.internal.PropertyReference1Impl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class InAppNotificationUtils {
    @DexIgnore
    public static /* final */ la4 a; // = ma4.a(InAppNotificationUtils$Companion$instance$Anon2.INSTANCE);
    @DexIgnore
    public static /* final */ a b; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ /* synthetic */ le4[] a;

        /*
        static {
            PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(md4.a(a.class), "instance", "getInstance()Lcom/portfolio/platform/uirenew/inappnotification/InAppNotificationUtils;");
            md4.a((PropertyReference1) propertyReference1Impl);
            a = new le4[]{propertyReference1Impl};
        }
        */

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final InAppNotificationUtils a() {
            la4 a2 = InAppNotificationUtils.a;
            a aVar = InAppNotificationUtils.b;
            le4 le4 = a[0];
            return (InAppNotificationUtils) a2.getValue();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ InAppNotificationUtils a; // = new InAppNotificationUtils();
        @DexIgnore
        public static /* final */ b b; // = new b();

        @DexIgnore
        public final InAppNotificationUtils a() {
            return a;
        }
    }

    /*
    static {
        kd4.a((Object) InAppNotificationUtils$Companion$TAG$Anon1.INSTANCE.getClass().getSimpleName(), "InAppNotificationUtils::\u2026lass.javaClass.simpleName");
    }
    */

    @DexIgnore
    public final void a(Activity activity, InAppNotification inAppNotification) {
        throw null;
        // kd4.b(inAppNotification, "inAppNotification");
        // if (activity != null) {
        //     Toast.makeText(activity, "Title: " + inAppNotification.getTitle() + " - Message Body: " + inAppNotification.getContent(), 1).show();
        // }
    }
}
