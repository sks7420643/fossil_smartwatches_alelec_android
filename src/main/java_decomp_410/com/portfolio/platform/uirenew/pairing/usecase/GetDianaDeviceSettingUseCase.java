package com.portfolio.platform.uirenew.pairing.usecase;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.as3;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nn3;
import com.fossil.blesdk.obfuscated.on3;
import com.fossil.blesdk.obfuscated.pn3;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.vy2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetDianaDeviceSettingUseCase extends CoroutineUseCase<on3, pn3, nn3> {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ WatchAppRepository e;
    @DexIgnore
    public /* final */ ComplicationRepository f;
    @DexIgnore
    public /* final */ DianaPresetRepository g;
    @DexIgnore
    public /* final */ CategoryRepository h;
    @DexIgnore
    public /* final */ WatchFaceRepository i;
    @DexIgnore
    public /* final */ vy2 j;
    @DexIgnore
    public /* final */ px2 k;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase l;
    @DexIgnore
    public /* final */ en2 m;
    @DexIgnore
    public /* final */ WatchLocalizationRepository n;
    @DexIgnore
    public /* final */ AlarmsRepository o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = GetDianaDeviceSettingUseCase.class.getSimpleName();
        kd4.a((Object) simpleName, "GetDianaDeviceSettingUse\u2026se::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public GetDianaDeviceSettingUseCase(WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, DianaPresetRepository dianaPresetRepository, CategoryRepository categoryRepository, WatchFaceRepository watchFaceRepository, vy2 vy2, px2 px2, NotificationSettingsDatabase notificationSettingsDatabase, en2 en2, WatchLocalizationRepository watchLocalizationRepository, AlarmsRepository alarmsRepository) {
        kd4.b(watchAppRepository, "mWatchAppRepository");
        kd4.b(complicationRepository, "mComplicationRepository");
        kd4.b(dianaPresetRepository, "mDianaPresetRepository");
        kd4.b(categoryRepository, "mCategoryRepository");
        kd4.b(watchFaceRepository, "mWatchFaceRepository");
        kd4.b(vy2, "mGetApp");
        kd4.b(px2, "mGetAllContactGroups");
        kd4.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        kd4.b(en2, "mSharePref");
        kd4.b(watchLocalizationRepository, "mWatchLocalizationRepository");
        kd4.b(alarmsRepository, "mAlarmsRepository");
        this.e = watchAppRepository;
        this.f = complicationRepository;
        this.g = dianaPresetRepository;
        this.h = categoryRepository;
        this.i = watchFaceRepository;
        this.j = vy2;
        this.k = px2;
        this.l = notificationSettingsDatabase;
        this.m = en2;
        this.n = watchLocalizationRepository;
        this.o = alarmsRepository;
    }

    @DexIgnore
    public String c() {
        return p;
    }

    @DexIgnore
    public final fi4 d() {
        return ag4.b(b(), (CoroutineContext) null, (CoroutineStart) null, new GetDianaDeviceSettingUseCase$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public Object a(on3 on3, yb4<? super qa4> yb4) {
        if (on3 == null) {
            a(new nn3(600, ""));
            return qa4.a;
        }
        this.d = on3.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "download device setting of " + this.d);
        if (!as3.b(PortfolioApp.W.c())) {
            a(new nn3(601, ""));
            return qa4.a;
        }
        if (!TextUtils.isEmpty(this.d)) {
            DeviceHelper.a aVar = DeviceHelper.o;
            String str2 = this.d;
            if (str2 == null) {
                kd4.a();
                throw null;
            } else if (aVar.e(str2)) {
                d();
                return qa4.a;
            }
        }
        a(new nn3(600, ""));
        return qa4.a;
    }
}
