package com.portfolio.platform.uirenew.pairing.scanning;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.gn3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.sl2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.ul2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import java.util.HashMap;
import java.util.Map;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1", f = "PairingPresenter.kt", l = {281}, m = "invokeSuspend")
public final class PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ LinkDeviceUseCase.h $errorValue;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PairingPresenter$pairDeviceCallback$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1(PairingPresenter$pairDeviceCallback$Anon1 pairingPresenter$pairDeviceCallback$Anon1, LinkDeviceUseCase.h hVar, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = pairingPresenter$pairDeviceCallback$Anon1;
        this.$errorValue = hVar;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1 pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1 = new PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1(this.this$Anon0, this.$errorValue, yb4);
        pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1.p$ = (zg4) obj;
        return pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        String str = null;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = PairingPresenter.y.a();
            local.d(a2, "Inside .startPairClosestDevice pairDevice fail with error=" + this.$errorValue.b());
            ug4 a3 = this.this$Anon0.a.b();
            PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1 pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1 = new PairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(a3, pairingPresenter$pairDeviceCallback$Anon1$onError$Anon1$params$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        HashMap hashMap = (HashMap) obj;
        if (hashMap.containsKey("Style_Number") && hashMap.containsKey("Device_Name")) {
            this.this$Anon0.a.a("pair_fail", (Map<String, String>) hashMap);
        }
        this.this$Anon0.a.s.a();
        LinkDeviceUseCase.h hVar = this.$errorValue;
        if (hVar instanceof LinkDeviceUseCase.c) {
            gn3 g = this.this$Anon0.a.s;
            int b = this.$errorValue.b();
            ShineDevice f = this.this$Anon0.a.f;
            if (f != null) {
                str = f.getSerial();
            }
            g.c(b, str);
        } else if (hVar instanceof LinkDeviceUseCase.j) {
            this.this$Anon0.a.s.a(this.$errorValue.b(), this.$errorValue.a(), this.$errorValue.c());
        } else if (hVar instanceof LinkDeviceUseCase.d) {
            this.this$Anon0.a.s.y();
        }
        String valueOf = String.valueOf(this.$errorValue.b());
        sl2 a4 = AnalyticsHelper.f.a("setup_device_session_optional_error");
        a4.a("error_code", valueOf);
        ul2 o = this.this$Anon0.a.o();
        if (o != null) {
            o.a(a4);
        }
        ul2 o2 = this.this$Anon0.a.o();
        if (o2 != null) {
            o2.a(valueOf);
        }
        AnalyticsHelper.f.e("setup_device_session");
        return qa4.a;
    }
}
