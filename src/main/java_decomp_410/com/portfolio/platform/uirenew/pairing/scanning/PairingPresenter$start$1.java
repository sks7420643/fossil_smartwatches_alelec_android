package com.portfolio.platform.uirenew.pairing.scanning;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1", mo27670f = "PairingPresenter.kt", mo27671l = {100, 102}, mo27672m = "invokeSuspend")
public final class PairingPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24119p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1$1", mo27670f = "PairingPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1$1 */
    public static final class C68261 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<? extends com.portfolio.platform.data.model.Device>>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24120p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68261(com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1 pairingPresenter$start$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = pairingPresenter$start$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1.C68261 r0 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1.C68261(this.this$0, yb4);
            r0.f24120p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1.C68261) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return this.this$0.this$0.f24103u.getAllDevice();
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1$2")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1$2", mo27670f = "PairingPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1$2 */
    public static final class C68272 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.util.List<? extends com.portfolio.platform.data.model.SKUModel>>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24121p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68272(com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1 pairingPresenter$start$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = pairingPresenter$start$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1.C68272 r0 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1.C68272(this.this$0, yb4);
            r0.f24121p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1.C68272) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return this.this$0.this$0.f24103u.getSupportedSku();
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingPresenter$start$1(com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter pairingPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = pairingPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1 pairingPresenter$start$1 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1(this.this$0, yb4);
        pairingPresenter$start$1.f24119p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return pairingPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0112  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.util.ArrayList<com.portfolio.platform.data.model.SKUModel> arrayList;
        com.fossil.blesdk.obfuscated.gn3 g;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.util.ArrayList<com.portfolio.platform.data.model.Device> arrayList2;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f24119p$;
            this.this$0.mo41663s().clear();
            arrayList2 = this.this$0.mo41663s();
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31441c();
            com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1.C68261 r6 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1.C68261(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = arrayList2;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b, r6, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            arrayList2 = (java.util.ArrayList) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            arrayList = (java.util.ArrayList) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            arrayList.addAll((java.util.Collection) obj);
            com.fossil.blesdk.obfuscated.C2796rc.m13141a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c()).mo15484a(this.this$0.f24100r, new android.content.IntentFilter("SCAN_DEVICE_FOUND"));
            this.this$0.f24102t.mo40310k();
            this.this$0.f24091i = new android.os.Handler(android.os.Looper.getMainLooper());
            if (this.this$0.f24088f != null) {
                com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase e = this.this$0.f24102t;
                com.misfit.frameworks.buttonservice.model.ShineDevice f = this.this$0.f24088f;
                if (f != null) {
                    e.mo40296a(f, (com.portfolio.platform.CoroutineUseCase.C5606e<? super com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6150i, ? super com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6149h>) this.this$0.f24099q);
                    com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39773a(com.misfit.frameworks.buttonservice.communite.CommunicateMode.LINK);
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            }
            com.fossil.blesdk.obfuscated.bn2 bn2 = com.fossil.blesdk.obfuscated.bn2.f13679d;
            g = this.this$0.f24101s;
            if (g == null) {
                if (com.fossil.blesdk.obfuscated.bn2.m20275a(bn2, ((com.fossil.blesdk.obfuscated.lm3) g).getContext(), "PAIR_DEVICE", false, 4, (java.lang.Object) null)) {
                    this.this$0.mo41665u();
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.pairing.PairingFragment");
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        arrayList2.addAll((java.util.Collection) obj);
        this.this$0.mo41658n().clear();
        java.util.ArrayList<com.portfolio.platform.data.model.SKUModel> n = this.this$0.mo41658n();
        com.fossil.blesdk.obfuscated.ug4 b2 = this.this$0.mo31441c();
        com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1.C68272 r62 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1.C68272(this, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.L$1 = n;
        this.label = 2;
        java.lang.Object a2 = com.fossil.blesdk.obfuscated.yf4.m30997a(b2, r62, this);
        if (a2 == a) {
            return a;
        }
        arrayList = n;
        obj = a2;
        arrayList.addAll((java.util.Collection) obj);
        com.fossil.blesdk.obfuscated.C2796rc.m13141a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c()).mo15484a(this.this$0.f24100r, new android.content.IntentFilter("SCAN_DEVICE_FOUND"));
        this.this$0.f24102t.mo40310k();
        this.this$0.f24091i = new android.os.Handler(android.os.Looper.getMainLooper());
        if (this.this$0.f24088f != null) {
        }
        com.fossil.blesdk.obfuscated.bn2 bn22 = com.fossil.blesdk.obfuscated.bn2.f13679d;
        g = this.this$0.f24101s;
        if (g == null) {
        }
    }
}
