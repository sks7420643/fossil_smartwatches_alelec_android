package com.portfolio.platform.uirenew.pairing.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$1", mo27670f = "GetHybridDeviceSettingUseCase.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.util.SparseArray $data;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isMovemberModel;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serialNumber;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24144p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$1(android.util.SparseArray sparseArray, boolean z, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$data = sparseArray;
        this.$isMovemberModel = z;
        this.$serialNumber = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$1 getHybridDeviceSettingUseCase$saveNotificationSettingToDevice$1 = new com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$1(this.$data, this.$isMovemberModel, this.$serialNumber, yb4);
        getHybridDeviceSettingUseCase$saveNotificationSettingToDevice$1.f24144p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return getHybridDeviceSettingUseCase$saveNotificationSettingToDevice$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings a = com.portfolio.platform.util.NotificationAppHelper.f24430b.mo41917a((android.util.SparseArray<java.util.List<com.fossil.wearables.fsl.shared.BaseFeatureModel>>) this.$data, this.$isMovemberModel);
            com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34494a(a, this.$serialNumber);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String e = com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.f24136k;
            local.mo33255d(e, "saveNotificationSettingToDevice, total: " + a.getNotificationFilters().size() + " items");
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
