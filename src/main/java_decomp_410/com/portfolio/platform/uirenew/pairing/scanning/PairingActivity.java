package com.portfolio.platform.uirenew.pairing.scanning;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hn3;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lm3;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PairingActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ String C;
    @DexIgnore
    public static /* final */ a D; // = new a((fd4) null);
    @DexIgnore
    public PairingPresenter B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return PairingActivity.C;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final void a(Context context, boolean z) {
            kd4.b(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = a();
            local.d(a, "start isOnboarding=" + z);
            Intent intent = new Intent(context, PairingActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = PairingActivity.class.getSimpleName();
        kd4.a((Object) simpleName, "PairingActivity::class.java.simpleName");
        C = simpleName;
    }
    */

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        lm3 lm3 = (lm3) getSupportFragmentManager().a((int) R.id.content);
        Intent intent = getIntent();
        boolean z = false;
        if (intent != null) {
            z = intent.getBooleanExtra("IS_ONBOARDING_FLOW", false);
        }
        if (lm3 == null) {
            lm3 = lm3.n.a(z);
            a((Fragment) lm3, (int) R.id.content);
        }
        PortfolioApp.W.c().g().a(new hn3(lm3)).a(this);
        if (bundle != null) {
            PairingPresenter pairingPresenter = this.B;
            if (pairingPresenter != null) {
                pairingPresenter.d(bundle.getBoolean("KEY_IS_PAIR_DEVICE_FAIL_POPUP"));
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        if (bundle != null) {
            PairingPresenter pairingPresenter = this.B;
            if (pairingPresenter != null) {
                bundle.putBoolean("KEY_IS_PAIR_DEVICE_FAIL_POPUP", pairingPresenter.t());
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        s();
    }

    @DexIgnore
    public final void s() {
        ps3.a.a(this, MFDeviceService.class, Constants.START_FOREGROUND_ACTION);
        ps3.a.a(this, ButtonService.class, Constants.START_FOREGROUND_ACTION);
    }
}
