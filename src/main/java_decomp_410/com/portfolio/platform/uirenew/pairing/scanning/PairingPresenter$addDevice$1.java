package com.portfolio.platform.uirenew.pairing.scanning;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$1", mo27670f = "PairingPresenter.kt", mo27671l = {370}, mo27672m = "invokeSuspend")
public final class PairingPresenter$addDevice$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.model.ShineDevice $shineDevice;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24111p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingPresenter$addDevice$1(com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter pairingPresenter, com.misfit.frameworks.buttonservice.model.ShineDevice shineDevice, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = pairingPresenter;
        this.$shineDevice = shineDevice;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$1 pairingPresenter$addDevice$1 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$1(this.this$0, this.$shineDevice, yb4);
        pairingPresenter$addDevice$1.f24111p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return pairingPresenter$addDevice$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        T t;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24111p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$1$serial$1 pairingPresenter$addDevice$1$serial$1 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$1$serial$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, pairingPresenter$addDevice$1$serial$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        java.lang.String str = (java.lang.String) obj;
        java.util.Iterator<T> it = this.this$0.mo41662r().iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (com.fossil.blesdk.obfuscated.dc4.m20839a(com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.misfit.frameworks.buttonservice.model.ShineDevice) ((kotlin.Pair) t).getFirst()).getSerial(), (java.lang.Object) this.$shineDevice.getSerial())).booleanValue()) {
                break;
            }
        }
        kotlin.Pair pair = (kotlin.Pair) t;
        if (pair == null) {
            this.this$0.mo41662r().add(new kotlin.Pair(this.$shineDevice, str));
        } else {
            ((com.misfit.frameworks.buttonservice.model.ShineDevice) pair.getFirst()).updateRssi(this.$shineDevice.getRssi());
        }
        com.fossil.blesdk.obfuscated.gn3 g = this.this$0.f24101s;
        com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter pairingPresenter = this.this$0;
        g.mo27737h(pairingPresenter.mo41648a(pairingPresenter.mo41662r()));
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
