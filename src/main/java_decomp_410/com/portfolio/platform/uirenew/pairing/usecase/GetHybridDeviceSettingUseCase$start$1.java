package com.portfolio.platform.uirenew.pairing.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$start$1", mo27670f = "GetHybridDeviceSettingUseCase.kt", mo27671l = {61, 62, 63, 64, 65, 77, 85, 101}, mo27672m = "invokeSuspend")
public final class GetHybridDeviceSettingUseCase$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24145p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetHybridDeviceSettingUseCase$start$1(com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = getHybridDeviceSettingUseCase;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$start$1 getHybridDeviceSettingUseCase$start$1 = new com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$start$1(this.this$0, yb4);
        getHybridDeviceSettingUseCase$start$1.f24145p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return getHybridDeviceSettingUseCase$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0370, code lost:
        r14 = r13.this$0.f24141h;
        r0 = r13.this$0.f24137d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x037c, code lost:
        if (r0 == null) goto L_0x03d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x037e, code lost:
        r14 = r14.getAllNotificationsByHour(r0, com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        r0 = r13.this$0.f24140g;
        r1 = com.portfolio.platform.helper.DeviceHelper.f21188o;
        r2 = r13.this$0.f24137d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0396, code lost:
        if (r2 == null) goto L_0x03ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0398, code lost:
        r0 = r0.getSkuModelBySerialPrefix(r1.mo39559b(r2));
        r1 = com.portfolio.platform.util.NotificationAppHelper.f24430b;
        r2 = r13.this$0.f24137d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x03a8, code lost:
        if (r2 == null) goto L_0x03ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x03aa, code lost:
        r0 = r1.mo41923a(r0, r2);
        r1 = r13.this$0;
        r2 = r1.f24137d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x03b4, code lost:
        if (r2 == null) goto L_0x03c6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x03b6, code lost:
        com.fossil.blesdk.obfuscated.fi4 unused = r1.mo41679a(r14, r2, r0);
        r13.this$0.mo34436a(new com.fossil.blesdk.obfuscated.pn3());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x03c5, code lost:
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x03c6, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x03c9, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x03ca, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x03cd, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x03ce, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x03d1, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x03d2, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x03d5, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x03d6, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x03d9, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x03da, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x03dd, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x03de, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x03e1, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x03e2, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x03e5, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x03e6, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x03e9, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x03ea, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x03ed, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x03ee, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x03f1, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00a8, code lost:
        r14 = r13.this$0.f24139f;
        r5 = r13.this$0.f24137d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00b4, code lost:
        if (r5 == null) goto L_0x03ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00b6, code lost:
        r13.L$0 = r1;
        r13.label = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00bf, code lost:
        if (r14.downloadAllMicroApp(r5, r13) != r0) goto L_0x00c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00c1, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00c2, code lost:
        r14 = r13.this$0.f24138e;
        r5 = r13.this$0.f24137d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00ce, code lost:
        if (r5 == null) goto L_0x03ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00d0, code lost:
        r13.L$0 = r1;
        r13.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00d9, code lost:
        if (r14.downloadRecommendPresetList(r5, r13) != r0) goto L_0x00dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00db, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00dc, code lost:
        r14 = r13.this$0.f24138e;
        r5 = r13.this$0.f24137d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00e8, code lost:
        if (r5 == null) goto L_0x03e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ea, code lost:
        r13.L$0 = r1;
        r13.label = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00f3, code lost:
        if (r14.downloadPresetList(r5, r13) != r0) goto L_0x00f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00f5, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00f6, code lost:
        r14 = r13.this$0.f24143j;
        r13.L$0 = r1;
        r13.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0105, code lost:
        if (r14.downloadAlarms(r13) != r0) goto L_0x0108;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0107, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0108, code lost:
        r14 = new kotlin.jvm.internal.Ref$IntRef();
        r14.element = 1;
        r5 = new kotlin.jvm.internal.Ref$IntRef();
        r5.element = 0;
        r6 = r13.this$0.f24140g;
        r7 = r13.this$0.f24137d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0122, code lost:
        if (r7 == null) goto L_0x03e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0124, code lost:
        r6 = r6.getDeviceBySerial(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0128, code lost:
        if (r6 == null) goto L_0x0136;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x012a, code lost:
        r14.element = r6.getMajor();
        r5.element = r6.getMinor();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0136, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.f24136k, "download variant with major " + r14.element + " minor " + r5.element);
        r7 = r13.this$0.f24139f;
        r8 = r13.this$0.f24137d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x016c, code lost:
        if (r8 == null) goto L_0x03de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x016e, code lost:
        r9 = java.lang.String.valueOf(r14.element);
        r10 = java.lang.String.valueOf(r5.element);
        r13.L$0 = r1;
        r13.L$1 = r14;
        r13.L$2 = r5;
        r13.L$3 = r6;
        r13.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0189, code lost:
        if (r7.downloadMicroAppVariant(r8, r9, r10, r13) != r0) goto L_0x018c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x018b, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x018c, code lost:
        r7 = r14;
        r8 = r1;
        r12 = r6;
        r6 = r5;
        r5 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0191, code lost:
        r14 = r13.this$0.f24138e;
        r1 = r13.this$0.f24137d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x019d, code lost:
        if (r1 == null) goto L_0x03da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x019f, code lost:
        r1 = r14.getPresetList(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x01a7, code lost:
        if (r1.isEmpty() == false) goto L_0x01f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x01a9, code lost:
        r14 = r13.this$0.f24138e;
        r9 = r13.this$0.f24137d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x01b5, code lost:
        if (r9 == null) goto L_0x01f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x01b7, code lost:
        r14 = r14.getHybridRecommendPresetList(r9);
        r9 = r14.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x01c3, code lost:
        if (r9.hasNext() == false) goto L_0x01d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01c5, code lost:
        r1.add(com.portfolio.platform.data.model.room.microapp.HybridPreset.Companion.cloneFromDefault((com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset) r9.next()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x01d5, code lost:
        r9 = r13.this$0.f24138e;
        r13.L$0 = r8;
        r13.L$1 = r7;
        r13.L$2 = r6;
        r13.L$3 = r5;
        r13.L$4 = r1;
        r13.L$5 = r14;
        r13.label = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x01ee, code lost:
        if (r9.upsertHybridPresetList(r1, r13) != r0) goto L_0x01f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01f0, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x01f1, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x01f4, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x01f5, code lost:
        r14 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01fd, code lost:
        if (r14.hasNext() == false) goto L_0x0215;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01ff, code lost:
        r9 = r14.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0212, code lost:
        if (com.fossil.blesdk.obfuscated.dc4.m20839a(((com.portfolio.platform.data.model.room.microapp.HybridPreset) r9).isActive()).booleanValue() == false) goto L_0x01f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0215, code lost:
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0216, code lost:
        r9 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0218, code lost:
        if (r9 != null) goto L_0x0240;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x021e, code lost:
        if (r1.isEmpty() == false) goto L_0x0240;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0220, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.f24136k, "activePreset is null, preset list is empty?????");
        r13.this$0.mo34434a(new com.fossil.blesdk.obfuscated.nn3(600, ""));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x023f, code lost:
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0240, code lost:
        if (r9 != null) goto L_0x0287;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0242, code lost:
        r14 = r1.get(0);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.f24136k, "Active preset is null ,pick " + r14);
        r14.setActive(true);
        r2 = r13.this$0.f24138e;
        r13.L$0 = r8;
        r13.L$1 = r7;
        r13.L$2 = r6;
        r13.L$3 = r5;
        r13.L$4 = r1;
        r13.L$5 = r14;
        r13.label = 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0283, code lost:
        if (r2.upsertHybridPreset(r14, r13) != r0) goto L_0x0286;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0285, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0286, code lost:
        r9 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0287, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.f24136k, "activePreset=" + r9);
        r14 = r9.getButtons().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x02b1, code lost:
        if (r14.hasNext() == false) goto L_0x031d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x02b3, code lost:
        r0 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r14.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x02c7, code lost:
        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) r0.getAppId(), (java.lang.Object) com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue()) == false) goto L_0x02ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x02c9, code lost:
        r0 = r0.getSettings();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x02d1, code lost:
        if (android.text.TextUtils.isEmpty(r0) != false) goto L_0x02ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        r0 = (com.portfolio.platform.data.model.setting.SecondTimezoneSetting) new com.google.gson.Gson().mo23093a(r0, com.portfolio.platform.data.model.setting.SecondTimezoneSetting.class);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x02e0, code lost:
        if (r0 == null) goto L_0x02ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x02ea, code lost:
        if (android.text.TextUtils.isEmpty(r0.getTimeZoneId()) != false) goto L_0x02ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x02ec, code lost:
        com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34552n(r0.getTimeZoneId());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x02fa, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x02fb, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.f24136k, "parse secondTimezone, ex=" + r0);
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x031d, code lost:
        r14 = r13.this$0.f24137d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0323, code lost:
        if (r14 == null) goto L_0x03d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0325, code lost:
        r14 = com.fossil.blesdk.obfuscated.sj2.m27757a(r9, r14, r13.this$0.f24140g, r13.this$0.f24139f);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.f24136k, "bleMappingList " + r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0358, code lost:
        if ((!r14.isEmpty()) == false) goto L_0x0370;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x035a, code lost:
        r0 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
        r1 = r13.this$0.f24137d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0366, code lost:
        if (r1 == null) goto L_0x036c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0368, code lost:
        r0.mo34520b(r1, (java.util.List<? extends com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping>) r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x036c, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x036f, code lost:
        throw null;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        kotlin.jvm.internal.Ref$IntRef ref$IntRef;
        kotlin.jvm.internal.Ref$IntRef ref$IntRef2;
        com.portfolio.platform.data.model.Device device;
        com.fossil.blesdk.obfuscated.zg4 zg42;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        switch (this.label) {
            case 0:
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                zg42 = this.f24145p$;
                com.portfolio.platform.data.source.CategoryRepository b = this.this$0.f24142i;
                this.L$0 = zg42;
                this.label = 1;
                if (b.downloadCategories(this) == a) {
                    return a;
                }
                break;
            case 1:
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 2:
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 3:
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 4:
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 5:
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 6:
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                ref$IntRef = (kotlin.jvm.internal.Ref$IntRef) this.L$1;
                ref$IntRef2 = (kotlin.jvm.internal.Ref$IntRef) this.L$2;
                device = (com.portfolio.platform.data.model.Device) this.L$3;
                break;
            case 7:
                java.util.List list = (java.util.List) this.L$5;
                java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridPreset> arrayList = (java.util.ArrayList) this.L$4;
                device = (com.portfolio.platform.data.model.Device) this.L$3;
                ref$IntRef2 = (kotlin.jvm.internal.Ref$IntRef) this.L$2;
                ref$IntRef = (kotlin.jvm.internal.Ref$IntRef) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 8:
                java.util.ArrayList arrayList2 = (java.util.ArrayList) this.L$4;
                com.portfolio.platform.data.model.Device device2 = (com.portfolio.platform.data.model.Device) this.L$3;
                kotlin.jvm.internal.Ref$IntRef ref$IntRef3 = (kotlin.jvm.internal.Ref$IntRef) this.L$2;
                kotlin.jvm.internal.Ref$IntRef ref$IntRef4 = (kotlin.jvm.internal.Ref$IntRef) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.portfolio.platform.data.model.room.microapp.HybridPreset hybridPreset = (com.portfolio.platform.data.model.room.microapp.HybridPreset) this.L$5;
                break;
            default:
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
