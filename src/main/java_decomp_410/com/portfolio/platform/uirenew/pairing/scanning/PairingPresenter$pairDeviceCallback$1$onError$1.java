package com.portfolio.platform.uirenew.pairing.scanning;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1", mo27670f = "PairingPresenter.kt", mo27671l = {281}, mo27672m = "invokeSuspend")
public final class PairingPresenter$pairDeviceCallback$1$onError$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6149h $errorValue;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24114p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingPresenter$pairDeviceCallback$1$onError$1(com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1 pairingPresenter$pairDeviceCallback$1, com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6149h hVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = pairingPresenter$pairDeviceCallback$1;
        this.$errorValue = hVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1 pairingPresenter$pairDeviceCallback$1$onError$1 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1(this.this$0, this.$errorValue, yb4);
        pairingPresenter$pairDeviceCallback$1$onError$1.f24114p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return pairingPresenter$pairDeviceCallback$1$onError$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        java.lang.String str = null;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24114p$;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter.f24087y.mo41670a();
            local.mo33255d(a2, "Inside .startPairClosestDevice pairDevice fail with error=" + this.$errorValue.mo40320b());
            com.fossil.blesdk.obfuscated.ug4 a3 = this.this$0.f24113a.mo31440b();
            com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1$params$1 pairingPresenter$pairDeviceCallback$1$onError$1$params$1 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1$params$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a3, pairingPresenter$pairDeviceCallback$1$onError$1$params$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        java.util.HashMap hashMap = (java.util.HashMap) obj;
        if (hashMap.containsKey("Style_Number") && hashMap.containsKey("Device_Name")) {
            this.this$0.f24113a.mo41650a("pair_fail", (java.util.Map<java.lang.String, java.lang.String>) hashMap);
        }
        this.this$0.f24113a.f24101s.mo27729a();
        com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6149h hVar = this.$errorValue;
        if (hVar instanceof com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6144c) {
            com.fossil.blesdk.obfuscated.gn3 g = this.this$0.f24113a.f24101s;
            int b = this.$errorValue.mo40320b();
            com.misfit.frameworks.buttonservice.model.ShineDevice f = this.this$0.f24113a.f24088f;
            if (f != null) {
                str = f.getSerial();
            }
            g.mo27735c(b, str);
        } else if (hVar instanceof com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6151j) {
            this.this$0.f24113a.f24101s.mo27730a(this.$errorValue.mo40320b(), this.$errorValue.mo40319a(), this.$errorValue.mo40321c());
        } else if (hVar instanceof com.portfolio.platform.p007ui.device.domain.usecase.LinkDeviceUseCase.C6145d) {
            this.this$0.f24113a.f24101s.mo27743y();
        }
        java.lang.String valueOf = java.lang.String.valueOf(this.$errorValue.mo40320b());
        com.fossil.blesdk.obfuscated.sl2 a4 = com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39510a("setup_device_session_optional_error");
        a4.mo30871a("error_code", valueOf);
        com.fossil.blesdk.obfuscated.ul2 o = this.this$0.f24113a.mo41659o();
        if (o != null) {
            o.mo31554a(a4);
        }
        com.fossil.blesdk.obfuscated.ul2 o2 = this.this$0.f24113a.mo41659o();
        if (o2 != null) {
            o2.mo31555a(valueOf);
        }
        com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39521e("setup_device_session");
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
