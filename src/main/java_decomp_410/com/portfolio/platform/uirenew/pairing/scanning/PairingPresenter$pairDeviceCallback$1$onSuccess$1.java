package com.portfolio.platform.uirenew.pairing.scanning;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1", mo27670f = "PairingPresenter.kt", mo27671l = {254, 260}, mo27672m = "invokeSuspend")
public final class PairingPresenter$pairDeviceCallback$1$onSuccess$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $devicePairingSerial;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24116p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1 this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1$1", mo27670f = "PairingPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1$1 */
    public static final class C68251 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.fi4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24117p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68251(com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1 pairingPresenter$pairDeviceCallback$1$onSuccess$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = pairingPresenter$pairDeviceCallback$1$onSuccess$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1.C68251 r0 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1.C68251(this.this$0, yb4);
            r0.f24117p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1.C68251) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:5:0x0030, code lost:
            if (r0 != null) goto L_0x0035;
         */
        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.String str;
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                java.lang.String deviceNameBySerial = this.this$0.this$0.f24113a.f24103u.getDeviceNameBySerial(this.this$0.$devicePairingSerial);
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile a = com.portfolio.platform.helper.DeviceHelper.f21188o.mo39565e().mo39545a(this.this$0.$devicePairingSerial);
                if (a != null) {
                    str = a.getFirmwareVersion();
                }
                str = "";
                com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39517c().mo39497a(com.portfolio.platform.helper.DeviceHelper.f21188o.mo39559b(this.this$0.$devicePairingSerial), deviceNameBySerial, str);
                return this.this$0.this$0.f24113a.f24105w.mo34435a(new com.portfolio.platform.usecase.SetNotificationUseCase.C6914b(this.this$0.$devicePairingSerial), (com.portfolio.platform.CoroutineUseCase.C5606e) null);
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingPresenter$pairDeviceCallback$1$onSuccess$1(com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1 pairingPresenter$pairDeviceCallback$1, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = pairingPresenter$pairDeviceCallback$1;
        this.$devicePairingSerial = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1 pairingPresenter$pairDeviceCallback$1$onSuccess$1 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1(this.this$0, this.$devicePairingSerial, yb4);
        pairingPresenter$pairDeviceCallback$1$onSuccess$1.f24116p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return pairingPresenter$pairDeviceCallback$1$onSuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008e  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.ul2 o;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f24116p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.f24113a.mo31440b();
            com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1$params$1 pairingPresenter$pairDeviceCallback$1$onSuccess$1$params$1 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1$params$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, pairingPresenter$pairDeviceCallback$1$onSuccess$1$params$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            java.util.HashMap hashMap = (java.util.HashMap) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            o = this.this$0.f24113a.mo41659o();
            if (o != null) {
                o.mo31555a("");
            }
            com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39521e("setup_device_session");
            this.this$0.f24113a.mo41666v();
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        java.util.HashMap hashMap2 = (java.util.HashMap) obj;
        if (hashMap2.containsKey("Style_Number") && hashMap2.containsKey("Device_Name")) {
            this.this$0.f24113a.mo41650a("pair_success", (java.util.Map<java.lang.String, java.lang.String>) hashMap2);
            this.this$0.f24113a.mo41651a((java.util.Map<java.lang.String, java.lang.String>) hashMap2);
        }
        com.fossil.blesdk.obfuscated.ug4 b = this.this$0.f24113a.mo31441c();
        com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1.C68251 r5 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1.C68251(this, (com.fossil.blesdk.obfuscated.yb4) null);
        this.L$0 = zg4;
        this.L$1 = hashMap2;
        this.label = 2;
        if (com.fossil.blesdk.obfuscated.yf4.m30997a(b, r5, this) == a) {
            return a;
        }
        o = this.this$0.f24113a.mo41659o();
        if (o != null) {
        }
        com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39521e("setup_device_session");
        this.this$0.f24113a.mo41666v();
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
