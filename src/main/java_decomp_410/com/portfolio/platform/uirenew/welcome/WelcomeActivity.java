package com.portfolio.platform.uirenew.welcome;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ps3;
import com.fossil.blesdk.obfuscated.wq3;
import com.fossil.blesdk.obfuscated.xq3;
import com.fossil.blesdk.obfuscated.yq3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WelcomeActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public xq3 B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            kd4.b(context, "context");
            context.startActivity(new Intent(context, WelcomeActivity.class));
        }

        @DexIgnore
        public final void b(Context context) {
            kd4.b(context, "context");
            Intent intent = new Intent(context, WelcomeActivity.class);
            intent.addFlags(268468224);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_base);
        wq3 wq3 = (wq3) getSupportFragmentManager().a((int) R.id.content);
        if (wq3 == null) {
            wq3 = new wq3();
            a((Fragment) wq3, "WelcomeFragment", (int) R.id.content);
        }
        PortfolioApp.W.c().g().a(new yq3(wq3)).a(this);
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        ps3.a.a(this, MFDeviceService.class, Constants.STOP_FOREGROUND_ACTION);
        ps3.a.a(this, ButtonService.class, Constants.STOP_FOREGROUND_ACTION);
    }
}
