package com.portfolio.platform.uirenew.watchsetting.finddevice;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$Anon1;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FindDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super String>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ FindDevicePresenter$mDeviceWrapperTransformation$Anon1.Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FindDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(yb4 yb4, FindDevicePresenter$mDeviceWrapperTransformation$Anon1.Anon1 anon1) {
        super(2, yb4);
        this.this$Anon0 = anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        FindDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 findDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$invokeSuspend$$inlined$let$lambda$Anon1 = new FindDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$invokeSuspend$$inlined$let$lambda$Anon1(yb4, this.this$Anon0);
        findDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$invokeSuspend$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return findDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$invokeSuspend$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((FindDevicePresenter$mDeviceWrapperTransformation$Anon1$Anon1$invokeSuspend$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            DeviceRepository b = this.this$Anon0.this$Anon0.a.o;
            String str = this.this$Anon0.$serial;
            kd4.a((Object) str, "serial");
            return b.getDeviceNameBySerial(str);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
