package com.portfolio.platform.uirenew.watchsetting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.tp3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchSettingActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ String B;
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return WatchSettingActivity.B;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            kd4.b(context, "context");
            kd4.b(str, "serial");
            Intent intent = new Intent(context, WatchSettingActivity.class);
            intent.putExtra("SERIAL", str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = a();
            local.d(a, "start with " + str);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = WatchSettingActivity.class.getSimpleName();
        kd4.a((Object) simpleName, "WatchSettingActivity::class.java.simpleName");
        B = simpleName;
    }
    */

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        tp3 tp3 = (tp3) getSupportFragmentManager().a((int) R.id.content);
        if (tp3 == null) {
            tp3 = tp3.q.b();
            a((Fragment) tp3, tp3.q.a(), (int) R.id.content);
        }
        if (bundle != null && bundle.containsKey("SERIAL")) {
            String string = bundle.getString("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String f = f();
            local.d(f, "retrieve serial from savedInstanceState " + string);
            if (string != null) {
                Bundle bundle2 = new Bundle();
                bundle2.putString("SERIAL", string);
                tp3.setArguments(bundle2);
            }
        }
        if (getIntent() != null) {
            String stringExtra = getIntent().getStringExtra("SERIAL");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String f2 = f();
            local2.d(f2, "retrieve serial from intent " + stringExtra);
            Bundle bundle3 = new Bundle();
            bundle3.putString("SERIAL", stringExtra);
            tp3.setArguments(bundle3);
        }
    }
}
