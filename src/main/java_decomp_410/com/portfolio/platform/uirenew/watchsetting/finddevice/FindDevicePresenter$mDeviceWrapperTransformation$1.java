package com.portfolio.platform.uirenew.watchsetting.finddevice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FindDevicePresenter$mDeviceWrapperTransformation$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter f24332a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1$1", mo27670f = "FindDevicePresenter.kt", mo27671l = {65, 68}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1$1 */
    public static final class C68811 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.lang.String $serial;
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public java.lang.Object L$1;
        @DexIgnore
        public java.lang.Object L$2;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24333p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68811(com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1 findDevicePresenter$mDeviceWrapperTransformation$1, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = findDevicePresenter$mDeviceWrapperTransformation$1;
            this.$serial = str;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1.C68811 r0 = new com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1.C68811(this.this$0, this.$serial, yb4);
            r0.f24333p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1.C68811) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0083  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00b9  */
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.portfolio.platform.data.model.Device device;
            com.fossil.blesdk.obfuscated.zg4 zg4;
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            java.lang.Boolean bool = null;
            boolean z = true;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                zg4 = this.f24333p$;
                com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.f24332a.mo31441c();
                com.portfolio.platform.uirenew.watchsetting.finddevice.C6882xccdee7fa findDevicePresenter$mDeviceWrapperTransformation$1$1$deviceModel$1 = new com.portfolio.platform.uirenew.watchsetting.finddevice.C6882xccdee7fa(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, findDevicePresenter$mDeviceWrapperTransformation$1$1$deviceModel$1, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else if (i == 2) {
                com.portfolio.platform.data.model.Device device2 = (com.portfolio.platform.data.model.Device) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                device = (com.portfolio.platform.data.model.Device) this.L$2;
                java.lang.String str = (java.lang.String) obj;
                if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.$serial, (java.lang.Object) this.this$0.f24332a.mo41824i())) {
                    com.misfit.frameworks.buttonservice.IButtonConnectivity b = com.portfolio.platform.PortfolioApp.f20941W.mo34585b();
                    if (b != null) {
                        if (b.getGattState(this.$serial) != 2) {
                            z = false;
                        }
                        bool = com.fossil.blesdk.obfuscated.dc4.m20839a(z);
                    }
                    androidx.lifecycle.MutableLiveData c = this.this$0.f24332a.f24307g;
                    com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.C6857c cVar = new com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.C6857c(device, str, bool != null ? bool.booleanValue() : false, true, (java.lang.Boolean) null, 16, (com.fossil.blesdk.obfuscated.fd4) null);
                    c.mo2280a(cVar);
                } else {
                    androidx.lifecycle.MutableLiveData c2 = this.this$0.f24332a.f24307g;
                    com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.C6857c cVar2 = new com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.C6857c(device, str, false, false, (java.lang.Boolean) null, 16, (com.fossil.blesdk.obfuscated.fd4) null);
                    c2.mo2280a(cVar2);
                }
                this.this$0.f24332a.f24317q.mo29260b(this.this$0.f24332a.f24316p.mo26968F(), false);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            com.portfolio.platform.data.model.Device device3 = (com.portfolio.platform.data.model.Device) obj;
            if (device3 != null) {
                com.fossil.blesdk.obfuscated.ug4 a3 = this.this$0.f24332a.mo31441c();
                com.portfolio.platform.uirenew.watchsetting.finddevice.C6880x58074274 findDevicePresenter$mDeviceWrapperTransformation$1$1$invokeSuspend$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.watchsetting.finddevice.C6880x58074274((com.fossil.blesdk.obfuscated.yb4) null, this);
                this.L$0 = zg4;
                this.L$1 = device3;
                this.L$2 = device3;
                this.label = 2;
                java.lang.Object a4 = com.fossil.blesdk.obfuscated.yf4.m30997a(a3, findDevicePresenter$mDeviceWrapperTransformation$1$1$invokeSuspend$$inlined$let$lambda$1, this);
                if (a4 == a) {
                    return a;
                }
                device = device3;
                obj = a4;
                java.lang.String str2 = (java.lang.String) obj;
                if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.$serial, (java.lang.Object) this.this$0.f24332a.mo41824i())) {
                }
            }
            this.this$0.f24332a.f24317q.mo29260b(this.this$0.f24332a.f24316p.mo26968F(), false);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
    }

    @DexIgnore
    public FindDevicePresenter$mDeviceWrapperTransformation$1(com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter findDevicePresenter) {
        this.f24332a = findDevicePresenter;
    }

    @DexIgnore
    /* renamed from: a */
    public final androidx.lifecycle.MutableLiveData<com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.C6857c> apply(java.lang.String str) {
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(this.f24332a.mo31443e(), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1.C68811(this, str, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
        return this.f24332a.f24307g;
    }
}
