package com.portfolio.platform.uirenew.watchsetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.Device $device$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.Device $this_run;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24299p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1$1")
    /* renamed from: com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1$1 */
    public static final class C68681 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.String>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24300p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68681(com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1 watchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = watchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1.C68681 r0 = new com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1.C68681(this.this$0, yb4);
            r0.f24300p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1.C68681) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return this.this$0.this$0.f24253m.getDeviceNameBySerial(this.this$0.$this_run.getDeviceId());
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1(com.portfolio.platform.data.model.Device device, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel watchSettingViewModel, com.portfolio.platform.data.model.Device device2) {
        super(2, yb4);
        this.$this_run = device;
        this.this$0 = watchSettingViewModel;
        this.$device$inlined = device2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1 watchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1 = new com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1(this.$this_run, yb4, this.this$0, this.$device$inlined);
        watchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1.f24299p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return watchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.String str;
        java.lang.Object obj2;
        boolean z;
        boolean z2;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        boolean z3 = false;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24299p$;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.f24242v.mo41770a();
            local.mo33255d(a2, "onDeviceChanged - device: " + this.$device$inlined);
            str = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e();
            boolean z4 = !com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isSamSlimDevice(this.$this_run.getDeviceId()) && !com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(this.$this_run.getDeviceId());
            if (this.$this_run.getVibrationStrength() == null) {
                this.$this_run.setVibrationStrength(com.fossil.blesdk.obfuscated.dc4.m20843a(25));
                com.fossil.blesdk.obfuscated.qa4 qa4 = com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            com.fossil.blesdk.obfuscated.ug4 b = com.fossil.blesdk.obfuscated.nh4.m25692b();
            com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1.C68681 r9 = new com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$onDeviceChanged$$inlined$run$lambda$1.C68681(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = str;
            this.Z$0 = z4;
            this.label = 1;
            obj2 = com.fossil.blesdk.obfuscated.yf4.m30997a(b, r9, this);
            if (obj2 == a) {
                return a;
            }
            z = z4;
        } else if (i == 1) {
            z = this.Z$0;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            str = (java.lang.String) this.L$1;
            obj2 = obj;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        java.lang.String str2 = (java.lang.String) obj2;
        if (this.$this_run.getBatteryLevel() > 100) {
            this.$this_run.setBatteryLevel(100);
        }
        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.$this_run.getDeviceId(), (java.lang.Object) str)) {
            try {
                com.misfit.frameworks.buttonservice.IButtonConnectivity b2 = com.portfolio.platform.PortfolioApp.f20941W.mo34585b();
                if (b2 != null && b2.getGattState(this.$this_run.getDeviceId()) == 2) {
                    z3 = true;
                }
                z2 = z3;
            } catch (java.lang.Exception unused) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String a3 = com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.f24242v.mo41770a();
                local2.mo33256e(a3, "exception when get gatt state of " + this.$this_run);
                z2 = false;
            }
            com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel watchSettingViewModel = this.this$0;
            com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.C6857c cVar = new com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.C6857c(this.$device$inlined, str2, z2, true, com.fossil.blesdk.obfuscated.dc4.m20839a(z));
            watchSettingViewModel.f24247g = cVar;
        } else {
            com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel watchSettingViewModel2 = this.this$0;
            com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.C6857c cVar2 = new com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.C6857c(this.$device$inlined, str2, false, false, com.fossil.blesdk.obfuscated.dc4.m20839a(z));
            watchSettingViewModel2.f24247g = cVar2;
        }
        com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.C6856b h = this.this$0.f24248h;
        com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.C6857c e = this.this$0.f24247g;
        if (e != null) {
            com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.C6856b.m35951a(h, false, true, false, e, (java.lang.String) null, (java.lang.Integer) null, (kotlin.Pair) null, false, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, false, false, (java.util.ArrayList) null, 65525, (java.lang.Object) null);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a4 = com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel.f24242v.mo41770a();
            local3.mo33255d(a4, "onDeviceChanged, mDeviceWrapper=" + this.this$0.f24247g);
            this.this$0.mo41752d();
            this.this$0.f24244d.removeCallbacksAndMessages((java.lang.Object) null);
            this.this$0.mo41769n();
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        com.fossil.blesdk.obfuscated.kd4.m24405a();
        throw null;
    }
}
