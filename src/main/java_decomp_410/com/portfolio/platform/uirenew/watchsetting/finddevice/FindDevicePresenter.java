package com.portfolio.platform.uirenew.watchsetting.finddevice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.model.PlaceFields;
import com.fossil.blesdk.obfuscated.bn2;
import com.fossil.blesdk.obfuscated.cc;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.er2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fr2;
import com.fossil.blesdk.obfuscated.gr2;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kq3;
import com.fossil.blesdk.obfuscated.lq3;
import com.fossil.blesdk.obfuscated.rc;
import com.fossil.blesdk.obfuscated.rp3;
import com.fossil.blesdk.obfuscated.zr2;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.network.utils.NetworkUtils;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import java.util.concurrent.TimeUnit;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FindDevicePresenter extends kq3 {
    @DexIgnore
    public static /* final */ long w; // = TimeUnit.SECONDS.toMillis(1);
    @DexIgnore
    public /* final */ MutableLiveData<String> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<WatchSettingViewModel.c> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<WatchSettingViewModel.c> h;
    @DexIgnore
    public int i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public /* final */ Runnable k;
    @DexIgnore
    public /* final */ f l;
    @DexIgnore
    public /* final */ e m;
    @DexIgnore
    public /* final */ rc n;
    @DexIgnore
    public /* final */ DeviceRepository o;
    @DexIgnore
    public /* final */ en2 p;
    @DexIgnore
    public /* final */ lq3 q;
    @DexIgnore
    public /* final */ er2 r;
    @DexIgnore
    public /* final */ fr2 s;
    @DexIgnore
    public /* final */ GetAddress t;
    @DexIgnore
    public /* final */ gr2 u;
    @DexIgnore
    public /* final */ PortfolioApp v;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<er2.d, er2.c> {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public b(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(er2.d dVar) {
            kd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation onSuccess");
            this.a.a(dVar.a());
        }

        @DexIgnore
        public void a(er2.c cVar) {
            kd4.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation onError");
            this.a.q.K0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.e<GetAddress.d, GetAddress.c> {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public c(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(GetAddress.d dVar) {
            kd4.b(dVar, "responseValue");
            String a2 = dVar.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDevicePresenter", "GetCityName onSuccess - address: " + a2);
            this.a.q.y(a2);
        }

        @DexIgnore
        public void a(GetAddress.c cVar) {
            kd4.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetCityName onError");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.e<fr2.d, fr2.c> {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public d(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(fr2.d dVar) {
            kd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation onSuccess");
            if (kd4.a((Object) this.a.k(), (Object) dVar.a().getDeviceSerial())) {
                this.a.a(dVar.a());
            }
        }

        @DexIgnore
        public void a(fr2.c cVar) {
            kd4.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation onError");
            this.a.h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public e(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            kd4.b(context, "context");
            kd4.b(intent, "intent");
            DeviceLocation deviceLocation = (DeviceLocation) intent.getSerializableExtra("device_location");
            String stringExtra = intent.getStringExtra("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDevicePresenter", "onReceive - location: " + deviceLocation + ",serial: " + stringExtra);
            if (kd4.a((Object) stringExtra, (Object) this.a.k()) && deviceLocation != null) {
                this.a.a(deviceLocation);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public f(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            kd4.b(context, "context");
            kd4.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            if (kd4.a((Object) stringExtra, (Object) this.a.k()) && kd4.a((Object) stringExtra, (Object) this.a.i())) {
                this.a.j().a(stringExtra);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements CoroutineUseCase.e<gr2.e, gr2.b> {
            @DexIgnore
            public /* final */ /* synthetic */ g a;

            @DexIgnore
            public a(g gVar) {
                this.a = gVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(gr2.e eVar) {
                kd4.b(eVar, "responseValue");
                int a2 = eVar.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FindDevicePresenter", "getRssi onSuccess - rssi: " + a2);
                this.a.e.a(a2);
                this.a.e.b(a2);
            }

            @DexIgnore
            public void a(gr2.b bVar) {
                kd4.b(bVar, "errorValue");
                FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "getRssi onError");
                this.a.e.a(-9999);
                this.a.e.b(-9999);
            }
        }

        @DexIgnore
        public g(FindDevicePresenter findDevicePresenter) {
            this.e = findDevicePresenter;
        }

        @DexIgnore
        public final void run() {
            String k = this.e.k();
            if (k != null) {
                this.e.u.a(new gr2.d(k), new a(this));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements cc<WatchSettingViewModel.c> {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public h(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        public final void a(WatchSettingViewModel.c cVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.v.a();
            local.d(a2, "observer device " + cVar);
            if (cVar != null) {
                this.a.q.a(cVar);
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public FindDevicePresenter(rc rcVar, DeviceRepository deviceRepository, en2 en2, lq3 lq3, er2 er2, fr2 fr2, GetAddress getAddress, gr2 gr2, PortfolioApp portfolioApp) {
        kd4.b(rcVar, "mLocalBroadcastManager");
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(lq3, "mView");
        kd4.b(er2, "mGetLocation");
        kd4.b(fr2, "mLoadLocation");
        kd4.b(getAddress, "mGetAddress");
        kd4.b(gr2, "mGetRssi");
        kd4.b(portfolioApp, "mApp");
        this.n = rcVar;
        this.o = deviceRepository;
        this.p = en2;
        this.q = lq3;
        this.r = er2;
        this.s = fr2;
        this.t = getAddress;
        this.u = gr2;
        this.v = portfolioApp;
        LiveData<WatchSettingViewModel.c> b2 = hc.b(this.f, new FindDevicePresenter$mDeviceWrapperTransformation$Anon1(this));
        kd4.a((Object) b2, "Transformations.switchMa\u2026viceWrapperLiveData\n    }");
        this.h = b2;
        this.j = new Handler();
        this.k = new g(this);
        this.l = new f(this);
        this.m = new e(this);
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation");
        String k2 = k();
        if (k2 != null) {
            this.r.a(new er2.b(k2), new b(this));
        }
    }

    @DexIgnore
    public final String i() {
        return PortfolioApp.W.c().e();
    }

    @DexIgnore
    public final MutableLiveData<String> j() {
        return this.f;
    }

    @DexIgnore
    public String k() {
        return this.f.a();
    }

    @DexIgnore
    public final void l() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation");
        this.s.a(new fr2.b(), new d(this));
    }

    @DexIgnore
    public final void m() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "locateOnMap");
        if (!NetworkUtils.isNetworkAvailable(this.v)) {
            this.q.a(601, (String) null);
            return;
        }
        bn2 bn2 = bn2.d;
        lq3 lq3 = this.q;
        if (lq3 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.watchsetting.FindDeviceFragment");
        } else if (!bn2.a(bn2, ((rp3) lq3).getContext(), "FIND_DEVICE", false, 4, (Object) null)) {
            this.q.b0();
        } else {
            String k2 = k();
            if (k2 != null) {
                if (TextUtils.equals(k2, i()) && a(k2)) {
                    Context applicationContext = this.v.getApplicationContext();
                    kd4.a((Object) applicationContext, "mApp.applicationContext");
                    if (a(applicationContext)) {
                        l();
                        return;
                    }
                }
                h();
            }
        }
    }

    @DexIgnore
    public void n() {
        this.q.a(this);
    }

    @DexIgnore
    public final void a(int i2) {
        this.i = i2;
    }

    @DexIgnore
    public void b(String str) {
        kd4.b(str, "serial");
        this.f.a(str);
    }

    @DexIgnore
    public void f() {
        this.n.a(this.m, new IntentFilter(MFDeviceService.U.a()));
        PortfolioApp portfolioApp = this.v;
        f fVar = this.l;
        portfolioApp.registerReceiver(fVar, new IntentFilter(this.v.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        LiveData<WatchSettingViewModel.c> liveData = this.h;
        lq3 lq3 = this.q;
        if (lq3 != null) {
            liveData.a((zr2) lq3, new h(this));
            this.u.f();
            b(this.i);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    public void g() {
        try {
            MutableLiveData<WatchSettingViewModel.c> mutableLiveData = this.g;
            lq3 lq3 = this.q;
            if (lq3 != null) {
                mutableLiveData.a((LifecycleOwner) (zr2) lq3);
                this.f.a((LifecycleOwner) this.q);
                this.h.a((LifecycleOwner) this.q);
                this.n.a((BroadcastReceiver) this.m);
                this.v.unregisterReceiver(this.l);
                this.u.g();
                this.j.removeCallbacks(this.k);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("FindDevicePresenter", "stop with " + e2);
        }
    }

    @DexIgnore
    public final void a(DeviceLocation deviceLocation) {
        kd4.b(deviceLocation, PlaceFields.LOCATION);
        this.q.a(deviceLocation.getTimeStamp());
        double latitude = deviceLocation.getLatitude();
        double longitude = deviceLocation.getLongitude();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDevicePresenter", "handleLocation latitude=" + latitude + ", longitude=" + longitude);
        if (latitude != 0.0d && longitude != 0.0d) {
            this.q.a(Double.valueOf(latitude), Double.valueOf(longitude));
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetCityName");
            this.t.a(new GetAddress.b(latitude, longitude), new c(this));
        }
    }

    @DexIgnore
    public final void b(int i2) {
        if (i2 != -9999) {
            this.q.k(i2);
        }
        this.j.removeCallbacks(this.k);
        this.j.postDelayed(this.k, w);
    }

    @DexIgnore
    public void a(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDevicePresenter", "enableLocate: enable = " + z);
        this.p.f(z);
        this.q.b(z, this.p.G());
        if (z) {
            m();
        }
    }

    @DexIgnore
    public final boolean a(String str) {
        IButtonConnectivity b2 = PortfolioApp.W.b();
        if (b2 == null || b2.getGattState(str) != 2) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean a(Context context) {
        return Settings.Secure.getInt(context.getContentResolver(), "location_mode") != 0;
    }
}
