package com.portfolio.platform.uirenew.onboarding.heightweight;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1", f = "OnboardingHeightWeightPresenter.kt", l = {123}, m = "invokeSuspend")
public final class OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isDefaultValuesUsed;
    @DexIgnore
    public /* final */ /* synthetic */ MFUser $user;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ OnboardingHeightWeightPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1$Anon1", f = "OnboardingHeightWeightPresenter.kt", l = {123}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qo2<MFUser>>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1 onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                UserRepository c = this.this$Anon0.this$Anon0.h;
                MFUser mFUser = this.this$Anon0.$user;
                this.L$Anon0 = zg4;
                this.label = 1;
                obj = c.updateUser(mFUser, true, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1(OnboardingHeightWeightPresenter onboardingHeightWeightPresenter, MFUser mFUser, boolean z, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = onboardingHeightWeightPresenter;
        this.$user = mFUser;
        this.$isDefaultValuesUsed = z;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1 onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1 = new OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1(this.this$Anon0, this.$user, this.$isDefaultValuesUsed, yb4);
        onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1.p$ = (zg4) obj;
        return onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            if (this.$user.isUseDefaultBiometric()) {
                this.$user.setUseDefaultBiometric(this.$isDefaultValuesUsed);
            }
            ug4 a2 = this.this$Anon0.b();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            if (yf4.a(a2, anon1, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
