package com.portfolio.platform.uirenew.onboarding.exploreWatch;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.PortfolioApp $portfolioApp;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f23998p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$1(com.portfolio.platform.PortfolioApp portfolioApp, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter exploreWatchPresenter) {
        super(2, yb4);
        this.$portfolioApp = portfolioApp;
        this.this$0 = exploreWatchPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$1 exploreWatchPresenter$navigateToHome$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$1(this.$portfolioApp, yb4, this.this$0);
        exploreWatchPresenter$navigateToHome$$inlined$let$lambda$1.f23998p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return exploreWatchPresenter$navigateToHome$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002a, code lost:
        if (r0 != null) goto L_0x002f;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.String str;
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.lang.String e = this.$portfolioApp.mo34532e();
            java.lang.String deviceNameBySerial = this.this$0.f23997h.getDeviceNameBySerial(e);
            com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile a = com.portfolio.platform.helper.DeviceHelper.f21188o.mo39565e().mo39545a(e);
            if (a != null) {
                str = a.getFirmwareVersion();
            }
            str = "";
            com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39517c().mo39497a(com.portfolio.platform.helper.DeviceHelper.f21188o.mo39559b(e), deviceNameBySerial, str);
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().mo33266i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER, e, "ExploreWatchPresenter", "[Sync Start] AUTO SYNC after pair new device");
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
