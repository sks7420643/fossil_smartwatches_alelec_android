package com.portfolio.platform.uirenew.onboarding.forgotPassword;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.es2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l42;
import com.fossil.blesdk.obfuscated.xk3;
import com.fossil.blesdk.obfuscated.zk3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ForgotPasswordActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((fd4) null);
    @DexIgnore
    public zk3 B;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str) {
            kd4.b(context, "context");
            kd4.b(str, "email");
            Intent intent = new Intent(context, ForgotPasswordActivity.class);
            intent.putExtra("EMAIL", str);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.base_activity);
        Intent intent = getIntent();
        String stringExtra = intent != null ? intent.getStringExtra("EMAIL") : null;
        es2 es2 = (es2) getSupportFragmentManager().a((int) R.id.content);
        if (es2 == null) {
            es2 = es2.n.b();
            a((Fragment) es2, es2.n.a(), (int) R.id.content);
        }
        l42 g = PortfolioApp.W.c().g();
        if (es2 != null) {
            g.a(new xk3(es2)).a(this);
            if (bundle == null) {
                zk3 zk3 = this.B;
                if (zk3 != null) {
                    if (stringExtra == null) {
                        stringExtra = "";
                    }
                    zk3.c(stringExtra);
                    return;
                }
                kd4.d("mPresenter");
                throw null;
            } else if (bundle.containsKey("EMAIL")) {
                zk3 zk32 = this.B;
                if (zk32 != null) {
                    String string = bundle.getString("EMAIL");
                    if (string == null) {
                        string = "";
                    }
                    zk32.c(string);
                    return;
                }
                kd4.d("mPresenter");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordContract.View");
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle, PersistableBundle persistableBundle) {
        if (bundle != null) {
            zk3 zk3 = this.B;
            if (zk3 != null) {
                zk3.a("EMAIL", bundle);
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        super.onSaveInstanceState(bundle, persistableBundle);
    }
}
