package com.portfolio.platform.uirenew.onboarding.ota;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Device $device$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceId;
    @DexIgnore
    public /* final */ /* synthetic */ String $lastFwTemp;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UpdateFirmwarePresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Device>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1 updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return this.this$Anon0.this$Anon0.n().getDeviceBySerial(this.this$Anon0.$deviceId);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1(String str, String str2, yb4 yb4, UpdateFirmwarePresenter updateFirmwarePresenter, Device device) {
        super(2, yb4);
        this.$deviceId = str;
        this.$lastFwTemp = str2;
        this.this$Anon0 = updateFirmwarePresenter;
        this.$device$inlined = device;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1 updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1 = new UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1(this.$deviceId, this.$lastFwTemp, yb4, this.this$Anon0, this.$device$inlined);
        updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ug4 b = this.this$Anon0.b();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = yf4.a(b, anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Device device = (Device) obj;
        FLogger.INSTANCE.getLocal().d(UpdateFirmwarePresenter.s.a(), "checkLastOTASuccess - getDeviceBySerial SUCCESS");
        if (device != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.s.a();
            local.d(a2, "checkLastOTASuccess - currentDeviceFw=" + device.getFirmwareRevision());
            if (!qf4.b(this.$lastFwTemp, device.getFirmwareRevision(), true)) {
                FLogger.INSTANCE.getLocal().d(UpdateFirmwarePresenter.s.a(), "Handle OTA complete on check last OTA success");
                this.this$Anon0.o.n(this.$deviceId);
                this.this$Anon0.o().v0();
            } else {
                this.this$Anon0.a(this.$device$inlined);
            }
        }
        return qa4.a;
    }
}
