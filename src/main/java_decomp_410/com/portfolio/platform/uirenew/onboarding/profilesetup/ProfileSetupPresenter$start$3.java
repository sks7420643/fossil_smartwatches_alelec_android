package com.portfolio.platform.uirenew.onboarding.profilesetup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3", mo27670f = "ProfileSetupPresenter.kt", mo27671l = {131}, mo27672m = "invokeSuspend")
public final class ProfileSetupPresenter$start$3 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24075p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3$1", mo27670f = "ProfileSetupPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3$1 */
    public static final class C68171 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.MFUser>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24076p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68171(com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3 profileSetupPresenter$start$3, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = profileSetupPresenter$start$3;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3.C68171 r0 = new com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3.C68171(this.this$0, yb4);
            r0.f24076p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3.C68171) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return this.this$0.this$0.f24068y.getCurrentUser();
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileSetupPresenter$start$3(com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter profileSetupPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = profileSetupPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3 profileSetupPresenter$start$3 = new com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3(this.this$0, yb4);
        profileSetupPresenter$start$3.f24075p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return profileSetupPresenter$start$3;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter profileSetupPresenter;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24075p$;
            com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter profileSetupPresenter2 = this.this$0;
            com.fossil.blesdk.obfuscated.ug4 a2 = profileSetupPresenter2.mo31440b();
            com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3.C68171 r4 = new com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3.C68171(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = profileSetupPresenter2;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r4, this);
            if (obj == a) {
                return a;
            }
            profileSetupPresenter = profileSetupPresenter2;
        } else if (i == 1) {
            profileSetupPresenter = (com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        profileSetupPresenter.f24054k = (com.portfolio.platform.data.model.MFUser) obj;
        com.portfolio.platform.data.model.MFUser c = this.this$0.f24054k;
        if (c != null) {
            this.this$0.f24066w.mo28092b(c);
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
