package com.portfolio.platform.uirenew.onboarding.exploreWatch;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ PortfolioApp $portfolioApp;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ExploreWatchPresenter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1(PortfolioApp portfolioApp, yb4 yb4, ExploreWatchPresenter exploreWatchPresenter) {
        super(2, yb4);
        this.$portfolioApp = portfolioApp;
        this.this$Anon0 = exploreWatchPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1 exploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1 = new ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1(this.$portfolioApp, yb4, this.this$Anon0);
        exploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return exploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002a, code lost:
        if (r0 != null) goto L_0x002f;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        String str;
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            String e = this.$portfolioApp.e();
            String deviceNameBySerial = this.this$Anon0.h.getDeviceNameBySerial(e);
            MisfitDeviceProfile a = DeviceHelper.o.e().a(e);
            if (a != null) {
                str = a.getFirmwareVersion();
            }
            str = "";
            AnalyticsHelper.f.c().a(DeviceHelper.o.b(e), deviceNameBySerial, str);
            FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, e, "ExploreWatchPresenter", "[Sync Start] AUTO SYNC after pair new device");
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
