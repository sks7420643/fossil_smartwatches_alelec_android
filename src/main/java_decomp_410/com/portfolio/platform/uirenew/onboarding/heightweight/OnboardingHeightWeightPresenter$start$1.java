package com.portfolio.platform.uirenew.onboarding.heightweight;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1", mo27670f = "OnboardingHeightWeightPresenter.kt", mo27671l = {31}, mo27672m = "invokeSuspend")
public final class OnboardingHeightWeightPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24016p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1$1", mo27670f = "OnboardingHeightWeightPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1$1 */
    public static final class C68031 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.MFUser>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24017p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68031(com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1 onboardingHeightWeightPresenter$start$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = onboardingHeightWeightPresenter$start$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1.C68031 r0 = new com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1.C68031(this.this$0, yb4);
            r0.f24017p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1.C68031) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return this.this$0.this$0.f24009h.getCurrentUser();
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public OnboardingHeightWeightPresenter$start$1(com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter onboardingHeightWeightPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = onboardingHeightWeightPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1 onboardingHeightWeightPresenter$start$1 = new com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1(this.this$0, yb4);
        onboardingHeightWeightPresenter$start$1.f24016p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return onboardingHeightWeightPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0079  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.data.model.MFUser b;
        com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter onboardingHeightWeightPresenter;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24016p$;
            if (this.this$0.f24007f == null) {
                com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter onboardingHeightWeightPresenter2 = this.this$0;
                com.fossil.blesdk.obfuscated.ug4 a2 = onboardingHeightWeightPresenter2.mo31440b();
                com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1.C68031 r4 = new com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1.C68031(this, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg4;
                this.L$1 = onboardingHeightWeightPresenter2;
                this.label = 1;
                obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r4, this);
                if (obj == a) {
                    return a;
                }
                onboardingHeightWeightPresenter = onboardingHeightWeightPresenter2;
            }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a3 = com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter.f24006k.mo41600a();
            local.mo33255d(a3, "start with currentUser=" + this.this$0.f24007f);
            b = this.this$0.f24007f;
            if (b != null) {
                if (b.getHeightInCentimeters() == 0 || b.getWeightInGrams() == 0) {
                    com.fossil.blesdk.obfuscated.mk2 mk2 = com.fossil.blesdk.obfuscated.mk2.f16783a;
                    com.portfolio.platform.enums.Gender gender = b.getGender();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) gender, "it.gender");
                    kotlin.Pair<java.lang.Integer, java.lang.Integer> a4 = mk2.mo29427a(gender, com.portfolio.platform.data.model.MFUser.getAge(b.getBirthday()));
                    if (b.getHeightInCentimeters() == 0) {
                        b.setHeightInCentimeters(a4.getFirst().intValue());
                    }
                    if (b.getWeightInGrams() == 0) {
                        b.setWeightInGrams(a4.getSecond().intValue() * 1000);
                    }
                }
                if (b.getHeightInCentimeters() > 0) {
                    com.fossil.blesdk.obfuscated.fl3 d = this.this$0.f24008g;
                    int heightInCentimeters = b.getHeightInCentimeters();
                    com.portfolio.platform.enums.Unit heightUnit = b.getHeightUnit();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) heightUnit, "it.heightUnit");
                    d.mo27421a(heightInCentimeters, heightUnit);
                }
                if (b.getWeightInGrams() > 0) {
                    com.fossil.blesdk.obfuscated.fl3 d2 = this.this$0.f24008g;
                    int weightInGrams = b.getWeightInGrams();
                    com.portfolio.platform.enums.Unit weightUnit = b.getWeightUnit();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) weightUnit, "it.weightUnit");
                    d2.mo27422b(weightInGrams, weightUnit);
                }
            }
            this.this$0.f24008g.mo27423g();
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else if (i == 1) {
            onboardingHeightWeightPresenter = (com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        onboardingHeightWeightPresenter.f24007f = (com.portfolio.platform.data.model.MFUser) obj;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a32 = com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter.f24006k.mo41600a();
        local2.mo33255d(a32, "start with currentUser=" + this.this$0.f24007f);
        b = this.this$0.f24007f;
        if (b != null) {
        }
        this.this$0.f24008g.mo27423g();
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
