package com.portfolio.platform.uirenew.onboarding.ota;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$1 */
public final class C6807xf7ece11b extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.Device $device$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $deviceId;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $lastFwTemp;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24034p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$1$1")
    /* renamed from: com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$1$1 */
    public static final class C68081 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.Device>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24035p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.ota.C6807xf7ece11b this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68081(com.portfolio.platform.uirenew.onboarding.ota.C6807xf7ece11b updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.onboarding.ota.C6807xf7ece11b.C68081 r0 = new com.portfolio.platform.uirenew.onboarding.ota.C6807xf7ece11b.C68081(this.this$0, yb4);
            r0.f24035p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.onboarding.ota.C6807xf7ece11b.C68081) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return this.this$0.this$0.mo41607n().getDeviceBySerial(this.this$0.$deviceId);
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C6807xf7ece11b(java.lang.String str, java.lang.String str2, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter updateFirmwarePresenter, com.portfolio.platform.data.model.Device device) {
        super(2, yb4);
        this.$deviceId = str;
        this.$lastFwTemp = str2;
        this.this$0 = updateFirmwarePresenter;
        this.$device$inlined = device;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.onboarding.ota.C6807xf7ece11b updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$1 = new com.portfolio.platform.uirenew.onboarding.ota.C6807xf7ece11b(this.$deviceId, this.$lastFwTemp, yb4, this.this$0, this.$device$inlined);
        updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$1.f24034p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return updateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.onboarding.ota.C6807xf7ece11b) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24034p$;
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.onboarding.ota.C6807xf7ece11b.C68081 r3 = new com.portfolio.platform.uirenew.onboarding.ota.C6807xf7ece11b.C68081(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b, r3, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.model.Device device = (com.portfolio.platform.data.model.Device) obj;
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter.f24019s.mo41610a(), "checkLastOTASuccess - getDeviceBySerial SUCCESS");
        if (device != null) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter.f24019s.mo41610a();
            local.mo33255d(a2, "checkLastOTASuccess - currentDeviceFw=" + device.getFirmwareRevision());
            if (!com.fossil.blesdk.obfuscated.qf4.m26954b(this.$lastFwTemp, device.getFirmwareRevision(), true)) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter.f24019s.mo41610a(), "Handle OTA complete on check last OTA success");
                this.this$0.f24029o.mo27061n(this.$deviceId);
                this.this$0.mo41608o().mo28542v0();
            } else {
                this.this$0.mo41603a(this.$device$inlined);
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
