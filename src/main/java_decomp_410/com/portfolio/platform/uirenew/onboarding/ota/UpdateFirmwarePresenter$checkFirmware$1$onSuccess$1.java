package com.portfolio.platform.uirenew.onboarding.ota;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1", mo27670f = "UpdateFirmwarePresenter.kt", mo27671l = {213, 216}, mo27672m = "invokeSuspend")
public final class UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6139d $responseValue;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24039p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1(com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1 updateFirmwarePresenter$checkFirmware$1, com.portfolio.platform.p007ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase.C6139d dVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = updateFirmwarePresenter$checkFirmware$1;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1 updateFirmwarePresenter$checkFirmware$1$onSuccess$1 = new com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1(this.this$0, this.$responseValue, yb4);
        updateFirmwarePresenter$checkFirmware$1$onSuccess$1.f24039p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return updateFirmwarePresenter$checkFirmware$1$onSuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b0  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.String str;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f24039p$;
            str = this.$responseValue.mo40290a();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter.f24019s.mo41610a();
            local.mo33255d(a2, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + str);
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.f24036a.mo31440b();
            com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1$isDianaEV1$1 updateFirmwarePresenter$checkFirmware$1$onSuccess$1$isDianaEV1$1 = new com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1$isDianaEV1$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg42;
            this.L$1 = str;
            this.label = 1;
            java.lang.Object a3 = com.fossil.blesdk.obfuscated.yf4.m30997a(b, updateFirmwarePresenter$checkFirmware$1$onSuccess$1$isDianaEV1$1, this);
            if (a3 == a) {
                return a;
            }
            java.lang.Object obj2 = a3;
            zg4 = zg42;
            obj = obj2;
        } else if (i == 1) {
            str = (java.lang.String) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            java.lang.String str2 = (java.lang.String) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            if (!((java.lang.Boolean) obj).booleanValue()) {
                this.this$0.f24036a.mo41608o().mo28542v0();
            } else {
                com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1 updateFirmwarePresenter$checkFirmware$1 = this.this$0;
                updateFirmwarePresenter$checkFirmware$1.f24036a.mo41605c(updateFirmwarePresenter$checkFirmware$1.f24038c);
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        boolean booleanValue = ((java.lang.Boolean) obj).booleanValue();
        if (!booleanValue) {
            com.fossil.blesdk.obfuscated.ug4 b2 = this.this$0.f24036a.mo31440b();
            com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1$isLatestFw$1 updateFirmwarePresenter$checkFirmware$1$onSuccess$1$isLatestFw$1 = new com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1$isLatestFw$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = str;
            this.Z$0 = booleanValue;
            this.label = 2;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b2, updateFirmwarePresenter$checkFirmware$1$onSuccess$1$isLatestFw$1, this);
            if (obj == a) {
                return a;
            }
            if (!((java.lang.Boolean) obj).booleanValue()) {
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a4 = com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter.f24019s.mo41610a();
        local2.mo33256e(a4, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + str + " but device is DianaEV1!!!");
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
