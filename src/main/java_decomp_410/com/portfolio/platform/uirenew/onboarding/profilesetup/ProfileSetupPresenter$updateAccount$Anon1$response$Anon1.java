package com.portfolio.platform.uirenew.onboarding.profilesetup;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$Anon1$response$Anon1", f = "ProfileSetupPresenter.kt", l = {374}, m = "invokeSuspend")
public final class ProfileSetupPresenter$updateAccount$Anon1$response$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qo2<MFUser>>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileSetupPresenter$updateAccount$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileSetupPresenter$updateAccount$Anon1$response$Anon1(ProfileSetupPresenter$updateAccount$Anon1 profileSetupPresenter$updateAccount$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = profileSetupPresenter$updateAccount$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ProfileSetupPresenter$updateAccount$Anon1$response$Anon1 profileSetupPresenter$updateAccount$Anon1$response$Anon1 = new ProfileSetupPresenter$updateAccount$Anon1$response$Anon1(this.this$Anon0, yb4);
        profileSetupPresenter$updateAccount$Anon1$response$Anon1.p$ = (zg4) obj;
        return profileSetupPresenter$updateAccount$Anon1$response$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ProfileSetupPresenter$updateAccount$Anon1$response$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            UserRepository d = this.this$Anon0.this$Anon0.y;
            MFUser mFUser = this.this$Anon0.$user;
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = d.updateUser(mFUser, true, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
