package com.portfolio.platform.uirenew.onboarding.exploreWatch;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.Explore;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter$start$Anon1$data$Anon1", f = "ExploreWatchPresenter.kt", l = {}, m = "invokeSuspend")
public final class ExploreWatchPresenter$start$Anon1$data$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super List<? extends Explore>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ExploreWatchPresenter$start$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ExploreWatchPresenter$start$Anon1$data$Anon1(ExploreWatchPresenter$start$Anon1 exploreWatchPresenter$start$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = exploreWatchPresenter$start$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        ExploreWatchPresenter$start$Anon1$data$Anon1 exploreWatchPresenter$start$Anon1$data$Anon1 = new ExploreWatchPresenter$start$Anon1$data$Anon1(this.this$Anon0, yb4);
        exploreWatchPresenter$start$Anon1$data$Anon1.p$ = (zg4) obj;
        return exploreWatchPresenter$start$Anon1$data$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ExploreWatchPresenter$start$Anon1$data$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return this.this$Anon0.this$Anon0.i();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
