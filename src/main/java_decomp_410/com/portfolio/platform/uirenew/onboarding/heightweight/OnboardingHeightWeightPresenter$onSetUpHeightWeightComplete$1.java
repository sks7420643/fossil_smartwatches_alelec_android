package com.portfolio.platform.uirenew.onboarding.heightweight;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1", mo27670f = "OnboardingHeightWeightPresenter.kt", mo27671l = {123}, mo27672m = "invokeSuspend")
public final class OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isDefaultValuesUsed;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.MFUser $user;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24014p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1$1", mo27670f = "OnboardingHeightWeightPresenter.kt", mo27671l = {123}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1$1 */
    public static final class C68021 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qo2<com.portfolio.platform.data.model.MFUser>>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24015p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68021(com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1 onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1.C68021 r0 = new com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1.C68021(this.this$0, yb4);
            r0.f24015p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1.C68021) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24015p$;
                com.portfolio.platform.data.source.UserRepository c = this.this$0.this$0.f24009h;
                com.portfolio.platform.data.model.MFUser mFUser = this.this$0.$user;
                this.L$0 = zg4;
                this.label = 1;
                obj = c.updateUser(mFUser, true, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1(com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter onboardingHeightWeightPresenter, com.portfolio.platform.data.model.MFUser mFUser, boolean z, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = onboardingHeightWeightPresenter;
        this.$user = mFUser;
        this.$isDefaultValuesUsed = z;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1 onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1 = new com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1(this.this$0, this.$user, this.$isDefaultValuesUsed, yb4);
        onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1.f24014p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return onboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24014p$;
            if (this.$user.isUseDefaultBiometric()) {
                this.$user.setUseDefaultBiometric(this.$isDefaultValuesUsed);
            }
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1.C68021 r3 = new com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1.C68021(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r3, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
