package com.portfolio.platform.uirenew.onboarding.exploreWatch;

import android.content.Context;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nk3;
import com.fossil.blesdk.obfuscated.ok3;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.vj2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.DeviceRepository;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ExploreWatchPresenter extends nk3 {
    @DexIgnore
    public /* final */ ok3 f;
    @DexIgnore
    public /* final */ vj2 g;
    @DexIgnore
    public /* final */ DeviceRepository h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public ExploreWatchPresenter(ok3 ok3, vj2 vj2, DeviceRepository deviceRepository) {
        kd4.b(ok3, "mView");
        kd4.b(vj2, "mDeviceSettingFactory");
        kd4.b(deviceRepository, "mDeviceRepository");
        this.f = ok3;
        this.g = vj2;
        this.h = deviceRepository;
    }

    @DexIgnore
    public void a(boolean z) {
    }

    @DexIgnore
    public void f() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new ExploreWatchPresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        PortfolioApp c = PortfolioApp.W.c();
        fi4 unused = ag4.b(ah4.a(c()), (CoroutineContext) null, (CoroutineStart) null, new ExploreWatchPresenter$navigateToHome$$inlined$let$lambda$Anon1(c, (yb4) null, this), 3, (Object) null);
        c.a(this.g, false, 13);
        this.f.s0();
    }

    @DexIgnore
    public final List<Explore> i() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        explore.setTitle(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Updates_FirmwareUpdatedDiana_Title__FlickTheWrist));
        explore.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Updates_FirmwareUpdatedDiana_Text__QuicklyTurnYourWristAwayTo));
        explore.setExploreType(Explore.ExploreType.WRIST_FLICK);
        Explore explore2 = new Explore();
        explore2.setTitle(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Updates_FirmwareUpdatedDiana_Title__DoubleTap));
        explore2.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Updates_FirmwareUpdatedDiana_Text__BrightenYourWatchFaceForEasy));
        explore2.setExploreType(Explore.ExploreType.DOUBLE_TAP);
        Explore explore3 = new Explore();
        explore3.setTitle(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Updates_FirmwareUpdatedDiana_Title__PressHoldMiddleButton));
        explore3.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Updates_FirmwareUpdatedDiana_Text__GoBackToYourMainWatch));
        explore3.setExploreType(Explore.ExploreType.PRESS_AND_HOLD);
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        return arrayList;
    }

    @DexIgnore
    public final ok3 j() {
        return this.f;
    }

    @DexIgnore
    public void k() {
        this.f.a(this);
    }
}
