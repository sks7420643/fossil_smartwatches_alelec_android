package com.portfolio.platform.uirenew.onboarding.ota;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1", mo27670f = "UpdateFirmwarePresenter.kt", mo27671l = {105}, mo27672m = "invokeSuspend")
public final class UpdateFirmwarePresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24042p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1$1", mo27670f = "UpdateFirmwarePresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1$1 */
    public static final class C68091 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.Device>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.lang.String $activeSerial;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f24043p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C68091(com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1 updateFirmwarePresenter$start$1, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = updateFirmwarePresenter$start$1;
            this.$activeSerial = str;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1.C68091 r0 = new com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1.C68091(this.this$0, this.$activeSerial, yb4);
            r0.f24043p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1.C68091) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return this.this$0.this$0.mo41607n().getDeviceBySerial(this.$activeSerial);
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateFirmwarePresenter$start$1(com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter updateFirmwarePresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = updateFirmwarePresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1 updateFirmwarePresenter$start$1 = new com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1(this.this$0, yb4);
        updateFirmwarePresenter$start$1.f24042p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return updateFirmwarePresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.String str;
        com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter updateFirmwarePresenter;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24042p$;
            str = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e();
            com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter updateFirmwarePresenter2 = this.this$0;
            com.fossil.blesdk.obfuscated.ug4 b = updateFirmwarePresenter2.mo31440b();
            com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1.C68091 r5 = new com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1.C68091(this, str, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = str;
            this.L$2 = updateFirmwarePresenter2;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b, r5, this);
            if (obj == a) {
                return a;
            }
            updateFirmwarePresenter = updateFirmwarePresenter2;
        } else if (i == 1) {
            updateFirmwarePresenter = (com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter) this.L$2;
            str = (java.lang.String) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        updateFirmwarePresenter.f24022h = (com.portfolio.platform.data.model.Device) obj;
        boolean A = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34454A();
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a2 = com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter.f24019s.mo41610a();
        local.mo33255d(a2, "start - activeSerial=" + str + ", isDeviceOtaing=" + A);
        if (!A) {
            com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter updateFirmwarePresenter3 = this.this$0;
            updateFirmwarePresenter3.mo41604b(updateFirmwarePresenter3.f24022h);
        }
        this.this$0.mo41606m();
        com.fossil.blesdk.obfuscated.ul2 b2 = com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39514b("ota_session");
        this.this$0.f24023i = b2;
        com.portfolio.platform.helper.AnalyticsHelper.f21165f.mo39513a("ota_session", b2);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
