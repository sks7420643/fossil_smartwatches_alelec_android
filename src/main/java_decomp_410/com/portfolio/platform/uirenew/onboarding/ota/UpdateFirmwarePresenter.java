package com.portfolio.platform.uirenew.onboarding.ota;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ll3;
import com.fossil.blesdk.obfuscated.ml3;
import com.fossil.blesdk.obfuscated.pl3;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.ul2;
import com.fossil.blesdk.obfuscated.vj2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import java.util.ArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UpdateFirmwarePresenter extends ll3 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a((fd4) null);
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public boolean g;
    @DexIgnore
    public Device h;
    @DexIgnore
    public ul2 i;
    @DexIgnore
    public /* final */ c j; // = new c(this);
    @DexIgnore
    public /* final */ b k; // = new b(this);
    @DexIgnore
    public /* final */ ml3 l;
    @DexIgnore
    public /* final */ DeviceRepository m;
    @DexIgnore
    public /* final */ vj2 n;
    @DexIgnore
    public /* final */ en2 o;
    @DexIgnore
    public /* final */ UpdateFirmwareUsecase p;
    @DexIgnore
    public /* final */ DownloadFirmwareByDeviceModelUsecase q;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return UpdateFirmwarePresenter.r;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter a;

        @DexIgnore
        public b(UpdateFirmwarePresenter updateFirmwarePresenter) {
            this.a = updateFirmwarePresenter;
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            kd4.b(communicateMode, "communicateMode");
            kd4.b(intent, "intent");
            boolean booleanExtra = intent.getBooleanExtra("OTA_RESULT", false);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.s.a();
            local.d(a2, "otaCompleteReceiver - isSuccess=" + booleanExtra);
            this.a.o().b(booleanExtra);
            this.a.o.n(PortfolioApp.W.c().e());
            if (booleanExtra) {
                FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.a.f, UpdateFirmwarePresenter.s.a(), "[Sync Start] AUTO SYNC after OTA");
                PortfolioApp.W.c().a(this.a.n, false, 13);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter a;

        @DexIgnore
        public c(UpdateFirmwarePresenter updateFirmwarePresenter) {
            this.a = updateFirmwarePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            kd4.b(context, "context");
            kd4.b(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.s.a();
            local.d(a2, "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
            this.a.f = otaEvent.getSerial();
            if (!TextUtils.isEmpty(otaEvent.getSerial()) && qf4.b(otaEvent.getSerial(), PortfolioApp.W.c().e(), true)) {
                this.a.o().g((int) (otaEvent.getProcess() * ((float) 10)));
            }
        }
    }

    /*
    static {
        String simpleName = UpdateFirmwarePresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "UpdateFirmwarePresenter::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public UpdateFirmwarePresenter(ml3 ml3, DeviceRepository deviceRepository, UserRepository userRepository, vj2 vj2, en2 en2, UpdateFirmwareUsecase updateFirmwareUsecase, DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase) {
        kd4.b(ml3, "mView");
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(vj2, "mDeviceSettingFactory");
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(updateFirmwareUsecase, "mUpdateFirmwareUseCase");
        kd4.b(downloadFirmwareByDeviceModelUsecase, "mDownloadFwByDeviceModel");
        this.l = ml3;
        this.m = deviceRepository;
        this.n = vj2;
        this.o = en2;
        this.p = updateFirmwareUsecase;
        this.q = downloadFirmwareByDeviceModelUsecase;
    }

    @DexIgnore
    public boolean j() {
        return this.g;
    }

    @DexIgnore
    public void k() {
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.W.c().e());
        if (deviceBySerial != null && pl3.a[deviceBySerial.ordinal()] == 1) {
            this.l.t();
        } else {
            this.l.h();
        }
    }

    @DexIgnore
    public void l() {
        FLogger.INSTANCE.getLocal().d(r, "tryOtaAgain");
        Device device = this.h;
        if (device != null) {
            c(device);
        }
        this.l.n0();
    }

    @DexIgnore
    public final void m() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.f);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "serial=" + this.f + ", mCurrentDeviceType=" + deviceBySerial);
        if (deviceBySerial == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
            explore.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__NeverMissAStepAndKeep));
            explore.setBackground(R.drawable.update_fw_hybrid_one);
            explore2.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__ReceiveDiscreteVibrationNotificationsForYour));
            explore2.setBackground(R.drawable.update_fw_diana_two);
            explore3.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__ControlTheWorldFromYourWrist));
            explore3.setBackground(R.drawable.update_fw_hybrid_three);
            explore4.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__GlanceableContentReadyOnTheWrist));
            explore4.setBackground(R.drawable.update_fw_diana_four);
        } else {
            explore.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__TrackActivitySleepAndPersonalGoals));
            explore.setBackground(R.drawable.update_fw_hybrid_one);
            explore2.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__ReceiveDiscreteVibrationNotificationsForYour));
            explore2.setBackground(R.drawable.update_fw_hybrid_two);
            explore3.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__ControlTheWorldFromYourWrist));
            explore3.setBackground(R.drawable.update_fw_hybrid_three);
            explore4.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__NoChargingNeededYourWatchIs));
            explore4.setBackground(R.drawable.update_fw_hybrid_four);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.l.i(arrayList);
    }

    @DexIgnore
    public final DeviceRepository n() {
        return this.m;
    }

    @DexIgnore
    public final ml3 o() {
        return this.l;
    }

    @DexIgnore
    public void p() {
        this.l.a(this);
    }

    @DexIgnore
    public void f() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "start - mIsOnBoardingFlow=" + this.g);
        if (!this.g) {
            this.l.X();
        }
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.k, CommunicateMode.OTA);
        PortfolioApp c2 = PortfolioApp.W.c();
        c cVar = this.j;
        c2.registerReceiver(cVar, new IntentFilter(PortfolioApp.W.c().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        this.l.g();
        BleCommandResultManager.d.a(CommunicateMode.OTA);
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new UpdateFirmwarePresenter$start$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        try {
            PortfolioApp.W.c().unregisterReceiver(this.j);
            BleCommandResultManager.d.b((BleCommandResultManager.b) this.k, CommunicateMode.OTA);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = r;
            local.e(str, "stop - e=" + e);
        }
    }

    @DexIgnore
    public void h() {
        BleCommandResultManager.d.a(CommunicateMode.OTA);
    }

    @DexIgnore
    public void i() {
        FLogger.INSTANCE.getLocal().d(r, "checkFwAgain");
        b(this.h);
    }

    @DexIgnore
    public final void b(Device device) {
        if (device != null) {
            String deviceId = device.getDeviceId();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = r;
            local.d(str, "checkLastOTASuccess - deviceId=" + deviceId + ", sku=" + device.getSku());
            if (TextUtils.isEmpty(deviceId)) {
                FLogger.INSTANCE.getLocal().e(r, "checkLastOTASuccess - DEVICE ID IS EMPTY!!!");
                return;
            }
            String i2 = this.o.i(deviceId);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = r;
            local2.d(str2, "checkLastOTASuccess - lastTempFw=" + i2);
            if (TextUtils.isEmpty(i2)) {
                a(device);
                return;
            }
            fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new UpdateFirmwarePresenter$checkLastOTASuccess$$inlined$let$lambda$Anon1(deviceId, i2, (yb4) null, this, device), 3, (Object) null);
        }
    }

    @DexIgnore
    public final void c(Device device) {
        kd4.b(device, "Device");
        String deviceId = device.getDeviceId();
        boolean A = PortfolioApp.W.c().A();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "updateFirmware - activeSerial=" + deviceId + ", isDeviceOtaing=" + A);
        if (!A) {
            fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new UpdateFirmwarePresenter$updateFirmware$Anon1(this, deviceId, (yb4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public final void a(Device device) {
        String deviceId = device.getDeviceId();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "checkFirmware - deviceId=" + deviceId + ", sku=" + device.getSku() + ", fw=" + device.getFirmwareRevision());
        if (PortfolioApp.W.c().D() || !this.o.T()) {
            DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase = this.q;
            String sku = device.getSku();
            if (sku == null) {
                sku = "";
            }
            downloadFirmwareByDeviceModelUsecase.a(new DownloadFirmwareByDeviceModelUsecase.b(sku), new UpdateFirmwarePresenter$checkFirmware$Anon1(this, deviceId, device));
            return;
        }
        this.l.v0();
    }

    @DexIgnore
    public void a(boolean z) {
        this.g = z;
    }
}
