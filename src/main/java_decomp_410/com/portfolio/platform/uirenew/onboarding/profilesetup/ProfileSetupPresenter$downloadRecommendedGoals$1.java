package com.portfolio.platform.uirenew.onboarding.profilesetup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1", mo27670f = "ProfileSetupPresenter.kt", mo27671l = {251, 258}, mo27672m = "invokeSuspend")
public final class ProfileSetupPresenter$downloadRecommendedGoals$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $service;
    @DexIgnore
    public int I$0;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24072p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1$a")
    /* renamed from: com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1$a */
    public static final class C6816a implements com.portfolio.platform.CoroutineUseCase.C5606e<com.portfolio.platform.usecase.GetRecommendedGoalUseCase.C6900d, com.portfolio.platform.usecase.GetRecommendedGoalUseCase.C6898b> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1 f24073a;

        @DexIgnore
        public C6816a(com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1 profileSetupPresenter$downloadRecommendedGoals$1) {
            this.f24073a = profileSetupPresenter$downloadRecommendedGoals$1;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(com.portfolio.platform.usecase.GetRecommendedGoalUseCase.C6900d dVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(dVar, "responseValue");
            com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1 profileSetupPresenter$downloadRecommendedGoals$1 = this.f24073a;
            profileSetupPresenter$downloadRecommendedGoals$1.this$0.mo41624e(profileSetupPresenter$downloadRecommendedGoals$1.$service);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo29641a(com.portfolio.platform.usecase.GetRecommendedGoalUseCase.C6898b bVar) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(bVar, "errorValue");
            com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1 profileSetupPresenter$downloadRecommendedGoals$1 = this.f24073a;
            profileSetupPresenter$downloadRecommendedGoals$1.this$0.mo41624e(profileSetupPresenter$downloadRecommendedGoals$1.$service);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileSetupPresenter$downloadRecommendedGoals$1(com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter profileSetupPresenter, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = profileSetupPresenter;
        this.$service = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1 profileSetupPresenter$downloadRecommendedGoals$1 = new com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1(this.this$0, this.$service, yb4);
        profileSetupPresenter$downloadRecommendedGoals$1.f24072p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return profileSetupPresenter$downloadRecommendedGoals$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.data.model.MFUser mFUser;
        int i;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i2 = this.label;
        if (i2 == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f24072p$;
            com.fossil.blesdk.obfuscated.ug4 a2 = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1$currentUser$1 profileSetupPresenter$downloadRecommendedGoals$1$currentUser$1 = new com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1$currentUser$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, profileSetupPresenter$downloadRecommendedGoals$1$currentUser$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i2 == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i2 == 2) {
            i = this.I$0;
            com.portfolio.platform.data.model.MFUser mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            mFUser = (com.portfolio.platform.data.model.MFUser) this.L$2;
            com.portfolio.platform.usecase.GetRecommendedGoalUseCase b = this.this$0.f24067x;
            int heightInCentimeters = mFUser.getHeightInCentimeters();
            int weightInGrams = mFUser.getWeightInGrams();
            com.portfolio.platform.enums.Gender gender = mFUser.getGender();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) gender, "it.gender");
            b.mo34435a(new com.portfolio.platform.usecase.GetRecommendedGoalUseCase.C6899c(i, heightInCentimeters, weightInGrams, gender), new com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1.C6816a(this));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        mFUser = (com.portfolio.platform.data.model.MFUser) obj;
        if (mFUser != null) {
            int age = com.portfolio.platform.data.model.MFUser.getAge(mFUser.getBirthday());
            this.this$0.mo41626m().mo39501b(mFUser.getUserId());
            com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
            java.lang.String userId = mFUser.getUserId();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) userId, "it.userId");
            c.mo34567p(userId);
            com.portfolio.platform.PortfolioApp c2 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
            this.L$0 = zg4;
            this.L$1 = mFUser;
            this.L$2 = mFUser;
            this.I$0 = age;
            this.label = 2;
            if (c2.mo34480a((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) this) == a) {
                return a;
            }
            i = age;
            com.portfolio.platform.usecase.GetRecommendedGoalUseCase b2 = this.this$0.f24067x;
            int heightInCentimeters2 = mFUser.getHeightInCentimeters();
            int weightInGrams2 = mFUser.getWeightInGrams();
            com.portfolio.platform.enums.Gender gender2 = mFUser.getGender();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) gender2, "it.gender");
            b2.mo34435a(new com.portfolio.platform.usecase.GetRecommendedGoalUseCase.C6899c(i, heightInCentimeters2, weightInGrams2, gender2), new com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1.C6816a(this));
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
