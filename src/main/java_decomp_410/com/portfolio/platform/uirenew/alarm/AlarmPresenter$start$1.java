package com.portfolio.platform.uirenew.alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1", mo27670f = "AlarmPresenter.kt", mo27671l = {61}, mo27672m = "invokeSuspend")
public final class AlarmPresenter$start$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f22195p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.alarm.AlarmPresenter this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1$1", mo27670f = "AlarmPresenter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1$1 */
    public static final class C62631 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.portfolio.platform.data.model.MFUser>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f22196p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C62631(com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1 alarmPresenter$start$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = alarmPresenter$start$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1.C62631 r0 = new com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1.C62631(this.this$0, yb4);
            r0.f22196p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1.C62631) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                return this.this$0.this$0.f22192t.getCurrentUser();
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmPresenter$start$1(com.portfolio.platform.uirenew.alarm.AlarmPresenter alarmPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = alarmPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1 alarmPresenter$start$1 = new com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1(this.this$0, yb4);
        alarmPresenter$start$1.f22195p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return alarmPresenter$start$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.uirenew.alarm.AlarmPresenter alarmPresenter;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f22195p$;
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.uirenew.alarm.AlarmPresenter.f22177v.mo40683a(), com.facebook.share.internal.VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
            com.portfolio.platform.PortfolioApp.f20941W.mo34588b((java.lang.Object) zg4);
            this.this$0.f22189q.mo40707f();
            this.this$0.f22191s.mo40693f();
            com.portfolio.platform.service.BleCommandResultManager.f21345d.mo39773a(com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_LIST_ALARM);
            com.portfolio.platform.uirenew.alarm.AlarmPresenter alarmPresenter2 = this.this$0;
            com.fossil.blesdk.obfuscated.ug4 a2 = alarmPresenter2.mo31441c();
            com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1.C62631 r6 = new com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1.C62631(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = alarmPresenter2;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(a2, r6, this);
            if (obj == a) {
                return a;
            }
            alarmPresenter = alarmPresenter2;
        } else if (i == 1) {
            alarmPresenter = (com.portfolio.platform.uirenew.alarm.AlarmPresenter) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        alarmPresenter.f22183k = (com.portfolio.platform.data.model.MFUser) obj;
        com.portfolio.platform.data.model.MFUser j = this.this$0.f22183k;
        if (!(j == null || j.getUserId() == null)) {
            this.this$0.f22185m.mo32894l(this.this$0.f22188p != null);
            if (this.this$0.f22181i == null) {
                this.this$0.f22181i = new android.util.SparseIntArray();
            }
            if (this.this$0.f22178f == null) {
                if (this.this$0.f22188p != null) {
                    com.portfolio.platform.uirenew.alarm.AlarmPresenter alarmPresenter3 = this.this$0;
                    alarmPresenter3.f22178f = alarmPresenter3.f22188p.getTitle();
                    com.portfolio.platform.uirenew.alarm.AlarmPresenter alarmPresenter4 = this.this$0;
                    alarmPresenter4.f22179g = alarmPresenter4.f22188p.getTotalMinutes();
                    com.portfolio.platform.uirenew.alarm.AlarmPresenter alarmPresenter5 = this.this$0;
                    alarmPresenter5.f22180h = alarmPresenter5.f22188p.isRepeated();
                    com.portfolio.platform.uirenew.alarm.AlarmPresenter alarmPresenter6 = this.this$0;
                    alarmPresenter6.f22181i = alarmPresenter6.mo40677a(alarmPresenter6.f22188p.getDays());
                    com.portfolio.platform.uirenew.alarm.AlarmPresenter alarmPresenter7 = this.this$0;
                    alarmPresenter7.f22182j = alarmPresenter7.f22188p.isActive();
                    this.this$0.f22185m.mo32892e(false);
                    this.this$0.f22185m.mo32884K();
                } else {
                    this.this$0.f22178f = com.fossil.blesdk.obfuscated.sm2.m27795a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c(), (int) com.fossil.wearables.fossil.R.string.Customization_Buttons_HybridAlarm_Title__Alarm);
                    this.this$0.f22182j = true;
                    this.this$0.f22180h = false;
                    this.this$0.f22181i = new android.util.SparseIntArray();
                    this.this$0.mo40681k();
                    this.this$0.f22185m.mo32892e(true);
                }
            }
            this.this$0.f22185m.mo32887a(this.this$0.f22179g);
            this.this$0.f22185m.mo32893k(this.this$0.f22180h);
            com.fossil.blesdk.obfuscated.yt2 l = this.this$0.f22185m;
            android.util.SparseIntArray d = this.this$0.f22181i;
            if (d != null) {
                l.mo32888a(d);
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
