package com.portfolio.platform.uirenew.alarm;

import android.content.Context;
import android.util.SparseIntArray;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.yt2;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.service.BleCommandResultManager;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$Anon1", f = "AlarmPresenter.kt", l = {61}, m = "invokeSuspend")
public final class AlarmPresenter$start$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmPresenter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$Anon1$Anon1", f = "AlarmPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super MFUser>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmPresenter$start$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(AlarmPresenter$start$Anon1 alarmPresenter$start$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = alarmPresenter$start$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                return this.this$Anon0.this$Anon0.t.getCurrentUser();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmPresenter$start$Anon1(AlarmPresenter alarmPresenter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = alarmPresenter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        AlarmPresenter$start$Anon1 alarmPresenter$start$Anon1 = new AlarmPresenter$start$Anon1(this.this$Anon0, yb4);
        alarmPresenter$start$Anon1.p$ = (zg4) obj;
        return alarmPresenter$start$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AlarmPresenter$start$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        AlarmPresenter alarmPresenter;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            FLogger.INSTANCE.getLocal().d(AlarmPresenter.v.a(), VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
            PortfolioApp.W.b((Object) zg4);
            this.this$Anon0.q.f();
            this.this$Anon0.s.f();
            BleCommandResultManager.d.a(CommunicateMode.SET_LIST_ALARM);
            AlarmPresenter alarmPresenter2 = this.this$Anon0;
            ug4 a2 = alarmPresenter2.c();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = alarmPresenter2;
            this.label = 1;
            obj = yf4.a(a2, anon1, this);
            if (obj == a) {
                return a;
            }
            alarmPresenter = alarmPresenter2;
        } else if (i == 1) {
            alarmPresenter = (AlarmPresenter) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        alarmPresenter.k = (MFUser) obj;
        MFUser j = this.this$Anon0.k;
        if (!(j == null || j.getUserId() == null)) {
            this.this$Anon0.m.l(this.this$Anon0.p != null);
            if (this.this$Anon0.i == null) {
                this.this$Anon0.i = new SparseIntArray();
            }
            if (this.this$Anon0.f == null) {
                if (this.this$Anon0.p != null) {
                    AlarmPresenter alarmPresenter3 = this.this$Anon0;
                    alarmPresenter3.f = alarmPresenter3.p.getTitle();
                    AlarmPresenter alarmPresenter4 = this.this$Anon0;
                    alarmPresenter4.g = alarmPresenter4.p.getTotalMinutes();
                    AlarmPresenter alarmPresenter5 = this.this$Anon0;
                    alarmPresenter5.h = alarmPresenter5.p.isRepeated();
                    AlarmPresenter alarmPresenter6 = this.this$Anon0;
                    alarmPresenter6.i = alarmPresenter6.a(alarmPresenter6.p.getDays());
                    AlarmPresenter alarmPresenter7 = this.this$Anon0;
                    alarmPresenter7.j = alarmPresenter7.p.isActive();
                    this.this$Anon0.m.e(false);
                    this.this$Anon0.m.K();
                } else {
                    this.this$Anon0.f = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_HybridAlarm_Title__Alarm);
                    this.this$Anon0.j = true;
                    this.this$Anon0.h = false;
                    this.this$Anon0.i = new SparseIntArray();
                    this.this$Anon0.k();
                    this.this$Anon0.m.e(true);
                }
            }
            this.this$Anon0.m.a(this.this$Anon0.g);
            this.this$Anon0.m.k(this.this$Anon0.h);
            yt2 l = this.this$Anon0.m;
            SparseIntArray d = this.this$Anon0.i;
            if (d != null) {
                l.a(d);
            } else {
                kd4.a();
                throw null;
            }
        }
        return qa4.a;
    }
}
