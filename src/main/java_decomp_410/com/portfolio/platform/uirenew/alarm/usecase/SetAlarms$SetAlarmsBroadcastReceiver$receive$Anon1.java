package com.portfolio.platform.uirenew.alarm.usecase;

import android.content.Intent;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ul2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.uirenew.alarm.usecase.SetAlarms$SetAlarmsBroadcastReceiver$receive$Anon1", f = "SetAlarms.kt", l = {43, 45, 58}, m = "invokeSuspend")
public final class SetAlarms$SetAlarmsBroadcastReceiver$receive$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Intent $intent;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SetAlarms.SetAlarmsBroadcastReceiver this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAlarms$SetAlarmsBroadcastReceiver$receive$Anon1(SetAlarms.SetAlarmsBroadcastReceiver setAlarmsBroadcastReceiver, Intent intent, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = setAlarmsBroadcastReceiver;
        this.$intent = intent;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SetAlarms$SetAlarmsBroadcastReceiver$receive$Anon1 setAlarms$SetAlarmsBroadcastReceiver$receive$Anon1 = new SetAlarms$SetAlarmsBroadcastReceiver$receive$Anon1(this.this$Anon0, this.$intent, yb4);
        setAlarms$SetAlarmsBroadcastReceiver$receive$Anon1.p$ = (zg4) obj;
        return setAlarms$SetAlarmsBroadcastReceiver$receive$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SetAlarms$SetAlarmsBroadcastReceiver$receive$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Alarm alarm;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            alarm = SetAlarms.this.h.getAlarmById(SetAlarms.this.e().a().getUri());
            if (this.$intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                FLogger.INSTANCE.getLocal().d(SetAlarms.j.a(), "onReceive success");
                if (alarm == null) {
                    AlarmsRepository a2 = SetAlarms.this.h;
                    Alarm a3 = SetAlarms.this.e().a();
                    this.L$Anon0 = zg4;
                    this.L$Anon1 = alarm;
                    this.label = 1;
                    if (a2.insertAlarm(a3, this) == a) {
                        return a;
                    }
                } else {
                    AlarmsRepository a4 = SetAlarms.this.h;
                    Alarm a5 = SetAlarms.this.e().a();
                    this.L$Anon0 = zg4;
                    this.L$Anon1 = alarm;
                    this.label = 2;
                    if (a4.updateAlarm(a5, this) == a) {
                        return a;
                    }
                }
            } else {
                FLogger.INSTANCE.getLocal().d(SetAlarms.j.a(), "onReceive failed");
                Alarm copy$default = Alarm.copy$default(SetAlarms.this.e().a(), (String) null, (String) null, (String) null, 0, 0, (int[]) null, false, false, (String) null, (String) null, 0, 2047, (Object) null);
                if (alarm == null) {
                    copy$default.setActive(false);
                    AlarmsRepository a6 = SetAlarms.this.h;
                    this.L$Anon0 = zg4;
                    this.L$Anon1 = alarm;
                    this.L$Anon2 = copy$default;
                    this.label = 3;
                    if (a6.insertAlarm(copy$default, this) == a) {
                        return a;
                    }
                    alarm = copy$default;
                }
                SetAlarms.this.a(this.$intent, alarm);
                return qa4.a;
            }
        } else if (i == 1 || i == 2) {
            Alarm alarm2 = (Alarm) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 3) {
            Alarm alarm3 = (Alarm) this.L$Anon1;
            zg4 zg43 = (zg4) this.L$Anon0;
            na4.a(obj);
            alarm = (Alarm) this.L$Anon2;
            SetAlarms.this.a(this.$intent, alarm);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ul2 c = AnalyticsHelper.f.c("set_alarms");
        if (c != null) {
            c.a("");
        }
        AnalyticsHelper.f.e("set_alarms");
        SetAlarms setAlarms = SetAlarms.this;
        setAlarms.a(new SetAlarms.d(setAlarms.e().a()));
        return qa4.a;
    }
}
