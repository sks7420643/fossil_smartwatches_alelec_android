package com.portfolio.platform.uirenew.alarm.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm", mo27670f = "DeleteAlarm.kt", mo27671l = {87}, mo27672m = "run")
public final class DeleteAlarm$run$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeleteAlarm$run$1(com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm deleteAlarm, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = deleteAlarm;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.mo26310a((com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm.C6266c) null, (com.fossil.blesdk.obfuscated.yb4<java.lang.Object>) this);
    }
}
