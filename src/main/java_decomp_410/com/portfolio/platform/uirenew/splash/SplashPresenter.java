package com.portfolio.platform.uirenew.splash;

import android.os.Handler;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ap3;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.zo3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.migration.MigrationHelper;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SplashPresenter extends zo3 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a((fd4) null);
    @DexIgnore
    public /* final */ Handler f; // = new Handler();
    @DexIgnore
    public /* final */ Runnable g; // = new b(this);
    @DexIgnore
    public /* final */ ap3 h;
    @DexIgnore
    public /* final */ UserRepository i;
    @DexIgnore
    public /* final */ MigrationHelper j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return SplashPresenter.k;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ SplashPresenter e;

        @DexIgnore
        public b(SplashPresenter splashPresenter) {
            this.e = splashPresenter;
        }

        @DexIgnore
        public final void run() {
            FLogger.INSTANCE.getLocal().d(SplashPresenter.l.a(), "runnable");
            this.e.h();
        }
    }

    /*
    static {
        String simpleName = SplashPresenter.class.getSimpleName();
        kd4.a((Object) simpleName, "SplashPresenter::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public SplashPresenter(ap3 ap3, UserRepository userRepository, MigrationHelper migrationHelper, en2 en2) {
        kd4.b(ap3, "mView");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(migrationHelper, "mMigrationHelper");
        kd4.b(en2, "mSharePrefs");
        this.h = ap3;
        this.i = userRepository;
        this.j = migrationHelper;
    }

    @DexIgnore
    public final void h() {
        fi4 unused = ag4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new SplashPresenter$checkToGoToNextStep$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        this.h.a(this);
    }

    @DexIgnore
    public void f() {
        String h2 = PortfolioApp.W.c().h();
        boolean a2 = this.j.a(h2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "currentAppVersion " + h2 + " complete " + a2);
        if (!a2) {
            this.j.b(h2);
        }
        this.f.postDelayed(this.g, 1000);
    }

    @DexIgnore
    public void g() {
        this.f.removeCallbacksAndMessages((Object) null);
    }
}
