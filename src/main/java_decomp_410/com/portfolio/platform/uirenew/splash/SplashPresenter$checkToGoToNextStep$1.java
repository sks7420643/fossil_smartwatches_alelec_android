package com.portfolio.platform.uirenew.splash;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1", mo27670f = "SplashPresenter.kt", mo27671l = {63}, mo27672m = "invokeSuspend")
public final class SplashPresenter$checkToGoToNextStep$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24234p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.uirenew.splash.SplashPresenter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SplashPresenter$checkToGoToNextStep$1(com.portfolio.platform.uirenew.splash.SplashPresenter splashPresenter, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = splashPresenter;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1 splashPresenter$checkToGoToNextStep$1 = new com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1(this.this$0, yb4);
        splashPresenter$checkToGoToNextStep$1.f24234p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return splashPresenter$checkToGoToNextStep$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24234p$;
            java.lang.String h = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34539h();
            boolean a2 = this.this$0.f24232j.mo39712a(h);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a3 = com.portfolio.platform.uirenew.splash.SplashPresenter.f24227l.mo41738a();
            local.mo33255d(a3, "checkToGoToNextStep isMigrationComplete " + a2);
            if (!a2) {
                this.this$0.f24228f.postDelayed(this.this$0.f24229g, 500);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            com.fossil.blesdk.obfuscated.ug4 b = this.this$0.mo31440b();
            com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1$mCurrentUser$1 splashPresenter$checkToGoToNextStep$1$mCurrentUser$1 = new com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1$mCurrentUser$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = h;
            this.Z$0 = a2;
            this.label = 1;
            obj = com.fossil.blesdk.obfuscated.yf4.m30997a(b, splashPresenter$checkToGoToNextStep$1$mCurrentUser$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            java.lang.String str = (java.lang.String) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.portfolio.platform.data.model.MFUser mFUser = (com.portfolio.platform.data.model.MFUser) obj;
        if (mFUser == null) {
            this.this$0.f24230h.mo25754p0();
        } else {
            java.lang.String email = mFUser.getEmail();
            boolean z = false;
            if (!(email == null || com.fossil.blesdk.obfuscated.qf4.m26950a(email))) {
                java.lang.String birthday = mFUser.getBirthday();
                if (birthday == null || com.fossil.blesdk.obfuscated.qf4.m26950a(birthday)) {
                    z = true;
                }
                if (!z) {
                    java.lang.String firstName = mFUser.getFirstName();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) firstName, "mCurrentUser.firstName");
                    if (!com.fossil.blesdk.obfuscated.qf4.m26950a(firstName)) {
                        java.lang.String lastName = mFUser.getLastName();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) lastName, "mCurrentUser.lastName");
                        if (!com.fossil.blesdk.obfuscated.qf4.m26950a(lastName)) {
                            this.this$0.f24230h.mo25753h();
                        }
                    }
                }
            }
            this.this$0.f24230h.mo25752d0();
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
