package com.portfolio.platform.gson;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.tz1;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.vz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.google.gson.JsonElement;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AlarmHelper;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmDeserializer implements vz1<Alarm> {
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00cb  */
    public Alarm deserialize(JsonElement jsonElement, Type type, uz1 uz1) {
        String str;
        JsonElement a;
        String str2;
        JsonElement a2;
        String str3;
        tz1 b;
        int[] iArr;
        xz1 d = jsonElement != null ? jsonElement.d() : null;
        if (d == null) {
            return null;
        }
        JsonElement a3 = d.a("id");
        String f = a3 != null ? a3.f() : null;
        if (f == null) {
            return null;
        }
        JsonElement a4 = d.a("title");
        if (a4 != null) {
            String f2 = a4.f();
            if (f2 != null) {
                str = f2;
                JsonElement a5 = d.a(MFSleepGoal.COLUMN_MINUTE);
                int i = 0;
                int b2 = a5 == null ? a5.b() : 0;
                JsonElement a6 = d.a(AppFilter.COLUMN_HOUR);
                int b3 = a6 == null ? a6.b() : 0;
                JsonElement a7 = d.a("isActive");
                boolean a8 = a7 == null ? a7.a() : false;
                JsonElement a9 = d.a("isRepeated");
                boolean a10 = a9 == null ? a9.a() : false;
                a = d.a("createdAt");
                if (a != null) {
                    String f3 = a.f();
                    if (f3 != null) {
                        str2 = f3;
                        a2 = d.a("updatedAt");
                        if (a2 != null) {
                            String f4 = a2.f();
                            if (f4 != null) {
                                str3 = f4;
                                b = d.b(com.misfit.frameworks.buttonservice.model.Alarm.COLUMN_DAYS);
                                if (b == null) {
                                    int[] iArr2 = new int[b.size()];
                                    for (Object next : b) {
                                        int i2 = i + 1;
                                        if (i >= 0) {
                                            JsonElement jsonElement2 = (JsonElement) next;
                                            if (jsonElement2 != null) {
                                                AlarmHelper.a aVar = AlarmHelper.f;
                                                String f5 = jsonElement2.f();
                                                kd4.a((Object) f5, "it.asString");
                                                iArr2[i] = aVar.a(f5);
                                            }
                                            i = i2;
                                        } else {
                                            cb4.c();
                                            throw null;
                                        }
                                    }
                                    iArr = iArr2;
                                } else {
                                    iArr = null;
                                }
                                return new Alarm(f, f, str, b3, b2, iArr, a8, a10, str2, str3, 0);
                            }
                        }
                        str3 = "";
                        b = d.b(com.misfit.frameworks.buttonservice.model.Alarm.COLUMN_DAYS);
                        if (b == null) {
                        }
                        return new Alarm(f, f, str, b3, b2, iArr, a8, a10, str2, str3, 0);
                    }
                }
                str2 = "";
                a2 = d.a("updatedAt");
                if (a2 != null) {
                }
                str3 = "";
                b = d.b(com.misfit.frameworks.buttonservice.model.Alarm.COLUMN_DAYS);
                if (b == null) {
                }
                return new Alarm(f, f, str, b3, b2, iArr, a8, a10, str2, str3, 0);
            }
        }
        str = "";
        JsonElement a52 = d.a(MFSleepGoal.COLUMN_MINUTE);
        int i3 = 0;
        if (a52 == null) {
        }
        JsonElement a62 = d.a(AppFilter.COLUMN_HOUR);
        if (a62 == null) {
        }
        JsonElement a72 = d.a("isActive");
        if (a72 == null) {
        }
        JsonElement a92 = d.a("isRepeated");
        if (a92 == null) {
        }
        a = d.a("createdAt");
        if (a != null) {
        }
        str2 = "";
        a2 = d.a("updatedAt");
        if (a2 != null) {
        }
        str3 = "";
        b = d.b(com.misfit.frameworks.buttonservice.model.Alarm.COLUMN_DAYS);
        if (b == null) {
        }
        return new Alarm(f, f, str, b3, b2, iArr, a8, a10, str2, str3, 0);
    }
}
