package com.portfolio.platform.gson;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rj2;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.vz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridPresetDeserializer implements vz1<HybridPreset> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public HybridPreset deserialize(JsonElement jsonElement, Type type, uz1 uz1) {
        String str;
        String str2;
        String str3;
        String str4;
        Iterator<JsonElement> it;
        String str5;
        String str6;
        String str7;
        if (jsonElement != null) {
            xz1 d = jsonElement.d();
            JsonElement a2 = d.a("name");
            kd4.a((Object) a2, "jsonObject.get(Constants.JSON_KEY_NAME)");
            String f = a2.f();
            JsonElement a3 = d.a("id");
            kd4.a((Object) a3, "jsonObject.get(\"id\")");
            String f2 = a3.f();
            String str8 = "";
            if (d.d("serialNumber")) {
                JsonElement a4 = d.a("serialNumber");
                kd4.a((Object) a4, "jsonObject.get(Constants.JSON_KEY_SERIAL_NUMBER)");
                str = a4.f();
            } else {
                str = str8;
            }
            JsonElement a5 = d.a("isActive");
            kd4.a((Object) a5, "jsonObject.get(Constants\u2026SON_KEY_IS_PRESET_ACTIVE)");
            boolean a6 = a5.a();
            JsonElement a7 = d.a("updatedAt");
            kd4.a((Object) a7, "jsonObject.get(Constants.JSON_KEY_UPDATED_AT)");
            String f3 = a7.f();
            JsonElement a8 = d.a("createdAt");
            kd4.a((Object) a8, "jsonObject.get(Constants.JSON_KEY_CREATED_AT)");
            String f4 = a8.f();
            ArrayList arrayList = new ArrayList();
            if (uz1 != null) {
                Iterator<JsonElement> it2 = d.b("buttons").iterator();
                while (it2.hasNext()) {
                    JsonElement next = it2.next();
                    kd4.a((Object) next, "item");
                    xz1 d2 = next.d();
                    if (d2.d("buttonPosition")) {
                        JsonElement a9 = d2.a("buttonPosition");
                        kd4.a((Object) a9, "itemJsonObject.get(Constants.JSON_KEY_BUTTON_POS)");
                        str2 = a9.f();
                    } else {
                        str2 = str8;
                    }
                    if (d2.d("appId")) {
                        JsonElement a10 = d2.a("appId");
                        kd4.a((Object) a10, "itemJsonObject.get(Constants.JSON_KEY_APP_ID)");
                        str3 = a10.f();
                    } else {
                        str3 = str8;
                    }
                    if (d2.d("localUpdatedAt")) {
                        it = it2;
                        JsonElement a11 = d2.a("localUpdatedAt");
                        str4 = str8;
                        kd4.a((Object) a11, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        str5 = a11.f();
                    } else {
                        it = it2;
                        str4 = str8;
                        Calendar instance = Calendar.getInstance();
                        kd4.a((Object) instance, "Calendar.getInstance()");
                        str5 = rk2.t(instance.getTime());
                    }
                    if (d2.d(Constants.USER_SETTING)) {
                        try {
                            JsonElement a12 = d2.a(Constants.USER_SETTING);
                            kd4.a((Object) a12, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str7 = rj2.a(a12.d());
                            str6 = f3;
                        } catch (Exception unused) {
                            str6 = f3;
                            FLogger.INSTANCE.getLocal().d("HybridPresetDeserializer", "Exception when parse json string");
                        }
                    } else {
                        str6 = f3;
                        str7 = str4;
                    }
                    kd4.a((Object) str2, "position");
                    kd4.a((Object) str3, "appId");
                    kd4.a((Object) str5, "localUpdatedAt");
                    arrayList.add(new HybridPresetAppSetting(str2, str3, str5, str7));
                    it2 = it;
                    str8 = str4;
                    f3 = str6;
                }
            }
            kd4.a((Object) f2, "id");
            kd4.a((Object) str, "serialNumber");
            HybridPreset hybridPreset = new HybridPreset(f2, f, str, arrayList, a6);
            hybridPreset.setCreatedAt(f4);
            hybridPreset.setUpdatedAt(f3);
            return hybridPreset;
        }
        kd4.a();
        throw null;
    }
}
