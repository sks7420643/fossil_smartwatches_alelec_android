package com.portfolio.platform.gson;

import com.fossil.blesdk.obfuscated.a02;
import com.fossil.blesdk.obfuscated.b02;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.oj2;
import com.fossil.blesdk.obfuscated.tz1;
import com.fossil.blesdk.obfuscated.wz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yz1;
import com.google.gson.JsonElement;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import java.lang.reflect.Type;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridPresetAppSettingSerializer implements b02<List<? extends HybridPresetAppSetting>> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(List<HybridPresetAppSetting> list, Type type, a02 a02) {
        kd4.b(list, "src");
        tz1 tz1 = new tz1();
        yz1 yz1 = new yz1();
        for (HybridPresetAppSetting next : list) {
            String component1 = next.component1();
            String component2 = next.component2();
            String component3 = next.component3();
            String component4 = next.component4();
            xz1 xz1 = new xz1();
            xz1.a("appId", component2);
            xz1.a("buttonPosition", component1);
            xz1.a("localUpdatedAt", component3);
            if (!oj2.a(component4)) {
                try {
                    JsonElement a = yz1.a(component4);
                    if (a != null) {
                        xz1.a(Constants.USER_SETTING, (JsonElement) (xz1) a);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.google.gson.JsonObject");
                    }
                } catch (Exception unused) {
                    xz1.a(Constants.USER_SETTING, (JsonElement) new wz1());
                }
            } else {
                xz1.a(Constants.USER_SETTING, (JsonElement) new wz1());
            }
            tz1.a((JsonElement) xz1);
        }
        return tz1;
    }
}
