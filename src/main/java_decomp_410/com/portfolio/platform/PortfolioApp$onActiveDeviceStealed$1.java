package com.portfolio.platform;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.PortfolioApp$onActiveDeviceStealed$1", mo27670f = "PortfolioApp.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class PortfolioApp$onActiveDeviceStealed$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20985p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.PortfolioApp this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PortfolioApp$onActiveDeviceStealed$1(com.portfolio.platform.PortfolioApp portfolioApp, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = portfolioApp;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.PortfolioApp$onActiveDeviceStealed$1 portfolioApp$onActiveDeviceStealed$1 = new com.portfolio.platform.PortfolioApp$onActiveDeviceStealed$1(this.this$0, yb4);
        portfolioApp$onActiveDeviceStealed$1.f20985p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return portfolioApp$onActiveDeviceStealed$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.PortfolioApp$onActiveDeviceStealed$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String d = com.portfolio.platform.PortfolioApp.f20941W.mo34591d();
            local.mo33255d(d, "activeDevice=" + this.this$0.mo34532e() + ", was stealed remove it!!!");
            if (com.portfolio.platform.helper.DeviceHelper.f21188o.mo39568f(this.this$0.mo34532e())) {
                this.this$0.mo34575v().deleteWatchFacesWithSerial(this.this$0.mo34532e());
            }
            com.portfolio.platform.PortfolioApp portfolioApp = this.this$0;
            portfolioApp.mo34571r(portfolioApp.mo34532e());
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
