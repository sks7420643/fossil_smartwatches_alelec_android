package com.portfolio.platform;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.databinding.ViewDataBinding;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.i6;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m82;
import com.fossil.blesdk.obfuscated.qa;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.view.FlexibleTextView;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@SuppressLint({"Registered"})
public class BaseWebViewActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ String D;
    @DexIgnore
    public static /* final */ a E; // = new a((fd4) null);
    @DexIgnore
    public m82 B;
    @DexIgnore
    public String C;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return BaseWebViewActivity.D;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            kd4.b(context, "context");
            kd4.b(str, "title");
            kd4.b(str2, "url");
            Intent intent = new Intent(context, BaseWebViewActivity.class);
            intent.putExtra("urlToLoad", str2);
            intent.putExtra("title", str);
            context.startActivity(intent);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends WebViewClient {
        @DexIgnore
        public /* final */ /* synthetic */ BaseWebViewActivity a;

        @DexIgnore
        public b(BaseWebViewActivity baseWebViewActivity) {
            this.a = baseWebViewActivity;
        }

        @DexIgnore
        @TargetApi(21)
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            throw null;
            // ILocalFLogger local = FLogger.INSTANCE.getLocal();
            // String a2 = BaseWebViewActivity.E.a();
            // StringBuilder sb = new StringBuilder();
            // sb.append("Should override ");
            // Uri uri = null;
            // sb.append(webResourceRequest != null ? webResourceRequest.getUrl() : null);
            // local.d(a2, sb.toString());
            // if (!qf4.c(String.valueOf(webResourceRequest != null ? webResourceRequest.getUrl() : null), "mailto", false, 2, (Object) null)) {
            //     return super.shouldOverrideUrlLoading(webView, webResourceRequest);
            // }
            // FLogger.INSTANCE.getLocal().d(BaseWebViewActivity.E.a(), "We are overriding the mailto urlToLoad");
            // if (webResourceRequest != null) {
            //     uri = webResourceRequest.getUrl();
            // }
            // String valueOf = String.valueOf(uri);
            // if (valueOf != null) {
            //     String substring = valueOf.substring(7);
            //     kd4.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
            //     i6 a3 = i6.a((Activity) this.a);
            //     a3.b("message/rfc822");
            //     a3.a(substring);
            //     a3.c();
            //     return true;
            // }
            // throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends WebChromeClient {
        @DexIgnore
        public /* final */ /* synthetic */ BaseWebViewActivity a;

        @DexIgnore
        public c(BaseWebViewActivity baseWebViewActivity) {
            this.a = baseWebViewActivity;
        }

        @DexIgnore
        public void onProgressChanged(WebView webView, int i) {
            kd4.b(webView, "view");
            if (i == 100) {
                this.a.h();
            }
        }
    }

    /*
    static {
        String simpleName = BaseWebViewActivity.class.getSimpleName();
        kd4.a((Object) simpleName, "BaseWebViewActivity::class.java.simpleName");
        D = simpleName;
    }
    */

    @DexIgnore
    public void a(m82 m82) {
        kd4.b(m82, "<set-?>");
        this.B = m82;
    }

    @DexIgnore
    public void c(String str) {
        kd4.b(str, "<set-?>");
        this.C = str;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        if (getIntent().getStringExtra("urlToLoad") == null) {
            str = "";
        } else {
            str = getIntent().getStringExtra("urlToLoad");
            kd4.a((Object) str, "intent.getStringExtra(KEY_URL)");
        }
        c(str);
        ViewDataBinding a2 = qa.a(this, R.layout.activity_webview);
        kd4.a((Object) a2, "DataBindingUtil.setConte\u2026.layout.activity_webview)");
        a((m82) a2);
        FlexibleTextView flexibleTextView = u().q;
        kd4.a((Object) flexibleTextView, "binding.ftvTitle");
        flexibleTextView.setText(getIntent().getStringExtra("title"));
        BaseActivity.a(this, true, (String) null, 2, (Object) null);
        t();
        w();
    }

    @DexIgnore
    public WebViewClient s() {
        FLogger.INSTANCE.getLocal().d(D, "Building default web client");
        return new b(this);
    }

    @DexIgnore
    public final void t() {
        if (Build.VERSION.SDK_INT >= 22) {
            CookieManager.getInstance().removeAllCookies((ValueCallback) null);
            CookieManager.getInstance().flush();
            return;
        }
        CookieSyncManager createInstance = CookieSyncManager.createInstance(getBaseContext());
        createInstance.startSync();
        CookieManager instance = CookieManager.getInstance();
        instance.removeAllCookie();
        instance.removeSessionCookie();
        createInstance.stopSync();
        createInstance.sync();
    }

    @DexIgnore
    public m82 u() {
        m82 m82 = this.B;
        if (m82 != null) {
            return m82;
        }
        kd4.d("binding");
        throw null;
    }

    @DexIgnore
    public String v() {
        String str = this.C;
        if (str != null) {
            return str;
        }
        kd4.d("urlToLoad");
        throw null;
    }

    @DexIgnore
    @SuppressLint({"SetJavaScriptEnabled"})
    public void w() {
        WebView webView = u().r;
        kd4.a((Object) webView, "binding.webView");
        WebSettings settings = webView.getSettings();
        kd4.a((Object) settings, "ws");
        settings.setSaveFormData(false);
        settings.setSavePassword(false);
        u().r.clearCache(true);
        WebView webView2 = u().r;
        kd4.a((Object) webView2, "binding.webView");
        webView2.setWebViewClient(s());
        WebView webView3 = u().r;
        kd4.a((Object) webView3, "binding.webView");
        WebSettings settings2 = webView3.getSettings();
        kd4.a((Object) settings2, Constants.USER_SETTING);
        settings2.setJavaScriptEnabled(true);
        settings2.setDomStorageEnabled(true);
        settings2.setAllowFileAccess(true);
        settings2.setAllowFileAccessFromFileURLs(true);
        settings2.setAllowUniversalAccessFromFileURLs(true);
        settings2.setUserAgentString("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36");
        WebView webView4 = u().r;
        kd4.a((Object) webView4, "binding.webView");
        webView4.setWebChromeClient(new c(this));
        if (PortfolioApp.W.e() && TextUtils.isEmpty(v())) {
            FLogger.INSTANCE.getLocal().e(D, "You must create a urlToLoad to load before the webview is created");
        }
        u().r.loadUrl(v());
    }
}
