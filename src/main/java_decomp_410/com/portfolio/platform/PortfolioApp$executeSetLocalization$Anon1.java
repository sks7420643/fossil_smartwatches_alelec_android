package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.PortfolioApp$executeSetLocalization$Anon1", f = "PortfolioApp.kt", l = {1203}, m = "invokeSuspend")
public final class PortfolioApp$executeSetLocalization$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PortfolioApp this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PortfolioApp$executeSetLocalization$Anon1(PortfolioApp portfolioApp, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = portfolioApp;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        PortfolioApp$executeSetLocalization$Anon1 portfolioApp$executeSetLocalization$Anon1 = new PortfolioApp$executeSetLocalization$Anon1(this.this$Anon0, yb4);
        portfolioApp$executeSetLocalization$Anon1.p$ = (zg4) obj;
        return portfolioApp$executeSetLocalization$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PortfolioApp$executeSetLocalization$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            WatchLocalizationRepository t = this.this$Anon0.t();
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = t.getWatchLocalizationFromServer(true, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        String str = (String) obj;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String d = PortfolioApp.W.d();
        local.d(d, "path of localization file: " + str);
        if (str != null) {
            IButtonConnectivity b = PortfolioApp.W.b();
            if (b != null) {
                b.setLocalizationData(new LocalizationData(str, (String) null, 2, (fd4) null));
            }
        }
        return qa4.a;
    }
}
