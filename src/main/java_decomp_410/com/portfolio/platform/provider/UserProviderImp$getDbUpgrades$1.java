package com.portfolio.platform.provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UserProviderImp$getDbUpgrades$1 extends java.util.HashMap<java.lang.Integer, com.fossil.wearables.fsl.shared.UpgradeCommand> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.provider.UserProviderImp this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1$a")
    /* renamed from: com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1$a */
    public static final class C5987a implements com.fossil.wearables.fsl.shared.UpgradeCommand {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1 f21313a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1$a$a")
        /* renamed from: com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1$a$a */
        public static final class C5988a<V> implements java.util.concurrent.Callable<T> {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1.C5987a f21314e;

            @DexIgnore
            /* renamed from: f */
            public /* final */ /* synthetic */ android.database.sqlite.SQLiteDatabase f21315f;

            @DexIgnore
            public C5988a(com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1.C5987a aVar, android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
                this.f21314e = aVar;
                this.f21315f = sQLiteDatabase;
            }

            @DexIgnore
            public final boolean call() {
                java.lang.String str;
                java.lang.String str2;
                java.lang.String str3;
                java.lang.String str4;
                java.lang.String str5;
                java.lang.String str6;
                java.lang.String str7;
                java.lang.String str8;
                java.lang.String str9;
                java.lang.String str10;
                java.lang.String str11;
                java.lang.String str12;
                java.lang.String str13;
                com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1.C5987a.C5988a aVar;
                java.util.ArrayList arrayList;
                java.lang.String str14;
                com.portfolio.platform.enums.Unit unit;
                com.portfolio.platform.enums.Unit unit2;
                java.lang.String str15;
                java.lang.String str16;
                java.lang.String str17;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String b = this.f21314e.f21313a.this$0.TAG;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) b, "TAG");
                local.mo33255d(b, "Start migration user from version 1 to 2");
                android.database.Cursor query = this.f21315f.query(true, "user", new java.lang.String[]{com.portfolio.platform.data.model.MFUser.USER_ACCESS_TOKEN, "uid", "createdAt", "updatedAt", "email", "authType", "username", "activeDeviceId", "firstName", "lastName", "weightInGrams", "heightInCentimeters", com.portfolio.platform.data.model.MFUser.HEIGHT_UNIT, com.portfolio.platform.data.model.MFUser.WEIGHT_UNIT, com.portfolio.platform.data.model.MFUser.DISTANCE_UNIT, "gender", "birthday", "profilePicture", "brand", "registrationComplete", "isOnboardingComplete", "integrations", "emailOptIn", "diagnosticEnabled", "pinType", "registerDate", com.portfolio.platform.data.model.MFUser.AVERAGE_STEP, com.portfolio.platform.data.model.MFUser.AVERAGE_SLEEP}, (java.lang.String) null, (java.lang.String[]) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null);
                java.util.ArrayList arrayList2 = new java.util.ArrayList();
                java.lang.String str18 = com.portfolio.platform.data.model.MFUser.DISTANCE_UNIT;
                java.lang.String str19 = com.portfolio.platform.data.model.MFUser.WEIGHT_UNIT;
                java.util.ArrayList arrayList3 = arrayList2;
                java.lang.String str20 = com.portfolio.platform.data.model.MFUser.HEIGHT_UNIT;
                java.lang.String str21 = "TAG";
                java.lang.String str22 = "heightInCentimeters";
                java.lang.String str23 = "weightInGrams";
                java.lang.String str24 = "registerDate";
                java.lang.String str25 = "lastName";
                java.lang.String str26 = "diagnosticEnabled";
                java.lang.String str27 = "emailOptIn";
                java.lang.String str28 = "integrations";
                java.lang.String str29 = "isOnboardingComplete";
                java.lang.String str30 = "registrationComplete";
                java.lang.String str31 = "brand";
                java.lang.String str32 = "profilePicture";
                java.lang.String str33 = "birthday";
                java.lang.String str34 = "gender";
                if (query != null) {
                    query.moveToFirst();
                    while (!query.isAfterLast()) {
                        java.lang.String str35 = str18;
                        com.portfolio.platform.data.model.MFUser mFUser = new com.portfolio.platform.data.model.MFUser();
                        mFUser.setUserAccessToken(query.getString(query.getColumnIndex(com.portfolio.platform.data.model.MFUser.USER_ACCESS_TOKEN)));
                        mFUser.setUserId(query.getString(query.getColumnIndex("uid")));
                        mFUser.setCreatedAt(query.getString(query.getColumnIndex("createdAt")));
                        mFUser.setUpdatedAt(query.getString(query.getColumnIndex("updatedAt")));
                        mFUser.setEmail(query.getString(query.getColumnIndex("email")));
                        mFUser.setAuthType(query.getString(query.getColumnIndex("authType")));
                        mFUser.setUsername(query.getString(query.getColumnIndex("username")));
                        mFUser.setActiveDeviceId(query.getString(query.getColumnIndex("activeDeviceId")));
                        mFUser.setFirstName(query.getString(query.getColumnIndex("firstName")));
                        mFUser.setLastName(query.getString(query.getColumnIndex(str25)));
                        mFUser.setWeightInGrams(query.getInt(query.getColumnIndex(str23)));
                        mFUser.setHeightInCentimeters(query.getInt(query.getColumnIndex(str22)));
                        mFUser.setHeightUnit(query.getString(query.getColumnIndex(str20)));
                        java.lang.String str36 = str19;
                        java.lang.String str37 = str20;
                        mFUser.setWeightUnit(query.getString(query.getColumnIndex(str36)));
                        java.lang.String str38 = str35;
                        java.lang.String str39 = str36;
                        mFUser.setDistanceUnit(query.getString(query.getColumnIndex(str38)));
                        java.lang.String str40 = str34;
                        java.lang.String str41 = str38;
                        mFUser.setGender(query.getString(query.getColumnIndex(str40)));
                        java.lang.String str42 = str33;
                        java.lang.String str43 = str40;
                        mFUser.setBirthday(query.getString(query.getColumnIndex(str42)));
                        java.lang.String str44 = str32;
                        java.lang.String str45 = str42;
                        mFUser.setProfilePicture(query.getString(query.getColumnIndex(str44)));
                        java.lang.String str46 = str31;
                        java.lang.String str47 = str44;
                        mFUser.setBrand(query.getString(query.getColumnIndex(str46)));
                        java.lang.String str48 = str30;
                        java.lang.String str49 = str46;
                        java.lang.String str50 = str48;
                        mFUser.setRegistrationComplete(query.getInt(query.getColumnIndex(str48)) == 1);
                        java.lang.String str51 = str29;
                        java.lang.String str52 = str22;
                        mFUser.setOnboardingComplete(query.getInt(query.getColumnIndex(str51)) == 1);
                        mFUser.setIntegrations(query.getString(query.getColumnIndex(str28)));
                        mFUser.setEmailOptIn(query.getInt(query.getColumnIndex(str27)) == 1);
                        java.lang.String str53 = str26;
                        java.lang.String str54 = str51;
                        mFUser.setDiagnosticEnabled(query.getInt(query.getColumnIndex(str53)) == 1);
                        mFUser.setRegisterDate(query.getString(query.getColumnIndex(str24)));
                        mFUser.setPinType(query.getString(query.getColumnIndex("pinType")));
                        mFUser.setAverageSleep(query.getInt(query.getColumnIndex(com.portfolio.platform.data.model.MFUser.AVERAGE_SLEEP)));
                        mFUser.setAverageStep(query.getInt(query.getColumnIndex(com.portfolio.platform.data.model.MFUser.AVERAGE_STEP)));
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String str55 = str53;
                        java.lang.String b2 = this.f21314e.f21313a.this$0.TAG;
                        java.lang.String str56 = str23;
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) b2, str21);
                        local2.mo33255d(b2, "Add user=" + mFUser + " to migration user list");
                        arrayList3.add(mFUser);
                        query.moveToNext();
                        str22 = str52;
                        str18 = str41;
                        str19 = str39;
                        str20 = str37;
                        str23 = str56;
                        str25 = str25;
                        str29 = str54;
                        str34 = str43;
                        str26 = str55;
                        str33 = str45;
                        str32 = str47;
                        str31 = str49;
                        str30 = str50;
                    }
                    str2 = str23;
                    str5 = str20;
                    str = str25;
                    str6 = str19;
                    arrayList = arrayList3;
                    str4 = str26;
                    str13 = str29;
                    str3 = str30;
                    str11 = str31;
                    str10 = str32;
                    str9 = str33;
                    str8 = str34;
                    aVar = this;
                    str12 = str22;
                    str7 = str18;
                    query.close();
                } else {
                    str2 = str23;
                    str5 = str20;
                    str = str25;
                    str6 = str19;
                    arrayList = arrayList3;
                    str4 = str26;
                    str13 = str29;
                    str3 = str30;
                    str11 = str31;
                    str10 = str32;
                    str9 = str33;
                    str8 = str34;
                    aVar = this;
                    str12 = str22;
                    str7 = str18;
                }
                aVar.f21315f.execSQL("CREATE TABLE user_copy (uid VARCHAR PRIMARY KEY, userAccessToken VARCHAR, refreshToken VARCHAR, accessTokenExpiresAt VARCHAR, createdAt VARCHAR, updatedAt INTEGER, email INTEGER, authType INTEGER,username VARCHAR, activeDeviceId INTEGER, firstName VARCHAR, lastName VARCHAR, weightInGrams INTEGER, heightInCentimeters INTEGER, heightUnit INTEGER, weightUnit INTEGER,distanceUnit VARCHAR, birthday VARCHAR, gender INTEGER, profilePicture VARCHAR, brand VARCHAR, registrationComplete INTEGER, isOnboardingComplete INTEGER, integrations INTEGER, emailOptIn INTEGER, diagnosticEnabled INTEGER, registerDate INTEGER, pinType VARCHAR, averageSleep INTEGER, averageStep INTEGER);");
                if (!arrayList.isEmpty()) {
                    java.util.Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        com.portfolio.platform.data.model.MFUser mFUser2 = (com.portfolio.platform.data.model.MFUser) it.next();
                        android.content.ContentValues contentValues = new android.content.ContentValues();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) mFUser2, "user");
                        contentValues.put("uid", mFUser2.getUserId());
                        contentValues.put(com.portfolio.platform.data.model.MFUser.USER_ACCESS_TOKEN, mFUser2.getUserAccessToken());
                        contentValues.put("createdAt", mFUser2.getCreatedAt());
                        contentValues.put("updatedAt", mFUser2.getUpdatedAt());
                        contentValues.put("email", mFUser2.getEmail());
                        com.portfolio.platform.enums.AuthType authType = mFUser2.getAuthType();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) authType, "user.authType");
                        contentValues.put("authType", authType.getValue());
                        contentValues.put("username", mFUser2.getUsername());
                        contentValues.put("activeDeviceId", mFUser2.getActiveDeviceId());
                        contentValues.put("firstName", mFUser2.getFirstName());
                        java.lang.String str57 = str;
                        contentValues.put(str57, mFUser2.getLastName());
                        java.lang.String str58 = str2;
                        contentValues.put(str58, java.lang.Integer.valueOf(mFUser2.getWeightInGrams()));
                        java.util.Iterator it2 = it;
                        java.lang.String str59 = str12;
                        contentValues.put(str59, java.lang.Integer.valueOf(mFUser2.getHeightInCentimeters()));
                        if (mFUser2.getHeightUnit() != null) {
                            com.portfolio.platform.enums.Unit heightUnit = mFUser2.getHeightUnit();
                            str17 = str59;
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) heightUnit, "user.heightUnit");
                            str14 = heightUnit.getValue();
                        } else {
                            str17 = str59;
                            str14 = com.portfolio.platform.enums.Unit.METRIC.getValue();
                        }
                        java.lang.String str60 = str5;
                        contentValues.put(str60, str14);
                        if (mFUser2.getWeightUnit() != null) {
                            unit = mFUser2.getWeightUnit();
                            str16 = str60;
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) unit, "user.weightUnit");
                        } else {
                            str16 = str60;
                            unit = com.portfolio.platform.enums.Unit.METRIC;
                        }
                        java.lang.String str61 = str6;
                        contentValues.put(str61, unit.getValue());
                        if (mFUser2.getDistanceUnit() != null) {
                            unit2 = mFUser2.getDistanceUnit();
                            str15 = str61;
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) unit2, "user.distanceUnit");
                        } else {
                            str15 = str61;
                            unit2 = com.portfolio.platform.enums.Unit.METRIC;
                        }
                        contentValues.put(str7, unit2.getValue());
                        java.lang.String str62 = str9;
                        contentValues.put(str62, mFUser2.getBirthday());
                        java.lang.String str63 = str8;
                        java.lang.String str64 = str62;
                        java.lang.String str65 = str63;
                        contentValues.put(str65, mFUser2.getGender() != null ? mFUser2.getGender().toString() : "");
                        contentValues.put(str10, mFUser2.getProfilePicture());
                        contentValues.put(str11, mFUser2.getBrand());
                        contentValues.put(str3, java.lang.Integer.valueOf(mFUser2.isRegistrationComplete() ? 1 : 0));
                        contentValues.put(str13, java.lang.Integer.valueOf(mFUser2.isOnboardingComplete() ? 1 : 0));
                        contentValues.put(str28, mFUser2.getIntegrationsRaw());
                        contentValues.put(str27, java.lang.Integer.valueOf(mFUser2.isEmailOptIn() ? 1 : 0));
                        contentValues.put(str4, java.lang.Integer.valueOf(mFUser2.isDiagnosticEnabled() ? 1 : 0));
                        contentValues.put(str24, mFUser2.getRegisterDate());
                        contentValues.put("pinType", mFUser2.getPinType());
                        contentValues.put(com.portfolio.platform.data.model.MFUser.AVERAGE_SLEEP, java.lang.Integer.valueOf(mFUser2.getAverageSleep()));
                        contentValues.put(com.portfolio.platform.data.model.MFUser.AVERAGE_STEP, java.lang.Integer.valueOf(mFUser2.getAverageStep()));
                        aVar.f21315f.insert("user_copy", (java.lang.String) null, contentValues);
                        str = str57;
                        str2 = str58;
                        it = it2;
                        java.lang.String str66 = str64;
                        str8 = str65;
                        str9 = str66;
                    }
                }
                aVar.f21315f.execSQL("DROP TABLE user;");
                aVar.f21315f.execSQL("ALTER TABLE user_copy RENAME TO user;");
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String b3 = aVar.f21314e.f21313a.this$0.TAG;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) b3, str21);
                local3.mo33255d(b3, "Migration user complete");
                return true;
            }
        }

        @DexIgnore
        public C5987a(com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1 userProviderImp$getDbUpgrades$1) {
            this.f21313a = userProviderImp$getDbUpgrades$1;
        }

        @DexIgnore
        public final void execute(android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
            try {
                com.fossil.wearables.fsl.shared.DatabaseHelper a = this.f21313a.this$0.databaseHelper;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "databaseHelper");
                com.j256.ormlite.misc.TransactionManager.callInTransaction(a.getConnectionSource(), new com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1.C5987a.C5988a(this, sQLiteDatabase));
            } catch (java.lang.Exception e) {
                e.printStackTrace();
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String b = this.f21313a.this$0.TAG;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) b, "TAG");
                local.mo33255d(b, "Exception when migrate user.");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1$b")
    /* renamed from: com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1$b */
    public static final class C5989b implements com.fossil.wearables.fsl.shared.UpgradeCommand {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1 f21316a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1$b$a")
        /* renamed from: com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1$b$a */
        public static final class C5990a<V> implements java.util.concurrent.Callable<T> {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1.C5989b f21317e;

            @DexIgnore
            /* renamed from: f */
            public /* final */ /* synthetic */ android.database.sqlite.SQLiteDatabase f21318f;

            @DexIgnore
            public C5990a(com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1.C5989b bVar, android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
                this.f21317e = bVar;
                this.f21318f = sQLiteDatabase;
            }

            @DexIgnore
            public final void call() {
                java.lang.String str;
                java.lang.String str2;
                java.lang.String str3;
                java.lang.String str4;
                java.lang.String str5;
                java.lang.String str6;
                java.lang.String str7;
                java.lang.String str8;
                java.lang.String str9;
                java.lang.String str10;
                java.lang.String str11;
                java.lang.String str12;
                java.lang.String str13;
                java.lang.String str14;
                com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1.C5989b.C5990a aVar;
                java.util.ArrayList arrayList;
                com.portfolio.platform.enums.Unit unit;
                com.portfolio.platform.enums.Unit unit2;
                com.portfolio.platform.enums.Unit unit3;
                java.lang.String str15;
                java.lang.String str16;
                java.lang.String str17;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String b = this.f21317e.f21316a.this$0.TAG;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) b, "TAG");
                local.mo33255d(b, "start migration from db version 3");
                android.database.Cursor query = this.f21318f.query(true, "user", new java.lang.String[]{"uid", com.portfolio.platform.data.model.MFUser.USER_ACCESS_TOKEN, "refreshToken", com.portfolio.platform.data.model.MFUser.ACCESS_TOKEN_EXPIRED_AT, "createdAt", "updatedAt", "email", "authType", "username", "activeDeviceId", "firstName", "lastName", "weightInGrams", "heightInCentimeters", com.portfolio.platform.data.model.MFUser.HEIGHT_UNIT, com.portfolio.platform.data.model.MFUser.WEIGHT_UNIT, com.portfolio.platform.data.model.MFUser.DISTANCE_UNIT, "gender", "birthday", "profilePicture", "brand", "registrationComplete", "isOnboardingComplete", "integrations", "emailOptIn", "diagnosticEnabled", "registerDate", "pinType", com.portfolio.platform.data.model.MFUser.AVERAGE_STEP, com.portfolio.platform.data.model.MFUser.AVERAGE_SLEEP}, (java.lang.String) null, (java.lang.String[]) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null);
                java.util.ArrayList arrayList2 = new java.util.ArrayList();
                java.lang.String str18 = com.portfolio.platform.data.model.MFUser.HEIGHT_UNIT;
                java.lang.String str19 = "heightInCentimeters";
                java.util.ArrayList arrayList3 = arrayList2;
                java.lang.String str20 = "weightInGrams";
                java.lang.String str21 = "lastName";
                java.lang.String str22 = "diagnosticEnabled";
                java.lang.String str23 = "firstName";
                java.lang.String str24 = "emailOptIn";
                java.lang.String str25 = "integrations";
                java.lang.String str26 = "isOnboardingComplete";
                java.lang.String str27 = "registrationComplete";
                java.lang.String str28 = "brand";
                java.lang.String str29 = "profilePicture";
                java.lang.String str30 = "birthday";
                java.lang.String str31 = "gender";
                java.lang.String str32 = com.portfolio.platform.data.model.MFUser.DISTANCE_UNIT;
                java.lang.String str33 = com.portfolio.platform.data.model.MFUser.WEIGHT_UNIT;
                if (query != null) {
                    query.moveToFirst();
                    while (!query.isAfterLast()) {
                        java.lang.String str34 = str18;
                        com.portfolio.platform.data.model.MFUser mFUser = new com.portfolio.platform.data.model.MFUser();
                        mFUser.setUserId(query.getString(query.getColumnIndex("uid")));
                        mFUser.setUserAccessToken(query.getString(query.getColumnIndex(com.portfolio.platform.data.model.MFUser.USER_ACCESS_TOKEN)));
                        mFUser.setRefreshToken(query.getString(query.getColumnIndex("refreshToken")));
                        mFUser.setAccessTokenExpiresAt(query.getString(query.getColumnIndex(com.portfolio.platform.data.model.MFUser.ACCESS_TOKEN_EXPIRED_AT)));
                        mFUser.setCreatedAt(query.getString(query.getColumnIndex("createdAt")));
                        mFUser.setUpdatedAt(query.getString(query.getColumnIndex("updatedAt")));
                        mFUser.setEmail(query.getString(query.getColumnIndex("email")));
                        mFUser.setAuthType(query.getString(query.getColumnIndex("authType")));
                        mFUser.setUsername(query.getString(query.getColumnIndex("username")));
                        mFUser.setActiveDeviceId(query.getString(query.getColumnIndex("activeDeviceId")));
                        mFUser.setFirstName(query.getString(query.getColumnIndex(str23)));
                        mFUser.setLastName(query.getString(query.getColumnIndex(str21)));
                        mFUser.setWeightInGrams(query.getInt(query.getColumnIndex(str20)));
                        java.lang.String str35 = str19;
                        java.lang.String str36 = str20;
                        mFUser.setHeightInCentimeters(query.getInt(query.getColumnIndex(str35)));
                        java.lang.String str37 = str34;
                        java.lang.String str38 = str35;
                        mFUser.setHeightUnit(query.getString(query.getColumnIndex(str37)));
                        java.lang.String str39 = str33;
                        java.lang.String str40 = str37;
                        mFUser.setWeightUnit(query.getString(query.getColumnIndex(str39)));
                        java.lang.String str41 = str32;
                        java.lang.String str42 = str39;
                        mFUser.setDistanceUnit(query.getString(query.getColumnIndex(str41)));
                        java.lang.String str43 = str31;
                        java.lang.String str44 = str41;
                        mFUser.setGender(query.getString(query.getColumnIndex(str43)));
                        java.lang.String str45 = str30;
                        java.lang.String str46 = str43;
                        mFUser.setBirthday(query.getString(query.getColumnIndex(str45)));
                        java.lang.String str47 = str29;
                        java.lang.String str48 = str45;
                        mFUser.setProfilePicture(query.getString(query.getColumnIndex(str47)));
                        java.lang.String str49 = str28;
                        java.lang.String str50 = str47;
                        mFUser.setBrand(query.getString(query.getColumnIndex(str49)));
                        java.lang.String str51 = str27;
                        java.lang.String str52 = str49;
                        java.lang.String str53 = str51;
                        mFUser.setRegistrationComplete(query.getInt(query.getColumnIndex(str51)) == 1);
                        java.lang.String str54 = str26;
                        java.lang.String str55 = str21;
                        mFUser.setOnboardingComplete(query.getInt(query.getColumnIndex(str54)) == 1);
                        mFUser.setIntegrations(query.getString(query.getColumnIndex(str25)));
                        mFUser.setEmailOptIn(query.getInt(query.getColumnIndex(str24)) == 1);
                        java.lang.String str56 = str22;
                        java.lang.String str57 = str54;
                        mFUser.setDiagnosticEnabled(query.getInt(query.getColumnIndex(str56)) == 1);
                        mFUser.setRegisterDate(query.getString(query.getColumnIndex("registerDate")));
                        mFUser.setPinType(query.getString(query.getColumnIndex("pinType")));
                        mFUser.setAverageSleep(query.getInt(query.getColumnIndex(com.portfolio.platform.data.model.MFUser.AVERAGE_SLEEP)));
                        mFUser.setAverageStep(query.getInt(query.getColumnIndex(com.portfolio.platform.data.model.MFUser.AVERAGE_STEP)));
                        java.lang.String b2 = this.f21317e.f21316a.this$0.TAG;
                        java.lang.String str58 = str56;
                        com.misfit.frameworks.common.log.MFLogger.m31689d(b2, "Add user=" + mFUser + " to migration user list");
                        arrayList3.add(mFUser);
                        query.moveToNext();
                        str21 = str55;
                        str18 = str40;
                        str19 = str38;
                        str20 = str36;
                        str23 = str23;
                        str26 = str57;
                        str33 = str42;
                        str22 = str58;
                        str32 = str44;
                        str31 = str46;
                        str30 = str48;
                        str29 = str50;
                        str28 = str52;
                        str27 = str53;
                    }
                    str4 = str20;
                    str = str23;
                    str5 = str19;
                    str3 = str22;
                    str14 = str26;
                    str2 = str27;
                    str12 = str28;
                    str11 = str29;
                    str10 = str30;
                    str9 = str31;
                    str8 = str32;
                    str7 = str33;
                    aVar = this;
                    str13 = str21;
                    str6 = str18;
                    arrayList = arrayList3;
                    query.close();
                } else {
                    str4 = str20;
                    str = str23;
                    str5 = str19;
                    str3 = str22;
                    str14 = str26;
                    str2 = str27;
                    str12 = str28;
                    str11 = str29;
                    str10 = str30;
                    str9 = str31;
                    str8 = str32;
                    str7 = str33;
                    aVar = this;
                    str13 = str21;
                    str6 = str18;
                    arrayList = arrayList3;
                }
                aVar.f21318f.execSQL("CREATE TABLE user_copy (uid VARCHAR PRIMARY KEY, userAccessToken VARCHAR, refreshToken VARCHAR, accessTokenExpiresAt VARCHAR, createdAt VARCHAR, updatedAt INTEGER, email INTEGER, authType INTEGER,username VARCHAR, activeDeviceId INTEGER, firstName VARCHAR, lastName VARCHAR, weightInGrams INTEGER, heightInCentimeters INTEGER, heightUnit INTEGER, weightUnit INTEGER,distanceUnit VARCHAR, birthday VARCHAR, gender INTEGER, profilePicture VARCHAR, brand VARCHAR, registrationComplete INTEGER, isOnboardingComplete INTEGER, integrations INTEGER, emailOptIn INTEGER, diagnosticEnabled INTEGER, registerDate INTEGER, pinType VARCHAR, averageSleep INTEGER, averageStep INTEGER);");
                if (!arrayList.isEmpty()) {
                    java.util.Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        com.portfolio.platform.data.model.MFUser mFUser2 = (com.portfolio.platform.data.model.MFUser) it.next();
                        android.content.ContentValues contentValues = new android.content.ContentValues();
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) mFUser2, "user");
                        contentValues.put("uid", mFUser2.getUserId());
                        contentValues.put(com.portfolio.platform.data.model.MFUser.USER_ACCESS_TOKEN, mFUser2.getUserAccessToken());
                        contentValues.put("refreshToken", mFUser2.getRefreshToken());
                        contentValues.put(com.portfolio.platform.data.model.MFUser.ACCESS_TOKEN_EXPIRED_AT, query.getString(query.getColumnIndex(com.portfolio.platform.data.model.MFUser.ACCESS_TOKEN_EXPIRED_AT)));
                        contentValues.put("createdAt", mFUser2.getCreatedAt());
                        contentValues.put("updatedAt", mFUser2.getUpdatedAt());
                        contentValues.put("email", mFUser2.getEmail());
                        com.portfolio.platform.enums.AuthType authType = mFUser2.getAuthType();
                        java.util.Iterator it2 = it;
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) authType, "user.authType");
                        contentValues.put("authType", authType.getValue());
                        contentValues.put("username", mFUser2.getUsername());
                        contentValues.put("activeDeviceId", mFUser2.getActiveDeviceId());
                        java.lang.String str59 = str;
                        contentValues.put(str59, mFUser2.getFirstName());
                        android.database.Cursor cursor = query;
                        contentValues.put(str13, mFUser2.getLastName());
                        contentValues.put(str4, java.lang.Integer.valueOf(mFUser2.getWeightInGrams()));
                        java.lang.String str60 = str5;
                        contentValues.put(str60, java.lang.Integer.valueOf(mFUser2.getHeightInCentimeters()));
                        if (mFUser2.getHeightUnit() != null) {
                            unit = mFUser2.getHeightUnit();
                            str17 = str60;
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) unit, "user.heightUnit");
                        } else {
                            str17 = str60;
                            unit = com.portfolio.platform.enums.Unit.METRIC;
                        }
                        java.lang.String str61 = str6;
                        contentValues.put(str61, unit.getValue());
                        if (mFUser2.getWeightUnit() != null) {
                            unit2 = mFUser2.getWeightUnit();
                            str16 = str61;
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) unit2, "user.weightUnit");
                        } else {
                            str16 = str61;
                            unit2 = com.portfolio.platform.enums.Unit.METRIC;
                        }
                        java.lang.String str62 = str7;
                        contentValues.put(str62, unit2.getValue());
                        if (mFUser2.getDistanceUnit() != null) {
                            unit3 = mFUser2.getDistanceUnit();
                            str15 = str62;
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) unit3, "user.distanceUnit");
                        } else {
                            str15 = str62;
                            unit3 = com.portfolio.platform.enums.Unit.METRIC;
                        }
                        contentValues.put(str8, unit3.getValue());
                        java.lang.String str63 = str10;
                        contentValues.put(str63, mFUser2.getBirthday());
                        java.lang.String str64 = str9;
                        java.lang.String str65 = str63;
                        java.lang.String str66 = str64;
                        contentValues.put(str66, mFUser2.getGender() != null ? mFUser2.getGender().toString() : "");
                        java.lang.String str67 = str66;
                        contentValues.put(str11, mFUser2.getProfilePicture());
                        contentValues.put(str12, mFUser2.getBrand());
                        contentValues.put(str2, java.lang.Integer.valueOf(mFUser2.isRegistrationComplete() ? 1 : 0));
                        contentValues.put(str14, java.lang.Integer.valueOf(mFUser2.isOnboardingComplete() ? 1 : 0));
                        contentValues.put(str25, mFUser2.getIntegrationsRaw());
                        contentValues.put(str24, java.lang.Integer.valueOf(mFUser2.isEmailOptIn() ? 1 : 0));
                        contentValues.put(str3, java.lang.Integer.valueOf(mFUser2.isDiagnosticEnabled() ? 1 : 0));
                        contentValues.put("registerDate", mFUser2.getRegisterDate());
                        contentValues.put("pinType", mFUser2.getPinType());
                        contentValues.put(com.portfolio.platform.data.model.MFUser.AVERAGE_SLEEP, java.lang.Integer.valueOf(mFUser2.getAverageSleep()));
                        contentValues.put(com.portfolio.platform.data.model.MFUser.AVERAGE_STEP, java.lang.Integer.valueOf(mFUser2.getAverageStep()));
                        aVar.f21318f.insert("user_copy", (java.lang.String) null, contentValues);
                        str = str59;
                        it = it2;
                        query = cursor;
                        java.lang.String str68 = str65;
                        str9 = str67;
                        str10 = str68;
                    }
                }
                aVar.f21318f.execSQL("DROP TABLE user;");
                aVar.f21318f.execSQL("ALTER TABLE user_copy RENAME TO user;");
            }
        }

        @DexIgnore
        public C5989b(com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1 userProviderImp$getDbUpgrades$1) {
            this.f21316a = userProviderImp$getDbUpgrades$1;
        }

        @DexIgnore
        public final void execute(android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
            com.fossil.wearables.fsl.shared.DatabaseHelper a = this.f21316a.this$0.databaseHelper;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "databaseHelper");
            com.j256.ormlite.misc.TransactionManager.callInTransaction(a.getConnectionSource(), new com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1.C5989b.C5990a(this, sQLiteDatabase));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1$c")
    /* renamed from: com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1$c */
    public static final class C5991c implements com.fossil.wearables.fsl.shared.UpgradeCommand {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1 f21319a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1$c$a")
        /* renamed from: com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1$c$a */
        public static final class C5992a<V> implements java.util.concurrent.Callable<T> {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1.C5991c f21320e;

            @DexIgnore
            /* renamed from: f */
            public /* final */ /* synthetic */ android.database.sqlite.SQLiteDatabase f21321f;

            @DexIgnore
            public C5992a(com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1.C5991c cVar, android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
                this.f21320e = cVar;
                this.f21321f = sQLiteDatabase;
            }

            @DexIgnore
            public final void call() {
                java.lang.String str;
                java.lang.String str2;
                java.lang.String str3;
                java.lang.String str4;
                java.lang.String str5;
                java.lang.String str6;
                java.lang.String str7;
                java.lang.String str8;
                java.lang.String str9;
                java.lang.String str10;
                java.util.ArrayList arrayList;
                com.portfolio.platform.enums.Unit unit;
                com.portfolio.platform.enums.Unit unit2;
                com.portfolio.platform.enums.Unit unit3;
                java.lang.String str11;
                java.lang.String str12;
                java.lang.String str13;
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String b = this.f21320e.f21319a.this$0.TAG;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) b, "TAG");
                local.mo33255d(b, "start migration from db version 4");
                android.database.Cursor query = this.f21321f.query(true, "user", new java.lang.String[]{com.portfolio.platform.data.model.MFUser.USER_ACCESS_TOKEN, "refreshToken", com.portfolio.platform.data.model.MFUser.ACCESS_TOKEN_EXPIRED_AT, "uid", "createdAt", "updatedAt", "email", "authType", "username", "activeDeviceId", "firstName", "lastName", "weightInGrams", "heightInCentimeters", com.portfolio.platform.data.model.MFUser.HEIGHT_UNIT, com.portfolio.platform.data.model.MFUser.WEIGHT_UNIT, com.portfolio.platform.data.model.MFUser.DISTANCE_UNIT, "gender", "birthday", "profilePicture", "brand", "registrationComplete", "isOnboardingComplete", "integrations", "emailOptIn", "diagnosticEnabled", "pinType", "registerDate", com.portfolio.platform.data.model.MFUser.AVERAGE_STEP, com.portfolio.platform.data.model.MFUser.AVERAGE_SLEEP}, (java.lang.String) null, (java.lang.String[]) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null);
                java.util.ArrayList arrayList2 = new java.util.ArrayList();
                java.lang.String str14 = com.portfolio.platform.data.model.MFUser.HEIGHT_UNIT;
                java.lang.String str15 = "heightInCentimeters";
                java.lang.String str16 = "weightInGrams";
                java.lang.String str17 = "username";
                java.util.ArrayList arrayList3 = arrayList2;
                java.lang.String str18 = "authType";
                java.lang.String str19 = "registrationComplete";
                java.lang.String str20 = "email";
                java.lang.String str21 = "brand";
                java.lang.String str22 = "profilePicture";
                java.lang.String str23 = "birthday";
                java.lang.String str24 = "gender";
                java.lang.String str25 = com.portfolio.platform.data.model.MFUser.DISTANCE_UNIT;
                java.lang.String str26 = com.portfolio.platform.data.model.MFUser.WEIGHT_UNIT;
                if (query != null) {
                    query.moveToFirst();
                    while (!query.isAfterLast()) {
                        java.lang.String str27 = str14;
                        com.portfolio.platform.data.model.MFUser mFUser = new com.portfolio.platform.data.model.MFUser();
                        mFUser.setUserAccessToken(query.getString(query.getColumnIndex(com.portfolio.platform.data.model.MFUser.USER_ACCESS_TOKEN)));
                        mFUser.setRefreshToken(query.getString(query.getColumnIndex("refreshToken")));
                        mFUser.setAccessTokenExpiresAt(query.getString(query.getColumnIndex(com.portfolio.platform.data.model.MFUser.ACCESS_TOKEN_EXPIRED_AT)));
                        mFUser.setUserId(query.getString(query.getColumnIndex("uid")));
                        mFUser.setCreatedAt(query.getString(query.getColumnIndex("createdAt")));
                        mFUser.setUpdatedAt(query.getString(query.getColumnIndex("updatedAt")));
                        mFUser.setEmail(query.getString(query.getColumnIndex(str20)));
                        mFUser.setAuthType(query.getString(query.getColumnIndex(str18)));
                        mFUser.setUsername(query.getString(query.getColumnIndex(str17)));
                        mFUser.setActiveDeviceId(query.getString(query.getColumnIndex("activeDeviceId")));
                        mFUser.setFirstName(query.getString(query.getColumnIndex("firstName")));
                        mFUser.setLastName(query.getString(query.getColumnIndex("lastName")));
                        mFUser.setWeightInGrams(query.getInt(query.getColumnIndex(str16)));
                        java.lang.String str28 = str15;
                        java.lang.String str29 = str16;
                        mFUser.setHeightInCentimeters(query.getInt(query.getColumnIndex(str28)));
                        java.lang.String str30 = str27;
                        java.lang.String str31 = str28;
                        mFUser.setHeightUnit(query.getString(query.getColumnIndex(str30)));
                        java.lang.String str32 = str26;
                        java.lang.String str33 = str30;
                        mFUser.setWeightUnit(query.getString(query.getColumnIndex(str32)));
                        java.lang.String str34 = str25;
                        java.lang.String str35 = str32;
                        mFUser.setDistanceUnit(query.getString(query.getColumnIndex(str34)));
                        java.lang.String str36 = str24;
                        java.lang.String str37 = str34;
                        mFUser.setGender(query.getString(query.getColumnIndex(str36)));
                        java.lang.String str38 = str23;
                        java.lang.String str39 = str36;
                        mFUser.setBirthday(query.getString(query.getColumnIndex(str38)));
                        java.lang.String str40 = str22;
                        java.lang.String str41 = str38;
                        mFUser.setProfilePicture(query.getString(query.getColumnIndex(str40)));
                        java.lang.String str42 = str21;
                        java.lang.String str43 = str40;
                        mFUser.setBrand(query.getString(query.getColumnIndex(str42)));
                        java.lang.String str44 = str19;
                        java.lang.String str45 = str42;
                        java.lang.String str46 = str44;
                        mFUser.setRegistrationComplete(query.getInt(query.getColumnIndex(str44)) == 1);
                        mFUser.setOnboardingComplete(query.getInt(query.getColumnIndex("isOnboardingComplete")) == 1);
                        mFUser.setIntegrations(query.getString(query.getColumnIndex("integrations")));
                        mFUser.setEmailOptIn(query.getInt(query.getColumnIndex("emailOptIn")) == 1);
                        mFUser.setDiagnosticEnabled(query.getInt(query.getColumnIndex("diagnosticEnabled")) == 1);
                        mFUser.setRegisterDate(query.getString(query.getColumnIndex("registerDate")));
                        mFUser.setPinType(query.getString(query.getColumnIndex("pinType")));
                        mFUser.setAverageSleep(query.getInt(query.getColumnIndex(com.portfolio.platform.data.model.MFUser.AVERAGE_SLEEP)));
                        mFUser.setAverageStep(query.getInt(query.getColumnIndex(com.portfolio.platform.data.model.MFUser.AVERAGE_STEP)));
                        arrayList3.add(mFUser);
                        query.moveToNext();
                        str14 = str33;
                        str15 = str31;
                        str16 = str29;
                        str26 = str35;
                        str25 = str37;
                        str24 = str39;
                        str23 = str41;
                        str22 = str43;
                        str21 = str45;
                        str19 = str46;
                    }
                    str3 = str15;
                    str2 = str16;
                    arrayList = arrayList3;
                    str = str19;
                    str10 = str21;
                    str9 = str22;
                    str8 = str23;
                    str7 = str24;
                    str6 = str25;
                    str5 = str26;
                    str4 = str14;
                    query.close();
                } else {
                    str3 = str15;
                    str2 = str16;
                    arrayList = arrayList3;
                    str = str19;
                    str10 = str21;
                    str9 = str22;
                    str8 = str23;
                    str7 = str24;
                    str6 = str25;
                    str5 = str26;
                    str4 = str14;
                }
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append("CREATE TABLE user_copy (uid VARCHAR PRIMARY KEY, userAccessToken VARCHAR, refreshToken VARCHAR, accessTokenExpiresAt VARCHAR, accessTokenExpiresIn INTEGER, createdAt VARCHAR, updatedAt INTEGER, email INTEGER, authType INTEGER,username VARCHAR, activeDeviceId INTEGER, firstName VARCHAR, lastName VARCHAR, weightInGrams INTEGER, heightInCentimeters INTEGER, heightUnit INTEGER, weightUnit INTEGER,distanceUnit VARCHAR, birthday VARCHAR, gender INTEGER, profilePicture VARCHAR, brand VARCHAR, registrationComplete INTEGER, isOnboardingComplete INTEGER, integrations INTEGER, emailOptIn INTEGER, diagnosticEnabled INTEGER, registerDate INTEGER, pinType VARCHAR, averageSleep INTEGER, averageStep INTEGER, temperatureUnit VARCHAR DEFAULT '");
                sb.append(com.portfolio.platform.enums.Unit.METRIC.getValue());
                sb.append("', ");
                sb.append(com.portfolio.platform.data.model.MFUser.USE_DEFAULT_GOALS);
                sb.append(" INTEGER DEFAULT 0, ");
                sb.append(com.portfolio.platform.data.model.MFUser.USE_DEFAULT_BIOMETRIC);
                sb.append(" INTEGER DEFAULT 0, ");
                sb.append("home");
                sb.append(" VARCHAR, ");
                sb.append("work");
                java.lang.String str47 = "work";
                sb.append(" VARCHAR");
                sb.append(");");
                java.lang.String str48 = "home";
                this.f21321f.execSQL(sb.toString());
                if (!arrayList.isEmpty()) {
                    java.util.Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        com.portfolio.platform.data.model.MFUser mFUser2 = (com.portfolio.platform.data.model.MFUser) it.next();
                        android.content.ContentValues contentValues = new android.content.ContentValues();
                        java.util.Iterator it2 = it;
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) mFUser2, "user");
                        contentValues.put("uid", mFUser2.getUserId());
                        contentValues.put(com.portfolio.platform.data.model.MFUser.USER_ACCESS_TOKEN, mFUser2.getUserAccessToken());
                        contentValues.put(com.portfolio.platform.data.model.MFUser.ACCESS_TOKEN_EXPIRED_AT, mFUser2.getAccessTokenExpiresAt());
                        contentValues.put("refreshToken", mFUser2.getRefreshToken());
                        contentValues.put("createdAt", mFUser2.getCreatedAt());
                        contentValues.put("updatedAt", mFUser2.getUpdatedAt());
                        contentValues.put(str20, mFUser2.getEmail());
                        com.portfolio.platform.enums.AuthType authType = mFUser2.getAuthType();
                        java.lang.String str49 = str20;
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) authType, "user.authType");
                        contentValues.put(str18, authType.getValue());
                        contentValues.put(str17, mFUser2.getUsername());
                        contentValues.put("activeDeviceId", mFUser2.getActiveDeviceId());
                        contentValues.put("firstName", mFUser2.getFirstName());
                        contentValues.put("lastName", mFUser2.getLastName());
                        java.lang.String str50 = str2;
                        contentValues.put(str50, java.lang.Integer.valueOf(mFUser2.getWeightInGrams()));
                        java.lang.String str51 = str17;
                        java.lang.String str52 = str3;
                        contentValues.put(str52, java.lang.Integer.valueOf(mFUser2.getHeightInCentimeters()));
                        if (mFUser2.getHeightUnit() != null) {
                            unit = mFUser2.getHeightUnit();
                            str13 = str52;
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) unit, "user.heightUnit");
                        } else {
                            str13 = str52;
                            unit = com.portfolio.platform.enums.Unit.METRIC;
                        }
                        java.lang.String value = unit.getValue();
                        java.lang.String str53 = str4;
                        contentValues.put(str53, value);
                        if (mFUser2.getWeightUnit() != null) {
                            unit2 = mFUser2.getWeightUnit();
                            str12 = str53;
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) unit2, "user.weightUnit");
                        } else {
                            str12 = str53;
                            unit2 = com.portfolio.platform.enums.Unit.METRIC;
                        }
                        java.lang.String str54 = str5;
                        contentValues.put(str54, unit2.getValue());
                        if (mFUser2.getDistanceUnit() != null) {
                            unit3 = mFUser2.getDistanceUnit();
                            str11 = str54;
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) unit3, "user.distanceUnit");
                        } else {
                            str11 = str54;
                            unit3 = com.portfolio.platform.enums.Unit.METRIC;
                        }
                        contentValues.put(str6, unit3.getValue());
                        contentValues.put(str8, mFUser2.getBirthday());
                        java.lang.String str55 = str7;
                        java.lang.String str56 = str18;
                        java.lang.String str57 = str55;
                        contentValues.put(str57, mFUser2.getGender() != null ? mFUser2.getGender().toString() : "");
                        java.lang.String str58 = str57;
                        contentValues.put(str9, mFUser2.getProfilePicture());
                        contentValues.put(str10, mFUser2.getBrand());
                        contentValues.put(str, java.lang.Integer.valueOf(mFUser2.isRegistrationComplete() ? 1 : 0));
                        contentValues.put("isOnboardingComplete", java.lang.Integer.valueOf(mFUser2.isOnboardingComplete() ? 1 : 0));
                        contentValues.put("integrations", mFUser2.getIntegrationsRaw());
                        contentValues.put("emailOptIn", java.lang.Integer.valueOf(mFUser2.isEmailOptIn() ? 1 : 0));
                        contentValues.put("diagnosticEnabled", java.lang.Integer.valueOf(mFUser2.isDiagnosticEnabled() ? 1 : 0));
                        contentValues.put("registerDate", mFUser2.getRegisterDate());
                        contentValues.put("pinType", mFUser2.getPinType());
                        contentValues.put(com.portfolio.platform.data.model.MFUser.AVERAGE_SLEEP, java.lang.Integer.valueOf(mFUser2.getAverageSleep()));
                        contentValues.put(com.portfolio.platform.data.model.MFUser.AVERAGE_STEP, java.lang.Integer.valueOf(mFUser2.getAverageStep()));
                        contentValues.put(str48, "");
                        contentValues.put(str47, "");
                        contentValues.put(com.portfolio.platform.data.model.MFUser.ACCESS_TOKEN_EXPIRED_IN, 86400);
                        this.f21321f.insert("user_copy", (java.lang.String) null, contentValues);
                        it = it2;
                        str18 = str56;
                        str17 = str51;
                        str7 = str58;
                        str2 = str50;
                        str20 = str49;
                    }
                }
                this.f21321f.execSQL("DROP TABLE user;");
                this.f21321f.execSQL("ALTER TABLE user_copy RENAME TO user;");
            }
        }

        @DexIgnore
        public C5991c(com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1 userProviderImp$getDbUpgrades$1) {
            this.f21319a = userProviderImp$getDbUpgrades$1;
        }

        @DexIgnore
        public final void execute(android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
            com.fossil.wearables.fsl.shared.DatabaseHelper a = this.f21319a.this$0.databaseHelper;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "databaseHelper");
            com.j256.ormlite.misc.TransactionManager.callInTransaction(a.getConnectionSource(), new com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1.C5991c.C5992a(this, sQLiteDatabase));
        }
    }

    @DexIgnore
    public UserProviderImp$getDbUpgrades$1(com.portfolio.platform.provider.UserProviderImp userProviderImp) {
        this.this$0 = userProviderImp;
        put(2, new com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1.C5987a(this));
        put(3, new com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1.C5989b(this));
        put(4, new com.portfolio.platform.provider.UserProviderImp$getDbUpgrades$1.C5991c(this));
    }

    @DexIgnore
    public /* bridge */ boolean containsKey(java.lang.Integer num) {
        return super.containsKey(num);
    }

    @DexIgnore
    public /* bridge */ boolean containsValue(com.fossil.wearables.fsl.shared.UpgradeCommand upgradeCommand) {
        return super.containsValue(upgradeCommand);
    }

    @DexIgnore
    public final /* bridge */ java.util.Set<java.util.Map.Entry<java.lang.Integer, com.fossil.wearables.fsl.shared.UpgradeCommand>> entrySet() {
        return getEntries();
    }

    @DexIgnore
    public /* bridge */ com.fossil.wearables.fsl.shared.UpgradeCommand get(java.lang.Integer num) {
        return (com.fossil.wearables.fsl.shared.UpgradeCommand) super.get(num);
    }

    @DexIgnore
    public /* bridge */ java.util.Set getEntries() {
        return super.entrySet();
    }

    @DexIgnore
    public /* bridge */ java.util.Set getKeys() {
        return super.keySet();
    }

    @DexIgnore
    public /* bridge */ com.fossil.wearables.fsl.shared.UpgradeCommand getOrDefault(java.lang.Integer num, com.fossil.wearables.fsl.shared.UpgradeCommand upgradeCommand) {
        return (com.fossil.wearables.fsl.shared.UpgradeCommand) super.getOrDefault(num, upgradeCommand);
    }

    @DexIgnore
    public /* bridge */ int getSize() {
        return super.size();
    }

    @DexIgnore
    public /* bridge */ java.util.Collection getValues() {
        return super.values();
    }

    @DexIgnore
    public final /* bridge */ java.util.Set<java.lang.Integer> keySet() {
        return getKeys();
    }

    @DexIgnore
    public /* bridge */ com.fossil.wearables.fsl.shared.UpgradeCommand remove(java.lang.Integer num) {
        return (com.fossil.wearables.fsl.shared.UpgradeCommand) super.remove(num);
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return getSize();
    }

    @DexIgnore
    public final /* bridge */ java.util.Collection<com.fossil.wearables.fsl.shared.UpgradeCommand> values() {
        return getValues();
    }

    @DexIgnore
    public final /* bridge */ boolean containsKey(java.lang.Object obj) {
        if (obj instanceof java.lang.Integer) {
            return containsKey((java.lang.Integer) obj);
        }
        return false;
    }

    @DexIgnore
    public final /* bridge */ boolean containsValue(java.lang.Object obj) {
        if (obj instanceof com.fossil.wearables.fsl.shared.UpgradeCommand) {
            return containsValue((com.fossil.wearables.fsl.shared.UpgradeCommand) obj);
        }
        return false;
    }

    @DexIgnore
    public final /* bridge */ java.lang.Object get(java.lang.Object obj) {
        if (obj instanceof java.lang.Integer) {
            return get((java.lang.Integer) obj);
        }
        return null;
    }

    @DexIgnore
    public final /* bridge */ java.lang.Object getOrDefault(java.lang.Object obj, java.lang.Object obj2) {
        return obj != null ? obj instanceof java.lang.Integer : true ? getOrDefault((java.lang.Integer) obj, (com.fossil.wearables.fsl.shared.UpgradeCommand) obj2) : obj2;
    }

    @DexIgnore
    public final /* bridge */ java.lang.Object remove(java.lang.Object obj) {
        if (obj instanceof java.lang.Integer) {
            return remove((java.lang.Integer) obj);
        }
        return null;
    }

    @DexIgnore
    public /* bridge */ boolean remove(java.lang.Integer num, com.fossil.wearables.fsl.shared.UpgradeCommand upgradeCommand) {
        return super.remove(num, upgradeCommand);
    }

    @DexIgnore
    public final /* bridge */ boolean remove(java.lang.Object obj, java.lang.Object obj2) {
        boolean z = true;
        if (!(obj != null ? obj instanceof java.lang.Integer : true)) {
            return false;
        }
        if (obj2 != null) {
            z = obj2 instanceof com.fossil.wearables.fsl.shared.UpgradeCommand;
        }
        if (z) {
            return remove((java.lang.Integer) obj, (com.fossil.wearables.fsl.shared.UpgradeCommand) obj2);
        }
        return false;
    }
}
