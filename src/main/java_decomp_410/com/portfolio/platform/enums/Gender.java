package com.portfolio.platform.enums;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.qf4;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum Gender {
    FEMALE(DeviceIdentityUtils.FLASH_SERIAL_NUMBER_PREFIX),
    MALE("M"),
    OTHER("O");
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String value;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final Gender a(String str) {
            if (TextUtils.isEmpty(str)) {
                return Gender.OTHER;
            }
            for (Gender gender : Gender.values()) {
                if (qf4.b(str, gender.getValue(), true)) {
                    return gender;
                }
            }
            return Gender.OTHER;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    Gender(String str) {
        this.value = str;
    }

    @DexIgnore
    public final String getValue() {
        return this.value;
    }

    @DexIgnore
    public String toString() {
        return this.value;
    }
}
