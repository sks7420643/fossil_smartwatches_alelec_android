package com.portfolio.platform.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum FossilBrand {
    PORTFOLIO(0, "Diana"),
    KATESPADE(1, "Kate Spade"),
    MICHAELKORS(2, "Micheal Kors"),
    DIESEL(3, "Diesel"),
    SKAGEN(4, " Skagen"),
    CHAPS(5, "Chaps"),
    EA(6, "Emporio Armani"),
    AX(7, "Armani Exchange"),
    MJ(8, "Marc Jacobs"),
    RELIC(9, "Relic"),
    FOSSIL(10, "Fossil"),
    UNIVERSAL(11, "Universal"),
    CITIZEN(11, "Citizen");
    
    @DexIgnore
    public /* final */ String name;
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    FossilBrand(int i, String str) {
        this.value = i;
        this.name = str;
    }

    @DexIgnore
    public static FossilBrand fromInt(int i) {
        for (FossilBrand fossilBrand : values()) {
            if (fossilBrand.value == i) {
                return fossilBrand;
            }
        }
        return PORTFOLIO;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }
}
