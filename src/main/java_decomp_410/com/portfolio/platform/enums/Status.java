package com.portfolio.platform.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum Status {
    SUCCESS,
    ERROR,
    DATABASE_LOADING,
    NETWORK_LOADING
}
