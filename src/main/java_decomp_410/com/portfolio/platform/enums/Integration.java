package com.portfolio.platform.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum Integration {
    Jawbone("jawbone"),
    UnderArmour("underarmour"),
    HealthKit("healthkit"),
    GoogleFit("googlefit");
    
    @DexIgnore
    public /* final */ String name;

    @DexIgnore
    Integration(String str) {
        this.name = str;
    }

    @DexIgnore
    public static Integration fromName(String str) {
        for (Integration integration : values()) {
            if (integration.name.equalsIgnoreCase(str)) {
                return integration;
            }
        }
        return null;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }
}
