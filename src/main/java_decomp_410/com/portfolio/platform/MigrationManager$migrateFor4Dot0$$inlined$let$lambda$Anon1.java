package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.LegacyGoalTrackingSettings;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MigrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ LegacyGoalTrackingSettings $it;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MigrationManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MigrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1(LegacyGoalTrackingSettings legacyGoalTrackingSettings, yb4 yb4, MigrationManager migrationManager) {
        super(2, yb4);
        this.$it = legacyGoalTrackingSettings;
        this.this$Anon0 = migrationManager;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MigrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1 migrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1 = new MigrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1(this.$it, yb4, this.this$Anon0);
        migrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return migrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MigrationManager$migrateFor4Dot0$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MigrationManager.n.a();
            local.d(a2, "Migrate goal tracking with target " + this.$it.getTarget() + " value " + this.$it.getValue());
            this.this$Anon0.g.getGoalTrackingDao().upsertGoalSettings(new GoalSetting(this.$it.getTarget()));
            ArrayList arrayList = new ArrayList();
            int value = this.$it.getValue();
            for (int i2 = 0; i2 < value; i2++) {
                String uuid = UUID.randomUUID().toString();
                kd4.a((Object) uuid, "UUID.randomUUID().toString()");
                Date date = new Date();
                TimeZone timeZone = TimeZone.getDefault();
                kd4.a((Object) timeZone, "TimeZone.getDefault()");
                DateTime a3 = rk2.a(date, timeZone.getRawOffset() / 1000);
                kd4.a((Object) a3, "DateHelper.createDateTim\u2026fault().rawOffset / 1000)");
                TimeZone timeZone2 = TimeZone.getDefault();
                kd4.a((Object) timeZone2, "TimeZone.getDefault()");
                GoalTrackingData goalTrackingData = r7;
                GoalTrackingData goalTrackingData2 = new GoalTrackingData(uuid, a3, timeZone2.getRawOffset() / 1000, new Date(), new Date().getTime(), new Date().getTime());
                arrayList.add(goalTrackingData);
            }
            GoalTrackingRepository d = this.this$Anon0.f;
            List d2 = kb4.d(arrayList);
            this.L$Anon0 = zg4;
            this.L$Anon1 = arrayList;
            this.label = 1;
            if (d.insertFromDevice(d2, this) == a) {
                return a;
            }
        } else if (i == 1) {
            ArrayList arrayList2 = (ArrayList) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
