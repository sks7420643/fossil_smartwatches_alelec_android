package com.portfolio.platform;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.PortfolioApp$executeSetLocalization$1", mo27670f = "PortfolioApp.kt", mo27671l = {1203}, mo27672m = "invokeSuspend")
public final class PortfolioApp$executeSetLocalization$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20984p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.PortfolioApp this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PortfolioApp$executeSetLocalization$1(com.portfolio.platform.PortfolioApp portfolioApp, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = portfolioApp;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.PortfolioApp$executeSetLocalization$1 portfolioApp$executeSetLocalization$1 = new com.portfolio.platform.PortfolioApp$executeSetLocalization$1(this.this$0, yb4);
        portfolioApp$executeSetLocalization$1.f20984p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return portfolioApp$executeSetLocalization$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.PortfolioApp$executeSetLocalization$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f20984p$;
            com.portfolio.platform.data.source.WatchLocalizationRepository t = this.this$0.mo34573t();
            this.L$0 = zg4;
            this.label = 1;
            obj = t.getWatchLocalizationFromServer(true, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        java.lang.String str = (java.lang.String) obj;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String d = com.portfolio.platform.PortfolioApp.f20941W.mo34591d();
        local.mo33255d(d, "path of localization file: " + str);
        if (str != null) {
            com.misfit.frameworks.buttonservice.IButtonConnectivity b = com.portfolio.platform.PortfolioApp.f20941W.mo34585b();
            if (b != null) {
                b.setLocalizationData(new com.misfit.frameworks.buttonservice.model.LocalizationData(str, (java.lang.String) null, 2, (com.fossil.blesdk.obfuscated.fd4) null));
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
