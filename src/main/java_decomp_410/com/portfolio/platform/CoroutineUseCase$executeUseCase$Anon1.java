package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.CoroutineUseCase$executeUseCase$Anon1", f = "CoroutineUseCase.kt", l = {35}, m = "invokeSuspend")
public final class CoroutineUseCase$executeUseCase$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ CoroutineUseCase.e $callBack;
    @DexIgnore
    public /* final */ /* synthetic */ CoroutineUseCase.b $requestValues;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CoroutineUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CoroutineUseCase$executeUseCase$Anon1(CoroutineUseCase coroutineUseCase, CoroutineUseCase.e eVar, CoroutineUseCase.b bVar, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = coroutineUseCase;
        this.$callBack = eVar;
        this.$requestValues = bVar;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CoroutineUseCase$executeUseCase$Anon1 coroutineUseCase$executeUseCase$Anon1 = new CoroutineUseCase$executeUseCase$Anon1(this.this$Anon0, this.$callBack, this.$requestValues, yb4);
        coroutineUseCase$executeUseCase$Anon1.p$ = (zg4) obj;
        return coroutineUseCase$executeUseCase$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CoroutineUseCase$executeUseCase$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            this.this$Anon0.a = this.$callBack;
            CoroutineUseCase coroutineUseCase = this.this$Anon0;
            coroutineUseCase.b = coroutineUseCase.c();
            FLogger.INSTANCE.getLocal().d(this.this$Anon0.b, "Start UseCase");
            CoroutineUseCase coroutineUseCase2 = this.this$Anon0;
            CoroutineUseCase.b bVar = this.$requestValues;
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = coroutineUseCase2.a(bVar, (yb4<Object>) this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (obj instanceof CoroutineUseCase.d) {
            this.this$Anon0.a((CoroutineUseCase.d) obj);
        } else if (obj instanceof CoroutineUseCase.a) {
            this.this$Anon0.a((CoroutineUseCase.a) obj);
        }
        return qa4.a;
    }
}
