package com.portfolio.platform.view.indicator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kl2;
import com.fossil.blesdk.obfuscated.yt3;
import com.fossil.wearables.fossil.R;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class CirclePageIndicator extends View implements yt3 {
    @DexIgnore
    public float e;
    @DexIgnore
    public /* final */ Paint f;
    @DexIgnore
    public /* final */ Paint g;
    @DexIgnore
    public /* final */ Paint h;
    @DexIgnore
    public RecyclerView i;
    @DexIgnore
    public ViewPager j;
    @DexIgnore
    public ViewPager.i k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public float n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public float s;
    @DexIgnore
    public int t;
    @DexIgnore
    public float u;
    @DexIgnore
    public int v;
    @DexIgnore
    public boolean w;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.q {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            super.onScrolled(recyclerView, i, i2);
            int G = ((LinearLayoutManager) CirclePageIndicator.this.i.getLayoutManager()).G();
            CirclePageIndicator circlePageIndicator = CirclePageIndicator.this;
            if (G == -1) {
                G = circlePageIndicator.l;
            } else if (kl2.a(circlePageIndicator.getContext())) {
                G = (CirclePageIndicator.this.i.getAdapter().getItemCount() - G) - 1;
            }
            circlePageIndicator.l = G;
            CirclePageIndicator circlePageIndicator2 = CirclePageIndicator.this;
            ViewPager.i iVar = circlePageIndicator2.k;
            if (iVar != null) {
                iVar.a(circlePageIndicator2.l);
            }
            CirclePageIndicator.this.invalidate();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends View.BaseSavedState {
        @DexIgnore
        public static /* final */ Parcelable.Creator<b> CREATOR; // = new a();
        @DexIgnore
        public int e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a implements Parcelable.Creator<b> {
            @DexIgnore
            public b createFromParcel(Parcel parcel) {
                return new b(parcel);
            }

            @DexIgnore
            public b[] newArray(int i) {
                return new b[i];
            }
        }

        @DexIgnore
        public b(Parcelable parcelable) {
            super(parcelable);
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.e);
        }

        @DexIgnore
        public b(Parcel parcel) {
            super(parcel);
            this.e = parcel.readInt();
        }
    }

    @DexIgnore
    public CirclePageIndicator(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public void a(ViewPager viewPager, int i2) {
        setViewPager(viewPager);
        setCurrentItem(i2);
    }

    @DexIgnore
    public void b(int i2) {
        this.o = i2;
        ViewPager.i iVar = this.k;
        if (iVar != null) {
            iVar.b(i2);
        }
    }

    @DexIgnore
    public final int c(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            return size;
        }
        ViewPager viewPager = this.j;
        if (viewPager == null) {
            return size;
        }
        int a2 = viewPager.getAdapter().a();
        float f2 = this.e;
        int paddingLeft = (int) (((float) (getPaddingLeft() + getPaddingRight())) + (((float) (a2 * 2)) * f2) + (((float) (a2 - 1)) * f2) + 1.0f);
        return mode == Integer.MIN_VALUE ? Math.min(paddingLeft, size) : paddingLeft;
    }

    @DexIgnore
    public final int d(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            return size;
        }
        int paddingTop = (int) ((this.e * 2.0f) + ((float) getPaddingTop()) + ((float) getPaddingBottom()) + 1.0f);
        return mode == Integer.MIN_VALUE ? Math.min(paddingTop, size) : paddingTop;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        float f2;
        float f3;
        super.onDraw(canvas);
        ViewPager viewPager = this.j;
        if (viewPager == null || viewPager.getAdapter() == null) {
            RecyclerView recyclerView = this.i;
            i2 = (recyclerView == null || recyclerView.getAdapter() == null) ? 0 : this.i.getAdapter().getItemCount();
        } else {
            i2 = this.j.getAdapter().a();
        }
        if (i2 != 0) {
            if (this.l >= i2) {
                setCurrentItem(i2 - 1);
                return;
            }
            if (this.p == 0) {
                i6 = getWidth();
                i5 = getPaddingLeft();
                i4 = getPaddingRight();
                i3 = getPaddingTop();
            } else {
                i6 = getHeight();
                i5 = getPaddingTop();
                i4 = getPaddingBottom();
                i3 = getPaddingLeft();
            }
            float f4 = this.e;
            float f5 = (f4 * 2.0f) + this.s;
            float f6 = ((float) i3) + f4;
            float f7 = ((float) i5) + f4;
            if (this.q) {
                f7 += (((float) ((i6 - i5) - i4)) / 2.0f) - ((((float) i2) * f5) / 2.0f);
            }
            float f8 = this.e;
            if (this.g.getStrokeWidth() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                f8 -= this.g.getStrokeWidth() / 2.0f;
            }
            for (int i7 = 0; i7 < i2; i7++) {
                float f9 = (((float) i7) * f5) + f7;
                if (this.p == 0) {
                    f3 = f6;
                } else {
                    f3 = f9;
                    f9 = f6;
                }
                if (this.f.getAlpha() > 0) {
                    canvas.drawCircle(f9, f3, f8, this.f);
                }
                float f10 = this.e;
                if (f8 != f10) {
                    canvas.drawCircle(f9, f3, f10, this.g);
                }
            }
            float f11 = ((float) (this.r ? this.m : this.l)) * f5;
            if (!this.r) {
                f11 += this.n * f5;
            }
            if (this.p == 0) {
                f2 = f11 + f7;
            } else {
                float f12 = f6;
                f6 = f11 + f7;
                f2 = f12;
            }
            canvas.drawCircle(f2, f6, this.e, this.h);
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (this.p == 0) {
            setMeasuredDimension(c(i2), d(i3));
        } else {
            setMeasuredDimension(d(i2), c(i3));
        }
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        b bVar = (b) parcelable;
        super.onRestoreInstanceState(bVar.getSuperState());
        int i2 = bVar.e;
        this.l = i2;
        this.m = i2;
        requestLayout();
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        b bVar = new b(super.onSaveInstanceState());
        bVar.e = this.l;
        return bVar;
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        ViewPager viewPager = this.j;
        int i2 = 0;
        if (viewPager == null || viewPager.getAdapter() == null || this.j.getAdapter().a() == 0) {
            return false;
        }
        int action = motionEvent.getAction() & 255;
        if (action != 0) {
            if (action != 1) {
                if (action == 2) {
                    float x = motionEvent.getX(motionEvent.findPointerIndex(this.v));
                    float f2 = x - this.u;
                    if (!this.w && Math.abs(f2) > ((float) this.t)) {
                        this.w = true;
                    }
                    if (this.w) {
                        this.u = x;
                        if (this.j.g() || this.j.a()) {
                            this.j.b(f2);
                        }
                    }
                } else if (action != 3) {
                    if (action == 5) {
                        int actionIndex = motionEvent.getActionIndex();
                        this.u = motionEvent.getX(actionIndex);
                        this.v = motionEvent.getPointerId(actionIndex);
                    } else if (action == 6) {
                        int actionIndex2 = motionEvent.getActionIndex();
                        if (motionEvent.getPointerId(actionIndex2) == this.v) {
                            if (actionIndex2 == 0) {
                                i2 = 1;
                            }
                            this.v = motionEvent.getPointerId(i2);
                        }
                        this.u = motionEvent.getX(motionEvent.findPointerIndex(this.v));
                    }
                }
            }
            if (!this.w) {
                int a2 = this.j.getAdapter().a();
                float width = (float) getWidth();
                float f3 = width / 2.0f;
                float f4 = width / 6.0f;
                if (this.l > 0 && motionEvent.getX() < f3 - f4) {
                    if (action != 3) {
                        this.j.setCurrentItem(this.l - 1);
                    }
                    return true;
                } else if (this.l < a2 - 1 && motionEvent.getX() > f3 + f4) {
                    if (action != 3) {
                        this.j.setCurrentItem(this.l + 1);
                    }
                    return true;
                }
            }
            this.w = false;
            this.v = -1;
            if (this.j.g()) {
                this.j.d();
            }
        } else {
            this.v = motionEvent.getPointerId(0);
            this.u = motionEvent.getX();
        }
        return true;
    }

    @DexIgnore
    public void setCurrentItem(int i2) {
        if (this.j == null && this.i == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        ViewPager viewPager = this.j;
        if (viewPager != null) {
            viewPager.setCurrentItem(i2);
            if (kl2.a(getContext())) {
                i2 = (this.j.getAdapter().a() - i2) - 1;
            }
            this.l = i2;
        } else {
            if (kl2.a(getContext())) {
                i2 = (this.i.getAdapter().getItemCount() - i2) - 1;
            }
            this.l = i2;
        }
        invalidate();
    }

    @DexIgnore
    public void setOnPageChangeListener(ViewPager.i iVar) {
        this.k = iVar;
    }

    @DexIgnore
    public void setViewPager(ViewPager viewPager) {
        if (!Objects.equals(this.j, viewPager)) {
            ViewPager viewPager2 = this.j;
            if (viewPager2 != null) {
                viewPager2.b((ViewPager.i) this);
            }
            if (viewPager.getAdapter() != null) {
                this.j = viewPager;
                this.j.a((ViewPager.i) this);
                invalidate();
                return;
            }
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
    }

    @DexIgnore
    public CirclePageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.vpiCirclePageIndicatorStyle);
    }

    @DexIgnore
    public CirclePageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f = new Paint(1);
        this.g = new Paint(1);
        this.h = new Paint(1);
        this.u = -1.0f;
        this.v = -1;
        if (!isInEditMode()) {
            Resources resources = getResources();
            int a2 = k6.a(context, (int) R.color.hex80A3A19E);
            int a3 = k6.a(context, (int) R.color.fossilOrange);
            int a4 = k6.a(context, (int) R.color.transparent);
            float dimension = resources.getDimension(R.dimen.dp6);
            float dimension2 = resources.getDimension(R.dimen.dp9);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.CirclePageIndicator, i2, 0);
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, h62.LinePageIndicator, i2, 0);
            this.q = obtainStyledAttributes.getBoolean(3, true);
            this.p = obtainStyledAttributes.getInt(0, 0);
            this.f.setStyle(Paint.Style.FILL);
            this.f.setColor(obtainStyledAttributes.getColor(4, a2));
            this.g.setStyle(Paint.Style.STROKE);
            this.g.setColor(obtainStyledAttributes.getColor(7, a4));
            this.g.setStrokeWidth(obtainStyledAttributes.getDimension(8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            this.h.setStyle(Paint.Style.FILL);
            this.h.setColor(obtainStyledAttributes.getColor(2, a3));
            this.e = obtainStyledAttributes.getDimension(5, dimension);
            this.r = obtainStyledAttributes.getBoolean(6, false);
            this.s = obtainStyledAttributes2.getDimension(1, dimension2);
            Drawable drawable = obtainStyledAttributes.getDrawable(1);
            if (drawable != null) {
                setBackground(drawable);
            }
            obtainStyledAttributes.recycle();
            obtainStyledAttributes2.recycle();
            this.t = ViewConfiguration.get(context).getScaledPagingTouchSlop();
        }
    }

    @DexIgnore
    public void a(RecyclerView recyclerView, int i2) {
        if (Objects.equals(this.i, recyclerView)) {
            return;
        }
        if (recyclerView.getAdapter() != null) {
            this.i = recyclerView;
            this.i.a((RecyclerView.q) new a());
            if (kl2.a(getContext())) {
                i2 = (this.i.getAdapter().getItemCount() - i2) - 1;
            }
            this.l = i2;
            invalidate();
            return;
        }
        throw new IllegalStateException("ViewPager does not have adapter instance.");
    }

    @DexIgnore
    public void a(int i2, float f2, int i3) {
        this.l = i2;
        this.n = f2;
        invalidate();
        ViewPager.i iVar = this.k;
        if (iVar != null) {
            iVar.a(i2, f2, i3);
        }
    }

    @DexIgnore
    public void a(int i2) {
        if (this.r || this.o == 0) {
            this.l = kl2.a(getContext()) ? (this.j.getAdapter().a() - i2) - 1 : i2;
            this.m = i2;
            invalidate();
        }
        ViewPager.i iVar = this.k;
        if (iVar != null) {
            iVar.a(i2);
        }
    }
}
