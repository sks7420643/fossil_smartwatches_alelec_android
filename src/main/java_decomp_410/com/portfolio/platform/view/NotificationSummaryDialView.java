package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.fossil.blesdk.obfuscated.bw;
import com.fossil.blesdk.obfuscated.ck2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fk2;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.hk2;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lv;
import com.fossil.blesdk.obfuscated.nk2;
import com.fossil.blesdk.obfuscated.oo;
import com.fossil.blesdk.obfuscated.pp;
import com.fossil.blesdk.obfuscated.qv;
import com.fossil.blesdk.obfuscated.rv;
import com.fossil.blesdk.obfuscated.ts3;
import com.fossil.blesdk.obfuscated.x9;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationSummaryDialView extends ViewGroup {
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public Paint j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;
    @DexIgnore
    public int o;
    @DexIgnore
    public ViewGroup.LayoutParams p;
    @DexIgnore
    public SparseArray<Bitmap> q; // = new SparseArray<>();
    @DexIgnore
    public /* final */ Handler r; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public Bitmap s;
    @DexIgnore
    public SparseArray<List<BaseFeatureModel>> t; // = new SparseArray<>();
    @DexIgnore
    public SparseArray<List<BaseFeatureModel>> u; // = new SparseArray<>();
    @DexIgnore
    public b v;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSummaryDialView e;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList f;

        @DexIgnore
        public c(NotificationSummaryDialView notificationSummaryDialView, ArrayList arrayList) {
            this.e = notificationSummaryDialView;
            this.f = arrayList;
        }

        @DexIgnore
        public final void run() {
            this.e.a();
            Iterator it = this.f.iterator();
            while (it.hasNext()) {
                AppCompatImageView appCompatImageView = (AppCompatImageView) it.next();
                if (appCompatImageView != null) {
                    try {
                        this.e.addView(appCompatImageView);
                    } catch (Exception e2) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String e3 = NotificationSummaryDialView.w;
                        local.d(e3, "onLoadBitmapAsyncComplete exception " + e2);
                    }
                }
            }
            this.e.requestLayout();
            this.e.u.clear();
            if (this.e.t.size() > 0) {
                FLogger.INSTANCE.getLocal().d(NotificationSummaryDialView.w, "onLoadBitmapAsyncComplete pendingList exists, start set again");
                NotificationSummaryDialView notificationSummaryDialView = this.e;
                SparseArray clone = notificationSummaryDialView.t.clone();
                kd4.a((Object) clone, "mPendingData.clone()");
                notificationSummaryDialView.setDataAsync(clone);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSummaryDialView e;

        @DexIgnore
        public d(NotificationSummaryDialView notificationSummaryDialView) {
            this.e = notificationSummaryDialView;
        }

        @DexIgnore
        public final void onClick(View view) {
            b c = this.e.v;
            if (c != null) {
                Object tag = view.getTag(123456789);
                if (tag != null) {
                    c.a(((Integer) tag).intValue());
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Int");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements qv<Bitmap> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSummaryDialView e;
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ AppCompatImageView g;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList h;

        @DexIgnore
        public e(NotificationSummaryDialView notificationSummaryDialView, int i, AppCompatImageView appCompatImageView, ArrayList arrayList) {
            this.e = notificationSummaryDialView;
            this.f = i;
            this.g = appCompatImageView;
            this.h = arrayList;
        }

        @DexIgnore
        public boolean a(GlideException glideException, Object obj, bw<Bitmap> bwVar, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e2 = NotificationSummaryDialView.w;
            local.d(e2, "setDataAsync onLoadFailed " + obj);
            if (this.e.q.get(this.f) != null) {
                this.g.setImageBitmap((Bitmap) this.e.q.get(this.f));
            }
            this.h.add(this.g);
            this.e.a((ArrayList<AppCompatImageView>) this.h);
            return false;
        }

        @DexIgnore
        public boolean a(Bitmap bitmap, Object obj, bw<Bitmap> bwVar, DataSource dataSource, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e2 = NotificationSummaryDialView.w;
            local.d(e2, "setDataAsync onResourceReady " + obj);
            if (bitmap != null) {
                this.e.q.put(this.f, bitmap);
                this.g.setImageBitmap(bitmap);
            }
            this.h.add(this.g);
            this.e.a((ArrayList<AppCompatImageView>) this.h);
            return false;
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = NotificationSummaryDialView.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationSummaryDialView::class.java.simpleName");
        w = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationSummaryDialView(Context context) {
        super(context);
        kd4.b(context, "context");
        d();
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        kd4.b(canvas, "canvas");
        FLogger.INSTANCE.getLocal().d(w, "dispatchDraw");
        a(canvas);
        super.dispatchDraw(canvas);
        c();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        this.r.removeCallbacksAndMessages((Object) null);
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        this.o = (Math.min((i4 - i2) - 100, i5 - i3) / 2) - (this.n / 2);
        destroyDrawingCache();
        buildDrawingCache();
        int i6 = 0;
        while (i6 < childCount) {
            View childAt = getChildAt(i6);
            if (childAt != null) {
                AppCompatImageView appCompatImageView = (AppCompatImageView) childAt;
                appCompatImageView.setLayoutParams(this.p);
                Object tag = appCompatImageView.getTag(123456789);
                if (tag != null) {
                    a(appCompatImageView, ((Integer) tag).intValue());
                    i6++;
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Int");
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type androidx.appcompat.widget.AppCompatImageView");
            }
        }
    }

    @DexIgnore
    public final void setDataAsync(SparseArray<List<BaseFeatureModel>> sparseArray) {
        kd4.b(sparseArray, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "setDataAsync data size " + sparseArray.size());
        int size = this.u.size();
        this.t.clear();
        if (size > 0) {
            SparseArray<List<BaseFeatureModel>> clone = sparseArray.clone();
            kd4.a((Object) clone, "data.clone()");
            this.t = clone;
            FLogger.INSTANCE.getLocal().d(w, "setDataAsync executing in progress, put new list in pending list");
            return;
        }
        SparseArray<List<BaseFeatureModel>> clone2 = sparseArray.clone();
        kd4.a((Object) clone2, "data.clone()");
        this.u = clone2;
        ArrayList arrayList = new ArrayList();
        fk2 a2 = ck2.a(getContext());
        kd4.a((Object) a2, "GlideApp.with(context)");
        TypedValue typedValue = new TypedValue();
        ColorStateList valueOf = ColorStateList.valueOf(this.f);
        kd4.a((Object) valueOf, "ColorStateList.valueOf(mDialItemTintColor)");
        lv a3 = ((rv) ((rv) new rv().a((oo<Bitmap>) new nk2())).a(pp.a)).a(true);
        kd4.a((Object) a3, "RequestOptions()\n       \u2026   .skipMemoryCache(true)");
        rv rvVar = (rv) a3;
        Context context = getContext();
        kd4.a((Object) context, "context");
        context.getTheme().resolveAttribute(16843534, typedValue, true);
        for (int i2 = 1; i2 <= 12; i2++) {
            AppCompatImageView appCompatImageView = new AppCompatImageView(getContext());
            appCompatImageView.setLayoutParams(this.p);
            appCompatImageView.setTag(123456789, Integer.valueOf(i2));
            appCompatImageView.setOnClickListener(new d(this));
            if (sparseArray.get(i2) != null) {
                kd4.a((Object) a2.e().a((Object) new hk2(sparseArray.get(i2))).b(new e(this, i2, appCompatImageView, arrayList)).a((lv<?>) rvVar).a(pp.a).U(), "glideApp.asBitmap()\n    \u2026                .submit()");
            } else {
                appCompatImageView.setImageResource(this.e);
                int i3 = this.h;
                appCompatImageView.setPadding(i3, i3, i3, i3);
                appCompatImageView.setElevation(50.0f);
                int i4 = this.g;
                if (i4 != -1) {
                    appCompatImageView.setBackgroundResource(i4);
                } else {
                    appCompatImageView.setBackgroundResource(typedValue.resourceId);
                }
                x9.a((ImageView) appCompatImageView, valueOf);
                arrayList.add(appCompatImageView);
            }
        }
        a((ArrayList<AppCompatImageView>) arrayList);
    }

    @DexIgnore
    public final void setOnItemClickListener(b bVar) {
        kd4.b(bVar, "listener");
        this.v = bVar;
    }

    @DexIgnore
    public final void b() {
        setDataAsync(new SparseArray());
    }

    @DexIgnore
    public final void c() {
        FLogger.INSTANCE.getLocal().d(w, "initParams");
        this.k = getMeasuredWidth();
        this.l = getMeasuredHeight();
        ViewGroup.LayoutParams layoutParams = this.p;
        if (layoutParams != null) {
            layoutParams.width = this.n;
        }
        ViewGroup.LayoutParams layoutParams2 = this.p;
        if (layoutParams2 != null) {
            layoutParams2.height = this.n;
        }
        this.m = Math.min(this.k, this.l) / 2;
        this.n = (int) (((float) this.m) / 3.5f);
    }

    @DexIgnore
    public final void d() {
        this.j = new Paint(1);
        Paint paint = this.j;
        if (paint != null) {
            paint.setStyle(Paint.Style.FILL);
            this.p = new ViewGroup.LayoutParams(-1, -1);
            return;
        }
        kd4.d("mBitmapPaint");
        throw null;
    }

    @DexIgnore
    public final void a(ArrayList<AppCompatImageView> arrayList) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "onLoadBitmapAsyncComplete size " + arrayList.size());
        if (arrayList.size() == 12) {
            this.r.post(new c(this, arrayList));
        }
    }

    @DexIgnore
    public final void a() {
        removeAllViews();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotificationSummaryDialView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        kd4.b(context, "context");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.NotificationSummaryDialView);
        this.e = obtainStyledAttributes.getResourceId(3, R.drawable.ic_preset_plus);
        this.f = obtainStyledAttributes.getColor(4, k6.a(context, (int) R.color.primaryColor));
        this.g = obtainStyledAttributes.getResourceId(1, -1);
        this.h = (int) obtainStyledAttributes.getDimension(2, ts3.a(15.0f));
        this.i = obtainStyledAttributes.getResourceId(0, R.drawable.ic_launcher);
        this.s = BitmapFactory.decodeResource(getResources(), this.i);
        obtainStyledAttributes.recycle();
        d();
        b();
    }

    @DexIgnore
    public final void a(AppCompatImageView appCompatImageView, int i2) {
        double d2 = ((double) (i2 - 3)) * 0.5235987755982988d;
        int i3 = this.n;
        int width = (int) (((double) ((getWidth() / 2) - (i3 / 2))) + (((double) this.o) * Math.cos(d2)));
        int height = (int) (((double) ((getHeight() / 2) - (i3 / 2))) + (((double) this.o) * Math.sin(d2)));
        appCompatImageView.layout(width, height, width + i3, i3 + height);
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        FLogger.INSTANCE.getLocal().d(w, "initClockImage");
        int i2 = this.n;
        float f2 = ((float) i2) * 1.5f;
        float f3 = ((float) this.k) - (((float) i2) * 1.5f);
        float f4 = f3 - f2;
        float f5 = (((float) this.l) - f4) / ((float) 2);
        float f6 = f4 + f5;
        Bitmap bitmap = this.s;
        if (bitmap == null) {
            return;
        }
        if (bitmap != null) {
            Rect rect = new Rect((int) f2, (int) f5, (int) f3, (int) f6);
            Paint paint = this.j;
            if (paint != null) {
                canvas.drawBitmap(bitmap, (Rect) null, rect, paint);
            } else {
                kd4.d("mBitmapPaint");
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }
}
