package com.portfolio.platform.view;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.blesdk.obfuscated.ck2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n8;
import com.fossil.blesdk.obfuscated.zs3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.utils.ResourceManager;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomizeWidget extends ConstraintLayout {
    @DexIgnore
    public /* final */ int A; // = Color.parseColor("#FFFF00");
    @DexIgnore
    public Integer B;
    @DexIgnore
    public Integer C;
    @DexIgnore
    public Integer D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public Drawable H;
    @DexIgnore
    public Drawable I;
    @DexIgnore
    public Drawable J;
    @DexIgnore
    public String K;
    @DexIgnore
    public String L;
    @DexIgnore
    public String M;
    @DexIgnore
    public int N;
    @DexIgnore
    public int O;
    @DexIgnore
    public float P;
    @DexIgnore
    public float Q;
    @DexIgnore
    public String R;
    @DexIgnore
    public int S;
    @DexIgnore
    public int T;
    @DexIgnore
    public float U;
    @DexIgnore
    public float V;
    @DexIgnore
    public String W;
    @DexIgnore
    public int a0;
    @DexIgnore
    public int b0;
    @DexIgnore
    public int c0;
    @DexIgnore
    public float d0;
    @DexIgnore
    public float e0;
    @DexIgnore
    public boolean f0;
    @DexIgnore
    public boolean g0;
    @DexIgnore
    public Drawable h0;
    @DexIgnore
    public /* final */ ColorDrawable i0;
    @DexIgnore
    public boolean j0;
    @DexIgnore
    public View u;
    @DexIgnore
    public View v;
    @DexIgnore
    public ImageView w;
    @DexIgnore
    public TextView x;
    @DexIgnore
    public TextView y;
    @DexIgnore
    public ProgressBar z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(CustomizeWidget customizeWidget);
    }

    @DexIgnore
    public interface c {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements n8.c {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeWidget a;
        @DexIgnore
        public /* final */ /* synthetic */ b b;
        @DexIgnore
        public /* final */ /* synthetic */ float c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ Intent e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends View.DragShadowBuilder {
            @DexIgnore
            public /* final */ /* synthetic */ d a;
            @DexIgnore
            public /* final */ /* synthetic */ n8 b;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, n8 n8Var, View view, View view2) {
                super(view2);
                this.a = dVar;
                this.b = n8Var;
            }

            @DexIgnore
            public void onDrawShadow(Canvas canvas) {
                kd4.b(canvas, "canvas");
                float f = this.a.c;
                canvas.scale(f, f);
                View view = getView();
                if (view != null) {
                    view.draw(canvas);
                }
                this.a.a.setDragMode(true);
            }

            @DexIgnore
            public void onProvideShadowMetrics(Point point, Point point2) {
                kd4.b(point, "shadowSize");
                kd4.b(point2, "shadowTouchPoint");
                super.onProvideShadowMetrics(point, point2);
                View view = getView();
                kd4.a((Object) view, "view");
                int width = (int) (((float) view.getWidth()) * this.a.c);
                View view2 = getView();
                kd4.a((Object) view2, "view");
                int height = (int) (((float) view2.getHeight()) * this.a.c);
                point.set(width, height);
                point2.set(width / 2, height / 2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WidgetControl", "onDragStart - shadowTouchPoint=" + point2);
                n8 n8Var = this.b;
                if (n8Var != null) {
                    n8Var.a(point2);
                }
            }
        }

        @DexIgnore
        public d(CustomizeWidget customizeWidget, b bVar, float f, String str, Intent intent) {
            this.a = customizeWidget;
            this.b = bVar;
            this.c = f;
            this.d = str;
            this.e = intent;
        }

        @DexIgnore
        public final boolean a(View view, n8 n8Var) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onDragStart - v=");
            sb.append(view != null ? Integer.valueOf(view.getId()) : null);
            sb.append(", helper=");
            sb.append(n8Var);
            local.d("WidgetControl", sb.toString());
            b bVar = this.b;
            if (bVar != null) {
                bVar.a(this.a);
            }
            a aVar = new a(this, n8Var, view, view);
            ClipData clipData = new ClipData(new ClipDescription(this.d, new String[]{"text/plain"}), new ClipData.Item(this.e));
            FLogger.INSTANCE.getLocal().d("WidgetControl", "onDragStart - created ClipDescription");
            if (Build.VERSION.SDK_INT >= 24) {
                return this.a.startDragAndDrop(clipData, aVar, (Object) null, 257);
            }
            return this.a.startDrag(clipData, aVar, (Object) null, 257);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public CustomizeWidget(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        int i = this.A;
        this.E = i;
        this.F = i;
        this.G = i;
        this.N = i;
        this.O = i;
        this.P = -1.0f;
        this.Q = -1.0f;
        this.S = i;
        this.T = i;
        this.U = -1.0f;
        this.V = -1.0f;
        this.a0 = i;
        this.b0 = i;
        this.c0 = i;
        this.d0 = -1.0f;
        this.e0 = -1.0f;
        this.i0 = new ColorDrawable(k6.a(getContext(), (int) R.color.transparent));
        TypedArray typedArray = null;
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        View inflate = layoutInflater != null ? layoutInflater.inflate(R.layout.view_widget_control, this, true) : null;
        if (inflate != null) {
            View findViewById = findViewById(R.id.root);
            kd4.a((Object) findViewById, "findViewById(R.id.root)");
            this.u = findViewById;
            this.u.setElevation(getElevation());
            View findViewById2 = findViewById(R.id.holder);
            kd4.a((Object) findViewById2, "findViewById(R.id.holder)");
            this.v = findViewById2;
            View findViewById3 = inflate.findViewById(R.id.iv_top);
            kd4.a((Object) findViewById3, "view.findViewById(R.id.iv_top)");
            this.w = (ImageView) findViewById3;
            View findViewById4 = inflate.findViewById(R.id.tv_top);
            kd4.a((Object) findViewById4, "view.findViewById(R.id.tv_top)");
            this.x = (TextView) findViewById4;
            View findViewById5 = inflate.findViewById(R.id.tv_bottom);
            kd4.a((Object) findViewById5, "view.findViewById(R.id.tv_bottom)");
            this.y = (TextView) findViewById5;
            View findViewById6 = inflate.findViewById(R.id.pb_progress);
            kd4.a((Object) findViewById6, "view.findViewById(R.id.pb_progress)");
            this.z = (ProgressBar) findViewById6;
            typedArray = context != null ? context.obtainStyledAttributes(attributeSet, h62.WidgetControl) : typedArray;
            if (typedArray != null) {
                this.B = Integer.valueOf(typedArray.getResourceId(0, R.drawable.bg_widget_control_gray));
                this.C = Integer.valueOf(typedArray.getResourceId(1, R.drawable.bg_widget_control_white));
                Integer.valueOf(typedArray.getResourceId(1, R.drawable.bg_widget_control_white));
                this.D = this.B;
                this.H = typedArray.getDrawable(20);
                this.W = typedArray.getString(5);
                this.I = typedArray.getDrawable(19);
                this.J = typedArray.getDrawable(21);
                this.M = typedArray.getString(14);
                this.N = typedArray.getColor(15, this.A);
                this.O = typedArray.getColor(16, this.A);
                this.P = typedArray.getDimension(17, -1.0f);
                this.Q = typedArray.getDimension(18, -1.0f);
                this.R = typedArray.getString(2);
                this.S = typedArray.getColor(3, this.A);
                this.T = typedArray.getColor(4, this.A);
                this.U = typedArray.getDimension(6, -1.0f);
                this.V = typedArray.getDimension(7, -1.0f);
                this.a0 = typedArray.getColor(8, this.A);
                this.b0 = typedArray.getColor(9, this.A);
                this.c0 = this.a0;
                this.U = typedArray.getDimension(10, -1.0f);
                this.e0 = typedArray.getDimension(11, -1.0f);
                this.f0 = typedArray.getBoolean(13, false);
                this.g0 = typedArray.getBoolean(12, false);
                setRemoveMode(this.g0);
                typedArray.recycle();
                return;
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    private final void setTopIconSrc(Drawable drawable) {
        this.I = drawable;
        if (drawable != null) {
            this.I = drawable;
            this.w.setImageDrawable(this.I);
            if (!this.f0) {
                this.w.setImageDrawable(this.I);
                this.w.setImageTintList(ColorStateList.valueOf(this.a0));
                this.c0 = this.a0;
            }
        }
    }

    @DexIgnore
    public final CustomizeWidget a(c cVar) {
        return this;
    }

    @DexIgnore
    public final void a(Integer num, Integer num2, Integer num3, Integer num4) {
        if (num3 != null) {
            num3.intValue();
            this.a0 = num3.intValue();
            this.N = num3.intValue();
            this.S = num3.intValue();
        }
        if (num != null) {
            num.intValue();
            this.b0 = num.intValue();
            this.O = num.intValue();
            this.T = num.intValue();
        }
        if (num2 != null) {
            num2.intValue();
            this.F = num2.intValue();
        }
        if (num4 != null) {
            num4.intValue();
            this.E = num4.intValue();
        }
        f();
    }

    @DexIgnore
    public final void b(String str) {
        kd4.b(str, "complicationId");
        switch (str.hashCode()) {
            case -829740640:
                if (str.equals("commute-time")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_preset_commute));
                    return;
                }
                return;
            case -85386984:
                if (str.equals("active-minutes")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_preset_workout));
                    return;
                }
                return;
            case -48173007:
                if (str.equals("chance-of-rain")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_preset_rain));
                    return;
                }
                return;
            case 3076014:
                if (str.equals("date")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_preset_calendar));
                    return;
                }
                return;
            case 96634189:
                if (str.equals("empty")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_preset_close));
                    return;
                }
                return;
            case 109761319:
                if (str.equals("steps")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_preset_steps));
                    return;
                }
                return;
            case 134170930:
                if (str.equals("second-timezone")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_preset_timezone));
                    return;
                }
                return;
            case 1223440372:
                if (str.equals("weather")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_preset_weather));
                    return;
                }
                return;
            case 1884273159:
                if (str.equals("heart-rate")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_preset_heart));
                    return;
                }
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void c(String str) {
        kd4.b(str, "microAppId");
        switch (zs3.a[MicroAppInstruction.MicroAppID.Companion.getMicroAppId(str).ordinal()]) {
            case 1:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_modetoggle));
                return;
            case 2:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_photo));
                return;
            case 3:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_activity));
                return;
            case 4:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_alarm));
                return;
            case 5:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_lastalert));
                return;
            case 6:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_date));
                return;
            case 7:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_commute));
                return;
            case 8:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_goal));
                return;
            case 9:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_music));
                return;
            case 10:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_volumedown));
                return;
            case 11:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_volumeup));
                return;
            case 12:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_2tz));
                return;
            case 13:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_weather));
                return;
            case 14:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_ring));
                return;
            case 15:
                setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_stopwatch));
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void d(String str) {
        kd4.b(str, "watchAppId");
        switch (str.hashCode()) {
            case -829740640:
                if (str.equals("commute-time")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_black_commute));
                    return;
                }
                return;
            case -740386388:
                if (str.equals("diagnostics")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_date));
                    return;
                }
                return;
            case -420342747:
                if (str.equals("wellness")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_preset_wellness));
                    return;
                }
                return;
            case 96634189:
                if (str.equals("empty")) {
                    setTopIconSrc((Drawable) null);
                    return;
                }
                return;
            case 104263205:
                if (str.equals(Constants.MUSIC)) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_preset_music_control));
                    return;
                }
                return;
            case 110364485:
                if (str.equals("timer")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_watch_app_timer));
                    return;
                }
                return;
            case 1223440372:
                if (str.equals("weather")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_preset_weather));
                    return;
                }
                return;
            case 1374620322:
                if (str.equals("notification-panel")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_lastalert));
                    return;
                }
                return;
            case 1525170845:
                if (str.equals("workout")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_buddy));
                    return;
                }
                return;
            case 1860261700:
                if (str.equals("stop-watch")) {
                    setTopIconSrc(k6.c(getContext(), R.drawable.ic_shortcuts_stopwatch));
                    return;
                }
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void e() {
        if (this.j0) {
            this.j0 = false;
            this.z.setProgress(0);
        }
    }

    @DexIgnore
    public final void f() {
        if (!TextUtils.isEmpty(this.M)) {
            this.x.setText(this.M);
        } else {
            this.x.setVisibility(4);
        }
        if (this.g0) {
            this.w.setVisibility(0);
            ImageView imageView = this.w;
            Drawable drawable = this.H;
            if (drawable == null) {
                drawable = this.i0;
            }
            imageView.setImageDrawable(drawable);
            this.x.setVisibility(4);
            if (!TextUtils.isEmpty(this.W)) {
                this.y.setText(this.W);
                this.y.setVisibility(0);
            } else {
                this.y.setVisibility(8);
            }
        } else if (this.f0) {
            String str = this.L;
            if (str == null) {
                str = this.K;
            }
            if (!TextUtils.isEmpty(str)) {
                kd4.a((Object) ck2.a((View) this.w).a(str).b().a(this.w), "GlideApp.with(ivTop)\n   \u2026             .into(ivTop)");
            } else {
                Drawable drawable2 = this.J;
                if (drawable2 == null) {
                    drawable2 = this.I;
                }
                if (drawable2 != null) {
                    this.w.setImageDrawable(drawable2);
                    this.w.setVisibility(0);
                    this.x.setVisibility(4);
                } else {
                    this.w.setImageDrawable(this.i0);
                    this.w.setVisibility(4);
                    this.x.setVisibility(0);
                }
            }
            if (!TextUtils.isEmpty(this.R)) {
                this.y.setText(this.R);
                this.y.setVisibility(0);
            } else {
                this.y.setVisibility(8);
            }
        } else {
            if (!TextUtils.isEmpty(this.K)) {
                kd4.a((Object) ck2.a((View) this.w).a(this.K).b().a(this.w), "GlideApp.with(ivTop)\n   \u2026             .into(ivTop)");
            } else {
                Drawable drawable3 = this.I;
                if (drawable3 != null) {
                    this.w.setImageDrawable(drawable3);
                    this.w.setVisibility(0);
                    this.x.setVisibility(4);
                } else {
                    this.w.setImageDrawable(this.i0);
                    this.w.setVisibility(4);
                    this.x.setVisibility(0);
                }
            }
            if (!TextUtils.isEmpty(this.R)) {
                this.y.setText(this.R);
                this.y.setVisibility(0);
            } else {
                this.y.setVisibility(8);
            }
        }
        this.w.setVisibility(0);
        if (this.f0) {
            this.D = this.C;
            this.G = this.F;
            this.u.getBackground().clearColorFilter();
            if (this.F != this.A) {
                this.u.getBackground().mutate().setColorFilter(this.F, PorterDuff.Mode.SRC_IN);
            } else {
                Drawable drawable4 = this.h0;
                if (drawable4 != null) {
                    this.u.setBackground(drawable4);
                } else {
                    View view = this.u;
                    Context context = getContext();
                    Integer num = this.D;
                    if (num != null) {
                        view.setBackground(k6.c(context, num.intValue()));
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            }
            int i = this.b0;
            if (i != this.A) {
                this.w.setImageTintList(ColorStateList.valueOf(i));
                this.c0 = this.b0;
            } else {
                this.w.setImageTintList(ColorStateList.valueOf(this.a0));
                this.c0 = this.a0;
            }
            int i2 = this.O;
            int i3 = this.A;
            if (i2 != i3) {
                this.x.setTextColor(i2);
            } else {
                int i4 = this.N;
                if (i4 != i3) {
                    this.x.setTextColor(i4);
                } else {
                    int i5 = this.b0;
                    if (i5 != i3) {
                        this.x.setTextColor(i5);
                    } else {
                        this.x.setTextColor(this.a0);
                    }
                }
            }
            float f = this.Q;
            if (f != -1.0f) {
                this.x.setTextSize(f);
            } else {
                float f2 = this.P;
                if (f2 != -1.0f) {
                    this.x.setTextSize(f2);
                } else {
                    float f3 = this.e0;
                    if (f3 != -1.0f) {
                        this.x.setTextSize(f3);
                    } else {
                        float f4 = this.d0;
                        if (f4 != -1.0f) {
                            this.x.setTextSize(f4);
                        }
                    }
                }
            }
            int i6 = this.T;
            int i7 = this.A;
            if (i6 != i7) {
                this.y.setTextColor(i6);
            } else {
                int i8 = this.S;
                if (i8 != i7) {
                    this.y.setTextColor(i8);
                } else {
                    int i9 = this.b0;
                    if (i9 != i7) {
                        this.y.setTextColor(i9);
                    } else {
                        this.y.setTextColor(this.a0);
                    }
                }
            }
            float f5 = this.V;
            if (f5 != -1.0f) {
                this.y.setTextSize(f5);
                return;
            }
            float f6 = this.U;
            if (f6 != -1.0f) {
                this.y.setTextSize(f6);
                return;
            }
            float f7 = this.e0;
            if (f7 != -1.0f) {
                this.y.setTextSize(f7);
                return;
            }
            float f8 = this.d0;
            if (f8 != -1.0f) {
                this.y.setTextSize(f8);
                return;
            }
            return;
        }
        this.D = this.B;
        this.G = this.E;
        this.u.getBackground().clearColorFilter();
        if (this.E != this.A) {
            this.u.getBackground().mutate().setColorFilter(this.E, PorterDuff.Mode.SRC_IN);
        } else {
            Drawable drawable5 = this.h0;
            if (drawable5 != null) {
                this.u.setBackground(drawable5);
            } else {
                View view2 = this.u;
                Context context2 = getContext();
                Integer num2 = this.D;
                if (num2 != null) {
                    view2.setBackground(k6.c(context2, num2.intValue()));
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }
        this.w.setImageTintList(ColorStateList.valueOf(this.a0));
        int i10 = this.a0;
        this.c0 = i10;
        int i11 = this.N;
        int i12 = this.A;
        if (i11 != i12) {
            this.x.setTextColor(i11);
        } else if (i10 != i12) {
            this.x.setTextColor(i10);
        }
        float f9 = this.P;
        if (f9 != -1.0f) {
            this.x.setTextSize(f9);
        } else {
            float f10 = this.d0;
            if (f10 != -1.0f) {
                this.x.setTextSize(f10);
            }
        }
        int i13 = this.S;
        if (i13 != this.A) {
            this.y.setTextColor(i13);
        } else {
            this.y.setTextColor(this.a0);
        }
        float f11 = this.U;
        if (f11 != -1.0f) {
            this.y.setTextSize(f11);
            return;
        }
        float f12 = this.d0;
        if (f12 != -1.0f) {
            this.y.setTextSize(f12);
        }
    }

    @DexIgnore
    public final void g() {
        int i = this.A;
        this.E = i;
        this.F = i;
        this.b0 = i;
        this.O = i;
        this.T = i;
        this.N = i;
        this.S = i;
        f();
    }

    @DexIgnore
    public final int getBackgroundDrawableColor() {
        int i = this.G;
        if (i != this.A) {
            return i;
        }
        Integer num = this.D;
        if (num != null && num.intValue() == R.drawable.bg_widget_control_gray) {
            return R.color.hex636363_90;
        }
        return (num != null && num.intValue() == R.drawable.bg_widget_control_active_color_primary) ? R.color.activeColorPrimary : R.color.white;
    }

    @DexIgnore
    public final int getBottomTextColor() {
        return this.y.getCurrentTextColor();
    }

    @DexIgnore
    public final int getIconTintColor() {
        return this.c0;
    }

    @DexIgnore
    public final void h() {
        f();
    }

    @DexIgnore
    public final void setBackgroundColorRes(Integer num) {
        if (num != null) {
            num.intValue();
            this.E = num.intValue();
            f();
        }
    }

    @DexIgnore
    public final void setBackgroundColorResSelected(Integer num) {
        if (num != null) {
            num.intValue();
            this.F = num.intValue();
            f();
        }
    }

    @DexIgnore
    public final void setBackgroundDrawableCus(Drawable drawable) {
        kd4.b(drawable, ResourceManager.DRAWABLE);
        this.h0 = drawable;
        f();
    }

    @DexIgnore
    public final void setBackgroundRes(Integer num) {
        this.h0 = null;
        this.B = num;
        f();
    }

    @DexIgnore
    public final void setBottomContent(String str) {
        this.R = str;
        if (this.g0) {
            if (!TextUtils.isEmpty(str)) {
                this.y.setText(str);
            }
        } else if (!TextUtils.isEmpty(str)) {
            this.y.setVisibility(0);
            this.y.setText(str);
        } else {
            this.y.setVisibility(8);
        }
    }

    @DexIgnore
    public final void setContentColorRes(Integer num) {
        if (num != null) {
            num.intValue();
            this.N = num.intValue();
            this.S = num.intValue();
            f();
        }
    }

    @DexIgnore
    public final void setContentColorResSelected(Integer num) {
        if (num != null) {
            num.intValue();
            this.O = num.intValue();
            this.T = num.intValue();
            f();
        }
    }

    @DexIgnore
    public final void setDefaultColorRes(Integer num) {
        if (num != null) {
            num.intValue();
            this.a0 = num.intValue();
            f();
        }
    }

    @DexIgnore
    public final void setDefaultColorResSelected(Integer num) {
        if (num != null) {
            num.intValue();
            this.b0 = num.intValue();
            f();
        }
    }

    @DexIgnore
    public final void setDragMode(boolean z2) {
        if (z2) {
            this.u.setVisibility(4);
            this.v.setVisibility(0);
            setAlpha(1.0f);
            return;
        }
        this.u.setVisibility(0);
        this.v.setVisibility(4);
        setAlpha(1.0f);
    }

    @DexIgnore
    public final void setRemoveMode(boolean z2) {
        this.g0 = z2;
        f();
    }

    @DexIgnore
    public final void setSelectedWc(boolean z2) {
        this.f0 = z2;
        f();
    }

    @DexIgnore
    public static /* synthetic */ n8 a(CustomizeWidget customizeWidget, String str, Intent intent, View.OnDragListener onDragListener, b bVar, int i, Object obj) {
        if ((i & 4) != 0) {
            onDragListener = null;
        }
        if ((i & 8) != 0) {
            bVar = null;
        }
        return customizeWidget.a(str, intent, onDragListener, bVar);
    }

    @DexIgnore
    public final n8 a(String str, Intent intent, View.OnDragListener onDragListener, b bVar) {
        kd4.b(str, "label");
        kd4.b(intent, "intentItem");
        setTag(getTag());
        n8 n8Var = new n8(this, new d(this, bVar, 1.0f, str, intent));
        n8Var.a();
        if (onDragListener != null) {
            setOnDragListener(onDragListener);
        }
        return n8Var;
    }

    @DexIgnore
    public final void d() {
        this.j0 = true;
        this.z.setProgress(100);
    }
}
