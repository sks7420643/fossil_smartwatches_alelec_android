package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.gd4;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.wr3;
import com.fossil.blesdk.obfuscated.xt3;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.sleep.SleepDayData;
import com.portfolio.platform.data.model.sleep.SleepSessionData;
import com.portfolio.platform.enums.GoalType;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import kotlin.jvm.internal.Ref$IntRef;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepWeekDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String H;
    @DexIgnore
    public /* final */ int A;
    @DexIgnore
    public /* final */ ArrayList<SleepDayData> B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public GoalType F;
    @DexIgnore
    public /* final */ String[] G;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public /* final */ int n;
    @DexIgnore
    public /* final */ String o;
    @DexIgnore
    public /* final */ Paint p;
    @DexIgnore
    public /* final */ Paint q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = SleepWeekDetailsChart.class.getSimpleName();
        kd4.a((Object) simpleName, "SleepWeekDetailsChart::class.java.simpleName");
        H = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepWeekDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        kd4.b(context, "context");
        this.p = new Paint(1);
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = context.getResources().getDimensionPixelSize(R.dimen.dp15);
        this.w = context.getResources().getDimensionPixelSize(R.dimen.dp15);
        this.x = context.getResources().getDimensionPixelSize(R.dimen.dp10);
        this.y = context.getResources().getDimensionPixelSize(R.dimen.dp10);
        this.z = context.getResources().getDimensionPixelSize(R.dimen.dp5);
        this.A = context.getResources().getDimensionPixelSize(R.dimen.dp3);
        this.B = new ArrayList<>();
        this.F = GoalType.TOTAL_SLEEP;
        String[] stringArray = context.getResources().getStringArray(R.array.days_of_week_alphabet);
        kd4.a((Object) stringArray, "context.resources.getStr\u2026ay.days_of_week_alphabet)");
        this.G = stringArray;
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, h62.SleepWeekDetailsChart, 0, 0));
        }
        int a2 = k6.a(context, (int) R.color.gray_30);
        String string = context.getString(R.string.font_path_regular);
        kd4.a((Object) string, "context.getString(R.string.font_path_regular)");
        TypedArray mTypedArray = getMTypedArray();
        this.h = mTypedArray != null ? mTypedArray.getColor(3, a2) : a2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.i = mTypedArray2 != null ? mTypedArray2.getColor(1, a2) : a2;
        TypedArray mTypedArray3 = getMTypedArray();
        this.j = mTypedArray3 != null ? mTypedArray3.getColor(0, a2) : a2;
        TypedArray mTypedArray4 = getMTypedArray();
        this.k = mTypedArray4 != null ? mTypedArray4.getColor(2, a2) : a2;
        TypedArray mTypedArray5 = getMTypedArray();
        this.l = mTypedArray5 != null ? mTypedArray5.getColor(4, a2) : a2;
        TypedArray mTypedArray6 = getMTypedArray();
        this.m = mTypedArray6 != null ? mTypedArray6.getColor(5, a2) : a2;
        TypedArray mTypedArray7 = getMTypedArray();
        this.n = mTypedArray7 != null ? mTypedArray7.getDimensionPixelSize(7, 40) : 40;
        TypedArray mTypedArray8 = getMTypedArray();
        if (mTypedArray8 != null) {
            String string2 = mTypedArray8.getString(6);
            if (string2 != null) {
                string = string2;
            }
        }
        this.o = string;
        TypedArray mTypedArray9 = getMTypedArray();
        if (mTypedArray9 != null) {
            mTypedArray9.recycle();
        }
    }

    @DexIgnore
    private final int getMChartMax() {
        int i2;
        int i3 = xt3.a[this.F.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = H;
            local.d(str, "Max of awake: " + this.E);
            i2 = this.E;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = H;
            local2.d(str2, "Max of light: " + this.D);
            i2 = this.D;
        } else if (i3 != 3) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = H;
            local3.d(str3, "Max of total: " + getMMaxSleepMinutes());
            i2 = getMMaxSleepMinutes();
        } else {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = H;
            local4.d(str4, "Max of restful: " + this.C);
            i2 = this.C;
        }
        return Math.max(i2, getMMaxGoal());
    }

    @DexIgnore
    private final int getMMaxGoal() {
        T t2;
        Iterator<T> it = this.B.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                int sleepGoal = ((SleepDayData) t2).getSleepGoal();
                do {
                    T next = it.next();
                    int sleepGoal2 = ((SleepDayData) next).getSleepGoal();
                    if (sleepGoal < sleepGoal2) {
                        t2 = next;
                        sleepGoal = sleepGoal2;
                    }
                } while (it.hasNext());
            }
        }
        SleepDayData sleepDayData = (SleepDayData) t2;
        if (sleepDayData != null) {
            return sleepDayData.getSleepGoal();
        }
        return 480;
    }

    @DexIgnore
    private final int getMMaxSleepMinutes() {
        T t2;
        Iterator<T> it = this.B.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                int totalSleepMinutes = ((SleepDayData) t2).getTotalSleepMinutes();
                do {
                    T next = it.next();
                    int totalSleepMinutes2 = ((SleepDayData) next).getTotalSleepMinutes();
                    if (totalSleepMinutes < totalSleepMinutes2) {
                        t2 = next;
                        totalSleepMinutes = totalSleepMinutes2;
                    }
                } while (it.hasNext());
            }
        }
        SleepDayData sleepDayData = (SleepDayData) t2;
        if (sleepDayData != null) {
            return sleepDayData.getTotalSleepMinutes();
        }
        return 0;
    }

    @DexIgnore
    private final int getMSleepMode() {
        int i2 = xt3.b[this.F.ordinal()];
        if (i2 == 1) {
            return 0;
        }
        if (i2 != 2) {
            return i2 != 3 ? 3 : 2;
        }
        return 1;
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3) {
        List<Integer> list2 = list;
        int i4 = i2;
        if (!this.B.isEmpty()) {
            float mChartMax = (float) getMChartMax();
            int size = list.size();
            for (int i5 = 0; i5 < size; i5++) {
                SleepDayData sleepDayData = this.B.get(i5);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = H;
                local.d(str, "Actual sleep: " + sleepDayData.getTotalSleepMinutes() + ", chart max: " + mChartMax);
                int intValue = list2.get(i5).intValue();
                int totalSleepMinutes = sleepDayData.getTotalSleepMinutes();
                int mSleepMode = getMSleepMode();
                if (mSleepMode == 0) {
                    int dayAwake = sleepDayData.getDayAwake();
                    Canvas canvas2 = canvas;
                    a(canvas2, this.s, intValue, i2, (int) ((((float) dayAwake) / mChartMax) * ((float) i4)), i3, dayAwake, totalSleepMinutes);
                } else if (mSleepMode == 1) {
                    int dayLight = sleepDayData.getDayLight();
                    Canvas canvas3 = canvas;
                    a(canvas3, this.t, intValue, i2, (int) ((((float) dayLight) / mChartMax) * ((float) i4)), i3, dayLight, totalSleepMinutes);
                } else if (mSleepMode != 2) {
                    int intValue2 = list2.get(i5).intValue();
                    int i6 = (int) ((((float) totalSleepMinutes) / mChartMax) * ((float) i4));
                    SleepDayData sleepDayData2 = this.B.get(i5);
                    kd4.a((Object) sleepDayData2, "mSleepDayDataList[i]");
                    a(canvas, intValue2, i2, i6, i3, sleepDayData2);
                } else {
                    int dayRestful = sleepDayData.getDayRestful();
                    Canvas canvas4 = canvas;
                    a(canvas4, this.u, intValue, i2, (int) ((((float) dayRestful) / mChartMax) * ((float) i4)), i3, dayRestful, totalSleepMinutes);
                }
            }
        }
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(-1);
            int width = (getWidth() - getStartBitmap().getWidth()) - (this.z * 2);
            Ref$IntRef ref$IntRef = new Ref$IntRef();
            ref$IntRef.element = 0;
            Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            a(canvas, width, (yc4<? super Integer, ? super List<Integer>, qa4>) new SleepWeekDetailsChart$draw$Anon1$Anon1(ref$IntRef, ref$ObjectRef));
            int height = (getHeight() - ref$IntRef.element) - this.x;
            List list = (List) ref$ObjectRef.element;
            if (list != null) {
                a(canvas, list, height, this.z * 2);
                a(canvas, (List<Integer>) list, width, height, this.z * 2, height);
            }
            a(canvas, 0, height + 4);
        }
    }

    @DexIgnore
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    public int getStarSizeInPx() {
        return this.v;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.p.setColor(this.h);
        float f = (float) 4;
        this.p.setStrokeWidth(f);
        this.r.setColor(this.m);
        this.r.setStyle(Paint.Style.FILL);
        this.r.setTextSize((float) this.n);
        Paint paint = this.r;
        Context context = getContext();
        kd4.a((Object) context, "context");
        paint.setTypeface(Typeface.createFromAsset(context.getAssets(), this.o));
        this.q.setColor(this.i);
        this.q.setStyle(Paint.Style.STROKE);
        this.q.setStrokeWidth(f);
        this.q.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.s.setColor(this.j);
        this.s.setStrokeWidth((float) this.y);
        this.s.setStyle(Paint.Style.FILL);
        this.t.setColor(this.k);
        this.t.setStrokeWidth((float) this.y);
        this.t.setStyle(Paint.Style.FILL);
        this.u.setColor(this.l);
        this.u.setStrokeWidth((float) this.y);
        this.u.setStyle(Paint.Style.FILL);
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Canvas canvas, Paint paint, int i2, int i3, int i4, int i5, int i6, int i7) {
        int i8 = i2 - (this.y / 2);
        if (i8 < 0) {
            i8 = 0;
        }
        RectF rectF = new RectF((float) i8, (float) ((i3 - ((int) ((((float) i6) / ((float) i7)) * ((float) i4)))) + i5), (float) (this.y + i8), (float) i3);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = H;
        local.d(str, "duration: " + i6 + ", totalMinutes: " + i7 + ", rect top: " + rectF.top + ", rect bot: " + rectF.bottom);
        wr3.c(canvas, rectF, paint, rectF.width() / ((float) 3));
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3, int i4, int i5, SleepDayData sleepDayData) {
        int i6;
        int i7;
        int i8;
        int i9;
        Paint paint;
        int i10 = i2 - (this.y / 2);
        if (i10 < 0) {
            i10 = 0;
        }
        int i11 = this.y + i10;
        int size = (i4 - i5) - (this.A * (sleepDayData.getSessionList().size() - 1 > 0 ? sleepDayData.getSessionList().size() - 1 : 0));
        Iterator<SleepSessionData> it = sleepDayData.getSessionList().iterator();
        int i12 = 0;
        while (it.hasNext()) {
            int component1 = it.next().component1();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = H;
            local.d(str, "session duration: " + component1);
            i12 += component1;
        }
        int size2 = sleepDayData.getSessionList().size();
        int i13 = i3;
        int i14 = 0;
        while (i14 < size2) {
            SleepSessionData sleepSessionData = sleepDayData.getSessionList().get(i14);
            kd4.a((Object) sleepSessionData, "sleepDayData.sessionList[sessionIndex]");
            SleepSessionData sleepSessionData2 = sleepSessionData;
            int durationInMinutes = sleepSessionData2.getDurationInMinutes();
            float f = (float) durationInMinutes;
            int i15 = (int) ((f / ((float) i12)) * ((float) size));
            List<WrapperSleepStateChange> sleepStates = sleepSessionData2.getSleepStates();
            int size3 = sleepStates.size();
            int i16 = i13;
            int i17 = 0;
            while (i17 < size3) {
                int i18 = sleepStates.get(i17).state;
                if (i17 < size3 - 1) {
                    i6 = size;
                    i8 = size2;
                    i7 = i12;
                    i9 = ((int) sleepStates.get(i17 + 1).index) - ((int) sleepStates.get(i17).index);
                } else {
                    i6 = size;
                    i8 = size2;
                    i7 = i12;
                    i9 = durationInMinutes - ((int) sleepStates.get(i17).index);
                }
                int i19 = (int) ((((float) i9) / f) * ((float) i15));
                if (i19 < 1) {
                    i19 = 1;
                }
                int i20 = i16 - i19;
                int i21 = i10;
                RectF rectF = new RectF((float) i10, (float) i20, (float) i11, (float) i16);
                if (i18 == 1) {
                    paint = this.t;
                } else if (i18 != 2) {
                    paint = this.s;
                } else {
                    paint = this.u;
                }
                canvas.drawRect(rectF, paint);
                i17++;
                size = i6;
                i16 = i20;
                size2 = i8;
                i12 = i7;
                i10 = i21;
            }
            Canvas canvas2 = canvas;
            int i22 = i12;
            i13 = i16 - this.A;
            i14++;
            size = size;
            size2 = size2;
            i10 = i10;
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepWeekDetailsChart(Context context) {
        this(context, (AttributeSet) null, 0);
        kd4.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepWeekDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        kd4.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, yc4<? super Integer, ? super List<Integer>, qa4> yc4) {
        int length = this.G.length;
        Rect rect = new Rect();
        String str = this.G[0];
        gd4 gd4 = gd4.a;
        Paint paint = this.r;
        kd4.a((Object) str, "sundayCharacter");
        paint.getTextBounds(str, 0, StringsKt__StringsKt.c(str) > 0 ? StringsKt__StringsKt.c(str) : 1, rect);
        float measureText = this.r.measureText(str);
        float height = (float) rect.height();
        float f = (((float) (i2 - (this.w * 2))) - (((float) length) * measureText)) / ((float) (length - 1));
        float f2 = (float) 2;
        float height2 = ((float) getHeight()) - (height / f2);
        float f3 = (float) this.w;
        LinkedList linkedList = new LinkedList();
        for (String drawText : this.G) {
            canvas.drawText(drawText, f3, height2, this.r);
            linkedList.add(Integer.valueOf((int) ((measureText / f2) + f3)));
            f3 += measureText + f;
        }
        yc4.invoke(Integer.valueOf((int) height), linkedList);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.p);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.p);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3, int i4, int i5) {
        Canvas canvas2 = canvas;
        List<Integer> list2 = list;
        if ((!this.B.isEmpty()) && this.B.size() > 1) {
            float mChartMax = (float) getMChartMax();
            Path path = new Path();
            int size = list.size();
            float height = (float) getStartBitmap().getHeight();
            int i6 = 1;
            while (i6 < size) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = H;
                StringBuilder sb = new StringBuilder();
                sb.append("Previous sleep goal: ");
                int i7 = i6 - 1;
                sb.append(this.B.get(i7).getSleepGoal());
                sb.append(", current sleep goal: ");
                sb.append(this.B.get(i6).getSleepGoal());
                sb.append(", chart max: ");
                sb.append(mChartMax);
                local.d(str, sb.toString());
                float intValue = (float) list2.get(i6).intValue();
                float f = (float) i3;
                float f2 = (float) i4;
                float f3 = (float) i5;
                float min = Math.min(((1.0f - (((float) this.B.get(i6).getSleepGoal()) / mChartMax)) * f) + f2, f3);
                float intValue2 = (float) list2.get(i7).intValue();
                float min2 = Math.min(((1.0f - (((float) this.B.get(i7).getSleepGoal()) / mChartMax)) * f) + f2, f3);
                if (min == min2) {
                    path.moveTo(intValue2, min2);
                    if (i6 == list.size() - 1) {
                        path.lineTo((float) (i2 + this.z), min);
                    } else {
                        path.lineTo(intValue, min);
                    }
                    canvas2.drawPath(path, this.q);
                } else {
                    path.moveTo(intValue2, min2);
                    path.lineTo(intValue, min2);
                    canvas2.drawPath(path, this.q);
                    path.moveTo(intValue, min2);
                    path.lineTo(intValue, min);
                    canvas2.drawPath(path, this.q);
                    if (i6 == list.size() - 1) {
                        path.moveTo(intValue, min);
                        path.lineTo((float) (i2 + this.z), min);
                        canvas2.drawPath(path, this.q);
                    }
                }
                i6++;
                list2 = list;
                height = min;
            }
            canvas2.drawBitmap(getStartBitmap(), (float) (i2 + this.z), height - ((float) (getStartBitmap().getHeight() / 2)), this.r);
        }
    }
}
