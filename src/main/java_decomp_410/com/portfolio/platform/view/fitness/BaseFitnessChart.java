package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class BaseFitnessChart extends View implements ViewTreeObserver.OnGlobalLayoutListener {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public TypedArray e;
    @DexIgnore
    public Bitmap f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = BaseFitnessChart.class.getSimpleName();
        kd4.a((Object) simpleName, "BaseFitnessChart::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseFitnessChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        kd4.b(context, "context");
    }

    @DexIgnore
    public final void a() {
        Bitmap bitmap = this.f;
        if (bitmap != null) {
            if (bitmap == null) {
                kd4.d("mStarBitmap");
                throw null;
            } else if (!bitmap.isRecycled()) {
                return;
            }
        }
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), getStarIconResId());
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeResource, getStarSizeInPx(), getStarSizeInPx(), false);
        kd4.a((Object) createScaledBitmap, "Bitmap.createScaledBitma\u2026nPx, starSizeInPx, false)");
        this.f = createScaledBitmap;
        Bitmap bitmap2 = this.f;
        if (bitmap2 == null) {
            kd4.d("mStarBitmap");
            throw null;
        } else if (!kd4.a((Object) bitmap2, (Object) decodeResource)) {
            decodeResource.recycle();
        }
    }

    @DexIgnore
    public final void b() {
        Bitmap bitmap = this.f;
        if (bitmap == null) {
            return;
        }
        if (bitmap != null) {
            bitmap.recycle();
        } else {
            kd4.d("mStarBitmap");
            throw null;
        }
    }

    @DexIgnore
    public final TypedArray getMTypedArray() {
        return this.e;
    }

    @DexIgnore
    public abstract int getStarIconResId();

    @DexIgnore
    public abstract int getStarSizeInPx();

    @DexIgnore
    public final Bitmap getStartBitmap() {
        if (this.f == null) {
            a();
        }
        Bitmap bitmap = this.f;
        if (bitmap != null) {
            return bitmap;
        }
        kd4.d("mStarBitmap");
        throw null;
    }

    @DexIgnore
    public void onAttachedToWindow() {
        FLogger.INSTANCE.getLocal().d(g, "onAttachedToWindow, initStartBitmap");
        super.onAttachedToWindow();
        a();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        FLogger.INSTANCE.getLocal().d(g, "onDetachedFromWindow, recycleStarBitmap");
        super.onDetachedFromWindow();
        b();
    }

    @DexIgnore
    public final void setMTypedArray(TypedArray typedArray) {
        this.e = typedArray;
    }
}
