package com.portfolio.platform.view.fitness;

import android.graphics.Canvas;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import java.util.LinkedList;
import java.util.List;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityMonthDetailsChart$draw$$inlined$let$lambda$Anon1 extends Lambda implements xc4<LinkedList<Integer>, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ Canvas $canvas$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $chartHeight;
    @DexIgnore
    public /* final */ /* synthetic */ int $chartWidth;
    @DexIgnore
    public /* final */ /* synthetic */ ActivityMonthDetailsChart this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityMonthDetailsChart$draw$$inlined$let$lambda$Anon1(int i, int i2, ActivityMonthDetailsChart activityMonthDetailsChart, Canvas canvas) {
        super(1);
        this.$chartWidth = i;
        this.$chartHeight = i2;
        this.this$Anon0 = activityMonthDetailsChart;
        this.$canvas$inlined = canvas;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((LinkedList<Integer>) (LinkedList) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(LinkedList<Integer> linkedList) {
        kd4.b(linkedList, "barCenterXList");
        this.this$Anon0.a(this.$canvas$inlined, linkedList);
        ActivityMonthDetailsChart activityMonthDetailsChart = this.this$Anon0;
        Canvas canvas = this.$canvas$inlined;
        int i = this.$chartWidth;
        int i2 = this.$chartHeight;
        activityMonthDetailsChart.a(canvas, (List<Integer>) linkedList, i, i2, i2 - 4);
    }
}
