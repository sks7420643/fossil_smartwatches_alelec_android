package com.portfolio.platform.view.cardstackview;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.mt3;
import com.fossil.blesdk.obfuscated.ts3;
import com.fossil.blesdk.obfuscated.u8;
import com.fossil.wearables.fossil.R;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class CardContainerView extends FrameLayout {
    @DexIgnore
    public mt3 e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public ViewGroup l;
    @DexIgnore
    public ViewGroup m;
    @DexIgnore
    public View n;
    @DexIgnore
    public View o;
    @DexIgnore
    public View p;
    @DexIgnore
    public View q;
    @DexIgnore
    public c r;
    @DexIgnore
    public GestureDetector.SimpleOnGestureListener s;
    @DexIgnore
    public GestureDetector t;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            c cVar = CardContainerView.this.r;
            if (cVar == null) {
                return true;
            }
            cVar.a();
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class b {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[Quadrant.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /*
        static {
            a[Quadrant.TopLeft.ordinal()] = 1;
            a[Quadrant.TopRight.ordinal()] = 2;
            a[Quadrant.BottomLeft.ordinal()] = 3;
            try {
                a[Quadrant.BottomRight.ordinal()] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();

        @DexIgnore
        void a(float f, float f2);

        @DexIgnore
        void a(Point point, SwipeDirection swipeDirection);

        @DexIgnore
        void b();
    }

    @DexIgnore
    public CardContainerView(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a(MotionEvent motionEvent) {
        this.h = motionEvent.getRawX();
        this.i = motionEvent.getRawY();
    }

    @DexIgnore
    public final void b(MotionEvent motionEvent) {
        this.j = true;
        d(motionEvent);
        h();
        g();
        c cVar = this.r;
        if (cVar != null) {
            cVar.a(getPercentX(), getPercentY());
        }
    }

    @DexIgnore
    public final void c(MotionEvent motionEvent) {
        float f2;
        if (this.j) {
            this.j = false;
            float rawX = motionEvent.getRawX();
            float rawY = motionEvent.getRawY();
            Point c2 = ts3.c(this.h, this.i, rawX, rawY);
            Quadrant a2 = ts3.a(this.h, this.i, rawX, rawY);
            double b2 = ts3.b(this.h, this.i, rawX, rawY);
            SwipeDirection swipeDirection = null;
            int i2 = b.a[a2.ordinal()];
            if (i2 != 1) {
                if (i2 != 2) {
                    if (i2 != 3) {
                        if (i2 == 4) {
                            if (Math.cos(Math.toRadians(360.0d - Math.toDegrees(b2))) < 0.5d) {
                                swipeDirection = SwipeDirection.Bottom;
                            } else {
                                swipeDirection = SwipeDirection.Right;
                            }
                        }
                    } else if (Math.cos(Math.toRadians(Math.toDegrees(b2) + 180.0d)) < -0.5d) {
                        swipeDirection = SwipeDirection.Left;
                    } else {
                        swipeDirection = SwipeDirection.Bottom;
                    }
                } else if (Math.cos(Math.toRadians(Math.toDegrees(b2))) < 0.5d) {
                    swipeDirection = SwipeDirection.Top;
                } else {
                    swipeDirection = SwipeDirection.Right;
                }
            } else if (Math.cos(Math.toRadians(180.0d - Math.toDegrees(b2))) < -0.5d) {
                swipeDirection = SwipeDirection.Left;
            } else {
                swipeDirection = SwipeDirection.Top;
            }
            if (swipeDirection == SwipeDirection.Left || swipeDirection == SwipeDirection.Right) {
                f2 = getPercentX();
            } else {
                f2 = getPercentY();
            }
            float abs = Math.abs(f2);
            mt3 mt3 = this.e;
            if (abs <= mt3.b) {
                a();
                c cVar = this.r;
                if (cVar != null) {
                    cVar.b();
                }
            } else if (mt3.l.contains(swipeDirection)) {
                c cVar2 = this.r;
                if (cVar2 != null) {
                    cVar2.a(c2, swipeDirection);
                }
            } else {
                a();
                c cVar3 = this.r;
                if (cVar3 != null) {
                    cVar3.b();
                }
            }
        }
        this.h = motionEvent.getRawX();
        this.i = motionEvent.getRawY();
    }

    @DexIgnore
    public final void d(MotionEvent motionEvent) {
        setTranslationX((this.f + motionEvent.getRawX()) - this.h);
        setTranslationY((this.g + motionEvent.getRawY()) - this.i);
    }

    @DexIgnore
    public void e() {
        View view = this.n;
        if (view != null) {
            view.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view2 = this.p;
        if (view2 != null) {
            view2.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view3 = this.q;
        if (view3 != null) {
            view3.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view4 = this.o;
        if (view4 != null) {
            view4.setAlpha(1.0f);
        }
    }

    @DexIgnore
    public void f() {
        View view = this.n;
        if (view != null) {
            view.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view2 = this.p;
        if (view2 != null) {
            view2.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view3 = this.q;
        if (view3 != null) {
            view3.setAlpha(1.0f);
        }
        View view4 = this.o;
        if (view4 != null) {
            view4.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public final void g() {
        float percentX = getPercentX();
        float percentY = getPercentY();
        List<SwipeDirection> list = this.e.l;
        if (list == SwipeDirection.HORIZONTAL) {
            a(percentX);
        } else if (list == SwipeDirection.VERTICAL) {
            b(percentY);
        } else if (list == SwipeDirection.FREEDOM_NO_BOTTOM) {
            if (Math.abs(percentX) >= Math.abs(percentY) || percentY >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                a(percentX);
                return;
            }
            f();
            setOverlayAlpha(Math.abs(percentY));
        } else if (list == SwipeDirection.FREEDOM) {
            if (Math.abs(percentX) > Math.abs(percentY)) {
                a(percentX);
            } else {
                b(percentY);
            }
        } else if (Math.abs(percentX) > Math.abs(percentY)) {
            if (percentX < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                d();
            } else {
                e();
            }
            setOverlayAlpha(Math.abs(percentX));
        } else {
            if (percentY < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                f();
            } else {
                c();
            }
            setOverlayAlpha(Math.abs(percentY));
        }
    }

    @DexIgnore
    public ViewGroup getContentContainer() {
        return this.l;
    }

    @DexIgnore
    public ViewGroup getOverlayContainer() {
        return this.m;
    }

    @DexIgnore
    public float getPercentX() {
        float translationX = ((getTranslationX() - this.f) * 2.0f) / ((float) getWidth());
        if (translationX > 1.0f) {
            translationX = 1.0f;
        }
        if (translationX < -1.0f) {
            return -1.0f;
        }
        return translationX;
    }

    @DexIgnore
    public float getPercentY() {
        float translationY = ((getTranslationY() - this.g) * 2.0f) / ((float) getHeight());
        if (translationY > 1.0f) {
            translationY = 1.0f;
        }
        if (translationY < -1.0f) {
            return -1.0f;
        }
        return translationY;
    }

    @DexIgnore
    public float getViewOriginX() {
        return this.f;
    }

    @DexIgnore
    public float getViewOriginY() {
        return this.g;
    }

    @DexIgnore
    public final void h() {
        setRotation(getPercentX() * 20.0f);
    }

    @DexIgnore
    public void onFinishInflate() {
        super.onFinishInflate();
        FrameLayout.inflate(getContext(), R.layout.card_frame, this);
        this.l = (ViewGroup) findViewById(R.id.card_frame_content_container);
        this.m = (ViewGroup) findViewById(R.id.card_frame_overlay_container);
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.t.onTouchEvent(motionEvent);
        if (this.e.g && this.k) {
            int a2 = u8.a(motionEvent);
            if (a2 == 0) {
                a(motionEvent);
                getParent().getParent().requestDisallowInterceptTouchEvent(true);
            } else if (a2 == 1) {
                c(motionEvent);
                getParent().getParent().requestDisallowInterceptTouchEvent(false);
            } else if (a2 == 2) {
                b(motionEvent);
            } else if (a2 == 3) {
                getParent().getParent().requestDisallowInterceptTouchEvent(false);
            }
        }
        return true;
    }

    @DexIgnore
    public void setCardStackOption(mt3 mt3) {
        this.e = mt3;
    }

    @DexIgnore
    public void setContainerEventListener(c cVar) {
        this.r = cVar;
        this.f = getTranslationX();
        this.g = getTranslationY();
    }

    @DexIgnore
    public void setDraggable(boolean z) {
        this.k = z;
    }

    @DexIgnore
    public void setOverlayAlpha(AnimatorSet animatorSet) {
        if (animatorSet != null) {
            animatorSet.start();
        }
    }

    @DexIgnore
    public CardContainerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public void setOverlayAlpha(float f2) {
        this.m.setAlpha(f2);
    }

    @DexIgnore
    public CardContainerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.g = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.h = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.i = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.j = false;
        this.k = true;
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.s = new a();
        this.t = new GestureDetector(getContext(), this.s);
    }

    @DexIgnore
    public final void a(float f2) {
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            d();
        } else {
            e();
        }
        setOverlayAlpha(Math.abs(f2));
    }

    @DexIgnore
    public void d() {
        View view = this.n;
        if (view != null) {
            view.setAlpha(1.0f);
        }
        View view2 = this.o;
        if (view2 != null) {
            view2.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view3 = this.p;
        if (view3 != null) {
            view3.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view4 = this.q;
        if (view4 != null) {
            view4.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public final void a() {
        animate().translationX(this.f).translationY(this.g).setDuration(300).setInterpolator(new OvershootInterpolator(1.0f)).setListener((Animator.AnimatorListener) null).start();
    }

    @DexIgnore
    public final void b(float f2) {
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            f();
        } else {
            c();
        }
        setOverlayAlpha(Math.abs(f2));
    }

    @DexIgnore
    public void b() {
        this.l.setAlpha(1.0f);
        this.m.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5) {
        View view = this.n;
        if (view != null) {
            this.m.removeView(view);
        }
        if (i2 != 0) {
            this.n = LayoutInflater.from(getContext()).inflate(i2, this.m, false);
            this.m.addView(this.n);
            this.n.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view2 = this.o;
        if (view2 != null) {
            this.m.removeView(view2);
        }
        if (i3 != 0) {
            this.o = LayoutInflater.from(getContext()).inflate(i3, this.m, false);
            this.m.addView(this.o);
            this.o.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view3 = this.p;
        if (view3 != null) {
            this.m.removeView(view3);
        }
        if (i4 != 0) {
            this.p = LayoutInflater.from(getContext()).inflate(i4, this.m, false);
            this.m.addView(this.p);
            this.p.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view4 = this.q;
        if (view4 != null) {
            this.m.removeView(view4);
        }
        if (i5 != 0) {
            this.q = LayoutInflater.from(getContext()).inflate(i5, this.m, false);
            this.m.addView(this.q);
            this.q.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public void c() {
        View view = this.n;
        if (view != null) {
            view.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view2 = this.p;
        if (view2 != null) {
            view2.setAlpha(1.0f);
        }
        View view3 = this.q;
        if (view3 != null) {
            view3.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view4 = this.o;
        if (view4 != null) {
            view4.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }
}
