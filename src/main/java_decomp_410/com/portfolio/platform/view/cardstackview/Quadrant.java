package com.portfolio.platform.view.cardstackview;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum Quadrant {
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight
}
