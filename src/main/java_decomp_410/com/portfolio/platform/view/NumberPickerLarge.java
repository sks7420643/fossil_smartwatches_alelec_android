package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatEditText;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.ct3;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.r6;
import com.fossil.wearables.fossil.R;
import java.io.Serializable;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class NumberPickerLarge extends LinearLayout {
    @DexIgnore
    public static /* final */ TwoDigitFormatter m0; // = new TwoDigitFormatter();
    @DexIgnore
    public /* final */ Drawable A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public /* final */ ct3 E;
    @DexIgnore
    public /* final */ ct3 F;
    @DexIgnore
    public int G;
    @DexIgnore
    public i H;
    @DexIgnore
    public e I;
    @DexIgnore
    public d J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public VelocityTracker M;
    @DexIgnore
    public int N;
    @DexIgnore
    public int O;
    @DexIgnore
    public int P;
    @DexIgnore
    public boolean Q;
    @DexIgnore
    public /* final */ int R;
    @DexIgnore
    public /* final */ int S;
    @DexIgnore
    public /* final */ int T;
    @DexIgnore
    public /* final */ boolean U;
    @DexIgnore
    public /* final */ Drawable V;
    @DexIgnore
    public /* final */ int W;
    @DexIgnore
    public int a0;
    @DexIgnore
    public boolean b0;
    @DexIgnore
    public boolean c0;
    @DexIgnore
    public int d0;
    @DexIgnore
    public /* final */ ImageButton e;
    @DexIgnore
    public int e0;
    @DexIgnore
    public /* final */ ImageButton f;
    @DexIgnore
    public int f0;
    @DexIgnore
    public /* final */ AppCompatEditText g;
    @DexIgnore
    public boolean g0;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public boolean h0;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public j i0;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ h j0;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public int k0;
    @DexIgnore
    public int l;
    @DexIgnore
    public Typeface l0;
    @DexIgnore
    public /* final */ boolean m;
    @DexIgnore
    public /* final */ int n;
    @DexIgnore
    public int o;
    @DexIgnore
    public String[] p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public g t;
    @DexIgnore
    public f u;
    @DexIgnore
    public Formatter v;
    @DexIgnore
    public /* final */ SparseArray<String> w;
    @DexIgnore
    public /* final */ int[] x;
    @DexIgnore
    public /* final */ Paint y;
    @DexIgnore
    public /* final */ Paint z;

    @DexIgnore
    public interface Formatter extends Serializable {
        @DexIgnore
        String format(int i);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class TwoDigitFormatter implements Formatter {
        @DexIgnore
        public /* final */ Object[] mArgs; // = new Object[1];
        @DexIgnore
        public /* final */ StringBuilder mBuilder; // = new StringBuilder();
        @DexIgnore
        public transient java.util.Formatter mFmt;
        @DexIgnore
        public char mZeroDigit;

        @DexIgnore
        public TwoDigitFormatter() {
            b(Locale.getDefault());
        }

        @DexIgnore
        public static char c(Locale locale) {
            return new DecimalFormatSymbols(locale).getZeroDigit();
        }

        @DexIgnore
        public final java.util.Formatter a(Locale locale) {
            return new java.util.Formatter(this.mBuilder, locale);
        }

        @DexIgnore
        public final void b(Locale locale) {
            this.mFmt = a(locale);
            this.mZeroDigit = c(locale);
        }

        @DexIgnore
        public String format(int i) {
            Locale locale = Locale.getDefault();
            if (this.mZeroDigit != c(locale)) {
                b(locale);
            }
            this.mArgs[0] = Integer.valueOf(i);
            StringBuilder sb = this.mBuilder;
            sb.delete(0, sb.length());
            this.mFmt.format("%02d", this.mArgs);
            return this.mFmt.toString();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            NumberPickerLarge.this.b();
            NumberPickerLarge.this.g.clearFocus();
            if (view.getId() == R.id.np__increment) {
                NumberPickerLarge.this.a(true);
            } else {
                NumberPickerLarge.this.a(false);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnLongClickListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            NumberPickerLarge.this.b();
            NumberPickerLarge.this.g.clearFocus();
            if (view.getId() == R.id.np__increment) {
                NumberPickerLarge.this.a(true, 0);
            } else {
                NumberPickerLarge.this.a(false, 0);
            }
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            NumberPickerLarge.this.j();
            NumberPickerLarge.this.b0 = true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public boolean e;

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a(boolean z) {
            this.e = z;
        }

        @DexIgnore
        public void run() {
            NumberPickerLarge.this.a(this.e);
            NumberPickerLarge.this.postDelayed(this, 300);
        }
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        void a(NumberPickerLarge numberPickerLarge, int i);
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(NumberPickerLarge numberPickerLarge, int i, int i2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements Runnable {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class j {
        @DexIgnore
        public c a;

        @DexIgnore
        public j(NumberPickerLarge numberPickerLarge) {
            this.a = new c();
        }

        @DexIgnore
        public boolean a(int i, int i2, Bundle bundle) {
            c cVar = this.a;
            return cVar != null && cVar.performAction(i, i2, bundle);
        }

        @DexIgnore
        public void a(int i, int i2) {
            c cVar = this.a;
            if (cVar != null) {
                cVar.a(i, i2);
            }
        }
    }

    @DexIgnore
    public NumberPickerLarge(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    private j getSupportAccessibilityNodeProvider() {
        return new j(this);
    }

    @DexIgnore
    public static Formatter getTwoDigitFormatter() {
        return m0;
    }

    @DexIgnore
    public static int resolveSizeAndState(int i2, int i3, int i4) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode != Integer.MIN_VALUE) {
            if (mode != 0 && mode == 1073741824) {
                i2 = size;
            }
        } else if (size < i2) {
            i2 = 16777216 | size;
        }
        return i2 | (-16777216 & i4);
    }

    @DexIgnore
    private void setWrapSelectorWheel(boolean z2) {
        boolean z3 = this.r - this.q >= this.x.length;
        if ((!z2 || z3) && z2 != this.Q) {
            this.Q = z2;
        }
    }

    @DexIgnore
    public final boolean a(ct3 ct3) {
        ct3.a(true);
        int d2 = ct3.d() - ct3.c();
        int i2 = this.C - ((this.D + d2) % this.B);
        if (i2 == 0) {
            return false;
        }
        int abs = Math.abs(i2);
        int i3 = this.B;
        if (abs > i3 / 2) {
            i2 = i2 > 0 ? i2 - i3 : i2 + i3;
        }
        scrollBy(0, d2 + i2);
        return true;
    }

    @DexIgnore
    public void b() {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null && inputMethodManager.isActive(this.g)) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            if (this.U) {
                this.g.setVisibility(4);
            }
        }
    }

    @DexIgnore
    public final void c() {
        setVerticalFadingEdgeEnabled(true);
        setFadingEdgeLength(((getBottom() - getTop()) - this.n) / 2);
    }

    @DexIgnore
    public void computeScroll() {
        ct3 ct3 = this.E;
        if (ct3.f()) {
            ct3 = this.F;
            if (ct3.f()) {
                return;
            }
        }
        ct3.a();
        int c2 = ct3.c();
        if (this.G == 0) {
            this.G = ct3.e();
        }
        scrollBy(0, c2 - this.G);
        this.G = c2;
        if (ct3.f()) {
            b(ct3);
        } else {
            invalidate();
        }
    }

    @DexIgnore
    public final void d() {
        e();
        int[] iArr = this.x;
        this.o = (int) ((((float) ((getBottom() - getTop()) - (iArr.length * this.n))) / ((float) iArr.length)) + 0.5f);
        this.B = this.n + this.o;
        this.C = (this.g.getBaseline() + this.g.getTop()) - (this.B * 2);
        this.D = this.C;
        l();
    }

    @DexIgnore
    public boolean dispatchHoverEvent(MotionEvent motionEvent) {
        int i2;
        if (!this.U) {
            return super.dispatchHoverEvent(motionEvent);
        }
        if (!((AccessibilityManager) getContext().getSystemService("accessibility")).isEnabled()) {
            return false;
        }
        int y2 = (int) motionEvent.getY();
        if (y2 < this.d0) {
            i2 = 3;
        } else {
            i2 = y2 > this.e0 ? 1 : 2;
        }
        int action = motionEvent.getAction() & 255;
        j supportAccessibilityNodeProvider = getSupportAccessibilityNodeProvider();
        if (action == 7) {
            int i3 = this.f0;
            if (i3 == i2 || i3 == -1) {
                return false;
            }
            supportAccessibilityNodeProvider.a(i3, 256);
            supportAccessibilityNodeProvider.a(i2, 128);
            this.f0 = i2;
            supportAccessibilityNodeProvider.a(i2, 64, (Bundle) null);
            return false;
        } else if (action == 9) {
            supportAccessibilityNodeProvider.a(i2, 128);
            this.f0 = i2;
            supportAccessibilityNodeProvider.a(i2, 64, (Bundle) null);
            return false;
        } else if (action != 10) {
            return false;
        } else {
            supportAccessibilityNodeProvider.a(i2, 256);
            this.f0 = -1;
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
        requestFocus();
        r5.k0 = r0;
        g();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
        if (r5.E.f() == false) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0054, code lost:
        if (r0 != 20) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0056, code lost:
        r6 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0058, code lost:
        r6 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0059, code lost:
        a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005c, code lost:
        return true;
     */
    @DexIgnore
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        if (keyCode == 19 || keyCode == 20) {
            if (this.U) {
                int action = keyEvent.getAction();
                if (action != 0) {
                    if (action == 1 && this.k0 == keyCode) {
                        this.k0 = -1;
                        return true;
                    }
                } else if (!this.Q) {
                }
            }
        } else if (keyCode == 23 || keyCode == 66) {
            g();
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    @DexIgnore
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 3) {
            g();
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    @DexIgnore
    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 3) {
            g();
        }
        return super.dispatchTrackballEvent(motionEvent);
    }

    @DexIgnore
    public final void e() {
        this.w.clear();
        int[] iArr = this.x;
        int i2 = this.s;
        for (int i3 = 0; i3 < this.x.length; i3++) {
            int i4 = (i3 - 2) + i2;
            if (this.Q) {
                i4 = d(i4);
            }
            iArr[i3] = i4;
            a(iArr[i3]);
        }
    }

    @DexIgnore
    public final void f(int i2) {
        if (this.a0 != i2) {
            this.a0 = i2;
            f fVar = this.u;
            if (fVar != null) {
                fVar.a(this, i2);
            }
        }
    }

    @DexIgnore
    public final void g() {
        e eVar = this.I;
        if (eVar != null) {
            removeCallbacks(eVar);
        }
        i iVar = this.H;
        if (iVar != null) {
            removeCallbacks(iVar);
        }
        d dVar = this.J;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
        this.j0.a();
    }

    @DexIgnore
    public AccessibilityNodeProvider getAccessibilityNodeProvider() {
        if (!this.U) {
            return super.getAccessibilityNodeProvider();
        }
        if (this.i0 == null) {
            this.i0 = new j(this);
        }
        return this.i0.a;
    }

    @DexIgnore
    public float getBottomFadingEdgeStrength() {
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public int getMaxValue() {
        return this.r;
    }

    @DexIgnore
    public int getMinValue() {
        return this.q;
    }

    @DexIgnore
    public int getSolidColor() {
        return this.R;
    }

    @DexIgnore
    public float getTopFadingEdgeStrength() {
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public int getValue() {
        return this.s;
    }

    @DexIgnore
    public boolean getWrapSelectorWheel() {
        return this.Q;
    }

    @DexIgnore
    public final void h() {
        d dVar = this.J;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
    }

    @DexIgnore
    public final void i() {
        e eVar = this.I;
        if (eVar != null) {
            removeCallbacks(eVar);
        }
    }

    @DexIgnore
    public void j() {
    }

    @DexIgnore
    public final void k() {
        int i2;
        if (this.m) {
            String[] strArr = this.p;
            int i3 = 0;
            if (strArr == null) {
                float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                for (int i4 = 0; i4 <= 9; i4++) {
                    float measureText = this.y.measureText(g(i4));
                    if (measureText > f2) {
                        f2 = measureText;
                    }
                }
                for (int i5 = this.r; i5 > 0; i5 /= 10) {
                    i3++;
                }
                i2 = (int) (((float) i3) * f2);
            } else {
                int length = strArr.length;
                int i6 = 0;
                while (i3 < length) {
                    float measureText2 = this.y.measureText(strArr[i3]);
                    if (measureText2 > ((float) i6)) {
                        i6 = (int) measureText2;
                    }
                    i3++;
                }
                i2 = i6;
            }
            int paddingLeft = i2 + this.g.getPaddingLeft() + this.g.getPaddingRight();
            if (this.l != paddingLeft) {
                int i7 = this.k;
                if (paddingLeft > i7) {
                    this.l = paddingLeft;
                } else {
                    this.l = i7;
                }
                invalidate();
            }
        }
    }

    @DexIgnore
    public final boolean l() {
        String[] strArr = this.p;
        String c2 = strArr == null ? c(this.s) : strArr[this.s - this.q];
        if (TextUtils.isEmpty(c2) || c2.equals(this.g.getText().toString())) {
            return false;
        }
        this.g.setText(c2);
        return true;
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        g();
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (!this.U) {
            super.onDraw(canvas);
            return;
        }
        float right = (float) ((getRight() - getLeft()) / 2);
        float f2 = (float) this.D;
        Drawable drawable = this.A;
        if (drawable != null && this.a0 == 0) {
            if (this.h0) {
                drawable.setState(LinearLayout.PRESSED_ENABLED_STATE_SET);
                this.A.setBounds(0, 0, getRight(), this.d0);
                this.A.draw(canvas);
            }
            if (this.g0) {
                this.A.setState(LinearLayout.PRESSED_ENABLED_STATE_SET);
                this.A.setBounds(0, this.e0, getRight(), getBottom());
                this.A.draw(canvas);
            }
        }
        int[] iArr = this.x;
        float f3 = f2;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            String str = this.w.get(iArr[i2]);
            if (i2 != 2) {
                canvas.drawText(str, right, f3, this.y);
            } else if (this.g.getVisibility() != 0) {
                canvas.drawText(str, right, f3, this.z);
            }
            f3 += (float) this.B;
        }
        Drawable drawable2 = this.V;
        if (drawable2 != null) {
            int i3 = this.d0;
            drawable2.setBounds(0, i3, getRight(), this.W + i3);
            this.V.draw(canvas);
            int i4 = this.e0;
            this.V.setBounds(0, i4 - this.W, getRight(), i4);
            this.V.draw(canvas);
        }
    }

    @DexIgnore
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(NumberPickerLarge.class.getName());
        accessibilityEvent.setScrollable(true);
        accessibilityEvent.setScrollY((this.q + this.s) * this.B);
        accessibilityEvent.setMaxScrollY((this.r - this.q) * this.B);
    }

    @DexIgnore
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.U || !isEnabled() || (motionEvent.getAction() & 255) != 0) {
            return false;
        }
        g();
        this.g.setVisibility(4);
        float y2 = motionEvent.getY();
        this.K = y2;
        this.L = y2;
        motionEvent.getEventTime();
        this.b0 = false;
        this.c0 = false;
        float f2 = this.K;
        if (f2 < ((float) this.d0)) {
            if (this.a0 == 0) {
                this.j0.a(2);
            }
        } else if (f2 > ((float) this.e0) && this.a0 == 0) {
            this.j0.a(1);
        }
        getParent().requestDisallowInterceptTouchEvent(true);
        if (!this.E.f()) {
            this.E.a(true);
            this.F.a(true);
            f(0);
        } else if (!this.F.f()) {
            this.E.a(true);
            this.F.a(true);
        } else {
            float f3 = this.K;
            if (f3 < ((float) this.d0)) {
                b();
                a(false, (long) ViewConfiguration.getLongPressTimeout());
            } else if (f3 > ((float) this.e0)) {
                b();
                a(true, (long) ViewConfiguration.getLongPressTimeout());
            } else {
                this.c0 = true;
                f();
            }
        }
        return true;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        if (!this.U) {
            super.onLayout(z2, i2, i3, i4, i5);
            return;
        }
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        int measuredWidth2 = this.g.getMeasuredWidth();
        int measuredHeight2 = this.g.getMeasuredHeight();
        int i6 = (measuredWidth - measuredWidth2) / 2;
        int i7 = (measuredHeight - measuredHeight2) / 2;
        this.g.layout(i6, i7, measuredWidth2 + i6, measuredHeight2 + i7);
        if (z2) {
            d();
            c();
            int height = getHeight();
            int i8 = this.h;
            int i9 = this.W;
            this.d0 = ((height - i8) / 2) - i9;
            this.e0 = this.d0 + (i9 * 2) + i8;
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (!this.U) {
            super.onMeasure(i2, i3);
            return;
        }
        super.onMeasure(a(i2, this.l), a(i3, this.j));
        setMeasuredDimension(a(this.k, getMeasuredWidth(), i2), a(this.i, getMeasuredHeight(), i3));
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!isEnabled() || !this.U) {
            return false;
        }
        if (this.M == null) {
            this.M = VelocityTracker.obtain();
        }
        this.M.addMovement(motionEvent);
        int action = motionEvent.getAction() & 255;
        if (action == 1) {
            h();
            i();
            this.j0.a();
            VelocityTracker velocityTracker = this.M;
            velocityTracker.computeCurrentVelocity(1000, (float) this.P);
            int yVelocity = (int) velocityTracker.getYVelocity();
            if (Math.abs(yVelocity) > this.O) {
                b(yVelocity);
                f(2);
            } else {
                int y2 = (int) motionEvent.getY();
                if (((int) Math.abs(((float) y2) - this.K)) > this.N) {
                    a();
                } else if (this.c0) {
                    this.c0 = false;
                    j();
                } else {
                    int i2 = (y2 / this.B) - 2;
                    if (i2 > 0) {
                        a(true);
                        this.j0.b(1);
                    } else if (i2 < 0) {
                        a(false);
                        this.j0.b(2);
                    }
                }
                f(0);
            }
            this.M.recycle();
            this.M = null;
        } else if (action == 2 && !this.b0) {
            float y3 = motionEvent.getY();
            if (this.a0 == 1) {
                scrollBy(0, (int) (y3 - this.L));
                invalidate();
            } else if (((int) Math.abs(y3 - this.K)) > this.N) {
                g();
                f(1);
            }
            this.L = y3;
        }
        return true;
    }

    @DexIgnore
    public void scrollBy(int i2, int i3) {
        int[] iArr = this.x;
        if (!this.Q && i3 > 0 && iArr[2] <= this.q) {
            this.D = this.C;
        } else if (this.Q || i3 >= 0 || iArr[2] < this.r) {
            this.D += i3;
            while (true) {
                int i4 = this.D;
                if (i4 - this.C <= this.o) {
                    break;
                }
                this.D = i4 - this.B;
                a(iArr);
                a(iArr[2], true);
                if (!this.Q && iArr[2] <= this.q) {
                    this.D = this.C;
                }
            }
            while (true) {
                int i5 = this.D;
                if (i5 - this.C < (-this.o)) {
                    this.D = i5 + this.B;
                    b(iArr);
                    a(iArr[2], true);
                    if (!this.Q && iArr[2] >= this.r) {
                        this.D = this.C;
                    }
                } else {
                    return;
                }
            }
        } else {
            this.D = this.C;
        }
    }

    @DexIgnore
    public void setDisplayedValues(String[] strArr) {
        if (!Arrays.equals(this.p, strArr)) {
            this.p = strArr;
            if (this.p != null) {
                this.g.setRawInputType(524289);
            } else {
                this.g.setRawInputType(2);
            }
            l();
            e();
            k();
        }
    }

    @DexIgnore
    public void setEnabled(boolean z2) {
        super.setEnabled(z2);
        if (!this.U) {
            ImageButton imageButton = this.e;
            if (imageButton != null) {
                imageButton.setEnabled(z2);
            }
        }
        if (!this.U) {
            ImageButton imageButton2 = this.f;
            if (imageButton2 != null) {
                imageButton2.setEnabled(z2);
            }
        }
        this.g.setEnabled(z2);
    }

    @DexIgnore
    public void setFormatter(Formatter formatter) {
        if (!Objects.equals(formatter, this.v)) {
            this.v = formatter;
            e();
            l();
        }
    }

    @DexIgnore
    public void setMaxValue(int i2) {
        if (this.r != i2) {
            if (i2 >= 0) {
                this.r = i2;
                int i3 = this.r;
                if (i3 < this.s) {
                    this.s = i3;
                }
                setWrapSelectorWheel(this.r - this.q > this.x.length);
                e();
                l();
                k();
                invalidate();
                return;
            }
            throw new IllegalArgumentException("maxValue must be >= 0");
        }
    }

    @DexIgnore
    public void setMinValue(int i2) {
        if (this.q != i2) {
            if (i2 >= 0) {
                this.q = i2;
                int i3 = this.q;
                if (i3 > this.s) {
                    this.s = i3;
                }
                setWrapSelectorWheel(this.r - this.q > this.x.length);
                e();
                l();
                k();
                invalidate();
                return;
            }
            throw new IllegalArgumentException("minValue must be >= 0");
        }
    }

    @DexIgnore
    public void setOnValueChangedListener(g gVar) {
        this.t = gVar;
    }

    @DexIgnore
    public void setValue(int i2) {
        a(i2, false);
    }

    @DexIgnore
    public NumberPickerLarge(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.numberPickerStyle);
    }

    @DexIgnore
    public NumberPickerLarge(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        this.w = new SparseArray<>();
        this.x = new int[5];
        this.C = Integer.MIN_VALUE;
        this.a0 = 0;
        this.k0 = -1;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.NumberPicker, i2, 0);
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        this.U = resourceId != 0;
        this.R = obtainStyledAttributes.getColor(12, 0);
        this.S = obtainStyledAttributes.getColor(7, 0);
        this.T = obtainStyledAttributes.getColor(13, 0);
        this.V = obtainStyledAttributes.getDrawable(9);
        this.W = obtainStyledAttributes.getDimensionPixelSize(10, (int) TypedValue.applyDimension(1, 2.0f, getResources().getDisplayMetrics()));
        this.h = obtainStyledAttributes.getDimensionPixelSize(11, (int) TypedValue.applyDimension(1, 48.0f, getResources().getDisplayMetrics()));
        this.i = obtainStyledAttributes.getDimensionPixelSize(3, -1);
        this.j = obtainStyledAttributes.getDimensionPixelSize(1, -1);
        int i3 = this.i;
        if (i3 != -1) {
            int i4 = this.j;
            if (i4 != -1 && i3 > i4) {
                throw new IllegalArgumentException("minHeight > maxHeight");
            }
        }
        this.k = obtainStyledAttributes.getDimensionPixelSize(4, -1);
        this.l = obtainStyledAttributes.getDimensionPixelSize(2, -1);
        int i5 = this.k;
        if (i5 != -1) {
            int i6 = this.l;
            if (i6 != -1 && i5 > i6) {
                throw new IllegalArgumentException("minWidth > maxWidth");
            }
        }
        this.m = this.l == -1;
        this.A = obtainStyledAttributes.getDrawable(14);
        String string = obtainStyledAttributes.getString(6);
        if (obtainStyledAttributes.hasValue(5)) {
            this.l0 = r6.a(getContext(), obtainStyledAttributes.getResourceId(5, -1));
        }
        obtainStyledAttributes.recycle();
        this.j0 = new h();
        setWillNotDraw(!this.U);
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
        if (layoutInflater != null) {
            layoutInflater.inflate(resourceId, this, true);
        }
        a aVar = new a();
        b bVar = new b();
        if (!this.U) {
            this.e = (ImageButton) findViewById(R.id.np__increment);
            this.e.setOnClickListener(aVar);
            this.e.setOnLongClickListener(bVar);
        } else {
            this.e = null;
        }
        if (!this.U) {
            this.f = (ImageButton) findViewById(R.id.np__decrement);
            this.f.setOnClickListener(aVar);
            this.f.setOnLongClickListener(bVar);
        } else {
            this.f = null;
        }
        this.g = (AppCompatEditText) findViewById(R.id.np__numberpicker_input);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.N = viewConfiguration.getScaledTouchSlop();
        this.O = viewConfiguration.getScaledMinimumFlingVelocity();
        this.P = viewConfiguration.getScaledMaximumFlingVelocity() / 8;
        this.n = (int) this.g.getTextSize();
        Paint paint = new Paint(1);
        paint.setAntiAlias(true);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize((float) this.n);
        Typeface typeface = this.l0;
        if (typeface != null) {
            paint.setTypeface(typeface);
        } else {
            paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), string));
        }
        int i7 = this.T;
        this.g.setTextColor(new ColorStateList(new int[][]{new int[]{16842910}, new int[]{-16842910}, new int[]{-16842912}, new int[]{16842919}}, new int[]{i7, i7, i7, i7}));
        paint.setColor(this.T);
        this.y = paint;
        Paint paint2 = new Paint(1);
        paint2.setAntiAlias(true);
        paint2.setTextAlign(Paint.Align.CENTER);
        paint2.setTextSize((float) this.n);
        Typeface typeface2 = this.l0;
        if (typeface2 != null) {
            paint2.setTypeface(typeface2);
        } else {
            paint2.setTypeface(Typeface.createFromAsset(getResources().getAssets(), string));
        }
        paint2.setColor(this.S);
        this.z = paint2;
        this.E = new ct3(getContext(), (Interpolator) null, true);
        this.F = new ct3(getContext(), new DecelerateInterpolator(2.5f));
        l();
        if (getImportantForAccessibility() == 0) {
            setImportantForAccessibility(1);
        }
    }

    @DexIgnore
    public String c(int i2) {
        Formatter formatter = this.v;
        return formatter != null ? formatter.format(i2) : g(i2);
    }

    @DexIgnore
    public final void f() {
        d dVar = this.J;
        if (dVar == null) {
            this.J = new d();
        } else {
            removeCallbacks(dVar);
        }
        postDelayed(this.J, (long) ViewConfiguration.getLongPressTimeout());
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements Runnable {
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;

        @DexIgnore
        public h() {
        }

        @DexIgnore
        public void a() {
            this.f = 0;
            this.e = 0;
            NumberPickerLarge.this.removeCallbacks(this);
            NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
            if (numberPickerLarge.g0) {
                numberPickerLarge.g0 = false;
                numberPickerLarge.invalidate(0, numberPickerLarge.e0, numberPickerLarge.getRight(), NumberPickerLarge.this.getBottom());
            }
        }

        @DexIgnore
        public void b(int i) {
            a();
            this.f = 2;
            this.e = i;
            NumberPickerLarge.this.post(this);
        }

        @DexIgnore
        public void run() {
            int i = this.f;
            if (i == 1) {
                int i2 = this.e;
                if (i2 == 1) {
                    NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                    numberPickerLarge.g0 = true;
                    numberPickerLarge.invalidate(0, numberPickerLarge.e0, numberPickerLarge.getRight(), NumberPickerLarge.this.getBottom());
                } else if (i2 == 2) {
                    NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
                    numberPickerLarge2.h0 = true;
                    numberPickerLarge2.invalidate(0, 0, numberPickerLarge2.getRight(), NumberPickerLarge.this.d0);
                }
            } else if (i == 2) {
                int i3 = this.e;
                if (i3 == 1) {
                    NumberPickerLarge numberPickerLarge3 = NumberPickerLarge.this;
                    if (!numberPickerLarge3.g0) {
                        numberPickerLarge3.postDelayed(this, (long) ViewConfiguration.getPressedStateDuration());
                    }
                    NumberPickerLarge numberPickerLarge4 = NumberPickerLarge.this;
                    numberPickerLarge4.g0 = !numberPickerLarge4.g0;
                    numberPickerLarge4.invalidate(0, numberPickerLarge4.e0, numberPickerLarge4.getRight(), NumberPickerLarge.this.getBottom());
                } else if (i3 == 2) {
                    NumberPickerLarge numberPickerLarge5 = NumberPickerLarge.this;
                    if (!numberPickerLarge5.h0) {
                        numberPickerLarge5.postDelayed(this, (long) ViewConfiguration.getPressedStateDuration());
                    }
                    NumberPickerLarge numberPickerLarge6 = NumberPickerLarge.this;
                    numberPickerLarge6.h0 = !numberPickerLarge6.h0;
                    numberPickerLarge6.invalidate(0, 0, numberPickerLarge6.getRight(), NumberPickerLarge.this.d0);
                }
            }
        }

        @DexIgnore
        public void a(int i) {
            a();
            this.f = 1;
            this.e = i;
            NumberPickerLarge.this.postDelayed(this, (long) ViewConfiguration.getTapTimeout());
        }
    }

    @DexIgnore
    public final void b(ct3 ct3) {
        if (Objects.equals(ct3, this.E)) {
            if (!a()) {
                l();
            }
            f(0);
        } else if (this.a0 != 1) {
            l();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends AccessibilityNodeProvider {
        @DexIgnore
        public /* final */ Rect a; // = new Rect();
        @DexIgnore
        public /* final */ int[] b; // = new int[2];
        @DexIgnore
        public int c; // = Integer.MIN_VALUE;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(int i, int i2) {
            if (i != 1) {
                if (i == 2) {
                    a(i2);
                } else if (i == 3 && d()) {
                    a(i, i2, b());
                }
            } else if (e()) {
                a(i, i2, c());
            }
        }

        @DexIgnore
        public final String b() {
            NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
            int i = numberPickerLarge.s - 1;
            if (numberPickerLarge.Q) {
                i = numberPickerLarge.d(i);
            }
            NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
            int i2 = numberPickerLarge2.q;
            if (i < i2) {
                return null;
            }
            String[] strArr = numberPickerLarge2.p;
            return strArr == null ? numberPickerLarge2.c(i) : strArr[i - i2];
        }

        @DexIgnore
        public final String c() {
            NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
            int i = numberPickerLarge.s + 1;
            if (numberPickerLarge.Q) {
                i = numberPickerLarge.d(i);
            }
            NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
            if (i > numberPickerLarge2.r) {
                return null;
            }
            String[] strArr = numberPickerLarge2.p;
            return strArr == null ? numberPickerLarge2.c(i) : strArr[i - numberPickerLarge2.q];
        }

        @DexIgnore
        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            if (i == -1) {
                return a(NumberPickerLarge.this.getScrollX(), NumberPickerLarge.this.getScrollY(), NumberPickerLarge.this.getScrollX() + (NumberPickerLarge.this.getRight() - NumberPickerLarge.this.getLeft()), NumberPickerLarge.this.getScrollY() + (NumberPickerLarge.this.getBottom() - NumberPickerLarge.this.getTop()));
            }
            if (i == 1) {
                String c2 = c();
                int scrollX = NumberPickerLarge.this.getScrollX();
                NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                return a(1, c2, scrollX, numberPickerLarge.e0 - numberPickerLarge.W, numberPickerLarge.getScrollX() + (NumberPickerLarge.this.getRight() - NumberPickerLarge.this.getLeft()), NumberPickerLarge.this.getScrollY() + (NumberPickerLarge.this.getBottom() - NumberPickerLarge.this.getTop()));
            } else if (i == 2) {
                return a();
            } else {
                if (i != 3) {
                    return super.createAccessibilityNodeInfo(i);
                }
                String b2 = b();
                int scrollX2 = NumberPickerLarge.this.getScrollX();
                int scrollY = NumberPickerLarge.this.getScrollY();
                int scrollX3 = NumberPickerLarge.this.getScrollX() + (NumberPickerLarge.this.getRight() - NumberPickerLarge.this.getLeft());
                NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
                return a(3, b2, scrollX2, scrollY, scrollX3, numberPickerLarge2.d0 + numberPickerLarge2.W);
            }
        }

        @DexIgnore
        public final boolean d() {
            return NumberPickerLarge.this.getWrapSelectorWheel() || NumberPickerLarge.this.getValue() > NumberPickerLarge.this.getMinValue();
        }

        @DexIgnore
        public final boolean e() {
            return NumberPickerLarge.this.getWrapSelectorWheel() || NumberPickerLarge.this.getValue() < NumberPickerLarge.this.getMaxValue();
        }

        @DexIgnore
        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            if (TextUtils.isEmpty(str)) {
                return Collections.emptyList();
            }
            String lowerCase = str.toLowerCase();
            ArrayList arrayList = new ArrayList();
            if (i == -1) {
                a(lowerCase, 3, (List<AccessibilityNodeInfo>) arrayList);
                a(lowerCase, 2, (List<AccessibilityNodeInfo>) arrayList);
                a(lowerCase, 1, (List<AccessibilityNodeInfo>) arrayList);
                return arrayList;
            } else if (i != 1 && i != 2 && i != 3) {
                return super.findAccessibilityNodeInfosByText(str, i);
            } else {
                a(lowerCase, i, (List<AccessibilityNodeInfo>) arrayList);
                return arrayList;
            }
        }

        @DexIgnore
        public boolean performAction(int i, int i2, Bundle bundle) {
            if (i != -1) {
                if (i != 1) {
                    if (i != 2) {
                        if (i == 3) {
                            if (i2 != 16) {
                                if (i2 != 64) {
                                    if (i2 != 128 || this.c != i) {
                                        return false;
                                    }
                                    this.c = Integer.MIN_VALUE;
                                    a(i, 65536);
                                    NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                                    numberPickerLarge.invalidate(0, 0, numberPickerLarge.getRight(), NumberPickerLarge.this.d0);
                                    return true;
                                } else if (this.c == i) {
                                    return false;
                                } else {
                                    this.c = i;
                                    a(i, 32768);
                                    NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
                                    numberPickerLarge2.invalidate(0, 0, numberPickerLarge2.getRight(), NumberPickerLarge.this.d0);
                                    return true;
                                }
                            } else if (!NumberPickerLarge.this.isEnabled()) {
                                return false;
                            } else {
                                NumberPickerLarge.this.a(false);
                                a(i, 1);
                                return true;
                            }
                        }
                    } else if (i2 != 1) {
                        if (i2 != 2) {
                            if (i2 != 16) {
                                if (i2 != 64) {
                                    if (i2 != 128) {
                                        return NumberPickerLarge.this.g.performAccessibilityAction(i2, bundle);
                                    }
                                    if (this.c != i) {
                                        return false;
                                    }
                                    this.c = Integer.MIN_VALUE;
                                    a(i, 65536);
                                    NumberPickerLarge.this.g.invalidate();
                                    return true;
                                } else if (this.c == i) {
                                    return false;
                                } else {
                                    this.c = i;
                                    a(i, 32768);
                                    NumberPickerLarge.this.g.invalidate();
                                    return true;
                                }
                            } else if (!NumberPickerLarge.this.isEnabled()) {
                                return false;
                            } else {
                                NumberPickerLarge.this.j();
                                return true;
                            }
                        } else if (!NumberPickerLarge.this.isEnabled() || !NumberPickerLarge.this.g.isFocused()) {
                            return false;
                        } else {
                            NumberPickerLarge.this.g.clearFocus();
                            return true;
                        }
                    } else if (!NumberPickerLarge.this.isEnabled() || NumberPickerLarge.this.g.isFocused()) {
                        return false;
                    } else {
                        return NumberPickerLarge.this.g.requestFocus();
                    }
                } else if (i2 != 16) {
                    if (i2 != 64) {
                        if (i2 != 128 || this.c != i) {
                            return false;
                        }
                        this.c = Integer.MIN_VALUE;
                        a(i, 65536);
                        NumberPickerLarge numberPickerLarge3 = NumberPickerLarge.this;
                        numberPickerLarge3.invalidate(0, numberPickerLarge3.e0, numberPickerLarge3.getRight(), NumberPickerLarge.this.getBottom());
                        return true;
                    } else if (this.c == i) {
                        return false;
                    } else {
                        this.c = i;
                        a(i, 32768);
                        NumberPickerLarge numberPickerLarge4 = NumberPickerLarge.this;
                        numberPickerLarge4.invalidate(0, numberPickerLarge4.e0, numberPickerLarge4.getRight(), NumberPickerLarge.this.getBottom());
                        return true;
                    }
                } else if (!NumberPickerLarge.this.isEnabled()) {
                    return false;
                } else {
                    NumberPickerLarge.this.a(true);
                    a(i, 1);
                    return true;
                }
            } else if (i2 != 64) {
                if (i2 != 128) {
                    if (i2 != 4096) {
                        if (i2 == 8192) {
                            if (!NumberPickerLarge.this.isEnabled() || (!NumberPickerLarge.this.getWrapSelectorWheel() && NumberPickerLarge.this.getValue() <= NumberPickerLarge.this.getMinValue())) {
                                return false;
                            }
                            NumberPickerLarge.this.a(false);
                            return true;
                        }
                    } else if (!NumberPickerLarge.this.isEnabled() || (!NumberPickerLarge.this.getWrapSelectorWheel() && NumberPickerLarge.this.getValue() >= NumberPickerLarge.this.getMaxValue())) {
                        return false;
                    } else {
                        NumberPickerLarge.this.a(true);
                        return true;
                    }
                } else if (this.c != i) {
                    return false;
                } else {
                    this.c = Integer.MIN_VALUE;
                    NumberPickerLarge.this.performAccessibilityAction(128, (Bundle) null);
                    return true;
                }
            } else if (this.c == i) {
                return false;
            } else {
                this.c = i;
                NumberPickerLarge.this.performAccessibilityAction(64, (Bundle) null);
                return true;
            }
            return super.performAction(i, i2, bundle);
        }

        @DexIgnore
        public final void a(int i) {
            if (((AccessibilityManager) NumberPickerLarge.this.getContext().getSystemService("accessibility")).isEnabled()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i);
                NumberPickerLarge.this.g.onInitializeAccessibilityEvent(obtain);
                NumberPickerLarge.this.g.onPopulateAccessibilityEvent(obtain);
                obtain.setSource(NumberPickerLarge.this, 2);
                NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                numberPickerLarge.requestSendAccessibilityEvent(numberPickerLarge, obtain);
            }
        }

        @DexIgnore
        public final void a(int i, int i2, String str) {
            if (((AccessibilityManager) NumberPickerLarge.this.getContext().getSystemService("accessibility")).isEnabled()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
                obtain.setClassName(Button.class.getName());
                obtain.setPackageName(NumberPickerLarge.this.getContext().getPackageName());
                obtain.getText().add(str);
                obtain.setEnabled(NumberPickerLarge.this.isEnabled());
                obtain.setSource(NumberPickerLarge.this, i);
                NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                numberPickerLarge.requestSendAccessibilityEvent(numberPickerLarge, obtain);
            }
        }

        @DexIgnore
        public final void a(String str, int i, List<AccessibilityNodeInfo> list) {
            if (i == 1) {
                String c2 = c();
                if (!TextUtils.isEmpty(c2) && c2.toLowerCase().contains(str)) {
                    list.add(createAccessibilityNodeInfo(1));
                }
            } else if (i == 2) {
                Editable text = NumberPickerLarge.this.g.getText();
                if (TextUtils.isEmpty(text) || !text.toString().toLowerCase().contains(str)) {
                    Editable text2 = NumberPickerLarge.this.g.getText();
                    if (!TextUtils.isEmpty(text2) && text2.toString().toLowerCase().contains(str)) {
                        list.add(createAccessibilityNodeInfo(2));
                        return;
                    }
                    return;
                }
                list.add(createAccessibilityNodeInfo(2));
            } else if (i == 3) {
                String b2 = b();
                if (!TextUtils.isEmpty(b2) && b2.toLowerCase().contains(str)) {
                    list.add(createAccessibilityNodeInfo(3));
                }
            }
        }

        @DexIgnore
        public final AccessibilityNodeInfo a() {
            AccessibilityNodeInfo createAccessibilityNodeInfo = NumberPickerLarge.this.g.createAccessibilityNodeInfo();
            createAccessibilityNodeInfo.setSource(NumberPickerLarge.this, 2);
            if (this.c != 2) {
                createAccessibilityNodeInfo.addAction(64);
            }
            if (this.c == 2) {
                createAccessibilityNodeInfo.addAction(128);
            }
            return createAccessibilityNodeInfo;
        }

        @DexIgnore
        public final AccessibilityNodeInfo a(int i, String str, int i2, int i3, int i4, int i5) {
            AccessibilityNodeInfo obtain = AccessibilityNodeInfo.obtain();
            obtain.setClassName(Button.class.getName());
            obtain.setPackageName(NumberPickerLarge.this.getContext().getPackageName());
            obtain.setSource(NumberPickerLarge.this, i);
            obtain.setParent(NumberPickerLarge.this);
            obtain.setText(str);
            obtain.setClickable(true);
            obtain.setLongClickable(true);
            obtain.setEnabled(NumberPickerLarge.this.isEnabled());
            Rect rect = this.a;
            rect.set(i2, i3, i4, i5);
            obtain.setBoundsInParent(rect);
            int[] iArr = this.b;
            NumberPickerLarge.this.getLocationOnScreen(iArr);
            rect.offset(iArr[0], iArr[1]);
            obtain.setBoundsInScreen(rect);
            if (this.c != i) {
                obtain.addAction(64);
            }
            if (this.c == i) {
                obtain.addAction(128);
            }
            if (NumberPickerLarge.this.isEnabled()) {
                obtain.addAction(16);
            }
            return obtain;
        }

        @DexIgnore
        public final AccessibilityNodeInfo a(int i, int i2, int i3, int i4) {
            AccessibilityNodeInfo obtain = AccessibilityNodeInfo.obtain();
            obtain.setClassName(NumberPickerLarge.class.getName());
            obtain.setPackageName(NumberPickerLarge.this.getContext().getPackageName());
            obtain.setSource(NumberPickerLarge.this);
            if (d()) {
                obtain.addChild(NumberPickerLarge.this, 3);
            }
            obtain.addChild(NumberPickerLarge.this, 2);
            if (e()) {
                obtain.addChild(NumberPickerLarge.this, 1);
            }
            obtain.setParent((View) NumberPickerLarge.this.getParentForAccessibility());
            obtain.setEnabled(NumberPickerLarge.this.isEnabled());
            obtain.setScrollable(true);
            if (this.c != -1) {
                obtain.addAction(64);
            }
            if (this.c == -1) {
                obtain.addAction(128);
            }
            if (NumberPickerLarge.this.isEnabled()) {
                if (NumberPickerLarge.this.getWrapSelectorWheel() || NumberPickerLarge.this.getValue() < NumberPickerLarge.this.getMaxValue()) {
                    obtain.addAction(4096);
                }
                if (NumberPickerLarge.this.getWrapSelectorWheel() || NumberPickerLarge.this.getValue() > NumberPickerLarge.this.getMinValue()) {
                    obtain.addAction(8192);
                }
            }
            return obtain;
        }
    }

    @DexIgnore
    public final int a(int i2, int i3) {
        if (i3 == -1) {
            return i2;
        }
        int size = View.MeasureSpec.getSize(i2);
        int mode = View.MeasureSpec.getMode(i2);
        if (mode == Integer.MIN_VALUE) {
            return View.MeasureSpec.makeMeasureSpec(Math.min(size, i3), 1073741824);
        }
        if (mode == 0) {
            return View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
        }
        if (mode == 1073741824) {
            return i2;
        }
        throw new IllegalArgumentException("Unknown measure mode: " + mode);
    }

    @DexIgnore
    public static String g(int i2) {
        return String.format(Locale.getDefault(), "%d", new Object[]{Integer.valueOf(i2)});
    }

    @DexIgnore
    public final void e(int i2) {
        g gVar = this.t;
        if (gVar != null) {
            gVar.a(this, i2, this.s);
        }
    }

    @DexIgnore
    public final int a(int i2, int i3, int i4) {
        return i2 != -1 ? resolveSizeAndState(Math.max(i2, i3), i4, 0) : i3;
    }

    @DexIgnore
    public final void b(int i2) {
        this.G = 0;
        if (i2 > 0) {
            this.E.a(0, 0, 0, i2, 0, 0, 0, Integer.MAX_VALUE);
        } else {
            this.E.a(0, Integer.MAX_VALUE, 0, i2, 0, 0, 0, Integer.MAX_VALUE);
        }
        invalidate();
    }

    @DexIgnore
    public int d(int i2) {
        int i3 = this.r;
        if (i2 > i3) {
            int i4 = this.q;
            return (i4 + ((i2 - i3) % (i3 - i4))) - 1;
        }
        int i5 = this.q;
        return i2 < i5 ? (i3 - ((i5 - i2) % (i3 - i5))) + 1 : i2;
    }

    @DexIgnore
    public final void a(int i2, boolean z2) {
        int i3;
        if (this.s != i2) {
            if (this.Q) {
                i3 = d(i2);
            } else {
                i3 = Math.min(Math.max(i2, this.q), this.r);
            }
            int i4 = this.s;
            this.s = i3;
            l();
            if (z2) {
                e(i4);
            }
            e();
            invalidate();
        }
    }

    @DexIgnore
    public final void b(int[] iArr) {
        System.arraycopy(iArr, 1, iArr, 0, iArr.length - 1);
        int i2 = iArr[iArr.length - 2] + 1;
        if (this.Q && i2 > this.r) {
            i2 = this.q;
        }
        iArr[iArr.length - 1] = i2;
        a(i2);
    }

    @DexIgnore
    public void a(boolean z2) {
        if (this.U) {
            this.g.setVisibility(4);
            if (!a(this.E)) {
                a(this.F);
            }
            this.G = 0;
            if (z2) {
                this.E.a(0, 0, 0, -this.B, 300);
            } else {
                this.E.a(0, 0, 0, this.B, 300);
            }
            invalidate();
        } else if (z2) {
            a(this.s + 1, true);
        } else {
            a(this.s - 1, true);
        }
    }

    @DexIgnore
    public final void a(int[] iArr) {
        System.arraycopy(iArr, 0, iArr, 1, iArr.length - 1);
        int i2 = iArr[1] - 1;
        if (this.Q && i2 < this.q) {
            i2 = this.r;
        }
        iArr[0] = i2;
        a(i2);
    }

    @DexIgnore
    public final void a(int i2) {
        String str;
        SparseArray<String> sparseArray = this.w;
        if (sparseArray.get(i2) == null) {
            int i3 = this.q;
            if (i2 < i3 || i2 > this.r) {
                str = "";
            } else {
                String[] strArr = this.p;
                str = strArr != null ? strArr[i2 - i3] : c(i2);
            }
            sparseArray.put(i2, str);
        }
    }

    @DexIgnore
    public void a(boolean z2, long j2) {
        e eVar = this.I;
        if (eVar == null) {
            this.I = new e();
        } else {
            removeCallbacks(eVar);
        }
        this.I.a(z2);
        postDelayed(this.I, j2);
    }

    @DexIgnore
    public final boolean a() {
        int i2 = this.C - this.D;
        if (i2 == 0) {
            return false;
        }
        this.G = 0;
        int abs = Math.abs(i2);
        int i3 = this.B;
        if (abs > i3 / 2) {
            if (i2 > 0) {
                i3 = -i3;
            }
            i2 += i3;
        }
        this.F.a(0, 0, 0, i2, 800);
        invalidate();
        return true;
    }
}
