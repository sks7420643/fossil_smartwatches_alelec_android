package com.portfolio.platform.view.recyclerview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ts3;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import kotlin.text.Regex;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RecyclerViewAlphabetIndex extends RecyclerView {
    @DexIgnore
    public String[] N0; // = {"#", "A", "B", "C", "D", "E", DeviceIdentityUtils.FLASH_SERIAL_NUMBER_PREFIX, "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX, "T", "U", "V", "W", "X", "Y", "Z"};
    @DexIgnore
    public float O0;
    @DexIgnore
    public int P0;
    @DexIgnore
    public boolean Q0;
    @DexIgnore
    public a R0;
    @DexIgnore
    public LinearLayoutManager S0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.g<a.C0162a> {
        @DexIgnore
        public /* final */ Typeface a;
        @DexIgnore
        public /* final */ Typeface b;
        @DexIgnore
        public String[] c;
        @DexIgnore
        public int d;
        @DexIgnore
        public b e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex$a$a")
        /* renamed from: com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex$a$a  reason: collision with other inner class name */
        public final class C0162a extends RecyclerView.ViewHolder {
            @DexIgnore
            public /* final */ FlexibleTextView a;
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex$a$a$a")
            /* renamed from: com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex$a$a$a  reason: collision with other inner class name */
            public  final class C0163a implements View.OnClickListener {
                @DexIgnore
                public /* final */ /* synthetic */ C0162a e;

                @DexIgnore
                public C0163a(C0162a aVar) {
                    this.e = aVar;
                }

                @DexIgnore
                public final void onClick(View view) {
                    int adapterPosition = this.e.getAdapterPosition();
                    if (adapterPosition != -1) {
                        String str = "" + this.e.a().getText().toString();
                        if (RecyclerViewAlphabetIndex.this.R()) {
                            RecyclerViewAlphabetIndex.this.setLetterToBold(str);
                        }
                        b b = this.e.b.b();
                        if (b != null) {
                            kd4.a((Object) view, "it");
                            b.a(view, adapterPosition, str);
                        }
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0162a(a aVar, View view) {
                super(view);
                kd4.b(view, "itemView");
                this.b = aVar;
                View findViewById = view.findViewById(com.fossil.wearables.fossil.R.id.tvLetter);
                if (findViewById != null) {
                    this.a = (FlexibleTextView) findViewById;
                    view.setOnClickListener(new C0163a(this));
                    return;
                }
                kd4.a();
                throw null;
            }

            @DexIgnore
            public final FlexibleTextView a() {
                return this.a;
            }
        }

        @DexIgnore
        @SuppressLint("WrongConstant")
        public a() {
             Typeface defaultFromStyle = Typeface.defaultFromStyle(0);
            if (defaultFromStyle != null) {
                this.a = defaultFromStyle;
                this.b = Typeface.defaultFromStyle(1);
                return;
            }
            kd4.a();
            throw null;
        }

        @DexIgnore
        /* renamed from: a */
        public void onBindViewHolder(C0162a aVar, int i) {
            kd4.b(aVar, "holder");
            String[] strArr = this.c;
            if (strArr != null) {
                aVar.a().setText(strArr[i]);
                if (RecyclerViewAlphabetIndex.this.R()) {
                    aVar.a().setTypeface(i == this.d ? this.b : this.a);
                    aVar.a().setTextSize(2, RecyclerViewAlphabetIndex.this.getMFontSize$app_fossilRelease());
                    if (RecyclerViewAlphabetIndex.this.getMItemColor$app_fossilRelease() != 0) {
                        aVar.a().setTextColor(RecyclerViewAlphabetIndex.this.getMItemColor$app_fossilRelease());
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.a();
            throw null;
        }

        @DexIgnore
        public final b b() {
            return this.e;
        }

        @DexIgnore
        public int getItemCount() {
            String[] strArr = this.c;
            if (strArr != null) {
                return strArr.length;
            }
            return 0;
        }

        @DexIgnore
        public C0162a onCreateViewHolder(ViewGroup viewGroup, int i) {
            kd4.b(viewGroup, "parent");
            View inflate = LayoutInflater.from(RecyclerViewAlphabetIndex.this.getContext()).inflate(com.fossil.wearables.fossil.R.layout.item_letter, viewGroup, false);
            kd4.a((Object) inflate, "view");
            return new C0162a(this, inflate);
        }

        @DexIgnore
        public final void a(b bVar) {
            kd4.b(bVar, "sectionIndexClickListener");
            this.e = bVar;
        }

        @DexIgnore
        public final void a(int i) {
            this.d = i;
            notifyDataSetChanged();
        }

        @DexIgnore
        public final void a(String[] strArr) {
            kd4.b(strArr, "alphabet");
            this.c = strArr;
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(View view, int i, String str);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewAlphabetIndex(Context context) {
        super(context);
        kd4.b(context, "context");
        setOverScrollMode(2);
        Q();
    }

    @DexIgnore
    private final void setAlphabet(String[] strArr) {
        Arrays.sort(strArr);
        a aVar = this.R0;
        if (aVar != null) {
            aVar.a(strArr);
        }
    }

    @DexIgnore
    public final void P() {
        setAlphabet(this.N0);
    }

    @DexIgnore
    public final void Q() {
        this.R0 = new a();
        this.S0 = new LinearLayoutManager(getContext(), 1, false);
        setHasFixedSize(true);
        setAdapter(this.R0);
        setLayoutManager(this.S0);
    }

    @DexIgnore
    public final boolean R() {
        return this.Q0;
    }

    @SuppressLint("ResourceType")
    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.RecyclerViewAlphabetIndex);
        obtainStyledAttributes.getDimensionPixelSize(4, 15);
        this.O0 = obtainStyledAttributes.getDimension(1, (float) ts3.b(3.0f));
        this.P0 = obtainStyledAttributes.getColor(2, k6.a(context, (int) com.fossil.wearables.fossil.R.color.primaryColor));
        this.Q0 = obtainStyledAttributes.getBoolean(0, false);
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public final float getMFontSize$app_fossilRelease() {
        return this.O0;
    }

    @DexIgnore
    public final int getMItemColor$app_fossilRelease() {
        return this.P0;
    }

    @DexIgnore
    public final void setCustomizable$app_fossilRelease(boolean z) {
        this.Q0 = z;
    }

    @DexIgnore
    public final void setLetterToBold(String str) {
        throw null;
        // kd4.b(str, "letter");
        // int c = za4.c((T[]) this.N0, str);
        // if (new Regex("[0-9]+").matches(str)) {
        //     c = this.N0.length - 1;
        // }
        // LinearLayoutManager linearLayoutManager = this.S0;
        // if (linearLayoutManager != null) {
        //     linearLayoutManager.f(c, 0);
        // }
        // a aVar = this.R0;
        // if (aVar != null) {
        //     aVar.a(c);
        // }
    }

    @DexIgnore
    public final void setMFontSize$app_fossilRelease(float f) {
        this.O0 = f;
    }

    @DexIgnore
    public final void setMItemColor$app_fossilRelease(int i) {
        this.P0 = i;
    }

    @DexIgnore
    public final void setOnSectionIndexClickListener(b bVar) {
        kd4.b(bVar, "sectionIndexClickListener");
        a aVar = this.R0;
        if (aVar != null) {
            aVar.a(bVar);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewAlphabetIndex(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        kd4.b(context, "context");
        kd4.b(attributeSet, "attrs");
        setOverScrollMode(2);
        a(context, attributeSet);
        Q();
    }
}
