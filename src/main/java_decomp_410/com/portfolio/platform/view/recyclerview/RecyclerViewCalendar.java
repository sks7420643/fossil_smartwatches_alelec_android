package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.as3;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.zt3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Calendar;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class RecyclerViewCalendar extends ConstraintLayout implements View.OnClickListener {
    @DexIgnore
    public View A;
    @DexIgnore
    public TextView B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public int H;
    @DexIgnore
    public Calendar I;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public GridLayoutManager v;
    @DexIgnore
    public zt3 w;
    @DexIgnore
    public e x;
    @DexIgnore
    public f y;
    @DexIgnore
    public View z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends GridLayoutManager {
        @DexIgnore
        public /* final */ /* synthetic */ Context P;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(RecyclerViewCalendar recyclerViewCalendar, Context context, int i, int i2, boolean z, Context context2) {
            super(context, i, i2, z);
            this.P = context2;
        }

        @DexIgnore
        public boolean a() {
            return false;
        }

        @DexIgnore
        public int k(RecyclerView.State state) {
            return as3.a(this.P);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends GridLayoutManager.b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public int b(int i) {
            int itemViewType = RecyclerViewCalendar.this.w.getItemViewType(i);
            return (itemViewType == 0 || itemViewType == 1) ? 1 : -1;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView e;

        @DexIgnore
        public c(RecyclerView recyclerView) {
            this.e = recyclerView;
        }

        @DexIgnore
        public void onGlobalLayout() {
            RecyclerViewCalendar.this.w.b(this.e.getMeasuredWidth() / 7);
            this.e.setAdapter(RecyclerViewCalendar.this.w);
            this.e.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(int i, Calendar calendar);
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(Calendar calendar);
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        void a();

        @DexIgnore
        void next();
    }

    @DexIgnore
    public RecyclerViewCalendar(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a(Context context) {
        Context context2 = context;
        View inflate = ViewGroup.inflate(context2, R.layout.view_calendar, this);
        this.B = (TextView) inflate.findViewById(R.id.month);
        this.z = inflate.findViewById(R.id.next);
        this.A = inflate.findViewById(R.id.prev);
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(R.id.days);
        this.w = new zt3(context2);
        this.w.a(this.C, this.D, this.E, this.F, this.G);
        this.v = new a(this, context, 7, 0, true, context2);
        this.v.a((GridLayoutManager.b) new b());
        recyclerView.setLayoutManager(this.v);
        recyclerView.setItemAnimator((RecyclerView.j) null);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new c(recyclerView));
        this.z.setOnClickListener(this);
        this.A.setOnClickListener(this);
    }

    @DexIgnore
    public final void c(int i) {
        f fVar = this.y;
        if (fVar == null) {
            return;
        }
        if (i == R.id.next) {
            fVar.next();
        } else if (i == R.id.prev) {
            fVar.a();
        }
    }

    @DexIgnore
    public final void d() {
        Calendar b2 = this.w.b();
        Calendar c2 = this.w.c();
        e eVar = this.x;
        if (eVar != null) {
            eVar.a(this.I);
        }
        a(c2, b2);
    }

    @DexIgnore
    public void onClick(View view) {
        setEnableButtonNextAndPrevMonth(false);
        int id = view.getId();
        if (id == R.id.next) {
            this.H++;
        } else if (id == R.id.prev) {
            this.H--;
        }
        this.I = rk2.a(this.H, this.w.b());
        d();
        c(view.getId());
    }

    @DexIgnore
    public void setData(Map<Long, Float> map) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("RecyclerViewCalendar", "setData dataSize=" + map.size());
        this.w.a(map, this.I);
        this.w.notifyDataSetChanged();
    }

    @DexIgnore
    public void setEnableButtonNextAndPrevMonth(Boolean bool) {
        this.z.setEnabled(bool.booleanValue());
        this.A.setEnabled(bool.booleanValue());
    }

    @DexIgnore
    public void setEndDate(Calendar calendar) {
        this.I = rk2.a(this.H, calendar);
        this.w.a(calendar);
        this.w.notifyDataSetChanged();
        a(this.w.c(), calendar);
    }

    @DexIgnore
    public void setNavigatingListener(f fVar) {
        this.y = fVar;
    }

    @DexIgnore
    public void setOnCalendarItemClickListener(d dVar) {
        this.w.a(dVar);
    }

    @DexIgnore
    public void setOnCalendarMonthChanged(e eVar) {
        this.x = eVar;
    }

    @DexIgnore
    public void setStartDate(Calendar calendar) {
        this.w.c(calendar);
        this.w.notifyDataSetChanged();
        a(calendar, this.w.b());
    }

    @DexIgnore
    public void setTintColor(int i) {
    }

    @DexIgnore
    public RecyclerViewCalendar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public RecyclerViewCalendar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.u = Color.parseColor("#FFFF00");
        int i2 = this.u;
        this.C = i2;
        this.D = i2;
        this.E = i2;
        this.F = i2;
        this.G = i2;
        this.H = 0;
        this.I = Calendar.getInstance();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.RecyclerViewCalendar);
            try {
                this.C = obtainStyledAttributes.getColor(0, this.u);
                this.D = obtainStyledAttributes.getColor(1, this.u);
                this.E = obtainStyledAttributes.getColor(2, this.u);
                this.F = obtainStyledAttributes.getColor(4, this.u);
                this.G = obtainStyledAttributes.getColor(3, this.u);
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("RecyclerViewCalendar", "RecyclerViewCalendar - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        a(context);
    }

    @DexIgnore
    public void a(Calendar calendar, Calendar calendar2, Calendar calendar3) {
        this.w.c(calendar2);
        this.w.a(calendar3);
        this.w.b(calendar);
        a(calendar2, calendar3);
        this.w.notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(Calendar calendar, Calendar calendar2) {
        if (calendar != null && calendar2 != null) {
            int i = this.I.get(2);
            int i2 = this.I.get(1);
            int i3 = 8;
            this.A.setVisibility((i == calendar.get(2) && i2 == calendar.get(1)) ? 8 : 0);
            View view = this.z;
            if (!(i == calendar2.get(2) && i2 == calendar2.get(1))) {
                i3 = 0;
            }
            view.setVisibility(i3);
            this.B.setText(String.format("%s %s", new Object[]{a(this.I), Integer.valueOf(i2)}).trim());
        }
    }

    @DexIgnore
    public final String a(Calendar calendar) {
        switch (calendar.get(2)) {
            case 0:
                return sm2.a((Context) PortfolioApp.R, (int) R.string.General_Months_Month_Title__January);
            case 1:
                return sm2.a((Context) PortfolioApp.R, (int) R.string.General_Months_Month_Title__February);
            case 2:
                return sm2.a((Context) PortfolioApp.R, (int) R.string.General_Months_Month_Title__March);
            case 3:
                return sm2.a((Context) PortfolioApp.R, (int) R.string.General_Months_Month_Title__April);
            case 4:
                return sm2.a((Context) PortfolioApp.R, (int) R.string.General_Months_Month_Title__May);
            case 5:
                return sm2.a((Context) PortfolioApp.R, (int) R.string.General_Months_Month_Title__June);
            case 6:
                return sm2.a((Context) PortfolioApp.R, (int) R.string.General_Months_Month_Title__July);
            case 7:
                return sm2.a((Context) PortfolioApp.R, (int) R.string.General_Months_Month_Title__August);
            case 8:
                return sm2.a((Context) PortfolioApp.R, (int) R.string.General_Months_Month_Title__September);
            case 9:
                return sm2.a((Context) PortfolioApp.R, (int) R.string.General_Months_Month_Title__October);
            case 10:
                return sm2.a((Context) PortfolioApp.R, (int) R.string.General_Months_Month_Title__November);
            case 11:
                return sm2.a((Context) PortfolioApp.R, (int) R.string.General_Months_Month_Title__December);
            default:
                return sm2.a((Context) PortfolioApp.R, (int) R.string.General_Months_Month_Title__January);
        }
    }
}
