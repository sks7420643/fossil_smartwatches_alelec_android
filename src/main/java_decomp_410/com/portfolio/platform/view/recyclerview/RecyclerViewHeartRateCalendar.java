package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.au3;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RecyclerViewHeartRateCalendar extends ConstraintLayout implements View.OnClickListener {
    @DexIgnore
    public static /* final */ int J; // = Color.parseColor("#FFFF00");
    @DexIgnore
    public TextView A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public Calendar H;
    @DexIgnore
    public int I;
    @DexIgnore
    public GridLayoutManager u;
    @DexIgnore
    public au3 v;
    @DexIgnore
    public c w;
    @DexIgnore
    public d x;
    @DexIgnore
    public View y;
    @DexIgnore
    public View z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, Calendar calendar);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(Calendar calendar);
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();

        @DexIgnore
        void next();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends GridLayoutManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewHeartRateCalendar c;

        @DexIgnore
        public e(RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar) {
            this.c = recyclerViewHeartRateCalendar;
        }

        @DexIgnore
        public int b(int i) {
            au3 mAdapter$app_fossilRelease = this.c.getMAdapter$app_fossilRelease();
            if (mAdapter$app_fossilRelease != null) {
                int itemViewType = mAdapter$app_fossilRelease.getItemViewType(i);
                return (itemViewType == 0 || itemViewType == 1) ? 1 : -1;
            }
            kd4.a();
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewHeartRateCalendar e;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView f;

        @DexIgnore
        public f(RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar, RecyclerView recyclerView) {
            this.e = recyclerViewHeartRateCalendar;
            this.f = recyclerView;
        }

        @DexIgnore
        public void onGlobalLayout() {
            au3 mAdapter$app_fossilRelease = this.e.getMAdapter$app_fossilRelease();
            if (mAdapter$app_fossilRelease != null) {
                RecyclerView recyclerView = this.f;
                kd4.a((Object) recyclerView, "recyclerView");
                mAdapter$app_fossilRelease.b(recyclerView.getMeasuredWidth() / 7);
                RecyclerView recyclerView2 = this.f;
                kd4.a((Object) recyclerView2, "recyclerView");
                recyclerView2.setAdapter(this.e.getMAdapter$app_fossilRelease());
                RecyclerView recyclerView3 = this.f;
                kd4.a((Object) recyclerView3, "recyclerView");
                recyclerView3.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                return;
            }
            kd4.a();
            throw null;
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public RecyclerViewHeartRateCalendar(Context context) {
        this(context, (AttributeSet) null, 0, 6, (fd4) null);
    }

    @DexIgnore
    public RecyclerViewHeartRateCalendar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (fd4) null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewHeartRateCalendar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        kd4.b(context, "context");
        int i2 = J;
        this.B = i2;
        this.C = i2;
        this.D = i2;
        this.E = i2;
        this.F = i2;
        this.G = i2;
        this.H = Calendar.getInstance();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.RecyclerViewHeartRateCalendar);
            try {
                this.B = obtainStyledAttributes.getColor(3, J);
                this.C = obtainStyledAttributes.getColor(4, J);
                this.D = obtainStyledAttributes.getColor(5, J);
                this.E = obtainStyledAttributes.getColor(2, J);
                this.F = obtainStyledAttributes.getColor(0, J);
                this.G = obtainStyledAttributes.getColor(1, J);
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("RecyclerViewHeartRateCalendar", "RecyclerViewHeartRateCalendar - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        a(context);
    }

    @DexIgnore
    public final void a(Context context) {
        Context context2 = context;
        View inflate = View.inflate(context2, R.layout.view_calendar, this);
        this.A = (TextView) inflate.findViewById(R.id.month);
        this.y = inflate.findViewById(R.id.next);
        this.z = inflate.findViewById(R.id.prev);
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(R.id.days);
        this.v = new au3(context2);
        au3 au3 = this.v;
        if (au3 != null) {
            au3.a(this.B, this.C, this.D, this.E, this.F, this.G);
            this.u = new RecyclerViewHeartRateCalendar$init$Anon1(context, context, 7, 0, true);
            GridLayoutManager gridLayoutManager = this.u;
            if (gridLayoutManager != null) {
                gridLayoutManager.a((GridLayoutManager.b) new e(this));
                kd4.a((Object) recyclerView, "recyclerView");
                recyclerView.setLayoutManager(this.u);
                recyclerView.setItemAnimator((RecyclerView.j) null);
                recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new f(this, recyclerView));
                View view = this.y;
                if (view != null) {
                    view.setOnClickListener(this);
                    View view2 = this.z;
                    if (view2 != null) {
                        view2.setOnClickListener(this);
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void c(int i) {
        d dVar = this.x;
        if (dVar == null) {
            return;
        }
        if (i != R.id.next) {
            if (i == R.id.prev) {
                if (dVar != null) {
                    dVar.a();
                } else {
                    kd4.a();
                    throw null;
                }
            }
        } else if (dVar != null) {
            dVar.next();
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void d() {
        au3 au3 = this.v;
        Calendar calendar = null;
        Calendar c2 = au3 != null ? au3.c() : null;
        au3 au32 = this.v;
        if (au32 != null) {
            calendar = au32.o();
        }
        c cVar = this.w;
        if (cVar != null) {
            Calendar calendar2 = this.H;
            kd4.a((Object) calendar2, "chosenCalendar");
            cVar.a(calendar2);
        }
        a(calendar, c2);
    }

    @DexIgnore
    public final au3 getMAdapter$app_fossilRelease() {
        return this.v;
    }

    @DexIgnore
    public void onClick(View view) {
        kd4.b(view, "view");
        setEnableButtonNextAndPrevMonth(false);
        int id = view.getId();
        if (id == R.id.next) {
            this.I++;
        } else if (id == R.id.prev) {
            this.I--;
        }
        au3 au3 = this.v;
        if (au3 != null) {
            this.H = rk2.a(this.I, au3.c());
        }
        d();
        c(view.getId());
    }

    @DexIgnore
    public final void setData(Map<Long, Integer> map) {
        int i;
        kd4.b(map, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("RecyclerViewHeartRateCalendar", "setData dataSize=" + map.size());
        Collection<Integer> values = map.values();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = values.iterator();
        while (true) {
            boolean z2 = true;
            i = 0;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (((Number) next).intValue() <= 0) {
                z2 = false;
            }
            if (z2) {
                arrayList.add(next);
            }
        }
        Integer num = (Integer) kb4.f(arrayList);
        int intValue = num != null ? num.intValue() : 0;
        Collection<Integer> values2 = map.values();
        ArrayList arrayList2 = new ArrayList();
        for (T next2 : values2) {
            if (((Number) next2).intValue() > 0) {
                arrayList2.add(next2);
            }
        }
        Integer num2 = (Integer) kb4.e(arrayList2);
        int intValue2 = num2 != null ? num2.intValue() : 100;
        if (intValue != intValue2) {
            i = intValue;
        } else if (intValue2 == 0) {
            intValue2 = 100;
        }
        au3 au3 = this.v;
        if (au3 != null) {
            Calendar calendar = this.H;
            kd4.a((Object) calendar, "chosenCalendar");
            au3.a(map, i, intValue2, calendar);
        }
        au3 au32 = this.v;
        if (au32 != null) {
            au32.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public final void setEnableButtonNextAndPrevMonth(boolean z2) {
        View view = this.y;
        if (view != null) {
            view.setEnabled(z2);
        }
        View view2 = this.z;
        if (view2 != null) {
            view2.setEnabled(z2);
        }
    }

    @DexIgnore
    public final void setEndDate(Calendar calendar) {
        kd4.b(calendar, GoalPhase.COLUMN_END_DATE);
        this.H = rk2.a(this.I, calendar);
        au3 au3 = this.v;
        if (au3 != null) {
            au3.a(calendar);
            au3 au32 = this.v;
            if (au32 != null) {
                au32.notifyDataSetChanged();
                au3 au33 = this.v;
                if (au33 != null) {
                    a(au33.o(), calendar);
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void setMAdapter$app_fossilRelease(au3 au3) {
        this.v = au3;
    }

    @DexIgnore
    public final void setOnCalendarItemClickListener(b bVar) {
        kd4.b(bVar, "listener");
        au3 au3 = this.v;
        if (au3 != null) {
            au3.a(bVar);
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void setOnCalendarMonthChanged(c cVar) {
        kd4.b(cVar, "listener");
        this.w = cVar;
    }

    @DexIgnore
    public final void setTintColor(int i) {
    }

    @DexIgnore
    public final void a(Calendar calendar, Calendar calendar2, Calendar calendar3) {
        kd4.b(calendar, "currentCalendar");
        kd4.b(calendar2, "startCalendar");
        kd4.b(calendar3, "endCalendar");
        au3 au3 = this.v;
        if (au3 != null) {
            au3.c(calendar2);
            au3.a(calendar3);
            au3.b(calendar);
            au3.notifyDataSetChanged();
            a(calendar2, calendar3);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ RecyclerViewHeartRateCalendar(Context context, AttributeSet attributeSet, int i, int i2, fd4 fd4) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    @DexIgnore
    public final void a(Calendar calendar, Calendar calendar2) {
        if (calendar != null && calendar2 != null) {
            int i = this.H.get(2);
            int i2 = this.H.get(1);
            View view = this.z;
            if (view != null) {
                int i3 = 8;
                view.setVisibility((i == calendar.get(2) && i2 == calendar.get(1)) ? 8 : 0);
                View view2 = this.y;
                if (view2 != null) {
                    if (!(i == calendar2.get(2) && i2 == calendar2.get(1))) {
                        i3 = 0;
                    }
                    view2.setVisibility(i3);
                    TextView textView = this.A;
                    if (textView != null) {
                        pd4 pd4 = pd4.a;
                        Calendar calendar3 = this.H;
                        kd4.a((Object) calendar3, "chosenCalendar");
                        Object[] objArr = {a(calendar3), Integer.valueOf(i2)};
                        String format = String.format("%s %s", Arrays.copyOf(objArr, objArr.length));
                        kd4.a((Object) format, "java.lang.String.format(format, *args)");
                        int length = format.length() - 1;
                        int i4 = 0;
                        boolean z2 = false;
                        while (i4 <= length) {
                            boolean z3 = format.charAt(!z2 ? i4 : length) <= ' ';
                            if (!z2) {
                                if (!z3) {
                                    z2 = true;
                                } else {
                                    i4++;
                                }
                            } else if (!z3) {
                                break;
                            } else {
                                length--;
                            }
                        }
                        textView.setText(format.subSequence(i4, length + 1).toString());
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final String a(Calendar calendar) {
        switch (calendar.get(2)) {
            case 0:
                String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Months_Month_Title__January);
                kd4.a((Object) a2, "LanguageHelper.getString\u2026ths_Month_Title__January)");
                return a2;
            case 1:
                String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Months_Month_Title__February);
                kd4.a((Object) a3, "LanguageHelper.getString\u2026hs_Month_Title__February)");
                return a3;
            case 2:
                String a4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Months_Month_Title__March);
                kd4.a((Object) a4, "LanguageHelper.getString\u2026onths_Month_Title__March)");
                return a4;
            case 3:
                String a5 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Months_Month_Title__April);
                kd4.a((Object) a5, "LanguageHelper.getString\u2026onths_Month_Title__April)");
                return a5;
            case 4:
                String a6 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Months_Month_Title__May);
                kd4.a((Object) a6, "LanguageHelper.getString\u2026_Months_Month_Title__May)");
                return a6;
            case 5:
                String a7 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Months_Month_Title__June);
                kd4.a((Object) a7, "LanguageHelper.getString\u2026Months_Month_Title__June)");
                return a7;
            case 6:
                String a8 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Months_Month_Title__July);
                kd4.a((Object) a8, "LanguageHelper.getString\u2026Months_Month_Title__July)");
                return a8;
            case 7:
                String a9 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Months_Month_Title__August);
                kd4.a((Object) a9, "LanguageHelper.getString\u2026nths_Month_Title__August)");
                return a9;
            case 8:
                String a10 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Months_Month_Title__September);
                kd4.a((Object) a10, "LanguageHelper.getString\u2026s_Month_Title__September)");
                return a10;
            case 9:
                String a11 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Months_Month_Title__October);
                kd4.a((Object) a11, "LanguageHelper.getString\u2026ths_Month_Title__October)");
                return a11;
            case 10:
                String a12 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Months_Month_Title__November);
                kd4.a((Object) a12, "LanguageHelper.getString\u2026hs_Month_Title__November)");
                return a12;
            case 11:
                String a13 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Months_Month_Title__December);
                kd4.a((Object) a13, "LanguageHelper.getString\u2026hs_Month_Title__December)");
                return a13;
            default:
                String a14 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Months_Month_Title__January);
                kd4.a((Object) a14, "LanguageHelper.getString\u2026ths_Month_Title__January)");
                return a14;
        }
    }
}
