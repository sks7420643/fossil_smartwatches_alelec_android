package com.portfolio.platform.view.blur;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import androidx.renderscript.Allocation;
import androidx.renderscript.Element;
import androidx.renderscript.RSRuntimeException;
import androidx.renderscript.RenderScript;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.Cif;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qf4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BlurView extends View {
    @DexIgnore
    public static int v;
    @DexIgnore
    public static /* final */ StopException w; // = new StopException();
    @DexIgnore
    public static Boolean x; // = null;
    @DexIgnore
    public static /* final */ a y; // = new a((fd4) null);
    @DexIgnore
    public /* final */ float e;
    @DexIgnore
    public int f;
    @DexIgnore
    public /* final */ float g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public Bitmap i;
    @DexIgnore
    public Bitmap j;
    @DexIgnore
    public Canvas k;
    @DexIgnore
    public RenderScript l;
    @DexIgnore
    public Cif m;
    @DexIgnore
    public Allocation n;
    @DexIgnore
    public Allocation o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public /* final */ Rect q; // = new Rect();
    @DexIgnore
    public /* final */ Rect r; // = new Rect();
    @DexIgnore
    public View s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public /* final */ ViewTreeObserver.OnPreDrawListener u; // = new b(this);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class StopException extends RuntimeException {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final Boolean a() {
            return BlurView.x;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final void a(Boolean bool) {
            BlurView.x = bool;
        }

        @DexIgnore
        public final boolean a(Context context) {
            if (a() == null && context != null) {
                a(Boolean.valueOf((context.getApplicationInfo().flags & 2) != 0));
            }
            if (a() == Boolean.TRUE) {
                return true;
            }
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnPreDrawListener {
        @DexIgnore
        public /* final */ /* synthetic */ BlurView e;

        @DexIgnore
        public b(BlurView blurView) {
            this.e = blurView;
        }

        @DexIgnore
        public final boolean onPreDraw() {
            Canvas canvas;
            int[] iArr = new int[2];
            Bitmap b = this.e.j;
            View d = this.e.s;
            if (d != null && this.e.isShown() && this.e.a()) {
                boolean z = !kd4.a((Object) this.e.j, (Object) b);
                d.getLocationOnScreen(iArr);
                this.e.getLocationOnScreen(iArr);
                int i = (-iArr[0]) + iArr[0];
                int i2 = (-iArr[1]) + iArr[1];
                Bitmap a = this.e.i;
                if (a != null) {
                    a.eraseColor(this.e.f & 16777215);
                    Canvas c = this.e.k;
                    if (c != null) {
                        int save = c.save();
                        this.e.p = true;
                        BlurView.v = BlurView.v + 1;
                        try {
                            Canvas c2 = this.e.k;
                            if (c2 != null) {
                                Bitmap a2 = this.e.i;
                                if (a2 != null) {
                                    float width = (((float) a2.getWidth()) * 1.0f) / ((float) this.e.getWidth());
                                    Bitmap a3 = this.e.i;
                                    if (a3 != null) {
                                        c2.scale(width, (1.0f * ((float) a3.getHeight())) / ((float) this.e.getHeight()));
                                        Canvas c3 = this.e.k;
                                        if (c3 != null) {
                                            c3.translate((float) (-i), (float) (-i2));
                                            if (d.getBackground() != null) {
                                                Drawable background = d.getBackground();
                                                Canvas c4 = this.e.k;
                                                if (c4 != null) {
                                                    background.draw(c4);
                                                } else {
                                                    kd4.a();
                                                    throw null;
                                                }
                                            }
                                            d.draw(this.e.k);
                                            this.e.p = false;
                                            BlurView.v = BlurView.v - 1;
                                            canvas = this.e.k;
                                            if (canvas == null) {
                                                kd4.a();
                                                throw null;
                                            }
                                            canvas.restoreToCount(save);
                                            BlurView blurView = this.e;
                                            Bitmap a4 = blurView.i;
                                            if (a4 != null) {
                                                Bitmap b2 = this.e.j;
                                                if (b2 != null) {
                                                    blurView.a(a4, b2);
                                                    if (z || this.e.t) {
                                                        this.e.invalidate();
                                                    }
                                                } else {
                                                    kd4.a();
                                                    throw null;
                                                }
                                            } else {
                                                kd4.a();
                                                throw null;
                                            }
                                        } else {
                                            kd4.a();
                                            throw null;
                                        }
                                    } else {
                                        kd4.a();
                                        throw null;
                                    }
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } catch (StopException unused) {
                            this.e.p = false;
                            BlurView.v = BlurView.v - 1;
                            canvas = this.e.k;
                            if (canvas == null) {
                                kd4.a();
                                throw null;
                            }
                        } catch (Throwable th) {
                            this.e.p = false;
                            BlurView.v = BlurView.v - 1;
                            Canvas c5 = this.e.k;
                            if (c5 == null) {
                                kd4.a();
                                throw null;
                            }
                            c5.restoreToCount(save);
                            throw th;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
            return true;
        }
    }

    /*
    static {
        try {
            ClassLoader classLoader = BlurView.class.getClassLoader();
            if (classLoader != null) {
                classLoader.loadClass("android.support.v8.renderscript.RenderScript");
                return;
            }
            kd4.a();
            throw null;
        } catch (ClassNotFoundException unused) {
            throw new RuntimeException("RenderScript support not enabled. Add \"android { defaultConfig { renderscriptSupportModeEnabled true }}\" in your build.gradle");
        }
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BlurView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        kd4.b(context, "context");
        kd4.b(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.BlurView);
        Resources resources = context.getResources();
        kd4.a((Object) resources, "context.resources");
        this.g = obtainStyledAttributes.getDimension(2, TypedValue.applyDimension(1, 10.0f, resources.getDisplayMetrics()));
        this.e = obtainStyledAttributes.getFloat(0, 4.0f);
        this.f = obtainStyledAttributes.getColor(1, 0);
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    private final View getActivityDecorView() {
        Context context = getContext();
        for (int i2 = 0; i2 < 4 && context != null && !(context instanceof Activity) && (context instanceof ContextWrapper); i2++) {
            context = ((ContextWrapper) context).getBaseContext();
        }
        if (!(context instanceof Activity)) {
            return null;
        }
        Window window = ((Activity) context).getWindow();
        kd4.a((Object) window, "ctx.window");
        return window.getDecorView();
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        kd4.b(canvas, "canvas");
        if (this.p) {
            throw w;
        } else if (v <= 0) {
            super.draw(canvas);
        }
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.s = getActivityDecorView();
        View view = this.s;
        boolean z = false;
        if (view == null) {
            this.t = false;
        } else if (view != null) {
            view.getViewTreeObserver().addOnPreDrawListener(this.u);
            View view2 = this.s;
            if (view2 != null) {
                if (view2.getRootView() != getRootView()) {
                    z = true;
                }
                this.t = z;
                if (this.t) {
                    View view3 = this.s;
                    if (view3 != null) {
                        view3.postInvalidate();
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        View view = this.s;
        if (view != null) {
            if (view != null) {
                view.getViewTreeObserver().removeOnPreDrawListener(this.u);
            } else {
                kd4.a();
                throw null;
            }
        }
        b();
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        kd4.b(canvas, "canvas");
        super.onDraw(canvas);
        a(canvas, this.j, this.f);
    }

    @DexIgnore
    public final void b() {
        c();
        d();
    }

    @DexIgnore
    public final void c() {
        Allocation allocation = this.n;
        if (allocation != null) {
            if (allocation != null) {
                allocation.b();
                this.n = null;
            } else {
                kd4.a();
                throw null;
            }
        }
        Allocation allocation2 = this.o;
        if (allocation2 != null) {
            if (allocation2 != null) {
                allocation2.b();
                this.o = null;
            } else {
                kd4.a();
                throw null;
            }
        }
        Bitmap bitmap = this.i;
        if (bitmap != null) {
            if (bitmap != null) {
                bitmap.recycle();
                this.i = null;
            } else {
                kd4.a();
                throw null;
            }
        }
        Bitmap bitmap2 = this.j;
        if (bitmap2 == null) {
            return;
        }
        if (bitmap2 != null) {
            bitmap2.recycle();
            this.j = null;
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void d() {
        RenderScript renderScript = this.l;
        if (renderScript != null) {
            if (renderScript != null) {
                renderScript.a();
                this.l = null;
            } else {
                kd4.a();
                throw null;
            }
        }
        Cif ifVar = this.m;
        if (ifVar == null) {
            return;
        }
        if (ifVar != null) {
            ifVar.b();
            this.m = null;
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final boolean a() {
        if (this.g == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            b();
            return false;
        }
        float f2 = this.e;
        if (this.h || this.l == null) {
            if (this.l == null) {
                try {
                    this.l = RenderScript.a(getContext());
                    RenderScript renderScript = this.l;
                    if (renderScript != null) {
                        RenderScript renderScript2 = this.l;
                        if (renderScript2 != null) {
                            this.m = Cif.a(renderScript, Element.h(renderScript2));
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } catch (RSRuntimeException e2) {
                    if (y.a(getContext())) {
                        if (e2.getMessage() != null) {
                            String message = e2.getMessage();
                            if (message == null) {
                                kd4.a();
                                throw null;
                            } else if (qf4.c(message, "Error networkLoading RS jni library: java.lang.UnsatisfiedLinkError:", false, 2, (Object) null)) {
                                throw new RuntimeException("Error networkLoading RS jni library, Upgrade buildToolsVersion=\"24.0.2\" or higher may solve this issue");
                            }
                        }
                        throw e2;
                    }
                    d();
                    return false;
                }
            }
            this.h = false;
            float f3 = this.g / f2;
            if (f3 > 25.0f) {
                f2 = (f2 * f3) / 25.0f;
                f3 = 25.0f;
            }
            Cif ifVar = this.m;
            if (ifVar != null) {
                ifVar.a(f3);
            } else {
                kd4.a();
                throw null;
            }
        }
        int width = getWidth();
        int height = getHeight();
        int max = Math.max(1, (int) (((float) width) / f2));
        int max2 = Math.max(1, (int) (((float) height) / f2));
        if (this.k != null) {
            Bitmap bitmap = this.j;
            if (bitmap != null) {
                if (bitmap == null) {
                    kd4.a();
                    throw null;
                } else if (bitmap.getWidth() == max) {
                    Bitmap bitmap2 = this.j;
                    if (bitmap2 == null) {
                        kd4.a();
                        throw null;
                    } else if (bitmap2.getHeight() == max2) {
                        return true;
                    }
                }
            }
        }
        c();
        try {
            this.i = Bitmap.createBitmap(max, max2, Bitmap.Config.ARGB_8888);
            if (this.i == null) {
                c();
                return false;
            }
            Bitmap bitmap3 = this.i;
            if (bitmap3 != null) {
                this.k = new Canvas(bitmap3);
                RenderScript renderScript3 = this.l;
                if (renderScript3 != null) {
                    Bitmap bitmap4 = this.i;
                    if (bitmap4 != null) {
                        this.n = Allocation.a(renderScript3, bitmap4, Allocation.MipmapControl.MIPMAP_NONE, 1);
                        RenderScript renderScript4 = this.l;
                        if (renderScript4 != null) {
                            Allocation allocation = this.n;
                            if (allocation != null) {
                                this.o = Allocation.a(renderScript4, allocation.e());
                                this.j = Bitmap.createBitmap(max, max2, Bitmap.Config.ARGB_8888);
                                if (this.j != null) {
                                    return true;
                                }
                                c();
                                return false;
                            }
                            kd4.a();
                            throw null;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        } catch (OutOfMemoryError unused) {
            c();
            return false;
        } catch (Throwable unused2) {
            c();
            return false;
        }
    }

    @DexIgnore
    public final void a(Bitmap bitmap, Bitmap bitmap2) {
        Allocation allocation = this.n;
        if (allocation != null) {
            allocation.a(bitmap);
            Cif ifVar = this.m;
            if (ifVar != null) {
                ifVar.c(this.n);
                Cif ifVar2 = this.m;
                if (ifVar2 != null) {
                    ifVar2.b(this.o);
                    Allocation allocation2 = this.o;
                    if (allocation2 != null) {
                        allocation2.b(bitmap2);
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, Bitmap bitmap, int i2) {
        if (bitmap != null) {
            this.q.right = bitmap.getWidth();
            this.q.bottom = bitmap.getHeight();
            this.r.right = getWidth();
            this.r.bottom = getHeight();
            canvas.drawBitmap(bitmap, this.q, this.r, (Paint) null);
        }
        canvas.drawColor(i2);
    }
}
