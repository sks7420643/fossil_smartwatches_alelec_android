package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ot3;
import com.fossil.blesdk.obfuscated.pt3;
import com.fossil.blesdk.obfuscated.r6;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.ts3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WLHeartRateChart extends View {
    @DexIgnore
    public short A;
    @DexIgnore
    public short B;
    @DexIgnore
    public Paint C;
    @DexIgnore
    public Paint D;
    @DexIgnore
    public Paint E;
    @DexIgnore
    public Paint F;
    @DexIgnore
    public Paint G;
    @DexIgnore
    public Paint H;
    @DexIgnore
    public Paint I;
    @DexIgnore
    public List<RectF> J;
    @DexIgnore
    public STATE K; // = STATE.AVERAGE;
    @DexIgnore
    public List<pt3> L;
    @DexIgnore
    public int M; // = TYPE.NONE.ordinal();
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public Integer i;
    @DexIgnore
    public float j;
    @DexIgnore
    public Integer k;
    @DexIgnore
    public float l;
    @DexIgnore
    public float m;
    @DexIgnore
    public float n;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public float q;
    @DexIgnore
    public float r;
    @DexIgnore
    public float s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public short y;
    @DexIgnore
    public short z;

    @DexIgnore
    public enum STATE {
        AVERAGE,
        RESTING,
        PEAK
    }

    @DexIgnore
    public enum TYPE {
        GENERAL,
        DAY,
        WEEK,
        MONTH,
        NONE
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        kd4.a((Object) WLHeartRateChart.class.getSimpleName(), "WLHeartRateChart::class.java.simpleName");
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WLHeartRateChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        kd4.b(context, "context");
        kd4.b(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.WLHeartRateChart);
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.getColor(2, -3355444);
            this.e = obtainStyledAttributes.getColor(0, -3355444);
            this.f = obtainStyledAttributes.getColor(1, -3355444);
            this.g = obtainStyledAttributes.getColor(5, -3355444);
            this.h = obtainStyledAttributes.getColor(7, -3355444);
            this.i = Integer.valueOf(obtainStyledAttributes.getResourceId(4, -1));
            this.j = obtainStyledAttributes.getDimension(3, ts3.c(13.0f));
            this.k = Integer.valueOf(obtainStyledAttributes.getResourceId(9, -1));
            this.l = obtainStyledAttributes.getDimension(8, ts3.c(13.0f));
            this.m = obtainStyledAttributes.getDimension(6, ts3.a(4.0f));
            this.M = obtainStyledAttributes.getInt(10, TYPE.NONE.ordinal());
            this.C = new Paint();
            this.C.setColor(this.e);
            this.C.setStrokeWidth(2.0f);
            this.C.setStyle(Paint.Style.STROKE);
            this.D = new Paint();
            this.D.setColor(this.e);
            this.D.setStyle(Paint.Style.STROKE);
            this.D.setPathEffect(new DashPathEffect(new float[]{6.0f, 6.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            this.E = new Paint(1);
            this.E.setColor(this.h);
            this.E.setStyle(Paint.Style.FILL);
            this.E.setTextSize(this.j);
            Integer num = this.i;
            if (num == null || num.intValue() != -1) {
                Paint paint = this.E;
                Integer num2 = this.i;
                if (num2 != null) {
                    paint.setTypeface(r6.a(context, num2.intValue()));
                } else {
                    kd4.a();
                    throw null;
                }
            }
            this.F = new Paint(1);
            this.F.setColor(this.h);
            this.F.setStyle(Paint.Style.FILL);
            this.F.setTextSize(this.l);
            Integer num3 = this.k;
            if (num3 == null || num3.intValue() != -1) {
                Paint paint2 = this.F;
                Integer num4 = this.k;
                if (num4 != null) {
                    paint2.setTypeface(r6.a(context, num4.intValue()));
                } else {
                    kd4.a();
                    throw null;
                }
            }
            this.G = new Paint(1);
            this.G.setColor(this.f);
            this.G.setStyle(Paint.Style.FILL);
            this.H = new Paint(1);
            this.H.setColor(-1);
            this.H.setStyle(Paint.Style.FILL);
            this.I = new Paint(1);
            this.I.setColor(this.g);
            this.I.setStyle(Paint.Style.STROKE);
            Rect rect = new Rect();
            this.E.getTextBounds("222", 0, 3, rect);
            this.v = (float) rect.width();
            this.w = (float) rect.height();
            Rect rect2 = new Rect();
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string._12am);
            this.F.getTextBounds(a2, 0, a2.length(), rect2);
            this.x = (float) rect2.height();
            this.L = new ArrayList();
            this.J = new ArrayList();
            obtainStyledAttributes.recycle();
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        e(canvas);
        float size = (this.t - this.s) / ((float) ((this.L.size() + this.L.size()) - 1));
        a(canvas, size, ((this.t - this.s) - (((float) this.L.size()) * size)) / ((float) (this.L.size() - 1)));
        a(canvas, cb4.d(sm2.a((Context) PortfolioApp.W.c(), (int) R.string._12am), sm2.a((Context) PortfolioApp.W.c(), (int) R.string._12pm), sm2.a((Context) PortfolioApp.W.c(), (int) R.string._12am)), cb4.d(Float.valueOf(this.J.get(0).left), Float.valueOf(this.J.get(11).left), Float.valueOf(this.J.get(23).left)), this.o + (((((float) getMeasuredHeight()) - this.o) + this.x) / ((float) 2)));
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        this.u = this.o;
        f(canvas);
        float size = (this.t - this.s) / ((float) ((this.L.size() + this.L.size()) - 1));
        a(canvas, size, ((this.t - this.s) - (((float) this.L.size()) * size)) / ((float) (this.L.size() - 1)));
        a(canvas, cb4.d(sm2.a((Context) PortfolioApp.W.c(), (int) R.string._12am), sm2.a((Context) PortfolioApp.W.c(), (int) R.string._12pm), sm2.a((Context) PortfolioApp.W.c(), (int) R.string._12am)), cb4.d(Float.valueOf(this.J.get(0).left), Float.valueOf(this.J.get(11).left), Float.valueOf(this.J.get(23).left)), this.o + (((((float) getMeasuredHeight()) - this.o) + this.x) / ((float) 2)));
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        e(canvas);
        float size = (this.t - this.s) / ((float) ((this.L.size() + this.L.size()) - 1));
        a(canvas, size, ((this.t - this.s) - (((float) this.L.size()) * size)) / ((float) (this.L.size() - 1)));
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        float measuredHeight = this.o + (((((float) getMeasuredHeight()) - this.o) + this.x) / ((float) 2));
        int size2 = this.L.size() / 4;
        int i2 = 0;
        int i3 = 0;
        while (i2 < this.L.size()) {
            arrayList.add(String.valueOf(i2 + 1));
            arrayList2.add(Float.valueOf(this.J.get(i2).left));
            int i4 = i3 + 1;
            int i5 = i3 + (size2 * i4);
            i3 = i4;
            i2 = i5;
        }
        arrayList.add(String.valueOf(this.J.size()));
        List<RectF> list = this.J;
        arrayList2.add(Float.valueOf(list.get(cb4.a(list)).left));
        a(canvas, arrayList, arrayList2, measuredHeight);
    }

    @DexIgnore
    public final void d(Canvas canvas) {
        e(canvas);
        float f2 = this.t;
        float f3 = this.s;
        float f4 = (f2 - f3) / ((float) 48);
        a(canvas, f4, ((f2 - f3) - (((float) 7) * f4)) / ((float) 6));
        List d = cb4.d(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__S), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__M), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__T), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__W), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__T_1), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__F), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_Steps7days_Label__S_1));
        List<RectF> list = this.J;
        ArrayList arrayList = new ArrayList(db4.a(list, 10));
        for (RectF rectF : list) {
            arrayList.add(Float.valueOf(rectF.left));
        }
        a(canvas, d, kb4.d(arrayList), this.o + (((((float) getMeasuredHeight()) - this.o) + this.x) / ((float) 2)));
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        canvas.drawLine(this.p, this.o, (float) getMeasuredWidth(), this.o, this.C);
        canvas.drawLine(this.p, this.n, (float) getMeasuredWidth(), this.n, this.C);
        float f2 = (float) 2;
        float measuredWidth = this.q + (((((float) getMeasuredWidth()) - this.q) - this.E.measureText(String.valueOf(this.A))) / f2);
        float f3 = this.u;
        float f4 = (float) 1;
        float f5 = (float) 4;
        float f6 = f3 - (((f3 - this.n) * f4) / f5);
        canvas.drawText(String.valueOf(this.A), measuredWidth, (this.w / f2) + f6, this.E);
        float measuredWidth2 = this.q + (((((float) getMeasuredWidth()) - this.q) - this.E.measureText(String.valueOf(this.B))) / f2);
        float f7 = this.r;
        float f8 = f7 + (((this.u - f7) * f4) / f5);
        canvas.drawText(String.valueOf(this.B), measuredWidth2, (this.w / f2) + f8, this.E);
        Path path = new Path();
        path.moveTo(this.p, f6);
        path.lineTo(this.q, f6);
        canvas.drawPath(path, this.D);
        path.moveTo(this.p, f8);
        path.lineTo(this.q, f8);
        canvas.drawPath(path, this.D);
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        float f2 = (float) 2;
        float measuredWidth = this.q + (((((float) getMeasuredWidth()) - this.q) - this.E.measureText(String.valueOf(this.z))) / f2);
        float f3 = this.r;
        canvas.drawText(String.valueOf(this.z), measuredWidth, (this.w / f2) + f3, this.E);
        float measuredWidth2 = this.q + (((((float) getMeasuredWidth()) - this.q) - this.E.measureText(String.valueOf(this.y))) / f2);
        float f4 = this.u;
        canvas.drawText(String.valueOf(this.y), measuredWidth2, (this.w / f2) + f4, this.E);
        Path path = new Path();
        path.moveTo(this.p, f3);
        path.lineTo(this.q, f3);
        canvas.drawPath(path, this.D);
        path.moveTo(this.p, f4);
        path.lineTo(this.q, f4);
        canvas.drawPath(path, this.D);
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        kd4.b(canvas, "canvas");
        this.n = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.o = ((float) getMeasuredHeight()) - (this.x * ((float) 2));
        this.p = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.q = ((float) getMeasuredWidth()) - (this.v * 1.5f);
        this.r = this.l;
        this.s = this.p + ts3.a(5.0f);
        this.u = this.o - this.l;
        this.t = this.q - ts3.a(5.0f);
        int i2 = this.M;
        if (i2 == TYPE.GENERAL.ordinal()) {
            b(canvas);
        } else if (i2 == TYPE.DAY.ordinal()) {
            if (!this.L.isEmpty()) {
                a(canvas);
            }
        } else if (i2 == TYPE.WEEK.ordinal()) {
            if (!this.L.isEmpty()) {
                d(canvas);
            }
        } else if (i2 == TYPE.MONTH.ordinal() && (!this.L.isEmpty())) {
            c(canvas);
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, float f2, float f3) {
        Canvas canvas2 = canvas;
        this.J.clear();
        int i2 = 0;
        int i3 = 0;
        for (T next : this.L) {
            int i4 = i3 + 1;
            if (i3 >= 0) {
                pt3 pt3 = (pt3) next;
                short a2 = pt3.a();
                short b = pt3.b();
                float f4 = this.r;
                float f5 = this.u;
                short s2 = this.z;
                short s3 = this.y;
                float f6 = (((f5 - f4) / ((float) (s2 - s3))) * ((float) (s2 - b))) + f4;
                float f7 = this.s + ((f2 + f3) * ((float) i3));
                RectF rectF = new RectF(f7, f6, f7 + f2, (((float) (b - a2)) * ((f5 - f4) / ((float) (s2 - s3)))) + f6);
                float f8 = this.m;
                canvas2.drawRoundRect(rectF, f8, f8, this.G);
                if (a2 == -1 && b == -1) {
                    rectF.top = 1.0f;
                    rectF.bottom = -1.0f;
                }
                this.J.add(rectF);
                i3 = i4;
            } else {
                cb4.c();
                throw null;
            }
        }
        int i5 = ot3.a[this.K.ordinal()];
        if (i5 != 1 && i5 == 2 && (!this.J.isEmpty())) {
            float f9 = f2 / ((float) 2);
            this.I.setStrokeWidth(f9 / 1.5f);
            for (T next2 : this.J) {
                int i6 = i2 + 1;
                if (i2 >= 0) {
                    RectF rectF2 = (RectF) next2;
                    if (i2 < cb4.a(this.J)) {
                        RectF rectF3 = this.J.get(i6);
                        float f10 = rectF2.top;
                        if (f10 <= rectF2.bottom) {
                            float f11 = rectF3.top;
                            if (f11 <= rectF3.bottom) {
                                canvas.drawLine(rectF2.left + f9, f10 + f9, rectF3.left + f9, f11 + f9, this.I);
                            }
                        }
                    }
                    float f12 = rectF2.top;
                    if (f12 <= rectF2.bottom) {
                        canvas2.drawCircle(rectF2.left + f9, f12 + f9, f9, this.H);
                        canvas2.drawCircle(rectF2.left + f9, rectF2.top + f9, f9, this.I);
                    }
                    i2 = i6;
                } else {
                    cb4.c();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, List<String> list, List<Float> list2, float f2) {
        int i2 = 0;
        for (T next : list) {
            int i3 = i2 + 1;
            if (i2 >= 0) {
                canvas.drawText((String) next, list2.get(i2).floatValue(), f2, this.F);
                i2 = i3;
            } else {
                cb4.c();
                throw null;
            }
        }
    }
}
