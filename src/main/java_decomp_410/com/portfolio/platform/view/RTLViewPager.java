package com.portfolio.platform.view;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewpager.widget.ViewPager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.b8;
import com.fossil.blesdk.obfuscated.g4;
import com.fossil.blesdk.obfuscated.wi;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class RTLViewPager extends ViewPager {
    @DexIgnore
    public /* final */ Map<ViewPager.i, d> o0; // = new g4(1);
    @DexIgnore
    public DataSetObserver p0;
    @DexIgnore
    public boolean q0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends wi {
        @DexIgnore
        public /* final */ wi b;

        @DexIgnore
        public a(RTLViewPager rTLViewPager, wi wiVar) {
            this.b = wiVar;
        }

        @DexIgnore
        public int a() {
            return this.b.a();
        }

        @DexIgnore
        public float b(int i) {
            return this.b.b(i);
        }

        @DexIgnore
        public wi c() {
            return this.b;
        }

        @DexIgnore
        public boolean a(View view, Object obj) {
            return this.b.a(view, obj);
        }

        @DexIgnore
        public void b(ViewGroup viewGroup, int i, Object obj) {
            this.b.b(viewGroup, i, obj);
        }

        @DexIgnore
        public void c(DataSetObserver dataSetObserver) {
            this.b.c(dataSetObserver);
        }

        @DexIgnore
        public CharSequence a(int i) {
            return this.b.a(i);
        }

        @DexIgnore
        public Parcelable b() {
            return this.b.b();
        }

        @DexIgnore
        public int a(Object obj) {
            return this.b.a(obj);
        }

        @DexIgnore
        public void b(ViewGroup viewGroup) {
            this.b.b(viewGroup);
        }

        @DexIgnore
        public Object a(ViewGroup viewGroup, int i) {
            return this.b.a(viewGroup, i);
        }

        @DexIgnore
        public void a(ViewGroup viewGroup, int i, Object obj) {
            this.b.a(viewGroup, i, obj);
        }

        @DexIgnore
        public void a(DataSetObserver dataSetObserver) {
            this.b.a(dataSetObserver);
        }

        @DexIgnore
        public void a(Parcelable parcelable, ClassLoader classLoader) {
            this.b.a(parcelable, classLoader);
        }

        @DexIgnore
        public void a(ViewGroup viewGroup) {
            this.b.a(viewGroup);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends DataSetObserver {
        @DexIgnore
        public /* final */ c a;

        @DexIgnore
        public b(c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public void onChanged() {
            super.onChanged();
            this.a.d();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends a {
        @DexIgnore
        public int c;

        @DexIgnore
        public c(wi wiVar) {
            super(RTLViewPager.this, wiVar);
            this.c = wiVar.a();
        }

        @DexIgnore
        public CharSequence a(int i) {
            return super.a(c(i));
        }

        @DexIgnore
        public float b(int i) {
            return super.b(c(i));
        }

        @DexIgnore
        public final int c(int i) {
            return (a() - i) - 1;
        }

        @DexIgnore
        public void d() {
            int a = a();
            int i = this.c;
            if (a != i) {
                RTLViewPager.this.setCurrentItemWithoutNotification(Math.max(0, i - 1));
                this.c = a;
            }
        }

        @DexIgnore
        public int a(Object obj) {
            int a = super.a(obj);
            return a < 0 ? a : c(a);
        }

        @DexIgnore
        public void b(ViewGroup viewGroup, int i, Object obj) {
            super.b(viewGroup, (this.c - i) - 1, obj);
        }

        @DexIgnore
        public Object a(ViewGroup viewGroup, int i) {
            return super.a(viewGroup, c(i));
        }

        @DexIgnore
        public void a(ViewGroup viewGroup, int i, Object obj) {
            super.a(viewGroup, c(i), obj);
        }
    }

    @DexIgnore
    public RTLViewPager(Context context) {
        super(context);
    }

    @DexIgnore
    public final void a(wi wiVar) {
        if ((wiVar instanceof c) && this.p0 == null) {
            c cVar = (c) wiVar;
            this.p0 = new b(cVar);
            wiVar.a(this.p0);
            cVar.d();
        }
    }

    @DexIgnore
    public void b(float f) {
        if (!n()) {
            f = -f;
        }
        super.b(f);
    }

    @DexIgnore
    public final int g(int i) {
        if (i < 0 || !n()) {
            return i;
        }
        if (getAdapter() == null) {
            return 0;
        }
        return (getAdapter().a() - i) - 1;
    }

    @DexIgnore
    public wi getAdapter() {
        wi adapter = super.getAdapter();
        return adapter instanceof c ? ((c) adapter).c() : adapter;
    }

    @DexIgnore
    public int getCurrentItem() {
        return g(super.getCurrentItem());
    }

    @DexIgnore
    public final boolean n() {
        return b8.b(getContext().getResources().getConfiguration().locale) == 1;
    }

    @DexIgnore
    public final void o() {
        wi adapter = super.getAdapter();
        if (adapter instanceof c) {
            DataSetObserver dataSetObserver = this.p0;
            if (dataSetObserver != null) {
                adapter.c(dataSetObserver);
                this.p0 = null;
            }
        }
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        a(super.getAdapter());
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        o();
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        e eVar = (e) parcelable;
        super.onRestoreInstanceState(eVar.e);
        if (eVar.g != n()) {
            a(eVar.f, false);
        }
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        return new e(super.onSaveInstanceState(), getCurrentItem(), n());
    }

    @DexIgnore
    public void setAdapter(wi wiVar) {
        o();
        boolean z = wiVar != null && n();
        if (z) {
            c cVar = new c(wiVar);
            a((wi) cVar);
            wiVar = cVar;
        }
        super.setAdapter(wiVar);
        if (z) {
            setCurrentItemWithoutNotification(0);
        }
    }

    @DexIgnore
    public void setCurrentItem(int i) {
        super.setCurrentItem(g(i));
    }

    @DexIgnore
    public void setCurrentItemWithoutNotification(int i) {
        this.q0 = true;
        a(i, false);
        this.q0 = false;
    }

    @DexIgnore
    public void b(ViewPager.i iVar) {
        if (n()) {
            iVar = this.o0.remove(iVar);
        }
        super.b(iVar);
    }

    @DexIgnore
    public RTLViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements ViewPager.i {
        @DexIgnore
        public /* final */ ViewPager.i e;
        @DexIgnore
        public int f; // = -1;

        @DexIgnore
        public d(ViewPager.i iVar) {
            this.e = iVar;
        }

        @DexIgnore
        public void a(int i, float f2, int i2) {
            if (!RTLViewPager.this.q0) {
                int i3 = (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : (f2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 0 : -1));
                if (i3 == 0 && i2 == 0) {
                    this.f = c(i);
                } else {
                    this.f = c(i + 1);
                }
                ViewPager.i iVar = this.e;
                int i4 = this.f;
                if (i3 > 0) {
                    f2 = 1.0f - f2;
                }
                iVar.a(i4, f2, i2);
            }
        }

        @DexIgnore
        public void b(int i) {
            if (!RTLViewPager.this.q0) {
                this.e.b(i);
            }
        }

        @DexIgnore
        public final int c(int i) {
            wi adapter = RTLViewPager.this.getAdapter();
            return adapter == null ? i : (adapter.a() - i) - 1;
        }

        @DexIgnore
        public void a(int i) {
            if (!RTLViewPager.this.q0) {
                this.e.a(c(i));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.ClassLoaderCreator<e> CREATOR; // = new a();
        @DexIgnore
        public /* final */ Parcelable e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ boolean g;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a implements Parcelable.ClassLoaderCreator<e> {
            @DexIgnore
            public e[] newArray(int i) {
                return new e[i];
            }

            @DexIgnore
            public e createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new e(parcel, classLoader);
            }

            @DexIgnore
            public e createFromParcel(Parcel parcel) {
                return new e(parcel, (ClassLoader) null);
            }
        }

        @DexIgnore
        public e(Parcelable parcelable, int i, boolean z) {
            this.e = parcelable;
            this.f = i;
            this.g = z;
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeParcelable(this.e, i);
            parcel.writeInt(this.f);
            parcel.writeByte(this.g ? (byte) 1 : 0);
        }

        @DexIgnore
        public e(Parcel parcel, ClassLoader classLoader) {
            this.e = parcel.readParcelable(classLoader == null ? e.class.getClassLoader() : classLoader);
            this.f = parcel.readInt();
            this.g = parcel.readByte() != 0;
        }
    }

    @DexIgnore
    public void a(int i, boolean z) {
        super.a(g(i), z);
    }

    @DexIgnore
    public void a(ViewPager.i iVar) {
        if (n()) {
            d dVar = new d(iVar);
            this.o0.put(iVar, dVar);
            iVar = dVar;
        }
        super.a(iVar);
    }
}
