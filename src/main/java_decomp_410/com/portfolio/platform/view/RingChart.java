package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.data.background.BackgroundImageConfig;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RingChart extends View {
    @DexIgnore
    public /* final */ ArrayList<b> e; // = new ArrayList<>();
    @DexIgnore
    public float f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public float i;
    @DexIgnore
    public Paint j; // = new Paint(1);
    @DexIgnore
    public Paint k;
    @DexIgnore
    public Paint l;
    @DexIgnore
    public GestureDetector m;
    @DexIgnore
    public View.OnClickListener n;
    @DexIgnore
    public RectF o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onDown(MotionEvent motionEvent) {
            kd4.b(motionEvent, "e");
            return RingChart.this.getMListener$app_fossilRelease() != null;
        }

        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            kd4.b(motionEvent, "e");
            if (RingChart.this.getMListener$app_fossilRelease() != null) {
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                float measuredWidth = (float) (RingChart.this.getMeasuredWidth() / 2);
                float f = x - measuredWidth;
                float f2 = y - measuredWidth;
                if ((f * f) + (f2 * f2) < (RingChart.this.getMThickness$app_fossilRelease() + measuredWidth) * (measuredWidth + RingChart.this.getMThickness$app_fossilRelease())) {
                    View.OnClickListener mListener$app_fossilRelease = RingChart.this.getMListener$app_fossilRelease();
                    if (mListener$app_fossilRelease != null) {
                        mListener$app_fossilRelease.onClick(RingChart.this);
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            }
            return super.onSingleTapUp(motionEvent);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b {
        @DexIgnore
        public float a;
        @DexIgnore
        public int b;
        @DexIgnore
        public float c;
        @DexIgnore
        public float d;

        @DexIgnore
        public b(RingChart ringChart) {
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final void b(float f) {
            this.c = f;
        }

        @DexIgnore
        public final void c(float f) {
            this.a = f;
        }

        @DexIgnore
        public final float d() {
            return this.a;
        }

        @DexIgnore
        public final void a(int i) {
            this.b = i;
        }

        @DexIgnore
        public final float b() {
            return this.d;
        }

        @DexIgnore
        public final float c() {
            return this.c;
        }

        @DexIgnore
        public final void a(float f) {
            this.d = f;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingChart(Context context) {
        super(context);
        kd4.b(context, "context");
        a();
    }

    @DexIgnore
    public final void a(float f2, int i2) {
        b bVar = new b(this);
        bVar.a(i2);
        bVar.c(f2);
        this.f += f2;
        this.e.add(bVar);
    }

    @DexIgnore
    public final void b() {
        float max = Math.max(this.f, (float) this.g);
        Iterator<b> it = this.e.iterator();
        float f2 = -90.0f;
        while (it.hasNext()) {
            b next = it.next();
            next.b(f2);
            if (max != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                f2 += (next.d() / max) * 360.0f;
            }
            next.a(f2);
            if (next.b() > ((float) BackgroundImageConfig.LEFT_BACKGROUND_ANGLE)) {
                next.a(270.0f);
            }
            f2 = next.b();
        }
        invalidate();
    }

    @DexIgnore
    public final View.OnClickListener getMListener$app_fossilRelease() {
        return this.n;
    }

    @DexIgnore
    public final float getMThickness$app_fossilRelease() {
        return this.i;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        float f2;
        kd4.b(canvas, "canvas");
        super.onDraw(canvas);
        RectF rectF = this.o;
        if (rectF != null) {
            float centerX = rectF.centerX();
            RectF rectF2 = this.o;
            if (rectF2 != null) {
                float centerY = rectF2.centerY();
                RectF rectF3 = this.o;
                if (rectF3 != null) {
                    float f3 = (float) 2;
                    float width = rectF3.width() / f3;
                    Paint paint = this.l;
                    if (paint != null) {
                        canvas.drawCircle(centerX, centerY, width, paint);
                        Iterator<b> it = this.e.iterator();
                        while (it.hasNext()) {
                            b next = it.next();
                            this.j.setColor(next.a());
                            if (next.b() > next.c()) {
                                RectF rectF4 = this.o;
                                if (rectF4 != null) {
                                    canvas.drawArc(rectF4, next.c(), next.b() - next.c(), true, this.j);
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            }
                        }
                        float f4 = this.f;
                        if (f4 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f4 < ((float) this.g)) {
                            this.j.setColor(this.h);
                            if (this.e.isEmpty() || this.f == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                f2 = -90.0f;
                            } else {
                                ArrayList<b> arrayList = this.e;
                                f2 = arrayList.get(arrayList.size() - 1).b();
                            }
                            if (270.0f > f2) {
                                RectF rectF5 = this.o;
                                if (rectF5 != null) {
                                    canvas.drawArc(rectF5, f2, 270.0f - f2, true, this.j);
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            }
                        }
                        RectF rectF6 = this.o;
                        if (rectF6 != null) {
                            float centerX2 = rectF6.centerX();
                            RectF rectF7 = this.o;
                            if (rectF7 != null) {
                                float centerY2 = rectF7.centerY();
                                RectF rectF8 = this.o;
                                if (rectF8 != null) {
                                    float width2 = (rectF8.width() / f3) - this.i;
                                    Paint paint2 = this.k;
                                    if (paint2 != null) {
                                        canvas.drawCircle(centerX2, centerY2, width2, paint2);
                                    } else {
                                        kd4.d("mClearPaint");
                                        throw null;
                                    }
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.d("mBackgroundPaint");
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        RectF rectF = this.o;
        if (rectF == null) {
            this.o = new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i2, (float) i3);
        } else if (rectF != null) {
            rectF.right = (float) i2;
            if (rectF != null) {
                rectF.bottom = (float) i3;
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
        b();
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        kd4.b(motionEvent, Constants.EVENT);
        GestureDetector gestureDetector = this.m;
        if (gestureDetector != null) {
            return gestureDetector.onTouchEvent(motionEvent);
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void setGoal(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public final void setMListener$app_fossilRelease(View.OnClickListener onClickListener) {
        this.n = onClickListener;
    }

    @DexIgnore
    public final void setMThickness$app_fossilRelease(float f2) {
        this.i = f2;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        kd4.b(context, "context");
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, h62.RingChart, 0, 0);
        try {
            this.g = obtainStyledAttributes.getInteger(1, 0);
            this.h = obtainStyledAttributes.getColor(0, 0);
            Resources resources = context.getResources();
            kd4.a((Object) resources, "context.resources");
            this.i = obtainStyledAttributes.getDimension(2, resources.getDisplayMetrics().density * ((float) 8));
            obtainStyledAttributes.recycle();
            a();
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    @DexIgnore
    public final void a() {
        setLayerType(1, (Paint) null);
        this.j.setStyle(Paint.Style.FILL);
        this.k = new Paint(this.j);
        Paint paint = this.k;
        if (paint != null) {
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            this.m = new GestureDetector(getContext(), new a());
            this.l = new Paint(this.j);
            Paint paint2 = this.l;
            if (paint2 != null) {
                paint2.setAlpha(50);
                if (isInEditMode()) {
                    Random random = new Random();
                    float f2 = (float) 100;
                    float nextFloat = random.nextFloat() * f2;
                    float f3 = f2 - nextFloat;
                    float nextFloat2 = random.nextFloat() * f3;
                    a(nextFloat, -65536);
                    a(nextFloat2, -16711936);
                    a(random.nextFloat() * (f3 - nextFloat2), -16776961);
                    if (this.g == 0) {
                        this.g = 100;
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBackgroundPaint");
            throw null;
        }
        kd4.d("mClearPaint");
        throw null;
    }
}
