package com.portfolio.platform.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h62;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RingProgressBar extends View {
    @DexIgnore
    public ObjectAnimator A;
    @DexIgnore
    public int B;
    @DexIgnore
    public /* final */ RectF e;
    @DexIgnore
    public /* final */ RectF f;
    @DexIgnore
    public /* final */ RectF g;
    @DexIgnore
    public int h;
    @DexIgnore
    public float i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public float k;
    @DexIgnore
    public float l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;
    @DexIgnore
    public Paint o;
    @DexIgnore
    public Paint p;
    @DexIgnore
    public Paint q;
    @DexIgnore
    public Paint r;
    @DexIgnore
    public Paint s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public float y;
    @DexIgnore
    public int z;

    @DexIgnore
    public enum Type {
        ACTIVE_TIME,
        STEPS,
        CALORIES,
        SLEEP,
        GOAL
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;

        @DexIgnore
        public b(RingProgressBar ringProgressBar) {
            this.a = ringProgressBar;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.a.invalidate();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public c(RingProgressBar ringProgressBar, int i) {
            this.a = ringProgressBar;
            this.b = i;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            kd4.b(animator, "animator");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            kd4.b(animator, "animator");
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            kd4.b(animator, "animator");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            kd4.b(animator, "animator");
            this.a.setMReachAnimateState$app_fossilRelease(this.b);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;

        @DexIgnore
        public d(RingProgressBar ringProgressBar) {
            this.a = ringProgressBar;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            RingProgressBar ringProgressBar = this.a;
            float f = (float) 2;
            ringProgressBar.invalidate((ringProgressBar.getWidth() / 2) - ((int) ((((float) this.a.w) * 1.15f) / f)), (int) ((this.a.i / 2.0f) + (this.a.i * 0.14999998f)), (this.a.getWidth() / 2) + ((int) ((((float) this.a.w) * 1.15f) / f)), (int) ((this.a.i / 2.0f) + (this.a.i * 0.14999998f) + (((float) this.a.w) * 1.15f)));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;

        @DexIgnore
        public e(RingProgressBar ringProgressBar) {
            this.a = ringProgressBar;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            kd4.b(animator, "animator");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            kd4.b(animator, "animator");
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            kd4.b(animator, "animator");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            kd4.b(animator, "animator");
            this.a.setMReachAnimateState$app_fossilRelease(1);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public f(RingProgressBar ringProgressBar, boolean z) {
            this.a = ringProgressBar;
            this.b = z;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            if (this.a.getMProgress$app_fossilRelease() >= ((float) 1) && this.a.getMReachAnimateState$app_fossilRelease() == 0) {
                if (this.b) {
                    RingProgressBar ringProgressBar = this.a;
                    ringProgressBar.b(ringProgressBar.getMProgressIndex$app_fossilRelease());
                    return;
                }
                this.a.b(0);
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public RingProgressBar(Context context) {
        this(context, (AttributeSet) null);
        kd4.b(context, "context");
    }

    @DexIgnore
    private final void setIconBackgroundColor(int i2) {
        this.x = i2;
        invalidate();
    }

    @DexIgnore
    private final void setIconSize(int i2) {
        this.w = i2;
        invalidate();
    }

    @DexIgnore
    private final void setMaxProgress(float f2) {
        this.y = f2;
        invalidate();
    }

    @DexIgnore
    private final void setProgressBackgroundColor(int i2) {
        this.m = i2;
        b();
    }

    @DexIgnore
    private final void setSpaceWidth(int i2) {
        b();
        c();
    }

    @DexIgnore
    public final void c() {
        a(this.o, this.n, (float) this.h);
        a(this.q, b(this.n, 30), ((float) this.h) * 1.9f);
        a(this.r, b(this.n, 30), ((float) this.h) * 2.7f);
        a(this.s, b(this.n, 30), ((float) this.h) * 3.5f);
        invalidate();
    }

    @DexIgnore
    public final float getMProgress$app_fossilRelease() {
        return this.k;
    }

    @DexIgnore
    public final int getMProgressIndex$app_fossilRelease() {
        return this.z;
    }

    @DexIgnore
    public final int getMReachAnimateState$app_fossilRelease() {
        return this.B;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        kd4.b(canvas, "canvas");
        canvas.translate(this.t, this.u);
        float f2 = this.k * ((float) 360);
        canvas.drawArc(this.e, 270.0f, -(360.0f - f2), false, this.p);
        canvas.drawArc(this.e, 270.0f, this.k > 1.0f ? 360.0f : f2, false, this.o);
        float f3 = this.k;
        if (f3 > 1.0f) {
            canvas.drawArc(this.f, 270.0f, f3 > 2.0f ? 360.0f : f2 - 360.0f, false, this.o);
        }
        a(canvas, f2);
        canvas.translate(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, -this.u);
        a(canvas);
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (i2 <= i3) {
            i2 = i3;
        }
        int defaultSize = View.getDefaultSize(getSuggestedMinimumWidth() + getPaddingLeft() + getPaddingRight(), i2);
        setMeasuredDimension(defaultSize, defaultSize);
        float f2 = ((float) defaultSize) / 2.0f;
        int i4 = this.h;
        this.i = (float) (i4 + 2);
        float f3 = (f2 - ((float) i4)) - ((((float) this.w) * 1.15f) / ((float) 2));
        float f4 = this.i;
        float f5 = f3 - f4;
        float f6 = f5 - f4;
        float f7 = -f3;
        this.e.set(f7, f7, f3, f3);
        float f8 = -f5;
        this.f.set(f8, f8, f5, f5);
        float f9 = -f6;
        this.g.set(f9, f9, f6, f6);
        this.t = f2;
        this.u = f2;
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        kd4.b(parcelable, "state");
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            setProgress(bundle.getFloat("mProgress"));
            int i2 = bundle.getInt("progress_color");
            if (i2 != this.n) {
                this.n = i2;
                c();
            }
            int i3 = bundle.getInt("progress_background_color");
            if (i3 != this.m) {
                this.m = i3;
                b();
            }
            super.onRestoreInstanceState(bundle.getParcelable("saved_state"));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("saved_state", super.onSaveInstanceState());
        bundle.putFloat("mProgress", this.k);
        bundle.putInt("progress_color", this.n);
        bundle.putInt("progress_background_color", this.m);
        return bundle;
    }

    @DexIgnore
    public final void setIconSource(int i2) {
        this.v = i2;
        invalidate();
    }

    @DexIgnore
    public final void setMProgress$app_fossilRelease(float f2) {
        this.k = f2;
    }

    @DexIgnore
    public final void setMProgressIndex$app_fossilRelease(int i2) {
        this.z = i2;
    }

    @DexIgnore
    public final void setMReachAnimateState$app_fossilRelease(int i2) {
        this.B = i2;
    }

    @DexIgnore
    @SuppressLint({"AnimatorKeep"})
    public final void setProgress(float f2) {
        if (f2 != this.k) {
            float f3 = this.y;
            if (f2 >= f3) {
                f2 = f3;
            }
            this.k = f2;
            if (!this.j) {
                invalidate();
            }
        }
    }

    @DexIgnore
    public final void setProgressColor(int i2) {
        this.n = i2;
        c();
    }

    @DexIgnore
    public final void setStrokeWidth(int i2) {
        this.h = i2;
        b();
        c();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public RingProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        kd4.b(context, "context");
    }

    @DexIgnore
    public final void a(Paint paint, int i2, float f2) {
        paint.setColor(i2);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(f2);
    }

    @DexIgnore
    public final int b(int i2, int i3) {
        return Color.argb(i3, Color.red(i2), Color.green(i2), Color.blue(i2));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingProgressBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        kd4.b(context, "context");
        this.e = new RectF();
        this.f = new RectF();
        this.g = new RectF();
        this.j = true;
        this.o = new Paint(1);
        this.p = new Paint(1);
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, h62.RingProgressBar, i2, 0);
        if (obtainStyledAttributes != null) {
            try {
                setProgressColor(obtainStyledAttributes.getColor(6, -16711681));
                setProgressBackgroundColor(obtainStyledAttributes.getColor(5, -16711936));
                setProgress(obtainStyledAttributes.getFloat(4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                setStrokeWidth((int) obtainStyledAttributes.getDimension(9, (float) 10));
                setSpaceWidth((int) obtainStyledAttributes.getDimension(8, (float) 2));
                setIconSize((int) obtainStyledAttributes.getDimension(1, (float) 60));
                setMaxProgress(obtainStyledAttributes.getFloat(3, 2.0f));
                setIconSource(obtainStyledAttributes.getResourceId(2, 0));
                setIconBackgroundColor(obtainStyledAttributes.getResourceId(0, 0));
                this.z = obtainStyledAttributes.getInteger(7, 0);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        b();
        c();
        this.j = false;
    }

    @DexIgnore
    public final void a(Canvas canvas, float f2) {
        Paint[] paintArr;
        int i2 = this.B;
        if (i2 == 2) {
            paintArr = new Paint[]{this.q};
        } else if (i2 != 3) {
            paintArr = i2 != 4 ? new Paint[0] : new Paint[]{this.s, this.r, this.q};
        } else {
            paintArr = new Paint[]{this.r, this.q};
        }
        for (Paint drawArc : paintArr) {
            canvas.drawArc(this.e, 270.0f, this.k > 1.0f ? 360.0f : f2, false, drawArc);
        }
    }

    @DexIgnore
    public final void b() {
        a(this.p, this.m, (float) this.h);
        invalidate();
    }

    @DexIgnore
    public final void b(int i2) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playSequentially(new Animator[]{a(170), a(60, 2), a(60, 3), a(120, 4), a(60, 3), a(60, 2), a(60, 0)});
        animatorSet.setStartDelay(((long) i2) * ((long) 170));
        animatorSet.start();
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), this.v);
        if (decodeResource != null) {
            int i2 = this.w;
            float f2 = this.i;
            float f3 = (((float) i2) * 0.14999998f) + (f2 / 2.0f);
            if (this.B == 1) {
                i2 = (int) (((float) i2) * 1.15f);
                f3 = (f2 / 2.0f) + (f2 * 0.14999998f);
            }
            Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeResource, i2, i2, false);
            Paint paint = new Paint(1);
            paint.setColorFilter(new PorterDuffColorFilter(k6.a(getContext(), this.x), PorterDuff.Mode.SRC_OVER));
            int i3 = this.w;
            float f4 = this.i;
            Canvas canvas2 = canvas;
            canvas2.drawRect((((float) (-i3)) * 1.15f) / 2.0f, (f4 / 2.0f) + (f4 * 0.14999998f) + 10.0f, (((float) i3) * 1.15f) / 2.0f, ((float) i3) * 2.0f, paint);
            paint.setColorFilter(new PorterDuffColorFilter(this.n, PorterDuff.Mode.SRC_IN));
            if (createScaledBitmap != null) {
                canvas.drawBitmap(createScaledBitmap, ((float) (-createScaledBitmap.getWidth())) / 2.0f, f3, paint);
                createScaledBitmap.recycle();
                return;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(float f2, boolean z2) {
        if (f2 != this.l) {
            a();
            a(f2, 1000, z2);
            this.l = f2;
        }
    }

    @DexIgnore
    public final void a(float f2, int i2, boolean z2) {
        this.A = ObjectAnimator.ofFloat(this, "progress", new float[]{this.k, f2});
        ObjectAnimator objectAnimator = this.A;
        if (objectAnimator != null) {
            objectAnimator.setDuration((long) i2);
            ObjectAnimator objectAnimator2 = this.A;
            if (objectAnimator2 != null) {
                objectAnimator2.reverse();
                ObjectAnimator objectAnimator3 = this.A;
                if (objectAnimator3 != null) {
                    objectAnimator3.addListener(new f(this, z2));
                    ObjectAnimator objectAnimator4 = this.A;
                    if (objectAnimator4 != null) {
                        objectAnimator4.start();
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a() {
        ObjectAnimator objectAnimator = this.A;
        if (objectAnimator == null) {
            return;
        }
        if (objectAnimator != null) {
            objectAnimator.cancel();
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    @SuppressLint({"ObjectAnimatorBinding"})
    public final ObjectAnimator a(int i2) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "", new float[]{1.0f});
        kd4.a((Object) ofFloat, "scaleAnimator");
        ofFloat.setDuration((long) i2);
        ofFloat.addUpdateListener(new d(this));
        ofFloat.addListener(new e(this));
        return ofFloat;
    }

    @DexIgnore
    @SuppressLint({"ObjectAnimatorBinding"})
    public final ObjectAnimator a(int i2, int i3) {
        ObjectAnimator ofInt = ObjectAnimator.ofInt(this, "", new int[]{0});
        kd4.a((Object) ofInt, "gradientAnimator");
        ofInt.setDuration((long) i2);
        ofInt.setStartDelay((long) 33);
        ofInt.addUpdateListener(new b(this));
        ofInt.addListener(new c(this, i3));
        return ofInt;
    }
}
