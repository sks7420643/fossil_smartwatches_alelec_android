package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import com.fossil.blesdk.obfuscated.ts3;
import com.fossil.blesdk.obfuscated.xn;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class FossilNotificationImageView extends ViewGroup {
    @DexIgnore
    public static /* final */ String j; // = FossilNotificationImageView.class.getSimpleName();
    @DexIgnore
    public /* final */ Paint e; // = new Paint();
    @DexIgnore
    public /* final */ Paint f; // = new Paint();
    @DexIgnore
    public /* final */ Rect g; // = new Rect();
    @DexIgnore
    public FossilCircleImageView h;
    @DexIgnore
    public boolean i;

    @DexIgnore
    public FossilNotificationImageView(Context context) {
        super(context);
        a(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            this.h = new FossilCircleImageView(context, attributeSet);
        } else {
            this.h = new FossilCircleImageView(context);
        }
        addView(this.h);
    }

    @DexIgnore
    public void b() {
        this.h.setBorderColor(PortfolioApp.R.getResources().getColor(R.color.transparent));
        this.h.setBorderWidth(3);
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        int i2;
        super.dispatchDraw(canvas);
        if (!TextUtils.isEmpty(this.h.getHandNumber())) {
            if (this.i) {
                i2 = getWidth() / 6;
            } else {
                i2 = getWidth() / 4;
            }
            this.f.setAntiAlias(true);
            this.f.setFilterBitmap(true);
            this.f.setDither(true);
            this.f.setColor(-65536);
            this.f.setStyle(Paint.Style.FILL);
            if (this.i) {
                canvas.drawCircle((float) (getWidth() - (getWidth() / 6)), (float) (getHeight() / 6), (float) (getWidth() / 6), this.f);
            } else {
                canvas.drawCircle((float) (getWidth() / 2), ((float) (getHeight() - i2)) - ts3.a(5.0f), (float) i2, this.f);
            }
            this.e.setAntiAlias(true);
            this.e.setColor(-1);
            this.e.setStrokeWidth(4.0f);
            this.e.setStyle(Paint.Style.STROKE);
            if (this.i) {
                canvas.drawCircle((float) (getWidth() - (getWidth() / 6)), (float) (getHeight() / 6), (float) (getWidth() / 6), this.e);
            } else {
                canvas.drawCircle((float) (getWidth() / 2), ((float) (getHeight() - i2)) - ts3.a(5.0f), (float) i2, this.e);
            }
            this.f.setColor(-1);
            this.f.setTextSize((float) i2);
            this.f.getTextBounds(this.h.getHandNumber(), 0, this.h.getHandNumber().length(), this.g);
            float measureText = this.f.measureText(this.h.getHandNumber());
            int height = this.g.height();
            if (this.i) {
                canvas.drawText(this.h.getHandNumber(), ((float) (getWidth() - (getWidth() / 6))) - (measureText / 2.0f), (float) ((getHeight() / 6) + (height / 2)), this.f);
            } else {
                canvas.drawText(this.h.getHandNumber(), ((float) (getWidth() / 2)) - (measureText / 2.0f), (((float) (getHeight() - i2)) - ts3.a(5.0f)) + ((float) (height / 2)), this.f);
            }
        }
    }

    @DexIgnore
    public FossilCircleImageView getFossilCircleImageView() {
        return this.h;
    }

    @DexIgnore
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6 = i4 - i2;
        this.h.layout(0, 0, i6, i6);
    }

    @DexIgnore
    public void setDefault(boolean z) {
        this.h.setDefault(z);
    }

    @DexIgnore
    public void setHandNumber(String str) {
        this.h.setHandNumber(str);
    }

    @DexIgnore
    public void setImageDrawable(Drawable drawable) {
        this.h.setImageDrawable(drawable);
    }

    @DexIgnore
    public void setTopRightBadge(boolean z) {
        this.i = z;
    }

    @DexIgnore
    public void a(String str, xn xnVar) {
        this.h.a(str, xnVar);
    }

    @DexIgnore
    public void a(Bitmap bitmap, xn xnVar) {
        if (bitmap == null) {
            FLogger.INSTANCE.getLocal().d(j, "Inside. setImageBitmap 1 bitmap, bitmap == null");
        } else {
            this.h.a(bitmap, xnVar);
        }
    }

    @DexIgnore
    public FossilNotificationImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2) {
        if (bitmap == null || bitmap2 == null) {
            FLogger.INSTANCE.getLocal().d(j, "Inside. setImageBitmap 2 bitmap, bitmap == null");
        } else {
            this.h.a(bitmap, bitmap2);
        }
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3) {
        if (bitmap == null || bitmap2 == null || bitmap3 == null) {
            FLogger.INSTANCE.getLocal().d(j, "Inside. setImageBitmap 3 bitmap, bitmap == null");
        } else {
            this.h.a(bitmap, bitmap2, bitmap3);
        }
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, Bitmap bitmap4) {
        if (bitmap == null || bitmap2 == null || bitmap3 == null || bitmap4 == null) {
            FLogger.INSTANCE.getLocal().d(j, "Inside. setImageBitmap 4 bitmap, bitmap == null");
        } else {
            this.h.a(bitmap, bitmap2, bitmap3, bitmap4);
        }
    }

    @DexIgnore
    public void a(int i2, xn xnVar) {
        this.h.a(i2, xnVar);
    }

    @DexIgnore
    public void a() {
        this.h.setBorderColor(PortfolioApp.R.getResources().getColor(R.color.fossilCoolGray));
        this.h.setBorderWidth(3);
    }
}
