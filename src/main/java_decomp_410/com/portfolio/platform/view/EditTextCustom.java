package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class EditTextCustom extends FlexibleEditText {
    @DexIgnore
    public Drawable k;
    @DexIgnore
    public Drawable l;
    @DexIgnore
    public Drawable m;
    @DexIgnore
    public Drawable n;
    @DexIgnore
    public a o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public DrawableClickListener r;

    @DexIgnore
    public interface DrawableClickListener {

        @DexIgnore
        public enum DrawablePosition {
            TOP,
            BOTTOM,
            LEFT,
            RIGHT
        }

        @DexIgnore
        void a(DrawablePosition drawablePosition);
    }

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public EditTextCustom(Context context) {
        super(context);
    }

    @DexIgnore
    public void finalize() throws Throwable {
        this.k = null;
        this.n = null;
        this.l = null;
        this.m = null;
        super.finalize();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @DexIgnore
    public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 4 && keyEvent.getAction() == 1) {
            a aVar = this.o;
            if (aVar != null) {
                aVar.a();
            }
            clearFocus();
        }
        return true;
    }

    @DexIgnore
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.p = (int) motionEvent.getX();
            this.q = (int) motionEvent.getY();
            Drawable drawable = this.n;
            if (drawable == null || !drawable.getBounds().contains(this.p, this.q)) {
                Drawable drawable2 = this.m;
                if (drawable2 == null || !drawable2.getBounds().contains(this.p, this.q)) {
                    Drawable drawable3 = this.l;
                    if (drawable3 != null) {
                        Rect bounds = drawable3.getBounds();
                        int i = (int) (((double) (getResources().getDisplayMetrics().density * 13.0f)) + 0.5d);
                        int i2 = this.p;
                        int i3 = this.q;
                        if (!bounds.contains(i2, i3)) {
                            i2 = this.p;
                            int i4 = i2 - i;
                            int i5 = this.q - i;
                            if (i4 > 0) {
                                i2 = i4;
                            }
                            i3 = i5 <= 0 ? this.q : i5;
                        }
                        if (bounds.contains(i2, i3)) {
                            DrawableClickListener drawableClickListener = this.r;
                            if (drawableClickListener != null) {
                                drawableClickListener.a(DrawableClickListener.DrawablePosition.LEFT);
                                motionEvent.setAction(3);
                                return false;
                            }
                        }
                    }
                    Drawable drawable4 = this.k;
                    if (drawable4 != null) {
                        Rect bounds2 = drawable4.getBounds();
                        int i6 = this.q - 13;
                        int width = getWidth() - (this.p + 13);
                        if (width <= 0) {
                            width += 13;
                        }
                        if (i6 <= 0) {
                            i6 = this.q;
                        }
                        if (bounds2.contains(width, i6)) {
                            DrawableClickListener drawableClickListener2 = this.r;
                            if (drawableClickListener2 != null) {
                                drawableClickListener2.a(DrawableClickListener.DrawablePosition.RIGHT);
                                motionEvent.setAction(3);
                                return false;
                            }
                        }
                        return super.onTouchEvent(motionEvent);
                    }
                } else {
                    this.r.a(DrawableClickListener.DrawablePosition.TOP);
                    return super.onTouchEvent(motionEvent);
                }
            } else {
                this.r.a(DrawableClickListener.DrawablePosition.BOTTOM);
                return super.onTouchEvent(motionEvent);
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    public void setBackPressListener(a aVar) {
        this.o = aVar;
    }

    @DexIgnore
    public void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            this.l = drawable;
        }
        if (drawable3 != null) {
            this.k = drawable3;
        }
        if (drawable2 != null) {
            this.m = drawable2;
        }
        if (drawable4 != null) {
            this.n = drawable4;
        }
        super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
    }

    @DexIgnore
    public void setDrawableClickListener(DrawableClickListener drawableClickListener) {
        this.r = drawableClickListener;
    }

    @DexIgnore
    public void setTypeface(Typeface typeface) {
        super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Fossil_Scout-Regular_10.otf"));
    }

    @DexIgnore
    public EditTextCustom(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @DexIgnore
    public EditTextCustom(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
