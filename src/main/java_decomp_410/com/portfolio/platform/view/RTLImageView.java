package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class RTLImageView extends AppCompatImageView {
    @DexIgnore
    public RTLImageView(Context context) {
        super(context);
    }

    @DexIgnore
    public void setImageDrawable(Drawable drawable) {
        if (drawable != null) {
            drawable.setAutoMirrored(true);
            super.setImageDrawable(drawable);
        }
    }

    @DexIgnore
    public RTLImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @DexIgnore
    public RTLImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
