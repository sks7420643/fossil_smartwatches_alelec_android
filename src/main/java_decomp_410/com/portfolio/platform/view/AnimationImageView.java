package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.blesdk.obfuscated.al2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.pd4;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AnimationImageView extends AppCompatImageView {
    @DexIgnore
    public /* final */ ArrayList<String> g; // = new ArrayList<>();
    @DexIgnore
    public int h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public Bitmap n;
    @DexIgnore
    public Bitmap o;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public Runnable r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ AnimationImageView e;

        @DexIgnore
        public a(AnimationImageView animationImageView) {
            this.e = animationImageView;
        }

        @DexIgnore
        public final void run() {
            AnimationImageView animationImageView = this.e;
            animationImageView.setImageBitmap(animationImageView.o);
            AnimationImageView animationImageView2 = this.e;
            animationImageView2.a(animationImageView2.n);
            AnimationImageView animationImageView3 = this.e;
            animationImageView3.n = animationImageView3.o;
            if (this.e.m == this.e.j) {
                AnimationImageView animationImageView4 = this.e;
                Object obj = animationImageView4.g.get(0);
                kd4.a(obj, "mListImagesPathFromAssets[0]");
                animationImageView4.o = animationImageView4.a((String) obj);
                this.e.a(0);
                return;
            }
            AnimationImageView animationImageView5 = this.e;
            Object obj2 = animationImageView5.g.get(this.e.m + 1);
            kd4.a(obj2, "mListImagesPathFromAssets[mFrameNo + 1]");
            animationImageView5.o = animationImageView5.a((String) obj2);
            AnimationImageView animationImageView6 = this.e;
            animationImageView6.a(animationImageView6.m + 1);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnimationImageView(Context context) {
        super(context);
        kd4.b(context, "context");
        Context context2 = getContext();
        kd4.a((Object) context2, "context");
        Resources resources = context2.getResources();
        kd4.a((Object) resources, "context.resources");
        this.p = resources.getDisplayMetrics().heightPixels;
        Context context3 = getContext();
        kd4.a((Object) context3, "context");
        Resources resources2 = context3.getResources();
        kd4.a((Object) resources2, "context.resources");
        this.q = resources2.getDisplayMetrics().widthPixels;
        this.r = new a(this);
    }

    @DexIgnore
    public final void d() {
        removeCallbacks(this.r);
        this.i = false;
    }

    @DexIgnore
    public final void a(String str, int i2, int i3, int i4, int i5, int i6) {
        kd4.b(str, "imagesPathFromAssets");
        this.g.clear();
        if (i2 <= i3) {
            while (true) {
                ArrayList<String> arrayList = this.g;
                pd4 pd4 = pd4.a;
                Object[] objArr = {Integer.valueOf(i2)};
                String format = String.format(str, Arrays.copyOf(objArr, objArr.length));
                kd4.a((Object) format, "java.lang.String.format(format, *args)");
                arrayList.add(format);
                if (i2 == i3) {
                    break;
                }
                i2++;
            }
        }
        if (this.g.size() > 1) {
            this.h = i4;
            this.k = i5;
            this.l = i6;
            this.j = this.g.size() - 1;
            String str2 = this.g.get(0);
            kd4.a((Object) str2, "mListImagesPathFromAssets[0]");
            this.n = a(str2);
            String str3 = this.g.get(1);
            kd4.a((Object) str3, "mListImagesPathFromAssets[1]");
            this.o = a(str3);
            setImageBitmap(this.n);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnimationImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        kd4.b(context, "context");
        kd4.b(attributeSet, "attrs");
        Context context2 = getContext();
        kd4.a((Object) context2, "context");
        Resources resources = context2.getResources();
        kd4.a((Object) resources, "context.resources");
        this.p = resources.getDisplayMetrics().heightPixels;
        Context context3 = getContext();
        kd4.a((Object) context3, "context");
        Resources resources2 = context3.getResources();
        kd4.a((Object) resources2, "context.resources");
        this.q = resources2.getDisplayMetrics().widthPixels;
        this.r = new a(this);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnimationImageView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        kd4.b(context, "context");
        kd4.b(attributeSet, "attrs");
        Context context2 = getContext();
        kd4.a((Object) context2, "context");
        Resources resources = context2.getResources();
        kd4.a((Object) resources, "context.resources");
        this.p = resources.getDisplayMetrics().heightPixels;
        Context context3 = getContext();
        kd4.a((Object) context3, "context");
        Resources resources2 = context3.getResources();
        kd4.a((Object) resources2, "context.resources");
        this.q = resources2.getDisplayMetrics().widthPixels;
        this.r = new a(this);
    }

    @DexIgnore
    public final void a() {
        if (this.i) {
            this.m = 0;
            return;
        }
        this.i = true;
        removeCallbacks(this.r);
        a(1);
    }

    @DexIgnore
    public final void a(Bitmap bitmap) {
        if (bitmap != null) {
            try {
                bitmap.recycle();
            } catch (Exception unused) {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0015, code lost:
        if (r1 > 0) goto L_0x0019;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000d, code lost:
        if (r1 > 0) goto L_0x0019;
     */
    @DexIgnore
    public final void a(int i2) {
        int i3;
        if (this.i) {
            int i4 = this.h;
            if (i2 == this.j) {
                i3 = this.l;
            }
            if (i2 == 1) {
                i3 = this.k;
            }
            i3 = i4;
            this.m = i2;
            try {
                postDelayed(this.r, (long) i3);
            } catch (Exception unused) {
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: android.graphics.Bitmap} */
    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v1, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r0v4 */
    /* JADX WARNING: type inference failed for: r0v5 */
    /* JADX WARNING: type inference failed for: r0v7 */
    /* JADX WARNING: type inference failed for: r0v8 */
    /* JADX WARNING: type inference failed for: r0v9 */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        if (r5 != null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001c, code lost:
        if (r5 != null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001e, code lost:
        r5.close();
        r0 = r0;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0035  */
    public final Bitmap a(String str) {
        Throwable th;
        InputStream inputStream;
        Object r0 = 0;
        try {
            Context context = getContext();
            kd4.a((Object) context, "context");
            inputStream = context.getAssets().open(str);
            try {
                Bitmap a2 = al2.a(inputStream, this.q, this.p / 2);
                r0 = a2;
                r0 = a2;
            } catch (IOException e) {
                e = e;
                try {
                    e.printStackTrace();
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    r0 = inputStream;
                    th = th3;
                    if (r0 != 0) {
                    }
                    throw th;
                }
            }
        } catch (IOException e2) {
            e = e2;
            inputStream = null;
            e.printStackTrace();
        } catch (Throwable th4) {
            th = th4;
            if (r0 != 0) {
                r0.close();
            }
            throw th;
        }
    }
}
