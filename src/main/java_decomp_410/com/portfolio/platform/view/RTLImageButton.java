package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class RTLImageButton extends AppCompatImageButton {
    @DexIgnore
    public RTLImageButton(Context context) {
        super(context);
    }

    @DexIgnore
    public void setImageDrawable(Drawable drawable) {
        if (drawable != null) {
            drawable.setAutoMirrored(true);
            super.setImageDrawable(drawable);
        }
    }

    @DexIgnore
    public RTLImageButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @DexIgnore
    public RTLImageButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
