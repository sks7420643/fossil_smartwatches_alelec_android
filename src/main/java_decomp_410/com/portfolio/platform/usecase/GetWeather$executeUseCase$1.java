package com.portfolio.platform.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.usecase.GetWeather$executeUseCase$1", mo27670f = "GetWeather.kt", mo27671l = {31}, mo27672m = "invokeSuspend")
public final class GetWeather$executeUseCase$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.usecase.GetWeather.C6907c $requestValues;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f24378p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.usecase.GetWeather this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetWeather$executeUseCase$1(com.portfolio.platform.usecase.GetWeather getWeather, com.portfolio.platform.usecase.GetWeather.C6907c cVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = getWeather;
        this.$requestValues = cVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.usecase.GetWeather$executeUseCase$1 getWeather$executeUseCase$1 = new com.portfolio.platform.usecase.GetWeather$executeUseCase$1(this.this$0, this.$requestValues, yb4);
        getWeather$executeUseCase$1.f24378p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return getWeather$executeUseCase$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.usecase.GetWeather$executeUseCase$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f24378p$;
            com.portfolio.platform.usecase.GetWeather$executeUseCase$1$response$1 getWeather$executeUseCase$1$response$1 = new com.portfolio.platform.usecase.GetWeather$executeUseCase$1$response$1(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.label = 1;
            obj = com.portfolio.platform.response.ResponseKt.m32232a(getWeather$executeUseCase$1$response$1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) obj;
        if (qo2 instanceof com.fossil.blesdk.obfuscated.ro2) {
            com.fossil.blesdk.obfuscated.vo2 vo2 = new com.fossil.blesdk.obfuscated.vo2();
            vo2.mo31874a((com.fossil.blesdk.obfuscated.xz1) ((com.fossil.blesdk.obfuscated.ro2) qo2).mo30673a());
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String a2 = com.portfolio.platform.usecase.GetWeather.f24372f.mo41881a();
            local.mo33255d(a2, "onSuccess" + vo2.mo31873a());
            com.fossil.blesdk.obfuscated.i62.C4439d b = this.this$0.mo28246a();
            com.portfolio.platform.data.model.microapp.weather.Weather a3 = vo2.mo31873a();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a3, "mfWeatherResponse.weather");
            b.onSuccess(new com.portfolio.platform.usecase.GetWeather.C6908d(a3));
        } else if (qo2 instanceof com.fossil.blesdk.obfuscated.po2) {
            this.this$0.mo28246a().mo28250a(new com.portfolio.platform.usecase.GetWeather.C6906b(com.portfolio.platform.enums.SyncErrorCode.NETWORK_ERROR.name()));
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
