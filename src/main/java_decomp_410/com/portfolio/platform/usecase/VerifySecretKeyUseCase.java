package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kr3;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.w52;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class VerifySecretKeyUseCase extends CoroutineUseCase<VerifySecretKeyUseCase.b, VerifySecretKeyUseCase.d, VerifySecretKeyUseCase.c> {
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e;
    @DexIgnore
    public /* final */ DeviceRepository f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ kr3 h;
    @DexIgnore
    public /* final */ PortfolioApp i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            kd4.b(str, "deviceId");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i, String str) {
            kd4.b(str, "errorMesagge");
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public VerifySecretKeyUseCase(DeviceRepository deviceRepository, UserRepository userRepository, kr3 kr3, PortfolioApp portfolioApp) {
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(kr3, "mDecryptValueKeyStoreUseCase");
        kd4.b(portfolioApp, "mApp");
        this.f = deviceRepository;
        this.g = userRepository;
        this.h = kr3;
        this.i = portfolioApp;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final /* synthetic */ Object b(yb4<? super String> yb4) {
        VerifySecretKeyUseCase$getServerSecretKey$Anon1 verifySecretKeyUseCase$getServerSecretKey$Anon1;
        int i2;
        qo2 qo2;
        if (yb4 instanceof VerifySecretKeyUseCase$getServerSecretKey$Anon1) {
            verifySecretKeyUseCase$getServerSecretKey$Anon1 = (VerifySecretKeyUseCase$getServerSecretKey$Anon1) yb4;
            int i3 = verifySecretKeyUseCase$getServerSecretKey$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                verifySecretKeyUseCase$getServerSecretKey$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = verifySecretKeyUseCase$getServerSecretKey$Anon1.result;
                Object a2 = cc4.a();
                i2 = verifySecretKeyUseCase$getServerSecretKey$Anon1.label;
                if (i2 != 0) {
                    na4.a(obj);
                    DeviceRepository deviceRepository = this.f;
                    String str = this.d;
                    verifySecretKeyUseCase$getServerSecretKey$Anon1.L$Anon0 = this;
                    verifySecretKeyUseCase$getServerSecretKey$Anon1.label = 1;
                    obj = deviceRepository.getDeviceSecretKey(str, verifySecretKeyUseCase$getServerSecretKey$Anon1);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i2 == 1) {
                    VerifySecretKeyUseCase verifySecretKeyUseCase = (VerifySecretKeyUseCase) verifySecretKeyUseCase$getServerSecretKey$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("VerifySecretKeyUseCase", "getServerSecretKey " + qo2);
                if (!(qo2 instanceof ro2)) {
                    Object a3 = ((ro2) qo2).a();
                    if (a3 != null) {
                        return (String) a3;
                    }
                    kd4.a();
                    throw null;
                } else if (qo2 instanceof po2) {
                    return null;
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        verifySecretKeyUseCase$getServerSecretKey$Anon1 = new VerifySecretKeyUseCase$getServerSecretKey$Anon1(this, yb4);
        Object obj2 = verifySecretKeyUseCase$getServerSecretKey$Anon1.result;
        Object a22 = cc4.a();
        i2 = verifySecretKeyUseCase$getServerSecretKey$Anon1.label;
        if (i2 != 0) {
        }
        qo2 = (qo2) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("VerifySecretKeyUseCase", "getServerSecretKey " + qo2);
        if (!(qo2 instanceof ro2)) {
        }

        throw null;
    }

    @DexIgnore
    public String c() {
        return "VerifySecretKeyUseCase";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0125, code lost:
        if (r0.a(new com.portfolio.platform.usecase.VerifySecretKeyUseCase.d(r10)) == null) goto L_0x0129;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00c1 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public Object a(b bVar, yb4<Object> yb4) {
        throw null;
        // VerifySecretKeyUseCase$run$Anon1 verifySecretKeyUseCase$run$Anon1;
        // Object a2;
        // int i2;
        // VerifySecretKeyUseCase verifySecretKeyUseCase;
        // String str;
        // Object obj;
        // String str2;
        // VerifySecretKeyUseCase verifySecretKeyUseCase2;
        // MFUser mFUser;
        // MFUser mFUser2;
        // if (yb4 instanceof VerifySecretKeyUseCase$run$Anon1) {
        //     verifySecretKeyUseCase$run$Anon1 = (VerifySecretKeyUseCase$run$Anon1) yb4;
        //     int i3 = verifySecretKeyUseCase$run$Anon1.label;
        //     if ((i3 & Integer.MIN_VALUE) != 0) {
        //         verifySecretKeyUseCase$run$Anon1.label = i3 - Integer.MIN_VALUE;
        //         Object obj2 = verifySecretKeyUseCase$run$Anon1.result;
        //         a2 = cc4.a();
        //         i2 = verifySecretKeyUseCase$run$Anon1.label;
        //         if (i2 != 0) {
        //             na4.a(obj2);
        //             if (bVar != null) {
        //                 this.d = bVar.a();
        //                 MFUser currentUser = this.g.getCurrentUser();
        //                 if (currentUser != null) {
        //                     this.e = currentUser.getUserId() + ':' + this.i.e();
        //                     verifySecretKeyUseCase$run$Anon1.L$Anon0 = this;
        //                     verifySecretKeyUseCase$run$Anon1.L$Anon1 = bVar;
        //                     verifySecretKeyUseCase$run$Anon1.L$Anon2 = currentUser;
        //                     verifySecretKeyUseCase$run$Anon1.L$Anon3 = currentUser;
        //                     verifySecretKeyUseCase$run$Anon1.label = 1;
        //                     Object b2 = b(verifySecretKeyUseCase$run$Anon1);
        //                     if (b2 == a2) {
        //                         return a2;
        //                     }
        //                     verifySecretKeyUseCase2 = this;
        //                     mFUser = currentUser;
        //                     obj2 = b2;
        //                     mFUser2 = mFUser;
        //                 } else {
        //                     verifySecretKeyUseCase = this;
        //                     verifySecretKeyUseCase.a(new c(600, ""));
        //                     return new Object();
        //                 }
        //             } else {
        //                 kd4.a();
        //                 throw null;
        //             }
        //         } else if (i2 == 1) {
        //             mFUser2 = (MFUser) verifySecretKeyUseCase$run$Anon1.L$Anon2;
        //             verifySecretKeyUseCase2 = (VerifySecretKeyUseCase) verifySecretKeyUseCase$run$Anon1.L$Anon0;
        //             na4.a(obj2);
        //             b bVar2 = (b) verifySecretKeyUseCase$run$Anon1.L$Anon1;
        //             mFUser = (MFUser) verifySecretKeyUseCase$run$Anon1.L$Anon3;
        //             bVar = bVar2;
        //         } else if (i2 == 2) {
        //             MFUser mFUser3 = (MFUser) verifySecretKeyUseCase$run$Anon1.L$Anon3;
        //             MFUser mFUser4 = (MFUser) verifySecretKeyUseCase$run$Anon1.L$Anon2;
        //             b bVar3 = (b) verifySecretKeyUseCase$run$Anon1.L$Anon1;
        //             verifySecretKeyUseCase = (VerifySecretKeyUseCase) verifySecretKeyUseCase$run$Anon1.L$Anon0;
        //             na4.a(obj2);
        //             Object obj3 = obj2;
        //             str = (String) verifySecretKeyUseCase$run$Anon1.L$Anon4;
        //             obj = obj3;
        //             str2 = (String) obj;
        //             ILocalFLogger local = FLogger.INSTANCE.getLocal();
        //             local.d("VerifySecretKeyUseCase", "localSecretKey " + str2 + " serverSecretKey " + str);
        //             if (str != null && ((str.hashCode() != 0 || !str.equals("")) && (!kd4.a((Object) str2, (Object) str)))) {
        //                 str2 = str;
        //             }
        //             ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        //             local2.d("VerifySecretKeyUseCase", "secret key " + str2);
        //         } else {
        //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        //         }
        //         str = (String) obj2;
        //         verifySecretKeyUseCase$run$Anon1.L$Anon0 = verifySecretKeyUseCase2;
        //         verifySecretKeyUseCase$run$Anon1.L$Anon1 = bVar;
        //         verifySecretKeyUseCase$run$Anon1.L$Anon2 = mFUser2;
        //         verifySecretKeyUseCase$run$Anon1.L$Anon3 = mFUser;
        //         verifySecretKeyUseCase$run$Anon1.L$Anon4 = str;
        //         verifySecretKeyUseCase$run$Anon1.label = 2;
        //         obj = verifySecretKeyUseCase2.a(verifySecretKeyUseCase$run$Anon1);
        //         if (obj != a2) {
        //             return a2;
        //         }
        //         verifySecretKeyUseCase = verifySecretKeyUseCase2;
        //         str2 = (String) obj;
        //         ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        //         local3.d("VerifySecretKeyUseCase", "localSecretKey " + str2 + " serverSecretKey " + str);
        //         str2 = str;
        //         ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        //         local22.d("VerifySecretKeyUseCase", "secret key " + str2);
        //     }
        // }
        // verifySecretKeyUseCase$run$Anon1 = new VerifySecretKeyUseCase$run$Anon1(this, yb4);
        // Object obj22 = verifySecretKeyUseCase$run$Anon1.result;
        // a2 = cc4.a();
        // i2 = verifySecretKeyUseCase$run$Anon1.label;
        // if (i2 != 0) {
        // }
        // str = (String) obj22;
        // verifySecretKeyUseCase$run$Anon1.L$Anon0 = verifySecretKeyUseCase2;
        // verifySecretKeyUseCase$run$Anon1.L$Anon1 = bVar;
        // verifySecretKeyUseCase$run$Anon1.L$Anon2 = mFUser2;
        // verifySecretKeyUseCase$run$Anon1.L$Anon3 = mFUser;
        // verifySecretKeyUseCase$run$Anon1.L$Anon4 = str;
        // verifySecretKeyUseCase$run$Anon1.label = 2;
        // obj = verifySecretKeyUseCase2.a(verifySecretKeyUseCase$run$Anon1);
        // if (obj != a2) {
        // }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final /* synthetic */ Object a(yb4<? super String> yb4) {
        VerifySecretKeyUseCase$getLocalSecretKey$Anon1 verifySecretKeyUseCase$getLocalSecretKey$Anon1;
        int i2;
        CoroutineUseCase.c cVar;
        if (yb4 instanceof VerifySecretKeyUseCase$getLocalSecretKey$Anon1) {
            verifySecretKeyUseCase$getLocalSecretKey$Anon1 = (VerifySecretKeyUseCase$getLocalSecretKey$Anon1) yb4;
            int i3 = verifySecretKeyUseCase$getLocalSecretKey$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                verifySecretKeyUseCase$getLocalSecretKey$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = verifySecretKeyUseCase$getLocalSecretKey$Anon1.result;
                Object a2 = cc4.a();
                i2 = verifySecretKeyUseCase$getLocalSecretKey$Anon1.label;
                if (i2 != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d("VerifySecretKeyUseCase", "getLocalSecretKey");
                    kr3 kr3 = this.h;
                    String str = this.e;
                    if (str != null) {
                        kr3.b bVar = new kr3.b(str);
                        verifySecretKeyUseCase$getLocalSecretKey$Anon1.L$Anon0 = this;
                        verifySecretKeyUseCase$getLocalSecretKey$Anon1.label = 1;
                        obj = w52.a(kr3, bVar, verifySecretKeyUseCase$getLocalSecretKey$Anon1);
                        if (obj == a2) {
                            return a2;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else if (i2 == 1) {
                    VerifySecretKeyUseCase verifySecretKeyUseCase = (VerifySecretKeyUseCase) verifySecretKeyUseCase$getLocalSecretKey$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                cVar = (CoroutineUseCase.c) obj;
                if (!(cVar instanceof kr3.d)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("Get local key success ");
                    kr3.d dVar = (kr3.d) cVar;
                    sb.append(dVar.a());
                    local.d("VerifySecretKeyUseCase", sb.toString());
                    return dVar.a();
                }
                boolean z = cVar instanceof kr3.c;
                return null;
            }
        }
        verifySecretKeyUseCase$getLocalSecretKey$Anon1 = new VerifySecretKeyUseCase$getLocalSecretKey$Anon1(this, yb4);
        Object obj2 = verifySecretKeyUseCase$getLocalSecretKey$Anon1.result;
        Object a22 = cc4.a();
        i2 = verifySecretKeyUseCase$getLocalSecretKey$Anon1.label;
        if (i2 != 0) {
        }
        cVar = (CoroutineUseCase.c) obj2;
        if (!(cVar instanceof kr3.d)) {
        }
        throw null;
    }
}
