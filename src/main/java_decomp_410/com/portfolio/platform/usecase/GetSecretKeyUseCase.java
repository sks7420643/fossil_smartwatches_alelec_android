package com.portfolio.platform.usecase;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.mr3;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.w52;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetSecretKeyUseCase extends CoroutineUseCase<GetSecretKeyUseCase.b, GetSecretKeyUseCase.d, GetSecretKeyUseCase.c> {
    @DexIgnore
    public /* final */ mr3 d;
    @DexIgnore
    public /* final */ DeviceRepository e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(String str, String str2) {
            kd4.b(str, "deviceId");
            kd4.b(str2, ButtonService.USER_ID);
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public c(int i, String str) {
            kd4.b(str, "errorMesagge");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public GetSecretKeyUseCase(mr3 mr3, DeviceRepository deviceRepository) {
        kd4.b(mr3, "mEncryptValueKeyStoreUseCase");
        kd4.b(deviceRepository, "mDeviceRepository");
        this.d = mr3;
        this.e = deviceRepository;
    }

    @DexIgnore
    public String c() {
        return "GetSecretKeyUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public Object a(b bVar, yb4<Object> yb4) {
        GetSecretKeyUseCase$run$Anon1 getSecretKeyUseCase$run$Anon1;
        int i;
        GetSecretKeyUseCase getSecretKeyUseCase;
        String str;
        qo2 qo2;
        if (yb4 instanceof GetSecretKeyUseCase$run$Anon1) {
            getSecretKeyUseCase$run$Anon1 = (GetSecretKeyUseCase$run$Anon1) yb4;
            int i2 = getSecretKeyUseCase$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                getSecretKeyUseCase$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = getSecretKeyUseCase$run$Anon1.result;
                Object a2 = cc4.a();
                i = getSecretKeyUseCase$run$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    DeviceRepository deviceRepository = this.e;
                    if (bVar != null) {
                        String a3 = bVar.a();
                        getSecretKeyUseCase$run$Anon1.L$Anon0 = this;
                        getSecretKeyUseCase$run$Anon1.L$Anon1 = bVar;
                        getSecretKeyUseCase$run$Anon1.label = 1;
                        obj = deviceRepository.getDeviceSecretKey(a3, getSecretKeyUseCase$run$Anon1);
                        if (obj == a2) {
                            return a2;
                        }
                        getSecretKeyUseCase = this;
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else if (i == 1) {
                    bVar = (b) getSecretKeyUseCase$run$Anon1.L$Anon1;
                    getSecretKeyUseCase = (GetSecretKeyUseCase) getSecretKeyUseCase$run$Anon1.L$Anon0;
                    na4.a(obj);
                } else if (i == 2) {
                    String str2 = (String) getSecretKeyUseCase$run$Anon1.L$Anon4;
                    str = (String) getSecretKeyUseCase$run$Anon1.L$Anon3;
                    qo2 qo22 = (qo2) getSecretKeyUseCase$run$Anon1.L$Anon2;
                    b bVar2 = (b) getSecretKeyUseCase$run$Anon1.L$Anon1;
                    na4.a(obj);
                    getSecretKeyUseCase = (GetSecretKeyUseCase) getSecretKeyUseCase$run$Anon1.L$Anon0;
                    PortfolioApp.W.c().e(str, PortfolioApp.W.c().e());
                    getSecretKeyUseCase.a(new d());
                    return new Object();
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("success to get server secret key ");
                    ro2 ro2 = (ro2) qo2;
                    sb.append((String) ro2.a());
                    local.d("GetSecretKeyUseCase", sb.toString());
                    if (!TextUtils.isEmpty((CharSequence) ro2.a())) {
                        Object a4 = ro2.a();
                        if (a4 != null) {
                            String str3 = (String) a4;
                            String str4 = bVar.b() + ':' + bVar.a();
                            mr3 mr3 = getSecretKeyUseCase.d;
                            mr3.b bVar3 = new mr3.b(str4, str3);
                            getSecretKeyUseCase$run$Anon1.L$Anon0 = getSecretKeyUseCase;
                            getSecretKeyUseCase$run$Anon1.L$Anon1 = bVar;
                            getSecretKeyUseCase$run$Anon1.L$Anon2 = qo2;
                            getSecretKeyUseCase$run$Anon1.L$Anon3 = str3;
                            getSecretKeyUseCase$run$Anon1.L$Anon4 = str4;
                            getSecretKeyUseCase$run$Anon1.label = 2;
                            if (w52.a(mr3, bVar3, getSecretKeyUseCase$run$Anon1) == a2) {
                                return a2;
                            }
                            str = str3;
                            PortfolioApp.W.c().e(str, PortfolioApp.W.c().e());
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    getSecretKeyUseCase.a(new d());
                    return new Object();
                }
                if (qo2 instanceof po2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("fail to get server secret key ");
                    po2 po2 = (po2) qo2;
                    sb2.append(po2.a());
                    local2.d("GetSecretKeyUseCase", sb2.toString());
                    getSecretKeyUseCase.a(new c(po2.a(), ""));
                }
                return new Object();
            }
        }
        getSecretKeyUseCase$run$Anon1 = new GetSecretKeyUseCase$run$Anon1(this, yb4);
        Object obj2 = getSecretKeyUseCase$run$Anon1.result;
        Object a22 = cc4.a();
        i = getSecretKeyUseCase$run$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }
}
