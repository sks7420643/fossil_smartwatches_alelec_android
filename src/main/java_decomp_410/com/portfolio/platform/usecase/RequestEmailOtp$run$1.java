package com.portfolio.platform.usecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.usecase.RequestEmailOtp", mo27670f = "RequestEmailOtp.kt", mo27671l = {21}, mo27672m = "run")
public final class RequestEmailOtp$run$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.usecase.RequestEmailOtp this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RequestEmailOtp$run$1(com.portfolio.platform.usecase.RequestEmailOtp requestEmailOtp, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = requestEmailOtp;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.mo26310a((com.portfolio.platform.usecase.RequestEmailOtp.C6910b) null, (com.fossil.blesdk.obfuscated.yb4<java.lang.Object>) this);
    }
}
