package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.usecase.CheckAuthenticationEmailExisting;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.usecase.CheckAuthenticationEmailExisting", f = "CheckAuthenticationEmailExisting.kt", l = {21}, m = "run")
public final class CheckAuthenticationEmailExisting$run$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ CheckAuthenticationEmailExisting this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CheckAuthenticationEmailExisting$run$Anon1(CheckAuthenticationEmailExisting checkAuthenticationEmailExisting, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = checkAuthenticationEmailExisting;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((CheckAuthenticationEmailExisting.b) null, (yb4<Object>) this);
    }
}
