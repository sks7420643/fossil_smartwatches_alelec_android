package com.portfolio.platform.usecase;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RequestEmailOtp extends CoroutineUseCase<RequestEmailOtp.b, RequestEmailOtp.d, RequestEmailOtp.c> {
    @DexIgnore
    public /* final */ UserRepository d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            kd4.b(str, "email");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(int i, String str) {
            kd4.b(str, "errorMesagge");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public RequestEmailOtp(UserRepository userRepository) {
        kd4.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    public String c() {
        return "RequestEmailOtp";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public Object a(b bVar, yb4<Object> yb4) {
        RequestEmailOtp$run$Anon1 requestEmailOtp$run$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof RequestEmailOtp$run$Anon1) {
            requestEmailOtp$run$Anon1 = (RequestEmailOtp$run$Anon1) yb4;
            int i2 = requestEmailOtp$run$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                requestEmailOtp$run$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = requestEmailOtp$run$Anon1.result;
                Object a2 = cc4.a();
                i = requestEmailOtp$run$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    if (bVar == null) {
                        return new c(600, "");
                    }
                    UserRepository userRepository = this.d;
                    String a3 = bVar.a();
                    requestEmailOtp$run$Anon1.L$Anon0 = this;
                    requestEmailOtp$run$Anon1.L$Anon1 = bVar;
                    requestEmailOtp$run$Anon1.label = 1;
                    obj = userRepository.requestEmailOtp(a3, requestEmailOtp$run$Anon1);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    b bVar2 = (b) requestEmailOtp$run$Anon1.L$Anon1;
                    RequestEmailOtp requestEmailOtp = (RequestEmailOtp) requestEmailOtp$run$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    FLogger.INSTANCE.getLocal().d("RequestEmailOtp", "request OTP success");
                    return new d();
                } else if (!(qo2 instanceof po2)) {
                    return new c(600, "");
                } else {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("request OTP failed ");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.c());
                    local.d("RequestEmailOtp", sb.toString());
                    return new c(po2.a(), "");
                }
            }
        }
        requestEmailOtp$run$Anon1 = new RequestEmailOtp$run$Anon1(this, yb4);
        Object obj2 = requestEmailOtp$run$Anon1.result;
        Object a22 = cc4.a();
        i = requestEmailOtp$run$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }
}
