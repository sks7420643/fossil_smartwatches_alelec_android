package com.portfolio.platform.viewmodel;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.viewmodel.FirmwareDebugViewModel$loadFirmware$Anon1", f = "FirmwareDebugViewModel.kt", l = {16}, m = "invokeSuspend")
public final class FirmwareDebugViewModel$loadFirmware$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ xc4 $block;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ FirmwareDebugViewModel this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FirmwareDebugViewModel$loadFirmware$Anon1(FirmwareDebugViewModel firmwareDebugViewModel, xc4 xc4, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = firmwareDebugViewModel;
        this.$block = xc4;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        FirmwareDebugViewModel$loadFirmware$Anon1 firmwareDebugViewModel$loadFirmware$Anon1 = new FirmwareDebugViewModel$loadFirmware$Anon1(this.this$Anon0, this.$block, yb4);
        firmwareDebugViewModel$loadFirmware$Anon1.p$ = (zg4) obj;
        return firmwareDebugViewModel$loadFirmware$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((FirmwareDebugViewModel$loadFirmware$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            xc4 xc4 = this.$block;
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = xc4.invoke(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$Anon0.c().a((List) obj);
        return qa4.a;
    }
}
