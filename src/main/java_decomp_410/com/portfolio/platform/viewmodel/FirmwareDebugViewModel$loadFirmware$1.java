package com.portfolio.platform.viewmodel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.viewmodel.FirmwareDebugViewModel$loadFirmware$1", mo27670f = "FirmwareDebugViewModel.kt", mo27671l = {16}, mo27672m = "invokeSuspend")
public final class FirmwareDebugViewModel$loadFirmware$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.xc4 $block;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f25689p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.viewmodel.FirmwareDebugViewModel this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FirmwareDebugViewModel$loadFirmware$1(com.portfolio.platform.viewmodel.FirmwareDebugViewModel firmwareDebugViewModel, com.fossil.blesdk.obfuscated.xc4 xc4, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = firmwareDebugViewModel;
        this.$block = xc4;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.viewmodel.FirmwareDebugViewModel$loadFirmware$1 firmwareDebugViewModel$loadFirmware$1 = new com.portfolio.platform.viewmodel.FirmwareDebugViewModel$loadFirmware$1(this.this$0, this.$block, yb4);
        firmwareDebugViewModel$loadFirmware$1.f25689p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return firmwareDebugViewModel$loadFirmware$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.viewmodel.FirmwareDebugViewModel$loadFirmware$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f25689p$;
            com.fossil.blesdk.obfuscated.xc4 xc4 = this.$block;
            this.L$0 = zg4;
            this.label = 1;
            obj = xc4.invoke(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.mo42929c().mo2280a((java.util.List) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
