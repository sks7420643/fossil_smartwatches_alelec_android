package com.portfolio.platform.migration;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.migration.MigrationHelper$startMigrationForVersion$Anon1", f = "MigrationHelper.kt", l = {}, m = "invokeSuspend")
public final class MigrationHelper$startMigrationForVersion$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $version;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MigrationHelper this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MigrationHelper$startMigrationForVersion$Anon1(MigrationHelper migrationHelper, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = migrationHelper;
        this.$version = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MigrationHelper$startMigrationForVersion$Anon1 migrationHelper$startMigrationForVersion$Anon1 = new MigrationHelper$startMigrationForVersion$Anon1(this.this$Anon0, this.$version, yb4);
        migrationHelper$startMigrationForVersion$Anon1.p$ = (zg4) obj;
        return migrationHelper$startMigrationForVersion$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MigrationHelper$startMigrationForVersion$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            String m = this.this$Anon0.a.m();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MigrationHelper", "start migration for " + this.$version + " lastVersion " + m);
            this.this$Anon0.b.d();
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
