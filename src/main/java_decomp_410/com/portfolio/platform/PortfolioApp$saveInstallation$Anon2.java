package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.Installation;
import com.portfolio.platform.response.ResponseKt;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.PortfolioApp$saveInstallation$Anon2", f = "PortfolioApp.kt", l = {1041}, m = "invokeSuspend")
public final class PortfolioApp$saveInstallation$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Installation $installation;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PortfolioApp this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PortfolioApp$saveInstallation$Anon2(PortfolioApp portfolioApp, Installation installation, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = portfolioApp;
        this.$installation = installation;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        PortfolioApp$saveInstallation$Anon2 portfolioApp$saveInstallation$Anon2 = new PortfolioApp$saveInstallation$Anon2(this.this$Anon0, this.$installation, yb4);
        portfolioApp$saveInstallation$Anon2.p$ = (zg4) obj;
        return portfolioApp$saveInstallation$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((PortfolioApp$saveInstallation$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            PortfolioApp$saveInstallation$Anon2$response$Anon1 portfolioApp$saveInstallation$Anon2$response$Anon1 = new PortfolioApp$saveInstallation$Anon2$response$Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = ResponseKt.a(portfolioApp$saveInstallation$Anon2$response$Anon1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo2 = (qo2) obj;
        if (qo2 instanceof ro2) {
            ro2 ro2 = (ro2) qo2;
            if (ro2.a() != null) {
                this.this$Anon0.u().p(((Installation) ro2.a()).getInstallationId());
            }
        }
        return qa4.a;
    }
}
