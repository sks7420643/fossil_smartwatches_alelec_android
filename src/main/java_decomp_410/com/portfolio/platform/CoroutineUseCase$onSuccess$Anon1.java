package com.portfolio.platform;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.CoroutineUseCase$onSuccess$Anon1", f = "CoroutineUseCase.kt", l = {}, m = "invokeSuspend")
public final class CoroutineUseCase$onSuccess$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ CoroutineUseCase.d $response;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CoroutineUseCase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CoroutineUseCase$onSuccess$Anon1(CoroutineUseCase coroutineUseCase, CoroutineUseCase.d dVar, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = coroutineUseCase;
        this.$response = dVar;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CoroutineUseCase$onSuccess$Anon1 coroutineUseCase$onSuccess$Anon1 = new CoroutineUseCase$onSuccess$Anon1(this.this$Anon0, this.$response, yb4);
        coroutineUseCase$onSuccess$Anon1.p$ = (zg4) obj;
        return coroutineUseCase$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CoroutineUseCase$onSuccess$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            FLogger.INSTANCE.getLocal().d(this.this$Anon0.b, "Trigger UseCase callback success");
            CoroutineUseCase.e a = this.this$Anon0.a;
            if (a != null) {
                a.onSuccess(this.$response);
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
