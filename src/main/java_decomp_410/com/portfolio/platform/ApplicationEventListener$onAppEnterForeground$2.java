package com.portfolio.platform;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$2", mo27670f = "ApplicationEventListener.kt", mo27671l = {77, 78, 79, 81, 82, 83, 86, 87, 91, 92, 93, 94, 95, 98, 99, 100}, mo27672m = "invokeSuspend")
public final class ApplicationEventListener$onAppEnterForeground$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $mActiveSerial;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20898p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.ApplicationEventListener this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ApplicationEventListener$onAppEnterForeground$2(com.portfolio.platform.ApplicationEventListener applicationEventListener, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = applicationEventListener;
        this.$mActiveSerial = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$2 applicationEventListener$onAppEnterForeground$2 = new com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$2(this.this$0, this.$mActiveSerial, yb4);
        applicationEventListener$onAppEnterForeground$2.f20898p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return applicationEventListener$onAppEnterForeground$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0152, code lost:
        r14 = r13.this$0.f20891i;
        r13.L$0 = r6;
        r13.L$1 = r5;
        r13.L$2 = r1;
        r13.label = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0165, code lost:
        if (r14.downloadDeviceList(r13) != r0) goto L_0x0168;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0167, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0168, code lost:
        r14 = r13.this$0.f20892j;
        r13.L$0 = r6;
        r13.L$1 = r5;
        r13.L$2 = r1;
        r13.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x017b, code lost:
        if (r14.loadUserInfo(r13) != r0) goto L_0x017e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x017d, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x017e, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().mo33266i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER, com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e(), com.portfolio.platform.ApplicationEventListener.f20882p.mo34421a(), "[App Open] Start download firmware");
        r14 = com.portfolio.platform.util.DeviceUtils.f24406g.mo41910a();
        r13.L$0 = r6;
        r13.L$1 = r5;
        r13.L$2 = r1;
        r13.label = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x01b0, code lost:
        if (r14.mo41904a((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) r13) != r0) goto L_0x01b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x01b2, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x01b3, code lost:
        r14 = r13.this$0.f20891i;
        r13.L$0 = r6;
        r13.L$1 = r5;
        r13.L$2 = r1;
        r13.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x01c6, code lost:
        if (com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r14, 0, r13, 1, (java.lang.Object) null) != r0) goto L_0x01c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x01c8, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x01c9, code lost:
        r2 = r5;
        r5 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x01cb, code lost:
        r14 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34566p();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x01d5, code lost:
        if (r14 == null) goto L_0x0302;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x01d7, code lost:
        r14 = r14.mo39603a((com.portfolio.platform.localization.LocalizationManager.C5962d) null);
        r13.L$0 = r5;
        r13.L$1 = r2;
        r13.L$2 = r1;
        r13.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x01e8, code lost:
        if (r14.mo39604a((com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>) r13) != r0) goto L_0x01eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x01ea, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x01eb, code lost:
        r3 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x01f2, code lost:
        if (android.text.TextUtils.isEmpty(r13.$mActiveSerial) != false) goto L_0x02fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x01f4, code lost:
        r14 = r13.this$0.f20886d;
        r13.L$0 = r3;
        r13.L$1 = r2;
        r13.L$2 = r1;
        r13.label = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0207, code lost:
        if (r14.downloadCategories(r13) != r0) goto L_0x020a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0209, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x020a, code lost:
        r14 = r13.this$0.f20894l;
        r13.L$0 = r3;
        r13.L$1 = r2;
        r13.L$2 = r1;
        r13.label = 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x021e, code lost:
        if (r14.downloadAlarms(r13) != r0) goto L_0x0221;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0220, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0221, code lost:
        r14 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.getDeviceBySerial(r13.$mActiveSerial);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0227, code lost:
        if (r14 != null) goto L_0x022a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0232, code lost:
        if (com.fossil.blesdk.obfuscated.m42.f16641a[r14.ordinal()] == 1) goto L_0x027f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0234, code lost:
        r14 = r13.this$0.f20885c;
        r4 = r13.$mActiveSerial;
        r13.L$0 = r3;
        r13.L$1 = r2;
        r13.L$2 = r1;
        r13.label = 14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x024a, code lost:
        if (r14.downloadRecommendPresetList(r4, r13) != r0) goto L_0x024d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x024c, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x024d, code lost:
        r14 = r13.this$0.f20889g;
        r4 = r13.$mActiveSerial;
        r13.L$0 = r3;
        r13.L$1 = r2;
        r13.L$2 = r1;
        r13.label = 15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0263, code lost:
        if (r14.downloadAllMicroApp(r4, r13) != r0) goto L_0x0266;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0265, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0266, code lost:
        r14 = r13.this$0.f20885c;
        r4 = r13.$mActiveSerial;
        r13.L$0 = r3;
        r13.L$1 = r2;
        r13.L$2 = r1;
        r13.label = 16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x027c, code lost:
        if (r14.downloadPresetList(r4, r13) != r0) goto L_0x02fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x027e, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x027f, code lost:
        r14 = r13.this$0.f20887e;
        r4 = r13.$mActiveSerial;
        r13.L$0 = r3;
        r13.L$1 = r2;
        r13.L$2 = r1;
        r13.label = 9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0295, code lost:
        if (r14.downloadWatchApp(r4, r13) != r0) goto L_0x0298;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0297, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0298, code lost:
        r14 = r13.this$0.f20888f;
        r4 = r13.$mActiveSerial;
        r13.L$0 = r3;
        r13.L$1 = r2;
        r13.L$2 = r1;
        r13.label = 10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x02ae, code lost:
        if (r14.downloadAllComplication(r4, r13) != r0) goto L_0x02b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x02b0, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x02b1, code lost:
        r14 = r13.this$0.f20890h;
        r4 = r13.$mActiveSerial;
        r13.L$0 = r3;
        r13.L$1 = r2;
        r13.L$2 = r1;
        r13.label = 11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x02c7, code lost:
        if (r14.downloadPresetList(r4, r13) != r0) goto L_0x02ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x02c9, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x02ca, code lost:
        r14 = r13.this$0.f20890h;
        r4 = r13.$mActiveSerial;
        r13.L$0 = r3;
        r13.L$1 = r2;
        r13.L$2 = r1;
        r13.label = 12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x02e0, code lost:
        if (r14.downloadRecommendPresetList(r4, r13) != r0) goto L_0x02e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x02e2, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x02e3, code lost:
        r14 = r13.this$0.f20895m;
        r4 = r13.$mActiveSerial;
        r13.L$0 = r3;
        r13.L$1 = r2;
        r13.L$2 = r1;
        r13.label = 13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x02f9, code lost:
        if (r14.getWatchFacesFromServer(r4, r13) != r0) goto L_0x02fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x02fb, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x02fc, code lost:
        r13.this$0.mo34418b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0302, code lost:
        com.fossil.blesdk.obfuscated.kd4.m24405a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0305, code lost:
        throw null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0319, code lost:
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        com.portfolio.platform.data.model.MFUser mFUser;
        com.portfolio.platform.data.model.MFUser mFUser2;
        com.fossil.blesdk.obfuscated.zg4 zg42;
        com.portfolio.platform.data.model.MFUser mFUser3;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        switch (this.label) {
            case 0:
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg43 = this.f20898p$;
                mFUser2 = this.this$0.f20892j.getCurrentUser();
                if (mFUser2 == null) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.ApplicationEventListener.f20882p.mo34421a(), "user is not log in yet, do nothing");
                    break;
                } else {
                    if (!android.text.TextUtils.isEmpty(this.$mActiveSerial)) {
                        this.this$0.mo34417a();
                    }
                    if (com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34576w()) {
                        com.portfolio.platform.data.source.WatchLocalizationRepository m = this.this$0.f20896n;
                        this.L$0 = zg43;
                        this.L$1 = mFUser2;
                        this.L$2 = mFUser2;
                        this.label = 1;
                        if (m.getWatchLocalizationFromServer(false, this) != a) {
                            zg42 = zg43;
                            mFUser3 = mFUser2;
                            break;
                        } else {
                            return a;
                        }
                    }
                }
                break;
            case 1:
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                mFUser3 = (com.portfolio.platform.data.model.MFUser) this.L$1;
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 2:
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                mFUser3 = (com.portfolio.platform.data.model.MFUser) this.L$1;
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 3:
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                mFUser3 = (com.portfolio.platform.data.model.MFUser) this.L$1;
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 4:
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                mFUser3 = (com.portfolio.platform.data.model.MFUser) this.L$1;
                zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 5:
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg44 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 6:
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 7:
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 8:
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 9:
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 10:
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 11:
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 12:
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 13:
            case 16:
                com.portfolio.platform.data.model.MFUser mFUser4 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                com.portfolio.platform.data.model.MFUser mFUser5 = (com.portfolio.platform.data.model.MFUser) this.L$1;
                com.fossil.blesdk.obfuscated.zg4 zg45 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 14:
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            case 15:
                mFUser2 = (com.portfolio.platform.data.model.MFUser) this.L$2;
                mFUser = (com.portfolio.platform.data.model.MFUser) this.L$1;
                zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                break;
            default:
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
