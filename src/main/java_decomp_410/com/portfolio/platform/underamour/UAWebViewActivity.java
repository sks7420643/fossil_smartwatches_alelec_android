package com.portfolio.platform.underamour;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.fossil.blesdk.obfuscated.f62;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qf4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.BaseWebViewActivity;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UAWebViewActivity extends BaseWebViewActivity {
    @DexIgnore
    public static /* final */ String F;
    @DexIgnore
    public static /* final */ String G; // = f62.x.r();
    @DexIgnore
    public static /* final */ a H; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return UAWebViewActivity.G;
        }

        @DexIgnore
        public final String b() {
            return UAWebViewActivity.F;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final void a(Activity activity, String str) {
            kd4.b(activity, Constants.ACTIVITY);
            kd4.b(str, "authorizationUrl");
            Intent intent = new Intent(activity, UAWebViewActivity.class);
            intent.putExtra("urlToLoad", str);
            activity.startActivityForResult(intent, 3534);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends WebViewClient {
        @DexIgnore
        public /* final */ /* synthetic */ UAWebViewActivity a;

        @DexIgnore
        public b(UAWebViewActivity uAWebViewActivity) {
            this.a = uAWebViewActivity;
        }

        @DexIgnore
        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            kd4.b(str, "url");
            if (StringsKt__StringsKt.a((CharSequence) str, (CharSequence) "www.mapmyfitness.com/my_home", false, 2, (Object) null)) {
                if (webView != null) {
                    webView.loadUrl(this.a.v());
                }
                return super.shouldOverrideUrlLoading(webView, str);
            } else if (!qf4.c(str, "uasdk", false, 2, (Object) null)) {
                return super.shouldOverrideUrlLoading(webView, str);
            } else {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = UAWebViewActivity.H.b();
                local.d(b, "Got url: " + str);
                String queryParameter = Uri.parse(str).getQueryParameter("code");
                if (queryParameter != null) {
                    Intent intent = this.a.getIntent();
                    if (intent == null) {
                        intent = new Intent();
                    }
                    intent.putExtra("code", queryParameter);
                    this.a.setResult(-1, intent);
                    this.a.finish();
                    return true;
                }
                this.a.setResult(0);
                this.a.finish();
                return true;
            }
        }
    }

    /*
    static {
        String simpleName = UAWebViewActivity.class.getSimpleName();
        kd4.a((Object) simpleName, "UAWebViewActivity::class.java.simpleName");
        F = simpleName;
    }
    */

    @DexIgnore
    public WebViewClient s() {
        return new b(this);
    }
}
