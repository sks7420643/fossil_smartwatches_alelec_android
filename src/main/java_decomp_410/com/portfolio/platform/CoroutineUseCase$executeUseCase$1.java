package com.portfolio.platform;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.CoroutineUseCase$executeUseCase$1", mo27670f = "CoroutineUseCase.kt", mo27671l = {35}, mo27672m = "invokeSuspend")
public final class CoroutineUseCase$executeUseCase$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.CoroutineUseCase.C5606e $callBack;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.CoroutineUseCase.C5603b $requestValues;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20910p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.CoroutineUseCase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CoroutineUseCase$executeUseCase$1(com.portfolio.platform.CoroutineUseCase coroutineUseCase, com.portfolio.platform.CoroutineUseCase.C5606e eVar, com.portfolio.platform.CoroutineUseCase.C5603b bVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = coroutineUseCase;
        this.$callBack = eVar;
        this.$requestValues = bVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.CoroutineUseCase$executeUseCase$1 coroutineUseCase$executeUseCase$1 = new com.portfolio.platform.CoroutineUseCase$executeUseCase$1(this.this$0, this.$callBack, this.$requestValues, yb4);
        coroutineUseCase$executeUseCase$1.f20910p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return coroutineUseCase$executeUseCase$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.CoroutineUseCase$executeUseCase$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f20910p$;
            this.this$0.f20907a = this.$callBack;
            com.portfolio.platform.CoroutineUseCase coroutineUseCase = this.this$0;
            coroutineUseCase.f20908b = coroutineUseCase.mo26311c();
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.this$0.f20908b, "Start UseCase");
            com.portfolio.platform.CoroutineUseCase coroutineUseCase2 = this.this$0;
            com.portfolio.platform.CoroutineUseCase.C5603b bVar = this.$requestValues;
            this.L$0 = zg4;
            this.label = 1;
            obj = coroutineUseCase2.mo26310a(bVar, (com.fossil.blesdk.obfuscated.yb4<java.lang.Object>) this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (obj instanceof com.portfolio.platform.CoroutineUseCase.C5605d) {
            this.this$0.mo34436a((com.portfolio.platform.CoroutineUseCase.C5605d) obj);
        } else if (obj instanceof com.portfolio.platform.CoroutineUseCase.C5602a) {
            this.this$0.mo34434a((com.portfolio.platform.CoroutineUseCase.C5602a) obj);
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
