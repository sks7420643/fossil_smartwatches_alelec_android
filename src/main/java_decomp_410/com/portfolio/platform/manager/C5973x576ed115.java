package com.portfolio.platform.manager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.manager.WeatherManager$getWeatherForWatchApp$1$invokeSuspend$$inlined$let$lambda$1 */
public final class C5973x576ed115 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super kotlin.Pair<? extends com.portfolio.platform.data.model.microapp.weather.Weather, ? extends java.lang.Boolean>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper $location;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $tempUnit$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.zg4 $this_launch$inlined;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21281p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.manager.WeatherManager$getWeatherForWatchApp$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C5973x576ed115(com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper weatherLocationWrapper, com.fossil.blesdk.obfuscated.yb4 yb4, java.lang.String str, com.portfolio.platform.manager.WeatherManager$getWeatherForWatchApp$1 weatherManager$getWeatherForWatchApp$1, com.fossil.blesdk.obfuscated.zg4 zg4) {
        super(2, yb4);
        this.$location = weatherLocationWrapper;
        this.$tempUnit$inlined = str;
        this.this$0 = weatherManager$getWeatherForWatchApp$1;
        this.$this_launch$inlined = zg4;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.manager.C5973x576ed115 weatherManager$getWeatherForWatchApp$1$invokeSuspend$$inlined$let$lambda$1 = new com.portfolio.platform.manager.C5973x576ed115(this.$location, yb4, this.$tempUnit$inlined, this.this$0, this.$this_launch$inlined);
        weatherManager$getWeatherForWatchApp$1$invokeSuspend$$inlined$let$lambda$1.f21281p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return weatherManager$getWeatherForWatchApp$1$invokeSuspend$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.manager.C5973x576ed115) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21281p$;
            if (this.$location.isUseCurrentLocation()) {
                com.portfolio.platform.manager.WeatherManager weatherManager = this.this$0.this$0;
                java.lang.String str = this.$tempUnit$inlined;
                this.L$0 = zg4;
                this.label = 1;
                obj = weatherManager.mo39687a("weather", str, (com.fossil.blesdk.obfuscated.yb4<? super kotlin.Pair<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>>) this);
                if (obj == a) {
                    return a;
                }
            } else {
                com.portfolio.platform.manager.WeatherManager weatherManager2 = this.this$0.this$0;
                double lat = this.$location.getLat();
                double lng = this.$location.getLng();
                java.lang.String str2 = this.$tempUnit$inlined;
                this.L$0 = zg4;
                this.label = 2;
                obj = weatherManager2.mo39686a(lat, lng, str2, (com.fossil.blesdk.obfuscated.yb4<? super kotlin.Pair<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>>) this);
                if (obj == a) {
                    return a;
                }
            }
        } else if (i == 1 || i == 2) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return (kotlin.Pair) obj;
    }
}
