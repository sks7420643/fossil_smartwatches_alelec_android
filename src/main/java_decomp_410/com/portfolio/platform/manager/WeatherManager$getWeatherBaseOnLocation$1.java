package com.portfolio.platform.manager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.manager.WeatherManager", mo27670f = "WeatherManager.kt", mo27671l = {345, 188, 193}, mo27672m = "getWeatherBaseOnLocation")
public final class WeatherManager$getWeatherBaseOnLocation$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.manager.WeatherManager this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$getWeatherBaseOnLocation$1(com.portfolio.platform.manager.WeatherManager weatherManager, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = weatherManager;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.mo39687a((java.lang.String) null, (java.lang.String) null, (com.fossil.blesdk.obfuscated.yb4<? super kotlin.Pair<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>>) this);
    }
}
