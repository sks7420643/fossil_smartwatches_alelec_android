package com.portfolio.platform.manager;

import android.location.Location;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.dl4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.fl4;
import com.fossil.blesdk.obfuscated.gn2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.pk2;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sl2;
import com.fossil.blesdk.obfuscated.tz1;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.ul2;
import com.fossil.blesdk.obfuscated.vo2;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yf4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherInfoWatchAppResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherWatchAppInfo;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.microapp.weather.Temperature;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.enums.SyncErrorCode;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.response.ResponseKt;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Ref$BooleanRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherManager {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static WeatherManager m;
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public PortfolioApp a;
    @DexIgnore
    public ApiServiceV2 b;
    @DexIgnore
    public LocationSource c;
    @DexIgnore
    public UserRepository d;
    @DexIgnore
    public CustomizeRealDataRepository e;
    @DexIgnore
    public DianaPresetRepository f;
    @DexIgnore
    public GoogleApiService g;
    @DexIgnore
    public WeatherWatchAppSetting h;
    @DexIgnore
    public String i;
    @DexIgnore
    public Weather j;
    @DexIgnore
    public dl4 k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(WeatherManager weatherManager) {
            WeatherManager.m = weatherManager;
        }

        @DexIgnore
        public final WeatherManager b() {
            return WeatherManager.m;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final synchronized WeatherManager a() {
            WeatherManager b;
            if (WeatherManager.n.b() == null) {
                WeatherManager.n.a(new WeatherManager((fd4) null));
            }
            b = WeatherManager.n.b();
            if (b == null) {
                kd4.a();
                throw null;
            }
            return b;
        }
    }

    /*
    static {
        String simpleName = WeatherManager.class.getSimpleName();
        kd4.a((Object) simpleName, "WeatherManager::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public WeatherManager() {
        this.k = fl4.a(false, 1, (Object) null);
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final UserRepository e() {
        UserRepository userRepository = this.d;
        if (userRepository != null) {
            return userRepository;
        }
        kd4.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final void f() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final DianaPresetRepository c() {
        DianaPresetRepository dianaPresetRepository = this.f;
        if (dianaPresetRepository != null) {
            return dianaPresetRepository;
        }
        kd4.d("mDianaPresetRepository");
        throw null;
    }

    @DexIgnore
    public final GoogleApiService d() {
        GoogleApiService googleApiService = this.g;
        if (googleApiService != null) {
            return googleApiService;
        }
        kd4.d("mGoogleApiService");
        throw null;
    }

    @DexIgnore
    public final CustomizeRealDataRepository b() {
        CustomizeRealDataRepository customizeRealDataRepository = this.e;
        if (customizeRealDataRepository != null) {
            return customizeRealDataRepository;
        }
        kd4.d("mCustomizeRealDataRepository");
        throw null;
    }

    @DexIgnore
    public final void c(String str) {
        kd4.b(str, "serial");
        FLogger.INSTANCE.getLocal().d(l, "getWeatherForWatchApp");
        this.i = str;
        fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new WeatherManager$getWeatherForWatchApp$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public /* synthetic */ WeatherManager(fd4 fd4) {
        this();
    }

    @DexIgnore
    public final void b(String str) {
        kd4.b(str, "serial");
        FLogger.INSTANCE.getLocal().d(l, "getWeatherForTemperature");
        this.i = str;
        fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new WeatherManager$getWeatherForTemperature$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final ApiServiceV2 a() {
        ApiServiceV2 apiServiceV2 = this.b;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        kd4.d("mApiServiceV2");
        throw null;
    }

    @DexIgnore
    public final void a(String str) {
        kd4.b(str, "serial");
        FLogger.INSTANCE.getLocal().d(l, "getWeatherForChanceOfRain");
        this.i = str;
        fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new WeatherManager$getWeatherForChanceOfRain$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void b(Weather weather, boolean z) {
        float f2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        StringBuilder sb = new StringBuilder();
        sb.append("showTemperature - forecast=");
        sb.append(weather.getCurrently().getForecast());
        sb.append(", temperature=");
        Temperature temperature = weather.getCurrently().getTemperature();
        if (temperature != null) {
            sb.append(temperature.getCurrently());
            sb.append(", temperatureUnit=");
            sb.append(weather.getCurrently().getTemperature().getUnit());
            local.d(str, sb.toString());
            Weather.TEMP_UNIT.Companion companion = Weather.TEMP_UNIT.Companion;
            String unit = weather.getCurrently().getTemperature().getUnit();
            kd4.a((Object) unit, "weather.currently.temperature.unit");
            if (companion.getTempUnit(unit) == Weather.TEMP_UNIT.FAHRENHEIT) {
                f2 = pk2.c(weather.getCurrently().getTemperature().getCurrently());
            } else {
                f2 = weather.getCurrently().getTemperature().getCurrently();
            }
            WeatherComplicationAppInfo weatherComplicationAppInfo = weather.toWeatherComplicationAppInfo();
            String str2 = this.i;
            if (str2 != null) {
                PortfolioApp.W.c().a((DeviceAppResponse) weatherComplicationAppInfo, str2);
            }
            fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new WeatherManager$showTemperature$Anon2(this, f2, (yb4) null), 3, (Object) null);
            ul2 c2 = AnalyticsHelper.f.c("weather");
            if (c2 != null) {
                c2.a(z, "");
            }
            AnalyticsHelper.f.e("weather");
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v23, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v17, resolved type: com.fossil.blesdk.obfuscated.dl4} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00bb A[Catch:{ all -> 0x0201 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00f1 A[Catch:{ all -> 0x0201 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0116 A[Catch:{ all -> 0x01ed }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01f8  */
    public final /* synthetic */ Object a(String str, String str2, yb4<? super Pair<Weather, Boolean>> yb4) {
        WeatherManager$getWeatherBaseOnLocation$Anon1 weatherManager$getWeatherBaseOnLocation$Anon1;
        int i2;
        dl4 dl4;
        Object obj;
        Object obj2;
        Pair pair;
        WeatherManager weatherManager;
        Ref$BooleanRef ref$BooleanRef;
        dl4 dl42;
        String str3;
        String str4;
        dl4 dl43;
        LocationSource.Result result;
        WeatherManager weatherManager2;
        String str5;
        String str6;
        LocationSource locationSource;
        yb4<? super Pair<Weather, Boolean>> yb42 = yb4;
        if (yb42 instanceof WeatherManager$getWeatherBaseOnLocation$Anon1) {
            weatherManager$getWeatherBaseOnLocation$Anon1 = (WeatherManager$getWeatherBaseOnLocation$Anon1) yb42;
            int i3 = weatherManager$getWeatherBaseOnLocation$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                weatherManager$getWeatherBaseOnLocation$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj3 = weatherManager$getWeatherBaseOnLocation$Anon1.result;
                Object a2 = cc4.a();
                i2 = weatherManager$getWeatherBaseOnLocation$Anon1.label;
                if (i2 != 0) {
                    na4.a(obj3);
                    dl4 dl44 = this.k;
                    weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon0 = this;
                    str5 = str;
                    weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon1 = str5;
                    str6 = str2;
                    weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon2 = str6;
                    weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon3 = dl44;
                    weatherManager$getWeatherBaseOnLocation$Anon1.label = 1;
                    if (dl44.a((Object) null, weatherManager$getWeatherBaseOnLocation$Anon1) == a2) {
                        return a2;
                    }
                    dl4 = dl44;
                    weatherManager2 = this;
                } else if (i2 == 1) {
                    dl4 = (dl4) weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon3;
                    str6 = (String) weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon2;
                    str5 = (String) weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon1;
                    weatherManager2 = (WeatherManager) weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon0;
                    na4.a(obj3);
                } else if (i2 == 2) {
                    dl42 = weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon3;
                    String str7 = (String) weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon2;
                    String str8 = (String) weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon1;
                    WeatherManager weatherManager3 = (WeatherManager) weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon0;
                    na4.a(obj3);
                    str3 = str7;
                    dl43 = dl42;
                    str4 = str8;
                    weatherManager = weatherManager3;
                    try {
                        result = (LocationSource.Result) obj3;
                        if (result.getErrorState() != LocationSource.ErrorState.SUCCESS) {
                            Location location = result.getLocation();
                            Ref$BooleanRef ref$BooleanRef2 = new Ref$BooleanRef();
                            ref$BooleanRef2.element = false;
                            Location location2 = location;
                            WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1 weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1 = r3;
                            Ref$BooleanRef ref$BooleanRef3 = ref$BooleanRef2;
                            ug4 a3 = nh4.a();
                            ref$BooleanRef = ref$BooleanRef2;
                            Location location3 = location;
                            dl4 dl45 = dl43;
                            String str9 = str4;
                            try {
                                WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1 weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon12 = new WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1(location2, ref$BooleanRef3, (yb4) null, weatherManager, weatherManager$getWeatherBaseOnLocation$Anon1, str3, str4);
                                weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon0 = weatherManager;
                                weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon1 = str9;
                                weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon2 = str3;
                                dl4 = dl45;
                            } catch (Throwable th) {
                                th = th;
                                dl4 = dl45;
                                obj = null;
                                dl4.a(obj);
                                throw th;
                            }
                            try {
                                weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon3 = dl4;
                                weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon4 = result;
                                weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon5 = location3;
                                weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon6 = ref$BooleanRef;
                                weatherManager$getWeatherBaseOnLocation$Anon1.label = 3;
                                if (yf4.a(a3, weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1, weatherManager$getWeatherBaseOnLocation$Anon1) == a2) {
                                    return a2;
                                }
                                dl42 = dl4;
                                pair = new Pair(weatherManager.j, dc4.a(ref$BooleanRef.element));
                                dl4 = dl42;
                                obj2 = null;
                                dl4.a(obj2);
                                return pair;
                            } catch (Throwable th2) {
                                th = th2;
                                obj = null;
                                dl4.a(obj);
                                throw th;
                            }
                        } else {
                            dl4 = dl43;
                            String str10 = str4;
                            FLogger.INSTANCE.getLocal().d(l, "getWeatherBaseOnLocation getLocation error=" + result.getErrorState());
                            ul2 c2 = AnalyticsHelper.f.c(str10);
                            if (c2 != null) {
                                pd4 pd4 = pd4.a;
                                Object[] objArr = {AnalyticsHelper.f.d("weather")};
                                String format = String.format("update_%s_optional_error", Arrays.copyOf(objArr, objArr.length));
                                kd4.a((Object) format, "java.lang.String.format(format, *args)");
                                sl2 a4 = AnalyticsHelper.f.a(format);
                                a4.a("error_code", weatherManager.a(result.getErrorState()));
                                c2.a(a4);
                            }
                            obj2 = null;
                            try {
                                pair = new Pair(null, dc4.a(false));
                                dl4.a(obj2);
                                return pair;
                            } catch (Throwable th3) {
                                th = th3;
                                obj = null;
                                dl4.a(obj);
                                throw th;
                            }
                        }
                    } catch (Throwable th4) {
                        th = th4;
                        dl4 = dl43;
                        obj = null;
                        dl4.a(obj);
                        throw th;
                    }
                } else if (i2 == 3) {
                    Ref$BooleanRef ref$BooleanRef4 = (Ref$BooleanRef) weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon6;
                    Location location4 = (Location) weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon5;
                    LocationSource.Result result2 = (LocationSource.Result) weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon4;
                    dl42 = (dl4) weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon3;
                    String str11 = (String) weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon2;
                    String str12 = (String) weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon1;
                    WeatherManager weatherManager4 = (WeatherManager) weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon0;
                    try {
                        na4.a(obj3);
                        weatherManager = weatherManager4;
                        ref$BooleanRef = ref$BooleanRef4;
                    } catch (Throwable th5) {
                        th = th5;
                        dl4 = dl42;
                        obj = null;
                        dl4.a(obj);
                        throw th;
                    }
                    try {
                        pair = new Pair(weatherManager.j, dc4.a(ref$BooleanRef.element));
                        dl4 = dl42;
                        obj2 = null;
                        dl4.a(obj2);
                        return pair;
                    } catch (Throwable th6) {
                        th = th6;
                        dl4 = dl42;
                        obj = null;
                        dl4.a(obj);
                        throw th;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                FLogger.INSTANCE.getLocal().d(l, "getWeatherBaseOnLocation");
                if (weatherManager2.j != null) {
                    Calendar instance = Calendar.getInstance();
                    kd4.a((Object) instance, "Calendar.getInstance()");
                    long timeInMillis = instance.getTimeInMillis();
                    Weather weather = weatherManager2.j;
                    if (weather == null) {
                        kd4.a();
                        throw null;
                    } else if (timeInMillis - weather.getUpdatedAt() < ((long) 60000)) {
                        Pair pair2 = new Pair(weatherManager2.j, dc4.a(true));
                        dl4.a((Object) null);
                        return pair2;
                    }
                }
                locationSource = weatherManager2.c;
                if (locationSource == null) {
                    PortfolioApp portfolioApp = weatherManager2.a;
                    if (portfolioApp != null) {
                        weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon0 = weatherManager2;
                        weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon1 = str5;
                        weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon2 = str6;
                        weatherManager$getWeatherBaseOnLocation$Anon1.L$Anon3 = dl4;
                        weatherManager$getWeatherBaseOnLocation$Anon1.label = 2;
                        obj3 = locationSource.getLocation(portfolioApp, false, weatherManager$getWeatherBaseOnLocation$Anon1);
                        if (obj3 == a2) {
                            return a2;
                        }
                        str3 = str6;
                        str4 = str5;
                        weatherManager = weatherManager2;
                        dl43 = dl4;
                        result = (LocationSource.Result) obj3;
                        if (result.getErrorState() != LocationSource.ErrorState.SUCCESS) {
                        }
                    } else {
                        kd4.d("mPortfolioApp");
                        throw null;
                    }
                } else {
                    obj = null;
                    try {
                        kd4.d("mLocationSource");
                        throw null;
                    } catch (Throwable th7) {
                        th = th7;
                        dl4.a(obj);
                        throw th;
                    }
                }
            }
        }
        weatherManager$getWeatherBaseOnLocation$Anon1 = new WeatherManager$getWeatherBaseOnLocation$Anon1(this, yb42);
        Object obj32 = weatherManager$getWeatherBaseOnLocation$Anon1.result;
        Object a22 = cc4.a();
        i2 = weatherManager$getWeatherBaseOnLocation$Anon1.label;
        if (i2 != 0) {
        }
        try {
            FLogger.INSTANCE.getLocal().d(l, "getWeatherBaseOnLocation");
            if (weatherManager2.j != null) {
            }
            locationSource = weatherManager2.c;
            if (locationSource == null) {
            }
        } catch (Throwable th8) {
            th = th8;
            obj = null;
            dl4.a(obj);
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    public final /* synthetic */ Object a(double d2, double d3, yb4<? super String> yb4) {
        WeatherManager$getAddressBaseOnLocation$Anon1 weatherManager$getAddressBaseOnLocation$Anon1;
        int i2;
        String str;
        qo2 qo2;
        double d4 = d2;
        double d5 = d3;
        yb4<? super String> yb42 = yb4;
        if (yb42 instanceof WeatherManager$getAddressBaseOnLocation$Anon1) {
            weatherManager$getAddressBaseOnLocation$Anon1 = (WeatherManager$getAddressBaseOnLocation$Anon1) yb42;
            int i3 = weatherManager$getAddressBaseOnLocation$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                weatherManager$getAddressBaseOnLocation$Anon1.label = i3 - Integer.MIN_VALUE;
                WeatherManager$getAddressBaseOnLocation$Anon1 weatherManager$getAddressBaseOnLocation$Anon12 = weatherManager$getAddressBaseOnLocation$Anon1;
                Object obj = weatherManager$getAddressBaseOnLocation$Anon12.result;
                Object a2 = cc4.a();
                i2 = weatherManager$getAddressBaseOnLocation$Anon12.label;
                if (i2 != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = l;
                    local.d(str2, "getAddressBaseOnLocation latLng=" + d4 + ',' + d5);
                    WeatherManager$getAddressBaseOnLocation$response$Anon1 weatherManager$getAddressBaseOnLocation$response$Anon1 = r0;
                    str = "administrative_area_level_1";
                    WeatherManager$getAddressBaseOnLocation$response$Anon1 weatherManager$getAddressBaseOnLocation$response$Anon12 = new WeatherManager$getAddressBaseOnLocation$response$Anon1(this, d2, d3, "administrative_area_level_1", (yb4) null);
                    weatherManager$getAddressBaseOnLocation$Anon12.L$Anon0 = this;
                    weatherManager$getAddressBaseOnLocation$Anon12.D$Anon0 = d4;
                    weatherManager$getAddressBaseOnLocation$Anon12.D$Anon1 = d5;
                    weatherManager$getAddressBaseOnLocation$Anon12.L$Anon1 = str;
                    weatherManager$getAddressBaseOnLocation$Anon12.label = 1;
                    obj = ResponseKt.a(weatherManager$getAddressBaseOnLocation$response$Anon1, weatherManager$getAddressBaseOnLocation$Anon12);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i2 == 1) {
                    double d6 = weatherManager$getAddressBaseOnLocation$Anon12.D$Anon1;
                    double d7 = weatherManager$getAddressBaseOnLocation$Anon12.D$Anon0;
                    WeatherManager weatherManager = (WeatherManager) weatherManager$getAddressBaseOnLocation$Anon12.L$Anon0;
                    na4.a(obj);
                    str = (String) weatherManager$getAddressBaseOnLocation$Anon12.L$Anon1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    xz1 xz1 = (xz1) ((ro2) qo2).a();
                    tz1 b2 = xz1 != null ? xz1.b("results") : null;
                    if (b2 != null) {
                        Iterator<JsonElement> it = b2.iterator();
                        while (it.hasNext()) {
                            JsonElement next = it.next();
                            kd4.a((Object) next, Constants.RESULT);
                            xz1 d8 = next.d();
                            tz1 b3 = d8 != null ? d8.b("address_components") : null;
                            if (b3 != null) {
                                Iterator<JsonElement> it2 = b3.iterator();
                                while (it2.hasNext()) {
                                    JsonElement next2 = it2.next();
                                    kd4.a((Object) next2, "addressComponent");
                                    xz1 d9 = next2.d();
                                    tz1 b4 = d9 != null ? d9.b("types") : null;
                                    if (b4 != null && b4.size() > 0) {
                                        JsonElement jsonElement = b4.get(0);
                                        kd4.a((Object) jsonElement, "types[0]");
                                        if (kd4.a((Object) jsonElement.f(), (Object) str)) {
                                            JsonElement a3 = next2.d().a("long_name");
                                            kd4.a((Object) a3, "addressComponent.asJsonObject.get(\"long_name\")");
                                            String f2 = a3.f();
                                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                            String str3 = l;
                                            local2.d(str3, "cityName=" + f2);
                                            kd4.a((Object) f2, "cityName");
                                            return f2;
                                        }
                                    }
                                }
                                continue;
                            }
                        }
                    }
                    return "";
                } else if (qo2 instanceof po2) {
                    return "";
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        weatherManager$getAddressBaseOnLocation$Anon1 = new WeatherManager$getAddressBaseOnLocation$Anon1(this, yb42);
        WeatherManager$getAddressBaseOnLocation$Anon1 weatherManager$getAddressBaseOnLocation$Anon122 = weatherManager$getAddressBaseOnLocation$Anon1;
        Object obj2 = weatherManager$getAddressBaseOnLocation$Anon122.result;
        Object a22 = cc4.a();
        i2 = weatherManager$getAddressBaseOnLocation$Anon122.label;
        if (i2 != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final /* synthetic */ Object a(double d2, double d3, String str, yb4<? super Pair<Weather, Boolean>> yb4) {
        WeatherManager$getWeather$Anon1 weatherManager$getWeather$Anon1;
        int i2;
        qo2 qo2;
        yb4<? super Pair<Weather, Boolean>> yb42 = yb4;
        if (yb42 instanceof WeatherManager$getWeather$Anon1) {
            weatherManager$getWeather$Anon1 = (WeatherManager$getWeather$Anon1) yb42;
            int i3 = weatherManager$getWeather$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                weatherManager$getWeather$Anon1.label = i3 - Integer.MIN_VALUE;
                WeatherManager$getWeather$Anon1 weatherManager$getWeather$Anon12 = weatherManager$getWeather$Anon1;
                Object obj = weatherManager$getWeather$Anon12.result;
                Object a2 = cc4.a();
                i2 = weatherManager$getWeather$Anon12.label;
                if (i2 != 0) {
                    na4.a(obj);
                    WeatherManager$getWeather$repoResponse$Anon1 weatherManager$getWeather$repoResponse$Anon1 = new WeatherManager$getWeather$repoResponse$Anon1(this, d2, d3, str, (yb4) null);
                    weatherManager$getWeather$Anon12.L$Anon0 = this;
                    weatherManager$getWeather$Anon12.D$Anon0 = d2;
                    weatherManager$getWeather$Anon12.D$Anon1 = d3;
                    weatherManager$getWeather$Anon12.L$Anon1 = str;
                    weatherManager$getWeather$Anon12.label = 1;
                    obj = ResponseKt.a(weatherManager$getWeather$repoResponse$Anon1, weatherManager$getWeather$Anon12);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i2 == 1) {
                    String str2 = (String) weatherManager$getWeather$Anon12.L$Anon1;
                    double d4 = weatherManager$getWeather$Anon12.D$Anon1;
                    double d5 = weatherManager$getWeather$Anon12.D$Anon0;
                    WeatherManager weatherManager = (WeatherManager) weatherManager$getWeather$Anon12.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = l;
                local.d(str3, "getWeather onResponse: response = " + qo2);
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    if (ro2.a() == null) {
                        return new Pair(null, dc4.a(false));
                    }
                    vo2 vo2 = new vo2();
                    vo2.a((xz1) ro2.a());
                    return new Pair(vo2.a(), dc4.a(false));
                } else if (qo2 instanceof po2) {
                    return new Pair(null, dc4.a(false));
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        weatherManager$getWeather$Anon1 = new WeatherManager$getWeather$Anon1(this, yb42);
        WeatherManager$getWeather$Anon1 weatherManager$getWeather$Anon122 = weatherManager$getWeather$Anon1;
        Object obj2 = weatherManager$getWeather$Anon122.result;
        Object a22 = cc4.a();
        i2 = weatherManager$getWeather$Anon122.label;
        if (i2 != 0) {
        }
        qo2 = (qo2) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str32 = l;
        local2.d(str32, "getWeather onResponse: response = " + qo2);
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    public final void a(Weather weather, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "showChanceOfRain - probability=" + weather.getCurrently().getRainProbability());
        int rainProbability = (int) (weather.getCurrently().getRainProbability() * ((float) 100));
        ChanceOfRainComplicationAppInfo chanceOfRainComplicationAppInfo = weather.toChanceOfRainComplicationAppInfo();
        String str2 = this.i;
        if (str2 != null) {
            PortfolioApp.W.c().a((DeviceAppResponse) chanceOfRainComplicationAppInfo, str2);
        }
        fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new WeatherManager$showChanceOfRain$Anon2(this, rainProbability, (yb4) null), 3, (Object) null);
        ul2 c2 = AnalyticsHelper.f.c("chance-of-rain");
        if (c2 != null) {
            c2.a(z, "");
        }
        AnalyticsHelper.f.e("chance-of-rain");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x003f  */
    public final void a(Pair<Weather, Boolean> pair, Pair<Weather, Boolean> pair2, Pair<Weather, Boolean> pair3) {
        WeatherWatchAppInfo weatherWatchAppInfo;
        WeatherWatchAppInfo weatherWatchAppInfo2;
        WeatherWatchAppInfo weatherWatchAppInfo3;
        FLogger.INSTANCE.getLocal().d(l, "showWeatherWatchApp - firstWeather=" + pair + ", secondWeather=" + pair2 + ", thirdWeather=" + pair3);
        if (pair != null) {
            Weather first = pair.getFirst();
            if (first != null) {
                weatherWatchAppInfo = first.toWeatherWatchAppInfo();
                if (pair2 != null) {
                    Weather first2 = pair2.getFirst();
                    if (first2 != null) {
                        weatherWatchAppInfo2 = first2.toWeatherWatchAppInfo();
                        if (pair3 != null) {
                            Weather first3 = pair3.getFirst();
                            if (first3 != null) {
                                weatherWatchAppInfo3 = first3.toWeatherWatchAppInfo();
                                if (weatherWatchAppInfo != null) {
                                    return;
                                }
                                if (weatherWatchAppInfo != null) {
                                    PortfolioApp.W.c().a((DeviceAppResponse) new WeatherInfoWatchAppResponse(weatherWatchAppInfo, weatherWatchAppInfo2, weatherWatchAppInfo3), PortfolioApp.W.c().e());
                                    ul2 c2 = AnalyticsHelper.f.c("weather");
                                    if (c2 != null) {
                                        Boolean second = pair != null ? pair.getSecond() : null;
                                        if (second != null) {
                                            c2.a(second.booleanValue(), "");
                                        } else {
                                            kd4.a();
                                            throw null;
                                        }
                                    }
                                    AnalyticsHelper.f.e("weather");
                                    return;
                                }
                                kd4.a();
                                throw null;
                            }
                        }
                        weatherWatchAppInfo3 = null;
                        if (weatherWatchAppInfo != null) {
                        }
                    }
                }
                weatherWatchAppInfo2 = null;
                if (pair3 != null) {
                }
                weatherWatchAppInfo3 = null;
                if (weatherWatchAppInfo != null) {
                }
            }
        }
        weatherWatchAppInfo = null;
        if (pair2 != null) {
        }
        weatherWatchAppInfo2 = null;
        if (pair3 != null) {
        }
        weatherWatchAppInfo3 = null;
        if (weatherWatchAppInfo != null) {
        }
    }

    @DexIgnore
    public final String a(LocationSource.ErrorState errorState) {
        int i2 = gn2.d[errorState.ordinal()];
        if (i2 == 1) {
            return SyncErrorCode.LOCATION_ACCESS_DISABLED.getCode();
        }
        if (i2 == 2) {
            return SyncErrorCode.BACKGROUND_LOCATION_ACCESS_DISABLED.getCode();
        }
        if (i2 != 3) {
            return SyncErrorCode.UNKNOWN.getCode();
        }
        return SyncErrorCode.LOCATION_SERVICE_DISABLED.getCode();
    }
}
