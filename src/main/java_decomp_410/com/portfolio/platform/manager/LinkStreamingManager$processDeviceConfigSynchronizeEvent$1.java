package com.portfolio.platform.manager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.manager.LinkStreamingManager$processDeviceConfigSynchronizeEvent$1", mo27670f = "LinkStreamingManager.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class LinkStreamingManager$processDeviceConfigSynchronizeEvent$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $action;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $deviceId;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21259p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinkStreamingManager$processDeviceConfigSynchronizeEvent$1(int i, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$action = i;
        this.$deviceId = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.manager.LinkStreamingManager$processDeviceConfigSynchronizeEvent$1 linkStreamingManager$processDeviceConfigSynchronizeEvent$1 = new com.portfolio.platform.manager.LinkStreamingManager$processDeviceConfigSynchronizeEvent$1(this.$action, this.$deviceId, yb4);
        linkStreamingManager$processDeviceConfigSynchronizeEvent$1.f21259p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return linkStreamingManager$processDeviceConfigSynchronizeEvent$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.manager.LinkStreamingManager$processDeviceConfigSynchronizeEvent$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            if (this.$action == com.fossil.blesdk.device.event.SynchronizationAction.SET.ordinal()) {
                com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34554o(this.$deviceId);
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
