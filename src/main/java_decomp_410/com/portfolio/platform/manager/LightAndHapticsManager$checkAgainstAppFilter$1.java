package com.portfolio.platform.manager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstAppFilter$1", mo27670f = "LightAndHapticsManager.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class LightAndHapticsManager$checkAgainstAppFilter$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.NotificationInfo $info;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21245p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.manager.LightAndHapticsManager this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LightAndHapticsManager$checkAgainstAppFilter$1(com.portfolio.platform.manager.LightAndHapticsManager lightAndHapticsManager, com.portfolio.platform.data.model.NotificationInfo notificationInfo, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = lightAndHapticsManager;
        this.$info = notificationInfo;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstAppFilter$1 lightAndHapticsManager$checkAgainstAppFilter$1 = new com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstAppFilter$1(this.this$0, this.$info, yb4);
        lightAndHapticsManager$checkAgainstAppFilter$1.f21245p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return lightAndHapticsManager$checkAgainstAppFilter$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstAppFilter$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.manager.LightAndHapticsManager lightAndHapticsManager = this.this$0;
            java.lang.String packageName = this.$info.getPackageName();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) packageName, "info.packageName");
            com.portfolio.platform.data.model.LightAndHaptics a = lightAndHapticsManager.mo39642a(packageName, this.$info.getBody());
            if (a != null) {
                a.setNotificationType(com.portfolio.platform.data.NotificationType.APP_FILTER);
                this.this$0.mo39644a(a);
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
