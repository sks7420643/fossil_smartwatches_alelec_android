package com.portfolio.platform.manager;

import android.location.Location;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import java.util.Calendar;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$BooleanRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ yb4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Location $currentLocation;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$BooleanRef $isFromCaches;
    @DexIgnore
    public /* final */ /* synthetic */ String $tempUnit$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ String $tracingKey$inlined;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Pair<? extends Weather, ? extends Boolean>>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1 weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1 weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1 = this.this$Anon0;
                WeatherManager weatherManager = weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1.this$Anon0;
                Location location = weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1.$currentLocation;
                if (location != null) {
                    double latitude = location.getLatitude();
                    double longitude = this.this$Anon0.$currentLocation.getLongitude();
                    String str = this.this$Anon0.$tempUnit$inlined;
                    this.L$Anon0 = zg4;
                    this.label = 1;
                    obj = weatherManager.a(latitude, longitude, str, (yb4<? super Pair<Weather, Boolean>>) this);
                    if (obj == a) {
                        return a;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super String>, Object> {
        @DexIgnore
        public Object L$Anon0;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1 weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, yb4);
            anon2.p$ = (zg4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = cc4.a();
            int i = this.label;
            if (i == 0) {
                na4.a(obj);
                zg4 zg4 = this.p$;
                WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1 weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1 = this.this$Anon0;
                WeatherManager weatherManager = weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1.this$Anon0;
                Location location = weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1.$currentLocation;
                if (location != null) {
                    double latitude = location.getLatitude();
                    double longitude = this.this$Anon0.$currentLocation.getLongitude();
                    this.L$Anon0 = zg4;
                    this.label = 1;
                    obj = weatherManager.a(latitude, longitude, (yb4<? super String>) this);
                    if (obj == a) {
                        return a;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else if (i == 1) {
                zg4 zg42 = (zg4) this.L$Anon0;
                na4.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1(Location location, Ref$BooleanRef ref$BooleanRef, yb4 yb4, WeatherManager weatherManager, yb4 yb42, String str, String str2) {
        super(2, yb4);
        this.$currentLocation = location;
        this.$isFromCaches = ref$BooleanRef;
        this.this$Anon0 = weatherManager;
        this.$continuation$inlined = yb42;
        this.$tempUnit$inlined = str;
        this.$tracingKey$inlined = str2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1 weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1 = new WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1(this.$currentLocation, this.$isFromCaches, yb4, this.this$Anon0, this.$continuation$inlined, this.$tempUnit$inlined, this.$tracingKey$inlined);
        weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1.p$ = (zg4) obj;
        return weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b3  */
    public final Object invokeSuspend(Object obj) {
        Weather a;
        Weather weather;
        zg4 zg4;
        gh4 gh4;
        gh4 gh42;
        Object a2 = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg42 = this.p$;
            zg4 zg43 = zg42;
            gh4 a3 = ag4.a(zg43, (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (yb4) null), 3, (Object) null);
            gh42 = ag4.a(zg43, (CoroutineContext) null, (CoroutineStart) null, new Anon2(this, (yb4) null), 3, (Object) null);
            this.L$Anon0 = zg42;
            this.L$Anon1 = a3;
            this.L$Anon2 = gh42;
            this.label = 1;
            Object a4 = a3.a(this);
            if (a4 == a2) {
                return a2;
            }
            gh4 gh43 = a3;
            zg4 = zg42;
            obj = a4;
            gh4 = gh43;
        } else if (i == 1) {
            gh42 = (gh4) this.L$Anon2;
            gh4 = (gh4) this.L$Anon1;
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2) {
            weather = (Weather) this.L$Anon4;
            Pair pair = (Pair) this.L$Anon3;
            gh4 gh44 = (gh4) this.L$Anon2;
            gh4 gh45 = (gh4) this.L$Anon1;
            zg4 zg44 = (zg4) this.L$Anon0;
            na4.a(obj);
            weather.setAddress((String) obj);
            a = this.this$Anon0.j;
            if (a != null) {
                Calendar instance = Calendar.getInstance();
                kd4.a((Object) instance, "Calendar.getInstance()");
                a.setUpdatedAt(instance.getTimeInMillis());
            }
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair2 = (Pair) obj;
        this.this$Anon0.j = (Weather) pair2.getFirst();
        this.$isFromCaches.element = ((Boolean) pair2.getSecond()).booleanValue();
        Weather a5 = this.this$Anon0.j;
        if (a5 != null) {
            this.L$Anon0 = zg4;
            this.L$Anon1 = gh4;
            this.L$Anon2 = gh42;
            this.L$Anon3 = pair2;
            this.L$Anon4 = a5;
            this.label = 2;
            obj = gh42.a(this);
            if (obj == a2) {
                return a2;
            }
            weather = a5;
            weather.setAddress((String) obj);
        }
        a = this.this$Anon0.j;
        if (a != null) {
        }
        return qa4.a;
    }
}
