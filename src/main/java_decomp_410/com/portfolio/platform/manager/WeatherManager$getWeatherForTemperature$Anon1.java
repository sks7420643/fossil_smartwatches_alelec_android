package com.portfolio.platform.manager;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.gn2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.enums.Unit;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForTemperature$Anon1", f = "WeatherManager.kt", l = {174}, m = "invokeSuspend")
public final class WeatherManager$getWeatherForTemperature$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$getWeatherForTemperature$Anon1(WeatherManager weatherManager, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = weatherManager;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WeatherManager$getWeatherForTemperature$Anon1 weatherManager$getWeatherForTemperature$Anon1 = new WeatherManager$getWeatherForTemperature$Anon1(this.this$Anon0, yb4);
        weatherManager$getWeatherForTemperature$Anon1.p$ = (zg4) obj;
        return weatherManager$getWeatherForTemperature$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WeatherManager$getWeatherForTemperature$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006e A[RETURN] */
    public final Object invokeSuspend(Object obj) {
        String str;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            MFUser currentUser = this.this$Anon0.e().getCurrentUser();
            if (currentUser != null) {
                Unit temperatureUnit = currentUser.getTemperatureUnit();
                if (temperatureUnit != null) {
                    int i2 = gn2.c[temperatureUnit.ordinal()];
                    if (i2 == 1) {
                        str = Weather.TEMP_UNIT.CELSIUS.getValue();
                    } else if (i2 == 2) {
                        str = Weather.TEMP_UNIT.FAHRENHEIT.getValue();
                    }
                    WeatherManager weatherManager = this.this$Anon0;
                    this.L$Anon0 = zg4;
                    this.L$Anon1 = currentUser;
                    this.L$Anon2 = str;
                    this.label = 1;
                    obj = weatherManager.a("weather", str, (yb4<? super Pair<Weather, Boolean>>) this);
                    if (obj == a) {
                        return a;
                    }
                }
                str = Weather.TEMP_UNIT.CELSIUS.getValue();
                WeatherManager weatherManager2 = this.this$Anon0;
                this.L$Anon0 = zg4;
                this.L$Anon1 = currentUser;
                this.L$Anon2 = str;
                this.label = 1;
                obj = weatherManager2.a("weather", str, (yb4<? super Pair<Weather, Boolean>>) this);
                if (obj == a) {
                }
            }
            return qa4.a;
        } else if (i == 1) {
            String str2 = (String) this.L$Anon2;
            MFUser mFUser = (MFUser) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Pair pair = (Pair) obj;
        Weather weather = (Weather) pair.component1();
        boolean booleanValue = ((Boolean) pair.component2()).booleanValue();
        if (weather != null) {
            this.this$Anon0.b(weather, booleanValue);
        }
        return qa4.a;
    }
}
