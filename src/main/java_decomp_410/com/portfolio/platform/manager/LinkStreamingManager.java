package com.portfolio.platform.manager;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.share.internal.ShareConstants;
import com.fossil.blesdk.device.data.notification.AppNotificationControlAction;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.model.enumerate.RingPhoneState;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cn2;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.fn2;
import com.fossil.blesdk.obfuscated.gj2;
import com.fossil.blesdk.obfuscated.hj2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kj2;
import com.fossil.blesdk.obfuscated.lj2;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.ox3;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.pp2;
import com.fossil.blesdk.obfuscated.px2;
import com.fossil.blesdk.obfuscated.rp2;
import com.fossil.blesdk.obfuscated.ul2;
import com.fossil.blesdk.obfuscated.vy2;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.RingPhoneComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.RingMyPhoneMicroAppResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.enums.ServiceStatus;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LinkStreamingManager implements rp2 {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((fd4) null);
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<pp2> a; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ HybridPresetRepository b;
    @DexIgnore
    public /* final */ AlarmsRepository c;
    @DexIgnore
    public /* final */ SetNotificationUseCase d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return LinkStreamingManager.e;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Runnable {
        @DexIgnore
        public /* final */ String e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ LinkStreamingManager g;

        @DexIgnore
        public b(LinkStreamingManager linkStreamingManager, String str, int i) {
            kd4.b(str, "serial");
            this.g = linkStreamingManager;
            this.e = str;
            this.f = i;
        }

        @DexIgnore
        public void run() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = LinkStreamingManager.f.a();
            local.d(a, "Inside SteamingAction .run - eventId=" + this.f + ", serial=" + this.e);
            if (TextUtils.isEmpty(this.e)) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a2 = LinkStreamingManager.f.a();
                local2.e(a2, "Inside .onStreamingEvent of " + this.e + " no active device");
                return;
            }
            HybridPreset activePresetBySerial = this.g.b.getActivePresetBySerial(this.e);
            if (activePresetBySerial != null) {
                Iterator<HybridPresetAppSetting> it = activePresetBySerial.getButtons().iterator();
                while (it.hasNext()) {
                    HybridPresetAppSetting next = it.next();
                    if (kd4.a((Object) next.getAppId(), (Object) MicroAppInstruction.MicroAppID.Companion.getMicroAppIdFromDeviceEventId(this.f).getValue())) {
                        int i = this.f;
                        if (i == DeviceEventId.RING_MY_PHONE_MICRO_APP.ordinal()) {
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String a3 = LinkStreamingManager.f.a();
                            local3.d(a3, "MicroAppAction.run UAPP_RING_PHONE microAppSetting " + next.getSettings());
                            LinkStreamingManager linkStreamingManager = this.g;
                            String str = this.e;
                            String settings = next.getSettings();
                            if (settings != null) {
                                linkStreamingManager.a(str, settings);
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else if (i == DeviceEventId.COMMUTE_TIME_ETA_MICRO_APP.ordinal() || i == DeviceEventId.COMMUTE_TIME_TRAVEL_MICRO_APP.ordinal()) {
                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.EXTRA_INFO, next.getSettings());
                            CommuteTimeService.A.a(PortfolioApp.W.c(), bundle, this.g);
                        }
                    }
                }
            }
        }
    }

    /*
    static {
        String canonicalName = LinkStreamingManager.class.getCanonicalName();
        if (canonicalName != null) {
            kd4.a((Object) canonicalName, "LinkStreamingManager::class.java.canonicalName!!");
            e = canonicalName;
            return;
        }
        kd4.a();
        throw null;
    }
    */

    @DexIgnore
    public LinkStreamingManager(HybridPresetRepository hybridPresetRepository, vy2 vy2, px2 px2, NotificationSettingsDatabase notificationSettingsDatabase, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, en2 en2, AlarmsRepository alarmsRepository, SetNotificationUseCase setNotificationUseCase) {
        kd4.b(hybridPresetRepository, "mPresetRepository");
        kd4.b(vy2, "mGetApp");
        kd4.b(px2, "mGetAllContactGroups");
        kd4.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        kd4.b(notificationsRepository, "mNotificationsRepository");
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(en2, "mSharePref");
        kd4.b(alarmsRepository, "mAlarmsRepository");
        kd4.b(setNotificationUseCase, "mSetNotificationUseCase");
        this.b = hybridPresetRepository;
        this.c = alarmsRepository;
        this.d = setNotificationUseCase;
        PortfolioApp.W.b((Object) this);
    }

    @DexIgnore
    public final void d(String str, int i) {
        RingPhoneState ringPhoneState;
        FLogger.INSTANCE.getLocal().d(e, "startRingMyPhone");
        if (i == RingPhoneState.ON.ordinal()) {
            fn2.o.a().a(new Ringtone(Constants.RINGTONE_DEFAULT, ""), 60000);
            ringPhoneState = RingPhoneState.ON;
        } else {
            fn2.o.a().b();
            ringPhoneState = RingPhoneState.OFF;
        }
        PortfolioApp.W.c().a((DeviceAppResponse) new RingPhoneComplicationAppInfo(ringPhoneState), str);
    }

    @DexIgnore
    @ox3
    public final void onDeviceAppEvent(gj2 gj2) {
        kd4.b(gj2, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, "Inside " + e + ".onDeviceAppEvent - appId=" + gj2.a() + ", serial=" + gj2.c());
        int a2 = gj2.a();
        if (a2 == DeviceEventId.WEATHER_COMPLICATION.ordinal()) {
            String d2 = AnalyticsHelper.f.d("weather");
            ul2 b2 = AnalyticsHelper.f.b(a(d2));
            b2.d();
            AnalyticsHelper.f.a(d2, b2);
            WeatherManager a3 = WeatherManager.n.a();
            String c2 = gj2.c();
            kd4.a((Object) c2, "event.serial");
            a3.b(c2);
        } else if (a2 == DeviceEventId.CHANCE_OF_RAIN_COMPLICATION.ordinal()) {
            ul2 b3 = AnalyticsHelper.f.b(a(AnalyticsHelper.f.d("chance-of-rain")));
            b3.d();
            AnalyticsHelper.f.a("chance-of-rain", b3);
            WeatherManager a4 = WeatherManager.n.a();
            String c3 = gj2.c();
            kd4.a((Object) c3, "event.serial");
            a4.a(c3);
        } else if (a2 == DeviceEventId.RING_PHONE.ordinal()) {
            int i = gj2.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String c4 = gj2.c();
            kd4.a((Object) c4, "event.serial");
            d(c4, i);
        } else if (a2 == DeviceEventId.WEATHER_WATCH_APP.ordinal()) {
            WeatherManager a5 = WeatherManager.n.a();
            String c5 = gj2.c();
            kd4.a((Object) c5, "event.serial");
            a5.c(c5);
        } else if (a2 == DeviceEventId.NOTIFICATION_FILTER_SYNC.ordinal()) {
            int i2 = gj2.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String c6 = gj2.c();
            kd4.a((Object) c6, "event.serial");
            c(c6, i2);
        } else if (a2 == DeviceEventId.ALARM_SYNC.ordinal()) {
            int i3 = gj2.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String c7 = gj2.c();
            kd4.a((Object) c7, "event.serial");
            a(c7, i3);
        } else if (a2 == DeviceEventId.DEVICE_CONFIG_SYNC.ordinal()) {
            int i4 = gj2.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String c8 = gj2.c();
            kd4.a((Object) c8, "event.serial");
            b(c8, i4);
        } else if (a2 == DeviceEventId.MUSIC_CONTROL.ordinal()) {
            NotifyMusicEventResponse.MusicMediaAction musicMediaAction = NotifyMusicEventResponse.MusicMediaAction.values()[gj2.b().getInt(ButtonService.Companion.getMUSIC_ACTION_EVENT(), NotifyMusicEventResponse.MusicMediaAction.NONE.ordinal())];
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = e;
            local2.d(str2, "Inside " + e + ".onMusicActionEvent - musicActionEvent=" + musicMediaAction + ", serial=" + gj2.c());
            ((MusicControlComponent) MusicControlComponent.o.a(PortfolioApp.W.c())).a(musicMediaAction);
        } else if (a2 == DeviceEventId.APP_NOTIFICATION_CONTROL.ordinal()) {
            FLogger.INSTANCE.getLocal().d(e, "on app notification control received");
            a(gj2.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1));
        } else if (a2 == DeviceEventId.COMMUTE_TIME_WATCH_APP.ordinal()) {
            ul2 b4 = AnalyticsHelper.f.b(a(AnalyticsHelper.f.d("commute-time")));
            b4.d();
            AnalyticsHelper.f.a("commute-time", b4);
            int i5 = gj2.b().getInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, -1);
            String string = gj2.b().getString(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, "");
            WatchAppCommuteTimeManager a6 = WatchAppCommuteTimeManager.s.a();
            String c9 = gj2.c();
            kd4.a((Object) c9, "event.serial");
            kd4.a((Object) string, ShareConstants.DESTINATION);
            a6.a(c9, string, i5);
        } else {
            String c10 = gj2.c();
            kd4.a((Object) c10, "event.serial");
            new Thread(new b(this, c10, gj2.a())).start();
        }
    }

    @DexIgnore
    @ox3
    public final void onMicroAppCancelEvent(hj2 hj2) {
        kd4.b(hj2, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, "Inside " + e + ".onMicroAppCancelEvent - event=" + hj2.a() + ", serial=" + hj2.b());
        a((List<? extends pp2>) this.a);
    }

    @DexIgnore
    @ox3
    public final void onMusicActionEvent(kj2 kj2) {
        kd4.b(kj2, Constants.EVENT);
        FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("Inside ");
        sb.append(e);
        sb.append(".onMusicActionEvent - musicActionEvent=");
        kj2.a();
        throw null;
    }

    @DexIgnore
    @ox3
    public final void onStreamingEvent(lj2 lj2) {
        kd4.b(lj2, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, "Inside " + e + ".onStreamingEvent - event=" + lj2.a() + ", serial=" + lj2.b());
        String b2 = lj2.b();
        kd4.a((Object) b2, "event.serial");
        new Thread(new b(this, b2, lj2.a())).start();
    }

    @DexIgnore
    public final void a() {
        PortfolioApp.W.c(this);
    }

    @DexIgnore
    public final fi4 c(String str, int i) {
        throw null;
        // return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new LinkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1(this, i, str, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(int i) {
        if (DeviceHelper.o.l()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "process call event: action = " + i);
            if (i == AppNotificationControlAction.REJECT_PHONE_CALL.ordinal()) {
                cn2.b.a().b();
            } else if (i == AppNotificationControlAction.ACCEPT_PHONE_CALL.ordinal()) {
                cn2.b.a().a();
            } else {
                if (i == AppNotificationControlAction.DISMISS_NOTIFICATION.ordinal()) {
                }
            }
        }
    }

    @DexIgnore
    public void b(pp2 pp2) {
        kd4.b(pp2, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(e, "removeObject");
        if (!this.a.isEmpty()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "removeObject mServiceListSize=" + this.a.size());
            Iterator<pp2> it = this.a.iterator();
            while (it.hasNext()) {
                pp2 next = it.next();
                kd4.a((Object) next, "autoStopBaseService");
                if (next.c() == pp2.c()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = e;
                    local2.d(str2, "removeObject actionId " + pp2.c());
                    this.a.remove(next);
                }
            }
        }
    }

    @DexIgnore
    public void a(int i, ServiceStatus serviceStatus) {
        kd4.b(serviceStatus, "status");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, "onStatusChanged action " + i);
    }

    @DexIgnore
    public void a(pp2 pp2) {
        kd4.b(pp2, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(e, "addObject");
        if (!this.a.contains(pp2)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "addObject service actionId " + pp2.c());
            this.a.add(pp2);
        }
    }

    @DexIgnore
    public final fi4 b(String str, int i) {
        throw null;
        // return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new LinkStreamingManager$processDeviceConfigSynchronizeEvent$Anon1(i, str, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(List<? extends pp2> list) {
        if (list != null && !this.a.isEmpty()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "stopServices serviceListSize=" + this.a.size());
            for (pp2 b2 : list) {
                b2.b();
            }
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        throw null;
        // kd4.b(str, "deviceId");
        // kd4.b(str2, MicroAppSetting.SETTING);
        // ILocalFLogger local = FLogger.INSTANCE.getLocal();
        // String str3 = e;
        // local.d(str3, "startRingMyPhone, setting=" + str2);
        // try {
        //     if (TextUtils.isEmpty(str2)) {
        //         List<Ringtone> c2 = AppHelper.f.c();
        //         Ringtone ringtone = c2.get(0);
        //         ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        //         String str4 = e;
        //         local2.d(str4, "ringtones=" + c2 + ", defaultRingtone=" + ringtone);
        //         fn2.o.a().b(ringtone);
        //         return;
        //     }
        //     String component1 = ((Ringtone) new Gson().a(str2, Ringtone.class)).component1();
        //     ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        //     String str5 = e;
        //     local3.d(str5, "MicroAppAction.run - ringtone " + str2);
        //     PortfolioApp.W.c().a((DeviceAppResponse) new RingMyPhoneMicroAppResponse(), str);
        //     fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new LinkStreamingManager$startRingMyPhone$Anon1(component1, (yb4) null), 3, (Object) null);
        // } catch (Exception e2) {
        //     ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        //     local4.d("AppUtil", "Error when read asset file - ex=" + e2);
        // }
    }

    @DexIgnore
    public final fi4 a(String str, int i) {
        throw null;
        // return ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new LinkStreamingManager$processAlarmSynchronizeEvent$Anon1(this, i, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final String a(String str) {
        throw null;
        // pd4 pd4 = pd4.a;
        // Object[] objArr = {str};
        // String format = String.format("update_%s", Arrays.copyOf(objArr, objArr.length));
        // kd4.a((Object) format, "java.lang.String.format(format, *args)");
        // return format;
    }
}
