package com.portfolio.platform.manager;

import com.fossil.blesdk.device.event.SynchronizationAction;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.manager.LinkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1", f = "LinkStreamingManager.kt", l = {}, m = "invokeSuspend")
public final class LinkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $action;
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceId;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LinkStreamingManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1(LinkStreamingManager linkStreamingManager, int i, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = linkStreamingManager;
        this.$action = i;
        this.$deviceId = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        LinkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1 linkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1 = new LinkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1(this.this$Anon0, this.$action, this.$deviceId, yb4);
        linkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1.p$ = (zg4) obj;
        return linkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LinkStreamingManager$processNotificationFilterSynchronizeEvent$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            if (this.$action == SynchronizationAction.SET.ordinal()) {
                this.this$Anon0.d.a(new SetNotificationUseCase.b(this.$deviceId), (CoroutineUseCase.e) null);
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
