package com.portfolio.platform.manager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.manager.WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1 */
public final class C5970x9e4ad940 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.yb4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ android.location.Location $currentLocation;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$BooleanRef $isFromCaches;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $tempUnit$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $tracingKey$inlined;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21278p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.manager.WeatherManager this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.manager.WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1$1")
    /* renamed from: com.portfolio.platform.manager.WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1$1 */
    public static final class C59711 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super kotlin.Pair<? extends com.portfolio.platform.data.model.microapp.weather.Weather, ? extends java.lang.Boolean>>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21279p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.manager.C5970x9e4ad940 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C59711(com.portfolio.platform.manager.C5970x9e4ad940 weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.manager.C5970x9e4ad940.C59711 r0 = new com.portfolio.platform.manager.C5970x9e4ad940.C59711(this.this$0, yb4);
            r0.f21279p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.manager.C5970x9e4ad940.C59711) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21279p$;
                com.portfolio.platform.manager.C5970x9e4ad940 weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1 = this.this$0;
                com.portfolio.platform.manager.WeatherManager weatherManager = weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1.this$0;
                android.location.Location location = weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1.$currentLocation;
                if (location != null) {
                    double latitude = location.getLatitude();
                    double longitude = this.this$0.$currentLocation.getLongitude();
                    java.lang.String str = this.this$0.$tempUnit$inlined;
                    this.L$0 = zg4;
                    this.label = 1;
                    obj = weatherManager.mo39686a(latitude, longitude, str, (com.fossil.blesdk.obfuscated.yb4<? super kotlin.Pair<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>>) this);
                    if (obj == a) {
                        return a;
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.manager.WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1$2")
    /* renamed from: com.portfolio.platform.manager.WeatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1$2 */
    public static final class C59722 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.String>, java.lang.Object> {
        @DexIgnore
        public java.lang.Object L$0;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21280p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.manager.C5970x9e4ad940 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C59722(com.portfolio.platform.manager.C5970x9e4ad940 weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.manager.C5970x9e4ad940.C59722 r0 = new com.portfolio.platform.manager.C5970x9e4ad940.C59722(this.this$0, yb4);
            r0.f21280p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.manager.C5970x9e4ad940.C59722) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
            int i = this.label;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21280p$;
                com.portfolio.platform.manager.C5970x9e4ad940 weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1 = this.this$0;
                com.portfolio.platform.manager.WeatherManager weatherManager = weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1.this$0;
                android.location.Location location = weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1.$currentLocation;
                if (location != null) {
                    double latitude = location.getLatitude();
                    double longitude = this.this$0.$currentLocation.getLongitude();
                    this.L$0 = zg4;
                    this.label = 1;
                    obj = weatherManager.mo39685a(latitude, longitude, (com.fossil.blesdk.obfuscated.yb4<? super java.lang.String>) this);
                    if (obj == a) {
                        return a;
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            } else if (i == 1) {
                com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            } else {
                throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C5970x9e4ad940(android.location.Location location, kotlin.jvm.internal.Ref$BooleanRef ref$BooleanRef, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.manager.WeatherManager weatherManager, com.fossil.blesdk.obfuscated.yb4 yb42, java.lang.String str, java.lang.String str2) {
        super(2, yb4);
        this.$currentLocation = location;
        this.$isFromCaches = ref$BooleanRef;
        this.this$0 = weatherManager;
        this.$continuation$inlined = yb42;
        this.$tempUnit$inlined = str;
        this.$tracingKey$inlined = str2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.manager.C5970x9e4ad940 weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1 = new com.portfolio.platform.manager.C5970x9e4ad940(this.$currentLocation, this.$isFromCaches, yb4, this.this$0, this.$continuation$inlined, this.$tempUnit$inlined, this.$tracingKey$inlined);
        weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1.f21278p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return weatherManager$getWeatherBaseOnLocation$$inlined$withLock$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.manager.C5970x9e4ad940) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b3  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.data.model.microapp.weather.Weather a;
        com.portfolio.platform.data.model.microapp.weather.Weather weather;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        com.fossil.blesdk.obfuscated.gh4 gh4;
        com.fossil.blesdk.obfuscated.gh4 gh42;
        java.lang.Object a2 = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f21278p$;
            com.fossil.blesdk.obfuscated.zg4 zg43 = zg42;
            com.fossil.blesdk.obfuscated.gh4 a3 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg43, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.manager.C5970x9e4ad940.C59711(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            gh42 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg43, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.manager.C5970x9e4ad940.C59722(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            this.L$0 = zg42;
            this.L$1 = a3;
            this.L$2 = gh42;
            this.label = 1;
            java.lang.Object a4 = a3.mo27693a(this);
            if (a4 == a2) {
                return a2;
            }
            com.fossil.blesdk.obfuscated.gh4 gh43 = a3;
            zg4 = zg42;
            obj = a4;
            gh4 = gh43;
        } else if (i == 1) {
            gh42 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
            gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2) {
            weather = (com.portfolio.platform.data.model.microapp.weather.Weather) this.L$4;
            kotlin.Pair pair = (kotlin.Pair) this.L$3;
            com.fossil.blesdk.obfuscated.gh4 gh44 = (com.fossil.blesdk.obfuscated.gh4) this.L$2;
            com.fossil.blesdk.obfuscated.gh4 gh45 = (com.fossil.blesdk.obfuscated.gh4) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg44 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            weather.setAddress((java.lang.String) obj);
            a = this.this$0.f21276j;
            if (a != null) {
                java.util.Calendar instance = java.util.Calendar.getInstance();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "Calendar.getInstance()");
                a.setUpdatedAt(instance.getTimeInMillis());
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        kotlin.Pair pair2 = (kotlin.Pair) obj;
        this.this$0.f21276j = (com.portfolio.platform.data.model.microapp.weather.Weather) pair2.getFirst();
        this.$isFromCaches.element = ((java.lang.Boolean) pair2.getSecond()).booleanValue();
        com.portfolio.platform.data.model.microapp.weather.Weather a5 = this.this$0.f21276j;
        if (a5 != null) {
            this.L$0 = zg4;
            this.L$1 = gh4;
            this.L$2 = gh42;
            this.L$3 = pair2;
            this.L$4 = a5;
            this.label = 2;
            obj = gh42.mo27693a(this);
            if (obj == a2) {
                return a2;
            }
            weather = a5;
            weather.setAddress((java.lang.String) obj);
        }
        a = this.this$0.f21276j;
        if (a != null) {
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
