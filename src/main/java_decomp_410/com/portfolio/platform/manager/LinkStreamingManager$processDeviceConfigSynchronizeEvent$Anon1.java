package com.portfolio.platform.manager;

import com.fossil.blesdk.device.event.SynchronizationAction;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.PortfolioApp;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.manager.LinkStreamingManager$processDeviceConfigSynchronizeEvent$Anon1", f = "LinkStreamingManager.kt", l = {}, m = "invokeSuspend")
public final class LinkStreamingManager$processDeviceConfigSynchronizeEvent$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $action;
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceId;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinkStreamingManager$processDeviceConfigSynchronizeEvent$Anon1(int i, String str, yb4 yb4) {
        super(2, yb4);
        this.$action = i;
        this.$deviceId = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        LinkStreamingManager$processDeviceConfigSynchronizeEvent$Anon1 linkStreamingManager$processDeviceConfigSynchronizeEvent$Anon1 = new LinkStreamingManager$processDeviceConfigSynchronizeEvent$Anon1(this.$action, this.$deviceId, yb4);
        linkStreamingManager$processDeviceConfigSynchronizeEvent$Anon1.p$ = (zg4) obj;
        return linkStreamingManager$processDeviceConfigSynchronizeEvent$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LinkStreamingManager$processDeviceConfigSynchronizeEvent$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            if (this.$action == SynchronizationAction.SET.ordinal()) {
                PortfolioApp.W.c().o(this.$deviceId);
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
