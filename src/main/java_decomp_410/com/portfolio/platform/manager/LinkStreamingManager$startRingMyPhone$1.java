package com.portfolio.platform.manager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.manager.LinkStreamingManager$startRingMyPhone$1", mo27670f = "LinkStreamingManager.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class LinkStreamingManager$startRingMyPhone$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $ringtoneName;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21261p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinkStreamingManager$startRingMyPhone$1(java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$ringtoneName = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.manager.LinkStreamingManager$startRingMyPhone$1 linkStreamingManager$startRingMyPhone$1 = new com.portfolio.platform.manager.LinkStreamingManager$startRingMyPhone$1(this.$ringtoneName, yb4);
        linkStreamingManager$startRingMyPhone$1.f21261p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return linkStreamingManager$startRingMyPhone$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.manager.LinkStreamingManager$startRingMyPhone$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.util.List<com.portfolio.platform.data.model.Ringtone> c = com.portfolio.platform.helper.AppHelper.f21170f.mo39533c();
            if (!c.isEmpty()) {
                for (com.portfolio.platform.data.model.Ringtone next : c) {
                    if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) next.getRingtoneName(), (java.lang.Object) this.$ringtoneName)) {
                        com.fossil.blesdk.obfuscated.fn2.f14849o.mo27449a().mo27446b(next);
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
