package com.portfolio.platform.manager.login;

import android.os.Handler;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ln2;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.ud0;
import com.fossil.blesdk.obfuscated.us3;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFLoginWechatManager$loginWithWechat$Anon2 implements us3.a {
    @DexIgnore
    public /* final */ /* synthetic */ MFLoginWechatManager a;
    @DexIgnore
    public /* final */ /* synthetic */ Handler b;
    @DexIgnore
    public /* final */ /* synthetic */ ln2 c;

    @DexIgnore
    public MFLoginWechatManager$loginWithWechat$Anon2(MFLoginWechatManager mFLoginWechatManager, Handler handler, ln2 ln2) {
        this.a = mFLoginWechatManager;
        this.b = handler;
        this.c = ln2;
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, "authToken");
        this.a.a(true);
        this.b.removeCallbacksAndMessages((Object) null);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = MFLoginWechatManager.h.a();
        local.d(a2, "Wechat onAuthSuccess: " + str);
        FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.h.a(), "Wechat step 1: Login using wechat success");
        fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new MFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1(this, str, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void b() {
        this.a.a(true);
        this.b.removeCallbacksAndMessages((Object) null);
        FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.h.a(), "Wechat onAuthAppNotInstalled");
        this.c.a(600, (ud0) null, "");
    }

    @DexIgnore
    public void c() {
        this.a.a(true);
        this.b.removeCallbacksAndMessages((Object) null);
        FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.h.a(), "Wechat onAuthCancel");
        this.c.a(2, (ud0) null, "");
    }

    @DexIgnore
    public void a() {
        this.a.a(true);
        this.b.removeCallbacksAndMessages((Object) null);
        FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.h.a(), "Wechat onAuthFailed");
        this.c.a(600, (ud0) null, "");
    }
}
