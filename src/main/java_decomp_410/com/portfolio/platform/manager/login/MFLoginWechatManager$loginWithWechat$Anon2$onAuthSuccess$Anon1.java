package com.portfolio.platform.manager.login;

import android.content.Context;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.er4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.ud0;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.WechatToken;
import com.portfolio.platform.data.source.remote.WechatApiService;
import com.portfolio.platform.manager.SoLibraryLoader;
import java.net.SocketTimeoutException;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1", f = "MFLoginWechatManager.kt", l = {}, m = "invokeSuspend")
public final class MFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $authToken;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MFLoginWechatManager$loginWithWechat$Anon2 this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements er4<WechatToken> {
        @DexIgnore
        public /* final */ /* synthetic */ MFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1 e;

        @DexIgnore
        public a(MFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1 mFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1) {
            this.e = mFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1;
        }

        @DexIgnore
        public void onFailure(Call<WechatToken> call, Throwable th) {
            kd4.b(call, "call");
            kd4.b(th, "t");
            FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.h.a(), "getWechatToken onFailure");
            if (th instanceof SocketTimeoutException) {
                this.e.this$Anon0.c.a(MFNetworkReturnCode.CLIENT_TIMEOUT, (ud0) null, "");
            } else {
                this.e.this$Anon0.c.a(601, (ud0) null, "");
            }
        }

        @DexIgnore
        public void onResponse(Call<WechatToken> call, qr4<WechatToken> qr4) {
            kd4.b(call, "call");
            kd4.b(qr4, "response");
            if (qr4.d()) {
                FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.h.a(), "getWechatToken isSuccessful");
                WechatToken a = qr4.a();
                if (a != null) {
                    kd4.a((Object) a, "response.body()!!");
                    WechatToken wechatToken = a;
                    this.e.this$Anon0.a.a(wechatToken.getOpenId());
                    SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                    signUpSocialAuth.setToken(wechatToken.getAccessToken());
                    signUpSocialAuth.setService("wechat");
                    this.e.this$Anon0.c.a(signUpSocialAuth);
                    return;
                }
                kd4.a();
                throw null;
            }
            FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.h.a(), "getWechatToken isNotSuccessful");
            this.e.this$Anon0.c.a(600, (ud0) null, qr4.e());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1(MFLoginWechatManager$loginWithWechat$Anon2 mFLoginWechatManager$loginWithWechat$Anon2, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = mFLoginWechatManager$loginWithWechat$Anon2;
        this.$authToken = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        MFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1 mFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1 = new MFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1(this.this$Anon0, this.$authToken, yb4);
        mFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1.p$ = (zg4) obj;
        return mFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((MFLoginWechatManager$loginWithWechat$Anon2$onAuthSuccess$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0029, code lost:
        if (r2 != null) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0033, code lost:
        if (r5 != null) goto L_0x0037;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        String str;
        String str2;
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            Access a2 = new SoLibraryLoader().a((Context) PortfolioApp.W.c());
            WechatApiService a3 = this.this$Anon0.a.a();
            if (a2 != null) {
                str = a2.getD();
            }
            str = "";
            if (a2 != null) {
                str2 = a2.getE();
            }
            str2 = "";
            a3.getWechatToken(str, str2, this.$authToken, "authorization_code").a(new a(this));
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
