package com.portfolio.platform.manager.login;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MFLoginWechatManager$loginWithWechat$2 implements com.fossil.blesdk.obfuscated.us3.C5240a {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.portfolio.platform.manager.login.MFLoginWechatManager f21293a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ /* synthetic */ android.os.Handler f21294b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.ln2 f21295c;

    @DexIgnore
    public MFLoginWechatManager$loginWithWechat$2(com.portfolio.platform.manager.login.MFLoginWechatManager mFLoginWechatManager, android.os.Handler handler, com.fossil.blesdk.obfuscated.ln2 ln2) {
        this.f21293a = mFLoginWechatManager;
        this.f21294b = handler;
        this.f21295c = ln2;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo31653a(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "authToken");
        this.f21293a.mo39707a(true);
        this.f21294b.removeCallbacksAndMessages((java.lang.Object) null);
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String a = com.portfolio.platform.manager.login.MFLoginWechatManager.f21288h.mo39710a();
        local.mo33255d(a, "Wechat onAuthSuccess: " + str);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.manager.login.MFLoginWechatManager.f21288h.mo39710a(), "Wechat step 1: Login using wechat success");
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1(this, str, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo31654b() {
        this.f21293a.mo39707a(true);
        this.f21294b.removeCallbacksAndMessages((java.lang.Object) null);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.manager.login.MFLoginWechatManager.f21288h.mo39710a(), "Wechat onAuthAppNotInstalled");
        this.f21295c.mo29244a(600, (com.fossil.blesdk.obfuscated.ud0) null, "");
    }

    @DexIgnore
    /* renamed from: c */
    public void mo31655c() {
        this.f21293a.mo39707a(true);
        this.f21294b.removeCallbacksAndMessages((java.lang.Object) null);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.manager.login.MFLoginWechatManager.f21288h.mo39710a(), "Wechat onAuthCancel");
        this.f21295c.mo29244a(2, (com.fossil.blesdk.obfuscated.ud0) null, "");
    }

    @DexIgnore
    /* renamed from: a */
    public void mo31652a() {
        this.f21293a.mo39707a(true);
        this.f21294b.removeCallbacksAndMessages((java.lang.Object) null);
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.manager.login.MFLoginWechatManager.f21288h.mo39710a(), "Wechat onAuthFailed");
        this.f21295c.mo29244a(600, (com.fossil.blesdk.obfuscated.ud0) null, "");
    }
}
