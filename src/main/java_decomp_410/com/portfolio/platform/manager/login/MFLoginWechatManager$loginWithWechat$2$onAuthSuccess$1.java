package com.portfolio.platform.manager.login;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1", mo27670f = "MFLoginWechatManager.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $authToken;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21296p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2 this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1$a")
    /* renamed from: com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1$a */
    public static final class C5976a implements com.fossil.blesdk.obfuscated.er4<com.portfolio.platform.data.WechatToken> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1 f21297e;

        @DexIgnore
        public C5976a(com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1 mFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1) {
            this.f21297e = mFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1;
        }

        @DexIgnore
        public void onFailure(retrofit2.Call<com.portfolio.platform.data.WechatToken> call, java.lang.Throwable th) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(call, "call");
            com.fossil.blesdk.obfuscated.kd4.m24411b(th, "t");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.manager.login.MFLoginWechatManager.f21288h.mo39710a(), "getWechatToken onFailure");
            if (th instanceof java.net.SocketTimeoutException) {
                this.f21297e.this$0.f21295c.mo29244a(com.misfit.frameworks.common.constants.MFNetworkReturnCode.CLIENT_TIMEOUT, (com.fossil.blesdk.obfuscated.ud0) null, "");
            } else {
                this.f21297e.this$0.f21295c.mo29244a(601, (com.fossil.blesdk.obfuscated.ud0) null, "");
            }
        }

        @DexIgnore
        public void onResponse(retrofit2.Call<com.portfolio.platform.data.WechatToken> call, com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.WechatToken> qr4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(call, "call");
            com.fossil.blesdk.obfuscated.kd4.m24411b(qr4, "response");
            if (qr4.mo30499d()) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.manager.login.MFLoginWechatManager.f21288h.mo39710a(), "getWechatToken isSuccessful");
                com.portfolio.platform.data.WechatToken a = qr4.mo30496a();
                if (a != null) {
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "response.body()!!");
                    com.portfolio.platform.data.WechatToken wechatToken = a;
                    this.f21297e.this$0.f21293a.mo39706a(wechatToken.getOpenId());
                    com.portfolio.platform.data.SignUpSocialAuth signUpSocialAuth = new com.portfolio.platform.data.SignUpSocialAuth();
                    signUpSocialAuth.setToken(wechatToken.getAccessToken());
                    signUpSocialAuth.setService("wechat");
                    this.f21297e.this$0.f21295c.mo29245a(signUpSocialAuth);
                    return;
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.manager.login.MFLoginWechatManager.f21288h.mo39710a(), "getWechatToken isNotSuccessful");
            this.f21297e.this$0.f21295c.mo29244a(600, (com.fossil.blesdk.obfuscated.ud0) null, qr4.mo30500e());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1(com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2 mFLoginWechatManager$loginWithWechat$2, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = mFLoginWechatManager$loginWithWechat$2;
        this.$authToken = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1 mFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1 = new com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1(this.this$0, this.$authToken, yb4);
        mFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1.f21296p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return mFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0029, code lost:
        if (r2 != null) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0033, code lost:
        if (r5 != null) goto L_0x0037;
     */
    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.String str;
        java.lang.String str2;
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.Access a = new com.portfolio.platform.manager.SoLibraryLoader().mo39677a((android.content.Context) com.portfolio.platform.PortfolioApp.f20941W.mo34589c());
            com.portfolio.platform.data.source.remote.WechatApiService a2 = this.this$0.f21293a.mo39703a();
            if (a != null) {
                str = a.getD();
            }
            str = "";
            if (a != null) {
                str2 = a.getE();
            }
            str2 = "";
            a2.getWechatToken(str, str2, this.$authToken, "authorization_code").mo28073a(new com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1.C5976a(this));
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
