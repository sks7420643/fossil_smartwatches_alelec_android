package com.portfolio.platform.manager;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.manager.WeatherManager$getWeather$repoResponse$Anon1", f = "WeatherManager.kt", l = {249}, m = "invokeSuspend")
public final class WeatherManager$getWeather$repoResponse$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<xz1>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ double $lat;
    @DexIgnore
    public /* final */ /* synthetic */ double $lng;
    @DexIgnore
    public /* final */ /* synthetic */ String $tempUnit;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherManager$getWeather$repoResponse$Anon1(WeatherManager weatherManager, double d, double d2, String str, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = weatherManager;
        this.$lat = d;
        this.$lng = d2;
        this.$tempUnit = str;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new WeatherManager$getWeather$repoResponse$Anon1(this.this$Anon0, this.$lat, this.$lng, this.$tempUnit, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((WeatherManager$getWeather$repoResponse$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            ApiServiceV2 a2 = this.this$Anon0.a();
            String valueOf = String.valueOf(this.$lat);
            String valueOf2 = String.valueOf(this.$lng);
            String str = this.$tempUnit;
            this.label = 1;
            obj = a2.getWeather(valueOf, valueOf2, str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
