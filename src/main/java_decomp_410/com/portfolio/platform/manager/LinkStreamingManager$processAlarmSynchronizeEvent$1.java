package com.portfolio.platform.manager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.manager.LinkStreamingManager$processAlarmSynchronizeEvent$1", mo27670f = "LinkStreamingManager.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class LinkStreamingManager$processAlarmSynchronizeEvent$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $action;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21258p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.manager.LinkStreamingManager this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinkStreamingManager$processAlarmSynchronizeEvent$1(com.portfolio.platform.manager.LinkStreamingManager linkStreamingManager, int i, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = linkStreamingManager;
        this.$action = i;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.manager.LinkStreamingManager$processAlarmSynchronizeEvent$1 linkStreamingManager$processAlarmSynchronizeEvent$1 = new com.portfolio.platform.manager.LinkStreamingManager$processAlarmSynchronizeEvent$1(this.this$0, this.$action, yb4);
        linkStreamingManager$processAlarmSynchronizeEvent$1.f21258p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return linkStreamingManager$processAlarmSynchronizeEvent$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.manager.LinkStreamingManager$processAlarmSynchronizeEvent$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            if (this.$action == com.fossil.blesdk.device.event.SynchronizationAction.SET.ordinal()) {
                java.util.List activeAlarms = this.this$0.f21253c.getActiveAlarms();
                if (activeAlarms == null) {
                    activeAlarms = new java.util.ArrayList();
                }
                com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34508a((java.util.List<? extends com.misfit.frameworks.buttonservice.model.Alarm>) com.fossil.blesdk.obfuscated.nj2.m25699a(activeAlarms));
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
