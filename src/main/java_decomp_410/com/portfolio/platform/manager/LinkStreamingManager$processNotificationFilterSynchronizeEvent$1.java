package com.portfolio.platform.manager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.manager.LinkStreamingManager$processNotificationFilterSynchronizeEvent$1", mo27670f = "LinkStreamingManager.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class LinkStreamingManager$processNotificationFilterSynchronizeEvent$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $action;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $deviceId;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21260p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.manager.LinkStreamingManager this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinkStreamingManager$processNotificationFilterSynchronizeEvent$1(com.portfolio.platform.manager.LinkStreamingManager linkStreamingManager, int i, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = linkStreamingManager;
        this.$action = i;
        this.$deviceId = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.manager.LinkStreamingManager$processNotificationFilterSynchronizeEvent$1 linkStreamingManager$processNotificationFilterSynchronizeEvent$1 = new com.portfolio.platform.manager.LinkStreamingManager$processNotificationFilterSynchronizeEvent$1(this.this$0, this.$action, this.$deviceId, yb4);
        linkStreamingManager$processNotificationFilterSynchronizeEvent$1.f21260p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return linkStreamingManager$processNotificationFilterSynchronizeEvent$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.manager.LinkStreamingManager$processNotificationFilterSynchronizeEvent$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            if (this.$action == com.fossil.blesdk.device.event.SynchronizationAction.SET.ordinal()) {
                this.this$0.f21254d.mo34435a(new com.portfolio.platform.usecase.SetNotificationUseCase.C6914b(this.$deviceId), (com.portfolio.platform.CoroutineUseCase.C5606e) null);
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
