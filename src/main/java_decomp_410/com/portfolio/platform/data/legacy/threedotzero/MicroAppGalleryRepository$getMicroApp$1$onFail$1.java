package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepository$getMicroApp$1$onFail$1 implements com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository$getMicroApp$1 this$0;

    @DexIgnore
    public MicroAppGalleryRepository$getMicroApp$1$onFail$1(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository$getMicroApp$1 microAppGalleryRepository$getMicroApp$1) {
        this.this$0 = microAppGalleryRepository$getMicroApp$1;
    }

    @DexIgnore
    public void onFail() {
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "getMicroApp microAppId=" + this.this$0.$microAppId + " remote onFail");
        com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback = this.this$0.$callback;
        if (getMicroAppCallback != null) {
            getMicroAppCallback.onFail();
        }
    }

    @DexIgnore
    public void onSuccess(java.util.List<? extends com.portfolio.platform.data.legacy.threedotzero.MicroApp> list) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "microAppList");
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "getMicroApp microAppId=" + this.this$0.$microAppId + " remote onSuccess");
        int size = list.size();
        for (int i = 0; i < size; i++) {
            ((com.portfolio.platform.data.legacy.threedotzero.MicroApp) list.get(i)).setPlatform(this.this$0.$deviceSerial);
            if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.portfolio.platform.data.legacy.threedotzero.MicroApp) list.get(i)).getAppId(), (java.lang.Object) this.this$0.$microAppId)) {
                com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback = this.this$0.$callback;
                if (getMicroAppCallback != null) {
                    getMicroAppCallback.onSuccess((com.portfolio.platform.data.legacy.threedotzero.MicroApp) list.get(i));
                }
            }
        }
        this.this$0.this$0.mAppExecutors.mo27946a().execute(new com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository$getMicroApp$1$onFail$1$onSuccess$1(this, list));
    }
}
