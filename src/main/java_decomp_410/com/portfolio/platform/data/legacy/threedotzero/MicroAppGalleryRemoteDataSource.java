package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import com.portfolio.platform.data.source.remote.ShortcutApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRemoteDataSource extends MicroAppGalleryDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ h42 mAppExecutors;
    @DexIgnore
    public /* final */ ShortcutApiService mShortcutApiService;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppGalleryRemoteDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            kd4.b(str, "<set-?>");
            MicroAppGalleryRemoteDataSource.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppGalleryRemoteDataSource.class.getSimpleName();
        kd4.a((Object) simpleName, "MicroAppGalleryRemoteDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppGalleryRemoteDataSource(ShortcutApiService shortcutApiService, h42 h42) {
        kd4.b(shortcutApiService, "mShortcutApiService");
        kd4.b(h42, "mAppExecutors");
        this.mShortcutApiService = shortcutApiService;
        this.mAppExecutors = h42;
    }

    @DexIgnore
    public void getMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        kd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "getMicroAppGallery deviceSerial=" + str);
        this.mShortcutApiService.getMicroAppGallery(0, 100, str).a(new MicroAppGalleryRemoteDataSource$getMicroAppGallery$Anon1(str, getMicroAppGalleryCallback));
    }
}
