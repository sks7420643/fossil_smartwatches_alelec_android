package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepositoryModule_ProvideGalleryRemoteDataSource$app_fossilReleaseFactory */
public final class C5633x1383abe1 implements dagger.internal.Factory<com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource> {
    @DexIgnore
    public /* final */ javax.inject.Provider<com.fossil.blesdk.obfuscated.h42> appExecutorsProvider;
    @DexIgnore
    public /* final */ com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepositoryModule module;
    @DexIgnore
    public /* final */ javax.inject.Provider<com.portfolio.platform.data.source.remote.ShortcutApiService> shortcutApiServiceProvider;

    @DexIgnore
    public C5633x1383abe1(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule, javax.inject.Provider<com.portfolio.platform.data.source.remote.ShortcutApiService> provider, javax.inject.Provider<com.fossil.blesdk.obfuscated.h42> provider2) {
        this.module = microAppGalleryRepositoryModule;
        this.shortcutApiServiceProvider = provider;
        this.appExecutorsProvider = provider2;
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.C5633x1383abe1 create(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule, javax.inject.Provider<com.portfolio.platform.data.source.remote.ShortcutApiService> provider, javax.inject.Provider<com.fossil.blesdk.obfuscated.h42> provider2) {
        return new com.portfolio.platform.data.legacy.threedotzero.C5633x1383abe1(microAppGalleryRepositoryModule, provider, provider2);
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource provideInstance(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule, javax.inject.Provider<com.portfolio.platform.data.source.remote.ShortcutApiService> provider, javax.inject.Provider<com.fossil.blesdk.obfuscated.h42> provider2) {
        return proxyProvideGalleryRemoteDataSource$app_fossilRelease(microAppGalleryRepositoryModule, provider.get(), provider2.get());
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource proxyProvideGalleryRemoteDataSource$app_fossilRelease(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule, com.portfolio.platform.data.source.remote.ShortcutApiService shortcutApiService, com.fossil.blesdk.obfuscated.h42 h42) {
        com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource provideGalleryRemoteDataSource$app_fossilRelease = microAppGalleryRepositoryModule.provideGalleryRemoteDataSource$app_fossilRelease(shortcutApiService, h42);
        com.fossil.blesdk.obfuscated.n44.m25602a(provideGalleryRemoteDataSource$app_fossilRelease, "Cannot return null from a non-@Nullable @Provides method");
        return provideGalleryRemoteDataSource$app_fossilRelease;
    }

    @DexIgnore
    public com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource get() {
        return provideInstance(this.module, this.shortcutApiServiceProvider, this.appExecutorsProvider);
    }
}
