package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRepository$getMicroAppVariant$1$onFail$1 implements com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback {
    @DexIgnore
    public /* final */ /* synthetic */ int $errorCode;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository$getMicroAppVariant$1 this$0;

    @DexIgnore
    public MicroAppVariantRepository$getMicroAppVariant$1$onFail$1(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository$getMicroAppVariant$1 microAppVariantRepository$getMicroAppVariant$1, int i) {
        this.this$0 = microAppVariantRepository$getMicroAppVariant$1;
        this.$errorCode = i;
    }

    @DexIgnore
    public void onFail(int i) {
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "getMicroAppVariant remote serialNumber=" + this.this$0.$serialNumber + " major=" + this.this$0.$major + " minor=" + this.this$0.$minor + " onFail");
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantCallback getVariantCallback = this.this$0.$callback;
        if (getVariantCallback != null) {
            getVariantCallback.onFail(i);
        }
    }

    @DexIgnore
    public void onSuccess(java.util.List<com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant> list) {
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant microAppVariant;
        T t;
        T t2;
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "variantList");
        com.misfit.frameworks.common.log.MFLogger.m31689d(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository.Companion.getTAG(), "getMicroAppVariant remote serialNumber=" + this.this$0.$serialNumber + " major=" + this.this$0.$major + " minor=" + this.this$0.$minor + " onSuccess");
        if (com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository.WhenMappings.$EnumSwitchMapping$0[com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.this$0.$microAppId).ordinal()] != 1) {
            java.util.Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant) t2).getAppId(), (java.lang.Object) this.this$0.$microAppId)) {
                    break;
                }
            }
            microAppVariant = (com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant) t2;
        } else {
            java.util.Iterator<T> it2 = list.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t = null;
                    break;
                }
                t = it2.next();
                if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) ((com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant) t).getName(), (java.lang.Object) this.this$0.$variantName)) {
                    break;
                }
            }
            microAppVariant = (com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant) t;
        }
        if (microAppVariant != null) {
            com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantCallback getVariantCallback = this.this$0.$callback;
            if (getVariantCallback != null) {
                getVariantCallback.onSuccess(microAppVariant);
                return;
            }
            return;
        }
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantCallback getVariantCallback2 = this.this$0.$callback;
        if (getVariantCallback2 != null) {
            getVariantCallback2.onFail(this.$errorCode);
        }
    }
}
