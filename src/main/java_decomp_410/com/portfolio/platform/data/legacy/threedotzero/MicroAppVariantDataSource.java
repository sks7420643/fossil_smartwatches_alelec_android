package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wo2;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class MicroAppVariantDataSource {

    @DexIgnore
    public interface AddOrUpdateDeclarationFileCallback {
        @DexIgnore
        void onFail();

        @DexIgnore
        void onSuccess(DeclarationFile declarationFile);
    }

    @DexIgnore
    public interface GetVariantCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(MicroAppVariant microAppVariant);
    }

    @DexIgnore
    public interface GetVariantListCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(List<MicroAppVariant> list);
    }

    @DexIgnore
    public interface GetVariantListRemoteCallback extends GetVariantListCallback {
        @DexIgnore
        void onSuccess(ArrayList<wo2> arrayList);
    }

    @DexIgnore
    public interface MigrateVariantCallback {
        @DexIgnore
        void onDone();
    }

    @DexIgnore
    public void addOrUpdateDeclarationFile(DeclarationFile declarationFile, AddOrUpdateDeclarationFileCallback addOrUpdateDeclarationFileCallback) {
        kd4.b(declarationFile, "declarationFile");
        kd4.b(addOrUpdateDeclarationFileCallback, Constants.CALLBACK);
    }

    @DexIgnore
    public abstract void getAllMicroAppVariants(String str, int i, int i2, GetVariantListCallback getVariantListCallback);

    @DexIgnore
    public void getMicroAppVariant(String str, String str2, String str3, int i, int i2, GetVariantCallback getVariantCallback) {
        kd4.b(str, "serialNumber");
        kd4.b(str2, "microAppId");
        kd4.b(str3, "variantName");
    }

    @DexIgnore
    public void removeMicroAppVariants(String str, int i, int i2) {
        kd4.b(str, "serialNumber");
    }
}
