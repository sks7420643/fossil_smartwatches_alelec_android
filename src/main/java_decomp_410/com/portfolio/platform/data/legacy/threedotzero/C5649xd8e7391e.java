package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepositoryModule_ProvideMicroAppVariantRemoteDataSource$app_fossilReleaseFactory */
public final class C5649xd8e7391e implements dagger.internal.Factory<com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource> {
    @DexIgnore
    public /* final */ javax.inject.Provider<com.fossil.blesdk.obfuscated.h42> appExecutorsProvider;
    @DexIgnore
    public /* final */ com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepositoryModule module;
    @DexIgnore
    public /* final */ javax.inject.Provider<com.portfolio.platform.data.source.remote.ShortcutApiService> shortcutApiServiceProvider;

    @DexIgnore
    public C5649xd8e7391e(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepositoryModule microAppVariantRepositoryModule, javax.inject.Provider<com.portfolio.platform.data.source.remote.ShortcutApiService> provider, javax.inject.Provider<com.fossil.blesdk.obfuscated.h42> provider2) {
        this.module = microAppVariantRepositoryModule;
        this.shortcutApiServiceProvider = provider;
        this.appExecutorsProvider = provider2;
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.C5649xd8e7391e create(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepositoryModule microAppVariantRepositoryModule, javax.inject.Provider<com.portfolio.platform.data.source.remote.ShortcutApiService> provider, javax.inject.Provider<com.fossil.blesdk.obfuscated.h42> provider2) {
        return new com.portfolio.platform.data.legacy.threedotzero.C5649xd8e7391e(microAppVariantRepositoryModule, provider, provider2);
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource provideInstance(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepositoryModule microAppVariantRepositoryModule, javax.inject.Provider<com.portfolio.platform.data.source.remote.ShortcutApiService> provider, javax.inject.Provider<com.fossil.blesdk.obfuscated.h42> provider2) {
        return proxyProvideMicroAppVariantRemoteDataSource$app_fossilRelease(microAppVariantRepositoryModule, provider.get(), provider2.get());
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource proxyProvideMicroAppVariantRemoteDataSource$app_fossilRelease(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepositoryModule microAppVariantRepositoryModule, com.portfolio.platform.data.source.remote.ShortcutApiService shortcutApiService, com.fossil.blesdk.obfuscated.h42 h42) {
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource provideMicroAppVariantRemoteDataSource$app_fossilRelease = microAppVariantRepositoryModule.provideMicroAppVariantRemoteDataSource$app_fossilRelease(shortcutApiService, h42);
        com.fossil.blesdk.obfuscated.n44.m25602a(provideMicroAppVariantRemoteDataSource$app_fossilRelease, "Cannot return null from a non-@Nullable @Provides method");
        return provideMicroAppVariantRemoteDataSource$app_fossilRelease;
    }

    @DexIgnore
    public com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource get() {
        return provideInstance(this.module, this.shortcutApiServiceProvider, this.appExecutorsProvider);
    }
}
