package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepositoryModule {
    @DexIgnore
    @Local
    public final MicroAppGalleryDataSource provideGalleryLocalDataSource$app_fossilRelease() {
        return new MicroAppGalleryLocalDataSource();
    }

    @DexIgnore
    @Remote
    public final MicroAppGalleryDataSource provideGalleryRemoteDataSource$app_fossilRelease(ShortcutApiService shortcutApiService, h42 h42) {
        kd4.b(shortcutApiService, "shortcutApiService");
        kd4.b(h42, "appExecutors");
        return new MicroAppGalleryRemoteDataSource(shortcutApiService, h42);
    }
}
