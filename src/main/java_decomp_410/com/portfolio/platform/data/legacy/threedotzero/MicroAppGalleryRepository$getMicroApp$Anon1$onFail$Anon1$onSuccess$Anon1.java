package com.portfolio.platform.data.legacy.threedotzero;

import com.misfit.frameworks.common.log.MFLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1$onSuccess$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ List $microAppList;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1 this$Anon0;

    @DexIgnore
    public MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1$onSuccess$Anon1(MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1 microAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1, List list) {
        this.this$Anon0 = microAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1;
        this.$microAppList = list;
    }

    @DexIgnore
    public final void run() {
        MFLogger.d(MicroAppGalleryRepository.Companion.getTAG(), "diskIO enter getMicroApp");
        this.this$Anon0.this$Anon0.this$Anon0.mMicroAppSettingLocalDataSource.updateListMicroApp(this.$microAppList);
        MFLogger.d(MicroAppGalleryRepository.Companion.getTAG(), "diskIO exit getMicroApp");
    }
}
