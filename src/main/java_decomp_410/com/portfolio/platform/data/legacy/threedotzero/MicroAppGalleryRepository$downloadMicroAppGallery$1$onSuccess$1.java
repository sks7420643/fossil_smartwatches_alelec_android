package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepository$downloadMicroAppGallery$1$onSuccess$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.ArrayList $listSupportedMicroApp;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository$downloadMicroAppGallery$1 this$0;

    @DexIgnore
    public MicroAppGalleryRepository$downloadMicroAppGallery$1$onSuccess$1(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository$downloadMicroAppGallery$1 microAppGalleryRepository$downloadMicroAppGallery$1, java.util.ArrayList arrayList) {
        this.this$0 = microAppGalleryRepository$downloadMicroAppGallery$1;
        this.$listSupportedMicroApp = arrayList;
    }

    @DexIgnore
    public final void run() {
        com.misfit.frameworks.common.log.MFLogger.m31689d(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG(), "diskIO enter onSuccess downloadMicroAppGallery");
        this.this$0.this$0.mMicroAppSettingLocalDataSource.deleteListMicroApp(this.this$0.$deviceSerial);
        this.this$0.this$0.mMicroAppSettingLocalDataSource.updateListMicroApp(this.$listSupportedMicroApp);
        com.misfit.frameworks.common.log.MFLogger.m31689d(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG(), "diskIO exit onSuccess downloadMicroAppGallery");
    }
}
