package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.er4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.List;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRemoteDataSource$getMicroAppGallery$Anon1 implements er4<ApiResponse<MicroApp>> {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppGalleryDataSource.GetMicroAppGalleryCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceSerial;

    @DexIgnore
    public MicroAppGalleryRemoteDataSource$getMicroAppGallery$Anon1(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        this.$deviceSerial = str;
        this.$callback = getMicroAppGalleryCallback;
    }

    @DexIgnore
    public void onFailure(Call<ApiResponse<MicroApp>> call, Throwable th) {
        kd4.b(call, "call");
        kd4.b(th, "throwable");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = MicroAppGalleryRemoteDataSource.Companion.getTAG();
        StringBuilder sb = new StringBuilder();
        sb.append("getMicroAppGallery deviceSerial=");
        sb.append(this.$deviceSerial);
        sb.append(" onFailure ex=");
        th.printStackTrace();
        sb.append(qa4.a);
        local.d(tag, sb.toString());
        MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onFail();
        }
    }

    @DexIgnore
    public void onResponse(Call<ApiResponse<MicroApp>> call, qr4<ApiResponse<MicroApp>> qr4) {
        kd4.b(call, "call");
        kd4.b(qr4, "response");
        if (qr4.d()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = MicroAppGalleryRemoteDataSource.Companion.getTAG();
            local.d(tag, "getMicroAppGallery deviceSerial=" + this.$deviceSerial + " onSuccess response=" + qr4.a());
            ApiResponse a = qr4.a();
            List list = a != null ? a.get_items() : null;
            if (list == null || !(!list.isEmpty())) {
                MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
                if (getMicroAppGalleryCallback != null) {
                    getMicroAppGalleryCallback.onFail();
                    return;
                }
                return;
            }
            MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback2 = this.$callback;
            if (getMicroAppGalleryCallback2 != null) {
                getMicroAppGalleryCallback2.onSuccess(list);
                return;
            }
            return;
        }
        MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback3 = this.$callback;
        if (getMicroAppGalleryCallback3 != null) {
            getMicroAppGalleryCallback3.onFail();
        }
    }
}
