package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRemoteDataSource$getAllMicroAppVariants$resultCallback$1 implements com.fossil.blesdk.obfuscated.er4<com.fossil.blesdk.obfuscated.xz1> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ int $major;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.ArrayList $microAppVariants;
    @DexIgnore
    public /* final */ /* synthetic */ int $minor;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRemoteDataSource this$0;

    @DexIgnore
    public MicroAppVariantRemoteDataSource$getAllMicroAppVariants$resultCallback$1(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRemoteDataSource microAppVariantRemoteDataSource, java.lang.String str, java.util.ArrayList arrayList, int i, int i2, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        this.this$0 = microAppVariantRemoteDataSource;
        this.$serialNumber = str;
        this.$microAppVariants = arrayList;
        this.$major = i;
        this.$minor = i2;
        this.$callback = getVariantListCallback;
    }

    @DexIgnore
    public void onFailure(retrofit2.Call<com.fossil.blesdk.obfuscated.xz1> call, java.lang.Throwable th) {
        com.fossil.blesdk.obfuscated.kd4.b(call, "call");
        com.fossil.blesdk.obfuscated.kd4.b(th, "throwable");
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRemoteDataSource.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.d(tag, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onFailure");
        if (!this.$microAppVariants.isEmpty()) {
            java.lang.String tag2 = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRemoteDataSource.Companion.getTAG();
            com.misfit.frameworks.common.log.MFLogger.d(tag2, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onFailure microAppVariants not null");
            com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback = this.$callback;
            if (getVariantListCallback != null) {
                ((com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback) getVariantListCallback).onSuccess(this.$microAppVariants);
                return;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback");
        }
        java.lang.String tag3 = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRemoteDataSource.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.d(tag3, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onFailure microAppVariants is null");
        if (th instanceof java.net.SocketTimeoutException) {
            com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback2 = this.$callback;
            if (getVariantListCallback2 != null) {
                getVariantListCallback2.onFail(com.misfit.frameworks.common.constants.MFNetworkReturnCode.CLIENT_TIMEOUT);
                return;
            }
            return;
        }
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback3 = this.$callback;
        if (getVariantListCallback3 != null) {
            getVariantListCallback3.onFail(601);
        }
    }

    @DexIgnore
    public void onResponse(retrofit2.Call<com.fossil.blesdk.obfuscated.xz1> call, com.fossil.blesdk.obfuscated.qr4<com.fossil.blesdk.obfuscated.xz1> qr4) {
        com.fossil.blesdk.obfuscated.kd4.b(call, "call");
        com.fossil.blesdk.obfuscated.kd4.b(qr4, "response");
        if (qr4.d()) {
            com.fossil.blesdk.obfuscated.xz1 a = qr4.a();
            java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRemoteDataSource.Companion.getTAG();
            com.misfit.frameworks.common.log.MFLogger.d(tag, "getMicroAppGallery deviceSerial=" + this.$serialNumber + " onSuccess response=" + a);
            com.fossil.blesdk.obfuscated.uo2 uo2 = new com.fossil.blesdk.obfuscated.uo2();
            uo2.a(a);
            this.$microAppVariants.addAll(uo2.a());
            com.portfolio.platform.data.model.Range b = uo2.b();
            if (b == null || !b.isHasNext()) {
                java.lang.String tag2 = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRemoteDataSource.Companion.getTAG();
                com.misfit.frameworks.common.log.MFLogger.d(tag2, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onSuccess hasNext=false");
                if (this.$microAppVariants.isEmpty()) {
                    com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback = this.$callback;
                    if (getVariantListCallback != null) {
                        getVariantListCallback.onFail(qr4.b());
                        return;
                    }
                    return;
                }
                com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback2 = this.$callback;
                if (getVariantListCallback2 != null) {
                    ((com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback) getVariantListCallback2).onSuccess(this.$microAppVariants);
                    return;
                }
                throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback");
            }
            java.lang.String tag3 = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRemoteDataSource.Companion.getTAG();
            com.misfit.frameworks.common.log.MFLogger.d(tag3, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " onSuccess hasNext=true");
            this.this$0.getAllMicroAppVariants$app_fossilRelease(this.$serialNumber, this.$major, this.$minor, b.getOffset() + b.getLimit(), b.getLimit(), this);
            return;
        }
        java.lang.String tag4 = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRemoteDataSource.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.d(tag4, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " !isSuccessful");
        if (!this.$microAppVariants.isEmpty()) {
            java.lang.String tag5 = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRemoteDataSource.Companion.getTAG();
            com.misfit.frameworks.common.log.MFLogger.d(tag5, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " !isSuccessful microAppVariants not null");
            com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback3 = this.$callback;
            if (getVariantListCallback3 != null) {
                ((com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback) getVariantListCallback3).onSuccess(this.$microAppVariants);
                return;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback");
        }
        java.lang.String tag6 = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRemoteDataSource.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.d(tag6, "getAllMicroAppVariants deviceSerial=" + this.$serialNumber + " !isSuccessful microAppVariants is null");
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback4 = this.$callback;
        if (getVariantListCallback4 != null) {
            getVariantListCallback4.onFail(com.misfit.frameworks.common.constants.MFNetworkReturnCode.NOT_FOUND);
        }
    }
}
