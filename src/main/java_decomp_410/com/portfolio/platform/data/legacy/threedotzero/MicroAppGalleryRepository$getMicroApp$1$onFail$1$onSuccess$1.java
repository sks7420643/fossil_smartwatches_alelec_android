package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepository$getMicroApp$1$onFail$1$onSuccess$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $microAppList;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository$getMicroApp$1$onFail$1 this$0;

    @DexIgnore
    public MicroAppGalleryRepository$getMicroApp$1$onFail$1$onSuccess$1(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository$getMicroApp$1$onFail$1 microAppGalleryRepository$getMicroApp$1$onFail$1, java.util.List list) {
        this.this$0 = microAppGalleryRepository$getMicroApp$1$onFail$1;
        this.$microAppList = list;
    }

    @DexIgnore
    public final void run() {
        com.misfit.frameworks.common.log.MFLogger.m31689d(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG(), "diskIO enter getMicroApp");
        this.this$0.this$0.this$0.mMicroAppSettingLocalDataSource.updateListMicroApp(this.$microAppList);
        com.misfit.frameworks.common.log.MFLogger.m31689d(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG(), "diskIO exit getMicroApp");
    }
}
