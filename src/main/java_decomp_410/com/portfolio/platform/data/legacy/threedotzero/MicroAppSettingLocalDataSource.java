package com.portfolio.platform.data.legacy.threedotzero;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.wn2;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MicroAppSettingLocalDataSource implements MicroAppSettingDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "MicroAppSettingLocalDataSource";

    @DexIgnore
    public void addOrUpdateMicroAppSetting(MicroAppSetting microAppSetting, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateMicroAppSetting microAppSetting=" + microAppSetting.getSetting() + "microAppId=" + microAppSetting.getMicroAppId());
        wn2 h = dn2.p.a().h();
        MicroAppSetting c = h.c(microAppSetting.getMicroAppId());
        if (c != null && c.getFirstUsed() > 0) {
            microAppSetting.setFirstUsed(c.getFirstUsed());
        }
        microAppSetting.setUpdatedAt(System.currentTimeMillis());
        if (h.a(microAppSetting)) {
            String str2 = TAG;
            MFLogger.d(str2, "addOrUpdateMicroAppSetting onSuccess microAppSetting=" + microAppSetting.getSetting());
            microAppSettingCallback.onSuccess(microAppSetting);
            return;
        }
        String str3 = TAG;
        MFLogger.d(str3, "addOrUpdateMicroAppSetting onFail microAppSetting=" + microAppSetting.getSetting());
        microAppSettingCallback.onFail();
    }

    @DexIgnore
    public void clearData() {
        dn2.p.a().h().c();
    }

    @DexIgnore
    public void getMicroAppSetting(String str, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getMicroAppSetting getMicroAppId=" + str);
        if (TextUtils.isEmpty(str)) {
            String str3 = TAG;
            MFLogger.d(str3, "getMicroAppSetting onFail getMicroAppId=" + str);
            microAppSettingCallback.onFail();
            return;
        }
        MicroAppSetting c = dn2.p.a().h().c(str);
        if (c != null) {
            String str4 = TAG;
            MFLogger.d(str4, "getMicroAppSetting onSuccess getMicroAppId=" + str);
            microAppSettingCallback.onSuccess(c);
            return;
        }
        String str5 = TAG;
        MFLogger.d(str5, "getMicroAppSetting onFail getMicroAppId=" + str);
        microAppSettingCallback.onFail();
    }

    @DexIgnore
    public void getMicroAppSettingList(MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback) {
        MFLogger.d(TAG, "getMicroAppSettingList");
        List<MicroAppSetting> d = dn2.p.a().h().d();
        if (d == null || d.isEmpty()) {
            MFLogger.d(TAG, "getMicroAppSettingList onFail");
            microAppSettingListCallback.onFail();
            return;
        }
        String str = TAG;
        MFLogger.d(str, "getMicroAppSettingList onSuccess microAppSettingListSize=" + d.size());
        microAppSettingListCallback.onSuccess(d);
    }

    @DexIgnore
    public List<MicroAppSetting> getPendingMicroAppSettings() {
        return dn2.p.a().h().getPendingMicroAppSettings();
    }

    @DexIgnore
    public void mergeMicroAppSetting(MicroAppSetting microAppSetting, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
        String str = TAG;
        MFLogger.d(str, "mergeMicroAppSetting microAppSetting=" + microAppSetting.getSetting());
        wn2 h = dn2.p.a().h();
        MicroAppSetting c = h.c(microAppSetting.getMicroAppId());
        if (c != null) {
            if (c.getUpdatedAt() > microAppSetting.getUpdatedAt()) {
                microAppSetting.setCreatedAt(c.getCreatedAt());
                microAppSetting.setSetting(c.getSetting());
                microAppSetting.setLike(c.isLike());
            }
            if (c.getFirstUsed() > 0) {
                microAppSetting.setFirstUsed(c.getFirstUsed());
            }
        }
        microAppSetting.setUpdatedAt(System.currentTimeMillis());
        if (h.a(microAppSetting)) {
            String str2 = TAG;
            MFLogger.d(str2, "mergeMicroAppSetting onSuccess microAppSetting=" + microAppSetting.getSetting());
            microAppSettingCallback.onSuccess(microAppSetting);
            return;
        }
        String str3 = TAG;
        MFLogger.d(str3, "mergeMicroAppSetting onFail microAppSetting=" + microAppSetting.getSetting());
        microAppSettingCallback.onFail();
    }

    @DexIgnore
    public void updateMicroAppSettingPinType(String str, int i) {
        String str2 = TAG;
        MFLogger.d(str2, "updateMicroAppSettingPinType microAppId=" + str + ", pinType=" + i);
        dn2.p.a().h().a(str, i);
    }
}
