package com.portfolio.platform.data.legacy.threedotzero;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.st1;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class PresetLocalDataSource extends PresetDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "PresetLocalDataSource";

    @DexIgnore
    private SavedPreset findLocalPreset(String str, List<SavedPreset> list) {
        for (SavedPreset next : list) {
            if (next.getId().equalsIgnoreCase(str)) {
                return next;
            }
        }
        return null;
    }

    @DexIgnore
    public void addOrUpdateActivePreset(ActivePreset activePreset, PresetDataSource.AddOrUpdateActivePresetCallback addOrUpdateActivePresetCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateActivePreset serial=" + activePreset.getSerialNumber());
        if (dn2.p.a().d().addOrUpdateActivePreset(activePreset)) {
            MFLogger.d(TAG, "addOrUpdateActivePreset onSuccess");
            if (addOrUpdateActivePresetCallback != null) {
                addOrUpdateActivePresetCallback.onSuccess(activePreset);
                return;
            }
            return;
        }
        MFLogger.d(TAG, "addOrUpdateActivePreset onFail");
        if (addOrUpdateActivePresetCallback != null) {
            addOrUpdateActivePresetCallback.onFail();
        }
    }

    @DexIgnore
    public boolean addOrUpdateDefaultPreset(RecommendedPreset recommendedPreset) {
        if (recommendedPreset == null) {
            return false;
        }
        String str = TAG;
        MFLogger.d(str, ".addOrUpdateDefaultPreset - presetListId=" + recommendedPreset.getId() + ", serial=" + recommendedPreset.getSerialNumber());
        DeviceProvider d = dn2.p.a().d();
        RecommendedPreset defaultPreset = d.getDefaultPreset(recommendedPreset.getSerialNumber());
        if (defaultPreset != null) {
            recommendedPreset.setId(defaultPreset.getId());
        }
        return d.addOrUpdateRecommendedPreset(recommendedPreset);
    }

    @DexIgnore
    public boolean addOrUpdateRecommendedPresets(RecommendedPreset recommendedPreset) {
        return dn2.p.a().d().addOrUpdateRecommendedPreset(recommendedPreset);
    }

    @DexIgnore
    public void addOrUpdateSavedPreset(SavedPreset savedPreset, PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback) {
        st1.a(savedPreset);
        SavedPreset savedPreset2 = savedPreset;
        String str = TAG;
        MFLogger.d(str, "addOrUpdateSavedPreset presetId=" + savedPreset2.getId() + " presetName=" + savedPreset2.getName() + ", pinType=" + savedPreset2.getPinType());
        SavedPreset savedPresetById = dn2.p.a().d().getSavedPresetById(savedPreset2.getId());
        if (savedPresetById != null && savedPresetById.getPinType() == 3) {
            MFLogger.d(TAG, "addOrUpdateSavedPreset onFail, this saved preset is deleted, cannot add or edit.");
            if (addOrUpdateSavedPresetCallback != null) {
                addOrUpdateSavedPresetCallback.onFail();
            }
        } else if (dn2.p.a().d().addOrUpdateSavedPreset(savedPreset2)) {
            MFLogger.d(TAG, "addOrUpdateSavedPreset onSuccess");
            if (addOrUpdateSavedPresetCallback != null) {
                addOrUpdateSavedPresetCallback.onSuccess(savedPreset2);
            }
        } else {
            MFLogger.d(TAG, "addOrUpdateSavedPreset onFail");
            if (addOrUpdateSavedPresetCallback != null) {
                addOrUpdateSavedPresetCallback.onFail();
            }
        }
    }

    @DexIgnore
    public void addOrUpdateSavedPresetList(List<SavedPreset> list, PresetDataSource.AddOrUpdateSavedPresetListCallback addOrUpdateSavedPresetListCallback) {
        MFLogger.d(TAG, "addOrUpdateSavedPresets");
        if (list == null || list.isEmpty()) {
            MFLogger.d(TAG, "addOrUpdateSavedPresetList saved preset list is empty");
            if (addOrUpdateSavedPresetListCallback != null) {
                addOrUpdateSavedPresetListCallback.onFail();
                return;
            }
            return;
        }
        String str = TAG;
        MFLogger.d(str, ".addOrUpdateSavedPresets - mappingSetListSize=" + list.size());
        DeviceProvider d = dn2.p.a().d();
        d.getAllSavedPresets();
        ArrayList arrayList = new ArrayList();
        for (SavedPreset next : list) {
            SavedPreset findLocalPreset = findLocalPreset(next.getId(), arrayList);
            if ((findLocalPreset == null || findLocalPreset.getPinType() != 3) && d.addOrUpdateSavedPreset(next)) {
                arrayList.add(next);
            }
        }
        if (addOrUpdateSavedPresetListCallback != null) {
            addOrUpdateSavedPresetListCallback.onSuccess(arrayList);
        }
    }

    @DexIgnore
    public void clearData() {
        DeviceProvider d = dn2.p.a().d();
        d.clearActivePreset();
        d.clearRecommendedPreset();
        d.clearSavedPreset();
    }

    @DexIgnore
    public void deleteAllRecommendedPresets(String str, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "deleteRecommendedPreset serial=" + str);
        if (dn2.p.a().d().deleteRecommendedPreset(str)) {
            MFLogger.d(TAG, "deleteRecommendedPreset onSuccess");
            if (deleteMappingSetCallback != null) {
                deleteMappingSetCallback.onSuccess();
                return;
            }
            return;
        }
        MFLogger.d(TAG, "deleteRecommendedPreset onFail");
        if (deleteMappingSetCallback != null) {
            deleteMappingSetCallback.onFail();
        }
    }

    @DexIgnore
    public void deleteSavedPreset(SavedPreset savedPreset, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        String str = TAG;
        MFLogger.d(str, "deleteSavedPreset mappingSetId=" + savedPreset.getId());
        if (dn2.p.a().d().deleteSavedPreset(savedPreset.getId())) {
            MFLogger.d(TAG, "deleteSavedPreset onSuccess");
            if (deleteMappingSetCallback != null) {
                deleteMappingSetCallback.onSuccess();
                return;
            }
            return;
        }
        MFLogger.d(TAG, "deleteSavedPreset onFail");
        if (deleteMappingSetCallback != null) {
            deleteMappingSetCallback.onFail();
        }
    }

    @DexIgnore
    public void deleteSavedPresetList(List<SavedPreset> list, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        String str = TAG;
        MFLogger.d(str, "deleteSavedPresetList mappingSetId=" + list.size());
        for (SavedPreset id : list) {
            dn2.p.a().d().deleteSavedPreset(id.getId());
        }
        MFLogger.d(TAG, "deleteSavedPresetList onSuccess");
        if (deleteMappingSetCallback != null) {
            deleteMappingSetCallback.onSuccess();
        }
    }

    @DexIgnore
    public void downloadActivePresetList(PresetDataSource.GetActivePresetListCallback getActivePresetListCallback) {
    }

    @DexIgnore
    public void getActivePreset(String str, PresetDataSource.GetActivePresetCallback getActivePresetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getActivePreset serial=" + str);
        ActivePreset activePreset = dn2.p.a().d().getActivePreset(str);
        if (activePreset != null) {
            String str3 = TAG;
            MFLogger.d(str3, "getActivePreset onSuccess - activePreset: " + activePreset.getButtons());
            if (getActivePresetCallback != null) {
                getActivePresetCallback.onSuccess(activePreset);
                return;
            }
            return;
        }
        MFLogger.d(TAG, "getActivePreset onFail");
        if (getActivePresetCallback != null) {
            getActivePresetCallback.onFail();
        }
    }

    @DexIgnore
    public List<ActivePreset> getAllPendingActivePresets() {
        return dn2.p.a().d().getPendingActivePresets();
    }

    @DexIgnore
    public void getDefaultPreset(String str, PresetDataSource.GetRecommendedPresetCallback getRecommendedPresetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getDefaultPreset deviceSerial=" + str);
        RecommendedPreset defaultPreset = dn2.p.a().d().getDefaultPreset(str);
        if (defaultPreset == null) {
            String str3 = TAG;
            MFLogger.d(str3, "getDefaultPreset deviceSerial=" + str + " onFail");
            if (getRecommendedPresetCallback != null) {
                getRecommendedPresetCallback.onFail();
                return;
            }
            return;
        }
        String str4 = TAG;
        MFLogger.d(str4, "getDefaultPreset deviceSerial=" + str + " onSuccess");
        if (getRecommendedPresetCallback != null) {
            getRecommendedPresetCallback.onSuccess(defaultPreset);
        }
    }

    @DexIgnore
    public List<SavedPreset> getPendingDeletedUserPresets() {
        return dn2.p.a().d().getPendingDeletedUserPresets();
    }

    @DexIgnore
    public List<SavedPreset> getPendingUserPresets() {
        return dn2.p.a().d().getPendingUserPresets();
    }

    @DexIgnore
    public void getRecommendedPresets(String str, PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getRecommendedPresets deviceSerial=" + str);
        if (!TextUtils.isEmpty(str)) {
            List<RecommendedPreset> listRecommendedPresetBySerial = dn2.p.a().d().getListRecommendedPresetBySerial(str);
            if (listRecommendedPresetBySerial == null || listRecommendedPresetBySerial.isEmpty()) {
                String str3 = TAG;
                MFLogger.d(str3, "getRecommendedPresets deviceSerial=" + str + " onFail");
                if (getRecommendedPresetListCallback != null) {
                    getRecommendedPresetListCallback.onFail();
                    return;
                }
                return;
            }
            String str4 = TAG;
            MFLogger.d(str4, "getRecommendedPresets deviceSerial=" + str + " onSuccess");
            if (getRecommendedPresetListCallback != null) {
                getRecommendedPresetListCallback.onSuccess(listRecommendedPresetBySerial);
            }
        } else if (getRecommendedPresetListCallback != null) {
            getRecommendedPresetListCallback.onFail();
        }
    }

    @DexIgnore
    public void getSavedPreset(String str, PresetDataSource.GetSavedPresetCallback getSavedPresetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getSavedPreset presetId=" + str);
        SavedPreset savedPresetById = dn2.p.a().d().getSavedPresetById(str);
        if (savedPresetById != null) {
            MFLogger.d(TAG, "getSavedPreset onSuccess");
            if (getSavedPresetCallback != null) {
                getSavedPresetCallback.onSuccess(savedPresetById);
                return;
            }
            return;
        }
        MFLogger.d(TAG, "getSavedPreset onFail");
        if (getSavedPresetCallback != null) {
            getSavedPresetCallback.onFail();
        }
    }

    @DexIgnore
    public void getSavedPresetList(PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
        MFLogger.d(TAG, "getSavedPresetList");
        List<SavedPreset> allSavedPresetsExcludeDeleteType = dn2.p.a().d().getAllSavedPresetsExcludeDeleteType();
        if (allSavedPresetsExcludeDeleteType == null || allSavedPresetsExcludeDeleteType.isEmpty()) {
            MFLogger.d(TAG, "getSavedPresetList onFail");
            if (getSavedPresetListCallback != null) {
                getSavedPresetListCallback.onFail();
                return;
            }
            return;
        }
        MFLogger.d(TAG, "getSavedPresetList onSuccess");
        if (getSavedPresetListCallback != null) {
            getSavedPresetListCallback.onSuccess(allSavedPresetsExcludeDeleteType);
        }
    }

    @DexIgnore
    public void updateActivePresetPinType(String str, int i) {
        DeviceProvider d = dn2.p.a().d();
        ActivePreset activePreset = d.getActivePreset(str);
        if (activePreset != null) {
            activePreset.setPinType(i);
            d.addOrUpdateActivePreset(activePreset);
        }
    }

    @DexIgnore
    public void updateSavedPresetPinType(String str, int i) {
        String str2 = TAG;
        MFLogger.d(str2, "updateSavedPresetPinType presetId=" + str);
        DeviceProvider d = dn2.p.a().d();
        SavedPreset savedPresetById = d.getSavedPresetById(str);
        String str3 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Is save preset in db yet=");
        sb.append(savedPresetById != null);
        MFLogger.d(str3, sb.toString());
        if (savedPresetById != null) {
            savedPresetById.setPinType(i);
            d.addOrUpdateSavedPreset(savedPresetById);
        }
    }
}
