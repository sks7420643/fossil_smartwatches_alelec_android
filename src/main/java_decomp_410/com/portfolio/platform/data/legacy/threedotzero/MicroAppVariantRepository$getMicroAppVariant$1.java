package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRepository$getMicroAppVariant$1 implements com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantCallback {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ int $major;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $microAppId;
    @DexIgnore
    public /* final */ /* synthetic */ int $minor;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $variantName;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository this$0;

    @DexIgnore
    public MicroAppVariantRepository$getMicroAppVariant$1(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository microAppVariantRepository, java.lang.String str, int i, int i2, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantCallback getVariantCallback, java.lang.String str2, java.lang.String str3) {
        this.this$0 = microAppVariantRepository;
        this.$serialNumber = str;
        this.$major = i;
        this.$minor = i2;
        this.$callback = getVariantCallback;
        this.$microAppId = str2;
        this.$variantName = str3;
    }

    @DexIgnore
    public void onFail(int i) {
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "getMicroAppVariant local serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onFail");
        this.this$0.downloadAllVariants(this.$serialNumber, this.$major, this.$minor, new com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository$getMicroAppVariant$1$onFail$1(this, i));
    }

    @DexIgnore
    public void onSuccess(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant microAppVariant) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(microAppVariant, "microAppVariant");
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "getMicroAppVariant local serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onSuccess");
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantCallback getVariantCallback = this.$callback;
        if (getVariantCallback != null) {
            getVariantCallback.onSuccess(microAppVariant);
        }
    }
}
