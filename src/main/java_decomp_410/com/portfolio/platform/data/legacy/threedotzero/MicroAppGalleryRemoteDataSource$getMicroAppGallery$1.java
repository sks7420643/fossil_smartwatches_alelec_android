package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRemoteDataSource$getMicroAppGallery$1 implements com.fossil.blesdk.obfuscated.er4<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.legacy.threedotzero.MicroApp>> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $deviceSerial;

    @DexIgnore
    public MicroAppGalleryRemoteDataSource$getMicroAppGallery$1(java.lang.String str, com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        this.$deviceSerial = str;
        this.$callback = getMicroAppGalleryCallback;
    }

    @DexIgnore
    public void onFailure(retrofit2.Call<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.legacy.threedotzero.MicroApp>> call, java.lang.Throwable th) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(call, "call");
        com.fossil.blesdk.obfuscated.kd4.m24411b(th, "throwable");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRemoteDataSource.Companion.getTAG();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("getMicroAppGallery deviceSerial=");
        sb.append(this.$deviceSerial);
        sb.append(" onFailure ex=");
        th.printStackTrace();
        sb.append(com.fossil.blesdk.obfuscated.qa4.f17909a);
        local.mo33255d(tag, sb.toString());
        com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onFail();
        }
    }

    @DexIgnore
    public void onResponse(retrofit2.Call<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.legacy.threedotzero.MicroApp>> call, com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.legacy.threedotzero.MicroApp>> qr4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(call, "call");
        com.fossil.blesdk.obfuscated.kd4.m24411b(qr4, "response");
        if (qr4.mo30499d()) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRemoteDataSource.Companion.getTAG();
            local.mo33255d(tag, "getMicroAppGallery deviceSerial=" + this.$deviceSerial + " onSuccess response=" + qr4.mo30496a());
            com.portfolio.platform.data.source.remote.ApiResponse a = qr4.mo30496a();
            java.util.List list = a != null ? a.get_items() : null;
            if (list == null || !(!list.isEmpty())) {
                com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
                if (getMicroAppGalleryCallback != null) {
                    getMicroAppGalleryCallback.onFail();
                    return;
                }
                return;
            }
            com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback2 = this.$callback;
            if (getMicroAppGalleryCallback2 != null) {
                getMicroAppGalleryCallback2.onSuccess(list);
                return;
            }
            return;
        }
        com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback3 = this.$callback;
        if (getMicroAppGalleryCallback3 != null) {
            getMicroAppGalleryCallback3.onFail();
        }
    }
}
