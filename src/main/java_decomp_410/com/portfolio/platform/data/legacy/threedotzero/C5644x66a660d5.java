package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory */
public final class C5644x66a660d5 implements dagger.internal.Factory<com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource> {
    @DexIgnore
    public /* final */ com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule module;

    @DexIgnore
    public C5644x66a660d5(com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule microAppSettingRepositoryModule) {
        this.module = microAppSettingRepositoryModule;
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.C5644x66a660d5 create(com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule microAppSettingRepositoryModule) {
        return new com.portfolio.platform.data.legacy.threedotzero.C5644x66a660d5(microAppSettingRepositoryModule);
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource provideInstance(com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule microAppSettingRepositoryModule) {
        return proxyProvideFavoriteMappingSetLocalDataSource(microAppSettingRepositoryModule);
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource proxyProvideFavoriteMappingSetLocalDataSource(com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule microAppSettingRepositoryModule) {
        com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource provideFavoriteMappingSetLocalDataSource = microAppSettingRepositoryModule.provideFavoriteMappingSetLocalDataSource();
        com.fossil.blesdk.obfuscated.n44.m25602a(provideFavoriteMappingSetLocalDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideFavoriteMappingSetLocalDataSource;
    }

    @DexIgnore
    public com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource get() {
        return provideInstance(this.module);
    }
}
