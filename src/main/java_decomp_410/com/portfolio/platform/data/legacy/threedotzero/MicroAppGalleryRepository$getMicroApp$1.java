package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepository$getMicroApp$1 implements com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppCallback {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $deviceSerial;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $microAppId;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository this$0;

    @DexIgnore
    public MicroAppGalleryRepository$getMicroApp$1(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository microAppGalleryRepository, java.lang.String str, com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback, java.lang.String str2) {
        this.this$0 = microAppGalleryRepository;
        this.$microAppId = str;
        this.$callback = getMicroAppCallback;
        this.$deviceSerial = str2;
    }

    @DexIgnore
    public void onFail() {
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "getMicroApp microAppId=" + this.$microAppId + " local onFail");
        this.this$0.mMicroAppSettingRemoteDataSource.getMicroAppGallery(this.$deviceSerial, new com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository$getMicroApp$1$onFail$1(this));
    }

    @DexIgnore
    public void onSuccess(com.portfolio.platform.data.legacy.threedotzero.MicroApp microApp) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(microApp, "microApp");
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "getMicroApp microAppId=" + this.$microAppId + " local onSuccess");
        com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback = this.$callback;
        if (getMicroAppCallback != null) {
            getMicroAppCallback.onSuccess(microApp);
        }
    }
}
