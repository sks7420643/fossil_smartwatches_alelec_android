package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepositoryModule_ProvideGalleryLocalDataSource$app_fossilReleaseFactory */
public final class C5632x79a0a086 implements dagger.internal.Factory<com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource> {
    @DexIgnore
    public /* final */ com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepositoryModule module;

    @DexIgnore
    public C5632x79a0a086(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule) {
        this.module = microAppGalleryRepositoryModule;
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.C5632x79a0a086 create(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule) {
        return new com.portfolio.platform.data.legacy.threedotzero.C5632x79a0a086(microAppGalleryRepositoryModule);
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource provideInstance(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule) {
        return proxyProvideGalleryLocalDataSource$app_fossilRelease(microAppGalleryRepositoryModule);
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource proxyProvideGalleryLocalDataSource$app_fossilRelease(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule) {
        com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource provideGalleryLocalDataSource$app_fossilRelease = microAppGalleryRepositoryModule.provideGalleryLocalDataSource$app_fossilRelease();
        com.fossil.blesdk.obfuscated.n44.m25602a(provideGalleryLocalDataSource$app_fossilRelease, "Cannot return null from a non-@Nullable @Provides method");
        return provideGalleryLocalDataSource$app_fossilRelease;
    }

    @DexIgnore
    public com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource get() {
        return provideInstance(this.module);
    }
}
