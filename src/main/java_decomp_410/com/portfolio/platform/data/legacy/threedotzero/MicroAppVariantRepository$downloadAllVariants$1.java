package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRepository$downloadAllVariants$1 implements com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListRemoteCallback {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ int $major;
    @DexIgnore
    public /* final */ /* synthetic */ int $minor;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository this$0;

    @DexIgnore
    public MicroAppVariantRepository$downloadAllVariants$1(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository microAppVariantRepository, java.lang.String str, int i, int i2, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        this.this$0 = microAppVariantRepository;
        this.$serialNumber = str;
        this.$major = i;
        this.$minor = i2;
        this.$callback = getVariantListCallback;
    }

    @DexIgnore
    public void onFail(int i) {
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "getAllMicroAppVariants remote serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onFail");
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback = this.$callback;
        if (getVariantListCallback != null) {
            getVariantListCallback.onFail(i);
        }
    }

    @DexIgnore
    public void onSuccess(java.util.ArrayList<com.fossil.blesdk.obfuscated.wo2> arrayList) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(arrayList, "variantParserList");
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "getAllMicroAppVariants remote serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onSuccess");
        this.this$0.mAppExecutors.mo27946a().execute(new com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository$downloadAllVariants$1$onSuccess$1(this, arrayList));
    }

    @DexIgnore
    public void onSuccess(java.util.List<com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant> list) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "variantList");
    }
}
