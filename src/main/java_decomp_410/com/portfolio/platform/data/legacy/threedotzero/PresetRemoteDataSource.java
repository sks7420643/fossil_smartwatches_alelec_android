package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.er4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.to2;
import com.fossil.blesdk.obfuscated.tz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class PresetRemoteDataSource extends PresetDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "PresetRemoteDataSource";
    @DexIgnore
    public /* final */ ShortcutApiService mApiService;
    @DexIgnore
    public /* final */ h42 mAppExecutors;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 implements er4<xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.GetSavedPresetListCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ List val$savedPresets;

        @DexIgnore
        public Anon1(List list, PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
            this.val$savedPresets = list;
            this.val$callback = getSavedPresetListCallback;
        }

        @DexIgnore
        public void onFailure(Call<xz1> call, Throwable th) {
            MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onFailure");
            if (!this.val$savedPresets.isEmpty()) {
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onFailure presetList not null");
                PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback = this.val$callback;
                if (getSavedPresetListCallback != null) {
                    getSavedPresetListCallback.onSuccess(this.val$savedPresets);
                    return;
                }
                return;
            }
            MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onFailure presetList is null");
            PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback2 = this.val$callback;
            if (getSavedPresetListCallback2 != null) {
                getSavedPresetListCallback2.onFail();
            }
        }

        @DexIgnore
        public void onResponse(Call<xz1> call, qr4<xz1> qr4) {
            if (qr4.d()) {
                to2 to2 = new to2();
                to2.a(qr4.a());
                this.val$savedPresets.addAll(to2.b());
                Range a = to2.a();
                if (a != null && a.isHasNext()) {
                    MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onSuccess hasNext=true");
                    PresetRemoteDataSource.this.getUserPresets(a.getOffset() + a.getLimit() + 1, a.getLimit(), this);
                } else if (this.val$callback != null) {
                    MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onSuccess hasNext=false");
                    if (!this.val$savedPresets.isEmpty()) {
                        this.val$callback.onSuccess(this.val$savedPresets);
                    } else {
                        this.val$callback.onFail();
                    }
                }
            } else {
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList !isSuccessful");
                if (!this.val$savedPresets.isEmpty()) {
                    MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList !isSuccessful presetList not null");
                    PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback = this.val$callback;
                    if (getSavedPresetListCallback != null) {
                        getSavedPresetListCallback.onSuccess(this.val$savedPresets);
                        return;
                    }
                    return;
                }
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList !isSuccessful presetList is null");
                PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback2 = this.val$callback;
                if (getSavedPresetListCallback2 != null) {
                    getSavedPresetListCallback2.onFail();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 implements PresetDataSource.AddOrUpdateSavedPresetListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.AddOrUpdateSavedPresetCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ SavedPreset val$savedPreset;

        @DexIgnore
        public Anon2(SavedPreset savedPreset, PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback) {
            this.val$savedPreset = savedPreset;
            this.val$callback = addOrUpdateSavedPresetCallback;
        }

        @DexIgnore
        public void onFail() {
            String str = PresetRemoteDataSource.TAG;
            MFLogger.d(str, "addOrUpdateSavedPreset onFail mappingSetId=" + this.val$savedPreset.getId() + " mappingSetName=" + this.val$savedPreset.getName());
            PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback = this.val$callback;
            if (addOrUpdateSavedPresetCallback != null) {
                addOrUpdateSavedPresetCallback.onFail();
            }
        }

        @DexIgnore
        public void onSuccess(List<SavedPreset> list) {
            String str = PresetRemoteDataSource.TAG;
            MFLogger.d(str, "addOrUpdateSavedPreset onSuccess mappingSetId=" + this.val$savedPreset.getId() + " mappingSetName=" + this.val$savedPreset.getName());
            if (!list.isEmpty()) {
                PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback = this.val$callback;
                if (addOrUpdateSavedPresetCallback != null) {
                    addOrUpdateSavedPresetCallback.onSuccess(list.get(0));
                    return;
                }
                return;
            }
            PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback2 = this.val$callback;
            if (addOrUpdateSavedPresetCallback2 != null) {
                addOrUpdateSavedPresetCallback2.onFail();
            }
        }
    }

    @DexIgnore
    public PresetRemoteDataSource(ShortcutApiService shortcutApiService, h42 h42) {
        this.mApiService = shortcutApiService;
        this.mAppExecutors = h42;
    }

    @DexIgnore
    private xz1 createListItemJsonObject(tz1 tz1) {
        xz1 xz1 = new xz1();
        xz1.a(CloudLogWriter.ITEMS_PARAM, (JsonElement) tz1);
        return xz1;
    }

    @DexIgnore
    public void addOrUpdateActivePreset(ActivePreset activePreset, PresetDataSource.AddOrUpdateActivePresetCallback addOrUpdateActivePresetCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateActivePreset serialNumber=" + activePreset.getSerialNumber() + " originalId=" + activePreset.getOriginalId());
        tz1 tz1 = new tz1();
        tz1.a((JsonElement) activePreset.getJsonObject());
        xz1 createListItemJsonObject = createListItemJsonObject(tz1);
        String str2 = TAG;
        MFLogger.d(str2, "initJsonData - json: " + createListItemJsonObject);
    }

    @DexIgnore
    public void addOrUpdateSavedPreset(SavedPreset savedPreset, PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateSavedPreset mappingSetId=" + savedPreset.getId() + " mappingSetName=" + savedPreset.getName());
        ArrayList arrayList = new ArrayList();
        arrayList.add(savedPreset);
        addOrUpdateSavedPresetList(arrayList, new Anon2(savedPreset, addOrUpdateSavedPresetCallback));
    }

    @DexIgnore
    public void addOrUpdateSavedPresetList(List<SavedPreset> list, PresetDataSource.AddOrUpdateSavedPresetListCallback addOrUpdateSavedPresetListCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateSavedPreset presetListSize=" + list.size());
        tz1 tz1 = new tz1();
        for (SavedPreset jsonObject : list) {
            tz1.a((JsonElement) jsonObject.getJsonObject());
        }
        xz1 createListItemJsonObject = createListItemJsonObject(tz1);
        String str2 = TAG;
        MFLogger.d(str2, "initJsonData - json: " + createListItemJsonObject);
    }

    @DexIgnore
    public void clearData() {
    }

    @DexIgnore
    public void deleteSavedPresetList(List<SavedPreset> list, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        String str = TAG;
        MFLogger.d(str, "deleteSavedPresetList presetListSize=" + list.size());
        tz1 tz1 = new tz1();
        try {
            for (SavedPreset id : list) {
                xz1 xz1 = new xz1();
                xz1.a("id", id.getId());
                tz1.a((JsonElement) xz1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        createListItemJsonObject(tz1);
    }

    @DexIgnore
    public void downloadActivePresetList(PresetDataSource.GetActivePresetListCallback getActivePresetListCallback) {
    }

    @DexIgnore
    public void getDefaultPreset(String str, PresetDataSource.GetRecommendedPresetCallback getRecommendedPresetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getDefaultPreset deviceSerial=" + str);
        new ArrayList();
    }

    @DexIgnore
    public void getRecommendedPresets(String str, PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getRecommendedPresets deviceSerial=" + str);
        new ArrayList();
    }

    @DexIgnore
    public void getSavedPresetList(PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
        MFLogger.d(TAG, "getSavedPresetList");
        getUserPresets(0, 100, new Anon1(new ArrayList(), getSavedPresetListCallback));
    }

    @DexIgnore
    public void getUserPresets(int i, int i2, er4<xz1> er4) {
    }
}
