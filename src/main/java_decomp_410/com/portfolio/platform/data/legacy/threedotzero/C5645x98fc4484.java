package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory */
public final class C5645x98fc4484 implements dagger.internal.Factory<com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource> {
    @DexIgnore
    public /* final */ javax.inject.Provider<com.fossil.blesdk.obfuscated.h42> appExecutorsProvider;
    @DexIgnore
    public /* final */ com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule module;
    @DexIgnore
    public /* final */ javax.inject.Provider<com.portfolio.platform.data.source.remote.ShortcutApiService> shortcutApiServiceProvider;

    @DexIgnore
    public C5645x98fc4484(com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule microAppSettingRepositoryModule, javax.inject.Provider<com.portfolio.platform.data.source.remote.ShortcutApiService> provider, javax.inject.Provider<com.fossil.blesdk.obfuscated.h42> provider2) {
        this.module = microAppSettingRepositoryModule;
        this.shortcutApiServiceProvider = provider;
        this.appExecutorsProvider = provider2;
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.C5645x98fc4484 create(com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule microAppSettingRepositoryModule, javax.inject.Provider<com.portfolio.platform.data.source.remote.ShortcutApiService> provider, javax.inject.Provider<com.fossil.blesdk.obfuscated.h42> provider2) {
        return new com.portfolio.platform.data.legacy.threedotzero.C5645x98fc4484(microAppSettingRepositoryModule, provider, provider2);
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource provideInstance(com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule microAppSettingRepositoryModule, javax.inject.Provider<com.portfolio.platform.data.source.remote.ShortcutApiService> provider, javax.inject.Provider<com.fossil.blesdk.obfuscated.h42> provider2) {
        return proxyProvideFavoriteMappingSetRemoteDataSource(microAppSettingRepositoryModule, provider.get(), provider2.get());
    }

    @DexIgnore
    public static com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource proxyProvideFavoriteMappingSetRemoteDataSource(com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule microAppSettingRepositoryModule, com.portfolio.platform.data.source.remote.ShortcutApiService shortcutApiService, com.fossil.blesdk.obfuscated.h42 h42) {
        com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource provideFavoriteMappingSetRemoteDataSource = microAppSettingRepositoryModule.provideFavoriteMappingSetRemoteDataSource(shortcutApiService, h42);
        com.fossil.blesdk.obfuscated.n44.m25602a(provideFavoriteMappingSetRemoteDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideFavoriteMappingSetRemoteDataSource;
    }

    @DexIgnore
    public com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource get() {
        return provideInstance(this.module, this.shortcutApiServiceProvider, this.appExecutorsProvider);
    }
}
