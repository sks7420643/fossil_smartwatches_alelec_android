package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRepository$saveDeclarationFileList$1 implements com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource.AddOrUpdateDeclarationFileCallback {
    @DexIgnore
    public void onFail() {
        com.misfit.frameworks.common.log.MFLogger.m31689d(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository.Companion.getTAG(), "save microAppDeclarationFile onFail");
    }

    @DexIgnore
    public void onSuccess(com.portfolio.platform.data.legacy.threedotzero.DeclarationFile declarationFile) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(declarationFile, "declarationFile");
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository.Companion.getTAG();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("save microAppDeclarationFile onSuccess variantId=");
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant microAppVariant = declarationFile.getMicroAppVariant();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) microAppVariant, "declarationFile.microAppVariant");
        sb.append(microAppVariant.getId());
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, sb.toString());
    }
}
