package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.f02;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@DatabaseTable(tableName = "device")
public final class DeviceModel {
    @DexIgnore
    @f02("batteryLevel")
    @DatabaseField(columnName = "deviceBattery")
    public int batteryLevel;
    @DexIgnore
    @f02("createdAt")
    @DatabaseField(columnName = "createdAt")
    public String createdAt;
    @DexIgnore
    @f02("id")
    @DatabaseField(columnName = "deviceId", id = true)
    public String deviceId;
    @DexIgnore
    @f02("firmwareRevision")
    @DatabaseField(columnName = "firmwareRevision")
    public String firmwareRevision;
    @DexIgnore
    @f02("mac_address")
    @DatabaseField(columnName = "macAddress")
    public String macAddress;
    @DexIgnore
    @f02("majorNumber")
    @DatabaseField(columnName = "major")
    public int major;
    @DexIgnore
    @f02("minorNumber")
    @DatabaseField(columnName = "minor")
    public int minor;
    @DexIgnore
    @f02("sku")
    @DatabaseField(columnName = "sku")
    public String sku;
    @DexIgnore
    @f02("updatedAt")
    @DatabaseField(columnName = "updateAt")
    public String updateAt;
    @DexIgnore
    @DatabaseField(columnName = "vibrationStrength")
    public int vibrationStrength; // = 50;

    @DexIgnore
    public final int getBatteryLevel() {
        return this.batteryLevel;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getDeviceId() {
        return this.deviceId;
    }

    @DexIgnore
    public final String getFirmwareRevision() {
        return this.firmwareRevision;
    }

    @DexIgnore
    public final String getMacAddress() {
        return this.macAddress;
    }

    @DexIgnore
    public final int getMajor() {
        return this.major;
    }

    @DexIgnore
    public final int getMinor() {
        return this.minor;
    }

    @DexIgnore
    public final String getSku() {
        return this.sku;
    }

    @DexIgnore
    public final String getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public final int getVibrationStrength() {
        return this.vibrationStrength;
    }

    @DexIgnore
    public final void setBatteryLevel(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDeviceId(String str) {
        this.deviceId = str;
    }

    @DexIgnore
    public final void setFirmwareRevision(String str) {
        this.firmwareRevision = str;
    }

    @DexIgnore
    public final void setMacAddress(String str) {
        this.macAddress = str;
    }

    @DexIgnore
    public final void setMajor(int i) {
        this.major = i;
    }

    @DexIgnore
    public final void setMinor(int i) {
        this.minor = i;
    }

    @DexIgnore
    public final void setSku(String str) {
        this.sku = str;
    }

    @DexIgnore
    public final void setUpdateAt(String str) {
        this.updateAt = str;
    }

    @DexIgnore
    public final void setVibrationStrength(int i) {
        this.vibrationStrength = i;
    }
}
