package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppGalleryRepository$downloadMicroAppGallery$1 implements com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $deviceSerial;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository this$0;

    @DexIgnore
    public MicroAppGalleryRepository$downloadMicroAppGallery$1(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository microAppGalleryRepository, java.lang.String str, com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        this.this$0 = microAppGalleryRepository;
        this.$deviceSerial = str;
        this.$callback = getMicroAppGalleryCallback;
    }

    @DexIgnore
    public void onFail() {
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "downloadMicroAppGallery deviceSerial=" + this.$deviceSerial + " remote onFail");
        com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onFail();
        }
    }

    @DexIgnore
    public void onSuccess(java.util.List<? extends com.portfolio.platform.data.legacy.threedotzero.MicroApp> list) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "microAppList");
        java.util.ArrayList arrayList = new java.util.ArrayList();
        java.lang.String tag = com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG();
        com.misfit.frameworks.common.log.MFLogger.m31689d(tag, "downloadMicroAppGallery deviceSerial=" + this.$deviceSerial + " remote onSuccess");
        for (com.portfolio.platform.data.legacy.threedotzero.MicroApp microApp : list) {
            java.lang.String s = com.fossil.blesdk.obfuscated.f62.f14731x.mo27344s();
            java.lang.String appId = microApp.getAppId();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) appId, "microApp.appId");
            if (!kotlin.text.StringsKt__StringsKt.m37073a((java.lang.CharSequence) s, (java.lang.CharSequence) appId, false, 2, (java.lang.Object) null)) {
                java.lang.String tag2 = com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG();
                com.misfit.frameworks.common.log.MFLogger.m31689d(tag2, "Add micro app id=" + microApp.getAppId());
                microApp.setPlatform(this.$deviceSerial);
                arrayList.add(microApp);
            } else {
                java.lang.String tag3 = com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG();
                com.misfit.frameworks.common.log.MFLogger.m31689d(tag3, "Ignore micro app id=" + microApp.getAppId());
            }
        }
        com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onSuccess(arrayList);
        }
        this.this$0.mAppExecutors.mo27946a().execute(new com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository$downloadMicroAppGallery$1$onSuccess$1(this, arrayList));
    }
}
