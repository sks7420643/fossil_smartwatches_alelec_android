package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppVariantRepository$downloadAllVariants$1$onSuccess$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.ArrayList $variantParserList;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository$downloadAllVariants$1 this$0;

    @DexIgnore
    public MicroAppVariantRepository$downloadAllVariants$1$onSuccess$1(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository$downloadAllVariants$1 microAppVariantRepository$downloadAllVariants$1, java.util.ArrayList arrayList) {
        this.this$0 = microAppVariantRepository$downloadAllVariants$1;
        this.$variantParserList = arrayList;
    }

    @DexIgnore
    public final void run() {
        com.misfit.frameworks.common.log.MFLogger.m31689d(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG(), "diskIO enter onSuccess downloadAllVariants");
        java.util.ArrayList<com.fossil.blesdk.obfuscated.wo2> filterVariantList$app_fossilRelease = this.this$0.this$0.filterVariantList$app_fossilRelease(this.$variantParserList);
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository$downloadAllVariants$1 microAppVariantRepository$downloadAllVariants$1 = this.this$0;
        microAppVariantRepository$downloadAllVariants$1.this$0.saveMicroAppVariant$app_fossilRelease(microAppVariantRepository$downloadAllVariants$1.$serialNumber, filterVariantList$app_fossilRelease);
        com.portfolio.platform.data.legacy.threedotzero.UAppSystemVersionModel uAppSystemVersionModel = this.this$0.this$0.mUAppSystemVersionRepository.getUAppSystemVersionModel(this.this$0.$serialNumber);
        if (uAppSystemVersionModel != null && uAppSystemVersionModel.getMajorVersion() == this.this$0.$major && uAppSystemVersionModel.getMinorVersion() == this.this$0.$minor) {
            uAppSystemVersionModel.setPinType(0);
            this.this$0.this$0.mUAppSystemVersionRepository.addOrUpdateUAppSystemVersionModel(uAppSystemVersionModel);
        }
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository$downloadAllVariants$1 microAppVariantRepository$downloadAllVariants$12 = this.this$0;
        microAppVariantRepository$downloadAllVariants$12.this$0.notifyStatusChanged("DECLARATION_FILES_DOWNLOADED", microAppVariantRepository$downloadAllVariants$12.$serialNumber);
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource access$getMMicroAppVariantLocalDataSource$p = this.this$0.this$0.mMicroAppVariantLocalDataSource;
        com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantRepository$downloadAllVariants$1 microAppVariantRepository$downloadAllVariants$13 = this.this$0;
        access$getMMicroAppVariantLocalDataSource$p.getAllMicroAppVariants(microAppVariantRepository$downloadAllVariants$13.$serialNumber, microAppVariantRepository$downloadAllVariants$13.$major, microAppVariantRepository$downloadAllVariants$13.$minor, microAppVariantRepository$downloadAllVariants$13.$callback);
        com.misfit.frameworks.common.log.MFLogger.m31689d(com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryRepository.Companion.getTAG(), "diskIO exit onSuccess downloadAllVariants");
    }
}
