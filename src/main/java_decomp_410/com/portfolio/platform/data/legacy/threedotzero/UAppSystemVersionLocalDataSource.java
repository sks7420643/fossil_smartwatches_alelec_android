package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.blesdk.obfuscated.dn2;
import com.portfolio.platform.data.source.UAppSystemVersionDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class UAppSystemVersionLocalDataSource implements UAppSystemVersionDataSource {
    @DexIgnore
    public void addOrUpdateUAppSystemVersionModel(UAppSystemVersionModel uAppSystemVersionModel) {
        dn2.p.a().d().addOrUpdateUAppSystemVersionModel(uAppSystemVersionModel);
    }

    @DexIgnore
    public UAppSystemVersionModel getUAppSystemVersionModel(String str) {
        return dn2.p.a().d().getUAppSystemVersionModel(str);
    }
}
