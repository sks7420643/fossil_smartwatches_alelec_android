package com.portfolio.platform.data;

import com.fossil.blesdk.obfuscated.d02;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class InactivityNudgeTimeModel {
    @DexIgnore
    @d02
    public int minutes;
    @DexIgnore
    @d02
    public String nudgeTimeName;
    @DexIgnore
    @d02
    public int nudgeTimeType;

    @DexIgnore
    public InactivityNudgeTimeModel(String str, int i, int i2) {
        kd4.b(str, "nudgeTimeName");
        this.nudgeTimeName = str;
        this.minutes = i;
        this.nudgeTimeType = i2;
    }

    @DexIgnore
    public final int getMinutes() {
        return this.minutes;
    }

    @DexIgnore
    public final String getNudgeTimeName() {
        return this.nudgeTimeName;
    }

    @DexIgnore
    public final int getNudgeTimeType() {
        return this.nudgeTimeType;
    }

    @DexIgnore
    public final void setMinutes(int i) {
        this.minutes = i;
    }

    @DexIgnore
    public final void setNudgeTimeName(String str) {
        kd4.b(str, "<set-?>");
        this.nudgeTimeName = str;
    }

    @DexIgnore
    public final void setNudgeTimeType(int i) {
        this.nudgeTimeType = i;
    }
}
