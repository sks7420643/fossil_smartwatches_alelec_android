package com.portfolio.platform.data;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.enums.Gender;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WechatUserinfo {
    @DexIgnore
    @f02("city")
    public /* final */ String mCity;
    @DexIgnore
    @f02("country")
    public /* final */ String mCountry;
    @DexIgnore
    @f02("headimgurl")
    public /* final */ String mHeadimgurl;
    @DexIgnore
    @f02("nickname")
    public /* final */ String mNickname;
    @DexIgnore
    @f02("openid")
    public /* final */ String mOpenid;
    @DexIgnore
    @f02("privilege")
    public /* final */ List<String> mPrivilege;
    @DexIgnore
    @f02("province")
    public /* final */ String mProvince;
    @DexIgnore
    @f02("sex")
    public /* final */ int mSex;
    @DexIgnore
    @f02("unionId")
    public /* final */ String mUnionId;

    @DexIgnore
    public WechatUserinfo(String str, String str2, int i, String str3, String str4, String str5, String str6, List<String> list, String str7) {
        kd4.b(str, "mOpenid");
        kd4.b(str2, "mNickname");
        kd4.b(str3, "mCity");
        kd4.b(str4, "mProvince");
        kd4.b(str5, "mCountry");
        kd4.b(str6, "mHeadimgurl");
        kd4.b(list, "mPrivilege");
        kd4.b(str7, "mUnionId");
        this.mOpenid = str;
        this.mNickname = str2;
        this.mSex = i;
        this.mCity = str3;
        this.mProvince = str4;
        this.mCountry = str5;
        this.mHeadimgurl = str6;
        this.mPrivilege = list;
        this.mUnionId = str7;
    }

    @DexIgnore
    public final String getHeadimgurl() {
        return this.mHeadimgurl;
    }

    @DexIgnore
    public final String getSex() {
        int i = this.mSex;
        if (i == 1) {
            return Gender.MALE.toString();
        }
        if (i != 2) {
            return Gender.OTHER.toString();
        }
        return Gender.FEMALE.toString();
    }
}
