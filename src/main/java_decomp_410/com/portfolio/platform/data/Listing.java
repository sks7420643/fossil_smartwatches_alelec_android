package com.portfolio.platform.data;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.wc4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Listing<T> {
    @DexIgnore
    public /* final */ LiveData<NetworkState> networkState;
    @DexIgnore
    public /* final */ LiveData<qd<T>> pagedList;
    @DexIgnore
    public /* final */ wc4<qa4> refresh;
    @DexIgnore
    public /* final */ wc4<qa4> retry;

    @DexIgnore
    public Listing(LiveData<qd<T>> liveData, LiveData<NetworkState> liveData2, wc4<qa4> wc4, wc4<qa4> wc42) {
        kd4.b(liveData, "pagedList");
        kd4.b(liveData2, "networkState");
        kd4.b(wc4, "refresh");
        kd4.b(wc42, "retry");
        this.pagedList = liveData;
        this.networkState = liveData2;
        this.refresh = wc4;
        this.retry = wc42;
    }

    // @DexIgnore
    // public static /* synthetic */ Listing copy$default(Listing listing, LiveData<qd<T>> liveData, LiveData<NetworkState> liveData2, wc4<qa4> wc4, wc4<qa4> wc42, int i, Object obj) {
    //     if ((i & 1) != 0) {
    //         liveData = listing.pagedList;
    //     }
    //     if ((i & 2) != 0) {
    //         liveData2 = listing.networkState;
    //     }
    //     if ((i & 4) != 0) {
    //         wc4 = listing.refresh;
    //     }
    //     if ((i & 8) != 0) {
    //         wc42 = listing.retry;
    //     }
    //     return listing.copy(liveData, liveData2, wc4, wc42);
    // }

    @DexIgnore
    public final LiveData<qd<T>> component1() {
        return this.pagedList;
    }

    @DexIgnore
    public final LiveData<NetworkState> component2() {
        return this.networkState;
    }

    @DexIgnore
    public final wc4<qa4> component3() {
        return this.refresh;
    }

    @DexIgnore
    public final wc4<qa4> component4() {
        return this.retry;
    }

    @DexIgnore
    public final Listing<T> copy(LiveData<qd<T>> liveData, LiveData<NetworkState> liveData2, wc4<qa4> wc4, wc4<qa4> wc42) {
        kd4.b(liveData, "pagedList");
        kd4.b(liveData2, "networkState");
        kd4.b(wc4, "refresh");
        kd4.b(wc42, "retry");
        return new Listing<>(liveData, liveData2, wc4, wc42);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Listing)) {
            return false;
        }
        Listing listing = (Listing) obj;
        return kd4.a((Object) this.pagedList, (Object) listing.pagedList) && kd4.a((Object) this.networkState, (Object) listing.networkState) && kd4.a((Object) this.refresh, (Object) listing.refresh) && kd4.a((Object) this.retry, (Object) listing.retry);
    }

    @DexIgnore
    public final LiveData<NetworkState> getNetworkState() {
        return this.networkState;
    }

    @DexIgnore
    public final LiveData<qd<T>> getPagedList() {
        return this.pagedList;
    }

    @DexIgnore
    public final wc4<qa4> getRefresh() {
        return this.refresh;
    }

    @DexIgnore
    public final wc4<qa4> getRetry() {
        return this.retry;
    }

    @DexIgnore
    public int hashCode() {
        LiveData<qd<T>> liveData = this.pagedList;
        int i = 0;
        int hashCode = (liveData != null ? liveData.hashCode() : 0) * 31;
        LiveData<NetworkState> liveData2 = this.networkState;
        int hashCode2 = (hashCode + (liveData2 != null ? liveData2.hashCode() : 0)) * 31;
        wc4<qa4> wc4 = this.refresh;
        int hashCode3 = (hashCode2 + (wc4 != null ? wc4.hashCode() : 0)) * 31;
        wc4<qa4> wc42 = this.retry;
        if (wc42 != null) {
            i = wc42.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public String toString() {
        return "Listing(pagedList=" + this.pagedList + ", networkState=" + this.networkState + ", refresh=" + this.refresh + ", retry=" + this.retry + ")";
    }
}
