package com.portfolio.platform.data;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.za4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.MFUser;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class User {
    @DexIgnore
    @f02("activeDeviceId")
    public /* final */ String mActiveDeviceId;
    @DexIgnore
    @f02("addresses")
    public /* final */ CommuteAddress mAddress;
    @DexIgnore
    @f02("authType")
    public /* final */ String mAuthType;
    @DexIgnore
    @f02("birthday")
    public /* final */ String mBirthday;
    @DexIgnore
    @f02("brand")
    public /* final */ String mBrand;
    @DexIgnore
    @f02("createdAt")
    public /* final */ String mCreatedAt;
    @DexIgnore
    @f02("diagnosticEnabled")
    public /* final */ Boolean mDiagnosticEnabled;
    @DexIgnore
    @f02("email")
    public /* final */ String mEmail;
    @DexIgnore
    @f02("emailOptIn")
    public /* final */ Boolean mEmailOptIn;
    @DexIgnore
    @f02("emailProgress")
    public /* final */ Boolean mEmailProgress;
    @DexIgnore
    @f02("externalId")
    public /* final */ String mExternalId;
    @DexIgnore
    @f02("firstName")
    public /* final */ String mFirstName;
    @DexIgnore
    @f02("gender")
    public /* final */ String mGender;
    @DexIgnore
    @f02("heightInCentimeters")
    public /* final */ Double mHeightInCentimeters;
    @DexIgnore
    @f02("integrations")
    public /* final */ String[] mIntegrations;
    @DexIgnore
    @f02("isOnboardingComplete")
    public /* final */ Boolean mIsOnboardingComplete;
    @DexIgnore
    @f02("lastName")
    public /* final */ String mLastName;
    @DexIgnore
    @f02("profilePicture")
    public /* final */ String mProfilePicture;
    @DexIgnore
    @f02("registerDate")
    public /* final */ String mRegisterDate;
    @DexIgnore
    @f02("registrationComplete")
    public /* final */ Boolean mRegistrationComplete;
    @DexIgnore
    @f02("unitGroup")
    public /* final */ UnitGroup mUnitGroup;
    @DexIgnore
    @f02("updatedAt")
    public /* final */ String mUpdatedAt;
    @DexIgnore
    @f02("useDefaultGoals")
    public /* final */ Boolean mUseDefaultGoals;
    @DexIgnore
    @f02("useDefaultBiometric")
    public /* final */ Boolean mUserDefaultBiometric;
    @DexIgnore
    @f02("username")
    public /* final */ String mUsername;
    @DexIgnore
    @f02("weightInGrams")
    public /* final */ Double mWeightInGrams;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CommuteAddress {
        @DexIgnore
        @f02("home")
        public /* final */ String home;
        @DexIgnore
        @f02("work")
        public /* final */ String work;

        @DexIgnore
        public CommuteAddress(String str, String str2) {
            kd4.b(str, "home");
            kd4.b(str2, "work");
            this.home = str;
            this.work = str2;
        }

        @DexIgnore
        public final String getHome$app_fossilRelease() {
            return this.home;
        }

        @DexIgnore
        public final String getWork$app_fossilRelease() {
            return this.work;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class UnitGroup {
        @DexIgnore
        @f02("distance")
        public /* final */ String distance;
        @DexIgnore
        @f02("height")
        public /* final */ String height;
        @DexIgnore
        @f02("temperature")
        public /* final */ String temperature;
        @DexIgnore
        @f02("weight")
        public /* final */ String weight;

        @DexIgnore
        public UnitGroup(String str, String str2, String str3, String str4) {
            kd4.b(str, "distance");
            kd4.b(str2, Constants.PROFILE_KEY_UNITS_WEIGHT);
            kd4.b(str3, "height");
            kd4.b(str4, "temperature");
            this.distance = str;
            this.weight = str2;
            this.height = str3;
            this.temperature = str4;
        }

        @DexIgnore
        public final String getDistance$app_fossilRelease() {
            return this.distance;
        }

        @DexIgnore
        public final String getHeight$app_fossilRelease() {
            return this.height;
        }

        @DexIgnore
        public final String getTemperature$app_fossilRelease() {
            return this.temperature;
        }

        @DexIgnore
        public final String getWeight$app_fossilRelease() {
            return this.weight;
        }
    }

    @DexIgnore
    public User(String str, String str2, String str3, String str4, String str5, UnitGroup unitGroup, CommuteAddress commuteAddress, Boolean bool, Boolean bool2, String str6, String[] strArr, String str7, String str8, String str9, Double d, Double d2, String str10, String str11, String str12, String str13, Boolean bool3, Boolean bool4, Boolean bool5, String str14, Boolean bool6, Boolean bool7) {
        String str15 = str7;
        String str16 = str8;
        String str17 = str10;
        kd4.b(str, "mCreatedAt");
        kd4.b(str2, "mUpdatedAt");
        kd4.b(str4, "mAuthType");
        kd4.b(str15, "mFirstName");
        kd4.b(str16, "mLastName");
        kd4.b(str17, "mBirthday");
        this.mCreatedAt = str;
        this.mUpdatedAt = str2;
        this.mUsername = str3;
        this.mAuthType = str4;
        this.mExternalId = str5;
        this.mUnitGroup = unitGroup;
        this.mAddress = commuteAddress;
        this.mEmailProgress = bool;
        this.mEmailOptIn = bool2;
        this.mActiveDeviceId = str6;
        this.mIntegrations = strArr;
        this.mFirstName = str15;
        this.mLastName = str16;
        this.mEmail = str9;
        this.mWeightInGrams = d;
        this.mHeightInCentimeters = d2;
        this.mBirthday = str17;
        this.mGender = str11;
        this.mProfilePicture = str12;
        this.mBrand = str13;
        this.mDiagnosticEnabled = bool3;
        this.mRegistrationComplete = bool4;
        this.mIsOnboardingComplete = bool5;
        this.mRegisterDate = str14;
        this.mUserDefaultBiometric = bool6;
        this.mUseDefaultGoals = bool7;
    }

    @DexIgnore
    public final MFUser toMFUser(MFUser mFUser) {
        if (mFUser == null) {
            mFUser = new MFUser();
        }
        mFUser.setCreatedAt(this.mCreatedAt);
        mFUser.setUpdatedAt(this.mUpdatedAt);
        mFUser.setEmail(this.mEmail);
        mFUser.setAuthType(this.mAuthType);
        mFUser.setUsername(this.mUsername);
        mFUser.setActiveDeviceId(this.mActiveDeviceId);
        mFUser.setFirstName(this.mFirstName);
        mFUser.setLastName(this.mLastName);
        Double d = this.mWeightInGrams;
        boolean z = false;
        mFUser.setWeightInGrams(d != null ? (int) d.doubleValue() : 0);
        Double d2 = this.mHeightInCentimeters;
        mFUser.setHeightInCentimeters(d2 != null ? (int) d2.doubleValue() : 0);
        Boolean bool = this.mUserDefaultBiometric;
        boolean z2 = true;
        mFUser.setUseDefaultBiometric(bool != null ? bool.booleanValue() : true);
        Boolean bool2 = this.mUseDefaultGoals;
        if (bool2 != null) {
            z2 = bool2.booleanValue();
        }
        mFUser.setUseDefaultGoals(z2);
        UnitGroup unitGroup = this.mUnitGroup;
        if (unitGroup != null) {
            mFUser.setHeightUnit(unitGroup.getHeight$app_fossilRelease());
            mFUser.setWeightUnit(this.mUnitGroup.getWeight$app_fossilRelease());
            mFUser.setDistanceUnit(this.mUnitGroup.getDistance$app_fossilRelease());
            mFUser.setTemperatureUnit(this.mUnitGroup.getTemperature$app_fossilRelease());
        }
        CommuteAddress commuteAddress = this.mAddress;
        if (commuteAddress != null) {
            mFUser.setHome(commuteAddress.getHome$app_fossilRelease());
            mFUser.setWork(commuteAddress.getWork$app_fossilRelease());
        }
        Boolean bool3 = this.mEmailOptIn;
        mFUser.setEmailOptIn(bool3 != null ? bool3.booleanValue() : false);
        mFUser.setRegisterDate(this.mRegisterDate);
        mFUser.setBirthday(this.mBirthday);
        mFUser.setGender(this.mGender);
        mFUser.setProfilePicture(this.mProfilePicture);
        mFUser.setBrand(this.mBrand);
        Boolean bool4 = this.mDiagnosticEnabled;
        mFUser.setDiagnosticEnabled(bool4 != null ? bool4.booleanValue() : false);
        Boolean bool5 = this.mIsOnboardingComplete;
        mFUser.setOnboardingComplete(bool5 != null ? bool5.booleanValue() : false);
        Boolean bool6 = this.mRegistrationComplete;
        if (bool6 != null) {
            z = bool6.booleanValue();
        }
        mFUser.setRegistrationComplete(z);
        String[] strArr = this.mIntegrations;
        mFUser.setIntegrations((List<String>) strArr != null ? za4.e(strArr) : null);
        return mFUser;
    }
}
