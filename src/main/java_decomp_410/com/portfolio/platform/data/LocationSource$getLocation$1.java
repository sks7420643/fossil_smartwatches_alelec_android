package com.portfolio.platform.data;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.LocationSource", mo27670f = "LocationSource.kt", mo27671l = {62}, mo27672m = "getLocation")
public final class LocationSource$getLocation$1 extends kotlin.coroutines.jvm.internal.ContinuationImpl {
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ java.lang.Object result;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.LocationSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LocationSource$getLocation$1(com.portfolio.platform.data.LocationSource locationSource, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(yb4);
        this.this$0 = locationSource;
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.getLocation((android.content.Context) null, false, this);
    }
}
