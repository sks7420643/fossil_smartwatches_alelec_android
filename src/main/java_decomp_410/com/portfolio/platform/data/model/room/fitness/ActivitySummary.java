package com.portfolio.platform.data.model.room.fitness;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.wearables.fsl.fitness.SampleDay;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitySummary {
    @DexIgnore
    public int activeTime;
    @DexIgnore
    public int activeTimeGoal;
    @DexIgnore
    public double calories;
    @DexIgnore
    public int caloriesGoal;
    @DexIgnore
    public DateTime createdAt;
    @DexIgnore
    public int day;
    @DexIgnore
    public double distance;
    @DexIgnore
    public Integer dstOffset;
    @DexIgnore
    public List<Integer> intensities;
    @DexIgnore
    public int month;
    @DexIgnore
    public int pinType;
    @DexIgnore
    public int stepGoal;
    @DexIgnore
    public double steps;
    @DexIgnore
    public String timezoneName;
    @DexIgnore
    public TotalValuesOfWeek totalValuesOfWeek;
    @DexIgnore
    public DateTime updatedAt;
    @DexIgnore
    public int year;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class TotalValuesOfWeek {
        @DexIgnore
        public int totalActiveTimeOfWeek;
        @DexIgnore
        public double totalCaloriesOfWeek;
        @DexIgnore
        public double totalStepsOfWeek;

        @DexIgnore
        public TotalValuesOfWeek(double d, double d2, int i) {
            this.totalStepsOfWeek = d;
            this.totalCaloriesOfWeek = d2;
            this.totalActiveTimeOfWeek = i;
        }

        @DexIgnore
        public static /* synthetic */ TotalValuesOfWeek copy$default(TotalValuesOfWeek totalValuesOfWeek, double d, double d2, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                d = totalValuesOfWeek.totalStepsOfWeek;
            }
            double d3 = d;
            if ((i2 & 2) != 0) {
                d2 = totalValuesOfWeek.totalCaloriesOfWeek;
            }
            double d4 = d2;
            if ((i2 & 4) != 0) {
                i = totalValuesOfWeek.totalActiveTimeOfWeek;
            }
            return totalValuesOfWeek.copy(d3, d4, i);
        }

        @DexIgnore
        public final double component1() {
            return this.totalStepsOfWeek;
        }

        @DexIgnore
        public final double component2() {
            return this.totalCaloriesOfWeek;
        }

        @DexIgnore
        public final int component3() {
            return this.totalActiveTimeOfWeek;
        }

        @DexIgnore
        public final TotalValuesOfWeek copy(double d, double d2, int i) {
            return new TotalValuesOfWeek(d, d2, i);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof TotalValuesOfWeek) {
                    TotalValuesOfWeek totalValuesOfWeek = (TotalValuesOfWeek) obj;
                    if (Double.compare(this.totalStepsOfWeek, totalValuesOfWeek.totalStepsOfWeek) == 0 && Double.compare(this.totalCaloriesOfWeek, totalValuesOfWeek.totalCaloriesOfWeek) == 0) {
                        if (this.totalActiveTimeOfWeek == totalValuesOfWeek.totalActiveTimeOfWeek) {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final int getTotalActiveTimeOfWeek() {
            return this.totalActiveTimeOfWeek;
        }

        @DexIgnore
        public final double getTotalCaloriesOfWeek() {
            return this.totalCaloriesOfWeek;
        }

        @DexIgnore
        public final double getTotalStepsOfWeek() {
            return this.totalStepsOfWeek;
        }

        @DexIgnore
        public int hashCode() {
            long doubleToLongBits = Double.doubleToLongBits(this.totalStepsOfWeek);
            long doubleToLongBits2 = Double.doubleToLongBits(this.totalCaloriesOfWeek);
            return (((((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)))) * 31) + this.totalActiveTimeOfWeek;
        }

        @DexIgnore
        public final void setTotalActiveTimeOfWeek(int i) {
            this.totalActiveTimeOfWeek = i;
        }

        @DexIgnore
        public final void setTotalCaloriesOfWeek(double d) {
            this.totalCaloriesOfWeek = d;
        }

        @DexIgnore
        public final void setTotalStepsOfWeek(double d) {
            this.totalStepsOfWeek = d;
        }

        @DexIgnore
        public String toString() {
            return "TotalValuesOfWeek(totalStepsOfWeek=" + this.totalStepsOfWeek + ", totalCaloriesOfWeek=" + this.totalCaloriesOfWeek + ", totalActiveTimeOfWeek=" + this.totalActiveTimeOfWeek + ")";
        }
    }

    @DexIgnore
    public ActivitySummary(int i, int i2, int i3, String str, Integer num, double d, double d2, double d3, List<Integer> list, int i4, int i5, int i6, int i7) {
        List<Integer> list2 = list;
        kd4.b(list2, SampleDay.COLUMN_INTENSITIES);
        this.year = i;
        this.month = i2;
        this.day = i3;
        this.timezoneName = str;
        this.dstOffset = num;
        this.steps = d;
        this.calories = d2;
        this.distance = d3;
        this.intensities = list2;
        this.activeTime = i4;
        this.stepGoal = i5;
        this.caloriesGoal = i6;
        this.activeTimeGoal = i7;
    }

    @DexIgnore
    public static /* synthetic */ ActivitySummary copy$default(ActivitySummary activitySummary, int i, int i2, int i3, String str, Integer num, double d, double d2, double d3, List list, int i4, int i5, int i6, int i7, int i8, Object obj) {
        ActivitySummary activitySummary2 = activitySummary;
        int i9 = i8;
        return activitySummary.copy((i9 & 1) != 0 ? activitySummary2.year : i, (i9 & 2) != 0 ? activitySummary2.month : i2, (i9 & 4) != 0 ? activitySummary2.day : i3, (i9 & 8) != 0 ? activitySummary2.timezoneName : str, (i9 & 16) != 0 ? activitySummary2.dstOffset : num, (i9 & 32) != 0 ? activitySummary2.steps : d, (i9 & 64) != 0 ? activitySummary2.calories : d2, (i9 & 128) != 0 ? activitySummary2.distance : d3, (i9 & 256) != 0 ? activitySummary2.intensities : list, (i9 & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? activitySummary2.activeTime : i4, (i9 & 1024) != 0 ? activitySummary2.stepGoal : i5, (i9 & 2048) != 0 ? activitySummary2.caloriesGoal : i6, (i9 & 4096) != 0 ? activitySummary2.activeTimeGoal : i7);
    }

    @DexIgnore
    public final int component1() {
        return this.year;
    }

    @DexIgnore
    public final int component10() {
        return this.activeTime;
    }

    @DexIgnore
    public final int component11() {
        return this.stepGoal;
    }

    @DexIgnore
    public final int component12() {
        return this.caloriesGoal;
    }

    @DexIgnore
    public final int component13() {
        return this.activeTimeGoal;
    }

    @DexIgnore
    public final int component2() {
        return this.month;
    }

    @DexIgnore
    public final int component3() {
        return this.day;
    }

    @DexIgnore
    public final String component4() {
        return this.timezoneName;
    }

    @DexIgnore
    public final Integer component5() {
        return this.dstOffset;
    }

    @DexIgnore
    public final double component6() {
        return this.steps;
    }

    @DexIgnore
    public final double component7() {
        return this.calories;
    }

    @DexIgnore
    public final double component8() {
        return this.distance;
    }

    @DexIgnore
    public final List<Integer> component9() {
        return this.intensities;
    }

    @DexIgnore
    public final ActivitySummary copy(int i, int i2, int i3, String str, Integer num, double d, double d2, double d3, List<Integer> list, int i4, int i5, int i6, int i7) {
        int i8 = i;
        kd4.b(list, SampleDay.COLUMN_INTENSITIES);
        return new ActivitySummary(i, i2, i3, str, num, d, d2, d3, list, i4, i5, i6, i7);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ActivitySummary) {
                ActivitySummary activitySummary = (ActivitySummary) obj;
                if (this.year == activitySummary.year) {
                    if (this.month == activitySummary.month) {
                        if ((this.day == activitySummary.day) && kd4.a((Object) this.timezoneName, (Object) activitySummary.timezoneName) && kd4.a((Object) this.dstOffset, (Object) activitySummary.dstOffset) && Double.compare(this.steps, activitySummary.steps) == 0 && Double.compare(this.calories, activitySummary.calories) == 0 && Double.compare(this.distance, activitySummary.distance) == 0 && kd4.a((Object) this.intensities, (Object) activitySummary.intensities)) {
                            if (this.activeTime == activitySummary.activeTime) {
                                if (this.stepGoal == activitySummary.stepGoal) {
                                    if (this.caloriesGoal == activitySummary.caloriesGoal) {
                                        if (this.activeTimeGoal == activitySummary.activeTimeGoal) {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getActiveTime() {
        return this.activeTime;
    }

    @DexIgnore
    public final int getActiveTimeGoal() {
        return this.activeTimeGoal;
    }

    @DexIgnore
    public final double getCalories() {
        return this.calories;
    }

    @DexIgnore
    public final int getCaloriesGoal() {
        return this.caloriesGoal;
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        Calendar instance = Calendar.getInstance();
        instance.set(this.year, this.month - 1, this.day);
        kd4.a((Object) instance, "calendar");
        Date time = instance.getTime();
        kd4.a((Object) time, "calendar.time");
        return time;
    }

    @DexIgnore
    public final int getDay() {
        return this.day;
    }

    @DexIgnore
    public final double getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final Integer getDstOffset() {
        return this.dstOffset;
    }

    @DexIgnore
    public final List<Integer> getIntensities() {
        return this.intensities;
    }

    @DexIgnore
    public final int getMonth() {
        return this.month;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getStepGoal() {
        return this.stepGoal;
    }

    @DexIgnore
    public final double getSteps() {
        return this.steps;
    }

    @DexIgnore
    public final String getTimezoneName() {
        return this.timezoneName;
    }

    @DexIgnore
    public final TotalValuesOfWeek getTotalValuesOfWeek() {
        return this.totalValuesOfWeek;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final int getYear() {
        return this.year;
    }

    @DexIgnore
    public int hashCode() {
        int i = ((((this.year * 31) + this.month) * 31) + this.day) * 31;
        String str = this.timezoneName;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        Integer num = this.dstOffset;
        int hashCode2 = num != null ? num.hashCode() : 0;
        long doubleToLongBits = Double.doubleToLongBits(this.steps);
        long doubleToLongBits2 = Double.doubleToLongBits(this.calories);
        long doubleToLongBits3 = Double.doubleToLongBits(this.distance);
        int i3 = (((((((hashCode + hashCode2) * 31) + ((int) (doubleToLongBits ^ (doubleToLongBits >>> 32)))) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)))) * 31) + ((int) (doubleToLongBits3 ^ (doubleToLongBits3 >>> 32)))) * 31;
        List<Integer> list = this.intensities;
        if (list != null) {
            i2 = list.hashCode();
        }
        return ((((((((i3 + i2) * 31) + this.activeTime) * 31) + this.stepGoal) * 31) + this.caloriesGoal) * 31) + this.activeTimeGoal;
    }

    @DexIgnore
    public final void setActiveTime(int i) {
        this.activeTime = i;
    }

    @DexIgnore
    public final void setActiveTimeGoal(int i) {
        this.activeTimeGoal = i;
    }

    @DexIgnore
    public final void setCalories(double d) {
        this.calories = d;
    }

    @DexIgnore
    public final void setCaloriesGoal(int i) {
        this.caloriesGoal = i;
    }

    @DexIgnore
    public final void setCreatedAt(DateTime dateTime) {
        this.createdAt = dateTime;
    }

    @DexIgnore
    public final void setDay(int i) {
        this.day = i;
    }

    @DexIgnore
    public final void setDistance(double d) {
        this.distance = d;
    }

    @DexIgnore
    public final void setDstOffset(Integer num) {
        this.dstOffset = num;
    }

    @DexIgnore
    public final void setIntensities(List<Integer> list) {
        kd4.b(list, "<set-?>");
        this.intensities = list;
    }

    @DexIgnore
    public final void setMonth(int i) {
        this.month = i;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setStepGoal(int i) {
        this.stepGoal = i;
    }

    @DexIgnore
    public final void setSteps(double d) {
        this.steps = d;
    }

    @DexIgnore
    public final void setTimezoneName(String str) {
        this.timezoneName = str;
    }

    @DexIgnore
    public final void setTotalValuesOfWeek(TotalValuesOfWeek totalValuesOfWeek2) {
        this.totalValuesOfWeek = totalValuesOfWeek2;
    }

    @DexIgnore
    public final void setUpdatedAt(DateTime dateTime) {
        this.updatedAt = dateTime;
    }

    @DexIgnore
    public final void setYear(int i) {
        this.year = i;
    }

    @DexIgnore
    public String toString() {
        return "ActivitySummary(year=" + this.year + ", month=" + this.month + ", day=" + this.day + ", timezoneName=" + this.timezoneName + ", dstOffset=" + this.dstOffset + ", steps=" + this.steps + ", calories=" + this.calories + ", distance=" + this.distance + ", intensities=" + this.intensities + ", activeTime=" + this.activeTime + ", stepGoal=" + this.stepGoal + ", caloriesGoal=" + this.caloriesGoal + ", activeTimeGoal=" + this.activeTimeGoal + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ ActivitySummary(int i, int i2, int i3, String str, Integer num, double d, double d2, double d3, List list, int i4, int i5, int i6, int i7, int i8, fd4 fd4) {
        this(i, i2, i3, str, num, d, d2, d3, list, (r0 & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? 0 : i4, (r0 & 1024) != 0 ? 0 : i5, (r0 & 2048) != 0 ? 0 : i6, (r0 & 4096) != 0 ? 0 : i7);
        int i9 = i8;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ActivitySummary(ActivitySummary activitySummary) {
        throw null;
/*        this(r1, r2, r3, r4, r5, r6, r8, r10, r12, r13, r14, r15, r16);
        ActivitySummary activitySummary2 = activitySummary;
        kd4.b(activitySummary2, "activitySummary");
        int i = activitySummary2.year;
        int i2 = activitySummary2.month;
        int i3 = activitySummary2.day;
        String str = activitySummary2.timezoneName;
        Integer num = activitySummary2.dstOffset;
        double d = activitySummary2.steps;
        double d2 = activitySummary2.calories;
        double d3 = activitySummary2.distance;
        List<Integer> list = activitySummary2.intensities;
        int i4 = activitySummary2.activeTime;
        ActivitySummary activitySummary3 = activitySummary2;
        int i5 = activitySummary2.stepGoal;
        int i6 = activitySummary3.caloriesGoal;
        int i7 = activitySummary3.activeTimeGoal;
        ActivitySummary activitySummary4 = activitySummary;
        this.createdAt = activitySummary4.createdAt;
        this.updatedAt = activitySummary4.updatedAt;
        this.pinType = activitySummary4.pinType;
        this.totalValuesOfWeek = activitySummary4.totalValuesOfWeek;
*/    }
}
