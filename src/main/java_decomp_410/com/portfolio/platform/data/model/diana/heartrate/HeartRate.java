package com.portfolio.platform.data.model.diana.heartrate;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rk2;
import com.portfolio.platform.data.model.ServerError;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRate extends ServerError {
    @DexIgnore
    @f02("average")
    public /* final */ float mAverage;
    @DexIgnore
    @f02("createdAt")
    public /* final */ Date mCreatedAt;
    @DexIgnore
    @f02("date")
    public /* final */ Date mDate;
    @DexIgnore
    @f02("endTime")
    public /* final */ String mEndTime;
    @DexIgnore
    @f02("id")
    public /* final */ String mId;
    @DexIgnore
    @f02("max")
    public /* final */ int mMax;
    @DexIgnore
    @f02("min")
    public /* final */ int mMin;
    @DexIgnore
    @f02("minuteCount")
    public /* final */ int mMinuteCount;
    @DexIgnore
    @f02("resting")
    public /* final */ Resting mResting;
    @DexIgnore
    @f02("startTime")
    public /* final */ String mStartTime;
    @DexIgnore
    @f02("timezoneOffset")
    public /* final */ int mTimeZoneOffsetInSecond;
    @DexIgnore
    @f02("updatedAt")
    public /* final */ Date mUpdatedAt;

    @DexIgnore
    public HeartRate(String str, float f, Date date, Date date2, Date date3, String str2, String str3, int i, int i2, int i3, int i4, Resting resting) {
        kd4.b(str, "mId");
        kd4.b(date, "mDate");
        kd4.b(date2, "mCreatedAt");
        kd4.b(date3, "mUpdatedAt");
        kd4.b(str2, "mEndTime");
        kd4.b(str3, "mStartTime");
        this.mId = str;
        this.mAverage = f;
        this.mDate = date;
        this.mCreatedAt = date2;
        this.mUpdatedAt = date3;
        this.mEndTime = str2;
        this.mStartTime = str3;
        this.mTimeZoneOffsetInSecond = i;
        this.mMin = i2;
        this.mMax = i3;
        this.mMinuteCount = i4;
        this.mResting = resting;
    }

    @DexIgnore
    public final HeartRateSample toHeartRateSample() {
        Date date;
        Date date2;
        Date date3;
        try {
            Calendar instance = Calendar.getInstance();
            date = rk2.a(this.mTimeZoneOffsetInSecond, this.mStartTime);
            try {
                kd4.a((Object) instance, "calendar");
                instance.setTime(date);
                instance.set(13, 0);
                instance.set(14, 0);
                date = instance.getTime();
                date3 = rk2.a(this.mTimeZoneOffsetInSecond, this.mEndTime);
            } catch (ParseException e) {
                e = e;
                date3 = null;
                e.printStackTrace();
                date2 = date3;
                String str = this.mId;
                float f = this.mAverage;
                Date date4 = this.mDate;
                long time = this.mCreatedAt.getTime();
                long time2 = this.mUpdatedAt.getTime();
                DateTime a = rk2.a(date2, this.mTimeZoneOffsetInSecond);
                kd4.a((Object) a, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                DateTime a2 = rk2.a(date, this.mTimeZoneOffsetInSecond);
                kd4.a((Object) a2, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                return new HeartRateSample(str, f, date4, time, time2, a, a2, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
            }
            try {
                instance.setTime(date3);
                instance.set(13, 0);
                instance.set(14, 0);
                date2 = instance.getTime();
            } catch (ParseException e2) {
                e = e2;
                e.printStackTrace();
                date2 = date3;
                String str2 = this.mId;
                float f2 = this.mAverage;
                Date date42 = this.mDate;
                long time3 = this.mCreatedAt.getTime();
                long time22 = this.mUpdatedAt.getTime();
                DateTime a3 = rk2.a(date2, this.mTimeZoneOffsetInSecond);
                kd4.a((Object) a3, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                DateTime a22 = rk2.a(date, this.mTimeZoneOffsetInSecond);
                kd4.a((Object) a22, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                return new HeartRateSample(str2, f2, date42, time3, time22, a3, a22, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
            }
        } catch (ParseException e3) {
            e = e3;
            date = null;
            date3 = null;
            e.printStackTrace();
            date2 = date3;
            String str22 = this.mId;
            float f22 = this.mAverage;
            Date date422 = this.mDate;
            long time32 = this.mCreatedAt.getTime();
            long time222 = this.mUpdatedAt.getTime();
            DateTime a32 = rk2.a(date2, this.mTimeZoneOffsetInSecond);
            kd4.a((Object) a32, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            DateTime a222 = rk2.a(date, this.mTimeZoneOffsetInSecond);
            kd4.a((Object) a222, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            return new HeartRateSample(str22, f22, date422, time32, time222, a32, a222, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
        }
        try {
            String str222 = this.mId;
            float f222 = this.mAverage;
            Date date4222 = this.mDate;
            long time322 = this.mCreatedAt.getTime();
            long time2222 = this.mUpdatedAt.getTime();
            DateTime a322 = rk2.a(date2, this.mTimeZoneOffsetInSecond);
            kd4.a((Object) a322, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            DateTime a2222 = rk2.a(date, this.mTimeZoneOffsetInSecond);
            kd4.a((Object) a2222, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            return new HeartRateSample(str222, f222, date4222, time322, time2222, a322, a2222, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
        } catch (Exception e4) {
            e4.printStackTrace();
            return null;
        }
    }
}
