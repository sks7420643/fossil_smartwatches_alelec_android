package com.portfolio.platform.data.model.diana.commutetime;

import com.facebook.share.internal.ShareConstants;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.kd4;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TrafficRequest {
    @DexIgnore
    @f02("avoid")
    public List<String> avoid;
    @DexIgnore
    @f02("destination")
    public Address destination;
    @DexIgnore
    @f02("mode")
    public String mode; // = "driving";
    @DexIgnore
    @f02("origin")
    public Address origin;

    @DexIgnore
    public TrafficRequest(boolean z, Address address, Address address2) {
        kd4.b(address, ShareConstants.DESTINATION);
        kd4.b(address2, "origin");
        this.destination = address;
        this.origin = address2;
        this.avoid = z ? cb4.a((T[]) new String[]{"tolls"}) : null;
        this.mode = "driving";
    }

    @DexIgnore
    public final List<String> getAvoid() {
        return this.avoid;
    }

    @DexIgnore
    public final Address getDestination() {
        return this.destination;
    }

    @DexIgnore
    public final String getMode() {
        return this.mode;
    }

    @DexIgnore
    public final Address getOrigin() {
        return this.origin;
    }

    @DexIgnore
    public final void setAvoid(List<String> list) {
        this.avoid = list;
    }

    @DexIgnore
    public final void setDestination(Address address) {
        kd4.b(address, "<set-?>");
        this.destination = address;
    }

    @DexIgnore
    public final void setMode(String str) {
        kd4.b(str, "<set-?>");
        this.mode = str;
    }

    @DexIgnore
    public final void setOrigin(Address address) {
        kd4.b(address, "<set-?>");
        this.origin = address;
    }
}
