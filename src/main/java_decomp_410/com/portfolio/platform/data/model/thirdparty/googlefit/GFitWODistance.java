package com.portfolio.platform.data.model.thirdparty.googlefit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GFitWODistance {
    @DexIgnore
    public float distance;
    @DexIgnore
    public long endTime;
    @DexIgnore
    public long startTime;

    @DexIgnore
    public GFitWODistance(float f, long j, long j2) {
        this.distance = f;
        this.startTime = j;
        this.endTime = j2;
    }

    @DexIgnore
    public final float getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final void setDistance(float f) {
        this.distance = f;
    }

    @DexIgnore
    public final void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public final void setStartTime(long j) {
        this.startTime = j;
    }
}
