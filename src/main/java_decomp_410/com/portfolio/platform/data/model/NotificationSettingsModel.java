package com.portfolio.platform.data.model;

import com.fossil.blesdk.obfuscated.d02;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationSettingsModel {
    @DexIgnore
    @d02
    public boolean isCall;
    @DexIgnore
    @d02
    public String settingsName;
    @DexIgnore
    @d02
    public int settingsType;

    @DexIgnore
    public NotificationSettingsModel(String str, int i, boolean z) {
        kd4.b(str, "settingsName");
        this.settingsName = str;
        this.settingsType = i;
        this.isCall = z;
    }

    @DexIgnore
    public static /* synthetic */ NotificationSettingsModel copy$default(NotificationSettingsModel notificationSettingsModel, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = notificationSettingsModel.settingsName;
        }
        if ((i2 & 2) != 0) {
            i = notificationSettingsModel.settingsType;
        }
        if ((i2 & 4) != 0) {
            z = notificationSettingsModel.isCall;
        }
        return notificationSettingsModel.copy(str, i, z);
    }

    @DexIgnore
    public final String component1() {
        return this.settingsName;
    }

    @DexIgnore
    public final int component2() {
        return this.settingsType;
    }

    @DexIgnore
    public final boolean component3() {
        return this.isCall;
    }

    @DexIgnore
    public final NotificationSettingsModel copy(String str, int i, boolean z) {
        kd4.b(str, "settingsName");
        return new NotificationSettingsModel(str, i, z);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof NotificationSettingsModel) {
                NotificationSettingsModel notificationSettingsModel = (NotificationSettingsModel) obj;
                if (kd4.a((Object) this.settingsName, (Object) notificationSettingsModel.settingsName)) {
                    if (this.settingsType == notificationSettingsModel.settingsType) {
                        if (this.isCall == notificationSettingsModel.isCall) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getSettingsName() {
        return this.settingsName;
    }

    @DexIgnore
    public final int getSettingsType() {
        return this.settingsType;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.settingsName;
        int hashCode = (((str != null ? str.hashCode() : 0) * 31) + this.settingsType) * 31;
        boolean z = this.isCall;
        if (z) {
            z = true;
        }
        return hashCode + (z ? 1 : 0);
    }

    @DexIgnore
    public final boolean isCall() {
        return this.isCall;
    }

    @DexIgnore
    public final void setCall(boolean z) {
        this.isCall = z;
    }

    @DexIgnore
    public final void setSettingsName(String str) {
        kd4.b(str, "<set-?>");
        this.settingsName = str;
    }

    @DexIgnore
    public final void setSettingsType(int i) {
        this.settingsType = i;
    }

    @DexIgnore
    public String toString() {
        return "NotificationSettingsModel(settingsName=" + this.settingsName + ", settingsType=" + this.settingsType + ", isCall=" + this.isCall + ")";
    }
}
