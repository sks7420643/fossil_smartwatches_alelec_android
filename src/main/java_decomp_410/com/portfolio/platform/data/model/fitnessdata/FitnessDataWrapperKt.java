package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import java.util.Date;
import java.util.List;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FitnessDataWrapperKt {
    @DexIgnore
    public static final Pair<Date, Date> calculateRangeDownload(List<FitnessDataWrapper> list, Date date, Date date2) {
        kd4.b(list, "$this$calculateRangeDownload");
        kd4.b(date, GoalPhase.COLUMN_START_DATE);
        kd4.b(date2, GoalPhase.COLUMN_END_DATE);
        if (list.isEmpty()) {
            return new Pair<>(date, date2);
        }
        if (!rk2.d(((FitnessDataWrapper) kb4.f(list)).getStartTimeTZ().toDate(), date2)) {
            return new Pair<>(((FitnessDataWrapper) kb4.f(list)).getStartTimeTZ().toDate(), date2);
        }
        if (rk2.d(((FitnessDataWrapper) kb4.d(list)).getStartTimeTZ().toDate(), date)) {
            return null;
        }
        return new Pair<>(date, rk2.m(((FitnessDataWrapper) kb4.d(list)).getStartTimeTZ().toDate()));
    }
}
