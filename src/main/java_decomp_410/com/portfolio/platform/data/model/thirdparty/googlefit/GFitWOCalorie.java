package com.portfolio.platform.data.model.thirdparty.googlefit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GFitWOCalorie {
    @DexIgnore
    public float calorie;
    @DexIgnore
    public long endTime;
    @DexIgnore
    public long startTime;

    @DexIgnore
    public GFitWOCalorie(float f, long j, long j2) {
        this.calorie = f;
        this.startTime = j;
        this.endTime = j2;
    }

    @DexIgnore
    public final float getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public final long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final void setCalorie(float f) {
        this.calorie = f;
    }

    @DexIgnore
    public final void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public final void setStartTime(long j) {
        this.startTime = j;
    }
}
