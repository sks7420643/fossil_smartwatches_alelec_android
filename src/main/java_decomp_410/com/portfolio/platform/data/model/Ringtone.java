package com.portfolio.platform.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.ik2;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Ringtone implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    @ik2
    public String ringtoneId;
    @DexIgnore
    @f02("ringTone")
    public String ringtoneName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<Ringtone> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public Ringtone createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new Ringtone(parcel);
        }

        @DexIgnore
        public Ringtone[] newArray(int i) {
            return new Ringtone[i];
        }
    }

    @DexIgnore
    public Ringtone() {
        this((String) null, (String) null, 3, (fd4) null);
    }

    @DexIgnore
    public Ringtone(String str, String str2) {
        kd4.b(str, "ringtoneName");
        this.ringtoneName = str;
        this.ringtoneId = str2;
    }

    @DexIgnore
    public static /* synthetic */ Ringtone copy$default(Ringtone ringtone, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = ringtone.ringtoneName;
        }
        if ((i & 2) != 0) {
            str2 = ringtone.ringtoneId;
        }
        return ringtone.copy(str, str2);
    }

    @DexIgnore
    public final String component1() {
        return this.ringtoneName;
    }

    @DexIgnore
    public final String component2() {
        return this.ringtoneId;
    }

    @DexIgnore
    public final Ringtone copy(String str, String str2) {
        kd4.b(str, "ringtoneName");
        return new Ringtone(str, str2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Ringtone)) {
            return false;
        }
        Ringtone ringtone = (Ringtone) obj;
        return kd4.a((Object) this.ringtoneName, (Object) ringtone.ringtoneName) && kd4.a((Object) this.ringtoneId, (Object) ringtone.ringtoneId);
    }

    @DexIgnore
    public final String getRingtoneId() {
        return this.ringtoneId;
    }

    @DexIgnore
    public final String getRingtoneName() {
        return this.ringtoneName;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.ringtoneName;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.ringtoneId;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setRingtoneId(String str) {
        this.ringtoneId = str;
    }

    @DexIgnore
    public final void setRingtoneName(String str) {
        kd4.b(str, "<set-?>");
        this.ringtoneName = str;
    }

    @DexIgnore
    public String toString() {
        return "Ringtone(ringtoneName=" + this.ringtoneName + ", ringtoneId=" + this.ringtoneId + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(this.ringtoneName);
        parcel.writeString(this.ringtoneId);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Ringtone(String str, String str2, int i, fd4 fd4) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? "" : str2);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public Ringtone(Parcel parcel) {
        throw null;
/*        this(r0 == null ? "" : r0, parcel.readString());
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
*/    }
}
