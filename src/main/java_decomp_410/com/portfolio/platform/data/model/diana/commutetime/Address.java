package com.portfolio.platform.data.model.diana.commutetime;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.fd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Address {
    @DexIgnore
    @f02("lat")
    public double lat;
    @DexIgnore
    @f02("lng")
    public double lng;

    @DexIgnore
    public Address() {
        this(0.0d, 0.0d, 3, (fd4) null);
    }

    @DexIgnore
    public Address(double d, double d2) {
        this.lat = d;
        this.lng = d2;
    }

    @DexIgnore
    public static /* synthetic */ Address copy$default(Address address, double d, double d2, int i, Object obj) {
        if ((i & 1) != 0) {
            d = address.lat;
        }
        if ((i & 2) != 0) {
            d2 = address.lng;
        }
        return address.copy(d, d2);
    }

    @DexIgnore
    public final double component1() {
        return this.lat;
    }

    @DexIgnore
    public final double component2() {
        return this.lng;
    }

    @DexIgnore
    public final Address copy(double d, double d2) {
        return new Address(d, d2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Address)) {
            return false;
        }
        Address address = (Address) obj;
        return Double.compare(this.lat, address.lat) == 0 && Double.compare(this.lng, address.lng) == 0;
    }

    @DexIgnore
    public final double getLat() {
        return this.lat;
    }

    @DexIgnore
    public final double getLng() {
        return this.lng;
    }

    @DexIgnore
    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.lat);
        long doubleToLongBits2 = Double.doubleToLongBits(this.lng);
        return (((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)));
    }

    @DexIgnore
    public final void setLat(double d) {
        this.lat = d;
    }

    @DexIgnore
    public final void setLng(double d) {
        this.lng = d;
    }

    @DexIgnore
    public String toString() {
        return "Address(lat=" + this.lat + ", lng=" + this.lng + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Address(double d, double d2, int i, fd4 fd4) {
        this((i & 1) != 0 ? 0.0d : d, (i & 2) != 0 ? 0.0d : d2);
    }
}
