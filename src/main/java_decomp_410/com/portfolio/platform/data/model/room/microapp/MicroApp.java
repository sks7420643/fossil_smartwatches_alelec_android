package com.portfolio.platform.data.model.room.microapp;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroApp {
    @DexIgnore
    @f02("categoryIds")
    public ArrayList<String> categories;
    @DexIgnore
    @f02("englishDescription")
    public String description;
    @DexIgnore
    @f02("description")
    public String descriptionKey;
    @DexIgnore
    @f02("icon")
    public String icon;
    @DexIgnore
    @f02("id")
    public String id;
    @DexIgnore
    @f02("englishName")
    public String name;
    @DexIgnore
    @f02("name")
    public String nameKey;
    @DexIgnore
    public String serialNumber;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = new int[MicroAppInstruction.MicroAppID.values().length];

        /*
        static {
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_TOGGLE_MODE.ordinal()] = 1;
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_SELFIE.ordinal()] = 2;
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_ACTIVITY_TAGGING_ID.ordinal()] = 3;
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_ALARM_ID.ordinal()] = 4;
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_ALERT_ID.ordinal()] = 5;
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_DATE_ID.ordinal()] = 6;
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.ordinal()] = 7;
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID.ordinal()] = 8;
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.ordinal()] = 9;
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_DOWN_ID.ordinal()] = 10;
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_UP_ID.ordinal()] = 11;
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.ordinal()] = 12;
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_WEATHER_STANDARD.ordinal()] = 13;
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.ordinal()] = 14;
            $EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.UAPP_STOPWATCH.ordinal()] = 15;
        }
        */
    }

    @DexIgnore
    public MicroApp(String str, String str2, String str3, String str4, ArrayList<String> arrayList, String str5, String str6, String str7) {
        kd4.b(str, "id");
        kd4.b(str2, "name");
        kd4.b(str3, "nameKey");
        kd4.b(str4, "serialNumber");
        kd4.b(arrayList, "categories");
        kd4.b(str5, "description");
        kd4.b(str6, "descriptionKey");
        this.id = str;
        this.name = str2;
        this.nameKey = str3;
        this.serialNumber = str4;
        this.categories = arrayList;
        this.description = str5;
        this.descriptionKey = str6;
        this.icon = str7;
    }

    @DexIgnore
    public static /* synthetic */ MicroApp copy$default(MicroApp microApp, String str, String str2, String str3, String str4, ArrayList arrayList, String str5, String str6, String str7, int i, Object obj) {
        MicroApp microApp2 = microApp;
        int i2 = i;
        return microApp.copy((i2 & 1) != 0 ? microApp2.id : str, (i2 & 2) != 0 ? microApp2.name : str2, (i2 & 4) != 0 ? microApp2.nameKey : str3, (i2 & 8) != 0 ? microApp2.serialNumber : str4, (i2 & 16) != 0 ? microApp2.categories : arrayList, (i2 & 32) != 0 ? microApp2.description : str5, (i2 & 64) != 0 ? microApp2.descriptionKey : str6, (i2 & 128) != 0 ? microApp2.icon : str7);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final String component3() {
        return this.nameKey;
    }

    @DexIgnore
    public final String component4() {
        return this.serialNumber;
    }

    @DexIgnore
    public final ArrayList<String> component5() {
        return this.categories;
    }

    @DexIgnore
    public final String component6() {
        return this.description;
    }

    @DexIgnore
    public final String component7() {
        return this.descriptionKey;
    }

    @DexIgnore
    public final String component8() {
        return this.icon;
    }

    @DexIgnore
    public final MicroApp copy(String str, String str2, String str3, String str4, ArrayList<String> arrayList, String str5, String str6, String str7) {
        kd4.b(str, "id");
        kd4.b(str2, "name");
        kd4.b(str3, "nameKey");
        kd4.b(str4, "serialNumber");
        kd4.b(arrayList, "categories");
        String str8 = str5;
        kd4.b(str8, "description");
        String str9 = str6;
        kd4.b(str9, "descriptionKey");
        return new MicroApp(str, str2, str3, str4, arrayList, str8, str9, str7);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MicroApp)) {
            return false;
        }
        MicroApp microApp = (MicroApp) obj;
        return kd4.a((Object) this.id, (Object) microApp.id) && kd4.a((Object) this.name, (Object) microApp.name) && kd4.a((Object) this.nameKey, (Object) microApp.nameKey) && kd4.a((Object) this.serialNumber, (Object) microApp.serialNumber) && kd4.a((Object) this.categories, (Object) microApp.categories) && kd4.a((Object) this.description, (Object) microApp.description) && kd4.a((Object) this.descriptionKey, (Object) microApp.descriptionKey) && kd4.a((Object) this.icon, (Object) microApp.icon);
    }

    @DexIgnore
    public final ArrayList<String> getCategories() {
        return this.categories;
    }

    @DexIgnore
    public final int getDefaultIconId() {
        switch (WhenMappings.$EnumSwitchMapping$Anon0[MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.id).ordinal()]) {
            case 1:
                return R.drawable.ic_modetoggle;
            case 2:
                return R.drawable.ic_shortcuts_photo;
            case 3:
                return R.drawable.ic_shortcuts_activity;
            case 4:
                return R.drawable.ic_shortcuts_alarm;
            case 5:
                return R.drawable.ic_shortcuts_lastalert;
            case 6:
                return R.drawable.ic_shortcuts_date;
            case 7:
                return R.drawable.ic_shortcuts_commute;
            case 8:
                return R.drawable.ic_shortcuts_goals;
            case 9:
                return R.drawable.ic_shortcuts_music;
            case 10:
                return R.drawable.ic_shortcuts_volumedown;
            case 11:
                return R.drawable.ic_shortcuts_volumeup;
            case 12:
                return R.drawable.ic_shortcuts_2tz;
            case 13:
                return R.drawable.ic_shortcuts_weather;
            case 14:
                return R.drawable.ic_shortcuts_ring;
            case 15:
                return R.drawable.ic_shortcuts_stopwatch;
            default:
                return R.drawable.ic_modetoggle;
        }
    }

    @DexIgnore
    public final String getDescription() {
        return this.description;
    }

    @DexIgnore
    public final String getDescriptionKey() {
        return this.descriptionKey;
    }

    @DexIgnore
    public final String getIcon() {
        return this.icon;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getNameKey() {
        return this.nameKey;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.nameKey;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.serialNumber;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        ArrayList<String> arrayList = this.categories;
        int hashCode5 = (hashCode4 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        String str5 = this.description;
        int hashCode6 = (hashCode5 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.descriptionKey;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.icon;
        if (str7 != null) {
            i = str7.hashCode();
        }
        return hashCode7 + i;
    }

    @DexIgnore
    public final void setCategories(ArrayList<String> arrayList) {
        kd4.b(arrayList, "<set-?>");
        this.categories = arrayList;
    }

    @DexIgnore
    public final void setDescription(String str) {
        kd4.b(str, "<set-?>");
        this.description = str;
    }

    @DexIgnore
    public final void setDescriptionKey(String str) {
        kd4.b(str, "<set-?>");
        this.descriptionKey = str;
    }

    @DexIgnore
    public final void setIcon(String str) {
        this.icon = str;
    }

    @DexIgnore
    public final void setId(String str) {
        kd4.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        kd4.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setNameKey(String str) {
        kd4.b(str, "<set-?>");
        this.nameKey = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        kd4.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public String toString() {
        return "MicroApp(id=" + this.id + ", name=" + this.name + ", nameKey=" + this.nameKey + ", serialNumber=" + this.serialNumber + ", categories=" + this.categories + ", description=" + this.description + ", descriptionKey=" + this.descriptionKey + ", icon=" + this.icon + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MicroApp(String str, String str2, String str3, String str4, ArrayList arrayList, String str5, String str6, String str7, int i, fd4 fd4) {
        this(str, str2, str3, str4, arrayList, str5, str6, (i & 128) != 0 ? "" : str7);
    }
}
