package com.portfolio.platform.data.model;

import com.fossil.blesdk.obfuscated.f02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class Range {
    @DexIgnore
    @f02("hasNext")
    public boolean hasNext;
    @DexIgnore
    @f02("limit")
    public int limit;
    @DexIgnore
    @f02("offset")
    public int offset;

    @DexIgnore
    public int getLimit() {
        return this.limit;
    }

    @DexIgnore
    public int getOffset() {
        return this.offset;
    }

    @DexIgnore
    public boolean isHasNext() {
        return this.hasNext;
    }

    @DexIgnore
    public void setHasNext(boolean z) {
        this.hasNext = z;
    }

    @DexIgnore
    public void setOffset(int i) {
        this.offset = i;
    }
}
