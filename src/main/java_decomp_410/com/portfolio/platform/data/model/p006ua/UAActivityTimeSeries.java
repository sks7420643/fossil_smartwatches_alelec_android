package com.portfolio.platform.data.model.p006ua;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.model.ua.UAActivityTimeSeries */
public final class UAActivityTimeSeries {
    @DexIgnore
    public java.util.Map<java.lang.Long, java.lang.Double> caloriesMap;
    @DexIgnore
    public java.util.Map<java.lang.Long, java.lang.Double> distanceMap;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.xz1 extraJsonObject;
    @DexIgnore
    public java.lang.String mExternalId;
    @DexIgnore
    public java.util.Map<java.lang.Long, java.lang.Integer> stepsMap;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.model.ua.UAActivityTimeSeries$Builder")
    /* renamed from: com.portfolio.platform.data.model.ua.UAActivityTimeSeries$Builder */
    public static final class Builder {
        @DexIgnore
        public java.util.Map<java.lang.Long, java.lang.Double> caloriesMap;
        @DexIgnore
        public java.util.Map<java.lang.Long, java.lang.Double> distanceMap;
        @DexIgnore
        public java.lang.String mExternalId;
        @DexIgnore
        public java.util.Map<java.lang.Long, java.lang.Integer> stepsMap;

        @DexIgnore
        public final void addCalories(long j, double d) {
            if (this.caloriesMap == null) {
                this.caloriesMap = new java.util.HashMap();
            }
            java.util.Map<java.lang.Long, java.lang.Double> map = this.caloriesMap;
            if (map != null) {
                ((java.util.HashMap) map).put(java.lang.Long.valueOf(j), java.lang.Double.valueOf(d));
                return;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
        }

        @DexIgnore
        public final void addDistance(long j, double d) {
            if (this.distanceMap == null) {
                this.distanceMap = new java.util.HashMap();
            }
            java.util.Map<java.lang.Long, java.lang.Double> map = this.distanceMap;
            if (map != null) {
                ((java.util.HashMap) map).put(java.lang.Long.valueOf(j), java.lang.Double.valueOf(d));
                return;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
        }

        @DexIgnore
        public final void addSteps(long j, int i) {
            if (this.stepsMap == null) {
                this.stepsMap = new java.util.HashMap();
            }
            java.util.Map<java.lang.Long, java.lang.Integer> map = this.stepsMap;
            if (map != null) {
                ((java.util.HashMap) map).put(java.lang.Long.valueOf(j), java.lang.Integer.valueOf(i));
                return;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Int>");
        }

        @DexIgnore
        public final com.portfolio.platform.data.model.p006ua.UAActivityTimeSeries build() {
            if (this.mExternalId == null) {
                this.mExternalId = java.util.UUID.randomUUID().toString();
            }
            return new com.portfolio.platform.data.model.p006ua.UAActivityTimeSeries(this);
        }

        @DexIgnore
        public final java.util.Map<java.lang.Long, java.lang.Double> getCaloriesMap() {
            return this.caloriesMap;
        }

        @DexIgnore
        public final java.util.Map<java.lang.Long, java.lang.Double> getDistanceMap() {
            return this.distanceMap;
        }

        @DexIgnore
        public final java.lang.String getMExternalId() {
            return this.mExternalId;
        }

        @DexIgnore
        public final java.util.Map<java.lang.Long, java.lang.Integer> getStepsMap() {
            return this.stepsMap;
        }

        @DexIgnore
        public final void setCaloriesMap(java.util.Map<java.lang.Long, java.lang.Double> map) {
            this.caloriesMap = map;
        }

        @DexIgnore
        public final void setDistanceMap(java.util.Map<java.lang.Long, java.lang.Double> map) {
            this.distanceMap = map;
        }

        @DexIgnore
        public final void setExternalId(java.lang.String str) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(str, com.misfit.frameworks.common.constants.Constants.PROFILE_KEY_EXTERNALID);
            this.mExternalId = str;
        }

        @DexIgnore
        public final void setMExternalId(java.lang.String str) {
            this.mExternalId = str;
        }

        @DexIgnore
        public final void setStepsMap(java.util.Map<java.lang.Long, java.lang.Integer> map) {
            this.stepsMap = map;
        }
    }

    @DexIgnore
    public UAActivityTimeSeries(java.lang.String str, java.util.Map<java.lang.Long, java.lang.Integer> map, java.util.Map<java.lang.Long, java.lang.Double> map2, java.util.Map<java.lang.Long, java.lang.Double> map3) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "mExternalId");
        this.mExternalId = str;
        this.stepsMap = map;
        this.distanceMap = map2;
        this.caloriesMap = map3;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.xz1 getExtraJsonObject() {
        return this.extraJsonObject;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.xz1 getJsonData() {
        com.fossil.blesdk.obfuscated.xz1 xz1 = new com.fossil.blesdk.obfuscated.xz1();
        xz1.mo17937a("external_id", (com.google.gson.JsonElement) new com.fossil.blesdk.obfuscated.zz1(this.mExternalId));
        com.fossil.blesdk.obfuscated.xz1 xz12 = new com.fossil.blesdk.obfuscated.xz1();
        if (this.stepsMap != null) {
            com.fossil.blesdk.obfuscated.xz1 xz13 = new com.fossil.blesdk.obfuscated.xz1();
            xz13.mo17937a("interval", (com.google.gson.JsonElement) new com.fossil.blesdk.obfuscated.zz1((java.lang.Number) 100));
            com.google.gson.Gson gson = new com.google.gson.Gson();
            java.util.Map<java.lang.Long, java.lang.Integer> map = this.stepsMap;
            if (map != null) {
                xz13.mo17937a("values", (com.google.gson.JsonElement) gson.mo23093a(com.fossil.blesdk.obfuscated.qf4.m26948a(com.fossil.blesdk.obfuscated.qf4.m26948a(com.fossil.blesdk.obfuscated.sb4.m27725a((java.util.HashMap) map).toString(), "(", "[", false), ")", "]", false), com.fossil.blesdk.obfuscated.tz1.class));
                xz12.mo17937a("steps", (com.google.gson.JsonElement) xz13);
            } else {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Int>");
            }
        }
        if (this.distanceMap != null) {
            com.fossil.blesdk.obfuscated.xz1 xz14 = new com.fossil.blesdk.obfuscated.xz1();
            xz14.mo17937a("interval", (com.google.gson.JsonElement) new com.fossil.blesdk.obfuscated.zz1((java.lang.Number) 100));
            com.google.gson.Gson gson2 = new com.google.gson.Gson();
            java.util.Map<java.lang.Long, java.lang.Double> map2 = this.distanceMap;
            if (map2 != null) {
                xz14.mo17937a("values", (com.google.gson.JsonElement) gson2.mo23093a(com.fossil.blesdk.obfuscated.qf4.m26948a(com.fossil.blesdk.obfuscated.qf4.m26948a(com.fossil.blesdk.obfuscated.sb4.m27725a((java.util.HashMap) map2).toString(), "(", "[", false), ")", "]", false), com.fossil.blesdk.obfuscated.tz1.class));
                xz12.mo17937a("distance", (com.google.gson.JsonElement) xz14);
            } else {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
            }
        }
        if (this.caloriesMap != null) {
            com.fossil.blesdk.obfuscated.xz1 xz15 = new com.fossil.blesdk.obfuscated.xz1();
            xz15.mo17937a("interval", (com.google.gson.JsonElement) new com.fossil.blesdk.obfuscated.zz1((java.lang.Number) 100));
            com.google.gson.Gson gson3 = new com.google.gson.Gson();
            java.util.Map<java.lang.Long, java.lang.Double> map3 = this.caloriesMap;
            if (map3 != null) {
                xz15.mo17937a("values", (com.google.gson.JsonElement) gson3.mo23093a(com.fossil.blesdk.obfuscated.qf4.m26948a(com.fossil.blesdk.obfuscated.qf4.m26948a(com.fossil.blesdk.obfuscated.sb4.m27725a((java.util.HashMap) map3).toString(), "(", "[", false), ")", "]", false), com.fossil.blesdk.obfuscated.tz1.class));
                xz12.mo17937a("energy_expended", (com.google.gson.JsonElement) xz15);
            } else {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
            }
        }
        xz1.mo17937a("time_series", (com.google.gson.JsonElement) xz12);
        if (this.extraJsonObject != null) {
            com.fossil.blesdk.obfuscated.tz1 tz1 = new com.fossil.blesdk.obfuscated.tz1();
            tz1.mo16612a((com.google.gson.JsonElement) this.extraJsonObject);
            com.fossil.blesdk.obfuscated.xz1 xz16 = new com.fossil.blesdk.obfuscated.xz1();
            xz16.mo17937a("data_source", (com.google.gson.JsonElement) tz1);
            xz1.mo17937a("_links", (com.google.gson.JsonElement) xz16);
        }
        return xz1;
    }

    @DexIgnore
    public final void setExtraJsonObject(com.fossil.blesdk.obfuscated.xz1 xz1) {
        this.extraJsonObject = xz1;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public UAActivityTimeSeries(com.portfolio.platform.data.model.p006ua.UAActivityTimeSeries.Builder builder) {
        this(r0, builder.getStepsMap(), builder.getDistanceMap(), builder.getCaloriesMap());
        com.fossil.blesdk.obfuscated.kd4.m24411b(builder, "builder");
        java.lang.String mExternalId2 = builder.getMExternalId();
        if (mExternalId2 != null) {
        } else {
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        }
    }
}
