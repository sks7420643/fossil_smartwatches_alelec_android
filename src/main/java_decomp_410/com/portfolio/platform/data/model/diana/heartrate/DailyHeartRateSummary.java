package com.portfolio.platform.data.model.diana.heartrate;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.PlaceManager;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import java.io.Serializable;
import java.util.Date;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DailyHeartRateSummary implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    public float average;
    @DexIgnore
    public Integer avgRestingHeartRateOfWeek;
    @DexIgnore
    public long createdAt;
    @DexIgnore
    public Date date;
    @DexIgnore
    public int max;
    @DexIgnore
    public int min;
    @DexIgnore
    public int minuteCount;
    @DexIgnore
    public Resting resting;
    @DexIgnore
    public long updatedAt;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<HeartRateSample> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public HeartRateSample createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new HeartRateSample(parcel);
        }

        @DexIgnore
        public HeartRateSample[] newArray(int i) {
            return new HeartRateSample[i];
        }
    }

    @DexIgnore
    public DailyHeartRateSummary(float f, Date date2, long j, long j2, int i, int i2, int i3, Resting resting2) {
        kd4.b(date2, "date");
        this.average = f;
        this.date = date2;
        this.createdAt = j;
        this.updatedAt = j2;
        this.min = i;
        this.max = i2;
        this.minuteCount = i3;
        this.resting = resting2;
    }

    @DexIgnore
    public static /* synthetic */ DailyHeartRateSummary copy$default(DailyHeartRateSummary dailyHeartRateSummary, float f, Date date2, long j, long j2, int i, int i2, int i3, Resting resting2, int i4, Object obj) {
        DailyHeartRateSummary dailyHeartRateSummary2 = dailyHeartRateSummary;
        int i5 = i4;
        return dailyHeartRateSummary.copy((i5 & 1) != 0 ? dailyHeartRateSummary2.average : f, (i5 & 2) != 0 ? dailyHeartRateSummary2.date : date2, (i5 & 4) != 0 ? dailyHeartRateSummary2.createdAt : j, (i5 & 8) != 0 ? dailyHeartRateSummary2.updatedAt : j2, (i5 & 16) != 0 ? dailyHeartRateSummary2.min : i, (i5 & 32) != 0 ? dailyHeartRateSummary2.max : i2, (i5 & 64) != 0 ? dailyHeartRateSummary2.minuteCount : i3, (i5 & 128) != 0 ? dailyHeartRateSummary2.resting : resting2);
    }

    @DexIgnore
    public final float component1() {
        return this.average;
    }

    @DexIgnore
    public final Date component2() {
        return this.date;
    }

    @DexIgnore
    public final long component3() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component4() {
        return this.updatedAt;
    }

    @DexIgnore
    public final int component5() {
        return this.min;
    }

    @DexIgnore
    public final int component6() {
        return this.max;
    }

    @DexIgnore
    public final int component7() {
        return this.minuteCount;
    }

    @DexIgnore
    public final Resting component8() {
        return this.resting;
    }

    @DexIgnore
    public final DailyHeartRateSummary copy(float f, Date date2, long j, long j2, int i, int i2, int i3, Resting resting2) {
        kd4.b(date2, "date");
        return new DailyHeartRateSummary(f, date2, j, j2, i, i2, i3, resting2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DailyHeartRateSummary) {
                DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) obj;
                if (Float.compare(this.average, dailyHeartRateSummary.average) == 0 && kd4.a((Object) this.date, (Object) dailyHeartRateSummary.date)) {
                    if (this.createdAt == dailyHeartRateSummary.createdAt) {
                        if (this.updatedAt == dailyHeartRateSummary.updatedAt) {
                            if (this.min == dailyHeartRateSummary.min) {
                                if (this.max == dailyHeartRateSummary.max) {
                                    if (!(this.minuteCount == dailyHeartRateSummary.minuteCount) || !kd4.a((Object) this.resting, (Object) dailyHeartRateSummary.resting)) {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final float getAverage() {
        return this.average;
    }

    @DexIgnore
    public final Integer getAvgRestingHeartRateOfWeek() {
        return this.avgRestingHeartRateOfWeek;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final int getMax() {
        return this.max;
    }

    @DexIgnore
    public final int getMin() {
        return this.min;
    }

    @DexIgnore
    public final int getMinuteCount() {
        return this.minuteCount;
    }

    @DexIgnore
    public final Resting getResting() {
        return this.resting;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int floatToIntBits = Float.floatToIntBits(this.average) * 31;
        Date date2 = this.date;
        int i = 0;
        int hashCode = date2 != null ? date2.hashCode() : 0;
        long j = this.createdAt;
        long j2 = this.updatedAt;
        int i2 = (((((((((((floatToIntBits + hashCode) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.min) * 31) + this.max) * 31) + this.minuteCount) * 31;
        Resting resting2 = this.resting;
        if (resting2 != null) {
            i = resting2.hashCode();
        }
        return i2 + i;
    }

    @DexIgnore
    public final void setAverage(float f) {
        this.average = f;
    }

    @DexIgnore
    public final void setAvgRestingHeartRateOfWeek(Integer num) {
        this.avgRestingHeartRateOfWeek = num;
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        kd4.b(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setMax(int i) {
        this.max = i;
    }

    @DexIgnore
    public final void setMin(int i) {
        this.min = i;
    }

    @DexIgnore
    public final void setMinuteCount(int i) {
        this.minuteCount = i;
    }

    @DexIgnore
    public final void setResting(Resting resting2) {
        this.resting = resting2;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "average=" + this.average + ", date=" + this.date + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", min=" + this.min + ", max=" + this.max + ", minuteCount=" + this.minuteCount + ", resting=" + this.resting;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeFloat(this.average);
        parcel.writeSerializable(this.date);
        parcel.writeLong(this.createdAt);
        parcel.writeLong(this.updatedAt);
        parcel.writeInt(this.min);
        parcel.writeInt(this.max);
        parcel.writeInt(this.minuteCount);
        Resting resting2 = this.resting;
        if (resting2 != null) {
            parcel.writeSerializable(resting2);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ DailyHeartRateSummary(float f, Date date2, long j, long j2, int i, int i2, int i3, Resting resting2, int i4, fd4 fd4) {
        this(f, date2, j, j2, i, i2, i3, (i4 & 128) != 0 ? null : resting2);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DailyHeartRateSummary(DailyHeartRateSummary dailyHeartRateSummary) {
        this(dailyHeartRateSummary.average, dailyHeartRateSummary.date, dailyHeartRateSummary.createdAt, dailyHeartRateSummary.updatedAt, dailyHeartRateSummary.min, dailyHeartRateSummary.max, dailyHeartRateSummary.minuteCount, dailyHeartRateSummary.resting);
        kd4.b(dailyHeartRateSummary, PlaceManager.PARAM_SUMMARY);
        this.avgRestingHeartRateOfWeek = dailyHeartRateSummary.avgRestingHeartRateOfWeek;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public DailyHeartRateSummary(Parcel parcel) {
        throw null;
/*        this(r2, (Date) r0, parcel.readLong(), parcel.readLong(), parcel.readInt(), parcel.readInt(), parcel.readInt(), (Resting) parcel.readSerializable());
        kd4.b(parcel, "parcel");
        float readFloat = parcel.readFloat();
        Serializable readSerializable = parcel.readSerializable();
        if (readSerializable != null) {
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.util.Date");
*/    }
}
