package com.portfolio.platform.data.model.diana.workout;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper;
import com.portfolio.platform.data.model.fitnessdata.WorkoutSessionWrapper;
import com.portfolio.platform.data.model.fitnessdata.WorkoutStateChangeWrapper;
import com.portfolio.platform.enums.WorkoutSourceType;
import com.portfolio.platform.enums.WorkoutState;
import com.portfolio.platform.enums.WorkoutType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSession {
    @DexIgnore
    public /* final */ WorkoutCalorie calorie;
    @DexIgnore
    public /* final */ long createdAt;
    @DexIgnore
    public /* final */ Date date;
    @DexIgnore
    public /* final */ String deviceSerialNumber;
    @DexIgnore
    public /* final */ WorkoutDistance distance;
    @DexIgnore
    public /* final */ int duration;
    @DexIgnore
    public /* final */ DateTime endTime;
    @DexIgnore
    public /* final */ WorkoutHeartRate heartRate;
    @DexIgnore
    public /* final */ String id;
    @DexIgnore
    public WorkoutLocation location;
    @DexIgnore
    public /* final */ WorkoutSourceType sourceType;
    @DexIgnore
    public WorkoutSpeed speed;
    @DexIgnore
    public /* final */ DateTime startTime;
    @DexIgnore
    public List<WorkoutStateChange> states;
    @DexIgnore
    public /* final */ WorkoutStep step;
    @DexIgnore
    public /* final */ int timezoneOffsetInSecond;
    @DexIgnore
    public /* final */ long updatedAt;
    @DexIgnore
    public /* final */ WorkoutType workoutType;

    @DexIgnore
    public WorkoutSession(String str, Date date2, DateTime dateTime, DateTime dateTime2, String str2, WorkoutStep workoutStep, WorkoutCalorie workoutCalorie, WorkoutDistance workoutDistance, WorkoutHeartRate workoutHeartRate, WorkoutSourceType workoutSourceType, WorkoutType workoutType2, int i, int i2, long j, long j2) {
        kd4.b(str, "id");
        kd4.b(date2, "date");
        kd4.b(dateTime, SampleRaw.COLUMN_START_TIME);
        kd4.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        this.id = str;
        this.date = date2;
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.deviceSerialNumber = str2;
        this.step = workoutStep;
        this.calorie = workoutCalorie;
        this.distance = workoutDistance;
        this.heartRate = workoutHeartRate;
        this.sourceType = workoutSourceType;
        this.workoutType = workoutType2;
        this.timezoneOffsetInSecond = i;
        this.duration = i2;
        this.createdAt = j;
        this.updatedAt = j2;
        WorkoutDistance workoutDistance2 = this.distance;
        this.speed = workoutDistance2 != null ? workoutDistance2.calculateSpeed() : null;
        this.states = new ArrayList();
    }

    @DexIgnore
    public static /* synthetic */ WorkoutSession copy$default(WorkoutSession workoutSession, String str, Date date2, DateTime dateTime, DateTime dateTime2, String str2, WorkoutStep workoutStep, WorkoutCalorie workoutCalorie, WorkoutDistance workoutDistance, WorkoutHeartRate workoutHeartRate, WorkoutSourceType workoutSourceType, WorkoutType workoutType2, int i, int i2, long j, long j2, int i3, Object obj) {
        WorkoutSession workoutSession2 = workoutSession;
        int i4 = i3;
        return workoutSession.copy((i4 & 1) != 0 ? workoutSession2.id : str, (i4 & 2) != 0 ? workoutSession2.date : date2, (i4 & 4) != 0 ? workoutSession2.startTime : dateTime, (i4 & 8) != 0 ? workoutSession2.endTime : dateTime2, (i4 & 16) != 0 ? workoutSession2.deviceSerialNumber : str2, (i4 & 32) != 0 ? workoutSession2.step : workoutStep, (i4 & 64) != 0 ? workoutSession2.calorie : workoutCalorie, (i4 & 128) != 0 ? workoutSession2.distance : workoutDistance, (i4 & 256) != 0 ? workoutSession2.heartRate : workoutHeartRate, (i4 & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? workoutSession2.sourceType : workoutSourceType, (i4 & 1024) != 0 ? workoutSession2.workoutType : workoutType2, (i4 & 2048) != 0 ? workoutSession2.timezoneOffsetInSecond : i, (i4 & 4096) != 0 ? workoutSession2.duration : i2, (i4 & 8192) != 0 ? workoutSession2.createdAt : j, (i4 & RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE) != 0 ? workoutSession2.updatedAt : j2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final WorkoutSourceType component10() {
        return this.sourceType;
    }

    @DexIgnore
    public final WorkoutType component11() {
        return this.workoutType;
    }

    @DexIgnore
    public final int component12() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int component13() {
        return this.duration;
    }

    @DexIgnore
    public final long component14() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component15() {
        return this.updatedAt;
    }

    @DexIgnore
    public final Date component2() {
        return this.date;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component4() {
        return this.endTime;
    }

    @DexIgnore
    public final String component5() {
        return this.deviceSerialNumber;
    }

    @DexIgnore
    public final WorkoutStep component6() {
        return this.step;
    }

    @DexIgnore
    public final WorkoutCalorie component7() {
        return this.calorie;
    }

    @DexIgnore
    public final WorkoutDistance component8() {
        return this.distance;
    }

    @DexIgnore
    public final WorkoutHeartRate component9() {
        return this.heartRate;
    }

    @DexIgnore
    public final WorkoutSession copy(String str, Date date2, DateTime dateTime, DateTime dateTime2, String str2, WorkoutStep workoutStep, WorkoutCalorie workoutCalorie, WorkoutDistance workoutDistance, WorkoutHeartRate workoutHeartRate, WorkoutSourceType workoutSourceType, WorkoutType workoutType2, int i, int i2, long j, long j2) {
        String str3 = str;
        kd4.b(str3, "id");
        kd4.b(date2, "date");
        kd4.b(dateTime, SampleRaw.COLUMN_START_TIME);
        kd4.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        return new WorkoutSession(str3, date2, dateTime, dateTime2, str2, workoutStep, workoutCalorie, workoutDistance, workoutHeartRate, workoutSourceType, workoutType2, i, i2, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutSession) {
                WorkoutSession workoutSession = (WorkoutSession) obj;
                if (kd4.a((Object) this.id, (Object) workoutSession.id) && kd4.a((Object) this.date, (Object) workoutSession.date) && kd4.a((Object) this.startTime, (Object) workoutSession.startTime) && kd4.a((Object) this.endTime, (Object) workoutSession.endTime) && kd4.a((Object) this.deviceSerialNumber, (Object) workoutSession.deviceSerialNumber) && kd4.a((Object) this.step, (Object) workoutSession.step) && kd4.a((Object) this.calorie, (Object) workoutSession.calorie) && kd4.a((Object) this.distance, (Object) workoutSession.distance) && kd4.a((Object) this.heartRate, (Object) workoutSession.heartRate) && kd4.a((Object) this.sourceType, (Object) workoutSession.sourceType) && kd4.a((Object) this.workoutType, (Object) workoutSession.workoutType)) {
                    if (this.timezoneOffsetInSecond == workoutSession.timezoneOffsetInSecond) {
                        if (this.duration == workoutSession.duration) {
                            if (this.createdAt == workoutSession.createdAt) {
                                if (this.updatedAt == workoutSession.updatedAt) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Float getAverageHeartRate() {
        WorkoutHeartRate workoutHeartRate = this.heartRate;
        if (workoutHeartRate != null) {
            return Float.valueOf(workoutHeartRate.getAverage());
        }
        return null;
    }

    @DexIgnore
    public final WorkoutCalorie getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }

    @DexIgnore
    public final WorkoutDistance getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final int getDuration() {
        return this.duration;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final WorkoutHeartRate getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final WorkoutLocation getLocation() {
        return this.location;
    }

    @DexIgnore
    public final Integer getMaxHeartRate() {
        WorkoutHeartRate workoutHeartRate = this.heartRate;
        if (workoutHeartRate != null) {
            return Integer.valueOf(workoutHeartRate.getMax());
        }
        return null;
    }

    @DexIgnore
    public final WorkoutSourceType getSourceType() {
        return this.sourceType;
    }

    @DexIgnore
    public final WorkoutSpeed getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final List<WorkoutStateChange> getStates() {
        return this.states;
    }

    @DexIgnore
    public final WorkoutStep getStep() {
        return this.step;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final Float getTotalCalorie() {
        WorkoutCalorie workoutCalorie = this.calorie;
        if (workoutCalorie != null) {
            return Float.valueOf(workoutCalorie.getTotal());
        }
        return null;
    }

    @DexIgnore
    public final Double getTotalDistance() {
        WorkoutDistance workoutDistance = this.distance;
        if (workoutDistance != null) {
            return Double.valueOf(workoutDistance.getTotal());
        }
        return null;
    }

    @DexIgnore
    public final Integer getTotalSteps() {
        WorkoutStep workoutStep = this.step;
        if (workoutStep != null) {
            return Integer.valueOf(workoutStep.getTotal());
        }
        return null;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final WorkoutType getWorkoutType() {
        return this.workoutType;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Date date2 = this.date;
        int hashCode2 = (hashCode + (date2 != null ? date2.hashCode() : 0)) * 31;
        DateTime dateTime = this.startTime;
        int hashCode3 = (hashCode2 + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.endTime;
        int hashCode4 = (hashCode3 + (dateTime2 != null ? dateTime2.hashCode() : 0)) * 31;
        String str2 = this.deviceSerialNumber;
        int hashCode5 = (hashCode4 + (str2 != null ? str2.hashCode() : 0)) * 31;
        WorkoutStep workoutStep = this.step;
        int hashCode6 = (hashCode5 + (workoutStep != null ? workoutStep.hashCode() : 0)) * 31;
        WorkoutCalorie workoutCalorie = this.calorie;
        int hashCode7 = (hashCode6 + (workoutCalorie != null ? workoutCalorie.hashCode() : 0)) * 31;
        WorkoutDistance workoutDistance = this.distance;
        int hashCode8 = (hashCode7 + (workoutDistance != null ? workoutDistance.hashCode() : 0)) * 31;
        WorkoutHeartRate workoutHeartRate = this.heartRate;
        int hashCode9 = (hashCode8 + (workoutHeartRate != null ? workoutHeartRate.hashCode() : 0)) * 31;
        WorkoutSourceType workoutSourceType = this.sourceType;
        int hashCode10 = (hashCode9 + (workoutSourceType != null ? workoutSourceType.hashCode() : 0)) * 31;
        WorkoutType workoutType2 = this.workoutType;
        if (workoutType2 != null) {
            i = workoutType2.hashCode();
        }
        long j = this.createdAt;
        long j2 = this.updatedAt;
        return ((((((((hashCode10 + i) * 31) + this.timezoneOffsetInSecond) * 31) + this.duration) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)));
    }

    @DexIgnore
    public final void setLocation(WorkoutLocation workoutLocation) {
        this.location = workoutLocation;
    }

    @DexIgnore
    public final void setSpeed(WorkoutSpeed workoutSpeed) {
        this.speed = workoutSpeed;
    }

    @DexIgnore
    public final void setStates(List<WorkoutStateChange> list) {
        kd4.b(list, "<set-?>");
        this.states = list;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSession(id=" + this.id + ", date=" + this.date + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", deviceSerialNumber=" + this.deviceSerialNumber + ", step=" + this.step + ", calorie=" + this.calorie + ", distance=" + this.distance + ", heartRate=" + this.heartRate + ", sourceType=" + this.sourceType + ", workoutType=" + this.workoutType + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", duration=" + this.duration + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public WorkoutSession(WorkoutSessionWrapper workoutSessionWrapper, String str, String str2) {
        throw null;
/*        this(r4, r5, r6, r7, str, r9, r10, r11, r0 != null ? new WorkoutHeartRate(r0) : null, WorkoutSourceType.DEVICE, WorkoutType.Companion.a(workoutSessionWrapper.getType()), workoutSessionWrapper.getTimezoneOffsetInSecond(), workoutSessionWrapper.getDuration(), System.currentTimeMillis(), System.currentTimeMillis());
        String str3 = str2;
        kd4.b(workoutSessionWrapper, "workoutSession");
        kd4.b(str, "serialNumber");
        kd4.b(str3, ButtonService.USER_ID);
        String str4 = str3 + ":device:" + (workoutSessionWrapper.getStartTime().getMillis() / ((long) 1000));
        Date date2 = workoutSessionWrapper.getStartTime().toLocalDateTime().toDate();
        kd4.a((Object) date2, "workoutSession.startTime\u2026oLocalDateTime().toDate()");
        DateTime startTime2 = workoutSessionWrapper.getStartTime();
        DateTime endTime2 = workoutSessionWrapper.getEndTime();
        WorkoutStep workoutStep = new WorkoutStep(workoutSessionWrapper.getStep());
        WorkoutCalorie workoutCalorie = new WorkoutCalorie(workoutSessionWrapper.getCalorie());
        WorkoutDistance workoutDistance = new WorkoutDistance(workoutSessionWrapper.getDistance());
        HeartRateWrapper heartRate2 = workoutSessionWrapper.getHeartRate();
        for (WorkoutStateChangeWrapper workoutStateChangeWrapper : workoutSessionWrapper.getStateChanges()) {
            this.states.add(new WorkoutStateChange(WorkoutState.Companion.a(workoutStateChangeWrapper.getState()), new Date(((long) workoutStateChangeWrapper.getIndexInSecond()) * 1000)));
        }
*/    }
}
