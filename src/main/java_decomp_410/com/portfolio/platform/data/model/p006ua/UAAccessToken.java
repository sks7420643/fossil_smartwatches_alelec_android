package com.portfolio.platform.data.model.p006ua;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.model.ua.UAAccessToken */
public final class UAAccessToken {
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("access_token")
    public java.lang.String accessToken;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("expires_at")
    public java.lang.Long expiresAt;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("expires_in")
    public java.lang.Long expiresIn;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("refresh_token")
    public java.lang.String refreshToken;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("scope")
    public java.lang.String scope;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("token_type")
    public java.lang.String tokenType;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("user_href")
    public java.lang.String userHref;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("user_id")
    public java.lang.String userId;

    @DexIgnore
    public final java.lang.String getAccessToken() {
        return this.accessToken;
    }

    @DexIgnore
    public final java.lang.Long getExpiresAt() {
        return this.expiresAt;
    }

    @DexIgnore
    public final java.lang.Long getExpiresIn() {
        return this.expiresIn;
    }

    @DexIgnore
    public final java.lang.String getRefreshToken() {
        return this.refreshToken;
    }

    @DexIgnore
    public final java.lang.String getScope() {
        return this.scope;
    }

    @DexIgnore
    public final java.lang.String getTokenType() {
        return this.tokenType;
    }

    @DexIgnore
    public final java.lang.String getUserHref() {
        return this.userHref;
    }

    @DexIgnore
    public final java.lang.String getUserId() {
        return this.userId;
    }

    @DexIgnore
    public final void setAccessToken(java.lang.String str) {
        this.accessToken = str;
    }

    @DexIgnore
    public final void setExpiresAt(java.lang.Long l) {
        this.expiresAt = l;
    }

    @DexIgnore
    public final void setExpiresIn(java.lang.Long l) {
        this.expiresIn = l;
    }

    @DexIgnore
    public final void setRefreshToken(java.lang.String str) {
        this.refreshToken = str;
    }

    @DexIgnore
    public final void setScope(java.lang.String str) {
        this.scope = str;
    }

    @DexIgnore
    public final void setTokenType(java.lang.String str) {
        this.tokenType = str;
    }

    @DexIgnore
    public final void setUserHref(java.lang.String str) {
        this.userHref = str;
    }

    @DexIgnore
    public final void setUserId(java.lang.String str) {
        this.userId = str;
    }
}
