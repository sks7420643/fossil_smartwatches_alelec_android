package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.fitness.Calorie;
import java.util.ArrayList;
import java.util.List;
import kotlin.jvm.internal.Lambda;
import kotlin.sequences.SequencesKt___SequencesKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CalorieWrapper {
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public float total;
    @DexIgnore
    public List<Float> values;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends Lambda implements xc4<Byte, Float> {
        @DexIgnore
        public static /* final */ Anon1 INSTANCE; // = new Anon1();

        @DexIgnore
        public Anon1() {
            super(1);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return Float.valueOf(invoke((Byte) obj));
        }

        @DexIgnore
        public final float invoke(Byte b) {
            return (float) b.byteValue();
        }
    }

    @DexIgnore
    public CalorieWrapper(int i, List<Float> list, float f) {
        kd4.b(list, "values");
        this.resolutionInSecond = i;
        this.values = list;
        this.total = f;
    }

    @DexIgnore
    public static /* synthetic */ CalorieWrapper copy$default(CalorieWrapper calorieWrapper, int i, List<Float> list, float f, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = calorieWrapper.resolutionInSecond;
        }
        if ((i2 & 2) != 0) {
            list = calorieWrapper.values;
        }
        if ((i2 & 4) != 0) {
            f = calorieWrapper.total;
        }
        return calorieWrapper.copy(i, list, f);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Float> component2() {
        return this.values;
    }

    @DexIgnore
    public final float component3() {
        return this.total;
    }

    @DexIgnore
    public final CalorieWrapper copy(int i, List<Float> list, float f) {
        kd4.b(list, "values");
        return new CalorieWrapper(i, list, f);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof CalorieWrapper) {
                CalorieWrapper calorieWrapper = (CalorieWrapper) obj;
                if (!(this.resolutionInSecond == calorieWrapper.resolutionInSecond) || !kd4.a((Object) this.values, (Object) calorieWrapper.values) || Float.compare(this.total, calorieWrapper.total) != 0) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final float getTotal() {
        return this.total;
    }

    @DexIgnore
    public final List<Float> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.resolutionInSecond * 31;
        List<Float> list = this.values;
        return ((i + (list != null ? list.hashCode() : 0)) * 31) + Float.floatToIntBits(this.total);
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setTotal(float f) {
        this.total = f;
    }

    @DexIgnore
    public final void setValues(List<Float> list) {
        kd4.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "CalorieWrapper(resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ", total=" + this.total + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public CalorieWrapper(Calorie calorie) {
        throw null;
/*        this(r0, SequencesKt___SequencesKt.g(SequencesKt___SequencesKt.c(kb4.b(r1), Anon1.INSTANCE)), (float) calorie.getTotal());
        kd4.b(calorie, "calorie");
        int resolutionInSecond2 = calorie.getResolutionInSecond();
        ArrayList<Byte> values2 = calorie.getValues();
        kd4.a((Object) values2, "calorie.values");
*/    }
}
