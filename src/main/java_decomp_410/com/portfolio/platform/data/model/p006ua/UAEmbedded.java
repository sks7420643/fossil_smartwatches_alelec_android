package com.portfolio.platform.data.model.p006ua;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.model.ua.UAEmbedded */
public final class UAEmbedded {
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("device")
    public java.util.List<com.portfolio.platform.data.model.p006ua.UADevice> device;

    @DexIgnore
    public final java.util.List<com.portfolio.platform.data.model.p006ua.UADevice> getDevice() {
        return this.device;
    }

    @DexIgnore
    public final void setDevice(java.util.List<com.portfolio.platform.data.model.p006ua.UADevice> list) {
        this.device = list;
    }
}
