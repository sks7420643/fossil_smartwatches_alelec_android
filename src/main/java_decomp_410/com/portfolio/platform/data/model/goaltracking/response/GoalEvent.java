package com.portfolio.platform.data.model.goaltracking.response;

import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalEvent {
    @DexIgnore
    @f02("createdAt")
    public DateTime mCreatedAt;
    @DexIgnore
    @f02("date")
    public Date mDate;
    @DexIgnore
    @f02("id")
    public String mId;
    @DexIgnore
    @f02("timezoneOffset")
    public /* final */ int mTimeZoneOffsetInSecond;
    @DexIgnore
    @f02("trackedAt")
    public /* final */ DateTime mTrackedAt;
    @DexIgnore
    @f02("updatedAt")
    public DateTime mUpdatedAt;

    @DexIgnore
    public GoalEvent(String str, DateTime dateTime, int i, Date date, DateTime dateTime2, DateTime dateTime3) {
        kd4.b(str, "mId");
        kd4.b(dateTime, "mTrackedAt");
        kd4.b(date, "mDate");
        kd4.b(dateTime2, "mCreatedAt");
        kd4.b(dateTime3, "mUpdatedAt");
        this.mId = str;
        this.mTrackedAt = dateTime;
        this.mTimeZoneOffsetInSecond = i;
        this.mDate = date;
        this.mCreatedAt = dateTime2;
        this.mUpdatedAt = dateTime3;
    }

    @DexIgnore
    public static /* synthetic */ GoalEvent copy$default(GoalEvent goalEvent, String str, DateTime dateTime, int i, Date date, DateTime dateTime2, DateTime dateTime3, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = goalEvent.mId;
        }
        if ((i2 & 2) != 0) {
            dateTime = goalEvent.mTrackedAt;
        }
        DateTime dateTime4 = dateTime;
        if ((i2 & 4) != 0) {
            i = goalEvent.mTimeZoneOffsetInSecond;
        }
        int i3 = i;
        if ((i2 & 8) != 0) {
            date = goalEvent.mDate;
        }
        Date date2 = date;
        if ((i2 & 16) != 0) {
            dateTime2 = goalEvent.mCreatedAt;
        }
        DateTime dateTime5 = dateTime2;
        if ((i2 & 32) != 0) {
            dateTime3 = goalEvent.mUpdatedAt;
        }
        return goalEvent.copy(str, dateTime4, i3, date2, dateTime5, dateTime3);
    }

    @DexIgnore
    public final String component1() {
        return this.mId;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.mTrackedAt;
    }

    @DexIgnore
    public final int component3() {
        return this.mTimeZoneOffsetInSecond;
    }

    @DexIgnore
    public final Date component4() {
        return this.mDate;
    }

    @DexIgnore
    public final DateTime component5() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final DateTime component6() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public final GoalEvent copy(String str, DateTime dateTime, int i, Date date, DateTime dateTime2, DateTime dateTime3) {
        kd4.b(str, "mId");
        kd4.b(dateTime, "mTrackedAt");
        kd4.b(date, "mDate");
        kd4.b(dateTime2, "mCreatedAt");
        kd4.b(dateTime3, "mUpdatedAt");
        return new GoalEvent(str, dateTime, i, date, dateTime2, dateTime3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof GoalEvent) {
                GoalEvent goalEvent = (GoalEvent) obj;
                if (kd4.a((Object) this.mId, (Object) goalEvent.mId) && kd4.a((Object) this.mTrackedAt, (Object) goalEvent.mTrackedAt)) {
                    if (!(this.mTimeZoneOffsetInSecond == goalEvent.mTimeZoneOffsetInSecond) || !kd4.a((Object) this.mDate, (Object) goalEvent.mDate) || !kd4.a((Object) this.mCreatedAt, (Object) goalEvent.mCreatedAt) || !kd4.a((Object) this.mUpdatedAt, (Object) goalEvent.mUpdatedAt)) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final DateTime getMCreatedAt() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final Date getMDate() {
        return this.mDate;
    }

    @DexIgnore
    public final String getMId() {
        return this.mId;
    }

    @DexIgnore
    public final int getMTimeZoneOffsetInSecond() {
        return this.mTimeZoneOffsetInSecond;
    }

    @DexIgnore
    public final DateTime getMTrackedAt() {
        return this.mTrackedAt;
    }

    @DexIgnore
    public final DateTime getMUpdatedAt() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.mId;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        DateTime dateTime = this.mTrackedAt;
        int hashCode2 = (((hashCode + (dateTime != null ? dateTime.hashCode() : 0)) * 31) + this.mTimeZoneOffsetInSecond) * 31;
        Date date = this.mDate;
        int hashCode3 = (hashCode2 + (date != null ? date.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.mCreatedAt;
        int hashCode4 = (hashCode3 + (dateTime2 != null ? dateTime2.hashCode() : 0)) * 31;
        DateTime dateTime3 = this.mUpdatedAt;
        if (dateTime3 != null) {
            i = dateTime3.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public final void setMCreatedAt(DateTime dateTime) {
        kd4.b(dateTime, "<set-?>");
        this.mCreatedAt = dateTime;
    }

    @DexIgnore
    public final void setMDate(Date date) {
        kd4.b(date, "<set-?>");
        this.mDate = date;
    }

    @DexIgnore
    public final void setMId(String str) {
        kd4.b(str, "<set-?>");
        this.mId = str;
    }

    @DexIgnore
    public final void setMUpdatedAt(DateTime dateTime) {
        kd4.b(dateTime, "<set-?>");
        this.mUpdatedAt = dateTime;
    }

    @DexIgnore
    public final GoalTrackingData toGoalTrackingData() {
        try {
            String str = this.mId;
            DateTime withZone = this.mTrackedAt.withZone(DateTimeZone.forOffsetMillis(this.mTimeZoneOffsetInSecond * 1000));
            kd4.a((Object) withZone, "mTrackedAt.withZone(Date\u2026neOffsetInSecond * 1000))");
            return new GoalTrackingData(str, withZone, this.mTimeZoneOffsetInSecond, this.mDate, this.mCreatedAt.getMillis(), this.mUpdatedAt.getMillis());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("GoalEvent", "toGoalTrackingData exception=" + e);
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public String toString() {
        return "GoalEvent(mId=" + this.mId + ", mTrackedAt=" + this.mTrackedAt + ", mTimeZoneOffsetInSecond=" + this.mTimeZoneOffsetInSecond + ", mDate=" + this.mDate + ", mCreatedAt=" + this.mCreatedAt + ", mUpdatedAt=" + this.mUpdatedAt + ")";
    }
}
