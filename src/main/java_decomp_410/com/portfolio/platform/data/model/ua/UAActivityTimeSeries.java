package com.portfolio.platform.data.model.ua;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.sb4;
import com.fossil.blesdk.obfuscated.tz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.zz1;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UAActivityTimeSeries {
    @DexIgnore
    public Map<Long, Double> caloriesMap;
    @DexIgnore
    public Map<Long, Double> distanceMap;
    @DexIgnore
    public xz1 extraJsonObject;
    @DexIgnore
    public String mExternalId;
    @DexIgnore
    public Map<Long, Integer> stepsMap;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Builder {
        @DexIgnore
        public Map<Long, Double> caloriesMap;
        @DexIgnore
        public Map<Long, Double> distanceMap;
        @DexIgnore
        public String mExternalId;
        @DexIgnore
        public Map<Long, Integer> stepsMap;

        @DexIgnore
        public final void addCalories(long j, double d) {
            if (this.caloriesMap == null) {
                this.caloriesMap = new HashMap();
            }
            Map<Long, Double> map = this.caloriesMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Double.valueOf(d));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
        }

        @DexIgnore
        public final void addDistance(long j, double d) {
            if (this.distanceMap == null) {
                this.distanceMap = new HashMap();
            }
            Map<Long, Double> map = this.distanceMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Double.valueOf(d));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
        }

        @DexIgnore
        public final void addSteps(long j, int i) {
            if (this.stepsMap == null) {
                this.stepsMap = new HashMap();
            }
            Map<Long, Integer> map = this.stepsMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Integer.valueOf(i));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Int>");
        }

        @DexIgnore
        public final UAActivityTimeSeries build() {
            if (this.mExternalId == null) {
                this.mExternalId = UUID.randomUUID().toString();
            }
            return new UAActivityTimeSeries(this);
        }

        @DexIgnore
        public final Map<Long, Double> getCaloriesMap() {
            return this.caloriesMap;
        }

        @DexIgnore
        public final Map<Long, Double> getDistanceMap() {
            return this.distanceMap;
        }

        @DexIgnore
        public final String getMExternalId() {
            return this.mExternalId;
        }

        @DexIgnore
        public final Map<Long, Integer> getStepsMap() {
            return this.stepsMap;
        }

        @DexIgnore
        public final void setCaloriesMap(Map<Long, Double> map) {
            this.caloriesMap = map;
        }

        @DexIgnore
        public final void setDistanceMap(Map<Long, Double> map) {
            this.distanceMap = map;
        }

        @DexIgnore
        public final void setExternalId(String str) {
            kd4.b(str, Constants.PROFILE_KEY_EXTERNALID);
            this.mExternalId = str;
        }

        @DexIgnore
        public final void setMExternalId(String str) {
            this.mExternalId = str;
        }

        @DexIgnore
        public final void setStepsMap(Map<Long, Integer> map) {
            this.stepsMap = map;
        }
    }

    @DexIgnore
    public UAActivityTimeSeries(String str, Map<Long, Integer> map, Map<Long, Double> map2, Map<Long, Double> map3) {
        kd4.b(str, "mExternalId");
        this.mExternalId = str;
        this.stepsMap = map;
        this.distanceMap = map2;
        this.caloriesMap = map3;
    }

    @DexIgnore
    public final xz1 getExtraJsonObject() {
        return this.extraJsonObject;
    }

    @DexIgnore
    public final xz1 getJsonData() {
        xz1 xz1 = new xz1();
        xz1.a("external_id", (JsonElement) new zz1(this.mExternalId));
        xz1 xz12 = new xz1();
        if (this.stepsMap != null) {
            xz1 xz13 = new xz1();
            xz13.a("interval", (JsonElement) new zz1((Number) 100));
            Gson gson = new Gson();
            Map<Long, Integer> map = this.stepsMap;
            if (map != null) {
                xz13.a("values", (JsonElement) gson.a(qf4.a(qf4.a(sb4.a((HashMap) map).toString(), "(", "[", false), ")", "]", false), tz1.class));
                xz12.a("steps", (JsonElement) xz13);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Int>");
            }
        }
        if (this.distanceMap != null) {
            xz1 xz14 = new xz1();
            xz14.a("interval", (JsonElement) new zz1((Number) 100));
            Gson gson2 = new Gson();
            Map<Long, Double> map2 = this.distanceMap;
            if (map2 != null) {
                xz14.a("values", (JsonElement) gson2.a(qf4.a(qf4.a(sb4.a((HashMap) map2).toString(), "(", "[", false), ")", "]", false), tz1.class));
                xz12.a("distance", (JsonElement) xz14);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
            }
        }
        if (this.caloriesMap != null) {
            xz1 xz15 = new xz1();
            xz15.a("interval", (JsonElement) new zz1((Number) 100));
            Gson gson3 = new Gson();
            Map<Long, Double> map3 = this.caloriesMap;
            if (map3 != null) {
                xz15.a("values", (JsonElement) gson3.a(qf4.a(qf4.a(sb4.a((HashMap) map3).toString(), "(", "[", false), ")", "]", false), tz1.class));
                xz12.a("energy_expended", (JsonElement) xz15);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
            }
        }
        xz1.a("time_series", (JsonElement) xz12);
        if (this.extraJsonObject != null) {
            tz1 tz1 = new tz1();
            tz1.a((JsonElement) this.extraJsonObject);
            xz1 xz16 = new xz1();
            xz16.a("data_source", (JsonElement) tz1);
            xz1.a("_links", (JsonElement) xz16);
        }
        return xz1;
    }

    @DexIgnore
    public final void setExtraJsonObject(xz1 xz1) {
        this.extraJsonObject = xz1;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public UAActivityTimeSeries(Builder builder) {
        throw null;
/*        this(r0, builder.getStepsMap(), builder.getDistanceMap(), builder.getCaloriesMap());
        kd4.b(builder, "builder");
        String mExternalId2 = builder.getMExternalId();
        if (mExternalId2 != null) {
        } else {
            kd4.a();
            throw null;
        }
*/    }
}
