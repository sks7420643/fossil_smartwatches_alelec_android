package com.portfolio.platform.data.model.diana.workout;

import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutHeartRate {
    @DexIgnore
    public float average;
    @DexIgnore
    public int max;
    @DexIgnore
    public int min;
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public List<Short> values;

    @DexIgnore
    public WorkoutHeartRate(int i, List<Short> list) {
        kd4.b(list, "values");
        this.resolutionInSecond = i;
        this.values = list;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutHeartRate copy$default(WorkoutHeartRate workoutHeartRate, int i, List<Short> list, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = workoutHeartRate.resolutionInSecond;
        }
        if ((i2 & 2) != 0) {
            list = workoutHeartRate.values;
        }
        return workoutHeartRate.copy(i, list);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Short> component2() {
        return this.values;
    }

    @DexIgnore
    public final WorkoutHeartRate copy(int i, List<Short> list) {
        kd4.b(list, "values");
        return new WorkoutHeartRate(i, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutHeartRate) {
                WorkoutHeartRate workoutHeartRate = (WorkoutHeartRate) obj;
                if (!(this.resolutionInSecond == workoutHeartRate.resolutionInSecond) || !kd4.a((Object) this.values, (Object) workoutHeartRate.values)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final float getAverage() {
        return this.average;
    }

    @DexIgnore
    public final int getMax() {
        return this.max;
    }

    @DexIgnore
    public final int getMin() {
        return this.min;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Short> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.resolutionInSecond * 31;
        List<Short> list = this.values;
        return i + (list != null ? list.hashCode() : 0);
    }

    @DexIgnore
    public final void setAverage(float f) {
        this.average = f;
    }

    @DexIgnore
    public final void setMax(int i) {
        this.max = i;
    }

    @DexIgnore
    public final void setMin(int i) {
        this.min = i;
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setValues(List<Short> list) {
        kd4.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutHeartRate(resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutHeartRate(HeartRateWrapper heartRateWrapper) {
        this(heartRateWrapper.getResolutionInSecond(), heartRateWrapper.getValues());
        kd4.b(heartRateWrapper, "heartRate");
        this.min = Integer.MAX_VALUE;
        for (Number shortValue : this.values) {
            short shortValue2 = shortValue.shortValue();
            if (shortValue2 > 0) {
                if (shortValue2 > this.max) {
                    this.max = shortValue2;
                }
                if (shortValue2 < this.min) {
                    this.min = shortValue2;
                }
            }
        }
        this.average = (float) heartRateWrapper.getAverage();
    }
}
