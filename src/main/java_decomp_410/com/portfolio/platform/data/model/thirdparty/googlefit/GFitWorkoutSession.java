package com.portfolio.platform.data.model.thirdparty.googlefit;

import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GFitWorkoutSession {
    @DexIgnore
    public List<GFitWOCalorie> calories;
    @DexIgnore
    public List<GFitWODistance> distances;
    @DexIgnore
    public long endTime;
    @DexIgnore
    public List<GFitWOHeartRate> heartRates;
    @DexIgnore
    public int id;
    @DexIgnore
    public long startTime;
    @DexIgnore
    public List<GFitWOStep> steps;
    @DexIgnore
    public int workoutType;

    @DexIgnore
    public GFitWorkoutSession(long j, long j2, int i, List<GFitWOStep> list, List<GFitWOCalorie> list2, List<GFitWODistance> list3, List<GFitWOHeartRate> list4) {
        kd4.b(list, "steps");
        kd4.b(list2, "calories");
        kd4.b(list3, "distances");
        kd4.b(list4, "heartRates");
        this.startTime = j;
        this.endTime = j2;
        this.workoutType = i;
        this.steps = list;
        this.calories = list2;
        this.distances = list3;
        this.heartRates = list4;
    }

    @DexIgnore
    public static /* synthetic */ GFitWorkoutSession copy$default(GFitWorkoutSession gFitWorkoutSession, long j, long j2, int i, List list, List list2, List list3, List list4, int i2, Object obj) {
        GFitWorkoutSession gFitWorkoutSession2 = gFitWorkoutSession;
        return gFitWorkoutSession.copy((i2 & 1) != 0 ? gFitWorkoutSession2.startTime : j, (i2 & 2) != 0 ? gFitWorkoutSession2.endTime : j2, (i2 & 4) != 0 ? gFitWorkoutSession2.workoutType : i, (i2 & 8) != 0 ? gFitWorkoutSession2.steps : list, (i2 & 16) != 0 ? gFitWorkoutSession2.calories : list2, (i2 & 32) != 0 ? gFitWorkoutSession2.distances : list3, (i2 & 64) != 0 ? gFitWorkoutSession2.heartRates : list4);
    }

    @DexIgnore
    public final long component1() {
        return this.startTime;
    }

    @DexIgnore
    public final long component2() {
        return this.endTime;
    }

    @DexIgnore
    public final int component3() {
        return this.workoutType;
    }

    @DexIgnore
    public final List<GFitWOStep> component4() {
        return this.steps;
    }

    @DexIgnore
    public final List<GFitWOCalorie> component5() {
        return this.calories;
    }

    @DexIgnore
    public final List<GFitWODistance> component6() {
        return this.distances;
    }

    @DexIgnore
    public final List<GFitWOHeartRate> component7() {
        return this.heartRates;
    }

    @DexIgnore
    public final GFitWorkoutSession copy(long j, long j2, int i, List<GFitWOStep> list, List<GFitWOCalorie> list2, List<GFitWODistance> list3, List<GFitWOHeartRate> list4) {
        List<GFitWOStep> list5 = list;
        kd4.b(list5, "steps");
        List<GFitWOCalorie> list6 = list2;
        kd4.b(list6, "calories");
        List<GFitWODistance> list7 = list3;
        kd4.b(list7, "distances");
        List<GFitWOHeartRate> list8 = list4;
        kd4.b(list8, "heartRates");
        return new GFitWorkoutSession(j, j2, i, list5, list6, list7, list8);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof GFitWorkoutSession) {
                GFitWorkoutSession gFitWorkoutSession = (GFitWorkoutSession) obj;
                if (this.startTime == gFitWorkoutSession.startTime) {
                    if (this.endTime == gFitWorkoutSession.endTime) {
                        if (!(this.workoutType == gFitWorkoutSession.workoutType) || !kd4.a((Object) this.steps, (Object) gFitWorkoutSession.steps) || !kd4.a((Object) this.calories, (Object) gFitWorkoutSession.calories) || !kd4.a((Object) this.distances, (Object) gFitWorkoutSession.distances) || !kd4.a((Object) this.heartRates, (Object) gFitWorkoutSession.heartRates)) {
                            return false;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final List<GFitWOCalorie> getCalories() {
        return this.calories;
    }

    @DexIgnore
    public final List<GFitWODistance> getDistances() {
        return this.distances;
    }

    @DexIgnore
    public final long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final List<GFitWOHeartRate> getHeartRates() {
        return this.heartRates;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final List<GFitWOStep> getSteps() {
        return this.steps;
    }

    @DexIgnore
    public final int getWorkoutType() {
        return this.workoutType;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.startTime;
        long j2 = this.endTime;
        int i = ((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.workoutType) * 31;
        List<GFitWOStep> list = this.steps;
        int i2 = 0;
        int hashCode = (i + (list != null ? list.hashCode() : 0)) * 31;
        List<GFitWOCalorie> list2 = this.calories;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<GFitWODistance> list3 = this.distances;
        int hashCode3 = (hashCode2 + (list3 != null ? list3.hashCode() : 0)) * 31;
        List<GFitWOHeartRate> list4 = this.heartRates;
        if (list4 != null) {
            i2 = list4.hashCode();
        }
        return hashCode3 + i2;
    }

    @DexIgnore
    public final void setCalories(List<GFitWOCalorie> list) {
        kd4.b(list, "<set-?>");
        this.calories = list;
    }

    @DexIgnore
    public final void setDistances(List<GFitWODistance> list) {
        kd4.b(list, "<set-?>");
        this.distances = list;
    }

    @DexIgnore
    public final void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public final void setHeartRates(List<GFitWOHeartRate> list) {
        kd4.b(list, "<set-?>");
        this.heartRates = list;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setStartTime(long j) {
        this.startTime = j;
    }

    @DexIgnore
    public final void setSteps(List<GFitWOStep> list) {
        kd4.b(list, "<set-?>");
        this.steps = list;
    }

    @DexIgnore
    public final void setWorkoutType(int i) {
        this.workoutType = i;
    }

    @DexIgnore
    public String toString() {
        return "GFitWorkoutSession(startTime=" + this.startTime + ", endTime=" + this.endTime + ", workoutType=" + this.workoutType + ", steps=" + this.steps + ", calories=" + this.calories + ", distances=" + this.distances + ", heartRates=" + this.heartRates + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ GFitWorkoutSession(long j, long j2, int i, List list, List list2, List list3, List list4, int i2, fd4 fd4) {
        this(j, j2, i, (i2 & 8) != 0 ? cb4.a() : list, (i2 & 16) != 0 ? cb4.a() : list2, (i2 & 32) != 0 ? cb4.a() : list3, (i2 & 64) != 0 ? cb4.a() : list4);
    }
}
