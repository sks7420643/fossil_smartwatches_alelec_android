package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.fitness.ActiveMinute;
import com.fossil.fitness.Calorie;
import com.fossil.fitness.Distance;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.GoalTracking;
import com.fossil.fitness.HeartRate;
import com.fossil.fitness.Resting;
import com.fossil.fitness.SleepSession;
import com.fossil.fitness.Step;
import com.fossil.fitness.WorkoutSession;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.service.syncmodel.WrapperTapEventSummary;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FitnessDataWrapper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public ActiveMinuteWrapper activeMinute;
    @DexIgnore
    public CalorieWrapper calorie;
    @DexIgnore
    public DistanceWrapper distance;
    @DexIgnore
    public DateTime endTime;
    @DexIgnore
    public List<WrapperTapEventSummary> goalTrackings;
    @DexIgnore
    public HeartRateWrapper heartRate;
    @DexIgnore
    public List<RestingWrapper> resting;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public List<SleepSessionWrapper> sleeps;
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public StepWrapper step;
    @DexIgnore
    public StressWrapper stress;
    @DexIgnore
    public DateTime syncTime;
    @DexIgnore
    public int timezoneOffsetInSecond;
    @DexIgnore
    public List<WorkoutSessionWrapper> workouts;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = FitnessDataWrapper.class.getSimpleName();
        kd4.a((Object) simpleName, "FitnessDataWrapper::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public FitnessDataWrapper(DateTime dateTime, DateTime dateTime2, DateTime dateTime3, int i, String str) {
        kd4.b(dateTime, SampleRaw.COLUMN_START_TIME);
        kd4.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        kd4.b(dateTime3, "syncTime");
        kd4.b(str, "serialNumber");
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.syncTime = dateTime3;
        this.timezoneOffsetInSecond = i;
        this.serialNumber = str;
        this.resting = new ArrayList();
        this.sleeps = new ArrayList();
        this.workouts = new ArrayList();
        this.goalTrackings = new ArrayList();
    }

    @DexIgnore
    public static /* synthetic */ FitnessDataWrapper copy$default(FitnessDataWrapper fitnessDataWrapper, DateTime dateTime, DateTime dateTime2, DateTime dateTime3, int i, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            dateTime = fitnessDataWrapper.startTime;
        }
        if ((i2 & 2) != 0) {
            dateTime2 = fitnessDataWrapper.endTime;
        }
        DateTime dateTime4 = dateTime2;
        if ((i2 & 4) != 0) {
            dateTime3 = fitnessDataWrapper.syncTime;
        }
        DateTime dateTime5 = dateTime3;
        if ((i2 & 8) != 0) {
            i = fitnessDataWrapper.timezoneOffsetInSecond;
        }
        int i3 = i;
        if ((i2 & 16) != 0) {
            str = fitnessDataWrapper.serialNumber;
        }
        return fitnessDataWrapper.copy(dateTime, dateTime4, dateTime5, i3, str);
    }

    @DexIgnore
    public final DateTime component1() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.endTime;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.syncTime;
    }

    @DexIgnore
    public final int component4() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final String component5() {
        return this.serialNumber;
    }

    @DexIgnore
    public final FitnessDataWrapper copy(DateTime dateTime, DateTime dateTime2, DateTime dateTime3, int i, String str) {
        kd4.b(dateTime, SampleRaw.COLUMN_START_TIME);
        kd4.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        kd4.b(dateTime3, "syncTime");
        kd4.b(str, "serialNumber");
        return new FitnessDataWrapper(dateTime, dateTime2, dateTime3, i, str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof FitnessDataWrapper) {
                FitnessDataWrapper fitnessDataWrapper = (FitnessDataWrapper) obj;
                if (kd4.a((Object) this.startTime, (Object) fitnessDataWrapper.startTime) && kd4.a((Object) this.endTime, (Object) fitnessDataWrapper.endTime) && kd4.a((Object) this.syncTime, (Object) fitnessDataWrapper.syncTime)) {
                    if (!(this.timezoneOffsetInSecond == fitnessDataWrapper.timezoneOffsetInSecond) || !kd4.a((Object) this.serialNumber, (Object) fitnessDataWrapper.serialNumber)) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ActiveMinuteWrapper getActiveMinute() {
        ActiveMinuteWrapper activeMinuteWrapper = this.activeMinute;
        if (activeMinuteWrapper != null) {
            return activeMinuteWrapper;
        }
        kd4.d("activeMinute");
        throw null;
    }

    @DexIgnore
    public final CalorieWrapper getCalorie() {
        CalorieWrapper calorieWrapper = this.calorie;
        if (calorieWrapper != null) {
            return calorieWrapper;
        }
        kd4.d("calorie");
        throw null;
    }

    @DexIgnore
    public final DistanceWrapper getDistance() {
        DistanceWrapper distanceWrapper = this.distance;
        if (distanceWrapper != null) {
            return distanceWrapper;
        }
        kd4.d("distance");
        throw null;
    }

    @DexIgnore
    public final long getEndLongTime() {
        return this.endTime.getMillis();
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final DateTime getEndTimeTZ() {
        return new DateTime(this.endTime.getMillis(), DateTimeZone.forOffsetMillis(this.timezoneOffsetInSecond * 1000));
    }

    @DexIgnore
    public final List<WrapperTapEventSummary> getGoalTrackings() {
        return this.goalTrackings;
    }

    @DexIgnore
    public final HeartRateWrapper getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final List<RestingWrapper> getResting() {
        return this.resting;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final List<SleepSessionWrapper> getSleeps() {
        return this.sleeps;
    }

    @DexIgnore
    public final long getStartLongTime() {
        return this.startTime.getMillis();
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime getStartTimeTZ() {
        return new DateTime(this.startTime.getMillis(), DateTimeZone.forOffsetMillis(this.timezoneOffsetInSecond * 1000));
    }

    @DexIgnore
    public final StepWrapper getStep() {
        StepWrapper stepWrapper = this.step;
        if (stepWrapper != null) {
            return stepWrapper;
        }
        kd4.d("step");
        throw null;
    }

    @DexIgnore
    public final StressWrapper getStress() {
        return this.stress;
    }

    @DexIgnore
    public final DateTime getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final List<WorkoutSessionWrapper> getWorkouts() {
        return this.workouts;
    }

    @DexIgnore
    public int hashCode() {
        DateTime dateTime = this.startTime;
        int i = 0;
        int hashCode = (dateTime != null ? dateTime.hashCode() : 0) * 31;
        DateTime dateTime2 = this.endTime;
        int hashCode2 = (hashCode + (dateTime2 != null ? dateTime2.hashCode() : 0)) * 31;
        DateTime dateTime3 = this.syncTime;
        int hashCode3 = (((hashCode2 + (dateTime3 != null ? dateTime3.hashCode() : 0)) * 31) + this.timezoneOffsetInSecond) * 31;
        String str = this.serialNumber;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public final void setActiveMinute(ActiveMinuteWrapper activeMinuteWrapper) {
        kd4.b(activeMinuteWrapper, "<set-?>");
        this.activeMinute = activeMinuteWrapper;
    }

    @DexIgnore
    public final void setCalorie(CalorieWrapper calorieWrapper) {
        kd4.b(calorieWrapper, "<set-?>");
        this.calorie = calorieWrapper;
    }

    @DexIgnore
    public final void setDistance(DistanceWrapper distanceWrapper) {
        kd4.b(distanceWrapper, "<set-?>");
        this.distance = distanceWrapper;
    }

    @DexIgnore
    public final void setEndTime(DateTime dateTime) {
        kd4.b(dateTime, "<set-?>");
        this.endTime = dateTime;
    }

    @DexIgnore
    public final void setGoalTrackings(List<WrapperTapEventSummary> list) {
        kd4.b(list, "<set-?>");
        this.goalTrackings = list;
    }

    @DexIgnore
    public final void setHeartRate(HeartRateWrapper heartRateWrapper) {
        this.heartRate = heartRateWrapper;
    }

    @DexIgnore
    public final void setResting(List<RestingWrapper> list) {
        kd4.b(list, "<set-?>");
        this.resting = list;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        kd4.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setSleeps(List<SleepSessionWrapper> list) {
        kd4.b(list, "<set-?>");
        this.sleeps = list;
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        kd4.b(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setStep(StepWrapper stepWrapper) {
        kd4.b(stepWrapper, "<set-?>");
        this.step = stepWrapper;
    }

    @DexIgnore
    public final void setStress(StressWrapper stressWrapper) {
        this.stress = stressWrapper;
    }

    @DexIgnore
    public final void setSyncTime(DateTime dateTime) {
        kd4.b(dateTime, "<set-?>");
        this.syncTime = dateTime;
    }

    @DexIgnore
    public final void setTimezoneOffsetInSecond(int i) {
        this.timezoneOffsetInSecond = i;
    }

    @DexIgnore
    public final void setWorkouts(List<WorkoutSessionWrapper> list) {
        kd4.b(list, "<set-?>");
        this.workouts = list;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("steps ");
        StepWrapper stepWrapper = this.step;
        if (stepWrapper != null) {
            sb.append(stepWrapper);
            sb.append(" \n activeMinute ");
            ActiveMinuteWrapper activeMinuteWrapper = this.activeMinute;
            if (activeMinuteWrapper != null) {
                sb.append(activeMinuteWrapper);
                sb.append(" \n calorie ");
                CalorieWrapper calorieWrapper = this.calorie;
                if (calorieWrapper != null) {
                    sb.append(calorieWrapper);
                    sb.append(" \n distance ");
                    DistanceWrapper distanceWrapper = this.distance;
                    if (distanceWrapper != null) {
                        sb.append(distanceWrapper);
                        sb.append(" \n sleeps ");
                        sb.append(this.sleeps);
                        sb.append(" \n heartrate ");
                        sb.append(this.heartRate);
                        return sb.toString();
                    }
                    kd4.d("distance");
                    throw null;
                }
                kd4.d("calorie");
                throw null;
            }
            kd4.d("activeMinute");
            throw null;
        }
        kd4.d("step");
        throw null;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public FitnessDataWrapper(FitnessData fitnessData, String str, DateTime dateTime) {
        this(new DateTime(((long) fitnessData.getStartTime()) * 1000, DateTimeZone.forOffsetMillis(fitnessData.getTimezoneOffsetInSecond() * 1000)), new DateTime(((long) fitnessData.getEndTime()) * 1000, DateTimeZone.forOffsetMillis(fitnessData.getTimezoneOffsetInSecond() * 1000)), dateTime, fitnessData.getTimezoneOffsetInSecond(), str);
        kd4.b(fitnessData, "fitnessData");
        kd4.b(str, "serialNumber");
        kd4.b(dateTime, "syncTime");
        Step step2 = fitnessData.getStep();
        kd4.a((Object) step2, "fitnessData.step");
        this.step = new StepWrapper(step2);
        ActiveMinute activeMinute2 = fitnessData.getActiveMinute();
        if (activeMinute2 != null) {
            this.activeMinute = new ActiveMinuteWrapper(activeMinute2);
        }
        Calorie calorie2 = fitnessData.getCalorie();
        kd4.a((Object) calorie2, "fitnessData.calorie");
        this.calorie = new CalorieWrapper(calorie2);
        Distance distance2 = fitnessData.getDistance();
        kd4.a((Object) distance2, "fitnessData.distance");
        this.distance = new DistanceWrapper(distance2);
        ArrayList<Resting> resting2 = fitnessData.getResting();
        if (resting2 != null) {
            for (Resting resting3 : resting2) {
                List<RestingWrapper> list = this.resting;
                kd4.a((Object) resting3, "it");
                DateTime withZone = new DateTime(((long) resting3.getStartTime()) * 1000).withZone(DateTimeZone.forOffsetHours(resting3.getTimezoneOffsetInSecond() / 3600));
                kd4.a((Object) withZone, "DateTime(it.startTime * \u2026neOffsetInSecond / 3600))");
                list.add(new RestingWrapper(withZone, resting3.getTimezoneOffsetInSecond(), resting3.getValue()));
            }
        } else {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.e(str2, "getResting(), list of resting null????, fitnessData startTime: " + fitnessData.getStartTime());
        }
        HeartRate heartrate = fitnessData.getHeartrate();
        if (heartrate != null) {
            this.heartRate = new HeartRateWrapper(heartrate);
        }
        ArrayList<SleepSession> sleeps2 = fitnessData.getSleeps();
        if (sleeps2 != null) {
            for (SleepSession sleepSession : sleeps2) {
                List<SleepSessionWrapper> list2 = this.sleeps;
                kd4.a((Object) sleepSession, "it");
                list2.add(new SleepSessionWrapper(sleepSession));
            }
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.e(str3, "getSleepSessions(), list of sleeps null????, fitnessData startTime: " + fitnessData.getStartTime());
        }
        ArrayList<WorkoutSession> workouts2 = fitnessData.getWorkouts();
        if (workouts2 != null) {
            for (WorkoutSession workoutSession : workouts2) {
                List<WorkoutSessionWrapper> list3 = this.workouts;
                kd4.a((Object) workoutSession, "it");
                list3.add(new WorkoutSessionWrapper(workoutSession));
            }
        } else {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local3.e(str4, "getWorkoutSessions(), list of workout null????, fitnessData startTime: " + fitnessData.getStartTime());
        }
        ArrayList<GoalTracking> goals = fitnessData.getGoals();
        if (goals != null) {
            for (GoalTracking goalTracking : goals) {
                List<WrapperTapEventSummary> list4 = this.goalTrackings;
                kd4.a((Object) goalTracking, "it");
                list4.add(new WrapperTapEventSummary(goalTracking.getStartTime(), goalTracking.getTimezoneOffsetInSecond(), goalTracking.getGoalId()));
            }
            return;
        }
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str5 = TAG;
        local4.e(str5, "getGoalTrackings(), list of goal trackings null????, fitnessData startTime: " + fitnessData.getStartTime());
    }
}
