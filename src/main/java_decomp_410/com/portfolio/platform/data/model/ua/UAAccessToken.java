package com.portfolio.platform.data.model.ua;

import com.fossil.blesdk.obfuscated.f02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UAAccessToken {
    @DexIgnore
    @f02("access_token")
    public String accessToken;
    @DexIgnore
    @f02("expires_at")
    public Long expiresAt;
    @DexIgnore
    @f02("expires_in")
    public Long expiresIn;
    @DexIgnore
    @f02("refresh_token")
    public String refreshToken;
    @DexIgnore
    @f02("scope")
    public String scope;
    @DexIgnore
    @f02("token_type")
    public String tokenType;
    @DexIgnore
    @f02("user_href")
    public String userHref;
    @DexIgnore
    @f02("user_id")
    public String userId;

    @DexIgnore
    public final String getAccessToken() {
        return this.accessToken;
    }

    @DexIgnore
    public final Long getExpiresAt() {
        return this.expiresAt;
    }

    @DexIgnore
    public final Long getExpiresIn() {
        return this.expiresIn;
    }

    @DexIgnore
    public final String getRefreshToken() {
        return this.refreshToken;
    }

    @DexIgnore
    public final String getScope() {
        return this.scope;
    }

    @DexIgnore
    public final String getTokenType() {
        return this.tokenType;
    }

    @DexIgnore
    public final String getUserHref() {
        return this.userHref;
    }

    @DexIgnore
    public final String getUserId() {
        return this.userId;
    }

    @DexIgnore
    public final void setAccessToken(String str) {
        this.accessToken = str;
    }

    @DexIgnore
    public final void setExpiresAt(Long l) {
        this.expiresAt = l;
    }

    @DexIgnore
    public final void setExpiresIn(Long l) {
        this.expiresIn = l;
    }

    @DexIgnore
    public final void setRefreshToken(String str) {
        this.refreshToken = str;
    }

    @DexIgnore
    public final void setScope(String str) {
        this.scope = str;
    }

    @DexIgnore
    public final void setTokenType(String str) {
        this.tokenType = str;
    }

    @DexIgnore
    public final void setUserHref(String str) {
        this.userHref = str;
    }

    @DexIgnore
    public final void setUserId(String str) {
        this.userId = str;
    }
}
