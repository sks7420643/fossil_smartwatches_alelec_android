package com.portfolio.platform.data.model.p006ua;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.model.ua.UALink */
public final class UALink {
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("href")
    public java.lang.String href;
    @com.fossil.blesdk.obfuscated.f02("id")

    @DexIgnore
    /* renamed from: id */
    public java.lang.String f21078id;

    @DexIgnore
    public final java.lang.String getHref() {
        return this.href;
    }

    @DexIgnore
    public final java.lang.String getId() {
        return this.f21078id;
    }

    @DexIgnore
    public final void setHref(java.lang.String str) {
        this.href = str;
    }

    @DexIgnore
    public final void setId(java.lang.String str) {
        this.f21078id = str;
    }
}
