package com.portfolio.platform.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomizeRealData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    public String id;
    @DexIgnore
    public /* final */ String value;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<CustomizeRealData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public CustomizeRealData createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new CustomizeRealData(parcel);
        }

        @DexIgnore
        public CustomizeRealData[] newArray(int i) {
            return new CustomizeRealData[i];
        }
    }

    @DexIgnore
    public CustomizeRealData(String str, String str2) {
        kd4.b(str, "id");
        kd4.b(str2, "value");
        this.id = str;
        this.value = str2;
    }

    @DexIgnore
    public static /* synthetic */ CustomizeRealData copy$default(CustomizeRealData customizeRealData, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = customizeRealData.id;
        }
        if ((i & 2) != 0) {
            str2 = customizeRealData.value;
        }
        return customizeRealData.copy(str, str2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.value;
    }

    @DexIgnore
    public final CustomizeRealData copy(String str, String str2) {
        kd4.b(str, "id");
        kd4.b(str2, "value");
        return new CustomizeRealData(str, str2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CustomizeRealData)) {
            return false;
        }
        CustomizeRealData customizeRealData = (CustomizeRealData) obj;
        return kd4.a((Object) this.id, (Object) customizeRealData.id) && kd4.a((Object) this.value, (Object) customizeRealData.value);
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getValue() {
        return this.value;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.value;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setId(String str) {
        kd4.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public String toString() {
        return "CustomizeRealData(id=" + this.id + ", value=" + this.value + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeString(this.value);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public CustomizeRealData(Parcel parcel) {
        throw null;
/*        this(r0, r3 == null ? "" : r3);
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        readString = readString == null ? "" : readString;
        String readString2 = parcel.readString();
*/    }
}
