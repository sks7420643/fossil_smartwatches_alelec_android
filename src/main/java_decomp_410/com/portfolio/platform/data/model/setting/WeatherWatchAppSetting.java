package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherWatchAppSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    @f02("locations")
    public List<WeatherLocationWrapper> locations; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherWatchAppSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public WeatherWatchAppSetting createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new WeatherWatchAppSetting(parcel);
        }

        @DexIgnore
        public WeatherWatchAppSetting[] newArray(int i) {
            return new WeatherWatchAppSetting[i];
        }
    }

    @DexIgnore
    public WeatherWatchAppSetting() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final List<WeatherLocationWrapper> getLocations() {
        return this.locations;
    }

    @DexIgnore
    public final void setLocations(List<WeatherLocationWrapper> list) {
        kd4.b(list, "<set-?>");
        this.locations = list;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeList(kb4.k(this.locations));
    }

    @DexIgnore
    public WeatherWatchAppSetting(Parcel parcel) {
        kd4.b(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        parcel.readList(arrayList, WeatherLocationWrapper.class.getClassLoader());
        this.locations = arrayList;
    }
}
