package com.portfolio.platform.data.model.p006ua;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.model.ua.UADevice */
public final class UADevice {
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("description")
    public java.lang.String description;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("_links")
    public com.portfolio.platform.data.model.p006ua.UALinks links;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("manufacturer")
    public java.lang.String manufacturer;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("model")
    public java.lang.String model;
    @DexIgnore
    @com.fossil.blesdk.obfuscated.f02("name")
    public java.lang.String name;

    @DexIgnore
    public final java.lang.String getDescription() {
        return this.description;
    }

    @DexIgnore
    public final com.portfolio.platform.data.model.p006ua.UALinks getLinks() {
        return this.links;
    }

    @DexIgnore
    public final java.lang.String getManufacturer() {
        return this.manufacturer;
    }

    @DexIgnore
    public final java.lang.String getModel() {
        return this.model;
    }

    @DexIgnore
    public final java.lang.String getName() {
        return this.name;
    }

    @DexIgnore
    public final void setDescription(java.lang.String str) {
        this.description = str;
    }

    @DexIgnore
    public final void setLinks(com.portfolio.platform.data.model.p006ua.UALinks uALinks) {
        this.links = uALinks;
    }

    @DexIgnore
    public final void setManufacturer(java.lang.String str) {
        this.manufacturer = str;
    }

    @DexIgnore
    public final void setModel(java.lang.String str) {
        this.model = str;
    }

    @DexIgnore
    public final void setName(java.lang.String str) {
        this.name = str;
    }
}
