package com.portfolio.platform.data;

import android.content.Context;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.pd4;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.NumberPickerLarge;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ProfileFormatter implements NumberPickerLarge.Formatter, RulerValuePicker.Formatter, Serializable {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ int UNIT_DEFAULT; // = -1;
    @DexIgnore
    public static /* final */ int UNIT_DOT; // = 2;
    @DexIgnore
    public static /* final */ int UNIT_FEET; // = 0;
    @DexIgnore
    public static /* final */ int UNIT_FEET_INCHES; // = 3;
    @DexIgnore
    public static /* final */ int UNIT_INCHES; // = 1;
    @DexIgnore
    public static /* final */ int UNIT_WEIGHT; // = 4;
    @DexIgnore
    public /* final */ int mUnit;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public ProfileFormatter(int i) {
        this.mUnit = i;
    }

    @DexIgnore
    public String format(int i) {
        int i2 = this.mUnit;
        if (i2 == 0) {
            pd4 pd4 = pd4.a;
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            String a = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.feet_format);
            kd4.a((Object) a, "LanguageHelper.getString\u2026ce, R.string.feet_format)");
            Object[] objArr = {Integer.valueOf(i)};
            String format = String.format(locale, a, Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
            return format;
        } else if (i2 == 1) {
            pd4 pd42 = pd4.a;
            Locale locale2 = Locale.US;
            kd4.a((Object) locale2, "Locale.US");
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.inches_format);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026, R.string.inches_format)");
            Object[] objArr2 = {Integer.valueOf(i)};
            String format2 = String.format(locale2, a2, Arrays.copyOf(objArr2, objArr2.length));
            kd4.a((Object) format2, "java.lang.String.format(locale, format, *args)");
            return format2;
        } else if (i2 == 2) {
            pd4 pd43 = pd4.a;
            Locale locale3 = Locale.US;
            kd4.a((Object) locale3, "Locale.US");
            String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.dot_format);
            kd4.a((Object) a3, "LanguageHelper.getString\u2026nce, R.string.dot_format)");
            Object[] objArr3 = {Integer.valueOf(i)};
            String format3 = String.format(locale3, a3, Arrays.copyOf(objArr3, objArr3.length));
            kd4.a((Object) format3, "java.lang.String.format(locale, format, *args)");
            return format3;
        } else if (i2 == 3) {
            pd4 pd44 = pd4.a;
            Locale locale4 = Locale.US;
            kd4.a((Object) locale4, "Locale.US");
            String a4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.feet_inches_format);
            kd4.a((Object) a4, "LanguageHelper.getString\u2026tring.feet_inches_format)");
            Object[] objArr4 = {Integer.valueOf(i / 12), Integer.valueOf(i % 12)};
            String format4 = String.format(locale4, a4, Arrays.copyOf(objArr4, objArr4.length));
            kd4.a((Object) format4, "java.lang.String.format(locale, format, *args)");
            return format4;
        } else if (i2 != 4) {
            pd4 pd45 = pd4.a;
            Locale locale5 = Locale.US;
            kd4.a((Object) locale5, "Locale.US");
            String a5 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.normal_format);
            kd4.a((Object) a5, "LanguageHelper.getString\u2026, R.string.normal_format)");
            Object[] objArr5 = {Integer.valueOf(i)};
            String format5 = String.format(locale5, a5, Arrays.copyOf(objArr5, objArr5.length));
            kd4.a((Object) format5, "java.lang.String.format(locale, format, *args)");
            return format5;
        } else {
            pd4 pd46 = pd4.a;
            Locale locale6 = Locale.US;
            kd4.a((Object) locale6, "Locale.US");
            String a6 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.weight_format);
            kd4.a((Object) a6, "LanguageHelper.getString\u2026, R.string.weight_format)");
            Object[] objArr6 = {Integer.valueOf(i / 10), Integer.valueOf(i % 10)};
            String format6 = String.format(locale6, a6, Arrays.copyOf(objArr6, objArr6.length));
            kd4.a((Object) format6, "java.lang.String.format(locale, format, *args)");
            return format6;
        }
    }

    @DexIgnore
    public final int getMUnit() {
        return this.mUnit;
    }
}
