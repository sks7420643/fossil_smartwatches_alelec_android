package com.portfolio.platform.data;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$1 extends com.fossil.blesdk.obfuscated.qc1 {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.dg4 $continuation;
    @DexIgnore
    public /* final */ /* synthetic */ com.google.android.gms.location.LocationRequest $locationRequest$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.oc1 $this_requestLocationUpdates$inlined;

    @DexIgnore
    public LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$1(com.fossil.blesdk.obfuscated.dg4 dg4, com.fossil.blesdk.obfuscated.oc1 oc1, com.google.android.gms.location.LocationRequest locationRequest) {
        this.$continuation = dg4;
        this.$this_requestLocationUpdates$inlined = oc1;
        this.$locationRequest$inlined = locationRequest;
    }

    @DexIgnore
    public void onLocationResult(com.google.android.gms.location.LocationResult locationResult) {
        com.fossil.blesdk.obfuscated.kd4.b(locationResult, "locationResult");
        super.onLocationResult(locationResult);
        android.location.Location H = locationResult.H();
        if (H != null) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "onLocationResult lastLocation=" + H);
        } else {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.data.LocationSource.Companion.getTAG$app_fossilRelease(), "onLocationResult lastLocation is null");
        }
        this.$this_requestLocationUpdates$inlined.a((com.fossil.blesdk.obfuscated.qc1) this);
        if (this.$continuation.isActive()) {
            com.fossil.blesdk.obfuscated.dg4 dg4 = this.$continuation;
            kotlin.Result.a aVar = kotlin.Result.Companion;
            dg4.resumeWith(kotlin.Result.constructor-impl(H));
        }
    }
}
