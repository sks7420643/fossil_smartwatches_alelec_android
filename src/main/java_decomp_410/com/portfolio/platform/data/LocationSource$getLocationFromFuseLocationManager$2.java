package com.portfolio.platform.data;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.LocationSource$getLocationFromFuseLocationManager$2", mo27670f = "LocationSource.kt", mo27671l = {130}, mo27672m = "invokeSuspend")
public final class LocationSource$getLocationFromFuseLocationManager$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super android.location.Location>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.oc1 $fusedLocationClient;
    @DexIgnore
    public /* final */ /* synthetic */ com.google.android.gms.location.LocationRequest $locationRequest;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21024p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.LocationSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LocationSource$getLocationFromFuseLocationManager$2(com.portfolio.platform.data.LocationSource locationSource, com.fossil.blesdk.obfuscated.oc1 oc1, com.google.android.gms.location.LocationRequest locationRequest, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = locationSource;
        this.$fusedLocationClient = oc1;
        this.$locationRequest = locationRequest;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.LocationSource$getLocationFromFuseLocationManager$2 locationSource$getLocationFromFuseLocationManager$2 = new com.portfolio.platform.data.LocationSource$getLocationFromFuseLocationManager$2(this.this$0, this.$fusedLocationClient, this.$locationRequest, yb4);
        locationSource$getLocationFromFuseLocationManager$2.f21024p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return locationSource$getLocationFromFuseLocationManager$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.LocationSource$getLocationFromFuseLocationManager$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21024p$;
            com.portfolio.platform.data.LocationSource locationSource = this.this$0;
            com.fossil.blesdk.obfuscated.oc1 oc1 = this.$fusedLocationClient;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) oc1, "fusedLocationClient");
            com.google.android.gms.location.LocationRequest locationRequest = this.$locationRequest;
            this.L$0 = zg4;
            this.label = 1;
            obj = locationSource.requestLocationUpdates(oc1, locationRequest, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
