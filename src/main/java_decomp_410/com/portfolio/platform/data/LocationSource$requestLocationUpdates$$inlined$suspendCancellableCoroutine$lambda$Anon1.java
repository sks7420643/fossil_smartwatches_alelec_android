package com.portfolio.platform.data;

import android.location.Location;
import com.fossil.blesdk.obfuscated.dg4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.oc1;
import com.fossil.blesdk.obfuscated.qc1;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlin.Result;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1 extends qc1 {
    @DexIgnore
    public /* final */ /* synthetic */ dg4 $continuation;
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest $locationRequest$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ oc1 $this_requestLocationUpdates$inlined;

    @DexIgnore
    public LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1(dg4 dg4, oc1 oc1, LocationRequest locationRequest) {
        this.$continuation = dg4;
        this.$this_requestLocationUpdates$inlined = oc1;
        this.$locationRequest$inlined = locationRequest;
    }

    @DexIgnore
    public void onLocationResult(LocationResult locationResult) {
        kd4.b(locationResult, "locationResult");
        super.onLocationResult(locationResult);
        Location H = locationResult.H();
        if (H != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "onLocationResult lastLocation=" + H);
        } else {
            FLogger.INSTANCE.getLocal().d(LocationSource.Companion.getTAG$app_fossilRelease(), "onLocationResult lastLocation is null");
        }
        this.$this_requestLocationUpdates$inlined.a((qc1) this);
        if (this.$continuation.isActive()) {
            dg4 dg4 = this.$continuation;
            Result.a aVar = Result.Companion;
            dg4.resumeWith(Result.m3constructorimpl(H));
        }
    }
}
