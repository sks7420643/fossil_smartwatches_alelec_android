package com.portfolio.platform.data;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LocationSource_Factory implements Factory<LocationSource> {
    @DexIgnore
    public static /* final */ LocationSource_Factory INSTANCE; // = new LocationSource_Factory();

    @DexIgnore
    public static LocationSource_Factory create() {
        return INSTANCE;
    }

    @DexIgnore
    public static LocationSource newLocationSource() {
        return new LocationSource();
    }

    @DexIgnore
    public static LocationSource provideInstance() {
        return new LocationSource();
    }

    @DexIgnore
    public LocationSource get() {
        return provideInstance();
    }
}
