package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.os.Build;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridCustomizeDatabase_Impl extends HybridCustomizeDatabase {
    @DexIgnore
    public volatile HybridPresetDao _hybridPresetDao;
    @DexIgnore
    public volatile MicroAppDao _microAppDao;
    @DexIgnore
    public volatile MicroAppLastSettingDao _microAppLastSettingDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `microApp` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `nameKey` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `categories` TEXT NOT NULL, `description` TEXT NOT NULL, `descriptionKey` TEXT NOT NULL, `icon` TEXT, PRIMARY KEY(`id`, `serialNumber`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `microAppSetting` (`id` TEXT NOT NULL, `appId` TEXT NOT NULL, `setting` TEXT, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`appId`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `microAppVariant` (`id` TEXT NOT NULL, `appId` TEXT NOT NULL, `name` TEXT NOT NULL, `description` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `majorNumber` INTEGER NOT NULL, `minorNumber` INTEGER NOT NULL, `serialNumber` TEXT NOT NULL, PRIMARY KEY(`appId`, `serialNumber`, `name`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `microAppLastSetting` (`appId` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`appId`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `declarationFile` (`appId` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `variantName` TEXT NOT NULL, `id` TEXT NOT NULL, `description` TEXT, `content` TEXT, PRIMARY KEY(`appId`, `serialNumber`, `variantName`, `id`), FOREIGN KEY(`appId`, `serialNumber`, `variantName`) REFERENCES `microAppVariant`(`appId`, `serialNumber`, `name`) ON UPDATE CASCADE ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED)");
            ggVar.b("CREATE TABLE IF NOT EXISTS `hybridPreset` (`pinType` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT, `id` TEXT NOT NULL, `name` TEXT, `serialNumber` TEXT NOT NULL, `buttons` TEXT NOT NULL, `isActive` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `hybridRecommendPreset` (`id` TEXT NOT NULL, `name` TEXT, `serialNumber` TEXT NOT NULL, `buttons` TEXT NOT NULL, `isDefault` INTEGER NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '1cd16a2fbeeede74e77b1faa1351cfe8')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `microApp`");
            ggVar.b("DROP TABLE IF EXISTS `microAppSetting`");
            ggVar.b("DROP TABLE IF EXISTS `microAppVariant`");
            ggVar.b("DROP TABLE IF EXISTS `microAppLastSetting`");
            ggVar.b("DROP TABLE IF EXISTS `declarationFile`");
            ggVar.b("DROP TABLE IF EXISTS `hybridPreset`");
            ggVar.b("DROP TABLE IF EXISTS `hybridRecommendPreset`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (HybridCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = HybridCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) HybridCustomizeDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = HybridCustomizeDatabase_Impl.this.mDatabase = ggVar;
            ggVar.b("PRAGMA foreign_keys = ON");
            HybridCustomizeDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (HybridCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = HybridCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) HybridCustomizeDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            gg ggVar2 = ggVar;
            HashMap hashMap = new HashMap(8);
            hashMap.put("id", new eg.a("id", "TEXT", true, 1));
            hashMap.put("name", new eg.a("name", "TEXT", true, 0));
            hashMap.put("nameKey", new eg.a("nameKey", "TEXT", true, 0));
            hashMap.put("serialNumber", new eg.a("serialNumber", "TEXT", true, 2));
            hashMap.put("categories", new eg.a("categories", "TEXT", true, 0));
            hashMap.put("description", new eg.a("description", "TEXT", true, 0));
            hashMap.put("descriptionKey", new eg.a("descriptionKey", "TEXT", true, 0));
            hashMap.put("icon", new eg.a("icon", "TEXT", false, 0));
            eg egVar = new eg("microApp", hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar2, "microApp");
            if (egVar.equals(a)) {
                HashMap hashMap2 = new HashMap(6);
                hashMap2.put("id", new eg.a("id", "TEXT", true, 0));
                hashMap2.put("appId", new eg.a("appId", "TEXT", true, 1));
                hashMap2.put(MicroAppSetting.SETTING, new eg.a(MicroAppSetting.SETTING, "TEXT", false, 0));
                hashMap2.put("createdAt", new eg.a("createdAt", "TEXT", true, 0));
                hashMap2.put("updatedAt", new eg.a("updatedAt", "TEXT", true, 0));
                String str = "\n Found:\n";
                hashMap2.put("pinType", new eg.a("pinType", "INTEGER", true, 0));
                String str2 = "pinType";
                eg egVar2 = new eg(MicroAppSetting.TABLE_NAME, hashMap2, new HashSet(0), new HashSet(0));
                eg a2 = eg.a(ggVar2, MicroAppSetting.TABLE_NAME);
                if (egVar2.equals(a2)) {
                    HashMap hashMap3 = new HashMap(9);
                    hashMap3.put("id", new eg.a("id", "TEXT", true, 0));
                    hashMap3.put("appId", new eg.a("appId", "TEXT", true, 1));
                    hashMap3.put("name", new eg.a("name", "TEXT", true, 3));
                    hashMap3.put("description", new eg.a("description", "TEXT", true, 0));
                    hashMap3.put("createdAt", new eg.a("createdAt", "INTEGER", true, 0));
                    hashMap3.put("updatedAt", new eg.a("updatedAt", "INTEGER", true, 0));
                    hashMap3.put(MicroAppVariant.COLUMN_MAJOR_NUMBER, new eg.a(MicroAppVariant.COLUMN_MAJOR_NUMBER, "INTEGER", true, 0));
                    hashMap3.put(MicroAppVariant.COLUMN_MINOR_NUMBER, new eg.a(MicroAppVariant.COLUMN_MINOR_NUMBER, "INTEGER", true, 0));
                    hashMap3.put("serialNumber", new eg.a("serialNumber", "TEXT", true, 2));
                    eg egVar3 = new eg("microAppVariant", hashMap3, new HashSet(0), new HashSet(0));
                    eg a3 = eg.a(ggVar2, "microAppVariant");
                    if (egVar3.equals(a3)) {
                        HashMap hashMap4 = new HashMap(3);
                        hashMap4.put("appId", new eg.a("appId", "TEXT", true, 1));
                        hashMap4.put("updatedAt", new eg.a("updatedAt", "TEXT", true, 0));
                        hashMap4.put(MicroAppSetting.SETTING, new eg.a(MicroAppSetting.SETTING, "TEXT", true, 0));
                        eg egVar4 = new eg("microAppLastSetting", hashMap4, new HashSet(0), new HashSet(0));
                        eg a4 = eg.a(ggVar2, "microAppLastSetting");
                        if (egVar4.equals(a4)) {
                            HashMap hashMap5 = new HashMap(6);
                            hashMap5.put("appId", new eg.a("appId", "TEXT", true, 1));
                            hashMap5.put("serialNumber", new eg.a("serialNumber", "TEXT", true, 2));
                            hashMap5.put("variantName", new eg.a("variantName", "TEXT", true, 3));
                            hashMap5.put("id", new eg.a("id", "TEXT", true, 4));
                            hashMap5.put("description", new eg.a("description", "TEXT", false, 0));
                            hashMap5.put("content", new eg.a("content", "TEXT", false, 0));
                            HashSet hashSet = new HashSet(1);
                            hashSet.add(new eg.b("microAppVariant", "NO ACTION", "CASCADE", Arrays.asList(new String[]{"appId", "serialNumber", "variantName"}), Arrays.asList(new String[]{"appId", "serialNumber", "name"})));
                            eg egVar5 = new eg("declarationFile", hashMap5, hashSet, new HashSet(0));
                            eg a5 = eg.a(ggVar2, "declarationFile");
                            if (egVar5.equals(a5)) {
                                HashMap hashMap6 = new HashMap(8);
                                String str3 = str2;
                                hashMap6.put(str3, new eg.a(str3, "INTEGER", true, 0));
                                hashMap6.put("createdAt", new eg.a("createdAt", "TEXT", false, 0));
                                hashMap6.put("updatedAt", new eg.a("updatedAt", "TEXT", false, 0));
                                hashMap6.put("id", new eg.a("id", "TEXT", true, 1));
                                hashMap6.put("name", new eg.a("name", "TEXT", false, 0));
                                hashMap6.put("serialNumber", new eg.a("serialNumber", "TEXT", true, 0));
                                hashMap6.put("buttons", new eg.a("buttons", "TEXT", true, 0));
                                hashMap6.put("isActive", new eg.a("isActive", "INTEGER", true, 0));
                                eg egVar6 = new eg("hybridPreset", hashMap6, new HashSet(0), new HashSet(0));
                                eg a6 = eg.a(ggVar2, "hybridPreset");
                                if (egVar6.equals(a6)) {
                                    HashMap hashMap7 = new HashMap(7);
                                    hashMap7.put("id", new eg.a("id", "TEXT", true, 1));
                                    hashMap7.put("name", new eg.a("name", "TEXT", false, 0));
                                    hashMap7.put("serialNumber", new eg.a("serialNumber", "TEXT", true, 0));
                                    hashMap7.put("buttons", new eg.a("buttons", "TEXT", true, 0));
                                    hashMap7.put("isDefault", new eg.a("isDefault", "INTEGER", true, 0));
                                    hashMap7.put("createdAt", new eg.a("createdAt", "TEXT", true, 0));
                                    hashMap7.put("updatedAt", new eg.a("updatedAt", "TEXT", true, 0));
                                    eg egVar7 = new eg("hybridRecommendPreset", hashMap7, new HashSet(0), new HashSet(0));
                                    eg a7 = eg.a(ggVar2, "hybridRecommendPreset");
                                    if (!egVar7.equals(a7)) {
                                        throw new IllegalStateException("Migration didn't properly handle hybridRecommendPreset(com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset).\n Expected:\n" + egVar7 + str + a7);
                                    }
                                    return;
                                }
                                throw new IllegalStateException("Migration didn't properly handle hybridPreset(com.portfolio.platform.data.model.room.microapp.HybridPreset).\n Expected:\n" + egVar6 + str + a6);
                            }
                            throw new IllegalStateException("Migration didn't properly handle declarationFile(com.portfolio.platform.data.model.room.microapp.DeclarationFile).\n Expected:\n" + egVar5 + str + a5);
                        }
                        throw new IllegalStateException("Migration didn't properly handle microAppLastSetting(com.portfolio.platform.data.model.microapp.MicroAppLastSetting).\n Expected:\n" + egVar4 + str + a4);
                    }
                    throw new IllegalStateException("Migration didn't properly handle microAppVariant(com.portfolio.platform.data.model.room.microapp.MicroAppVariant).\n Expected:\n" + egVar3 + str + a3);
                }
                throw new IllegalStateException("Migration didn't properly handle microAppSetting(com.portfolio.platform.data.model.room.microapp.MicroAppSetting).\n Expected:\n" + egVar2 + str + a2);
            }
            throw new IllegalStateException("Migration didn't properly handle microApp(com.portfolio.platform.data.model.room.microapp.MicroApp).\n Expected:\n" + egVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        boolean z = Build.VERSION.SDK_INT >= 21;
        if (!z) {
            try {
                a.b("PRAGMA foreign_keys = FALSE");
            } catch (Throwable th) {
                super.endTransaction();
                if (!z) {
                    a.b("PRAGMA foreign_keys = TRUE");
                }
                a.d("PRAGMA wal_checkpoint(FULL)").close();
                if (!a.x()) {
                    a.b("VACUUM");
                }
                throw th;
            }
        }
        super.beginTransaction();
        if (z) {
            a.b("PRAGMA defer_foreign_keys = TRUE");
        }
        a.b("DELETE FROM `microApp`");
        a.b("DELETE FROM `microAppSetting`");
        a.b("DELETE FROM `microAppVariant`");
        a.b("DELETE FROM `microAppLastSetting`");
        a.b("DELETE FROM `declarationFile`");
        a.b("DELETE FROM `hybridPreset`");
        a.b("DELETE FROM `hybridRecommendPreset`");
        super.setTransactionSuccessful();
        super.endTransaction();
        if (!z) {
            a.b("PRAGMA foreign_keys = TRUE");
        }
        a.d("PRAGMA wal_checkpoint(FULL)").close();
        if (!a.x()) {
            a.b("VACUUM");
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), "microApp", MicroAppSetting.TABLE_NAME, "microAppVariant", "microAppLastSetting", "declarationFile", "hybridPreset", "hybridRecommendPreset");
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(10), "1cd16a2fbeeede74e77b1faa1351cfe8", "c7fc8c005bafc0b2f864a0c6cb79cc7b");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }

    @DexIgnore
    public MicroAppDao microAppDao() {
        MicroAppDao microAppDao;
        if (this._microAppDao != null) {
            return this._microAppDao;
        }
        synchronized (this) {
            if (this._microAppDao == null) {
                this._microAppDao = new MicroAppDao_Impl(this);
            }
            microAppDao = this._microAppDao;
        }
        return microAppDao;
    }

    @DexIgnore
    public MicroAppLastSettingDao microAppLastSettingDao() {
        MicroAppLastSettingDao microAppLastSettingDao;
        if (this._microAppLastSettingDao != null) {
            return this._microAppLastSettingDao;
        }
        synchronized (this) {
            if (this._microAppLastSettingDao == null) {
                this._microAppLastSettingDao = new MicroAppLastSettingDao_Impl(this);
            }
            microAppLastSettingDao = this._microAppLastSettingDao;
        }
        return microAppLastSettingDao;
    }

    @DexIgnore
    public HybridPresetDao presetDao() {
        HybridPresetDao hybridPresetDao;
        if (this._hybridPresetDao != null) {
            return this._hybridPresetDao;
        }
        synchronized (this) {
            if (this._hybridPresetDao == null) {
                this._hybridPresetDao = new HybridPresetDao_Impl(this);
            }
            hybridPresetDao = this._hybridPresetDao;
        }
        return hybridPresetDao;
    }
}
