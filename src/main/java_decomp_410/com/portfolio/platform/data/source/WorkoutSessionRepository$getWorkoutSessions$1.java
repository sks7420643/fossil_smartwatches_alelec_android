package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSessionRepository$getWorkoutSessions$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.WorkoutSessionRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$1$1")
    /* renamed from: com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$1$1 */
    public static final class C57531 extends com.portfolio.platform.util.NetworkBoundResource<java.util.List<com.portfolio.platform.data.model.diana.workout.WorkoutSession>, com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.ServerWorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $offset;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$1 this$0;

        @DexIgnore
        public C57531(com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$1 workoutSessionRepository$getWorkoutSessions$1, kotlin.jvm.internal.Ref$IntRef ref$IntRef, int i, java.util.List list) {
            this.this$0 = workoutSessionRepository$getWorkoutSessions$1;
            this.$offset = ref$IntRef;
            this.$limit = i;
            this.$fitnessDataList = list;
        }

        @DexIgnore
        public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.ServerWorkoutSession>>> yb4) {
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
            java.lang.String e = com.fossil.blesdk.obfuscated.rk2.m27397e(this.this$0.$startDate);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.formatShortDate(startDate)");
            java.lang.String e2 = com.fossil.blesdk.obfuscated.rk2.m27397e(this.this$0.$endDate);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e2, "DateHelper.formatShortDate(endDate)");
            return access$getMApiService$p.getWorkoutSessions(e, e2, this.$offset.element, this.$limit, yb4);
        }

        @DexIgnore
        public androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.diana.workout.WorkoutSession>> loadFromDb() {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
            local.mo33255d(tAG$app_fossilRelease, "getWorkoutSessions - loadFromDb -- end=" + this.this$0.$end + ", startDate=" + this.this$0.$startDate + ", endDate=" + this.this$0.$endDate);
            com.portfolio.platform.data.source.local.diana.workout.WorkoutDao access$getMWorkoutDao$p = this.this$0.this$0.mWorkoutDao;
            java.util.Date date = this.this$0.$startDate;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_START_DATE);
            java.util.Date date2 = this.this$0.$endDate;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date2, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_END_DATE);
            return access$getMWorkoutDao$p.getWorkoutSessionsDesc(date, date2);
        }

        @DexIgnore
        public void onFetchFailed(java.lang.Throwable th) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - onFetchFailed");
        }

        @DexIgnore
        public boolean processContinueFetching(com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.ServerWorkoutSession> apiResponse) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(apiResponse, "item");
            com.portfolio.platform.data.model.Range range = apiResponse.get_range();
            if (range == null || !range.isHasNext()) {
                return false;
            }
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - processContinueFetching -- hasNext=TRUE");
            this.$offset.element += this.$limit;
            return true;
        }

        @DexIgnore
        public void saveCallResult(com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.ServerWorkoutSession> apiResponse) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(apiResponse, "item");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("getWorkoutSessions - saveCallResult -- item.size=");
            sb.append(apiResponse.get_items().size());
            sb.append(", hasNext=");
            com.portfolio.platform.data.model.Range range = apiResponse.get_range();
            sb.append(range != null ? java.lang.Boolean.valueOf(range.isHasNext()) : null);
            local.mo33255d(tAG$app_fossilRelease, sb.toString());
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease2 = com.portfolio.platform.data.source.WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
            local2.mo33255d(tAG$app_fossilRelease2, "getWorkoutSessions - saveCallResult -- items=" + apiResponse.get_items());
            java.util.ArrayList arrayList = new java.util.ArrayList();
            for (com.portfolio.platform.data.ServerWorkoutSession workoutSession : apiResponse.get_items()) {
                com.portfolio.platform.data.model.diana.workout.WorkoutSession workoutSession2 = workoutSession.toWorkoutSession();
                if (workoutSession2 != null) {
                    arrayList.add(workoutSession2);
                }
            }
            if (!apiResponse.get_items().isEmpty()) {
                this.this$0.this$0.mWorkoutDao.upsertListWorkoutSession(arrayList);
            }
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - saveCallResult -- DONE!!!");
        }

        @DexIgnore
        public boolean shouldFetch(java.util.List<com.portfolio.platform.data.model.diana.workout.WorkoutSession> list) {
            return this.this$0.$shouldFetch && this.$fitnessDataList.isEmpty();
        }
    }

    @DexIgnore
    public WorkoutSessionRepository$getWorkoutSessions$1(com.portfolio.platform.data.source.WorkoutSessionRepository workoutSessionRepository, boolean z, java.util.Date date, java.util.Date date2, java.util.Date date3) {
        this.this$0 = workoutSessionRepository;
        this.$shouldFetch = z;
        this.$end = date;
        this.$startDate = date2;
        this.$endDate = date3;
    }

    @DexIgnore
    public final androidx.lifecycle.LiveData<com.fossil.blesdk.obfuscated.os3<java.util.List<com.portfolio.platform.data.model.diana.workout.WorkoutSession>>> apply(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> list) {
        kotlin.jvm.internal.Ref$IntRef ref$IntRef = new kotlin.jvm.internal.Ref$IntRef();
        ref$IntRef.element = 0;
        return new com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$1.C57531(this, ref$IntRef, 100, list).asLiveData();
    }
}
