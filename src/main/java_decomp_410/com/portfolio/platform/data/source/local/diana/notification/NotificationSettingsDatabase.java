package com.portfolio.platform.data.source.local.diana.notification;

import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class NotificationSettingsDatabase extends RoomDatabase {
    @DexIgnore
    public abstract NotificationSettingsDao getNotificationSettingsDao();
}
