package com.portfolio.platform.data.source.local.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitySampleDao$getActivitySamplesLiveData$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.fitness.ActivitySampleDao this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.local.fitness.ActivitySampleDao$getActivitySamplesLiveData$1$1")
    /* renamed from: com.portfolio.platform.data.source.local.fitness.ActivitySampleDao$getActivitySamplesLiveData$1$1 */
    public static final class C58231<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, Y> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $activitySamples;

        @DexIgnore
        public C58231(java.util.List list) {
            this.$activitySamples = list;
        }

        @DexIgnore
        public final java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample> apply(java.util.List<com.portfolio.platform.data.model.room.fitness.SampleRaw> list) {
            java.util.List list2 = this.$activitySamples;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) list2, "activitySamples");
            if ((!list2.isEmpty()) || list.isEmpty()) {
                return this.$activitySamples;
            }
            this.$activitySamples.clear();
            java.util.List list3 = this.$activitySamples;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) list, "samplesRaw");
            java.util.ArrayList arrayList = new java.util.ArrayList(com.fossil.blesdk.obfuscated.db4.m20831a(list, 10));
            for (com.portfolio.platform.data.model.room.fitness.SampleRaw activitySample : list) {
                arrayList.add(activitySample.toActivitySample());
            }
            list3.addAll(arrayList);
            return this.$activitySamples;
        }
    }

    @DexIgnore
    public ActivitySampleDao$getActivitySamplesLiveData$1(com.portfolio.platform.data.source.local.fitness.ActivitySampleDao activitySampleDao, java.util.Date date, java.util.Date date2) {
        this.this$0 = activitySampleDao;
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    public final androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>> apply(java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample> list) {
        return com.fossil.blesdk.obfuscated.C1935hc.m7843a(this.this$0.getActivitySamplesLiveDataV1(this.$startDate, this.$endDate), new com.portfolio.platform.data.source.local.fitness.ActivitySampleDao$getActivitySamplesLiveData$1.C58231(list));
    }
}
