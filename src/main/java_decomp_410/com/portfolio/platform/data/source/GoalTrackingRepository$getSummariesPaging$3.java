package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository$getSummariesPaging$3 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getSummariesPaging$3(com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryDataSourceFactory goalTrackingSummaryDataSourceFactory) {
        super(0);
        this.$sourceFactory = goalTrackingSummaryDataSourceFactory;
    }

    @DexIgnore
    public final void invoke() {
        com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource a = this.$sourceFactory.getSourceLiveData().mo2275a();
        if (a != null) {
            com.portfolio.platform.helper.PagingRequestHelper mHelper = a.getMHelper();
            if (mHelper != null) {
                mHelper.mo39586b();
            }
        }
    }
}
