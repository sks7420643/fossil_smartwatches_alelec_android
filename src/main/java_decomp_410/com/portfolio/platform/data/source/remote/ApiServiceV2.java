package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.at4;
import com.fossil.blesdk.obfuscated.bt4;
import com.fossil.blesdk.obfuscated.ct4;
import com.fossil.blesdk.obfuscated.em4;
import com.fossil.blesdk.obfuscated.ft4;
import com.fossil.blesdk.obfuscated.gt4;
import com.fossil.blesdk.obfuscated.kt4;
import com.fossil.blesdk.obfuscated.ps4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.qs4;
import com.fossil.blesdk.obfuscated.us4;
import com.fossil.blesdk.obfuscated.ws4;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.ServerFitnessData;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.User;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.Installation;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.commutetime.TrafficRequest;
import com.portfolio.platform.data.model.diana.commutetime.TrafficResponse;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.model.setting.WatchLocalization;
import com.portfolio.platform.data.source.local.alarm.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ApiServiceV2 {
    @DexIgnore
    @ws4(hasBody = true, method = "DELETE", path = "users/me/diana-presets")
    Object batchDeleteDianaPresetList(@ps4 xz1 xz1, yb4<? super qr4<Void>> yb4);

    @DexIgnore
    @ws4(hasBody = true, method = "DELETE", path = "users/me/hybrid-presets")
    Object batchDeleteHybridPresetList(@ps4 xz1 xz1, yb4<? super qr4<Void>> yb4);

    @DexIgnore
    @qs4("users/me/alarms/{id}")
    Object deleteAlarm(@ft4("id") String str, yb4<? super qr4<Void>> yb4);

    @DexIgnore
    @ws4(hasBody = true, method = "DELETE", path = "users/me/alarms")
    Object deleteAlarms(@ps4 xz1 xz1, yb4<? super qr4<ApiResponse<xz1>>> yb4);

    @DexIgnore
    @qs4("users/me/devices/{deviceId}")
    Object deleteDevice(@ft4("deviceId") String str, yb4<? super qr4<Void>> yb4);

    @DexIgnore
    @ws4(hasBody = true, method = "DELETE", path = "users/me/goal-events")
    Object deleteGoalTrackingData(@ps4 xz1 xz1, yb4<? super qr4<GoalEvent>> yb4);

    @DexIgnore
    @qs4("users/me")
    Object deleteUser(yb4<? super qr4<Void>> yb4);

    @DexIgnore
    @us4
    Object downloadFile(@kt4 String str, yb4<? super qr4<em4>> yb4);

    @DexIgnore
    @us4("users/me/heart-rate-daily-summaries")
    Object fetchDailyHeartRateSummaries(@gt4("startDate") String str, @gt4("endDate") String str2, @gt4("offset") int i, @gt4("limit") int i2, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @bt4("rpc/device/generate-pairing-key")
    Object generatePairingKey(@ps4 xz1 xz1, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @us4("users/me/activities")
    Object getActivities(@gt4("startDate") String str, @gt4("endDate") String str2, @gt4("offset") int i, @gt4("limit") int i2, yb4<? super qr4<ApiResponse<Activity>>> yb4);

    @DexIgnore
    @us4("users/me/activity-settings")
    Object getActivitySetting(yb4<? super qr4<ActivitySettings>> yb4);

    @DexIgnore
    @us4("users/me/activity-statistic")
    Object getActivityStatistic(yb4<? super qr4<ActivityStatistic>> yb4);

    @DexIgnore
    @us4("users/me/alarms")
    Object getAlarms(@gt4("limit") int i, yb4<? super qr4<ApiResponse<Alarm>>> yb4);

    @DexIgnore
    @us4("diana-complication-apps")
    Object getAllComplication(@gt4("serialNumber") String str, yb4<? super qr4<ApiResponse<Complication>>> yb4);

    @DexIgnore
    @us4("hybrid-apps")
    Object getAllMicroApp(@gt4("serialNumber") String str, yb4<? super qr4<ApiResponse<MicroApp>>> yb4);

    @DexIgnore
    @us4("hybrid-app-variants")
    Object getAllMicroAppVariant(@gt4("serialNumber") String str, @gt4("majorNumber") String str2, @gt4("minorNumber") String str3, yb4<? super qr4<ApiResponse<MicroAppVariant>>> yb4);

    @DexIgnore
    @us4("diana-pusher-apps")
    Object getAllWatchApp(@gt4("serialNumber") String str, yb4<? super qr4<ApiResponse<WatchApp>>> yb4);

    @DexIgnore
    @us4("app-categories")
    Object getCategories(yb4<? super qr4<ApiResponse<Category>>> yb4);

    @DexIgnore
    @us4("users/me/heart-rate-daily-summaries")
    Object getDailyHeartRateSummaries(@gt4("startDate") String str, @gt4("endDate") String str2, @gt4("offset") int i, @gt4("limit") int i2, yb4<? super qr4<ApiResponse<xz1>>> yb4);

    @DexIgnore
    @us4("users/me/devices/{deviceId}")
    Object getDevice(@ft4("deviceId") String str, yb4<? super qr4<Device>> yb4);

    @DexIgnore
    @us4("assets/app-sku-images")
    Object getDeviceAssets(@gt4("size") int i, @gt4("offset") int i2, @gt4("metadata.serialNumber") String str, @gt4("metadata.feature") String str2, @gt4("metadata.resolution") String str3, @gt4("metadata.platform") String str4, yb4<? super qr4<ApiResponse<xz1>>> yb4);

    @DexIgnore
    @us4("users/me/devices/{id}/secret-key")
    Object getDeviceSecretKey(@ft4("id") String str, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @us4("users/me/devices")
    Object getDevices(yb4<? super qr4<ApiResponse<Device>>> yb4);

    @DexIgnore
    @us4("users/me/diana-presets")
    Object getDianaPresetList(@gt4("serialNumber") String str, yb4<? super qr4<ApiResponse<DianaPreset>>> yb4);

    @DexIgnore
    @us4("diana-recommended-presets")
    Object getDianaRecommendPresetList(@gt4("serialNumber") String str, yb4<? super qr4<ApiResponse<DianaRecommendPreset>>> yb4);

    @DexIgnore
    @us4("users/me/goal-settings")
    Object getGoalSetting(yb4<? super qr4<GoalSetting>> yb4);

    @DexIgnore
    @us4("users/me/goal-events")
    Object getGoalTrackingDataList(@gt4("startDate") String str, @gt4("endDate") String str2, @gt4("offset") int i, @gt4("limit") int i2, yb4<? super qr4<ApiResponse<GoalEvent>>> yb4);

    @DexIgnore
    @us4("users/me/goal-daily-summaries")
    Object getGoalTrackingSummaries(@gt4("startDate") String str, @gt4("endDate") String str2, @gt4("offset") int i, @gt4("limit") int i2, yb4<? super qr4<ApiResponse<GoalDailySummary>>> yb4);

    @DexIgnore
    @us4("users/me/goal-daily-summaries/{date}")
    Object getGoalTrackingSummary(@ft4("date") String str, yb4<? super qr4<GoalDailySummary>> yb4);

    @DexIgnore
    @us4("users/me/heart-rates")
    Object getHeartRateSamples(@gt4("startDate") String str, @gt4("endDate") String str2, @gt4("offset") int i, @gt4("limit") int i2, yb4<? super qr4<ApiResponse<HeartRate>>> yb4);

    @DexIgnore
    @us4("users/me/hybrid-presets")
    Object getHybridPresetList(@gt4("serialNumber") String str, yb4<? super qr4<ApiResponse<HybridPreset>>> yb4);

    @DexIgnore
    @us4("hybrid-recommended-presets")
    Object getHybridRecommendPresetList(@gt4("serialNumber") String str, yb4<? super qr4<ApiResponse<HybridRecommendPreset>>> yb4);

    @DexIgnore
    @us4("users/me/devices/latest-active")
    Object getLastActiveDevice(yb4<? super qr4<Device>> yb4);

    @DexIgnore
    @us4("assets/watch-params")
    Object getLatestWatchParams(@gt4("metadata.serialNumber") String str, @gt4("metadata.version.major") int i, @gt4("sortBy") String str2, @gt4("offset") int i2, @gt4("limit") int i3, yb4<? super qr4<ApiResponse<WatchParameterResponse>>> yb4);

    @DexIgnore
    @us4("rpc/activity/get-recommended-goals")
    Object getRecommendedGoalsRaw(@gt4("age") int i, @gt4("weightInGrams") int i2, @gt4("heightInCentimeters") int i3, @gt4("gender") String str, yb4<? super qr4<ActivityRecommendedGoals>> yb4);

    @DexIgnore
    @us4("rpc/sleep/get-recommended-goals")
    Object getRecommendedSleepGoalRaw(@gt4("age") int i, @gt4("weightInGrams") int i2, @gt4("heightInCentimeters") int i3, @gt4("gender") String str, yb4<? super qr4<SleepRecommendedGoal>> yb4);

    @DexIgnore
    @us4("server-settings")
    Object getServerSettingList(@gt4("limit") int i, @gt4("offset") int i2, yb4<? super qr4<ServerSettingList>> yb4);

    @DexIgnore
    @us4("skus")
    Object getSkus(@gt4("limit") int i, @gt4("offset") int i2, yb4<? super qr4<ApiResponse<SKUModel>>> yb4);

    @DexIgnore
    @us4("users/me/sleep-sessions")
    Object getSleepSessions(@gt4("startDate") String str, @gt4("endDate") String str2, @gt4("offset") int i, @gt4("limit") int i2, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @us4("users/me/sleep-settings")
    Object getSleepSetting(yb4<? super qr4<MFSleepSettings>> yb4);

    @DexIgnore
    @us4("users/me/sleep-statistic")
    Object getSleepStatistic(yb4<? super qr4<SleepStatistic>> yb4);

    @DexIgnore
    @us4("users/me/sleep-daily-summaries")
    Object getSleepSummaries(@gt4("startDate") String str, @gt4("endDate") String str2, @gt4("offset") int i, @gt4("limit") int i2, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @us4("users/me/activity-daily-summaries")
    Object getSummaries(@gt4("startDate") String str, @gt4("endDate") String str2, @gt4("offset") int i, @gt4("limit") int i2, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @bt4("rpc/commute-time/calc-traffic-status")
    Object getTrafficStatus(@ps4 TrafficRequest trafficRequest, yb4<? super qr4<TrafficResponse>> yb4);

    @DexIgnore
    @us4("users/me/profile")
    Object getUser(yb4<? super qr4<User>> yb4);

    @DexIgnore
    @us4("/v2/diana-watch-faces")
    Object getWatchFaces(@gt4("serialNumber") String str, yb4<? super qr4<ApiResponse<WatchFace>>> yb4);

    @DexIgnore
    @us4("/v2/assets/diana-watch-localizations")
    Object getWatchLocalizationData(@gt4("metadata.locale") String str, yb4<? super qr4<ApiResponse<WatchLocalization>>> yb4);

    @DexIgnore
    @us4("weather-info")
    Object getWeather(@gt4("lat") String str, @gt4("lng") String str2, @gt4("temperatureUnit") String str3, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @us4("users/me/workout-sessions")
    Object getWorkoutSessions(@gt4("startDate") String str, @gt4("endDate") String str2, @gt4("offset") int i, @gt4("limit") int i2, yb4<? super qr4<ApiResponse<ServerWorkoutSession>>> yb4);

    @DexIgnore
    @bt4("users/me/activities")
    Object insertActivities(@ps4 xz1 xz1, yb4<? super qr4<ApiResponse<Activity>>> yb4);

    @DexIgnore
    @bt4("users/me/fitness-files")
    Object insertFitnessDataFiles(@ps4 ApiResponse<ServerFitnessData> apiResponse, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @bt4("users/me/goal-events")
    Object insertGoalTrackingDataList(@ps4 xz1 xz1, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @ct4("users/me/installations")
    Object insertInstallation(@ps4 Installation installation, yb4<? super qr4<Installation>> yb4);

    @DexIgnore
    @bt4("users/me/sleep-sessions")
    Object insertSleepSessions(@ps4 xz1 xz1, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @ct4("users/me/devices")
    Object linkDevice(@ps4 Device device, yb4<? super qr4<Void>> yb4);

    @DexIgnore
    @ct4("users/me/diana-presets")
    Object replaceDianaPresetList(@ps4 xz1 xz1, yb4<? super qr4<ApiResponse<DianaPreset>>> yb4);

    @DexIgnore
    @ct4("users/me/hybrid-presets")
    Object replaceHybridPresetList(@ps4 xz1 xz1, yb4<? super qr4<ApiResponse<HybridPreset>>> yb4);

    @DexIgnore
    @at4("users/me/sleep-settings")
    Object setSleepSetting(@ps4 xz1 xz1, yb4<? super qr4<MFSleepSettings>> yb4);

    @DexIgnore
    @bt4("rpc/device/swap-pairing-keys")
    Object swapPairingKey(@ps4 xz1 xz1, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @at4("users/me/activity-settings")
    Object updateActivitySetting(@ps4 xz1 xz1, yb4<? super qr4<ActivitySettings>> yb4);

    @DexIgnore
    @at4("users/me/devices/{deviceId}")
    Object updateDevice(@ft4("deviceId") String str, @ps4 Device device, yb4<? super qr4<Void>> yb4);

    @DexIgnore
    @at4("users/me/devices/{id}/secret-key")
    Object updateDeviceSecretKey(@ft4("id") String str, @ps4 xz1 xz1, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @at4("users/me/profile")
    Object updateUser(@ps4 xz1 xz1, yb4<? super qr4<User>> yb4);

    @DexIgnore
    @at4("users/me/alarms")
    Object upsertAlarms(@ps4 xz1 xz1, yb4<? super qr4<ApiResponse<Alarm>>> yb4);

    @DexIgnore
    @at4("users/me/diana-presets")
    Object upsertDianaPresetList(@ps4 xz1 xz1, yb4<? super qr4<ApiResponse<DianaPreset>>> yb4);

    @DexIgnore
    @at4("users/me/goal-settings")
    Object upsertGoalSetting(@ps4 xz1 xz1, yb4<? super qr4<GoalSetting>> yb4);

    @DexIgnore
    @at4("users/me/hybrid-presets")
    Object upsertHybridPresetList(@ps4 xz1 xz1, yb4<? super qr4<ApiResponse<HybridPreset>>> yb4);
}
