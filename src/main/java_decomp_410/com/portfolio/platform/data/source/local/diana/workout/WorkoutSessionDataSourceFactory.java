package com.portfolio.platform.data.source.local.diana.workout;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ld;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSessionDataSourceFactory extends ld.b<Long, WorkoutSession> {
    @DexIgnore
    public /* final */ h42 appExecutors;
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public /* final */ FitnessDataDao fitnessDataDao;
    @DexIgnore
    public /* final */ PagingRequestHelper.a listener;
    @DexIgnore
    public WorkoutSessionLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ MutableLiveData<WorkoutSessionLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ WorkoutDao workoutDao;
    @DexIgnore
    public /* final */ FitnessDatabase workoutDatabase;
    @DexIgnore
    public /* final */ WorkoutSessionRepository workoutSessionRepository;

    @DexIgnore
    public WorkoutSessionDataSourceFactory(WorkoutSessionRepository workoutSessionRepository2, FitnessDataDao fitnessDataDao2, WorkoutDao workoutDao2, FitnessDatabase fitnessDatabase, Date date, h42 h42, PagingRequestHelper.a aVar) {
        kd4.b(workoutSessionRepository2, "workoutSessionRepository");
        kd4.b(fitnessDataDao2, "fitnessDataDao");
        kd4.b(workoutDao2, "workoutDao");
        kd4.b(fitnessDatabase, "workoutDatabase");
        kd4.b(date, "currentDate");
        kd4.b(h42, "appExecutors");
        kd4.b(aVar, "listener");
        this.workoutSessionRepository = workoutSessionRepository2;
        this.fitnessDataDao = fitnessDataDao2;
        this.workoutDao = workoutDao2;
        this.workoutDatabase = fitnessDatabase;
        this.currentDate = date;
        this.appExecutors = h42;
        this.listener = aVar;
    }

    @DexIgnore
    public ld<Long, WorkoutSession> create() {
        this.localDataSource = new WorkoutSessionLocalDataSource(this.workoutSessionRepository, this.fitnessDataDao, this.workoutDao, this.workoutDatabase, this.currentDate, this.appExecutors, this.listener);
        this.sourceLiveData.a(this.localDataSource);
        WorkoutSessionLocalDataSource workoutSessionLocalDataSource = this.localDataSource;
        if (workoutSessionLocalDataSource != null) {
            return workoutSessionLocalDataSource;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final WorkoutSessionLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<WorkoutSessionLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(WorkoutSessionLocalDataSource workoutSessionLocalDataSource) {
        this.localDataSource = workoutSessionLocalDataSource;
    }
}
