package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository$getSummary$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $date;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.GoalTrackingRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$1$1")
    /* renamed from: com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$1$1 */
    public static final class C56991 extends com.portfolio.platform.util.NetworkBoundResource<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary, com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $pendingList;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$1 this$0;

        @DexIgnore
        public C56991(com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$1 goalTrackingRepository$getSummary$1, java.util.List list) {
            this.this$0 = goalTrackingRepository$getSummary$1;
            this.$pendingList = list;
        }

        @DexIgnore
        public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary>> yb4) {
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiServiceV2$p = this.this$0.this$0.mApiServiceV2;
            java.lang.String e = com.fossil.blesdk.obfuscated.rk2.m27397e(this.this$0.$date);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.formatShortDate(date)");
            return access$getMApiServiceV2$p.getGoalTrackingSummary(e, yb4);
        }

        @DexIgnore
        public androidx.lifecycle.LiveData<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary> loadFromDb() {
            return this.this$0.this$0.mGoalTrackingDao.getGoalTrackingSummaryLiveData(this.this$0.$date);
        }

        @DexIgnore
        public void onFetchFailed(java.lang.Throwable th) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(com.portfolio.platform.data.source.GoalTrackingRepository.Companion.getTAG(), "getSummary onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary goalDailySummary) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(goalDailySummary, "item");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tag = com.portfolio.platform.data.source.GoalTrackingRepository.Companion.getTAG();
            local.mo33255d(tag, "getSummary date=" + this.this$0.$date + " saveCallResult onResponse: response = " + goalDailySummary);
            try {
                com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao access$getMGoalTrackingDao$p = this.this$0.this$0.mGoalTrackingDao;
                com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary goalTrackingSummary = goalDailySummary.toGoalTrackingSummary();
                if (goalTrackingSummary != null) {
                    access$getMGoalTrackingDao$p.upsertGoalTrackingSummary(goalTrackingSummary);
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            } catch (java.lang.Exception e) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String tag2 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion.getTAG();
                local2.mo33256e(tag2, "getSummary date=" + this.this$0.$date + " exception=" + e);
                e.printStackTrace();
            }
        }

        @DexIgnore
        public boolean shouldFetch(com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary goalTrackingSummary) {
            return this.$pendingList.isEmpty();
        }
    }

    @DexIgnore
    public GoalTrackingRepository$getSummary$1(com.portfolio.platform.data.source.GoalTrackingRepository goalTrackingRepository, java.util.Date date) {
        this.this$0 = goalTrackingRepository;
        this.$date = date;
    }

    @DexIgnore
    public final androidx.lifecycle.LiveData<com.fossil.blesdk.obfuscated.os3<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>> apply(java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> list) {
        return new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$1.C56991(this, list).asLiveData();
    }
}
