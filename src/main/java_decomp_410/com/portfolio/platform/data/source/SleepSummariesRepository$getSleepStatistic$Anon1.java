package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.util.NetworkBoundResource;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository$getSleepStatistic$Anon1 extends NetworkBoundResource<SleepStatistic, SleepStatistic> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$Anon0;

    @DexIgnore
    public SleepSummariesRepository$getSleepStatistic$Anon1(SleepSummariesRepository sleepSummariesRepository, boolean z) {
        this.this$Anon0 = sleepSummariesRepository;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public Object createCall(yb4<? super qr4<SleepStatistic>> yb4) {
        return this.this$Anon0.mApiService.getSleepStatistic(yb4);
    }

    @DexIgnore
    public LiveData<SleepStatistic> loadFromDb() {
        return this.this$Anon0.mSleepDao.getSleepStatisticLiveData();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "getSleepStatistic - onFetchFailed");
    }

    @DexIgnore
    public void saveCallResult(SleepStatistic sleepStatistic) {
        kd4.b(sleepStatistic, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tAG$app_fossilRelease = SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
        local.d(tAG$app_fossilRelease, "getSleepStatistic - saveCallResult -- item=" + sleepStatistic);
        fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1(this, sleepStatistic, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public boolean shouldFetch(SleepStatistic sleepStatistic) {
        return this.$shouldFetch;
    }
}
