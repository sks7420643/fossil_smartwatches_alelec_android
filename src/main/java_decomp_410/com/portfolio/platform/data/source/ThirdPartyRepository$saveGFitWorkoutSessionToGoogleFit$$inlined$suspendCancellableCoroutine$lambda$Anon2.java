package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.dg4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.sn1;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.List;
import kotlin.Result;
import kotlin.jvm.internal.Ref$IntRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2 implements sn1 {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ dg4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$IntRef $countSizeOfList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $gFitWorkoutSessionList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ GoogleSignInAccount $googleSignInAccount$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfGFitWorkoutSessionList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$Anon0;

    @DexIgnore
    public ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(GoogleSignInAccount googleSignInAccount, Ref$IntRef ref$IntRef, int i, dg4 dg4, ThirdPartyRepository thirdPartyRepository, List list, String str) {
        this.$googleSignInAccount$inlined = googleSignInAccount;
        this.$countSizeOfList$inlined = ref$IntRef;
        this.$sizeOfGFitWorkoutSessionList$inlined = i;
        this.$continuation$inlined = dg4;
        this.this$Anon0 = thirdPartyRepository;
        this.$gFitWorkoutSessionList$inlined = list;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    public final void onFailure(Exception exc) {
        kd4.b(exc, "it");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("There was a problem inserting the session: ");
        exc.printStackTrace();
        sb.append(qa4.a);
        local.e(ThirdPartyRepository.TAG, sb.toString());
        Ref$IntRef ref$IntRef = this.$countSizeOfList$inlined;
        ref$IntRef.element++;
        if (ref$IntRef.element >= this.$sizeOfGFitWorkoutSessionList$inlined && this.$continuation$inlined.isActive()) {
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveGFitWorkoutSessionToGoogleFit");
            dg4 dg4 = this.$continuation$inlined;
            Result.a aVar = Result.Companion;
            dg4.resumeWith(Result.m3constructorimpl((Object) null));
        }
    }
}
