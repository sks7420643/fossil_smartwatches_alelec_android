package com.portfolio.platform.data.source.local.thirdparty;

import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ThirdPartyDatabase extends RoomDatabase {
    @DexIgnore
    public abstract GFitActiveTimeDao getGFitActiveTimeDao();

    @DexIgnore
    public abstract GFitHeartRateDao getGFitHeartRateDao();

    @DexIgnore
    public abstract GFitSampleDao getGFitSampleDao();

    @DexIgnore
    public abstract GFitSleepDao getGFitSleepDao();

    @DexIgnore
    public abstract GFitWorkoutSessionDao getGFitWorkoutSessionDao();

    @DexIgnore
    public abstract UASampleDao getUASampleDao();
}
