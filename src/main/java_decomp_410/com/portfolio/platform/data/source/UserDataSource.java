package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class UserDataSource {
    @DexIgnore
    public static /* synthetic */ Object checkAuthenticationEmailExisting$suspendImpl(UserDataSource userDataSource, String str, yb4 yb4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object checkAuthenticationSocialExisting$suspendImpl(UserDataSource userDataSource, String str, String str2, yb4 yb4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loadUserInfo$suspendImpl(UserDataSource userDataSource, MFUser mFUser, yb4 yb4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loginEmail$suspendImpl(UserDataSource userDataSource, String str, String str2, yb4 yb4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loginWithSocial$suspendImpl(UserDataSource userDataSource, String str, String str2, String str3, yb4 yb4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object requestEmailOtp$suspendImpl(UserDataSource userDataSource, String str, yb4 yb4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object resetPassword$suspendImpl(UserDataSource userDataSource, String str, yb4 yb4) {
        throw null;
        // return new ro2(dc4.a(200), false, 2, (fd4) null);
    }

    @DexIgnore
    public static /* synthetic */ Object signUpEmail$suspendImpl(UserDataSource userDataSource, SignUpEmailAuth signUpEmailAuth, yb4 yb4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object signUpSocial$suspendImpl(UserDataSource userDataSource, SignUpSocialAuth signUpSocialAuth, yb4 yb4) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object verifyEmailOtp$suspendImpl(UserDataSource userDataSource, String str, String str2, yb4 yb4) {
        return null;
    }

    @DexIgnore
    public Object checkAuthenticationEmailExisting(String str, yb4<? super qo2<Boolean>> yb4) {
        return checkAuthenticationEmailExisting$suspendImpl(this, str, yb4);
    }

    @DexIgnore
    public Object checkAuthenticationSocialExisting(String str, String str2, yb4<? super qo2<Boolean>> yb4) {
        return checkAuthenticationSocialExisting$suspendImpl(this, str, str2, yb4);
    }

    @DexIgnore
    public void clearAllUser() {
    }

    @DexIgnore
    public Object deleteUser(MFUser mFUser, yb4<? super Integer> yb4) {
        return dc4.a(0);
    }

    @DexIgnore
    public MFUser getCurrentUser() {
        return null;
    }

    @DexIgnore
    public abstract void insertUser(MFUser mFUser);

    @DexIgnore
    public Object loadUserInfo(MFUser mFUser, yb4<? super qo2<MFUser>> yb4) {
        return loadUserInfo$suspendImpl(this, mFUser, yb4);
    }

    @DexIgnore
    public Object loginEmail(String str, String str2, yb4<? super qo2<Auth>> yb4) {
        return loginEmail$suspendImpl(this, str, str2, yb4);
    }

    @DexIgnore
    public Object loginWithSocial(String str, String str2, String str3, yb4<? super qo2<Auth>> yb4) {
        return loginWithSocial$suspendImpl(this, str, str2, str3, yb4);
    }

    @DexIgnore
    public Object logoutUser(yb4<? super Integer> yb4) {
        return dc4.a(0);
    }

    @DexIgnore
    public Object requestEmailOtp(String str, yb4<? super qo2<Void>> yb4) {
        return requestEmailOtp$suspendImpl(this, str, yb4);
    }

    @DexIgnore
    public Object resetPassword(String str, yb4<? super qo2<Integer>> yb4) {
        return resetPassword$suspendImpl(this, str, yb4);
    }

    @DexIgnore
    public Object signUpEmail(SignUpEmailAuth signUpEmailAuth, yb4<? super qo2<Auth>> yb4) {
        return signUpEmail$suspendImpl(this, signUpEmailAuth, yb4);
    }

    @DexIgnore
    public Object signUpSocial(SignUpSocialAuth signUpSocialAuth, yb4<? super qo2<Auth>> yb4) {
        return signUpSocial$suspendImpl(this, signUpSocialAuth, yb4);
    }

    @DexIgnore
    public abstract Object updateUser(MFUser mFUser, boolean z, yb4<? super qo2<MFUser>> yb4);

    @DexIgnore
    public Object verifyEmailOtp(String str, String str2, yb4<? super qo2<Void>> yb4) {
        return verifyEmailOtp$suspendImpl(this, str, str2, yb4);
    }
}
