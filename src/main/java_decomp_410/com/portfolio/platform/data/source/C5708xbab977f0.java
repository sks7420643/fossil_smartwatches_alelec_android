package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory */
public final class C5708xbab977f0 implements dagger.internal.Factory<com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase> {
    @DexIgnore
    public /* final */ javax.inject.Provider<com.portfolio.platform.PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ com.portfolio.platform.data.source.PortfolioDatabaseModule module;

    @DexIgnore
    public C5708xbab977f0(com.portfolio.platform.data.source.PortfolioDatabaseModule portfolioDatabaseModule, javax.inject.Provider<com.portfolio.platform.PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static com.portfolio.platform.data.source.C5708xbab977f0 create(com.portfolio.platform.data.source.PortfolioDatabaseModule portfolioDatabaseModule, javax.inject.Provider<com.portfolio.platform.PortfolioApp> provider) {
        return new com.portfolio.platform.data.source.C5708xbab977f0(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase provideInstance(com.portfolio.platform.data.source.PortfolioDatabaseModule portfolioDatabaseModule, javax.inject.Provider<com.portfolio.platform.PortfolioApp> provider) {
        return proxyProvideNotificationSettingsDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase proxyProvideNotificationSettingsDatabase(com.portfolio.platform.data.source.PortfolioDatabaseModule portfolioDatabaseModule, com.portfolio.platform.PortfolioApp portfolioApp) {
        com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase provideNotificationSettingsDatabase = portfolioDatabaseModule.provideNotificationSettingsDatabase(portfolioApp);
        com.fossil.blesdk.obfuscated.n44.m25602a(provideNotificationSettingsDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideNotificationSettingsDatabase;
    }

    @DexIgnore
    public com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
