package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.gt4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.us4;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface GoogleApiService {
    @DexIgnore
    @us4("geocode/json")
    Object getAddress(@gt4("latlng") String str, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @us4("geocode/json")
    Object getAddressWithType(@gt4("latlng") String str, @gt4("result_type") String str2, yb4<? super qr4<xz1>> yb4);
}
