package com.portfolio.platform.data.source.local.inapp;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.InAppNotification;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class InAppNotificationRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "InAppNotificationRepository";
    @DexIgnore
    public /* final */ InAppNotificationDao mInAppNotificationDao;
    @DexIgnore
    public /* final */ List<InAppNotification> mNotificationList; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public InAppNotificationRepository(InAppNotificationDao inAppNotificationDao) {
        kd4.b(inAppNotificationDao, "mInAppNotificationDao");
        this.mInAppNotificationDao = inAppNotificationDao;
    }

    @DexIgnore
    public final Object delete(InAppNotification inAppNotification, yb4<? super qa4> yb4) {
        this.mInAppNotificationDao.delete(inAppNotification);
        return qa4.a;
    }

    @DexIgnore
    public final LiveData<List<InAppNotification>> getAllInAppNotifications() {
        return this.mInAppNotificationDao.getAllInAppNotification();
    }

    @DexIgnore
    public final void handleReceivingNotification(InAppNotification inAppNotification) {
        kd4.b(inAppNotification, "inAppNotification");
        this.mNotificationList.add(inAppNotification);
    }

    @DexIgnore
    public final Object insert(List<InAppNotification> list, yb4<? super qa4> yb4) {
        this.mInAppNotificationDao.insertListInAppNotification(list);
        return qa4.a;
    }

    @DexIgnore
    public final void removeNotificationAfterSending(InAppNotification inAppNotification) {
        kd4.b(inAppNotification, "inAppNotification");
        this.mNotificationList.remove(inAppNotification);
    }
}
