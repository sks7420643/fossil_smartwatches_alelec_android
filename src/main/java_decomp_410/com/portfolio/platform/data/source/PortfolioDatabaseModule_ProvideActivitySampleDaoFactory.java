package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import com.portfolio.platform.data.source.local.fitness.ActivitySampleDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideActivitySampleDaoFactory implements Factory<ActivitySampleDao> {
    @DexIgnore
    public /* final */ Provider<FitnessDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideActivitySampleDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideActivitySampleDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideActivitySampleDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static ActivitySampleDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        return proxyProvideActivitySampleDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static ActivitySampleDao proxyProvideActivitySampleDao(PortfolioDatabaseModule portfolioDatabaseModule, FitnessDatabase fitnessDatabase) {
        ActivitySampleDao provideActivitySampleDao = portfolioDatabaseModule.provideActivitySampleDao(fitnessDatabase);
        n44.a(provideActivitySampleDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideActivitySampleDao;
    }

    @DexIgnore
    public ActivitySampleDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
