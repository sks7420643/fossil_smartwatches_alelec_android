package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpSocialAuth;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$response$Anon1", f = "UserRemoteDataSource.kt", l = {202}, m = "invokeSuspend")
public final class UserRemoteDataSource$signUpSocial$response$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<Auth>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ SignUpSocialAuth $socialAuth;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ UserRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRemoteDataSource$signUpSocial$response$Anon1(UserRemoteDataSource userRemoteDataSource, SignUpSocialAuth signUpSocialAuth, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = userRemoteDataSource;
        this.$socialAuth = signUpSocialAuth;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new UserRemoteDataSource$signUpSocial$response$Anon1(this.this$Anon0, this.$socialAuth, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((UserRemoteDataSource$signUpSocial$response$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            AuthApiGuestService access$getMAuthApiGuestService$p = this.this$Anon0.mAuthApiGuestService;
            SignUpSocialAuth signUpSocialAuth = this.$socialAuth;
            this.label = 1;
            obj = access$getMAuthApiGuestService$p.registerSocial(signUpSocialAuth, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
