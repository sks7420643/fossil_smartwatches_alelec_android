package com.portfolio.platform.data.source.local.hybrid.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1", mo27670f = "GoalTrackingSummaryLocalDataSource.kt", mo27671l = {149, 153, 158}, mo27672m = "invokeSuspend")
public final class GoalTrackingSummaryLocalDataSource$loadData$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.helper.PagingRequestHelper.RequestType $requestType;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $startDate;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21125p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1$1", mo27670f = "GoalTrackingSummaryLocalDataSource.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1$1 */
    public static final class C58621 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21126p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C58621(com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1 goalTrackingSummaryLocalDataSource$loadData$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = goalTrackingSummaryLocalDataSource$loadData$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1.C58621 r0 = new com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1.C58621(this.this$0, yb4);
            r0.f21126p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1.C58621) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.$helperCallback.mo39588a();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1$2")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1$2", mo27670f = "GoalTrackingSummaryLocalDataSource.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1$2 */
    public static final class C58632 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.qo2 $data;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21127p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C58632(com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1 goalTrackingSummaryLocalDataSource$loadData$1, com.fossil.blesdk.obfuscated.qo2 qo2, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = goalTrackingSummaryLocalDataSource$loadData$1;
            this.$data = qo2;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1.C58632 r0 = new com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1.C58632(this.this$0, this.$data, yb4);
            r0.f21127p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1.C58632) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                if (((com.fossil.blesdk.obfuscated.po2) this.$data).mo30179d() != null) {
                    this.this$0.$helperCallback.mo39589a(((com.fossil.blesdk.obfuscated.po2) this.$data).mo30179d());
                } else if (((com.fossil.blesdk.obfuscated.po2) this.$data).mo30178c() != null) {
                    com.portfolio.platform.data.model.ServerError c = ((com.fossil.blesdk.obfuscated.po2) this.$data).mo30178c();
                    com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a aVar = this.this$0.$helperCallback;
                    java.lang.String userMessage = c.getUserMessage();
                    if (userMessage == null) {
                        userMessage = c.getMessage();
                    }
                    if (userMessage == null) {
                        userMessage = "";
                    }
                    aVar.mo39589a(new java.lang.Throwable(userMessage));
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingSummaryLocalDataSource$loadData$1(com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource, java.util.Date date, java.util.Date date2, com.portfolio.platform.helper.PagingRequestHelper.RequestType requestType, com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a aVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = goalTrackingSummaryLocalDataSource;
        this.$startDate = date;
        this.$endDate = date2;
        this.$requestType = requestType;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1 goalTrackingSummaryLocalDataSource$loadData$1 = new com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1(this.this$0, this.$startDate, this.$endDate, this.$requestType, this.$helperCallback, yb4);
        goalTrackingSummaryLocalDataSource$loadData$1.f21125p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return goalTrackingSummaryLocalDataSource$loadData$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> list;
        kotlin.Pair<java.util.Date, java.util.Date> pair;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f21125p$;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d(com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource.TAG, "loadData start=" + this.$startDate + ", end=" + this.$endDate);
            list = this.this$0.mGoalTrackingRepository.getPendingGoalTrackingDataList(this.$startDate, this.$endDate);
            pair = com.portfolio.platform.data.model.goaltracking.GoalTrackingDataKt.calculateRangeDownload(list, this.$startDate, this.$endDate);
            if (pair != null) {
                this.L$0 = zg4;
                this.L$1 = list;
                this.L$2 = pair;
                this.label = 1;
                obj = this.this$0.mGoalTrackingRepository.loadSummaries(pair.getFirst(), pair.getSecond(), this);
                if (obj == a) {
                    return a;
                }
            } else {
                this.$helperCallback.mo39588a();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        } else if (i == 1) {
            pair = (kotlin.Pair) this.L$2;
            list = (java.util.List) this.L$1;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else if (i == 2 || i == 3) {
            com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) this.L$3;
            kotlin.Pair pair2 = (kotlin.Pair) this.L$2;
            java.util.List list2 = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo22 = (com.fossil.blesdk.obfuscated.qo2) obj;
        if (qo22 instanceof com.fossil.blesdk.obfuscated.ro2) {
            if (this.$requestType == com.portfolio.platform.helper.PagingRequestHelper.RequestType.AFTER && (!this.this$0.mRequestAfterQueue.isEmpty())) {
                this.this$0.mRequestAfterQueue.remove(0);
            }
            com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1.C58621 r4 = new com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1.C58621(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = list;
            this.L$2 = pair;
            this.L$3 = qo22;
            this.label = 2;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r4, this) == a) {
                return a;
            }
        } else if (qo22 instanceof com.fossil.blesdk.obfuscated.po2) {
            com.fossil.blesdk.obfuscated.pi4 c2 = com.fossil.blesdk.obfuscated.nh4.m25693c();
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1.C58632 r42 = new com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1.C58632(this, qo22, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = list;
            this.L$2 = pair;
            this.L$3 = qo22;
            this.label = 3;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(c2, r42, this) == a) {
                return a;
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
