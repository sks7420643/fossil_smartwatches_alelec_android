package com.portfolio.platform.data.source.local.sleep;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$Anon1", f = "SleepSummaryLocalDataSource.kt", l = {179, 179}, m = "invokeSuspend")
public final class SleepSummaryLocalDataSource$loadData$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.RequestType $requestType;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public Object L$Anon4;
    @DexIgnore
    public Object L$Anon5;
    @DexIgnore
    public Object L$Anon6;
    @DexIgnore
    public Object L$Anon7;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummaryLocalDataSource$loadData$Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource, Date date, Date date2, PagingRequestHelper.RequestType requestType, PagingRequestHelper.b.a aVar, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = sleepSummaryLocalDataSource;
        this.$startDate = date;
        this.$endDate = date2;
        this.$requestType = requestType;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SleepSummaryLocalDataSource$loadData$Anon1 sleepSummaryLocalDataSource$loadData$Anon1 = new SleepSummaryLocalDataSource$loadData$Anon1(this.this$Anon0, this.$startDate, this.$endDate, this.$requestType, this.$helperCallback, yb4);
        sleepSummaryLocalDataSource$loadData$Anon1.p$ = (zg4) obj;
        return sleepSummaryLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepSummaryLocalDataSource$loadData$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource;
        Object obj2;
        PagingRequestHelper.RequestType requestType;
        qo2 qo2;
        gh4 gh4;
        List<FitnessDataWrapper> list;
        Pair<Date, Date> pair;
        zg4 zg4;
        gh4 gh42;
        Object obj3;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg42 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SleepSummaryLocalDataSource.TAG, "loadData start=" + this.$startDate + ", end=" + this.$endDate);
            list = this.this$Anon0.mFitnessDataRepository.getFitnessData(this.$startDate, this.$endDate);
            pair = FitnessDataWrapperKt.calculateRangeDownload(list, this.$startDate, this.$endDate);
            if (pair != null) {
                zg4 zg43 = zg42;
                gh4 a2 = ag4.a(zg43, (CoroutineContext) null, (CoroutineStart) null, new SleepSummaryLocalDataSource$loadData$Anon1$summariesDeferred$Anon1(this, pair, (yb4) null), 3, (Object) null);
                gh42 = ag4.a(zg43, (CoroutineContext) null, (CoroutineStart) null, new SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1(this, pair, (yb4) null), 3, (Object) null);
                sleepSummaryLocalDataSource = this.this$Anon0;
                PagingRequestHelper.RequestType requestType2 = this.$requestType;
                this.L$Anon0 = zg42;
                this.L$Anon1 = list;
                this.L$Anon2 = pair;
                this.L$Anon3 = a2;
                this.L$Anon4 = gh42;
                this.L$Anon5 = sleepSummaryLocalDataSource;
                this.L$Anon6 = requestType2;
                this.label = 1;
                obj3 = a2.a(this);
                if (obj3 == a) {
                    return a;
                }
                gh4 gh43 = a2;
                zg4 = zg42;
                requestType = requestType2;
                gh4 = gh43;
            } else {
                this.$helperCallback.a();
                return qa4.a;
            }
        } else if (i == 1) {
            requestType = (PagingRequestHelper.RequestType) this.L$Anon6;
            gh42 = (gh4) this.L$Anon4;
            pair = (Pair) this.L$Anon2;
            list = (List) this.L$Anon1;
            na4.a(obj);
            gh4 = (gh4) this.L$Anon3;
            zg4 = (zg4) this.L$Anon0;
            sleepSummaryLocalDataSource = (SleepSummaryLocalDataSource) this.L$Anon5;
            obj3 = obj;
        } else if (i == 2) {
            qo2 = (qo2) this.L$Anon7;
            requestType = (PagingRequestHelper.RequestType) this.L$Anon6;
            gh4 gh44 = (gh4) this.L$Anon4;
            gh4 gh45 = (gh4) this.L$Anon3;
            Pair pair2 = (Pair) this.L$Anon2;
            List list2 = (List) this.L$Anon1;
            zg4 zg44 = (zg4) this.L$Anon0;
            na4.a(obj);
            sleepSummaryLocalDataSource = (SleepSummaryLocalDataSource) this.L$Anon5;
            obj2 = obj;
            sleepSummaryLocalDataSource.combineData(requestType, qo2, (qo2) obj2, this.$helperCallback);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo22 = (qo2) obj3;
        this.L$Anon0 = zg4;
        this.L$Anon1 = list;
        this.L$Anon2 = pair;
        this.L$Anon3 = gh4;
        this.L$Anon4 = gh42;
        this.L$Anon5 = sleepSummaryLocalDataSource;
        this.L$Anon6 = requestType;
        this.L$Anon7 = qo22;
        this.label = 2;
        obj2 = gh42.a(this);
        if (obj2 == a) {
            return a;
        }
        qo2 = qo22;
        sleepSummaryLocalDataSource.combineData(requestType, qo2, (qo2) obj2, this.$helperCallback);
        return qa4.a;
    }
}
