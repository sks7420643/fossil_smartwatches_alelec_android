package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.n44;
import com.fossil.blesdk.obfuscated.xk2;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideFitnessHelperFactory implements Factory<xk2> {
    @DexIgnore
    public /* final */ Provider<ActivitySummaryDao> activitySummaryDaoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;
    @DexIgnore
    public /* final */ Provider<en2> sharedPreferencesManagerProvider;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideFitnessHelperFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<en2> provider, Provider<ActivitySummaryDao> provider2) {
        this.module = portfolioDatabaseModule;
        this.sharedPreferencesManagerProvider = provider;
        this.activitySummaryDaoProvider = provider2;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideFitnessHelperFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<en2> provider, Provider<ActivitySummaryDao> provider2) {
        return new PortfolioDatabaseModule_ProvideFitnessHelperFactory(portfolioDatabaseModule, provider, provider2);
    }

    @DexIgnore
    public static xk2 provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<en2> provider, Provider<ActivitySummaryDao> provider2) {
        return proxyProvideFitnessHelper(portfolioDatabaseModule, provider.get(), provider2.get());
    }

    @DexIgnore
    public static xk2 proxyProvideFitnessHelper(PortfolioDatabaseModule portfolioDatabaseModule, en2 en2, ActivitySummaryDao activitySummaryDao) {
        xk2 provideFitnessHelper = portfolioDatabaseModule.provideFitnessHelper(en2, activitySummaryDao);
        n44.a(provideFitnessHelper, "Cannot return null from a non-@Nullable @Provides method");
        return provideFitnessHelper;
    }

    @DexIgnore
    public xk2 get() {
        return provideInstance(this.module, this.sharedPreferencesManagerProvider, this.activitySummaryDaoProvider);
    }
}
