package com.portfolio.platform.data.source.local;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomizeRealDataDatabase_Impl extends CustomizeRealDataDatabase {
    @DexIgnore
    public volatile CustomizeRealDataDao _customizeRealDataDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `customizeRealData` (`id` TEXT NOT NULL, `value` TEXT NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '6ca914ed7b753a02dc3ff84030dc3e24')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `customizeRealData`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (CustomizeRealDataDatabase_Impl.this.mCallbacks != null) {
                int size = CustomizeRealDataDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) CustomizeRealDataDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = CustomizeRealDataDatabase_Impl.this.mDatabase = ggVar;
            CustomizeRealDataDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (CustomizeRealDataDatabase_Impl.this.mCallbacks != null) {
                int size = CustomizeRealDataDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) CustomizeRealDataDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            HashMap hashMap = new HashMap(2);
            hashMap.put("id", new eg.a("id", "TEXT", true, 1));
            hashMap.put("value", new eg.a("value", "TEXT", true, 0));
            eg egVar = new eg("customizeRealData", hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar, "customizeRealData");
            if (!egVar.equals(a)) {
                throw new IllegalStateException("Migration didn't properly handle customizeRealData(com.portfolio.platform.data.model.CustomizeRealData).\n Expected:\n" + egVar + "\n Found:\n" + a);
            }
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `customizeRealData`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), "customizeRealData");
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(1), "6ca914ed7b753a02dc3ff84030dc3e24", "8e8005f3461a63f640274ec577069e4f");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }

    @DexIgnore
    public CustomizeRealDataDao realDataDao() {
        CustomizeRealDataDao customizeRealDataDao;
        if (this._customizeRealDataDao != null) {
            return this._customizeRealDataDao;
        }
        synchronized (this) {
            if (this._customizeRealDataDao == null) {
                this._customizeRealDataDao = new CustomizeRealDataDao_Impl(this);
            }
            customizeRealDataDao = this._customizeRealDataDao;
        }
        return customizeRealDataDao;
    }
}
