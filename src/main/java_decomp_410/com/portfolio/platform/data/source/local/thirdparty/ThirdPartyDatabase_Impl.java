package com.portfolio.platform.data.source.local.thirdparty;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ThirdPartyDatabase_Impl extends ThirdPartyDatabase {
    @DexIgnore
    public volatile GFitActiveTimeDao _gFitActiveTimeDao;
    @DexIgnore
    public volatile GFitHeartRateDao _gFitHeartRateDao;
    @DexIgnore
    public volatile GFitSampleDao _gFitSampleDao;
    @DexIgnore
    public volatile GFitSleepDao _gFitSleepDao;
    @DexIgnore
    public volatile GFitWorkoutSessionDao _gFitWorkoutSessionDao;
    @DexIgnore
    public volatile UASampleDao _uASampleDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `gFitSample` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `step` INTEGER NOT NULL, `distance` REAL NOT NULL, `calorie` REAL NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            ggVar.b("CREATE TABLE IF NOT EXISTS `gFitActiveTime` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `activeTimes` TEXT NOT NULL)");
            ggVar.b("CREATE TABLE IF NOT EXISTS `gFitHeartRate` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `value` REAL NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            ggVar.b("CREATE TABLE IF NOT EXISTS `gFitWorkoutSession` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL, `workoutType` INTEGER NOT NULL, `steps` TEXT NOT NULL, `calories` TEXT NOT NULL, `distances` TEXT NOT NULL, `heartRates` TEXT NOT NULL)");
            ggVar.b("CREATE TABLE IF NOT EXISTS `uaSample` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `step` INTEGER NOT NULL, `distance` REAL NOT NULL, `calorie` REAL NOT NULL, `time` INTEGER NOT NULL)");
            ggVar.b("CREATE TABLE IF NOT EXISTS `gFitSleep` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `sleepMins` INTEGER NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '4a002011cd11b3072c8efe0a76ce12f3')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `gFitSample`");
            ggVar.b("DROP TABLE IF EXISTS `gFitActiveTime`");
            ggVar.b("DROP TABLE IF EXISTS `gFitHeartRate`");
            ggVar.b("DROP TABLE IF EXISTS `gFitWorkoutSession`");
            ggVar.b("DROP TABLE IF EXISTS `uaSample`");
            ggVar.b("DROP TABLE IF EXISTS `gFitSleep`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (ThirdPartyDatabase_Impl.this.mCallbacks != null) {
                int size = ThirdPartyDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) ThirdPartyDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = ThirdPartyDatabase_Impl.this.mDatabase = ggVar;
            ThirdPartyDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (ThirdPartyDatabase_Impl.this.mCallbacks != null) {
                int size = ThirdPartyDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) ThirdPartyDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            gg ggVar2 = ggVar;
            HashMap hashMap = new HashMap(6);
            hashMap.put("id", new eg.a("id", "INTEGER", true, 1));
            hashMap.put("step", new eg.a("step", "INTEGER", true, 0));
            hashMap.put("distance", new eg.a("distance", "REAL", true, 0));
            hashMap.put("calorie", new eg.a("calorie", "REAL", true, 0));
            hashMap.put(SampleRaw.COLUMN_START_TIME, new eg.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0));
            hashMap.put(SampleRaw.COLUMN_END_TIME, new eg.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0));
            eg egVar = new eg("gFitSample", hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar2, "gFitSample");
            if (egVar.equals(a)) {
                HashMap hashMap2 = new HashMap(2);
                hashMap2.put("id", new eg.a("id", "INTEGER", true, 1));
                hashMap2.put("activeTimes", new eg.a("activeTimes", "TEXT", true, 0));
                eg egVar2 = new eg("gFitActiveTime", hashMap2, new HashSet(0), new HashSet(0));
                eg a2 = eg.a(ggVar2, "gFitActiveTime");
                if (egVar2.equals(a2)) {
                    HashMap hashMap3 = new HashMap(4);
                    hashMap3.put("id", new eg.a("id", "INTEGER", true, 1));
                    hashMap3.put("value", new eg.a("value", "REAL", true, 0));
                    hashMap3.put(SampleRaw.COLUMN_START_TIME, new eg.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0));
                    hashMap3.put(SampleRaw.COLUMN_END_TIME, new eg.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0));
                    eg egVar3 = new eg("gFitHeartRate", hashMap3, new HashSet(0), new HashSet(0));
                    eg a3 = eg.a(ggVar2, "gFitHeartRate");
                    if (egVar3.equals(a3)) {
                        HashMap hashMap4 = new HashMap(8);
                        hashMap4.put("id", new eg.a("id", "INTEGER", true, 1));
                        hashMap4.put(SampleRaw.COLUMN_START_TIME, new eg.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0));
                        hashMap4.put(SampleRaw.COLUMN_END_TIME, new eg.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0));
                        hashMap4.put("workoutType", new eg.a("workoutType", "INTEGER", true, 0));
                        hashMap4.put("steps", new eg.a("steps", "TEXT", true, 0));
                        hashMap4.put("calories", new eg.a("calories", "TEXT", true, 0));
                        hashMap4.put("distances", new eg.a("distances", "TEXT", true, 0));
                        hashMap4.put("heartRates", new eg.a("heartRates", "TEXT", true, 0));
                        eg egVar4 = new eg("gFitWorkoutSession", hashMap4, new HashSet(0), new HashSet(0));
                        eg a4 = eg.a(ggVar2, "gFitWorkoutSession");
                        if (egVar4.equals(a4)) {
                            HashMap hashMap5 = new HashMap(5);
                            hashMap5.put("id", new eg.a("id", "INTEGER", true, 1));
                            hashMap5.put("step", new eg.a("step", "INTEGER", true, 0));
                            hashMap5.put("distance", new eg.a("distance", "REAL", true, 0));
                            hashMap5.put("calorie", new eg.a("calorie", "REAL", true, 0));
                            hashMap5.put(LogBuilder.KEY_TIME, new eg.a(LogBuilder.KEY_TIME, "INTEGER", true, 0));
                            eg egVar5 = new eg("uaSample", hashMap5, new HashSet(0), new HashSet(0));
                            eg a5 = eg.a(ggVar2, "uaSample");
                            if (egVar5.equals(a5)) {
                                HashMap hashMap6 = new HashMap(4);
                                hashMap6.put("id", new eg.a("id", "INTEGER", true, 1));
                                hashMap6.put("sleepMins", new eg.a("sleepMins", "INTEGER", true, 0));
                                hashMap6.put(SampleRaw.COLUMN_START_TIME, new eg.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0));
                                hashMap6.put(SampleRaw.COLUMN_END_TIME, new eg.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0));
                                eg egVar6 = new eg("gFitSleep", hashMap6, new HashSet(0), new HashSet(0));
                                eg a6 = eg.a(ggVar2, "gFitSleep");
                                if (!egVar6.equals(a6)) {
                                    throw new IllegalStateException("Migration didn't properly handle gFitSleep(com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep).\n Expected:\n" + egVar6 + "\n Found:\n" + a6);
                                }
                                return;
                            }
                            throw new IllegalStateException("Migration didn't properly handle uaSample(com.portfolio.platform.data.model.thirdparty.ua.UASample).\n Expected:\n" + egVar5 + "\n Found:\n" + a5);
                        }
                        throw new IllegalStateException("Migration didn't properly handle gFitWorkoutSession(com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession).\n Expected:\n" + egVar4 + "\n Found:\n" + a4);
                    }
                    throw new IllegalStateException("Migration didn't properly handle gFitHeartRate(com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate).\n Expected:\n" + egVar3 + "\n Found:\n" + a3);
                }
                throw new IllegalStateException("Migration didn't properly handle gFitActiveTime(com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime).\n Expected:\n" + egVar2 + "\n Found:\n" + a2);
            }
            throw new IllegalStateException("Migration didn't properly handle gFitSample(com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample).\n Expected:\n" + egVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `gFitSample`");
            a.b("DELETE FROM `gFitActiveTime`");
            a.b("DELETE FROM `gFitHeartRate`");
            a.b("DELETE FROM `gFitWorkoutSession`");
            a.b("DELETE FROM `uaSample`");
            a.b("DELETE FROM `gFitSleep`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), "gFitSample", "gFitActiveTime", "gFitHeartRate", "gFitWorkoutSession", "uaSample", "gFitSleep");
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(2), "4a002011cd11b3072c8efe0a76ce12f3", "fd54d50c044f828885f0e40cfeeed303");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }

    @DexIgnore
    public GFitActiveTimeDao getGFitActiveTimeDao() {
        GFitActiveTimeDao gFitActiveTimeDao;
        if (this._gFitActiveTimeDao != null) {
            return this._gFitActiveTimeDao;
        }
        synchronized (this) {
            if (this._gFitActiveTimeDao == null) {
                this._gFitActiveTimeDao = new GFitActiveTimeDao_Impl(this);
            }
            gFitActiveTimeDao = this._gFitActiveTimeDao;
        }
        return gFitActiveTimeDao;
    }

    @DexIgnore
    public GFitHeartRateDao getGFitHeartRateDao() {
        GFitHeartRateDao gFitHeartRateDao;
        if (this._gFitHeartRateDao != null) {
            return this._gFitHeartRateDao;
        }
        synchronized (this) {
            if (this._gFitHeartRateDao == null) {
                this._gFitHeartRateDao = new GFitHeartRateDao_Impl(this);
            }
            gFitHeartRateDao = this._gFitHeartRateDao;
        }
        return gFitHeartRateDao;
    }

    @DexIgnore
    public GFitSampleDao getGFitSampleDao() {
        GFitSampleDao gFitSampleDao;
        if (this._gFitSampleDao != null) {
            return this._gFitSampleDao;
        }
        synchronized (this) {
            if (this._gFitSampleDao == null) {
                this._gFitSampleDao = new GFitSampleDao_Impl(this);
            }
            gFitSampleDao = this._gFitSampleDao;
        }
        return gFitSampleDao;
    }

    @DexIgnore
    public GFitSleepDao getGFitSleepDao() {
        GFitSleepDao gFitSleepDao;
        if (this._gFitSleepDao != null) {
            return this._gFitSleepDao;
        }
        synchronized (this) {
            if (this._gFitSleepDao == null) {
                this._gFitSleepDao = new GFitSleepDao_Impl(this);
            }
            gFitSleepDao = this._gFitSleepDao;
        }
        return gFitSleepDao;
    }

    @DexIgnore
    public GFitWorkoutSessionDao getGFitWorkoutSessionDao() {
        GFitWorkoutSessionDao gFitWorkoutSessionDao;
        if (this._gFitWorkoutSessionDao != null) {
            return this._gFitWorkoutSessionDao;
        }
        synchronized (this) {
            if (this._gFitWorkoutSessionDao == null) {
                this._gFitWorkoutSessionDao = new GFitWorkoutSessionDao_Impl(this);
            }
            gFitWorkoutSessionDao = this._gFitWorkoutSessionDao;
        }
        return gFitWorkoutSessionDao;
    }

    @DexIgnore
    public UASampleDao getUASampleDao() {
        UASampleDao uASampleDao;
        if (this._uASampleDao != null) {
            return this._uASampleDao;
        }
        synchronized (this) {
            if (this._uASampleDao == null) {
                this._uASampleDao = new UASampleDao_Impl(this);
            }
            uASampleDao = this._uASampleDao;
        }
        return uASampleDao;
    }
}
