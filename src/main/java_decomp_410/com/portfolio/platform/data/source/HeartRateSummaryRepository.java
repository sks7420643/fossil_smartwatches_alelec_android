package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nd;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConvertDateTimeToLong;
import com.portfolio.platform.helper.GsonConverterShortDate;
import com.portfolio.platform.helper.PagingRequestHelper;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSummaryRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public /* final */ HeartRateDailySummaryDao mHeartRateSummaryDao;
    @DexIgnore
    public List<HeartRateSummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return HeartRateSummaryRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = HeartRateSummaryRepository.class.getSimpleName();
        kd4.a((Object) simpleName, "HeartRateSummaryRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public HeartRateSummaryRepository(HeartRateDailySummaryDao heartRateDailySummaryDao, FitnessDataDao fitnessDataDao, ApiServiceV2 apiServiceV2) {
        kd4.b(heartRateDailySummaryDao, "mHeartRateSummaryDao");
        kd4.b(fitnessDataDao, "mFitnessDataDao");
        kd4.b(apiServiceV2, "mApiService");
        this.mHeartRateSummaryDao = heartRateDailySummaryDao;
        this.mFitnessDataDao = fitnessDataDao;
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        removePagingListener();
        this.mHeartRateSummaryDao.deleteAllHeartRateSummaries();
    }

    @DexIgnore
    public final LiveData<os3<List<DailyHeartRateSummary>>> getHeartRateSummaries(Date date, Date date2, boolean z) {
        kd4.b(date, GoalPhase.COLUMN_START_DATE);
        kd4.b(date2, GoalPhase.COLUMN_END_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getHeartRateSummaries: startDate = " + date + ", endDate = " + date2);
        LiveData<os3<List<DailyHeartRateSummary>>> b = hc.b(this.mFitnessDataDao.getFitnessDataLiveData(date, date2), new HeartRateSummaryRepository$getHeartRateSummaries$Anon1(this, date, date2, z));
        kd4.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final Listing<DailyHeartRateSummary> getSummariesPaging(HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, HeartRateDailySummaryDao heartRateDailySummaryDao, FitnessDatabase fitnessDatabase, Date date, h42 h42, PagingRequestHelper.a aVar) {
        kd4.b(heartRateSummaryRepository, "summariesRepository");
        kd4.b(fitnessDataRepository, "fitnessDataRepository");
        kd4.b(heartRateDailySummaryDao, "heartRateDailySummaryDao");
        kd4.b(fitnessDatabase, "heartRateDatabase");
        kd4.b(date, "createdDate");
        h42 h422 = h42;
        kd4.b(h422, "appExecutors");
        PagingRequestHelper.a aVar2 = aVar;
        kd4.b(aVar2, "listener");
        HeartRateSummaryLocalDataSource.Companion companion = HeartRateSummaryLocalDataSource.Companion;
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        Date time = instance.getTime();
        kd4.a((Object) time, "Calendar.getInstance().time");
        Date calculateNextKey = companion.calculateNextKey(time, date);
        Calendar instance2 = Calendar.getInstance();
        kd4.a((Object) instance2, "calendar");
        instance2.setTime(calculateNextKey);
        HeartRateSummaryDataSourceFactory heartRateSummaryDataSourceFactory = new HeartRateSummaryDataSourceFactory(heartRateSummaryRepository, fitnessDataRepository, heartRateDailySummaryDao, fitnessDatabase, date, h422, aVar2, instance2);
        this.mSourceFactoryList.add(heartRateSummaryDataSourceFactory);
        qd.f.a aVar3 = new qd.f.a();
        aVar3.a(30);
        aVar3.a(false);
        aVar3.b(30);
        aVar3.c(5);
        qd.f a = aVar3.a();
        kd4.a((Object) a, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a2 = new nd(heartRateSummaryDataSourceFactory, a).a();
        kd4.a((Object) a2, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData<Y> b = hc.b(heartRateSummaryDataSourceFactory.getSourceLiveData(), HeartRateSummaryRepository$getSummariesPaging$Anon1.INSTANCE);
        kd4.a((Object) b, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing<>(a2, b, new HeartRateSummaryRepository$getSummariesPaging$Anon2(heartRateSummaryDataSourceFactory), new HeartRateSummaryRepository$getSummariesPaging$Anon3(heartRateSummaryDataSourceFactory));
    }

    @DexIgnore
    public final void insertFromDevice(List<DailyHeartRateSummary> list) {
        kd4.b(list, "heartRateSummaries");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insertFromDevice: heartRateSummaries = " + list);
        this.mHeartRateSummaryDao.insertHeartRateSummaries(list);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v8, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v10, resolved type: java.util.List} */
    /* JADX WARNING: type inference failed for: r4v0 */
    /* JADX WARNING: type inference failed for: r4v5 */
    /* JADX WARNING: type inference failed for: r4v6, types: [java.util.List, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v9 */
    /* JADX WARNING: Failed to insert additional move for type inference */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object loadSummaries(Date date, Date date2, yb4<? super qo2<xz1>> yb4) {
        HeartRateSummaryRepository$loadSummaries$Anon1 heartRateSummaryRepository$loadSummaries$Anon1;
        int i;
        HeartRateSummaryRepository heartRateSummaryRepository;
        qo2 qo2;
        if (yb4 instanceof HeartRateSummaryRepository$loadSummaries$Anon1) {
            heartRateSummaryRepository$loadSummaries$Anon1 = (HeartRateSummaryRepository$loadSummaries$Anon1) yb4;
            int i2 = heartRateSummaryRepository$loadSummaries$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                heartRateSummaryRepository$loadSummaries$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = heartRateSummaryRepository$loadSummaries$Anon1.result;
                Object a = cc4.a();
                i = heartRateSummaryRepository$loadSummaries$Anon1.label;
                List list = 0;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "loadSummaries startDate=" + date + ", endDate=" + date2);
                    HeartRateSummaryRepository$loadSummaries$response$Anon1 heartRateSummaryRepository$loadSummaries$response$Anon1 = new HeartRateSummaryRepository$loadSummaries$response$Anon1(this, date, date2, (yb4) null);
                    heartRateSummaryRepository$loadSummaries$Anon1.L$Anon0 = this;
                    heartRateSummaryRepository$loadSummaries$Anon1.L$Anon1 = date;
                    heartRateSummaryRepository$loadSummaries$Anon1.L$Anon2 = date2;
                    heartRateSummaryRepository$loadSummaries$Anon1.label = 1;
                    obj = ResponseKt.a(heartRateSummaryRepository$loadSummaries$response$Anon1, heartRateSummaryRepository$loadSummaries$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    heartRateSummaryRepository = this;
                } else if (i == 1) {
                    Date date3 = (Date) heartRateSummaryRepository$loadSummaries$Anon1.L$Anon2;
                    Date date4 = (Date) heartRateSummaryRepository$loadSummaries$Anon1.L$Anon1;
                    heartRateSummaryRepository = (HeartRateSummaryRepository) heartRateSummaryRepository$loadSummaries$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    if (ro2.a() != null && !ro2.b()) {
                        try {
                            rz1 rz1 = new rz1();
                            rz1.a(Long.TYPE, new GsonConvertDateTimeToLong());
                            rz1.a(DateTime.class, new GsonConvertDateTime());
                            rz1.a(Date.class, new GsonConverterShortDate());
                            ApiResponse apiResponse = (ApiResponse) rz1.a().a(((xz1) ((ro2) qo2).a()).toString(), new HeartRateSummaryRepository$loadSummaries$summaries$Anon1().getType());
                            if (apiResponse != null) {
                                list = apiResponse.get_items();
                            }
                            if (list != 0) {
                                FLogger.INSTANCE.getLocal().d(TAG, String.valueOf(list));
                                heartRateSummaryRepository.mHeartRateSummaryDao.insertListDailyHeartRateSummary(list);
                            }
                        } catch (Exception e) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str2 = TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("loadSummaries exception=");
                            e.printStackTrace();
                            sb.append(qa4.a);
                            local2.e(str2, sb.toString());
                        }
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("loadSummaries Failure code=");
                    po2 po2 = (po2) qo2;
                    sb2.append(po2.a());
                    sb2.append(" message=");
                    ServerError c = po2.c();
                    if (c != null) {
                        String message = c.getMessage();
                        if (message != null) {
                            list = message;
                            if (list == 0) {
                                list = "";
                            }
                            sb2.append(list);
                            local3.d(str3, sb2.toString());
                        }
                    }
                    ServerError c2 = po2.c();
                    if (c2 != null) {
                        list = c2.getUserMessage();
                    }
                    if (list == 0) {
                    }
                    sb2.append(list);
                    local3.d(str3, sb2.toString());
                }
                return qo2;
            }
        }
        heartRateSummaryRepository$loadSummaries$Anon1 = new HeartRateSummaryRepository$loadSummaries$Anon1(this, yb4);
        Object obj2 = heartRateSummaryRepository$loadSummaries$Anon1.result;
        Object a2 = cc4.a();
        i = heartRateSummaryRepository$loadSummaries$Anon1.label;
        List list2 = 0;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
        return qo2;
    }

    @DexIgnore
    public final void removePagingListener() {
        for (HeartRateSummaryDataSourceFactory localDataSource : this.mSourceFactoryList) {
            HeartRateSummaryLocalDataSource localDataSource2 = localDataSource.getLocalDataSource();
            if (localDataSource2 != null) {
                localDataSource2.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }
}
