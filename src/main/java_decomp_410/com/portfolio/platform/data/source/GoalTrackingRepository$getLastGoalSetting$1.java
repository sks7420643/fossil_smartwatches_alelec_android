package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository$getLastGoalSetting$1 extends com.portfolio.platform.util.NetworkBoundResource<java.lang.Integer, com.portfolio.platform.data.model.GoalSetting> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.GoalTrackingRepository this$0;

    @DexIgnore
    public GoalTrackingRepository$getLastGoalSetting$1(com.portfolio.platform.data.source.GoalTrackingRepository goalTrackingRepository) {
        this.this$0 = goalTrackingRepository;
    }

    @DexIgnore
    public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.model.GoalSetting>> yb4) {
        return this.this$0.mApiServiceV2.getGoalSetting(yb4);
    }

    @DexIgnore
    public androidx.lifecycle.LiveData<java.lang.Integer> loadFromDb() {
        return this.this$0.mGoalTrackingDao.getLastGoalSettingLiveData();
    }

    @DexIgnore
    public void onFetchFailed(java.lang.Throwable th) {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.GoalTrackingRepository.Companion.getTAG(), "getLastGoalSetting onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(java.lang.Integer num) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(com.portfolio.platform.data.model.GoalSetting goalSetting) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(goalSetting, "item");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String tag = com.portfolio.platform.data.source.GoalTrackingRepository.Companion.getTAG();
        local.mo33255d(tag, "getLastGoalSetting saveCallResult goal: " + goalSetting);
        this.this$0.saveSettingToDB(goalSetting);
    }
}
