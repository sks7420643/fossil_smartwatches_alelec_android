package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.em4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2", f = "WatchFaceRemoteDataSource.kt", l = {33}, m = "invokeSuspend")
public final class WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2 extends SuspendLambda implements xc4<yb4<? super qr4<em4>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $url;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WatchFaceRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2(WatchFaceRemoteDataSource watchFaceRemoteDataSource, String str, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = watchFaceRemoteDataSource;
        this.$url = str;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2(this.this$Anon0, this.$url, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            ApiServiceV2 access$getApi$p = this.this$Anon0.api;
            String str = this.$url;
            this.label = 1;
            obj = access$getApi$p.downloadFile(str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
