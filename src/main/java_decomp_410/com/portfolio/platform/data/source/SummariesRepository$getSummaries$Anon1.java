package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.FitnessDayData;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDate;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getSummaries$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends NetworkBoundResource<List<ActivitySummary>, xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ Pair $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon1 this$Anon0;

        @DexIgnore
        public Anon1(SummariesRepository$getSummaries$Anon1 summariesRepository$getSummaries$Anon1, Pair pair) {
            this.this$Anon0 = summariesRepository$getSummaries$Anon1;
            this.$downloadingDate = pair;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
            if (r0 != null) goto L_0x0019;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
            if (r0 != null) goto L_0x0033;
         */
        @DexIgnore
        public Object createCall(yb4<? super qr4<xz1>> yb4) {
            Date date;
            Date date2;
            ApiServiceV2 access$getMApiServiceV2$p = this.this$Anon0.this$Anon0.mApiServiceV2;
            Pair pair = this.$downloadingDate;
            if (pair != null) {
                date = (Date) pair.getFirst();
            }
            date = this.this$Anon0.$startDate;
            String e = rk2.e(date);
            kd4.a((Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            Pair pair2 = this.$downloadingDate;
            if (pair2 != null) {
                date2 = (Date) pair2.getSecond();
            }
            date2 = this.this$Anon0.$endDate;
            String e2 = rk2.e(date2);
            kd4.a((Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiServiceV2$p.getSummaries(e, e2, 0, 100, yb4);
        }

        @DexIgnore
        public LiveData<List<ActivitySummary>> loadFromDb() {
            if (!rk2.s(this.this$Anon0.$endDate).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d(SummariesRepository.TAG, "getSummaries - loadFromDb -- isNotToday - startDate=" + this.this$Anon0.$startDate + ", endDate=" + this.this$Anon0.$endDate);
                ActivitySummaryDao access$getMActivitySummaryDao$p = this.this$Anon0.this$Anon0.mActivitySummaryDao;
                SummariesRepository$getSummaries$Anon1 summariesRepository$getSummaries$Anon1 = this.this$Anon0;
                return access$getMActivitySummaryDao$p.getActivitySummariesLiveData(summariesRepository$getSummaries$Anon1.$startDate, summariesRepository$getSummaries$Anon1.$endDate);
            }
            FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getSummaries - loadFromDb -- isToday");
            ActivitySummaryDao access$getMActivitySummaryDao$p2 = this.this$Anon0.this$Anon0.mActivitySummaryDao;
            SummariesRepository$getSummaries$Anon1 summariesRepository$getSummaries$Anon12 = this.this$Anon0;
            LiveData<List<ActivitySummary>> activitySummariesLiveData = access$getMActivitySummaryDao$p2.getActivitySummariesLiveData(summariesRepository$getSummaries$Anon12.$startDate, summariesRepository$getSummaries$Anon12.$endDate);
            fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1(this, (yb4) null), 3, (Object) null);
            LiveData<List<ActivitySummary>> a = hc.a(activitySummariesLiveData, new SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon2(this, activitySummariesLiveData));
            kd4.a((Object) a, "Transformations.map(resu\u2026                        }");
            return a;
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getSummaries - onFetchFailed");
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0098 A[Catch:{ Exception -> 0x00f3 }] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00cd A[Catch:{ Exception -> 0x00f3 }] */
        /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
        public void saveCallResult(xz1 xz1) {
            Date date;
            Pair pair;
            Date date2;
            List<FitnessDataWrapper> fitnessData;
            kd4.b(xz1, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "getSummaries - saveCallResult -- item=" + xz1);
            try {
                ArrayList arrayList = new ArrayList();
                rz1 rz1 = new rz1();
                rz1.a(Date.class, new GsonConvertDate());
                rz1.a(DateTime.class, new GsonConvertDateTime());
                ApiResponse apiResponse = (ApiResponse) rz1.a().a(xz1.toString(), new SummariesRepository$getSummaries$Anon1$Anon1$saveCallResult$Anon1().getType());
                if (apiResponse != null) {
                    List<FitnessDayData> list = apiResponse.get_items();
                    if (list != null) {
                        for (FitnessDayData activitySummary : list) {
                            ActivitySummary activitySummary2 = activitySummary.toActivitySummary();
                            kd4.a((Object) activitySummary2, "it.toActivitySummary()");
                            arrayList.add(activitySummary2);
                        }
                    }
                }
                FitnessDataDao access$getMFitnessDataDao$p = this.this$Anon0.this$Anon0.mFitnessDataDao;
                Pair pair2 = this.$downloadingDate;
                if (pair2 != null) {
                    date = (Date) pair2.getFirst();
                    if (date != null) {
                        pair = this.$downloadingDate;
                        if (pair != null) {
                            date2 = (Date) pair.getSecond();
                            if (date2 != null) {
                                fitnessData = access$getMFitnessDataDao$p.getFitnessData(date, date2);
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                local2.d(SummariesRepository.TAG, "fitnessDatasSize " + fitnessData.size());
                                if (fitnessData.isEmpty()) {
                                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                    local3.d(SummariesRepository.TAG, "upsert 1 list " + arrayList);
                                    this.this$Anon0.this$Anon0.mActivitySummaryDao.upsertActivitySummaries(arrayList);
                                    return;
                                }
                                return;
                            }
                        }
                        date2 = this.this$Anon0.$endDate;
                        fitnessData = access$getMFitnessDataDao$p.getFitnessData(date, date2);
                        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                        local22.d(SummariesRepository.TAG, "fitnessDatasSize " + fitnessData.size());
                        if (fitnessData.isEmpty()) {
                        }
                    }
                }
                date = this.this$Anon0.$startDate;
                pair = this.$downloadingDate;
                if (pair != null) {
                }
                date2 = this.this$Anon0.$endDate;
                fitnessData = access$getMFitnessDataDao$p.getFitnessData(date, date2);
                ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
                local222.d(SummariesRepository.TAG, "fitnessDatasSize " + fitnessData.size());
                if (fitnessData.isEmpty()) {
                }
            } catch (Exception e) {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getSummaries - saveCallResult -- e=");
                e.printStackTrace();
                sb.append(qa4.a);
                local4.e(SummariesRepository.TAG, sb.toString());
            }
        }

        @DexIgnore
        public boolean shouldFetch(List<ActivitySummary> list) {
            return this.this$Anon0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public SummariesRepository$getSummaries$Anon1(SummariesRepository summariesRepository, Date date, Date date2, boolean z) {
        this.this$Anon0 = summariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public final LiveData<os3<List<ActivitySummary>>> apply(List<FitnessDataWrapper> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getSummaries - startDate=" + this.$startDate + ", endDate=" + this.$endDate + " fitnessDataList=" + list.size());
        kd4.a((Object) list, "fitnessDataList");
        return new Anon1(this, FitnessDataWrapperKt.calculateRangeDownload(list, this.$startDate, this.$endDate)).asLiveData();
    }
}
