package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.Device;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface DeviceDao {
    @DexIgnore
    void addAllDevice(List<Device> list);

    @DexIgnore
    void addOrUpdateDevice(Device device);

    @DexIgnore
    void cleanUp();

    @DexIgnore
    List<Device> getAllDevice() throws Throwable;

    @DexIgnore
    LiveData<List<Device>> getAllDeviceAsLiveData();

    @DexIgnore
    Device getDeviceByDeviceId(String str) throws Throwable;

    @DexIgnore
    LiveData<Device> getDeviceBySerialAsLiveData(String str);

    @DexIgnore
    void removeDeviceByDeviceId(String str);
}
