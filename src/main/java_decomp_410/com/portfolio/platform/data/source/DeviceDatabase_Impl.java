package com.portfolio.platform.data.source;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceDatabase_Impl extends DeviceDatabase {
    @DexIgnore
    public volatile DeviceDao _deviceDao;
    @DexIgnore
    public volatile SkuDao _skuDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `device` (`major` INTEGER NOT NULL, `minor` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT, `owner` TEXT, `productDisplayName` TEXT, `manufacturer` TEXT, `softwareRevision` TEXT, `hardwareRevision` TEXT, `deviceId` TEXT NOT NULL, `macAddress` TEXT, `sku` TEXT, `firmwareRevision` TEXT, `batteryLevel` INTEGER NOT NULL, `vibrationStrength` INTEGER, `isActive` INTEGER NOT NULL, PRIMARY KEY(`deviceId`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `SKU` (`createdAt` TEXT, `updatedAt` TEXT, `serialNumberPrefix` TEXT NOT NULL, `sku` TEXT, `deviceName` TEXT, `groupName` TEXT, `gender` TEXT, `deviceType` TEXT, PRIMARY KEY(`serialNumberPrefix`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `watchParam` (`prefixSerial` TEXT NOT NULL, `versionMajor` TEXT, `versionMinor` TEXT, `data` TEXT, PRIMARY KEY(`prefixSerial`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'ebb28bd48a859f6e14a64ae279368044')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `device`");
            ggVar.b("DROP TABLE IF EXISTS `SKU`");
            ggVar.b("DROP TABLE IF EXISTS `watchParam`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (DeviceDatabase_Impl.this.mCallbacks != null) {
                int size = DeviceDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) DeviceDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = DeviceDatabase_Impl.this.mDatabase = ggVar;
            DeviceDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (DeviceDatabase_Impl.this.mCallbacks != null) {
                int size = DeviceDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) DeviceDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            HashMap hashMap = new HashMap(16);
            hashMap.put("major", new eg.a("major", "INTEGER", true, 0));
            hashMap.put("minor", new eg.a("minor", "INTEGER", true, 0));
            hashMap.put("createdAt", new eg.a("createdAt", "TEXT", false, 0));
            hashMap.put("updatedAt", new eg.a("updatedAt", "TEXT", false, 0));
            hashMap.put("owner", new eg.a("owner", "TEXT", false, 0));
            hashMap.put("productDisplayName", new eg.a("productDisplayName", "TEXT", false, 0));
            hashMap.put("manufacturer", new eg.a("manufacturer", "TEXT", false, 0));
            hashMap.put("softwareRevision", new eg.a("softwareRevision", "TEXT", false, 0));
            hashMap.put("hardwareRevision", new eg.a("hardwareRevision", "TEXT", false, 0));
            hashMap.put("deviceId", new eg.a("deviceId", "TEXT", true, 1));
            hashMap.put("macAddress", new eg.a("macAddress", "TEXT", false, 0));
            hashMap.put(LegacyDeviceModel.COLUMN_DEVICE_MODEL, new eg.a(LegacyDeviceModel.COLUMN_DEVICE_MODEL, "TEXT", false, 0));
            hashMap.put(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION, new eg.a(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION, "TEXT", false, 0));
            hashMap.put(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, new eg.a(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, "INTEGER", true, 0));
            hashMap.put("vibrationStrength", new eg.a("vibrationStrength", "INTEGER", false, 0));
            hashMap.put("isActive", new eg.a("isActive", "INTEGER", true, 0));
            eg egVar = new eg("device", hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar, "device");
            if (egVar.equals(a)) {
                HashMap hashMap2 = new HashMap(8);
                hashMap2.put("createdAt", new eg.a("createdAt", "TEXT", false, 0));
                hashMap2.put("updatedAt", new eg.a("updatedAt", "TEXT", false, 0));
                hashMap2.put("serialNumberPrefix", new eg.a("serialNumberPrefix", "TEXT", true, 1));
                hashMap2.put(LegacyDeviceModel.COLUMN_DEVICE_MODEL, new eg.a(LegacyDeviceModel.COLUMN_DEVICE_MODEL, "TEXT", false, 0));
                hashMap2.put("deviceName", new eg.a("deviceName", "TEXT", false, 0));
                hashMap2.put("groupName", new eg.a("groupName", "TEXT", false, 0));
                hashMap2.put("gender", new eg.a("gender", "TEXT", false, 0));
                hashMap2.put("deviceType", new eg.a("deviceType", "TEXT", false, 0));
                eg egVar2 = new eg("SKU", hashMap2, new HashSet(0), new HashSet(0));
                eg a2 = eg.a(ggVar, "SKU");
                if (egVar2.equals(a2)) {
                    HashMap hashMap3 = new HashMap(4);
                    hashMap3.put("prefixSerial", new eg.a("prefixSerial", "TEXT", true, 1));
                    hashMap3.put("versionMajor", new eg.a("versionMajor", "TEXT", false, 0));
                    hashMap3.put("versionMinor", new eg.a("versionMinor", "TEXT", false, 0));
                    hashMap3.put("data", new eg.a("data", "TEXT", false, 0));
                    eg egVar3 = new eg("watchParam", hashMap3, new HashSet(0), new HashSet(0));
                    eg a3 = eg.a(ggVar, "watchParam");
                    if (!egVar3.equals(a3)) {
                        throw new IllegalStateException("Migration didn't properly handle watchParam(com.portfolio.platform.data.model.WatchParam).\n Expected:\n" + egVar3 + "\n Found:\n" + a3);
                    }
                    return;
                }
                throw new IllegalStateException("Migration didn't properly handle SKU(com.portfolio.platform.data.model.SKUModel).\n Expected:\n" + egVar2 + "\n Found:\n" + a2);
            }
            throw new IllegalStateException("Migration didn't properly handle device(com.portfolio.platform.data.model.Device).\n Expected:\n" + egVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `device`");
            a.b("DELETE FROM `SKU`");
            a.b("DELETE FROM `watchParam`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), "device", "SKU", "watchParam");
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(2), "ebb28bd48a859f6e14a64ae279368044", "a98fb7f948bc0742615d4020fcfa6e4e");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }

    @DexIgnore
    public DeviceDao deviceDao() {
        DeviceDao deviceDao;
        if (this._deviceDao != null) {
            return this._deviceDao;
        }
        synchronized (this) {
            if (this._deviceDao == null) {
                this._deviceDao = new DeviceDao_Impl(this);
            }
            deviceDao = this._deviceDao;
        }
        return deviceDao;
    }

    @DexIgnore
    public SkuDao skuDao() {
        SkuDao skuDao;
        if (this._skuDao != null) {
            return this._skuDao;
        }
        synchronized (this) {
            if (this._skuDao == null) {
                this._skuDao = new SkuDao_Impl(this);
            }
            skuDao = this._skuDao;
        }
        return skuDao;
    }
}
