package com.portfolio.platform.data.source.local.inapp;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class InAppNotificationDatabase_Impl extends InAppNotificationDatabase {
    @DexIgnore
    public volatile InAppNotificationDao _inAppNotificationDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `inAppNotification` (`id` TEXT NOT NULL, `title` TEXT NOT NULL, `content` TEXT NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'b9f81264aeec3206295ee3fe096e0c84')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `inAppNotification`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (InAppNotificationDatabase_Impl.this.mCallbacks != null) {
                int size = InAppNotificationDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) InAppNotificationDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = InAppNotificationDatabase_Impl.this.mDatabase = ggVar;
            InAppNotificationDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (InAppNotificationDatabase_Impl.this.mCallbacks != null) {
                int size = InAppNotificationDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) InAppNotificationDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("id", new eg.a("id", "TEXT", true, 1));
            hashMap.put("title", new eg.a("title", "TEXT", true, 0));
            hashMap.put("content", new eg.a("content", "TEXT", true, 0));
            eg egVar = new eg("inAppNotification", hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar, "inAppNotification");
            if (!egVar.equals(a)) {
                throw new IllegalStateException("Migration didn't properly handle inAppNotification(com.portfolio.platform.data.InAppNotification).\n Expected:\n" + egVar + "\n Found:\n" + a);
            }
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `inAppNotification`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), "inAppNotification");
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(1), "b9f81264aeec3206295ee3fe096e0c84", "9dd4de0568e77b393b85859b42ea3ad5");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }

    @DexIgnore
    public InAppNotificationDao inAppNotificationDao() {
        InAppNotificationDao inAppNotificationDao;
        if (this._inAppNotificationDao != null) {
            return this._inAppNotificationDao;
        }
        synchronized (this) {
            if (this._inAppNotificationDao == null) {
                this._inAppNotificationDao = new InAppNotificationDao_Impl(this);
            }
            inAppNotificationDao = this._inAppNotificationDao;
        }
        return inAppNotificationDao;
    }
}
