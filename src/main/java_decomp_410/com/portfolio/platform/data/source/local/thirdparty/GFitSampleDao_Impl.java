package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GFitSampleDao_Impl implements GFitSampleDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ kf __deletionAdapterOfGFitSample;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfGFitSample;
    @DexIgnore
    public /* final */ wf __preparedStmtOfClearAll;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<GFitSample> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitSample`(`id`,`step`,`distance`,`calorie`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, GFitSample gFitSample) {
            kgVar.b(1, (long) gFitSample.getId());
            kgVar.b(2, (long) gFitSample.getStep());
            kgVar.a(3, (double) gFitSample.getDistance());
            kgVar.a(4, (double) gFitSample.getCalorie());
            kgVar.b(5, gFitSample.getStartTime());
            kgVar.b(6, gFitSample.getEndTime());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends kf<GFitSample> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `gFitSample` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(kg kgVar, GFitSample gFitSample) {
            kgVar.b(1, (long) gFitSample.getId());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends wf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM gFitSample";
        }
    }

    @DexIgnore
    public GFitSampleDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfGFitSample = new Anon1(roomDatabase);
        this.__deletionAdapterOfGFitSample = new Anon2(roomDatabase);
        this.__preparedStmtOfClearAll = new Anon3(roomDatabase);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public void deleteListGFitSample(List<GFitSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitSample.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<GFitSample> getAllGFitSample() {
        uf b = uf.b("SELECT * FROM gFitSample", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "step");
            int b4 = ag.b(a, "distance");
            int b5 = ag.b(a, "calorie");
            int b6 = ag.b(a, SampleRaw.COLUMN_START_TIME);
            int b7 = ag.b(a, SampleRaw.COLUMN_END_TIME);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GFitSample gFitSample = new GFitSample(a.getInt(b3), a.getFloat(b4), a.getFloat(b5), a.getLong(b6), a.getLong(b7));
                gFitSample.setId(a.getInt(b2));
                arrayList.add(gFitSample);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertGFitSample(GFitSample gFitSample) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSample.insert(gFitSample);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListGFitSample(List<GFitSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
