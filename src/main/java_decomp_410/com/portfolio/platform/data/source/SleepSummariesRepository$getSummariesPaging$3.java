package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository$getSummariesPaging$3 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.sleep.SleepSummaryDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSummariesPaging$3(com.portfolio.platform.data.source.local.sleep.SleepSummaryDataSourceFactory sleepSummaryDataSourceFactory) {
        super(0);
        this.$sourceFactory = sleepSummaryDataSourceFactory;
    }

    @DexIgnore
    public final void invoke() {
        com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource a = this.$sourceFactory.getSourceLiveData().mo2275a();
        if (a != null) {
            com.portfolio.platform.helper.PagingRequestHelper mHelper = a.getMHelper();
            if (mHelper != null) {
                mHelper.mo39586b();
            }
        }
    }
}
