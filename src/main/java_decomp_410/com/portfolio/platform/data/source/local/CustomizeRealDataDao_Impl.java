package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.portfolio.platform.data.model.CustomizeRealData;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CustomizeRealDataDao_Impl implements CustomizeRealDataDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfCustomizeRealData;
    @DexIgnore
    public /* final */ wf __preparedStmtOfCleanUp;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<CustomizeRealData> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `customizeRealData`(`id`,`value`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, CustomizeRealData customizeRealData) {
            if (customizeRealData.getId() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, customizeRealData.getId());
            }
            if (customizeRealData.getValue() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, customizeRealData.getValue());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends wf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM customizeRealData";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<CustomizeRealData>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon3(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<CustomizeRealData> call() throws Exception {
            Cursor a = bg.a(CustomizeRealDataDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "id");
                int b2 = ag.b(a, "value");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new CustomizeRealData(a.getString(b), a.getString(b2)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public CustomizeRealDataDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfCustomizeRealData = new Anon1(roomDatabase);
        this.__preparedStmtOfCleanUp = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    public LiveData<List<CustomizeRealData>> getAllRealDataAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"customizeRealData"}, false, new Anon3(uf.b("SELECT * FROM customizeRealData", 0)));
    }

    @DexIgnore
    public List<CustomizeRealData> getAllRealDataRaw() {
        uf b = uf.b("SELECT * FROM customizeRealData", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "value");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new CustomizeRealData(a.getString(b2), a.getString(b3)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public CustomizeRealData getRealData(String str) {
        uf b = uf.b("SELECT * FROM customizeRealData WHERE id=?", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            return a.moveToFirst() ? new CustomizeRealData(a.getString(ag.b(a, "id")), a.getString(ag.b(a, "value"))) : null;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertRealData(CustomizeRealData customizeRealData) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfCustomizeRealData.insert(customizeRealData);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
