package com.portfolio.platform.data.source;

import com.google.common.reflect.TypeToken;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.source.remote.UpsertApiResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitiesRepository$saveActivitiesToServer$type$Anon1 extends TypeToken<UpsertApiResponse<Activity>> {
}
