package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon2", f = "DeviceRepository.kt", l = {158}, m = "invokeSuspend")
public final class DeviceRepository$downloadSupportedSku$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRepository$downloadSupportedSku$Anon2(DeviceRepository deviceRepository, int i, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = deviceRepository;
        this.$offset = i;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DeviceRepository$downloadSupportedSku$Anon2 deviceRepository$downloadSupportedSku$Anon2 = new DeviceRepository$downloadSupportedSku$Anon2(this.this$Anon0, this.$offset, yb4);
        deviceRepository$downloadSupportedSku$Anon2.p$ = (zg4) obj;
        return deviceRepository$downloadSupportedSku$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DeviceRepository$downloadSupportedSku$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            this.L$Anon0 = this.p$;
            this.label = 1;
            if (this.this$Anon0.downloadSupportedSku(this.$offset + 100, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
