package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ld;
import com.fossil.blesdk.obfuscated.xk2;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitySummaryDataSourceFactory extends ld.b<Date, ActivitySummary> {
    @DexIgnore
    public /* final */ ActivitySummaryDao activitySummaryDao;
    @DexIgnore
    public /* final */ h42 appExecutors;
    @DexIgnore
    public /* final */ Date createdDate;
    @DexIgnore
    public /* final */ FitnessDataRepository fitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase fitnessDatabase;
    @DexIgnore
    public /* final */ xk2 fitnessHelper;
    @DexIgnore
    public /* final */ PagingRequestHelper.a listener;
    @DexIgnore
    public ActivitySummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<ActivitySummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ SummariesRepository summariesRepository;

    @DexIgnore
    public ActivitySummaryDataSourceFactory(SummariesRepository summariesRepository2, xk2 xk2, FitnessDataRepository fitnessDataRepository2, ActivitySummaryDao activitySummaryDao2, FitnessDatabase fitnessDatabase2, Date date, h42 h42, PagingRequestHelper.a aVar, Calendar calendar) {
        kd4.b(summariesRepository2, "summariesRepository");
        kd4.b(xk2, "fitnessHelper");
        kd4.b(fitnessDataRepository2, "fitnessDataRepository");
        kd4.b(activitySummaryDao2, "activitySummaryDao");
        kd4.b(fitnessDatabase2, "fitnessDatabase");
        kd4.b(date, "createdDate");
        kd4.b(h42, "appExecutors");
        kd4.b(aVar, "listener");
        kd4.b(calendar, "mStartCalendar");
        this.summariesRepository = summariesRepository2;
        this.fitnessHelper = xk2;
        this.fitnessDataRepository = fitnessDataRepository2;
        this.activitySummaryDao = activitySummaryDao2;
        this.fitnessDatabase = fitnessDatabase2;
        this.createdDate = date;
        this.appExecutors = h42;
        this.listener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    public ld<Date, ActivitySummary> create() {
        this.localDataSource = new ActivitySummaryLocalDataSource(this.summariesRepository, this.fitnessHelper, this.fitnessDataRepository, this.activitySummaryDao, this.fitnessDatabase, this.createdDate, this.appExecutors, this.listener, this.mStartCalendar);
        this.sourceLiveData.a(this.localDataSource);
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource = this.localDataSource;
        if (activitySummaryLocalDataSource != null) {
            return activitySummaryLocalDataSource;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final ActivitySummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<ActivitySummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.localDataSource = activitySummaryLocalDataSource;
    }
}
