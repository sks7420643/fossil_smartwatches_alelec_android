package com.portfolio.platform.data.source;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yf4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.AlarmsLocalDataSource;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmsRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ AlarmsRemoteDataSource mAlarmRemoteDataSource;
    @DexIgnore
    public /* final */ AlarmsLocalDataSource mAlarmsLocalDataSource;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = AlarmsRepository.class.getSimpleName();
        kd4.a((Object) simpleName, "AlarmsRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public AlarmsRepository(AlarmsLocalDataSource alarmsLocalDataSource, AlarmsRemoteDataSource alarmsRemoteDataSource) {
        kd4.b(alarmsLocalDataSource, "mAlarmsLocalDataSource");
        kd4.b(alarmsRemoteDataSource, "mAlarmRemoteDataSource");
        this.mAlarmsLocalDataSource = alarmsLocalDataSource;
        this.mAlarmRemoteDataSource = alarmsRemoteDataSource;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mAlarmsLocalDataSource.cleanUp();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object deleteAlarm(Alarm alarm, yb4<? super qo2<Void>> yb4) {
        AlarmsRepository$deleteAlarm$Anon1 alarmsRepository$deleteAlarm$Anon1;
        int i;
        qo2 qo2;
        AlarmsRepository alarmsRepository;
        qo2 qo22;
        if (yb4 instanceof AlarmsRepository$deleteAlarm$Anon1) {
            alarmsRepository$deleteAlarm$Anon1 = (AlarmsRepository$deleteAlarm$Anon1) yb4;
            int i2 = alarmsRepository$deleteAlarm$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRepository$deleteAlarm$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRepository$deleteAlarm$Anon1.result;
                Object a = cc4.a();
                i = alarmsRepository$deleteAlarm$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "deleteAlarm - alarm= " + alarm.getTitle() + " - " + alarm.getId() + " - " + alarm.getPinType());
                    this.mAlarmsLocalDataSource.deleteAlarm(alarm);
                    if (alarm.getPinType() == 1 || TextUtils.isEmpty(alarm.getId())) {
                        return new ro2((Object) null, false, 2, (fd4) null);
                    }
                    AlarmsRemoteDataSource alarmsRemoteDataSource = this.mAlarmRemoteDataSource;
                    String id = alarm.getId();
                    if (id != null) {
                        alarmsRepository$deleteAlarm$Anon1.L$Anon0 = this;
                        alarmsRepository$deleteAlarm$Anon1.L$Anon1 = alarm;
                        alarmsRepository$deleteAlarm$Anon1.label = 1;
                        obj = alarmsRemoteDataSource.deleteAlarm(id, alarmsRepository$deleteAlarm$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        alarmsRepository = this;
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else if (i == 1) {
                    alarm = (Alarm) alarmsRepository$deleteAlarm$Anon1.L$Anon1;
                    alarmsRepository = (AlarmsRepository) alarmsRepository$deleteAlarm$Anon1.L$Anon0;
                    na4.a(obj);
                } else if (i == 2) {
                    qo2 = (qo2) alarmsRepository$deleteAlarm$Anon1.L$Anon2;
                    Alarm alarm2 = (Alarm) alarmsRepository$deleteAlarm$Anon1.L$Anon1;
                    AlarmsRepository alarmsRepository2 = (AlarmsRepository) alarmsRepository$deleteAlarm$Anon1.L$Anon0;
                    na4.a(obj);
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), po2.d(), (String) null, 8, (fd4) null);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo22 = (qo2) obj;
                if (!(qo22 instanceof ro2)) {
                    return new ro2((Object) null, false, 2, (fd4) null);
                }
                if (qo22 instanceof po2) {
                    ug4 b = nh4.b();
                    AlarmsRepository$deleteAlarm$Anon2 alarmsRepository$deleteAlarm$Anon2 = new AlarmsRepository$deleteAlarm$Anon2(alarmsRepository, alarm, (yb4) null);
                    alarmsRepository$deleteAlarm$Anon1.L$Anon0 = alarmsRepository;
                    alarmsRepository$deleteAlarm$Anon1.L$Anon1 = alarm;
                    alarmsRepository$deleteAlarm$Anon1.L$Anon2 = qo22;
                    alarmsRepository$deleteAlarm$Anon1.label = 2;
                    if (yf4.a(b, alarmsRepository$deleteAlarm$Anon2, alarmsRepository$deleteAlarm$Anon1) == a) {
                        return a;
                    }
                    qo2 = qo22;
                    po2 po22 = (po2) qo2;
                    return new po2(po22.a(), po22.c(), po22.d(), (String) null, 8, (fd4) null);
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        alarmsRepository$deleteAlarm$Anon1 = new AlarmsRepository$deleteAlarm$Anon1(this, yb4);
        Object obj2 = alarmsRepository$deleteAlarm$Anon1.result;
        Object a2 = cc4.a();
        i = alarmsRepository$deleteAlarm$Anon1.label;
        if (i != 0) {
        }
        qo22 = (qo2) obj2;
        if (!(qo22 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object downloadAlarms(yb4<? super qa4> yb4) {
        AlarmsRepository$downloadAlarms$Anon1 alarmsRepository$downloadAlarms$Anon1;
        int i;
        boolean z;
        AlarmsRepository alarmsRepository;
        qo2 qo2;
        boolean booleanValue;
        if (yb4 instanceof AlarmsRepository$downloadAlarms$Anon1) {
            alarmsRepository$downloadAlarms$Anon1 = (AlarmsRepository$downloadAlarms$Anon1) yb4;
            int i2 = alarmsRepository$downloadAlarms$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRepository$downloadAlarms$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRepository$downloadAlarms$Anon1.result;
                Object a = cc4.a();
                i = alarmsRepository$downloadAlarms$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "start download");
                    alarmsRepository$downloadAlarms$Anon1.L$Anon0 = this;
                    alarmsRepository$downloadAlarms$Anon1.label = 1;
                    obj = executePendingRequest(alarmsRepository$downloadAlarms$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    alarmsRepository = this;
                } else if (i == 1) {
                    alarmsRepository = (AlarmsRepository) alarmsRepository$downloadAlarms$Anon1.L$Anon0;
                    na4.a(obj);
                } else if (i == 2) {
                    boolean z2 = alarmsRepository$downloadAlarms$Anon1.Z$Anon0;
                    na4.a(obj);
                    z = z2;
                    alarmsRepository = (AlarmsRepository) alarmsRepository$downloadAlarms$Anon1.L$Anon0;
                    qo2 = (qo2) obj;
                    if (!(qo2 instanceof ro2)) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = TAG;
                        local.d(str, "downloadAlarms success isFromCache " + ((ro2) qo2).b());
                        ug4 b = nh4.b();
                        AlarmsRepository$downloadAlarms$Anon2 alarmsRepository$downloadAlarms$Anon2 = new AlarmsRepository$downloadAlarms$Anon2(alarmsRepository, qo2, (yb4) null);
                        alarmsRepository$downloadAlarms$Anon1.L$Anon0 = alarmsRepository;
                        alarmsRepository$downloadAlarms$Anon1.Z$Anon0 = z;
                        alarmsRepository$downloadAlarms$Anon1.L$Anon1 = qo2;
                        alarmsRepository$downloadAlarms$Anon1.label = 3;
                        obj = yf4.a(b, alarmsRepository$downloadAlarms$Anon2, alarmsRepository$downloadAlarms$Anon1);
                        return obj == a ? a : obj;
                    }
                    if (qo2 instanceof po2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        local2.d(str2, "downloadAlarms fail!! " + ((po2) qo2).a());
                    }
                    return qa4.a;
                } else if (i == 3) {
                    qo2 qo22 = (qo2) alarmsRepository$downloadAlarms$Anon1.L$Anon1;
                    boolean z3 = alarmsRepository$downloadAlarms$Anon1.Z$Anon0;
                    AlarmsRepository alarmsRepository2 = (AlarmsRepository) alarmsRepository$downloadAlarms$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                booleanValue = ((Boolean) obj).booleanValue();
                if (booleanValue) {
                    AlarmsRemoteDataSource alarmsRemoteDataSource = alarmsRepository.mAlarmRemoteDataSource;
                    alarmsRepository$downloadAlarms$Anon1.L$Anon0 = alarmsRepository;
                    alarmsRepository$downloadAlarms$Anon1.Z$Anon0 = booleanValue;
                    alarmsRepository$downloadAlarms$Anon1.label = 2;
                    Object alarms = alarmsRemoteDataSource.getAlarms(alarmsRepository$downloadAlarms$Anon1);
                    if (alarms == a) {
                        return a;
                    }
                    Object obj2 = alarms;
                    z = booleanValue;
                    obj = obj2;
                    qo2 = (qo2) obj;
                    if (!(qo2 instanceof ro2)) {
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(TAG, "downloadAlarms not execute due to pending data");
                    return qa4.a;
                }
            }
        }
        alarmsRepository$downloadAlarms$Anon1 = new AlarmsRepository$downloadAlarms$Anon1(this, yb4);
        Object obj3 = alarmsRepository$downloadAlarms$Anon1.result;
        Object a2 = cc4.a();
        i = alarmsRepository$downloadAlarms$Anon1.label;
        if (i != 0) {
        }
        booleanValue = ((Boolean) obj3).booleanValue();
        if (booleanValue) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01c5  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01ea A[SYNTHETIC, Splitter:B:66:0x01ea] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x021f A[LOOP:3: B:75:0x0219->B:77:0x021f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x026f  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x028a  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x02a9 A[SYNTHETIC, Splitter:B:92:0x02a9] */
    public final synchronized Object executePendingRequest(yb4<? super Boolean> yb4) {
        AlarmsRepository$executePendingRequest$Anon1 alarmsRepository$executePendingRequest$Anon1;
        int i;
        AlarmsRepository alarmsRepository;
        List<Alarm> list;
        List<Alarm> list2;
        List list3;
        qo2 qo2;
        AlarmsRepository alarmsRepository2;
        List<Alarm> list4;
        ArrayList arrayList;
        List<Alarm> list5;
        qo2 qo22;
        yb4<? super Boolean> yb42 = yb4;
        synchronized (this) {
            if (yb42 instanceof AlarmsRepository$executePendingRequest$Anon1) {
                alarmsRepository$executePendingRequest$Anon1 = (AlarmsRepository$executePendingRequest$Anon1) yb42;
                if ((alarmsRepository$executePendingRequest$Anon1.label & Integer.MIN_VALUE) != 0) {
                    alarmsRepository$executePendingRequest$Anon1.label -= Integer.MIN_VALUE;
                    Object obj = alarmsRepository$executePendingRequest$Anon1.result;
                    Object a = cc4.a();
                    i = alarmsRepository$executePendingRequest$Anon1.label;
                    if (i != 0) {
                        na4.a(obj);
                        List<Alarm> allPendingAlarm = this.mAlarmsLocalDataSource.getAllPendingAlarm();
                        FLogger.INSTANCE.getLocal().d(TAG, "executePendingRequest allPendingAlarm " + allPendingAlarm.size());
                        if (allPendingAlarm.isEmpty()) {
                            Boolean a2 = dc4.a(false);
                            return a2;
                        }
                        list5 = new ArrayList<>();
                        for (T next : allPendingAlarm) {
                            if (dc4.a(((Alarm) next).getPinType() != 3).booleanValue()) {
                                list5.add(next);
                            }
                        }
                        ArrayList arrayList2 = new ArrayList();
                        for (T next2 : allPendingAlarm) {
                            if (dc4.a(((Alarm) next2).getPinType() == 3).booleanValue()) {
                                arrayList2.add(next2);
                            }
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("executePendingRequest upsertAlarmPendingList ");
                        ArrayList arrayList3 = new ArrayList(db4.a(list5, 10));
                        for (Alarm alarm : list5) {
                            arrayList3.add(alarm.getUri() + " - " + alarm.getId() + " - " + alarm.getPinType() + " - " + alarm.getTitle());
                        }
                        sb.append(arrayList3);
                        local.d(str, sb.toString());
                        if (!list5.isEmpty()) {
                            AlarmsRemoteDataSource alarmsRemoteDataSource = this.mAlarmRemoteDataSource;
                            alarmsRepository$executePendingRequest$Anon1.L$Anon0 = this;
                            alarmsRepository$executePendingRequest$Anon1.L$Anon1 = allPendingAlarm;
                            alarmsRepository$executePendingRequest$Anon1.L$Anon2 = list5;
                            alarmsRepository$executePendingRequest$Anon1.L$Anon3 = arrayList2;
                            alarmsRepository$executePendingRequest$Anon1.label = 1;
                            Object upsertAlarms = alarmsRemoteDataSource.upsertAlarms(list5, alarmsRepository$executePendingRequest$Anon1);
                            if (upsertAlarms == a) {
                                return a;
                            }
                            alarmsRepository2 = this;
                            ArrayList arrayList4 = arrayList2;
                            list4 = allPendingAlarm;
                            obj = upsertAlarms;
                            arrayList = arrayList4;
                        } else {
                            list = allPendingAlarm;
                            alarmsRepository = this;
                            list2 = list5;
                            list3 = arrayList2;
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str2 = TAG;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("executePendingRequest deleteAlarmPendingList ");
                            ArrayList arrayList5 = new ArrayList(db4.a(list2, 10));
                            while (r13.hasNext()) {
                            }
                            sb2.append(arrayList5);
                            local2.d(str2, sb2.toString());
                            if (!list3.isEmpty()) {
                            }
                            Boolean a3 = dc4.a(false);
                            return a3;
                        }
                    } else if (i == 1) {
                        alarmsRepository2 = (AlarmsRepository) alarmsRepository$executePendingRequest$Anon1.L$Anon0;
                        na4.a(obj);
                        arrayList = (List) alarmsRepository$executePendingRequest$Anon1.L$Anon3;
                        list5 = (List) alarmsRepository$executePendingRequest$Anon1.L$Anon2;
                        list4 = (List) alarmsRepository$executePendingRequest$Anon1.L$Anon1;
                    } else if (i == 2) {
                        qo2 qo23 = (qo2) alarmsRepository$executePendingRequest$Anon1.L$Anon4;
                        list4 = (List) alarmsRepository$executePendingRequest$Anon1.L$Anon1;
                        na4.a(obj);
                        arrayList = (List) alarmsRepository$executePendingRequest$Anon1.L$Anon3;
                        list5 = (List) alarmsRepository$executePendingRequest$Anon1.L$Anon2;
                        alarmsRepository2 = (AlarmsRepository) alarmsRepository$executePendingRequest$Anon1.L$Anon0;
                        list = list4;
                        alarmsRepository = alarmsRepository2;
                        List list6 = arrayList;
                        list2 = list5;
                        list3 = list6;
                        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                        String str22 = TAG;
                        StringBuilder sb22 = new StringBuilder();
                        sb22.append("executePendingRequest deleteAlarmPendingList ");
                        ArrayList arrayList52 = new ArrayList(db4.a(list2, 10));
                        for (Alarm alarm2 : list2) {
                            arrayList52.add(alarm2.getUri() + " - " + alarm2.getId() + " - " + alarm2.getPinType() + " - " + alarm2.getTitle());
                        }
                        sb22.append(arrayList52);
                        local22.d(str22, sb22.toString());
                        if (!list3.isEmpty()) {
                            AlarmsRemoteDataSource alarmsRemoteDataSource2 = alarmsRepository.mAlarmRemoteDataSource;
                            alarmsRepository$executePendingRequest$Anon1.L$Anon0 = alarmsRepository;
                            alarmsRepository$executePendingRequest$Anon1.L$Anon1 = list;
                            alarmsRepository$executePendingRequest$Anon1.L$Anon2 = list2;
                            alarmsRepository$executePendingRequest$Anon1.L$Anon3 = list3;
                            alarmsRepository$executePendingRequest$Anon1.label = 3;
                            obj = alarmsRemoteDataSource2.deleteAlarms(list3, alarmsRepository$executePendingRequest$Anon1);
                            if (obj == a) {
                                return a;
                            }
                            qo2 = (qo2) obj;
                            if (!(qo2 instanceof ro2)) {
                            }
                        }
                        Boolean a32 = dc4.a(false);
                        return a32;
                    } else if (i == 3) {
                        list3 = (List) alarmsRepository$executePendingRequest$Anon1.L$Anon3;
                        list2 = (List) alarmsRepository$executePendingRequest$Anon1.L$Anon2;
                        list = (List) alarmsRepository$executePendingRequest$Anon1.L$Anon1;
                        alarmsRepository = (AlarmsRepository) alarmsRepository$executePendingRequest$Anon1.L$Anon0;
                        na4.a(obj);
                        qo2 = (qo2) obj;
                        if (!(qo2 instanceof ro2)) {
                            ug4 b = nh4.b();
                            AlarmsRepository$executePendingRequest$Anon5 alarmsRepository$executePendingRequest$Anon5 = new AlarmsRepository$executePendingRequest$Anon5(alarmsRepository, list3, (yb4) null);
                            alarmsRepository$executePendingRequest$Anon1.L$Anon0 = alarmsRepository;
                            alarmsRepository$executePendingRequest$Anon1.L$Anon1 = list;
                            alarmsRepository$executePendingRequest$Anon1.L$Anon2 = list2;
                            alarmsRepository$executePendingRequest$Anon1.L$Anon3 = list3;
                            alarmsRepository$executePendingRequest$Anon1.label = 4;
                            if (yf4.a(b, alarmsRepository$executePendingRequest$Anon5, alarmsRepository$executePendingRequest$Anon1) == a) {
                                return a;
                            }
                        } else if (qo2 instanceof po2) {
                            Boolean a4 = dc4.a(true);
                            return a4;
                        }
                        Boolean a322 = dc4.a(false);
                        return a322;
                    } else if (i == 4) {
                        List list7 = (List) alarmsRepository$executePendingRequest$Anon1.L$Anon3;
                        List list8 = (List) alarmsRepository$executePendingRequest$Anon1.L$Anon2;
                        List list9 = (List) alarmsRepository$executePendingRequest$Anon1.L$Anon1;
                        AlarmsRepository alarmsRepository3 = (AlarmsRepository) alarmsRepository$executePendingRequest$Anon1.L$Anon0;
                        na4.a(obj);
                        Boolean a3222 = dc4.a(false);
                        return a3222;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    qo22 = (qo2) obj;
                    if (!(qo22 instanceof ro2)) {
                        ug4 b2 = nh4.b();
                        AlarmsRepository$executePendingRequest$Anon3 alarmsRepository$executePendingRequest$Anon3 = new AlarmsRepository$executePendingRequest$Anon3(alarmsRepository2, qo22, (yb4) null);
                        alarmsRepository$executePendingRequest$Anon1.L$Anon0 = alarmsRepository2;
                        alarmsRepository$executePendingRequest$Anon1.L$Anon1 = list4;
                        alarmsRepository$executePendingRequest$Anon1.L$Anon2 = list5;
                        alarmsRepository$executePendingRequest$Anon1.L$Anon3 = arrayList;
                        alarmsRepository$executePendingRequest$Anon1.L$Anon4 = qo22;
                        alarmsRepository$executePendingRequest$Anon1.label = 2;
                        if (yf4.a(b2, alarmsRepository$executePendingRequest$Anon3, alarmsRepository$executePendingRequest$Anon1) == a) {
                            return a;
                        }
                    } else if (qo22 instanceof po2) {
                        Boolean a5 = dc4.a(true);
                        return a5;
                    }
                    list = list4;
                    alarmsRepository = alarmsRepository2;
                    List list62 = arrayList;
                    list2 = list5;
                    list3 = list62;
                    ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
                    String str222 = TAG;
                    StringBuilder sb222 = new StringBuilder();
                    sb222.append("executePendingRequest deleteAlarmPendingList ");
                    ArrayList arrayList522 = new ArrayList(db4.a(list2, 10));
                    while (r13.hasNext()) {
                    }
                    sb222.append(arrayList522);
                    local222.d(str222, sb222.toString());
                    if (!list3.isEmpty()) {
                    }
                    Boolean a32222 = dc4.a(false);
                    return a32222;
                }
            }
            alarmsRepository$executePendingRequest$Anon1 = new AlarmsRepository$executePendingRequest$Anon1(this, yb42);
            Object obj2 = alarmsRepository$executePendingRequest$Anon1.result;
            Object a6 = cc4.a();
            i = alarmsRepository$executePendingRequest$Anon1.label;
            if (i != 0) {
            }
            qo22 = (qo2) obj2;
            if (!(qo22 instanceof ro2)) {
            }
            list = list4;
            alarmsRepository = alarmsRepository2;
            List list622 = arrayList;
            list2 = list5;
            list3 = list622;
            ILocalFLogger local2222 = FLogger.INSTANCE.getLocal();
            String str2222 = TAG;
            StringBuilder sb2222 = new StringBuilder();
            sb2222.append("executePendingRequest deleteAlarmPendingList ");
            ArrayList arrayList5222 = new ArrayList(db4.a(list2, 10));
            while (r13.hasNext()) {
            }
            sb2222.append(arrayList5222);
            local2222.d(str2222, sb2222.toString());
            if (!list3.isEmpty()) {
            }
            Boolean a322222 = dc4.a(false);
            return a322222;
        }
    }

    @DexIgnore
    public final Alarm findNextActiveAlarm() {
        return this.mAlarmsLocalDataSource.findNexActiveAlarm();
    }

    @DexIgnore
    public final List<Alarm> getActiveAlarms() {
        return this.mAlarmsLocalDataSource.getActiveAlarms();
    }

    @DexIgnore
    public final Alarm getAlarmById(String str) {
        kd4.b(str, "id");
        return this.mAlarmsLocalDataSource.getAlarmById(str);
    }

    @DexIgnore
    public final List<Alarm> getAllAlarmIgnoreDeletePinType() {
        return this.mAlarmsLocalDataSource.getAllAlarmIgnoreDeletePinType();
    }

    @DexIgnore
    public final Object insertAlarm(Alarm alarm, yb4<? super qo2<Alarm>> yb4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insertAlarm - alarmId=" + alarm.getId());
        alarm.setPinType(1);
        this.mAlarmsLocalDataSource.upsertAlarm(alarm);
        return upsertAlarm(alarm, yb4);
    }

    @DexIgnore
    public final Object updateAlarm(Alarm alarm, yb4<? super qo2<Alarm>> yb4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "updateAlarm - alarmId=" + alarm.getId());
        if (alarm.getPinType() != 1) {
            alarm.setPinType(2);
        }
        this.mAlarmsLocalDataSource.upsertAlarm(alarm);
        return upsertAlarm(alarm, yb4);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v1, resolved type: java.util.List} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a  */
    public final /* synthetic */ Object upsertAlarm(Alarm alarm, yb4<? super qo2<Alarm>> yb4) {
        AlarmsRepository$upsertAlarm$Anon1 alarmsRepository$upsertAlarm$Anon1;
        int i;
        List list;
        ArrayList arrayList;
        AlarmsRepository alarmsRepository;
        qo2 qo2;
        Alarm alarm2 = alarm;
        yb4<? super qo2<Alarm>> yb42 = yb4;
        if (yb42 instanceof AlarmsRepository$upsertAlarm$Anon1) {
            alarmsRepository$upsertAlarm$Anon1 = (AlarmsRepository$upsertAlarm$Anon1) yb42;
            int i2 = alarmsRepository$upsertAlarm$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRepository$upsertAlarm$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRepository$upsertAlarm$Anon1.result;
                Object a = cc4.a();
                i = alarmsRepository$upsertAlarm$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    ArrayList arrayList2 = new ArrayList();
                    arrayList2.add(alarm2);
                    AlarmsRemoteDataSource alarmsRemoteDataSource = this.mAlarmRemoteDataSource;
                    alarmsRepository$upsertAlarm$Anon1.L$Anon0 = this;
                    alarmsRepository$upsertAlarm$Anon1.L$Anon1 = alarm2;
                    alarmsRepository$upsertAlarm$Anon1.L$Anon2 = arrayList2;
                    alarmsRepository$upsertAlarm$Anon1.label = 1;
                    Object upsertAlarms = alarmsRemoteDataSource.upsertAlarms(arrayList2, alarmsRepository$upsertAlarm$Anon1);
                    if (upsertAlarms == a) {
                        return a;
                    }
                    alarmsRepository = this;
                    arrayList = arrayList2;
                    obj = upsertAlarms;
                } else if (i == 1) {
                    na4.a(obj);
                    arrayList = (ArrayList) alarmsRepository$upsertAlarm$Anon1.L$Anon2;
                    alarm2 = (Alarm) alarmsRepository$upsertAlarm$Anon1.L$Anon1;
                    alarmsRepository = (AlarmsRepository) alarmsRepository$upsertAlarm$Anon1.L$Anon0;
                } else if (i == 2) {
                    List list2 = (List) alarmsRepository$upsertAlarm$Anon1.L$Anon5;
                    list = (List) alarmsRepository$upsertAlarm$Anon1.L$Anon4;
                    qo2 qo22 = (qo2) alarmsRepository$upsertAlarm$Anon1.L$Anon3;
                    ArrayList arrayList3 = (ArrayList) alarmsRepository$upsertAlarm$Anon1.L$Anon2;
                    Alarm alarm3 = (Alarm) alarmsRepository$upsertAlarm$Anon1.L$Anon1;
                    AlarmsRepository alarmsRepository2 = (AlarmsRepository) alarmsRepository$upsertAlarm$Anon1.L$Anon0;
                    na4.a(obj);
                    return new ro2(list.get(0), false, 2, (fd4) null);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    List a2 = ((ro2) qo2).a();
                    if (a2 == null) {
                        return new ro2(alarm2, false, 2, (fd4) null);
                    }
                    AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 = r4;
                    ug4 b = nh4.b();
                    AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon12 = new AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1(a2, (yb4) null, alarmsRepository, alarmsRepository$upsertAlarm$Anon1, a2);
                    alarmsRepository$upsertAlarm$Anon1.L$Anon0 = alarmsRepository;
                    alarmsRepository$upsertAlarm$Anon1.L$Anon1 = alarm2;
                    alarmsRepository$upsertAlarm$Anon1.L$Anon2 = arrayList;
                    alarmsRepository$upsertAlarm$Anon1.L$Anon3 = qo2;
                    list = a2;
                    alarmsRepository$upsertAlarm$Anon1.L$Anon4 = list;
                    alarmsRepository$upsertAlarm$Anon1.L$Anon5 = list;
                    alarmsRepository$upsertAlarm$Anon1.label = 2;
                    if (yf4.a(b, alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1, alarmsRepository$upsertAlarm$Anon1) == a) {
                        return a;
                    }
                    return new ro2(list.get(0), false, 2, (fd4) null);
                } else if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), po2.d(), (String) null, 8, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        alarmsRepository$upsertAlarm$Anon1 = new AlarmsRepository$upsertAlarm$Anon1(this, yb42);
        Object obj2 = alarmsRepository$upsertAlarm$Anon1.result;
        Object a3 = cc4.a();
        i = alarmsRepository$upsertAlarm$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }
}
