package com.portfolio.platform.data.source.remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getLatestWatchParamFromServer$response$1", mo27670f = "DeviceRemoteDataSource.kt", mo27671l = {139}, mo27672m = "invokeSuspend")
public final class DeviceRemoteDataSource$getLatestWatchParamFromServer$response$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.WatchParameterResponse>>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $majorVersion;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serial;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.remote.DeviceRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRemoteDataSource$getLatestWatchParamFromServer$response$1(com.portfolio.platform.data.source.remote.DeviceRemoteDataSource deviceRemoteDataSource, java.lang.String str, int i, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(1, yb4);
        this.this$0 = deviceRemoteDataSource;
        this.$serial = str;
        this.$majorVersion = i;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        return new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getLatestWatchParamFromServer$response$1(this.this$0, this.$serial, this.$majorVersion, yb4);
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj) {
        return ((com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getLatestWatchParamFromServer$response$1) create((com.fossil.blesdk.obfuscated.yb4) obj)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiService$p = this.this$0.mApiService;
            java.lang.String str = this.$serial;
            int i2 = this.$majorVersion;
            this.label = 1;
            obj = access$getMApiService$p.getLatestWatchParams(str, i2, "-metadata.version.minor", 0, 1, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
