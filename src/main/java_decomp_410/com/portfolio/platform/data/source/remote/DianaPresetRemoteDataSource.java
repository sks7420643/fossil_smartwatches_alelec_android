package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.jk2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.tz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaPresetRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "DianaPresetRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2Dot1;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public DianaPresetRemoteDataSource(ApiServiceV2 apiServiceV2) {
        kd4.b(apiServiceV2, "mApiServiceV2Dot1");
        this.mApiServiceV2Dot1 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object deleteDianaPreset(DianaPreset dianaPreset, yb4<? super qo2<Void>> yb4) {
        DianaPresetRemoteDataSource$deleteDianaPreset$Anon1 dianaPresetRemoteDataSource$deleteDianaPreset$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof DianaPresetRemoteDataSource$deleteDianaPreset$Anon1) {
            dianaPresetRemoteDataSource$deleteDianaPreset$Anon1 = (DianaPresetRemoteDataSource$deleteDianaPreset$Anon1) yb4;
            int i2 = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.result;
                Object a = cc4.a();
                i = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    xz1 xz1 = new xz1();
                    tz1 tz1 = new tz1();
                    tz1.a(dianaPreset.getId());
                    xz1.a("_ids", (JsonElement) tz1);
                    DianaPresetRemoteDataSource$deleteDianaPreset$response$Anon1 dianaPresetRemoteDataSource$deleteDianaPreset$response$Anon1 = new DianaPresetRemoteDataSource$deleteDianaPreset$response$Anon1(this, xz1, (yb4) null);
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon0 = this;
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon1 = dianaPreset;
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon2 = xz1;
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon3 = tz1;
                    dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label = 1;
                    obj = ResponseKt.a(dianaPresetRemoteDataSource$deleteDianaPreset$response$Anon1, dianaPresetRemoteDataSource$deleteDianaPreset$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    tz1 tz12 = (tz1) dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon3;
                    xz1 xz12 = (xz1) dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon2;
                    DianaPreset dianaPreset2 = (DianaPreset) dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    return new ro2((Object) null, false, 2, (fd4) null);
                }
                if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), (Throwable) null, (String) null, 12, (fd4) null);
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        dianaPresetRemoteDataSource$deleteDianaPreset$Anon1 = new DianaPresetRemoteDataSource$deleteDianaPreset$Anon1(this, yb4);
        Object obj2 = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.result;
        Object a2 = cc4.a();
        i = dianaPresetRemoteDataSource$deleteDianaPreset$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object downloadDianaPresetList(String str, yb4<? super qo2<ArrayList<DianaPreset>>> yb4) {
        DianaPresetRemoteDataSource$downloadDianaPresetList$Anon1 dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof DianaPresetRemoteDataSource$downloadDianaPresetList$Anon1) {
            dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1 = (DianaPresetRemoteDataSource$downloadDianaPresetList$Anon1) yb4;
            int i2 = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.result;
                Object a = cc4.a();
                i = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    DianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1 dianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1 = new DianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1(this, str, (yb4) null);
                    dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.L$Anon0 = this;
                    dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.L$Anon1 = str;
                    dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label = 1;
                    obj = ResponseKt.a(dianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1, dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.L$Anon1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ArrayList arrayList = new ArrayList();
                    ro2 ro2 = (ro2) qo2;
                    if (!ro2.b()) {
                        Object a2 = ro2.a();
                        if (a2 != null) {
                            for (DianaPreset dianaPreset : ((ApiResponse) a2).get_items()) {
                                dianaPreset.setPinType(0);
                                dianaPreset.setSerialNumber(str);
                                arrayList.add(dianaPreset);
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    return new ro2(arrayList, ro2.b());
                } else if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), (Throwable) null, (String) null, 12, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1 = new DianaPresetRemoteDataSource$downloadDianaPresetList$Anon1(this, yb4);
        Object obj2 = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.result;
        Object a3 = cc4.a();
        i = dianaPresetRemoteDataSource$downloadDianaPresetList$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object downloadDianaRecommendPresetList(String str, yb4<? super qo2<ArrayList<DianaRecommendPreset>>> yb4) {
        DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1 dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1) {
            dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1 = (DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1) yb4;
            int i2 = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.result;
                Object a = cc4.a();
                i = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "downloadDianaRecommendPresetList " + str);
                    DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$response$Anon1 dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$response$Anon1 = new DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$response$Anon1(this, str, (yb4) null);
                    dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.L$Anon0 = this;
                    dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.L$Anon1 = str;
                    dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label = 1;
                    obj = ResponseKt.a(dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$response$Anon1, dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.L$Anon1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ArrayList arrayList = new ArrayList();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadDianaRecommendPresetList success ");
                    sb.append(arrayList);
                    sb.append(" isFromCache ");
                    ro2 ro2 = (ro2) qo2;
                    sb.append(ro2.b());
                    local2.d(TAG, sb.toString());
                    if (!ro2.b()) {
                        String t = rk2.t(new Date(System.currentTimeMillis()));
                        Object a2 = ro2.a();
                        if (a2 != null) {
                            for (DianaRecommendPreset dianaRecommendPreset : ((ApiResponse) a2).get_items()) {
                                dianaRecommendPreset.setSerialNumber(str);
                                kd4.a((Object) t, "timestamp");
                                dianaRecommendPreset.setCreatedAt(t);
                                dianaRecommendPreset.setUpdatedAt(t);
                                arrayList.add(dianaRecommendPreset);
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    return new ro2(arrayList, ro2.b());
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadDianaRecommendPresetList fail code ");
                    po2 po2 = (po2) qo2;
                    sb2.append(po2.a());
                    sb2.append(" serverError ");
                    sb2.append(po2.c());
                    local3.d(TAG, sb2.toString());
                    return new po2(po2.a(), po2.c(), (Throwable) null, (String) null, 12, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1 = new DianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1(this, yb4);
        Object obj2 = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.result;
        Object a3 = cc4.a();
        i = dianaPresetRemoteDataSource$downloadDianaRecommendPresetList$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object replaceDianaPresetList(List<DianaPreset> list, yb4<? super qo2<ArrayList<DianaPreset>>> yb4) {
        DianaPresetRemoteDataSource$replaceDianaPresetList$Anon1 dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof DianaPresetRemoteDataSource$replaceDianaPresetList$Anon1) {
            dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1 = (DianaPresetRemoteDataSource$replaceDianaPresetList$Anon1) yb4;
            int i2 = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.result;
                Object a = cc4.a();
                i = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    rz1 rz1 = new rz1();
                    rz1.b(new jk2());
                    Gson a2 = rz1.a();
                    xz1 xz1 = new xz1();
                    Object[] array = list.toArray(new DianaPreset[0]);
                    if (array != null) {
                        xz1.a(CloudLogWriter.ITEMS_PARAM, a2.b((Object) array));
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d(TAG, "replaceDianaPresetList jsonObject " + xz1);
                        DianaPresetRemoteDataSource$replaceDianaPresetList$response$Anon1 dianaPresetRemoteDataSource$replaceDianaPresetList$response$Anon1 = new DianaPresetRemoteDataSource$replaceDianaPresetList$response$Anon1(this, xz1, (yb4) null);
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon0 = this;
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon1 = list;
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon2 = a2;
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon3 = xz1;
                        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label = 1;
                        obj = ResponseKt.a(dianaPresetRemoteDataSource$replaceDianaPresetList$response$Anon1, dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                } else if (i == 1) {
                    xz1 xz12 = (xz1) dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon3;
                    Gson gson = (Gson) dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon2;
                    List list2 = (List) dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ArrayList arrayList = new ArrayList();
                    Object a3 = ((ro2) qo2).a();
                    if (a3 != null) {
                        for (DianaPreset dianaPreset : ((ApiResponse) a3).get_items()) {
                            dianaPreset.setPinType(0);
                            arrayList.add(dianaPreset);
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d(TAG, "replaceDianaPresetList success " + arrayList);
                        return new ro2(arrayList, false, 2, (fd4) null);
                    }
                    kd4.a();
                    throw null;
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("replaceDianaPresetList fail code ");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(" serverError ");
                    sb.append(po2.c());
                    local3.d(TAG, sb.toString());
                    return new po2(po2.a(), po2.c(), (Throwable) null, (String) null, 12, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1 = new DianaPresetRemoteDataSource$replaceDianaPresetList$Anon1(this, yb4);
        Object obj2 = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.result;
        Object a4 = cc4.a();
        i = dianaPresetRemoteDataSource$replaceDianaPresetList$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0124  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    public final Object upsertDianaPresetList(List<DianaPreset> list, yb4<? super qo2<ArrayList<DianaPreset>>> yb4) {
        DianaPresetRemoteDataSource$upsertDianaPresetList$Anon1 dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1;
        int i;
        qo2 qo2;
        List<DianaPreset> list2 = list;
        yb4<? super qo2<ArrayList<DianaPreset>>> yb42 = yb4;
        if (yb42 instanceof DianaPresetRemoteDataSource$upsertDianaPresetList$Anon1) {
            dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1 = (DianaPresetRemoteDataSource$upsertDianaPresetList$Anon1) yb42;
            int i2 = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.result;
                Object a = cc4.a();
                i = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    rz1 rz1 = new rz1();
                    rz1.b(new jk2());
                    Gson a2 = rz1.a();
                    xz1 xz1 = new xz1();
                    Object[] array = list2.toArray(new DianaPreset[0]);
                    if (array != null) {
                        xz1.a(CloudLogWriter.ITEMS_PARAM, a2.b((Object) array));
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d(TAG, "upsertPresetList jsonObject " + xz1);
                        DianaPresetRemoteDataSource$upsertDianaPresetList$response$Anon1 dianaPresetRemoteDataSource$upsertDianaPresetList$response$Anon1 = new DianaPresetRemoteDataSource$upsertDianaPresetList$response$Anon1(this, xz1, (yb4) null);
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon0 = this;
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon1 = list2;
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon2 = a2;
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon3 = xz1;
                        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label = 1;
                        obj = ResponseKt.a(dianaPresetRemoteDataSource$upsertDianaPresetList$response$Anon1, dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                } else if (i == 1) {
                    xz1 xz12 = (xz1) dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon3;
                    Gson gson = (Gson) dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon2;
                    List list3 = (List) dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon1;
                    DianaPresetRemoteDataSource dianaPresetRemoteDataSource = (DianaPresetRemoteDataSource) dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ArrayList arrayList = new ArrayList();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("success isCache ");
                    ro2 ro2 = (ro2) qo2;
                    sb.append(ro2.b());
                    local2.d(TAG, sb.toString());
                    Object a3 = ro2.a();
                    if (a3 != null) {
                        for (DianaPreset dianaPreset : ((ApiResponse) a3).get_items()) {
                            FLogger.INSTANCE.getLocal().d(TAG, "update pin tpye as synced");
                            dianaPreset.setPinType(0);
                            arrayList.add(dianaPreset);
                        }
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        local3.d(TAG, "upsertPresetList success " + arrayList);
                        return new ro2(arrayList, false, 2, (fd4) null);
                    }
                    kd4.a();
                    throw null;
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("upsertPresetList fail code ");
                    po2 po2 = (po2) qo2;
                    sb2.append(po2.a());
                    sb2.append(" serverError ");
                    sb2.append(po2.c());
                    local4.d(TAG, sb2.toString());
                    return new po2(po2.a(), po2.c(), (Throwable) null, (String) null, 12, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1 = new DianaPresetRemoteDataSource$upsertDianaPresetList$Anon1(this, yb42);
        Object obj2 = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.result;
        Object a4 = cc4.a();
        i = dianaPresetRemoteDataSource$upsertDianaPresetList$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }
}
