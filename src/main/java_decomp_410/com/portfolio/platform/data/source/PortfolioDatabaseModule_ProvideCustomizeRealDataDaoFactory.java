package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory implements Factory<CustomizeRealDataDao> {
    @DexIgnore
    public /* final */ Provider<CustomizeRealDataDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<CustomizeRealDataDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<CustomizeRealDataDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static CustomizeRealDataDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<CustomizeRealDataDatabase> provider) {
        return proxyProvideCustomizeRealDataDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static CustomizeRealDataDao proxyProvideCustomizeRealDataDao(PortfolioDatabaseModule portfolioDatabaseModule, CustomizeRealDataDatabase customizeRealDataDatabase) {
        CustomizeRealDataDao provideCustomizeRealDataDao = portfolioDatabaseModule.provideCustomizeRealDataDao(customizeRealDataDatabase);
        n44.a(provideCustomizeRealDataDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideCustomizeRealDataDao;
    }

    @DexIgnore
    public CustomizeRealDataDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
