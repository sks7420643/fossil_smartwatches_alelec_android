package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.util.NetworkBoundResource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository$getLastGoalSetting$Anon1 extends NetworkBoundResource<Integer, GoalSetting> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$Anon0;

    @DexIgnore
    public GoalTrackingRepository$getLastGoalSetting$Anon1(GoalTrackingRepository goalTrackingRepository) {
        this.this$Anon0 = goalTrackingRepository;
    }

    @DexIgnore
    public Object createCall(yb4<? super qr4<GoalSetting>> yb4) {
        return this.this$Anon0.mApiServiceV2.getGoalSetting(yb4);
    }

    @DexIgnore
    public LiveData<Integer> loadFromDb() {
        return this.this$Anon0.mGoalTrackingDao.getLastGoalSettingLiveData();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "getLastGoalSetting onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(Integer num) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(GoalSetting goalSetting) {
        kd4.b(goalSetting, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = GoalTrackingRepository.Companion.getTAG();
        local.d(tag, "getLastGoalSetting saveCallResult goal: " + goalSetting);
        this.this$Anon0.saveSettingToDB(goalSetting);
    }
}
