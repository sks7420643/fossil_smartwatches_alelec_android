package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.wc4;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource;
import com.portfolio.platform.helper.PagingRequestHelper;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getSummariesPaging$Anon3 extends Lambda implements wc4<qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySummaryDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummariesPaging$Anon3(ActivitySummaryDataSourceFactory activitySummaryDataSourceFactory) {
        super(0);
        this.$sourceFactory = activitySummaryDataSourceFactory;
    }

    @DexIgnore
    public final void invoke() {
        ActivitySummaryLocalDataSource a = this.$sourceFactory.getSourceLiveData().a();
        if (a != null) {
            PagingRequestHelper mHelper = a.getMHelper();
            if (mHelper != null) {
                mHelper.b();
            }
        }
    }
}
