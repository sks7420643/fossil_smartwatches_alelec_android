package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ServerSettingRemoteDataSource implements ServerSettingDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 apiService;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ServerSettingRemoteDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = ServerSettingRemoteDataSource.class.getSimpleName();
        kd4.a((Object) simpleName, "ServerSettingRemoteDataS\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ServerSettingRemoteDataSource(ApiServiceV2 apiServiceV2) {
        kd4.b(apiServiceV2, "apiService");
        this.apiService = apiServiceV2;
    }

    @DexIgnore
    public void addOrUpdateServerSetting(ServerSetting serverSetting) {
    }

    @DexIgnore
    public void addOrUpdateServerSettingList(List<ServerSetting> list) {
    }

    @DexIgnore
    public void clearData() {
    }

    @DexIgnore
    public ServerSetting getServerSettingByKey(String str) {
        kd4.b(str, "key");
        return null;
    }

    @DexIgnore
    public void getServerSettingList(ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        throw null;
        // kd4.b(onGetServerSettingList, Constants.CALLBACK);
        // fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new ServerSettingRemoteDataSource$getServerSettingList$Anon1(this, onGetServerSettingList, (yb4) null), 3, (Object) null);
    }
}
