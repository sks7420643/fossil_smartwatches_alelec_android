package com.portfolio.platform.data.source.local.hybrid.microapp;

import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class HybridCustomizeDatabase extends RoomDatabase {
    @DexIgnore
    public abstract MicroAppDao microAppDao();

    @DexIgnore
    public abstract MicroAppLastSettingDao microAppLastSettingDao();

    @DexIgnore
    public abstract HybridPresetDao presetDao();
}
