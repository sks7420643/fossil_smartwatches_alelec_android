package com.portfolio.platform.data.source.local.sleep;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1", mo27670f = "SleepSummaryLocalDataSource.kt", mo27671l = {179, 179}, mo27672m = "invokeSuspend")
public final class SleepSummaryLocalDataSource$loadData$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.helper.PagingRequestHelper.RequestType $requestType;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $startDate;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public java.lang.Object L$3;
    @DexIgnore
    public java.lang.Object L$4;
    @DexIgnore
    public java.lang.Object L$5;
    @DexIgnore
    public java.lang.Object L$6;
    @DexIgnore
    public java.lang.Object L$7;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21128p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummaryLocalDataSource$loadData$1(com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource sleepSummaryLocalDataSource, java.util.Date date, java.util.Date date2, com.portfolio.platform.helper.PagingRequestHelper.RequestType requestType, com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a aVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = sleepSummaryLocalDataSource;
        this.$startDate = date;
        this.$endDate = date2;
        this.$requestType = requestType;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1 sleepSummaryLocalDataSource$loadData$1 = new com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1(this.this$0, this.$startDate, this.$endDate, this.$requestType, this.$helperCallback, yb4);
        sleepSummaryLocalDataSource$loadData$1.f21128p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return sleepSummaryLocalDataSource$loadData$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource sleepSummaryLocalDataSource;
        java.lang.Object obj2;
        com.portfolio.platform.helper.PagingRequestHelper.RequestType requestType;
        com.fossil.blesdk.obfuscated.qo2 qo2;
        com.fossil.blesdk.obfuscated.gh4 gh4;
        java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> list;
        kotlin.Pair<java.util.Date, java.util.Date> pair;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        com.fossil.blesdk.obfuscated.gh4 gh42;
        java.lang.Object obj3;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f21128p$;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d(com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource.TAG, "loadData start=" + this.$startDate + ", end=" + this.$endDate);
            list = this.this$0.mFitnessDataRepository.getFitnessData(this.$startDate, this.$endDate);
            pair = com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt.calculateRangeDownload(list, this.$startDate, this.$endDate);
            if (pair != null) {
                com.fossil.blesdk.obfuscated.zg4 zg43 = zg42;
                com.fossil.blesdk.obfuscated.gh4 a2 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg43, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1$summariesDeferred$1(this, pair, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                gh42 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg43, (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1$sleepSessionsDeferred$1(this, pair, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                sleepSummaryLocalDataSource = this.this$0;
                com.portfolio.platform.helper.PagingRequestHelper.RequestType requestType2 = this.$requestType;
                this.L$0 = zg42;
                this.L$1 = list;
                this.L$2 = pair;
                this.L$3 = a2;
                this.L$4 = gh42;
                this.L$5 = sleepSummaryLocalDataSource;
                this.L$6 = requestType2;
                this.label = 1;
                obj3 = a2.mo27693a(this);
                if (obj3 == a) {
                    return a;
                }
                com.fossil.blesdk.obfuscated.gh4 gh43 = a2;
                zg4 = zg42;
                requestType = requestType2;
                gh4 = gh43;
            } else {
                this.$helperCallback.mo39588a();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        } else if (i == 1) {
            requestType = (com.portfolio.platform.helper.PagingRequestHelper.RequestType) this.L$6;
            gh42 = (com.fossil.blesdk.obfuscated.gh4) this.L$4;
            pair = (kotlin.Pair) this.L$2;
            list = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            gh4 = (com.fossil.blesdk.obfuscated.gh4) this.L$3;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            sleepSummaryLocalDataSource = (com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource) this.L$5;
            obj3 = obj;
        } else if (i == 2) {
            qo2 = (com.fossil.blesdk.obfuscated.qo2) this.L$7;
            requestType = (com.portfolio.platform.helper.PagingRequestHelper.RequestType) this.L$6;
            com.fossil.blesdk.obfuscated.gh4 gh44 = (com.fossil.blesdk.obfuscated.gh4) this.L$4;
            com.fossil.blesdk.obfuscated.gh4 gh45 = (com.fossil.blesdk.obfuscated.gh4) this.L$3;
            kotlin.Pair pair2 = (kotlin.Pair) this.L$2;
            java.util.List list2 = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg44 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            sleepSummaryLocalDataSource = (com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource) this.L$5;
            obj2 = obj;
            sleepSummaryLocalDataSource.combineData(requestType, qo2, (com.fossil.blesdk.obfuscated.qo2) obj2, this.$helperCallback);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo22 = (com.fossil.blesdk.obfuscated.qo2) obj3;
        this.L$0 = zg4;
        this.L$1 = list;
        this.L$2 = pair;
        this.L$3 = gh4;
        this.L$4 = gh42;
        this.L$5 = sleepSummaryLocalDataSource;
        this.L$6 = requestType;
        this.L$7 = qo22;
        this.label = 2;
        obj2 = gh42.mo27693a(this);
        if (obj2 == a) {
            return a;
        }
        qo2 = qo22;
        sleepSummaryLocalDataSource.combineData(requestType, qo2, (com.fossil.blesdk.obfuscated.qo2) obj2, this.$helperCallback);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
