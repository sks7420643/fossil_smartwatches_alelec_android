package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.gt4;
import com.fossil.blesdk.obfuscated.us4;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ShortcutApiService {
    @DexIgnore
    @us4("default-presets")
    Call<ApiResponse<RecommendedPreset>> getDefaultPreset(@gt4("offset") int i, @gt4("size") int i2, @gt4("serialNumber") String str);

    @DexIgnore
    @us4("micro-apps")
    Call<ApiResponse<MicroApp>> getMicroAppGallery(@gt4("page") int i, @gt4("size") int i2, @gt4("serialNumber") String str);
}
