package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository$getSummary$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends NetworkBoundResource<GoalTrackingSummary, GoalDailySummary> {
        @DexIgnore
        public /* final */ /* synthetic */ List $pendingList;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingRepository$getSummary$Anon1 this$Anon0;

        @DexIgnore
        public Anon1(GoalTrackingRepository$getSummary$Anon1 goalTrackingRepository$getSummary$Anon1, List list) {
            this.this$Anon0 = goalTrackingRepository$getSummary$Anon1;
            this.$pendingList = list;
        }

        @DexIgnore
        public Object createCall(yb4<? super qr4<GoalDailySummary>> yb4) {
            ApiServiceV2 access$getMApiServiceV2$p = this.this$Anon0.this$Anon0.mApiServiceV2;
            String e = rk2.e(this.this$Anon0.$date);
            kd4.a((Object) e, "DateHelper.formatShortDate(date)");
            return access$getMApiServiceV2$p.getGoalTrackingSummary(e, yb4);
        }

        @DexIgnore
        public LiveData<GoalTrackingSummary> loadFromDb() {
            return this.this$Anon0.this$Anon0.mGoalTrackingDao.getGoalTrackingSummaryLiveData(this.this$Anon0.$date);
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().e(GoalTrackingRepository.Companion.getTAG(), "getSummary onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(GoalDailySummary goalDailySummary) {
            kd4.b(goalDailySummary, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = GoalTrackingRepository.Companion.getTAG();
            local.d(tag, "getSummary date=" + this.this$Anon0.$date + " saveCallResult onResponse: response = " + goalDailySummary);
            try {
                GoalTrackingDao access$getMGoalTrackingDao$p = this.this$Anon0.this$Anon0.mGoalTrackingDao;
                GoalTrackingSummary goalTrackingSummary = goalDailySummary.toGoalTrackingSummary();
                if (goalTrackingSummary != null) {
                    access$getMGoalTrackingDao$p.upsertGoalTrackingSummary(goalTrackingSummary);
                } else {
                    kd4.a();
                    throw null;
                }
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String tag2 = GoalTrackingRepository.Companion.getTAG();
                local2.e(tag2, "getSummary date=" + this.this$Anon0.$date + " exception=" + e);
                e.printStackTrace();
            }
        }

        @DexIgnore
        public boolean shouldFetch(GoalTrackingSummary goalTrackingSummary) {
            return this.$pendingList.isEmpty();
        }
    }

    @DexIgnore
    public GoalTrackingRepository$getSummary$Anon1(GoalTrackingRepository goalTrackingRepository, Date date) {
        this.this$Anon0 = goalTrackingRepository;
        this.$date = date;
    }

    @DexIgnore
    public final LiveData<os3<GoalTrackingSummary>> apply(List<GoalTrackingData> list) {
        return new Anon1(this, list).asLiveData();
    }
}
