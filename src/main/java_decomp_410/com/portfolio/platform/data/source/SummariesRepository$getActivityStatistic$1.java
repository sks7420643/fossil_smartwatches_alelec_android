package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getActivityStatistic$1 extends com.portfolio.platform.util.NetworkBoundResource<com.portfolio.platform.data.ActivityStatistic, com.portfolio.platform.data.ActivityStatistic> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SummariesRepository this$0;

    @DexIgnore
    public SummariesRepository$getActivityStatistic$1(com.portfolio.platform.data.source.SummariesRepository summariesRepository, boolean z) {
        this.this$0 = summariesRepository;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.ActivityStatistic>> yb4) {
        return this.this$0.mApiServiceV2.getActivityStatistic(yb4);
    }

    @DexIgnore
    public androidx.lifecycle.LiveData<com.portfolio.platform.data.ActivityStatistic> loadFromDb() {
        return this.this$0.mActivitySummaryDao.getActivityStatisticLiveData();
    }

    @DexIgnore
    public void onFetchFailed(java.lang.Throwable th) {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "getActivityStatistic - onFetchFailed");
    }

    @DexIgnore
    public void saveCallResult(com.portfolio.platform.data.ActivityStatistic activityStatistic) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(activityStatistic, "item");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "getActivityStatistic - saveCallResult -- item=" + activityStatistic);
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$1$saveCallResult$1(this, activityStatistic, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }

    @DexIgnore
    public boolean shouldFetch(com.portfolio.platform.data.ActivityStatistic activityStatistic) {
        return this.$shouldFetch;
    }
}
