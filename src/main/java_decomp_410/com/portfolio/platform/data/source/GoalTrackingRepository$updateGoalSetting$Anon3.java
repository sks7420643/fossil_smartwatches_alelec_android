package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalSetting$Anon3", f = "GoalTrackingRepository.kt", l = {}, m = "invokeSuspend")
public final class GoalTrackingRepository$updateGoalSetting$Anon3 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository.UpdateGoalSettingCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ qo2 $repoResponse;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$updateGoalSetting$Anon3(GoalTrackingRepository.UpdateGoalSettingCallback updateGoalSettingCallback, qo2 qo2, yb4 yb4) {
        super(2, yb4);
        this.$callback = updateGoalSettingCallback;
        this.$repoResponse = qo2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        GoalTrackingRepository$updateGoalSetting$Anon3 goalTrackingRepository$updateGoalSetting$Anon3 = new GoalTrackingRepository$updateGoalSetting$Anon3(this.$callback, this.$repoResponse, yb4);
        goalTrackingRepository$updateGoalSetting$Anon3.p$ = (zg4) obj;
        return goalTrackingRepository$updateGoalSetting$Anon3;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingRepository$updateGoalSetting$Anon3) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            GoalTrackingRepository.UpdateGoalSettingCallback updateGoalSettingCallback = this.$callback;
            if (updateGoalSettingCallback == null) {
                return null;
            }
            updateGoalSettingCallback.onFail(new po2(((po2) this.$repoResponse).a(), ((po2) this.$repoResponse).c(), ((po2) this.$repoResponse).d(), ((po2) this.$repoResponse).b()));
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
