package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository$getSleepSummaries$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $end;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $start;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSummariesRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$1$1")
    /* renamed from: com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$1$1 */
    public static final class C57151 extends com.portfolio.platform.util.NetworkBoundResource<java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepDay>, com.fossil.blesdk.obfuscated.xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.Pair $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$1 this$0;

        @DexIgnore
        public C57151(com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$1 sleepSummariesRepository$getSleepSummaries$1, kotlin.Pair pair) {
            this.this$0 = sleepSummariesRepository$getSleepSummaries$1;
            this.$downloadingDate = pair;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
            if (r0 != null) goto L_0x0019;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
            if (r0 != null) goto L_0x0033;
         */
        @DexIgnore
        public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.fossil.blesdk.obfuscated.xz1>> yb4) {
            java.util.Date date;
            java.util.Date date2;
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
            kotlin.Pair pair = this.$downloadingDate;
            if (pair != null) {
                date = (java.util.Date) pair.getFirst();
            }
            date = this.this$0.$startDate;
            java.lang.String e = com.fossil.blesdk.obfuscated.rk2.m27397e(date);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            kotlin.Pair pair2 = this.$downloadingDate;
            if (pair2 != null) {
                date2 = (java.util.Date) pair2.getSecond();
            }
            date2 = this.this$0.$endDate;
            java.lang.String e2 = com.fossil.blesdk.obfuscated.rk2.m27397e(date2);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiService$p.getSleepSummaries(e, e2, 0, 100, yb4);
        }

        @DexIgnore
        public androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepDay>> loadFromDb() {
            com.portfolio.platform.data.source.local.sleep.SleepDao access$getMSleepDao$p = this.this$0.this$0.mSleepDao;
            java.lang.String str = this.this$0.$start;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str, com.facebook.share.internal.VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
            java.lang.String str2 = this.this$0.$end;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str2, "end");
            return access$getMSleepDao$p.getSleepDaysLiveData(str, str2);
        }

        @DexIgnore
        public void onFetchFailed(java.lang.Throwable th) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(com.portfolio.platform.data.source.SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "getActivityList onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(com.fossil.blesdk.obfuscated.xz1 xz1) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(xz1, "item");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
            local.mo33255d(tAG$app_fossilRelease, "getSleepSummaries saveCallResult onResponse: response = " + xz1);
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$1 sleepSummariesRepository$getSleepSummaries$1 = this.this$0;
            sleepSummariesRepository$getSleepSummaries$1.this$0.saveSleepSummaries$app_fossilRelease(xz1, sleepSummariesRepository$getSleepSummaries$1.$startDate, sleepSummariesRepository$getSleepSummaries$1.$endDate, this.$downloadingDate);
        }

        @DexIgnore
        public boolean shouldFetch(java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepDay> list) {
            return this.this$0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public SleepSummariesRepository$getSleepSummaries$1(com.portfolio.platform.data.source.SleepSummariesRepository sleepSummariesRepository, java.util.Date date, java.util.Date date2, boolean z, java.lang.String str, java.lang.String str2) {
        this.this$0 = sleepSummariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
        this.$start = str;
        this.$end = str2;
    }

    @DexIgnore
    public final androidx.lifecycle.LiveData<com.fossil.blesdk.obfuscated.os3<java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepDay>>> apply(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> list) {
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) list, "fitnessDataList");
        return new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$1.C57151(this, com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt.calculateRangeDownload(list, this.$startDate, this.$endDate)).asLiveData();
    }
}
