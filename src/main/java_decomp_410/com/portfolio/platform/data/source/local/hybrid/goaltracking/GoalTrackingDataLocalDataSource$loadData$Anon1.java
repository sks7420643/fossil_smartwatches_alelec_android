package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$Anon1", f = "GoalTrackingDataLocalDataSource.kt", l = {94, 97, 102}, m = "invokeSuspend")
public final class GoalTrackingDataLocalDataSource$loadData$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$Anon1$Anon1", f = "GoalTrackingDataLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource$loadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingDataLocalDataSource$loadData$Anon1 goalTrackingDataLocalDataSource$loadData$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = goalTrackingDataLocalDataSource$loadData$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.$helperCallback.a();
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$Anon1$Anon2", f = "GoalTrackingDataLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ qo2 $data;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource$loadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(GoalTrackingDataLocalDataSource$loadData$Anon1 goalTrackingDataLocalDataSource$loadData$Anon1, qo2 qo2, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = goalTrackingDataLocalDataSource$loadData$Anon1;
            this.$data = qo2;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, this.$data, yb4);
            anon2.p$ = (zg4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                if (((po2) this.$data).d() != null) {
                    this.this$Anon0.$helperCallback.a(((po2) this.$data).d());
                } else if (((po2) this.$data).c() != null) {
                    ServerError c = ((po2) this.$data).c();
                    PagingRequestHelper.b.a aVar = this.this$Anon0.$helperCallback;
                    String userMessage = c.getUserMessage();
                    if (userMessage == null) {
                        userMessage = c.getMessage();
                    }
                    if (userMessage == null) {
                        userMessage = "";
                    }
                    aVar.a(new Throwable(userMessage));
                }
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDataLocalDataSource$loadData$Anon1(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource, int i, PagingRequestHelper.b.a aVar, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = goalTrackingDataLocalDataSource;
        this.$offset = i;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        GoalTrackingDataLocalDataSource$loadData$Anon1 goalTrackingDataLocalDataSource$loadData$Anon1 = new GoalTrackingDataLocalDataSource$loadData$Anon1(this.this$Anon0, this.$offset, this.$helperCallback, yb4);
        goalTrackingDataLocalDataSource$loadData$Anon1.p$ = (zg4) obj;
        return goalTrackingDataLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingDataLocalDataSource$loadData$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = GoalTrackingDataLocalDataSource.Companion.getTAG();
            local.d(tag, "loadData currentDate=" + this.this$Anon0.mCurrentDate + ", offset=" + this.$offset);
            GoalTrackingRepository access$getMGoalTrackingRepository$p = this.this$Anon0.mGoalTrackingRepository;
            Date access$getMCurrentDate$p = this.this$Anon0.mCurrentDate;
            Date access$getMCurrentDate$p2 = this.this$Anon0.mCurrentDate;
            int i2 = this.$offset;
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = GoalTrackingRepository.loadGoalTrackingDataList$default(access$getMGoalTrackingRepository$p, access$getMCurrentDate$p, access$getMCurrentDate$p2, i2, 0, this, 8, (Object) null);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else if (i == 2 || i == 3) {
            qo2 qo2 = (qo2) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo22 = (qo2) obj;
        if (qo22 instanceof ro2) {
            GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = this.this$Anon0;
            goalTrackingDataLocalDataSource.mOffset = goalTrackingDataLocalDataSource.mOffset + 100;
            pi4 c = nh4.c();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = qo22;
            this.label = 2;
            if (yf4.a(c, anon1, this) == a) {
                return a;
            }
        } else if (qo22 instanceof po2) {
            pi4 c2 = nh4.c();
            Anon2 anon2 = new Anon2(this, qo22, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = qo22;
            this.label = 3;
            if (yf4.a(c2, anon2, this) == a) {
                return a;
            }
        }
        return qa4.a;
    }
}
