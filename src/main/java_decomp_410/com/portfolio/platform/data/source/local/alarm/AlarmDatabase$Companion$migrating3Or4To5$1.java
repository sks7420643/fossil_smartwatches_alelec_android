package com.portfolio.platform.data.source.local.alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmDatabase$Companion$migrating3Or4To5$1 extends com.fossil.blesdk.obfuscated.C3361yf {
    @DexIgnore
    public /* final */ /* synthetic */ int $previous;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $userId;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmDatabase$Companion$migrating3Or4To5$1(int i, java.lang.String str, int i2, int i3) {
        super(i2, i3);
        this.$previous = i;
        this.$userId = str;
    }

    @DexIgnore
    public void migrate(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        com.portfolio.platform.data.source.local.alarm.AlarmDatabase$Companion$migrating3Or4To5$1 alarmDatabase$Companion$migrating3Or4To5$1 = this;
        com.fossil.blesdk.obfuscated.C1874gg ggVar2 = ggVar;
        com.fossil.blesdk.obfuscated.kd4.m24411b(ggVar2, "database");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String access$getTAG$cp = com.portfolio.platform.data.source.local.alarm.AlarmDatabase.TAG;
        local.mo33255d(access$getTAG$cp, "migrating3Or4To5 - previous: " + alarmDatabase$Companion$migrating3Or4To5$1.$previous);
        ggVar.mo11234s();
        try {
            ggVar2.mo11230b("CREATE TABLE alarm_temp (id TEXT, uri TEXT PRIMARY KEY NOT NULL, title TEXT NOT NULL, hour INTEGER NOT NULL, minute INTEGER NOT NULL, days TEXT, isActive INTEGER NOT NULL, isRepeated INTEGER NOT NULL, createdAt TEXT, updatedAt TEXT NOT NULL, pinType INTEGER NOT NULL DEFAULT 1)");
            if (alarmDatabase$Companion$migrating3Or4To5$1.$previous == 4) {
                ggVar2.mo11230b("INSERT INTO alarm_temp (id, uri, title, hour, minute, days, isActive, isRepeated, createdAt, updatedAt, pinType) SELECT objectId, uri, alarmTitle, 0, alarmMinute, days, isActiveAlarm, isRepeat, createdAt, updatedAt, pinType FROM alarm");
            } else {
                ggVar2.mo11230b("INSERT INTO alarm_temp (id, uri, title, hour, minute, days, isActive, isRepeated, createdAt, updatedAt, pinType) SELECT objectId, uri, alarmTitle, 0, alarmMinute, days, isActiveAlarm, isRepeat, createdAt, updatedAt FROM alarm");
            }
            ggVar2.mo11230b("UPDATE alarm_temp SET hour = minute/60, minute = minute%60");
            ggVar2.mo11230b("DROP TABLE alarm");
            ggVar2.mo11230b("ALTER TABLE alarm_temp RENAME TO alarm");
            android.database.Cursor d = ggVar2.mo11232d("SELECT*FROM alarm WHERE instr(uri, 'uri:') > 0 or instr(uri, ':') = 0");
            d.moveToFirst();
            while (true) {
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) d, "cursor");
                if (d.isAfterLast()) {
                    break;
                }
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append(alarmDatabase$Companion$migrating3Or4To5$1.$userId);
                sb.append(':');
                java.util.Calendar instance = java.util.Calendar.getInstance();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "Calendar.getInstance()");
                sb.append(instance.getTimeInMillis());
                java.lang.String sb2 = sb.toString();
                java.lang.String string = d.getString(d.getColumnIndex("title"));
                int i = d.getInt(d.getColumnIndex(com.fossil.wearables.fsl.appfilter.AppFilter.COLUMN_HOUR));
                int i2 = d.getInt(d.getColumnIndex(com.fossil.wearables.fsl.sleep.MFSleepGoal.COLUMN_MINUTE));
                java.lang.String string2 = d.getString(d.getColumnIndex(com.misfit.frameworks.buttonservice.model.Alarm.COLUMN_DAYS));
                int i3 = d.getInt(d.getColumnIndex("isActive"));
                int i4 = d.getInt(d.getColumnIndex("isRepeated"));
                java.lang.String string3 = d.getString(d.getColumnIndex("createdAt"));
                java.lang.String string4 = d.getString(d.getColumnIndex("updatedAt"));
                java.lang.StringBuilder sb3 = new java.lang.StringBuilder();
                android.database.Cursor cursor = d;
                sb3.append("INSERT INTO alarm(id, uri, title, hour, minute, days, isActive, isRepeated, createdAt, updatedAt, pinType) VALUES (null, '");
                sb3.append(sb2);
                sb3.append("', '");
                sb3.append(string);
                sb3.append("', ");
                sb3.append(i);
                sb3.append(", ");
                sb3.append(i2);
                sb3.append(", '");
                sb3.append(string2);
                sb3.append("', ");
                sb3.append(i3);
                sb3.append(", ");
                sb3.append(i4);
                sb3.append(", '");
                sb3.append(string3);
                sb3.append("', '");
                sb3.append(string4);
                sb3.append("', 1)");
                ggVar2.mo11230b(sb3.toString());
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String access$getTAG$cp2 = com.portfolio.platform.data.source.local.alarm.AlarmDatabase.TAG;
                local2.mo33255d(access$getTAG$cp2, "migrating3Or4To5 uri: " + sb2 + " - title: " + string);
                cursor.moveToNext();
                alarmDatabase$Companion$migrating3Or4To5$1 = this;
                d = cursor;
            }
            d.close();
            ggVar2.mo11230b("UPDATE alarm SET pinType = 3 WHERE instr(uri, 'uri:') > 0 or instr(uri, ':') = 0");
        } catch (java.lang.Exception unused) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(com.portfolio.platform.data.source.local.alarm.AlarmDatabase.TAG, "migration is failed!");
            ggVar2.mo11230b("DROP TABLE IF EXISTS alarm_temp");
            ggVar2.mo11230b("DROP TABLE IF EXISTS alarm");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS  alarm (id TEXT, uri TEXT PRIMARY KEY NOT NULL, title TEXT NOT NULL, hour INTEGER NOT NULL, minute INTEGER NOT NULL, days TEXT, isActive INTEGER NOT NULL, isRepeated INTEGER NOT NULL, createdAt TEXT, updatedAt TEXT NOT NULL, pinType INTEGER NOT NULL DEFAULT 1)");
        }
        ggVar.mo11236u();
        ggVar.mo11237v();
    }
}
