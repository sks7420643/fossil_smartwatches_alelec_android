package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.em4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.vk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.WatchFaceRepository$processDownloadAndStore$Anon1", f = "WatchFaceRepository.kt", l = {78}, m = "invokeSuspend")
public final class WatchFaceRepository$processDownloadAndStore$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super Boolean>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $path;
    @DexIgnore
    public /* final */ /* synthetic */ String $url;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchFaceRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchFaceRepository$processDownloadAndStore$Anon1(WatchFaceRepository watchFaceRepository, String str, String str2, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = watchFaceRepository;
        this.$url = str;
        this.$path = str2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WatchFaceRepository$processDownloadAndStore$Anon1 watchFaceRepository$processDownloadAndStore$Anon1 = new WatchFaceRepository$processDownloadAndStore$Anon1(this.this$Anon0, this.$url, this.$path, yb4);
        watchFaceRepository$processDownloadAndStore$Anon1.p$ = (zg4) obj;
        return watchFaceRepository$processDownloadAndStore$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WatchFaceRepository$processDownloadAndStore$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        boolean z;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            WatchFaceRemoteDataSource access$getWatchFaceRemoteDataSource$p = this.this$Anon0.watchFaceRemoteDataSource;
            String str = this.$url;
            String str2 = this.$path;
            this.L$Anon0 = zg4;
            this.label = 1;
            obj = access$getWatchFaceRemoteDataSource$p.downloadWatchFaceFile(str, str2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo2 = (qo2) obj;
        if (qo2 instanceof ro2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$p = this.this$Anon0.TAG;
            local.d(access$getTAG$p, "start storing for url: " + this.$url + " to path: " + this.$path);
            vk2 vk2 = vk2.a;
            Object a2 = ((ro2) qo2).a();
            if (a2 != null) {
                z = vk2.a((em4) a2, this.$path);
            } else {
                kd4.a();
                throw null;
            }
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String access$getTAG$p2 = this.this$Anon0.TAG;
            local2.e(access$getTAG$p2, "download: " + this.$url + " | FAILED");
            z = false;
        }
        return dc4.a(z);
    }
}
