package com.portfolio.platform.data.source.local.diana.heartrate;

import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSummaryLocalDataSource$loadInitial$Anon1 implements PagingRequestHelper.b {
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryLocalDataSource this$Anon0;

    @DexIgnore
    public HeartRateSummaryLocalDataSource$loadInitial$Anon1(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        this.this$Anon0 = heartRateSummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(PagingRequestHelper.b.a aVar) {
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource = this.this$Anon0;
        heartRateSummaryLocalDataSource.calculateStartDate(heartRateSummaryLocalDataSource.mCreatedDate);
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource2 = this.this$Anon0;
        Date mStartDate = heartRateSummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$Anon0.getMEndDate();
        kd4.a((Object) aVar, "helperCallback");
        fi4 unused = heartRateSummaryLocalDataSource2.loadData(mStartDate, mEndDate, aVar);
    }
}
