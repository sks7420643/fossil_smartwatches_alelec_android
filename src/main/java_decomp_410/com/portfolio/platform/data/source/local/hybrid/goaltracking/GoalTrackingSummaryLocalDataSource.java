package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.jl2;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.od;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import kotlin.Pair;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingSummaryLocalDataSource extends od<Date, GoalTrackingSummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "GoalTrackingSummaryLocalDataSource";
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ PagingRequestHelper.a listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ GoalTrackingDao mGoalTrackingDao;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public PagingRequestHelper mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState; // = jl2.a(this.mHelper);
    @DexIgnore
    public /* final */ pf.c mObserver;
    @DexIgnore
    public List<Pair<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public Date mStartDate; // = new Date();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends pf.c {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$Anon0 = goalTrackingSummaryLocalDataSource;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            kd4.b(set, "tables");
            this.this$Anon0.invalidate();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            kd4.b(date, "date");
            kd4.b(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(GoalTrackingSummaryLocalDataSource.TAG, "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar c = rk2.c(instance);
            if (rk2.b(date2, c.getTime())) {
                c.setTime(date2);
            }
            Date time = c.getTime();
            kd4.a((Object) time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public GoalTrackingSummaryLocalDataSource(GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, Date date, h42 h42, PagingRequestHelper.a aVar, Calendar calendar) {
        kd4.b(goalTrackingRepository, "mGoalTrackingRepository");
        kd4.b(goalTrackingDao, "mGoalTrackingDao");
        kd4.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        kd4.b(date, "mCreatedDate");
        kd4.b(h42, "appExecutors");
        kd4.b(aVar, "listener");
        kd4.b(calendar, "key");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDao = goalTrackingDao;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mCreatedDate = date;
        this.listener = aVar;
        this.key = calendar;
        this.mHelper = new PagingRequestHelper(h42.a());
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "goalTrackingDay", new String[0]);
        this.mGoalTrackingDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar c = rk2.c(instance);
        c.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + c.getTime());
        Date time = c.getTime();
        kd4.a((Object) time, "calendar.time");
        this.mStartDate = time;
        if (rk2.b(date, this.mStartDate)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final List<GoalTrackingSummary> calculateSummaries(List<GoalTrackingSummary> list) {
        int i;
        int i2;
        List<GoalTrackingSummary> list2 = list;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "endCalendar");
            instance.setTime(((GoalTrackingSummary) kb4.f(list)).getDate());
            if (instance.get(7) != 1) {
                Calendar p = rk2.p(instance.getTime());
                kd4.a((Object) p, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                GoalTrackingDao goalTrackingDao = this.mGoalTrackingDao;
                Date time = p.getTime();
                kd4.a((Object) time, "startCalendar.time");
                Date time2 = instance.getTime();
                kd4.a((Object) time2, "endCalendar.time");
                GoalTrackingDao.TotalSummary goalTrackingValueAndTarget = goalTrackingDao.getGoalTrackingValueAndTarget(time, time2);
                i = goalTrackingValueAndTarget.getValues();
                i2 = goalTrackingValueAndTarget.getTargets();
            } else {
                i2 = 0;
                i = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            kd4.a((Object) instance2, "calendar");
            instance2.setTime(((GoalTrackingSummary) kb4.d(list)).getDate());
            Calendar p2 = rk2.p(instance2.getTime());
            kd4.a((Object) p2, "DateHelper.getStartOfWeek(calendar.time)");
            p2.add(5, -1);
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            for (T next : list) {
                int i7 = i6 + 1;
                if (i6 >= 0) {
                    GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) next;
                    Date component1 = goalTrackingSummary.component1();
                    int component2 = goalTrackingSummary.component2();
                    int component3 = goalTrackingSummary.component3();
                    if (rk2.d(component1, p2.getTime())) {
                        list2.get(i3).setTotalValueOfWeek(i4);
                        list2.get(i3).setTotalTargetOfWeek(i5);
                        p2.add(5, -7);
                        i3 = i6;
                        i4 = 0;
                        i5 = 0;
                    }
                    i4 += component2;
                    i5 += component3;
                    if (i6 == list.size() - 1) {
                        i4 += i;
                        i5 += i2;
                    }
                    i6 = i7;
                } else {
                    cb4.c();
                    throw null;
                }
            }
            list2.get(i3).setTotalValueOfWeek(i4);
            list2.get(i3).setTotalTargetOfWeek(i5);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(TAG, "calculateSummaries summaries.size=" + list.size());
        }
        return list2;
    }

    @DexIgnore
    private final GoalTrackingSummary dummySummary(Date date) {
        return new GoalTrackingSummary(date, 0, this.mGoalTrackingDao.getNearestGoalTrackingTargetFromDate(date), new Date().getTime(), new Date().getTime());
    }

    @DexIgnore
    private final List<GoalTrackingSummary> getDataInDatabase(Date date, Date date2) {
        FLogger.INSTANCE.getLocal().d(TAG, "getDataInDatabase - startDate=" + date + ", endDate=" + date2);
        List<GoalTrackingSummary> goalTrackingSummaries = this.mGoalTrackingRepository.getGoalTrackingSummaries(date, date2);
        ArrayList arrayList = new ArrayList();
        int size = goalTrackingSummaries.size() + -1;
        FLogger.INSTANCE.getLocal().d(TAG, "getDataInDatabase - summaries.size=" + goalTrackingSummaries.size() + ", " + "startDate=" + date + ", endDateToFill=" + date2);
        while (rk2.c(date2, date)) {
            if (size < 0 || !rk2.d(goalTrackingSummaries.get(size).getDate(), date2)) {
                arrayList.add(dummySummary(date2));
            } else {
                arrayList.add(goalTrackingSummaries.get(size));
                size--;
            }
            date2 = rk2.m(date2);
            kd4.a((Object) date2, "DateHelper.getPrevDate(endDateToFill)");
        }
        calculateSummaries(arrayList);
        if (!arrayList.isEmpty()) {
            GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) kb4.d(arrayList);
            Boolean s = rk2.s(goalTrackingSummary.getDate());
            kd4.a((Object) s, "DateHelper.isToday(todaySummary.date)");
            if (s.booleanValue()) {
                arrayList.add(0, GoalTrackingSummary.copy$default(goalTrackingSummary, goalTrackingSummary.getDate(), goalTrackingSummary.getTotalTracked(), goalTrackingSummary.getGoalTarget(), 0, 0, 24, (Object) null));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final fi4 loadData(PagingRequestHelper.RequestType requestType, Date date, Date date2, PagingRequestHelper.b.a aVar) {
        return ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingSummaryLocalDataSource$loadData$Anon1(this, date, date2, requestType, aVar, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final PagingRequestHelper getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    public boolean isInvalid() {
        this.mGoalTrackingDatabase.getInvalidationTracker().b();
        return super.isInvalid();
    }

    @DexIgnore
    public void loadAfter(od.f<Date> fVar, od.a<Date, GoalTrackingSummary> aVar) {
        kd4.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        kd4.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Date) fVar.a));
        if (rk2.b((Date) fVar.a, this.mCreatedDate)) {
            Key key2 = fVar.a;
            kd4.a((Object) key2, "params.key");
            Date date = (Date) key2;
            Companion companion = Companion;
            Key key3 = fVar.a;
            kd4.a((Object) key3, "params.key");
            Date calculateNextKey = companion.calculateNextKey((Date) key3, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date l = rk2.d(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : rk2.l(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + l + ", endQueryDate=" + date);
            kd4.a((Object) l, "startQueryDate");
            aVar.a(getDataInDatabase(l, date), calculateNextKey);
            if (rk2.b(this.mStartDate, date)) {
                this.mEndDate = date;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d(TAG, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new Pair(this.mStartDate, this.mEndDate));
                this.mHelper.a(PagingRequestHelper.RequestType.AFTER, (PagingRequestHelper.b) new GoalTrackingSummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    public void loadBefore(od.f<Date> fVar, od.a<Date, GoalTrackingSummary> aVar) {
        kd4.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        kd4.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    public void loadInitial(od.e<Date> eVar, od.c<Date, GoalTrackingSummary> cVar) {
        kd4.b(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        kd4.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date l = rk2.d(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : rk2.l(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + l + ", endQueryDate=" + date);
        kd4.a((Object) l, "startQueryDate");
        cVar.a(getDataInDatabase(l, date), null, this.key.getTime());
        this.mHelper.a(PagingRequestHelper.RequestType.INITIAL, (PagingRequestHelper.b) new GoalTrackingSummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mGoalTrackingDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        kd4.b(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(PagingRequestHelper pagingRequestHelper) {
        kd4.b(pagingRequestHelper, "<set-?>");
        this.mHelper = pagingRequestHelper;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        kd4.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        kd4.b(date, "<set-?>");
        this.mStartDate = date;
    }
}
