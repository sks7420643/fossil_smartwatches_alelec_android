package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.CategoryDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideCategoryDatabaseFactory implements Factory<CategoryDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideCategoryDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideCategoryDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideCategoryDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static CategoryDatabase provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return proxyProvideCategoryDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static CategoryDatabase proxyProvideCategoryDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        CategoryDatabase provideCategoryDatabase = portfolioDatabaseModule.provideCategoryDatabase(portfolioApp);
        n44.a(provideCategoryDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideCategoryDatabase;
    }

    @DexIgnore
    public CategoryDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
