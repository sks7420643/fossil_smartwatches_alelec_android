package com.portfolio.platform.data.source;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.diana.ComplicationDao;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationRepository_Factory implements Factory<ComplicationRepository> {
    @DexIgnore
    public /* final */ Provider<ComplicationDao> mComplicationDaoProvider;
    @DexIgnore
    public /* final */ Provider<ComplicationRemoteDataSource> mComplicationRemoteDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> mPortfolioAppProvider;

    @DexIgnore
    public ComplicationRepository_Factory(Provider<ComplicationDao> provider, Provider<ComplicationRemoteDataSource> provider2, Provider<PortfolioApp> provider3) {
        this.mComplicationDaoProvider = provider;
        this.mComplicationRemoteDataSourceProvider = provider2;
        this.mPortfolioAppProvider = provider3;
    }

    @DexIgnore
    public static ComplicationRepository_Factory create(Provider<ComplicationDao> provider, Provider<ComplicationRemoteDataSource> provider2, Provider<PortfolioApp> provider3) {
        return new ComplicationRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static ComplicationRepository newComplicationRepository(ComplicationDao complicationDao, ComplicationRemoteDataSource complicationRemoteDataSource, PortfolioApp portfolioApp) {
        return new ComplicationRepository(complicationDao, complicationRemoteDataSource, portfolioApp);
    }

    @DexIgnore
    public static ComplicationRepository provideInstance(Provider<ComplicationDao> provider, Provider<ComplicationRemoteDataSource> provider2, Provider<PortfolioApp> provider3) {
        return new ComplicationRepository(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public ComplicationRepository get() {
        return provideInstance(this.mComplicationDaoProvider, this.mComplicationRemoteDataSourceProvider, this.mPortfolioAppProvider);
    }
}
