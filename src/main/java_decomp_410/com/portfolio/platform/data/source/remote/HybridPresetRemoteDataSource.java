package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.jk2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.tz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HybridPresetRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "HybridPresetRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public HybridPresetRemoteDataSource(ApiServiceV2 apiServiceV2) {
        kd4.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object deletePreset(HybridPreset hybridPreset, yb4<? super qo2<Void>> yb4) {
        HybridPresetRemoteDataSource$deletePreset$Anon1 hybridPresetRemoteDataSource$deletePreset$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof HybridPresetRemoteDataSource$deletePreset$Anon1) {
            hybridPresetRemoteDataSource$deletePreset$Anon1 = (HybridPresetRemoteDataSource$deletePreset$Anon1) yb4;
            int i2 = hybridPresetRemoteDataSource$deletePreset$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridPresetRemoteDataSource$deletePreset$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = hybridPresetRemoteDataSource$deletePreset$Anon1.result;
                Object a = cc4.a();
                i = hybridPresetRemoteDataSource$deletePreset$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    xz1 xz1 = new xz1();
                    tz1 tz1 = new tz1();
                    tz1.a(hybridPreset.getId());
                    xz1.a("_ids", (JsonElement) tz1);
                    HybridPresetRemoteDataSource$deletePreset$response$Anon1 hybridPresetRemoteDataSource$deletePreset$response$Anon1 = new HybridPresetRemoteDataSource$deletePreset$response$Anon1(this, xz1, (yb4) null);
                    hybridPresetRemoteDataSource$deletePreset$Anon1.L$Anon0 = this;
                    hybridPresetRemoteDataSource$deletePreset$Anon1.L$Anon1 = hybridPreset;
                    hybridPresetRemoteDataSource$deletePreset$Anon1.L$Anon2 = xz1;
                    hybridPresetRemoteDataSource$deletePreset$Anon1.L$Anon3 = tz1;
                    hybridPresetRemoteDataSource$deletePreset$Anon1.label = 1;
                    obj = ResponseKt.a(hybridPresetRemoteDataSource$deletePreset$response$Anon1, hybridPresetRemoteDataSource$deletePreset$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    tz1 tz12 = (tz1) hybridPresetRemoteDataSource$deletePreset$Anon1.L$Anon3;
                    xz1 xz12 = (xz1) hybridPresetRemoteDataSource$deletePreset$Anon1.L$Anon2;
                    HybridPreset hybridPreset2 = (HybridPreset) hybridPresetRemoteDataSource$deletePreset$Anon1.L$Anon1;
                    HybridPresetRemoteDataSource hybridPresetRemoteDataSource = (HybridPresetRemoteDataSource) hybridPresetRemoteDataSource$deletePreset$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    return new ro2((Object) null, false, 2, (fd4) null);
                }
                if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), (Throwable) null, (String) null, 12, (fd4) null);
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        hybridPresetRemoteDataSource$deletePreset$Anon1 = new HybridPresetRemoteDataSource$deletePreset$Anon1(this, yb4);
        Object obj2 = hybridPresetRemoteDataSource$deletePreset$Anon1.result;
        Object a2 = cc4.a();
        i = hybridPresetRemoteDataSource$deletePreset$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object downloadHybridPresetList(String str, yb4<? super qo2<ArrayList<HybridPreset>>> yb4) {
        HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1 hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1) {
            hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1 = (HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1) yb4;
            int i2 = hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.result;
                Object a = cc4.a();
                i = hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    HybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1 hybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1 = new HybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1(this, str, (yb4) null);
                    hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.L$Anon0 = this;
                    hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.L$Anon1 = str;
                    hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.label = 1;
                    obj = ResponseKt.a(hybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1, hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.L$Anon1;
                    HybridPresetRemoteDataSource hybridPresetRemoteDataSource = (HybridPresetRemoteDataSource) hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ArrayList arrayList = new ArrayList();
                    ro2 ro2 = (ro2) qo2;
                    if (!ro2.b()) {
                        Object a2 = ro2.a();
                        if (a2 != null) {
                            for (HybridPreset hybridPreset : ((ApiResponse) a2).get_items()) {
                                hybridPreset.setPinType(0);
                                hybridPreset.setSerialNumber(str);
                                arrayList.add(hybridPreset);
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    return new ro2(arrayList, ro2.b());
                } else if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), (Throwable) null, (String) null, 12, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1 = new HybridPresetRemoteDataSource$downloadHybridPresetList$Anon1(this, yb4);
        Object obj2 = hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.result;
        Object a3 = cc4.a();
        i = hybridPresetRemoteDataSource$downloadHybridPresetList$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object downloadHybridRecommendPresetList(String str, yb4<? super qo2<ArrayList<HybridRecommendPreset>>> yb4) {
        HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1 hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1) {
            hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1 = (HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1) yb4;
            int i2 = hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.result;
                Object a = cc4.a();
                i = hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "downloadHybridRecommendPresetList " + str);
                    HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1 hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1 = new HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1(this, str, (yb4) null);
                    hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.L$Anon0 = this;
                    hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.L$Anon1 = str;
                    hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.label = 1;
                    obj = ResponseKt.a(hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$response$Anon1, hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.L$Anon1;
                    HybridPresetRemoteDataSource hybridPresetRemoteDataSource = (HybridPresetRemoteDataSource) hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ArrayList arrayList = new ArrayList();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadHybridRecommendPresetList success ");
                    sb.append(arrayList);
                    sb.append(" isFromCache ");
                    ro2 ro2 = (ro2) qo2;
                    sb.append(ro2.b());
                    local2.d(TAG, sb.toString());
                    if (!ro2.b()) {
                        String t = rk2.t(new Date(System.currentTimeMillis()));
                        Object a2 = ro2.a();
                        if (a2 != null) {
                            for (HybridRecommendPreset hybridRecommendPreset : ((ApiResponse) a2).get_items()) {
                                hybridRecommendPreset.setSerialNumber(str);
                                kd4.a((Object) t, "timestamp");
                                hybridRecommendPreset.setCreatedAt(t);
                                hybridRecommendPreset.setUpdatedAt(t);
                                arrayList.add(hybridRecommendPreset);
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    return new ro2(arrayList, ro2.b());
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadHybridRecommendPresetList fail code ");
                    po2 po2 = (po2) qo2;
                    sb2.append(po2.a());
                    sb2.append(" serverError ");
                    sb2.append(po2.c());
                    local3.d(TAG, sb2.toString());
                    return new po2(po2.a(), po2.c(), (Throwable) null, (String) null, 12, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1 = new HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1(this, yb4);
        Object obj2 = hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.result;
        Object a3 = cc4.a();
        i = hybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object replaceHybridPresetList(List<HybridPreset> list, yb4<? super qo2<ArrayList<HybridPreset>>> yb4) {
        HybridPresetRemoteDataSource$replaceHybridPresetList$Anon1 hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof HybridPresetRemoteDataSource$replaceHybridPresetList$Anon1) {
            hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1 = (HybridPresetRemoteDataSource$replaceHybridPresetList$Anon1) yb4;
            int i2 = hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.result;
                Object a = cc4.a();
                i = hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    rz1 rz1 = new rz1();
                    rz1.b(new jk2());
                    Gson a2 = rz1.a();
                    xz1 xz1 = new xz1();
                    Object[] array = list.toArray(new HybridPreset[0]);
                    if (array != null) {
                        xz1.a(CloudLogWriter.ITEMS_PARAM, a2.b((Object) array));
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d(TAG, "replaceHybridPresetList jsonObject " + xz1);
                        HybridPresetRemoteDataSource$replaceHybridPresetList$response$Anon1 hybridPresetRemoteDataSource$replaceHybridPresetList$response$Anon1 = new HybridPresetRemoteDataSource$replaceHybridPresetList$response$Anon1(this, xz1, (yb4) null);
                        hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$Anon0 = this;
                        hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$Anon1 = list;
                        hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$Anon2 = a2;
                        hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$Anon3 = xz1;
                        hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.label = 1;
                        obj = ResponseKt.a(hybridPresetRemoteDataSource$replaceHybridPresetList$response$Anon1, hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                } else if (i == 1) {
                    xz1 xz12 = (xz1) hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$Anon3;
                    Gson gson = (Gson) hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$Anon2;
                    List list2 = (List) hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$Anon1;
                    HybridPresetRemoteDataSource hybridPresetRemoteDataSource = (HybridPresetRemoteDataSource) hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ArrayList arrayList = new ArrayList();
                    Object a3 = ((ro2) qo2).a();
                    if (a3 != null) {
                        for (HybridPreset hybridPreset : ((ApiResponse) a3).get_items()) {
                            hybridPreset.setPinType(0);
                            arrayList.add(hybridPreset);
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d(TAG, "replaceHybridPresetList success " + arrayList);
                        return new ro2(arrayList, false, 2, (fd4) null);
                    }
                    kd4.a();
                    throw null;
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("replaceHybridPresetList fail code ");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(" serverError ");
                    sb.append(po2.c());
                    local3.d(TAG, sb.toString());
                    return new po2(po2.a(), po2.c(), (Throwable) null, (String) null, 12, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1 = new HybridPresetRemoteDataSource$replaceHybridPresetList$Anon1(this, yb4);
        Object obj2 = hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.result;
        Object a4 = cc4.a();
        i = hybridPresetRemoteDataSource$replaceHybridPresetList$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object upsertHybridPresetList(List<HybridPreset> list, yb4<? super qo2<ArrayList<HybridPreset>>> yb4) {
        HybridPresetRemoteDataSource$upsertHybridPresetList$Anon1 hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof HybridPresetRemoteDataSource$upsertHybridPresetList$Anon1) {
            hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1 = (HybridPresetRemoteDataSource$upsertHybridPresetList$Anon1) yb4;
            int i2 = hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.result;
                Object a = cc4.a();
                i = hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    rz1 rz1 = new rz1();
                    rz1.b(new jk2());
                    Gson a2 = rz1.a();
                    xz1 xz1 = new xz1();
                    Object[] array = list.toArray(new HybridPreset[0]);
                    if (array != null) {
                        xz1.a(CloudLogWriter.ITEMS_PARAM, a2.b((Object) array));
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d(TAG, "upsertHybridPresetList jsonObject " + xz1);
                        HybridPresetRemoteDataSource$upsertHybridPresetList$response$Anon1 hybridPresetRemoteDataSource$upsertHybridPresetList$response$Anon1 = new HybridPresetRemoteDataSource$upsertHybridPresetList$response$Anon1(this, xz1, (yb4) null);
                        hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$Anon0 = this;
                        hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$Anon1 = list;
                        hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$Anon2 = a2;
                        hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$Anon3 = xz1;
                        hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.label = 1;
                        obj = ResponseKt.a(hybridPresetRemoteDataSource$upsertHybridPresetList$response$Anon1, hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                } else if (i == 1) {
                    xz1 xz12 = (xz1) hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$Anon3;
                    Gson gson = (Gson) hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$Anon2;
                    List list2 = (List) hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$Anon1;
                    HybridPresetRemoteDataSource hybridPresetRemoteDataSource = (HybridPresetRemoteDataSource) hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ArrayList arrayList = new ArrayList();
                    Object a3 = ((ro2) qo2).a();
                    if (a3 != null) {
                        for (HybridPreset hybridPreset : ((ApiResponse) a3).get_items()) {
                            hybridPreset.setPinType(0);
                            arrayList.add(hybridPreset);
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d(TAG, "upsertHybridPresetList success " + arrayList);
                        return new ro2(arrayList, false, 2, (fd4) null);
                    }
                    kd4.a();
                    throw null;
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("upsertHybridPresetList fail code ");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(" serverError ");
                    sb.append(po2.c());
                    local3.d(TAG, sb.toString());
                    return new po2(po2.a(), po2.c(), (Throwable) null, (String) null, 12, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1 = new HybridPresetRemoteDataSource$upsertHybridPresetList$Anon1(this, yb4);
        Object obj2 = hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.result;
        Object a4 = cc4.a();
        i = hybridPresetRemoteDataSource$upsertHybridPresetList$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }
}
