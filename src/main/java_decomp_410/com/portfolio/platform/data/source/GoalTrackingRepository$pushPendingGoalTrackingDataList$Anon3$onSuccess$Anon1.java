package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1", f = "GoalTrackingRepository.kt", l = {637}, m = "invokeSuspend")
public final class GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Ref$ObjectRef $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$ObjectRef $startDate;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1(GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3 goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3, Ref$ObjectRef ref$ObjectRef, Ref$ObjectRef ref$ObjectRef2, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3;
        this.$startDate = ref$ObjectRef;
        this.$endDate = ref$ObjectRef2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1 goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1 = new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1(this.this$Anon0, this.$startDate, this.$endDate, yb4);
        goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1.p$ = (zg4) obj;
        return goalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            this.L$Anon0 = this.p$;
            this.label = 1;
            if (this.this$Anon0.this$Anon0.loadSummaries((Date) this.$startDate.element, (Date) this.$endDate.element, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
