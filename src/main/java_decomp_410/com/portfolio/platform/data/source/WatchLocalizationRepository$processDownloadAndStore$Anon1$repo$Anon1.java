package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.em4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1", f = "WatchLocalizationRepository.kt", l = {66}, m = "invokeSuspend")
public final class WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<em4>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WatchLocalizationRepository$processDownloadAndStore$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1(WatchLocalizationRepository$processDownloadAndStore$Anon1 watchLocalizationRepository$processDownloadAndStore$Anon1, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = watchLocalizationRepository$processDownloadAndStore$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1(this.this$Anon0, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            ApiServiceV2 access$getApi$p = this.this$Anon0.this$Anon0.api;
            String str = this.this$Anon0.$url;
            this.label = 1;
            obj = access$getApi$p.downloadFile(str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
