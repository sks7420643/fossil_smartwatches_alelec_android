package com.portfolio.platform.data.source.local.reminders;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.RemindTimeModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface RemindTimeDao {
    @DexIgnore
    void delete();

    @DexIgnore
    LiveData<RemindTimeModel> getRemindTime();

    @DexIgnore
    RemindTimeModel getRemindTimeModel();

    @DexIgnore
    void upsertRemindTimeModel(RemindTimeModel remindTimeModel);
}
