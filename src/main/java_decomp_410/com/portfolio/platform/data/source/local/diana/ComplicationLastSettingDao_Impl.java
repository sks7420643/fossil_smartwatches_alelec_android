package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.ComplicationLastSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationLastSettingDao_Impl implements ComplicationLastSettingDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfComplicationLastSetting;
    @DexIgnore
    public /* final */ wf __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeleteComplicationLastSettingByComplicationId;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<ComplicationLastSetting> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `complicationLastSetting`(`complicationId`,`updatedAt`,`setting`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, ComplicationLastSetting complicationLastSetting) {
            if (complicationLastSetting.getComplicationId() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, complicationLastSetting.getComplicationId());
            }
            if (complicationLastSetting.getUpdatedAt() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, complicationLastSetting.getUpdatedAt());
            }
            if (complicationLastSetting.getSetting() == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, complicationLastSetting.getSetting());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends wf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM complicationLastSetting WHERE complicationId=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends wf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM complicationLastSetting";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<ComplicationLastSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon4(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<ComplicationLastSetting> call() throws Exception {
            Cursor a = bg.a(ComplicationLastSettingDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "complicationId");
                int b2 = ag.b(a, "updatedAt");
                int b3 = ag.b(a, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new ComplicationLastSetting(a.getString(b), a.getString(b2), a.getString(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public ComplicationLastSettingDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfComplicationLastSetting = new Anon1(roomDatabase);
        this.__preparedStmtOfDeleteComplicationLastSettingByComplicationId = new Anon2(roomDatabase);
        this.__preparedStmtOfCleanUp = new Anon3(roomDatabase);
    }

    @DexIgnore
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    public void deleteComplicationLastSettingByComplicationId(String str) {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeleteComplicationLastSettingByComplicationId.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteComplicationLastSettingByComplicationId.release(acquire);
        }
    }

    @DexIgnore
    public List<ComplicationLastSetting> getAllComplicationLastSetting() {
        uf b = uf.b("SELECT * FROM complicationLastSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "complicationId");
            int b3 = ag.b(a, "updatedAt");
            int b4 = ag.b(a, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new ComplicationLastSetting(a.getString(b2), a.getString(b3), a.getString(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<ComplicationLastSetting>> getAllComplicationLastSettingAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"complicationLastSetting"}, false, new Anon4(uf.b("SELECT * FROM complicationLastSetting", 0)));
    }

    @DexIgnore
    public ComplicationLastSetting getComplicationLastSetting(String str) {
        uf b = uf.b("SELECT * FROM complicationLastSetting WHERE complicationId=? ", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            return a.moveToFirst() ? new ComplicationLastSetting(a.getString(ag.b(a, "complicationId")), a.getString(ag.b(a, "updatedAt")), a.getString(ag.b(a, MicroAppSetting.SETTING))) : null;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertComplicationLastSetting(ComplicationLastSetting complicationLastSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfComplicationLastSetting.insert(complicationLastSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
