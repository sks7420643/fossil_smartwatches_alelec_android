package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface AlarmsDataSource {
    @DexIgnore
    void cleanUp();

    @DexIgnore
    Object deleteAlarm(Alarm alarm, yb4<? super qo2<Alarm>> yb4);

    @DexIgnore
    Object findAlarm(String str, yb4<? super Alarm> yb4);

    @DexIgnore
    Object getActiveAlarms(yb4<? super List<Alarm>> yb4);

    @DexIgnore
    Object getAlarms(yb4<? super qo2<List<Alarm>>> yb4);

    @DexIgnore
    Object getAllAlarmIgnorePinType(yb4<? super List<Alarm>> yb4);

    @DexIgnore
    Object setAlarm(Alarm alarm, yb4<? super qo2<Alarm>> yb4);
}
