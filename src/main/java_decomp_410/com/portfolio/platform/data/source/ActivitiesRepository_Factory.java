package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.xk2;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySampleDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.fitness.SampleRawDao;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitiesRepository_Factory implements Factory<ActivitiesRepository> {
    @DexIgnore
    public /* final */ Provider<ActivitySampleDao> mActivitySampleDaoProvider;
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;
    @DexIgnore
    public /* final */ Provider<FitnessDataDao> mFitnessDataDaoProvider;
    @DexIgnore
    public /* final */ Provider<FitnessDatabase> mFitnessDatabaseProvider;
    @DexIgnore
    public /* final */ Provider<xk2> mFitnessHelperProvider;
    @DexIgnore
    public /* final */ Provider<SampleRawDao> mSampleRawDaoProvider;
    @DexIgnore
    public /* final */ Provider<UserRepository> mUserRepositoryProvider;

    @DexIgnore
    public ActivitiesRepository_Factory(Provider<ApiServiceV2> provider, Provider<SampleRawDao> provider2, Provider<ActivitySampleDao> provider3, Provider<FitnessDatabase> provider4, Provider<FitnessDataDao> provider5, Provider<UserRepository> provider6, Provider<xk2> provider7) {
        this.mApiServiceProvider = provider;
        this.mSampleRawDaoProvider = provider2;
        this.mActivitySampleDaoProvider = provider3;
        this.mFitnessDatabaseProvider = provider4;
        this.mFitnessDataDaoProvider = provider5;
        this.mUserRepositoryProvider = provider6;
        this.mFitnessHelperProvider = provider7;
    }

    @DexIgnore
    public static ActivitiesRepository_Factory create(Provider<ApiServiceV2> provider, Provider<SampleRawDao> provider2, Provider<ActivitySampleDao> provider3, Provider<FitnessDatabase> provider4, Provider<FitnessDataDao> provider5, Provider<UserRepository> provider6, Provider<xk2> provider7) {
        return new ActivitiesRepository_Factory(provider, provider2, provider3, provider4, provider5, provider6, provider7);
    }

    @DexIgnore
    public static ActivitiesRepository newActivitiesRepository(ApiServiceV2 apiServiceV2, SampleRawDao sampleRawDao, ActivitySampleDao activitySampleDao, FitnessDatabase fitnessDatabase, FitnessDataDao fitnessDataDao, UserRepository userRepository, xk2 xk2) {
        return new ActivitiesRepository(apiServiceV2, sampleRawDao, activitySampleDao, fitnessDatabase, fitnessDataDao, userRepository, xk2);
    }

    @DexIgnore
    public static ActivitiesRepository provideInstance(Provider<ApiServiceV2> provider, Provider<SampleRawDao> provider2, Provider<ActivitySampleDao> provider3, Provider<FitnessDatabase> provider4, Provider<FitnessDataDao> provider5, Provider<UserRepository> provider6, Provider<xk2> provider7) {
        return new ActivitiesRepository(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get(), provider7.get());
    }

    @DexIgnore
    public ActivitiesRepository get() {
        return provideInstance(this.mApiServiceProvider, this.mSampleRawDaoProvider, this.mActivitySampleDaoProvider, this.mFitnessDatabaseProvider, this.mFitnessDataDaoProvider, this.mUserRepositoryProvider, this.mFitnessHelperProvider);
    }
}
