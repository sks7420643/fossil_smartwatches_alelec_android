package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppLastSettingDao_Impl implements WatchAppLastSettingDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfWatchAppLastSetting;
    @DexIgnore
    public /* final */ wf __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeleteWatchAppLastSettingById;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<WatchAppLastSetting> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchAppLastSetting`(`watchAppId`,`updatedAt`,`setting`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, WatchAppLastSetting watchAppLastSetting) {
            if (watchAppLastSetting.getWatchAppId() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, watchAppLastSetting.getWatchAppId());
            }
            if (watchAppLastSetting.getUpdatedAt() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, watchAppLastSetting.getUpdatedAt());
            }
            if (watchAppLastSetting.getSetting() == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, watchAppLastSetting.getSetting());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends wf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM watchAppLastSetting WHERE watchAppId=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends wf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM watchAppLastSetting";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<WatchAppLastSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon4(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<WatchAppLastSetting> call() throws Exception {
            Cursor a = bg.a(WatchAppLastSettingDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "watchAppId");
                int b2 = ag.b(a, "updatedAt");
                int b3 = ag.b(a, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new WatchAppLastSetting(a.getString(b), a.getString(b2), a.getString(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public WatchAppLastSettingDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfWatchAppLastSetting = new Anon1(roomDatabase);
        this.__preparedStmtOfDeleteWatchAppLastSettingById = new Anon2(roomDatabase);
        this.__preparedStmtOfCleanUp = new Anon3(roomDatabase);
    }

    @DexIgnore
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    public void deleteWatchAppLastSettingById(String str) {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeleteWatchAppLastSettingById.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchAppLastSettingById.release(acquire);
        }
    }

    @DexIgnore
    public List<WatchAppLastSetting> getAllWatchAppLastSetting() {
        uf b = uf.b("SELECT * FROM watchAppLastSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "watchAppId");
            int b3 = ag.b(a, "updatedAt");
            int b4 = ag.b(a, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchAppLastSetting(a.getString(b2), a.getString(b3), a.getString(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<WatchAppLastSetting>> getAllWatchAppLastSettingAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"watchAppLastSetting"}, false, new Anon4(uf.b("SELECT * FROM watchAppLastSetting", 0)));
    }

    @DexIgnore
    public WatchAppLastSetting getWatchAppLastSetting(String str) {
        uf b = uf.b("SELECT * FROM watchAppLastSetting WHERE watchAppId=? ", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            return a.moveToFirst() ? new WatchAppLastSetting(a.getString(ag.b(a, "watchAppId")), a.getString(ag.b(a, "updatedAt")), a.getString(ag.b(a, MicroAppSetting.SETTING))) : null;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertWatchAppLastSetting(WatchAppLastSetting watchAppLastSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchAppLastSetting.insert(watchAppLastSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
