package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.Date;
import java.util.List;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository$getSleepSummaries$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ String $end;
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ String $start;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends NetworkBoundResource<List<MFSleepDay>, xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ Pair $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository$getSleepSummaries$Anon1 this$Anon0;

        @DexIgnore
        public Anon1(SleepSummariesRepository$getSleepSummaries$Anon1 sleepSummariesRepository$getSleepSummaries$Anon1, Pair pair) {
            this.this$Anon0 = sleepSummariesRepository$getSleepSummaries$Anon1;
            this.$downloadingDate = pair;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
            if (r0 != null) goto L_0x0019;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
            if (r0 != null) goto L_0x0033;
         */
        @DexIgnore
        public Object createCall(yb4<? super qr4<xz1>> yb4) {
            Date date;
            Date date2;
            ApiServiceV2 access$getMApiService$p = this.this$Anon0.this$Anon0.mApiService;
            Pair pair = this.$downloadingDate;
            if (pair != null) {
                date = (Date) pair.getFirst();
            }
            date = this.this$Anon0.$startDate;
            String e = rk2.e(date);
            kd4.a((Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            Pair pair2 = this.$downloadingDate;
            if (pair2 != null) {
                date2 = (Date) pair2.getSecond();
            }
            date2 = this.this$Anon0.$endDate;
            String e2 = rk2.e(date2);
            kd4.a((Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiService$p.getSleepSummaries(e, e2, 0, 100, yb4);
        }

        @DexIgnore
        public LiveData<List<MFSleepDay>> loadFromDb() {
            SleepDao access$getMSleepDao$p = this.this$Anon0.this$Anon0.mSleepDao;
            String str = this.this$Anon0.$start;
            kd4.a((Object) str, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
            String str2 = this.this$Anon0.$end;
            kd4.a((Object) str2, "end");
            return access$getMSleepDao$p.getSleepDaysLiveData(str, str2);
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().e(SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "getActivityList onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(xz1 xz1) {
            kd4.b(xz1, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getSleepSummaries saveCallResult onResponse: response = " + xz1);
            SleepSummariesRepository$getSleepSummaries$Anon1 sleepSummariesRepository$getSleepSummaries$Anon1 = this.this$Anon0;
            sleepSummariesRepository$getSleepSummaries$Anon1.this$Anon0.saveSleepSummaries$app_fossilRelease(xz1, sleepSummariesRepository$getSleepSummaries$Anon1.$startDate, sleepSummariesRepository$getSleepSummaries$Anon1.$endDate, this.$downloadingDate);
        }

        @DexIgnore
        public boolean shouldFetch(List<MFSleepDay> list) {
            return this.this$Anon0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public SleepSummariesRepository$getSleepSummaries$Anon1(SleepSummariesRepository sleepSummariesRepository, Date date, Date date2, boolean z, String str, String str2) {
        this.this$Anon0 = sleepSummariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
        this.$start = str;
        this.$end = str2;
    }

    @DexIgnore
    public final LiveData<os3<List<MFSleepDay>>> apply(List<FitnessDataWrapper> list) {
        kd4.a((Object) list, "fitnessDataList");
        return new Anon1(this, FitnessDataWrapperKt.calculateRangeDownload(list, this.$startDate, this.$endDate)).asLiveData();
    }
}
