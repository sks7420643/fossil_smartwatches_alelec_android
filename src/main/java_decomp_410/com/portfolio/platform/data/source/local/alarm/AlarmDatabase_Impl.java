package com.portfolio.platform.data.source.local.alarm;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmDatabase_Impl extends AlarmDatabase {
    @DexIgnore
    public volatile AlarmDao _alarmDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `alarm` (`id` TEXT, `uri` TEXT NOT NULL, `title` TEXT NOT NULL, `hour` INTEGER NOT NULL, `minute` INTEGER NOT NULL, `days` TEXT, `isActive` INTEGER NOT NULL, `isRepeated` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`uri`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '14bf1872d7012ddb8a9749aba4e0984f')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `alarm`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (AlarmDatabase_Impl.this.mCallbacks != null) {
                int size = AlarmDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) AlarmDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = AlarmDatabase_Impl.this.mDatabase = ggVar;
            AlarmDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (AlarmDatabase_Impl.this.mCallbacks != null) {
                int size = AlarmDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) AlarmDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            HashMap hashMap = new HashMap(11);
            hashMap.put("id", new eg.a("id", "TEXT", false, 0));
            hashMap.put("uri", new eg.a("uri", "TEXT", true, 1));
            hashMap.put("title", new eg.a("title", "TEXT", true, 0));
            hashMap.put(AppFilter.COLUMN_HOUR, new eg.a(AppFilter.COLUMN_HOUR, "INTEGER", true, 0));
            hashMap.put(MFSleepGoal.COLUMN_MINUTE, new eg.a(MFSleepGoal.COLUMN_MINUTE, "INTEGER", true, 0));
            hashMap.put(Alarm.COLUMN_DAYS, new eg.a(Alarm.COLUMN_DAYS, "TEXT", false, 0));
            hashMap.put("isActive", new eg.a("isActive", "INTEGER", true, 0));
            hashMap.put("isRepeated", new eg.a("isRepeated", "INTEGER", true, 0));
            hashMap.put("createdAt", new eg.a("createdAt", "TEXT", false, 0));
            hashMap.put("updatedAt", new eg.a("updatedAt", "TEXT", true, 0));
            hashMap.put("pinType", new eg.a("pinType", "INTEGER", true, 0));
            eg egVar = new eg(Alarm.TABLE_NAME, hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar, Alarm.TABLE_NAME);
            if (!egVar.equals(a)) {
                throw new IllegalStateException("Migration didn't properly handle alarm(com.portfolio.platform.data.source.local.alarm.Alarm).\n Expected:\n" + egVar + "\n Found:\n" + a);
            }
        }
    }

    @DexIgnore
    public AlarmDao alarmDao() {
        AlarmDao alarmDao;
        if (this._alarmDao != null) {
            return this._alarmDao;
        }
        synchronized (this) {
            if (this._alarmDao == null) {
                this._alarmDao = new AlarmDao_Impl(this);
            }
            alarmDao = this._alarmDao;
        }
        return alarmDao;
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `alarm`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), Alarm.TABLE_NAME);
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(5), "14bf1872d7012ddb8a9749aba4e0984f", "49074030507e5c41c4f79c122790dd1c");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }
}
