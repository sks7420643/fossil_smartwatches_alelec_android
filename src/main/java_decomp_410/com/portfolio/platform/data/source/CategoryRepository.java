package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CategoryRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "CategoryRepository";
    @DexIgnore
    public /* final */ CategoryDao mCategoryDao;
    @DexIgnore
    public /* final */ CategoryRemoteDataSource mCategoryRemoteDataSource;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public CategoryRepository(CategoryDao categoryDao, CategoryRemoteDataSource categoryRemoteDataSource) {
        kd4.b(categoryDao, "mCategoryDao");
        kd4.b(categoryRemoteDataSource, "mCategoryRemoteDataSource");
        this.mCategoryDao = categoryDao;
        this.mCategoryRemoteDataSource = categoryRemoteDataSource;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object downloadCategories(yb4<? super qa4> yb4) {
        CategoryRepository$downloadCategories$Anon1 categoryRepository$downloadCategories$Anon1;
        int i;
        CategoryRepository categoryRepository;
        qo2 qo2;
        if (yb4 instanceof CategoryRepository$downloadCategories$Anon1) {
            categoryRepository$downloadCategories$Anon1 = (CategoryRepository$downloadCategories$Anon1) yb4;
            int i2 = categoryRepository$downloadCategories$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                categoryRepository$downloadCategories$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = categoryRepository$downloadCategories$Anon1.result;
                Object a = cc4.a();
                i = categoryRepository$downloadCategories$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "downloadCategories ");
                    CategoryRemoteDataSource categoryRemoteDataSource = this.mCategoryRemoteDataSource;
                    categoryRepository$downloadCategories$Anon1.L$Anon0 = this;
                    categoryRepository$downloadCategories$Anon1.label = 1;
                    obj = categoryRemoteDataSource.getAllCategory(categoryRepository$downloadCategories$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    categoryRepository = this;
                } else if (i == 1) {
                    categoryRepository = (CategoryRepository) categoryRepository$downloadCategories$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                Integer num = null;
                if (!(qo2 instanceof ro2)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadCategories success isFromCache ");
                    ro2 ro2 = (ro2) qo2;
                    sb.append(ro2.b());
                    sb.append(" response ");
                    sb.append((List) ro2.a());
                    local.d(TAG, sb.toString());
                    if (!ro2.b()) {
                        Object a2 = ro2.a();
                        if (a2 == null) {
                            kd4.a();
                            throw null;
                        } else if (!((Collection) a2).isEmpty()) {
                            categoryRepository.mCategoryDao.upsertCategoryList((List) ro2.a());
                        }
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadCategories fail!!! error=");
                    po2 po2 = (po2) qo2;
                    sb2.append(po2.a());
                    sb2.append(" serverErrorCode=");
                    ServerError c = po2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local2.d(TAG, sb2.toString());
                }
                return qa4.a;
            }
        }
        categoryRepository$downloadCategories$Anon1 = new CategoryRepository$downloadCategories$Anon1(this, yb4);
        Object obj2 = categoryRepository$downloadCategories$Anon1.result;
        Object a3 = cc4.a();
        i = categoryRepository$downloadCategories$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        Integer num2 = null;
        if (!(qo2 instanceof ro2)) {
        }
        return qa4.a;
    }

    @DexIgnore
    public final List<Category> getAllCategories() {
        return this.mCategoryDao.getAllCategory();
    }
}
