package com.portfolio.platform.data.source.local;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CategoryDatabase_Impl extends CategoryDatabase {
    @DexIgnore
    public volatile CategoryDao _categoryDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `category` (`id` TEXT NOT NULL, `englishName` TEXT NOT NULL, `name` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `priority` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'deca3c3756d2b01ba1167f57e573ed39')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `category`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (CategoryDatabase_Impl.this.mCallbacks != null) {
                int size = CategoryDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) CategoryDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = CategoryDatabase_Impl.this.mDatabase = ggVar;
            CategoryDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (CategoryDatabase_Impl.this.mCallbacks != null) {
                int size = CategoryDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) CategoryDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            HashMap hashMap = new HashMap(6);
            hashMap.put("id", new eg.a("id", "TEXT", true, 1));
            hashMap.put("englishName", new eg.a("englishName", "TEXT", true, 0));
            hashMap.put("name", new eg.a("name", "TEXT", true, 0));
            hashMap.put("updatedAt", new eg.a("updatedAt", "TEXT", true, 0));
            hashMap.put("createdAt", new eg.a("createdAt", "TEXT", true, 0));
            hashMap.put("priority", new eg.a("priority", "INTEGER", true, 0));
            eg egVar = new eg("category", hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar, "category");
            if (!egVar.equals(a)) {
                throw new IllegalStateException("Migration didn't properly handle category(com.portfolio.platform.data.model.Category).\n Expected:\n" + egVar + "\n Found:\n" + a);
            }
        }
    }

    @DexIgnore
    public CategoryDao categoryDao() {
        CategoryDao categoryDao;
        if (this._categoryDao != null) {
            return this._categoryDao;
        }
        synchronized (this) {
            if (this._categoryDao == null) {
                this._categoryDao = new CategoryDao_Impl(this);
            }
            categoryDao = this._categoryDao;
        }
        return categoryDao;
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `category`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), "category");
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(2), "deca3c3756d2b01ba1167f57e573ed39", "e8ca7dfeb46b68975e9271e7b97e5f11");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }
}
