package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.SleepSummariesRepository$fetchSleepSummaries$response$Anon1", f = "SleepSummariesRepository.kt", l = {234}, m = "invokeSuspend")
public final class SleepSummariesRepository$fetchSleepSummaries$response$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<xz1>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$fetchSleepSummaries$response$Anon1(SleepSummariesRepository sleepSummariesRepository, Date date, Date date2, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = sleepSummariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new SleepSummariesRepository$fetchSleepSummaries$response$Anon1(this.this$Anon0, this.$startDate, this.$endDate, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((SleepSummariesRepository$fetchSleepSummaries$response$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            ApiServiceV2 access$getMApiService$p = this.this$Anon0.mApiService;
            String e = rk2.e(this.$startDate);
            kd4.a((Object) e, "DateHelper.formatShortDate(startDate)");
            String e2 = rk2.e(this.$endDate);
            kd4.a((Object) e2, "DateHelper.formatShortDate(endDate)");
            this.label = 1;
            obj = access$getMApiService$p.getSleepSummaries(e, e2, 0, 100, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
