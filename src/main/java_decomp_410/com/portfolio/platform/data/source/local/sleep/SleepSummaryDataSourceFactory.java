package com.portfolio.platform.data.source.local.sleep;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ld;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummaryDataSourceFactory extends ld.b<Date, SleepSummary> {
    @DexIgnore
    public SleepSummaryLocalDataSource localSource;
    @DexIgnore
    public /* final */ h42 mAppExecutors;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ PagingRequestHelper.a mListener;
    @DexIgnore
    public /* final */ SleepDao mSleepDao;
    @DexIgnore
    public /* final */ SleepDatabase mSleepDatabase;
    @DexIgnore
    public /* final */ SleepSessionsRepository mSleepSessionsRepository;
    @DexIgnore
    public /* final */ SleepSummariesRepository mSleepSummariesRepository;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<SleepSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public SleepSummaryDataSourceFactory(SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDao sleepDao, SleepDatabase sleepDatabase, Date date, h42 h42, PagingRequestHelper.a aVar, Calendar calendar) {
        kd4.b(sleepSummariesRepository, "mSleepSummariesRepository");
        kd4.b(sleepSessionsRepository, "mSleepSessionsRepository");
        kd4.b(fitnessDataRepository, "mFitnessDataRepository");
        kd4.b(sleepDao, "mSleepDao");
        kd4.b(sleepDatabase, "mSleepDatabase");
        kd4.b(date, "mCreatedDate");
        kd4.b(h42, "mAppExecutors");
        kd4.b(aVar, "mListener");
        kd4.b(calendar, "mStartCalendar");
        this.mSleepSummariesRepository = sleepSummariesRepository;
        this.mSleepSessionsRepository = sleepSessionsRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mSleepDao = sleepDao;
        this.mSleepDatabase = sleepDatabase;
        this.mCreatedDate = date;
        this.mAppExecutors = h42;
        this.mListener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    public ld<Date, SleepSummary> create() {
        this.localSource = new SleepSummaryLocalDataSource(this.mSleepSummariesRepository, this.mSleepSessionsRepository, this.mFitnessDataRepository, this.mSleepDao, this.mSleepDatabase, this.mCreatedDate, this.mAppExecutors, this.mListener, this.mStartCalendar);
        this.sourceLiveData.a(this.localSource);
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource = this.localSource;
        if (sleepSummaryLocalDataSource != null) {
            return sleepSummaryLocalDataSource;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final SleepSummaryLocalDataSource getLocalSource() {
        return this.localSource;
    }

    @DexIgnore
    public final MutableLiveData<SleepSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalSource(SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        this.localSource = sleepSummaryLocalDataSource;
    }
}
