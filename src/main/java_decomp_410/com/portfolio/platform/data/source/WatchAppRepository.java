package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.sm2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.source.local.diana.WatchAppDao;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppRepository {
    @DexIgnore
    public /* final */ PortfolioApp mPortfolioApp;
    @DexIgnore
    public /* final */ WatchAppDao mWatchAppDao;
    @DexIgnore
    public /* final */ WatchAppRemoteDataSource mWatchAppRemoteDataSource;

    @DexIgnore
    public WatchAppRepository(WatchAppDao watchAppDao, WatchAppRemoteDataSource watchAppRemoteDataSource, PortfolioApp portfolioApp) {
        kd4.b(watchAppDao, "mWatchAppDao");
        kd4.b(watchAppRemoteDataSource, "mWatchAppRemoteDataSource");
        kd4.b(portfolioApp, "mPortfolioApp");
        this.mWatchAppDao = watchAppDao;
        this.mWatchAppRemoteDataSource = watchAppRemoteDataSource;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mWatchAppDao.clearAll();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object downloadWatchApp(String str, yb4<? super qa4> yb4) {
        WatchAppRepository$downloadWatchApp$Anon1 watchAppRepository$downloadWatchApp$Anon1;
        int i;
        WatchAppRepository watchAppRepository;
        qo2 qo2;
        if (yb4 instanceof WatchAppRepository$downloadWatchApp$Anon1) {
            watchAppRepository$downloadWatchApp$Anon1 = (WatchAppRepository$downloadWatchApp$Anon1) yb4;
            int i2 = watchAppRepository$downloadWatchApp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                watchAppRepository$downloadWatchApp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = watchAppRepository$downloadWatchApp$Anon1.result;
                Object a = cc4.a();
                i = watchAppRepository$downloadWatchApp$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(ComplicationRepository.TAG, "downloadWatchApp of " + str);
                    WatchAppRemoteDataSource watchAppRemoteDataSource = this.mWatchAppRemoteDataSource;
                    watchAppRepository$downloadWatchApp$Anon1.L$Anon0 = this;
                    watchAppRepository$downloadWatchApp$Anon1.L$Anon1 = str;
                    watchAppRepository$downloadWatchApp$Anon1.label = 1;
                    obj = watchAppRemoteDataSource.getAllWatchApp(str, watchAppRepository$downloadWatchApp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    watchAppRepository = this;
                } else if (i == 1) {
                    str = (String) watchAppRepository$downloadWatchApp$Anon1.L$Anon1;
                    watchAppRepository = (WatchAppRepository) watchAppRepository$downloadWatchApp$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                Integer num = null;
                if (!(qo2 instanceof ro2)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("downloadWatchApp of ");
                    sb.append(str);
                    sb.append(" success isFromCache ");
                    ro2 ro2 = (ro2) qo2;
                    sb.append(ro2.b());
                    sb.append(" response ");
                    sb.append((List) ro2.a());
                    local2.d(ComplicationRepository.TAG, sb.toString());
                    if (!ro2.b()) {
                        Object a2 = ro2.a();
                        if (a2 == null) {
                            kd4.a();
                            throw null;
                        } else if (!((Collection) a2).isEmpty()) {
                            watchAppRepository.mWatchAppDao.upsertWatchAppList((List) ro2.a());
                        }
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("downloadWatchApp of ");
                    sb2.append(str);
                    sb2.append(" fail!!! error=");
                    po2 po2 = (po2) qo2;
                    sb2.append(po2.a());
                    sb2.append(" serverErrorCode=");
                    ServerError c = po2.c();
                    if (c != null) {
                        num = c.getCode();
                    }
                    sb2.append(num);
                    local3.d(ComplicationRepository.TAG, sb2.toString());
                }
                return qa4.a;
            }
        }
        watchAppRepository$downloadWatchApp$Anon1 = new WatchAppRepository$downloadWatchApp$Anon1(this, yb4);
        Object obj2 = watchAppRepository$downloadWatchApp$Anon1.result;
        Object a3 = cc4.a();
        i = watchAppRepository$downloadWatchApp$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        Integer num2 = null;
        if (!(qo2 instanceof ro2)) {
        }
        return qa4.a;
    }

    @DexIgnore
    public final List<WatchApp> getAllWatchAppRaw() {
        return this.mWatchAppDao.getAllWatchApp();
    }

    @DexIgnore
    public final List<WatchApp> getWatchAppByIds(List<String> list) {
        throw null;
        // kd4.b(list, "ids");
        // if (!list.isEmpty()) {
        //     return kb4.a(this.mWatchAppDao.getWatchAppByIds(list), new WatchAppRepository$getWatchAppByIds$$inlined$sortedBy$Anon1(list));
        // }
        // return new ArrayList();
    }

    @DexIgnore
    public final List<WatchApp> queryWatchAppByName(String str) {
        kd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        ArrayList arrayList = new ArrayList();
        for (WatchApp next : this.mWatchAppDao.getAllWatchApp()) {
            String normalize = Normalizer.normalize(sm2.a(this.mPortfolioApp, next.getNameKey(), next.getName()), Normalizer.Form.NFC);
            String normalize2 = Normalizer.normalize(str, Normalizer.Form.NFC);
            kd4.a((Object) normalize, "name");
            kd4.a((Object) normalize2, "searchQuery");
            if (StringsKt__StringsKt.a((CharSequence) normalize, (CharSequence) normalize2, true)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }
}
