package com.portfolio.platform.data.source.local.sleep;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.a72;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.b72;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.dg;
import com.fossil.blesdk.obfuscated.g4;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.l72;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.r72;
import com.fossil.blesdk.obfuscated.s72;
import com.fossil.blesdk.obfuscated.t72;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepDao_Impl extends SleepDao {
    @DexIgnore
    public /* final */ a72 __dateShortStringConverter; // = new a72();
    @DexIgnore
    public /* final */ b72 __dateTimeConverter; // = new b72();
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfMFSleepDay;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfMFSleepSession;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfMFSleepSession_1;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfMFSleepSettings;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfSleepRecommendedGoal;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfSleepStatistic;
    @DexIgnore
    public /* final */ l72 __integerArrayConverter; // = new l72();
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeleteAllSleepDays;
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeleteAllSleepSessions;
    @DexIgnore
    public /* final */ wf __preparedStmtOfUpdateSleepSettings;
    @DexIgnore
    public /* final */ r72 __sleepDistributionConverter; // = new r72();
    @DexIgnore
    public /* final */ s72 __sleepSessionHeartRateConverter; // = new s72();
    @DexIgnore
    public /* final */ t72 __sleepStatisticConverter; // = new t72();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<MFSleepSession> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `sleep_session`(`pinType`,`date`,`day`,`deviceSerialNumber`,`syncTime`,`bookmarkTime`,`normalizedSleepQuality`,`source`,`realStartTime`,`realEndTime`,`realSleepMinutes`,`realSleepStateDistInMinute`,`editedStartTime`,`editedEndTime`,`editedSleepMinutes`,`editedSleepStateDistInMinute`,`sleepStates`,`heartRate`,`createdAt`,`updatedAt`,`timezoneOffset`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, MFSleepSession mFSleepSession) {
            kgVar.b(1, (long) mFSleepSession.getPinType());
            kgVar.b(2, mFSleepSession.getDate());
            String a = SleepDao_Impl.this.__dateShortStringConverter.a(mFSleepSession.getDay());
            if (a == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, a);
            }
            if (mFSleepSession.getDeviceSerialNumber() == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, mFSleepSession.getDeviceSerialNumber());
            }
            if (mFSleepSession.getSyncTime() == null) {
                kgVar.a(5);
            } else {
                kgVar.b(5, (long) mFSleepSession.getSyncTime().intValue());
            }
            if (mFSleepSession.getBookmarkTime() == null) {
                kgVar.a(6);
            } else {
                kgVar.b(6, (long) mFSleepSession.getBookmarkTime().intValue());
            }
            kgVar.a(7, mFSleepSession.getNormalizedSleepQuality());
            kgVar.b(8, (long) mFSleepSession.getSource());
            kgVar.b(9, (long) mFSleepSession.getRealStartTime());
            kgVar.b(10, (long) mFSleepSession.getRealEndTime());
            kgVar.b(11, (long) mFSleepSession.getRealSleepMinutes());
            String a2 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepSession.getRealSleepStateDistInMinute());
            if (a2 == null) {
                kgVar.a(12);
            } else {
                kgVar.a(12, a2);
            }
            if (mFSleepSession.getEditedStartTime() == null) {
                kgVar.a(13);
            } else {
                kgVar.b(13, (long) mFSleepSession.getEditedStartTime().intValue());
            }
            if (mFSleepSession.getEditedEndTime() == null) {
                kgVar.a(14);
            } else {
                kgVar.b(14, (long) mFSleepSession.getEditedEndTime().intValue());
            }
            if (mFSleepSession.getEditedSleepMinutes() == null) {
                kgVar.a(15);
            } else {
                kgVar.b(15, (long) mFSleepSession.getEditedSleepMinutes().intValue());
            }
            String a3 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepSession.getEditedSleepStateDistInMinute());
            if (a3 == null) {
                kgVar.a(16);
            } else {
                kgVar.a(16, a3);
            }
            if (mFSleepSession.getSleepStates() == null) {
                kgVar.a(17);
            } else {
                kgVar.a(17, mFSleepSession.getSleepStates());
            }
            String a4 = SleepDao_Impl.this.__sleepSessionHeartRateConverter.a(mFSleepSession.getHeartRate());
            if (a4 == null) {
                kgVar.a(18);
            } else {
                kgVar.a(18, a4);
            }
            kgVar.b(19, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepSession.getCreatedAt()));
            kgVar.b(20, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepSession.getUpdatedAt()));
            kgVar.b(21, (long) mFSleepSession.getTimezoneOffset());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<List<MFSleepSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon10(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<MFSleepSession> call() throws Exception {
            Integer num;
            Integer num2;
            Integer num3;
            int i;
            Integer num4;
            int i2;
            Integer num5;
            int i3;
            int i4;
            Anon10 anon10 = this;
            Cursor a = bg.a(SleepDao_Impl.this.__db, anon10.val$_statement, false);
            try {
                int b = ag.b(a, "pinType");
                int b2 = ag.b(a, "date");
                int b3 = ag.b(a, "day");
                int b4 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
                int b5 = ag.b(a, "syncTime");
                int b6 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
                int b7 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
                int b8 = ag.b(a, "source");
                int b9 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
                int b10 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
                int b11 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
                int b12 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
                int b13 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
                int b14 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int i5 = b;
                int b15 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int b16 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int b17 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int b18 = ag.b(a, "heartRate");
                int b19 = ag.b(a, "createdAt");
                int b20 = ag.b(a, "updatedAt");
                int b21 = ag.b(a, "timezoneOffset");
                int i6 = b14;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j = a.getLong(b2);
                    int i7 = b2;
                    Date a2 = SleepDao_Impl.this.__dateShortStringConverter.a(a.getString(b3));
                    String string = a.getString(b4);
                    if (a.isNull(b5)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b5));
                    }
                    if (a.isNull(b6)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b6));
                    }
                    double d = a.getDouble(b7);
                    int i8 = a.getInt(b8);
                    int i9 = a.getInt(b9);
                    int i10 = a.getInt(b10);
                    int i11 = a.getInt(b11);
                    SleepDistribution a3 = SleepDao_Impl.this.__sleepDistributionConverter.a(a.getString(b12));
                    if (a.isNull(b13)) {
                        i = i6;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b13));
                        i = i6;
                    }
                    if (a.isNull(i)) {
                        i2 = b15;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b15;
                    }
                    if (a.isNull(i2)) {
                        i6 = i;
                        i3 = b3;
                        i4 = b16;
                        num5 = null;
                    } else {
                        i6 = i;
                        num5 = Integer.valueOf(a.getInt(i2));
                        i4 = b16;
                        i3 = b3;
                    }
                    b16 = i4;
                    SleepDistribution a4 = SleepDao_Impl.this.__sleepDistributionConverter.a(a.getString(i4));
                    int i12 = b17;
                    String string2 = a.getString(i12);
                    b17 = i12;
                    int i13 = b18;
                    b18 = i13;
                    SleepSessionHeartRate a5 = SleepDao_Impl.this.__sleepSessionHeartRateConverter.a(a.getString(i13));
                    int i14 = b19;
                    int i15 = b4;
                    int i16 = i14;
                    int i17 = b20;
                    b20 = i17;
                    int i18 = b21;
                    MFSleepSession mFSleepSession = new MFSleepSession(j, a2, string, num, num2, d, i8, i9, i10, i11, a3, num3, num4, num5, a4, string2, a5, SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(i14)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(i17)), a.getInt(i18));
                    int i19 = i5;
                    mFSleepSession.setPinType(a.getInt(i19));
                    arrayList.add(mFSleepSession);
                    anon10 = this;
                    b21 = i18;
                    i5 = i19;
                    b3 = i3;
                    b4 = i15;
                    b2 = i7;
                    b19 = i16;
                    b15 = i2;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon11 implements Callable<MFSleepDay> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon11(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public MFSleepDay call() throws Exception {
            MFSleepDay mFSleepDay;
            Cursor a = bg.a(SleepDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "pinType");
                int b2 = ag.b(a, "timezoneOffset");
                int b3 = ag.b(a, "date");
                int b4 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
                int b5 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
                int b6 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
                int b7 = ag.b(a, "createdAt");
                int b8 = ag.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    mFSleepDay = new MFSleepDay(SleepDao_Impl.this.__dateShortStringConverter.a(a.getString(b3)), a.getInt(b4), a.getInt(b5), SleepDao_Impl.this.__sleepDistributionConverter.a(a.getString(b6)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b7)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b8)));
                    mFSleepDay.setPinType(a.getInt(b));
                    mFSleepDay.setTimezoneOffset(a.getInt(b2));
                } else {
                    mFSleepDay = null;
                }
                return mFSleepDay;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon12 implements Callable<List<MFSleepDay>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon12(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<MFSleepDay> call() throws Exception {
            Cursor a = bg.a(SleepDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "pinType");
                int b2 = ag.b(a, "timezoneOffset");
                int b3 = ag.b(a, "date");
                int b4 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
                int b5 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
                int b6 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
                int b7 = ag.b(a, "createdAt");
                int b8 = ag.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    MFSleepDay mFSleepDay = new MFSleepDay(SleepDao_Impl.this.__dateShortStringConverter.a(a.getString(b3)), a.getInt(b4), a.getInt(b5), SleepDao_Impl.this.__sleepDistributionConverter.a(a.getString(b6)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b7)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b8)));
                    mFSleepDay.setPinType(a.getInt(b));
                    mFSleepDay.setTimezoneOffset(a.getInt(b2));
                    arrayList.add(mFSleepDay);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon13 implements Callable<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon13(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public Integer call() throws Exception {
            Cursor a = bg.a(SleepDao_Impl.this.__db, this.val$_statement, false);
            try {
                Integer num = null;
                if (a.moveToFirst()) {
                    if (!a.isNull(0)) {
                        num = Integer.valueOf(a.getInt(0));
                    }
                }
                return num;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon14 implements Callable<SleepRecommendedGoal> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon14(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public SleepRecommendedGoal call() throws Exception {
            SleepRecommendedGoal sleepRecommendedGoal;
            Cursor a = bg.a(SleepDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "id");
                int b2 = ag.b(a, "recommendedSleepGoal");
                if (a.moveToFirst()) {
                    sleepRecommendedGoal = new SleepRecommendedGoal(a.getInt(b2));
                    sleepRecommendedGoal.setId(a.getInt(b));
                } else {
                    sleepRecommendedGoal = null;
                }
                return sleepRecommendedGoal;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon15 implements Callable<SleepStatistic> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon15(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public SleepStatistic call() throws Exception {
            SleepStatistic sleepStatistic;
            Cursor a = bg.a(SleepDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "id");
                int b2 = ag.b(a, "uid");
                int b3 = ag.b(a, "sleepTimeBestDay");
                int b4 = ag.b(a, "sleepTimeBestStreak");
                int b5 = ag.b(a, "totalDays");
                int b6 = ag.b(a, "totalSleeps");
                int b7 = ag.b(a, "totalSleepMinutes");
                int b8 = ag.b(a, "totalSleepStateDistInMinute");
                int b9 = ag.b(a, "createdAt");
                int b10 = ag.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    sleepStatistic = new SleepStatistic(a.getString(b), a.getString(b2), SleepDao_Impl.this.__sleepStatisticConverter.a(a.getString(b3)), SleepDao_Impl.this.__sleepStatisticConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), a.getInt(b7), SleepDao_Impl.this.__integerArrayConverter.a(a.getString(b8)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b9)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b10)));
                } else {
                    sleepStatistic = null;
                }
                return sleepStatistic;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends lf<MFSleepSession> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleep_session`(`pinType`,`date`,`day`,`deviceSerialNumber`,`syncTime`,`bookmarkTime`,`normalizedSleepQuality`,`source`,`realStartTime`,`realEndTime`,`realSleepMinutes`,`realSleepStateDistInMinute`,`editedStartTime`,`editedEndTime`,`editedSleepMinutes`,`editedSleepStateDistInMinute`,`sleepStates`,`heartRate`,`createdAt`,`updatedAt`,`timezoneOffset`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, MFSleepSession mFSleepSession) {
            kgVar.b(1, (long) mFSleepSession.getPinType());
            kgVar.b(2, mFSleepSession.getDate());
            String a = SleepDao_Impl.this.__dateShortStringConverter.a(mFSleepSession.getDay());
            if (a == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, a);
            }
            if (mFSleepSession.getDeviceSerialNumber() == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, mFSleepSession.getDeviceSerialNumber());
            }
            if (mFSleepSession.getSyncTime() == null) {
                kgVar.a(5);
            } else {
                kgVar.b(5, (long) mFSleepSession.getSyncTime().intValue());
            }
            if (mFSleepSession.getBookmarkTime() == null) {
                kgVar.a(6);
            } else {
                kgVar.b(6, (long) mFSleepSession.getBookmarkTime().intValue());
            }
            kgVar.a(7, mFSleepSession.getNormalizedSleepQuality());
            kgVar.b(8, (long) mFSleepSession.getSource());
            kgVar.b(9, (long) mFSleepSession.getRealStartTime());
            kgVar.b(10, (long) mFSleepSession.getRealEndTime());
            kgVar.b(11, (long) mFSleepSession.getRealSleepMinutes());
            String a2 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepSession.getRealSleepStateDistInMinute());
            if (a2 == null) {
                kgVar.a(12);
            } else {
                kgVar.a(12, a2);
            }
            if (mFSleepSession.getEditedStartTime() == null) {
                kgVar.a(13);
            } else {
                kgVar.b(13, (long) mFSleepSession.getEditedStartTime().intValue());
            }
            if (mFSleepSession.getEditedEndTime() == null) {
                kgVar.a(14);
            } else {
                kgVar.b(14, (long) mFSleepSession.getEditedEndTime().intValue());
            }
            if (mFSleepSession.getEditedSleepMinutes() == null) {
                kgVar.a(15);
            } else {
                kgVar.b(15, (long) mFSleepSession.getEditedSleepMinutes().intValue());
            }
            String a3 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepSession.getEditedSleepStateDistInMinute());
            if (a3 == null) {
                kgVar.a(16);
            } else {
                kgVar.a(16, a3);
            }
            if (mFSleepSession.getSleepStates() == null) {
                kgVar.a(17);
            } else {
                kgVar.a(17, mFSleepSession.getSleepStates());
            }
            String a4 = SleepDao_Impl.this.__sleepSessionHeartRateConverter.a(mFSleepSession.getHeartRate());
            if (a4 == null) {
                kgVar.a(18);
            } else {
                kgVar.a(18, a4);
            }
            kgVar.b(19, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepSession.getCreatedAt()));
            kgVar.b(20, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepSession.getUpdatedAt()));
            kgVar.b(21, (long) mFSleepSession.getTimezoneOffset());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends lf<MFSleepDay> {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleep_date`(`pinType`,`timezoneOffset`,`date`,`goalMinutes`,`sleepMinutes`,`sleepStateDistInMinute`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, MFSleepDay mFSleepDay) {
            kgVar.b(1, (long) mFSleepDay.getPinType());
            kgVar.b(2, (long) mFSleepDay.getTimezoneOffset());
            String a = SleepDao_Impl.this.__dateShortStringConverter.a(mFSleepDay.getDate());
            if (a == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, a);
            }
            kgVar.b(4, (long) mFSleepDay.getGoalMinutes());
            kgVar.b(5, (long) mFSleepDay.getSleepMinutes());
            String a2 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepDay.getSleepStateDistInMinute());
            if (a2 == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, a2);
            }
            kgVar.b(7, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepDay.getCreatedAt()));
            kgVar.b(8, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepDay.getUpdatedAt()));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends lf<MFSleepSettings> {
        @DexIgnore
        public Anon4(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR FAIL INTO `sleep_settings`(`id`,`sleepGoal`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, MFSleepSettings mFSleepSettings) {
            kgVar.b(1, (long) mFSleepSettings.getId());
            kgVar.b(2, (long) mFSleepSettings.getSleepGoal());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends lf<SleepRecommendedGoal> {
        @DexIgnore
        public Anon5(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleepRecommendedGoals`(`id`,`recommendedSleepGoal`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, SleepRecommendedGoal sleepRecommendedGoal) {
            kgVar.b(1, (long) sleepRecommendedGoal.getId());
            kgVar.b(2, (long) sleepRecommendedGoal.getRecommendedSleepGoal());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends lf<SleepStatistic> {
        @DexIgnore
        public Anon6(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleep_statistic`(`id`,`uid`,`sleepTimeBestDay`,`sleepTimeBestStreak`,`totalDays`,`totalSleeps`,`totalSleepMinutes`,`totalSleepStateDistInMinute`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, SleepStatistic sleepStatistic) {
            if (sleepStatistic.getId() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, sleepStatistic.getId());
            }
            if (sleepStatistic.getUid() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, sleepStatistic.getUid());
            }
            String a = SleepDao_Impl.this.__sleepStatisticConverter.a(sleepStatistic.getSleepTimeBestDay());
            if (a == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, a);
            }
            String a2 = SleepDao_Impl.this.__sleepStatisticConverter.a(sleepStatistic.getSleepTimeBestStreak());
            if (a2 == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, a2);
            }
            kgVar.b(5, (long) sleepStatistic.getTotalDays());
            kgVar.b(6, (long) sleepStatistic.getTotalSleeps());
            kgVar.b(7, (long) sleepStatistic.getTotalSleepMinutes());
            String a3 = SleepDao_Impl.this.__integerArrayConverter.a(sleepStatistic.getTotalSleepStateDistInMinute());
            if (a3 == null) {
                kgVar.a(8);
            } else {
                kgVar.a(8, a3);
            }
            kgVar.b(9, SleepDao_Impl.this.__dateTimeConverter.a(sleepStatistic.getCreatedAt()));
            kgVar.b(10, SleepDao_Impl.this.__dateTimeConverter.a(sleepStatistic.getUpdatedAt()));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends wf {
        @DexIgnore
        public Anon7(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM sleep_session";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 extends wf {
        @DexIgnore
        public Anon8(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM sleep_date";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 extends wf {
        @DexIgnore
        public Anon9(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE sleep_settings SET sleepGoal = ?";
        }
    }

    @DexIgnore
    public SleepDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfMFSleepSession = new Anon1(roomDatabase);
        this.__insertionAdapterOfMFSleepSession_1 = new Anon2(roomDatabase);
        this.__insertionAdapterOfMFSleepDay = new Anon3(roomDatabase);
        this.__insertionAdapterOfMFSleepSettings = new Anon4(roomDatabase);
        this.__insertionAdapterOfSleepRecommendedGoal = new Anon5(roomDatabase);
        this.__insertionAdapterOfSleepStatistic = new Anon6(roomDatabase);
        this.__preparedStmtOfDeleteAllSleepSessions = new Anon7(roomDatabase);
        this.__preparedStmtOfDeleteAllSleepDays = new Anon8(roomDatabase);
        this.__preparedStmtOfUpdateSleepSettings = new Anon9(roomDatabase);
    }

    @DexIgnore
    private void __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(g4<String, ArrayList<MFSleepSession>> g4Var) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        Integer num;
        Integer num2;
        Integer num3;
        int i8;
        Integer num4;
        int i9;
        Integer num5;
        int i10;
        int i11;
        g4<String, ArrayList<MFSleepSession>> g4Var2 = g4Var;
        Set<String> keySet = g4Var.keySet();
        if (!keySet.isEmpty()) {
            if (g4Var.size() > 999) {
                g4 g4Var3 = new g4(999);
                int size = g4Var.size();
                g4 g4Var4 = g4Var3;
                int i12 = 0;
                loop0:
                while (true) {
                    i11 = 0;
                    while (i12 < size) {
                        g4Var4.put(g4Var2.c(i12), g4Var2.e(i12));
                        i12++;
                        i11++;
                        if (i11 == 999) {
                            __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(g4Var4);
                            g4Var4 = new g4(999);
                        }
                    }
                    break loop0;
                }
                if (i11 > 0) {
                    __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(g4Var4);
                    return;
                }
                return;
            }
            StringBuilder a = dg.a();
            a.append("SELECT `pinType`,`date`,`day`,`deviceSerialNumber`,`syncTime`,`bookmarkTime`,`normalizedSleepQuality`,`source`,`realStartTime`,`realEndTime`,`realSleepMinutes`,`realSleepStateDistInMinute`,`editedStartTime`,`editedEndTime`,`editedSleepMinutes`,`editedSleepStateDistInMinute`,`sleepStates`,`heartRate`,`createdAt`,`updatedAt`,`timezoneOffset` FROM `sleep_session` WHERE `day` IN (");
            int size2 = keySet.size();
            dg.a(a, size2);
            a.append(")");
            uf b = uf.b(a.toString(), size2 + 0);
            int i13 = 1;
            for (String next : keySet) {
                if (next == null) {
                    b.a(i13);
                } else {
                    b.a(i13, next);
                }
                i13++;
            }
            Cursor a2 = bg.a(this.__db, b, false);
            try {
                int a3 = ag.a(a2, "day");
                if (a3 != -1) {
                    int b2 = ag.b(a2, "pinType");
                    int b3 = ag.b(a2, "date");
                    int b4 = ag.b(a2, "day");
                    int b5 = ag.b(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
                    int b6 = ag.b(a2, "syncTime");
                    int b7 = ag.b(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
                    int b8 = ag.b(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
                    int b9 = ag.b(a2, "source");
                    int b10 = ag.b(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
                    int b11 = ag.b(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
                    int b12 = ag.b(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
                    int b13 = ag.b(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
                    int i14 = b2;
                    int b14 = ag.b(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
                    int b15 = ag.b(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                    int b16 = ag.b(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                    int b17 = ag.b(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                    int b18 = ag.b(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                    int b19 = ag.b(a2, "heartRate");
                    int b20 = ag.b(a2, "createdAt");
                    int b21 = ag.b(a2, "updatedAt");
                    int b22 = ag.b(a2, "timezoneOffset");
                    while (a2.moveToNext()) {
                        if (!a2.isNull(a3)) {
                            int i15 = b22;
                            ArrayList arrayList = g4Var2.get(a2.getString(a3));
                            if (arrayList != null) {
                                long j = a2.getLong(b3);
                                i4 = b4;
                                Date a4 = this.__dateShortStringConverter.a(a2.getString(b4));
                                String string = a2.getString(b5);
                                if (a2.isNull(b6)) {
                                    num = null;
                                } else {
                                    num = Integer.valueOf(a2.getInt(b6));
                                }
                                if (a2.isNull(b7)) {
                                    num2 = null;
                                } else {
                                    num2 = Integer.valueOf(a2.getInt(b7));
                                }
                                double d = a2.getDouble(b8);
                                int i16 = a2.getInt(b9);
                                int i17 = a2.getInt(b10);
                                int i18 = a2.getInt(b11);
                                int i19 = a2.getInt(b12);
                                SleepDistribution a5 = this.__sleepDistributionConverter.a(a2.getString(b13));
                                int i20 = b14;
                                if (a2.isNull(i20)) {
                                    i8 = b15;
                                    num3 = null;
                                } else {
                                    num3 = Integer.valueOf(a2.getInt(i20));
                                    i8 = b15;
                                }
                                if (a2.isNull(i8)) {
                                    i = i20;
                                    i9 = b16;
                                    num4 = null;
                                } else {
                                    i = i20;
                                    num4 = Integer.valueOf(a2.getInt(i8));
                                    i9 = b16;
                                }
                                if (a2.isNull(i9)) {
                                    b16 = i9;
                                    i2 = i8;
                                    i10 = b17;
                                    num5 = null;
                                } else {
                                    b16 = i9;
                                    num5 = Integer.valueOf(a2.getInt(i9));
                                    i10 = b17;
                                    i2 = i8;
                                }
                                b17 = i10;
                                SleepDistribution a6 = this.__sleepDistributionConverter.a(a2.getString(i10));
                                int i21 = b18;
                                String string2 = a2.getString(i21);
                                b18 = i21;
                                int i22 = b19;
                                b19 = i22;
                                SleepSessionHeartRate a7 = this.__sleepSessionHeartRateConverter.a(a2.getString(i22));
                                i7 = b3;
                                int i23 = b20;
                                i5 = b5;
                                i3 = i23;
                                int i24 = b21;
                                b21 = i24;
                                int i25 = i15;
                                MFSleepSession mFSleepSession = new MFSleepSession(j, a4, string, num, num2, d, i16, i17, i18, i19, a5, num3, num4, num5, a6, string2, a7, this.__dateTimeConverter.a(a2.getLong(i23)), this.__dateTimeConverter.a(a2.getLong(i24)), a2.getInt(i25));
                                i15 = i25;
                                i6 = i14;
                                mFSleepSession.setPinType(a2.getInt(i6));
                                arrayList.add(mFSleepSession);
                            } else {
                                i4 = b4;
                                i7 = b3;
                                i3 = b20;
                                i5 = b5;
                                i6 = i14;
                                int i26 = b15;
                                i = b14;
                                i2 = i26;
                            }
                            g4Var2 = g4Var;
                            b3 = i7;
                            i14 = i6;
                            b5 = i5;
                            b22 = i15;
                            b4 = i4;
                            b20 = i3;
                        } else {
                            int i27 = b20;
                            int i28 = b15;
                            i = b14;
                            i2 = i28;
                            g4Var2 = g4Var;
                        }
                        int i29 = i;
                        b15 = i2;
                        b14 = i29;
                    }
                    a2.close();
                }
            } finally {
                a2.close();
            }
        }
    }

    @DexIgnore
    public void deleteAllSleepDays() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeleteAllSleepDays.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllSleepDays.release(acquire);
        }
    }

    @DexIgnore
    public void deleteAllSleepSessions() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeleteAllSleepSessions.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllSleepSessions.release(acquire);
        }
    }

    @DexIgnore
    public MFSleepDay getLastSleepDay() {
        MFSleepDay mFSleepDay;
        uf b = uf.b("SELECT * FROM sleep_date ORDER BY date ASC LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "timezoneOffset");
            int b4 = ag.b(a, "date");
            int b5 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int b6 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int b7 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int b8 = ag.b(a, "createdAt");
            int b9 = ag.b(a, "updatedAt");
            if (a.moveToFirst()) {
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
            } else {
                mFSleepDay = null;
            }
            return mFSleepDay;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<Integer> getLastSleepGoal() {
        return this.__db.getInvalidationTracker().a(new String[]{"sleep_settings"}, false, new Anon13(uf.b("SELECT sleepGoal FROM sleep_settings LIMIT 1", 0)));
    }

    @DexIgnore
    public MFSleepDay getNearestSleepDayFromDate(String str) {
        MFSleepDay mFSleepDay;
        String str2 = str;
        uf b = uf.b("SELECT * FROM sleep_date WHERE date <= ? ORDER BY date ASC LIMIT 1", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "timezoneOffset");
            int b4 = ag.b(a, "date");
            int b5 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int b6 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int b7 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int b8 = ag.b(a, "createdAt");
            int b9 = ag.b(a, "updatedAt");
            if (a.moveToFirst()) {
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
            } else {
                mFSleepDay = null;
            }
            return mFSleepDay;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MFSleepSession> getPendingSleepSessions(long j, long j2) {
        uf ufVar;
        Integer num;
        Integer num2;
        Integer num3;
        int i;
        Integer num4;
        int i2;
        int i3;
        Integer num5;
        int i4;
        int i5;
        SleepDao_Impl sleepDao_Impl = this;
        uf b = uf.b("SELECT * FROM sleep_session WHERE date >= ? AND date <= ? AND pinType <> 0 ORDER BY editedStartTime ASC", 2);
        b.b(1, j);
        b.b(2, j2);
        sleepDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(sleepDao_Impl.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "date");
            int b4 = ag.b(a, "day");
            int b5 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b6 = ag.b(a, "syncTime");
            int b7 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int b8 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int b9 = ag.b(a, "source");
            int b10 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int b11 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int b12 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int b13 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int b14 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            ufVar = b;
            try {
                int b15 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int i6 = b2;
                int b16 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int b17 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int b18 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int b19 = ag.b(a, "heartRate");
                int b20 = ag.b(a, "createdAt");
                int b21 = ag.b(a, "updatedAt");
                int b22 = ag.b(a, "timezoneOffset");
                int i7 = b15;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j3 = a.getLong(b3);
                    int i8 = b3;
                    Date a2 = sleepDao_Impl.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i9 = a.getInt(b9);
                    int i10 = a.getInt(b10);
                    int i11 = a.getInt(b11);
                    int i12 = a.getInt(b12);
                    SleepDistribution a3 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        i = i7;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                        i = i7;
                    }
                    if (a.isNull(i)) {
                        i2 = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b16;
                    }
                    if (a.isNull(i2)) {
                        i3 = i;
                        i4 = i2;
                        num5 = null;
                        i5 = b17;
                    } else {
                        i3 = i;
                        num5 = Integer.valueOf(a.getInt(i2));
                        i5 = b17;
                        i4 = i2;
                    }
                    b17 = i5;
                    SleepDistribution a4 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(i5));
                    int i13 = b18;
                    String string2 = a.getString(i13);
                    b18 = i13;
                    int i14 = b19;
                    b19 = i14;
                    SleepSessionHeartRate a5 = sleepDao_Impl.__sleepSessionHeartRateConverter.a(a.getString(i14));
                    int i15 = b20;
                    int i16 = b4;
                    int i17 = i15;
                    int i18 = b21;
                    b21 = i18;
                    int i19 = b22;
                    MFSleepSession mFSleepSession = new MFSleepSession(j3, a2, string, num, num2, d, i9, i10, i11, i12, a3, num3, num4, num5, a4, string2, a5, sleepDao_Impl.__dateTimeConverter.a(a.getLong(i15)), sleepDao_Impl.__dateTimeConverter.a(a.getLong(i18)), a.getInt(i19));
                    int i20 = i6;
                    mFSleepSession.setPinType(a.getInt(i20));
                    arrayList.add(mFSleepSession);
                    sleepDao_Impl = this;
                    b22 = i19;
                    i6 = i20;
                    b4 = i16;
                    b3 = i8;
                    b20 = i17;
                    int i21 = i4;
                    i7 = i3;
                    b16 = i21;
                }
                a.close();
                ufVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public MFSleepDay getSleepDay(String str) {
        MFSleepDay mFSleepDay;
        String str2 = str;
        uf b = uf.b("SELECT * FROM sleep_date WHERE date == ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "timezoneOffset");
            int b4 = ag.b(a, "date");
            int b5 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int b6 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int b7 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int b8 = ag.b(a, "createdAt");
            int b9 = ag.b(a, "updatedAt");
            if (a.moveToFirst()) {
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
            } else {
                mFSleepDay = null;
            }
            return mFSleepDay;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<MFSleepDay> getSleepDayLiveData(String str) {
        uf b = uf.b("SELECT * FROM sleep_date WHERE date == ?", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{com.fossil.wearables.fsl.sleep.MFSleepDay.TABLE_NAME}, false, new Anon11(b));
    }

    @DexIgnore
    public List<MFSleepDay> getSleepDays(String str, String str2) {
        String str3 = str;
        String str4 = str2;
        uf b = uf.b("SELECT * FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        if (str3 == null) {
            b.a(1);
        } else {
            b.a(1, str3);
        }
        if (str4 == null) {
            b.a(2);
        } else {
            b.a(2, str4);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "timezoneOffset");
            int b4 = ag.b(a, "date");
            int b5 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int b6 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int b7 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int b8 = ag.b(a, "createdAt");
            int b9 = ag.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                MFSleepDay mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
                arrayList.add(mFSleepDay);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<MFSleepDay>> getSleepDaysLiveData(String str, String str2) {
        uf b = uf.b("SELECT * FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        if (str2 == null) {
            b.a(2);
        } else {
            b.a(2, str2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{com.fossil.wearables.fsl.sleep.MFSleepDay.TABLE_NAME}, false, new Anon12(b));
    }

    @DexIgnore
    public LiveData<SleepRecommendedGoal> getSleepRecommendedGoalLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"sleepRecommendedGoals"}, false, new Anon14(uf.b("SELECT * FROM sleepRecommendedGoals LIMIT 1", 0)));
    }

    @DexIgnore
    public MFSleepSession getSleepSession(long j) {
        uf ufVar;
        MFSleepSession mFSleepSession;
        Integer num;
        Integer num2;
        Integer num3;
        Integer num4;
        int i;
        uf b = uf.b("SELECT * FROM sleep_session WHERE realEndTime = ?", 1);
        b.b(1, j);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "date");
            int b4 = ag.b(a, "day");
            int b5 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b6 = ag.b(a, "syncTime");
            int b7 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int b8 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int b9 = ag.b(a, "source");
            int b10 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int b11 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int b12 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int b13 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int b14 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            ufVar = b;
            try {
                int b15 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int i2 = b2;
                int b16 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int b17 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int b18 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int b19 = ag.b(a, "heartRate");
                int b20 = ag.b(a, "createdAt");
                int b21 = ag.b(a, "updatedAt");
                int b22 = ag.b(a, "timezoneOffset");
                Integer num5 = null;
                if (a.moveToFirst()) {
                    long j2 = a.getLong(b3);
                    Date a2 = this.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i3 = a.getInt(b9);
                    int i4 = a.getInt(b10);
                    int i5 = a.getInt(b11);
                    int i6 = a.getInt(b12);
                    SleepDistribution a3 = this.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                    }
                    if (a.isNull(b15)) {
                        i = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(b15));
                        i = b16;
                    }
                    if (!a.isNull(i)) {
                        num5 = Integer.valueOf(a.getInt(i));
                    }
                    mFSleepSession = new MFSleepSession(j2, a2, string, num, num2, d, i3, i4, i5, i6, a3, num3, num4, num5, this.__sleepDistributionConverter.a(a.getString(b17)), a.getString(b18), this.__sleepSessionHeartRateConverter.a(a.getString(b19)), this.__dateTimeConverter.a(a.getLong(b20)), this.__dateTimeConverter.a(a.getLong(b21)), a.getInt(b22));
                    mFSleepSession.setPinType(a.getInt(i2));
                } else {
                    mFSleepSession = null;
                }
                a.close();
                ufVar.c();
                return mFSleepSession;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<MFSleepSession> getSleepSessions(String str) {
        uf ufVar;
        Integer num;
        Integer num2;
        Integer num3;
        int i;
        Integer num4;
        int i2;
        Integer num5;
        int i3;
        SleepDao_Impl sleepDao_Impl = this;
        String str2 = str;
        uf b = uf.b("SELECT * FROM sleep_session WHERE day = ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        sleepDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(sleepDao_Impl.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "date");
            int b4 = ag.b(a, "day");
            int b5 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b6 = ag.b(a, "syncTime");
            int b7 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int b8 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int b9 = ag.b(a, "source");
            int b10 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int b11 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int b12 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int b13 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int b14 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            ufVar = b;
            try {
                int b15 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int i4 = b2;
                int b16 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int b17 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int b18 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int b19 = ag.b(a, "heartRate");
                int b20 = ag.b(a, "createdAt");
                int b21 = ag.b(a, "updatedAt");
                int b22 = ag.b(a, "timezoneOffset");
                int i5 = b15;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j = a.getLong(b3);
                    int i6 = b3;
                    Date a2 = sleepDao_Impl.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i7 = a.getInt(b9);
                    int i8 = a.getInt(b10);
                    int i9 = a.getInt(b11);
                    int i10 = a.getInt(b12);
                    SleepDistribution a3 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        i = i5;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                        i = i5;
                    }
                    if (a.isNull(i)) {
                        i2 = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b16;
                    }
                    if (a.isNull(i2)) {
                        i5 = i;
                        b16 = i2;
                        i3 = b17;
                        num5 = null;
                    } else {
                        i5 = i;
                        num5 = Integer.valueOf(a.getInt(i2));
                        i3 = b17;
                        b16 = i2;
                    }
                    b17 = i3;
                    SleepDistribution a4 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(i3));
                    int i11 = b18;
                    String string2 = a.getString(i11);
                    b18 = i11;
                    int i12 = b19;
                    b19 = i12;
                    SleepSessionHeartRate a5 = sleepDao_Impl.__sleepSessionHeartRateConverter.a(a.getString(i12));
                    int i13 = b20;
                    int i14 = b4;
                    int i15 = i13;
                    int i16 = b21;
                    b21 = i16;
                    int i17 = b22;
                    MFSleepSession mFSleepSession = new MFSleepSession(j, a2, string, num, num2, d, i7, i8, i9, i10, a3, num3, num4, num5, a4, string2, a5, sleepDao_Impl.__dateTimeConverter.a(a.getLong(i13)), sleepDao_Impl.__dateTimeConverter.a(a.getLong(i16)), a.getInt(i17));
                    int i18 = i4;
                    mFSleepSession.setPinType(a.getInt(i18));
                    arrayList.add(mFSleepSession);
                    sleepDao_Impl = this;
                    b22 = i17;
                    i4 = i18;
                    b4 = i14;
                    b3 = i6;
                    b20 = i15;
                }
                a.close();
                ufVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public LiveData<List<MFSleepSession>> getSleepSessionsLiveData(long j, long j2) {
        uf b = uf.b("SELECT * FROM sleep_session WHERE date >= ? AND date <= ? ORDER BY editedStartTime ASC", 2);
        b.b(1, j);
        b.b(2, j2);
        return this.__db.getInvalidationTracker().a(new String[]{com.fossil.wearables.fsl.sleep.MFSleepSession.TABLE_NAME}, false, new Anon10(b));
    }

    @DexIgnore
    public MFSleepSettings getSleepSettings() {
        MFSleepSettings mFSleepSettings;
        uf b = uf.b("SELECT * FROM sleep_settings LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "sleepGoal");
            if (a.moveToFirst()) {
                mFSleepSettings = new MFSleepSettings(a.getInt(b3));
                mFSleepSettings.setId(a.getInt(b2));
            } else {
                mFSleepSettings = null;
            }
            return mFSleepSettings;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public SleepStatistic getSleepStatistic() {
        SleepStatistic sleepStatistic;
        uf b = uf.b("SELECT * FROM sleep_statistic LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "uid");
            int b4 = ag.b(a, "sleepTimeBestDay");
            int b5 = ag.b(a, "sleepTimeBestStreak");
            int b6 = ag.b(a, "totalDays");
            int b7 = ag.b(a, "totalSleeps");
            int b8 = ag.b(a, "totalSleepMinutes");
            int b9 = ag.b(a, "totalSleepStateDistInMinute");
            int b10 = ag.b(a, "createdAt");
            int b11 = ag.b(a, "updatedAt");
            if (a.moveToFirst()) {
                sleepStatistic = new SleepStatistic(a.getString(b2), a.getString(b3), this.__sleepStatisticConverter.a(a.getString(b4)), this.__sleepStatisticConverter.a(a.getString(b5)), a.getInt(b6), a.getInt(b7), a.getInt(b8), this.__integerArrayConverter.a(a.getString(b9)), this.__dateTimeConverter.a(a.getLong(b10)), this.__dateTimeConverter.a(a.getLong(b11)));
            } else {
                sleepStatistic = null;
            }
            return sleepStatistic;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<SleepStatistic> getSleepStatisticLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{SleepStatistic.TABLE_NAME}, false, new Anon15(uf.b("SELECT * FROM sleep_statistic LIMIT 1", 0)));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0115 A[Catch:{ all -> 0x0139 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0120 A[Catch:{ all -> 0x0139 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0123 A[Catch:{ all -> 0x0139 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0128 A[SYNTHETIC] */
    public List<SleepSummary> getSleepSummariesDesc(String str, String str2) {
        MFSleepDay mFSleepDay;
        ArrayList arrayList;
        String str3 = str;
        String str4 = str2;
        uf b = uf.b("SELECT * FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date DESC", 2);
        if (str3 == null) {
            b.a(1);
        } else {
            b.a(1, str3);
        }
        if (str4 == null) {
            b.a(2);
        } else {
            b.a(2, str4);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, true);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "timezoneOffset");
            int b4 = ag.b(a, "date");
            int b5 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int b6 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int b7 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int b8 = ag.b(a, "createdAt");
            int b9 = ag.b(a, "updatedAt");
            g4 g4Var = new g4();
            while (a.moveToNext()) {
                if (!a.isNull(b4)) {
                    String string = a.getString(b4);
                    if (((ArrayList) g4Var.get(string)) == null) {
                        g4Var.put(string, new ArrayList());
                    }
                }
            }
            a.moveToPosition(-1);
            __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(g4Var);
            ArrayList arrayList2 = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                if (a.isNull(b2) && a.isNull(b3) && a.isNull(b4) && a.isNull(b5) && a.isNull(b6) && a.isNull(b7) && a.isNull(b8)) {
                    if (a.isNull(b9)) {
                        mFSleepDay = null;
                        arrayList = a.isNull(b4) ? (ArrayList) g4Var.get(a.getString(b4)) : null;
                        if (arrayList != null) {
                            arrayList = new ArrayList();
                        }
                        arrayList2.add(new SleepSummary(mFSleepDay, arrayList));
                    }
                }
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
                if (a.isNull(b4)) {
                }
                if (arrayList != null) {
                }
                arrayList2.add(new SleepSummary(mFSleepDay, arrayList));
            }
            return arrayList2;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v4, resolved type: java.util.ArrayList} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0100 A[Catch:{ all -> 0x0120 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x010d A[Catch:{ all -> 0x0120 }] */
    public SleepSummary getSleepSummary(String str) {
        SleepSummary sleepSummary;
        MFSleepDay mFSleepDay;
        String str2 = str;
        uf b = uf.b("SELECT * FROM sleep_date WHERE date == ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, true);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "timezoneOffset");
            int b4 = ag.b(a, "date");
            int b5 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int b6 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int b7 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int b8 = ag.b(a, "createdAt");
            int b9 = ag.b(a, "updatedAt");
            g4 g4Var = new g4();
            while (a.moveToNext()) {
                if (!a.isNull(b4)) {
                    String string = a.getString(b4);
                    if (((ArrayList) g4Var.get(string)) == null) {
                        g4Var.put(string, new ArrayList());
                    }
                }
            }
            a.moveToPosition(-1);
            __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(g4Var);
            ArrayList arrayList = null;
            if (a.moveToFirst()) {
                if (a.isNull(b2) && a.isNull(b3) && a.isNull(b4) && a.isNull(b5) && a.isNull(b6) && a.isNull(b7) && a.isNull(b8)) {
                    if (a.isNull(b9)) {
                        mFSleepDay = null;
                        if (!a.isNull(b4)) {
                            arrayList = g4Var.get(a.getString(b4));
                        }
                        if (arrayList == null) {
                            arrayList = new ArrayList();
                        }
                        sleepSummary = new SleepSummary(mFSleepDay, arrayList);
                    }
                }
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
                if (!a.isNull(b4)) {
                }
                if (arrayList == null) {
                }
                sleepSummary = new SleepSummary(mFSleepDay, arrayList);
            } else {
                sleepSummary = null;
            }
            return sleepSummary;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public int getTotalSleep(String str, String str2) {
        uf b = uf.b("SELECT SUM(sleepMinutes) FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        if (str2 == null) {
            b.a(2);
        } else {
            b.a(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        int i = 0;
        Cursor a = bg.a(this.__db, b, false);
        try {
            if (a.moveToFirst()) {
                i = a.getInt(0);
            }
            return i;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertSleepSettings(MFSleepSettings mFSleepSettings) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepSettings.insert(mFSleepSettings);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void updateSleepSettings(int i) {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfUpdateSleepSettings.acquire();
        acquire.b(1, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfUpdateSleepSettings.release(acquire);
        }
    }

    @DexIgnore
    public void upsertSleepDay(MFSleepDay mFSleepDay) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepDay.insert(mFSleepDay);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertSleepDays(List<MFSleepDay> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepDay.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertSleepRecommendedGoal(SleepRecommendedGoal sleepRecommendedGoal) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSleepRecommendedGoal.insert(sleepRecommendedGoal);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertSleepSession(MFSleepSession mFSleepSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepSession.insert(mFSleepSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertSleepSessionList(List<MFSleepSession> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepSession_1.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public long upsertSleepStatistic(SleepStatistic sleepStatistic) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfSleepStatistic.insertAndReturnId(sleepStatistic);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<MFSleepSession> getPendingSleepSessions() {
        uf ufVar;
        Integer num;
        Integer num2;
        Integer num3;
        int i;
        Integer num4;
        int i2;
        Integer num5;
        int i3;
        SleepDao_Impl sleepDao_Impl = this;
        uf b = uf.b("SELECT * FROM sleep_session WHERE pinType <> 0", 0);
        sleepDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(sleepDao_Impl.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "date");
            int b4 = ag.b(a, "day");
            int b5 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b6 = ag.b(a, "syncTime");
            int b7 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int b8 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int b9 = ag.b(a, "source");
            int b10 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int b11 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int b12 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int b13 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int b14 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            ufVar = b;
            try {
                int b15 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int i4 = b2;
                int b16 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int b17 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int b18 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int b19 = ag.b(a, "heartRate");
                int b20 = ag.b(a, "createdAt");
                int b21 = ag.b(a, "updatedAt");
                int b22 = ag.b(a, "timezoneOffset");
                int i5 = b15;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j = a.getLong(b3);
                    int i6 = b3;
                    Date a2 = sleepDao_Impl.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i7 = a.getInt(b9);
                    int i8 = a.getInt(b10);
                    int i9 = a.getInt(b11);
                    int i10 = a.getInt(b12);
                    SleepDistribution a3 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        i = i5;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                        i = i5;
                    }
                    if (a.isNull(i)) {
                        i2 = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b16;
                    }
                    if (a.isNull(i2)) {
                        i5 = i;
                        b16 = i2;
                        i3 = b17;
                        num5 = null;
                    } else {
                        i5 = i;
                        num5 = Integer.valueOf(a.getInt(i2));
                        i3 = b17;
                        b16 = i2;
                    }
                    b17 = i3;
                    SleepDistribution a4 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(i3));
                    int i11 = b18;
                    String string2 = a.getString(i11);
                    b18 = i11;
                    int i12 = b19;
                    b19 = i12;
                    SleepSessionHeartRate a5 = sleepDao_Impl.__sleepSessionHeartRateConverter.a(a.getString(i12));
                    int i13 = b20;
                    int i14 = b4;
                    int i15 = i13;
                    int i16 = b21;
                    b21 = i16;
                    int i17 = b22;
                    MFSleepSession mFSleepSession = new MFSleepSession(j, a2, string, num, num2, d, i7, i8, i9, i10, a3, num3, num4, num5, a4, string2, a5, sleepDao_Impl.__dateTimeConverter.a(a.getLong(i13)), sleepDao_Impl.__dateTimeConverter.a(a.getLong(i16)), a.getInt(i17));
                    int i18 = i4;
                    mFSleepSession.setPinType(a.getInt(i18));
                    arrayList.add(mFSleepSession);
                    sleepDao_Impl = this;
                    b22 = i17;
                    i4 = i18;
                    b4 = i14;
                    b3 = i6;
                    b20 = i15;
                }
                a.close();
                ufVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<MFSleepSession> getSleepSessions(long j, long j2) {
        uf ufVar;
        Integer num;
        Integer num2;
        Integer num3;
        int i;
        Integer num4;
        int i2;
        int i3;
        Integer num5;
        int i4;
        int i5;
        SleepDao_Impl sleepDao_Impl = this;
        uf b = uf.b("SELECT * FROM sleep_session WHERE date >= ? AND date <= ? ORDER BY editedStartTime ASC", 2);
        b.b(1, j);
        b.b(2, j2);
        sleepDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(sleepDao_Impl.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "date");
            int b4 = ag.b(a, "day");
            int b5 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b6 = ag.b(a, "syncTime");
            int b7 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int b8 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int b9 = ag.b(a, "source");
            int b10 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int b11 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int b12 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int b13 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int b14 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            ufVar = b;
            try {
                int b15 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int i6 = b2;
                int b16 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int b17 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int b18 = ag.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int b19 = ag.b(a, "heartRate");
                int b20 = ag.b(a, "createdAt");
                int b21 = ag.b(a, "updatedAt");
                int b22 = ag.b(a, "timezoneOffset");
                int i7 = b15;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j3 = a.getLong(b3);
                    int i8 = b3;
                    Date a2 = sleepDao_Impl.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i9 = a.getInt(b9);
                    int i10 = a.getInt(b10);
                    int i11 = a.getInt(b11);
                    int i12 = a.getInt(b12);
                    SleepDistribution a3 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        i = i7;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                        i = i7;
                    }
                    if (a.isNull(i)) {
                        i2 = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b16;
                    }
                    if (a.isNull(i2)) {
                        i3 = i;
                        i4 = i2;
                        num5 = null;
                        i5 = b17;
                    } else {
                        i3 = i;
                        num5 = Integer.valueOf(a.getInt(i2));
                        i5 = b17;
                        i4 = i2;
                    }
                    b17 = i5;
                    SleepDistribution a4 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(i5));
                    int i13 = b18;
                    String string2 = a.getString(i13);
                    b18 = i13;
                    int i14 = b19;
                    b19 = i14;
                    SleepSessionHeartRate a5 = sleepDao_Impl.__sleepSessionHeartRateConverter.a(a.getString(i14));
                    int i15 = b20;
                    int i16 = b4;
                    int i17 = i15;
                    int i18 = b21;
                    b21 = i18;
                    int i19 = b22;
                    MFSleepSession mFSleepSession = new MFSleepSession(j3, a2, string, num, num2, d, i9, i10, i11, i12, a3, num3, num4, num5, a4, string2, a5, sleepDao_Impl.__dateTimeConverter.a(a.getLong(i15)), sleepDao_Impl.__dateTimeConverter.a(a.getLong(i18)), a.getInt(i19));
                    int i20 = i6;
                    mFSleepSession.setPinType(a.getInt(i20));
                    arrayList.add(mFSleepSession);
                    sleepDao_Impl = this;
                    b22 = i19;
                    i6 = i20;
                    b4 = i16;
                    b3 = i8;
                    b20 = i17;
                    int i21 = i4;
                    i7 = i3;
                    b16 = i21;
                }
                a.close();
                ufVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.c();
            throw th;
        }
    }
}
