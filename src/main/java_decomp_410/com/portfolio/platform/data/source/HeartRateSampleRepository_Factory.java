package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSampleRepository_Factory implements Factory<HeartRateSampleRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;
    @DexIgnore
    public /* final */ Provider<FitnessDataDao> mFitnessDataDaoProvider;
    @DexIgnore
    public /* final */ Provider<HeartRateSampleDao> mHeartRateSampleDaoProvider;

    @DexIgnore
    public HeartRateSampleRepository_Factory(Provider<HeartRateSampleDao> provider, Provider<FitnessDataDao> provider2, Provider<ApiServiceV2> provider3) {
        this.mHeartRateSampleDaoProvider = provider;
        this.mFitnessDataDaoProvider = provider2;
        this.mApiServiceProvider = provider3;
    }

    @DexIgnore
    public static HeartRateSampleRepository_Factory create(Provider<HeartRateSampleDao> provider, Provider<FitnessDataDao> provider2, Provider<ApiServiceV2> provider3) {
        return new HeartRateSampleRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static HeartRateSampleRepository newHeartRateSampleRepository(HeartRateSampleDao heartRateSampleDao, FitnessDataDao fitnessDataDao, ApiServiceV2 apiServiceV2) {
        return new HeartRateSampleRepository(heartRateSampleDao, fitnessDataDao, apiServiceV2);
    }

    @DexIgnore
    public static HeartRateSampleRepository provideInstance(Provider<HeartRateSampleDao> provider, Provider<FitnessDataDao> provider2, Provider<ApiServiceV2> provider3) {
        return new HeartRateSampleRepository(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public HeartRateSampleRepository get() {
        return provideInstance(this.mHeartRateSampleDaoProvider, this.mFitnessDataDaoProvider, this.mApiServiceProvider);
    }
}
