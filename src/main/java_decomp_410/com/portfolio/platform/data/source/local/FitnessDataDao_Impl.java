package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.d72;
import com.fossil.blesdk.obfuscated.e72;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FitnessDataDao_Impl extends FitnessDataDao {
    @DexIgnore
    public /* final */ d72 __dateTimeUTCStringConverter; // = new d72();
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ kf __deletionAdapterOfFitnessDataWrapper;
    @DexIgnore
    public /* final */ e72 __fitnessDataConverter; // = new e72();
    @DexIgnore
    public /* final */ lf __insertionAdapterOfFitnessDataWrapper;
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeleteAllFitnessData;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<FitnessDataWrapper> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `fitness_data`(`step`,`activeMinute`,`calorie`,`distance`,`stress`,`resting`,`heartRate`,`sleeps`,`workouts`,`startTime`,`endTime`,`syncTime`,`timezoneOffsetInSecond`,`serialNumber`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, FitnessDataWrapper fitnessDataWrapper) {
            String a = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.step);
            if (a == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, a);
            }
            String a2 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.activeMinute);
            if (a2 == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, a2);
            }
            String a3 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.calorie);
            if (a3 == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, a3);
            }
            String a4 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.distance);
            if (a4 == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, a4);
            }
            String a5 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.getStress());
            if (a5 == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, a5);
            }
            String a6 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.getResting());
            if (a6 == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, a6);
            }
            String a7 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.getHeartRate());
            if (a7 == null) {
                kgVar.a(7);
            } else {
                kgVar.a(7, a7);
            }
            String b = FitnessDataDao_Impl.this.__fitnessDataConverter.b(fitnessDataWrapper.getSleeps());
            if (b == null) {
                kgVar.a(8);
            } else {
                kgVar.a(8, b);
            }
            String c = FitnessDataDao_Impl.this.__fitnessDataConverter.c(fitnessDataWrapper.getWorkouts());
            if (c == null) {
                kgVar.a(9);
            } else {
                kgVar.a(9, c);
            }
            String a8 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getStartTime());
            if (a8 == null) {
                kgVar.a(10);
            } else {
                kgVar.a(10, a8);
            }
            String a9 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getEndTime());
            if (a9 == null) {
                kgVar.a(11);
            } else {
                kgVar.a(11, a9);
            }
            String a10 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getSyncTime());
            if (a10 == null) {
                kgVar.a(12);
            } else {
                kgVar.a(12, a10);
            }
            kgVar.b(13, (long) fitnessDataWrapper.getTimezoneOffsetInSecond());
            if (fitnessDataWrapper.getSerialNumber() == null) {
                kgVar.a(14);
            } else {
                kgVar.a(14, fitnessDataWrapper.getSerialNumber());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends kf<FitnessDataWrapper> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `fitness_data` WHERE `startTime` = ?";
        }

        @DexIgnore
        public void bind(kg kgVar, FitnessDataWrapper fitnessDataWrapper) {
            String a = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getStartTime());
            if (a == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, a);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends wf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM fitness_data";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<FitnessDataWrapper>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon4(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<FitnessDataWrapper> call() throws Exception {
            Cursor a = bg.a(FitnessDataDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "step");
                int b2 = ag.b(a, "activeMinute");
                int b3 = ag.b(a, "calorie");
                int b4 = ag.b(a, "distance");
                int b5 = ag.b(a, "stress");
                int b6 = ag.b(a, "resting");
                int b7 = ag.b(a, "heartRate");
                int b8 = ag.b(a, "sleeps");
                int b9 = ag.b(a, "workouts");
                int b10 = ag.b(a, SampleRaw.COLUMN_START_TIME);
                int b11 = ag.b(a, SampleRaw.COLUMN_END_TIME);
                int b12 = ag.b(a, "syncTime");
                int b13 = ag.b(a, "timezoneOffsetInSecond");
                int b14 = ag.b(a, "serialNumber");
                int i = b9;
                int i2 = b8;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i3 = b10;
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(a.getString(b10)), FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(a.getString(b11)), FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(a.getString(b12)), a.getInt(b13), a.getString(b14));
                    int i4 = b;
                    fitnessDataWrapper.step = FitnessDataDao_Impl.this.__fitnessDataConverter.g(a.getString(b));
                    fitnessDataWrapper.activeMinute = FitnessDataDao_Impl.this.__fitnessDataConverter.a(a.getString(b2));
                    fitnessDataWrapper.calorie = FitnessDataDao_Impl.this.__fitnessDataConverter.b(a.getString(b3));
                    fitnessDataWrapper.distance = FitnessDataDao_Impl.this.__fitnessDataConverter.c(a.getString(b4));
                    fitnessDataWrapper.setStress(FitnessDataDao_Impl.this.__fitnessDataConverter.h(a.getString(b5)));
                    fitnessDataWrapper.setResting(FitnessDataDao_Impl.this.__fitnessDataConverter.e(a.getString(b6)));
                    fitnessDataWrapper.setHeartRate(FitnessDataDao_Impl.this.__fitnessDataConverter.d(a.getString(b7)));
                    int i5 = i2;
                    i2 = i5;
                    fitnessDataWrapper.setSleeps(FitnessDataDao_Impl.this.__fitnessDataConverter.f(a.getString(i5)));
                    int i6 = i;
                    i = i6;
                    fitnessDataWrapper.setWorkouts(FitnessDataDao_Impl.this.__fitnessDataConverter.i(a.getString(i6)));
                    arrayList.add(fitnessDataWrapper);
                    b10 = i3;
                    b = i4;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public FitnessDataDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfFitnessDataWrapper = new Anon1(roomDatabase);
        this.__deletionAdapterOfFitnessDataWrapper = new Anon2(roomDatabase);
        this.__preparedStmtOfDeleteAllFitnessData = new Anon3(roomDatabase);
    }

    @DexIgnore
    public void deleteAllFitnessData() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeleteAllFitnessData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllFitnessData.release(acquire);
        }
    }

    @DexIgnore
    public void deleteFitnessData(List<FitnessDataWrapper> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfFitnessDataWrapper.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<FitnessDataWrapper> getListFitnessData(DateTime dateTime, DateTime dateTime2) {
        uf ufVar;
        uf b = uf.b("SELECT * FROM fitness_data WHERE startTime >= ? AND startTime <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateTimeUTCStringConverter.a(dateTime);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateTimeUTCStringConverter.a(dateTime2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a3, "step");
            int b3 = ag.b(a3, "activeMinute");
            int b4 = ag.b(a3, "calorie");
            int b5 = ag.b(a3, "distance");
            int b6 = ag.b(a3, "stress");
            int b7 = ag.b(a3, "resting");
            int b8 = ag.b(a3, "heartRate");
            int b9 = ag.b(a3, "sleeps");
            int b10 = ag.b(a3, "workouts");
            int b11 = ag.b(a3, SampleRaw.COLUMN_START_TIME);
            int b12 = ag.b(a3, SampleRaw.COLUMN_END_TIME);
            int b13 = ag.b(a3, "syncTime");
            int b14 = ag.b(a3, "timezoneOffsetInSecond");
            ufVar = b;
            try {
                int b15 = ag.b(a3, "serialNumber");
                int i = b10;
                int i2 = b9;
                ArrayList arrayList = new ArrayList(a3.getCount());
                while (a3.moveToNext()) {
                    int i3 = b11;
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(this.__dateTimeUTCStringConverter.a(a3.getString(b11)), this.__dateTimeUTCStringConverter.a(a3.getString(b12)), this.__dateTimeUTCStringConverter.a(a3.getString(b13)), a3.getInt(b14), a3.getString(b15));
                    int i4 = b2;
                    fitnessDataWrapper.step = this.__fitnessDataConverter.g(a3.getString(b2));
                    fitnessDataWrapper.activeMinute = this.__fitnessDataConverter.a(a3.getString(b3));
                    fitnessDataWrapper.calorie = this.__fitnessDataConverter.b(a3.getString(b4));
                    fitnessDataWrapper.distance = this.__fitnessDataConverter.c(a3.getString(b5));
                    fitnessDataWrapper.setStress(this.__fitnessDataConverter.h(a3.getString(b6)));
                    fitnessDataWrapper.setResting(this.__fitnessDataConverter.e(a3.getString(b7)));
                    fitnessDataWrapper.setHeartRate(this.__fitnessDataConverter.d(a3.getString(b8)));
                    int i5 = i2;
                    i2 = i5;
                    fitnessDataWrapper.setSleeps(this.__fitnessDataConverter.f(a3.getString(i5)));
                    int i6 = i;
                    i = i6;
                    fitnessDataWrapper.setWorkouts(this.__fitnessDataConverter.i(a3.getString(i6)));
                    arrayList.add(fitnessDataWrapper);
                    b11 = i3;
                    b2 = i4;
                }
                a3.close();
                ufVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a3.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a3.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public LiveData<List<FitnessDataWrapper>> getListFitnessDataLiveData(DateTime dateTime, DateTime dateTime2) {
        uf b = uf.b("SELECT * FROM fitness_data WHERE startTime >= ? AND startTime <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateTimeUTCStringConverter.a(dateTime);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateTimeUTCStringConverter.a(dateTime2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"fitness_data"}, false, new Anon4(b));
    }

    @DexIgnore
    public List<FitnessDataWrapper> getPendingFitnessData() {
        uf ufVar;
        uf b = uf.b("SELECT * FROM fitness_data ORDER BY startTime ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "step");
            int b3 = ag.b(a, "activeMinute");
            int b4 = ag.b(a, "calorie");
            int b5 = ag.b(a, "distance");
            int b6 = ag.b(a, "stress");
            int b7 = ag.b(a, "resting");
            int b8 = ag.b(a, "heartRate");
            int b9 = ag.b(a, "sleeps");
            int b10 = ag.b(a, "workouts");
            int b11 = ag.b(a, SampleRaw.COLUMN_START_TIME);
            int b12 = ag.b(a, SampleRaw.COLUMN_END_TIME);
            int b13 = ag.b(a, "syncTime");
            int b14 = ag.b(a, "timezoneOffsetInSecond");
            ufVar = b;
            try {
                int b15 = ag.b(a, "serialNumber");
                int i = b10;
                int i2 = b9;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i3 = b11;
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(this.__dateTimeUTCStringConverter.a(a.getString(b11)), this.__dateTimeUTCStringConverter.a(a.getString(b12)), this.__dateTimeUTCStringConverter.a(a.getString(b13)), a.getInt(b14), a.getString(b15));
                    int i4 = b2;
                    fitnessDataWrapper.step = this.__fitnessDataConverter.g(a.getString(b2));
                    fitnessDataWrapper.activeMinute = this.__fitnessDataConverter.a(a.getString(b3));
                    fitnessDataWrapper.calorie = this.__fitnessDataConverter.b(a.getString(b4));
                    fitnessDataWrapper.distance = this.__fitnessDataConverter.c(a.getString(b5));
                    fitnessDataWrapper.setStress(this.__fitnessDataConverter.h(a.getString(b6)));
                    fitnessDataWrapper.setResting(this.__fitnessDataConverter.e(a.getString(b7)));
                    fitnessDataWrapper.setHeartRate(this.__fitnessDataConverter.d(a.getString(b8)));
                    int i5 = i2;
                    i2 = i5;
                    fitnessDataWrapper.setSleeps(this.__fitnessDataConverter.f(a.getString(i5)));
                    int i6 = i;
                    i = i6;
                    fitnessDataWrapper.setWorkouts(this.__fitnessDataConverter.i(a.getString(i6)));
                    arrayList.add(fitnessDataWrapper);
                    b11 = i3;
                    b2 = i4;
                }
                a.close();
                ufVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public void insertFitnessDataList(List<FitnessDataWrapper> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfFitnessDataWrapper.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
