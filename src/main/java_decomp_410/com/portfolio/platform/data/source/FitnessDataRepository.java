package com.portfolio.platform.data.source;

import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ServerFitnessData;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FitnessDataRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = FitnessDataRepository.class.getSimpleName();
        kd4.a((Object) simpleName, "FitnessDataRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public FitnessDataRepository(FitnessDataDao fitnessDataDao, ApiServiceV2 apiServiceV2) {
        kd4.b(fitnessDataDao, "mFitnessDataDao");
        kd4.b(apiServiceV2, "mApiServiceV2");
        this.mFitnessDataDao = fitnessDataDao;
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mFitnessDataDao.deleteAllFitnessData();
    }

    @DexIgnore
    public final List<FitnessDataWrapper> getFitnessData(Date date, Date date2) {
        kd4.b(date, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        kd4.b(date2, "end");
        return this.mFitnessDataDao.getFitnessData(date, date2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object insert$app_fossilRelease(List<FitnessDataWrapper> list, yb4<? super qo2<List<FitnessDataWrapper>>> yb4) {
        throw null;
        // FitnessDataRepository$insert$Anon1 fitnessDataRepository$insert$Anon1;
        // int i;
        // qo2 qo2;
        // if (yb4 instanceof FitnessDataRepository$insert$Anon1) {
        //     fitnessDataRepository$insert$Anon1 = (FitnessDataRepository$insert$Anon1) yb4;
        //     int i2 = fitnessDataRepository$insert$Anon1.label;
        //     if ((i2 & Integer.MIN_VALUE) != 0) {
        //         fitnessDataRepository$insert$Anon1.label = i2 - Integer.MIN_VALUE;
        //         Object obj = fitnessDataRepository$insert$Anon1.result;
        //         Object a = cc4.a();
        //         i = fitnessDataRepository$insert$Anon1.label;
        //         String str = null;
        //         if (i != 0) {
        //             na4.a(obj);
        //             ApiResponse apiResponse = new ApiResponse();
        //             for (FitnessDataWrapper serverFitnessData : list) {
        //                 apiResponse.get_items().add(new ServerFitnessData(serverFitnessData));
        //             }
        //             FitnessDataRepository$insert$repoResponse$Anon1 fitnessDataRepository$insert$repoResponse$Anon1 = new FitnessDataRepository$insert$repoResponse$Anon1(this, apiResponse, (yb4) null);
        //             fitnessDataRepository$insert$Anon1.L$Anon0 = this;
        //             fitnessDataRepository$insert$Anon1.L$Anon1 = list;
        //             fitnessDataRepository$insert$Anon1.L$Anon2 = apiResponse;
        //             fitnessDataRepository$insert$Anon1.label = 1;
        //             obj = ResponseKt.a(fitnessDataRepository$insert$repoResponse$Anon1, fitnessDataRepository$insert$Anon1);
        //             if (obj == a) {
        //                 return a;
        //             }
        //         } else if (i == 1) {
        //             ApiResponse apiResponse2 = (ApiResponse) fitnessDataRepository$insert$Anon1.L$Anon2;
        //             list = (List) fitnessDataRepository$insert$Anon1.L$Anon1;
        //             FitnessDataRepository fitnessDataRepository = (FitnessDataRepository) fitnessDataRepository$insert$Anon1.L$Anon0;
        //             na4.a(obj);
        //         } else {
        //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        //         }
        //         qo2 = (qo2) obj;
        //         if (!(qo2 instanceof ro2)) {
        //             ILocalFLogger local = FLogger.INSTANCE.getLocal();
        //             String str2 = TAG;
        //             local.d(str2, "insert onResponse: response = " + qo2);
        //             return new ro2(list, false, 2, (fd4) null);
        //         } else if (qo2 instanceof po2) {
        //             ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        //             String str3 = TAG;
        //             StringBuilder sb = new StringBuilder();
        //             sb.append("insert Failure code=");
        //             po2 po2 = (po2) qo2;
        //             sb.append(po2.a());
        //             sb.append(" message=");
        //             ServerError c = po2.c();
        //             if (c != null) {
        //                 str = c.getMessage();
        //             }
        //             sb.append(str);
        //             local2.d(str3, sb.toString());
        //             return new po2(po2.a(), po2.c(), po2.d(), po2.b());
        //         } else {
        //             throw new NoWhenBranchMatchedException();
        //         }
        //     }
        // }
        // fitnessDataRepository$insert$Anon1 = new FitnessDataRepository$insert$Anon1(this, yb4);
        // Object obj2 = fitnessDataRepository$insert$Anon1.result;
        // Object a2 = cc4.a();
        // i = fitnessDataRepository$insert$Anon1.label;
        // String str4 = null;
        // if (i != 0) {
        // }
        // qo2 = (qo2) obj2;
        // if (!(qo2 instanceof ro2)) {
        // }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x011b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    public final Object pushFitnessData$app_fossilRelease(List<FitnessDataWrapper> list, yb4<? super qo2<List<FitnessDataWrapper>>> yb4) {
        FitnessDataRepository fitnessDataRepository;
        FitnessDataRepository$pushFitnessData$Anon1 fitnessDataRepository$pushFitnessData$Anon1;
        int i;
        List<FitnessDataWrapper> list2;
        Object obj;
        int i2;
        List list3;
        FitnessDataRepository$pushFitnessData$Anon1 fitnessDataRepository$pushFitnessData$Anon12;
        int i3;
        qo2 qo2;
        FitnessDataRepository fitnessDataRepository2;
        qo2 qo22;
        FitnessDataRepository$pushFitnessData$Anon1 fitnessDataRepository$pushFitnessData$Anon13;
        List<FitnessDataWrapper> list4;
        yb4<? super qo2<List<FitnessDataWrapper>>> yb42 = yb4;
        if (yb42 instanceof FitnessDataRepository$pushFitnessData$Anon1) {
            fitnessDataRepository$pushFitnessData$Anon1 = (FitnessDataRepository$pushFitnessData$Anon1) yb42;
            int i4 = fitnessDataRepository$pushFitnessData$Anon1.label;
            if ((i4 & Integer.MIN_VALUE) != 0) {
                fitnessDataRepository$pushFitnessData$Anon1.label = i4 - Integer.MIN_VALUE;
                fitnessDataRepository = this;
                Object obj2 = fitnessDataRepository$pushFitnessData$Anon1.result;
                Object a = cc4.a();
                i = fitnessDataRepository$pushFitnessData$Anon1.label;
                boolean z = false;
                int i5 = 1;
                if (i != 0) {
                    na4.a(obj2);
                    list3 = new ArrayList();
                    fitnessDataRepository$pushFitnessData$Anon13 = fitnessDataRepository$pushFitnessData$Anon1;
                    fitnessDataRepository2 = fitnessDataRepository;
                    obj = a;
                    qo2 = null;
                    i2 = 0;
                    list4 = list;
                } else if (i == 1) {
                    List list5 = (List) fitnessDataRepository$pushFitnessData$Anon1.L$Anon4;
                    i3 = fitnessDataRepository$pushFitnessData$Anon1.I$Anon1;
                    list3 = (List) fitnessDataRepository$pushFitnessData$Anon1.L$Anon3;
                    i2 = fitnessDataRepository$pushFitnessData$Anon1.I$Anon0;
                    list2 = (List) fitnessDataRepository$pushFitnessData$Anon1.L$Anon1;
                    na4.a(obj2);
                    fitnessDataRepository$pushFitnessData$Anon12 = fitnessDataRepository$pushFitnessData$Anon1;
                    fitnessDataRepository2 = (FitnessDataRepository) fitnessDataRepository$pushFitnessData$Anon1.L$Anon0;
                    obj = a;
                    qo2 = (qo2) fitnessDataRepository$pushFitnessData$Anon1.L$Anon2;
                    qo22 = (qo2) obj2;
                    i2 += 10;
                    if (qo22 instanceof ro2) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = TAG;
                        local.d(str, "pushFitnessData success, bravo!!! startIndex=" + i2 + " endIndex=" + i3);
                        Object a2 = ((ro2) qo22).a();
                        if (a2 != null) {
                            list3.addAll((List) a2);
                            if (i2 >= list2.size()) {
                                fitnessDataRepository2.mFitnessDataDao.deleteFitnessData(list3);
                            }
                            fitnessDataRepository2.mFitnessDataDao.deleteFitnessData(list3);
                            return new ro2(list3, z);
                        }
                        kd4.a();
                        throw null;
                    } else if (qo22 instanceof po2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        local2.d(str2, "pushFitnessData failed, errorCode=" + ((po2) qo22).a() + ' ' + "startIndex=" + i2 + " endIndex=" + i3);
                        if (i2 >= list2.size()) {
                            return qo22;
                        }
                    }
                    fitnessDataRepository$pushFitnessData$Anon13 = fitnessDataRepository$pushFitnessData$Anon12;
                    list4 = list2;
                    z = false;
                    i5 = 1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (i2 < list4.size()) {
                    int i6 = i2 + 10;
                    if (i6 > list4.size()) {
                        i6 = list4.size();
                    }
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local3.d(str3, "pushFitnessData listSize=" + list4.size() + ", startIndex=" + i2 + " endIndex=" + i6);
                    List<FitnessDataWrapper> subList = list4.subList(i2, i6);
                    fitnessDataRepository$pushFitnessData$Anon13.L$Anon0 = fitnessDataRepository2;
                    fitnessDataRepository$pushFitnessData$Anon13.L$Anon1 = list4;
                    fitnessDataRepository$pushFitnessData$Anon13.L$Anon2 = qo2;
                    fitnessDataRepository$pushFitnessData$Anon13.I$Anon0 = i2;
                    fitnessDataRepository$pushFitnessData$Anon13.L$Anon3 = list3;
                    fitnessDataRepository$pushFitnessData$Anon13.I$Anon1 = i6;
                    fitnessDataRepository$pushFitnessData$Anon13.L$Anon4 = subList;
                    fitnessDataRepository$pushFitnessData$Anon13.label = i5;
                    Object insert$app_fossilRelease = fitnessDataRepository2.insert$app_fossilRelease(subList, fitnessDataRepository$pushFitnessData$Anon13);
                    if (insert$app_fossilRelease == obj) {
                        return obj;
                    }
                    int i7 = i6;
                    list2 = list4;
                    obj2 = insert$app_fossilRelease;
                    fitnessDataRepository$pushFitnessData$Anon12 = fitnessDataRepository$pushFitnessData$Anon13;
                    i3 = i7;
                    qo22 = (qo2) obj2;
                    i2 += 10;
                    if (qo22 instanceof ro2) {
                    }
                    fitnessDataRepository$pushFitnessData$Anon13 = fitnessDataRepository$pushFitnessData$Anon12;
                    list4 = list2;
                    z = false;
                    i5 = 1;
                    if (i2 < list4.size()) {
                        return qo2;
                    }
                    return obj;
                }
                return qo2;
            }
        }
        fitnessDataRepository = this;
        fitnessDataRepository$pushFitnessData$Anon1 = new FitnessDataRepository$pushFitnessData$Anon1(fitnessDataRepository, yb42);
        Object obj22 = fitnessDataRepository$pushFitnessData$Anon1.result;
        Object a3 = cc4.a();
        i = fitnessDataRepository$pushFitnessData$Anon1.label;
        boolean z2 = false;
        int i52 = 1;
        if (i != 0) {
        }
        throw null;
        // if (i2 < list4.size()) {
        // }
        // return qo2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object pushPendingFitnessData(yb4<? super qo2<List<FitnessDataWrapper>>> yb4) {
        FitnessDataRepository$pushPendingFitnessData$Anon1 fitnessDataRepository$pushPendingFitnessData$Anon1;
        int i;
        if (yb4 instanceof FitnessDataRepository$pushPendingFitnessData$Anon1) {
            fitnessDataRepository$pushPendingFitnessData$Anon1 = (FitnessDataRepository$pushPendingFitnessData$Anon1) yb4;
            int i2 = fitnessDataRepository$pushPendingFitnessData$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fitnessDataRepository$pushPendingFitnessData$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = fitnessDataRepository$pushPendingFitnessData$Anon1.result;
                Object a = cc4.a();
                i = fitnessDataRepository$pushPendingFitnessData$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "pushPendingFitnessData");
                    List<FitnessDataWrapper> pendingFitnessData = this.mFitnessDataDao.getPendingFitnessData();
                    if (pendingFitnessData.size() <= 0) {
                        throw null;
                        // return new ro2(new ArrayList(), false, 2, (fd4) null);
                    }
                    fitnessDataRepository$pushPendingFitnessData$Anon1.L$Anon0 = this;
                    fitnessDataRepository$pushPendingFitnessData$Anon1.L$Anon1 = pendingFitnessData;
                    fitnessDataRepository$pushPendingFitnessData$Anon1.label = 1;
                    obj = pushFitnessData$app_fossilRelease(pendingFitnessData, fitnessDataRepository$pushPendingFitnessData$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    List list = (List) fitnessDataRepository$pushPendingFitnessData$Anon1.L$Anon1;
                    FitnessDataRepository fitnessDataRepository = (FitnessDataRepository) fitnessDataRepository$pushPendingFitnessData$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return (qo2) obj;
            }
        }
        fitnessDataRepository$pushPendingFitnessData$Anon1 = new FitnessDataRepository$pushPendingFitnessData$Anon1(this, yb4);
        Object obj2 = fitnessDataRepository$pushPendingFitnessData$Anon1.result;
        Object a2 = cc4.a();
        i = fitnessDataRepository$pushPendingFitnessData$Anon1.label;
        if (i != 0) {
        }
        return (qo2) obj2;
    }

    @DexIgnore
    public final void saveFitnessData(List<FitnessDataWrapper> list) {
        kd4.b(list, "fitnessDataList");
        this.mFitnessDataDao.insertFitnessDataList(list);
    }
}
