package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.SleepSessionsRepository$fetchSleepSessions$repoResponse$Anon1", f = "SleepSessionsRepository.kt", l = {172}, m = "invokeSuspend")
public final class SleepSessionsRepository$fetchSleepSessions$repoResponse$Anon1 extends SuspendLambda implements xc4<yb4<? super qr4<xz1>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$fetchSleepSessions$repoResponse$Anon1(SleepSessionsRepository sleepSessionsRepository, Date date, Date date2, int i, int i2, yb4 yb4) {
        super(1, yb4);
        this.this$Anon0 = sleepSessionsRepository;
        this.$start = date;
        this.$end = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    public final yb4<qa4> create(yb4<?> yb4) {
        kd4.b(yb4, "completion");
        return new SleepSessionsRepository$fetchSleepSessions$repoResponse$Anon1(this.this$Anon0, this.$start, this.$end, this.$offset, this.$limit, yb4);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((SleepSessionsRepository$fetchSleepSessions$repoResponse$Anon1) create((yb4) obj)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            ApiServiceV2 access$getMApiService$p = this.this$Anon0.mApiService;
            String e = rk2.e(this.$start);
            kd4.a((Object) e, "DateHelper.formatShortDate(start)");
            String e2 = rk2.e(this.$end);
            kd4.a((Object) e2, "DateHelper.formatShortDate(end)");
            int i2 = this.$offset;
            int i3 = this.$limit;
            this.label = 1;
            obj = access$getMApiService$p.getSleepSessions(e, e2, i2, i3, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
