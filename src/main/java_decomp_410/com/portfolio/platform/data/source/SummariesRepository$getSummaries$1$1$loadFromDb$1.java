package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.SummariesRepository$getSummaries$1$1$loadFromDb$1", mo27670f = "SummariesRepository.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class SummariesRepository$getSummaries$1$1$loadFromDb$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21094p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SummariesRepository$getSummaries$1.C57171 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummaries$1$1$loadFromDb$1(com.portfolio.platform.data.source.SummariesRepository$getSummaries$1.C57171 r1, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = r1;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.SummariesRepository$getSummaries$1$1$loadFromDb$1 summariesRepository$getSummaries$1$1$loadFromDb$1 = new com.portfolio.platform.data.source.SummariesRepository$getSummaries$1$1$loadFromDb$1(this.this$0, yb4);
        summariesRepository$getSummaries$1$1$loadFromDb$1.f21094p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return summariesRepository$getSummaries$1$1$loadFromDb$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.SummariesRepository$getSummaries$1$1$loadFromDb$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.model.room.fitness.ActivitySummary activitySummary = this.this$0.this$0.this$0.mActivitySummaryDao.getActivitySummary(this.this$0.this$0.$endDate);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "todayRawSummary " + activitySummary);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
