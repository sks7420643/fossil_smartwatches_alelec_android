package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.w62;
import com.fossil.blesdk.obfuscated.wf;
import com.fossil.blesdk.obfuscated.z62;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SampleRawDao_Impl extends SampleRawDao {
    @DexIgnore
    public /* final */ w62 __activityIntensitiesConverter; // = new w62();
    @DexIgnore
    public /* final */ z62 __dateLongStringConverter; // = new z62();
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfSampleRaw;
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeleteAllActivitySamples;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<SampleRaw> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sampleraw`(`id`,`pinType`,`uaPinType`,`startTime`,`endTime`,`sourceId`,`sourceTypeValue`,`movementTypeValue`,`steps`,`calories`,`distance`,`activeTime`,`intensityDistInSteps`,`timeZoneID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, SampleRaw sampleRaw) {
            if (sampleRaw.getId() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, sampleRaw.getId());
            }
            kgVar.b(2, (long) sampleRaw.getPinType());
            kgVar.b(3, (long) sampleRaw.getUaPinType());
            String a = SampleRawDao_Impl.this.__dateLongStringConverter.a(sampleRaw.getStartTime());
            if (a == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, a);
            }
            String a2 = SampleRawDao_Impl.this.__dateLongStringConverter.a(sampleRaw.getEndTime());
            if (a2 == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, a2);
            }
            if (sampleRaw.getSourceId() == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, sampleRaw.getSourceId());
            }
            if (sampleRaw.getSourceTypeValue() == null) {
                kgVar.a(7);
            } else {
                kgVar.a(7, sampleRaw.getSourceTypeValue());
            }
            if (sampleRaw.getMovementTypeValue() == null) {
                kgVar.a(8);
            } else {
                kgVar.a(8, sampleRaw.getMovementTypeValue());
            }
            kgVar.a(9, sampleRaw.getSteps());
            kgVar.a(10, sampleRaw.getCalories());
            kgVar.a(11, sampleRaw.getDistance());
            kgVar.b(12, (long) sampleRaw.getActiveTime());
            String a3 = SampleRawDao_Impl.this.__activityIntensitiesConverter.a(sampleRaw.getIntensityDistInSteps());
            if (a3 == null) {
                kgVar.a(13);
            } else {
                kgVar.a(13, a3);
            }
            if (sampleRaw.getTimeZoneID() == null) {
                kgVar.a(14);
            } else {
                kgVar.a(14, sampleRaw.getTimeZoneID());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends wf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM sampleraw";
        }
    }

    @DexIgnore
    public SampleRawDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfSampleRaw = new Anon1(roomDatabase);
        this.__preparedStmtOfDeleteAllActivitySamples = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void deleteAllActivitySamples() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeleteAllActivitySamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySamples.release(acquire);
        }
    }

    @DexIgnore
    public List<SampleRaw> getListActivitySampleByUaType(int i) {
        uf ufVar;
        SampleRawDao_Impl sampleRawDao_Impl = this;
        uf b = uf.b("SELECT * FROM sampleraw WHERE uaPinType = ?", 1);
        b.b(1, (long) i);
        sampleRawDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(sampleRawDao_Impl.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "pinType");
            int b4 = ag.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int b5 = ag.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int b6 = ag.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int b7 = ag.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int b8 = ag.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int b9 = ag.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int b10 = ag.b(a, "steps");
            int b11 = ag.b(a, "calories");
            int b12 = ag.b(a, "distance");
            int b13 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            int b14 = ag.b(a, "intensityDistInSteps");
            ufVar = b;
            try {
                int b15 = ag.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                int i2 = b4;
                int i3 = b3;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i4 = b5;
                    SampleRaw sampleRaw = new SampleRaw(sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b5)), sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9), a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), a.getInt(b13), sampleRawDao_Impl.__activityIntensitiesConverter.a(a.getString(b14)), a.getString(b15));
                    sampleRaw.setId(a.getString(b2));
                    int i5 = i3;
                    int i6 = b2;
                    sampleRaw.setPinType(a.getInt(i5));
                    int i7 = i2;
                    sampleRaw.setUaPinType(a.getInt(i7));
                    arrayList.add(sampleRaw);
                    sampleRawDao_Impl = this;
                    i2 = i7;
                    b2 = i6;
                    i3 = i5;
                    b5 = i4;
                }
                a.close();
                ufVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<SampleRaw> getPendingActivitySamples() {
        uf ufVar;
        SampleRawDao_Impl sampleRawDao_Impl = this;
        uf b = uf.b("SELECT * FROM sampleraw WHERE pinType <> 0", 0);
        sampleRawDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(sampleRawDao_Impl.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "pinType");
            int b4 = ag.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int b5 = ag.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int b6 = ag.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int b7 = ag.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int b8 = ag.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int b9 = ag.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int b10 = ag.b(a, "steps");
            int b11 = ag.b(a, "calories");
            int b12 = ag.b(a, "distance");
            int b13 = ag.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            int b14 = ag.b(a, "intensityDistInSteps");
            ufVar = b;
            try {
                int b15 = ag.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                int i = b4;
                int i2 = b3;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i3 = b5;
                    SampleRaw sampleRaw = new SampleRaw(sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b5)), sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9), a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), a.getInt(b13), sampleRawDao_Impl.__activityIntensitiesConverter.a(a.getString(b14)), a.getString(b15));
                    sampleRaw.setId(a.getString(b2));
                    int i4 = i2;
                    int i5 = b2;
                    sampleRaw.setPinType(a.getInt(i4));
                    int i6 = i;
                    sampleRaw.setUaPinType(a.getInt(i6));
                    arrayList.add(sampleRaw);
                    sampleRawDao_Impl = this;
                    i = i6;
                    b2 = i5;
                    i2 = i4;
                    b5 = i3;
                }
                a.close();
                ufVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public void upsertListActivitySample(List<SampleRaw> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSampleRaw.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<SampleRaw> getPendingActivitySamples(Date date, Date date2) {
        uf ufVar;
        SampleRawDao_Impl sampleRawDao_Impl = this;
        uf b = uf.b("SELECT * FROM sampleraw WHERE startTime >= ? AND startTime < ? AND pinType <> 0", 2);
        String a = sampleRawDao_Impl.__dateLongStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = sampleRawDao_Impl.__dateLongStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        sampleRawDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a3 = bg.a(sampleRawDao_Impl.__db, b, false);
        try {
            int b2 = ag.b(a3, "id");
            int b3 = ag.b(a3, "pinType");
            int b4 = ag.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int b5 = ag.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int b6 = ag.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int b7 = ag.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int b8 = ag.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int b9 = ag.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int b10 = ag.b(a3, "steps");
            int b11 = ag.b(a3, "calories");
            int b12 = ag.b(a3, "distance");
            int b13 = ag.b(a3, SampleDay.COLUMN_ACTIVE_TIME);
            int b14 = ag.b(a3, "intensityDistInSteps");
            ufVar = b;
            try {
                int b15 = ag.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                int i = b4;
                int i2 = b3;
                ArrayList arrayList = new ArrayList(a3.getCount());
                while (a3.moveToNext()) {
                    int i3 = b5;
                    SampleRaw sampleRaw = new SampleRaw(sampleRawDao_Impl.__dateLongStringConverter.a(a3.getString(b5)), sampleRawDao_Impl.__dateLongStringConverter.a(a3.getString(b6)), a3.getString(b7), a3.getString(b8), a3.getString(b9), a3.getDouble(b10), a3.getDouble(b11), a3.getDouble(b12), a3.getInt(b13), sampleRawDao_Impl.__activityIntensitiesConverter.a(a3.getString(b14)), a3.getString(b15));
                    sampleRaw.setId(a3.getString(b2));
                    int i4 = i2;
                    int i5 = b2;
                    sampleRaw.setPinType(a3.getInt(i4));
                    int i6 = i;
                    sampleRaw.setUaPinType(a3.getInt(i6));
                    arrayList.add(sampleRaw);
                    sampleRawDao_Impl = this;
                    i = i6;
                    b2 = i5;
                    i2 = i4;
                    b5 = i3;
                }
                a3.close();
                ufVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a3.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a3.close();
            ufVar.c();
            throw th;
        }
    }
}
