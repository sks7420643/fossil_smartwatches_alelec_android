package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository$saveGoalTrackingDataListToServer$subDeletedGoalTrackingDataListList$Anon1 extends Lambda implements xc4<GoalTrackingData, Boolean> {
    @DexIgnore
    public static /* final */ GoalTrackingRepository$saveGoalTrackingDataListToServer$subDeletedGoalTrackingDataListList$Anon1 INSTANCE; // = new GoalTrackingRepository$saveGoalTrackingDataListToServer$subDeletedGoalTrackingDataListList$Anon1();

    @DexIgnore
    public GoalTrackingRepository$saveGoalTrackingDataListToServer$subDeletedGoalTrackingDataListList$Anon1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(invoke((GoalTrackingData) obj));
    }

    @DexIgnore
    public final boolean invoke(GoalTrackingData goalTrackingData) {
        kd4.b(goalTrackingData, "it");
        return goalTrackingData.getPinType() == 3;
    }
}
