package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalSetting$Anon2", f = "GoalTrackingRepository.kt", l = {}, m = "invokeSuspend")
public final class GoalTrackingRepository$updateGoalSetting$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository.UpdateGoalSettingCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ GoalSetting $goalSetting;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$updateGoalSetting$Anon2(GoalTrackingRepository.UpdateGoalSettingCallback updateGoalSettingCallback, GoalSetting goalSetting, yb4 yb4) {
        super(2, yb4);
        this.$callback = updateGoalSettingCallback;
        this.$goalSetting = goalSetting;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        GoalTrackingRepository$updateGoalSetting$Anon2 goalTrackingRepository$updateGoalSetting$Anon2 = new GoalTrackingRepository$updateGoalSetting$Anon2(this.$callback, this.$goalSetting, yb4);
        goalTrackingRepository$updateGoalSetting$Anon2.p$ = (zg4) obj;
        return goalTrackingRepository$updateGoalSetting$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingRepository$updateGoalSetting$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            GoalTrackingRepository.UpdateGoalSettingCallback updateGoalSettingCallback = this.$callback;
            if (updateGoalSettingCallback == null) {
                return null;
            }
            updateGoalSettingCallback.onSuccess(new ro2(this.$goalSetting, false, 2, (fd4) null));
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
