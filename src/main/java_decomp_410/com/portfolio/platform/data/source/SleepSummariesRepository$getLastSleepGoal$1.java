package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository$getLastSleepGoal$1 extends com.portfolio.platform.util.NetworkBoundResource<java.lang.Integer, com.portfolio.platform.data.model.room.sleep.MFSleepSettings> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSummariesRepository this$0;

    @DexIgnore
    public SleepSummariesRepository$getLastSleepGoal$1(com.portfolio.platform.data.source.SleepSummariesRepository sleepSummariesRepository) {
        this.this$0 = sleepSummariesRepository;
    }

    @DexIgnore
    public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.model.room.sleep.MFSleepSettings>> yb4) {
        return this.this$0.mApiService.getSleepSetting(yb4);
    }

    @DexIgnore
    public androidx.lifecycle.LiveData<java.lang.Integer> loadFromDb() {
        return this.this$0.mSleepDao.getLastSleepGoal();
    }

    @DexIgnore
    public void onFetchFailed(java.lang.Throwable th) {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "getActivitySettings onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(java.lang.Integer num) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(com.portfolio.platform.data.model.room.sleep.MFSleepSettings mFSleepSettings) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(mFSleepSettings, "item");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
        local.mo33255d(tAG$app_fossilRelease, "getActivitySettings saveCallResult goal: " + mFSleepSettings);
        this.this$0.saveSleepSettingToDB$app_fossilRelease(mFSleepSettings.getSleepGoal());
    }
}
