package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppLastSettingRepository {
    @DexIgnore
    public /* final */ WatchAppLastSettingDao mWatchAppLastSettingDao;

    @DexIgnore
    public WatchAppLastSettingRepository(WatchAppLastSettingDao watchAppLastSettingDao) {
        kd4.b(watchAppLastSettingDao, "mWatchAppLastSettingDao");
        this.mWatchAppLastSettingDao = watchAppLastSettingDao;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mWatchAppLastSettingDao.cleanUp();
    }

    @DexIgnore
    public final WatchAppLastSetting getWatchAppLastSetting(String str) {
        kd4.b(str, "id");
        return this.mWatchAppLastSettingDao.getWatchAppLastSetting(str);
    }

    @DexIgnore
    public final void upsertWatchAppLastSetting(WatchAppLastSetting watchAppLastSetting) {
        kd4.b(watchAppLastSetting, "WatchAppLastSetting");
        this.mWatchAppLastSettingDao.upsertWatchAppLastSetting(watchAppLastSetting);
    }
}
