package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.m3;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository$getSummariesPaging$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public static /* final */ GoalTrackingRepository$getSummariesPaging$Anon1 INSTANCE; // = new GoalTrackingRepository$getSummariesPaging$Anon1();

    @DexIgnore
    public final LiveData<NetworkState> apply(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        return goalTrackingSummaryLocalDataSource.getMNetworkState();
    }
}
