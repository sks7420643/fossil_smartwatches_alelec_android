package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmsRepository$upsertAlarm$$inlined$let$lambda$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.yb4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $it;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $serverAlarm$inlined;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21079p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.AlarmsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$upsertAlarm$$inlined$let$lambda$1(java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4, com.portfolio.platform.data.source.AlarmsRepository alarmsRepository, com.fossil.blesdk.obfuscated.yb4 yb42, java.util.List list2) {
        super(2, yb4);
        this.$it = list;
        this.this$0 = alarmsRepository;
        this.$continuation$inlined = yb42;
        this.$serverAlarm$inlined = list2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.AlarmsRepository$upsertAlarm$$inlined$let$lambda$1 alarmsRepository$upsertAlarm$$inlined$let$lambda$1 = new com.portfolio.platform.data.source.AlarmsRepository$upsertAlarm$$inlined$let$lambda$1(this.$it, yb4, this.this$0, this.$continuation$inlined, this.$serverAlarm$inlined);
        alarmsRepository$upsertAlarm$$inlined$let$lambda$1.f21079p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return alarmsRepository$upsertAlarm$$inlined$let$lambda$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.AlarmsRepository$upsertAlarm$$inlined$let$lambda$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            this.this$0.mAlarmsLocalDataSource.insertAlarms(this.$it);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
