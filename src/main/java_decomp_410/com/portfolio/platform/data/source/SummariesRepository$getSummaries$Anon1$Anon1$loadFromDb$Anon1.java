package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1", f = "SummariesRepository.kt", l = {}, m = "invokeSuspend")
public final class SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon1.Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1(SummariesRepository$getSummaries$Anon1.Anon1 anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1 summariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1 = new SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1(this.this$Anon0, yb4);
        summariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1.p$ = (zg4) obj;
        return summariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            ActivitySummary activitySummary = this.this$Anon0.this$Anon0.this$Anon0.mActivitySummaryDao.getActivitySummary(this.this$Anon0.this$Anon0.$endDate);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "todayRawSummary " + activitySummary);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
