package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSummaryRepository$getHeartRateSummaries$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.HeartRateSummaryRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$1$1")
    /* renamed from: com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$1$1 */
    public static final class C57041 extends com.portfolio.platform.util.NetworkBoundResource<java.util.List<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>, com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.blesdk.obfuscated.xz1>> {
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.Pair $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$1 this$0;

        @DexIgnore
        public C57041(com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$1 heartRateSummaryRepository$getHeartRateSummaries$1, kotlin.Pair pair) {
            this.this$0 = heartRateSummaryRepository$getHeartRateSummaries$1;
            this.$downloadingDate = pair;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
            if (r0 != null) goto L_0x0019;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
            if (r0 != null) goto L_0x0033;
         */
        @DexIgnore
        public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.blesdk.obfuscated.xz1>>> yb4) {
            java.util.Date date;
            java.util.Date date2;
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
            kotlin.Pair pair = this.$downloadingDate;
            if (pair != null) {
                date = (java.util.Date) pair.getFirst();
            }
            date = this.this$0.$startDate;
            java.lang.String e = com.fossil.blesdk.obfuscated.rk2.m27397e(date);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            kotlin.Pair pair2 = this.$downloadingDate;
            if (pair2 != null) {
                date2 = (java.util.Date) pair2.getSecond();
            }
            date2 = this.this$0.$endDate;
            java.lang.String e2 = com.fossil.blesdk.obfuscated.rk2.m27397e(date2);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiService$p.getDailyHeartRateSummaries(e, e2, 0, 100, yb4);
        }

        @DexIgnore
        public androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>> loadFromDb() {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
            local.mo33255d(tAG$app_fossilRelease, "getHeartRateSummaries isNotToday startDate = " + this.this$0.$startDate + ", endDate = " + this.this$0.$endDate);
            com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao access$getMHeartRateSummaryDao$p = this.this$0.this$0.mHeartRateSummaryDao;
            com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$1 heartRateSummaryRepository$getHeartRateSummaries$1 = this.this$0;
            return access$getMHeartRateSummaryDao$p.getDailyHeartRateSummariesLiveData(heartRateSummaryRepository$getHeartRateSummaries$1.$startDate, heartRateSummaryRepository$getHeartRateSummaries$1.$endDate);
        }

        @DexIgnore
        public void onFetchFailed(java.lang.Throwable th) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease(), "getHeartRateSummaries onFetchFailed");
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00ab A[Catch:{ Exception -> 0x00fa }] */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00ee A[Catch:{ Exception -> 0x00fa }] */
        /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
        public void saveCallResult(com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.blesdk.obfuscated.xz1> apiResponse) {
            java.util.Date date;
            kotlin.Pair pair;
            java.util.Date date2;
            java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> fitnessData;
            com.fossil.blesdk.obfuscated.kd4.m24411b(apiResponse, "item");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
            local.mo33255d(tAG$app_fossilRelease, "saveCallResult onResponse: response = " + apiResponse);
            try {
                if (!apiResponse.get_items().isEmpty()) {
                    com.fossil.blesdk.obfuscated.rz1 rz1 = new com.fossil.blesdk.obfuscated.rz1();
                    rz1.mo15797a(java.lang.Long.TYPE, new com.portfolio.platform.helper.GsonConvertDateTimeToLong());
                    rz1.mo15797a(org.joda.time.DateTime.class, new com.portfolio.platform.helper.GsonConvertDateTime());
                    rz1.mo15797a(java.util.Date.class, new com.portfolio.platform.helper.GsonConverterShortDate());
                    com.google.gson.Gson a = rz1.mo15799a();
                    java.util.List<com.fossil.blesdk.obfuscated.xz1> list = apiResponse.get_items();
                    java.util.ArrayList arrayList = new java.util.ArrayList(com.fossil.blesdk.obfuscated.db4.m20831a(list, 10));
                    for (com.fossil.blesdk.obfuscated.xz1 a2 : list) {
                        arrayList.add((com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary) a.mo23090a((com.google.gson.JsonElement) a2, new com.portfolio.platform.data.source.C5705x779de442().getType()));
                    }
                    java.util.List d = com.fossil.blesdk.obfuscated.kb4.m24381d(arrayList);
                    com.portfolio.platform.data.source.local.FitnessDataDao access$getMFitnessDataDao$p = this.this$0.this$0.mFitnessDataDao;
                    kotlin.Pair pair2 = this.$downloadingDate;
                    if (pair2 != null) {
                        date = (java.util.Date) pair2.getFirst();
                        if (date != null) {
                            pair = this.$downloadingDate;
                            if (pair != null) {
                                date2 = (java.util.Date) pair.getSecond();
                                if (date2 != null) {
                                    fitnessData = access$getMFitnessDataDao$p.getFitnessData(date, date2);
                                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                                    java.lang.String tAG$app_fossilRelease2 = com.portfolio.platform.data.source.HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
                                    local2.mo33255d(tAG$app_fossilRelease2, "heartrate summary " + d + " fitnessDataSize " + fitnessData.size());
                                    if (fitnessData.isEmpty()) {
                                        this.this$0.this$0.mHeartRateSummaryDao.insertListDailyHeartRateSummary(d);
                                        return;
                                    }
                                    return;
                                }
                            }
                            date2 = this.this$0.$endDate;
                            fitnessData = access$getMFitnessDataDao$p.getFitnessData(date, date2);
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local22 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String tAG$app_fossilRelease22 = com.portfolio.platform.data.source.HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
                            local22.mo33255d(tAG$app_fossilRelease22, "heartrate summary " + d + " fitnessDataSize " + fitnessData.size());
                            if (fitnessData.isEmpty()) {
                            }
                        }
                    }
                    date = this.this$0.$startDate;
                    pair = this.$downloadingDate;
                    if (pair != null) {
                    }
                    date2 = this.this$0.$endDate;
                    fitnessData = access$getMFitnessDataDao$p.getFitnessData(date, date2);
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local222 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String tAG$app_fossilRelease222 = com.portfolio.platform.data.source.HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
                    local222.mo33255d(tAG$app_fossilRelease222, "heartrate summary " + d + " fitnessDataSize " + fitnessData.size());
                    if (fitnessData.isEmpty()) {
                    }
                }
            } catch (java.lang.Exception e) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String tAG$app_fossilRelease3 = com.portfolio.platform.data.source.HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append("saveCallResult exception=");
                e.printStackTrace();
                sb.append(com.fossil.blesdk.obfuscated.qa4.f17909a);
                local3.mo33256e(tAG$app_fossilRelease3, sb.toString());
            }
        }

        @DexIgnore
        public boolean shouldFetch(java.util.List<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary> list) {
            return this.this$0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public HeartRateSummaryRepository$getHeartRateSummaries$1(com.portfolio.platform.data.source.HeartRateSummaryRepository heartRateSummaryRepository, java.util.Date date, java.util.Date date2, boolean z) {
        this.this$0 = heartRateSummaryRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public final androidx.lifecycle.LiveData<com.fossil.blesdk.obfuscated.os3<java.util.List<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>>> apply(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> list) {
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) list, "fitnessDataList");
        return new com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$1.C57041(this, com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt.calculateRangeDownload(list, this.$startDate, this.$endDate)).asLiveData();
    }
}
