package com.portfolio.platform.data.source;

import android.content.Context;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.facebook.internal.AnalyticsEvents;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.vk2;
import com.fossil.blesdk.obfuscated.vm2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.diana.preset.Background;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.source.local.diana.WatchFaceDao;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchFaceRepository {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public /* final */ WatchFaceDao watchFaceDao;
    @DexIgnore
    public /* final */ WatchFaceRemoteDataSource watchFaceRemoteDataSource;

    @DexIgnore
    public WatchFaceRepository(Context context2, WatchFaceDao watchFaceDao2, WatchFaceRemoteDataSource watchFaceRemoteDataSource2) {
        kd4.b(context2, "context");
        kd4.b(watchFaceDao2, "watchFaceDao");
        kd4.b(watchFaceRemoteDataSource2, "watchFaceRemoteDataSource");
        this.context = context2;
        this.watchFaceDao = watchFaceDao2;
        this.watchFaceRemoteDataSource = watchFaceRemoteDataSource2;
        String simpleName = WatchFaceRepository.class.getSimpleName();
        kd4.a((Object) simpleName, "WatchFaceRepository::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    private final void downloadFromURL(String str, String str2, List<String> list) {
        if (TextUtils.isEmpty(str)) {
            return;
        }
        if (str != null) {
            String str3 = str2 + File.separator + vm2.a(str);
            if (!vk2.a.a(str3) && !list.contains(str)) {
                list.add(str);
                processDownloadAndStore(str, str3);
                return;
            }
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    private final gh4<Boolean> processDownloadAndStore(String str, String str2) {
        throw null;
        // return ag4.a(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new WatchFaceRepository$processDownloadAndStore$Anon1(this, str, str2, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void deleteWatchFacesWithSerial(String str) {
        kd4.b(str, "serial");
        this.watchFaceDao.deleteWatchFacesWithSerial(str);
    }

    @DexIgnore
    public final Context getContext() {
        return this.context;
    }

    @DexIgnore
    public final WatchFace getWatchFaceWithId(String str) {
        kd4.b(str, "id");
        return this.watchFaceDao.getWatchFaceWithId(str);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object getWatchFacesFromServer(String str, yb4<? super qa4> yb4) {
        WatchFaceRepository$getWatchFacesFromServer$Anon1 watchFaceRepository$getWatchFacesFromServer$Anon1;
        int i;
        WatchFaceRepository watchFaceRepository;
        qo2 qo2;
        if (yb4 instanceof WatchFaceRepository$getWatchFacesFromServer$Anon1) {
            watchFaceRepository$getWatchFacesFromServer$Anon1 = (WatchFaceRepository$getWatchFacesFromServer$Anon1) yb4;
            int i2 = watchFaceRepository$getWatchFacesFromServer$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                watchFaceRepository$getWatchFacesFromServer$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = watchFaceRepository$getWatchFacesFromServer$Anon1.result;
                Object a = cc4.a();
                i = watchFaceRepository$getWatchFacesFromServer$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    WatchFaceRemoteDataSource watchFaceRemoteDataSource2 = this.watchFaceRemoteDataSource;
                    watchFaceRepository$getWatchFacesFromServer$Anon1.L$Anon0 = this;
                    watchFaceRepository$getWatchFacesFromServer$Anon1.L$Anon1 = str;
                    watchFaceRepository$getWatchFacesFromServer$Anon1.label = 1;
                    obj = watchFaceRemoteDataSource2.getWatchFacesFromServer(str, watchFaceRepository$getWatchFacesFromServer$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    watchFaceRepository = this;
                } else if (i == 1) {
                    str = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$Anon1;
                    watchFaceRepository = (WatchFaceRepository) watchFaceRepository$getWatchFacesFromServer$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ArrayList<WatchFace> arrayList = (ArrayList) ((ro2) qo2).a();
                    if (arrayList != null) {
                        List<WatchFace> watchFacesWithSerial = watchFaceRepository.getWatchFacesWithSerial(str);
                        ArrayList arrayList2 = new ArrayList(db4.a(arrayList, 10));
                        for (WatchFace serial : arrayList) {
                            serial.setSerial(str);
                            arrayList2.add(qa4.a);
                        }
                        if (!kd4.a((Object) watchFacesWithSerial, (Object) arrayList)) {
                            watchFaceRepository.watchFaceDao.insertAllWatchFaces(arrayList);
                        }
                        String file = watchFaceRepository.context.getFilesDir().toString();
                        kd4.a((Object) file, "context.filesDir.toString()");
                        ArrayList arrayList3 = new ArrayList();
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            WatchFace watchFace = (WatchFace) it.next();
                            ArrayList<RingStyleItem> component3 = watchFace.component3();
                            Background component4 = watchFace.component4();
                            String component5 = watchFace.component5();
                            watchFaceRepository.downloadFromURL(component4.getData().getUrl(), file, arrayList3);
                            watchFaceRepository.downloadFromURL(component4.getData().getPreviewUrl(), file, arrayList3);
                            watchFaceRepository.downloadFromURL(component5, file, arrayList3);
                            if (component3 != null) {
                                for (RingStyleItem ringStyleItem : component3) {
                                    watchFaceRepository.downloadFromURL(ringStyleItem.getRingStyle().getData().getUrl(), file, arrayList3);
                                    watchFaceRepository.downloadFromURL(ringStyleItem.getRingStyle().getData().getPreviewUrl(), file, arrayList3);
                                }
                            }
                        }
                    }
                } else {
                    FLogger.INSTANCE.getLocal().e(watchFaceRepository.TAG, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_FAILED);
                }
                return qa4.a;
            }
        }
        watchFaceRepository$getWatchFacesFromServer$Anon1 = new WatchFaceRepository$getWatchFacesFromServer$Anon1(this, yb4);
        Object obj2 = watchFaceRepository$getWatchFacesFromServer$Anon1.result;
        Object a2 = cc4.a();
        i = watchFaceRepository$getWatchFacesFromServer$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
        return qa4.a;
    }

    @DexIgnore
    public final LiveData<List<WatchFace>> getWatchFacesLiveDataWithSerial(String str) {
        kd4.b(str, "serial");
        return this.watchFaceDao.getWatchFacesLiveData(str);
    }

    @DexIgnore
    public final List<WatchFace> getWatchFacesWithSerial(String str) {
        kd4.b(str, "serial");
        return this.watchFaceDao.getWatchFacesWithSerial(str);
    }
}
