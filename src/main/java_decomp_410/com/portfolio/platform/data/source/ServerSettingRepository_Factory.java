package com.portfolio.platform.data.source;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ServerSettingRepository_Factory implements Factory<ServerSettingRepository> {
    @DexIgnore
    public /* final */ Provider<ServerSettingDataSource> mServerSettingLocalDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<ServerSettingDataSource> mServerSettingRemoteDataSourceProvider;

    @DexIgnore
    public ServerSettingRepository_Factory(Provider<ServerSettingDataSource> provider, Provider<ServerSettingDataSource> provider2) {
        this.mServerSettingLocalDataSourceProvider = provider;
        this.mServerSettingRemoteDataSourceProvider = provider2;
    }

    @DexIgnore
    public static ServerSettingRepository_Factory create(Provider<ServerSettingDataSource> provider, Provider<ServerSettingDataSource> provider2) {
        return new ServerSettingRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static ServerSettingRepository newServerSettingRepository(ServerSettingDataSource serverSettingDataSource, ServerSettingDataSource serverSettingDataSource2) {
        return new ServerSettingRepository(serverSettingDataSource, serverSettingDataSource2);
    }

    @DexIgnore
    public static ServerSettingRepository provideInstance(Provider<ServerSettingDataSource> provider, Provider<ServerSettingDataSource> provider2) {
        return new ServerSettingRepository(provider.get(), provider2.get());
    }

    @DexIgnore
    public ServerSettingRepository get() {
        return provideInstance(this.mServerSettingLocalDataSourceProvider, this.mServerSettingRemoteDataSourceProvider);
    }
}
