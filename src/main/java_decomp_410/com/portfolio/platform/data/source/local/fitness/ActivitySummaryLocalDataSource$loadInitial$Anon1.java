package com.portfolio.platform.data.source.local.fitness;

import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitySummaryLocalDataSource$loadInitial$Anon1 implements PagingRequestHelper.b {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$Anon0;

    @DexIgnore
    public ActivitySummaryLocalDataSource$loadInitial$Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.this$Anon0 = activitySummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(PagingRequestHelper.b.a aVar) {
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource = this.this$Anon0;
        activitySummaryLocalDataSource.calculateStartDate(activitySummaryLocalDataSource.mCreatedDate);
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource2 = this.this$Anon0;
        PagingRequestHelper.RequestType requestType = PagingRequestHelper.RequestType.INITIAL;
        Date mStartDate = activitySummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$Anon0.getMEndDate();
        kd4.a((Object) aVar, "helperCallback");
        activitySummaryLocalDataSource2.loadData(requestType, mStartDate, mEndDate, aVar);
    }
}
