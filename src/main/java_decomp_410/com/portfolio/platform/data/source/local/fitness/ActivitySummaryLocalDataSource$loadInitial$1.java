package com.portfolio.platform.data.source.local.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitySummaryLocalDataSource$loadInitial$1 implements com.portfolio.platform.helper.PagingRequestHelper.C5952b {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource this$0;

    @DexIgnore
    public ActivitySummaryLocalDataSource$loadInitial$1(com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.this$0 = activitySummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a aVar) {
        com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource activitySummaryLocalDataSource = this.this$0;
        activitySummaryLocalDataSource.calculateStartDate(activitySummaryLocalDataSource.mCreatedDate);
        com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource activitySummaryLocalDataSource2 = this.this$0;
        com.portfolio.platform.helper.PagingRequestHelper.RequestType requestType = com.portfolio.platform.helper.PagingRequestHelper.RequestType.INITIAL;
        java.util.Date mStartDate = activitySummaryLocalDataSource2.getMStartDate();
        java.util.Date mEndDate = this.this$0.getMEndDate();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) aVar, "helperCallback");
        activitySummaryLocalDataSource2.loadData(requestType, mStartDate, mEndDate, aVar);
    }
}
