package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.ServerSettingRepository$getServerSettingList$1$onSuccess$1", mo27670f = "ServerSettingRepository.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class ServerSettingRepository$getServerSettingList$1$onSuccess$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.ServerSettingList $serverSettingList;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21089p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ServerSettingRepository$getServerSettingList$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ServerSettingRepository$getServerSettingList$1$onSuccess$1(com.portfolio.platform.data.source.ServerSettingRepository$getServerSettingList$1 serverSettingRepository$getServerSettingList$1, com.portfolio.platform.data.model.ServerSettingList serverSettingList, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = serverSettingRepository$getServerSettingList$1;
        this.$serverSettingList = serverSettingList;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.ServerSettingRepository$getServerSettingList$1$onSuccess$1 serverSettingRepository$getServerSettingList$1$onSuccess$1 = new com.portfolio.platform.data.source.ServerSettingRepository$getServerSettingList$1$onSuccess$1(this.this$0, this.$serverSettingList, yb4);
        serverSettingRepository$getServerSettingList$1$onSuccess$1.f21089p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return serverSettingRepository$getServerSettingList$1$onSuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.ServerSettingRepository$getServerSettingList$1$onSuccess$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.ServerSettingRepository.Companion.getTAG$app_fossilRelease();
            local.mo33255d(tAG$app_fossilRelease, "getServerSettingList - onSuccess - serverSettingList = [ " + this.$serverSettingList + " ]");
            com.portfolio.platform.data.model.ServerSettingList serverSettingList = this.$serverSettingList;
            if (serverSettingList != null) {
                java.util.List<com.portfolio.platform.data.model.ServerSetting> serverSettings = serverSettingList.getServerSettings();
                java.lang.Boolean a = serverSettings != null ? com.fossil.blesdk.obfuscated.dc4.m20839a(!serverSettings.isEmpty()) : null;
                if (a == null) {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                } else if (a.booleanValue()) {
                    this.this$0.this$0.mServerSettingLocalDataSource.addOrUpdateServerSettingList(this.$serverSettingList.getServerSettings());
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
