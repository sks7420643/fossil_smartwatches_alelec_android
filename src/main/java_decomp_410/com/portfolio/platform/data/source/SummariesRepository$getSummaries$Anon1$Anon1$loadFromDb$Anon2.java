package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon2<I, O> implements m3<X, Y> {
    @DexIgnore
    public /* final */ /* synthetic */ LiveData $resultList;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon1.Anon1 this$Anon0;

    @DexIgnore
    public SummariesRepository$getSummaries$Anon1$Anon1$loadFromDb$Anon2(SummariesRepository$getSummaries$Anon1.Anon1 anon1, LiveData liveData) {
        this.this$Anon0 = anon1;
        this.$resultList = liveData;
    }

    @DexIgnore
    public final List<ActivitySummary> apply(List<ActivitySummary> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getSummaries - loadFromDb -- isToday - resultList=" + ((List) this.$resultList.a()));
        kd4.a((Object) list, "activitySummaries");
        if (!list.isEmpty()) {
            ((ActivitySummary) kb4.f(list)).setSteps(Math.max((double) this.this$Anon0.this$Anon0.this$Anon0.mFitnessHelper.a(new Date()), ((ActivitySummary) kb4.f(list)).getSteps()));
        }
        return list;
    }
}
