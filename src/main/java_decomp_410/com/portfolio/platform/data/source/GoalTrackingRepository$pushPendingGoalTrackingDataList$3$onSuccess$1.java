package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(c = "com.portfolio.platform.data.source.GoalTrackingRepository$pushPendingGoalTrackingDataList$3$onSuccess$1", f = "GoalTrackingRepository.kt", l = {637}, m = "invokeSuspend")
public final class GoalTrackingRepository$pushPendingGoalTrackingDataList$3$onSuccess$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $startDate;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.GoalTrackingRepository$pushPendingGoalTrackingDataList$3 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$pushPendingGoalTrackingDataList$3$onSuccess$1(com.portfolio.platform.data.source.GoalTrackingRepository$pushPendingGoalTrackingDataList$3 goalTrackingRepository$pushPendingGoalTrackingDataList$3, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef, kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = goalTrackingRepository$pushPendingGoalTrackingDataList$3;
        this.$startDate = ref$ObjectRef;
        this.$endDate = ref$ObjectRef2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.b(yb4, "completion");
        com.portfolio.platform.data.source.GoalTrackingRepository$pushPendingGoalTrackingDataList$3$onSuccess$1 goalTrackingRepository$pushPendingGoalTrackingDataList$3$onSuccess$1 = new com.portfolio.platform.data.source.GoalTrackingRepository$pushPendingGoalTrackingDataList$3$onSuccess$1(this.this$0, this.$startDate, this.$endDate, yb4);
        goalTrackingRepository$pushPendingGoalTrackingDataList$3$onSuccess$1.p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return goalTrackingRepository$pushPendingGoalTrackingDataList$3$onSuccess$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.GoalTrackingRepository$pushPendingGoalTrackingDataList$3$onSuccess$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.a(obj);
            this.L$0 = this.p$;
            this.label = 1;
            if (this.this$0.this$0.loadSummaries((java.util.Date) this.$startDate.element, (java.util.Date) this.$endDate.element, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.a;
    }
}
