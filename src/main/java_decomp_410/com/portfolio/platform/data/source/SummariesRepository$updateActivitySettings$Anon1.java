package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.jk2;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.util.NetworkBoundResource;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$updateActivitySettings$Anon1 extends NetworkBoundResource<ActivitySettings, ActivitySettings> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySettings $activitySettings;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$Anon0;

    @DexIgnore
    public SummariesRepository$updateActivitySettings$Anon1(SummariesRepository summariesRepository, ActivitySettings activitySettings) {
        this.this$Anon0 = summariesRepository;
        this.$activitySettings = activitySettings;
    }

    @DexIgnore
    public Object createCall(yb4<? super qr4<ActivitySettings>> yb4) {
        rz1 rz1 = new rz1();
        rz1.b(new jk2());
        JsonElement b = rz1.a().b((Object) this.$activitySettings);
        kd4.a((Object) b, "GsonBuilder()\n          \u2026sonTree(activitySettings)");
        xz1 d = b.d();
        ApiServiceV2 access$getMApiServiceV2$p = this.this$Anon0.mApiServiceV2;
        kd4.a((Object) d, "jsonObject");
        return access$getMApiServiceV2$p.updateActivitySetting(d, yb4);
    }

    @DexIgnore
    public LiveData<ActivitySettings> loadFromDb() {
        return this.this$Anon0.mActivitySummaryDao.getActivitySettingLiveData();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().e(SummariesRepository.TAG, "updateActivitySettings - onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(ActivitySettings activitySettings) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(ActivitySettings activitySettings) {
        kd4.b(activitySettings, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "updateActivitySettings - saveCallResult -- item=" + activitySettings);
        fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new SummariesRepository$updateActivitySettings$Anon1$saveCallResult$Anon1(this, activitySettings, (yb4) null), 3, (Object) null);
    }
}
