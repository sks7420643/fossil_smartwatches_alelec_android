package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository$getSleepSummary$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends NetworkBoundResource<MFSleepDay, xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository$getSleepSummary$Anon1 this$Anon0;

        @DexIgnore
        public Anon1(SleepSummariesRepository$getSleepSummary$Anon1 sleepSummariesRepository$getSleepSummary$Anon1, List list) {
            this.this$Anon0 = sleepSummariesRepository$getSleepSummary$Anon1;
            this.$fitnessDataList = list;
        }

        @DexIgnore
        public Object createCall(yb4<? super qr4<xz1>> yb4) {
            Date n = rk2.n(this.this$Anon0.$date);
            Date i = rk2.i(this.this$Anon0.$date);
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "calendar");
            instance.setTimeInMillis(0);
            ApiServiceV2 access$getMApiService$p = this.this$Anon0.this$Anon0.mApiService;
            String e = rk2.e(n);
            kd4.a((Object) e, "DateHelper.formatShortDate(startDate)");
            String e2 = rk2.e(i);
            kd4.a((Object) e2, "DateHelper.formatShortDate(endDate)");
            return access$getMApiService$p.getSleepSummaries(e, e2, 0, 100, yb4);
        }

        @DexIgnore
        public LiveData<MFSleepDay> loadFromDb() {
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "calendar");
            instance.setTime(this.this$Anon0.$date);
            SleepDao access$getMSleepDao$p = this.this$Anon0.this$Anon0.mSleepDao;
            String e = rk2.e(this.this$Anon0.$date);
            kd4.a((Object) e, "DateHelper.formatShortDate(date)");
            return access$getMSleepDao$p.getSleepDayLiveData(e);
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "getActivityList onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(xz1 xz1) {
            kd4.b(xz1, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getSleepSummary saveCallResult onResponse: response = " + xz1);
            SleepSummariesRepository$getSleepSummary$Anon1 sleepSummariesRepository$getSleepSummary$Anon1 = this.this$Anon0;
            sleepSummariesRepository$getSleepSummary$Anon1.this$Anon0.saveSleepSummary$app_fossilRelease(xz1, sleepSummariesRepository$getSleepSummary$Anon1.$date);
        }

        @DexIgnore
        public boolean shouldFetch(MFSleepDay mFSleepDay) {
            return this.$fitnessDataList.isEmpty();
        }
    }

    @DexIgnore
    public SleepSummariesRepository$getSleepSummary$Anon1(SleepSummariesRepository sleepSummariesRepository, Date date) {
        this.this$Anon0 = sleepSummariesRepository;
        this.$date = date;
    }

    @DexIgnore
    public final LiveData<os3<MFSleepDay>> apply(List<FitnessDataWrapper> list) {
        return new Anon1(this, list).asLiveData();
    }
}
