package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.AlarmsRepository$downloadAlarms$2", mo27670f = "AlarmsRepository.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class AlarmsRepository$downloadAlarms$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.qo2 $alarmsResponse;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21081p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.AlarmsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$downloadAlarms$2(com.portfolio.platform.data.source.AlarmsRepository alarmsRepository, com.fossil.blesdk.obfuscated.qo2 qo2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = alarmsRepository;
        this.$alarmsResponse = qo2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.AlarmsRepository$downloadAlarms$2 alarmsRepository$downloadAlarms$2 = new com.portfolio.platform.data.source.AlarmsRepository$downloadAlarms$2(this.this$0, this.$alarmsResponse, yb4);
        alarmsRepository$downloadAlarms$2.f21081p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return alarmsRepository$downloadAlarms$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.AlarmsRepository$downloadAlarms$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            if (!((com.fossil.blesdk.obfuscated.ro2) this.$alarmsResponse).mo30674b()) {
                java.util.List<com.portfolio.platform.data.source.local.alarm.Alarm> list = (java.util.List) ((com.fossil.blesdk.obfuscated.ro2) this.$alarmsResponse).mo30673a();
                if (list != null) {
                    for (com.portfolio.platform.data.source.local.alarm.Alarm alarm : list) {
                        if (com.portfolio.platform.helper.AlarmHelper.f21153f.mo39490a(alarm)) {
                            alarm.setActive(false);
                            alarm.setPinType(2);
                        }
                    }
                    this.this$0.mAlarmsLocalDataSource.insertAlarms(list);
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
