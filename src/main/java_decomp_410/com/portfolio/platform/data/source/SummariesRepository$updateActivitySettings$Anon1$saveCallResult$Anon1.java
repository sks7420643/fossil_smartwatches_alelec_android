package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import java.util.Date;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.SummariesRepository$updateActivitySettings$Anon1$saveCallResult$Anon1", f = "SummariesRepository.kt", l = {}, m = "invokeSuspend")
public final class SummariesRepository$updateActivitySettings$Anon1$saveCallResult$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySettings $item;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$updateActivitySettings$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$updateActivitySettings$Anon1$saveCallResult$Anon1(SummariesRepository$updateActivitySettings$Anon1 summariesRepository$updateActivitySettings$Anon1, ActivitySettings activitySettings, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = summariesRepository$updateActivitySettings$Anon1;
        this.$item = activitySettings;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        SummariesRepository$updateActivitySettings$Anon1$saveCallResult$Anon1 summariesRepository$updateActivitySettings$Anon1$saveCallResult$Anon1 = new SummariesRepository$updateActivitySettings$Anon1$saveCallResult$Anon1(this.this$Anon0, this.$item, yb4);
        summariesRepository$updateActivitySettings$Anon1$saveCallResult$Anon1.p$ = (zg4) obj;
        return summariesRepository$updateActivitySettings$Anon1$saveCallResult$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SummariesRepository$updateActivitySettings$Anon1$saveCallResult$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            this.this$Anon0.this$Anon0.saveActivitySettingsToDB$app_fossilRelease(new Date(), this.$item);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
