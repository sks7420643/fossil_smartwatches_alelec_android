package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.yb4;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource", f = "HybridPresetRemoteDataSource.kt", l = {25}, m = "downloadHybridRecommendPresetList")
public final class HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1 extends ContinuationImpl {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ HybridPresetRemoteDataSource this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridPresetRemoteDataSource$downloadHybridRecommendPresetList$Anon1(HybridPresetRemoteDataSource hybridPresetRemoteDataSource, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = hybridPresetRemoteDataSource;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.downloadHybridRecommendPresetList((String) null, this);
    }
}
