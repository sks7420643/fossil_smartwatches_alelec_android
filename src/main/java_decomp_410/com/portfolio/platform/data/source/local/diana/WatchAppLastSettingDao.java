package com.portfolio.platform.data.source.local.diana;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface WatchAppLastSettingDao {
    @DexIgnore
    void cleanUp();

    @DexIgnore
    void deleteWatchAppLastSettingById(String str);

    @DexIgnore
    List<WatchAppLastSetting> getAllWatchAppLastSetting();

    @DexIgnore
    LiveData<List<WatchAppLastSetting>> getAllWatchAppLastSettingAsLiveData();

    @DexIgnore
    WatchAppLastSetting getWatchAppLastSetting(String str);

    @DexIgnore
    void upsertWatchAppLastSetting(WatchAppLastSetting watchAppLastSetting);
}
