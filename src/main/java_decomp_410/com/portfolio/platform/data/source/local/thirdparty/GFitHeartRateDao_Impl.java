package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GFitHeartRateDao_Impl implements GFitHeartRateDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ kf __deletionAdapterOfGFitHeartRate;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfGFitHeartRate;
    @DexIgnore
    public /* final */ wf __preparedStmtOfClearAll;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<GFitHeartRate> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitHeartRate`(`id`,`value`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, GFitHeartRate gFitHeartRate) {
            kgVar.b(1, (long) gFitHeartRate.getId());
            kgVar.a(2, (double) gFitHeartRate.getValue());
            kgVar.b(3, gFitHeartRate.getStartTime());
            kgVar.b(4, gFitHeartRate.getEndTime());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends kf<GFitHeartRate> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `gFitHeartRate` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(kg kgVar, GFitHeartRate gFitHeartRate) {
            kgVar.b(1, (long) gFitHeartRate.getId());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends wf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM gFitHeartRate";
        }
    }

    @DexIgnore
    public GFitHeartRateDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfGFitHeartRate = new Anon1(roomDatabase);
        this.__deletionAdapterOfGFitHeartRate = new Anon2(roomDatabase);
        this.__preparedStmtOfClearAll = new Anon3(roomDatabase);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public void deleteListGFitHeartRate(List<GFitHeartRate> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitHeartRate.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<GFitHeartRate> getAllGFitHeartRate() {
        uf b = uf.b("SELECT * FROM gFitHeartRate", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "value");
            int b4 = ag.b(a, SampleRaw.COLUMN_START_TIME);
            int b5 = ag.b(a, SampleRaw.COLUMN_END_TIME);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GFitHeartRate gFitHeartRate = new GFitHeartRate(a.getFloat(b3), a.getLong(b4), a.getLong(b5));
                gFitHeartRate.setId(a.getInt(b2));
                arrayList.add(gFitHeartRate);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertGFitHeartRate(GFitHeartRate gFitHeartRate) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitHeartRate.insert(gFitHeartRate);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListGFitHeartRate(List<GFitHeartRate> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitHeartRate.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
