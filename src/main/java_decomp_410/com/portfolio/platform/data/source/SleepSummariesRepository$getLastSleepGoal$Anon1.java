package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.util.NetworkBoundResource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository$getLastSleepGoal$Anon1 extends NetworkBoundResource<Integer, MFSleepSettings> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$Anon0;

    @DexIgnore
    public SleepSummariesRepository$getLastSleepGoal$Anon1(SleepSummariesRepository sleepSummariesRepository) {
        this.this$Anon0 = sleepSummariesRepository;
    }

    @DexIgnore
    public Object createCall(yb4<? super qr4<MFSleepSettings>> yb4) {
        return this.this$Anon0.mApiService.getSleepSetting(yb4);
    }

    @DexIgnore
    public LiveData<Integer> loadFromDb() {
        return this.this$Anon0.mSleepDao.getLastSleepGoal();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.Companion.getTAG$app_fossilRelease(), "getActivitySettings onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(Integer num) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(MFSleepSettings mFSleepSettings) {
        kd4.b(mFSleepSettings, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tAG$app_fossilRelease = SleepSummariesRepository.Companion.getTAG$app_fossilRelease();
        local.d(tAG$app_fossilRelease, "getActivitySettings saveCallResult goal: " + mFSleepSettings);
        this.this$Anon0.saveSleepSettingToDB$app_fossilRelease(mFSleepSettings.getSleepGoal());
    }
}
