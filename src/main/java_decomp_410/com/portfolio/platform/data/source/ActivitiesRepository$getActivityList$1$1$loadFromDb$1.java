package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivitiesRepository$getActivityList$1$1$loadFromDb$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$1.C56891 this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$1$1$loadFromDb$1$1")
    /* renamed from: com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$1$1$loadFromDb$1$1 */
    public static final class C56901<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, Y> {
        @DexIgnore
        public /* final */ /* synthetic */ java.util.List $resultList;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$1$1$loadFromDb$1 this$0;

        @DexIgnore
        public C56901(com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$1$1$loadFromDb$1 activitiesRepository$getActivityList$1$1$loadFromDb$1, java.util.List list) {
            this.this$0 = activitiesRepository$getActivityList$1$1$loadFromDb$1;
            this.$resultList = list;
        }

        @DexIgnore
        public final java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample> apply(java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample> list) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.ActivitiesRepository.Companion.getTAG$app_fossilRelease();
            local.mo33255d(tAG$app_fossilRelease, "getActivityList getActivityList Transformations activitySamples=" + list);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) list, "activitySamples");
            if (!list.isEmpty()) {
                java.util.List list2 = this.$resultList;
                this.this$0.this$0.this$0.this$0.mFitnessHelper.mo32684a(list, list.get(0).getUid());
                list2.addAll(list);
            }
            return this.$resultList;
        }
    }

    @DexIgnore
    public ActivitiesRepository$getActivityList$1$1$loadFromDb$1(com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$1.C56891 r1) {
        this.this$0 = r1;
    }

    @DexIgnore
    public final androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample>> apply(java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample> list) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.ActivitiesRepository.Companion.getTAG$app_fossilRelease();
        local.mo33255d(tAG$app_fossilRelease, "getActivityList loadFromDb: resultList=" + list);
        com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$1 activitiesRepository$getActivityList$1 = this.this$0.this$0;
        com.portfolio.platform.data.source.ActivitiesRepository activitiesRepository = activitiesRepository$getActivityList$1.this$0;
        java.util.Date date = activitiesRepository$getActivityList$1.$endDate;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_END_DATE);
        return com.fossil.blesdk.obfuscated.C1935hc.m7843a(activitiesRepository.getActivitySamplesInDate$app_fossilRelease(date), new com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$1$1$loadFromDb$1.C56901(this, list));
    }
}
