package com.portfolio.platform.data.source.local.hybrid.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingSummaryLocalDataSource$loadInitial$1 implements com.portfolio.platform.helper.PagingRequestHelper.C5952b {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource this$0;

    @DexIgnore
    public GoalTrackingSummaryLocalDataSource$loadInitial$1(com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        this.this$0 = goalTrackingSummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a aVar) {
        com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource = this.this$0;
        goalTrackingSummaryLocalDataSource.calculateStartDate(goalTrackingSummaryLocalDataSource.mCreatedDate);
        com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource2 = this.this$0;
        com.portfolio.platform.helper.PagingRequestHelper.RequestType requestType = com.portfolio.platform.helper.PagingRequestHelper.RequestType.INITIAL;
        java.util.Date mStartDate = goalTrackingSummaryLocalDataSource2.getMStartDate();
        java.util.Date mEndDate = this.this$0.getMEndDate();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) aVar, "helperCallback");
        com.fossil.blesdk.obfuscated.fi4 unused = goalTrackingSummaryLocalDataSource2.loadData(requestType, mStartDate, mEndDate, aVar);
    }
}
