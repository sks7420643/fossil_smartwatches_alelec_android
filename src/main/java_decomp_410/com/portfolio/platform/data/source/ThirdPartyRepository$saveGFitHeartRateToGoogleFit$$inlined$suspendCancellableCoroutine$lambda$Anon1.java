package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dg4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.ge0;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.me0;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.ne0;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.List;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$IntRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1<R extends me0> implements ne0<Status> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ dg4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Ref$IntRef $countSizeOfList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $gFitHeartRateList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ge0 $googleApiClient$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $sampleList;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfGFitList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$Anon0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1$Anon1$Anon1")
        /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1$Anon1$Anon1  reason: collision with other inner class name */
        public static final class C0114Anon1 implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Anon1 this$Anon0;

            @DexIgnore
            public C0114Anon1(Anon1 anon1) {
                this.this$Anon0 = anon1;
            }

            @DexIgnore
            public final void run() {
                this.this$Anon0.this$Anon0.this$Anon0.getMThirdPartyDatabase().getGFitHeartRateDao().deleteListGFitHeartRate(this.this$Anon0.this$Anon0.$sampleList);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = thirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.this$Anon0.getMThirdPartyDatabase().runInTransaction((Runnable) new C0114Anon1(this));
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(List list, ge0 ge0, Ref$IntRef ref$IntRef, int i, dg4 dg4, ThirdPartyRepository thirdPartyRepository, List list2, String str) {
        this.$sampleList = list;
        this.$googleApiClient$inlined = ge0;
        this.$countSizeOfList$inlined = ref$IntRef;
        this.$sizeOfGFitList$inlined = i;
        this.$continuation$inlined = dg4;
        this.this$Anon0 = thirdPartyRepository;
        this.$gFitHeartRateList$inlined = list2;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    public final void onResult(Status status) {
        kd4.b(status, "status");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("Sending GFitHeartRate: Status = ");
        status.G();
        sb.append(status);
        sb.append(" - Status Message = ");
        sb.append(status.J());
        local.d(ThirdPartyRepository.TAG, sb.toString());
        if (status.L()) {
            fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (yb4) null), 3, (Object) null);
        }
        Ref$IntRef ref$IntRef = this.$countSizeOfList$inlined;
        ref$IntRef.element++;
        if (ref$IntRef.element >= this.$sizeOfGFitList$inlined && this.$continuation$inlined.isActive()) {
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveGFitHeartRateToGoogleFit");
            dg4 dg4 = this.$continuation$inlined;
            Result.a aVar = Result.Companion;
            dg4.resumeWith(Result.m3constructorimpl((Object) null));
        }
    }
}
