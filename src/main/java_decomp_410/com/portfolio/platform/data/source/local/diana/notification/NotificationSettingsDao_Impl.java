package com.portfolio.platform.data.source.local.diana.notification;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationSettingsDao_Impl implements NotificationSettingsDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfNotificationSettingsModel;
    @DexIgnore
    public /* final */ wf __preparedStmtOfDelete;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<NotificationSettingsModel> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `notificationSettings`(`settingsName`,`settingsType`,`isCall`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, NotificationSettingsModel notificationSettingsModel) {
            if (notificationSettingsModel.getSettingsName() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, notificationSettingsModel.getSettingsName());
            }
            kgVar.b(2, (long) notificationSettingsModel.getSettingsType());
            kgVar.b(3, notificationSettingsModel.isCall() ? 1 : 0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends wf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM notificationSettings";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<NotificationSettingsModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon3(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<NotificationSettingsModel> call() throws Exception {
            Cursor a = bg.a(NotificationSettingsDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "settingsName");
                int b2 = ag.b(a, "settingsType");
                int b3 = ag.b(a, "isCall");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new NotificationSettingsModel(a.getString(b), a.getInt(b2), a.getInt(b3) != 0));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<NotificationSettingsModel> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon4(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public NotificationSettingsModel call() throws Exception {
            NotificationSettingsModel notificationSettingsModel;
            boolean z = false;
            Cursor a = bg.a(NotificationSettingsDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "settingsName");
                int b2 = ag.b(a, "settingsType");
                int b3 = ag.b(a, "isCall");
                if (a.moveToFirst()) {
                    String string = a.getString(b);
                    int i = a.getInt(b2);
                    if (a.getInt(b3) != 0) {
                        z = true;
                    }
                    notificationSettingsModel = new NotificationSettingsModel(string, i, z);
                } else {
                    notificationSettingsModel = null;
                }
                return notificationSettingsModel;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public NotificationSettingsDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfNotificationSettingsModel = new Anon1(roomDatabase);
        this.__preparedStmtOfDelete = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    public LiveData<List<NotificationSettingsModel>> getListNotificationSettings() {
        return this.__db.getInvalidationTracker().a(new String[]{"notificationSettings"}, false, new Anon3(uf.b("SELECT * FROM notificationSettings", 0)));
    }

    @DexIgnore
    public List<NotificationSettingsModel> getListNotificationSettingsNoLiveData() {
        uf b = uf.b("SELECT * FROM notificationSettings", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "settingsName");
            int b3 = ag.b(a, "settingsType");
            int b4 = ag.b(a, "isCall");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new NotificationSettingsModel(a.getString(b2), a.getInt(b3), a.getInt(b4) != 0));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<NotificationSettingsModel> getNotificationSettingsWithFieldIsCall(boolean z) {
        uf b = uf.b("SELECT * FROM notificationSettings WHERE isCall = ?", 1);
        b.b(1, z ? 1 : 0);
        return this.__db.getInvalidationTracker().a(new String[]{"notificationSettings"}, false, new Anon4(b));
    }

    @DexIgnore
    public NotificationSettingsModel getNotificationSettingsWithIsCallNoLiveData(boolean z) {
        NotificationSettingsModel notificationSettingsModel;
        boolean z2 = true;
        uf b = uf.b("SELECT * FROM notificationSettings WHERE isCall = ?", 1);
        b.b(1, z ? 1 : 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "settingsName");
            int b3 = ag.b(a, "settingsType");
            int b4 = ag.b(a, "isCall");
            if (a.moveToFirst()) {
                String string = a.getString(b2);
                int i = a.getInt(b3);
                if (a.getInt(b4) == 0) {
                    z2 = false;
                }
                notificationSettingsModel = new NotificationSettingsModel(string, i, z2);
            } else {
                notificationSettingsModel = null;
            }
            return notificationSettingsModel;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertListNotificationSettings(List<NotificationSettingsModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfNotificationSettingsModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertNotificationSettings(NotificationSettingsModel notificationSettingsModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfNotificationSettingsModel.insert(notificationSettingsModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
