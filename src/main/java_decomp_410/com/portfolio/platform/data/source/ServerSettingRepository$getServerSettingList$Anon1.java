package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ServerSettingRepository$getServerSettingList$Anon1 implements ServerSettingDataSource.OnGetServerSettingList {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingDataSource.OnGetServerSettingList $callback;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRepository this$Anon0;

    @DexIgnore
    public ServerSettingRepository$getServerSettingList$Anon1(ServerSettingRepository serverSettingRepository, ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        this.this$Anon0 = serverSettingRepository;
        this.$callback = onGetServerSettingList;
    }

    @DexIgnore
    public void onFailed(int i) {
        this.$callback.onFailed(i);
    }

    @DexIgnore
    public void onSuccess(ServerSettingList serverSettingList) {
        throw null;
        // fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1(this, serverSettingList, (yb4) null), 3, (Object) null);
        // this.$callback.onSuccess(serverSettingList);
    }
}
