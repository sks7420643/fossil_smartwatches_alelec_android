package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nd;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.PagingRequestHelper;
import com.portfolio.platform.response.ResponseKt;
import com.portfolio.platform.response.sleep.SleepDayParse;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import kotlin.Pair;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSummariesRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public /* final */ SleepDao mSleepDao;
    @DexIgnore
    public List<SleepSummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return SleepSummariesRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = SleepSummariesRepository.class.getSimpleName();
        kd4.a((Object) simpleName, "SleepSummariesRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public SleepSummariesRepository(SleepDao sleepDao, ApiServiceV2 apiServiceV2, FitnessDataDao fitnessDataDao) {
        kd4.b(sleepDao, "mSleepDao");
        kd4.b(apiServiceV2, "mApiService");
        kd4.b(fitnessDataDao, "mFitnessDataDao");
        this.mSleepDao = sleepDao;
        this.mApiService = apiServiceV2;
        this.mFitnessDataDao = fitnessDataDao;
    }

    @DexIgnore
    private final SleepStatistic getSleepStatisticDB() {
        FLogger.INSTANCE.getLocal().d(TAG, "getSleepStatisticDB");
        return this.mSleepDao.getSleepStatistic();
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        removePagingListener();
        this.mSleepDao.deleteAllSleepDays();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object fetchLastSleepGoal(yb4<? super qo2<MFSleepSettings>> yb4) {
        SleepSummariesRepository$fetchLastSleepGoal$Anon1 sleepSummariesRepository$fetchLastSleepGoal$Anon1;
        int i;
        SleepSummariesRepository sleepSummariesRepository;
        qo2 qo2;
        if (yb4 instanceof SleepSummariesRepository$fetchLastSleepGoal$Anon1) {
            sleepSummariesRepository$fetchLastSleepGoal$Anon1 = (SleepSummariesRepository$fetchLastSleepGoal$Anon1) yb4;
            int i2 = sleepSummariesRepository$fetchLastSleepGoal$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                sleepSummariesRepository$fetchLastSleepGoal$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = sleepSummariesRepository$fetchLastSleepGoal$Anon1.result;
                Object a = cc4.a();
                i = sleepSummariesRepository$fetchLastSleepGoal$Anon1.label;
                String str = null;
                if (i != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "fetchLastSleepGoal");
                    SleepSummariesRepository$fetchLastSleepGoal$response$Anon1 sleepSummariesRepository$fetchLastSleepGoal$response$Anon1 = new SleepSummariesRepository$fetchLastSleepGoal$response$Anon1(this, (yb4) null);
                    sleepSummariesRepository$fetchLastSleepGoal$Anon1.L$Anon0 = this;
                    sleepSummariesRepository$fetchLastSleepGoal$Anon1.label = 1;
                    obj = ResponseKt.a(sleepSummariesRepository$fetchLastSleepGoal$response$Anon1, sleepSummariesRepository$fetchLastSleepGoal$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    sleepSummariesRepository = this;
                } else if (i == 1) {
                    sleepSummariesRepository = (SleepSummariesRepository) sleepSummariesRepository$fetchLastSleepGoal$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    if (ro2.a() != null && !ro2.b()) {
                        sleepSummariesRepository.saveSleepSettingToDB$app_fossilRelease(((MFSleepSettings) ro2.a()).getSleepGoal());
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("fetchActivitySettings Failure code=");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(" message=");
                    ServerError c = po2.c();
                    if (c != null) {
                        String message = c.getMessage();
                        if (message != null) {
                            str = message;
                            if (str == null) {
                                str = "";
                            }
                            sb.append(str);
                            local.d(str2, sb.toString());
                        }
                    }
                    ServerError c2 = po2.c();
                    if (c2 != null) {
                        str = c2.getUserMessage();
                    }
                    if (str == null) {
                    }
                    sb.append(str);
                    local.d(str2, sb.toString());
                }
                return qo2;
            }
        }
        sleepSummariesRepository$fetchLastSleepGoal$Anon1 = new SleepSummariesRepository$fetchLastSleepGoal$Anon1(this, yb4);
        Object obj2 = sleepSummariesRepository$fetchLastSleepGoal$Anon1.result;
        Object a2 = cc4.a();
        i = sleepSummariesRepository$fetchLastSleepGoal$Anon1.label;
        String str3 = null;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
        return qo2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object fetchSleepSummaries(Date date, Date date2, yb4<? super qo2<xz1>> yb4) {
        SleepSummariesRepository$fetchSleepSummaries$Anon1 sleepSummariesRepository$fetchSleepSummaries$Anon1;
        int i;
        SleepSummariesRepository sleepSummariesRepository;
        qo2 qo2;
        if (yb4 instanceof SleepSummariesRepository$fetchSleepSummaries$Anon1) {
            sleepSummariesRepository$fetchSleepSummaries$Anon1 = (SleepSummariesRepository$fetchSleepSummaries$Anon1) yb4;
            int i2 = sleepSummariesRepository$fetchSleepSummaries$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                sleepSummariesRepository$fetchSleepSummaries$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = sleepSummariesRepository$fetchSleepSummaries$Anon1.result;
                Object a = cc4.a();
                i = sleepSummariesRepository$fetchSleepSummaries$Anon1.label;
                String str = null;
                if (i != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "loadSummaries");
                    SleepSummariesRepository$fetchSleepSummaries$response$Anon1 sleepSummariesRepository$fetchSleepSummaries$response$Anon1 = new SleepSummariesRepository$fetchSleepSummaries$response$Anon1(this, date, date2, (yb4) null);
                    sleepSummariesRepository$fetchSleepSummaries$Anon1.L$Anon0 = this;
                    sleepSummariesRepository$fetchSleepSummaries$Anon1.L$Anon1 = date;
                    sleepSummariesRepository$fetchSleepSummaries$Anon1.L$Anon2 = date2;
                    sleepSummariesRepository$fetchSleepSummaries$Anon1.label = 1;
                    obj = ResponseKt.a(sleepSummariesRepository$fetchSleepSummaries$response$Anon1, sleepSummariesRepository$fetchSleepSummaries$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    sleepSummariesRepository = this;
                } else if (i == 1) {
                    Date date3 = (Date) sleepSummariesRepository$fetchSleepSummaries$Anon1.L$Anon2;
                    Date date4 = (Date) sleepSummariesRepository$fetchSleepSummaries$Anon1.L$Anon1;
                    sleepSummariesRepository = (SleepSummariesRepository) sleepSummariesRepository$fetchSleepSummaries$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    if (ro2.a() != null && !ro2.b()) {
                        try {
                            ArrayList arrayList = new ArrayList();
                            ApiResponse apiResponse = (ApiResponse) new rz1().a().a(((xz1) ((ro2) qo2).a()).toString(), new SleepSummariesRepository$fetchSleepSummaries$Anon2().getType());
                            if (apiResponse != null) {
                                List<SleepDayParse> list = apiResponse.get_items();
                                if (list != null) {
                                    for (SleepDayParse mFSleepBySleepDayParse : list) {
                                        MFSleepDay mFSleepBySleepDayParse2 = mFSleepBySleepDayParse.getMFSleepBySleepDayParse();
                                        if (mFSleepBySleepDayParse2 != null) {
                                            arrayList.add(mFSleepBySleepDayParse2);
                                        }
                                    }
                                }
                            }
                            sleepSummariesRepository.mSleepDao.upsertSleepDays(arrayList);
                        } catch (Exception e) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String str2 = TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("loadSummaries exception=");
                            e.printStackTrace();
                            sb.append(qa4.a);
                            local.d(str2, sb.toString());
                        }
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("loadSummaries Failure code=");
                    po2 po2 = (po2) qo2;
                    sb2.append(po2.a());
                    sb2.append(" message=");
                    ServerError c = po2.c();
                    if (c != null) {
                        String message = c.getMessage();
                        if (message != null) {
                            str = message;
                            if (str == null) {
                                str = "";
                            }
                            sb2.append(str);
                            local2.d(str3, sb2.toString());
                        }
                    }
                    ServerError c2 = po2.c();
                    if (c2 != null) {
                        str = c2.getUserMessage();
                    }
                    if (str == null) {
                    }
                    sb2.append(str);
                    local2.d(str3, sb2.toString());
                }
                return qo2;
            }
        }
        sleepSummariesRepository$fetchSleepSummaries$Anon1 = new SleepSummariesRepository$fetchSleepSummaries$Anon1(this, yb4);
        Object obj2 = sleepSummariesRepository$fetchSleepSummaries$Anon1.result;
        Object a2 = cc4.a();
        i = sleepSummariesRepository$fetchSleepSummaries$Anon1.label;
        String str4 = null;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
        return qo2;
    }

    @DexIgnore
    public final int getCurrentLastSleepGoal() {
        MFSleepSettings sleepSettings = this.mSleepDao.getSleepSettings();
        if (sleepSettings != null) {
            return sleepSettings.getSleepGoal();
        }
        return 480;
    }

    @DexIgnore
    public final LiveData<os3<Integer>> getLastSleepGoal() {
        return new SleepSummariesRepository$getLastSleepGoal$Anon1(this).asLiveData();
    }

    @DexIgnore
    public final LiveData<os3<SleepStatistic>> getSleepStatistic(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSleepStatistic - shouldFetch=" + z);
        return new SleepSummariesRepository$getSleepStatistic$Anon1(this, z).asLiveData();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getSleepStatisticAwait(yb4<? super SleepStatistic> yb4) {
        SleepSummariesRepository$getSleepStatisticAwait$Anon1 sleepSummariesRepository$getSleepStatisticAwait$Anon1;
        int i;
        SleepSummariesRepository sleepSummariesRepository;
        qo2 qo2;
        if (yb4 instanceof SleepSummariesRepository$getSleepStatisticAwait$Anon1) {
            sleepSummariesRepository$getSleepStatisticAwait$Anon1 = (SleepSummariesRepository$getSleepStatisticAwait$Anon1) yb4;
            int i2 = sleepSummariesRepository$getSleepStatisticAwait$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                sleepSummariesRepository$getSleepStatisticAwait$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = sleepSummariesRepository$getSleepStatisticAwait$Anon1.result;
                Object a = cc4.a();
                i = sleepSummariesRepository$getSleepStatisticAwait$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "getSleepStatisticAwait");
                    SleepStatistic sleepStatisticDB = getSleepStatisticDB();
                    if (sleepStatisticDB != null) {
                        return sleepStatisticDB;
                    }
                    SleepSummariesRepository$getSleepStatisticAwait$response$Anon1 sleepSummariesRepository$getSleepStatisticAwait$response$Anon1 = new SleepSummariesRepository$getSleepStatisticAwait$response$Anon1(this, (yb4) null);
                    sleepSummariesRepository$getSleepStatisticAwait$Anon1.L$Anon0 = this;
                    sleepSummariesRepository$getSleepStatisticAwait$Anon1.L$Anon1 = sleepStatisticDB;
                    sleepSummariesRepository$getSleepStatisticAwait$Anon1.label = 1;
                    obj = ResponseKt.a(sleepSummariesRepository$getSleepStatisticAwait$response$Anon1, sleepSummariesRepository$getSleepStatisticAwait$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    sleepSummariesRepository = this;
                } else if (i == 1) {
                    SleepStatistic sleepStatistic = (SleepStatistic) sleepSummariesRepository$getSleepStatisticAwait$Anon1.L$Anon1;
                    sleepSummariesRepository = (SleepSummariesRepository) sleepSummariesRepository$getSleepStatisticAwait$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    if (ro2.a() != null) {
                        sleepSummariesRepository.mSleepDao.upsertSleepStatistic((SleepStatistic) ro2.a());
                        return ro2.a();
                    }
                } else if (qo2 instanceof po2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("getSleepStatisticAwait - Failure -- code=");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(", message=");
                    ServerError c = po2.c();
                    sb.append(c != null ? c.getMessage() : null);
                    local.e(str, sb.toString());
                }
                return null;
            }
        }
        sleepSummariesRepository$getSleepStatisticAwait$Anon1 = new SleepSummariesRepository$getSleepStatisticAwait$Anon1(this, yb4);
        Object obj2 = sleepSummariesRepository$getSleepStatisticAwait$Anon1.result;
        Object a2 = cc4.a();
        i = sleepSummariesRepository$getSleepStatisticAwait$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
        return null;
    }

    @DexIgnore
    public final LiveData<os3<List<MFSleepDay>>> getSleepSummaries(Date date, Date date2, boolean z) {
        kd4.b(date, GoalPhase.COLUMN_START_DATE);
        kd4.b(date2, GoalPhase.COLUMN_END_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSleepSummaries: startDate = " + date + ", endDate = " + date2);
        LiveData<os3<List<MFSleepDay>>> b = hc.b(this.mFitnessDataDao.getFitnessDataLiveData(date, date2), new SleepSummariesRepository$getSleepSummaries$Anon1(this, date, date2, z, rk2.e(date), rk2.e(date2)));
        kd4.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final LiveData<os3<MFSleepDay>> getSleepSummary(Date date) {
        kd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSleepSummary date=" + date);
        LiveData<os3<MFSleepDay>> b = hc.b(this.mFitnessDataDao.getFitnessDataLiveData(date, date), new SleepSummariesRepository$getSleepSummary$Anon1(this, date));
        kd4.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final MFSleepDay getSleepSummaryFromDb(Date date) {
        kd4.b(date, "date");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "getSummary: calendar = " + date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.mSleepDao.getSleepDay(date);
    }

    @DexIgnore
    public final Listing<SleepSummary> getSummariesPaging(SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDao sleepDao, SleepDatabase sleepDatabase, Date date, h42 h42, PagingRequestHelper.a aVar) {
        Date date2 = date;
        kd4.b(sleepSummariesRepository, "summariesRepository");
        kd4.b(sleepSessionsRepository, "sessionsRepository");
        kd4.b(fitnessDataRepository, "fitnessDataRepository");
        SleepDao sleepDao2 = sleepDao;
        kd4.b(sleepDao2, "sleepDao");
        SleepDatabase sleepDatabase2 = sleepDatabase;
        kd4.b(sleepDatabase2, "sleepDatabase");
        kd4.b(date2, "createdDate");
        h42 h422 = h42;
        kd4.b(h422, "appExecutors");
        PagingRequestHelper.a aVar2 = aVar;
        kd4.b(aVar2, "listener");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSummariesPaging - createdDate=" + date2);
        SleepSummaryLocalDataSource.Companion companion = SleepSummaryLocalDataSource.Companion;
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        Date time = instance.getTime();
        kd4.a((Object) time, "Calendar.getInstance().time");
        Date calculateNextKey = companion.calculateNextKey(time, date2);
        Calendar instance2 = Calendar.getInstance();
        kd4.a((Object) instance2, "calendar");
        instance2.setTime(calculateNextKey);
        SleepSummaryDataSourceFactory sleepSummaryDataSourceFactory = new SleepSummaryDataSourceFactory(sleepSummariesRepository, sleepSessionsRepository, fitnessDataRepository, sleepDao2, sleepDatabase2, date2, h422, aVar2, instance2);
        this.mSourceFactoryList.add(sleepSummaryDataSourceFactory);
        qd.f.a aVar3 = new qd.f.a();
        aVar3.a(30);
        aVar3.a(false);
        aVar3.b(30);
        aVar3.c(5);
        qd.f a = aVar3.a();
        kd4.a((Object) a, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a2 = new nd(sleepSummaryDataSourceFactory, a).a();
        kd4.a((Object) a2, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData<Y> b = hc.b(sleepSummaryDataSourceFactory.getSourceLiveData(), SleepSummariesRepository$getSummariesPaging$Anon1.INSTANCE);
        kd4.a((Object) b, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing<>(a2, b, new SleepSummariesRepository$getSummariesPaging$Anon2(sleepSummaryDataSourceFactory), new SleepSummariesRepository$getSummariesPaging$Anon3(sleepSummaryDataSourceFactory));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object pushLastSleepGoalToServer(int i, yb4<? super qa4> yb4) {
        SleepSummariesRepository$pushLastSleepGoalToServer$Anon1 sleepSummariesRepository$pushLastSleepGoalToServer$Anon1;
        int i2;
        if (yb4 instanceof SleepSummariesRepository$pushLastSleepGoalToServer$Anon1) {
            sleepSummariesRepository$pushLastSleepGoalToServer$Anon1 = (SleepSummariesRepository$pushLastSleepGoalToServer$Anon1) yb4;
            int i3 = sleepSummariesRepository$pushLastSleepGoalToServer$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                sleepSummariesRepository$pushLastSleepGoalToServer$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = sleepSummariesRepository$pushLastSleepGoalToServer$Anon1.result;
                Object a = cc4.a();
                i2 = sleepSummariesRepository$pushLastSleepGoalToServer$Anon1.label;
                if (i2 != 0) {
                    na4.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "pushLastSleepGoalToServer sleepGoal=" + i);
                    xz1 xz1 = new xz1();
                    try {
                        xz1.a("currentGoalMinutes", (Number) dc4.a(i));
                        TimeZone timeZone = TimeZone.getDefault();
                        kd4.a((Object) timeZone, "TimeZone.getDefault()");
                        xz1.a("timezoneOffset", (Number) dc4.a(timeZone.getRawOffset() / 1000));
                    } catch (Exception unused) {
                    }
                    SleepSummariesRepository$pushLastSleepGoalToServer$Anon2 sleepSummariesRepository$pushLastSleepGoalToServer$Anon2 = new SleepSummariesRepository$pushLastSleepGoalToServer$Anon2(this, xz1, (yb4) null);
                    sleepSummariesRepository$pushLastSleepGoalToServer$Anon1.L$Anon0 = this;
                    sleepSummariesRepository$pushLastSleepGoalToServer$Anon1.I$Anon0 = i;
                    sleepSummariesRepository$pushLastSleepGoalToServer$Anon1.L$Anon1 = xz1;
                    sleepSummariesRepository$pushLastSleepGoalToServer$Anon1.label = 1;
                    if (ResponseKt.a(sleepSummariesRepository$pushLastSleepGoalToServer$Anon2, sleepSummariesRepository$pushLastSleepGoalToServer$Anon1) == a) {
                        return a;
                    }
                } else if (i2 == 1) {
                    xz1 xz12 = (xz1) sleepSummariesRepository$pushLastSleepGoalToServer$Anon1.L$Anon1;
                    int i4 = sleepSummariesRepository$pushLastSleepGoalToServer$Anon1.I$Anon0;
                    SleepSummariesRepository sleepSummariesRepository = (SleepSummariesRepository) sleepSummariesRepository$pushLastSleepGoalToServer$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return qa4.a;
            }
        }
        sleepSummariesRepository$pushLastSleepGoalToServer$Anon1 = new SleepSummariesRepository$pushLastSleepGoalToServer$Anon1(this, yb4);
        Object obj2 = sleepSummariesRepository$pushLastSleepGoalToServer$Anon1.result;
        Object a2 = cc4.a();
        i2 = sleepSummariesRepository$pushLastSleepGoalToServer$Anon1.label;
        if (i2 != 0) {
        }
        return qa4.a;
    }

    @DexIgnore
    public final void removePagingListener() {
        for (SleepSummaryDataSourceFactory localSource : this.mSourceFactoryList) {
            SleepSummaryLocalDataSource localSource2 = localSource.getLocalSource();
            if (localSource2 != null) {
                localSource2.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }

    @DexIgnore
    public final void saveSleepSettingToDB$app_fossilRelease(int i) {
        this.mSleepDao.upsertSleepSettings(i);
        MFSleepDay sleepDay = this.mSleepDao.getSleepDay(new Date());
        if (sleepDay == null) {
            SleepDistribution sleepDistribution = new SleepDistribution(0, 0, 0);
            Date date = new Date();
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "Calendar.getInstance()");
            DateTime dateTime = new DateTime(instance.getTimeInMillis());
            Calendar instance2 = Calendar.getInstance();
            kd4.a((Object) instance2, "Calendar.getInstance()");
            sleepDay = new MFSleepDay(date, i, 0, sleepDistribution, dateTime, new DateTime(instance2.getTimeInMillis()));
        } else {
            sleepDay.setGoalMinutes(i);
        }
        this.mSleepDao.upsertSleepDay(sleepDay);
    }

    @DexIgnore
    public final void saveSleepSummaries$app_fossilRelease(xz1 xz1, Date date, Date date2, Pair<? extends Date, ? extends Date> pair) {
        kd4.b(xz1, "item");
        kd4.b(date, GoalPhase.COLUMN_START_DATE);
        kd4.b(date2, GoalPhase.COLUMN_END_DATE);
        try {
            ArrayList arrayList = new ArrayList();
            ApiResponse apiResponse = (ApiResponse) new rz1().a().a(xz1.toString(), new SleepSummariesRepository$saveSleepSummaries$Anon1().getType());
            if (apiResponse != null) {
                List<SleepDayParse> list = apiResponse.get_items();
                if (list != null) {
                    for (SleepDayParse mFSleepBySleepDayParse : list) {
                        MFSleepDay mFSleepBySleepDayParse2 = mFSleepBySleepDayParse.getMFSleepBySleepDayParse();
                        if (mFSleepBySleepDayParse2 != null) {
                            arrayList.add(mFSleepBySleepDayParse2);
                        }
                    }
                }
            }
            FitnessDataDao fitnessDataDao = this.mFitnessDataDao;
            if (pair != null) {
                Date date3 = (Date) pair.getFirst();
                if (date3 != null) {
                    date = date3;
                }
            }
            if (pair != null) {
                Date date4 = (Date) pair.getSecond();
                if (date4 != null) {
                    date2 = date4;
                }
            }
            List<FitnessDataWrapper> fitnessData = fitnessDataDao.getFitnessData(date, date2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "fitnessDatasSize " + fitnessData.size());
            if (fitnessData.isEmpty()) {
                this.mSleepDao.upsertSleepDays(arrayList);
            }
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("getSleepSummaries saveCallResult exception=");
            e.printStackTrace();
            sb.append(qa4.a);
            local2.e(str2, sb.toString());
        }
    }

    @DexIgnore
    public final void saveSleepSummary$app_fossilRelease(xz1 xz1, Date date) {
        kd4.b(xz1, "item");
        kd4.b(date, "date");
        try {
            ArrayList arrayList = new ArrayList();
            ApiResponse apiResponse = (ApiResponse) new rz1().a().a(xz1.toString(), new SleepSummariesRepository$saveSleepSummary$Anon1().getType());
            if (apiResponse != null) {
                List<SleepDayParse> list = apiResponse.get_items();
                if (list != null) {
                    for (SleepDayParse mFSleepBySleepDayParse : list) {
                        MFSleepDay mFSleepBySleepDayParse2 = mFSleepBySleepDayParse.getMFSleepBySleepDayParse();
                        if (mFSleepBySleepDayParse2 != null) {
                            arrayList.add(mFSleepBySleepDayParse2);
                        }
                    }
                }
            }
            List<FitnessDataWrapper> fitnessData = this.mFitnessDataDao.getFitnessData(date, date);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "fitnessDatasSize " + fitnessData.size());
            if ((!arrayList.isEmpty()) && fitnessData.isEmpty()) {
                SleepDao sleepDao = this.mSleepDao;
                Object obj = arrayList.get(0);
                kd4.a(obj, "summaryList[0]");
                sleepDao.upsertSleepDay((MFSleepDay) obj);
            }
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("getSleepSummary exception=");
            e.printStackTrace();
            sb.append(qa4.a);
            local2.d(str2, sb.toString());
        }
    }

    @DexIgnore
    public final LiveData<os3<Integer>> updateLastSleepGoal(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "updateLastSleepGoal sleepGoal=" + i);
        return new SleepSummariesRepository$updateLastSleepGoal$Anon1(this, i).asLiveData();
    }
}
