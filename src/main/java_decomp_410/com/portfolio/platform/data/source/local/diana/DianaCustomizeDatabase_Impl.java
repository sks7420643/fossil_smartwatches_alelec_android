package com.portfolio.platform.data.source.local.diana;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Explore;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaCustomizeDatabase_Impl extends DianaCustomizeDatabase {
    @DexIgnore
    public volatile ComplicationDao _complicationDao;
    @DexIgnore
    public volatile ComplicationLastSettingDao _complicationLastSettingDao;
    @DexIgnore
    public volatile DianaPresetDao _dianaPresetDao;
    @DexIgnore
    public volatile WatchAppDao _watchAppDao;
    @DexIgnore
    public volatile WatchAppLastSettingDao _watchAppLastSettingDao;
    @DexIgnore
    public volatile WatchFaceDao _watchFaceDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `complication` (`complicationId` TEXT NOT NULL, `name` TEXT NOT NULL, `nameKey` TEXT NOT NULL, `categories` TEXT NOT NULL, `description` TEXT NOT NULL, `descriptionKey` TEXT NOT NULL, `icon` TEXT, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`complicationId`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `watchApp` (`watchappId` TEXT NOT NULL, `name` TEXT NOT NULL, `nameKey` TEXT NOT NULL, `description` TEXT NOT NULL, `descriptionKey` TEXT NOT NULL, `categories` TEXT NOT NULL, `icon` TEXT, `updatedAt` TEXT NOT NULL, `createdAt` TEXT NOT NULL, PRIMARY KEY(`watchappId`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `complicationLastSetting` (`complicationId` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`complicationId`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `watchAppLastSetting` (`watchAppId` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`watchAppId`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `dianaPreset` (`createdAt` TEXT, `updatedAt` TEXT, `pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `name` TEXT NOT NULL, `isActive` INTEGER NOT NULL, `complications` TEXT NOT NULL, `watchapps` TEXT NOT NULL, `watchFaceId` TEXT NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `dianaRecommendPreset` (`serialNumber` TEXT NOT NULL, `id` TEXT NOT NULL, `name` TEXT NOT NULL, `isDefault` INTEGER NOT NULL, `complications` TEXT NOT NULL, `watchapps` TEXT NOT NULL, `watchFaceId` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `watch_face` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `ringStyleItems` TEXT, `background` TEXT NOT NULL, `previewUrl` TEXT NOT NULL, `serial` TEXT NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'b36140bba6a58f9cc8d2af308d56724d')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `complication`");
            ggVar.b("DROP TABLE IF EXISTS `watchApp`");
            ggVar.b("DROP TABLE IF EXISTS `complicationLastSetting`");
            ggVar.b("DROP TABLE IF EXISTS `watchAppLastSetting`");
            ggVar.b("DROP TABLE IF EXISTS `dianaPreset`");
            ggVar.b("DROP TABLE IF EXISTS `dianaRecommendPreset`");
            ggVar.b("DROP TABLE IF EXISTS `watch_face`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (DianaCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = DianaCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) DianaCustomizeDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = DianaCustomizeDatabase_Impl.this.mDatabase = ggVar;
            DianaCustomizeDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (DianaCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = DianaCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) DianaCustomizeDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            gg ggVar2 = ggVar;
            HashMap hashMap = new HashMap(9);
            hashMap.put("complicationId", new eg.a("complicationId", "TEXT", true, 1));
            hashMap.put("name", new eg.a("name", "TEXT", true, 0));
            hashMap.put("nameKey", new eg.a("nameKey", "TEXT", true, 0));
            hashMap.put("categories", new eg.a("categories", "TEXT", true, 0));
            hashMap.put("description", new eg.a("description", "TEXT", true, 0));
            hashMap.put("descriptionKey", new eg.a("descriptionKey", "TEXT", true, 0));
            hashMap.put("icon", new eg.a("icon", "TEXT", false, 0));
            hashMap.put("createdAt", new eg.a("createdAt", "TEXT", true, 0));
            hashMap.put("updatedAt", new eg.a("updatedAt", "TEXT", true, 0));
            eg egVar = new eg("complication", hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar2, "complication");
            if (egVar.equals(a)) {
                HashMap hashMap2 = new HashMap(9);
                hashMap2.put("watchappId", new eg.a("watchappId", "TEXT", true, 1));
                hashMap2.put("name", new eg.a("name", "TEXT", true, 0));
                hashMap2.put("nameKey", new eg.a("nameKey", "TEXT", true, 0));
                hashMap2.put("description", new eg.a("description", "TEXT", true, 0));
                hashMap2.put("descriptionKey", new eg.a("descriptionKey", "TEXT", true, 0));
                hashMap2.put("categories", new eg.a("categories", "TEXT", true, 0));
                hashMap2.put("icon", new eg.a("icon", "TEXT", false, 0));
                hashMap2.put("updatedAt", new eg.a("updatedAt", "TEXT", true, 0));
                hashMap2.put("createdAt", new eg.a("createdAt", "TEXT", true, 0));
                eg egVar2 = new eg("watchApp", hashMap2, new HashSet(0), new HashSet(0));
                eg a2 = eg.a(ggVar2, "watchApp");
                if (egVar2.equals(a2)) {
                    HashMap hashMap3 = new HashMap(3);
                    hashMap3.put("complicationId", new eg.a("complicationId", "TEXT", true, 1));
                    hashMap3.put("updatedAt", new eg.a("updatedAt", "TEXT", true, 0));
                    hashMap3.put(MicroAppSetting.SETTING, new eg.a(MicroAppSetting.SETTING, "TEXT", true, 0));
                    eg egVar3 = new eg("complicationLastSetting", hashMap3, new HashSet(0), new HashSet(0));
                    eg a3 = eg.a(ggVar2, "complicationLastSetting");
                    if (egVar3.equals(a3)) {
                        HashMap hashMap4 = new HashMap(3);
                        hashMap4.put("watchAppId", new eg.a("watchAppId", "TEXT", true, 1));
                        hashMap4.put("updatedAt", new eg.a("updatedAt", "TEXT", true, 0));
                        hashMap4.put(MicroAppSetting.SETTING, new eg.a(MicroAppSetting.SETTING, "TEXT", true, 0));
                        eg egVar4 = new eg("watchAppLastSetting", hashMap4, new HashSet(0), new HashSet(0));
                        eg a4 = eg.a(ggVar2, "watchAppLastSetting");
                        if (egVar4.equals(a4)) {
                            HashMap hashMap5 = new HashMap(10);
                            hashMap5.put("createdAt", new eg.a("createdAt", "TEXT", false, 0));
                            hashMap5.put("updatedAt", new eg.a("updatedAt", "TEXT", false, 0));
                            hashMap5.put("pinType", new eg.a("pinType", "INTEGER", true, 0));
                            hashMap5.put("id", new eg.a("id", "TEXT", true, 1));
                            hashMap5.put("serialNumber", new eg.a("serialNumber", "TEXT", true, 0));
                            hashMap5.put("name", new eg.a("name", "TEXT", true, 0));
                            hashMap5.put("isActive", new eg.a("isActive", "INTEGER", true, 0));
                            hashMap5.put("complications", new eg.a("complications", "TEXT", true, 0));
                            hashMap5.put("watchapps", new eg.a("watchapps", "TEXT", true, 0));
                            hashMap5.put("watchFaceId", new eg.a("watchFaceId", "TEXT", true, 0));
                            eg egVar5 = new eg("dianaPreset", hashMap5, new HashSet(0), new HashSet(0));
                            eg a5 = eg.a(ggVar2, "dianaPreset");
                            if (egVar5.equals(a5)) {
                                HashMap hashMap6 = new HashMap(9);
                                hashMap6.put("serialNumber", new eg.a("serialNumber", "TEXT", true, 0));
                                hashMap6.put("id", new eg.a("id", "TEXT", true, 1));
                                hashMap6.put("name", new eg.a("name", "TEXT", true, 0));
                                hashMap6.put("isDefault", new eg.a("isDefault", "INTEGER", true, 0));
                                hashMap6.put("complications", new eg.a("complications", "TEXT", true, 0));
                                hashMap6.put("watchapps", new eg.a("watchapps", "TEXT", true, 0));
                                hashMap6.put("watchFaceId", new eg.a("watchFaceId", "TEXT", true, 0));
                                hashMap6.put("createdAt", new eg.a("createdAt", "TEXT", true, 0));
                                hashMap6.put("updatedAt", new eg.a("updatedAt", "TEXT", true, 0));
                                eg egVar6 = new eg("dianaRecommendPreset", hashMap6, new HashSet(0), new HashSet(0));
                                eg a6 = eg.a(ggVar2, "dianaRecommendPreset");
                                if (egVar6.equals(a6)) {
                                    HashMap hashMap7 = new HashMap(6);
                                    hashMap7.put("id", new eg.a("id", "TEXT", true, 1));
                                    hashMap7.put("name", new eg.a("name", "TEXT", true, 0));
                                    hashMap7.put("ringStyleItems", new eg.a("ringStyleItems", "TEXT", false, 0));
                                    hashMap7.put(Explore.COLUMN_BACKGROUND, new eg.a(Explore.COLUMN_BACKGROUND, "TEXT", true, 0));
                                    hashMap7.put("previewUrl", new eg.a("previewUrl", "TEXT", true, 0));
                                    hashMap7.put("serial", new eg.a("serial", "TEXT", true, 0));
                                    eg egVar7 = new eg("watch_face", hashMap7, new HashSet(0), new HashSet(0));
                                    eg a7 = eg.a(ggVar2, "watch_face");
                                    if (!egVar7.equals(a7)) {
                                        throw new IllegalStateException("Migration didn't properly handle watch_face(com.portfolio.platform.data.model.diana.preset.WatchFace).\n Expected:\n" + egVar7 + "\n Found:\n" + a7);
                                    }
                                    return;
                                }
                                throw new IllegalStateException("Migration didn't properly handle dianaRecommendPreset(com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset).\n Expected:\n" + egVar6 + "\n Found:\n" + a6);
                            }
                            throw new IllegalStateException("Migration didn't properly handle dianaPreset(com.portfolio.platform.data.model.diana.preset.DianaPreset).\n Expected:\n" + egVar5 + "\n Found:\n" + a5);
                        }
                        throw new IllegalStateException("Migration didn't properly handle watchAppLastSetting(com.portfolio.platform.data.model.diana.WatchAppLastSetting).\n Expected:\n" + egVar4 + "\n Found:\n" + a4);
                    }
                    throw new IllegalStateException("Migration didn't properly handle complicationLastSetting(com.portfolio.platform.data.model.diana.ComplicationLastSetting).\n Expected:\n" + egVar3 + "\n Found:\n" + a3);
                }
                throw new IllegalStateException("Migration didn't properly handle watchApp(com.portfolio.platform.data.model.diana.WatchApp).\n Expected:\n" + egVar2 + "\n Found:\n" + a2);
            }
            throw new IllegalStateException("Migration didn't properly handle complication(com.portfolio.platform.data.model.diana.Complication).\n Expected:\n" + egVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `complication`");
            a.b("DELETE FROM `watchApp`");
            a.b("DELETE FROM `complicationLastSetting`");
            a.b("DELETE FROM `watchAppLastSetting`");
            a.b("DELETE FROM `dianaPreset`");
            a.b("DELETE FROM `dianaRecommendPreset`");
            a.b("DELETE FROM `watch_face`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), "complication", "watchApp", "complicationLastSetting", "watchAppLastSetting", "dianaPreset", "dianaRecommendPreset", "watch_face");
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(13), "b36140bba6a58f9cc8d2af308d56724d", "997a26c4a63a92c3bb7a9ba027efafaa");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }

    @DexIgnore
    public ComplicationDao getComplicationDao() {
        ComplicationDao complicationDao;
        if (this._complicationDao != null) {
            return this._complicationDao;
        }
        synchronized (this) {
            if (this._complicationDao == null) {
                this._complicationDao = new ComplicationDao_Impl(this);
            }
            complicationDao = this._complicationDao;
        }
        return complicationDao;
    }

    @DexIgnore
    public ComplicationLastSettingDao getComplicationSettingDao() {
        ComplicationLastSettingDao complicationLastSettingDao;
        if (this._complicationLastSettingDao != null) {
            return this._complicationLastSettingDao;
        }
        synchronized (this) {
            if (this._complicationLastSettingDao == null) {
                this._complicationLastSettingDao = new ComplicationLastSettingDao_Impl(this);
            }
            complicationLastSettingDao = this._complicationLastSettingDao;
        }
        return complicationLastSettingDao;
    }

    @DexIgnore
    public DianaPresetDao getPresetDao() {
        DianaPresetDao dianaPresetDao;
        if (this._dianaPresetDao != null) {
            return this._dianaPresetDao;
        }
        synchronized (this) {
            if (this._dianaPresetDao == null) {
                this._dianaPresetDao = new DianaPresetDao_Impl(this);
            }
            dianaPresetDao = this._dianaPresetDao;
        }
        return dianaPresetDao;
    }

    @DexIgnore
    public WatchAppDao getWatchAppDao() {
        WatchAppDao watchAppDao;
        if (this._watchAppDao != null) {
            return this._watchAppDao;
        }
        synchronized (this) {
            if (this._watchAppDao == null) {
                this._watchAppDao = new WatchAppDao_Impl(this);
            }
            watchAppDao = this._watchAppDao;
        }
        return watchAppDao;
    }

    @DexIgnore
    public WatchAppLastSettingDao getWatchAppSettingDao() {
        WatchAppLastSettingDao watchAppLastSettingDao;
        if (this._watchAppLastSettingDao != null) {
            return this._watchAppLastSettingDao;
        }
        synchronized (this) {
            if (this._watchAppLastSettingDao == null) {
                this._watchAppLastSettingDao = new WatchAppLastSettingDao_Impl(this);
            }
            watchAppLastSettingDao = this._watchAppLastSettingDao;
        }
        return watchAppLastSettingDao;
    }

    @DexIgnore
    public WatchFaceDao getWatchFaceDao() {
        WatchFaceDao watchFaceDao;
        if (this._watchFaceDao != null) {
            return this._watchFaceDao;
        }
        synchronized (this) {
            if (this._watchFaceDao == null) {
                this._watchFaceDao = new WatchFaceDao_Impl(this);
            }
            watchFaceDao = this._watchFaceDao;
        }
        return watchFaceDao;
    }
}
