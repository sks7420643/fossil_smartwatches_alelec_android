package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvidesAlarmDatabaseFactory implements Factory<AlarmDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesAlarmDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesAlarmDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvidesAlarmDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static AlarmDatabase provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return proxyProvidesAlarmDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static AlarmDatabase proxyProvidesAlarmDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        AlarmDatabase providesAlarmDatabase = portfolioDatabaseModule.providesAlarmDatabase(portfolioApp);
        n44.a(providesAlarmDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return providesAlarmDatabase;
    }

    @DexIgnore
    public AlarmDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
