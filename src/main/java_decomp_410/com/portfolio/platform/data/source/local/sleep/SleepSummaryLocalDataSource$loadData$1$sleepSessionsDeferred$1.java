package com.portfolio.platform.data.source.local.sleep;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1$sleepSessionsDeferred$1", mo27670f = "SleepSummaryLocalDataSource.kt", mo27671l = {176}, mo27672m = "invokeSuspend")
public final class SleepSummaryLocalDataSource$loadData$1$sleepSessionsDeferred$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qo2<com.fossil.blesdk.obfuscated.xz1>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.Pair $downloadingRange;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21129p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummaryLocalDataSource$loadData$1$sleepSessionsDeferred$1(com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1 sleepSummaryLocalDataSource$loadData$1, kotlin.Pair pair, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = sleepSummaryLocalDataSource$loadData$1;
        this.$downloadingRange = pair;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1$sleepSessionsDeferred$1 sleepSummaryLocalDataSource$loadData$1$sleepSessionsDeferred$1 = new com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1$sleepSessionsDeferred$1(this.this$0, this.$downloadingRange, yb4);
        sleepSummaryLocalDataSource$loadData$1$sleepSessionsDeferred$1.f21129p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return sleepSummaryLocalDataSource$loadData$1$sleepSessionsDeferred$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1$sleepSessionsDeferred$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            this.L$0 = this.f21129p$;
            this.label = 1;
            obj = com.portfolio.platform.data.source.SleepSessionsRepository.fetchSleepSessions$default(this.this$0.this$0.mSleepSessionsRepository, (java.util.Date) this.$downloadingRange.getFirst(), (java.util.Date) this.$downloadingRange.getSecond(), 0, 0, this, 12, (java.lang.Object) null);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
