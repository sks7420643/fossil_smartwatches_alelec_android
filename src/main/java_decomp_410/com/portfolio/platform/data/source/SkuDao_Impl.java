package com.portfolio.platform.data.source;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.WatchParam;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SkuDao_Impl implements SkuDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfSKUModel;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfWatchParam;
    @DexIgnore
    public /* final */ wf __preparedStmtOfCleanUpSku;
    @DexIgnore
    public /* final */ wf __preparedStmtOfCleanUpWatchParam;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<SKUModel> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `SKU`(`createdAt`,`updatedAt`,`serialNumberPrefix`,`sku`,`deviceName`,`groupName`,`gender`,`deviceType`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, SKUModel sKUModel) {
            if (sKUModel.getCreatedAt() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, sKUModel.getCreatedAt());
            }
            if (sKUModel.getUpdatedAt() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, sKUModel.getUpdatedAt());
            }
            if (sKUModel.getSerialNumberPrefix() == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, sKUModel.getSerialNumberPrefix());
            }
            if (sKUModel.getSku() == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, sKUModel.getSku());
            }
            if (sKUModel.getDeviceName() == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, sKUModel.getDeviceName());
            }
            if (sKUModel.getGroupName() == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, sKUModel.getGroupName());
            }
            if (sKUModel.getGender() == null) {
                kgVar.a(7);
            } else {
                kgVar.a(7, sKUModel.getGender());
            }
            if (sKUModel.getDeviceType() == null) {
                kgVar.a(8);
            } else {
                kgVar.a(8, sKUModel.getDeviceType());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends lf<WatchParam> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchParam`(`prefixSerial`,`versionMajor`,`versionMinor`,`data`) VALUES (?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, WatchParam watchParam) {
            if (watchParam.getPrefixSerial() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, watchParam.getPrefixSerial());
            }
            if (watchParam.getVersionMajor() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, watchParam.getVersionMajor());
            }
            if (watchParam.getVersionMinor() == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, watchParam.getVersionMinor());
            }
            if (watchParam.getData() == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, watchParam.getData());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends wf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM SKU";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends wf {
        @DexIgnore
        public Anon4(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM watchParam";
        }
    }

    @DexIgnore
    public SkuDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfSKUModel = new Anon1(roomDatabase);
        this.__insertionAdapterOfWatchParam = new Anon2(roomDatabase);
        this.__preparedStmtOfCleanUpSku = new Anon3(roomDatabase);
        this.__preparedStmtOfCleanUpWatchParam = new Anon4(roomDatabase);
    }

    @DexIgnore
    public void addOrUpdateSkuList(List<SKUModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSKUModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void addOrUpdateWatchParam(WatchParam watchParam) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchParam.insert(watchParam);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void cleanUpSku() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfCleanUpSku.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUpSku.release(acquire);
        }
    }

    @DexIgnore
    public void cleanUpWatchParam() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfCleanUpWatchParam.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUpWatchParam.release(acquire);
        }
    }

    @DexIgnore
    public List<SKUModel> getAllSkus() {
        uf b = uf.b("SELECT * FROM SKU", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "createdAt");
            int b3 = ag.b(a, "updatedAt");
            int b4 = ag.b(a, "serialNumberPrefix");
            int b5 = ag.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int b6 = ag.b(a, "deviceName");
            int b7 = ag.b(a, "groupName");
            int b8 = ag.b(a, "gender");
            int b9 = ag.b(a, "deviceType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                SKUModel sKUModel = new SKUModel(a.getString(b4), a.getString(b5), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9));
                sKUModel.setCreatedAt(a.getString(b2));
                sKUModel.setUpdatedAt(a.getString(b3));
                arrayList.add(sKUModel);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public SKUModel getSkuByDeviceIdPrefix(String str) {
        SKUModel sKUModel;
        String str2 = str;
        uf b = uf.b("SELECT * FROM SKU WHERE serialNumberPrefix=?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "createdAt");
            int b3 = ag.b(a, "updatedAt");
            int b4 = ag.b(a, "serialNumberPrefix");
            int b5 = ag.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int b6 = ag.b(a, "deviceName");
            int b7 = ag.b(a, "groupName");
            int b8 = ag.b(a, "gender");
            int b9 = ag.b(a, "deviceType");
            if (a.moveToFirst()) {
                sKUModel = new SKUModel(a.getString(b4), a.getString(b5), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9));
                sKUModel.setCreatedAt(a.getString(b2));
                sKUModel.setUpdatedAt(a.getString(b3));
            } else {
                sKUModel = null;
            }
            return sKUModel;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public WatchParam getWatchParamById(String str) {
        uf b = uf.b("SELECT * FROM watchParam WHERE prefixSerial =?", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            return a.moveToFirst() ? new WatchParam(a.getString(ag.b(a, "prefixSerial")), a.getString(ag.b(a, "versionMajor")), a.getString(ag.b(a, "versionMinor")), a.getString(ag.b(a, "data"))) : null;
        } finally {
            a.close();
            b.c();
        }
    }
}
