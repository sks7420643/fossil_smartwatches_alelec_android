package com.portfolio.platform.data.source.remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$response$1", mo27670f = "MicroAppRemoteDataSource.kt", mo27671l = {40}, mo27672m = "invokeSuspend")
public final class MicroAppRemoteDataSource$getAllMicroAppVariant$response$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.room.microapp.MicroAppVariant>>>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $majorNumber;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $minorNumber;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $serial;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppRemoteDataSource$getAllMicroAppVariant$response$1(com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource microAppRemoteDataSource, java.lang.String str, java.lang.String str2, java.lang.String str3, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(1, yb4);
        this.this$0 = microAppRemoteDataSource;
        this.$serial = str;
        this.$majorNumber = str2;
        this.$minorNumber = str3;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$response$1 microAppRemoteDataSource$getAllMicroAppVariant$response$1 = new com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$response$1(this.this$0, this.$serial, this.$majorNumber, this.$minorNumber, yb4);
        return microAppRemoteDataSource$getAllMicroAppVariant$response$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj) {
        return ((com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$response$1) create((com.fossil.blesdk.obfuscated.yb4) obj)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiServiceV2$p = this.this$0.mApiServiceV2;
            java.lang.String str = this.$serial;
            java.lang.String str2 = this.$majorNumber;
            java.lang.String str3 = this.$minorNumber;
            this.label = 1;
            obj = access$getMApiServiceV2$p.getAllMicroAppVariant(str, str2, str3, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
