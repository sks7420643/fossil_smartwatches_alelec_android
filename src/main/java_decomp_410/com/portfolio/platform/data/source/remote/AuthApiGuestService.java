package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.bt4;
import com.fossil.blesdk.obfuscated.ps4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface AuthApiGuestService {
    @DexIgnore
    @bt4("rpc/auth/check-account-existence-by-email")
    Object checkAuthenticationEmailExisting(@ps4 xz1 xz1, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @bt4("rpc/auth/check-account-existence-by-social")
    Object checkAuthenticationSocialExisting(@ps4 xz1 xz1, yb4<? super qr4<xz1>> yb4);

    @DexIgnore
    @bt4("rpc/auth/login")
    Object loginWithEmail(@ps4 xz1 xz1, yb4<? super qr4<Auth>> yb4);

    @DexIgnore
    @bt4("rpc/auth/login-with")
    Object loginWithSocial(@ps4 xz1 xz1, yb4<? super qr4<Auth>> yb4);

    @DexIgnore
    @bt4("rpc/auth/password/request-reset")
    Object passwordRequestReset(@ps4 xz1 xz1, yb4<? super qr4<Void>> yb4);

    @DexIgnore
    @bt4("rpc/auth/register")
    Object registerEmail(@ps4 SignUpEmailAuth signUpEmailAuth, yb4<? super qr4<Auth>> yb4);

    @DexIgnore
    @bt4("rpc/auth/register-with")
    Object registerSocial(@ps4 SignUpSocialAuth signUpSocialAuth, yb4<? super qr4<Auth>> yb4);

    @DexIgnore
    @bt4("rpc/auth/request-email-otp")
    Object requestEmailOtp(@ps4 xz1 xz1, yb4<? super qr4<Void>> yb4);

    @DexIgnore
    @bt4("rpc/auth/token/exchange-legacy")
    Call<Auth> tokenExchangeLegacy(@ps4 xz1 xz1);

    @DexIgnore
    @bt4("rpc/auth/token/refresh")
    Call<Auth> tokenRefresh(@ps4 xz1 xz1);

    @DexIgnore
    @bt4("rpc/auth/verify-email-otp")
    Object verifyEmailOtp(@ps4 xz1 xz1, yb4<? super qr4<Void>> yb4);
}
