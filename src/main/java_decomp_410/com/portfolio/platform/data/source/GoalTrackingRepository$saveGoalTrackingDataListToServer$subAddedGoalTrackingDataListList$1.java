package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.portfolio.platform.data.model.goaltracking.GoalTrackingData, java.lang.Boolean> {
    @DexIgnore
    public static /* final */ com.portfolio.platform.data.source.GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$1 INSTANCE; // = new com.portfolio.platform.data.source.GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$1();

    @DexIgnore
    public GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke((com.portfolio.platform.data.model.goaltracking.GoalTrackingData) obj));
    }

    @DexIgnore
    public final boolean invoke(com.portfolio.platform.data.model.goaltracking.GoalTrackingData goalTrackingData) {
        com.fossil.blesdk.obfuscated.kd4.b(goalTrackingData, "it");
        return goalTrackingData.getPinType() == 1;
    }
}
