package com.portfolio.platform.data.source.local.sleep;

import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.yf;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepDatabase$Companion$MIGRATION_FROM_2_TO_5$Anon1 extends yf {
    @DexIgnore
    public SleepDatabase$Companion$MIGRATION_FROM_2_TO_5$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    public void migrate(gg ggVar) {
        kd4.b(ggVar, "database");
        FLogger.INSTANCE.getLocal().d(SleepDatabase.TAG, "Migration 2 to 5 start");
        ggVar.s();
        try {
            FLogger.INSTANCE.getLocal().d(SleepDatabase.TAG, "Migrate sleep session table");
            ggVar.b("CREATE TABLE sleep_session_new (date INTEGER NOT NULL, day TEXT NOT NULL, deviceSerialNumber TEXT, syncTime INTEGER, bookmarkTime INTEGER, normalizedSleepQuality REAL NOT NULL DEFAULT 0, source INTEGER NOT NULL DEFAULT 0, realStartTime INTEGER NOT NULL DEFAULT 0, realEndTime INTEGER PRIMARY KEY NOT NULL DEFAULT 0, realSleepMinutes INTEGER NOT NULL DEFAULT 0, realSleepStateDistInMinute TEXT NOT NULL DEFAULT '{\"awake\":0,\"deep\":0,\"light\":0}', editedStartTime INTEGER, editedEndTime INTEGER, editedSleepMinutes INTEGER, editedSleepStateDistInMinute TEXT, sleepStates TEXT NOT NULL DEFAULT '', createdAt INTEGER NOT NULL DEFAULT 0, updatedAt INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 0, timezoneOffset INTEGER NOT NULL DEFAULT 0)");
            ggVar.b("INSERT INTO sleep_session_new (date, day, deviceSerialNumber, syncTime, bookmarkTime, normalizedSleepQuality, source, realStartTime, realEndTime, realSleepMinutes, realSleepMinutes, realSleepStateDistInMinute, editedStartTime, editedEndTime, editedSleepMinutes, editedSleepStateDistInMinute, sleepStates, createdAt, updatedAt, timezoneOffset) SELECT date, day, deviceSerialNumber, syncTime, bookmarkTime, normalizedSleepQuality, source, realStartTime, realEndTime, realSleepMinutes, realSleepMinutes, realSleepStateDistInMinute, editedStartTime, editedEndTime, editedSleepMinutes, editedSleepStateDistInMinute, sleepStates, createdAt, updatedAt, timezoneOffset  FROM sleep_session");
            ggVar.b("DROP TABLE sleep_session");
            ggVar.b("ALTER TABLE sleep_session_new RENAME TO sleep_session");
            FLogger.INSTANCE.getLocal().d(SleepDatabase.TAG, "Migrate sleep session table success");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SleepDatabase.TAG, "Migrate sleep session table fail " + e);
            ggVar.b("DROP TABLE IF EXISTS sleep_session_new");
            ggVar.b("DROP TABLE IF EXISTS sleep_session");
            ggVar.b("CREATE TABLE IF NOT EXISTS sleep_session (date INTEGER NOT NULL, day TEXT NOT NULL, deviceSerialNumber TEXT, syncTime INTEGER, bookmarkTime INTEGER, normalizedSleepQuality REAL NOT NULL DEFAULT 0, source INTEGER NOT NULL DEFAULT 0, realStartTime INTEGER NOT NULL DEFAULT 0, realEndTime INTEGER PRIMARY KEY NOT NULL DEFAULT 0, realSleepMinutes INTEGER NOT NULL DEFAULT 0, realSleepStateDistInMinute TEXT NOT NULL DEFAULT '{\"awake\":0,\"deep\":0,\"light\":0}', editedStartTime INTEGER, editedEndTime INTEGER, editedSleepMinutes INTEGER, editedSleepStateDistInMinute TEXT, sleepStates TEXT NOT NULL DEFAULT '', createdAt INTEGER NOT NULL DEFAULT 0, updatedAt INTEGER, pinType INTEGER NOT NULL DEFAULT 1, timezoneOffset INTEGER NOT NULL DEFAULT 0)");
        }
        try {
            FLogger.INSTANCE.getLocal().d(SleepDatabase.TAG, "Migrate sleep date table");
            ggVar.b("CREATE TABLE sleep_date_new (date TEXT PRIMARY KEY NOT NULL, goalMinutes INTEGER NOT NULL DEFAULT 0, sleepMinutes INTEGER NOT NULL DEFAULT 0, sleepStateDistInMinute TEXT, createdAt INTEGER, updatedAt INTEGER, pinType INTEGER NOT NULL DEFAULT 0, timezoneOffset INTEGER NOT NULL DEFAULT 0)");
            ggVar.b("INSERT INTO sleep_date_new (date, goalMinutes, sleepMinutes, sleepStateDistInMinute, createdAt, updatedAt, timezoneOffset) SELECT date, goalMinutes, sleepMinutes, sleepStateDistInMinute, createdAt, updatedAt, timezoneOffset  FROM sleep_date");
            ggVar.b("DROP TABLE sleep_date");
            ggVar.b("ALTER TABLE sleep_date_new RENAME TO sleep_date");
            FLogger.INSTANCE.getLocal().d(SleepDatabase.TAG, "Migrate sleep date table success");
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(SleepDatabase.TAG, "Migrate sleep date table fail " + e2);
            ggVar.b("DROP TABLE IF EXISTS sleep_date_new");
            ggVar.b("DROP TABLE IF EXISTS sleep_date");
            ggVar.b("CREATE TABLE IF NOT EXISTS sleep_date (date TEXT PRIMARY KEY NOT NULL, goalMinutes INTEGER NOT NULL DEFAULT 0, sleepMinutes INTEGER NOT NULL DEFAULT 0, sleepStateDistInMinute TEXT, createdAt INTEGER, updatedAt INTEGER, pinType INTEGER NOT NULL DEFAULT 0, timezoneOffset INTEGER NOT NULL DEFAULT 0)");
        }
        try {
            FLogger.INSTANCE.getLocal().d(SleepDatabase.TAG, "Migrate sleep setting");
            ggVar.b("CREATE TABLE sleep_settings (sleepGoal INTEGER NOT NULL DEFAULT 480, id INTEGER PRIMARY KEY ASC NOT NULL)");
            ggVar.b("INSERT INTO sleep_settings (sleepGoal) SELECT minute FROM sleep_goal ORDER BY date DESC LIMIT 1");
            ggVar.b("DROP TABLE sleep_goal");
            FLogger.INSTANCE.getLocal().d(SleepDatabase.TAG, "Migrate sleep setting success");
        } catch (Exception e3) {
            ggVar.b("CREATE TABLE IF NOT EXISTS sleep_settings (sleepGoal INTEGER NOT NULL DEFAULT 480, id INTEGER PRIMARY KEY ASC NOT NULL)");
            ggVar.b("DROP TABLE IF EXISTS sleep_goal");
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d(SleepDatabase.TAG, "Migrate sleep setting fail " + e3);
        }
        ggVar.b("CREATE TABLE sleepRecommendedGoals (recommendedSleepGoal INTEGER NOT NULL, id INTEGER PRIMARY KEY ASC NOT NULL)");
        try {
            ggVar.b("CREATE TABLE IF NOT EXISTS sleep_statistic (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, sleepMinutesBestDay TEXT, sleepTimeBestStreak TEXT, totalDays INTEGER NOT NULL, totalSleeps REAL NOT NULL, totalSleepMinutes INTEGER NOT NULL, totalSleepStateDistInMinute TEXT NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
        } catch (Exception e4) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.e(SleepDatabase.TAG, "MIGRATION_FROM_2_TO_5 - SleepStatistic -- e=" + e4);
            e4.printStackTrace();
        }
        ggVar.u();
        ggVar.v();
    }
}
