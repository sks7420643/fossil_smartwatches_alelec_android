package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$2 */
public final class C5737x754ae4cc implements com.fossil.blesdk.obfuscated.sn1 {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.dg4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $countSizeOfList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $gFitWorkoutSessionList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.google.android.gms.auth.api.signin.GoogleSignInAccount $googleSignInAccount$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfGFitWorkoutSessionList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository this$0;

    @DexIgnore
    public C5737x754ae4cc(com.google.android.gms.auth.api.signin.GoogleSignInAccount googleSignInAccount, kotlin.jvm.internal.Ref$IntRef ref$IntRef, int i, com.fossil.blesdk.obfuscated.dg4 dg4, com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository, java.util.List list, java.lang.String str) {
        this.$googleSignInAccount$inlined = googleSignInAccount;
        this.$countSizeOfList$inlined = ref$IntRef;
        this.$sizeOfGFitWorkoutSessionList$inlined = i;
        this.$continuation$inlined = dg4;
        this.this$0 = thirdPartyRepository;
        this.$gFitWorkoutSessionList$inlined = list;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    public final void onFailure(java.lang.Exception exc) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(exc, "it");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("There was a problem inserting the session: ");
        exc.printStackTrace();
        sb.append(com.fossil.blesdk.obfuscated.qa4.f17909a);
        local.mo33256e(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, sb.toString());
        kotlin.jvm.internal.Ref$IntRef ref$IntRef = this.$countSizeOfList$inlined;
        ref$IntRef.element++;
        if (ref$IntRef.element >= this.$sizeOfGFitWorkoutSessionList$inlined && this.$continuation$inlined.isActive()) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "End saveGFitWorkoutSessionToGoogleFit");
            com.fossil.blesdk.obfuscated.dg4 dg4 = this.$continuation$inlined;
            kotlin.Result.C7350a aVar = kotlin.Result.Companion;
            dg4.resumeWith(kotlin.Result.m37419constructorimpl((java.lang.Object) null));
        }
    }
}
