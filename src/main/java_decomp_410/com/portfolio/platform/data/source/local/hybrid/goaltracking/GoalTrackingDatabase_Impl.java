package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDatabase_Impl extends GoalTrackingDatabase {
    @DexIgnore
    public volatile GoalTrackingDao _goalTrackingDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `goalTrackingRaw` (`pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `trackedAt` TEXT NOT NULL, `timezoneOffsetInSecond` INTEGER NOT NULL, `date` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `goalTrackingDay` (`pinType` INTEGER NOT NULL, `date` TEXT NOT NULL, `totalTracked` INTEGER NOT NULL, `goalTarget` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`date`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `goalSetting` (`id` INTEGER NOT NULL, `currentTarget` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'ac5e072da94cafa67a74f62806ddb95f')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `goalTrackingRaw`");
            ggVar.b("DROP TABLE IF EXISTS `goalTrackingDay`");
            ggVar.b("DROP TABLE IF EXISTS `goalSetting`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (GoalTrackingDatabase_Impl.this.mCallbacks != null) {
                int size = GoalTrackingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) GoalTrackingDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = GoalTrackingDatabase_Impl.this.mDatabase = ggVar;
            GoalTrackingDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (GoalTrackingDatabase_Impl.this.mCallbacks != null) {
                int size = GoalTrackingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) GoalTrackingDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            HashMap hashMap = new HashMap(7);
            hashMap.put("pinType", new eg.a("pinType", "INTEGER", true, 0));
            hashMap.put("id", new eg.a("id", "TEXT", true, 1));
            hashMap.put(GoalTrackingEvent.COLUMN_TRACKED_AT, new eg.a(GoalTrackingEvent.COLUMN_TRACKED_AT, "TEXT", true, 0));
            hashMap.put("timezoneOffsetInSecond", new eg.a("timezoneOffsetInSecond", "INTEGER", true, 0));
            hashMap.put("date", new eg.a("date", "TEXT", true, 0));
            hashMap.put("createdAt", new eg.a("createdAt", "INTEGER", true, 0));
            hashMap.put("updatedAt", new eg.a("updatedAt", "INTEGER", true, 0));
            eg egVar = new eg("goalTrackingRaw", hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar, "goalTrackingRaw");
            if (egVar.equals(a)) {
                HashMap hashMap2 = new HashMap(6);
                hashMap2.put("pinType", new eg.a("pinType", "INTEGER", true, 0));
                hashMap2.put("date", new eg.a("date", "TEXT", true, 1));
                hashMap2.put("totalTracked", new eg.a("totalTracked", "INTEGER", true, 0));
                hashMap2.put("goalTarget", new eg.a("goalTarget", "INTEGER", true, 0));
                hashMap2.put("createdAt", new eg.a("createdAt", "INTEGER", true, 0));
                hashMap2.put("updatedAt", new eg.a("updatedAt", "INTEGER", true, 0));
                eg egVar2 = new eg("goalTrackingDay", hashMap2, new HashSet(0), new HashSet(0));
                eg a2 = eg.a(ggVar, "goalTrackingDay");
                if (egVar2.equals(a2)) {
                    HashMap hashMap3 = new HashMap(2);
                    hashMap3.put("id", new eg.a("id", "INTEGER", true, 1));
                    hashMap3.put("currentTarget", new eg.a("currentTarget", "INTEGER", true, 0));
                    eg egVar3 = new eg("goalSetting", hashMap3, new HashSet(0), new HashSet(0));
                    eg a3 = eg.a(ggVar, "goalSetting");
                    if (!egVar3.equals(a3)) {
                        throw new IllegalStateException("Migration didn't properly handle goalSetting(com.portfolio.platform.data.model.GoalSetting).\n Expected:\n" + egVar3 + "\n Found:\n" + a3);
                    }
                    return;
                }
                throw new IllegalStateException("Migration didn't properly handle goalTrackingDay(com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary).\n Expected:\n" + egVar2 + "\n Found:\n" + a2);
            }
            throw new IllegalStateException("Migration didn't properly handle goalTrackingRaw(com.portfolio.platform.data.model.goaltracking.GoalTrackingData).\n Expected:\n" + egVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `goalTrackingRaw`");
            a.b("DELETE FROM `goalTrackingDay`");
            a.b("DELETE FROM `goalSetting`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), "goalTrackingRaw", "goalTrackingDay", "goalSetting");
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(2), "ac5e072da94cafa67a74f62806ddb95f", "56ec29011feb0e074f1d2ba7ecbcbb17");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }

    @DexIgnore
    public GoalTrackingDao getGoalTrackingDao() {
        GoalTrackingDao goalTrackingDao;
        if (this._goalTrackingDao != null) {
            return this._goalTrackingDao;
        }
        synchronized (this) {
            if (this._goalTrackingDao == null) {
                this._goalTrackingDao = new GoalTrackingDao_Impl(this);
            }
            goalTrackingDao = this._goalTrackingDao;
        }
        return goalTrackingDao;
    }
}
