package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.tz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.response.ResponseKt;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmsRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = AlarmsRemoteDataSource.class.getSimpleName();
        kd4.a((Object) simpleName, "AlarmsRemoteDataSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public AlarmsRemoteDataSource(ApiServiceV2 apiServiceV2) {
        kd4.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    private final tz1 intArray2JsonArray(int[] iArr) {
        tz1 tz1 = new tz1();
        if (iArr == null) {
            iArr = new int[0];
        }
        Calendar instance = Calendar.getInstance();
        int length = iArr.length;
        int i = 0;
        while (i < length) {
            instance.set(7, iArr[i]);
            String displayName = instance.getDisplayName(7, 2, Locale.US);
            kd4.a((Object) displayName, "calendar.getDisplayName(\u2026Calendar.LONG, Locale.US)");
            if (displayName != null) {
                String substring = displayName.substring(0, 3);
                kd4.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                if (substring != null) {
                    String lowerCase = substring.toLowerCase();
                    kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    tz1.a(lowerCase);
                    i++;
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
        return tz1;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object deleteAlarm(String str, yb4<? super qo2<Void>> yb4) {
        AlarmsRemoteDataSource$deleteAlarm$Anon1 alarmsRemoteDataSource$deleteAlarm$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof AlarmsRemoteDataSource$deleteAlarm$Anon1) {
            alarmsRemoteDataSource$deleteAlarm$Anon1 = (AlarmsRemoteDataSource$deleteAlarm$Anon1) yb4;
            int i2 = alarmsRemoteDataSource$deleteAlarm$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRemoteDataSource$deleteAlarm$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRemoteDataSource$deleteAlarm$Anon1.result;
                Object a = cc4.a();
                i = alarmsRemoteDataSource$deleteAlarm$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    AlarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1 alarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1 = new AlarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1(this, str, (yb4) null);
                    alarmsRemoteDataSource$deleteAlarm$Anon1.L$Anon0 = this;
                    alarmsRemoteDataSource$deleteAlarm$Anon1.L$Anon1 = str;
                    alarmsRemoteDataSource$deleteAlarm$Anon1.label = 1;
                    obj = ResponseKt.a(alarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1, alarmsRemoteDataSource$deleteAlarm$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    String str2 = (String) alarmsRemoteDataSource$deleteAlarm$Anon1.L$Anon1;
                    AlarmsRemoteDataSource alarmsRemoteDataSource = (AlarmsRemoteDataSource) alarmsRemoteDataSource$deleteAlarm$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local.d(str3, "deleteAlarm onResponse: response = " + qo2);
                if (!(qo2 instanceof ro2)) {
                    return new ro2((Object) null, false, 2, (fd4) null);
                }
                if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), po2.d(), (String) null, 8, (fd4) null);
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        alarmsRemoteDataSource$deleteAlarm$Anon1 = new AlarmsRemoteDataSource$deleteAlarm$Anon1(this, yb4);
        Object obj2 = alarmsRemoteDataSource$deleteAlarm$Anon1.result;
        Object a2 = cc4.a();
        i = alarmsRemoteDataSource$deleteAlarm$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str32 = TAG;
        local2.d(str32, "deleteAlarm onResponse: response = " + qo2);
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object deleteAlarms(List<Alarm> list, yb4<? super qo2<Void>> yb4) {
        AlarmsRemoteDataSource$deleteAlarms$Anon1 alarmsRemoteDataSource$deleteAlarms$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof AlarmsRemoteDataSource$deleteAlarms$Anon1) {
            alarmsRemoteDataSource$deleteAlarms$Anon1 = (AlarmsRemoteDataSource$deleteAlarms$Anon1) yb4;
            int i2 = alarmsRemoteDataSource$deleteAlarms$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRemoteDataSource$deleteAlarms$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRemoteDataSource$deleteAlarms$Anon1.result;
                Object a = cc4.a();
                i = alarmsRemoteDataSource$deleteAlarms$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    xz1 xz1 = new xz1();
                    tz1 tz1 = new tz1();
                    for (Alarm id : list) {
                        tz1.a(id.getId());
                    }
                    xz1.a("_ids", (JsonElement) tz1);
                    AlarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1 alarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1 = new AlarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1(this, xz1, (yb4) null);
                    alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon0 = this;
                    alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon1 = list;
                    alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon2 = xz1;
                    alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon3 = tz1;
                    alarmsRemoteDataSource$deleteAlarms$Anon1.label = 1;
                    obj = ResponseKt.a(alarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1, alarmsRemoteDataSource$deleteAlarms$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    tz1 tz12 = (tz1) alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon3;
                    xz1 xz12 = (xz1) alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon2;
                    List list2 = (List) alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon1;
                    AlarmsRemoteDataSource alarmsRemoteDataSource = (AlarmsRemoteDataSource) alarmsRemoteDataSource$deleteAlarms$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = TAG;
                local.d(str, "deleteAlarms onResponse: response = " + qo2);
                if (!(qo2 instanceof ro2)) {
                    return new ro2((Object) null, false, 2, (fd4) null);
                }
                if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), po2.d(), (String) null, 8, (fd4) null);
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        alarmsRemoteDataSource$deleteAlarms$Anon1 = new AlarmsRemoteDataSource$deleteAlarms$Anon1(this, yb4);
        Object obj2 = alarmsRemoteDataSource$deleteAlarms$Anon1.result;
        Object a2 = cc4.a();
        i = alarmsRemoteDataSource$deleteAlarms$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "deleteAlarms onResponse: response = " + qo2);
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getAlarms(yb4<? super qo2<List<Alarm>>> yb4) {
        AlarmsRemoteDataSource$getAlarms$Anon1 alarmsRemoteDataSource$getAlarms$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof AlarmsRemoteDataSource$getAlarms$Anon1) {
            alarmsRemoteDataSource$getAlarms$Anon1 = (AlarmsRemoteDataSource$getAlarms$Anon1) yb4;
            int i2 = alarmsRemoteDataSource$getAlarms$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRemoteDataSource$getAlarms$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRemoteDataSource$getAlarms$Anon1.result;
                Object a = cc4.a();
                i = alarmsRemoteDataSource$getAlarms$Anon1.label;
                List list = null;
                if (i != 0) {
                    na4.a(obj);
                    AlarmsRemoteDataSource$getAlarms$repoResponse$Anon1 alarmsRemoteDataSource$getAlarms$repoResponse$Anon1 = new AlarmsRemoteDataSource$getAlarms$repoResponse$Anon1(this, (yb4) null);
                    alarmsRemoteDataSource$getAlarms$Anon1.L$Anon0 = this;
                    alarmsRemoteDataSource$getAlarms$Anon1.label = 1;
                    obj = ResponseKt.a(alarmsRemoteDataSource$getAlarms$repoResponse$Anon1, alarmsRemoteDataSource$getAlarms$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    AlarmsRemoteDataSource alarmsRemoteDataSource = (AlarmsRemoteDataSource) alarmsRemoteDataSource$getAlarms$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = TAG;
                local.d(str, "getAlarms onResponse: response = " + qo2);
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    ApiResponse apiResponse = (ApiResponse) ro2.a();
                    if (apiResponse != null) {
                        list = apiResponse.get_items();
                    }
                    return new ro2(list, ro2.b());
                } else if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), po2.d(), (String) null, 8, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        alarmsRemoteDataSource$getAlarms$Anon1 = new AlarmsRemoteDataSource$getAlarms$Anon1(this, yb4);
        Object obj2 = alarmsRemoteDataSource$getAlarms$Anon1.result;
        Object a2 = cc4.a();
        i = alarmsRemoteDataSource$getAlarms$Anon1.label;
        List list2 = null;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "getAlarms onResponse: response = " + qo2);
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0115  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object upsertAlarms(List<Alarm> list, yb4<? super qo2<List<Alarm>>> yb4) {
        AlarmsRemoteDataSource$upsertAlarms$Anon1 alarmsRemoteDataSource$upsertAlarms$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof AlarmsRemoteDataSource$upsertAlarms$Anon1) {
            alarmsRemoteDataSource$upsertAlarms$Anon1 = (AlarmsRemoteDataSource$upsertAlarms$Anon1) yb4;
            int i2 = alarmsRemoteDataSource$upsertAlarms$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRemoteDataSource$upsertAlarms$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRemoteDataSource$upsertAlarms$Anon1.result;
                Object a = cc4.a();
                i = alarmsRemoteDataSource$upsertAlarms$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    tz1 tz1 = new tz1();
                    for (Alarm next : list) {
                        xz1 xz1 = new xz1();
                        xz1.a(AppFilter.COLUMN_HOUR, (Number) dc4.a(next.getHour()));
                        xz1.a(MFSleepGoal.COLUMN_MINUTE, (Number) dc4.a(next.getMinute()));
                        xz1.a("id", next.getUri());
                        xz1.a("isRepeated", dc4.a(next.isRepeated()));
                        xz1.a("isActive", dc4.a(next.isActive()));
                        xz1.a("title", next.getTitle());
                        xz1.a(com.misfit.frameworks.buttonservice.model.Alarm.COLUMN_DAYS, (JsonElement) intArray2JsonArray(next.getDays()));
                        tz1.a((JsonElement) xz1);
                    }
                    xz1 xz12 = new xz1();
                    xz12.a(CloudLogWriter.ITEMS_PARAM, (JsonElement) tz1);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "upsertAlarms: body " + xz12);
                    AlarmsRemoteDataSource$upsertAlarms$repoResponse$Anon1 alarmsRemoteDataSource$upsertAlarms$repoResponse$Anon1 = new AlarmsRemoteDataSource$upsertAlarms$repoResponse$Anon1(this, xz12, (yb4) null);
                    alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon0 = this;
                    alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon1 = list;
                    alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon2 = tz1;
                    alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon3 = xz12;
                    alarmsRemoteDataSource$upsertAlarms$Anon1.label = 1;
                    obj = ResponseKt.a(alarmsRemoteDataSource$upsertAlarms$repoResponse$Anon1, alarmsRemoteDataSource$upsertAlarms$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    xz1 xz13 = (xz1) alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon3;
                    tz1 tz12 = (tz1) alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon2;
                    List list2 = (List) alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon1;
                    AlarmsRemoteDataSource alarmsRemoteDataSource = (AlarmsRemoteDataSource) alarmsRemoteDataSource$upsertAlarms$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "upsertAlarms onResponse: response = " + qo2);
                if (!(qo2 instanceof ro2)) {
                    ApiResponse apiResponse = (ApiResponse) ((ro2) qo2).a();
                    return new ro2(apiResponse != null ? apiResponse.get_items() : null, false, 2, (fd4) null);
                } else if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), po2.d(), (String) null, 8, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        alarmsRemoteDataSource$upsertAlarms$Anon1 = new AlarmsRemoteDataSource$upsertAlarms$Anon1(this, yb4);
        Object obj2 = alarmsRemoteDataSource$upsertAlarms$Anon1.result;
        Object a2 = cc4.a();
        i = alarmsRemoteDataSource$upsertAlarms$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        String str22 = TAG;
        local22.d(str22, "upsertAlarms onResponse: response = " + qo2);
        if (!(qo2 instanceof ro2)) {
        }
    }
}
