package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CategoryRemoteDataSource {
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore
    public CategoryRemoteDataSource(ApiServiceV2 apiServiceV2) {
        kd4.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getAllCategory(yb4<? super qo2<List<Category>>> yb4) {
        CategoryRemoteDataSource$getAllCategory$Anon1 categoryRemoteDataSource$getAllCategory$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof CategoryRemoteDataSource$getAllCategory$Anon1) {
            categoryRemoteDataSource$getAllCategory$Anon1 = (CategoryRemoteDataSource$getAllCategory$Anon1) yb4;
            int i2 = categoryRemoteDataSource$getAllCategory$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                categoryRemoteDataSource$getAllCategory$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = categoryRemoteDataSource$getAllCategory$Anon1.result;
                Object a = cc4.a();
                i = categoryRemoteDataSource$getAllCategory$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    CategoryRemoteDataSource$getAllCategory$response$Anon1 categoryRemoteDataSource$getAllCategory$response$Anon1 = new CategoryRemoteDataSource$getAllCategory$response$Anon1(this, (yb4) null);
                    categoryRemoteDataSource$getAllCategory$Anon1.L$Anon0 = this;
                    categoryRemoteDataSource$getAllCategory$Anon1.label = 1;
                    obj = ResponseKt.a(categoryRemoteDataSource$getAllCategory$response$Anon1, categoryRemoteDataSource$getAllCategory$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    CategoryRemoteDataSource categoryRemoteDataSource = (CategoryRemoteDataSource) categoryRemoteDataSource$getAllCategory$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ArrayList arrayList = new ArrayList();
                    ApiResponse apiResponse = (ApiResponse) ((ro2) qo2).a();
                    if (apiResponse != null) {
                        List list = apiResponse.get_items();
                        if (list != null) {
                            dc4.a(arrayList.addAll(list));
                        }
                    }
                    return new ro2(arrayList, false, 2, (fd4) null);
                } else if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), po2.d(), (String) null, 8, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        categoryRemoteDataSource$getAllCategory$Anon1 = new CategoryRemoteDataSource$getAllCategory$Anon1(this, yb4);
        Object obj2 = categoryRemoteDataSource$getAllCategory$Anon1.result;
        Object a2 = cc4.a();
        i = categoryRemoteDataSource$getAllCategory$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }
}
