package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon2", f = "DeviceRepository.kt", l = {}, m = "invokeSuspend")
public final class DeviceRepository$downloadDeviceList$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ qo2 $response;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceRepository this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRepository$downloadDeviceList$Anon2(DeviceRepository deviceRepository, qo2 qo2, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = deviceRepository;
        this.$response = qo2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DeviceRepository$downloadDeviceList$Anon2 deviceRepository$downloadDeviceList$Anon2 = new DeviceRepository$downloadDeviceList$Anon2(this.this$Anon0, this.$response, yb4);
        deviceRepository$downloadDeviceList$Anon2.p$ = (zg4) obj;
        return deviceRepository$downloadDeviceList$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DeviceRepository$downloadDeviceList$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            ApiResponse apiResponse = (ApiResponse) ((ro2) this.$response).a();
            List<Device> list = apiResponse != null ? apiResponse.get_items() : null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = DeviceRepository.Companion.getTAG();
            local.d(tag, "downloadDeviceList() - isFromCache = false " + list);
            this.this$Anon0.removeStealDevice(list);
            if (list != null) {
                for (Device device : list) {
                    if (DeviceHelper.o.e(device.getDeviceId())) {
                        Device deviceByDeviceId = this.this$Anon0.mDeviceDao.getDeviceByDeviceId(device.getDeviceId());
                        if (deviceByDeviceId != null) {
                            if (device.getBatteryLevel() <= 0) {
                                device.setBatteryLevel(deviceByDeviceId.getBatteryLevel());
                            }
                            device.setVibrationStrength(deviceByDeviceId.getVibrationStrength());
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String tag2 = DeviceRepository.Companion.getTAG();
                        local2.d(tag2, "update device: " + device);
                        this.this$Anon0.mDeviceDao.addOrUpdateDevice(device);
                    } else {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String tag3 = DeviceRepository.Companion.getTAG();
                        local3.d(tag3, "Ignoring legacy device=" + device.getDeviceId());
                    }
                }
                return qa4.a;
            }
            kd4.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
