package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Date;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3 implements GoalTrackingRepository.PushPendingGoalTrackingDataListCallback {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$Anon0;

    @DexIgnore
    public GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3(GoalTrackingRepository goalTrackingRepository) {
        this.this$Anon0 = goalTrackingRepository;
    }

    @DexIgnore
    public void onFail(int i) {
        FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "pushPendingGoalTrackingDataList onFetchFailed");
    }

    @DexIgnore
    public void onSuccess(List<GoalTrackingData> list) {
        kd4.b(list, "goalTrackingList");
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = list.get(0).getDate();
        Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
        ref$ObjectRef2.element = list.get(0).getDate();
        for (GoalTrackingData next : list) {
            if (next.getDate().getTime() < ((Date) ref$ObjectRef.element).getTime()) {
                ref$ObjectRef.element = next.getDate();
            }
            if (next.getDate().getTime() > ((Date) ref$ObjectRef2.element).getTime()) {
                ref$ObjectRef2.element = next.getDate();
            }
        }
        fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1(this, ref$ObjectRef, ref$ObjectRef2, (yb4) null), 3, (Object) null);
    }
}
