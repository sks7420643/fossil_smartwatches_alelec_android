package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceDatabase$Companion$MIGRATION_FROM_1_TO_2$1 extends com.fossil.blesdk.obfuscated.C3361yf {
    @DexIgnore
    public DeviceDatabase$Companion$MIGRATION_FROM_1_TO_2$1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    public void migrate(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(ggVar, "database");
        ggVar.mo11234s();
        try {
            ggVar.mo11230b("CREATE TABLE `watchParam` (`prefixSerial` TEXT, `versionMajor` TEXT, `versionMinor` TEXT, `data` TEXTPRIMARY KEY(`prefixSerial`))");
        } catch (java.lang.Exception unused) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.DeviceDatabase.TAG, "migrate DeviceDatabase from 1->2 failed");
            ggVar.mo11230b("DROP TABLE IF EXISTS watchParam");
            ggVar.mo11230b("CREATE TABLE IF NOT EXISTS watchParam (prefixSerial TEXT PRIMARY KEY NOT NULL, versionMajor TEXT, versionMinor TEXT, data TEXT)");
        }
        ggVar.mo11236u();
        ggVar.mo11237v();
    }
}
