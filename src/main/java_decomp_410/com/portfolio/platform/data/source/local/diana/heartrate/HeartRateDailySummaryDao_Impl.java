package com.portfolio.platform.data.source.local.diana.heartrate;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.a72;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.ld;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.q72;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.fossil.blesdk.obfuscated.zf;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateDailySummaryDao_Impl extends HeartRateDailySummaryDao {
    @DexIgnore
    public /* final */ a72 __dateShortStringConverter; // = new a72();
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfDailyHeartRateSummary;
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeleteAllHeartRateSummaries;
    @DexIgnore
    public /* final */ q72 __restingConverter; // = new q72();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<DailyHeartRateSummary> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `daily_heart_rate_summary`(`average`,`date`,`createdAt`,`updatedAt`,`min`,`max`,`minuteCount`,`resting`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, DailyHeartRateSummary dailyHeartRateSummary) {
            kgVar.a(1, (double) dailyHeartRateSummary.getAverage());
            String a = HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.a(dailyHeartRateSummary.getDate());
            if (a == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, a);
            }
            kgVar.b(3, dailyHeartRateSummary.getCreatedAt());
            kgVar.b(4, dailyHeartRateSummary.getUpdatedAt());
            kgVar.b(5, (long) dailyHeartRateSummary.getMin());
            kgVar.b(6, (long) dailyHeartRateSummary.getMax());
            kgVar.b(7, (long) dailyHeartRateSummary.getMinuteCount());
            String a2 = HeartRateDailySummaryDao_Impl.this.__restingConverter.a(dailyHeartRateSummary.getResting());
            if (a2 == null) {
                kgVar.a(8);
            } else {
                kgVar.a(8, a2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends wf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM daily_heart_rate_summary";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<DailyHeartRateSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon3(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<DailyHeartRateSummary> call() throws Exception {
            Cursor a = bg.a(HeartRateDailySummaryDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, GoalTrackingSummary.COLUMN_AVERAGE);
                int b2 = ag.b(a, "date");
                int b3 = ag.b(a, "createdAt");
                int b4 = ag.b(a, "updatedAt");
                int b5 = ag.b(a, "min");
                int b6 = ag.b(a, "max");
                int b7 = ag.b(a, "minuteCount");
                int b8 = ag.b(a, "resting");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new DailyHeartRateSummary(a.getFloat(b), HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.a(a.getString(b2)), a.getLong(b3), a.getLong(b4), a.getInt(b5), a.getInt(b6), a.getInt(b7), HeartRateDailySummaryDao_Impl.this.__restingConverter.a(a.getString(b8))));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends ld.b<Integer, DailyHeartRateSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1 extends zf<DailyHeartRateSummary> {
            @DexIgnore
            public Anon1(RoomDatabase roomDatabase, uf ufVar, boolean z, String... strArr) {
                super(roomDatabase, ufVar, z, strArr);
            }

            @DexIgnore
            public List<DailyHeartRateSummary> convertRows(Cursor cursor) {
                Cursor cursor2 = cursor;
                int b = ag.b(cursor2, GoalTrackingSummary.COLUMN_AVERAGE);
                int b2 = ag.b(cursor2, "date");
                int b3 = ag.b(cursor2, "createdAt");
                int b4 = ag.b(cursor2, "updatedAt");
                int b5 = ag.b(cursor2, "min");
                int b6 = ag.b(cursor2, "max");
                int b7 = ag.b(cursor2, "minuteCount");
                int b8 = ag.b(cursor2, "resting");
                ArrayList arrayList = new ArrayList(cursor.getCount());
                while (cursor.moveToNext()) {
                    arrayList.add(new DailyHeartRateSummary(cursor2.getFloat(b), HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.a(cursor2.getString(b2)), cursor2.getLong(b3), cursor2.getLong(b4), cursor2.getInt(b5), cursor2.getInt(b6), cursor2.getInt(b7), HeartRateDailySummaryDao_Impl.this.__restingConverter.a(cursor2.getString(b8))));
                }
                return arrayList;
            }
        }

        @DexIgnore
        public Anon4(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public zf<DailyHeartRateSummary> create() {
            return new Anon1(HeartRateDailySummaryDao_Impl.this.__db, this.val$_statement, false, "daily_heart_rate_summary");
        }
    }

    @DexIgnore
    public HeartRateDailySummaryDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfDailyHeartRateSummary = new Anon1(roomDatabase);
        this.__preparedStmtOfDeleteAllHeartRateSummaries = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void deleteAllHeartRateSummaries() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeleteAllHeartRateSummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllHeartRateSummaries.release(acquire);
        }
    }

    @DexIgnore
    public List<DailyHeartRateSummary> getDailyHeartRateSummariesDesc(Date date, Date date2) {
        uf b = uf.b("SELECT * FROM daily_heart_rate_summary WHERE date >= ? AND date <= ? ORDER BY date DESC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a3, GoalTrackingSummary.COLUMN_AVERAGE);
            int b3 = ag.b(a3, "date");
            int b4 = ag.b(a3, "createdAt");
            int b5 = ag.b(a3, "updatedAt");
            int b6 = ag.b(a3, "min");
            int b7 = ag.b(a3, "max");
            int b8 = ag.b(a3, "minuteCount");
            int b9 = ag.b(a3, "resting");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                arrayList.add(new DailyHeartRateSummary(a3.getFloat(b2), this.__dateShortStringConverter.a(a3.getString(b3)), a3.getLong(b4), a3.getLong(b5), a3.getInt(b6), a3.getInt(b7), a3.getInt(b8), this.__restingConverter.a(a3.getString(b9))));
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<DailyHeartRateSummary>> getDailyHeartRateSummariesLiveData(Date date, Date date2) {
        uf b = uf.b("SELECT * FROM daily_heart_rate_summary WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"daily_heart_rate_summary"}, false, new Anon3(b));
    }

    @DexIgnore
    public DailyHeartRateSummary getDailyHeartRateSummary(Date date) {
        DailyHeartRateSummary dailyHeartRateSummary;
        uf b = uf.b("SELECT * FROM daily_heart_rate_summary WHERE date = ?", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a2, GoalTrackingSummary.COLUMN_AVERAGE);
            int b3 = ag.b(a2, "date");
            int b4 = ag.b(a2, "createdAt");
            int b5 = ag.b(a2, "updatedAt");
            int b6 = ag.b(a2, "min");
            int b7 = ag.b(a2, "max");
            int b8 = ag.b(a2, "minuteCount");
            int b9 = ag.b(a2, "resting");
            if (a2.moveToFirst()) {
                dailyHeartRateSummary = new DailyHeartRateSummary(a2.getFloat(b2), this.__dateShortStringConverter.a(a2.getString(b3)), a2.getLong(b4), a2.getLong(b5), a2.getInt(b6), a2.getInt(b7), a2.getInt(b8), this.__restingConverter.a(a2.getString(b9)));
            } else {
                dailyHeartRateSummary = null;
            }
            return dailyHeartRateSummary;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public Date getLastDate() {
        Date date;
        uf b = uf.b("SELECT date FROM daily_heart_rate_summary ORDER BY date ASC LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            if (a.moveToFirst()) {
                date = this.__dateShortStringConverter.a(a.getString(0));
            } else {
                date = null;
            }
            return date;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public ld.b<Integer, DailyHeartRateSummary> getSummariesDataSource() {
        return new Anon4(uf.b("SELECT * FROM daily_heart_rate_summary ORDER BY date DESC", 0));
    }

    @DexIgnore
    public void insertDailyHeartRateSummary(DailyHeartRateSummary dailyHeartRateSummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDailyHeartRateSummary.insert(dailyHeartRateSummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListDailyHeartRateSummary(List<DailyHeartRateSummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDailyHeartRateSummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
