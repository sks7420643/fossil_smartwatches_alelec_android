package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository$getSummaries$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.GoalTrackingRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$1$1")
    /* renamed from: com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$1$1 */
    public static final class C56981 extends com.portfolio.platform.util.NetworkBoundResource<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>, com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.Pair $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$1 this$0;

        @DexIgnore
        public C56981(com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$1 goalTrackingRepository$getSummaries$1, kotlin.Pair pair) {
            this.this$0 = goalTrackingRepository$getSummaries$1;
            this.$downloadingDate = pair;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
            if (r0 != null) goto L_0x0019;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
            if (r0 != null) goto L_0x0033;
         */
        @DexIgnore
        public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary>>> yb4) {
            java.util.Date date;
            java.util.Date date2;
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiServiceV2$p = this.this$0.this$0.mApiServiceV2;
            kotlin.Pair pair = this.$downloadingDate;
            if (pair != null) {
                date = (java.util.Date) pair.getFirst();
            }
            date = this.this$0.$startDate;
            java.lang.String e = com.fossil.blesdk.obfuscated.rk2.m27397e(date);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            kotlin.Pair pair2 = this.$downloadingDate;
            if (pair2 != null) {
                date2 = (java.util.Date) pair2.getSecond();
            }
            date2 = this.this$0.$endDate;
            java.lang.String e2 = com.fossil.blesdk.obfuscated.rk2.m27397e(date2);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiServiceV2$p.getGoalTrackingSummaries(e, e2, 0, 100, yb4);
        }

        @DexIgnore
        public androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>> loadFromDb() {
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao access$getMGoalTrackingDao$p = this.this$0.this$0.mGoalTrackingDao;
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$1 goalTrackingRepository$getSummaries$1 = this.this$0;
            return access$getMGoalTrackingDao$p.getGoalTrackingSummariesLiveData(goalTrackingRepository$getSummaries$1.$startDate, goalTrackingRepository$getSummaries$1.$endDate);
        }

        @DexIgnore
        public void onFetchFailed(java.lang.Throwable th) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(com.portfolio.platform.data.source.GoalTrackingRepository.Companion.getTAG(), "getSummaries onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary> apiResponse) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(apiResponse, "item");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tag = com.portfolio.platform.data.source.GoalTrackingRepository.Companion.getTAG();
            local.mo33255d(tag, "getSummaries startDate=" + this.this$0.$startDate + ", endDate=" + this.this$0.$endDate + " saveCallResult onResponse: response = " + apiResponse);
            try {
                java.util.List<com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary> list = apiResponse.get_items();
                java.util.ArrayList arrayList = new java.util.ArrayList(com.fossil.blesdk.obfuscated.db4.m20831a(list, 10));
                for (com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary goalTrackingSummary : list) {
                    com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary goalTrackingSummary2 = goalTrackingSummary.toGoalTrackingSummary();
                    if (goalTrackingSummary2 != null) {
                        arrayList.add(goalTrackingSummary2);
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                }
                this.this$0.this$0.mGoalTrackingDao.upsertGoalTrackingSummaries(com.fossil.blesdk.obfuscated.kb4.m24381d(arrayList));
            } catch (java.lang.Exception e) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String tag2 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion.getTAG();
                local2.mo33256e(tag2, "getSummaries startDate=" + this.this$0.$startDate + ", endDate=" + this.this$0.$endDate + " exception=" + e + '}');
                e.printStackTrace();
            }
        }

        @DexIgnore
        public boolean shouldFetch(java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary> list) {
            return this.this$0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public GoalTrackingRepository$getSummaries$1(com.portfolio.platform.data.source.GoalTrackingRepository goalTrackingRepository, java.util.Date date, java.util.Date date2, boolean z) {
        this.this$0 = goalTrackingRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public final androidx.lifecycle.LiveData<com.fossil.blesdk.obfuscated.os3<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>>> apply(java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> list) {
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) list, "pendingList");
        return new com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$1.C56981(this, com.portfolio.platform.data.model.goaltracking.GoalTrackingDataKt.calculateRangeDownload(list, this.$startDate, this.$endDate)).asLiveData();
    }
}
