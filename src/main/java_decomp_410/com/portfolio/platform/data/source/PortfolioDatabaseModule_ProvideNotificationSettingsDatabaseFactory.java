package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory implements Factory<NotificationSettingsDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static NotificationSettingsDatabase provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return proxyProvideNotificationSettingsDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static NotificationSettingsDatabase proxyProvideNotificationSettingsDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        NotificationSettingsDatabase provideNotificationSettingsDatabase = portfolioDatabaseModule.provideNotificationSettingsDatabase(portfolioApp);
        n44.a(provideNotificationSettingsDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideNotificationSettingsDatabase;
    }

    @DexIgnore
    public NotificationSettingsDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
