package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.a72;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.c72;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.ld;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.fossil.blesdk.obfuscated.zf;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingDao_Impl extends GoalTrackingDao {
    @DexIgnore
    public /* final */ a72 __dateShortStringConverter; // = new a72();
    @DexIgnore
    public /* final */ c72 __dateTimeISOStringConverter; // = new c72();
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfGoalSetting;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfGoalTrackingData;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfGoalTrackingSummary;
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeleteAllGoalTrackingData;
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeleteAllGoalTrackingSummaries;
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeleteGoalSetting;
    @DexIgnore
    public /* final */ wf __preparedStmtOfRemoveDeletedGoalTrackingData;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<GoalSetting> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalSetting`(`id`,`currentTarget`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, GoalSetting goalSetting) {
            kgVar.b(1, (long) goalSetting.getId());
            kgVar.b(2, (long) goalSetting.getCurrentTarget());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<List<GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon10(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<GoalTrackingSummary> call() throws Exception {
            Cursor a = bg.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "pinType");
                int b2 = ag.b(a, "date");
                int b3 = ag.b(a, "totalTracked");
                int b4 = ag.b(a, "goalTarget");
                int b5 = ag.b(a, "createdAt");
                int b6 = ag.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b2)), a.getInt(b3), a.getInt(b4), a.getLong(b5), a.getLong(b6));
                    goalTrackingSummary.setPinType(a.getInt(b));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon11 implements Callable<GoalTrackingSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon11(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public GoalTrackingSummary call() throws Exception {
            GoalTrackingSummary goalTrackingSummary;
            Cursor a = bg.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "pinType");
                int b2 = ag.b(a, "date");
                int b3 = ag.b(a, "totalTracked");
                int b4 = ag.b(a, "goalTarget");
                int b5 = ag.b(a, "createdAt");
                int b6 = ag.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b2)), a.getInt(b3), a.getInt(b4), a.getLong(b5), a.getLong(b6));
                    goalTrackingSummary.setPinType(a.getInt(b));
                } else {
                    goalTrackingSummary = null;
                }
                return goalTrackingSummary;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon12 implements Callable<List<GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon12(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<GoalTrackingSummary> call() throws Exception {
            Cursor a = bg.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "pinType");
                int b2 = ag.b(a, "date");
                int b3 = ag.b(a, "totalTracked");
                int b4 = ag.b(a, "goalTarget");
                int b5 = ag.b(a, "createdAt");
                int b6 = ag.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b2)), a.getInt(b3), a.getInt(b4), a.getLong(b5), a.getLong(b6));
                    goalTrackingSummary.setPinType(a.getInt(b));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon13 implements Callable<List<GoalTrackingData>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon13(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<GoalTrackingData> call() throws Exception {
            Cursor a = bg.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "pinType");
                int b2 = ag.b(a, "id");
                int b3 = ag.b(a, GoalTrackingEvent.COLUMN_TRACKED_AT);
                int b4 = ag.b(a, "timezoneOffsetInSecond");
                int b5 = ag.b(a, "date");
                int b6 = ag.b(a, "createdAt");
                int b7 = ag.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    GoalTrackingData goalTrackingData = new GoalTrackingData(a.getString(b2), GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b3)), a.getInt(b4), GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b5)), a.getLong(b6), a.getLong(b7));
                    goalTrackingData.setPinType(a.getInt(b));
                    arrayList.add(goalTrackingData);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon14 implements Callable<List<GoalTrackingData>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon14(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<GoalTrackingData> call() throws Exception {
            Cursor a = bg.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "pinType");
                int b2 = ag.b(a, "id");
                int b3 = ag.b(a, GoalTrackingEvent.COLUMN_TRACKED_AT);
                int b4 = ag.b(a, "timezoneOffsetInSecond");
                int b5 = ag.b(a, "date");
                int b6 = ag.b(a, "createdAt");
                int b7 = ag.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    GoalTrackingData goalTrackingData = new GoalTrackingData(a.getString(b2), GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b3)), a.getInt(b4), GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b5)), a.getLong(b6), a.getLong(b7));
                    goalTrackingData.setPinType(a.getInt(b));
                    arrayList.add(goalTrackingData);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends lf<GoalTrackingSummary> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalTrackingDay`(`pinType`,`date`,`totalTracked`,`goalTarget`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, GoalTrackingSummary goalTrackingSummary) {
            kgVar.b(1, (long) goalTrackingSummary.getPinType());
            String a = GoalTrackingDao_Impl.this.__dateShortStringConverter.a(goalTrackingSummary.getDate());
            if (a == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, a);
            }
            kgVar.b(3, (long) goalTrackingSummary.getTotalTracked());
            kgVar.b(4, (long) goalTrackingSummary.getGoalTarget());
            kgVar.b(5, goalTrackingSummary.getCreatedAt());
            kgVar.b(6, goalTrackingSummary.getUpdatedAt());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends lf<GoalTrackingData> {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalTrackingRaw`(`pinType`,`id`,`trackedAt`,`timezoneOffsetInSecond`,`date`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, GoalTrackingData goalTrackingData) {
            kgVar.b(1, (long) goalTrackingData.getPinType());
            if (goalTrackingData.getId() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, goalTrackingData.getId());
            }
            String a = GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.a(goalTrackingData.getTrackedAt());
            if (a == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, a);
            }
            kgVar.b(4, (long) goalTrackingData.getTimezoneOffsetInSecond());
            String a2 = GoalTrackingDao_Impl.this.__dateShortStringConverter.a(goalTrackingData.getDate());
            if (a2 == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, a2);
            }
            kgVar.b(6, goalTrackingData.getCreatedAt());
            kgVar.b(7, goalTrackingData.getUpdatedAt());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends wf {
        @DexIgnore
        public Anon4(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM goalSetting";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends wf {
        @DexIgnore
        public Anon5(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM goalTrackingDay";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends wf {
        @DexIgnore
        public Anon6(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM goalTrackingRaw WHERE  id == ?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends wf {
        @DexIgnore
        public Anon7(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM goalTrackingRaw";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon8(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public Integer call() throws Exception {
            Cursor a = bg.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false);
            try {
                Integer num = null;
                if (a.moveToFirst()) {
                    if (!a.isNull(0)) {
                        num = Integer.valueOf(a.getInt(0));
                    }
                }
                return num;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 extends ld.b<Integer, GoalTrackingSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1 extends zf<GoalTrackingSummary> {
            @DexIgnore
            public Anon1(RoomDatabase roomDatabase, uf ufVar, boolean z, String... strArr) {
                super(roomDatabase, ufVar, z, strArr);
            }

            @DexIgnore
            public List<GoalTrackingSummary> convertRows(Cursor cursor) {
                Cursor cursor2 = cursor;
                int b = ag.b(cursor2, "pinType");
                int b2 = ag.b(cursor2, "date");
                int b3 = ag.b(cursor2, "totalTracked");
                int b4 = ag.b(cursor2, "goalTarget");
                int b5 = ag.b(cursor2, "createdAt");
                int b6 = ag.b(cursor2, "updatedAt");
                ArrayList arrayList = new ArrayList(cursor.getCount());
                while (cursor.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.a(cursor2.getString(b2)), cursor2.getInt(b3), cursor2.getInt(b4), cursor2.getLong(b5), cursor2.getLong(b6));
                    goalTrackingSummary.setPinType(cursor2.getInt(b));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            }
        }

        @DexIgnore
        public Anon9(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public zf<GoalTrackingSummary> create() {
            return new Anon1(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, "goalTrackingDay");
        }
    }

    @DexIgnore
    public GoalTrackingDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfGoalSetting = new Anon1(roomDatabase);
        this.__insertionAdapterOfGoalTrackingSummary = new Anon2(roomDatabase);
        this.__insertionAdapterOfGoalTrackingData = new Anon3(roomDatabase);
        this.__preparedStmtOfDeleteGoalSetting = new Anon4(roomDatabase);
        this.__preparedStmtOfDeleteAllGoalTrackingSummaries = new Anon5(roomDatabase);
        this.__preparedStmtOfRemoveDeletedGoalTrackingData = new Anon6(roomDatabase);
        this.__preparedStmtOfDeleteAllGoalTrackingData = new Anon7(roomDatabase);
    }

    @DexIgnore
    public void deleteAllGoalTrackingData() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeleteAllGoalTrackingData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllGoalTrackingData.release(acquire);
        }
    }

    @DexIgnore
    public void deleteAllGoalTrackingSummaries() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeleteAllGoalTrackingSummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllGoalTrackingSummaries.release(acquire);
        }
    }

    @DexIgnore
    public void deleteGoalSetting() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeleteGoalSetting.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteGoalSetting.release(acquire);
        }
    }

    @DexIgnore
    public List<GoalTrackingData> getGoalTrackingDataList(Date date, Date date2) {
        uf b = uf.b("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 3 ORDER BY trackedAt ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a3, "pinType");
            int b3 = ag.b(a3, "id");
            int b4 = ag.b(a3, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int b5 = ag.b(a3, "timezoneOffsetInSecond");
            int b6 = ag.b(a3, "date");
            int b7 = ag.b(a3, "createdAt");
            int b8 = ag.b(a3, "updatedAt");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a3.getString(b3), this.__dateTimeISOStringConverter.a(a3.getString(b4)), a3.getInt(b5), this.__dateShortStringConverter.a(a3.getString(b6)), a3.getLong(b7), a3.getLong(b8));
                goalTrackingData.setPinType(a3.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    public List<GoalTrackingData> getGoalTrackingDataListAfterInDate(Date date, DateTime dateTime, long j, int i) {
        uf b = uf.b("SELECT * FROM goalTrackingRaw WHERE date == ? AND updatedAt < ? AND trackedAt < ? AND pinType <> 3 ORDER BY trackedAt DESC limit ?", 4);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        b.b(2, j);
        String a2 = this.__dateTimeISOStringConverter.a(dateTime);
        if (a2 == null) {
            b.a(3);
        } else {
            b.a(3, a2);
        }
        b.b(4, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a3, "pinType");
            int b3 = ag.b(a3, "id");
            int b4 = ag.b(a3, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int b5 = ag.b(a3, "timezoneOffsetInSecond");
            int b6 = ag.b(a3, "date");
            int b7 = ag.b(a3, "createdAt");
            int b8 = ag.b(a3, "updatedAt");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a3.getString(b3), this.__dateTimeISOStringConverter.a(a3.getString(b4)), a3.getInt(b5), this.__dateShortStringConverter.a(a3.getString(b6)), a3.getLong(b7), a3.getLong(b8));
                goalTrackingData.setPinType(a3.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    public List<GoalTrackingData> getGoalTrackingDataListInitInDate(Date date, int i) {
        uf b = uf.b("SELECT * FROM goalTrackingRaw WHERE date == ? AND pinType <> 3 ORDER BY trackedAt DESC limit ?", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        b.b(2, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a2, "pinType");
            int b3 = ag.b(a2, "id");
            int b4 = ag.b(a2, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int b5 = ag.b(a2, "timezoneOffsetInSecond");
            int b6 = ag.b(a2, "date");
            int b7 = ag.b(a2, "createdAt");
            int b8 = ag.b(a2, "updatedAt");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a2.getString(b3), this.__dateTimeISOStringConverter.a(a2.getString(b4)), a2.getInt(b5), this.__dateShortStringConverter.a(a2.getString(b6)), a2.getLong(b7), a2.getLong(b8));
                goalTrackingData.setPinType(a2.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<GoalTrackingData>> getGoalTrackingDataListLiveData(Date date, Date date2) {
        uf b = uf.b("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 3 ORDER BY trackedAt ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingRaw"}, false, new Anon13(b));
    }

    @DexIgnore
    public List<GoalTrackingSummary> getGoalTrackingSummaries(Date date, Date date2) {
        uf b = uf.b("SELECT * FROM goalTrackingDay WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a3, "pinType");
            int b3 = ag.b(a3, "date");
            int b4 = ag.b(a3, "totalTracked");
            int b5 = ag.b(a3, "goalTarget");
            int b6 = ag.b(a3, "createdAt");
            int b7 = ag.b(a3, "updatedAt");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.a(a3.getString(b3)), a3.getInt(b4), a3.getInt(b5), a3.getLong(b6), a3.getLong(b7));
                goalTrackingSummary.setPinType(a3.getInt(b2));
                arrayList.add(goalTrackingSummary);
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<GoalTrackingSummary>> getGoalTrackingSummariesLiveData(Date date, Date date2) {
        uf b = uf.b("SELECT * FROM goalTrackingDay WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingDay"}, false, new Anon12(b));
    }

    @DexIgnore
    public GoalTrackingSummary getGoalTrackingSummary(Date date) {
        GoalTrackingSummary goalTrackingSummary;
        uf b = uf.b("SELECT * FROM goalTrackingDay WHERE date == ?", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a2, "pinType");
            int b3 = ag.b(a2, "date");
            int b4 = ag.b(a2, "totalTracked");
            int b5 = ag.b(a2, "goalTarget");
            int b6 = ag.b(a2, "createdAt");
            int b7 = ag.b(a2, "updatedAt");
            if (a2.moveToFirst()) {
                goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.a(a2.getString(b3)), a2.getInt(b4), a2.getInt(b5), a2.getLong(b6), a2.getLong(b7));
                goalTrackingSummary.setPinType(a2.getInt(b2));
            } else {
                goalTrackingSummary = null;
            }
            return goalTrackingSummary;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public List<GoalTrackingSummary> getGoalTrackingSummaryList() {
        uf b = uf.b("SELECT * FROM goalTrackingDay", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "date");
            int b4 = ag.b(a, "totalTracked");
            int b5 = ag.b(a, "goalTarget");
            int b6 = ag.b(a, "createdAt");
            int b7 = ag.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.a(a.getString(b3)), a.getInt(b4), a.getInt(b5), a.getLong(b6), a.getLong(b7));
                goalTrackingSummary.setPinType(a.getInt(b2));
                arrayList.add(goalTrackingSummary);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<GoalTrackingSummary>> getGoalTrackingSummaryListLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingDay"}, false, new Anon10(uf.b("SELECT * FROM goalTrackingDay", 0)));
    }

    @DexIgnore
    public LiveData<GoalTrackingSummary> getGoalTrackingSummaryLiveData(Date date) {
        uf b = uf.b("SELECT * FROM goalTrackingDay WHERE date == ?", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingDay"}, false, new Anon11(b));
    }

    @DexIgnore
    public GoalTrackingDao.TotalSummary getGoalTrackingValueAndTarget(Date date, Date date2) {
        GoalTrackingDao.TotalSummary totalSummary;
        uf b = uf.b("SELECT SUM(totalTracked) as total_values, SUM(goalTarget) as total_targets FROM goalTrackingDay\n        WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a3, "total_values");
            int b3 = ag.b(a3, "total_targets");
            if (a3.moveToFirst()) {
                totalSummary = new GoalTrackingDao.TotalSummary();
                totalSummary.setValues(a3.getInt(b2));
                totalSummary.setTargets(a3.getInt(b3));
            } else {
                totalSummary = null;
            }
            return totalSummary;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    public Integer getLastGoalSetting() {
        uf b = uf.b("SELECT currentTarget FROM goalSetting LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            Integer num = null;
            if (a.moveToFirst()) {
                if (!a.isNull(0)) {
                    num = Integer.valueOf(a.getInt(0));
                }
            }
            return num;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<Integer> getLastGoalSettingLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"goalSetting"}, false, new Anon8(uf.b("SELECT currentTarget FROM goalSetting LIMIT 1", 0)));
    }

    @DexIgnore
    public GoalTrackingSummary getNearestGoalTrackingFromDate(Date date) {
        GoalTrackingSummary goalTrackingSummary;
        uf b = uf.b("SELECT * FROM goalTrackingDay WHERE date <= ? ORDER BY date DESC LIMIT 1", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a2, "pinType");
            int b3 = ag.b(a2, "date");
            int b4 = ag.b(a2, "totalTracked");
            int b5 = ag.b(a2, "goalTarget");
            int b6 = ag.b(a2, "createdAt");
            int b7 = ag.b(a2, "updatedAt");
            if (a2.moveToFirst()) {
                goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.a(a2.getString(b3)), a2.getInt(b4), a2.getInt(b5), a2.getLong(b6), a2.getLong(b7));
                goalTrackingSummary.setPinType(a2.getInt(b2));
            } else {
                goalTrackingSummary = null;
            }
            return goalTrackingSummary;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public List<GoalTrackingData> getPendingGoalTrackingDataList(Date date, Date date2) {
        uf b = uf.b("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 0", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a3, "pinType");
            int b3 = ag.b(a3, "id");
            int b4 = ag.b(a3, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int b5 = ag.b(a3, "timezoneOffsetInSecond");
            int b6 = ag.b(a3, "date");
            int b7 = ag.b(a3, "createdAt");
            int b8 = ag.b(a3, "updatedAt");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a3.getString(b3), this.__dateTimeISOStringConverter.a(a3.getString(b4)), a3.getInt(b5), this.__dateShortStringConverter.a(a3.getString(b6)), a3.getLong(b7), a3.getLong(b8));
                goalTrackingData.setPinType(a3.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<GoalTrackingData>> getPendingGoalTrackingDataListLiveData(Date date, Date date2) {
        uf b = uf.b("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 0", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingRaw"}, false, new Anon14(b));
    }

    @DexIgnore
    public ld.b<Integer, GoalTrackingSummary> getSummariesDataSource() {
        return new Anon9(uf.b("SELECT * FROM goalTrackingDay ORDER BY date DESC", 0));
    }

    @DexIgnore
    public void removeDeletedGoalTrackingData(String str) {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfRemoveDeletedGoalTrackingData.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveDeletedGoalTrackingData.release(acquire);
        }
    }

    @DexIgnore
    public void upsertGoalSettings(GoalSetting goalSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalSetting.insert(goalSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertGoalTrackingData(GoalTrackingData goalTrackingData) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert(goalTrackingData);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertGoalTrackingDataList(List<GoalTrackingData> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertGoalTrackingSummaries(List<GoalTrackingSummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingSummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertGoalTrackingSummary(GoalTrackingSummary goalTrackingSummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingSummary.insert(goalTrackingSummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertListGoalTrackingData(List<GoalTrackingData> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<GoalTrackingData> getPendingGoalTrackingDataList() {
        uf b = uf.b("SELECT * FROM goalTrackingRaw WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "pinType");
            int b3 = ag.b(a, "id");
            int b4 = ag.b(a, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int b5 = ag.b(a, "timezoneOffsetInSecond");
            int b6 = ag.b(a, "date");
            int b7 = ag.b(a, "createdAt");
            int b8 = ag.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a.getString(b3), this.__dateTimeISOStringConverter.a(a.getString(b4)), a.getInt(b5), this.__dateShortStringConverter.a(a.getString(b6)), a.getLong(b7), a.getLong(b8));
                goalTrackingData.setPinType(a.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }
}
