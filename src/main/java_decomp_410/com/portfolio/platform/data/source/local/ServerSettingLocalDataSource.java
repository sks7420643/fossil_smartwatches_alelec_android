package com.portfolio.platform.data.source.local;

import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ServerSettingLocalDataSource implements ServerSettingDataSource {
    @DexIgnore
    public void addOrUpdateServerSetting(ServerSetting serverSetting) {
        dn2.p.a().l().addOrUpdateServerSetting(serverSetting);
    }

    @DexIgnore
    public void addOrUpdateServerSettingList(List<ServerSetting> list) {
        dn2.p.a().l().a(list);
    }

    @DexIgnore
    public void clearData() {
        dn2.p.a().l().a();
    }

    @DexIgnore
    public ServerSetting getServerSettingByKey(String str) {
        kd4.b(str, "key");
        return dn2.p.a().l().getServerSettingByKey(str);
    }

    @DexIgnore
    public void getServerSettingList(ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        kd4.b(onGetServerSettingList, Constants.CALLBACK);
    }
}
