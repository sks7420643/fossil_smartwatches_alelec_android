package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.dg;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.n72;
import com.fossil.blesdk.obfuscated.u72;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.portfolio.platform.data.model.room.microapp.DeclarationFile;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppDao_Impl implements MicroAppDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfDeclarationFile;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfMicroApp;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfMicroAppSetting;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfMicroAppVariant;
    @DexIgnore
    public /* final */ n72 __millisecondDateConverter; // = new n72();
    @DexIgnore
    public /* final */ wf __preparedStmtOfClearAllDeclarationFileTable;
    @DexIgnore
    public /* final */ wf __preparedStmtOfClearAllMicroAppGalleryTable;
    @DexIgnore
    public /* final */ wf __preparedStmtOfClearAllMicroAppSettingTable;
    @DexIgnore
    public /* final */ wf __preparedStmtOfClearAllMicroAppVariantTable;
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeleteMicroAppsBySerial;
    @DexIgnore
    public /* final */ u72 __stringArrayConverter; // = new u72();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<MicroApp> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microApp`(`id`,`name`,`nameKey`,`serialNumber`,`categories`,`description`,`descriptionKey`,`icon`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, MicroApp microApp) {
            if (microApp.getId() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, microApp.getId());
            }
            if (microApp.getName() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, microApp.getName());
            }
            if (microApp.getNameKey() == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, microApp.getNameKey());
            }
            if (microApp.getSerialNumber() == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, microApp.getSerialNumber());
            }
            String a = MicroAppDao_Impl.this.__stringArrayConverter.a(microApp.getCategories());
            if (a == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, a);
            }
            if (microApp.getDescription() == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, microApp.getDescription());
            }
            if (microApp.getDescriptionKey() == null) {
                kgVar.a(7);
            } else {
                kgVar.a(7, microApp.getDescriptionKey());
            }
            if (microApp.getIcon() == null) {
                kgVar.a(8);
            } else {
                kgVar.a(8, microApp.getIcon());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends lf<MicroAppVariant> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microAppVariant`(`id`,`appId`,`name`,`description`,`createdAt`,`updatedAt`,`majorNumber`,`minorNumber`,`serialNumber`) VALUES (?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, MicroAppVariant microAppVariant) {
            if (microAppVariant.getId() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, microAppVariant.getId());
            }
            if (microAppVariant.getAppId() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, microAppVariant.getAppId());
            }
            if (microAppVariant.getName() == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, microAppVariant.getName());
            }
            if (microAppVariant.getDescription() == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, microAppVariant.getDescription());
            }
            kgVar.b(5, MicroAppDao_Impl.this.__millisecondDateConverter.a(microAppVariant.getCreatedAt()));
            kgVar.b(6, MicroAppDao_Impl.this.__millisecondDateConverter.a(microAppVariant.getUpdatedAt()));
            kgVar.b(7, (long) microAppVariant.getMajorNumber());
            kgVar.b(8, (long) microAppVariant.getMinorNumber());
            if (microAppVariant.getSerialNumber() == null) {
                kgVar.a(9);
            } else {
                kgVar.a(9, microAppVariant.getSerialNumber());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends lf<MicroAppSetting> {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microAppSetting`(`id`,`appId`,`setting`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, MicroAppSetting microAppSetting) {
            if (microAppSetting.getId() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, microAppSetting.getId());
            }
            if (microAppSetting.getAppId() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, microAppSetting.getAppId());
            }
            if (microAppSetting.getSetting() == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, microAppSetting.getSetting());
            }
            if (microAppSetting.getCreatedAt() == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, microAppSetting.getCreatedAt());
            }
            if (microAppSetting.getUpdatedAt() == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, microAppSetting.getUpdatedAt());
            }
            kgVar.b(6, (long) microAppSetting.getPinType());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends lf<DeclarationFile> {
        @DexIgnore
        public Anon4(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `declarationFile`(`appId`,`serialNumber`,`variantName`,`id`,`description`,`content`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, DeclarationFile declarationFile) {
            String str = declarationFile.appId;
            if (str == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, str);
            }
            String str2 = declarationFile.serialNumber;
            if (str2 == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, str2);
            }
            String str3 = declarationFile.variantName;
            if (str3 == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, str3);
            }
            if (declarationFile.getId() == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, declarationFile.getId());
            }
            if (declarationFile.getDescription() == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, declarationFile.getDescription());
            }
            if (declarationFile.getContent() == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, declarationFile.getContent());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends wf {
        @DexIgnore
        public Anon5(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM microApp";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends wf {
        @DexIgnore
        public Anon6(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM microAppSetting";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends wf {
        @DexIgnore
        public Anon7(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM microAppVariant";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 extends wf {
        @DexIgnore
        public Anon8(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM declarationFile";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 extends wf {
        @DexIgnore
        public Anon9(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM microApp WHERE serialNumber = ?";
        }
    }

    @DexIgnore
    public MicroAppDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfMicroApp = new Anon1(roomDatabase);
        this.__insertionAdapterOfMicroAppVariant = new Anon2(roomDatabase);
        this.__insertionAdapterOfMicroAppSetting = new Anon3(roomDatabase);
        this.__insertionAdapterOfDeclarationFile = new Anon4(roomDatabase);
        this.__preparedStmtOfClearAllMicroAppGalleryTable = new Anon5(roomDatabase);
        this.__preparedStmtOfClearAllMicroAppSettingTable = new Anon6(roomDatabase);
        this.__preparedStmtOfClearAllMicroAppVariantTable = new Anon7(roomDatabase);
        this.__preparedStmtOfClearAllDeclarationFileTable = new Anon8(roomDatabase);
        this.__preparedStmtOfDeleteMicroAppsBySerial = new Anon9(roomDatabase);
    }

    @DexIgnore
    public void clearAllDeclarationFileTable() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfClearAllDeclarationFileTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllDeclarationFileTable.release(acquire);
        }
    }

    @DexIgnore
    public void clearAllMicroAppGalleryTable() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfClearAllMicroAppGalleryTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppGalleryTable.release(acquire);
        }
    }

    @DexIgnore
    public void clearAllMicroAppSettingTable() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfClearAllMicroAppSettingTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppSettingTable.release(acquire);
        }
    }

    @DexIgnore
    public void clearAllMicroAppVariantTable() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfClearAllMicroAppVariantTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppVariantTable.release(acquire);
        }
    }

    @DexIgnore
    public void deleteMicroAppsBySerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeleteMicroAppsBySerial.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteMicroAppsBySerial.release(acquire);
        }
    }

    @DexIgnore
    public List<DeclarationFile> getAllDeclarationFile() {
        uf b = uf.b("SELECT*FROM declarationFile", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "appId");
            int b3 = ag.b(a, "serialNumber");
            int b4 = ag.b(a, "variantName");
            int b5 = ag.b(a, "id");
            int b6 = ag.b(a, "description");
            int b7 = ag.b(a, "content");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                DeclarationFile declarationFile = new DeclarationFile(a.getString(b5), a.getString(b6), a.getString(b7));
                declarationFile.appId = a.getString(b2);
                declarationFile.serialNumber = a.getString(b3);
                declarationFile.variantName = a.getString(b4);
                arrayList.add(declarationFile);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroAppVariant> getAllMicroAppVariant(String str, int i) {
        String str2 = str;
        uf b = uf.b("SELECT * FROM microAppVariant WHERE serialNumber = ? AND majorNumber = ?", 2);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        b.b(2, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "appId");
            int b4 = ag.b(a, "name");
            int b5 = ag.b(a, "description");
            int b6 = ag.b(a, "createdAt");
            int b7 = ag.b(a, "updatedAt");
            int b8 = ag.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = ag.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = ag.b(a, "serialNumber");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                int i2 = b2;
                arrayList.add(new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10)));
                b2 = i2;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroAppVariant> getAllMicroAppVariantUniqueBySerial() {
        uf b = uf.b("SELECT * FROM microAppVariant GROUP BY serialNumber", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "appId");
            int b4 = ag.b(a, "name");
            int b5 = ag.b(a, "description");
            int b6 = ag.b(a, "createdAt");
            int b7 = ag.b(a, "updatedAt");
            int b8 = ag.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = ag.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = ag.b(a, "serialNumber");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                int i = b2;
                arrayList.add(new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10)));
                b2 = i;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroAppVariant> getAllMicroAppVariants() {
        uf b = uf.b("SELECT*FROM microAppVariant", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "appId");
            int b4 = ag.b(a, "name");
            int b5 = ag.b(a, "description");
            int b6 = ag.b(a, "createdAt");
            int b7 = ag.b(a, "updatedAt");
            int b8 = ag.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = ag.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = ag.b(a, "serialNumber");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                int i = b2;
                arrayList.add(new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10)));
                b2 = i;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroApp> getAllMicroApps() {
        uf b = uf.b("SELECT*FROM microApp", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "name");
            int b4 = ag.b(a, "nameKey");
            int b5 = ag.b(a, "serialNumber");
            int b6 = ag.b(a, "categories");
            int b7 = ag.b(a, "description");
            int b8 = ag.b(a, "descriptionKey");
            int b9 = ag.b(a, "icon");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<DeclarationFile> getDeclarationFiles(String str, String str2, String str3) {
        uf b = uf.b("SELECT * FROM declarationFile WHERE appId = ? AND serialNumber = ? AND variantName = ? ORDER BY rowid ASC", 3);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        if (str2 == null) {
            b.a(2);
        } else {
            b.a(2, str2);
        }
        if (str3 == null) {
            b.a(3);
        } else {
            b.a(3, str3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "appId");
            int b3 = ag.b(a, "serialNumber");
            int b4 = ag.b(a, "variantName");
            int b5 = ag.b(a, "id");
            int b6 = ag.b(a, "description");
            int b7 = ag.b(a, "content");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                DeclarationFile declarationFile = new DeclarationFile(a.getString(b5), a.getString(b6), a.getString(b7));
                declarationFile.appId = a.getString(b2);
                declarationFile.serialNumber = a.getString(b3);
                declarationFile.variantName = a.getString(b4);
                arrayList.add(declarationFile);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroApp> getListMicroApp(String str) {
        String str2 = str;
        uf b = uf.b("SELECT * FROM microApp WHERE serialNumber = ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "name");
            int b4 = ag.b(a, "nameKey");
            int b5 = ag.b(a, "serialNumber");
            int b6 = ag.b(a, "categories");
            int b7 = ag.b(a, "description");
            int b8 = ag.b(a, "descriptionKey");
            int b9 = ag.b(a, "icon");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroAppSetting> getListMicroAppSetting() {
        uf b = uf.b("SELECT * FROM microAppSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "appId");
            int b4 = ag.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting.SETTING);
            int b5 = ag.b(a, "createdAt");
            int b6 = ag.b(a, "updatedAt");
            int b7 = ag.b(a, "pinType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroAppSetting(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public MicroApp getMicroApp(String str, String str2) {
        MicroApp microApp;
        String str3 = str;
        String str4 = str2;
        uf b = uf.b("SELECT * FROM microApp WHERE serialNumber = ? AND id = ?", 2);
        if (str3 == null) {
            b.a(1);
        } else {
            b.a(1, str3);
        }
        if (str4 == null) {
            b.a(2);
        } else {
            b.a(2, str4);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "name");
            int b4 = ag.b(a, "nameKey");
            int b5 = ag.b(a, "serialNumber");
            int b6 = ag.b(a, "categories");
            int b7 = ag.b(a, "description");
            int b8 = ag.b(a, "descriptionKey");
            int b9 = ag.b(a, "icon");
            if (a.moveToFirst()) {
                microApp = new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9));
            } else {
                microApp = null;
            }
            return microApp;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public MicroApp getMicroAppById(String str, String str2) {
        MicroApp microApp;
        String str3 = str;
        String str4 = str2;
        uf b = uf.b("SELECT * FROM microApp WHERE id=? AND serialNumber=?", 2);
        if (str3 == null) {
            b.a(1);
        } else {
            b.a(1, str3);
        }
        if (str4 == null) {
            b.a(2);
        } else {
            b.a(2, str4);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "name");
            int b4 = ag.b(a, "nameKey");
            int b5 = ag.b(a, "serialNumber");
            int b6 = ag.b(a, "categories");
            int b7 = ag.b(a, "description");
            int b8 = ag.b(a, "descriptionKey");
            int b9 = ag.b(a, "icon");
            if (a.moveToFirst()) {
                microApp = new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9));
            } else {
                microApp = null;
            }
            return microApp;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroApp> getMicroAppByIds(List<String> list, String str) {
        String str2 = str;
        StringBuilder a = dg.a();
        a.append("SELECT * FROM microApp WHERE id IN (");
        int size = list.size();
        dg.a(a, size);
        a.append(") AND serialNumber=");
        a.append("?");
        int i = 1;
        int i2 = size + 1;
        uf b = uf.b(a.toString(), i2);
        for (String next : list) {
            if (next == null) {
                b.a(i);
            } else {
                b.a(i, next);
            }
            i++;
        }
        if (str2 == null) {
            b.a(i2);
        } else {
            b.a(i2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a2, "id");
            int b3 = ag.b(a2, "name");
            int b4 = ag.b(a2, "nameKey");
            int b5 = ag.b(a2, "serialNumber");
            int b6 = ag.b(a2, "categories");
            int b7 = ag.b(a2, "description");
            int b8 = ag.b(a2, "descriptionKey");
            int b9 = ag.b(a2, "icon");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(new MicroApp(a2.getString(b2), a2.getString(b3), a2.getString(b4), a2.getString(b5), this.__stringArrayConverter.a(a2.getString(b6)), a2.getString(b7), a2.getString(b8), a2.getString(b9)));
            }
            return arrayList;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public MicroAppSetting getMicroAppSetting(String str) {
        String str2 = str;
        uf b = uf.b("SELECT * FROM microAppSetting WHERE appId= ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            return a.moveToFirst() ? new MicroAppSetting(a.getString(ag.b(a, "id")), a.getString(ag.b(a, "appId")), a.getString(ag.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting.SETTING)), a.getString(ag.b(a, "createdAt")), a.getString(ag.b(a, "updatedAt")), a.getInt(ag.b(a, "pinType"))) : null;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public MicroAppVariant getMicroAppVariant(String str, String str2, int i, String str3) {
        MicroAppVariant microAppVariant;
        String str4 = str;
        String str5 = str2;
        String str6 = str3;
        uf b = uf.b("SELECT * FROM microAppVariant WHERE appId = ? AND serialNumber = ? AND majorNumber = ? AND name = ?", 4);
        if (str4 == null) {
            b.a(1);
        } else {
            b.a(1, str4);
        }
        if (str5 == null) {
            b.a(2);
        } else {
            b.a(2, str5);
        }
        b.b(3, (long) i);
        if (str6 == null) {
            b.a(4);
        } else {
            b.a(4, str6);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "appId");
            int b4 = ag.b(a, "name");
            int b5 = ag.b(a, "description");
            int b6 = ag.b(a, "createdAt");
            int b7 = ag.b(a, "updatedAt");
            int b8 = ag.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = ag.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = ag.b(a, "serialNumber");
            if (a.moveToFirst()) {
                microAppVariant = new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10));
            } else {
                microAppVariant = null;
            }
            return microAppVariant;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroAppSetting> getPendingMicroAppSettings() {
        uf b = uf.b("SELECT * FROM microAppSetting WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "appId");
            int b4 = ag.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting.SETTING);
            int b5 = ag.b(a, "createdAt");
            int b6 = ag.b(a, "updatedAt");
            int b7 = ag.b(a, "pinType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroAppSetting(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<MicroApp> queryMicroAppByName(String str, String str2) {
        String str3 = str;
        String str4 = str2;
        uf b = uf.b("SELECT * FROM microApp WHERE name LIKE '%' || ? || '%' AND serialNumber=?", 2);
        if (str3 == null) {
            b.a(1);
        } else {
            b.a(1, str3);
        }
        if (str4 == null) {
            b.a(2);
        } else {
            b.a(2, str4);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "name");
            int b4 = ag.b(a, "nameKey");
            int b5 = ag.b(a, "serialNumber");
            int b6 = ag.b(a, "categories");
            int b7 = ag.b(a, "description");
            int b8 = ag.b(a, "descriptionKey");
            int b9 = ag.b(a, "icon");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertDeclarationFileList(List<DeclarationFile> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDeclarationFile.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertListMicroApp(List<MicroApp> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroApp.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertMicroApp(MicroApp microApp) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroApp.insert(microApp);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertMicroAppSetting(MicroAppSetting microAppSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppSetting.insert(microAppSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertMicroAppSettingList(List<MicroAppSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppSetting.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertMicroAppVariant(MicroAppVariant microAppVariant) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppVariant.insert(microAppVariant);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertMicroAppVariantList(List<MicroAppVariant> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppVariant.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public MicroAppVariant getMicroAppVariant(String str, String str2, int i) {
        MicroAppVariant microAppVariant;
        String str3 = str;
        String str4 = str2;
        uf b = uf.b("SELECT * FROM microAppVariant WHERE appId = ? AND serialNumber = ? AND majorNumber = ?", 3);
        if (str3 == null) {
            b.a(1);
        } else {
            b.a(1, str3);
        }
        if (str4 == null) {
            b.a(2);
        } else {
            b.a(2, str4);
        }
        b.b(3, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "appId");
            int b4 = ag.b(a, "name");
            int b5 = ag.b(a, "description");
            int b6 = ag.b(a, "createdAt");
            int b7 = ag.b(a, "updatedAt");
            int b8 = ag.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = ag.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = ag.b(a, "serialNumber");
            if (a.moveToFirst()) {
                microAppVariant = new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10));
            } else {
                microAppVariant = null;
            }
            return microAppVariant;
        } finally {
            a.close();
            b.c();
        }
    }
}
