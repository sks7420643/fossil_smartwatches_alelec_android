package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingRepository$getGoalTrackingDataPaging$3 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getGoalTrackingDataPaging$3(com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataSourceFactory goalTrackingDataSourceFactory) {
        super(0);
        this.$sourceFactory = goalTrackingDataSourceFactory;
    }

    @DexIgnore
    public final void invoke() {
        com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource a = this.$sourceFactory.getSourceLiveData().mo2275a();
        if (a != null) {
            com.portfolio.platform.helper.PagingRequestHelper mHelper = a.getMHelper();
            if (mHelper != null) {
                mHelper.mo39586b();
            }
        }
    }
}
