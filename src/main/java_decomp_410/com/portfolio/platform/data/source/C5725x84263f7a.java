package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1 */
public final class C5725x84263f7a<R extends com.fossil.blesdk.obfuscated.me0> implements com.fossil.blesdk.obfuscated.ne0<com.google.android.gms.common.api.Status> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.dg4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $count;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $countSizeOfLists$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $gFitSampleList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.ge0 $googleApiClient$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $sampleList;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfDataSetList;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfListsOfGFitSampleList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1$1")
    /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1$1 */
    public static final class C57261 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21098p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.C5725x84263f7a this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1$1$1")
        /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1$1$1 */
        public static final class C57271 implements java.lang.Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.data.source.C5725x84263f7a.C57261 this$0;

            @DexIgnore
            public C57271(com.portfolio.platform.data.source.C5725x84263f7a.C57261 r1) {
                this.this$0 = r1;
            }

            @DexIgnore
            public final void run() {
                this.this$0.this$0.this$0.getMThirdPartyDatabase().getGFitSampleDao().deleteListGFitSample(this.this$0.this$0.$sampleList);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C57261(com.portfolio.platform.data.source.C5725x84263f7a thirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = thirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.data.source.C5725x84263f7a.C57261 r0 = new com.portfolio.platform.data.source.C5725x84263f7a.C57261(this.this$0, yb4);
            r0.f21098p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.data.source.C5725x84263f7a.C57261) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.this$0.getMThirdPartyDatabase().runInTransaction((java.lang.Runnable) new com.portfolio.platform.data.source.C5725x84263f7a.C57261.C57271(this));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public C5725x84263f7a(kotlin.jvm.internal.Ref$IntRef ref$IntRef, int i, java.util.List list, com.fossil.blesdk.obfuscated.ge0 ge0, kotlin.jvm.internal.Ref$IntRef ref$IntRef2, com.fossil.blesdk.obfuscated.dg4 dg4, int i2, com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository, java.util.List list2, java.lang.String str) {
        this.$count = ref$IntRef;
        this.$sizeOfDataSetList = i;
        this.$sampleList = list;
        this.$googleApiClient$inlined = ge0;
        this.$countSizeOfLists$inlined = ref$IntRef2;
        this.$continuation$inlined = dg4;
        this.$sizeOfListsOfGFitSampleList$inlined = i2;
        this.this$0 = thirdPartyRepository;
        this.$gFitSampleList$inlined = list2;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    public final void onResult(com.google.android.gms.common.api.Status status) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(status, "status");
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("Sending GFitSample: Status = ");
        status.mo10091G();
        sb.append(status);
        sb.append(" - Status Message = ");
        sb.append(status.mo19651J());
        local.mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, sb.toString());
        if (status.mo19653L()) {
            kotlin.jvm.internal.Ref$IntRef ref$IntRef = this.$count;
            ref$IntRef.element++;
            if (ref$IntRef.element >= this.$sizeOfDataSetList) {
                com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.C5725x84263f7a.C57261(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            }
        }
        kotlin.jvm.internal.Ref$IntRef ref$IntRef2 = this.$countSizeOfLists$inlined;
        ref$IntRef2.element++;
        if (ref$IntRef2.element >= this.$sizeOfListsOfGFitSampleList$inlined * this.$sizeOfDataSetList && this.$continuation$inlined.isActive()) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "End saveGFitSampleToGoogleFit");
            com.fossil.blesdk.obfuscated.dg4 dg4 = this.$continuation$inlined;
            kotlin.Result.C7350a aVar = kotlin.Result.Companion;
            dg4.resumeWith(kotlin.Result.m37419constructorimpl((java.lang.Object) null));
        }
    }
}
