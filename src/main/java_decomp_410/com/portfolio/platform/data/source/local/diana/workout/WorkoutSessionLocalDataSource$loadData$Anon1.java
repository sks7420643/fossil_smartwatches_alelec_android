package com.portfolio.platform.data.source.local.diana.workout;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$Anon1", f = "WorkoutSessionLocalDataSource.kt", l = {82, 85, 90}, m = "invokeSuspend")
public final class WorkoutSessionLocalDataSource$loadData$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionLocalDataSource this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$Anon1$Anon1", f = "WorkoutSessionLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionLocalDataSource$loadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WorkoutSessionLocalDataSource$loadData$Anon1 workoutSessionLocalDataSource$loadData$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = workoutSessionLocalDataSource$loadData$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                this.this$Anon0.$helperCallback.a();
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$Anon1$Anon2", f = "WorkoutSessionLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements yc4<Object, Object, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ qo2 $data;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionLocalDataSource$loadData$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(WorkoutSessionLocalDataSource$loadData$Anon1 workoutSessionLocalDataSource$loadData$Anon1, qo2 qo2, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = workoutSessionLocalDataSource$loadData$Anon1;
            this.$data = qo2;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, this.$data, yb4);
            anon2.p$ = (zg4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                if (((po2) this.$data).d() != null) {
                    this.this$Anon0.$helperCallback.a(((po2) this.$data).d());
                } else if (((po2) this.$data).c() != null) {
                    ServerError c = ((po2) this.$data).c();
                    PagingRequestHelper.b.a aVar = this.this$Anon0.$helperCallback;
                    String userMessage = c.getUserMessage();
                    if (userMessage == null) {
                        userMessage = c.getMessage();
                    }
                    if (userMessage == null) {
                        userMessage = "";
                    }
                    aVar.a(new Throwable(userMessage));
                }
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionLocalDataSource$loadData$Anon1(WorkoutSessionLocalDataSource workoutSessionLocalDataSource, int i, PagingRequestHelper.b.a aVar, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = workoutSessionLocalDataSource;
        this.$offset = i;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        WorkoutSessionLocalDataSource$loadData$Anon1 workoutSessionLocalDataSource$loadData$Anon1 = new WorkoutSessionLocalDataSource$loadData$Anon1(this.this$Anon0, this.$offset, this.$helperCallback, yb4);
        workoutSessionLocalDataSource$loadData$Anon1.p$ = (zg4) obj;
        return workoutSessionLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((WorkoutSessionLocalDataSource$loadData$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        List<FitnessDataWrapper> list;
        zg4 zg4;
        Object obj2;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg42 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = WorkoutSessionLocalDataSource.Companion.getTAG();
            local.d(tag, "loadData currentDate = " + this.this$Anon0.currentDate + ", offset=" + this.$offset);
            list = this.this$Anon0.fitnessDataDao.getFitnessData(this.this$Anon0.currentDate, this.this$Anon0.currentDate);
            if (list.isEmpty()) {
                WorkoutSessionRepository access$getWorkoutSessionRepository$p = this.this$Anon0.workoutSessionRepository;
                Date access$getCurrentDate$p = this.this$Anon0.currentDate;
                Date access$getCurrentDate$p2 = this.this$Anon0.currentDate;
                int i2 = this.$offset;
                this.L$Anon0 = zg42;
                this.L$Anon1 = list;
                this.label = 1;
                obj2 = WorkoutSessionRepository.fetchWorkoutSessions$default(access$getWorkoutSessionRepository$p, access$getCurrentDate$p, access$getCurrentDate$p2, i2, 0, this, 8, (Object) null);
                if (obj2 == a) {
                    return a;
                }
                zg4 = zg42;
            } else {
                this.$helperCallback.a();
                return qa4.a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            list = (List) this.L$Anon1;
            obj2 = obj;
        } else if (i == 2 || i == 3) {
            qo2 qo2 = (qo2) this.L$Anon2;
            List list2 = (List) this.L$Anon1;
            zg4 zg43 = (zg4) this.L$Anon0;
            na4.a(obj);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        qo2 qo22 = (qo2) obj2;
        if (qo22 instanceof ro2) {
            WorkoutSessionLocalDataSource workoutSessionLocalDataSource = this.this$Anon0;
            workoutSessionLocalDataSource.mOffset = workoutSessionLocalDataSource.mOffset + 100;
            pi4 c = nh4.c();
            Anon1 anon1 = new Anon1(this, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = list;
            this.L$Anon2 = qo22;
            this.label = 2;
            if (yf4.a(c, anon1, this) == a) {
                return a;
            }
        } else if (qo22 instanceof po2) {
            pi4 c2 = nh4.c();
            Anon2 anon2 = new Anon2(this, qo22, (yb4) null);
            this.L$Anon0 = zg4;
            this.L$Anon1 = list;
            this.L$Anon2 = qo22;
            this.label = 3;
            if (yf4.a(c2, anon2, this) == a) {
                return a;
            }
        }
        return qa4.a;
    }
}
