package com.portfolio.platform.data.source.local.fitness;

import androidx.room.RoomDatabase;
import com.facebook.places.PlaceManager;
import com.facebook.places.model.PlaceFields;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import com.fossil.wearables.fsl.fitness.ActivitySettings;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.FitnessDataDao_Impl;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao_Impl;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao_Impl;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao_Impl;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FitnessDatabase_Impl extends FitnessDatabase {
    @DexIgnore
    public volatile ActivitySampleDao _activitySampleDao;
    @DexIgnore
    public volatile ActivitySummaryDao _activitySummaryDao;
    @DexIgnore
    public volatile FitnessDataDao _fitnessDataDao;
    @DexIgnore
    public volatile HeartRateDailySummaryDao _heartRateDailySummaryDao;
    @DexIgnore
    public volatile HeartRateSampleDao _heartRateSampleDao;
    @DexIgnore
    public volatile SampleRawDao _sampleRawDao;
    @DexIgnore
    public volatile WorkoutDao _workoutDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `sampleraw` (`id` TEXT NOT NULL, `pinType` INTEGER NOT NULL, `uaPinType` INTEGER NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `sourceId` TEXT NOT NULL, `sourceTypeValue` TEXT NOT NULL, `movementTypeValue` TEXT, `steps` REAL NOT NULL, `calories` REAL NOT NULL, `distance` REAL NOT NULL, `activeTime` INTEGER NOT NULL, `intensityDistInSteps` TEXT NOT NULL, `timeZoneID` TEXT, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `activity_sample` (`id` TEXT NOT NULL, `uid` TEXT NOT NULL, `date` TEXT NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `steps` REAL NOT NULL, `calories` REAL NOT NULL, `distance` REAL NOT NULL, `activeTime` INTEGER NOT NULL, `intensityDistInSteps` TEXT NOT NULL, `timeZoneOffsetInSecond` INTEGER NOT NULL, `sourceId` TEXT NOT NULL, `syncTime` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `sampleday` (`createdAt` INTEGER, `updatedAt` INTEGER, `pinType` INTEGER NOT NULL, `year` INTEGER NOT NULL, `month` INTEGER NOT NULL, `day` INTEGER NOT NULL, `timezoneName` TEXT, `dstOffset` INTEGER, `steps` REAL NOT NULL, `calories` REAL NOT NULL, `distance` REAL NOT NULL, `intensities` TEXT NOT NULL, `activeTime` INTEGER NOT NULL, `stepGoal` INTEGER NOT NULL, `caloriesGoal` INTEGER NOT NULL, `activeTimeGoal` INTEGER NOT NULL, PRIMARY KEY(`year`, `month`, `day`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `activitySettings` (`id` INTEGER NOT NULL, `currentStepGoal` INTEGER NOT NULL, `currentCaloriesGoal` INTEGER NOT NULL, `currentActiveTimeGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `activityRecommendedGoals` (`id` INTEGER NOT NULL, `recommendedStepsGoal` INTEGER NOT NULL, `recommendedCaloriesGoal` INTEGER NOT NULL, `recommendedActiveTimeGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `workout_session` (`speed` TEXT, `location` TEXT, `states` TEXT NOT NULL, `id` TEXT NOT NULL, `date` TEXT NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `deviceSerialNumber` TEXT, `step` TEXT, `calorie` TEXT, `distance` TEXT, `heartRate` TEXT, `sourceType` TEXT, `workoutType` TEXT, `timezoneOffset` INTEGER NOT NULL, `duration` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `fitness_data` (`step` TEXT NOT NULL, `activeMinute` TEXT NOT NULL, `calorie` TEXT NOT NULL, `distance` TEXT NOT NULL, `stress` TEXT, `resting` TEXT NOT NULL, `heartRate` TEXT, `sleeps` TEXT NOT NULL, `workouts` TEXT NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `syncTime` TEXT NOT NULL, `timezoneOffsetInSecond` INTEGER NOT NULL, `serialNumber` TEXT NOT NULL, PRIMARY KEY(`startTime`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `heart_rate_sample` (`id` TEXT NOT NULL, `average` REAL NOT NULL, `date` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `endTime` TEXT NOT NULL, `startTime` TEXT NOT NULL, `timezoneOffset` INTEGER NOT NULL, `min` INTEGER NOT NULL, `max` INTEGER NOT NULL, `minuteCount` INTEGER NOT NULL, `resting` TEXT, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `daily_heart_rate_summary` (`average` REAL NOT NULL, `date` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `min` INTEGER NOT NULL, `max` INTEGER NOT NULL, `minuteCount` INTEGER NOT NULL, `resting` TEXT, PRIMARY KEY(`date`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS `activity_statistic` (`id` TEXT NOT NULL, `uid` TEXT NOT NULL, `activeTimeBestDay` TEXT, `activeTimeBestStreak` TEXT, `caloriesBestDay` TEXT, `caloriesBestStreak` TEXT, `stepsBestDay` TEXT, `stepsBestStreak` TEXT, `totalActiveTime` INTEGER NOT NULL, `totalCalories` REAL NOT NULL, `totalDays` INTEGER NOT NULL, `totalDistance` REAL NOT NULL, `totalSteps` INTEGER NOT NULL, `totalIntensityDistInStep` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '9c43c28ab51a9a7a22d2899f05515d5a')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `sampleraw`");
            ggVar.b("DROP TABLE IF EXISTS `activity_sample`");
            ggVar.b("DROP TABLE IF EXISTS `sampleday`");
            ggVar.b("DROP TABLE IF EXISTS `activitySettings`");
            ggVar.b("DROP TABLE IF EXISTS `activityRecommendedGoals`");
            ggVar.b("DROP TABLE IF EXISTS `workout_session`");
            ggVar.b("DROP TABLE IF EXISTS `fitness_data`");
            ggVar.b("DROP TABLE IF EXISTS `heart_rate_sample`");
            ggVar.b("DROP TABLE IF EXISTS `daily_heart_rate_summary`");
            ggVar.b("DROP TABLE IF EXISTS `activity_statistic`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (FitnessDatabase_Impl.this.mCallbacks != null) {
                int size = FitnessDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) FitnessDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = FitnessDatabase_Impl.this.mDatabase = ggVar;
            FitnessDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (FitnessDatabase_Impl.this.mCallbacks != null) {
                int size = FitnessDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) FitnessDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            gg ggVar2 = ggVar;
            HashMap hashMap = new HashMap(14);
            hashMap.put("id", new eg.a("id", "TEXT", true, 1));
            hashMap.put("pinType", new eg.a("pinType", "INTEGER", true, 0));
            hashMap.put(SampleRaw.COLUMN_UA_PIN_TYPE, new eg.a(SampleRaw.COLUMN_UA_PIN_TYPE, "INTEGER", true, 0));
            hashMap.put(SampleRaw.COLUMN_START_TIME, new eg.a(SampleRaw.COLUMN_START_TIME, "TEXT", true, 0));
            hashMap.put(SampleRaw.COLUMN_END_TIME, new eg.a(SampleRaw.COLUMN_END_TIME, "TEXT", true, 0));
            hashMap.put(SampleRaw.COLUMN_SOURCE_ID, new eg.a(SampleRaw.COLUMN_SOURCE_ID, "TEXT", true, 0));
            hashMap.put(SampleRaw.COLUMN_SOURCE_TYPE_VALUE, new eg.a(SampleRaw.COLUMN_SOURCE_TYPE_VALUE, "TEXT", true, 0));
            hashMap.put(SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE, new eg.a(SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE, "TEXT", false, 0));
            hashMap.put("steps", new eg.a("steps", "REAL", true, 0));
            hashMap.put("calories", new eg.a("calories", "REAL", true, 0));
            hashMap.put("distance", new eg.a("distance", "REAL", true, 0));
            hashMap.put(SampleDay.COLUMN_ACTIVE_TIME, new eg.a(SampleDay.COLUMN_ACTIVE_TIME, "INTEGER", true, 0));
            hashMap.put("intensityDistInSteps", new eg.a("intensityDistInSteps", "TEXT", true, 0));
            hashMap.put(SampleRaw.COLUMN_TIMEZONE_ID, new eg.a(SampleRaw.COLUMN_TIMEZONE_ID, "TEXT", false, 0));
            eg egVar = new eg(SampleRaw.TABLE_NAME, hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar2, SampleRaw.TABLE_NAME);
            if (egVar.equals(a)) {
                HashMap hashMap2 = new HashMap(15);
                hashMap2.put("id", new eg.a("id", "TEXT", true, 1));
                hashMap2.put("uid", new eg.a("uid", "TEXT", true, 0));
                hashMap2.put("date", new eg.a("date", "TEXT", true, 0));
                hashMap2.put(SampleRaw.COLUMN_START_TIME, new eg.a(SampleRaw.COLUMN_START_TIME, "TEXT", true, 0));
                hashMap2.put(SampleRaw.COLUMN_END_TIME, new eg.a(SampleRaw.COLUMN_END_TIME, "TEXT", true, 0));
                hashMap2.put("steps", new eg.a("steps", "REAL", true, 0));
                hashMap2.put("calories", new eg.a("calories", "REAL", true, 0));
                hashMap2.put("distance", new eg.a("distance", "REAL", true, 0));
                String str = "\n Found:\n";
                hashMap2.put(SampleDay.COLUMN_ACTIVE_TIME, new eg.a(SampleDay.COLUMN_ACTIVE_TIME, "INTEGER", true, 0));
                hashMap2.put("intensityDistInSteps", new eg.a("intensityDistInSteps", "TEXT", true, 0));
                hashMap2.put("timeZoneOffsetInSecond", new eg.a("timeZoneOffsetInSecond", "INTEGER", true, 0));
                hashMap2.put(SampleRaw.COLUMN_SOURCE_ID, new eg.a(SampleRaw.COLUMN_SOURCE_ID, "TEXT", true, 0));
                hashMap2.put("syncTime", new eg.a("syncTime", "INTEGER", true, 0));
                hashMap2.put("createdAt", new eg.a("createdAt", "INTEGER", true, 0));
                String str2 = SampleRaw.COLUMN_END_TIME;
                hashMap2.put("updatedAt", new eg.a("updatedAt", "INTEGER", true, 0));
                HashSet hashSet = new HashSet(0);
                HashSet hashSet2 = new HashSet(0);
                String str3 = SampleRaw.COLUMN_START_TIME;
                eg egVar2 = new eg(ActivitySample.TABLE_NAME, hashMap2, hashSet, hashSet2);
                eg a2 = eg.a(ggVar2, ActivitySample.TABLE_NAME);
                if (egVar2.equals(a2)) {
                    HashMap hashMap3 = new HashMap(16);
                    hashMap3.put("createdAt", new eg.a("createdAt", "INTEGER", false, 0));
                    hashMap3.put("updatedAt", new eg.a("updatedAt", "INTEGER", false, 0));
                    hashMap3.put("pinType", new eg.a("pinType", "INTEGER", true, 0));
                    hashMap3.put("year", new eg.a("year", "INTEGER", true, 1));
                    hashMap3.put("month", new eg.a("month", "INTEGER", true, 2));
                    hashMap3.put("day", new eg.a("day", "INTEGER", true, 3));
                    hashMap3.put(SampleDay.COLUMN_TIMEZONE_NAME, new eg.a(SampleDay.COLUMN_TIMEZONE_NAME, "TEXT", false, 0));
                    hashMap3.put(SampleDay.COLUMN_DST_OFFSET, new eg.a(SampleDay.COLUMN_DST_OFFSET, "INTEGER", false, 0));
                    hashMap3.put("steps", new eg.a("steps", "REAL", true, 0));
                    hashMap3.put("calories", new eg.a("calories", "REAL", true, 0));
                    hashMap3.put("distance", new eg.a("distance", "REAL", true, 0));
                    hashMap3.put(SampleDay.COLUMN_INTENSITIES, new eg.a(SampleDay.COLUMN_INTENSITIES, "TEXT", true, 0));
                    hashMap3.put(SampleDay.COLUMN_ACTIVE_TIME, new eg.a(SampleDay.COLUMN_ACTIVE_TIME, "INTEGER", true, 0));
                    hashMap3.put(SampleDay.COLUMN_STEP_GOAL, new eg.a(SampleDay.COLUMN_STEP_GOAL, "INTEGER", true, 0));
                    hashMap3.put(SampleDay.COLUMN_CALORIES_GOAL, new eg.a(SampleDay.COLUMN_CALORIES_GOAL, "INTEGER", true, 0));
                    hashMap3.put(SampleDay.COLUMN_ACTIVE_TIME_GOAL, new eg.a(SampleDay.COLUMN_ACTIVE_TIME_GOAL, "INTEGER", true, 0));
                    eg egVar3 = new eg(SampleDay.TABLE_NAME, hashMap3, new HashSet(0), new HashSet(0));
                    eg a3 = eg.a(ggVar2, SampleDay.TABLE_NAME);
                    if (egVar3.equals(a3)) {
                        HashMap hashMap4 = new HashMap(4);
                        hashMap4.put("id", new eg.a("id", "INTEGER", true, 1));
                        hashMap4.put(ActivitySettings.COLUMN_CURRENT_STEP_GOAL, new eg.a(ActivitySettings.COLUMN_CURRENT_STEP_GOAL, "INTEGER", true, 0));
                        hashMap4.put(ActivitySettings.COLUMN_CURRENT_CALORIES_GOAL, new eg.a(ActivitySettings.COLUMN_CURRENT_CALORIES_GOAL, "INTEGER", true, 0));
                        hashMap4.put(ActivitySettings.COLUMN_CURRENT_ACTIVE_TIME_GOAL, new eg.a(ActivitySettings.COLUMN_CURRENT_ACTIVE_TIME_GOAL, "INTEGER", true, 0));
                        eg egVar4 = new eg(ActivitySettings.TABLE_NAME, hashMap4, new HashSet(0), new HashSet(0));
                        eg a4 = eg.a(ggVar2, ActivitySettings.TABLE_NAME);
                        if (egVar4.equals(a4)) {
                            HashMap hashMap5 = new HashMap(4);
                            hashMap5.put("id", new eg.a("id", "INTEGER", true, 1));
                            hashMap5.put("recommendedStepsGoal", new eg.a("recommendedStepsGoal", "INTEGER", true, 0));
                            hashMap5.put("recommendedCaloriesGoal", new eg.a("recommendedCaloriesGoal", "INTEGER", true, 0));
                            hashMap5.put("recommendedActiveTimeGoal", new eg.a("recommendedActiveTimeGoal", "INTEGER", true, 0));
                            eg egVar5 = new eg("activityRecommendedGoals", hashMap5, new HashSet(0), new HashSet(0));
                            eg a5 = eg.a(ggVar2, "activityRecommendedGoals");
                            if (egVar5.equals(a5)) {
                                HashMap hashMap6 = new HashMap(18);
                                hashMap6.put(PlaceManager.PARAM_SPEED, new eg.a(PlaceManager.PARAM_SPEED, "TEXT", false, 0));
                                hashMap6.put(PlaceFields.LOCATION, new eg.a(PlaceFields.LOCATION, "TEXT", false, 0));
                                hashMap6.put("states", new eg.a("states", "TEXT", true, 0));
                                hashMap6.put("id", new eg.a("id", "TEXT", true, 1));
                                hashMap6.put("date", new eg.a("date", "TEXT", true, 0));
                                String str4 = str3;
                                hashMap6.put(str4, new eg.a(str4, "TEXT", true, 0));
                                String str5 = str2;
                                hashMap6.put(str5, new eg.a(str5, "TEXT", true, 0));
                                hashMap6.put(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, new eg.a(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, "TEXT", false, 0));
                                hashMap6.put("step", new eg.a("step", "TEXT", false, 0));
                                hashMap6.put("calorie", new eg.a("calorie", "TEXT", false, 0));
                                hashMap6.put("distance", new eg.a("distance", "TEXT", false, 0));
                                hashMap6.put("heartRate", new eg.a("heartRate", "TEXT", false, 0));
                                hashMap6.put("sourceType", new eg.a("sourceType", "TEXT", false, 0));
                                hashMap6.put("workoutType", new eg.a("workoutType", "TEXT", false, 0));
                                hashMap6.put("timezoneOffset", new eg.a("timezoneOffset", "INTEGER", true, 0));
                                hashMap6.put("duration", new eg.a("duration", "INTEGER", true, 0));
                                hashMap6.put("createdAt", new eg.a("createdAt", "INTEGER", true, 0));
                                hashMap6.put("updatedAt", new eg.a("updatedAt", "INTEGER", true, 0));
                                eg egVar6 = new eg("workout_session", hashMap6, new HashSet(0), new HashSet(0));
                                eg a6 = eg.a(ggVar2, "workout_session");
                                if (egVar6.equals(a6)) {
                                    HashMap hashMap7 = new HashMap(14);
                                    hashMap7.put("step", new eg.a("step", "TEXT", true, 0));
                                    hashMap7.put("activeMinute", new eg.a("activeMinute", "TEXT", true, 0));
                                    hashMap7.put("calorie", new eg.a("calorie", "TEXT", true, 0));
                                    hashMap7.put("distance", new eg.a("distance", "TEXT", true, 0));
                                    hashMap7.put("stress", new eg.a("stress", "TEXT", false, 0));
                                    hashMap7.put("resting", new eg.a("resting", "TEXT", true, 0));
                                    hashMap7.put("heartRate", new eg.a("heartRate", "TEXT", false, 0));
                                    hashMap7.put("sleeps", new eg.a("sleeps", "TEXT", true, 0));
                                    hashMap7.put("workouts", new eg.a("workouts", "TEXT", true, 0));
                                    hashMap7.put(str4, new eg.a(str4, "TEXT", true, 1));
                                    hashMap7.put(str5, new eg.a(str5, "TEXT", true, 0));
                                    hashMap7.put("syncTime", new eg.a("syncTime", "TEXT", true, 0));
                                    hashMap7.put("timezoneOffsetInSecond", new eg.a("timezoneOffsetInSecond", "INTEGER", true, 0));
                                    hashMap7.put("serialNumber", new eg.a("serialNumber", "TEXT", true, 0));
                                    eg egVar7 = new eg("fitness_data", hashMap7, new HashSet(0), new HashSet(0));
                                    eg a7 = eg.a(ggVar2, "fitness_data");
                                    if (egVar7.equals(a7)) {
                                        HashMap hashMap8 = new HashMap(12);
                                        hashMap8.put("id", new eg.a("id", "TEXT", true, 1));
                                        hashMap8.put(GoalTrackingSummary.COLUMN_AVERAGE, new eg.a(GoalTrackingSummary.COLUMN_AVERAGE, "REAL", true, 0));
                                        hashMap8.put("date", new eg.a("date", "TEXT", true, 0));
                                        hashMap8.put("createdAt", new eg.a("createdAt", "INTEGER", true, 0));
                                        hashMap8.put("updatedAt", new eg.a("updatedAt", "INTEGER", true, 0));
                                        hashMap8.put(str5, new eg.a(str5, "TEXT", true, 0));
                                        hashMap8.put(str4, new eg.a(str4, "TEXT", true, 0));
                                        hashMap8.put("timezoneOffset", new eg.a("timezoneOffset", "INTEGER", true, 0));
                                        hashMap8.put("min", new eg.a("min", "INTEGER", true, 0));
                                        hashMap8.put("max", new eg.a("max", "INTEGER", true, 0));
                                        hashMap8.put("minuteCount", new eg.a("minuteCount", "INTEGER", true, 0));
                                        hashMap8.put("resting", new eg.a("resting", "TEXT", false, 0));
                                        eg egVar8 = new eg("heart_rate_sample", hashMap8, new HashSet(0), new HashSet(0));
                                        eg a8 = eg.a(ggVar2, "heart_rate_sample");
                                        if (egVar8.equals(a8)) {
                                            HashMap hashMap9 = new HashMap(8);
                                            hashMap9.put(GoalTrackingSummary.COLUMN_AVERAGE, new eg.a(GoalTrackingSummary.COLUMN_AVERAGE, "REAL", true, 0));
                                            hashMap9.put("date", new eg.a("date", "TEXT", true, 1));
                                            hashMap9.put("createdAt", new eg.a("createdAt", "INTEGER", true, 0));
                                            hashMap9.put("updatedAt", new eg.a("updatedAt", "INTEGER", true, 0));
                                            hashMap9.put("min", new eg.a("min", "INTEGER", true, 0));
                                            hashMap9.put("max", new eg.a("max", "INTEGER", true, 0));
                                            hashMap9.put("minuteCount", new eg.a("minuteCount", "INTEGER", true, 0));
                                            hashMap9.put("resting", new eg.a("resting", "TEXT", false, 0));
                                            eg egVar9 = new eg("daily_heart_rate_summary", hashMap9, new HashSet(0), new HashSet(0));
                                            eg a9 = eg.a(ggVar2, "daily_heart_rate_summary");
                                            if (egVar9.equals(a9)) {
                                                HashMap hashMap10 = new HashMap(16);
                                                hashMap10.put("id", new eg.a("id", "TEXT", true, 1));
                                                hashMap10.put("uid", new eg.a("uid", "TEXT", true, 0));
                                                hashMap10.put("activeTimeBestDay", new eg.a("activeTimeBestDay", "TEXT", false, 0));
                                                hashMap10.put("activeTimeBestStreak", new eg.a("activeTimeBestStreak", "TEXT", false, 0));
                                                hashMap10.put("caloriesBestDay", new eg.a("caloriesBestDay", "TEXT", false, 0));
                                                hashMap10.put("caloriesBestStreak", new eg.a("caloriesBestStreak", "TEXT", false, 0));
                                                hashMap10.put("stepsBestDay", new eg.a("stepsBestDay", "TEXT", false, 0));
                                                hashMap10.put("stepsBestStreak", new eg.a("stepsBestStreak", "TEXT", false, 0));
                                                hashMap10.put("totalActiveTime", new eg.a("totalActiveTime", "INTEGER", true, 0));
                                                hashMap10.put("totalCalories", new eg.a("totalCalories", "REAL", true, 0));
                                                hashMap10.put("totalDays", new eg.a("totalDays", "INTEGER", true, 0));
                                                hashMap10.put("totalDistance", new eg.a("totalDistance", "REAL", true, 0));
                                                hashMap10.put("totalSteps", new eg.a("totalSteps", "INTEGER", true, 0));
                                                hashMap10.put("totalIntensityDistInStep", new eg.a("totalIntensityDistInStep", "TEXT", true, 0));
                                                hashMap10.put("createdAt", new eg.a("createdAt", "INTEGER", true, 0));
                                                hashMap10.put("updatedAt", new eg.a("updatedAt", "INTEGER", true, 0));
                                                eg egVar10 = new eg(ActivityStatistic.TABLE_NAME, hashMap10, new HashSet(0), new HashSet(0));
                                                eg a10 = eg.a(ggVar2, ActivityStatistic.TABLE_NAME);
                                                if (!egVar10.equals(a10)) {
                                                    throw new IllegalStateException("Migration didn't properly handle activity_statistic(com.portfolio.platform.data.ActivityStatistic).\n Expected:\n" + egVar10 + str + a10);
                                                }
                                                return;
                                            }
                                            throw new IllegalStateException("Migration didn't properly handle daily_heart_rate_summary(com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary).\n Expected:\n" + egVar9 + str + a9);
                                        }
                                        throw new IllegalStateException("Migration didn't properly handle heart_rate_sample(com.portfolio.platform.data.model.diana.heartrate.HeartRateSample).\n Expected:\n" + egVar8 + str + a8);
                                    }
                                    throw new IllegalStateException("Migration didn't properly handle fitness_data(com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper).\n Expected:\n" + egVar7 + str + a7);
                                }
                                throw new IllegalStateException("Migration didn't properly handle workout_session(com.portfolio.platform.data.model.diana.workout.WorkoutSession).\n Expected:\n" + egVar6 + str + a6);
                            }
                            throw new IllegalStateException("Migration didn't properly handle activityRecommendedGoals(com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals).\n Expected:\n" + egVar5 + str + a5);
                        }
                        throw new IllegalStateException("Migration didn't properly handle activitySettings(com.portfolio.platform.data.model.room.fitness.ActivitySettings).\n Expected:\n" + egVar4 + str + a4);
                    }
                    throw new IllegalStateException("Migration didn't properly handle sampleday(com.portfolio.platform.data.model.room.fitness.ActivitySummary).\n Expected:\n" + egVar3 + str + a3);
                }
                throw new IllegalStateException("Migration didn't properly handle activity_sample(com.portfolio.platform.data.model.room.fitness.ActivitySample).\n Expected:\n" + egVar2 + str + a2);
            }
            throw new IllegalStateException("Migration didn't properly handle sampleraw(com.portfolio.platform.data.model.room.fitness.SampleRaw).\n Expected:\n" + egVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public ActivitySampleDao activitySampleDao() {
        ActivitySampleDao activitySampleDao;
        if (this._activitySampleDao != null) {
            return this._activitySampleDao;
        }
        synchronized (this) {
            if (this._activitySampleDao == null) {
                this._activitySampleDao = new ActivitySampleDao_Impl(this);
            }
            activitySampleDao = this._activitySampleDao;
        }
        return activitySampleDao;
    }

    @DexIgnore
    public ActivitySummaryDao activitySummaryDao() {
        ActivitySummaryDao activitySummaryDao;
        if (this._activitySummaryDao != null) {
            return this._activitySummaryDao;
        }
        synchronized (this) {
            if (this._activitySummaryDao == null) {
                this._activitySummaryDao = new ActivitySummaryDao_Impl(this);
            }
            activitySummaryDao = this._activitySummaryDao;
        }
        return activitySummaryDao;
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `sampleraw`");
            a.b("DELETE FROM `activity_sample`");
            a.b("DELETE FROM `sampleday`");
            a.b("DELETE FROM `activitySettings`");
            a.b("DELETE FROM `activityRecommendedGoals`");
            a.b("DELETE FROM `workout_session`");
            a.b("DELETE FROM `fitness_data`");
            a.b("DELETE FROM `heart_rate_sample`");
            a.b("DELETE FROM `daily_heart_rate_summary`");
            a.b("DELETE FROM `activity_statistic`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), SampleRaw.TABLE_NAME, ActivitySample.TABLE_NAME, SampleDay.TABLE_NAME, ActivitySettings.TABLE_NAME, "activityRecommendedGoals", "workout_session", "fitness_data", "heart_rate_sample", "daily_heart_rate_summary", ActivityStatistic.TABLE_NAME);
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(21), "9c43c28ab51a9a7a22d2899f05515d5a", "62fbf7a454b61c101100bb808cb34c36");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }

    @DexIgnore
    public FitnessDataDao getFitnessDataDao() {
        FitnessDataDao fitnessDataDao;
        if (this._fitnessDataDao != null) {
            return this._fitnessDataDao;
        }
        synchronized (this) {
            if (this._fitnessDataDao == null) {
                this._fitnessDataDao = new FitnessDataDao_Impl(this);
            }
            fitnessDataDao = this._fitnessDataDao;
        }
        return fitnessDataDao;
    }

    @DexIgnore
    public HeartRateDailySummaryDao getHeartRateDailySummaryDao() {
        HeartRateDailySummaryDao heartRateDailySummaryDao;
        if (this._heartRateDailySummaryDao != null) {
            return this._heartRateDailySummaryDao;
        }
        synchronized (this) {
            if (this._heartRateDailySummaryDao == null) {
                this._heartRateDailySummaryDao = new HeartRateDailySummaryDao_Impl(this);
            }
            heartRateDailySummaryDao = this._heartRateDailySummaryDao;
        }
        return heartRateDailySummaryDao;
    }

    @DexIgnore
    public HeartRateSampleDao getHeartRateDao() {
        HeartRateSampleDao heartRateSampleDao;
        if (this._heartRateSampleDao != null) {
            return this._heartRateSampleDao;
        }
        synchronized (this) {
            if (this._heartRateSampleDao == null) {
                this._heartRateSampleDao = new HeartRateSampleDao_Impl(this);
            }
            heartRateSampleDao = this._heartRateSampleDao;
        }
        return heartRateSampleDao;
    }

    @DexIgnore
    public WorkoutDao getWorkoutDao() {
        WorkoutDao workoutDao;
        if (this._workoutDao != null) {
            return this._workoutDao;
        }
        synchronized (this) {
            if (this._workoutDao == null) {
                this._workoutDao = new WorkoutDao_Impl(this);
            }
            workoutDao = this._workoutDao;
        }
        return workoutDao;
    }

    @DexIgnore
    public SampleRawDao sampleRawDao() {
        SampleRawDao sampleRawDao;
        if (this._sampleRawDao != null) {
            return this._sampleRawDao;
        }
        synchronized (this) {
            if (this._sampleRawDao == null) {
                this._sampleRawDao = new SampleRawDao_Impl(this);
            }
            sampleRawDao = this._sampleRawDao;
        }
        return sampleRawDao;
    }
}
