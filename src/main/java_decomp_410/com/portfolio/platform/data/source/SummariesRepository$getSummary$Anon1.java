package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.FitnessDayData;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDate;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getSummary$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends NetworkBoundResource<ActivitySummary, xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository$getSummary$Anon1 this$Anon0;

        @DexIgnore
        public Anon1(SummariesRepository$getSummary$Anon1 summariesRepository$getSummary$Anon1, List list) {
            this.this$Anon0 = summariesRepository$getSummary$Anon1;
            this.$fitnessDataList = list;
        }

        @DexIgnore
        public Object createCall(yb4<? super qr4<xz1>> yb4) {
            Date n = rk2.n(this.this$Anon0.$date);
            Date i = rk2.i(this.this$Anon0.$date);
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "calendar");
            instance.setTimeInMillis(0);
            ApiServiceV2 access$getMApiServiceV2$p = this.this$Anon0.this$Anon0.mApiServiceV2;
            String e = rk2.e(n);
            kd4.a((Object) e, "DateHelper.formatShortDate(startDate)");
            String e2 = rk2.e(i);
            kd4.a((Object) e2, "DateHelper.formatShortDate(endDate)");
            return access$getMApiServiceV2$p.getSummaries(e, e2, 0, 100, yb4);
        }

        @DexIgnore
        public LiveData<ActivitySummary> loadFromDb() {
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "calendar");
            instance.setTime(this.this$Anon0.$date);
            LiveData<ActivitySummary> activitySummaryLiveData = this.this$Anon0.this$Anon0.mActivitySummaryDao.getActivitySummaryLiveData(instance.get(1), instance.get(2) + 1, instance.get(5));
            if (!rk2.s(this.this$Anon0.$date).booleanValue()) {
                return activitySummaryLiveData;
            }
            LiveData<ActivitySummary> a = hc.a(activitySummaryLiveData, new SummariesRepository$getSummary$Anon1$Anon1$loadFromDb$Anon1(this));
            kd4.a((Object) a, "Transformations.map(acti\u2026ary\n                    }");
            return a;
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().e(SummariesRepository.TAG, "getSummary - onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(xz1 xz1) {
            kd4.b(xz1, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "getSummary - saveCallResult -- date=" + this.this$Anon0.$date + ", item=" + xz1);
            try {
                ArrayList arrayList = new ArrayList();
                rz1 rz1 = new rz1();
                rz1.a(Date.class, new GsonConvertDate());
                rz1.a(DateTime.class, new GsonConvertDateTime());
                ApiResponse apiResponse = (ApiResponse) rz1.a().a(xz1.toString(), new SummariesRepository$getSummary$Anon1$Anon1$saveCallResult$Anon1().getType());
                if (apiResponse != null) {
                    List<FitnessDayData> list = apiResponse.get_items();
                    if (list != null) {
                        for (FitnessDayData activitySummary : list) {
                            ActivitySummary activitySummary2 = activitySummary.toActivitySummary();
                            kd4.a((Object) activitySummary2, "it.toActivitySummary()");
                            arrayList.add(activitySummary2);
                        }
                    }
                }
                List<FitnessDataWrapper> fitnessData = this.this$Anon0.this$Anon0.mFitnessDataDao.getFitnessData(this.this$Anon0.$date, this.this$Anon0.$date);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d(SummariesRepository.TAG, "fitnessDatasSize " + fitnessData.size());
                if ((!arrayList.isEmpty()) && fitnessData.isEmpty()) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d(SummariesRepository.TAG, "upsert " + ((ActivitySummary) arrayList.get(0)));
                    ActivitySummaryDao access$getMActivitySummaryDao$p = this.this$Anon0.this$Anon0.mActivitySummaryDao;
                    Object obj = arrayList.get(0);
                    kd4.a(obj, "summaryList[0]");
                    access$getMActivitySummaryDao$p.upsertActivitySummary((ActivitySummary) obj);
                }
            } catch (Exception e) {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getSummary - saveCallResult -- e=");
                e.printStackTrace();
                sb.append(qa4.a);
                local4.e(SummariesRepository.TAG, sb.toString());
            }
        }

        @DexIgnore
        public boolean shouldFetch(ActivitySummary activitySummary) {
            return this.$fitnessDataList.isEmpty();
        }
    }

    @DexIgnore
    public SummariesRepository$getSummary$Anon1(SummariesRepository summariesRepository, Date date) {
        this.this$Anon0 = summariesRepository;
        this.$date = date;
    }

    @DexIgnore
    public final LiveData<os3<ActivitySummary>> apply(List<FitnessDataWrapper> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getSummary - date=" + this.$date + " fitnessDataList=" + list.size());
        return new Anon1(this, list).asLiveData();
    }
}
