package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SummariesRepository$getSummary$1$1$loadFromDb$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, Y> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SummariesRepository$getSummary$1.C57181 this$0;

    @DexIgnore
    public SummariesRepository$getSummary$1$1$loadFromDb$1(com.portfolio.platform.data.source.SummariesRepository$getSummary$1.C57181 r1) {
        this.this$0 = r1;
    }

    @DexIgnore
    public final com.portfolio.platform.data.model.room.fitness.ActivitySummary apply(com.portfolio.platform.data.model.room.fitness.ActivitySummary activitySummary) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d(com.portfolio.platform.data.source.SummariesRepository.TAG, "getSummary - loadFromDb -- date=" + this.this$0.this$0.$date + ", summary=" + activitySummary);
        if (activitySummary != null) {
            activitySummary.setSteps(java.lang.Math.max((double) this.this$0.this$0.this$0.mFitnessHelper.mo32683a(new java.util.Date()), activitySummary.getSteps()));
        }
        return activitySummary;
    }
}
