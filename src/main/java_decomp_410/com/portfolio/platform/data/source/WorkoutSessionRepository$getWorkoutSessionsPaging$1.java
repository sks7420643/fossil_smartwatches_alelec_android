package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WorkoutSessionRepository$getWorkoutSessionsPaging$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {
    @DexIgnore
    public static /* final */ com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessionsPaging$1 INSTANCE; // = new com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessionsPaging$1();

    @DexIgnore
    public final androidx.lifecycle.LiveData<com.portfolio.platform.data.NetworkState> apply(com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource workoutSessionLocalDataSource) {
        return workoutSessionLocalDataSource.getMNetworkState();
    }
}
