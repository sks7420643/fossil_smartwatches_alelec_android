package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingSummaryLocalDataSource$loadAfter$Anon1 implements PagingRequestHelper.b {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource this$Anon0;

    @DexIgnore
    public GoalTrackingSummaryLocalDataSource$loadAfter$Anon1(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        this.this$Anon0 = goalTrackingSummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(PagingRequestHelper.b.a aVar) {
        Pair pair = (Pair) kb4.d(this.this$Anon0.mRequestAfterQueue);
        kd4.a((Object) aVar, "helperCallback");
        fi4 unused = this.this$Anon0.loadData(PagingRequestHelper.RequestType.AFTER, (Date) pair.getFirst(), (Date) pair.getSecond(), aVar);
    }
}
