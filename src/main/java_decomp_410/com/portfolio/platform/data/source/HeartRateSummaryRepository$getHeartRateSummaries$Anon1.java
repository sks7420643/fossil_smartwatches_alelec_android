package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m3;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConvertDateTimeToLong;
import com.portfolio.platform.helper.GsonConverterShortDate;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSummaryRepository$getHeartRateSummaries$Anon1<I, O> implements m3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryRepository this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends NetworkBoundResource<List<DailyHeartRateSummary>, ApiResponse<xz1>> {
        @DexIgnore
        public /* final */ /* synthetic */ Pair $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSummaryRepository$getHeartRateSummaries$Anon1 this$Anon0;

        @DexIgnore
        public Anon1(HeartRateSummaryRepository$getHeartRateSummaries$Anon1 heartRateSummaryRepository$getHeartRateSummaries$Anon1, Pair pair) {
            this.this$Anon0 = heartRateSummaryRepository$getHeartRateSummaries$Anon1;
            this.$downloadingDate = pair;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
            if (r0 != null) goto L_0x0019;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
            if (r0 != null) goto L_0x0033;
         */
        @DexIgnore
        public Object createCall(yb4<? super qr4<ApiResponse<xz1>>> yb4) {
            Date date;
            Date date2;
            ApiServiceV2 access$getMApiService$p = this.this$Anon0.this$Anon0.mApiService;
            Pair pair = this.$downloadingDate;
            if (pair != null) {
                date = (Date) pair.getFirst();
            }
            date = this.this$Anon0.$startDate;
            String e = rk2.e(date);
            kd4.a((Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            Pair pair2 = this.$downloadingDate;
            if (pair2 != null) {
                date2 = (Date) pair2.getSecond();
            }
            date2 = this.this$Anon0.$endDate;
            String e2 = rk2.e(date2);
            kd4.a((Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiService$p.getDailyHeartRateSummaries(e, e2, 0, 100, yb4);
        }

        @DexIgnore
        public LiveData<List<DailyHeartRateSummary>> loadFromDb() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getHeartRateSummaries isNotToday startDate = " + this.this$Anon0.$startDate + ", endDate = " + this.this$Anon0.$endDate);
            HeartRateDailySummaryDao access$getMHeartRateSummaryDao$p = this.this$Anon0.this$Anon0.mHeartRateSummaryDao;
            HeartRateSummaryRepository$getHeartRateSummaries$Anon1 heartRateSummaryRepository$getHeartRateSummaries$Anon1 = this.this$Anon0;
            return access$getMHeartRateSummaryDao$p.getDailyHeartRateSummariesLiveData(heartRateSummaryRepository$getHeartRateSummaries$Anon1.$startDate, heartRateSummaryRepository$getHeartRateSummaries$Anon1.$endDate);
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().d(HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease(), "getHeartRateSummaries onFetchFailed");
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00ab A[Catch:{ Exception -> 0x00fa }] */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00ee A[Catch:{ Exception -> 0x00fa }] */
        /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
        public void saveCallResult(ApiResponse<xz1> apiResponse) {
            Date date;
            Pair pair;
            Date date2;
            List<FitnessDataWrapper> fitnessData;
            kd4.b(apiResponse, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "saveCallResult onResponse: response = " + apiResponse);
            try {
                if (!apiResponse.get_items().isEmpty()) {
                    rz1 rz1 = new rz1();
                    rz1.a(Long.TYPE, new GsonConvertDateTimeToLong());
                    rz1.a(DateTime.class, new GsonConvertDateTime());
                    rz1.a(Date.class, new GsonConverterShortDate());
                    Gson a = rz1.a();
                    List<xz1> list = apiResponse.get_items();
                    ArrayList arrayList = new ArrayList(db4.a(list, 10));
                    for (xz1 a2 : list) {
                        arrayList.add((DailyHeartRateSummary) a.a((JsonElement) a2, new HeartRateSummaryRepository$getHeartRateSummaries$Anon1$Anon1$saveCallResult$summaries$Anon1$Anon1().getType()));
                    }
                    List d = kb4.d(arrayList);
                    FitnessDataDao access$getMFitnessDataDao$p = this.this$Anon0.this$Anon0.mFitnessDataDao;
                    Pair pair2 = this.$downloadingDate;
                    if (pair2 != null) {
                        date = (Date) pair2.getFirst();
                        if (date != null) {
                            pair = this.$downloadingDate;
                            if (pair != null) {
                                date2 = (Date) pair.getSecond();
                                if (date2 != null) {
                                    fitnessData = access$getMFitnessDataDao$p.getFitnessData(date, date2);
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String tAG$app_fossilRelease2 = HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
                                    local2.d(tAG$app_fossilRelease2, "heartrate summary " + d + " fitnessDataSize " + fitnessData.size());
                                    if (fitnessData.isEmpty()) {
                                        this.this$Anon0.this$Anon0.mHeartRateSummaryDao.insertListDailyHeartRateSummary(d);
                                        return;
                                    }
                                    return;
                                }
                            }
                            date2 = this.this$Anon0.$endDate;
                            fitnessData = access$getMFitnessDataDao$p.getFitnessData(date, date2);
                            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                            String tAG$app_fossilRelease22 = HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
                            local22.d(tAG$app_fossilRelease22, "heartrate summary " + d + " fitnessDataSize " + fitnessData.size());
                            if (fitnessData.isEmpty()) {
                            }
                        }
                    }
                    date = this.this$Anon0.$startDate;
                    pair = this.$downloadingDate;
                    if (pair != null) {
                    }
                    date2 = this.this$Anon0.$endDate;
                    fitnessData = access$getMFitnessDataDao$p.getFitnessData(date, date2);
                    ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
                    String tAG$app_fossilRelease222 = HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
                    local222.d(tAG$app_fossilRelease222, "heartrate summary " + d + " fitnessDataSize " + fitnessData.size());
                    if (fitnessData.isEmpty()) {
                    }
                }
            } catch (Exception e) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease3 = HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
                StringBuilder sb = new StringBuilder();
                sb.append("saveCallResult exception=");
                e.printStackTrace();
                sb.append(qa4.a);
                local3.e(tAG$app_fossilRelease3, sb.toString());
            }
        }

        @DexIgnore
        public boolean shouldFetch(List<DailyHeartRateSummary> list) {
            return this.this$Anon0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public HeartRateSummaryRepository$getHeartRateSummaries$Anon1(HeartRateSummaryRepository heartRateSummaryRepository, Date date, Date date2, boolean z) {
        this.this$Anon0 = heartRateSummaryRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public final LiveData<os3<List<DailyHeartRateSummary>>> apply(List<FitnessDataWrapper> list) {
        kd4.a((Object) list, "fitnessDataList");
        return new Anon1(this, FitnessDataWrapperKt.calculateRangeDownload(list, this.$startDate, this.$endDate)).asLiveData();
    }
}
