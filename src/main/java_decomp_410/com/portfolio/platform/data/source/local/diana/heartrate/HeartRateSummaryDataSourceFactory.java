package com.portfolio.platform.data.source.local.diana.heartrate;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ld;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSummaryDataSourceFactory extends ld.b<Date, DailyHeartRateSummary> {
    @DexIgnore
    public /* final */ h42 appExecutors;
    @DexIgnore
    public /* final */ Date createdDate;
    @DexIgnore
    public /* final */ FitnessDataRepository fitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase fitnessDatabase;
    @DexIgnore
    public /* final */ HeartRateDailySummaryDao heartRateSummaryDao;
    @DexIgnore
    public /* final */ PagingRequestHelper.a listener;
    @DexIgnore
    public HeartRateSummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<HeartRateSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ HeartRateSummaryRepository summariesRepository;

    @DexIgnore
    public HeartRateSummaryDataSourceFactory(HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository2, HeartRateDailySummaryDao heartRateDailySummaryDao, FitnessDatabase fitnessDatabase2, Date date, h42 h42, PagingRequestHelper.a aVar, Calendar calendar) {
        kd4.b(heartRateSummaryRepository, "summariesRepository");
        kd4.b(fitnessDataRepository2, "fitnessDataRepository");
        kd4.b(heartRateDailySummaryDao, "heartRateSummaryDao");
        kd4.b(fitnessDatabase2, "fitnessDatabase");
        kd4.b(date, "createdDate");
        kd4.b(h42, "appExecutors");
        kd4.b(aVar, "listener");
        kd4.b(calendar, "mStartCalendar");
        this.summariesRepository = heartRateSummaryRepository;
        this.fitnessDataRepository = fitnessDataRepository2;
        this.heartRateSummaryDao = heartRateDailySummaryDao;
        this.fitnessDatabase = fitnessDatabase2;
        this.createdDate = date;
        this.appExecutors = h42;
        this.listener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    public ld<Date, DailyHeartRateSummary> create() {
        this.localDataSource = new HeartRateSummaryLocalDataSource(this.summariesRepository, this.fitnessDataRepository, this.heartRateSummaryDao, this.fitnessDatabase, this.createdDate, this.appExecutors, this.listener, this.mStartCalendar);
        this.sourceLiveData.a(this.localDataSource);
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource = this.localDataSource;
        if (heartRateSummaryLocalDataSource != null) {
            return heartRateSummaryLocalDataSource;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final HeartRateSummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<HeartRateSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        this.localDataSource = heartRateSummaryLocalDataSource;
    }
}
