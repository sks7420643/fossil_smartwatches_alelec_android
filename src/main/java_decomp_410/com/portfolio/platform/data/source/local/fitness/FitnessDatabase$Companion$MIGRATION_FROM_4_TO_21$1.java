package com.portfolio.platform.data.source.local.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FitnessDatabase$Companion$MIGRATION_FROM_4_TO_21$1 extends com.fossil.blesdk.obfuscated.C3361yf {
    @DexIgnore
    public FitnessDatabase$Companion$MIGRATION_FROM_4_TO_21$1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    public void migrate(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        int i;
        com.fossil.blesdk.obfuscated.C1874gg ggVar2 = ggVar;
        com.fossil.blesdk.obfuscated.kd4.m24411b(ggVar2, "database");
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migration 4 to 21 start");
        ggVar.mo11234s();
        org.joda.time.DateTime dateTime = new org.joda.time.DateTime();
        java.util.TimeZone timeZone = dateTime.getZone().toTimeZone();
        int year = dateTime.getYear();
        int monthOfYear = dateTime.getMonthOfYear();
        int dayOfMonth = dateTime.getDayOfMonth();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) timeZone, "timeZone");
        java.lang.String id = timeZone.getID();
        int dSTSavings = timeZone.inDaylightTime(dateTime.toDate()) ? timeZone.getDSTSavings() : 0;
        com.portfolio.platform.data.model.MFUser b = com.fossil.blesdk.obfuscated.dn2.f14174p.mo26576a().mo26574n().mo26604b();
        if (b == null || b.getWeightInGrams() <= 0 || b.getHeightInCentimeters() <= 0) {
            i = com.fossil.wearables.fsl.enums.ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL;
        } else {
            int heightInCentimeters = b.getHeightInCentimeters();
            com.portfolio.platform.enums.Gender gender = b.getGender();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) gender, "user.gender");
            i = com.fossil.blesdk.obfuscated.mk2.f16783a.mo29426a(com.portfolio.platform.data.model.MFUser.getAge(b.getBirthday()), b.getWeightInGrams() / 1000, heightInCentimeters, gender);
        }
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate with calories goal " + i);
        try {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate sample raw table");
            ggVar2.mo11230b("CREATE TABLE sampleraw_new (id TEXT PRIMARY KEY NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, sourceId TEXT NOT NULL DEFAULT '', movementTypeValue TEXT, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, sourceTypeValue TEXT NOT NULL DEFAULT '" + com.portfolio.platform.enums.FitnessSourceType.Device.getValue() + "', pinType INTEGER NOT NULL DEFAULT 1, uaPinType INTEGER NOT NULL DEFAULT 1, timeZoneID TEXT, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '')");
            ggVar2.mo11230b("INSERT INTO sampleraw_new (id, startTime, endTime, sourceId, movementTypeValue, steps, calories, distance, sourceTypeValue, pinType, uaPinType, timeZoneID) SELECT id, substr(startTime, 1, 23), substr(endTime, 1, 23), sourceId, movementTypeValue, steps, calories, distance, sourceTypeValue, pinType, uaPinType, timeZoneID  FROM sampleraw");
            ggVar2.mo11230b("DROP TABLE sampleraw");
            ggVar2.mo11230b("ALTER TABLE sampleraw_new RENAME TO sampleraw");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate sample raw table success");
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local2.mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate sample raw table fail " + e);
            ggVar2.mo11230b("DROP TABLE IF EXISTS sampleraw_new");
            ggVar2.mo11230b("DROP TABLE IF EXISTS sampleraw");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS sampleraw (id TEXT PRIMARY KEY NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, sourceId TEXT NOT NULL DEFAULT '', movementTypeValue TEXT NOT NULL DEFAULT '" + com.portfolio.platform.enums.FitnessMovementType.WALKING.getValue() + "', steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, sourceTypeValue TEXT NOT NULL DEFAULT '" + com.portfolio.platform.enums.FitnessSourceType.Device.getValue() + "', pinType INTEGER NOT NULL DEFAULT 1, uaPinType INTEGER NOT NULL DEFAULT 1, timeZoneID TEXT, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '')");
        }
        try {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate intensity data for sampleraw");
            ggVar2.mo11230b("CREATE TABLE intensities (min INTEGER NOT NULL, max INTEGER NOT NULL, intensity TEXT NOT NULL DEFAULT '')");
            ggVar2.mo11230b("INSERT INTO intensities (min, max, intensity) VALUES (0, 70, 'light')");
            ggVar2.mo11230b("INSERT INTO intensities (min, max, intensity) VALUES (70, 140, 'moderate')");
            ggVar2.mo11230b("INSERT INTO intensities (min, max, intensity) VALUES (140, 2147483647, 'intense')");
            ggVar2.mo11230b("UPDATE sampleraw SET intensityDistInSteps=( SELECT '{\"' || intensity || '\":' || sampleraw.steps || '}' FROM intensities WHERE sampleraw.steps >= intensities.min AND sampleraw.steps < intensities.max)");
            ggVar2.mo11230b("DROP TABLE intensities");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate intensity data for sampleraw success");
        } catch (java.lang.Exception e2) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local3.mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate intensity data for sampleraw " + e2);
            ggVar2.mo11230b("DROP TABLE IF EXISTS intensities");
        }
        try {
            ggVar2.mo11230b("CREATE TABLE sampleday_new (year INTEGER NOT NULL, month INTEGER NOT NULL, day INTEGER NOT NULL, timezoneName TEXT, dstOffset INTEGER, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, intensities TEXT NOT NULL DEFAULT '', stepGoal INTEGER NOT NULL DEFAULT 0, caloriesGoal INTEGER NOT NULL DEFAULT " + i + ", activeTimeGoal INTEGER NOT NULL DEFAULT 0, createdAt INTEGER, updatedAt INTEGER, activeTime INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 1, PRIMARY KEY (year, month, day))");
            ggVar2.mo11230b("INSERT INTO sampleday_new (year, month, day, timezoneName, dstOffset, steps, calories, distance, createdAt, updatedAt, pinType) SELECT year, month, day, timezoneName, dstOffset, steps, calories, distance, createdAt, updatedAt, pinType  FROM sampleday");
            ggVar2.mo11230b("DROP TABLE sampleday");
            ggVar2.mo11230b("ALTER TABLE sampleday_new RENAME TO sampleday");
            ggVar2.mo11230b("INSERT OR IGNORE INTO sampleday (year, month, day, timezoneName, dstOffset, steps, calories, distance, intensities, stepGoal, caloriesGoal, activeTimeGoal, createdAt, updatedAt, activeTime, pinType)" + " VALUES(" + year + ", " + monthOfYear + ", " + dayOfMonth + ", '" + id + "', " + dSTSavings + ", 0.0, 0.0, 0.0, '[0,0,0]', 5000, " + i + ", 30, " + java.lang.System.currentTimeMillis() + ", " + java.lang.System.currentTimeMillis() + ", 0, 1)");
            ggVar2.mo11230b("UPDATE sampleday SET stepGoal=(SELECT steps FROM dailygoal) WHERE sampleday.year = year AND sampleday.month = month AND sampleday.day = day");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate sample day success");
        } catch (java.lang.Exception e3) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local4.mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migrate sample day fail " + e3);
            ggVar2.mo11230b("DROP TABLE IF EXISTS sampleday_new");
            ggVar2.mo11230b("DROP TABLE IF EXISTS sampleday");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS sampleday (year INTEGER NOT NULL, month INTEGER NOT NULL, day INTEGER NOT NULL, timezoneName TEXT, dstOffset INTEGER, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, intensities TEXT NOT NULL DEFAULT '', stepGoal INTEGER NOT NULL DEFAULT 0, caloriesGoal INTEGER NOT NULL DEFAULT 0, activeTimeGoal INTEGER NOT NULL DEFAULT 0, createdAt INTEGER, updatedAt INTEGER, activeTime INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 1, PRIMARY KEY (year, month, day))");
        }
        try {
            ggVar2.mo11230b("CREATE TABLE activitySettings (currentStepGoal INTEGER NOT NULL DEFAULT 5000, currentCaloriesGoal INTEGER NOT NULL DEFAULT " + i + ", currentActiveTimeGoal INTEGER NOT NULL DEFAULT 30, id INTEGER PRIMARY KEY ASC NOT NULL)");
            ggVar2.mo11230b("INSERT INTO activitySettings (currentStepGoal, currentCaloriesGoal) VALUES (IFNULL((SELECT steps FROM dailygoal ORDER BY year DESC, month DESC, day DESC LIMIT 1), 5000), " + i + ')');
            ggVar2.mo11230b("DROP TABLE dailygoal");
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migration activitySetting success");
        } catch (java.lang.Exception e4) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local5.mo33255d(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "Migration activitySetting fail " + e4);
            ggVar2.mo11230b("DROP TABLE IF EXISTS dailygoal");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS activitySettings (currentStepGoal INTEGER NOT NULL DEFAULT 5000, currentCaloriesGoal INTEGER NOT NULL DEFAULT " + i + ", currentActiveTimeGoal INTEGER NOT NULL DEFAULT 30, id INTEGER PRIMARY KEY ASC NOT NULL)");
        }
        ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS workout_session (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, deviceSerialNumber TEXT, step TEXT, calorie TEXT, distance TEXT, heartRate TEXT, speed TEXT, location TEXT, states TEXT NOT NULL DEFAULT '', sourceType TEXT, workoutType TEXT, timezoneOffset INTEGER NOT NULL, duration INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
        ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS fitness_data (startTime TEXT PRIMARY KEY NOT NULL, endTime TEXT NOT NULL, syncTime TEXT NOT NULL, timezoneOffsetInSecond INTEGER NOT NULL, serialNumber TEXT NOT NULL DEFAULT '', step TEXT NOT NULL, activeMinute TEXT NOT NULL, calorie TEXT NOT NULL, distance TEXT NOT NULL, stress TEXT, resting TEXT NOT NULL DEFAULT '', heartRate TEXT, sleeps TEXT NOT NULL DEFAULT '', workouts TEXT NOT NULL DEFAULT '')");
        ggVar2.mo11230b("CREATE TABLE activityRecommendedGoals (recommendedStepsGoal INTEGER NOT NULL, recommendedCaloriesGoal INTEGER NOT NULL, recommendedActiveTimeGoal INTEGER NOT NULL, id INTEGER PRIMARY KEY ASC NOT NULL)");
        ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS heart_rate_sample (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, average REAL NOT NULL, timezoneOffset INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
        ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS daily_heart_rate_summary (average REAL NOT NULL, date TEXT PRIMARY KEY NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT)");
        try {
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS activity_statistic (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, activeTimeBestDay TEXT, activeTimeBestStreak TEXT, caloriesBestDay TEXT, caloriesBestStreak TEXT, stepsBestDay TEXT, stepsBestStreak TEXT, totalActiveTime INTEGER NOT NULL, totalCalories REAL NOT NULL, totalDays INTEGER NOT NULL, totalDistance REAL NOT NULL, totalSteps INTEGER NOT NULL, totalIntensityDistInStep TEXT NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
            ggVar2.mo11230b("CREATE TABLE IF NOT EXISTS activity_sample (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '', timeZoneOffsetInSecond INTEGER NOT NULL, sourceId TEXT NOT NULL DEFAULT '', syncTime INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
        } catch (java.lang.Exception e5) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local6.mo33256e(com.portfolio.platform.data.source.local.fitness.FitnessDatabase.TAG, "MIGRATION_FROM_4_TO_13 - ActivityStatistic -- e=" + e5);
            e5.printStackTrace();
        }
        ggVar.mo11236u();
        ggVar.mo11237v();
    }
}
