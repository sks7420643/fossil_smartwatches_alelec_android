package com.portfolio.platform.data.source;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UAppSystemVersionRepository_Factory implements Factory<UAppSystemVersionRepository> {
    @DexIgnore
    public /* final */ Provider<UAppSystemVersionDataSource> uAppSystemVersionDataSourceProvider;

    @DexIgnore
    public UAppSystemVersionRepository_Factory(Provider<UAppSystemVersionDataSource> provider) {
        this.uAppSystemVersionDataSourceProvider = provider;
    }

    @DexIgnore
    public static UAppSystemVersionRepository_Factory create(Provider<UAppSystemVersionDataSource> provider) {
        return new UAppSystemVersionRepository_Factory(provider);
    }

    @DexIgnore
    public static UAppSystemVersionRepository newUAppSystemVersionRepository(UAppSystemVersionDataSource uAppSystemVersionDataSource) {
        return new UAppSystemVersionRepository(uAppSystemVersionDataSource);
    }

    @DexIgnore
    public static UAppSystemVersionRepository provideInstance(Provider<UAppSystemVersionDataSource> provider) {
        return new UAppSystemVersionRepository(provider.get());
    }

    @DexIgnore
    public UAppSystemVersionRepository get() {
        return provideInstance(this.uAppSystemVersionDataSourceProvider);
    }
}
