package com.portfolio.platform.data.source.local.diana.workout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1", mo27670f = "WorkoutSessionLocalDataSource.kt", mo27671l = {82, 85, 90}, mo27672m = "invokeSuspend")
public final class WorkoutSessionLocalDataSource$loadData$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21118p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1$1", mo27670f = "WorkoutSessionLocalDataSource.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1$1 */
    public static final class C58161 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21119p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C58161(com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1 workoutSessionLocalDataSource$loadData$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = workoutSessionLocalDataSource$loadData$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1.C58161 r0 = new com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1.C58161(this.this$0, yb4);
            r0.f21119p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1.C58161) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                this.this$0.$helperCallback.mo39588a();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1$2")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1$2", mo27670f = "WorkoutSessionLocalDataSource.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1$2 */
    public static final class C58172 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.qo2 $data;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21120p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C58172(com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1 workoutSessionLocalDataSource$loadData$1, com.fossil.blesdk.obfuscated.qo2 qo2, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = workoutSessionLocalDataSource$loadData$1;
            this.$data = qo2;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1.C58172 r0 = new com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1.C58172(this.this$0, this.$data, yb4);
            r0.f21120p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1.C58172) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                if (((com.fossil.blesdk.obfuscated.po2) this.$data).mo30179d() != null) {
                    this.this$0.$helperCallback.mo39589a(((com.fossil.blesdk.obfuscated.po2) this.$data).mo30179d());
                } else if (((com.fossil.blesdk.obfuscated.po2) this.$data).mo30178c() != null) {
                    com.portfolio.platform.data.model.ServerError c = ((com.fossil.blesdk.obfuscated.po2) this.$data).mo30178c();
                    com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a aVar = this.this$0.$helperCallback;
                    java.lang.String userMessage = c.getUserMessage();
                    if (userMessage == null) {
                        userMessage = c.getMessage();
                    }
                    if (userMessage == null) {
                        userMessage = "";
                    }
                    aVar.mo39589a(new java.lang.Throwable(userMessage));
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionLocalDataSource$loadData$1(com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource workoutSessionLocalDataSource, int i, com.portfolio.platform.helper.PagingRequestHelper.C5952b.C5953a aVar, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = workoutSessionLocalDataSource;
        this.$offset = i;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1 workoutSessionLocalDataSource$loadData$1 = new com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1(this.this$0, this.$offset, this.$helperCallback, yb4);
        workoutSessionLocalDataSource$loadData$1.f21118p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return workoutSessionLocalDataSource$loadData$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> list;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.lang.Object obj2;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f21118p$;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tag = com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource.Companion.getTAG();
            local.mo33255d(tag, "loadData currentDate = " + this.this$0.currentDate + ", offset=" + this.$offset);
            list = this.this$0.fitnessDataDao.getFitnessData(this.this$0.currentDate, this.this$0.currentDate);
            if (list.isEmpty()) {
                com.portfolio.platform.data.source.WorkoutSessionRepository access$getWorkoutSessionRepository$p = this.this$0.workoutSessionRepository;
                java.util.Date access$getCurrentDate$p = this.this$0.currentDate;
                java.util.Date access$getCurrentDate$p2 = this.this$0.currentDate;
                int i2 = this.$offset;
                this.L$0 = zg42;
                this.L$1 = list;
                this.label = 1;
                obj2 = com.portfolio.platform.data.source.WorkoutSessionRepository.fetchWorkoutSessions$default(access$getWorkoutSessionRepository$p, access$getCurrentDate$p, access$getCurrentDate$p2, i2, 0, this, 8, (java.lang.Object) null);
                if (obj2 == a) {
                    return a;
                }
                zg4 = zg42;
            } else {
                this.$helperCallback.mo39588a();
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            list = (java.util.List) this.L$1;
            obj2 = obj;
        } else if (i == 2 || i == 3) {
            com.fossil.blesdk.obfuscated.qo2 qo2 = (com.fossil.blesdk.obfuscated.qo2) this.L$2;
            java.util.List list2 = (java.util.List) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg43 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.qo2 qo22 = (com.fossil.blesdk.obfuscated.qo2) obj2;
        if (qo22 instanceof com.fossil.blesdk.obfuscated.ro2) {
            com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource workoutSessionLocalDataSource = this.this$0;
            workoutSessionLocalDataSource.mOffset = workoutSessionLocalDataSource.mOffset + 100;
            com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
            com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1.C58161 r4 = new com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1.C58161(this, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = list;
            this.L$2 = qo22;
            this.label = 2;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r4, this) == a) {
                return a;
            }
        } else if (qo22 instanceof com.fossil.blesdk.obfuscated.po2) {
            com.fossil.blesdk.obfuscated.pi4 c2 = com.fossil.blesdk.obfuscated.nh4.m25693c();
            com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1.C58172 r42 = new com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1.C58172(this, qo22, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = list;
            this.L$2 = qo22;
            this.label = 3;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(c2, r42, this) == a) {
                return a;
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
