package com.portfolio.platform.data.source;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.UAppSystemVersionModel;
import com.portfolio.platform.data.source.scope.Local;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class UAppSystemVersionRepository implements UAppSystemVersionDataSource {
    @DexIgnore
    public /* final */ int M_DEFAULT_VERSION; // = 255;
    @DexIgnore
    public /* final */ String TAG; // = UAppSystemVersionRepository.class.getSimpleName();
    @DexIgnore
    public /* final */ List<UAppSystemVersionRepositoryObserver> mObservers; // = new CopyOnWriteArrayList();
    @DexIgnore
    public /* final */ UAppSystemVersionDataSource mUAppSystemVersionDataSource;

    @DexIgnore
    public interface UAppSystemVersionRepositoryObserver {
        @DexIgnore
        void onUAppSystemVersionChanged(UAppSystemVersionModel uAppSystemVersionModel);
    }

    @DexIgnore
    public UAppSystemVersionRepository(@Local UAppSystemVersionDataSource uAppSystemVersionDataSource) {
        this.mUAppSystemVersionDataSource = uAppSystemVersionDataSource;
    }

    @DexIgnore
    private void notifyUAppSystemVersionChanged(UAppSystemVersionModel uAppSystemVersionModel) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, "Inside .notifyActiveDeviceChanged, observerList=" + this.mObservers.size() + ", observerList=" + this.mObservers);
        for (UAppSystemVersionRepositoryObserver onUAppSystemVersionChanged : this.mObservers) {
            onUAppSystemVersionChanged.onUAppSystemVersionChanged(uAppSystemVersionModel);
        }
    }

    @DexIgnore
    public void addContentObserver(UAppSystemVersionRepositoryObserver uAppSystemVersionRepositoryObserver) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, "Inside .addSavedPresetObserverobserver= " + uAppSystemVersionRepositoryObserver + ", isContains=" + this.mObservers.contains(uAppSystemVersionRepositoryObserver) + ", observer size=" + this.mObservers.size() + ", observerList=" + this.mObservers);
        if (!this.mObservers.contains(uAppSystemVersionRepositoryObserver)) {
            this.mObservers.add(uAppSystemVersionRepositoryObserver);
        }
    }

    @DexIgnore
    public void addOrUpdateUAppSystemVersionModel(UAppSystemVersionModel uAppSystemVersionModel) {
        UAppSystemVersionModel uAppSystemVersionModel2 = this.mUAppSystemVersionDataSource.getUAppSystemVersionModel(uAppSystemVersionModel.getDeviceId());
        if (uAppSystemVersionModel2 == null) {
            uAppSystemVersionModel.setPinType(2);
        } else if (!(uAppSystemVersionModel2.getMajorVersion() == uAppSystemVersionModel.getMajorVersion() && uAppSystemVersionModel2.getMinorVersion() == uAppSystemVersionModel.getMinorVersion())) {
            uAppSystemVersionModel.setPinType(2);
        }
        if (uAppSystemVersionModel.getMajorVersion() == 255 && uAppSystemVersionModel.getMinorVersion() == 255) {
            uAppSystemVersionModel.setPinType(0);
        }
        this.mUAppSystemVersionDataSource.addOrUpdateUAppSystemVersionModel(uAppSystemVersionModel);
        if (uAppSystemVersionModel2 == null || !uAppSystemVersionModel2.equals(uAppSystemVersionModel)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.d(str, "Inside .addOrUpdateUAppSystemVersionModel, data has changed, currentUAppSystemVersionModel=" + uAppSystemVersionModel.toString());
            notifyUAppSystemVersionChanged(uAppSystemVersionModel);
        }
    }

    @DexIgnore
    public UAppSystemVersionModel getUAppSystemVersionModel(String str) {
        return this.mUAppSystemVersionDataSource.getUAppSystemVersionModel(str);
    }

    @DexIgnore
    public void removeContentObserver(UAppSystemVersionRepositoryObserver uAppSystemVersionRepositoryObserver) {
        FLogger.INSTANCE.getLocal().d(this.TAG, "Inside .removeSavedPresetObserver");
        if (this.mObservers.contains(uAppSystemVersionRepositoryObserver)) {
            this.mObservers.remove(uAppSystemVersionRepositoryObserver);
        }
    }
}
