package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SleepSessionsRepository$getSleepSessionList$1<I, O> implements com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSessionsRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$1$1")
    /* renamed from: com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$1$1 */
    public static final class C57131 extends com.portfolio.platform.util.NetworkBoundResource<java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession>, com.fossil.blesdk.obfuscated.xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.Pair $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $offset;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$1 this$0;

        @DexIgnore
        public C57131(com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$1 sleepSessionsRepository$getSleepSessionList$1, kotlin.jvm.internal.Ref$IntRef ref$IntRef, int i, kotlin.Pair pair) {
            this.this$0 = sleepSessionsRepository$getSleepSessionList$1;
            this.$offset = ref$IntRef;
            this.$limit = i;
            this.$downloadingDate = pair;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
            if (r0 != null) goto L_0x0019;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
            if (r0 != null) goto L_0x0033;
         */
        @DexIgnore
        public java.lang.Object createCall(com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.fossil.blesdk.obfuscated.xz1>> yb4) {
            java.util.Date date;
            java.util.Date date2;
            com.portfolio.platform.data.source.remote.ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
            kotlin.Pair pair = this.$downloadingDate;
            if (pair != null) {
                date = (java.util.Date) pair.getFirst();
            }
            date = this.this$0.$startDate;
            java.lang.String e = com.fossil.blesdk.obfuscated.rk2.m27397e(date);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            kotlin.Pair pair2 = this.$downloadingDate;
            if (pair2 != null) {
                date2 = (java.util.Date) pair2.getSecond();
            }
            date2 = this.this$0.$endDate;
            java.lang.String e2 = com.fossil.blesdk.obfuscated.rk2.m27397e(date2);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiService$p.getSleepSessions(e, e2, this.$offset.element, this.$limit, yb4);
        }

        @DexIgnore
        public androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession>> loadFromDb() {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("getSleepSessionList: startMilli = ");
            java.util.Date date = this.this$0.$startDate;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_START_DATE);
            sb.append(date.getTime());
            sb.append(", endMilli = ");
            java.util.Date date2 = this.this$0.$endDate;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date2, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_END_DATE);
            sb.append(date2.getTime());
            local.mo33255d(tAG$app_fossilRelease, sb.toString());
            com.portfolio.platform.data.source.local.sleep.SleepDao access$getMSleepDao$p = this.this$0.this$0.mSleepDao;
            java.util.Date date3 = this.this$0.$startDate;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date3, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_START_DATE);
            long time = date3.getTime();
            java.util.Date date4 = this.this$0.$endDate;
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date4, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_END_DATE);
            return access$getMSleepDao$p.getSleepSessionsLiveData(time, date4.getTime());
        }

        @DexIgnore
        public void onFetchFailed(java.lang.Throwable th) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.SleepSessionsRepository.Companion.getTAG$app_fossilRelease(), "getSleepSessionList onFetchFailed");
        }

        @DexIgnore
        public boolean processContinueFetching(com.fossil.blesdk.obfuscated.xz1 xz1) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(xz1, "item");
            com.fossil.blesdk.obfuscated.rz1 rz1 = new com.fossil.blesdk.obfuscated.rz1();
            rz1.mo15797a(org.joda.time.DateTime.class, new com.portfolio.platform.helper.GsonConvertDateTime());
            rz1.mo15797a(java.util.Date.class, new com.portfolio.platform.helper.GsonConverterShortDate());
            com.portfolio.platform.data.model.Range range = ((com.portfolio.platform.data.source.remote.ApiResponse) rz1.mo15799a().mo23094a(xz1.toString(), new com.portfolio.platform.data.source.C5714x9289ab2d().getType())).get_range();
            if (range == null || !range.isHasNext()) {
                return false;
            }
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.SleepSessionsRepository.Companion.getTAG$app_fossilRelease(), "getSleepSessionList getActivityList processContinueFetching hasNext");
            this.$offset.element += this.$limit;
            return true;
        }

        @DexIgnore
        public void saveCallResult(com.fossil.blesdk.obfuscated.xz1 xz1) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(xz1, "item");
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
            local.mo33255d(tAG$app_fossilRelease, "getSleepSessionList saveCallResult onResponse: response = " + xz1);
            java.util.ArrayList arrayList = new java.util.ArrayList();
            try {
                com.fossil.blesdk.obfuscated.rz1 rz1 = new com.fossil.blesdk.obfuscated.rz1();
                rz1.mo15797a(org.joda.time.DateTime.class, new com.portfolio.platform.helper.GsonConvertDateTime());
                rz1.mo15797a(java.util.Date.class, new com.portfolio.platform.helper.GsonConverterShortDate());
                com.portfolio.platform.data.source.remote.ApiResponse apiResponse = (com.portfolio.platform.data.source.remote.ApiResponse) rz1.mo15799a().mo23094a(xz1.toString(), new com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$1$1$saveCallResult$1().getType());
                if (apiResponse != null) {
                    java.util.List<com.portfolio.platform.response.sleep.SleepSessionParse> list = apiResponse.get_items();
                    if (list != null) {
                        for (com.portfolio.platform.response.sleep.SleepSessionParse mfSleepSessionBySleepSessionParse : list) {
                            arrayList.add(mfSleepSessionBySleepSessionParse.getMfSleepSessionBySleepSessionParse());
                        }
                    }
                }
                this.this$0.this$0.insert$app_fossilRelease(arrayList);
            } catch (java.lang.Exception e) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String tAG$app_fossilRelease2 = com.portfolio.platform.data.source.SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append("getSleepSessionList saveCallResult exception=");
                e.printStackTrace();
                sb.append(com.fossil.blesdk.obfuscated.qa4.f17909a);
                local2.mo33256e(tAG$app_fossilRelease2, sb.toString());
            }
        }

        @DexIgnore
        public boolean shouldFetch(java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession> list) {
            return this.this$0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public SleepSessionsRepository$getSleepSessionList$1(com.portfolio.platform.data.source.SleepSessionsRepository sleepSessionsRepository, java.util.Date date, java.util.Date date2, boolean z) {
        this.this$0 = sleepSessionsRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public final androidx.lifecycle.LiveData<com.fossil.blesdk.obfuscated.os3<java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession>>> apply(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> list) {
        kotlin.jvm.internal.Ref$IntRef ref$IntRef = new kotlin.jvm.internal.Ref$IntRef();
        ref$IntRef.element = 0;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) list, "fitnessDataList");
        java.util.Date date = this.$startDate;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_START_DATE);
        java.util.Date date2 = this.$endDate;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) date2, com.fossil.wearables.fsl.goaltracking.GoalPhase.COLUMN_END_DATE);
        return new com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$1.C57131(this, ref$IntRef, 300, com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt.calculateRangeDownload(list, date, date2)).asLiveData();
    }
}
