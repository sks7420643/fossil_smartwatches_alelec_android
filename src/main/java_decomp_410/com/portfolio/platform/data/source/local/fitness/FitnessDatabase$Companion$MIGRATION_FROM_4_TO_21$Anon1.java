package com.portfolio.platform.data.source.local.fitness;

import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.mk2;
import com.fossil.blesdk.obfuscated.yf;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.FitnessMovementType;
import com.portfolio.platform.enums.FitnessSourceType;
import com.portfolio.platform.enums.Gender;
import java.util.TimeZone;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FitnessDatabase$Companion$MIGRATION_FROM_4_TO_21$Anon1 extends yf {
    @DexIgnore
    public FitnessDatabase$Companion$MIGRATION_FROM_4_TO_21$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    public void migrate(gg ggVar) {
        int i;
        gg ggVar2 = ggVar;
        kd4.b(ggVar2, "database");
        FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migration 4 to 21 start");
        ggVar.s();
        DateTime dateTime = new DateTime();
        TimeZone timeZone = dateTime.getZone().toTimeZone();
        int year = dateTime.getYear();
        int monthOfYear = dateTime.getMonthOfYear();
        int dayOfMonth = dateTime.getDayOfMonth();
        kd4.a((Object) timeZone, "timeZone");
        String id = timeZone.getID();
        int dSTSavings = timeZone.inDaylightTime(dateTime.toDate()) ? timeZone.getDSTSavings() : 0;
        MFUser b = dn2.p.a().n().b();
        if (b == null || b.getWeightInGrams() <= 0 || b.getHeightInCentimeters() <= 0) {
            i = ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL;
        } else {
            int heightInCentimeters = b.getHeightInCentimeters();
            Gender gender = b.getGender();
            kd4.a((Object) gender, "user.gender");
            i = mk2.a.a(MFUser.getAge(b.getBirthday()), b.getWeightInGrams() / 1000, heightInCentimeters, gender);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(FitnessDatabase.TAG, "Migrate with calories goal " + i);
        try {
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate sample raw table");
            ggVar2.b("CREATE TABLE sampleraw_new (id TEXT PRIMARY KEY NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, sourceId TEXT NOT NULL DEFAULT '', movementTypeValue TEXT, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, sourceTypeValue TEXT NOT NULL DEFAULT '" + FitnessSourceType.Device.getValue() + "', pinType INTEGER NOT NULL DEFAULT 1, uaPinType INTEGER NOT NULL DEFAULT 1, timeZoneID TEXT, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '')");
            ggVar2.b("INSERT INTO sampleraw_new (id, startTime, endTime, sourceId, movementTypeValue, steps, calories, distance, sourceTypeValue, pinType, uaPinType, timeZoneID) SELECT id, substr(startTime, 1, 23), substr(endTime, 1, 23), sourceId, movementTypeValue, steps, calories, distance, sourceTypeValue, pinType, uaPinType, timeZoneID  FROM sampleraw");
            ggVar2.b("DROP TABLE sampleraw");
            ggVar2.b("ALTER TABLE sampleraw_new RENAME TO sampleraw");
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate sample raw table success");
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(FitnessDatabase.TAG, "Migrate sample raw table fail " + e);
            ggVar2.b("DROP TABLE IF EXISTS sampleraw_new");
            ggVar2.b("DROP TABLE IF EXISTS sampleraw");
            ggVar2.b("CREATE TABLE IF NOT EXISTS sampleraw (id TEXT PRIMARY KEY NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, sourceId TEXT NOT NULL DEFAULT '', movementTypeValue TEXT NOT NULL DEFAULT '" + FitnessMovementType.WALKING.getValue() + "', steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, sourceTypeValue TEXT NOT NULL DEFAULT '" + FitnessSourceType.Device.getValue() + "', pinType INTEGER NOT NULL DEFAULT 1, uaPinType INTEGER NOT NULL DEFAULT 1, timeZoneID TEXT, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '')");
        }
        try {
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate intensity data for sampleraw");
            ggVar2.b("CREATE TABLE intensities (min INTEGER NOT NULL, max INTEGER NOT NULL, intensity TEXT NOT NULL DEFAULT '')");
            ggVar2.b("INSERT INTO intensities (min, max, intensity) VALUES (0, 70, 'light')");
            ggVar2.b("INSERT INTO intensities (min, max, intensity) VALUES (70, 140, 'moderate')");
            ggVar2.b("INSERT INTO intensities (min, max, intensity) VALUES (140, 2147483647, 'intense')");
            ggVar2.b("UPDATE sampleraw SET intensityDistInSteps=( SELECT '{\"' || intensity || '\":' || sampleraw.steps || '}' FROM intensities WHERE sampleraw.steps >= intensities.min AND sampleraw.steps < intensities.max)");
            ggVar2.b("DROP TABLE intensities");
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate intensity data for sampleraw success");
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d(FitnessDatabase.TAG, "Migrate intensity data for sampleraw " + e2);
            ggVar2.b("DROP TABLE IF EXISTS intensities");
        }
        try {
            ggVar2.b("CREATE TABLE sampleday_new (year INTEGER NOT NULL, month INTEGER NOT NULL, day INTEGER NOT NULL, timezoneName TEXT, dstOffset INTEGER, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, intensities TEXT NOT NULL DEFAULT '', stepGoal INTEGER NOT NULL DEFAULT 0, caloriesGoal INTEGER NOT NULL DEFAULT " + i + ", activeTimeGoal INTEGER NOT NULL DEFAULT 0, createdAt INTEGER, updatedAt INTEGER, activeTime INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 1, PRIMARY KEY (year, month, day))");
            ggVar2.b("INSERT INTO sampleday_new (year, month, day, timezoneName, dstOffset, steps, calories, distance, createdAt, updatedAt, pinType) SELECT year, month, day, timezoneName, dstOffset, steps, calories, distance, createdAt, updatedAt, pinType  FROM sampleday");
            ggVar2.b("DROP TABLE sampleday");
            ggVar2.b("ALTER TABLE sampleday_new RENAME TO sampleday");
            ggVar2.b("INSERT OR IGNORE INTO sampleday (year, month, day, timezoneName, dstOffset, steps, calories, distance, intensities, stepGoal, caloriesGoal, activeTimeGoal, createdAt, updatedAt, activeTime, pinType)" + " VALUES(" + year + ", " + monthOfYear + ", " + dayOfMonth + ", '" + id + "', " + dSTSavings + ", 0.0, 0.0, 0.0, '[0,0,0]', 5000, " + i + ", 30, " + System.currentTimeMillis() + ", " + System.currentTimeMillis() + ", 0, 1)");
            ggVar2.b("UPDATE sampleday SET stepGoal=(SELECT steps FROM dailygoal) WHERE sampleday.year = year AND sampleday.month = month AND sampleday.day = day");
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate sample day success");
        } catch (Exception e3) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d(FitnessDatabase.TAG, "Migrate sample day fail " + e3);
            ggVar2.b("DROP TABLE IF EXISTS sampleday_new");
            ggVar2.b("DROP TABLE IF EXISTS sampleday");
            ggVar2.b("CREATE TABLE IF NOT EXISTS sampleday (year INTEGER NOT NULL, month INTEGER NOT NULL, day INTEGER NOT NULL, timezoneName TEXT, dstOffset INTEGER, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, intensities TEXT NOT NULL DEFAULT '', stepGoal INTEGER NOT NULL DEFAULT 0, caloriesGoal INTEGER NOT NULL DEFAULT 0, activeTimeGoal INTEGER NOT NULL DEFAULT 0, createdAt INTEGER, updatedAt INTEGER, activeTime INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 1, PRIMARY KEY (year, month, day))");
        }
        try {
            ggVar2.b("CREATE TABLE activitySettings (currentStepGoal INTEGER NOT NULL DEFAULT 5000, currentCaloriesGoal INTEGER NOT NULL DEFAULT " + i + ", currentActiveTimeGoal INTEGER NOT NULL DEFAULT 30, id INTEGER PRIMARY KEY ASC NOT NULL)");
            ggVar2.b("INSERT INTO activitySettings (currentStepGoal, currentCaloriesGoal) VALUES (IFNULL((SELECT steps FROM dailygoal ORDER BY year DESC, month DESC, day DESC LIMIT 1), 5000), " + i + ')');
            ggVar2.b("DROP TABLE dailygoal");
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migration activitySetting success");
        } catch (Exception e4) {
            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            local5.d(FitnessDatabase.TAG, "Migration activitySetting fail " + e4);
            ggVar2.b("DROP TABLE IF EXISTS dailygoal");
            ggVar2.b("CREATE TABLE IF NOT EXISTS activitySettings (currentStepGoal INTEGER NOT NULL DEFAULT 5000, currentCaloriesGoal INTEGER NOT NULL DEFAULT " + i + ", currentActiveTimeGoal INTEGER NOT NULL DEFAULT 30, id INTEGER PRIMARY KEY ASC NOT NULL)");
        }
        ggVar2.b("CREATE TABLE IF NOT EXISTS workout_session (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, deviceSerialNumber TEXT, step TEXT, calorie TEXT, distance TEXT, heartRate TEXT, speed TEXT, location TEXT, states TEXT NOT NULL DEFAULT '', sourceType TEXT, workoutType TEXT, timezoneOffset INTEGER NOT NULL, duration INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
        ggVar2.b("CREATE TABLE IF NOT EXISTS fitness_data (startTime TEXT PRIMARY KEY NOT NULL, endTime TEXT NOT NULL, syncTime TEXT NOT NULL, timezoneOffsetInSecond INTEGER NOT NULL, serialNumber TEXT NOT NULL DEFAULT '', step TEXT NOT NULL, activeMinute TEXT NOT NULL, calorie TEXT NOT NULL, distance TEXT NOT NULL, stress TEXT, resting TEXT NOT NULL DEFAULT '', heartRate TEXT, sleeps TEXT NOT NULL DEFAULT '', workouts TEXT NOT NULL DEFAULT '')");
        ggVar2.b("CREATE TABLE activityRecommendedGoals (recommendedStepsGoal INTEGER NOT NULL, recommendedCaloriesGoal INTEGER NOT NULL, recommendedActiveTimeGoal INTEGER NOT NULL, id INTEGER PRIMARY KEY ASC NOT NULL)");
        ggVar2.b("CREATE TABLE IF NOT EXISTS heart_rate_sample (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, average REAL NOT NULL, timezoneOffset INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
        ggVar2.b("CREATE TABLE IF NOT EXISTS daily_heart_rate_summary (average REAL NOT NULL, date TEXT PRIMARY KEY NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT)");
        try {
            ggVar2.b("CREATE TABLE IF NOT EXISTS activity_statistic (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, activeTimeBestDay TEXT, activeTimeBestStreak TEXT, caloriesBestDay TEXT, caloriesBestStreak TEXT, stepsBestDay TEXT, stepsBestStreak TEXT, totalActiveTime INTEGER NOT NULL, totalCalories REAL NOT NULL, totalDays INTEGER NOT NULL, totalDistance REAL NOT NULL, totalSteps INTEGER NOT NULL, totalIntensityDistInStep TEXT NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
            ggVar2.b("CREATE TABLE IF NOT EXISTS activity_sample (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '', timeZoneOffsetInSecond INTEGER NOT NULL, sourceId TEXT NOT NULL DEFAULT '', syncTime INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
        } catch (Exception e5) {
            ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
            local6.e(FitnessDatabase.TAG, "MIGRATION_FROM_4_TO_13 - ActivityStatistic -- e=" + e5);
            e5.printStackTrace();
        }
        ggVar.u();
        ggVar.v();
    }
}
