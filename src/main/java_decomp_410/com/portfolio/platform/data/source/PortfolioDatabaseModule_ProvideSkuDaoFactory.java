package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.n44;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PortfolioDatabaseModule_ProvideSkuDaoFactory implements Factory<SkuDao> {
    @DexIgnore
    public /* final */ Provider<DeviceDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideSkuDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DeviceDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideSkuDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DeviceDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideSkuDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static SkuDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DeviceDatabase> provider) {
        return proxyProvideSkuDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static SkuDao proxyProvideSkuDao(PortfolioDatabaseModule portfolioDatabaseModule, DeviceDatabase deviceDatabase) {
        SkuDao provideSkuDao = portfolioDatabaseModule.provideSkuDao(deviceDatabase);
        n44.a(provideSkuDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideSkuDao;
    }

    @DexIgnore
    public SkuDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
