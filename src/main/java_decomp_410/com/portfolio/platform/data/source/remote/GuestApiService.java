package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.gt4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.us4;
import com.fossil.blesdk.obfuscated.xz1;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.Firmware;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface GuestApiService {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class DefaultImpls {
        @DexIgnore
        public static /* synthetic */ Object getFirmwares$default(GuestApiService guestApiService, String str, String str2, String str3, yb4 yb4, int i, Object obj) {
            if (obj == null) {
                if ((i & 4) != 0) {
                    str3 = "android";
                }
                return guestApiService.getFirmwares(str, str2, str3, yb4);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getFirmwares");
        }
    }

    @DexIgnore
    @us4("firmwares")
    Object getFirmwares(@gt4("appVersion") String str, @gt4("deviceModel") String str2, @gt4("os") String str3, yb4<? super qr4<ApiResponse<Firmware>>> yb4);

    @DexIgnore
    @us4("assets/app-localizations")
    Object getLocalizations(@gt4("metadata.appVersion") String str, @gt4("metadata.platform") String str2, yb4<? super qr4<ApiResponse<xz1>>> yb4);
}
