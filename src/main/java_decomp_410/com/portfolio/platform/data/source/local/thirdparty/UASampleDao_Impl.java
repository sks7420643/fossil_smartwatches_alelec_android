package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.portfolio.platform.data.model.thirdparty.ua.UASample;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UASampleDao_Impl implements UASampleDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ kf __deletionAdapterOfUASample;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfUASample;
    @DexIgnore
    public /* final */ wf __preparedStmtOfClearAll;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<UASample> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `uaSample`(`id`,`step`,`distance`,`calorie`,`time`) VALUES (nullif(?, 0),?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, UASample uASample) {
            kgVar.b(1, (long) uASample.getId());
            kgVar.b(2, (long) uASample.getStep());
            kgVar.a(3, uASample.getDistance());
            kgVar.a(4, uASample.getCalorie());
            kgVar.b(5, uASample.getTime());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends kf<UASample> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `uaSample` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(kg kgVar, UASample uASample) {
            kgVar.b(1, (long) uASample.getId());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends wf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM uaSample";
        }
    }

    @DexIgnore
    public UASampleDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfUASample = new Anon1(roomDatabase);
        this.__deletionAdapterOfUASample = new Anon2(roomDatabase);
        this.__preparedStmtOfClearAll = new Anon3(roomDatabase);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public void deleteListUASample(List<UASample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfUASample.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<UASample> getAllUASample() {
        uf b = uf.b("SELECT * FROM uaSample", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, "step");
            int b4 = ag.b(a, "distance");
            int b5 = ag.b(a, "calorie");
            int b6 = ag.b(a, LogBuilder.KEY_TIME);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                UASample uASample = new UASample(a.getInt(b3), a.getDouble(b4), a.getDouble(b5), a.getLong(b6));
                uASample.setId(a.getInt(b2));
                arrayList.add(uASample);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertListUASample(List<UASample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfUASample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertUASample(UASample uASample) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfUASample.insert(uASample);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
