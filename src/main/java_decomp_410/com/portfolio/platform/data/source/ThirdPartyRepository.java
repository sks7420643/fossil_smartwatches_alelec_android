package com.portfolio.platform.data.source;

import android.content.Context;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.ap0;
import com.fossil.blesdk.obfuscated.aq0;
import com.fossil.blesdk.obfuscated.bp0;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ee4;
import com.fossil.blesdk.obfuscated.eg4;
import com.fossil.blesdk.obfuscated.ep0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.ge0;
import com.fossil.blesdk.obfuscated.he0;
import com.fossil.blesdk.obfuscated.ic4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ne0;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.ro0;
import com.fossil.blesdk.obfuscated.sn1;
import com.fossil.blesdk.obfuscated.tn1;
import com.fossil.blesdk.obfuscated.ud0;
import com.fossil.blesdk.obfuscated.vb0;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.wn1;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.zk2;
import com.fossil.blesdk.obfuscated.zo0;
import com.fossil.fitness.WorkoutType;
import com.fossil.wearables.fossil.R;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOCalorie;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWODistance;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOStep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.model.thirdparty.ua.UASample;
import com.portfolio.platform.data.model.ua.UAActivityTimeSeries;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import kotlin.jvm.internal.Ref$IntRef;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ThirdPartyRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG; // = "ThirdPartyRepository";
    @DexIgnore
    public ActivitiesRepository mActivitiesRepository;
    @DexIgnore
    public zk2 mGoogleFitHelper;
    @DexIgnore
    public PortfolioApp mPortfolioApp;
    @DexIgnore
    public ThirdPartyDatabase mThirdPartyDatabase;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingThirdPartyDataCallback {
        @DexIgnore
        void onComplete();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = new int[WorkoutType.values().length];

        /*
        static {
            $EnumSwitchMapping$Anon0[WorkoutType.RUNNING.ordinal()] = 1;
            $EnumSwitchMapping$Anon0[WorkoutType.CYCLING.ordinal()] = 2;
            $EnumSwitchMapping$Anon0[WorkoutType.TREADMILL.ordinal()] = 3;
            $EnumSwitchMapping$Anon0[WorkoutType.ELLIPTICAL.ordinal()] = 4;
            $EnumSwitchMapping$Anon0[WorkoutType.WEIGHTS.ordinal()] = 5;
            $EnumSwitchMapping$Anon0[WorkoutType.UNKNOWN.ordinal()] = 6;
        }
        */
    }

    @DexIgnore
    public ThirdPartyRepository(zk2 zk2, ThirdPartyDatabase thirdPartyDatabase, ActivitiesRepository activitiesRepository, PortfolioApp portfolioApp) {
        kd4.b(zk2, "mGoogleFitHelper");
        kd4.b(thirdPartyDatabase, "mThirdPartyDatabase");
        kd4.b(activitiesRepository, "mActivitiesRepository");
        kd4.b(portfolioApp, "mPortfolioApp");
        this.mGoogleFitHelper = zk2;
        this.mThirdPartyDatabase = thirdPartyDatabase;
        this.mActivitiesRepository = activitiesRepository;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    private final List<GFitSleep> convertListMFSleepSessionToListGFitSleep(List<MFSleepSession> list) {
        ArrayList arrayList = new ArrayList();
        for (MFSleepSession next : list) {
            long j = (long) 1000;
            arrayList.add(new GFitSleep(next.getRealSleepMinutes(), ((long) next.getRealStartTime()) * j, j * ((long) next.getRealEndTime())));
        }
        return arrayList;
    }

    @DexIgnore
    private final DataSet createDataSetForActiveMinutes(GFitActiveTime gFitActiveTime, String str) {
        zo0.a aVar = new zo0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.p);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        aVar.a(new ap0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        List<T> h = kb4.h(gFitActiveTime.getActiveTimes());
        wd4 a2 = ee4.a((wd4) ee4.d(0, h.size()), 2);
        int a3 = a2.a();
        int b = a2.b();
        int c = a2.c();
        if (c < 0 ? a3 >= b : a3 <= b) {
            while (true) {
                DataPoint H = a.H();
                H.a(((Number) h.get(a3)).longValue(), ((Number) h.get(a3 + 1)).longValue(), TimeUnit.MILLISECONDS);
                H.a(bp0.h).e("unknown");
                a.a(H);
                if (a3 == b) {
                    break;
                }
                a3 += c;
            }
        }
        kd4.a((Object) a, "dataSetForActiveMinutes");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForCalories(List<GFitSample> list, String str) {
        zo0.a aVar = new zo0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.s);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        aVar.a(new ap0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitSample next : list) {
            float component3 = next.component3();
            long component4 = next.component4();
            long component5 = next.component5();
            DataPoint H = a.H();
            H.a(component4, component5, TimeUnit.MILLISECONDS);
            H.a(bp0.H).a(component3);
            a.a(H);
        }
        kd4.a((Object) a, "dataSetForCalories");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForDistances(List<GFitSample> list, String str) {
        zo0.a aVar = new zo0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.D);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        aVar.a(new ap0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitSample next : list) {
            float component2 = next.component2();
            long component4 = next.component4();
            long component5 = next.component5();
            DataPoint H = a.H();
            H.a(component4, component5, TimeUnit.MILLISECONDS);
            H.a(bp0.u).a(component2);
            a.a(H);
        }
        kd4.a((Object) a, "dataSetForDistances");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForHeartRates(List<GFitHeartRate> list, String str) {
        zo0.a aVar = new zo0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.A);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        aVar.a(new ap0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitHeartRate next : list) {
            float component1 = next.component1();
            long component2 = next.component2();
            long component3 = next.component3();
            DataPoint H = a.H();
            H.a(component2, component3, TimeUnit.MILLISECONDS);
            H.a(bp0.p).a(component1);
            a.a(H);
        }
        kd4.a((Object) a, "dataSet");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForSteps(List<GFitSample> list, String str) {
        zo0.a aVar = new zo0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.i);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        aVar.a(new ap0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitSample next : list) {
            int component1 = next.component1();
            long component4 = next.component4();
            long component5 = next.component5();
            DataPoint H = a.H();
            H.a(component4, component5, TimeUnit.MILLISECONDS);
            H.a(bp0.k).f(component1);
            a.a(H);
        }
        kd4.a((Object) a, "dataSetForSteps");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForWOCalories(List<GFitWOCalorie> list, String str, long j, long j2) {
        zo0.a aVar = new zo0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.s);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        String str2 = str;
        aVar.a(new ap0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitWOCalorie next : list) {
            if (next.getStartTime() >= j && next.getEndTime() <= j2) {
                DataPoint H = a.H();
                H.a(next.getStartTime(), next.getEndTime(), TimeUnit.MILLISECONDS);
                H.a(bp0.H).a(next.getCalorie());
                a.a(H);
            }
        }
        kd4.a((Object) a, "dataSetForCalories");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForWODistances(List<GFitWODistance> list, String str, long j, long j2) {
        zo0.a aVar = new zo0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.D);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        String str2 = str;
        aVar.a(new ap0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitWODistance next : list) {
            if (next.getStartTime() >= j && next.getEndTime() <= j2) {
                DataPoint H = a.H();
                H.a(next.getStartTime(), next.getEndTime(), TimeUnit.MILLISECONDS);
                H.a(bp0.u).a(next.getDistance());
                a.a(H);
            }
        }
        kd4.a((Object) a, "dataSetForDistances");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForWOHeartRates(List<GFitWOHeartRate> list, String str, long j, long j2) {
        zo0.a aVar = new zo0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.A);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        String str2 = str;
        aVar.a(new ap0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitWOHeartRate next : list) {
            if (next.getStartTime() >= j && next.getEndTime() <= j2) {
                DataPoint H = a.H();
                H.a(next.getStartTime(), next.getEndTime(), TimeUnit.MILLISECONDS);
                H.a(bp0.p).a(next.getHeartRate());
                a.a(H);
            }
        }
        kd4.a((Object) a, "dataSet");
        return a;
    }

    @DexIgnore
    private final DataSet createDataSetForWOSteps(List<GFitWOStep> list, String str, long j, long j2) {
        zo0.a aVar = new zo0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.i);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        String str2 = str;
        aVar.a(new ap0(this.mPortfolioApp.getString(R.string.app_name), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitWOStep next : list) {
            if (next.getStartTime() >= j && next.getEndTime() <= j2) {
                DataPoint H = a.H();
                H.a(next.getStartTime(), next.getEndTime(), TimeUnit.MILLISECONDS);
                H.a(bp0.k).f(next.getStep());
                a.a(H);
            }
        }
        kd4.a((Object) a, "dataSetForSteps");
        return a;
    }

    @DexIgnore
    private final aq0 createWorkoutSession(GFitWorkoutSession gFitWorkoutSession, String str) {
        DataSet createDataSetForWOSteps = createDataSetForWOSteps(gFitWorkoutSession.getSteps(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWOCalories = createDataSetForWOCalories(gFitWorkoutSession.getCalories(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWODistances = createDataSetForWODistances(gFitWorkoutSession.getDistances(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWOHeartRates = createDataSetForWOHeartRates(gFitWorkoutSession.getHeartRates(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        WorkoutType workoutType = getWorkoutType(gFitWorkoutSession.getWorkoutType());
        ep0.a aVar = new ep0.a();
        aVar.d(workoutType.name());
        aVar.a(getFitnessActivitiesType(workoutType));
        aVar.b(gFitWorkoutSession.getStartTime(), TimeUnit.MILLISECONDS);
        aVar.a(gFitWorkoutSession.getEndTime(), TimeUnit.MILLISECONDS);
        ep0 a = aVar.a();
        aq0.a aVar2 = new aq0.a();
        aVar2.a(a);
        if (!createDataSetForWOSteps.isEmpty()) {
            aVar2.a(createDataSetForWOSteps);
        }
        if (!createDataSetForWOCalories.isEmpty()) {
            aVar2.a(createDataSetForWOCalories);
        }
        if (!createDataSetForWODistances.isEmpty()) {
            aVar2.a(createDataSetForWODistances);
        }
        if (!createDataSetForWOHeartRates.isEmpty()) {
            aVar2.a(createDataSetForWOHeartRates);
        }
        aq0 a2 = aVar2.a();
        kd4.a((Object) a2, "sessionBuilder.build()");
        return a2;
    }

    @DexIgnore
    private final UAActivityTimeSeries generateTimeSeries(List<UASample> list) {
        UAActivityTimeSeries.Builder builder = new UAActivityTimeSeries.Builder();
        for (UASample next : list) {
            int component1 = next.component1();
            double component2 = next.component2();
            double component3 = next.component3();
            long component4 = next.component4();
            builder.addSteps(component4, component1);
            builder.addDistance(component4, component2);
            builder.addCalories(component4, component3);
        }
        return builder.build();
    }

    @DexIgnore
    private final UAActivityTimeSeries generateTimeSeriesOldFlow(List<SampleRaw> list) {
        UAActivityTimeSeries.Builder builder = new UAActivityTimeSeries.Builder();
        for (SampleRaw next : list) {
            long time = next.getStartTime().getTime() / ((long) 1000);
            builder.addSteps(time, (int) next.getSteps());
            builder.addDistance(time, next.getDistance());
            builder.addCalories(time, next.getCalories());
        }
        return builder.build();
    }

    @DexIgnore
    private final String getFitnessActivitiesType(WorkoutType workoutType) {
        switch (WhenMappings.$EnumSwitchMapping$Anon0[workoutType.ordinal()]) {
            case 1:
                return "running";
            case 2:
                return "biking";
            case 3:
                return "treadmill";
            case 4:
                return "elliptical";
            case 5:
                return "weightlifting";
            case 6:
                return "unknown";
            default:
                return FacebookRequestErrorClassification.KEY_OTHER;
        }
    }

    @DexIgnore
    private final WorkoutType getWorkoutType(int i) {
        if (i < WorkoutType.values().length) {
            return WorkoutType.values()[i];
        }
        return WorkoutType.UNKNOWN;
    }

    @DexIgnore
    public static /* synthetic */ void saveData$default(ThirdPartyRepository thirdPartyRepository, List list, List list2, GFitActiveTime gFitActiveTime, List list3, List list4, List list5, int i, Object obj) {
        if ((i & 1) != 0) {
            list = cb4.a();
        }
        if ((i & 2) != 0) {
            list2 = cb4.a();
        }
        List list6 = list2;
        if ((i & 4) != 0) {
            gFitActiveTime = null;
        }
        GFitActiveTime gFitActiveTime2 = gFitActiveTime;
        if ((i & 8) != 0) {
            list3 = cb4.a();
        }
        List list7 = list3;
        if ((i & 16) != 0) {
            list4 = cb4.a();
        }
        List list8 = list4;
        if ((i & 32) != 0) {
            list5 = cb4.a();
        }
        thirdPartyRepository.saveData(list, list6, gFitActiveTime2, list7, list8, list5);
    }

    @DexIgnore
    public static /* synthetic */ fi4 uploadData$default(ThirdPartyRepository thirdPartyRepository, PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, int i, Object obj) {
        if ((i & 1) != 0) {
            pushPendingThirdPartyDataCallback = null;
        }
        return thirdPartyRepository.uploadData(pushPendingThirdPartyDataCallback);
    }

    @DexIgnore
    public final DataSet createDataSetForSleepData(GFitSleep gFitSleep, ap0 ap0) {
        kd4.b(gFitSleep, "gFitSleep");
        kd4.b(ap0, "device");
        zo0.a aVar = new zo0.a();
        aVar.a((Context) this.mPortfolioApp);
        aVar.a(DataType.p);
        aVar.b(this.mPortfolioApp.getString(R.string.app_name));
        aVar.a(0);
        aVar.a(ap0);
        DataSet a = DataSet.a(aVar.a());
        long startTime = gFitSleep.getStartTime();
        long endTime = gFitSleep.getEndTime();
        DataPoint H = a.H();
        H.a(startTime, endTime, TimeUnit.MILLISECONDS);
        H.a(bp0.h).f(gFitSleep.getSleepMins());
        a.a(H);
        kd4.a((Object) a, "dataSetForSleep");
        return a;
    }

    @DexIgnore
    public final ActivitiesRepository getMActivitiesRepository() {
        return this.mActivitiesRepository;
    }

    @DexIgnore
    public final PortfolioApp getMPortfolioApp() {
        return this.mPortfolioApp;
    }

    @DexIgnore
    public final ThirdPartyDatabase getMThirdPartyDatabase() {
        return this.mThirdPartyDatabase;
    }

    @DexIgnore
    public final void pushPendingData(PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback) {
        kd4.b(pushPendingThirdPartyDataCallback, "pushPendingThirdPartyDataCallback");
        uploadData(pushPendingThirdPartyDataCallback);
    }

    @DexIgnore
    public final void saveData(List<GFitSample> list, List<UASample> list2, GFitActiveTime gFitActiveTime, List<GFitHeartRate> list3, List<GFitWorkoutSession> list4, List<MFSleepSession> list5) {
        List<GFitWorkoutSession> list6 = list4;
        List<MFSleepSession> list7 = list5;
        kd4.b(list, "listGFitSample");
        kd4.b(list2, "listUASample");
        kd4.b(list3, "listGFitHeartRate");
        kd4.b(list6, "listGFitWorkoutSession");
        kd4.b(list7, "listMFSleepSession");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("saveData = listGFitSample=");
        sb.append(list);
        sb.append(", listUASample=");
        sb.append(list2);
        sb.append(", gFitActiveTime=");
        GFitActiveTime gFitActiveTime2 = gFitActiveTime;
        sb.append(gFitActiveTime);
        sb.append(", listGFitHeartRate=");
        sb.append(list3);
        sb.append(", listGFitWorkoutSession=");
        sb.append(list6);
        sb.append(", listMFSleepSession= ");
        sb.append(list7);
        local.d(TAG, sb.toString());
        this.mThirdPartyDatabase.runInTransaction((Runnable) new ThirdPartyRepository$saveData$Anon1(this, list, gFitActiveTime2, list3, list6, list2, list7));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00e5  */
    public final /* synthetic */ Object saveGFitActiveTimeToGoogleFit(List<GFitActiveTime> list, String str, yb4<Object> yb4) {
        Object e;
        eg4 eg4 = new eg4(IntrinsicsKt__IntrinsicsJvmKt.a(yb4), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitActiveTimeToGoogleFit");
        ge0.a aVar = new ge0.a(getMPortfolioApp());
        aVar.a((de0<? extends de0.d.C0010d>) ro0.b, new Scope[0]);
        aVar.a((de0<? extends de0.d.C0010d>) ro0.a, new Scope[0]);
        aVar.a((de0<? extends de0.d.C0010d>) ro0.d, new Scope[0]);
        aVar.a(new String[]{"https://www.googleapis.com/auth/fitness.activity.write"});
        ge0 a = aVar.a();
        ud0 a2 = a.a();
        kd4.a((Object) a2, Constants.RESULT);
        if (a2.L()) {
            kd4.a((Object) a, "googleApiClient");
            if (a.g()) {
                FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitActiveTime to Google Fit");
                int size = list.size();
                Ref$IntRef ref$IntRef = new Ref$IntRef();
                ref$IntRef.element = 0;
                for (GFitActiveTime gFitActiveTime : list) {
                    he0<Status> a3 = ro0.c.a(a, createDataSetForActiveMinutes(gFitActiveTime, str));
                    ge0 ge0 = a;
                    ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r0;
                    int i = size;
                    ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(gFitActiveTime, a, ref$IntRef, size, eg4, this, list, str);
                    a3.a((ne0<? super Status>) thirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                    a = ge0;
                    size = i;
                }
                e = eg4.e();
                if (e == cc4.a()) {
                    ic4.c(yb4);
                }
                return e;
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "Not sending GFitActiveTime to Google Fit");
        FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitActiveTimeToGoogleFit");
        if (eg4.isActive()) {
            Result.a aVar2 = Result.Companion;
            eg4.resumeWith(Result.m3constructorimpl((Object) null));
        }
        e = eg4.e();
        if (e == cc4.a()) {
        }
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00ea  */
    public final /* synthetic */ Object saveGFitHeartRateToGoogleFit(List<GFitHeartRate> list, String str, yb4<Object> yb4) {
        Object e;
        eg4 eg4 = new eg4(IntrinsicsKt__IntrinsicsJvmKt.a(yb4), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitHeartRateToGoogleFit");
        ge0.a aVar = new ge0.a(getMPortfolioApp());
        aVar.a((de0<? extends de0.d.C0010d>) ro0.b, new Scope[0]);
        aVar.a((de0<? extends de0.d.C0010d>) ro0.a, new Scope[0]);
        aVar.a((de0<? extends de0.d.C0010d>) ro0.d, new Scope[0]);
        aVar.a(new Scope("https://www.googleapis.com/auth/fitness.body.write"));
        ge0 a = aVar.a();
        ud0 a2 = a.a();
        kd4.a((Object) a2, Constants.RESULT);
        if (a2.L()) {
            kd4.a((Object) a, "googleApiClient");
            if (a.g()) {
                FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitHeartRate to Google Fit");
                List<List<T>> b = kb4.b(list, 500);
                int size = b.size();
                Ref$IntRef ref$IntRef = new Ref$IntRef();
                ref$IntRef.element = 0;
                for (List list2 : b) {
                    String str2 = str;
                    he0<Status> a3 = ro0.c.a(a, createDataSetForHeartRates(list2, str2));
                    ge0 ge0 = a;
                    ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r0;
                    ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(list2, a, ref$IntRef, size, eg4, this, list, str2);
                    a3.a((ne0<? super Status>) thirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                    List<GFitHeartRate> list3 = list;
                    a = ge0;
                }
                e = eg4.e();
                if (e == cc4.a()) {
                    ic4.c(yb4);
                }
                return e;
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "Not sending GFitHeartRate to Google Fit");
        FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitHeartRateToGoogleFit");
        if (eg4.isActive()) {
            Result.a aVar2 = Result.Companion;
            eg4.resumeWith(Result.m3constructorimpl((Object) null));
        }
        e = eg4.e();
        if (e == cc4.a()) {
        }
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x012e  */
    public final /* synthetic */ Object saveGFitSampleToGoogleFit(List<GFitSample> list, String str, yb4<Object> yb4) {
        Object e;
        ThirdPartyRepository thirdPartyRepository = this;
        String str2 = str;
        eg4 eg4 = new eg4(IntrinsicsKt__IntrinsicsJvmKt.a(yb4), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitSampleToGoogleFit");
        ge0.a aVar = new ge0.a(getMPortfolioApp());
        int i = 0;
        aVar.a((de0<? extends de0.d.C0010d>) ro0.b, new Scope[0]);
        aVar.a((de0<? extends de0.d.C0010d>) ro0.a, new Scope[0]);
        aVar.a((de0<? extends de0.d.C0010d>) ro0.d, new Scope[0]);
        aVar.a(new String[]{"https://www.googleapis.com/auth/fitness.activity.write", "https://www.googleapis.com/auth/fitness.location.write"});
        ge0 a = aVar.a();
        ud0 a2 = a.a();
        kd4.a((Object) a2, Constants.RESULT);
        if (a2.L()) {
            kd4.a((Object) a, "googleApiClient");
            if (a.g()) {
                FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitSample to Google Fit");
                List<List<T>> b = kb4.b(list, 500);
                int size = b.size();
                Ref$IntRef ref$IntRef = new Ref$IntRef();
                ref$IntRef.element = 0;
                for (List list2 : b) {
                    ArrayList arrayList = new ArrayList();
                    DataSet access$createDataSetForSteps = thirdPartyRepository.createDataSetForSteps(list2, str2);
                    DataSet access$createDataSetForDistances = thirdPartyRepository.createDataSetForDistances(list2, str2);
                    DataSet access$createDataSetForCalories = thirdPartyRepository.createDataSetForCalories(list2, str2);
                    arrayList.add(access$createDataSetForSteps);
                    arrayList.add(access$createDataSetForDistances);
                    arrayList.add(access$createDataSetForCalories);
                    int size2 = arrayList.size();
                    Ref$IntRef ref$IntRef2 = new Ref$IntRef();
                    ref$IntRef2.element = i;
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r0;
                        ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(ref$IntRef2, size2, list2, a, ref$IntRef, eg4, size, this, list, str);
                        ro0.c.a(a, (DataSet) it.next()).a(thirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                        List<GFitSample> list3 = list;
                        ref$IntRef2 = ref$IntRef2;
                        list2 = list2;
                        ref$IntRef = ref$IntRef;
                        i = 0;
                    }
                    thirdPartyRepository = this;
                    List<GFitSample> list4 = list;
                }
                e = eg4.e();
                if (e == cc4.a()) {
                    ic4.c(yb4);
                }
                return e;
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "Not sending GFitSample to Google Fit");
        FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitSampleToGoogleFit");
        if (eg4.isActive()) {
            Result.a aVar2 = Result.Companion;
            eg4.resumeWith(Result.m3constructorimpl((Object) null));
        }
        e = eg4.e();
        if (e == cc4.a()) {
        }
        return e;
    }

    @DexIgnore
    public final Object saveGFitSleepDataToGoogleFit(List<GFitSleep> list, String str, yb4<Object> yb4) {
        String str2 = str;
        eg4 eg4 = new eg4(IntrinsicsKt__IntrinsicsJvmKt.a(yb4), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitSleepDataToGoogleFit");
        GoogleSignInAccount a = vb0.a((Context) getMPortfolioApp());
        if (a != null) {
            int size = list.size();
            Ref$IntRef ref$IntRef = new Ref$IntRef();
            ref$IntRef.element = 0;
            String nameBySerial = DeviceIdentityUtils.getNameBySerial(str);
            PortfolioApp mPortfolioApp2 = getMPortfolioApp();
            int i = R.string.app_name;
            ap0 ap0 = new ap0(mPortfolioApp2.getString(R.string.app_name), nameBySerial, str2, 3);
            ArrayList arrayList = new ArrayList();
            for (GFitSleep gFitSleep : list) {
                DataSet createDataSetForSleepData = createDataSetForSleepData(gFitSleep, ap0);
                ep0.a aVar = new ep0.a();
                aVar.c(str2 + new DateTime());
                aVar.d(getMPortfolioApp().getString(i));
                aVar.b("User Sleep");
                aVar.b(gFitSleep.getStartTime(), TimeUnit.MILLISECONDS);
                aVar.a(gFitSleep.getEndTime(), TimeUnit.MILLISECONDS);
                aVar.a("sleep");
                ep0 a2 = aVar.a();
                aq0.a aVar2 = new aq0.a();
                aVar2.a(a2);
                aVar2.a(createDataSetForSleepData);
                ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r0;
                int i2 = size;
                int i3 = size;
                wn1<Void> a3 = ro0.a(getMPortfolioApp(), a).a(aVar2.a());
                ap0 ap02 = ap0;
                ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(gFitSleep, ap0, a, ref$IntRef, arrayList, i2, eg4, this, list, str);
                a3.a((tn1<? super Void>) thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                a3.a((sn1) new ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(ap02, a, ref$IntRef, arrayList, i3, eg4, this, list, str));
                str2 = str;
                size = i3;
                ap0 = ap02;
                i = R.string.app_name;
            }
        } else {
            FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitSleepDataToGoogleFit");
            if (eg4.isActive()) {
                Result.a aVar3 = Result.Companion;
                eg4.resumeWith(Result.m3constructorimpl((Object) null));
            }
        }
        Object e = eg4.e();
        if (e == cc4.a()) {
            ic4.c(yb4);
        }
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0119  */
    public final /* synthetic */ Object saveGFitWorkoutSessionToGoogleFit(List<GFitWorkoutSession> list, String str, yb4<Object> yb4) {
        Object e;
        eg4 eg4 = new eg4(IntrinsicsKt__IntrinsicsJvmKt.a(yb4), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitWorkoutSessionToGoogleFit");
        ge0.a aVar = new ge0.a(getMPortfolioApp());
        aVar.a((de0<? extends de0.d.C0010d>) ro0.b, new Scope[0]);
        aVar.a(new String[]{"https://www.googleapis.com/auth/fitness.activity.write", "https://www.googleapis.com/auth/fitness.location.write", "https://www.googleapis.com/auth/fitness.location.write", "https://www.googleapis.com/auth/fitness.body.write"});
        ge0 a = aVar.a();
        ud0 a2 = a.a();
        kd4.a((Object) a2, Constants.RESULT);
        if (a2.L()) {
            kd4.a((Object) a, "googleApiClient");
            if (a.g()) {
                GoogleSignInAccount a3 = vb0.a((Context) getMPortfolioApp());
                if (a3 != null) {
                    FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitWorkoutSession to Google Fit");
                    int size = list.size();
                    Ref$IntRef ref$IntRef = new Ref$IntRef();
                    ref$IntRef.element = 0;
                    Iterator<T> it = list.iterator();
                    while (it.hasNext()) {
                        GFitWorkoutSession gFitWorkoutSession = (GFitWorkoutSession) it.next();
                        Iterator<T> it2 = it;
                        ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r0;
                        wn1<Void> a4 = ro0.a(getMPortfolioApp(), a3).a(createWorkoutSession(gFitWorkoutSession, str));
                        ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(gFitWorkoutSession, a3, ref$IntRef, size, eg4, this, list, str);
                        a4.a((tn1<? super Void>) thirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                        a4.a((sn1) new ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(a3, ref$IntRef, size, eg4, this, list, str));
                        it = it2;
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
                    FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitWorkoutSessionToGoogleFit");
                    if (eg4.isActive()) {
                        Result.a aVar2 = Result.Companion;
                        eg4.resumeWith(Result.m3constructorimpl((Object) null));
                    }
                }
                e = eg4.e();
                if (e == cc4.a()) {
                    ic4.c(yb4);
                }
                return e;
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "Not sending GFitWorkoutSession to Google Fit");
        FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitWorkoutSessionToGoogleFit");
        if (eg4.isActive()) {
            Result.a aVar3 = Result.Companion;
            eg4.resumeWith(Result.m3constructorimpl((Object) null));
        }
        e = eg4.e();
        if (e == cc4.a()) {
        }
        return e;
    }

    @DexIgnore
    public final /* synthetic */ Object saveToUA(yb4<Object> yb4) {
        eg4 eg4 = new eg4(IntrinsicsKt__IntrinsicsJvmKt.a(yb4), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveToUA");
        List<UASample> allUASample = getMThirdPartyDatabase().getUASampleDao().getAllUASample();
        if (true ^ allUASample.isEmpty()) {
            FLogger.INSTANCE.getLocal().d(TAG, "Sending UASample to UnderAmour");
            List<List<T>> b = kb4.b(allUASample, 500);
            int size = b.size();
            Ref$IntRef ref$IntRef = new Ref$IntRef();
            ref$IntRef.element = 0;
            for (List list : b) {
                fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new ThirdPartyRepository$saveToUA$$inlined$suspendCancellableCoroutine$lambda$Anon1(generateTimeSeries(list), list, (yb4) null, ref$IntRef, size, eg4, this), 3, (Object) null);
            }
        } else {
            FLogger.INSTANCE.getLocal().d(TAG, "UASample is empty");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveToUA");
            if (eg4.isActive()) {
                Result.a aVar = Result.Companion;
                eg4.resumeWith(Result.m3constructorimpl((Object) null));
            }
        }
        Object e = eg4.e();
        if (e == cc4.a()) {
            ic4.c(yb4);
        }
        return e;
    }

    @DexIgnore
    public final /* synthetic */ Object saveToUAOldFlow(yb4<Object> yb4) {
        eg4 eg4 = new eg4(IntrinsicsKt__IntrinsicsJvmKt.a(yb4), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveToUAOldFlow");
        List<SampleRaw> activityListByUAPinType = getMActivitiesRepository().getActivityListByUAPinType(1);
        if (true ^ activityListByUAPinType.isEmpty()) {
            FLogger.INSTANCE.getLocal().d(TAG, "Sending SampleRaw to UnderAmour");
            List<List<T>> b = kb4.b(activityListByUAPinType, 500);
            int size = b.size();
            Ref$IntRef ref$IntRef = new Ref$IntRef();
            ref$IntRef.element = 0;
            for (List list : b) {
                fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new ThirdPartyRepository$saveToUAOldFlow$$inlined$suspendCancellableCoroutine$lambda$Anon1(generateTimeSeriesOldFlow(list), list, (yb4) null, ref$IntRef, size, eg4, this), 3, (Object) null);
            }
        } else {
            FLogger.INSTANCE.getLocal().d(TAG, "All SampleRaw were synced!");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveToUAOldFlow");
            if (eg4.isActive()) {
                Result.a aVar = Result.Companion;
                eg4.resumeWith(Result.m3constructorimpl((Object) null));
            }
        }
        Object e = eg4.e();
        if (e == cc4.a()) {
            ic4.c(yb4);
        }
        return e;
    }

    @DexIgnore
    public final void setMActivitiesRepository(ActivitiesRepository activitiesRepository) {
        kd4.b(activitiesRepository, "<set-?>");
        this.mActivitiesRepository = activitiesRepository;
    }

    @DexIgnore
    public final void setMPortfolioApp(PortfolioApp portfolioApp) {
        kd4.b(portfolioApp, "<set-?>");
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void setMThirdPartyDatabase(ThirdPartyDatabase thirdPartyDatabase) {
        kd4.b(thirdPartyDatabase, "<set-?>");
        this.mThirdPartyDatabase = thirdPartyDatabase;
    }

    @DexIgnore
    public final fi4 uploadData(PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback) {
        return ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new ThirdPartyRepository$uploadData$Anon1(this, pushPendingThirdPartyDataCallback, (yb4) null), 3, (Object) null);
    }
}
