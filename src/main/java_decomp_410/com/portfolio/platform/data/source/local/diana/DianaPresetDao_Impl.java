package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.o72;
import com.fossil.blesdk.obfuscated.p72;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaPresetDao_Impl implements DianaPresetDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfDianaPreset;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfDianaRecommendPreset;
    @DexIgnore
    public /* final */ wf __preparedStmtOfClearDianaPresetTable;
    @DexIgnore
    public /* final */ wf __preparedStmtOfClearDianaRecommendPresetTable;
    @DexIgnore
    public /* final */ wf __preparedStmtOfDeletePreset;
    @DexIgnore
    public /* final */ wf __preparedStmtOfRemoveAllDeletePinTypePreset;
    @DexIgnore
    public /* final */ o72 __presetComplicationSettingTypeConverter; // = new o72();
    @DexIgnore
    public /* final */ p72 __presetWatchAppSettingTypeConverter; // = new p72();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<DianaRecommendPreset> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaRecommendPreset`(`serialNumber`,`id`,`name`,`isDefault`,`complications`,`watchapps`,`watchFaceId`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, DianaRecommendPreset dianaRecommendPreset) {
            if (dianaRecommendPreset.getSerialNumber() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, dianaRecommendPreset.getSerialNumber());
            }
            if (dianaRecommendPreset.getId() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, dianaRecommendPreset.getId());
            }
            if (dianaRecommendPreset.getName() == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, dianaRecommendPreset.getName());
            }
            kgVar.b(4, dianaRecommendPreset.isDefault() ? 1 : 0);
            String a = DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(dianaRecommendPreset.getComplications());
            if (a == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, a);
            }
            String a2 = DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(dianaRecommendPreset.getWatchapps());
            if (a2 == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, a2);
            }
            if (dianaRecommendPreset.getWatchFaceId() == null) {
                kgVar.a(7);
            } else {
                kgVar.a(7, dianaRecommendPreset.getWatchFaceId());
            }
            if (dianaRecommendPreset.getCreatedAt() == null) {
                kgVar.a(8);
            } else {
                kgVar.a(8, dianaRecommendPreset.getCreatedAt());
            }
            if (dianaRecommendPreset.getUpdatedAt() == null) {
                kgVar.a(9);
            } else {
                kgVar.a(9, dianaRecommendPreset.getUpdatedAt());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends lf<DianaPreset> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaPreset`(`createdAt`,`updatedAt`,`pinType`,`id`,`serialNumber`,`name`,`isActive`,`complications`,`watchapps`,`watchFaceId`) VALUES (?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, DianaPreset dianaPreset) {
            if (dianaPreset.getCreatedAt() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, dianaPreset.getCreatedAt());
            }
            if (dianaPreset.getUpdatedAt() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, dianaPreset.getUpdatedAt());
            }
            kgVar.b(3, (long) dianaPreset.getPinType());
            if (dianaPreset.getId() == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, dianaPreset.getId());
            }
            if (dianaPreset.getSerialNumber() == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, dianaPreset.getSerialNumber());
            }
            if (dianaPreset.getName() == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, dianaPreset.getName());
            }
            kgVar.b(7, dianaPreset.isActive() ? 1 : 0);
            String a = DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(dianaPreset.getComplications());
            if (a == null) {
                kgVar.a(8);
            } else {
                kgVar.a(8, a);
            }
            String a2 = DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(dianaPreset.getWatchapps());
            if (a2 == null) {
                kgVar.a(9);
            } else {
                kgVar.a(9, a2);
            }
            if (dianaPreset.getWatchFaceId() == null) {
                kgVar.a(10);
            } else {
                kgVar.a(10, dianaPreset.getWatchFaceId());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends wf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM dianaPreset";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends wf {
        @DexIgnore
        public Anon4(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM dianaRecommendPreset";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends wf {
        @DexIgnore
        public Anon5(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM dianaPreset WHERE id = ?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends wf {
        @DexIgnore
        public Anon6(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM dianaPreset WHERE pinType = 3";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements Callable<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon7(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public DianaPreset call() throws Exception {
            DianaPreset dianaPreset;
            Cursor a = bg.a(DianaPresetDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "createdAt");
                int b2 = ag.b(a, "updatedAt");
                int b3 = ag.b(a, "pinType");
                int b4 = ag.b(a, "id");
                int b5 = ag.b(a, "serialNumber");
                int b6 = ag.b(a, "name");
                int b7 = ag.b(a, "isActive");
                int b8 = ag.b(a, "complications");
                int b9 = ag.b(a, "watchapps");
                int b10 = ag.b(a, "watchFaceId");
                if (a.moveToFirst()) {
                    dianaPreset = new DianaPreset(a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7) != 0, DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(a.getString(b8)), DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(a.getString(b9)), a.getString(b10));
                    dianaPreset.setCreatedAt(a.getString(b));
                    dianaPreset.setUpdatedAt(a.getString(b2));
                    dianaPreset.setPinType(a.getInt(b3));
                } else {
                    dianaPreset = null;
                }
                return dianaPreset;
            } finally {
                a.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<List<DianaPreset>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon8(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<DianaPreset> call() throws Exception {
            Cursor a = bg.a(DianaPresetDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "createdAt");
                int b2 = ag.b(a, "updatedAt");
                int b3 = ag.b(a, "pinType");
                int b4 = ag.b(a, "id");
                int b5 = ag.b(a, "serialNumber");
                int b6 = ag.b(a, "name");
                int b7 = ag.b(a, "isActive");
                int b8 = ag.b(a, "complications");
                int b9 = ag.b(a, "watchapps");
                int b10 = ag.b(a, "watchFaceId");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    DianaPreset dianaPreset = new DianaPreset(a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7) != 0, DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(a.getString(b8)), DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(a.getString(b9)), a.getString(b10));
                    dianaPreset.setCreatedAt(a.getString(b));
                    dianaPreset.setUpdatedAt(a.getString(b2));
                    dianaPreset.setPinType(a.getInt(b3));
                    arrayList.add(dianaPreset);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public DianaPresetDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfDianaRecommendPreset = new Anon1(roomDatabase);
        this.__insertionAdapterOfDianaPreset = new Anon2(roomDatabase);
        this.__preparedStmtOfClearDianaPresetTable = new Anon3(roomDatabase);
        this.__preparedStmtOfClearDianaRecommendPresetTable = new Anon4(roomDatabase);
        this.__preparedStmtOfDeletePreset = new Anon5(roomDatabase);
        this.__preparedStmtOfRemoveAllDeletePinTypePreset = new Anon6(roomDatabase);
    }

    @DexIgnore
    public void clearDianaPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfClearDianaPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearDianaPresetTable.release(acquire);
        }
    }

    @DexIgnore
    public void clearDianaRecommendPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfClearDianaRecommendPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearDianaRecommendPresetTable.release(acquire);
        }
    }

    @DexIgnore
    public void deletePreset(String str) {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfDeletePreset.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeletePreset.release(acquire);
        }
    }

    @DexIgnore
    public DianaPreset getActivePresetBySerial(String str) {
        DianaPreset dianaPreset;
        String str2 = str;
        uf b = uf.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "createdAt");
            int b3 = ag.b(a, "updatedAt");
            int b4 = ag.b(a, "pinType");
            int b5 = ag.b(a, "id");
            int b6 = ag.b(a, "serialNumber");
            int b7 = ag.b(a, "name");
            int b8 = ag.b(a, "isActive");
            int b9 = ag.b(a, "complications");
            int b10 = ag.b(a, "watchapps");
            int b11 = ag.b(a, "watchFaceId");
            if (a.moveToFirst()) {
                dianaPreset = new DianaPreset(a.getString(b5), a.getString(b6), a.getString(b7), a.getInt(b8) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b9)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b10)), a.getString(b11));
                dianaPreset.setCreatedAt(a.getString(b2));
                dianaPreset.setUpdatedAt(a.getString(b3));
                dianaPreset.setPinType(a.getInt(b4));
            } else {
                dianaPreset = null;
            }
            return dianaPreset;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<DianaPreset> getActivePresetBySerialLiveData(String str) {
        uf b = uf.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"dianaPreset"}, false, new Anon7(b));
    }

    @DexIgnore
    public List<DianaPreset> getAllPendingPreset(String str) {
        String str2 = str;
        uf b = uf.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 0", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "createdAt");
            int b3 = ag.b(a, "updatedAt");
            int b4 = ag.b(a, "pinType");
            int b5 = ag.b(a, "id");
            int b6 = ag.b(a, "serialNumber");
            int b7 = ag.b(a, "name");
            int b8 = ag.b(a, "isActive");
            int b9 = ag.b(a, "complications");
            int b10 = ag.b(a, "watchapps");
            int b11 = ag.b(a, "watchFaceId");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(a.getString(b5), a.getString(b6), a.getString(b7), a.getInt(b8) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b9)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b10)), a.getString(b11));
                dianaPreset.setCreatedAt(a.getString(b2));
                dianaPreset.setUpdatedAt(a.getString(b3));
                dianaPreset.setPinType(a.getInt(b4));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<DianaPreset> getAllPreset(String str) {
        String str2 = str;
        uf b = uf.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC ", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "createdAt");
            int b3 = ag.b(a, "updatedAt");
            int b4 = ag.b(a, "pinType");
            int b5 = ag.b(a, "id");
            int b6 = ag.b(a, "serialNumber");
            int b7 = ag.b(a, "name");
            int b8 = ag.b(a, "isActive");
            int b9 = ag.b(a, "complications");
            int b10 = ag.b(a, "watchapps");
            int b11 = ag.b(a, "watchFaceId");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(a.getString(b5), a.getString(b6), a.getString(b7), a.getInt(b8) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b9)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b10)), a.getString(b11));
                dianaPreset.setCreatedAt(a.getString(b2));
                dianaPreset.setUpdatedAt(a.getString(b3));
                dianaPreset.setPinType(a.getInt(b4));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<DianaPreset>> getAllPresetAsLiveData(String str) {
        uf b = uf.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"dianaPreset"}, false, new Anon8(b));
    }

    @DexIgnore
    public List<DianaRecommendPreset> getDianaRecommendPresetList(String str) {
        String str2 = str;
        uf b = uf.b("SELECT * FROM dianaRecommendPreset WHERE serialNumber=?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "serialNumber");
            int b3 = ag.b(a, "id");
            int b4 = ag.b(a, "name");
            int b5 = ag.b(a, "isDefault");
            int b6 = ag.b(a, "complications");
            int b7 = ag.b(a, "watchapps");
            int b8 = ag.b(a, "watchFaceId");
            int b9 = ag.b(a, "createdAt");
            int b10 = ag.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new DianaRecommendPreset(a.getString(b2), a.getString(b3), a.getString(b4), a.getInt(b5) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b6)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b7)), a.getString(b8), a.getString(b9), a.getString(b10)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public DianaPreset getPresetById(String str) {
        DianaPreset dianaPreset;
        String str2 = str;
        uf b = uf.b("SELECT * FROM dianaPreset WHERE id=? AND pinType != 3", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "createdAt");
            int b3 = ag.b(a, "updatedAt");
            int b4 = ag.b(a, "pinType");
            int b5 = ag.b(a, "id");
            int b6 = ag.b(a, "serialNumber");
            int b7 = ag.b(a, "name");
            int b8 = ag.b(a, "isActive");
            int b9 = ag.b(a, "complications");
            int b10 = ag.b(a, "watchapps");
            int b11 = ag.b(a, "watchFaceId");
            if (a.moveToFirst()) {
                dianaPreset = new DianaPreset(a.getString(b5), a.getString(b6), a.getString(b7), a.getInt(b8) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b9)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b10)), a.getString(b11));
                dianaPreset.setCreatedAt(a.getString(b2));
                dianaPreset.setUpdatedAt(a.getString(b3));
                dianaPreset.setPinType(a.getInt(b4));
            } else {
                dianaPreset = null;
            }
            return dianaPreset;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void removeAllDeletePinTypePreset() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfRemoveAllDeletePinTypePreset.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAllDeletePinTypePreset.release(acquire);
        }
    }

    @DexIgnore
    public void upsertDianaRecommendPresetList(List<DianaRecommendPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaRecommendPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertPreset(DianaPreset dianaPreset) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaPreset.insert(dianaPreset);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertPresetList(List<DianaPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
