package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.mk2;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.pk2;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;
import com.portfolio.platform.enums.AuthType;
import com.portfolio.platform.enums.Gender;
import java.util.Date;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UserRepository extends UserDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ en2 mSharedPreferencesManager;
    @DexIgnore
    public /* final */ UserDataSource mUserLocalDataSource;
    @DexIgnore
    public /* final */ UserDataSource mUserRemoteDataSource;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = UserRepository.class.getSimpleName();
        kd4.a((Object) simpleName, "UserRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public UserRepository(@Remote UserDataSource userDataSource, @Local UserDataSource userDataSource2, en2 en2) {
        kd4.b(userDataSource, "mUserRemoteDataSource");
        kd4.b(userDataSource2, "mUserLocalDataSource");
        kd4.b(en2, "mSharedPreferencesManager");
        this.mUserRemoteDataSource = userDataSource;
        this.mUserLocalDataSource = userDataSource2;
        this.mSharedPreferencesManager = en2;
    }

    @DexIgnore
    public Object checkAuthenticationEmailExisting(String str, yb4<? super qo2<Boolean>> yb4) {
        return this.mUserRemoteDataSource.checkAuthenticationEmailExisting(str, yb4);
    }

    @DexIgnore
    public Object checkAuthenticationSocialExisting(String str, String str2, yb4<? super qo2<Boolean>> yb4) {
        return this.mUserRemoteDataSource.checkAuthenticationSocialExisting(str, str2, yb4);
    }

    @DexIgnore
    public void clearAllUser() {
        FLogger.INSTANCE.getLocal().e(TAG, "clearAllUser");
        this.mUserLocalDataSource.clearAllUser();
    }

    @DexIgnore
    public Object deleteUser(MFUser mFUser, yb4<? super Integer> yb4) {
        return this.mUserRemoteDataSource.deleteUser(mFUser, yb4);
    }

    @DexIgnore
    public MFUser getCurrentUser() {
        return this.mUserLocalDataSource.getCurrentUser();
    }

    @DexIgnore
    public void insertUser(MFUser mFUser) {
        kd4.b(mFUser, "user");
        this.mUserLocalDataSource.insertUser(mFUser);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object loadUserInfo(yb4<? super qo2<MFUser>> yb4) {
        UserRepository$loadUserInfo$Anon1 userRepository$loadUserInfo$Anon1;
        int i;
        UserRepository userRepository;
        MFUser mFUser;
        MFUser mFUser2;
        qo2 qo2;
        if (yb4 instanceof UserRepository$loadUserInfo$Anon1) {
            userRepository$loadUserInfo$Anon1 = (UserRepository$loadUserInfo$Anon1) yb4;
            int i2 = userRepository$loadUserInfo$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRepository$loadUserInfo$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRepository$loadUserInfo$Anon1.result;
                Object a = cc4.a();
                i = userRepository$loadUserInfo$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    mFUser2 = getCurrentUser();
                    if (mFUser2 != null) {
                        UserDataSource userDataSource = this.mUserRemoteDataSource;
                        userRepository$loadUserInfo$Anon1.L$Anon0 = this;
                        userRepository$loadUserInfo$Anon1.L$Anon1 = mFUser2;
                        userRepository$loadUserInfo$Anon1.L$Anon2 = mFUser2;
                        userRepository$loadUserInfo$Anon1.label = 1;
                        obj = userDataSource.loadUserInfo(mFUser2, userRepository$loadUserInfo$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        userRepository = this;
                        mFUser = mFUser2;
                    } else {
                        FLogger.INSTANCE.getLocal().e(TAG, "user is null");
                        return new po2(600, (ServerError) null, (Throwable) null, (String) null, 8, (fd4) null);
                    }
                } else if (i == 1) {
                    userRepository = (UserRepository) userRepository$loadUserInfo$Anon1.L$Anon0;
                    na4.a(obj);
                    MFUser mFUser3 = (MFUser) userRepository$loadUserInfo$Anon1.L$Anon1;
                    mFUser = (MFUser) userRepository$loadUserInfo$Anon1.L$Anon2;
                    mFUser2 = mFUser3;
                } else if (i == 2) {
                    qo2 qo22 = (qo2) userRepository$loadUserInfo$Anon1.L$Anon3;
                    MFUser mFUser4 = (MFUser) userRepository$loadUserInfo$Anon1.L$Anon2;
                    MFUser mFUser5 = (MFUser) userRepository$loadUserInfo$Anon1.L$Anon1;
                    UserRepository userRepository2 = (UserRepository) userRepository$loadUserInfo$Anon1.L$Anon0;
                    na4.a(obj);
                    return (qo2) obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (qo2 instanceof ro2) {
                    return qo2;
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("loadUserInfo success isFromCache ");
                ro2 ro2 = (ro2) qo2;
                sb.append(ro2.b());
                local.d(str, sb.toString());
                if (ro2.b()) {
                    return qo2;
                }
                UserDataSource userDataSource2 = userRepository.mUserLocalDataSource;
                Object a2 = ro2.a();
                if (a2 != null) {
                    userRepository$loadUserInfo$Anon1.L$Anon0 = userRepository;
                    userRepository$loadUserInfo$Anon1.L$Anon1 = mFUser2;
                    userRepository$loadUserInfo$Anon1.L$Anon2 = mFUser;
                    userRepository$loadUserInfo$Anon1.L$Anon3 = qo2;
                    userRepository$loadUserInfo$Anon1.label = 2;
                    obj = userDataSource2.updateUser((MFUser) a2, false, userRepository$loadUserInfo$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    return (qo2) obj;
                }
                kd4.a();
                throw null;
            }
        }
        userRepository$loadUserInfo$Anon1 = new UserRepository$loadUserInfo$Anon1(this, yb4);
        Object obj2 = userRepository$loadUserInfo$Anon1.result;
        Object a3 = cc4.a();
        i = userRepository$loadUserInfo$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (qo2 instanceof ro2) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    public Object loginEmail(String str, String str2, yb4<? super qo2<Auth>> yb4) {
        UserRepository$loginEmail$Anon1 userRepository$loginEmail$Anon1;
        int i;
        UserRepository userRepository;
        qo2 qo2;
        int i2;
        String str3 = str;
        String str4 = str2;
        yb4<? super qo2<Auth>> yb42 = yb4;
        if (yb42 instanceof UserRepository$loginEmail$Anon1) {
            userRepository$loginEmail$Anon1 = (UserRepository$loginEmail$Anon1) yb42;
            int i3 = userRepository$loginEmail$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                userRepository$loginEmail$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = userRepository$loginEmail$Anon1.result;
                Object a = cc4.a();
                i = userRepository$loginEmail$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    UserDataSource userDataSource = this.mUserRemoteDataSource;
                    userRepository$loginEmail$Anon1.L$Anon0 = this;
                    userRepository$loginEmail$Anon1.L$Anon1 = str3;
                    userRepository$loginEmail$Anon1.L$Anon2 = str4;
                    userRepository$loginEmail$Anon1.label = 1;
                    obj = userDataSource.loginEmail(str3, str4, userRepository$loginEmail$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    userRepository = this;
                } else if (i == 1) {
                    String str5 = (String) userRepository$loginEmail$Anon1.L$Anon2;
                    String str6 = (String) userRepository$loginEmail$Anon1.L$Anon1;
                    userRepository = (UserRepository) userRepository$loginEmail$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                String str7 = null;
                if (!(qo2 instanceof ro2)) {
                    Auth auth = (Auth) ((ro2) qo2).a();
                    MFUser mFUser = new MFUser();
                    mFUser.setAuthType(AuthType.EMAIL.getValue());
                    mFUser.setUserAccessToken(auth != null ? auth.getAccessToken() : null);
                    mFUser.setRefreshToken(auth != null ? auth.getRefreshToken() : null);
                    mFUser.setAccessTokenExpiresAt(rk2.t(auth != null ? auth.getAccessTokenExpiresAt() : null));
                    if (auth != null) {
                        Integer accessTokenExpiresIn = auth.getAccessTokenExpiresIn();
                        if (accessTokenExpiresIn != null) {
                            i2 = accessTokenExpiresIn.intValue();
                            mFUser.setAccessTokenExpiresIn(dc4.a(i2));
                            mFUser.setUserId(auth == null ? auth.getUid() : null);
                            userRepository.mUserLocalDataSource.insertUser(mFUser);
                            userRepository.mSharedPreferencesManager.w(auth == null ? auth.getAccessToken() : null);
                            userRepository.mSharedPreferencesManager.a(System.currentTimeMillis());
                            PortfolioApp.W.c().H();
                            return new ro2(auth, false, 2, (fd4) null);
                        }
                    }
                    i2 = 0;
                    mFUser.setAccessTokenExpiresIn(dc4.a(i2));
                    mFUser.setUserId(auth == null ? auth.getUid() : null);
                    userRepository.mUserLocalDataSource.insertUser(mFUser);
                    userRepository.mSharedPreferencesManager.w(auth == null ? auth.getAccessToken() : null);
                    userRepository.mSharedPreferencesManager.a(System.currentTimeMillis());
                    PortfolioApp.W.c().H();
                    return new ro2(auth, false, 2, (fd4) null);
                } else if (!(qo2 instanceof po2)) {
                    return new po2(600, new ServerError(), (Throwable) null, (String) null, 8, (fd4) null);
                } else {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str8 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("loginEmail Failure error=");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(" message=");
                    ServerError c = po2.c();
                    if (c != null) {
                        str7 = c.getMessage();
                    }
                    sb.append(str7);
                    local.d(str8, sb.toString());
                    return new po2(po2.a(), po2.c(), po2.d(), (String) null, 8, (fd4) null);
                }
            }
        }
        userRepository$loginEmail$Anon1 = new UserRepository$loginEmail$Anon1(this, yb42);
        Object obj2 = userRepository$loginEmail$Anon1.result;
        Object a2 = cc4.a();
        i = userRepository$loginEmail$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        String str72 = null;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    public Object loginWithSocial(String str, String str2, String str3, yb4<? super qo2<Auth>> yb4) {
        UserRepository$loginWithSocial$Anon1 userRepository$loginWithSocial$Anon1;
        int i;
        UserRepository userRepository;
        qo2 qo2;
        int i2;
        String str4 = str;
        String str5 = str2;
        String str6 = str3;
        yb4<? super qo2<Auth>> yb42 = yb4;
        if (yb42 instanceof UserRepository$loginWithSocial$Anon1) {
            userRepository$loginWithSocial$Anon1 = (UserRepository$loginWithSocial$Anon1) yb42;
            int i3 = userRepository$loginWithSocial$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                userRepository$loginWithSocial$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = userRepository$loginWithSocial$Anon1.result;
                Object a = cc4.a();
                i = userRepository$loginWithSocial$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    UserDataSource userDataSource = this.mUserRemoteDataSource;
                    userRepository$loginWithSocial$Anon1.L$Anon0 = this;
                    userRepository$loginWithSocial$Anon1.L$Anon1 = str4;
                    userRepository$loginWithSocial$Anon1.L$Anon2 = str5;
                    userRepository$loginWithSocial$Anon1.L$Anon3 = str6;
                    userRepository$loginWithSocial$Anon1.label = 1;
                    obj = userDataSource.loginWithSocial(str4, str5, str6, userRepository$loginWithSocial$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    userRepository = this;
                } else if (i == 1) {
                    String str7 = (String) userRepository$loginWithSocial$Anon1.L$Anon3;
                    String str8 = (String) userRepository$loginWithSocial$Anon1.L$Anon2;
                    str4 = (String) userRepository$loginWithSocial$Anon1.L$Anon1;
                    userRepository = (UserRepository) userRepository$loginWithSocial$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                String str9 = null;
                if (!(qo2 instanceof ro2)) {
                    Auth auth = (Auth) ((ro2) qo2).a();
                    MFUser mFUser = new MFUser();
                    switch (str4.hashCode()) {
                        case -1240244679:
                            if (str4.equals("google")) {
                                mFUser.setAuthType(AuthType.GOOGLE.getValue());
                                break;
                            }
                            break;
                        case -791770330:
                            if (str4.equals("wechat")) {
                                mFUser.setAuthType(AuthType.WECHAT.getValue());
                                break;
                            }
                            break;
                        case 93029210:
                            if (str4.equals("apple")) {
                                mFUser.setAuthType(AuthType.APPLE.getValue());
                                break;
                            }
                            break;
                        case 113011944:
                            if (str4.equals("weibo")) {
                                mFUser.setAuthType(AuthType.WEIBO.getValue());
                                break;
                            }
                            break;
                        case 497130182:
                            if (str4.equals(Constants.FACEBOOK)) {
                                mFUser.setAuthType(AuthType.FACEBOOK.getValue());
                                break;
                            }
                            break;
                    }
                    mFUser.setUserAccessToken(auth != null ? auth.getAccessToken() : null);
                    mFUser.setRefreshToken(auth != null ? auth.getRefreshToken() : null);
                    if (auth != null) {
                        Integer accessTokenExpiresIn = auth.getAccessTokenExpiresIn();
                        if (accessTokenExpiresIn != null) {
                            i2 = accessTokenExpiresIn.intValue();
                            mFUser.setAccessTokenExpiresIn(dc4.a(i2));
                            mFUser.setAccessTokenExpiresAt(rk2.t(auth == null ? auth.getAccessTokenExpiresAt() : null));
                            mFUser.setUserId(auth == null ? auth.getUid() : null);
                            userRepository.mUserLocalDataSource.insertUser(mFUser);
                            userRepository.mSharedPreferencesManager.w(auth == null ? auth.getAccessToken() : null);
                            userRepository.mSharedPreferencesManager.a(System.currentTimeMillis());
                            PortfolioApp.W.c().H();
                            return new ro2(auth, false, 2, (fd4) null);
                        }
                    }
                    i2 = 0;
                    mFUser.setAccessTokenExpiresIn(dc4.a(i2));
                    mFUser.setAccessTokenExpiresAt(rk2.t(auth == null ? auth.getAccessTokenExpiresAt() : null));
                    mFUser.setUserId(auth == null ? auth.getUid() : null);
                    userRepository.mUserLocalDataSource.insertUser(mFUser);
                    userRepository.mSharedPreferencesManager.w(auth == null ? auth.getAccessToken() : null);
                    userRepository.mSharedPreferencesManager.a(System.currentTimeMillis());
                    PortfolioApp.W.c().H();
                    return new ro2(auth, false, 2, (fd4) null);
                } else if (!(qo2 instanceof po2)) {
                    return new po2(600, new ServerError(), (Throwable) null, (String) null, 8, (fd4) null);
                } else {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str10 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("loginWithSocial Failure error=");
                    po2 po2 = (po2) qo2;
                    sb.append(po2.a());
                    sb.append(" message=");
                    ServerError c = po2.c();
                    if (c != null) {
                        str9 = c.getMessage();
                    }
                    sb.append(str9);
                    local.d(str10, sb.toString());
                    return new po2(po2.a(), po2.c(), po2.d(), (String) null, 8, (fd4) null);
                }
            }
        }
        userRepository$loginWithSocial$Anon1 = new UserRepository$loginWithSocial$Anon1(this, yb42);
        Object obj2 = userRepository$loginWithSocial$Anon1.result;
        Object a2 = cc4.a();
        i = userRepository$loginWithSocial$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        String str92 = null;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    public Object logoutUser(yb4<? super Integer> yb4) {
        return this.mUserRemoteDataSource.logoutUser(yb4);
    }

    @DexIgnore
    public Object requestEmailOtp(String str, yb4<? super qo2<Void>> yb4) {
        return this.mUserRemoteDataSource.requestEmailOtp(str, yb4);
    }

    @DexIgnore
    public Object resetPassword(String str, yb4<? super qo2<Integer>> yb4) {
        FLogger.INSTANCE.getLocal().d(TAG, "resetPassword");
        return this.mUserRemoteDataSource.resetPassword(str, yb4);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object signUpEmail(SignUpEmailAuth signUpEmailAuth, yb4<? super qo2<Auth>> yb4) {
        UserRepository$signUpEmail$Anon1 userRepository$signUpEmail$Anon1;
        int i;
        UserRepository userRepository;
        qo2 qo2;
        int i2;
        if (yb4 instanceof UserRepository$signUpEmail$Anon1) {
            userRepository$signUpEmail$Anon1 = (UserRepository$signUpEmail$Anon1) yb4;
            int i3 = userRepository$signUpEmail$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                userRepository$signUpEmail$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = userRepository$signUpEmail$Anon1.result;
                Object a = cc4.a();
                i = userRepository$signUpEmail$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    UserDataSource userDataSource = this.mUserRemoteDataSource;
                    userRepository$signUpEmail$Anon1.L$Anon0 = this;
                    userRepository$signUpEmail$Anon1.L$Anon1 = signUpEmailAuth;
                    userRepository$signUpEmail$Anon1.label = 1;
                    obj = userDataSource.signUpEmail(signUpEmailAuth, userRepository$signUpEmail$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    userRepository = this;
                } else if (i == 1) {
                    signUpEmailAuth = (SignUpEmailAuth) userRepository$signUpEmail$Anon1.L$Anon1;
                    userRepository = (UserRepository) userRepository$signUpEmail$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    Auth auth = (Auth) ro2.a();
                    MFUser mFUser = new MFUser();
                    Pair<Integer, Integer> a2 = mk2.a.a(Gender.Companion.a(signUpEmailAuth.getGender()), MFUser.getAge(signUpEmailAuth.getBirthday()));
                    mFUser.setAuthType(AuthType.EMAIL.getValue());
                    mFUser.setUserAccessToken(auth != null ? auth.getAccessToken() : null);
                    mFUser.setRefreshToken(auth != null ? auth.getRefreshToken() : null);
                    if (auth != null) {
                        Integer accessTokenExpiresIn = auth.getAccessTokenExpiresIn();
                        if (accessTokenExpiresIn != null) {
                            i2 = accessTokenExpiresIn.intValue();
                            mFUser.setAccessTokenExpiresIn(dc4.a(i2));
                            mFUser.setAccessTokenExpiresAt(rk2.t(auth == null ? auth.getAccessTokenExpiresAt() : null));
                            mFUser.setUserId(auth == null ? auth.getUid() : null);
                            mFUser.setEmail(signUpEmailAuth.getEmail());
                            mFUser.setFirstName(signUpEmailAuth.getFirstName());
                            mFUser.setLastName(signUpEmailAuth.getLastName());
                            mFUser.setGender(signUpEmailAuth.getGender());
                            mFUser.setBirthday(signUpEmailAuth.getBirthday());
                            mFUser.setDiagnosticEnabled(signUpEmailAuth.getDiagnosticEnabled());
                            mFUser.setCreatedAt(rk2.t(new Date()));
                            mFUser.setUpdatedAt(mFUser.getCreatedAt());
                            mFUser.setOnboardingComplete(true);
                            mFUser.setHeightInCentimeters(a2.getFirst().intValue());
                            mFUser.setUseDefaultBiometric(true);
                            mFUser.setUseDefaultGoals(true);
                            mFUser.setWeightInGrams((int) pk2.h((float) a2.getSecond().intValue()));
                            userRepository.mUserLocalDataSource.insertUser(mFUser);
                            userRepository.mSharedPreferencesManager.a(System.currentTimeMillis());
                            PortfolioApp.W.c().H();
                            return new ro2(ro2.a(), false, 2, (fd4) null);
                        }
                    }
                    i2 = 0;
                    mFUser.setAccessTokenExpiresIn(dc4.a(i2));
                    mFUser.setAccessTokenExpiresAt(rk2.t(auth == null ? auth.getAccessTokenExpiresAt() : null));
                    mFUser.setUserId(auth == null ? auth.getUid() : null);
                    mFUser.setEmail(signUpEmailAuth.getEmail());
                    mFUser.setFirstName(signUpEmailAuth.getFirstName());
                    mFUser.setLastName(signUpEmailAuth.getLastName());
                    mFUser.setGender(signUpEmailAuth.getGender());
                    mFUser.setBirthday(signUpEmailAuth.getBirthday());
                    mFUser.setDiagnosticEnabled(signUpEmailAuth.getDiagnosticEnabled());
                    mFUser.setCreatedAt(rk2.t(new Date()));
                    mFUser.setUpdatedAt(mFUser.getCreatedAt());
                    mFUser.setOnboardingComplete(true);
                    mFUser.setHeightInCentimeters(a2.getFirst().intValue());
                    mFUser.setUseDefaultBiometric(true);
                    mFUser.setUseDefaultGoals(true);
                    mFUser.setWeightInGrams((int) pk2.h((float) a2.getSecond().intValue()));
                    userRepository.mUserLocalDataSource.insertUser(mFUser);
                    userRepository.mSharedPreferencesManager.a(System.currentTimeMillis());
                    PortfolioApp.W.c().H();
                    return new ro2(ro2.a(), false, 2, (fd4) null);
                } else if (!(qo2 instanceof po2)) {
                    return null;
                } else {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), po2.d(), (String) null, 8, (fd4) null);
                }
            }
        }
        userRepository$signUpEmail$Anon1 = new UserRepository$signUpEmail$Anon1(this, yb4);
        Object obj2 = userRepository$signUpEmail$Anon1.result;
        Object a3 = cc4.a();
        i = userRepository$signUpEmail$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0151  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object signUpSocial(SignUpSocialAuth signUpSocialAuth, yb4<? super qo2<Auth>> yb4) {
        UserRepository$signUpSocial$Anon1 userRepository$signUpSocial$Anon1;
        int i;
        UserRepository userRepository;
        qo2 qo2;
        int i2;
        if (yb4 instanceof UserRepository$signUpSocial$Anon1) {
            userRepository$signUpSocial$Anon1 = (UserRepository$signUpSocial$Anon1) yb4;
            int i3 = userRepository$signUpSocial$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                userRepository$signUpSocial$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = userRepository$signUpSocial$Anon1.result;
                Object a = cc4.a();
                i = userRepository$signUpSocial$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    UserDataSource userDataSource = this.mUserRemoteDataSource;
                    userRepository$signUpSocial$Anon1.L$Anon0 = this;
                    userRepository$signUpSocial$Anon1.L$Anon1 = signUpSocialAuth;
                    userRepository$signUpSocial$Anon1.label = 1;
                    obj = userDataSource.signUpSocial(signUpSocialAuth, userRepository$signUpSocial$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    userRepository = this;
                } else if (i == 1) {
                    signUpSocialAuth = (SignUpSocialAuth) userRepository$signUpSocial$Anon1.L$Anon1;
                    userRepository = (UserRepository) userRepository$signUpSocial$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ro2 ro2 = (ro2) qo2;
                    Auth auth = (Auth) ro2.a();
                    MFUser mFUser = new MFUser();
                    mk2 mk2 = mk2.a;
                    Gender a2 = Gender.Companion.a(signUpSocialAuth.getGender());
                    if (a2 == null) {
                        a2 = Gender.OTHER;
                    }
                    Pair<Integer, Integer> a3 = mk2.a(a2, MFUser.getAge(signUpSocialAuth.getBirthday()));
                    mFUser.setAuthType(AuthType.EMAIL.getValue());
                    mFUser.setUserAccessToken(auth != null ? auth.getAccessToken() : null);
                    mFUser.setRefreshToken(auth != null ? auth.getRefreshToken() : null);
                    if (auth != null) {
                        Integer accessTokenExpiresIn = auth.getAccessTokenExpiresIn();
                        if (accessTokenExpiresIn != null) {
                            i2 = accessTokenExpiresIn.intValue();
                            mFUser.setAccessTokenExpiresIn(dc4.a(i2));
                            mFUser.setAccessTokenExpiresAt(rk2.t(auth == null ? auth.getAccessTokenExpiresAt() : null));
                            mFUser.setUserId(auth == null ? auth.getUid() : null);
                            mFUser.setEmail(signUpSocialAuth.getEmail());
                            mFUser.setFirstName(signUpSocialAuth.getFirstName());
                            mFUser.setLastName(signUpSocialAuth.getLastName());
                            mFUser.setGender(signUpSocialAuth.getGender());
                            mFUser.setBirthday(signUpSocialAuth.getBirthday());
                            mFUser.setDiagnosticEnabled(signUpSocialAuth.getDiagnosticEnabled());
                            mFUser.setCreatedAt(rk2.t(new Date()));
                            mFUser.setUpdatedAt(mFUser.getCreatedAt());
                            mFUser.setOnboardingComplete(true);
                            mFUser.setHeightInCentimeters(a3.getFirst().intValue());
                            mFUser.setUseDefaultBiometric(true);
                            mFUser.setWeightInGrams((int) pk2.h((float) a3.getSecond().intValue()));
                            userRepository.mUserLocalDataSource.insertUser(mFUser);
                            userRepository.mSharedPreferencesManager.a(System.currentTimeMillis());
                            PortfolioApp.W.c().H();
                            return new ro2(ro2.a(), false, 2, (fd4) null);
                        }
                    }
                    i2 = 0;
                    mFUser.setAccessTokenExpiresIn(dc4.a(i2));
                    mFUser.setAccessTokenExpiresAt(rk2.t(auth == null ? auth.getAccessTokenExpiresAt() : null));
                    mFUser.setUserId(auth == null ? auth.getUid() : null);
                    mFUser.setEmail(signUpSocialAuth.getEmail());
                    mFUser.setFirstName(signUpSocialAuth.getFirstName());
                    mFUser.setLastName(signUpSocialAuth.getLastName());
                    mFUser.setGender(signUpSocialAuth.getGender());
                    mFUser.setBirthday(signUpSocialAuth.getBirthday());
                    mFUser.setDiagnosticEnabled(signUpSocialAuth.getDiagnosticEnabled());
                    mFUser.setCreatedAt(rk2.t(new Date()));
                    mFUser.setUpdatedAt(mFUser.getCreatedAt());
                    mFUser.setOnboardingComplete(true);
                    mFUser.setHeightInCentimeters(a3.getFirst().intValue());
                    mFUser.setUseDefaultBiometric(true);
                    mFUser.setWeightInGrams((int) pk2.h((float) a3.getSecond().intValue()));
                    userRepository.mUserLocalDataSource.insertUser(mFUser);
                    userRepository.mSharedPreferencesManager.a(System.currentTimeMillis());
                    PortfolioApp.W.c().H();
                    return new ro2(ro2.a(), false, 2, (fd4) null);
                } else if (!(qo2 instanceof po2)) {
                    return null;
                } else {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), po2.d(), (String) null, 8, (fd4) null);
                }
            }
        }
        userRepository$signUpSocial$Anon1 = new UserRepository$signUpSocial$Anon1(this, yb4);
        Object obj2 = userRepository$signUpSocial$Anon1.result;
        Object a4 = cc4.a();
        i = userRepository$signUpSocial$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public Object updateUser(MFUser mFUser, boolean z, yb4<? super qo2<MFUser>> yb4) {
        UserRepository$updateUser$Anon1 userRepository$updateUser$Anon1;
        int i;
        UserRepository userRepository;
        qo2 qo2;
        if (yb4 instanceof UserRepository$updateUser$Anon1) {
            userRepository$updateUser$Anon1 = (UserRepository$updateUser$Anon1) yb4;
            int i2 = userRepository$updateUser$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                userRepository$updateUser$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = userRepository$updateUser$Anon1.result;
                Object a = cc4.a();
                i = userRepository$updateUser$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    if (z) {
                        FLogger.INSTANCE.getLocal().d(TAG, "Inside .updateUser updateUserInfo on server");
                        UserDataSource userDataSource = this.mUserRemoteDataSource;
                        userRepository$updateUser$Anon1.L$Anon0 = this;
                        userRepository$updateUser$Anon1.L$Anon1 = mFUser;
                        userRepository$updateUser$Anon1.Z$Anon0 = z;
                        userRepository$updateUser$Anon1.label = 1;
                        obj = userDataSource.updateUser(mFUser, true, userRepository$updateUser$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        userRepository = this;
                    } else {
                        UserDataSource userDataSource2 = this.mUserLocalDataSource;
                        userRepository$updateUser$Anon1.L$Anon0 = this;
                        userRepository$updateUser$Anon1.L$Anon1 = mFUser;
                        userRepository$updateUser$Anon1.Z$Anon0 = z;
                        userRepository$updateUser$Anon1.label = 3;
                        obj = userDataSource2.updateUser(mFUser, z, userRepository$updateUser$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        return (qo2) obj;
                    }
                } else if (i == 1) {
                    z = userRepository$updateUser$Anon1.Z$Anon0;
                    mFUser = (MFUser) userRepository$updateUser$Anon1.L$Anon1;
                    userRepository = (UserRepository) userRepository$updateUser$Anon1.L$Anon0;
                    na4.a(obj);
                } else if (i == 2) {
                    MFUser mFUser2 = (MFUser) userRepository$updateUser$Anon1.L$Anon3;
                    qo2 qo22 = (qo2) userRepository$updateUser$Anon1.L$Anon2;
                    boolean z2 = userRepository$updateUser$Anon1.Z$Anon0;
                    MFUser mFUser3 = (MFUser) userRepository$updateUser$Anon1.L$Anon1;
                    UserRepository userRepository2 = (UserRepository) userRepository$updateUser$Anon1.L$Anon0;
                    na4.a(obj);
                    return (qo2) obj;
                } else if (i == 3) {
                    boolean z3 = userRepository$updateUser$Anon1.Z$Anon0;
                    MFUser mFUser4 = (MFUser) userRepository$updateUser$Anon1.L$Anon1;
                    UserRepository userRepository3 = (UserRepository) userRepository$updateUser$Anon1.L$Anon0;
                    na4.a(obj);
                    return (qo2) obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "Inside .updateUser updateUserInfo success on server");
                    Object a2 = ((ro2) qo2).a();
                    if (a2 != null) {
                        MFUser mFUser5 = (MFUser) a2;
                        mFUser5.setOnboardingComplete(mFUser.isOnboardingComplete());
                        UserDataSource userDataSource3 = userRepository.mUserLocalDataSource;
                        userRepository$updateUser$Anon1.L$Anon0 = userRepository;
                        userRepository$updateUser$Anon1.L$Anon1 = mFUser;
                        userRepository$updateUser$Anon1.Z$Anon0 = z;
                        userRepository$updateUser$Anon1.L$Anon2 = qo2;
                        userRepository$updateUser$Anon1.L$Anon3 = mFUser5;
                        userRepository$updateUser$Anon1.label = 2;
                        obj = userDataSource3.updateUser(mFUser5, false, userRepository$updateUser$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        return (qo2) obj;
                    }
                    kd4.a();
                    throw null;
                }
                FLogger.INSTANCE.getLocal().d(TAG, "Inside .updateUser updateUserInfo error on server");
                return qo2;
            }
        }
        userRepository$updateUser$Anon1 = new UserRepository$updateUser$Anon1(this, yb4);
        Object obj2 = userRepository$updateUser$Anon1.result;
        Object a3 = cc4.a();
        i = userRepository$updateUser$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }

    @DexIgnore
    public Object verifyEmailOtp(String str, String str2, yb4<? super qo2<Void>> yb4) {
        return this.mUserRemoteDataSource.verifyEmailOtp(str, str2, yb4);
    }
}
