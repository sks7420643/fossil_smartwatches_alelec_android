package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.g72;
import com.fossil.blesdk.obfuscated.h72;
import com.fossil.blesdk.obfuscated.i72;
import com.fossil.blesdk.obfuscated.j72;
import com.fossil.blesdk.obfuscated.kf;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GFitWorkoutSessionDao_Impl implements GFitWorkoutSessionDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ kf __deletionAdapterOfGFitWorkoutSession;
    @DexIgnore
    public /* final */ g72 __gFitWOCaloriesConverter; // = new g72();
    @DexIgnore
    public /* final */ h72 __gFitWODistancesConverter; // = new h72();
    @DexIgnore
    public /* final */ i72 __gFitWOHeartRatesConverter; // = new i72();
    @DexIgnore
    public /* final */ j72 __gFitWOStepsConverter; // = new j72();
    @DexIgnore
    public /* final */ lf __insertionAdapterOfGFitWorkoutSession;
    @DexIgnore
    public /* final */ wf __preparedStmtOfClearAll;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<GFitWorkoutSession> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitWorkoutSession`(`id`,`startTime`,`endTime`,`workoutType`,`steps`,`calories`,`distances`,`heartRates`) VALUES (nullif(?, 0),?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, GFitWorkoutSession gFitWorkoutSession) {
            kgVar.b(1, (long) gFitWorkoutSession.getId());
            kgVar.b(2, gFitWorkoutSession.getStartTime());
            kgVar.b(3, gFitWorkoutSession.getEndTime());
            kgVar.b(4, (long) gFitWorkoutSession.getWorkoutType());
            String a = GFitWorkoutSessionDao_Impl.this.__gFitWOStepsConverter.a(gFitWorkoutSession.getSteps());
            if (a == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, a);
            }
            String a2 = GFitWorkoutSessionDao_Impl.this.__gFitWOCaloriesConverter.a(gFitWorkoutSession.getCalories());
            if (a2 == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, a2);
            }
            String a3 = GFitWorkoutSessionDao_Impl.this.__gFitWODistancesConverter.a(gFitWorkoutSession.getDistances());
            if (a3 == null) {
                kgVar.a(7);
            } else {
                kgVar.a(7, a3);
            }
            String a4 = GFitWorkoutSessionDao_Impl.this.__gFitWOHeartRatesConverter.a(gFitWorkoutSession.getHeartRates());
            if (a4 == null) {
                kgVar.a(8);
            } else {
                kgVar.a(8, a4);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends kf<GFitWorkoutSession> {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `gFitWorkoutSession` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(kg kgVar, GFitWorkoutSession gFitWorkoutSession) {
            kgVar.b(1, (long) gFitWorkoutSession.getId());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends wf {
        @DexIgnore
        public Anon3(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM gFitWorkoutSession";
        }
    }

    @DexIgnore
    public GFitWorkoutSessionDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfGFitWorkoutSession = new Anon1(roomDatabase);
        this.__deletionAdapterOfGFitWorkoutSession = new Anon2(roomDatabase);
        this.__preparedStmtOfClearAll = new Anon3(roomDatabase);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public void deleteGFitWorkoutSession(GFitWorkoutSession gFitWorkoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitWorkoutSession.handle(gFitWorkoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<GFitWorkoutSession> getAllGFitWorkoutSession() {
        uf b = uf.b("SELECT * FROM gFitWorkoutSession", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "id");
            int b3 = ag.b(a, SampleRaw.COLUMN_START_TIME);
            int b4 = ag.b(a, SampleRaw.COLUMN_END_TIME);
            int b5 = ag.b(a, "workoutType");
            int b6 = ag.b(a, "steps");
            int b7 = ag.b(a, "calories");
            int b8 = ag.b(a, "distances");
            int b9 = ag.b(a, "heartRates");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GFitWorkoutSession gFitWorkoutSession = new GFitWorkoutSession(a.getLong(b3), a.getLong(b4), a.getInt(b5), this.__gFitWOStepsConverter.a(a.getString(b6)), this.__gFitWOCaloriesConverter.a(a.getString(b7)), this.__gFitWODistancesConverter.a(a.getString(b8)), this.__gFitWOHeartRatesConverter.a(a.getString(b9)));
                gFitWorkoutSession.setId(a.getInt(b2));
                arrayList.add(gFitWorkoutSession);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertGFitWorkoutSession(GFitWorkoutSession gFitWorkoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitWorkoutSession.insert(gFitWorkoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListGFitWorkoutSession(List<GFitWorkoutSession> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitWorkoutSession.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
