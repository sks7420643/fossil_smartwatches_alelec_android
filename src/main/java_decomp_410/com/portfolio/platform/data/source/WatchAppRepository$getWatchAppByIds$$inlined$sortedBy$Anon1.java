package com.portfolio.platform.data.source;

import com.fossil.blesdk.obfuscated.wb4;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppRepository$getWatchAppByIds$$inlined$sortedBy$Anon1<T> implements Comparator<T> {
    @DexIgnore
    public /* final */ /* synthetic */ List $ids$inlined;

    @DexIgnore
    public WatchAppRepository$getWatchAppByIds$$inlined$sortedBy$Anon1(List list) {
        this.$ids$inlined = list;
    }

    @DexIgnore
    public final int compare(T t, T t2) {
        return wb4.a(Integer.valueOf(this.$ids$inlined.indexOf(((WatchApp) t).getWatchappId())), Integer.valueOf(this.$ids$inlined.indexOf(((WatchApp) t2).getWatchappId())));
    }
}
