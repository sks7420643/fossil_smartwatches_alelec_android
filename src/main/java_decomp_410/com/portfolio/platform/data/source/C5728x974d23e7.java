package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1 */
public final class C5728x974d23e7<TResult> implements com.fossil.blesdk.obfuscated.tn1<java.lang.Void> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.dg4 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$IntRef $countSizeOfLists$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.ap0 $device$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep $gFitSLeepData;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $gFitSLeepDataList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.google.android.gms.auth.api.signin.GoogleSignInAccount $googleSignInAccount$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.ArrayList $listInsertGFitSuccessFul$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfListsOfGFitSleepData$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1$1")
    /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1$1 */
    public static final class C57291 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21099p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.data.source.C5728x974d23e7 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1$1$1")
        /* renamed from: com.portfolio.platform.data.source.ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1$1$1 */
        public static final class C57301 implements java.lang.Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ com.portfolio.platform.data.source.C5728x974d23e7.C57291 this$0;

            @DexIgnore
            public C57301(com.portfolio.platform.data.source.C5728x974d23e7.C57291 r1) {
                this.this$0 = r1;
            }

            @DexIgnore
            public final void run() {
                this.this$0.this$0.this$0.getMThirdPartyDatabase().getGFitSleepDao().deleteListGFitSleep(this.this$0.this$0.$listInsertGFitSuccessFul$inlined);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C57291(com.portfolio.platform.data.source.C5728x974d23e7 thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$1;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.data.source.C5728x974d23e7.C57291 r0 = new com.portfolio.platform.data.source.C5728x974d23e7.C57291(this.this$0, yb4);
            r0.f21099p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.data.source.C5728x974d23e7.C57291) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local.mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "Delete " + this.this$0.$gFitSLeepData + " after pushing GoogleFit successful!");
                this.this$0.this$0.getMThirdPartyDatabase().runInTransaction((java.lang.Runnable) new com.portfolio.platform.data.source.C5728x974d23e7.C57291.C57301(this));
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public C5728x974d23e7(com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep gFitSleep, com.fossil.blesdk.obfuscated.ap0 ap0, com.google.android.gms.auth.api.signin.GoogleSignInAccount googleSignInAccount, kotlin.jvm.internal.Ref$IntRef ref$IntRef, java.util.ArrayList arrayList, int i, com.fossil.blesdk.obfuscated.dg4 dg4, com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository, java.util.List list, java.lang.String str) {
        this.$gFitSLeepData = gFitSleep;
        this.$device$inlined = ap0;
        this.$googleSignInAccount$inlined = googleSignInAccount;
        this.$countSizeOfLists$inlined = ref$IntRef;
        this.$listInsertGFitSuccessFul$inlined = arrayList;
        this.$sizeOfListsOfGFitSleepData$inlined = i;
        this.$continuation$inlined = dg4;
        this.this$0 = thirdPartyRepository;
        this.$gFitSLeepDataList$inlined = list;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    public final void onSuccess(java.lang.Void voidR) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        local.mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "Insert " + this.$gFitSLeepData + " to GoogleFit successful!");
        kotlin.jvm.internal.Ref$IntRef ref$IntRef = this.$countSizeOfLists$inlined;
        ref$IntRef.element = ref$IntRef.element + 1;
        this.$listInsertGFitSuccessFul$inlined.add(this.$gFitSLeepData);
        if (this.$countSizeOfLists$inlined.element >= this.$sizeOfListsOfGFitSleepData$inlined && this.$continuation$inlined.isActive()) {
            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.data.source.C5728x974d23e7.C57291(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "End saveGFitSleepDataToGoogleFit");
            com.fossil.blesdk.obfuscated.dg4 dg4 = this.$continuation$inlined;
            kotlin.Result.C7350a aVar = kotlin.Result.Companion;
            dg4.resumeWith(kotlin.Result.m37419constructorimpl((java.lang.Object) null));
        }
    }
}
