package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hc;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.os3;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSampleRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public /* final */ HeartRateSampleDao mHeartRateSampleDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return HeartRateSampleRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = HeartRateSampleRepository.class.getSimpleName();
        kd4.a((Object) simpleName, "HeartRateSampleRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public HeartRateSampleRepository(HeartRateSampleDao heartRateSampleDao, FitnessDataDao fitnessDataDao, ApiServiceV2 apiServiceV2) {
        kd4.b(heartRateSampleDao, "mHeartRateSampleDao");
        kd4.b(fitnessDataDao, "mFitnessDataDao");
        kd4.b(apiServiceV2, "mApiService");
        this.mHeartRateSampleDao = heartRateSampleDao;
        this.mFitnessDataDao = fitnessDataDao;
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchHeartRateSamples$default(HeartRateSampleRepository heartRateSampleRepository, Date date, Date date2, int i, int i2, yb4 yb4, int i3, Object obj) {
        return heartRateSampleRepository.fetchHeartRateSamples(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, yb4);
    }

    @DexIgnore
    public final void cleanUp() {
        this.mHeartRateSampleDao.deleteAllHeartRateSamples();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x017c  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x01bc  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    public final Object fetchHeartRateSamples(Date date, Date date2, int i, int i2, yb4<? super qo2<ApiResponse<HeartRate>>> yb4) {
        HeartRateSampleRepository$fetchHeartRateSamples$Anon1 heartRateSampleRepository$fetchHeartRateSamples$Anon1;
        int i3;
        qo2 qo2;
        qo2 qo22;
        Object obj;
        int i4;
        HeartRateSampleRepository heartRateSampleRepository;
        Date date3;
        int i5;
        Date date4 = date;
        Date date5 = date2;
        yb4<? super qo2<ApiResponse<HeartRate>>> yb42 = yb4;
        if (yb42 instanceof HeartRateSampleRepository$fetchHeartRateSamples$Anon1) {
            heartRateSampleRepository$fetchHeartRateSamples$Anon1 = (HeartRateSampleRepository$fetchHeartRateSamples$Anon1) yb42;
            int i6 = heartRateSampleRepository$fetchHeartRateSamples$Anon1.label;
            if ((i6 & Integer.MIN_VALUE) != 0) {
                heartRateSampleRepository$fetchHeartRateSamples$Anon1.label = i6 - Integer.MIN_VALUE;
                HeartRateSampleRepository$fetchHeartRateSamples$Anon1 heartRateSampleRepository$fetchHeartRateSamples$Anon12 = heartRateSampleRepository$fetchHeartRateSamples$Anon1;
                Object obj2 = heartRateSampleRepository$fetchHeartRateSamples$Anon12.result;
                Object a = cc4.a();
                i3 = heartRateSampleRepository$fetchHeartRateSamples$Anon12.label;
                if (i3 != 0) {
                    na4.a(obj2);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "fetchHeartRateSamples: start = " + date4 + ", end = " + date5);
                    HeartRateSampleRepository$fetchHeartRateSamples$repoResponse$Anon1 heartRateSampleRepository$fetchHeartRateSamples$repoResponse$Anon1 = new HeartRateSampleRepository$fetchHeartRateSamples$repoResponse$Anon1(this, date, date2, i, i2, (yb4) null);
                    heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$Anon0 = this;
                    heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$Anon1 = date4;
                    heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$Anon2 = date5;
                    i5 = i;
                    heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$Anon0 = i5;
                    int i7 = i2;
                    heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$Anon1 = i7;
                    heartRateSampleRepository$fetchHeartRateSamples$Anon12.label = 1;
                    Object a2 = ResponseKt.a(heartRateSampleRepository$fetchHeartRateSamples$repoResponse$Anon1, heartRateSampleRepository$fetchHeartRateSamples$Anon12);
                    if (a2 == a) {
                        return a;
                    }
                    i4 = i7;
                    obj2 = a2;
                    date3 = date5;
                    heartRateSampleRepository = this;
                } else if (i3 == 1) {
                    int i8 = heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$Anon1;
                    i5 = heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$Anon0;
                    date3 = (Date) heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$Anon2;
                    na4.a(obj2);
                    i4 = i8;
                    date4 = (Date) heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$Anon1;
                    heartRateSampleRepository = (HeartRateSampleRepository) heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$Anon0;
                } else if (i3 == 2) {
                    qo22 = (qo2) heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$Anon3;
                    int i9 = heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$Anon1;
                    int i10 = heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$Anon0;
                    Date date6 = (Date) heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$Anon2;
                    Date date7 = (Date) heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$Anon1;
                    HeartRateSampleRepository heartRateSampleRepository2 = (HeartRateSampleRepository) heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$Anon0;
                    try {
                        na4.a(obj2);
                        obj = obj2;
                        return (qo2) obj;
                    } catch (Exception e) {
                        e = e;
                        qo2 = qo22;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj2;
                String str2 = null;
                if (qo2 instanceof ro2) {
                    if (qo2 instanceof po2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str3 = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("fetchHeartRateSamples Failure code=");
                        po2 po2 = (po2) qo2;
                        sb.append(po2.a());
                        sb.append(" message=");
                        ServerError c = po2.c();
                        if (c != null) {
                            String message = c.getMessage();
                            if (message != null) {
                                str2 = message;
                                if (str2 == null) {
                                    str2 = "";
                                }
                                sb.append(str2);
                                local2.d(str3, sb.toString());
                            }
                        }
                        ServerError c2 = po2.c();
                        if (c2 != null) {
                            str2 = c2.getUserMessage();
                        }
                        if (str2 == null) {
                        }
                        sb.append(str2);
                        local2.d(str3, sb.toString());
                    }
                    return qo2;
                } else if (((ro2) qo2).a() == null) {
                    return qo2;
                } else {
                    try {
                        if (!((ro2) qo2).b()) {
                            ArrayList arrayList = new ArrayList();
                            for (HeartRate heartRateSample : ((ApiResponse) ((ro2) qo2).a()).get_items()) {
                                HeartRateSample heartRateSample2 = heartRateSample.toHeartRateSample();
                                if (heartRateSample2 != null) {
                                    arrayList.add(heartRateSample2);
                                }
                            }
                            if (!arrayList.isEmpty()) {
                                heartRateSampleRepository.mHeartRateSampleDao.upsertHeartRateSampleList(arrayList);
                            }
                        }
                        if (((ApiResponse) ((ro2) qo2).a()).get_range() == null) {
                            return qo2;
                        }
                        Range range = ((ApiResponse) ((ro2) qo2).a()).get_range();
                        if (range == null) {
                            kd4.a();
                            throw null;
                        } else if (!range.isHasNext()) {
                            return qo2;
                        } else {
                            heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$Anon0 = heartRateSampleRepository;
                            heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$Anon1 = date4;
                            heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$Anon2 = date3;
                            heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$Anon0 = i5;
                            heartRateSampleRepository$fetchHeartRateSamples$Anon12.I$Anon1 = i4;
                            heartRateSampleRepository$fetchHeartRateSamples$Anon12.L$Anon3 = qo2;
                            heartRateSampleRepository$fetchHeartRateSamples$Anon12.label = 2;
                            obj = heartRateSampleRepository.fetchHeartRateSamples(date4, date3, i5 + i4, i4, heartRateSampleRepository$fetchHeartRateSamples$Anon12);
                            if (obj == a) {
                                return a;
                            }
                            qo22 = qo2;
                            return (qo2) obj;
                        }
                    } catch (Exception e2) {
                        e = e2;
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str4 = TAG;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("fetchHeartRateSamples exception=");
                        e.printStackTrace();
                        sb2.append(qa4.a);
                        local3.e(str4, sb2.toString());
                        return qo2;
                    }
                }
            }
        }
        heartRateSampleRepository$fetchHeartRateSamples$Anon1 = new HeartRateSampleRepository$fetchHeartRateSamples$Anon1(this, yb42);
        HeartRateSampleRepository$fetchHeartRateSamples$Anon1 heartRateSampleRepository$fetchHeartRateSamples$Anon122 = heartRateSampleRepository$fetchHeartRateSamples$Anon1;
        Object obj22 = heartRateSampleRepository$fetchHeartRateSamples$Anon122.result;
        Object a3 = cc4.a();
        i3 = heartRateSampleRepository$fetchHeartRateSamples$Anon122.label;
        if (i3 != 0) {
        }
        qo2 = (qo2) obj22;
        String str22 = null;
        if (qo2 instanceof ro2) {
        }
    }

    @DexIgnore
    public final LiveData<os3<List<HeartRateSample>>> getHeartRateSamples(Date date, Date date2, boolean z) {
        kd4.b(date, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        kd4.b(date2, "end");
        Date n = rk2.n(date);
        Date i = rk2.i(date2);
        FitnessDataDao fitnessDataDao = this.mFitnessDataDao;
        kd4.a((Object) n, GoalPhase.COLUMN_START_DATE);
        kd4.a((Object) i, GoalPhase.COLUMN_END_DATE);
        LiveData<os3<List<HeartRateSample>>> b = hc.b(fitnessDataDao.getFitnessDataLiveData(n, i), new HeartRateSampleRepository$getHeartRateSamples$Anon1(this, n, i, z, date2));
        kd4.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final void insertFromDevice(List<HeartRateSample> list) {
        kd4.b(list, "heartRateSamples");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insertFromDevice: heartRateSamples = " + list);
        this.mHeartRateSampleDao.insertHeartRateSamples(list);
    }
}
