package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.dg;
import com.fossil.blesdk.obfuscated.kg;
import com.fossil.blesdk.obfuscated.lf;
import com.fossil.blesdk.obfuscated.u72;
import com.fossil.blesdk.obfuscated.uf;
import com.fossil.blesdk.obfuscated.wf;
import com.portfolio.platform.data.model.diana.Complication;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationDao_Impl implements ComplicationDao {
    @DexIgnore
    public /* final */ RoomDatabase __db;
    @DexIgnore
    public /* final */ lf __insertionAdapterOfComplication;
    @DexIgnore
    public /* final */ wf __preparedStmtOfClearAll;
    @DexIgnore
    public /* final */ u72 __stringArrayConverter; // = new u72();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends lf<Complication> {
        @DexIgnore
        public Anon1(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `complication`(`complicationId`,`name`,`nameKey`,`categories`,`description`,`descriptionKey`,`icon`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(kg kgVar, Complication complication) {
            if (complication.getComplicationId() == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, complication.getComplicationId());
            }
            if (complication.getName() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, complication.getName());
            }
            if (complication.getNameKey() == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, complication.getNameKey());
            }
            String a = ComplicationDao_Impl.this.__stringArrayConverter.a(complication.getCategories());
            if (a == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, a);
            }
            if (complication.getDescription() == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, complication.getDescription());
            }
            if (complication.getDescriptionKey() == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, complication.getDescriptionKey());
            }
            if (complication.getIcon() == null) {
                kgVar.a(7);
            } else {
                kgVar.a(7, complication.getIcon());
            }
            if (complication.getCreatedAt() == null) {
                kgVar.a(8);
            } else {
                kgVar.a(8, complication.getCreatedAt());
            }
            if (complication.getUpdatedAt() == null) {
                kgVar.a(9);
            } else {
                kgVar.a(9, complication.getUpdatedAt());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends wf {
        @DexIgnore
        public Anon2(RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM complication";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<Complication>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf val$_statement;

        @DexIgnore
        public Anon3(uf ufVar) {
            this.val$_statement = ufVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<Complication> call() throws Exception {
            Cursor a = bg.a(ComplicationDao_Impl.this.__db, this.val$_statement, false);
            try {
                int b = ag.b(a, "complicationId");
                int b2 = ag.b(a, "name");
                int b3 = ag.b(a, "nameKey");
                int b4 = ag.b(a, "categories");
                int b5 = ag.b(a, "description");
                int b6 = ag.b(a, "descriptionKey");
                int b7 = ag.b(a, "icon");
                int b8 = ag.b(a, "createdAt");
                int b9 = ag.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new Complication(a.getString(b), a.getString(b2), a.getString(b3), ComplicationDao_Impl.this.__stringArrayConverter.a(a.getString(b4)), a.getString(b5), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public ComplicationDao_Impl(RoomDatabase roomDatabase) {
        this.__db = roomDatabase;
        this.__insertionAdapterOfComplication = new Anon1(roomDatabase);
        this.__preparedStmtOfClearAll = new Anon2(roomDatabase);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        kg acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.n();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public List<Complication> getAllComplications() {
        uf b = uf.b("SELECT * FROM complication", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "complicationId");
            int b3 = ag.b(a, "name");
            int b4 = ag.b(a, "nameKey");
            int b5 = ag.b(a, "categories");
            int b6 = ag.b(a, "description");
            int b7 = ag.b(a, "descriptionKey");
            int b8 = ag.b(a, "icon");
            int b9 = ag.b(a, "createdAt");
            int b10 = ag.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new Complication(a.getString(b2), a.getString(b3), a.getString(b4), this.__stringArrayConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9), a.getString(b10)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<Complication>> getAllComplicationsAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"complication"}, false, new Anon3(uf.b("SELECT * FROM complication", 0)));
    }

    @DexIgnore
    public Complication getComplicationById(String str) {
        Complication complication;
        String str2 = str;
        uf b = uf.b("SELECT * FROM complication WHERE complicationId=?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "complicationId");
            int b3 = ag.b(a, "name");
            int b4 = ag.b(a, "nameKey");
            int b5 = ag.b(a, "categories");
            int b6 = ag.b(a, "description");
            int b7 = ag.b(a, "descriptionKey");
            int b8 = ag.b(a, "icon");
            int b9 = ag.b(a, "createdAt");
            int b10 = ag.b(a, "updatedAt");
            if (a.moveToFirst()) {
                complication = new Complication(a.getString(b2), a.getString(b3), a.getString(b4), this.__stringArrayConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9), a.getString(b10));
            } else {
                complication = null;
            }
            return complication;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<Complication> getComplicationByIds(List<String> list) {
        StringBuilder a = dg.a();
        a.append("SELECT * FROM complication WHERE complicationId IN (");
        int size = list.size();
        dg.a(a, size);
        a.append(")");
        uf b = uf.b(a.toString(), size + 0);
        int i = 1;
        for (String next : list) {
            if (next == null) {
                b.a(i);
            } else {
                b.a(i, next);
            }
            i++;
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a2, "complicationId");
            int b3 = ag.b(a2, "name");
            int b4 = ag.b(a2, "nameKey");
            int b5 = ag.b(a2, "categories");
            int b6 = ag.b(a2, "description");
            int b7 = ag.b(a2, "descriptionKey");
            int b8 = ag.b(a2, "icon");
            int b9 = ag.b(a2, "createdAt");
            int b10 = ag.b(a2, "updatedAt");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(new Complication(a2.getString(b2), a2.getString(b3), a2.getString(b4), this.__stringArrayConverter.a(a2.getString(b5)), a2.getString(b6), a2.getString(b7), a2.getString(b8), a2.getString(b9), a2.getString(b10)));
            }
            return arrayList;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public List<Complication> queryComplicationByName(String str) {
        String str2 = str;
        uf b = uf.b("SELECT * FROM complication WHERE name LIKE '%' || ? || '%'", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bg.a(this.__db, b, false);
        try {
            int b2 = ag.b(a, "complicationId");
            int b3 = ag.b(a, "name");
            int b4 = ag.b(a, "nameKey");
            int b5 = ag.b(a, "categories");
            int b6 = ag.b(a, "description");
            int b7 = ag.b(a, "descriptionKey");
            int b8 = ag.b(a, "icon");
            int b9 = ag.b(a, "createdAt");
            int b10 = ag.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new Complication(a.getString(b2), a.getString(b3), a.getString(b4), this.__stringArrayConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9), a.getString(b10)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertComplicationList(List<Complication> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfComplication.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
