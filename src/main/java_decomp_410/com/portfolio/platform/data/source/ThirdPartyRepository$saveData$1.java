package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ThirdPartyRepository$saveData$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime $gFitActiveTime;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $listGFitHeartRate;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $listGFitSample;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $listGFitWorkoutSession;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $listMFSleepSession;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $listUASample;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.ThirdPartyRepository this$0;

    @DexIgnore
    public ThirdPartyRepository$saveData$1(com.portfolio.platform.data.source.ThirdPartyRepository thirdPartyRepository, java.util.List list, com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime gFitActiveTime, java.util.List list2, java.util.List list3, java.util.List list4, java.util.List list5) {
        this.this$0 = thirdPartyRepository;
        this.$listGFitSample = list;
        this.$gFitActiveTime = gFitActiveTime;
        this.$listGFitHeartRate = list2;
        this.$listGFitWorkoutSession = list3;
        this.$listUASample = list4;
        this.$listMFSleepSession = list5;
    }

    @DexIgnore
    public final void run() {
        if (!this.$listGFitSample.isEmpty()) {
            this.this$0.getMThirdPartyDatabase().getGFitSampleDao().insertListGFitSample(this.$listGFitSample);
        }
        if (this.$gFitActiveTime != null) {
            this.this$0.getMThirdPartyDatabase().getGFitActiveTimeDao().insertGFitActiveTime(this.$gFitActiveTime);
        }
        if (!this.$listGFitHeartRate.isEmpty()) {
            this.this$0.getMThirdPartyDatabase().getGFitHeartRateDao().insertListGFitHeartRate(this.$listGFitHeartRate);
        }
        if (!this.$listGFitWorkoutSession.isEmpty()) {
            this.this$0.getMThirdPartyDatabase().getGFitWorkoutSessionDao().insertListGFitWorkoutSession(this.$listGFitWorkoutSession);
        }
        if (!this.$listUASample.isEmpty()) {
            this.this$0.getMThirdPartyDatabase().getUASampleDao().insertListUASample(this.$listUASample);
        }
        if (!this.$listMFSleepSession.isEmpty()) {
            this.this$0.getMThirdPartyDatabase().getGFitSleepDao().insertListGFitSleep(this.this$0.convertListMFSleepSessionToListGFitSleep(this.$listMFSleepSession));
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.ThirdPartyRepository.TAG, "listMFSleepSession.isNotEmpty");
        }
    }
}
