package com.portfolio.platform.data.source.local.diana.heartrate;

import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.helper.PagingRequestHelper;
import java.util.Date;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateSummaryLocalDataSource$loadAfter$Anon1 implements PagingRequestHelper.b {
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryLocalDataSource this$Anon0;

    @DexIgnore
    public HeartRateSummaryLocalDataSource$loadAfter$Anon1(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        this.this$Anon0 = heartRateSummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(PagingRequestHelper.b.a aVar) {
        Pair pair = (Pair) kb4.d(this.this$Anon0.mRequestAfterQueue);
        kd4.a((Object) aVar, "helperCallback");
        fi4 unused = this.this$Anon0.loadData((Date) pair.getFirst(), (Date) pair.getSecond(), aVar);
    }
}
