package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$2", mo27670f = "DeviceRepository.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class DeviceRepository$downloadDeviceList$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.qo2 $response;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21084p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.DeviceRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRepository$downloadDeviceList$2(com.portfolio.platform.data.source.DeviceRepository deviceRepository, com.fossil.blesdk.obfuscated.qo2 qo2, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = deviceRepository;
        this.$response = qo2;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$2 deviceRepository$downloadDeviceList$2 = new com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$2(this.this$0, this.$response, yb4);
        deviceRepository$downloadDeviceList$2.f21084p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return deviceRepository$downloadDeviceList$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.source.remote.ApiResponse apiResponse = (com.portfolio.platform.data.source.remote.ApiResponse) ((com.fossil.blesdk.obfuscated.ro2) this.$response).mo30673a();
            java.util.List<com.portfolio.platform.data.model.Device> list = apiResponse != null ? apiResponse.get_items() : null;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tag = com.portfolio.platform.data.source.DeviceRepository.Companion.getTAG();
            local.mo33255d(tag, "downloadDeviceList() - isFromCache = false " + list);
            this.this$0.removeStealDevice(list);
            if (list != null) {
                for (com.portfolio.platform.data.model.Device device : list) {
                    if (com.portfolio.platform.helper.DeviceHelper.f21188o.mo39566e(device.getDeviceId())) {
                        com.portfolio.platform.data.model.Device deviceByDeviceId = this.this$0.mDeviceDao.getDeviceByDeviceId(device.getDeviceId());
                        if (deviceByDeviceId != null) {
                            if (device.getBatteryLevel() <= 0) {
                                device.setBatteryLevel(deviceByDeviceId.getBatteryLevel());
                            }
                            device.setVibrationStrength(deviceByDeviceId.getVibrationStrength());
                        }
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String tag2 = com.portfolio.platform.data.source.DeviceRepository.Companion.getTAG();
                        local2.mo33255d(tag2, "update device: " + device);
                        this.this$0.mDeviceDao.addOrUpdateDevice(device);
                    } else {
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                        java.lang.String tag3 = com.portfolio.platform.data.source.DeviceRepository.Companion.getTAG();
                        local3.mo33255d(tag3, "Ignoring legacy device=" + device.getDeviceId());
                    }
                }
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
