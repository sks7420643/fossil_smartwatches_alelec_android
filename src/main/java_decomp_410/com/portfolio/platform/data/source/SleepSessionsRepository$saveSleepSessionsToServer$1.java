package com.portfolio.platform.data.source;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$1", mo27670f = "SleepSessionsRepository.kt", mo27671l = {282}, mo27672m = "invokeSuspend")
public final class SleepSessionsRepository$saveSleepSessionsToServer$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $allSleepSessionList;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback $pushPendingSleepSessionsCallback;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $userId;
    @DexIgnore
    public int I$0;
    @DexIgnore
    public int I$1;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21090p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.data.source.SleepSessionsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$saveSleepSessionsToServer$1(com.portfolio.platform.data.source.SleepSessionsRepository sleepSessionsRepository, java.util.List list, java.lang.String str, com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = sleepSessionsRepository;
        this.$allSleepSessionList = list;
        this.$userId = str;
        this.$pushPendingSleepSessionsCallback = pushPendingSleepSessionsCallback;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$1 sleepSessionsRepository$saveSleepSessionsToServer$1 = new com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$1(this.this$0, this.$allSleepSessionList, this.$userId, this.$pushPendingSleepSessionsCallback, yb4);
        sleepSessionsRepository$saveSleepSessionsToServer$1.f21090p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return sleepSessionsRepository$saveSleepSessionsToServer$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x01b8, code lost:
        if (r13.intValue() != 409000) goto L_0x01ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x01ce, code lost:
        if (r13.intValue() != 409001) goto L_0x01d0;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x022e  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x023f  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x005a  */
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.util.List list;
        java.lang.Object obj2;
        java.lang.Object obj3;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        int i;
        java.util.List list2;
        int i2;
        com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$1 sleepSessionsRepository$saveSleepSessionsToServer$1;
        com.fossil.blesdk.obfuscated.qo2 qo2;
        com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$1 sleepSessionsRepository$saveSleepSessionsToServer$12;
        java.lang.Object obj4;
        java.util.ArrayList arrayList;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i3 = this.label;
        int i4 = 0;
        boolean z = true;
        if (i3 == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f21090p$;
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.data.source.SleepSessionsRepository.Companion.getTAG$app_fossilRelease(), "saveSleepSessionsToServer");
            zg4 = zg42;
            list2 = new java.util.ArrayList();
            i = 0;
            obj4 = a;
            sleepSessionsRepository$saveSleepSessionsToServer$12 = this;
        } else if (i3 == 1) {
            i2 = this.I$1;
            list2 = (java.util.List) this.L$1;
            i = this.I$0;
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            obj2 = obj;
            obj3 = a;
            list = (java.util.List) this.L$2;
            sleepSessionsRepository$saveSleepSessionsToServer$1 = this;
            qo2 = (com.fossil.blesdk.obfuscated.qo2) obj2;
            i += 100;
            if (qo2 instanceof com.fossil.blesdk.obfuscated.ro2) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String tAG$app_fossilRelease = com.portfolio.platform.data.source.SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
                local.mo33255d(tAG$app_fossilRelease, "saveSleepSessionsToServer success, bravo!!! startIndex=" + i + " endIndex=" + i2);
                java.lang.Object a2 = ((com.fossil.blesdk.obfuscated.ro2) qo2).mo30673a();
                if (a2 != null) {
                    java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession> list3 = (java.util.List) a2;
                    for (com.portfolio.platform.data.model.room.sleep.MFSleepSession pinType : list3) {
                        pinType.setPinType(i4);
                    }
                    sleepSessionsRepository$saveSleepSessionsToServer$1.this$0.mSleepDao.upsertSleepSessionList(list3);
                    list2.addAll(list3);
                    if (i >= sleepSessionsRepository$saveSleepSessionsToServer$1.$allSleepSessionList.size()) {
                        com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback = sleepSessionsRepository$saveSleepSessionsToServer$1.$pushPendingSleepSessionsCallback;
                        if (pushPendingSleepSessionsCallback != null) {
                            pushPendingSleepSessionsCallback.onSuccess(list2);
                        }
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                }
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            } else if (qo2 instanceof com.fossil.blesdk.obfuscated.po2) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String tAG$app_fossilRelease2 = com.portfolio.platform.data.source.SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append("saveSleepSessionsToServer failed, errorCode=");
                com.fossil.blesdk.obfuscated.po2 po2 = (com.fossil.blesdk.obfuscated.po2) qo2;
                sb.append(po2.mo30176a());
                sb.append(' ');
                sb.append("startIndex=");
                sb.append(i);
                sb.append(" endIndex=");
                sb.append(i2);
                local2.mo33255d(tAG$app_fossilRelease2, sb.toString());
                if (po2.mo30176a() == 422) {
                    arrayList = new java.util.ArrayList();
                    if (!android.text.TextUtils.isEmpty(po2.mo30177b())) {
                        try {
                        } catch (java.lang.Exception e) {
                            e = e;
                        }
                        java.lang.Object a3 = new com.google.gson.Gson().mo23094a(((com.fossil.blesdk.obfuscated.po2) qo2).mo30177b(), new com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$1$type$1().getType());
                        com.fossil.blesdk.obfuscated.kd4.m24407a(a3, "Gson().fromJson(repoResponse.errorItems, type)");
                        java.util.List list4 = ((com.portfolio.platform.data.source.remote.UpsertApiResponse) a3).get_items();
                        if (list4.isEmpty() ^ z) {
                            int size = list4.size();
                            int i5 = 0;
                            while (i5 < size) {
                                java.lang.Integer code = ((com.portfolio.platform.data.SleepSession) list4.get(i5)).getCode();
                                if (code == null) {
                                }
                                java.lang.Integer code2 = ((com.portfolio.platform.data.SleepSession) list4.get(i5)).getCode();
                                if (code2 == null) {
                                }
                                if (((com.portfolio.platform.data.SleepSession) list4.get(i5)).getRealEndTime() != null) {
                                    com.portfolio.platform.data.model.room.sleep.MFSleepSession mFSleepSession = (com.portfolio.platform.data.model.room.sleep.MFSleepSession) list.get(i5);
                                    try {
                                        mFSleepSession.setPinType(0);
                                        arrayList.add(mFSleepSession);
                                    } catch (java.lang.Exception e2) {
                                        e = e2;
                                    }
                                    i5++;
                                } else {
                                    i5++;
                                }
                            }
                        }
                        sleepSessionsRepository$saveSleepSessionsToServer$1.this$0.mSleepDao.upsertSleepSessionList(arrayList);
                        list2.addAll(list);
                        if (i >= sleepSessionsRepository$saveSleepSessionsToServer$1.$allSleepSessionList.size()) {
                            com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback2 = sleepSessionsRepository$saveSleepSessionsToServer$1.$pushPendingSleepSessionsCallback;
                            if (pushPendingSleepSessionsCallback2 != null) {
                                pushPendingSleepSessionsCallback2.onSuccess(list2);
                            }
                            return com.fossil.blesdk.obfuscated.qa4.f17909a;
                        }
                        if (i >= sleepSessionsRepository$saveSleepSessionsToServer$1.$allSleepSessionList.size()) {
                            com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback3 = sleepSessionsRepository$saveSleepSessionsToServer$1.$pushPendingSleepSessionsCallback;
                            if (pushPendingSleepSessionsCallback3 != null) {
                                pushPendingSleepSessionsCallback3.onFail(po2.mo30176a());
                            }
                            return com.fossil.blesdk.obfuscated.qa4.f17909a;
                        }
                        sleepSessionsRepository$saveSleepSessionsToServer$12 = sleepSessionsRepository$saveSleepSessionsToServer$1;
                        obj4 = obj3;
                        i4 = 0;
                        z = true;
                    }
                }
                if (i >= sleepSessionsRepository$saveSleepSessionsToServer$1.$allSleepSessionList.size()) {
                }
                sleepSessionsRepository$saveSleepSessionsToServer$12 = sleepSessionsRepository$saveSleepSessionsToServer$1;
                obj4 = obj3;
                i4 = 0;
                z = true;
            }
            sleepSessionsRepository$saveSleepSessionsToServer$12 = sleepSessionsRepository$saveSleepSessionsToServer$1;
            obj4 = obj3;
            i4 = 0;
            z = true;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (i < sleepSessionsRepository$saveSleepSessionsToServer$12.$allSleepSessionList.size()) {
            i2 = i + 100;
            if (i2 > sleepSessionsRepository$saveSleepSessionsToServer$12.$allSleepSessionList.size()) {
                i2 = sleepSessionsRepository$saveSleepSessionsToServer$12.$allSleepSessionList.size();
            }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tAG$app_fossilRelease3 = com.portfolio.platform.data.source.SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
            local3.mo33255d(tAG$app_fossilRelease3, "saveSleepSessionsToServer startIndex=" + i + " endIndex=" + i2);
            java.util.List subList = sleepSessionsRepository$saveSleepSessionsToServer$12.$allSleepSessionList.subList(i, i2);
            com.portfolio.platform.data.source.SleepSessionsRepository sleepSessionsRepository = sleepSessionsRepository$saveSleepSessionsToServer$12.this$0;
            java.lang.String str = sleepSessionsRepository$saveSleepSessionsToServer$12.$userId;
            sleepSessionsRepository$saveSleepSessionsToServer$12.L$0 = zg4;
            sleepSessionsRepository$saveSleepSessionsToServer$12.I$0 = i;
            sleepSessionsRepository$saveSleepSessionsToServer$12.L$1 = list2;
            sleepSessionsRepository$saveSleepSessionsToServer$12.I$1 = i2;
            sleepSessionsRepository$saveSleepSessionsToServer$12.L$2 = subList;
            sleepSessionsRepository$saveSleepSessionsToServer$12.label = z ? 1 : 0;
            obj2 = sleepSessionsRepository.insertSleepSessionList(str, subList, sleepSessionsRepository$saveSleepSessionsToServer$12);
            if (obj2 == obj4) {
                return obj4;
            }
            list = subList;
            obj3 = obj4;
            sleepSessionsRepository$saveSleepSessionsToServer$1 = sleepSessionsRepository$saveSleepSessionsToServer$12;
            qo2 = (com.fossil.blesdk.obfuscated.qo2) obj2;
            i += 100;
            if (qo2 instanceof com.fossil.blesdk.obfuscated.ro2) {
            }
            sleepSessionsRepository$saveSleepSessionsToServer$12 = sleepSessionsRepository$saveSleepSessionsToServer$1;
            obj4 = obj3;
            i4 = 0;
            z = true;
            if (i < sleepSessionsRepository$saveSleepSessionsToServer$12.$allSleepSessionList.size()) {
            }
            return obj4;
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String tAG$app_fossilRelease4 = com.portfolio.platform.data.source.SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
        java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
        sb2.append("saveSleepSessionsToServer ex=");
        e.printStackTrace();
        sb2.append(com.fossil.blesdk.obfuscated.qa4.f17909a);
        local4.mo33256e(tAG$app_fossilRelease4, sb2.toString());
        sleepSessionsRepository$saveSleepSessionsToServer$1.this$0.mSleepDao.upsertSleepSessionList(arrayList);
        list2.addAll(list);
        if (i >= sleepSessionsRepository$saveSleepSessionsToServer$1.$allSleepSessionList.size()) {
        }
        if (i >= sleepSessionsRepository$saveSleepSessionsToServer$1.$allSleepSessionList.size()) {
        }
        sleepSessionsRepository$saveSleepSessionsToServer$12 = sleepSessionsRepository$saveSleepSessionsToServer$1;
        obj4 = obj3;
        i4 = 0;
        z = true;
        if (i < sleepSessionsRepository$saveSleepSessionsToServer$12.$allSleepSessionList.size()) {
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
