package com.portfolio.platform.data.source.remote;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.po2;
import com.fossil.blesdk.obfuscated.qo2;
import com.fossil.blesdk.obfuscated.ro2;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppRemoteDataSource {
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore
    public WatchAppRemoteDataSource(ApiServiceV2 apiServiceV2) {
        kd4.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object getAllWatchApp(String str, yb4<? super qo2<List<WatchApp>>> yb4) {
        WatchAppRemoteDataSource$getAllWatchApp$Anon1 watchAppRemoteDataSource$getAllWatchApp$Anon1;
        int i;
        qo2 qo2;
        if (yb4 instanceof WatchAppRemoteDataSource$getAllWatchApp$Anon1) {
            watchAppRemoteDataSource$getAllWatchApp$Anon1 = (WatchAppRemoteDataSource$getAllWatchApp$Anon1) yb4;
            int i2 = watchAppRemoteDataSource$getAllWatchApp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                watchAppRemoteDataSource$getAllWatchApp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = watchAppRemoteDataSource$getAllWatchApp$Anon1.result;
                Object a = cc4.a();
                i = watchAppRemoteDataSource$getAllWatchApp$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    WatchAppRemoteDataSource$getAllWatchApp$response$Anon1 watchAppRemoteDataSource$getAllWatchApp$response$Anon1 = new WatchAppRemoteDataSource$getAllWatchApp$response$Anon1(this, str, (yb4) null);
                    watchAppRemoteDataSource$getAllWatchApp$Anon1.L$Anon0 = this;
                    watchAppRemoteDataSource$getAllWatchApp$Anon1.L$Anon1 = str;
                    watchAppRemoteDataSource$getAllWatchApp$Anon1.label = 1;
                    obj = ResponseKt.a(watchAppRemoteDataSource$getAllWatchApp$response$Anon1, watchAppRemoteDataSource$getAllWatchApp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    String str2 = (String) watchAppRemoteDataSource$getAllWatchApp$Anon1.L$Anon1;
                    WatchAppRemoteDataSource watchAppRemoteDataSource = (WatchAppRemoteDataSource) watchAppRemoteDataSource$getAllWatchApp$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qo2 = (qo2) obj;
                if (!(qo2 instanceof ro2)) {
                    ArrayList arrayList = new ArrayList();
                    ro2 ro2 = (ro2) qo2;
                    ApiResponse apiResponse = (ApiResponse) ro2.a();
                    if (apiResponse != null) {
                        List list = apiResponse.get_items();
                        if (list != null) {
                            dc4.a(arrayList.addAll(list));
                        }
                    }
                    return new ro2(arrayList, ro2.b());
                } else if (qo2 instanceof po2) {
                    po2 po2 = (po2) qo2;
                    return new po2(po2.a(), po2.c(), po2.d(), (String) null, 8, (fd4) null);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
        watchAppRemoteDataSource$getAllWatchApp$Anon1 = new WatchAppRemoteDataSource$getAllWatchApp$Anon1(this, yb4);
        Object obj2 = watchAppRemoteDataSource$getAllWatchApp$Anon1.result;
        Object a2 = cc4.a();
        i = watchAppRemoteDataSource$getAllWatchApp$Anon1.label;
        if (i != 0) {
        }
        qo2 = (qo2) obj2;
        if (!(qo2 instanceof ro2)) {
        }
    }
}
