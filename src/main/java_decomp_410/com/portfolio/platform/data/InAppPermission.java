package com.portfolio.platform.data;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class InAppPermission {
    @DexIgnore
    public static /* final */ String ACCESS_BACKGROUND_LOCATION; // = "ACCESS_BACKGROUND_LOCATION";
    @DexIgnore
    public static /* final */ String ACCESS_FINE_LOCATION; // = "ACCESS_FINE_LOCATION";
    @DexIgnore
    public static /* final */ String BLUETOOTH; // = "BLUETOOTH";
    @DexIgnore
    public static /* final */ String CAMERA; // = "CAMERA";
    @DexIgnore
    public static /* final */ InAppPermission INSTANCE; // = new InAppPermission();
    @DexIgnore
    public static /* final */ String LOCATION_SERVICE; // = "LOCATION_SERVICE";
    @DexIgnore
    public static /* final */ String NOTIFICATION_ACCESS; // = "NOTIFICATION_ACCESS";
    @DexIgnore
    public static /* final */ String READ_CONTACTS; // = "READ_CONTACTS";
    @DexIgnore
    public static /* final */ String READ_PHONE_STATE; // = "READ_PHONE_STATE";
    @DexIgnore
    public static /* final */ String READ_SMS; // = "READ_SMS";
    @DexIgnore
    public static /* final */ String WRITE_EXTERNAL_STORAGE; // = "WRITE_EXTERNAL_STORAGE";
}
