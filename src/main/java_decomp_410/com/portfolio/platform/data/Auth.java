package com.portfolio.platform.data;

import com.fossil.blesdk.obfuscated.f02;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Auth {
    @DexIgnore
    @f02("accessToken")
    public /* final */ String mAccessToken;
    @DexIgnore
    @f02("accessTokenExpiresAt")
    public /* final */ Date mAccessTokenExpiresAt;
    @DexIgnore
    @f02("accessTokenExpiresIn")
    public /* final */ Integer mAccessTokenExpiresIn;
    @DexIgnore
    @f02("createdAt")
    public /* final */ String mCreatedAt;
    @DexIgnore
    @f02("objectId")
    public /* final */ String mObjectId;
    @DexIgnore
    @f02("openid")
    public /* final */ String mOpenid;
    @DexIgnore
    @f02("refreshToken")
    public /* final */ String mRefreshToken;
    @DexIgnore
    @f02("uid")
    public /* final */ String mUid;

    @DexIgnore
    public Auth(String str, String str2, Date date, Integer num, String str3, String str4, String str5, String str6) {
        this.mAccessToken = str;
        this.mRefreshToken = str2;
        this.mAccessTokenExpiresAt = date;
        this.mAccessTokenExpiresIn = num;
        this.mUid = str3;
        this.mCreatedAt = str4;
        this.mOpenid = str5;
        this.mObjectId = str6;
    }

    @DexIgnore
    public final String getAccessToken() {
        return this.mAccessToken;
    }

    @DexIgnore
    public final Date getAccessTokenExpiresAt() {
        return this.mAccessTokenExpiresAt;
    }

    @DexIgnore
    public final Integer getAccessTokenExpiresIn() {
        return this.mAccessTokenExpiresIn;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final String getObjectId() {
        return this.mObjectId;
    }

    @DexIgnore
    public final String getOpenid() {
        return this.mOpenid;
    }

    @DexIgnore
    public final String getRefreshToken() {
        return this.mRefreshToken;
    }

    @DexIgnore
    public final String getUid() {
        return this.mUid;
    }
}
