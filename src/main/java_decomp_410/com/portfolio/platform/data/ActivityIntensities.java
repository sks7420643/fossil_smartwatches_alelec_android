package com.portfolio.platform.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xk2;
import com.fossil.wearables.fsl.fitness.SampleDay;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ActivityIntensities implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    public double intense;
    @DexIgnore
    public double light;
    @DexIgnore
    public double moderate;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ActivityIntensities> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public ActivityIntensities createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new ActivityIntensities(parcel);
        }

        @DexIgnore
        public ActivityIntensities[] newArray(int i) {
            return new ActivityIntensities[i];
        }
    }

    @DexIgnore
    public ActivityIntensities(double d, double d2, double d3) {
        this.light = d;
        this.moderate = d2;
        this.intense = d3;
    }

    @DexIgnore
    public static /* synthetic */ ActivityIntensities copy$default(ActivityIntensities activityIntensities, double d, double d2, double d3, int i, Object obj) {
        if ((i & 1) != 0) {
            d = activityIntensities.light;
        }
        double d4 = d;
        if ((i & 2) != 0) {
            d2 = activityIntensities.moderate;
        }
        double d5 = d2;
        if ((i & 4) != 0) {
            d3 = activityIntensities.intense;
        }
        return activityIntensities.copy(d4, d5, d3);
    }

    @DexIgnore
    public final double component1() {
        return this.light;
    }

    @DexIgnore
    public final double component2() {
        return this.moderate;
    }

    @DexIgnore
    public final double component3() {
        return this.intense;
    }

    @DexIgnore
    public final ActivityIntensities copy(double d, double d2, double d3) {
        return new ActivityIntensities(d, d2, d3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActivityIntensities)) {
            return false;
        }
        ActivityIntensities activityIntensities = (ActivityIntensities) obj;
        return Double.compare(this.light, activityIntensities.light) == 0 && Double.compare(this.moderate, activityIntensities.moderate) == 0 && Double.compare(this.intense, activityIntensities.intense) == 0;
    }

    @DexIgnore
    public final double getIntense() {
        return this.intense;
    }

    @DexIgnore
    public final List<Integer> getIntensitiesInArray() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(Integer.valueOf((int) this.light));
        arrayList.add(Integer.valueOf((int) this.moderate));
        arrayList.add(Integer.valueOf((int) this.intense));
        return arrayList;
    }

    @DexIgnore
    public final double getLight() {
        return this.light;
    }

    @DexIgnore
    public final double getModerate() {
        return this.moderate;
    }

    @DexIgnore
    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.light);
        long doubleToLongBits2 = Double.doubleToLongBits(this.moderate);
        long doubleToLongBits3 = Double.doubleToLongBits(this.intense);
        return (((((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)))) * 31) + ((int) (doubleToLongBits3 ^ (doubleToLongBits3 >>> 32)));
    }

    @DexIgnore
    public final void setIntense(double d) {
        this.intense = d;
    }

    @DexIgnore
    public final void setIntensity(double d) {
        int a = xk2.d.a((long) d);
        if (a == 0) {
            this.light += d;
        } else if (a == 1) {
            this.moderate += d;
        } else if (a == 2) {
            this.intense += d;
        }
    }

    @DexIgnore
    public final void setLight(double d) {
        this.light = d;
    }

    @DexIgnore
    public final void setModerate(double d) {
        this.moderate = d;
    }

    @DexIgnore
    public String toString() {
        return "ActivityIntensities(light=" + this.light + ", moderate=" + this.moderate + ", intense=" + this.intense + ")";
    }

    @DexIgnore
    public final void updateActivityIntensities(ActivityIntensities activityIntensities) {
        kd4.b(activityIntensities, SampleDay.COLUMN_INTENSITIES);
        this.light += activityIntensities.light;
        this.moderate += activityIntensities.moderate;
        this.intense += activityIntensities.intense;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeDouble(this.light);
        parcel.writeDouble(this.moderate);
        parcel.writeDouble(this.intense);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ ActivityIntensities(double d, double d2, double d3, int i, fd4 fd4) {
        throw null;
/*        this(r2, (i & 2) != 0 ? 0.0d : d2, (i & 4) != 0 ? 0.0d : d3);
        double d4 = (i & 1) != 0 ? 0.0d : d;
*/    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityIntensities(Parcel parcel) {
        this(parcel.readDouble(), parcel.readDouble(), parcel.readDouble());
        kd4.b(parcel, "parcel");
    }

    @DexIgnore
    public ActivityIntensities() {
        this(0.0d, 0.0d, 0.0d);
    }
}
