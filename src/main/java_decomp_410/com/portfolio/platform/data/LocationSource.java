package com.portfolio.platform.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.Looper;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.places.model.PlaceFields;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.eg4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.ic4;
import com.fossil.blesdk.obfuscated.k6;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.oc1;
import com.fossil.blesdk.obfuscated.qc1;
import com.fossil.blesdk.obfuscated.sc1;
import com.fossil.blesdk.obfuscated.tc1;
import com.fossil.blesdk.obfuscated.ug4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zn1;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import kotlinx.coroutines.TimeoutKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LocationSource {
    @DexIgnore
    public static /* final */ int CACHE_THRESHOLD_TIME; // = 60000;
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ long LOCATION_WAITING_THRESHOLD_TIME; // = 5000;
    @DexIgnore
    public static /* final */ float SIGNIFICANT_DISPLACEMENT; // = 300.0f;
    @DexIgnore
    public static /* final */ float SMALLEST_DISPLACEMENT; // = 50.0f;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ int TWO_MINUTES; // = 120000;
    @DexIgnore
    public boolean isMonitoringObserved;
    @DexIgnore
    public boolean isSignificantObserved;
    @DexIgnore
    public long lastRetrievedTime; // = -1;
    @DexIgnore
    public Location locationCache;
    @DexIgnore
    public MonitoringLocationCallback mMonitoringCallback; // = new MonitoringLocationCallback();
    @DexIgnore
    public oc1 mMonitoringClient;
    @DexIgnore
    public List<LocationListener> mMonitoringLocationListeners; // = new ArrayList();
    @DexIgnore
    public SignificantLocationCallback mSignificantCallback; // = new SignificantLocationCallback();
    @DexIgnore
    public oc1 mSignificantClient;
    @DexIgnore
    public List<LocationListener> mSignificantLocationListeners; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return LocationSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public enum ErrorState {
        SUCCESS,
        LOCATION_PERMISSION_OFF,
        BACKGROUND_PERMISSION_OFF,
        LOCATION_SERVICE_OFF,
        UNKNOWN
    }

    @DexIgnore
    public interface LocationListener {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class DefaultImpls {
            @DexIgnore
            public static void onLocationResult(LocationListener locationListener, Location location) {
                kd4.b(location, PlaceFields.LOCATION);
            }
        }

        @DexIgnore
        void onLocationResult(Location location);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class MonitoringLocationCallback extends qc1 {
        @DexIgnore
        public MonitoringLocationCallback() {
        }

        @DexIgnore
        public void onLocationResult(LocationResult locationResult) {
            Location H = locationResult != null ? locationResult.H() : null;
            if (H != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
                local.d(tAG$app_fossilRelease, "MonitoringLocationCallback onLocationResult lastLocation=" + H);
                LocationSource locationSource = LocationSource.this;
                Calendar instance = Calendar.getInstance();
                kd4.a((Object) instance, "Calendar.getInstance()");
                locationSource.lastRetrievedTime = instance.getTimeInMillis();
                LocationSource.this.locationCache = H;
                for (LocationListener onLocationResult : LocationSource.this.mMonitoringLocationListeners) {
                    onLocationResult.onLocationResult(H);
                }
                return;
            }
            FLogger.INSTANCE.getLocal().d(LocationSource.Companion.getTAG$app_fossilRelease(), "MonitoringLocationCallback onLocationResult lastLocation is null");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Result {
        @DexIgnore
        public /* final */ ErrorState errorState;
        @DexIgnore
        public /* final */ boolean fromCache;
        @DexIgnore
        public /* final */ Location location;

        @DexIgnore
        public Result(Location location2, ErrorState errorState2, boolean z) {
            kd4.b(errorState2, "errorState");
            this.location = location2;
            this.errorState = errorState2;
            this.fromCache = z;
        }

        @DexIgnore
        public final ErrorState getErrorState() {
            return this.errorState;
        }

        @DexIgnore
        public final boolean getFromCache() {
            return this.fromCache;
        }

        @DexIgnore
        public final Location getLocation() {
            return this.location;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Result(Location location2, ErrorState errorState2, boolean z, int i, fd4 fd4) {
            this(location2, errorState2, (i & 4) != 0 ? false : z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SignificantLocationCallback extends qc1 {
        @DexIgnore
        public SignificantLocationCallback() {
        }

        @DexIgnore
        public void onLocationResult(LocationResult locationResult) {
            Location H = locationResult != null ? locationResult.H() : null;
            if (H != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
                local.d(tAG$app_fossilRelease, "SignificantLocationCallback onLocationResult lastLocation=" + H);
                LocationSource locationSource = LocationSource.this;
                Calendar instance = Calendar.getInstance();
                kd4.a((Object) instance, "Calendar.getInstance()");
                locationSource.lastRetrievedTime = instance.getTimeInMillis();
                LocationSource.this.locationCache = H;
                for (LocationListener onLocationResult : LocationSource.this.mSignificantLocationListeners) {
                    onLocationResult.onLocationResult(H);
                }
                return;
            }
            FLogger.INSTANCE.getLocal().d(LocationSource.Companion.getTAG$app_fossilRelease(), "SignificantLocationCallback onLocationResult lastLocation is null");
        }
    }

    /*
    static {
        String simpleName = LocationSource.class.getSimpleName();
        kd4.a((Object) simpleName, "LocationSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    private final Location isBetterLocation(Location location, Location location2) {
        if (location == null && location2 == null) {
            FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation both newLocation null and currentBestLocation null");
            return null;
        } else if (location != null && location2 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "isBetterLocation newLocation long=" + location.getLongitude() + " lat=" + location.getLatitude() + " time=" + location.getTime());
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "isBetterLocation currentBestLocation long=" + location2.getLongitude() + " lat=" + location2.getLatitude() + " time=" + location2.getTime());
            long time = location.getTime() - location2.getTime();
            boolean z = true;
            boolean z2 = time > ((long) 120000);
            boolean z3 = time < ((long) -120000);
            boolean z4 = time > 0;
            if (z2) {
                FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isSignificantlyNewer");
                return location;
            } else if (z3) {
                FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isSignificantlyOlder");
                return location2;
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.d(str3, "isBetterLocation accuracy location=" + location.getAccuracy() + " currentBestLocation=" + location2.getAccuracy());
                int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
                boolean z5 = accuracy > 0;
                boolean z6 = accuracy < 0;
                if (accuracy <= 200) {
                    z = false;
                }
                boolean isSameProvider = isSameProvider(location.getProvider(), location2.getProvider());
                if (z6) {
                    FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isMoreAccurate");
                    return location;
                } else if (z4 && !z5) {
                    FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isNewer && isLessAccurate=false");
                    return location;
                } else if (!z4 || z || !isSameProvider) {
                    return location2;
                } else {
                    FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isNewer && isSignificantlyLessAccurate=false && isFromSameProvider");
                    return location;
                }
            }
        } else if (location != null) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local4.d(str4, "isBetterLocation bestLocation long=" + location.getLongitude() + " lat=" + location.getLatitude() + " time=" + location.getTime());
            return location;
        } else {
            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            String str5 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("isBetterLocation bestLocation long=");
            if (location2 != null) {
                sb.append(location2.getLongitude());
                sb.append(" lat=");
                sb.append(location2.getLatitude());
                sb.append(" time=");
                sb.append(location2.getTime());
                local5.d(str5, sb.toString());
                return location2;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    private final boolean isSameProvider(String str, String str2) {
        FLogger.INSTANCE.getLocal().d(TAG, "isSameProvider");
        if (str == null) {
            return str2 == null;
        }
        return kd4.a((Object) str, (Object) str2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0107, code lost:
        return r9;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00fa  */
    public final synchronized Object getLocation(Context context, boolean z, yb4<? super Result> yb4) {
        LocationSource$getLocation$Anon1 locationSource$getLocation$Anon1;
        int i;
        Result result;
        LocationSource locationSource;
        Result result2;
        Location isBetterLocation;
        if (yb4 instanceof LocationSource$getLocation$Anon1) {
            locationSource$getLocation$Anon1 = (LocationSource$getLocation$Anon1) yb4;
            if ((locationSource$getLocation$Anon1.label & Integer.MIN_VALUE) != 0) {
                locationSource$getLocation$Anon1.label -= Integer.MIN_VALUE;
                Object obj = locationSource$getLocation$Anon1.result;
                Object a = cc4.a();
                i = locationSource$getLocation$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    if (this.locationCache != null) {
                        Calendar instance = Calendar.getInstance();
                        kd4.a((Object) instance, "Calendar.getInstance()");
                        if (instance.getTimeInMillis() - this.lastRetrievedTime < ((long) 60000) && !z) {
                            FLogger.INSTANCE.getLocal().d(TAG, "getLocation from cache");
                            result = new Result(this.locationCache, ErrorState.SUCCESS, true);
                        }
                    }
                    ug4 b = nh4.b();
                    LocationSource$getLocation$result$Anon1 locationSource$getLocation$result$Anon1 = new LocationSource$getLocation$result$Anon1(this, context, (yb4) null);
                    locationSource$getLocation$Anon1.L$Anon0 = this;
                    locationSource$getLocation$Anon1.L$Anon1 = context;
                    locationSource$getLocation$Anon1.Z$Anon0 = z;
                    locationSource$getLocation$Anon1.label = 1;
                    obj = yf4.a(b, locationSource$getLocation$result$Anon1, locationSource$getLocation$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    locationSource = this;
                } else if (i == 1) {
                    boolean z2 = locationSource$getLocation$Anon1.Z$Anon0;
                    Context context2 = (Context) locationSource$getLocation$Anon1.L$Anon1;
                    locationSource = (LocationSource) locationSource$getLocation$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                result2 = (Result) obj;
                FLogger.INSTANCE.getLocal().d(TAG, "getLocation result=" + result2.getErrorState() + " location=" + result2.getLocation());
                if (result2.getErrorState() != ErrorState.SUCCESS) {
                    if (locationSource.locationCache != null) {
                    }
                    result = result2;
                }
                isBetterLocation = locationSource.isBetterLocation(result2.getLocation(), locationSource.locationCache);
                if (isBetterLocation == null) {
                    Calendar instance2 = Calendar.getInstance();
                    kd4.a((Object) instance2, "Calendar.getInstance()");
                    locationSource.lastRetrievedTime = instance2.getTimeInMillis();
                    locationSource.locationCache = isBetterLocation;
                    result2 = new Result(locationSource.locationCache, ErrorState.SUCCESS, false, 4, (fd4) null);
                    result = result2;
                } else {
                    result = new Result((Location) null, ErrorState.UNKNOWN, false, 4, (fd4) null);
                }
            }
        }
        locationSource$getLocation$Anon1 = new LocationSource$getLocation$Anon1(this, yb4);
        Object obj2 = locationSource$getLocation$Anon1.result;
        Object a2 = cc4.a();
        i = locationSource$getLocation$Anon1.label;
        if (i != 0) {
        }
        result2 = (Result) obj2;
        FLogger.INSTANCE.getLocal().d(TAG, "getLocation result=" + result2.getErrorState() + " location=" + result2.getLocation());
        if (result2.getErrorState() != ErrorState.SUCCESS) {
        }
        isBetterLocation = locationSource.isBetterLocation(result2.getLocation(), locationSource.locationCache);
        if (isBetterLocation == null) {
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v5, resolved type: android.location.Location} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v10, resolved type: android.location.Location} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0161  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x016b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a  */
    public final /* synthetic */ Object getLocationFromFuseLocationManager(Context context, yb4<? super Result> yb4) {
        LocationSource$getLocationFromFuseLocationManager$Anon1 locationSource$getLocationFromFuseLocationManager$Anon1;
        int i;
        Location location;
        Result result;
        Location location2;
        oc1 oc1;
        Object obj;
        Context context2 = context;
        yb4<? super Result> yb42 = yb4;
        if (yb42 instanceof LocationSource$getLocationFromFuseLocationManager$Anon1) {
            locationSource$getLocationFromFuseLocationManager$Anon1 = (LocationSource$getLocationFromFuseLocationManager$Anon1) yb42;
            int i2 = locationSource$getLocationFromFuseLocationManager$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                locationSource$getLocationFromFuseLocationManager$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj2 = locationSource$getLocationFromFuseLocationManager$Anon1.result;
                Object a = cc4.a();
                i = locationSource$getLocationFromFuseLocationManager$Anon1.label;
                Location location3 = null;
                if (i != 0) {
                    na4.a(obj2);
                    FLogger.INSTANCE.getLocal().d(TAG, "getLocationFromFuseLocationManager");
                    oc1 = sc1.a(context);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.j(0);
                    locationRequest.i(0);
                    locationRequest.g(100);
                    locationRequest.a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    tc1.a aVar = new tc1.a();
                    aVar.a(locationRequest);
                    if (k6.a(context2, "android.permission.ACCESS_FINE_LOCATION") != 0 || k6.a(context2, "android.permission.ACCESS_COARSE_LOCATION") != 0) {
                        return new Result((Location) null, ErrorState.LOCATION_PERMISSION_OFF, false, 4, (fd4) null);
                    }
                    if (Build.VERSION.SDK_INT >= 29 && k6.a(context2, "android.permission.ACCESS_BACKGROUND_LOCATION") != 0) {
                        return new Result((Location) null, ErrorState.BACKGROUND_PERMISSION_OFF, false, 4, (fd4) null);
                    }
                    if (!LocationUtils.isLocationEnable(context)) {
                        return new Result((Location) null, ErrorState.LOCATION_SERVICE_OFF, false, 4, (fd4) null);
                    }
                    LocationSource$getLocationFromFuseLocationManager$Anon2 locationSource$getLocationFromFuseLocationManager$Anon2 = new LocationSource$getLocationFromFuseLocationManager$Anon2(this, oc1, locationRequest, (yb4) null);
                    locationSource$getLocationFromFuseLocationManager$Anon1.L$Anon0 = this;
                    locationSource$getLocationFromFuseLocationManager$Anon1.L$Anon1 = context2;
                    locationSource$getLocationFromFuseLocationManager$Anon1.L$Anon2 = null;
                    locationSource$getLocationFromFuseLocationManager$Anon1.L$Anon3 = oc1;
                    locationSource$getLocationFromFuseLocationManager$Anon1.L$Anon4 = locationRequest;
                    locationSource$getLocationFromFuseLocationManager$Anon1.L$Anon5 = aVar;
                    locationSource$getLocationFromFuseLocationManager$Anon1.label = 1;
                    obj = TimeoutKt.a(5000, locationSource$getLocationFromFuseLocationManager$Anon2, locationSource$getLocationFromFuseLocationManager$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    tc1.a aVar2 = (tc1.a) locationSource$getLocationFromFuseLocationManager$Anon1.L$Anon5;
                    LocationRequest locationRequest2 = (LocationRequest) locationSource$getLocationFromFuseLocationManager$Anon1.L$Anon4;
                    oc1 oc12 = (oc1) locationSource$getLocationFromFuseLocationManager$Anon1.L$Anon3;
                    location3 = locationSource$getLocationFromFuseLocationManager$Anon1.L$Anon2;
                    Context context3 = (Context) locationSource$getLocationFromFuseLocationManager$Anon1.L$Anon1;
                    LocationSource locationSource = (LocationSource) locationSource$getLocationFromFuseLocationManager$Anon1.L$Anon0;
                    try {
                        na4.a(obj2);
                        Object obj3 = obj2;
                        oc1 = oc12;
                        obj = obj3;
                    } catch (ExecutionException e) {
                        e = e;
                        location2 = location3;
                    } catch (InterruptedException e2) {
                        e = e2;
                        location2 = location3;
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = TAG;
                        local.e(str, ".getLocationFromFuseLocationManager(), e2=" + e);
                        location = location2;
                        if (location == null) {
                        }
                        return result;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                location2 = (Location) obj;
                if (location2 == null) {
                    try {
                        kd4.a((Object) oc1, "fusedLocationClient");
                        location = zn1.a(oc1.i());
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        local2.d(str2, "getLocationFromFuseLocationManager fusedLocationClient.lastLocation=" + location);
                    } catch (ExecutionException e3) {
                        e = e3;
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str3 = TAG;
                        local3.e(str3, ".getLocationFromFuseLocationManager(), e1=" + e);
                        location = location2;
                        if (location == null) {
                        }
                        return result;
                    } catch (InterruptedException e4) {
                        e = e4;
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String str4 = TAG;
                        local4.e(str4, ".getLocationFromFuseLocationManager(), e2=" + e);
                        location = location2;
                        if (location == null) {
                        }
                        return result;
                    }
                    if (location == null) {
                        ErrorState errorState = ErrorState.SUCCESS;
                    } else {
                        new Result((Location) null, ErrorState.UNKNOWN, false, 4, (fd4) null);
                    }
                    return result;
                }
                location = location2;
                if (location == null) {
                }
                return result;
            }
        }
        locationSource$getLocationFromFuseLocationManager$Anon1 = new LocationSource$getLocationFromFuseLocationManager$Anon1(this, yb42);
        Object obj22 = locationSource$getLocationFromFuseLocationManager$Anon1.result;
        Object a2 = cc4.a();
        i = locationSource$getLocationFromFuseLocationManager$Anon1.label;
        Location location32 = null;
        if (i != 0) {
        }
        location2 = (Location) obj;
        if (location2 == null) {
        }
        location = location2;
        if (location == null) {
        }
        return result;
    }

    @DexIgnore
    public final void observerLocation(Context context, LocationListener locationListener, boolean z) {
        kd4.b(context, "context");
        kd4.b(locationListener, "listener");
        if (!z || !this.isSignificantObserved) {
            if (z || !this.isMonitoringObserved) {
                oc1 a = sc1.a(context);
                LocationRequest locationRequest = new LocationRequest();
                locationRequest.j(1000);
                locationRequest.i(1000);
                locationRequest.g(100);
                locationRequest.a(z ? 300.0f : 50.0f);
                new tc1.a().a(locationRequest);
                if (k6.a(context, "android.permission.ACCESS_FINE_LOCATION") != 0 || k6.a(context, "android.permission.ACCESS_COARSE_LOCATION") != 0 || (Build.VERSION.SDK_INT >= 29 && k6.a(context, "android.permission.ACCESS_BACKGROUND_LOCATION") != 0)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "observerLocation permission missing");
                } else if (!LocationUtils.isLocationEnable(context)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "observerLocation location disable");
                } else if (z) {
                    try {
                        FLogger.INSTANCE.getLocal().d(TAG, "observerLocation start significantObserved");
                        this.mSignificantLocationListeners.add(locationListener);
                        a.a(locationRequest, this.mSignificantCallback, Looper.getMainLooper());
                        this.isSignificantObserved = true;
                        this.mSignificantClient = a;
                    } catch (ExecutionException e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = TAG;
                        local.e(str, ".getLocationFromFuseLocationManager(), e1=" + e);
                    } catch (InterruptedException e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        local2.e(str2, ".getLocationFromFuseLocationManager(), e2=" + e2);
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(TAG, "observerLocation start monitoringObserved");
                    this.mMonitoringLocationListeners.add(locationListener);
                    a.a(locationRequest, this.mMonitoringCallback, Looper.getMainLooper());
                    this.isMonitoringObserved = true;
                    this.mMonitoringClient = a;
                }
            } else if (!this.mMonitoringLocationListeners.contains(locationListener)) {
                this.mMonitoringLocationListeners.add(locationListener);
            }
        } else if (!this.mSignificantLocationListeners.contains(locationListener)) {
            this.mSignificantLocationListeners.add(locationListener);
        }
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public final Object requestLocationUpdates(oc1 oc1, LocationRequest locationRequest, yb4<? super Location> yb4) {
        eg4 eg4 = new eg4(IntrinsicsKt__IntrinsicsJvmKt.a(yb4), 1);
        oc1.a(locationRequest, new LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1(eg4, oc1, locationRequest), Looper.getMainLooper());
        Object e = eg4.e();
        if (e == cc4.a()) {
            ic4.c(yb4);
        }
        return e;
    }

    @DexIgnore
    public final void unObserverLocation(LocationListener locationListener, boolean z) {
        kd4.b(locationListener, "listener");
        if (z) {
            this.mSignificantLocationListeners.remove(locationListener);
            if (this.isSignificantObserved && this.mSignificantLocationListeners.isEmpty()) {
                FLogger.INSTANCE.getLocal().d(TAG, "observerLocation end significantObserved");
                oc1 oc1 = this.mSignificantClient;
                if (oc1 != null) {
                    oc1.a((qc1) this.mSignificantCallback);
                }
                this.isSignificantObserved = false;
                return;
            }
            return;
        }
        this.mMonitoringLocationListeners.remove(locationListener);
        if (this.isMonitoringObserved && this.mMonitoringLocationListeners.isEmpty()) {
            FLogger.INSTANCE.getLocal().d(TAG, "observerLocation end monitoringObserved");
            oc1 oc12 = this.mMonitoringClient;
            if (oc12 != null) {
                oc12.a((qc1) this.mMonitoringCallback);
            }
            this.isMonitoringObserved = false;
        }
    }
}
