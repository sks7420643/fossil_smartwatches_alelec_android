package com.portfolio.platform.receiver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.receiver.SmsMmsReceiver$onReceive$1", mo27670f = "SmsMmsReceiver.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class SmsMmsReceiver$onReceive$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $notificationInfo;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21341p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SmsMmsReceiver$onReceive$1(kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$notificationInfo = ref$ObjectRef;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.receiver.SmsMmsReceiver$onReceive$1 smsMmsReceiver$onReceive$1 = new com.portfolio.platform.receiver.SmsMmsReceiver$onReceive$1(this.$notificationInfo, yb4);
        smsMmsReceiver$onReceive$1.f21341p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return smsMmsReceiver$onReceive$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.receiver.SmsMmsReceiver$onReceive$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            java.lang.String defaultSmsPackage = android.provider.Telephony.Sms.getDefaultSmsPackage(com.portfolio.platform.PortfolioApp.f20941W.mo34589c());
            if (defaultSmsPackage == null || com.portfolio.platform.util.NotificationAppHelper.f24430b.mo41924a(defaultSmsPackage)) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.receiver.SmsMmsReceiver.f21338c, "onReceive() - SMS app filter is assigned - ignore");
            } else {
                com.portfolio.platform.manager.LightAndHapticsManager.f21237i.mo39658a().mo39645a((com.portfolio.platform.data.model.NotificationInfo) this.$notificationInfo.element);
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.portfolio.platform.receiver.SmsMmsReceiver.f21338c, "onReceive) - SMS app filter is not assigned - add to Queue");
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
