package com.portfolio.platform.receiver;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Process;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Iterator;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NetworkChangedReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public UserRepository a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = NetworkChangedReceiver.class.getSimpleName();
        kd4.a((Object) simpleName, "NetworkChangedReceiver::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public NetworkChangedReceiver() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final UserRepository a() {
        UserRepository userRepository = this.a;
        if (userRepository != null) {
            return userRepository;
        }
        kd4.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        throw null;
        // ActivityManager.RunningAppProcessInfo runningAppProcessInfo;
        // kd4.b(context, "context");
        // kd4.b(intent, "intent");
        // FLogger.INSTANCE.getLocal().d(b, "onReceive");
        // int myPid = Process.myPid();
        // ActivityManager activityManager = (ActivityManager) context.getSystemService(Constants.ACTIVITY);
        // if (activityManager != null) {
        //     if (activityManager.getRunningAppProcesses() != null) {
        //         Iterator<ActivityManager.RunningAppProcessInfo> it = activityManager.getRunningAppProcesses().iterator();
        //         while (true) {
        //             if (!it.hasNext()) {
        //                 break;
        //             }
        //             runningAppProcessInfo = it.next();
        //             FLogger.INSTANCE.getLocal().d(b, "onReceive processId=" + runningAppProcessInfo.pid);
        //             if (runningAppProcessInfo.pid == myPid) {
        //                 break;
        //             }
        //         }
        //     } else {
        //         return;
        //     }
        // }
        // runningAppProcessInfo = null;
        // if (runningAppProcessInfo != null) {
        //     if (!TextUtils.isEmpty(runningAppProcessInfo != null ? runningAppProcessInfo.processName : null)) {
        //         if (kd4.a((Object) runningAppProcessInfo != null ? runningAppProcessInfo.processName : null, (Object) PortfolioApp.W.c().getPackageName())) {
        //             ILocalFLogger local = FLogger.INSTANCE.getLocal();
        //             String str = b;
        //             StringBuilder sb = new StringBuilder();
        //             sb.append("onReceive processName=");
        //             sb.append(runningAppProcessInfo != null ? runningAppProcessInfo.processName : null);
        //             local.d(str, sb.toString());
        //             fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new NetworkChangedReceiver$onReceive$Anon2(this, a(context), (yb4) null), 3, (Object) null);
        //         }
        //     }
        // }
    }

    @DexIgnore
    public final boolean a(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }
}
