package com.portfolio.platform.receiver;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.workers.PushPendingDataWorker;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.receiver.NetworkChangedReceiver$onReceive$Anon2", f = "NetworkChangedReceiver.kt", l = {}, m = "invokeSuspend")
public final class NetworkChangedReceiver$onReceive$Anon2 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isInternetAvailable;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NetworkChangedReceiver this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NetworkChangedReceiver$onReceive$Anon2(NetworkChangedReceiver networkChangedReceiver, boolean z, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = networkChangedReceiver;
        this.$isInternetAvailable = z;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // NetworkChangedReceiver$onReceive$Anon2 networkChangedReceiver$onReceive$Anon2 = new NetworkChangedReceiver$onReceive$Anon2(this.this$Anon0, this.$isInternetAvailable, yb4);
        // networkChangedReceiver$onReceive$Anon2.p$ = (zg4) obj;
        // return networkChangedReceiver$onReceive$Anon2;
    }

    @DexIgnore
    public final Object invoke(zg4 obj, yb4<? super qa4>  obj2) {
        throw null;
        // return ((NetworkChangedReceiver$onReceive$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            if (this.this$Anon0.a().getCurrentUser() != null && !this.$isInternetAvailable) {
                PushPendingDataWorker.y.a();
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }

}
