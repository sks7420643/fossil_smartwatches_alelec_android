package com.portfolio.platform.receiver;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.util.NotificationAppHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.receiver.AppPackageRemoveReceiver$onReceive$Anon1", f = "AppPackageRemoveReceiver.kt", l = {}, m = "invokeSuspend")
public final class AppPackageRemoveReceiver$onReceive$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $packageName;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AppPackageRemoveReceiver this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AppPackageRemoveReceiver$onReceive$Anon1(AppPackageRemoveReceiver appPackageRemoveReceiver, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = appPackageRemoveReceiver;
        this.$packageName = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // AppPackageRemoveReceiver$onReceive$Anon1 appPackageRemoveReceiver$onReceive$Anon1 = new AppPackageRemoveReceiver$onReceive$Anon1(this.this$Anon0, this.$packageName, yb4);
        // appPackageRemoveReceiver$onReceive$Anon1.p$ = (zg4) obj;
        // return appPackageRemoveReceiver$onReceive$Anon1;
    }

    // @DexIgnore
    // public final Object invoke(Object obj, Object obj2) {
    //     return ((AppPackageRemoveReceiver$onReceive$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    // }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            AppFilter a = NotificationAppHelper.b.a(this.$packageName, MFDeviceFamily.DEVICE_FAMILY_SAM);
            if (a != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = AppPackageRemoveReceiver.b;
                local.d(b, "appFilter exist with same packageName " + this.$packageName);
                this.this$Anon0.a().removeAppFilter(a);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String b2 = AppPackageRemoveReceiver.b;
                local2.d(b2, "appFilter removed " + this.$packageName);
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }

    @Override
    @DexIgnore
    public Object invoke(zg4 zg4, yb4<? super qa4> yb4) {
        throw null;
    }
}
