package com.portfolio.platform.receiver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.receiver.AppPackageRemoveReceiver$onReceive$1", mo27670f = "AppPackageRemoveReceiver.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class AppPackageRemoveReceiver$onReceive$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $packageName;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21330p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.receiver.AppPackageRemoveReceiver this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AppPackageRemoveReceiver$onReceive$1(com.portfolio.platform.receiver.AppPackageRemoveReceiver appPackageRemoveReceiver, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = appPackageRemoveReceiver;
        this.$packageName = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.receiver.AppPackageRemoveReceiver$onReceive$1 appPackageRemoveReceiver$onReceive$1 = new com.portfolio.platform.receiver.AppPackageRemoveReceiver$onReceive$1(this.this$0, this.$packageName, yb4);
        appPackageRemoveReceiver$onReceive$1.f21330p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return appPackageRemoveReceiver$onReceive$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.receiver.AppPackageRemoveReceiver$onReceive$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.wearables.fsl.appfilter.AppFilter a = com.portfolio.platform.util.NotificationAppHelper.f24430b.mo41916a(this.$packageName, com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_SAM);
            if (a != null) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String b = com.portfolio.platform.receiver.AppPackageRemoveReceiver.f21328b;
                local.mo33255d(b, "appFilter exist with same packageName " + this.$packageName);
                this.this$0.mo39757a().removeAppFilter(a);
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String b2 = com.portfolio.platform.receiver.AppPackageRemoveReceiver.f21328b;
                local2.mo33255d(b2, "appFilter removed " + this.$packageName);
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
