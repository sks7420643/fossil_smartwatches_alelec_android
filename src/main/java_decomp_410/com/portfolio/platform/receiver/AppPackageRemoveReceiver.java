package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.NotificationsRepository;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AppPackageRemoveReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public NotificationsRepository a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = AppPackageRemoveReceiver.class.getSimpleName();
        kd4.a((Object) simpleName, "AppPackageRemoveReceiver::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public AppPackageRemoveReceiver() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final NotificationsRepository a() {
        NotificationsRepository notificationsRepository = this.a;
        if (notificationsRepository != null) {
            return notificationsRepository;
        }
        kd4.d("notificationsRepository");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0040, code lost:
        if (r7 != null) goto L_0x0045;
     */
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        throw null;
        // String str;
        // kd4.b(context, "context");
        // kd4.b(intent, "intent");
        // ILocalFLogger local = FLogger.INSTANCE.getLocal();
        // String str2 = b;
        // local.d(str2, "onReceive " + intent.getAction());
        // if (kd4.a((Object) "android.intent.action.PACKAGE_FULLY_REMOVED", (Object) intent.getAction())) {
        //     Uri data = intent.getData();
        //     if (data != null) {
        //         str = data.getSchemeSpecificPart();
        //     }
        //     str = "";
        //     ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        //     String str3 = b;
        //     local2.d(str3, "package removed " + str);
        //     fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new AppPackageRemoveReceiver$onReceive$Anon1(this, str, (yb4) null), 3, (Object) null);
        // }
    }
}
