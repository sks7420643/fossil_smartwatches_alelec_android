package com.portfolio.platform.receiver;

import android.provider.Telephony;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.NotificationInfo;
import com.portfolio.platform.manager.LightAndHapticsManager;
import com.portfolio.platform.util.NotificationAppHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.receiver.SmsMmsReceiver$onReceive$Anon1", f = "SmsMmsReceiver.kt", l = {}, m = "invokeSuspend")
public final class SmsMmsReceiver$onReceive$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Ref$ObjectRef $notificationInfo;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SmsMmsReceiver$onReceive$Anon1(Ref$ObjectRef ref$ObjectRef, yb4 yb4) {
        super(2, yb4);
        this.$notificationInfo = ref$ObjectRef;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // SmsMmsReceiver$onReceive$Anon1 smsMmsReceiver$onReceive$Anon1 = new SmsMmsReceiver$onReceive$Anon1(this.$notificationInfo, yb4);
        // smsMmsReceiver$onReceive$Anon1.p$ = (zg4) obj;
        // return smsMmsReceiver$onReceive$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((SmsMmsReceiver$onReceive$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            String defaultSmsPackage = Telephony.Sms.getDefaultSmsPackage(PortfolioApp.W.c());
            if (defaultSmsPackage == null || NotificationAppHelper.b.a(defaultSmsPackage)) {
                FLogger.INSTANCE.getLocal().d(SmsMmsReceiver.c, "onReceive() - SMS app filter is assigned - ignore");
            } else {
                LightAndHapticsManager.i.a().a((NotificationInfo) this.$notificationInfo.element);
                FLogger.INSTANCE.getLocal().d(SmsMmsReceiver.c, "onReceive) - SMS app filter is not assigned - add to Queue");
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
