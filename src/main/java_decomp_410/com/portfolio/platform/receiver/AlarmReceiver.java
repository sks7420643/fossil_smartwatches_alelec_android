package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AlarmHelper;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmReceiver extends BroadcastReceiver {
    @DexIgnore
    public UserRepository a;
    @DexIgnore
    public en2 b;
    @DexIgnore
    public DeviceRepository c;
    @DexIgnore
    public AlarmHelper d;
    @DexIgnore
    public AlarmsRepository e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public final AlarmHelper a() {
        AlarmHelper alarmHelper = this.d;
        if (alarmHelper != null) {
            return alarmHelper;
        }
        kd4.d("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public final AlarmsRepository b() {
        AlarmsRepository alarmsRepository = this.e;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        kd4.d("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository c() {
        DeviceRepository deviceRepository = this.c;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        kd4.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final en2 d() {
        en2 en2 = this.b;
        if (en2 != null) {
            return en2;
        }
        kd4.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final UserRepository e() {
        UserRepository userRepository = this.a;
        if (userRepository != null) {
            return userRepository;
        }
        kd4.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        throw null;
        // kd4.b(context, "context");
        // PortfolioApp.W.c().g().a(this);
        // FLogger.INSTANCE.getLocal().d("AlarmReceiver", "onReceive");
        // if (intent != null) {
        //     int intExtra = intent.getIntExtra("DEF_ALARM_RECEIVER_ACTION", -1);
        //     ILocalFLogger local = FLogger.INSTANCE.getLocal();
        //     local.d("AlarmReceiver", "onReceive - action=" + intExtra);
        //     fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new AlarmReceiver$onReceive$Anon1(this, intExtra, intent, context, (yb4) null), 3, (Object) null);
        // }
    }
}
