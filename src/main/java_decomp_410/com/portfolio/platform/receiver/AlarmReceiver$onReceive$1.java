package com.portfolio.platform.receiver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.receiver.AlarmReceiver$onReceive$1", mo27670f = "AlarmReceiver.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class AlarmReceiver$onReceive$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $action;
    @DexIgnore
    public /* final */ /* synthetic */ android.content.Context $context;
    @DexIgnore
    public /* final */ /* synthetic */ android.content.Intent $intent;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21327p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.receiver.AlarmReceiver this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmReceiver$onReceive$1(com.portfolio.platform.receiver.AlarmReceiver alarmReceiver, int i, android.content.Intent intent, android.content.Context context, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = alarmReceiver;
        this.$action = i;
        this.$intent = intent;
        this.$context = context;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.receiver.AlarmReceiver$onReceive$1 alarmReceiver$onReceive$1 = new com.portfolio.platform.receiver.AlarmReceiver$onReceive$1(this.this$0, this.$action, this.$intent, this.$context, yb4);
        alarmReceiver$onReceive$1.f21327p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return alarmReceiver$onReceive$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.receiver.AlarmReceiver$onReceive$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationFlag aNotificationFlag;
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.model.MFUser currentUser = this.this$0.mo39755e().getCurrentUser();
            int i = this.$action;
            if (i == 0) {
                com.portfolio.platform.data.source.local.alarm.Alarm findNextActiveAlarm = this.this$0.mo39752b().findNextActiveAlarm();
                java.lang.String stringExtra = this.$intent.getStringExtra("DEF_ALARM_RECEIVER_USER_ID");
                if (currentUser == null || android.text.TextUtils.isEmpty(stringExtra) || (true ^ com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) currentUser.getUserId(), (java.lang.Object) stringExtra)) || findNextActiveAlarm == null) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e("AlarmReceiver", "onReceive - user=null||empty||equals=false");
                    this.this$0.mo39751a().mo39480a(this.$context);
                } else {
                    android.os.Bundle bundle = new android.os.Bundle();
                    bundle.putInt("DEF_ALARM_RECEIVER_ACTION", 3);
                    bundle.putString("DEF_ALARM_RECEIVER_USER_ID", currentUser.getUserId());
                    android.content.Intent intent = new android.content.Intent(this.$context, com.portfolio.platform.receiver.AlarmReceiver.class);
                    intent.setAction("com.portfolio.platform.ALARM_RECEIVER");
                    intent.putExtras(bundle);
                    android.app.AlarmManager alarmManager = (android.app.AlarmManager) this.$context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
                    if (alarmManager != null) {
                        alarmManager.setExact(0, com.portfolio.platform.helper.AlarmHelper.f21153f.mo39489a(findNextActiveAlarm.getMillisecond()), android.app.PendingIntent.getBroadcast(this.$context, 101, intent, 134217728));
                    }
                }
            } else if (i == 2) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("AlarmReceiver", "onReceive - ACTION_ALARM_REPEAT");
                try {
                    java.lang.String e = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e();
                    if (e.length() == 0) {
                        return com.fossil.blesdk.obfuscated.qa4.f17909a;
                    }
                    long g = this.this$0.mo39754d().mo27039g(e);
                    long e2 = this.this$0.mo39754d().mo27034e(e);
                    long currentTimeMillis = java.lang.System.currentTimeMillis();
                    long j = (long) 432000000;
                    if (currentTimeMillis - e2 > j && currentTimeMillis - g > j) {
                        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("AlarmReceiver", "Remind to sync success");
                        this.this$0.mo39754d().mo26993a(currentTimeMillis, e);
                        com.portfolio.platform.data.model.MFUser currentUser2 = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34572s().getCurrentUser();
                        java.lang.String firstName = currentUser2 != null ? currentUser2.getFirstName() : null;
                        if (firstName != null) {
                            com.fossil.blesdk.obfuscated.pd4 pd4 = com.fossil.blesdk.obfuscated.pd4.f17594a;
                            java.lang.String a = com.fossil.blesdk.obfuscated.sm2.m27795a(this.$context, (int) com.fossil.wearables.fossil.R.string.hello_user);
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a, "LanguageHelper.getString\u2026ext, R.string.hello_user)");
                            java.lang.Object[] objArr = {firstName};
                            java.lang.String format = java.lang.String.format(a, java.util.Arrays.copyOf(objArr, objArr.length));
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) format, "java.lang.String.format(format, *args)");
                            java.lang.String a2 = com.fossil.blesdk.obfuscated.sm2.m27795a(this.$context, (int) com.fossil.wearables.fossil.R.string.General_Sync_Reminder_Text__ItsBeenAFewDaysSince);
                            com.portfolio.platform.news.notifications.FossilNotificationBar.Companion companion = com.portfolio.platform.news.notifications.FossilNotificationBar.f21301c;
                            android.content.Context applicationContext = com.portfolio.platform.PortfolioApp.f20941W.mo34589c().getApplicationContext();
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) applicationContext, "PortfolioApp.instance.applicationContext");
                            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) a2, "message");
                            companion.mo39719a(applicationContext, format, a2);
                            if (com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(e)) {
                                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("AlarmReceiver", "Remind to sync send notificaiton to watch");
                                com.portfolio.platform.PortfolioApp c = com.portfolio.platform.PortfolioApp.f20941W.mo34589c();
                                com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationType notificationType = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.FOSSIL.getNotificationType();
                                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName aApplicationName = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.FOSSIL;
                                com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationFlag[] aNotificationFlagArr = new com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationFlag[1];
                                if (com.portfolio.platform.helper.DeviceHelper.f21188o.mo39576l()) {
                                    aNotificationFlag = com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationFlag.ALLOW_USER_ACTION;
                                } else {
                                    aNotificationFlag = com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationFlag.IMPORTANT;
                                }
                                aNotificationFlagArr[0] = aNotificationFlag;
                                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj dianaNotificationObj = new com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj(99, notificationType, aApplicationName, "Smart Watch", "Fossil", a2, com.fossil.blesdk.obfuscated.cb4.m20544d(aNotificationFlagArr));
                                c.mo34502a(e, (com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj) dianaNotificationObj);
                            }
                        }
                    }
                } catch (java.lang.Exception e3) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e("AlarmReceiver", e3.getMessage());
                    e3.printStackTrace();
                }
            } else if (i == 3) {
                java.lang.String stringExtra2 = this.$intent.getStringExtra("DEF_ALARM_RECEIVER_USER_ID");
                if (currentUser == null || android.text.TextUtils.isEmpty(stringExtra2) || (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) currentUser.getUserId(), (java.lang.Object) stringExtra2))) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e("AlarmReceiver", "onReceive - user=null||empty||equals=false");
                    this.this$0.mo39751a().mo39480a(this.$context);
                } else {
                    this.this$0.mo39751a().mo39482b(this.$context);
                }
            }
            if (android.text.TextUtils.isEmpty(com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e())) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("AlarmReceiver", "onReceive - active device is EMPTY!!!");
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            com.portfolio.platform.data.model.Device deviceBySerial = this.this$0.mo39753c().getDeviceBySerial(com.portfolio.platform.PortfolioApp.f20941W.mo34589c().mo34532e());
            if (deviceBySerial != null) {
                int w = this.this$0.mo39754d().mo27087w();
                int intExtra = this.$intent.getIntExtra("REQUEST_CODE", -1);
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                local.mo33255d("AlarmReceiver", "onReceive - extraCode=" + intExtra + ", lowBatteryLevel=" + w + ", deviceBattery=" + deviceBySerial.getBatteryLevel());
                if (intExtra == 0) {
                    if (deviceBySerial.getBatteryLevel() >= w) {
                        this.this$0.mo39751a().mo39487f();
                    } else {
                        this.this$0.mo39751a().mo39483c();
                    }
                }
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
