package com.portfolio.platform.receiver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.receiver.NetworkChangedReceiver$onReceive$2", mo27670f = "NetworkChangedReceiver.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class NetworkChangedReceiver$onReceive$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isInternetAvailable;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21336p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.receiver.NetworkChangedReceiver this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NetworkChangedReceiver$onReceive$2(com.portfolio.platform.receiver.NetworkChangedReceiver networkChangedReceiver, boolean z, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = networkChangedReceiver;
        this.$isInternetAvailable = z;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.receiver.NetworkChangedReceiver$onReceive$2 networkChangedReceiver$onReceive$2 = new com.portfolio.platform.receiver.NetworkChangedReceiver$onReceive$2(this.this$0, this.$isInternetAvailable, yb4);
        networkChangedReceiver$onReceive$2.f21336p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return networkChangedReceiver$onReceive$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.receiver.NetworkChangedReceiver$onReceive$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            if (this.this$0.mo39762a().getCurrentUser() != null && !this.$isInternetAvailable) {
                com.portfolio.platform.workers.PushPendingDataWorker.f25690y.mo42934a();
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
