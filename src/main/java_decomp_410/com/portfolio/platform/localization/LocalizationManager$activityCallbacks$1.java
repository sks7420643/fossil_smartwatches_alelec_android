package com.portfolio.platform.localization;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LocalizationManager$activityCallbacks$1 implements android.app.Application.ActivityLifecycleCallbacks {

    @DexIgnore
    /* renamed from: e */
    public /* final */ /* synthetic */ com.portfolio.platform.localization.LocalizationManager f21234e;

    @DexIgnore
    public LocalizationManager$activityCallbacks$1(com.portfolio.platform.localization.LocalizationManager localizationManager) {
        this.f21234e = localizationManager;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    public void onActivityCreated(android.app.Activity activity, android.os.Bundle bundle) {
        java.lang.String str;
        if (activity != null) {
            this.f21234e.mo39610a((java.lang.ref.WeakReference<android.app.Activity>) new java.lang.ref.WeakReference(activity));
            java.lang.ref.WeakReference<android.app.Activity> h = this.f21234e.mo39618h();
            if (h != null) {
                android.app.Activity activity2 = (android.app.Activity) h.get();
                if (activity2 != null) {
                    java.lang.Class<?> cls = activity2.getClass();
                    if (cls != null) {
                        str = cls.getSimpleName();
                        if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) str, (java.lang.Object) this.f21234e.mo39616f())) {
                            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25692b()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.portfolio.platform.localization.C5964x80fd64c8((com.fossil.blesdk.obfuscated.yb4) null, this), 3, (java.lang.Object) null);
                            return;
                        }
                        return;
                    }
                }
                str = null;
                if (!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) str, (java.lang.Object) this.f21234e.mo39616f())) {
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void onActivityDestroyed(android.app.Activity activity) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(activity, com.misfit.frameworks.common.constants.Constants.ACTIVITY);
        if (this.f21234e.mo39618h() != null) {
            java.lang.ref.WeakReference<android.app.Activity> h = this.f21234e.mo39618h();
            if (h == null) {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            } else if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) (android.app.Activity) h.get(), (java.lang.Object) activity)) {
                this.f21234e.mo39610a((java.lang.ref.WeakReference<android.app.Activity>) null);
            }
        }
    }

    @DexIgnore
    public void onActivityPaused(android.app.Activity activity) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(activity, com.misfit.frameworks.common.constants.Constants.ACTIVITY);
    }

    @DexIgnore
    public void onActivityResumed(android.app.Activity activity) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(activity, com.misfit.frameworks.common.constants.Constants.ACTIVITY);
    }

    @DexIgnore
    public void onActivitySaveInstanceState(android.app.Activity activity, android.os.Bundle bundle) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(activity, com.misfit.frameworks.common.constants.Constants.ACTIVITY);
        com.fossil.blesdk.obfuscated.kd4.m24411b(bundle, "bundle");
    }

    @DexIgnore
    public void onActivityStarted(android.app.Activity activity) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(activity, com.misfit.frameworks.common.constants.Constants.ACTIVITY);
    }

    @DexIgnore
    public void onActivityStopped(android.app.Activity activity) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(activity, com.misfit.frameworks.common.constants.Constants.ACTIVITY);
    }
}
