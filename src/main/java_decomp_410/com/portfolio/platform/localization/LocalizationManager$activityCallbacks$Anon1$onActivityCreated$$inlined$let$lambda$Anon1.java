package com.portfolio.platform.localization;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.File;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LocalizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LocalizationManager$activityCallbacks$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LocalizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1(yb4 yb4, LocalizationManager$activityCallbacks$Anon1 localizationManager$activityCallbacks$Anon1) {
        super(2, yb4);
        this.this$Anon0 = localizationManager$activityCallbacks$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        LocalizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1 localizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1 = new LocalizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1(yb4, this.this$Anon0);
        localizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1.p$ = (zg4) obj;
        return localizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LocalizationManager$activityCallbacks$Anon1$onActivityCreated$$inlined$let$lambda$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            StringBuilder sb = new StringBuilder();
            File filesDir = this.this$Anon0.e.g().getFilesDir();
            kd4.a((Object) filesDir, "mContext.filesDir");
            sb.append(filesDir.getAbsolutePath());
            sb.append(ZendeskConfig.SLASH);
            sb.append("language.zip");
            String sb2 = sb.toString();
            File file = new File(sb2);
            if (!file.exists()) {
                LocalizationManager localizationManager = this.this$Anon0.e;
                this.L$Anon0 = zg4;
                this.L$Anon1 = sb2;
                this.L$Anon2 = file;
                this.label = 1;
                if (localizationManager.a((yb4<? super qa4>) this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            File file2 = (File) this.L$Anon2;
            String str = (String) this.L$Anon1;
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
