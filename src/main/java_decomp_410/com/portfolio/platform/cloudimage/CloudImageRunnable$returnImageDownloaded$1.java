package com.portfolio.platform.cloudimage;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1", mo27670f = "CloudImageRunnable.kt", mo27671l = {126, 132}, mo27672m = "invokeSuspend")
public final class CloudImageRunnable$returnImageDownloaded$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $destinationUnzipPath;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public java.lang.Object L$2;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21000p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.cloudimage.CloudImageRunnable this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1$1", mo27670f = "CloudImageRunnable.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1$1 */
    public static final class C56161 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.lang.String $filePath1;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21001p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C56161(com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1 cloudImageRunnable$returnImageDownloaded$1, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = cloudImageRunnable$returnImageDownloaded$1;
            this.$filePath1 = str;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1.C56161 r0 = new com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1.C56161(this.this$0, this.$filePath1, yb4);
            r0.f21001p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1.C56161) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$0.this$0.mListener;
                if (access$getMListener$p == null) {
                    return null;
                }
                access$getMListener$p.onImageCallback(com.portfolio.platform.cloudimage.CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), this.$filePath1);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1$2")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1$2", mo27670f = "CloudImageRunnable.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1$2 */
    public static final class C56172 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ java.lang.String $filePath2;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21002p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C56172(com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1 cloudImageRunnable$returnImageDownloaded$1, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = cloudImageRunnable$returnImageDownloaded$1;
            this.$filePath2 = str;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1.C56172 r0 = new com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1.C56172(this.this$0, this.$filePath2, yb4);
            r0.f21002p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1.C56172) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$0.this$0.mListener;
                if (access$getMListener$p == null) {
                    return null;
                }
                access$getMListener$p.onImageCallback(com.portfolio.platform.cloudimage.CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), this.$filePath2);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable$returnImageDownloaded$1(com.portfolio.platform.cloudimage.CloudImageRunnable cloudImageRunnable, java.lang.String str, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = cloudImageRunnable;
        this.$destinationUnzipPath = str;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1 cloudImageRunnable$returnImageDownloaded$1 = new com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1(this.this$0, this.$destinationUnzipPath, yb4);
        cloudImageRunnable$returnImageDownloaded$1.f21000p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return cloudImageRunnable$returnImageDownloaded$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i != 0) {
            if (i != 1) {
                if (i == 2) {
                    java.lang.String str = (java.lang.String) this.L$2;
                } else {
                    throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
            java.lang.String str2 = (java.lang.String) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg42 = this.f21000p$;
            java.lang.String str3 = this.$destinationUnzipPath + '/' + this.this$0.type + ".webp";
            if (com.portfolio.platform.cloudimage.AssetUtil.INSTANCE.checkFileExist(str3)) {
                com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
                com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1.C56161 r4 = new com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1.C56161(this, str3, (com.fossil.blesdk.obfuscated.yb4) null);
                this.L$0 = zg42;
                this.L$1 = str3;
                this.label = 1;
                if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r4, this) == a) {
                    return a;
                }
            } else {
                java.lang.String str4 = this.$destinationUnzipPath + '/' + this.this$0.type + ".png";
                if (com.portfolio.platform.cloudimage.AssetUtil.INSTANCE.checkFileExist(str4)) {
                    com.fossil.blesdk.obfuscated.pi4 c2 = com.fossil.blesdk.obfuscated.nh4.m25693c();
                    com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1.C56172 r5 = new com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1.C56172(this, str4, (com.fossil.blesdk.obfuscated.yb4) null);
                    this.L$0 = zg42;
                    this.L$1 = str3;
                    this.L$2 = str4;
                    this.label = 2;
                    if (com.fossil.blesdk.obfuscated.yf4.m30997a(c2, r5, this) == a) {
                        return a;
                    }
                }
            }
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
