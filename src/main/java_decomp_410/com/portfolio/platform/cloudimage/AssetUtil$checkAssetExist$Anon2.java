package com.portfolio.platform.cloudimage;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$Anon2", f = "AssetUtil.kt", l = {}, m = "invokeSuspend")
public final class AssetUtil$checkAssetExist$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $filePath1;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageHelper.OnImageCallbackListener $listener;
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AssetUtil$checkAssetExist$Anon2(CloudImageHelper.OnImageCallbackListener onImageCallbackListener, String str, String str2, yb4 yb4) {
        super(2, yb4);
        this.$listener = onImageCallbackListener;
        this.$serialNumber = str;
        this.$filePath1 = str2;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        AssetUtil$checkAssetExist$Anon2 assetUtil$checkAssetExist$Anon2 = new AssetUtil$checkAssetExist$Anon2(this.$listener, this.$serialNumber, this.$filePath1, yb4);
        assetUtil$checkAssetExist$Anon2.p$ = (zg4) obj;
        return assetUtil$checkAssetExist$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AssetUtil$checkAssetExist$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            CloudImageHelper.OnImageCallbackListener onImageCallbackListener = this.$listener;
            if (onImageCallbackListener == null) {
                return null;
            }
            onImageCallbackListener.onImageCallback(this.$serialNumber, this.$filePath1);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
