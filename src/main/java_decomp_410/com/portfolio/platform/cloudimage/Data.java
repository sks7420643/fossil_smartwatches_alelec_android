package com.portfolio.platform.cloudimage;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Data {
    @DexIgnore
    public /* final */ String url;

    @DexIgnore
    public Data() {
        this((String) null, 1, (fd4) null);
    }

    @DexIgnore
    public Data(String str) {
        this.url = str;
    }

    @DexIgnore
    public static /* synthetic */ Data copy$default(Data data, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = data.url;
        }
        return data.copy(str);
    }

    @DexIgnore
    public final String component1() {
        return this.url;
    }

    @DexIgnore
    public final Data copy(String str) {
        return new Data(str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof Data) && kd4.a((Object) this.url, (Object) ((Data) obj).url);
        }
        return true;
    }

    @DexIgnore
    public final String getUrl() {
        return this.url;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.url;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "Data(url=" + this.url + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Data(String str, int i, fd4 fd4) {
        this((i & 1) != 0 ? null : str);
    }
}
