package com.portfolio.platform.cloudimage;

import android.widget.ImageView;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import java.io.File;
import java.lang.ref.WeakReference;
import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$Anon1", f = "CloudImageHelper.kt", l = {112, 116}, m = "invokeSuspend")
public final class CloudImageHelper$ItemImage$downloadForCalibration$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageHelper.ItemImage this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$Anon1$Anon1", f = "CloudImageHelper.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageHelper$ItemImage$downloadForCalibration$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(CloudImageHelper$ItemImage$downloadForCalibration$Anon1 cloudImageHelper$ItemImage$downloadForCalibration$Anon1, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = cloudImageHelper$ItemImage$downloadForCalibration$Anon1;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                if (this.this$Anon0.this$Anon0.mWeakReferenceImageView != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String tag = CloudImageHelper.Companion.getTAG();
                    local.d(tag, "download setDefaultImage first, resourceId=" + this.this$Anon0.this$Anon0.mResourceId);
                    WeakReference access$getMWeakReferenceImageView$p = this.this$Anon0.this$Anon0.mWeakReferenceImageView;
                    if (access$getMWeakReferenceImageView$p != null) {
                        ImageView imageView = (ImageView) access$getMWeakReferenceImageView$p.get();
                        if (imageView != null) {
                            Integer access$getMResourceId$p = this.this$Anon0.this$Anon0.mResourceId;
                            if (access$getMResourceId$p != null) {
                                imageView.setImageResource(access$getMResourceId$p.intValue());
                            } else {
                                kd4.a();
                                throw null;
                            }
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.ref.WeakReference<android.widget.ImageView>");
                    }
                }
                File access$getMFile$p = this.this$Anon0.this$Anon0.mFile;
                if (access$getMFile$p != null) {
                    String access$getMSerialNumber$p = this.this$Anon0.this$Anon0.mSerialNumber;
                    if (access$getMSerialNumber$p != null) {
                        String access$getMSerialPrefix$p = this.this$Anon0.this$Anon0.mSerialPrefix;
                        if (access$getMSerialPrefix$p != null) {
                            CloudImageHelper.this.getMAppExecutors().b().execute(new CloudImageRunnable(access$getMFile$p, access$getMSerialNumber$p, access$getMSerialPrefix$p, ResolutionHelper.INSTANCE.getResolutionFromDevice().getResolution(), Constants.DownloadAssetType.CALIBRATION, this.this$Anon0.this$Anon0.mCalibrationType.getType(), this.this$Anon0.this$Anon0.mListener));
                            return qa4.a;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageHelper$ItemImage$downloadForCalibration$Anon1(CloudImageHelper.ItemImage itemImage, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = itemImage;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CloudImageHelper$ItemImage$downloadForCalibration$Anon1 cloudImageHelper$ItemImage$downloadForCalibration$Anon1 = new CloudImageHelper$ItemImage$downloadForCalibration$Anon1(this.this$Anon0, yb4);
        cloudImageHelper$ItemImage$downloadForCalibration$Anon1.p$ = (zg4) obj;
        return cloudImageHelper$ItemImage$downloadForCalibration$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CloudImageHelper$ItemImage$downloadForCalibration$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        zg4 zg4;
        Object obj2;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            if (this.this$Anon0.mFile == null) {
                CloudImageHelper.ItemImage itemImage = this.this$Anon0;
                itemImage.mFile = CloudImageHelper.this.getMApp().getFilesDir();
            }
            AssetUtil assetUtil = AssetUtil.INSTANCE;
            File access$getMFile$p = this.this$Anon0.mFile;
            if (access$getMFile$p != null) {
                String access$getMSerialNumber$p = this.this$Anon0.mSerialNumber;
                if (access$getMSerialNumber$p != null) {
                    String access$getMSerialPrefix$p = this.this$Anon0.mSerialPrefix;
                    if (access$getMSerialPrefix$p != null) {
                        String resolution = ResolutionHelper.INSTANCE.getResolutionFromDevice().getResolution();
                        String feature = Constants.Feature.CALIBRATION.getFeature();
                        String type = this.this$Anon0.mCalibrationType.getType();
                        CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$Anon0.mListener;
                        this.L$Anon0 = zg4;
                        this.label = 1;
                        obj2 = assetUtil.checkAssetExist(access$getMFile$p, access$getMSerialNumber$p, access$getMSerialPrefix$p, resolution, feature, type, access$getMListener$p, this);
                        if (obj2 == a) {
                            return a;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else if (i == 1) {
            na4.a(obj);
            zg4 = (zg4) this.L$Anon0;
            obj2 = obj;
        } else if (i == 2) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
            return qa4.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (((Boolean) obj2).booleanValue()) {
            return qa4.a;
        }
        pi4 c = nh4.c();
        Anon1 anon1 = new Anon1(this, (yb4) null);
        this.L$Anon0 = zg4;
        this.label = 2;
        if (yf4.a(c, anon1, this) == a) {
            return a;
        }
        return qa4.a;
    }
}
