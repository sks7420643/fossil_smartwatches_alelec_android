package com.portfolio.platform.cloudimage;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.pi4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.yf4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$Anon1", f = "CloudImageRunnable.kt", l = {126, 132}, m = "invokeSuspend")
public final class CloudImageRunnable$returnImageDownloaded$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $destinationUnzipPath;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageRunnable this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$Anon1$Anon1", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $filePath1;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnImageDownloaded$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(CloudImageRunnable$returnImageDownloaded$Anon1 cloudImageRunnable$returnImageDownloaded$Anon1, String str, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = cloudImageRunnable$returnImageDownloaded$Anon1;
            this.$filePath1 = str;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon1 anon1 = new Anon1(this.this$Anon0, this.$filePath1, yb4);
            anon1.p$ = (zg4) obj;
            return anon1;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$Anon0.this$Anon0.mListener;
                if (access$getMListener$p == null) {
                    return null;
                }
                access$getMListener$p.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$Anon0.this$Anon0), this.$filePath1);
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$Anon1$Anon2", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $filePath2;
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnImageDownloaded$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(CloudImageRunnable$returnImageDownloaded$Anon1 cloudImageRunnable$returnImageDownloaded$Anon1, String str, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = cloudImageRunnable$returnImageDownloaded$Anon1;
            this.$filePath2 = str;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, this.$filePath2, yb4);
            anon2.p$ = (zg4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$Anon0.this$Anon0.mListener;
                if (access$getMListener$p == null) {
                    return null;
                }
                access$getMListener$p.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$Anon0.this$Anon0), this.$filePath2);
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable$returnImageDownloaded$Anon1(CloudImageRunnable cloudImageRunnable, String str, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = cloudImageRunnable;
        this.$destinationUnzipPath = str;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        CloudImageRunnable$returnImageDownloaded$Anon1 cloudImageRunnable$returnImageDownloaded$Anon1 = new CloudImageRunnable$returnImageDownloaded$Anon1(this.this$Anon0, this.$destinationUnzipPath, yb4);
        cloudImageRunnable$returnImageDownloaded$Anon1.p$ = (zg4) obj;
        return cloudImageRunnable$returnImageDownloaded$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CloudImageRunnable$returnImageDownloaded$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i != 0) {
            if (i != 1) {
                if (i == 2) {
                    String str = (String) this.L$Anon2;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
            String str2 = (String) this.L$Anon1;
            zg4 zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            na4.a(obj);
            zg4 zg42 = this.p$;
            String str3 = this.$destinationUnzipPath + '/' + this.this$Anon0.type + ".webp";
            if (AssetUtil.INSTANCE.checkFileExist(str3)) {
                pi4 c = nh4.c();
                Anon1 anon1 = new Anon1(this, str3, (yb4) null);
                this.L$Anon0 = zg42;
                this.L$Anon1 = str3;
                this.label = 1;
                if (yf4.a(c, anon1, this) == a) {
                    return a;
                }
            } else {
                String str4 = this.$destinationUnzipPath + '/' + this.this$Anon0.type + ".png";
                if (AssetUtil.INSTANCE.checkFileExist(str4)) {
                    pi4 c2 = nh4.c();
                    Anon2 anon2 = new Anon2(this, str4, (yb4) null);
                    this.L$Anon0 = zg42;
                    this.L$Anon1 = str3;
                    this.L$Anon2 = str4;
                    this.label = 2;
                    if (yf4.a(c2, anon2, this) == a) {
                        return a;
                    }
                }
            }
        }
        return qa4.a;
    }
}
