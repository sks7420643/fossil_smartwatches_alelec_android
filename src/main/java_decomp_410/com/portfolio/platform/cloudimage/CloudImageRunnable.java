package com.portfolio.platform.cloudimage;

import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.cloudimage.FileDownloadTaskHelper;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;
import com.portfolio.platform.cloudimage.UnzippingTaskHelper;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.File;
import java.util.ArrayList;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CloudImageRunnable implements Runnable, FileDownloadTaskHelper.OnDownloadFinishListener, UnzippingTaskHelper.OnUnzipFinishListener, URLRequestTaskHelper.OnNextTaskListener {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static ArrayList<String> downloadingPath; // = new ArrayList<>();
    @DexIgnore
    public Constants.DownloadAssetType downloadAssetType;
    @DexIgnore
    public /* final */ File file;
    @DexIgnore
    public CloudImageHelper.OnForceDownloadCallbackListener mDownloadListener;
    @DexIgnore
    public CloudImageHelper.OnImageCallbackListener mListener;
    @DexIgnore
    public String resolution;
    @DexIgnore
    public int retryDownloadTime;
    @DexIgnore
    public int retryUnzipTime;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public String serialPrefix;
    @DexIgnore
    public String type;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = new int[Constants.DownloadAssetType.values().length];

        /*
        static {
            $EnumSwitchMapping$Anon0[Constants.DownloadAssetType.DEVICE.ordinal()] = 1;
            $EnumSwitchMapping$Anon0[Constants.DownloadAssetType.CALIBRATION.ordinal()] = 2;
        }
        */
    }

    /*
    static {
        String simpleName = CloudImageRunnable.class.getSimpleName();
        kd4.a((Object) simpleName, "CloudImageRunnable::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public CloudImageRunnable(File file2) {
        kd4.b(file2, "file");
        this.file = file2;
        this.downloadAssetType = Constants.DownloadAssetType.BOTH;
    }

    @DexIgnore
    public static final /* synthetic */ String access$getResolution$p(CloudImageRunnable cloudImageRunnable) {
        String str = cloudImageRunnable.resolution;
        if (str != null) {
            return str;
        }
        kd4.d("resolution");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ String access$getSerialNumber$p(CloudImageRunnable cloudImageRunnable) {
        String str = cloudImageRunnable.serialNumber;
        if (str != null) {
            return str;
        }
        kd4.d("serialNumber");
        throw null;
    }

    @DexIgnore
    private final void downloadingDeviceAssets(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.file.getAbsolutePath());
        sb.append(ZendeskConfig.SLASH);
        String str2 = this.serialPrefix;
        if (str2 != null) {
            sb.append(str2);
            sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
            String str3 = this.resolution;
            if (str3 != null) {
                sb.append(str3);
                sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                sb.append(str);
                sb.append(".zip");
                String sb2 = sb.toString();
                StringBuilder sb3 = new StringBuilder();
                sb3.append(this.file.getAbsolutePath());
                sb3.append(ZendeskConfig.SLASH);
                String str4 = this.serialPrefix;
                if (str4 != null) {
                    sb3.append(str4);
                    sb3.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                    String str5 = this.resolution;
                    if (str5 != null) {
                        sb3.append(str5);
                        sb3.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                        sb3.append(str);
                        fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new CloudImageRunnable$downloadingDeviceAssets$Anon1(this, sb2, sb3.toString(), str, (yb4) null), 3, (Object) null);
                        return;
                    }
                    kd4.d("resolution");
                    throw null;
                }
                kd4.d("serialPrefix");
                throw null;
            }
            kd4.d("resolution");
            throw null;
        }
        kd4.d("serialPrefix");
        throw null;
    }

    @DexIgnore
    private final URLRequestTaskHelper prepareURLRequestTask() {
        URLRequestTaskHelper newInstance = URLRequestTaskHelper.Companion.newInstance();
        newInstance.setOnNextTaskListener(this);
        return newInstance;
    }

    @DexIgnore
    private final void returnImageDownloaded(String str) {
        String str2 = this.type;
        if (!(str2 == null || str2.length() == 0)) {
            fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new CloudImageRunnable$returnImageDownloaded$Anon1(this, str, (yb4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    private final void startDownloadingAssets() {
        int i = WhenMappings.$EnumSwitchMapping$Anon0[this.downloadAssetType.ordinal()];
        if (i == 1) {
            downloadingDeviceAssets(Constants.Feature.DEVICE.getFeature());
        } else if (i != 2) {
            downloadingDeviceAssets(Constants.Feature.DEVICE.getFeature());
            downloadingDeviceAssets(Constants.Feature.CALIBRATION.getFeature());
        } else {
            downloadingDeviceAssets(Constants.Feature.CALIBRATION.getFeature());
        }
    }

    @DexIgnore
    public void downloadFile(String str, String str2, AssetsDeviceResponse assetsDeviceResponse) {
        kd4.b(assetsDeviceResponse, "response");
        synchronized (downloadingPath) {
            if (kb4.a(downloadingPath, str) < 0) {
                FileDownloadTaskHelper newInstance = FileDownloadTaskHelper.Companion.newInstance();
                newInstance.setOnDownloadFinishListener(this);
                if (str == null) {
                    kd4.a();
                    throw null;
                } else if (str2 != null) {
                    newInstance.init(str, str2, assetsDeviceResponse);
                    newInstance.execute();
                    downloadingPath.add(str);
                } else {
                    kd4.a();
                    throw null;
                }
            }
            qa4 qa4 = qa4.a;
        }
    }

    @DexIgnore
    public void onDownloadFail(String str, String str2, AssetsDeviceResponse assetsDeviceResponse) {
        kd4.b(str, "zipFilePath");
        kd4.b(str2, "destinationUnzipPath");
        kd4.b(assetsDeviceResponse, "response");
        synchronized (downloadingPath) {
            downloadingPath.remove(str);
        }
        this.retryDownloadTime++;
        if (this.retryDownloadTime >= 3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onDownloadFail: retry reaches max retry for serialNumber = [");
            Metadata metadata = assetsDeviceResponse.getMetadata();
            sb.append(metadata != null ? metadata.getSerialNumber() : null);
            sb.append("]");
            local.d(str3, sb.toString());
            this.retryDownloadTime = 0;
            CloudImageHelper.OnForceDownloadCallbackListener onForceDownloadCallbackListener = this.mDownloadListener;
            if (onForceDownloadCallbackListener != null) {
                onForceDownloadCallbackListener.onDownloadCallback(false);
                return;
            }
            return;
        }
        downloadFile(str, str2, assetsDeviceResponse);
    }

    @DexIgnore
    public void onDownloadSuccess(String str, String str2) {
        kd4.b(str, "zipFilePath");
        kd4.b(str2, "destinationUnzipPath");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, "onDownloadSuccess path=" + str);
        synchronized (downloadingPath) {
            downloadingPath.remove(str);
        }
        UnzippingTaskHelper newInstance = UnzippingTaskHelper.Companion.newInstance();
        newInstance.setOnUnzipFinishListener(this);
        newInstance.init(str, str2);
        newInstance.execute();
    }

    @DexIgnore
    public void onGetDeviceAssetFailed() {
        CloudImageHelper.OnForceDownloadCallbackListener onForceDownloadCallbackListener = this.mDownloadListener;
        if (onForceDownloadCallbackListener != null) {
            onForceDownloadCallbackListener.onDownloadCallback(false);
        }
    }

    @DexIgnore
    public void onUnzipFail(String str, String str2) {
        kd4.b(str, "zipFilePath");
        kd4.b(str2, "destinationUnzipPath");
        this.retryUnzipTime++;
        if (this.retryUnzipTime >= 3) {
            FLogger.INSTANCE.getLocal().d(TAG, "onUnzipFail: retry reach max retry!");
            this.retryUnzipTime = 0;
            CloudImageHelper.OnForceDownloadCallbackListener onForceDownloadCallbackListener = this.mDownloadListener;
            if (onForceDownloadCallbackListener != null) {
                onForceDownloadCallbackListener.onDownloadCallback(false);
                return;
            }
            return;
        }
        onDownloadSuccess(str, str2);
    }

    @DexIgnore
    public void onUnzipSuccess(String str, String str2) {
        kd4.b(str, "zipFilePath");
        kd4.b(str2, "destinationUnzipPath");
        CloudImageHelper.OnForceDownloadCallbackListener onForceDownloadCallbackListener = this.mDownloadListener;
        if (onForceDownloadCallbackListener != null) {
            onForceDownloadCallbackListener.onDownloadCallback(true);
        }
        returnImageDownloaded(str2);
    }

    @DexIgnore
    public void run() {
        startDownloadingAssets();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable(File file2, String str, String str2, String str3) {
        this(file2);
        kd4.b(file2, "file");
        kd4.b(str, "serialNumber");
        kd4.b(str2, "serialPrefix");
        kd4.b(str3, "resolution");
        this.serialNumber = str;
        this.serialPrefix = str2;
        this.resolution = str3;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable(File file2, String str, String str2, String str3, Constants.DownloadAssetType downloadAssetType2, String str4, CloudImageHelper.OnImageCallbackListener onImageCallbackListener) {
        this(file2, str, str2, str3);
        kd4.b(file2, "file");
        kd4.b(str, "serialNumber");
        kd4.b(str2, "serialPrefix");
        kd4.b(str3, "resolution");
        kd4.b(downloadAssetType2, "downloadAssetType");
        kd4.b(str4, "type");
        this.downloadAssetType = downloadAssetType2;
        this.type = str4;
        this.mListener = onImageCallbackListener;
    }
}
