package com.portfolio.platform.cloudimage;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.cloudimage.CloudImageRunnable$downloadingDeviceAssets$1", mo27670f = "CloudImageRunnable.kt", mo27671l = {62}, mo27672m = "invokeSuspend")
public final class CloudImageRunnable$downloadingDeviceAssets$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $destinationUnzipPath;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $feature;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $zipFilePath;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20999p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.cloudimage.CloudImageRunnable this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable$downloadingDeviceAssets$1(com.portfolio.platform.cloudimage.CloudImageRunnable cloudImageRunnable, java.lang.String str, java.lang.String str2, java.lang.String str3, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = cloudImageRunnable;
        this.$zipFilePath = str;
        this.$destinationUnzipPath = str2;
        this.$feature = str3;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.cloudimage.CloudImageRunnable$downloadingDeviceAssets$1 cloudImageRunnable$downloadingDeviceAssets$1 = new com.portfolio.platform.cloudimage.CloudImageRunnable$downloadingDeviceAssets$1(this.this$0, this.$zipFilePath, this.$destinationUnzipPath, this.$feature, yb4);
        cloudImageRunnable$downloadingDeviceAssets$1.f20999p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return cloudImageRunnable$downloadingDeviceAssets$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.cloudimage.CloudImageRunnable$downloadingDeviceAssets$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f20999p$;
            com.portfolio.platform.cloudimage.URLRequestTaskHelper access$prepareURLRequestTask = this.this$0.prepareURLRequestTask();
            access$prepareURLRequestTask.init(this.$zipFilePath, this.$destinationUnzipPath, com.portfolio.platform.cloudimage.CloudImageRunnable.access$getSerialNumber$p(this.this$0), this.$feature, com.portfolio.platform.cloudimage.CloudImageRunnable.access$getResolution$p(this.this$0));
            this.L$0 = zg4;
            this.L$1 = access$prepareURLRequestTask;
            this.label = 1;
            if (access$prepareURLRequestTask.execute(this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.portfolio.platform.cloudimage.URLRequestTaskHelper uRLRequestTaskHelper = (com.portfolio.platform.cloudimage.URLRequestTaskHelper) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
