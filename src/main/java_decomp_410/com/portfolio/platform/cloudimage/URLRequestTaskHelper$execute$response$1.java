package com.portfolio.platform.cloudimage;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$response$1", mo27670f = "URLRequestTaskHelper.kt", mo27671l = {62}, mo27672m = "invokeSuspend")
public final class URLRequestTaskHelper$execute$response$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qr4<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.blesdk.obfuscated.xz1>>>, java.lang.Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ com.portfolio.platform.cloudimage.URLRequestTaskHelper this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public URLRequestTaskHelper$execute$response$1(com.portfolio.platform.cloudimage.URLRequestTaskHelper uRLRequestTaskHelper, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(1, yb4);
        this.this$0 = uRLRequestTaskHelper;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        return new com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$response$1(this.this$0, yb4);
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj) {
        return ((com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$response$1) create((com.fossil.blesdk.obfuscated.yb4) obj)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.portfolio.platform.data.source.remote.ApiServiceV2 mApiService = this.this$0.getMApiService();
            java.lang.String serialNumber$app_fossilRelease = this.this$0.getSerialNumber$app_fossilRelease();
            if (serialNumber$app_fossilRelease != null) {
                java.lang.String feature$app_fossilRelease = this.this$0.getFeature$app_fossilRelease();
                if (feature$app_fossilRelease != null) {
                    java.lang.String access$getResolution$p = this.this$0.resolution;
                    if (access$getResolution$p != null) {
                        this.label = 1;
                        obj = mApiService.getDeviceAssets(20, 0, serialNumber$app_fossilRelease, feature$app_fossilRelease, access$getResolution$p, "ANDROID", this);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        com.fossil.blesdk.obfuscated.kd4.m24405a();
                        throw null;
                    }
                } else {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
