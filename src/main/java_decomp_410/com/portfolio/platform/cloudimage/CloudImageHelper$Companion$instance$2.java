package com.portfolio.platform.cloudimage;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CloudImageHelper$Companion$instance$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.portfolio.platform.cloudimage.CloudImageHelper> {
    @DexIgnore
    public static /* final */ com.portfolio.platform.cloudimage.CloudImageHelper$Companion$instance$2 INSTANCE; // = new com.portfolio.platform.cloudimage.CloudImageHelper$Companion$instance$2();

    @DexIgnore
    public CloudImageHelper$Companion$instance$2() {
        super(0);
    }

    @DexIgnore
    public final com.portfolio.platform.cloudimage.CloudImageHelper invoke() {
        return com.portfolio.platform.cloudimage.CloudImageHelper.Holder.INSTANCE.getINSTANCE();
    }
}
