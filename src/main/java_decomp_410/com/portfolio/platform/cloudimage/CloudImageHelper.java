package com.portfolio.platform.cloudimage;

import android.widget.ImageView;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.h42;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.la4;
import com.fossil.blesdk.obfuscated.le4;
import com.fossil.blesdk.obfuscated.ma4;
import com.fossil.blesdk.obfuscated.md4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.Constants;
import java.io.File;
import java.lang.ref.WeakReference;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.PropertyReference1;
import kotlin.jvm.internal.PropertyReference1Impl;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CloudImageHelper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ la4 instance$delegate; // = ma4.a(CloudImageHelper$Companion$instance$Anon2.INSTANCE);
    @DexIgnore
    public PortfolioApp mApp;
    @DexIgnore
    public h42 mAppExecutors;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public static /* final */ /* synthetic */ le4[] $$delegatedProperties;

        /*
        static {
            PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(md4.a(Companion.class), "instance", "getInstance()Lcom/portfolio/platform/cloudimage/CloudImageHelper;");
            md4.a((PropertyReference1) propertyReference1Impl);
            $$delegatedProperties = new le4[]{propertyReference1Impl};
        }
        */

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public static /* synthetic */ void instance$annotations() {
        }

        @DexIgnore
        public final CloudImageHelper getInstance() {
            la4 access$getInstance$cp = CloudImageHelper.instance$delegate;
            Companion companion = CloudImageHelper.Companion;
            le4 le4 = $$delegatedProperties[0];
            return (CloudImageHelper) access$getInstance$cp.getValue();
        }

        @DexIgnore
        public final String getTAG() {
            return CloudImageHelper.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Holder {
        @DexIgnore
        public static /* final */ Holder INSTANCE; // = new Holder();

        @DexIgnore
        /* renamed from: INSTANCE  reason: collision with other field name */
        public static /* final */ CloudImageHelper f2INSTANCE; // = new CloudImageHelper();

        @DexIgnore
        public final CloudImageHelper getINSTANCE() {
            return f2INSTANCE;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ItemImage {
        @DexIgnore
        public Constants.CalibrationType mCalibrationType; // = Constants.CalibrationType.NONE;
        @DexIgnore
        public Constants.DeviceType mDeviceType; // = Constants.DeviceType.NONE;
        @DexIgnore
        public File mFile;
        @DexIgnore
        public OnImageCallbackListener mListener;
        @DexIgnore
        public Integer mResourceId; // = -1;
        @DexIgnore
        public String mSerialNumber;
        @DexIgnore
        public String mSerialPrefix;
        @DexIgnore
        public WeakReference<ImageView> mWeakReferenceImageView;

        @DexIgnore
        public ItemImage() {
        }

        @DexIgnore
        public final void download() {
            fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new CloudImageHelper$ItemImage$download$Anon1(this, (yb4) null), 3, (Object) null);
        }

        @DexIgnore
        public final void downloadForCalibration() {
            fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new CloudImageHelper$ItemImage$downloadForCalibration$Anon1(this, (yb4) null), 3, (Object) null);
        }

        @DexIgnore
        public final File getFile() {
            return this.mFile;
        }

        @DexIgnore
        public final ItemImage setFile(File file) {
            this.mFile = file;
            return this;
        }

        @DexIgnore
        public final ItemImage setImageCallback(OnImageCallbackListener onImageCallbackListener) {
            this.mListener = onImageCallbackListener;
            return this;
        }

        @DexIgnore
        public final ItemImage setPlaceHolder(ImageView imageView, int i) {
            kd4.b(imageView, "imageView");
            this.mWeakReferenceImageView = new WeakReference<>(imageView);
            this.mResourceId = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        public final ItemImage setSerialNumber(String str) {
            kd4.b(str, "serialNumber");
            this.mSerialNumber = str;
            return this;
        }

        @DexIgnore
        public final ItemImage setSerialPrefix(String str) {
            kd4.b(str, "serialPrefix");
            this.mSerialPrefix = str;
            return this;
        }

        @DexIgnore
        public final ItemImage setType(Constants.DeviceType deviceType) {
            kd4.b(deviceType, "deviceType");
            this.mDeviceType = deviceType;
            return this;
        }

        @DexIgnore
        public final ItemImage setType(Constants.CalibrationType calibrationType) {
            kd4.b(calibrationType, "calibrationType");
            this.mCalibrationType = calibrationType;
            return this;
        }
    }

    @DexIgnore
    public interface OnForceDownloadCallbackListener {
        @DexIgnore
        void onDownloadCallback(boolean z);
    }

    @DexIgnore
    public interface OnImageCallbackListener {
        @DexIgnore
        void onImageCallback(String str, String str2);
    }

    /*
    static {
        String simpleName = CloudImageHelper$Companion$TAG$Anon1.INSTANCE.getClass().getSimpleName();
        kd4.a((Object) simpleName, "CloudImageHelper::javaClass.javaClass.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public CloudImageHelper() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public static final CloudImageHelper getInstance() {
        return Companion.getInstance();
    }

    @DexIgnore
    public final PortfolioApp getMApp() {
        PortfolioApp portfolioApp = this.mApp;
        if (portfolioApp != null) {
            return portfolioApp;
        }
        kd4.d("mApp");
        throw null;
    }

    @DexIgnore
    public final h42 getMAppExecutors() {
        h42 h42 = this.mAppExecutors;
        if (h42 != null) {
            return h42;
        }
        kd4.d("mAppExecutors");
        throw null;
    }

    @DexIgnore
    public final void setMApp(PortfolioApp portfolioApp) {
        kd4.b(portfolioApp, "<set-?>");
        this.mApp = portfolioApp;
    }

    @DexIgnore
    public final void setMAppExecutors(h42 h42) {
        kd4.b(h42, "<set-?>");
        this.mAppExecutors = h42;
    }

    @DexIgnore
    public final ItemImage with() {
        ItemImage itemImage = new ItemImage();
        if (itemImage.getFile() == null) {
            fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new CloudImageHelper$with$Anon1(this, itemImage, (yb4) null), 3, (Object) null);
        }
        return itemImage;
    }
}
