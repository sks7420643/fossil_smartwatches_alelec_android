package com.portfolio.platform.cloudimage;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$Anon2", f = "URLRequestTaskHelper.kt", l = {}, m = "invokeSuspend")
public final class URLRequestTaskHelper$execute$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ AssetsDeviceResponse $assetsDeviceResponse;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ URLRequestTaskHelper this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public URLRequestTaskHelper$execute$Anon2(URLRequestTaskHelper uRLRequestTaskHelper, AssetsDeviceResponse assetsDeviceResponse, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = uRLRequestTaskHelper;
        this.$assetsDeviceResponse = assetsDeviceResponse;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        URLRequestTaskHelper$execute$Anon2 uRLRequestTaskHelper$execute$Anon2 = new URLRequestTaskHelper$execute$Anon2(this.this$Anon0, this.$assetsDeviceResponse, yb4);
        uRLRequestTaskHelper$execute$Anon2.p$ = (zg4) obj;
        return uRLRequestTaskHelper$execute$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((URLRequestTaskHelper$execute$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            URLRequestTaskHelper.OnNextTaskListener listener$app_fossilRelease = this.this$Anon0.getListener$app_fossilRelease();
            if (listener$app_fossilRelease == null) {
                return null;
            }
            String zipFilePath$app_fossilRelease = this.this$Anon0.getZipFilePath$app_fossilRelease();
            String destinationUnzipPath$app_fossilRelease = this.this$Anon0.getDestinationUnzipPath$app_fossilRelease();
            AssetsDeviceResponse assetsDeviceResponse = this.$assetsDeviceResponse;
            kd4.a((Object) assetsDeviceResponse, "assetsDeviceResponse");
            listener$app_fossilRelease.downloadFile(zipFilePath$app_fossilRelease, destinationUnzipPath$app_fossilRelease, assetsDeviceResponse);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
