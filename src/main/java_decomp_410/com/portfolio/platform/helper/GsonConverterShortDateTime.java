package com.portfolio.platform.helper;

import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.vz1;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class GsonConverterShortDateTime implements vz1<DateTime> {
    @DexIgnore
    public DateTime deserialize(JsonElement jsonElement, Type type, uz1 uz1) throws JsonParseException {
        String f = jsonElement.f();
        if (f.isEmpty()) {
            return null;
        }
        return rk2.b(f);
    }
}
