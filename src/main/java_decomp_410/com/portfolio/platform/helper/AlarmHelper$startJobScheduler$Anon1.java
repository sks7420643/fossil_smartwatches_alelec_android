package com.portfolio.platform.helper;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.receiver.AlarmReceiver;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.helper.AlarmHelper$startJobScheduler$Anon1", f = "AlarmHelper.kt", l = {109}, m = "invokeSuspend")
public final class AlarmHelper$startJobScheduler$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Context $context;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public Object L$Anon2;
    @DexIgnore
    public Object L$Anon3;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmHelper this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmHelper$startJobScheduler$Anon1(AlarmHelper alarmHelper, Context context, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = alarmHelper;
        this.$context = context;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        AlarmHelper$startJobScheduler$Anon1 alarmHelper$startJobScheduler$Anon1 = new AlarmHelper$startJobScheduler$Anon1(this.this$Anon0, this.$context, yb4);
        alarmHelper$startJobScheduler$Anon1.p$ = (zg4) obj;
        return alarmHelper$startJobScheduler$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AlarmHelper$startJobScheduler$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004e  */
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        zg4 zg4;
        List<Alarm> list;
        Iterator<Alarm> it;
        AlarmHelper$startJobScheduler$Anon1 alarmHelper$startJobScheduler$Anon1;
        int i;
        Object a = cc4.a();
        int i2 = this.label;
        if (i2 == 0) {
            na4.a(obj);
            zg4 zg42 = this.p$;
            list = this.this$Anon0.a().getActiveAlarms();
            if (list != null) {
                obj2 = a;
                alarmHelper$startJobScheduler$Anon1 = this;
                zg4 = zg42;
                it = list.iterator();
                while (it.hasNext()) {
                }
            } else {
                alarmHelper$startJobScheduler$Anon1 = this;
            }
        } else if (i2 == 1) {
            it = (Iterator) this.L$Anon3;
            Alarm alarm = (Alarm) this.L$Anon2;
            list = (List) this.L$Anon1;
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            obj2 = a;
            alarmHelper$startJobScheduler$Anon1 = this;
            while (it.hasNext()) {
                Alarm next = it.next();
                if (AlarmHelper.f.a(next)) {
                    next.setActive(false);
                    AlarmsRepository a2 = alarmHelper$startJobScheduler$Anon1.this$Anon0.a();
                    alarmHelper$startJobScheduler$Anon1.L$Anon0 = zg4;
                    alarmHelper$startJobScheduler$Anon1.L$Anon1 = list;
                    alarmHelper$startJobScheduler$Anon1.L$Anon2 = next;
                    alarmHelper$startJobScheduler$Anon1.L$Anon3 = it;
                    alarmHelper$startJobScheduler$Anon1.label = 1;
                    if (a2.updateAlarm(next, alarmHelper$startJobScheduler$Anon1) == obj2) {
                        return obj2;
                    }
                }
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Alarm findNextActiveAlarm = alarmHelper$startJobScheduler$Anon1.this$Anon0.a().findNextActiveAlarm();
        if (findNextActiveAlarm == null) {
            return qa4.a;
        }
        long millisecond = findNextActiveAlarm.getMillisecond();
        Calendar instance = Calendar.getInstance();
        if (instance.get(9) == 1) {
            int i3 = instance.get(10);
            i = i3 == 12 ? 12 : i3 + 12;
        } else {
            i = instance.get(10);
        }
        long j = (((long) ((((i * 60) + instance.get(12)) * 60) + instance.get(13))) * 1000) + ((long) instance.get(14));
        long j2 = j <= millisecond ? millisecond - j : LogBuilder.MAX_INTERVAL - (j - millisecond);
        long currentTimeMillis = System.currentTimeMillis() + j2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AlarmHelper", "startJobScheduler - duration=" + j2);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("AlarmHelper", "startJobScheduler - alarmEnd=" + new Date(currentTimeMillis));
        MFUser currentUser = alarmHelper$startJobScheduler$Anon1.this$Anon0.b().getCurrentUser();
        if (currentUser == null || TextUtils.isEmpty(currentUser.getUserId())) {
            return qa4.a;
        }
        Bundle bundle = new Bundle();
        bundle.putString("DEF_ALARM_RECEIVER_USER_ID", currentUser.getUserId());
        bundle.putInt("DEF_ALARM_RECEIVER_ACTION", 0);
        Intent intent = new Intent(alarmHelper$startJobScheduler$Anon1.$context, AlarmReceiver.class);
        intent.setAction("com.portfolio.platform.ALARM_RECEIVER");
        intent.putExtras(bundle);
        PendingIntent broadcast = PendingIntent.getBroadcast(alarmHelper$startJobScheduler$Anon1.$context, 102, intent, 134217728);
        AlarmManager alarmManager = (AlarmManager) alarmHelper$startJobScheduler$Anon1.$context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        local3.d("AlarmHelper", "startJobScheduler - inexactAlarm=" + alarmManager);
        if (j2 <= 3600000 || alarmManager == null) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("DEF_ALARM_RECEIVER_USER_ID", currentUser.getUserId());
            bundle2.putInt("DEF_ALARM_RECEIVER_ACTION", 3);
            Intent intent2 = new Intent(alarmHelper$startJobScheduler$Anon1.$context, AlarmReceiver.class);
            intent2.setAction("com.portfolio.platform.ALARM_RECEIVER");
            intent2.putExtras(bundle2);
            AlarmManager alarmManager2 = (AlarmManager) alarmHelper$startJobScheduler$Anon1.$context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            if (alarmManager2 != null) {
                alarmManager2.setExact(0, currentTimeMillis, PendingIntent.getBroadcast(alarmHelper$startJobScheduler$Anon1.$context, 101, intent2, 134217728));
            }
        } else {
            alarmManager.set(0, currentTimeMillis - 3600000, broadcast);
        }
        return qa4.a;
    }
}
