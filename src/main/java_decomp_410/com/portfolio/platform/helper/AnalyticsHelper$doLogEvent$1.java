package com.portfolio.platform.helper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.helper.AnalyticsHelper$doLogEvent$1", mo27670f = "AnalyticsHelper.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class AnalyticsHelper$doLogEvent$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ android.os.Bundle $data;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $event;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21167p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnalyticsHelper$doLogEvent$1(java.lang.String str, android.os.Bundle bundle, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$event = str;
        this.$data = bundle;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.helper.AnalyticsHelper$doLogEvent$1 analyticsHelper$doLogEvent$1 = new com.portfolio.platform.helper.AnalyticsHelper$doLogEvent$1(this.$event, this.$data, yb4);
        analyticsHelper$doLogEvent$1.f21167p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return analyticsHelper$doLogEvent$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.helper.AnalyticsHelper$doLogEvent$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.google.firebase.analytics.FirebaseAnalytics d = com.portfolio.platform.helper.AnalyticsHelper.f21162c;
            if (d != null) {
                d.mo23033a(this.$event, this.$data);
            }
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
