package com.portfolio.platform.helper;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.yb4;
import kotlin.coroutines.jvm.internal.ContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.portfolio.platform.helper.WatchParamHelper", f = "WatchParamHelper.kt", l = {37}, m = "handleFailureResponseWatchParam")
public final class WatchParamHelper$handleFailureResponseWatchParam$Anon1 extends ContinuationImpl {
    @DexIgnore
    public float F$Anon0;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ WatchParamHelper this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchParamHelper$handleFailureResponseWatchParam$Anon1(WatchParamHelper watchParamHelper, yb4 yb4) {
        super(yb4);
        this.this$Anon0 = watchParamHelper;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$Anon0.a((String) null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this);
    }
}
