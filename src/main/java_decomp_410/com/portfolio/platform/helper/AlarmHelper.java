package com.portfolio.platform.helper;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.receiver.AlarmReceiver;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Calendar;
import java.util.Date;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmHelper {
    @DexIgnore
    public static /* final */ a f; // = new a((fd4) null);
    @DexIgnore
    public /* final */ AlarmManager a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ en2 c;
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ AlarmsRepository e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(String str) {
            kd4.b(str, "day");
            String lowerCase = str.toLowerCase();
            kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            switch (lowerCase.hashCode()) {
                case 101661:
                    if (lowerCase.equals("fri")) {
                        return 6;
                    }
                    break;
                case 108300:
                    if (lowerCase.equals("mon")) {
                        return 2;
                    }
                    break;
                case 113638:
                    if (lowerCase.equals("sat")) {
                        return 7;
                    }
                    break;
                case 114252:
                    if (lowerCase.equals("sun")) {
                        return 1;
                    }
                    break;
                case 114817:
                    if (lowerCase.equals("thu")) {
                        return 5;
                    }
                    break;
                case 115204:
                    if (lowerCase.equals("tue")) {
                        return 3;
                    }
                    break;
                case 117590:
                    if (lowerCase.equals("wed")) {
                        return 4;
                    }
                    break;
            }
            return -1;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final long a(long j) {
            int i;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmHelper", "getEndTimeFromAlarmMinute - duration=" + j);
            Calendar instance = Calendar.getInstance();
            if (instance.get(9) == 1) {
                int i2 = instance.get(10);
                i = i2 == 12 ? 12 : i2 + 12;
            } else {
                i = instance.get(10);
            }
            long j2 = ((long) ((((i * 60) + instance.get(12)) * 60) + instance.get(13))) * 1000;
            long j3 = j2 <= j ? j - j2 : LogBuilder.MAX_INTERVAL - (j2 - j);
            long currentTimeMillis = System.currentTimeMillis() + j;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("AlarmHelper", "getEndTimeFromAlarmMinute - duration=" + j3);
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("AlarmHelper", "getEndTimeFromAlarmMinute - currentSecond=" + instance.get(13));
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("AlarmHelper", "getEndTimeFromAlarmMinute - alarmEnd=" + new Date(currentTimeMillis));
            return currentTimeMillis;
        }

        @DexIgnore
        public final boolean a(Alarm alarm) {
            kd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            if (alarm.isRepeated() || !alarm.isActive()) {
                return false;
            }
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "calendar");
            instance.setTime(rk2.d(alarm.getUpdatedAt()));
            int i = instance.get(9);
            int i2 = instance.get(10);
            if (i == 1 && i2 < 12) {
                i2 += 12;
            }
            Calendar instance2 = Calendar.getInstance();
            kd4.a((Object) instance2, "instanceCalendar");
            long timeInMillis = instance2.getTimeInMillis();
            rk2.d(instance2);
            kd4.a((Object) instance2, "DateHelper.getStartOfDay(instanceCalendar)");
            long timeInMillis2 = timeInMillis - instance2.getTimeInMillis();
            long millisecond = alarm.getMillisecond();
            if ((((long) ((((i2 * 60) + instance.get(12)) * 60) + instance.get(13))) * 1000) + 1 <= millisecond && timeInMillis2 > millisecond) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public AlarmHelper(Context context, en2 en2, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        kd4.b(context, "mContext");
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(alarmsRepository, "mAlarmsRepository");
        this.b = context;
        this.c = en2;
        this.d = userRepository;
        this.e = alarmsRepository;
        Object systemService = this.b.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        if (systemService != null) {
            this.a = (AlarmManager) systemService;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.app.AlarmManager");
    }

    @DexIgnore
    public final AlarmsRepository a() {
        return this.e;
    }

    @DexIgnore
    public final UserRepository b() {
        return this.d;
    }

    @DexIgnore
    public final void c() {
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startExactReplaceBatteryAlarm");
        Calendar instance = Calendar.getInstance();
        instance.set(11, 9);
        instance.set(12, 0);
        instance.set(13, 0);
        kd4.a((Object) instance, "triggerCalendar");
        long timeInMillis = instance.getTimeInMillis();
        Intent intent = new Intent(this.b, AlarmReceiver.class);
        intent.putExtra("REQUEST_CODE", 1);
        PendingIntent broadcast = PendingIntent.getBroadcast(this.b, 1, intent, 134217728);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AlarmHelper", "startExactReplaceBatteryAlarm - triggerAtMillis=" + timeInMillis);
        if (Build.VERSION.SDK_INT >= 23) {
            this.a.setExactAndAllowWhileIdle(0, timeInMillis, broadcast);
        } else {
            this.a.setExact(0, timeInMillis, broadcast);
        }
    }

    @DexIgnore
    public final void d() {
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startHWLogScheduler");
        Intent intent = new Intent(this.b, AlarmReceiver.class);
        intent.putExtra("REQUEST_CODE", 10);
        PendingIntent broadcast = PendingIntent.getBroadcast(this.b, 10, intent, 134217728);
        this.a.cancel(broadcast);
        Calendar instance = Calendar.getInstance();
        instance.set(11, 10);
        instance.set(12, 0);
        instance.set(13, 0);
        kd4.a((Object) instance, "triggerCalendar");
        long timeInMillis = instance.getTimeInMillis();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AlarmHelper", "startHWLogScheduler - setInexactRepeating: triggerAtMillis=" + timeInMillis + ", interval=" + LogBuilder.MAX_INTERVAL);
        this.a.setInexactRepeating(0, timeInMillis, LogBuilder.MAX_INTERVAL, broadcast);
    }

    @DexIgnore
    public final void e() {
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startRemindSyncAlarm");
        Calendar instance = Calendar.getInstance();
        if (Calendar.getInstance().get(11) >= 15) {
            instance.add(6, 1);
        }
        instance.set(11, 15);
        instance.set(12, 0);
        instance.set(13, 0);
        Bundle bundle = new Bundle();
        bundle.putInt("DEF_ALARM_RECEIVER_ACTION", 2);
        Intent intent = new Intent(this.b, AlarmReceiver.class);
        intent.putExtras(bundle);
        PendingIntent broadcast = PendingIntent.getBroadcast(this.b, 20, intent, 268435456);
        AlarmManager alarmManager = this.a;
        kd4.a((Object) instance, "calendar");
        alarmManager.setRepeating(0, instance.getTimeInMillis(), LogBuilder.MAX_INTERVAL, broadcast);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AlarmHelper", "schedule SyncAlarm at = " + new Date(instance.getTimeInMillis()));
    }

    @DexIgnore
    public final void f() {
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "stopReplaceBatteryAlarm");
        this.c.u("");
        Intent intent = new Intent(this.b, AlarmReceiver.class);
        Intent intent2 = new Intent(this.b, AlarmReceiver.class);
        intent.putExtra("REQUEST_CODE", 0);
        intent2.putExtra("REQUEST_CODE", 1);
        PendingIntent broadcast = PendingIntent.getBroadcast(this.b, 0, intent, 134217728);
        PendingIntent broadcast2 = PendingIntent.getBroadcast(this.b, 1, intent2, 134217728);
        this.a.cancel(broadcast);
        this.a.cancel(broadcast2);
    }

    @DexIgnore
    public final void a(Context context) {
        kd4.b(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "cancelJobScheduler");
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.setAction("com.portfolio.platform.ALARM_RECEIVER");
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 101, intent, 134217728);
        if (!(broadcast == null || alarmManager == null)) {
            alarmManager.cancel(broadcast);
        }
        AlarmManager alarmManager2 = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        Intent intent2 = new Intent(context, AlarmReceiver.class);
        intent2.setAction("com.portfolio.platform.ALARM_RECEIVER");
        PendingIntent broadcast2 = PendingIntent.getBroadcast(context, 102, intent2, 134217728);
        if (alarmManager2 != null) {
            alarmManager2.cancel(broadcast2);
        }
    }

    @DexIgnore
    public final void b(Context context) {
        kd4.b(context, "context");
        Calendar instance = Calendar.getInstance();
        int i = (instance.get(11) * 60) + instance.get(12);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AlarmHelper", "endJobScheduler - currentMinute=" + i);
        fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new AlarmHelper$endJobScheduler$Anon1(this, i, context, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void c(Context context) {
        kd4.b(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startJobScheduler");
        a(context);
        fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new AlarmHelper$startJobScheduler$Anon1(this, context, (yb4) null), 3, (Object) null);
    }
}
