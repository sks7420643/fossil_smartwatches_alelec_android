package com.portfolio.platform.helper.mms;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MmsException extends Exception {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -7323249827281485390L;

    @DexIgnore
    public MmsException() {
    }

    @DexIgnore
    public MmsException(String str) {
        super(str);
    }

    @DexIgnore
    public MmsException(Throwable th) {
        super(th);
    }

    @DexIgnore
    public MmsException(String str, Throwable th) {
        super(str, th);
    }
}
