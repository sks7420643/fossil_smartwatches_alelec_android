package com.portfolio.platform.helper;

import com.fossil.blesdk.obfuscated.a02;
import com.fossil.blesdk.obfuscated.b02;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rk2;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.vz1;
import com.fossil.blesdk.obfuscated.zz1;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GsonISOConvertDateTime implements vz1<Date>, b02<Date> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(Date date, Type type, a02 a02) {
        String str;
        if (date == null) {
            str = "";
        } else {
            SimpleDateFormat simpleDateFormat = rk2.l.get();
            if (simpleDateFormat != null) {
                str = simpleDateFormat.format(date);
                kd4.a((Object) str, "DateHelper.SERVER_DATE_I\u2026ORMAT.get()!!.format(src)");
            } else {
                kd4.a();
                throw null;
            }
        }
        return new zz1(str);
    }

    @DexIgnore
    public Date deserialize(JsonElement jsonElement, Type type, uz1 uz1) {
        if (jsonElement != null) {
            String f = jsonElement.f();
            if (f != null) {
                try {
                    SimpleDateFormat simpleDateFormat = rk2.l.get();
                    if (simpleDateFormat != null) {
                        Date parse = simpleDateFormat.parse(f);
                        SimpleDateFormat simpleDateFormat2 = rk2.j.get();
                        if (simpleDateFormat2 != null) {
                            SimpleDateFormat simpleDateFormat3 = simpleDateFormat2;
                            SimpleDateFormat simpleDateFormat4 = rk2.j.get();
                            if (simpleDateFormat4 != null) {
                                Date parse2 = simpleDateFormat3.parse(simpleDateFormat4.format(parse));
                                kd4.a((Object) parse2, "DateHelper.LOCAL_DATE_SP\u2026MAT.get()!!.format(date))");
                                return parse2;
                            }
                            kd4.a();
                            throw null;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                } catch (ParseException e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("deserialize - json=");
                    sb.append(jsonElement.f());
                    sb.append(", e=");
                    e.printStackTrace();
                    sb.append(qa4.a);
                    local.e("GsonISOConvertDateTime", sb.toString());
                }
            }
        }
        return new Date(0);
    }
}
