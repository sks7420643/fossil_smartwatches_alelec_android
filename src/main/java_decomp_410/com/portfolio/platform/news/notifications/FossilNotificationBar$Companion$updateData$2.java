package com.portfolio.platform.news.notifications;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$2", mo27670f = "FossilNotificationBar.kt", mo27671l = {53}, mo27672m = "invokeSuspend")
public final class FossilNotificationBar$Companion$updateData$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $content;
    @DexIgnore
    public /* final */ /* synthetic */ android.content.Context $context;
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.String $title;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f21306p$;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$2$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$2$1", mo27670f = "FossilNotificationBar.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$2$1 */
    public static final class C59791 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.news.notifications.FossilNotificationBar $fossilNotificationBar;
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f21307p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C59791(com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$2 fossilNotificationBar$Companion$updateData$2, com.portfolio.platform.news.notifications.FossilNotificationBar fossilNotificationBar, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = fossilNotificationBar$Companion$updateData$2;
            this.$fossilNotificationBar = fossilNotificationBar;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$2.C59791 r0 = new com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$2.C59791(this.this$0, this.$fossilNotificationBar, yb4);
            r0.f21307p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$2.C59791) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.portfolio.platform.news.notifications.FossilNotificationBar.Companion.m32192b(com.portfolio.platform.news.notifications.FossilNotificationBar.f21301c, this.this$0.$context, this.$fossilNotificationBar, false, 4, (java.lang.Object) null);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FossilNotificationBar$Companion$updateData$2(java.lang.String str, java.lang.String str2, android.content.Context context, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.$content = str;
        this.$title = str2;
        this.$context = context;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$2 fossilNotificationBar$Companion$updateData$2 = new com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$2(this.$content, this.$title, this.$context, yb4);
        fossilNotificationBar$Companion$updateData$2.f21306p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return fossilNotificationBar$Companion$updateData$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f21306p$;
            com.portfolio.platform.news.notifications.FossilNotificationBar fossilNotificationBar = new com.portfolio.platform.news.notifications.FossilNotificationBar(this.$content, this.$title);
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            local.mo33255d("FossilNotificationBar", "content " + this.$content);
            com.fossil.blesdk.obfuscated.pi4 c = com.fossil.blesdk.obfuscated.nh4.m25693c();
            com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$2.C59791 r4 = new com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$2.C59791(this, fossilNotificationBar, (com.fossil.blesdk.obfuscated.yb4) null);
            this.L$0 = zg4;
            this.L$1 = fossilNotificationBar;
            this.label = 1;
            if (com.fossil.blesdk.obfuscated.yf4.m30997a(c, r4, this) == a) {
                return a;
            }
        } else if (i == 1) {
            com.portfolio.platform.news.notifications.FossilNotificationBar fossilNotificationBar2 = (com.portfolio.platform.news.notifications.FossilNotificationBar) this.L$1;
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
