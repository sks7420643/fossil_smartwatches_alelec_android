package com.portfolio.platform.news.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.dn2;
import com.fossil.blesdk.obfuscated.en2;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.vj2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationReceiver extends BroadcastReceiver {
    @DexIgnore
    public en2 a;
    @DexIgnore
    public vj2 b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public NotificationReceiver() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        FLogger.INSTANCE.getLocal().d("NotificationReceiver", "onReceive");
        if (dn2.p.a().n().b() == null) {
            FLogger.INSTANCE.getLocal().d("NotificationReceiver", "onReceive - user is NULL!!!");
            return;
        }
        int intExtra = intent != null ? intent.getIntExtra("ACTION_EVENT", 0) : 0;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("NotificationReceiver", "onReceive - actionEvent=" + intExtra);
        if (intExtra == 1) {
            PortfolioApp c = PortfolioApp.W.c();
            vj2 vj2 = this.b;
            if (vj2 != null) {
                c.a(vj2, false, 10);
            } else {
                kd4.d("mDeviceSettingFactory");
                throw null;
            }
        }
    }
}
