package com.fossil;

import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Rl6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Gu7 {
    @DexIgnore
    public static final <T> Rl6<T> a(Il6 il6, Af6 af6, Lv7 lv7, Coroutine<? super Il6, ? super Xe6<? super T>, ? extends Object> coroutine) {
        Af6 c = Cv7.c(il6, af6);
        Sv7 hx7 = lv7.isLazy() ? new Hx7(c, coroutine) : new Sv7(c, true);
        ((Au7) hx7).z0(lv7, hx7, coroutine);
        return hx7;
    }

    @DexIgnore
    public static /* synthetic */ Rl6 b(Il6 il6, Af6 af6, Lv7 lv7, Coroutine coroutine, int i, Object obj) {
        if ((i & 1) != 0) {
            af6 = Un7.INSTANCE;
        }
        if ((i & 2) != 0) {
            lv7 = Lv7.DEFAULT;
        }
        return Eu7.a(il6, af6, lv7, coroutine);
    }

    @DexIgnore
    public static final Rm6 c(Il6 il6, Af6 af6, Lv7 lv7, Coroutine<? super Il6, ? super Xe6<? super Cd6>, ? extends Object> coroutine) {
        Af6 c = Cv7.c(il6, af6);
        Au7 ix7 = lv7.isLazy() ? new Ix7(c, coroutine) : new Rx7(c, true);
        ix7.z0(lv7, ix7, coroutine);
        return ix7;
    }

    @DexIgnore
    public static /* synthetic */ Rm6 d(Il6 il6, Af6 af6, Lv7 lv7, Coroutine coroutine, int i, Object obj) {
        if ((i & 1) != 0) {
            af6 = Un7.INSTANCE;
        }
        if ((i & 2) != 0) {
            lv7 = Lv7.DEFAULT;
        }
        return Eu7.c(il6, af6, lv7, coroutine);
    }

    @DexIgnore
    public static final <T> Object e(Af6 af6, Coroutine<? super Il6, ? super Xe6<? super T>, ? extends Object> coroutine, Xe6<? super T> xe6) {
        Object A0;
        Af6 context = xe6.getContext();
        Af6 plus = context.plus(af6);
        Fy7.a(plus);
        if (plus == context) {
            Tz7 tz7 = new Tz7(plus, xe6);
            A0 = E08.c(tz7, tz7, coroutine);
        } else if (Wg6.a((Rn7) plus.get(Rn7.p), (Rn7) context.get(Rn7.p))) {
            Dy7 dy7 = new Dy7(plus, xe6);
            Object c = Zz7.c(plus, null);
            try {
                A0 = E08.c(dy7, dy7, coroutine);
            } finally {
                Zz7.a(plus, c);
            }
        } else {
            Xv7 xv7 = new Xv7(plus, xe6);
            xv7.v0();
            D08.c(coroutine, xv7, xv7);
            A0 = xv7.A0();
        }
        if (A0 == Yn7.d()) {
            Go7.c(xe6);
        }
        return A0;
    }
}
