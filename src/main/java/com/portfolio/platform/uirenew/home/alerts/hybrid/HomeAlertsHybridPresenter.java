package com.portfolio.platform.uirenew.home.alerts.hybrid;

import android.content.Context;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Jn5;
import com.fossil.Ko7;
import com.fossil.Lm7;
import com.fossil.Ls0;
import com.fossil.Mn7;
import com.fossil.Qi5;
import com.fossil.Tc7;
import com.fossil.Uh5;
import com.fossil.X36;
import com.fossil.Y36;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.HomeAlertsHybridFragment;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import com.mapped.HomeAlertsFragment;
import androidx.fragment.app.FragmentActivity;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexAppend;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;
import lanchon.dexpatcher.annotation.DexWrap;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeAlertsHybridPresenter extends X36 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ Ai o; // = new Ai(null);
    @DexIgnore
    public LiveData<String> e; // = PortfolioApp.get.instance().K();
    @DexIgnore
    public ArrayList<Alarm> f; // = new ArrayList<>();
    @DexIgnore
    public boolean g;
    @DexIgnore
    public volatile boolean h;
    @DexIgnore
    public /* final */ Y36 i;
    @DexIgnore
    public /* final */ AlarmHelper j;
    @DexIgnore
    public /* final */ SetAlarms k;
    @DexIgnore
    public /* final */ AlarmsRepository l;
    @DexIgnore
    public /* final */ An4 m;

    // Ensure changes are reflected in HomeAlertsHybridPresenter
    @DexAdd
    public Alarm alarmToAdd = null;
    @DexAdd
    public boolean closeOnFinish = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return HomeAlertsHybridPresenter.n;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ei<SetAlarms.Di, SetAlarms.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ Alarm b;

        @DexIgnore
        public Bi(HomeAlertsHybridPresenter homeAlertsHybridPresenter, Alarm alarm) {
            this.a = homeAlertsHybridPresenter;
            this.b = alarm;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(SetAlarms.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(SetAlarms.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.i.a();
            int c = bi.c();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsHybridPresenter.o.a();
            local.d(a2, "enableAlarm() - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.a.i.c();
                } else if (!(c == 1112 || c == 1113)) {
                    this.a.i.v0();
                }
                this.a.D(bi.a(), false);
                return;
            }
            List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(bi.b());
            Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            Y36 y36 = this.a.i;
            Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
            if (array != null) {
                Uh5[] uh5Arr = (Uh5[]) array;
                y36.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                this.a.D(bi.a(), false);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

//        @DexIgnore
//        public void c(SetAlarms.Di di) {
//            Wg6.c(di, "responseValue");
//            ILocalFLogger local = FLogger.INSTANCE.getLocal();
//            String a2 = HomeAlertsHybridPresenter.o.a();
//            local.d(a2, "enableAlarm - onSuccess: alarmUri = " + di.a().getUri() + ", alarmId = " + di.a().getId());
//            this.a.i.a();
//            this.a.D(this.b, true);
//        }

        // Ensure changes are reflected in HomeAlertsHybridPresenter
        @DexAppend
        public void c(SetAlarms.Di di) {
            if (this.a.closeOnFinish) {
                HomeAlertsHybridFragment f = (HomeAlertsHybridFragment) this.a.i;
                FragmentActivity activity = f.getActivity();
                if (activity != null) {
                    activity.finish();
                }

            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(SetAlarms.Di di) {
            c(di);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridPresenter a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1", f = "HomeAlertsHybridPresenter.kt", l = {62}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $deviceId;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1$allAlarms$1", f = "HomeAlertsHybridPresenter.kt", l = {63, 68}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super List<Alarm>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super List<Alarm>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Il6 il6;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        il6 = this.p$;
                        AlarmHelper alarmHelper = this.this$0.this$0.a.j;
                        this.L$0 = il6;
                        this.label = 1;
                        if (alarmHelper.j(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        il6 = (Il6) this.L$0;
                        El7.b(obj);
                    } else if (i == 2) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (!this.this$0.this$0.a.h) {
                        this.this$0.this$0.a.h = true;
                        AlarmHelper alarmHelper2 = this.this$0.this$0.a.j;
                        Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                        Wg6.b(applicationContext, "PortfolioApp.instance.applicationContext");
                        alarmHelper2.g(applicationContext);
                    }
                    AlarmsRepository alarmsRepository = this.this$0.this$0.a.l;
                    this.L$0 = il6;
                    this.label = 2;
                    Object allAlarmIgnoreDeletePinType = alarmsRepository.getAllAlarmIgnoreDeletePinType(this);
                    return allAlarmIgnoreDeletePinType == d ? d : allAlarmIgnoreDeletePinType;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Biii<T> implements Comparator<T> {
                @DexIgnore
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    throw null;
//                    return Mn7.c(Integer.valueOf(t.getTotalMinutes()), Integer.valueOf(t2.getTotalMinutes()));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, String str, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$deviceId = str;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$deviceId, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            // Ensure changes are reflected in HomeAlertsHybridPresenter
            @DexWrap
            @Override
            public final Object invokeSuspend(Object obj) {
                int i = this.label;
                Object ret = invokeSuspend(obj);
                if (i == 1) {
                    // alarms are loaded
                    HomeAlertsHybridPresenter ap = this.this$0.a;
                    if (ap.alarmToAdd != null) {
                        Alarm alarmToAdd = ap.alarmToAdd;
                        ap.alarmToAdd = null;
                        ap.closeOnFinish = true;

                        ArrayList<Alarm> list = ap.f;
                        list.add(alarmToAdd);
                        ap.o(alarmToAdd, true); // function with 'enableAlarm - alarmTotalMinu'
                    }
                }
                return ret;
            }

//            @DexIgnore
//            @Override // com.fossil.Zn7
//            public final Object invokeSuspend(Object obj) {
//                Object g;
//                Object d = Yn7.d();
//                int i = this.label;
//                if (i == 0) {
//                    El7.b(obj);
//                    Il6 il6 = this.p$;
//                    boolean o = DeviceHelper.o.j().o(this.$deviceId);
//                    this.this$0.a.i.j4(o);
//                    if (o) {
//                        Dv7 i2 = this.this$0.a.i();
//                        Aiii aiii = new Aiii(this, null);
//                        this.L$0 = il6;
//                        this.Z$0 = o;
//                        this.label = 1;
//                        g = Eu7.g(i2, aiii, this);
//                        if (g == d) {
//                            return d;
//                        }
//                    }
//                    HomeAlertsHybridPresenter homeAlertsHybridPresenter = this.this$0.a;
//                    homeAlertsHybridPresenter.g = homeAlertsHybridPresenter.m.Z();
//                    this.this$0.a.i.N1(this.this$0.a.g);
//                    this.this$0.a.i.u3(this.this$0.a.g);
//                    return Cd6.a;
//                } else if (i == 1) {
//                    Il6 il62 = (Il6) this.L$0;
//                    El7.b(obj);
//                    g = obj;
//                } else {
//                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
//                }
//                List<Alarm> list = (List) g;
//                ILocalFLogger local = FLogger.INSTANCE.getLocal();
//                String a2 = HomeAlertsHybridPresenter.o.a();
//                local.d(a2, "GetAlarms onSuccess: size = " + list.size());
//                this.this$0.a.f.clear();
//                for (Alarm alarm : list) {
//                    throw null;
////                    this.this$0.a.f.add(Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
//                }
//                ArrayList arrayList = this.this$0.a.f;
//                if (arrayList.size() > 1) {
//                    Lm7.r(arrayList, new Biii());
//                }
//                this.this$0.a.i.p0(this.this$0.a.f);
//                HomeAlertsHybridPresenter homeAlertsHybridPresenter2 = this.this$0.a;
//                homeAlertsHybridPresenter2.g = homeAlertsHybridPresenter2.m.Z();
//                this.this$0.a.i.N1(this.this$0.a.g);
//                this.this$0.a.i.u3(this.this$0.a.g);
//                return Cd6.a;
//            }
        }

        @DexIgnore
        public Ci(HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
            this.a = homeAlertsHybridPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            if (str == null || str.length() == 0) {
                this.a.i.r(true);
            } else {
                Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, str, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridPresenter a;

        @DexIgnore
        public Di(HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
            this.a = homeAlertsHybridPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            Y36 unused = this.a.i;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    /*
    static {
        String simpleName = HomeAlertsHybridPresenter.class.getSimpleName();
        Wg6.b(simpleName, "HomeAlertsHybridPresenter::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public HomeAlertsHybridPresenter(Y36 y36, AlarmHelper alarmHelper, SetAlarms setAlarms, AlarmsRepository alarmsRepository, An4 an4) {
        Wg6.c(y36, "mView");
        Wg6.c(alarmHelper, "mAlarmHelper");
        Wg6.c(setAlarms, "mSetAlarms");
        Wg6.c(alarmsRepository, "mAlarmRepository");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.i = y36;
        this.j = alarmHelper;
        this.k = setAlarms;
        this.l = alarmsRepository;
        this.m = an4;
    }

    @DexIgnore
    public final void D(Alarm alarm, boolean z) {
        Wg6.c(alarm, "editAlarm");
        Iterator<Alarm> it = this.f.iterator();
        while (it.hasNext()) {
            Alarm next = it.next();
            if (Wg6.a(next.getUri(), alarm.getUri())) {
                ArrayList<Alarm> arrayList = this.f;
                throw null;
//                arrayList.set(arrayList.indexOf(next), Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
//                if (!z) {
//                    break;
//                }
//                E();
            }
        }
        this.i.p0(this.f);
    }

    @DexIgnore
    public final void E() {
        FLogger.INSTANCE.getLocal().d(n, "onSetAlarmsSuccess");
        this.j.g(PortfolioApp.get.instance());
        PortfolioApp instance = PortfolioApp.get.instance();
        String e2 = this.e.e();
        if (e2 != null) {
            Wg6.b(e2, "mActiveSerial.value!!");
            instance.P0(e2);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public void F() {
        this.i.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(n, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.k.s();
        PortfolioApp.get.h(this);
        LiveData<String> liveData = this.e;
        Y36 y36 = this.i;
        if (y36 != null) {
            liveData.h((HomeAlertsHybridFragment) y36, new Ci(this));
            BleCommandResultManager.d.g(CommunicateMode.SET_LIST_ALARM);
            Y36 y362 = this.i;
            throw null;
//            y362.F(!Jn5.c(Jn5.b, ((HomeAlertsHybridFragment) y362).getContext(), Jn5.Ai.NOTIFICATION_HYBRID, false, false, false, null, 56, null));
//            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(n, "stop");
        this.e.m(new Di(this));
        this.k.w();
        PortfolioApp.get.l(this);
    }

    @DexIgnore
    @Override // com.fossil.X36
    public void n() {
        Jn5 jn5 = Jn5.b;
        Y36 y36 = this.i;
        if (y36 != null) {
            throw null;
//            Jn5.c(jn5, ((HomeAlertsHybridFragment) y36).getContext(), Jn5.Ai.NOTIFICATION_HYBRID, false, false, false, null, 60, null);
//            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
    }

    @DexIgnore
    @Override // com.fossil.X36
    public void o(Alarm alarm, boolean z) {
        Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        String e2 = this.e.e();
        if (!(e2 == null || e2.length() == 0)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = n;
            local.d(str, "enableAlarm - alarmTotalMinues: " + alarm.getTotalMinutes() + " - enable: " + z);
            alarm.setActive(z);
            this.i.b();
            SetAlarms setAlarms = this.k;
            String e3 = this.e.e();
            if (e3 != null) {
                Wg6.b(e3, "mActiveSerial.value!!");
                setAlarms.e(new SetAlarms.Ci(e3, this.f, alarm), new Bi(this, alarm));
                return;
            }
            Wg6.i();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(n, "enableAlarm - Current Active Device Serial Is Empty");
    }

    @DexIgnore
    @Tc7
    public final void onSetAlarmEventEndComplete(Qi5 qi5) {
        FLogger.INSTANCE.getLocal().d(n, "onSetAlarmEventEndComplete()");
        if (qi5 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = n;
            local.d(str, "onSetAlarmEventEndComplete() - event = " + qi5);
            if (qi5.b()) {
                String a2 = qi5.a();
                Iterator<Alarm> it = this.f.iterator();
                while (it.hasNext()) {
                    Alarm next = it.next();
                    if (Wg6.a(next.getUri(), a2)) {
                        next.setActive(false);
                    }
                }
                this.i.W();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.X36
    public void p() {
        boolean z = !this.g;
        this.g = z;
        this.m.V0(z);
        this.i.N1(this.g);
        this.i.u3(this.g);
    }

    @DexIgnore
    @Override // com.fossil.X36
    public void q(Alarm alarm) {
        String e2 = this.e.e();
        if (e2 == null || e2.length() == 0) {
            FLogger.INSTANCE.getLocal().d(n, "Current Active Device Serial Is Empty");
        } else if (alarm != null || this.f.size() < 32) {
            Y36 y36 = this.i;
            String e3 = this.e.e();
            if (e3 != null) {
                Wg6.b(e3, "mActiveSerial.value!!");
                y36.l0(e3, this.f, alarm);
                return;
            }
            Wg6.i();
            throw null;
        } else {
            this.i.U();
        }
    }
}
