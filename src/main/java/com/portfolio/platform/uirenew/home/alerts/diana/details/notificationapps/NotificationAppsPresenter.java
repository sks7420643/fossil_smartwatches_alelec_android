package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;

import com.facebook.share.internal.VideoUploader;
import com.fossil.D26;
import com.fossil.D47;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Jn5;
import com.fossil.Jq4;
import com.fossil.Ko7;
import com.fossil.M06;
import com.fossil.N06;
import com.fossil.Tq4;
import com.fossil.U06;
import com.fossil.Uh5;
import com.fossil.Um5;
import com.fossil.Uq4;
import com.fossil.V36;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.mapped.An4;
import com.mapped.AppWrapper;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.PermissionUtils;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.service.BleCommandResultManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexPrepend;
import lanchon.dexpatcher.annotation.DexWrap;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationAppsPresenter extends M06 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public boolean e; // = this.p.W();
    @DexIgnore
    public List<InstalledApp> f; // = new ArrayList();
    @DexIgnore
    public List<AppWrapper> g; // = new ArrayList();
    @DexIgnore
    public List<AppNotificationFilter> h; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> i; // = new ArrayList();
    @DexIgnore
    public /* final */ Bi j; // = new Bi();
    @DexIgnore
    public /* final */ N06 k;
    @DexIgnore
    public /* final */ Uq4 l;
    @DexIgnore
    public /* final */ V36 m;
    @DexIgnore
    public /* final */ D26 n;
    @DexIgnore
    public /* final */ U06 o;
    @DexIgnore
    public /* final */ An4 p;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase q;
    @DexAdd
    public boolean ListRestored;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return NotificationAppsPresenter.r;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi implements BleCommandResultManager.Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public /*static*/ final class Aii implements Tq4.Di<U06.Bi, Tq4.Ai> {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Aii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Tq4.Di
            public /* bridge */ /* synthetic */ void a(Tq4.Ai ai) {
                b(ai);
            }

            @DexIgnore
            public void b(Tq4.Ai ai) {
                FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), ".Inside mSaveAppsNotification onError");
                NotificationAppsPresenter.this.k.a();
                NotificationAppsPresenter.this.k.close();
            }

            @DexIgnore
            public void c(U06.Bi bi) {
                FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), ".Inside mSaveAppsNotification onSuccess");
                NotificationAppsPresenter.this.k.a();
                NotificationAppsPresenter.this.k.close();
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Tq4.Di
            public /* bridge */ /* synthetic */ void onSuccess(U06.Bi bi) {
                c(bi);
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "SetNotificationFilterReceiver");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationAppsPresenter.s.a();
            local.d(a2, "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
            if (communicateMode != CommunicateMode.SET_NOTIFICATION_FILTERS) {
                return;
            }
            if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "onReceive - success");
                NotificationAppsPresenter.this.p.J0(NotificationAppsPresenter.this.H());
                NotificationAppsPresenter.this.l.a(NotificationAppsPresenter.this.o, new U06.Ai(NotificationAppsPresenter.this.I()), new Aii(this));
                return;
            }
            FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "onReceive - failed");
            int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
            ArrayList<Integer> arrayList = integerArrayListExtra != null ? integerArrayListExtra : new ArrayList<>(intExtra2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = NotificationAppsPresenter.s.a();
            local2.d(a3, "permissionErrorCodes=" + arrayList + " , size=" + arrayList.size());
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = NotificationAppsPresenter.s.a();
                local3.d(a4, "error code " + i + " =" + arrayList.get(i));
            }
            if (intExtra2 != 1101) {
                if (intExtra2 == 1924) {
                    NotificationAppsPresenter.this.k.u();
                    return;
                } else if (intExtra2 == 8888) {
                    NotificationAppsPresenter.this.k.c();
                    return;
                } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                    return;
                }
            }
            List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(arrayList);
            Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
            N06 n06 = NotificationAppsPresenter.this.k;
            Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
            if (array != null) {
                Uh5[] uh5Arr = (Uh5[]) array;
                n06.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1", f = "NotificationAppsPresenter.kt", l = {130, 135, 184, 192, 201, 210}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$1", f = "NotificationAppsPresenter.kt", l = {131}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    PortfolioApp instance = PortfolioApp.get.instance();
                    this.L$0 = il6;
                    this.label = 1;
                    if (instance.V0(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$6", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $notificationSettings;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ Bii b;

                @DexIgnore
                public Aiii(Bii bii) {
                    this.b = bii;
                }

                @DexIgnore
                public final void run() {
                    this.b.this$0.this$0.q.getNotificationSettingsDao().insertListNotificationSettings(this.b.$notificationSettings);
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ci ci, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$notificationSettings = list;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, this.$notificationSettings, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.q.runInTransaction(new Aiii(this));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$7", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List<NotificationSettingsModel> $listNotificationSettings;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Ci ci, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$listNotificationSettings = list;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, this.$listNotificationSettings, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    for (NotificationSettingsModel notificationSettingsModel : this.$listNotificationSettings) {
                        int component2 = notificationSettingsModel.component2();
                        if (notificationSettingsModel.component3()) {
                            String J = this.this$0.this$0.J(component2);
                            FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "CALL settingsTypeName=" + J);
                            if (component2 == 0) {
                                FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL());
                                DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                this.this$0.this$0.h.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
                            } else if (component2 == 1) {
                                int size = this.this$0.this$0.i.size();
                                for (int i = 0; i < size; i++) {
                                    ContactGroup contactGroup = (ContactGroup) this.this$0.this$0.i.get(i);
                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                    String a2 = NotificationAppsPresenter.s.a();
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("mListAppNotificationFilter add PHONE item - ");
                                    sb.append(i);
                                    sb.append(" name = ");
                                    Contact contact = contactGroup.getContacts().get(0);
                                    Wg6.b(contact, "item.contacts[0]");
                                    sb.append(contact.getDisplayName());
                                    local.d(a2, sb.toString());
                                    DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType()));
                                    Contact contact2 = contactGroup.getContacts().get(0);
                                    Wg6.b(contact2, "item.contacts[0]");
                                    appNotificationFilter.setSender(contact2.getDisplayName());
                                    this.this$0.this$0.h.add(appNotificationFilter);
                                }
                            }
                        } else {
                            String J2 = this.this$0.this$0.J(component2);
                            FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "MESSAGE settingsTypeName=" + J2);
                            if (component2 == 0) {
                                FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getMESSAGES());
                                DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                this.this$0.this$0.h.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
                            } else if (component2 == 1) {
                                int size2 = this.this$0.this$0.i.size();
                                for (int i2 = 0; i2 < size2; i2++) {
                                    ContactGroup contactGroup2 = (ContactGroup) this.this$0.this$0.i.get(i2);
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String a3 = NotificationAppsPresenter.s.a();
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append("mListAppNotificationFilter add MESSAGE item - ");
                                    sb2.append(i2);
                                    sb2.append(" name = ");
                                    Contact contact3 = contactGroup2.getContacts().get(0);
                                    Wg6.b(contact3, "item.contacts[0]");
                                    sb2.append(contact3.getDisplayName());
                                    local2.d(a3, sb2.toString());
                                    DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType()));
                                    Contact contact4 = contactGroup2.getContacts().get(0);
                                    Wg6.b(contact4, "item.contacts[0]");
                                    appNotificationFilter2.setSender(contact4.getDisplayName());
                                    this.this$0.this$0.h.add(appNotificationFilter2);
                                }
                            }
                        }
                    }
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$getAllContactGroupsResponse$1", f = "NotificationAppsPresenter.kt", l = {186}, m = "invokeSuspend")
        public static final class Dii extends Ko7 implements Coroutine<Il6, Xe6<? super CoroutineUseCase.Ci>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Dii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Dii dii = new Dii(this.this$0, xe6);
                dii.p$ = (Il6) obj;
                throw null;
                //return dii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super CoroutineUseCase.Ci> xe6) {
                throw null;
                //return ((Dii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    D26 d26 = this.this$0.this$0.n;
                    this.L$0 = il6;
                    this.label = 1;
                    Object a2 = Jq4.a(d26, null, this);
                    return a2 == d ? d : a2;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$getAppResponse$1", f = "NotificationAppsPresenter.kt", l = {136}, m = "invokeSuspend")
        public static final class Eii extends Ko7 implements Coroutine<Il6, Xe6<? super CoroutineUseCase.Ci>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Eii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Eii eii = new Eii(this.this$0, xe6);
                eii.p$ = (Il6) obj;
                throw null;
                //return eii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super CoroutineUseCase.Ci> xe6) {
                throw null;
                //return ((Eii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    V36 v36 = this.this$0.this$0.m;
                    this.L$0 = il6;
                    this.label = 1;
                    Object a2 = Jq4.a(v36, null, this);
                    return a2 == d ? d : a2;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$listNotificationSettings$1", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Fii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends NotificationSettingsModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Fii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Fii fii = new Fii(this.this$0, xe6);
                fii.p$ = (Il6) obj;
                throw null;
                //return fii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends NotificationSettingsModel>> xe6) {
                throw null;
                //return ((Fii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.q.getNotificationSettingsDao().getListNotificationSettingsNoLiveData();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(NotificationAppsPresenter notificationAppsPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationAppsPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:104:0x030d, code lost:
            if (r0 == false) goto L_0x0331;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0054  */
        /* JADX WARNING: Removed duplicated region for block: B:113:0x0357  */
        /* JADX WARNING: Removed duplicated region for block: B:126:0x038e  */
        /* JADX WARNING: Removed duplicated region for block: B:129:0x03ad  */
        /* JADX WARNING: Removed duplicated region for block: B:132:0x03f8  */
        /* JADX WARNING: Removed duplicated region for block: B:136:0x0413  */
        /* JADX WARNING: Removed duplicated region for block: B:137:0x0416  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00a4  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x0111  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x01b5  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 1068
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = NotificationAppsPresenter.class.getSimpleName();
        Wg6.b(simpleName, "NotificationAppsPresenter::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public NotificationAppsPresenter(N06 n06, Uq4 uq4, V36 v36, D26 d26, U06 u06, An4 an4, NotificationSettingsDatabase notificationSettingsDatabase) {
        Wg6.c(n06, "mView");
        Wg6.c(uq4, "mUseCaseHandler");
        Wg6.c(v36, "mGetApps");
        Wg6.c(d26, "mGetAllContactGroup");
        Wg6.c(u06, "mSaveAppsNotification");
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.k = n06;
        this.l = uq4;
        this.m = v36;
        this.n = d26;
        this.o = u06;
        this.p = an4;
        this.q = notificationSettingsDatabase;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "init mIsAllAppToggleEnable " + this.e);
    }

    @DexIgnore
    public final boolean H() {
        return this.e;
    }

    @DexIgnore
    public final List<AppWrapper> I() {
        return this.g;
    }

    @DexIgnore
    public final String J(int i2) {
        if (i2 == 0) {
            String c = Um5.c(PortfolioApp.get.instance(), 2131886089);
            Wg6.b(c, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return c;
        } else if (i2 != 1) {
            String c2 = Um5.c(PortfolioApp.get.instance(), 2131886091);
            Wg6.b(c2, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return c2;
        } else {
            String c3 = Um5.c(PortfolioApp.get.instance(), 2131886090);
            Wg6.b(c3, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return c3;
        }
    }

    @DexIgnore
    public final boolean K(boolean z) {
        int i2;
        int i3;
        throw null;
//        List<AppWrapper> list = this.g;
//        if (!(list instanceof Collection) || !list.isEmpty()) {
//            i2 = 0;
//            for (T t : list) {
//                InstalledApp installedApp = t.getInstalledApp();
//                Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
//                if (isSelected == null) {
//                    Wg6.i();
//                    throw null;
//                } else if (isSelected.booleanValue() == z && t.getUri() != null) {
//                    int i4 = i2 + 1;
//                    if (i4 >= 0) {
//                        i2 = i4;
//                    } else {
//                        Hm7.k();
//                        throw null;
//                    }
//                }
//            }
//        } else {
//            i2 = 0;
//        }
//        List<AppWrapper> list2 = this.g;
//        if (!(list2 instanceof Collection) || !list2.isEmpty()) {
//            Iterator<T> it = list2.iterator();
//            i3 = 0;
//            while (it.hasNext()) {
//                if (it.next().getUri() != null) {
//                    int i5 = i3 + 1;
//                    if (i5 >= 0) {
//                        i3 = i5;
//                    } else {
//                        Hm7.k();
//                        throw null;
//                    }
//                }
//            }
//        } else {
//            i3 = 0;
//        }
//        return i2 == i3;
    }

    @DexWrap
    public final boolean L() {
        if (ListRestored) {
            return true;
        }
        return L();
    }

//    @DexIgnore
//    /* JADX WARNING: Removed duplicated region for block: B:14:0x0049  */
//    public final boolean L() {
//        InstalledApp t;
//        ArrayList<InstalledApp> arrayList = new ArrayList();
//        for (AppWrapper appWrapper : this.g) {
//            InstalledApp installedApp = appWrapper.getInstalledApp();
//            if (installedApp != null) {
//                Boolean isSelected = installedApp.isSelected();
//                Wg6.a((Object) isSelected, "it.isSelected");
//                if (isSelected.booleanValue()) {
//                    arrayList.add(installedApp);
//                }
//            }
//        }
//        if (arrayList.size() != this.f.size()) {
//            return true;
//        }
//        for (InstalledApp installedApp2 : arrayList) {
//            Iterator<InstalledApp> it = this.f.iterator();
//            while (true) {
//                if (!it.hasNext()) {
//                    t = null;
//                    break;
//                }
//                InstalledApp next = it.next();
//                if (Wg6.a((Object) next.getIdentifier(), (Object) installedApp2.getIdentifier())) {
//                    t = next;
//                    break;
//                }
//            }
//            InstalledApp t2 = t;
//            if (t2 == null || (!Wg6.a(t2.isSelected(), installedApp2.isSelected()))) {
//                return true;
//            }
//            throw null;
////            while (r4.hasNext()) {
////            }
//        }
//        return false;
//    }


    @DexIgnore
    public final void M() {
        FLogger.INSTANCE.getLocal().d(r, "registerBroadcastReceiver");
        BleCommandResultManager.d.e(this.j, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public final void N(boolean z) {
        InstalledApp installedApp;
        for (AppWrapper appWrapper : this.g) {
            if (!(appWrapper.getUri() == null || (installedApp = appWrapper.getInstalledApp()) == null)) {
                installedApp.setSelected(z);
            }
        }
    }

    @DexIgnore
    public final void O(List<AppWrapper> list) {
        Wg6.c(list, "<set-?>");
        this.g = list;
    }

    @DexIgnore
    public void P() {
        this.k.M5(this);
    }

    @DexIgnore
    public final void Q() {
        FLogger.INSTANCE.getLocal().d(r, "unregisterBroadcastReceiver");
        BleCommandResultManager.d.j(this.j, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(r, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        M();
        this.k.b();
        Rm6 unused = Gu7.d(k(), null, null, new Ci(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(r, "stop");
        Q();
        this.k.a();
    }

    @DexPrepend
    public void n() {  // function with "closeAndSave, nothing changed"
        List<AppWrapper> apps = this.g;
        StringBuilder sb = new StringBuilder();
        for (AppWrapper a : apps) {
            if (a.getInstalledApp().isSelected()) {
                sb.append(a.getInstalledApp().identifier);
                sb.append(",");
            }
        }
        try {
            File dir = Environment.getExternalStoragePublicDirectory("Fossil");
            NotificationAppsFragment frag = (NotificationAppsFragment) this.k;  // NotificationAppsFragment
            // Ask for storage permissions if not already granted
            PermissionUtils.a.s(frag.getActivity(), 255); // function with: "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"
            if (!dir.exists()) {
                dir.mkdir();
            }
            if (dir.exists()) {
                File enabledNotifications = new File(Environment.getExternalStoragePublicDirectory("Fossil"), "enabled_notifications.csv");
                FileOutputStream os = null;
                os = new FileOutputStream(enabledNotifications);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(os);
                outputStreamWriter.write(sb.toString());
                outputStreamWriter.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

//    @DexIgnore
//    @Override // com.fossil.M06
//    public void n() {
//        if (!L()) {
//            this.p.J0(this.e);
//            FLogger.INSTANCE.getLocal().d(r, "closeAndSave, nothing changed");
//            this.k.close();
//            return;
//        }
//        Jn5 jn5 = Jn5.b;
//        N06 n06 = this.k;
//        throw null;
////        if (n06 == null) {
////            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
////        } else if (Jn5.c(jn5, ((NotificationAppsFragment) n06).getContext(), Jn5.Ai.SET_BLE_COMMAND, false, false, false, null, 60, null)) {
////            FLogger.INSTANCE.getLocal().d(r, "setRuleToDevice, showProgressDialog");
////            this.k.b();
////            long currentTimeMillis = System.currentTimeMillis();
////            ILocalFLogger local = FLogger.INSTANCE.getLocal();
////            String str = r;
////            local.d(str, "filter notification, time start=" + currentTimeMillis);
////            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
////            String str2 = r;
////            local2.d(str2, "mListAppWrapper.size = " + this.g.size());
////            this.h.addAll(D47.c(this.g, false, 1, null));
////            long E1 = PortfolioApp.get.instance().E1(new AppNotificationFilterSettings(this.h, System.currentTimeMillis()), PortfolioApp.get.instance().J());
////            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
////            String str3 = r;
////            local3.d(str3, "filter notification, time end= " + E1);
////            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
////            String str4 = r;
////            local4.d(str4, "delayTime =" + (E1 - currentTimeMillis) + " mili seconds");
////        }
//    }

    @DexIgnore
    @Override // com.fossil.M06
    public void o() {
        this.k.a();
        this.k.close();
    }

    @DexIgnore
    @Override // com.fossil.M06
    public void p(AppWrapper appWrapper, boolean z) {
//        T t;
//        boolean z2;
//        InstalledApp installedApp;
//        Wg6.c(appWrapper, "appWrapper");
//        ILocalFLogger local = FLogger.INSTANCE.getLocal();
//        String str = r;
//        StringBuilder sb = new StringBuilder();
//        sb.append("setAppState: appName = ");
//        InstalledApp installedApp2 = appWrapper.getInstalledApp();
//        sb.append(installedApp2 != null ? installedApp2.getTitle() : null);
//        sb.append(", selected = ");
//        sb.append(z);
//        local.d(str, sb.toString());
//        Iterator<T> it = this.g.iterator();
//        while (true) {
//            if (!it.hasNext()) {
//                t = null;
//                break;
//            }
//            T next = it.next();
//            if (Wg6.a(next.getUri(), appWrapper.getUri())) {
//                t = next;
//                break;
//            }
//        }
//        T t2 = t;
//        if (!(t2 == null || (installedApp = t2.getInstalledApp()) == null)) {
//            installedApp.setSelected(z);
//        }
//        if (K(z) && this.e != z) {
//            this.e = z;
//            this.k.Q0(z);
//        } else if (!z && this.e) {
//            this.e = false;
//            this.k.Q0(false);
//        }
//        if (!this.e) {
//            List<AppWrapper> list = this.g;
//            if (!(list instanceof Collection) || !list.isEmpty()) {
//                Iterator<T> it2 = list.iterator();
//                while (true) {
//                    if (!it2.hasNext()) {
//                        z2 = false;
//                        break;
//                    }
//                    InstalledApp installedApp3 = it2.next().getInstalledApp();
//                    if (installedApp3 != null) {
//                        Boolean isSelected = installedApp3.isSelected();
//                        Wg6.b(isSelected, "it.installedApp!!.isSelected");
//                        if (isSelected.booleanValue()) {
//                            z2 = true;
//                            break;
//                        }
//                    } else {
//                        Wg6.i();
//                        throw null;
//                    }
//                }
//            } else {
//                z2 = false;
//            }
//            if (!z2) {
//                return;
//            }
//        }
//        N06 n06 = this.k;
//        if (n06 != null) {
//            Context context = ((NotificationAppsFragment) n06).getContext();
//            if (context != null) {
//                Jn5.c(Jn5.b, context, Jn5.Ai.NOTIFICATION_APPS, false, false, false, null, 60, null);
//                return;
//            }
//            return;
//        }
        throw null;
//        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
    }

    @DexIgnore
    @Override // com.fossil.M06
    public void q(boolean z) {
        boolean z2 = true;
        FLogger.INSTANCE.getLocal().d(r, "setEnableAllAppsToggle: enable = " + z);
        this.e = z;
        if (!z && K(true)) {
            N(false);
        } else if (z) {
            N(true);
        } else {
            z2 = false;
        }
        if (z2) {
            this.k.s2(this.g);
        }
        if (this.e) {
            Jn5 jn5 = Jn5.b;
            N06 n06 = this.k;
            if (n06 != null) {
                throw null;
//                Jn5.c(jn5, ((NotificationAppsFragment) n06).getContext(), Jn5.Ai.NOTIFICATION_APPS, false, false, false, null, 60, null);
//                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
        }
    }
}
