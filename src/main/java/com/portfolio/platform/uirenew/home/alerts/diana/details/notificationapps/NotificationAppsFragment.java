package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Aq0;
import com.fossil.Ex5;
import com.fossil.G37;
import com.fossil.M06;
import com.fossil.N06;
import com.fossil.Qv5;
import com.fossil.S37;
import com.fossil.Vl5;
import com.fossil.W16;
import com.fossil.Y16;
import com.fossil.Yu0;
import com.mapped.AlertDialogFragment;
import com.mapped.AppWrapper;
import com.mapped.Iface;
import com.mapped.NotificationAppsFragmentBinding;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.List;
import android.os.Environment;
import com.mapped.PermissionUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
public final class NotificationAppsFragment extends Qv5 implements N06, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public M06 h;  // NotificationAppsPresenter
    @DexIgnore
    public G37<NotificationAppsFragmentBinding> i;
    @DexIgnore
    public W16 j;
    @DexIgnore
    public Ex5 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore
    NotificationAppsFragment() {}

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return NotificationAppsFragment.m;
        }

        @DexIgnore
        public final NotificationAppsFragment b() {
            return new NotificationAppsFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Ex5.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragment a;

        @DexIgnore
        public Bi(NotificationAppsFragment notificationAppsFragment) {
            this.a = notificationAppsFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ex5.Bi
        public void a(AppWrapper appWrapper, boolean z) {
            Wg6.c(appWrapper, "appWrapper");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationAppsFragment.s.a();
            StringBuilder sb = new StringBuilder();
            sb.append("appName = ");
            InstalledApp installedApp = appWrapper.getInstalledApp();
            sb.append(installedApp != null ? installedApp.getTitle() : null);
            sb.append(", selected = ");
            sb.append(z);
            local.d(a2, sb.toString());
            NotificationAppsFragment.L6(this.a).p(appWrapper, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragment b;

        @DexIgnore
        public Ci(NotificationAppsFragment notificationAppsFragment) {
            this.b = notificationAppsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(NotificationAppsFragment.s.a(), "press on close button");
            NotificationAppsFragment.L6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragment a;

        @DexIgnore
        public Di(NotificationAppsFragment notificationAppsFragment) {
            this.a = notificationAppsFragment;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            NotificationAppsFragment.L6(this.a).q(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragment b;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragmentBinding c;

        @DexIgnore
        public Ei(NotificationAppsFragment notificationAppsFragment, NotificationAppsFragmentBinding notificationAppsFragmentBinding) {
            this.b = notificationAppsFragment;
            this.c = notificationAppsFragmentBinding;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @SuppressLint("WrongConstant")
        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            RTLImageView rTLImageView = this.c.u;
            Wg6.b(rTLImageView, "binding.ivClear");
            rTLImageView.setVisibility(i3 == 0 ? 4 : 0);
            NotificationAppsFragment.K6(this.b).getFilter().filter(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragmentBinding b;

        @DexIgnore
        public Fi(NotificationAppsFragmentBinding notificationAppsFragmentBinding) {
            this.b = notificationAppsFragmentBinding;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                NotificationAppsFragmentBinding notificationAppsFragmentBinding = this.b;
                Wg6.b(notificationAppsFragmentBinding, "binding");
                notificationAppsFragmentBinding.n().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragmentBinding b;

        @DexIgnore
        public Gi(NotificationAppsFragmentBinding notificationAppsFragmentBinding) {
            this.b = notificationAppsFragmentBinding;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.r.setText("");
        }
    }

    /*
    static {
        String simpleName = NotificationAppsFragment.class.getSimpleName();
        Wg6.b(simpleName, "NotificationAppsFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ Ex5 K6(NotificationAppsFragment notificationAppsFragment) {
        Ex5 ex5 = notificationAppsFragment.k;
        if (ex5 != null) {
            return ex5;
        }
        Wg6.n("mNotificationAppsAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ M06 L6(NotificationAppsFragment notificationAppsFragment) {
        M06 m06 = notificationAppsFragment.h;
        if (m06 != null) {
            return m06;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d(m, "onActivityBackPressed -");
        M06 m06 = this.h;
        if (m06 != null) {
            m06.n();
            return true;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(M06 obj) {
        N6((M06) obj);
    }

    @DexIgnore
    public void N6(M06 m06) {
        Wg6.c(m06, "presenter");
        this.h = m06;
    }

    @DexIgnore
    @Override // com.fossil.N06
    public void Q0(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        G37<NotificationAppsFragmentBinding> g37 = this.i;
        if (g37 != null) {
            NotificationAppsFragmentBinding a2 = g37.a();
            if (a2 != null && (flexibleSwitchCompat = a2.y) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != 927511079) {
            if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i2 == 2131363373) {
                M06 m06 = this.h;
                if (m06 != null) {
                    m06.o();
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        } else if (!str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
        } else {
            if (i2 == 2131362290) {
                M06 m062 = this.h;
                if (m062 != null) {
                    m062.n();
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            } else if (i2 != 2131362385) {
                if (i2 == 2131362686) {
                    close();
                }
            } else if (getActivity() != null) {
                HelpActivity.a aVar = HelpActivity.B;
                FragmentActivity requireActivity = requireActivity();
                Wg6.b(requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.N06
    public void c() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.N06
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexAdd
    public void loadCsv(View view) {
        try {
            File dir = Environment.getExternalStoragePublicDirectory("Fossil");
            // Ask for storage permissions if not already granted
            PermissionUtils.a.s(this.getActivity(), 255); // function with: "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"
            if (!dir.exists()) {
                dir.mkdir();
            }
            if (dir.exists()) {
                File enabledNotifications = new File(Environment.getExternalStoragePublicDirectory("Fossil"), "enabled_notifications.csv");
                if(enabledNotifications.exists()) {
                    FileInputStream inputStream = new FileInputStream(enabledNotifications);
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    String receiveString = "";
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((receiveString = bufferedReader.readLine()) != null) {
                        stringBuilder.append(receiveString).append("\n");
                    }
                    inputStream.close();
                    String contents = stringBuilder.toString();

                    List<String> enabled = Arrays.asList(contents.trim().split(","));

                    NotificationAppsPresenter pres = (NotificationAppsPresenter) this.h;
                    List<AppWrapper> apps = pres.g;
                    pres.k.b();  // function with "showProgressDialog"
                    for (AppWrapper a : apps) {
                        InstalledApp ia = a.getInstalledApp();
                        boolean z = enabled.contains(ia.identifier);
                        if (ia.isSelected() != z) {
                            pres.p(a, z);  // function with "setAppState: appName = "
                            pres.ListRestored = true;

                        }
                    }
                    onResume(); // This reloads the switches and gets rid of progress dialog
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @DexReplace
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        NotificationAppsFragmentBinding notificationAppsFragmentBinding = (NotificationAppsFragmentBinding) Aq0.f(layoutInflater, 2131558588, viewGroup, false, A6());
        notificationAppsFragmentBinding.v.setOnClickListener(new Ci(this));
        notificationAppsFragmentBinding.y.setOnCheckedChangeListener(new Di(this));
        notificationAppsFragmentBinding.r.addTextChangedListener(new Ei(this, notificationAppsFragmentBinding));
        notificationAppsFragmentBinding.r.setOnFocusChangeListener(new Fi(notificationAppsFragmentBinding));
        notificationAppsFragmentBinding.u.setOnClickListener(new Gi(notificationAppsFragmentBinding));
        notificationAppsFragmentBinding.buttonRestore.setOnClickListener(this::loadCsv);
        Ex5 ex5 = new Ex5();
        ex5.o(new Bi(this));
        this.k = ex5;
        W16 w16 = (W16) getChildFragmentManager().Z(W16.w.a());
        this.j = w16;
        if (w16 == null) {
            this.j = W16.w.b();
        }
        RecyclerView recyclerView = notificationAppsFragmentBinding.x;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        Ex5 ex52 = this.k;
        if (ex52 != null) {
            recyclerView.setAdapter(ex52);
            RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
            if (itemAnimator != null) {
                ((Yu0) itemAnimator).setSupportsChangeAnimations(false);
                recyclerView.setHasFixedSize(true);
                Iface iface = PortfolioApp.get.instance().getIface();
                W16 w162 = this.j;
                if (w162 != null) {
                    iface.U1(new Y16(w162)).a(this);
                    this.i = new G37<>(this, notificationAppsFragmentBinding);
                    E6("app_notification_view");
                    Wg6.b(notificationAppsFragmentBinding, "binding");
                    return notificationAppsFragmentBinding.n();
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
            }
            throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.DefaultItemAnimator");
        }
        Wg6.n("mNotificationAppsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        M06 m06 = this.h;
        if (m06 != null) {
            m06.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
            }
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        M06 m06 = this.h;
        if (m06 != null) {
            m06.l();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.N06
    public void s2(List<AppWrapper> list) {
        Wg6.c(list, "listAppWrapper");
        Ex5 ex5 = this.k;
        if (ex5 != null) {
            ex5.n(list);
        } else {
            Wg6.n("mNotificationAppsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.N06
    public void u() {
        if (isActive()) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            Context requireContext = requireContext();
            Wg6.b(requireContext, "requireContext()");
            throw null;
//            TroubleshootingActivity.a.c(aVar, requireContext, PortfolioApp.get.instance().J(), false, false, 12, null);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
