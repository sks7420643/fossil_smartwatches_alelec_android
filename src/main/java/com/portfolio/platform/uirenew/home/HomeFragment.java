package com.portfolio.platform.uirenew.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.Aq0;
import com.fossil.Aw5;
import com.fossil.Cl5;
import com.fossil.G37;
import com.fossil.G67;
import com.fossil.Ls0;
import com.fossil.M47;
import com.fossil.Nm0;
import com.fossil.Nv4;
import com.fossil.Ou6;
import com.fossil.Oz5;
import com.fossil.Qv5;
import com.fossil.R75;
import com.fossil.Ru6;
import com.fossil.S37;
import com.fossil.Tn6;
import com.fossil.Ts0;
import com.fossil.Un6;
import com.fossil.Vs0;
import com.fossil.Wr4;
import com.fossil.Y67;
import com.fossil.Yy5;
import com.fossil.Z47;
import com.fossil.Zy5;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mapped.AlertDialogFragment;
import com.mapped.Cd6;
import com.mapped.Hh6;
import com.mapped.HomeAlertsFragment;
import com.mapped.HomeAlertsHybridFragment;
import com.mapped.HomeDashboardFragment;
import com.mapped.HomeDianaCustomizeFragment;
import com.mapped.HomeHybridCustomizeFragment;
import com.mapped.HomeProfileFragment;
import com.mapped.Iface;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.ArrayList;
import java.util.HashMap;
import com.portfolio.platform.data.source.local.alarm.Alarm;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD, staticConstructorAction = DexAction.APPEND)
public final class HomeFragment extends Qv5 implements Zy5, AlertDialogFragment.Gi, Ou6.Ai {
    @DexIgnore
    public static /* final */ Ai A; // = new Ai(null);
    @DexIgnore
    public static /* final */ String z;
    @DexIgnore
    public HomeDashboardPresenter h;
    @DexIgnore
    public HomeDianaCustomizePresenter i;
    @DexIgnore
    public HomeHybridCustomizePresenter j;
    @DexIgnore
    public HomeProfilePresenter k;
    @DexIgnore
    public HomeAlertsPresenter l;
    @DexIgnore
    public HomeAlertsHybridPresenter m;
    @DexIgnore
    public Un6 s;
    @DexIgnore
    public Yy5 t;
    @DexIgnore
    public G37<R75> u;
    @DexIgnore
    public /* final */ ArrayList<Fragment> v; // = new ArrayList<>();
    @DexIgnore
    public String w; // = "";
    @DexIgnore
    public Y67 x;
    @DexIgnore
    public HashMap y;

    @DexAdd
    public static Alarm alarmToAdd = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ HomeFragment b(Ai ai, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = "";
            }
            return ai.a(str);
        }

        @DexIgnore
        public final HomeFragment a(String str) {
            Wg6.c(str, "presetId");
            HomeFragment homeFragment = new HomeFragment();
            homeFragment.setArguments(Nm0.a(new Lc6("PRESET_ID_EXTRA", str)));
            return homeFragment;
        }

        @DexIgnore
        public final HomeFragment c() {
            HomeFragment homeFragment = new HomeFragment();
            homeFragment.setArguments(Nm0.a(new Lc6("KEY_DIANA_REQUIRE", Boolean.TRUE)));
            return homeFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements BottomNavigationView.d {
        @DexIgnore
        public /* final */ /* synthetic */ Hh6 a;
        @DexIgnore
        public /* final */ /* synthetic */ Hh6 b;
        @DexIgnore
        public /* final */ /* synthetic */ HomeFragment c;
        @DexIgnore
        public /* final */ /* synthetic */ Typeface d;

        @DexIgnore
        public Bi(Hh6 hh6, Hh6 hh62, HomeFragment homeFragment, String str, boolean z, Typeface typeface) {
            this.a = hh6;
            this.b = hh62;
            this.c = homeFragment;
            this.d = typeface;
        }

        @SuppressLint("ResourceType")
        @DexIgnore
        @Override // com.google.android.material.bottomnavigation.BottomNavigationView.d
        public final boolean a(MenuItem menuItem) {
            int i;
            Wg6.c(menuItem, "item");
            R75 r75 = (R75) HomeFragment.K6(this.c).a();
            BottomNavigationView bottomNavigationView = r75 != null ? r75.q : null;
            if (bottomNavigationView != null) {
                Wg6.b(bottomNavigationView, "mBinding.get()?.bottomNavigation!!");
                Menu menu = bottomNavigationView.getMenu();
                Wg6.b(menu, "mBinding.get()?.bottomNavigation!!.menu");
                this.c.X6(menu, this.d, this.a.element);
                menu.findItem(2131362185).setIcon(2131231088);
                Drawable f = W6.f(this.c.requireContext(), 2131231013);
                if (f != null) {
                    f.setColorFilter(W6.d(PortfolioApp.get.instance(), 2131099820), PorterDuff.Mode.SRC_ATOP);
                }
                menuItem.setIcon(f);
                MenuItem findItem = menu.findItem(2131361983);
                Wg6.b(findItem, "menu.findItem(R.id.buddyChallengeFragment)");
                findItem.setIcon(f);
                menu.findItem(2131362173).setIcon(2131231063);
                menu.findItem(2131362967).setIcon(2131231145);
                menu.findItem(2131361886).setIcon(2131230990);
                MenuItem findItem2 = menu.findItem(2131361886);
                Wg6.b(findItem2, "menu.findItem(R.id.alertsFragment)");
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(findItem2.getTitle());
                spannableStringBuilder.setSpan(new ForegroundColorSpan(this.a.element), 0, spannableStringBuilder.length(), 0);
                menu.findItem(2131361886).setTitle(spannableStringBuilder);
                switch (menuItem.getItemId()) {
                    case 2131361886:
                        Drawable f2 = W6.f(this.c.requireContext(), 2131230991);
                        int i2 = this.b.element;
                        if (f2 != null) {
                            f2.setColorFilter(i2, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(f2);
                        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder2.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder2.length(), 0);
                        menu.findItem(2131361886).setTitle(spannableStringBuilder2);
                        i = 3;
                        break;
                    case 2131361983:
                        Drawable f3 = W6.f(this.c.requireContext(), 2131231015);
                        int i3 = this.b.element;
                        if (f3 != null) {
                            f3.setColorFilter(i3, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(f3);
                        SpannableStringBuilder spannableStringBuilder3 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder3.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder3.length(), 0);
                        menu.findItem(2131361983).setTitle(spannableStringBuilder3);
                        i = 1;
                        break;
                    case 2131362173:
                        Drawable f4 = W6.f(this.c.requireContext(), 2131231064);
                        int i4 = this.b.element;
                        if (f4 != null) {
                            f4.setColorFilter(i4, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(f4);
                        SpannableStringBuilder spannableStringBuilder4 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder4.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder4.length(), 0);
                        menu.findItem(2131362173).setTitle(spannableStringBuilder4);
                        i = 2;
                        break;
                    case 2131362185:
                        Drawable f5 = W6.f(this.c.requireContext(), 2131231089);
                        int i5 = this.b.element;
                        if (f5 != null) {
                            f5.setColorFilter(i5, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(f5);
                        SpannableStringBuilder spannableStringBuilder5 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder5.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder5.length(), 0);
                        menu.findItem(2131362185).setTitle(spannableStringBuilder5);
                        i = 0;
                        break;
                    case 2131362967:
                        Drawable f6 = W6.f(this.c.requireContext(), 2131231146);
                        int i6 = this.b.element;
                        if (f6 != null) {
                            f6.setColorFilter(i6, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(f6);
                        SpannableStringBuilder spannableStringBuilder6 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder6.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder6.length(), 0);
                        menu.findItem(2131362967).setTitle(spannableStringBuilder6);
                        i = 4;
                        break;
                    default:
                        i = 0;
                        break;
                }
                if (this.c.j != null && DeviceHelper.o.y(PortfolioApp.get.instance().J())) {
                    this.c.S6().R(i);
                }
                if (this.c.i != null && DeviceHelper.o.x(PortfolioApp.get.instance().J())) {
                    this.c.R6().m0(i);
                }
                if (i != 2) {
                    HomeFragment homeFragment = this.c;
                    if (homeFragment.i != null && homeFragment.R6().k0()) {
                        this.c.R6().g0();
                    }
                }
                FLogger.INSTANCE.getLocal().d(HomeFragment.z, "show tab with tab=" + i);
                HomeFragment.L6(this.c).t(i);
                this.c.c7(i);
                return true;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnLongClickListener {
        @DexIgnore
        public static /* final */ Ci b; // = new Ci();

        @DexIgnore
        public final boolean onLongClick(View view) {
            Wg6.b(view, "item");
            view.setHapticFeedbackEnabled(false);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeFragment a;

        @DexIgnore
        public Di(HomeFragment homeFragment) {
            this.a = homeFragment;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0071, code lost:
            if (((r0 == null || (r0 = r0.q) == null || (r0 = r0.getMenu()) == null || (r0 = r0.findItem(2131361983)) == null) ? false : r0.isVisible()) != false) goto L_0x0025;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a(java.lang.Integer r6) {
            /*
                r5 = this;
                r1 = 0
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r2 = com.portfolio.platform.uirenew.home.HomeFragment.M6()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "dashboardTab: "
                r3.append(r4)
                r3.append(r6)
                java.lang.String r3 = r3.toString()
                r0.d(r2, r3)
                if (r6 != 0) goto L_0x0045
            L_0x0021:
                java.lang.Integer r6 = java.lang.Integer.valueOf(r1)
            L_0x0025:
                com.portfolio.platform.uirenew.home.HomeFragment r0 = r5.a
                int r1 = r6.intValue()
                com.portfolio.platform.uirenew.home.HomeFragment.O6(r0, r1)
                com.portfolio.platform.uirenew.home.HomeFragment r0 = r5.a
                com.fossil.Yy5 r0 = com.portfolio.platform.uirenew.home.HomeFragment.L6(r0)
                int r1 = r6.intValue()
                r0.t(r1)
                com.portfolio.platform.uirenew.home.HomeFragment r0 = r5.a
                int r1 = r6.intValue()
                com.portfolio.platform.uirenew.home.HomeFragment.P6(r0, r1)
                return
            L_0x0045:
                int r0 = r6.intValue()
                r2 = 1
                if (r0 != r2) goto L_0x0025
                com.portfolio.platform.uirenew.home.HomeFragment r0 = r5.a
                com.fossil.G37 r0 = com.portfolio.platform.uirenew.home.HomeFragment.K6(r0)
                java.lang.Object r0 = r0.a()
                com.fossil.R75 r0 = (com.fossil.R75) r0
                if (r0 == 0) goto L_0x0074
                com.google.android.material.bottomnavigation.BottomNavigationView r0 = r0.q
                if (r0 == 0) goto L_0x0074
                android.view.Menu r0 = r0.getMenu()
                if (r0 == 0) goto L_0x0074
                r2 = 2131361983(0x7f0a00bf, float:1.8343734E38)
                android.view.MenuItem r0 = r0.findItem(r2)
                if (r0 == 0) goto L_0x0074
                boolean r0 = r0.isVisible()
            L_0x0071:
                if (r0 == 0) goto L_0x0021
                goto L_0x0025
            L_0x0074:
                r0 = r1
                goto L_0x0071
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.HomeFragment.Di.a(java.lang.Integer):void");
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Integer num) {
            a(num);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeFragment a;

        @DexIgnore
        public Ei(HomeFragment homeFragment) {
            this.a = homeFragment;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = HomeFragment.z;
            local.d(str2, "activeDeviceSerialLiveData onChange " + str);
            Yy5 L6 = HomeFragment.L6(this.a);
            if (str != null) {
                L6.r(str);
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    /*
    static {
        String simpleName = HomeFragment.class.getSimpleName();
        Wg6.b(simpleName, "HomeFragment::class.java.simpleName");
        z = simpleName;
    }
    */

    @DexIgnore
    public HomeFragment() {
        Ru6.b.a();
    }

    @DexIgnore
    public static final /* synthetic */ G37 K6(HomeFragment homeFragment) {
        G37<R75> g37 = homeFragment.u;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Yy5 L6(HomeFragment homeFragment) {
        Yy5 yy5 = homeFragment.t;
        if (yy5 != null) {
            return yy5;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zy5
    public void A(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = z;
        StringBuilder sb = new StringBuilder();
        sb.append("onUpdateFwComplete currentTab ");
        Yy5 yy5 = this.t;
        if (yy5 != null) {
            sb.append(yy5.p());
            local.d(str, sb.toString());
            HomeDashboardPresenter homeDashboardPresenter = this.h;
            if (homeDashboardPresenter != null) {
                if (homeDashboardPresenter != null) {
                    homeDashboardPresenter.U(z2);
                } else {
                    Wg6.n("mHomeDashboardPresenter");
                    throw null;
                }
            }
            HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.i;
            if (homeDianaCustomizePresenter != null) {
                if (homeDianaCustomizePresenter != null) {
                    homeDianaCustomizePresenter.n0();
                } else {
                    Wg6.n("mHomeDianaCustomizePresenter");
                    throw null;
                }
            }
            Yy5 yy52 = this.t;
            if (yy52 != null) {
                c7(yy52.p());
                if (!z2 && isActive()) {
                    S37 s37 = S37.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    Wg6.b(childFragmentManager, "childFragmentManager");
                    s37.z0(childFragmentManager);
                    return;
                }
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zy5
    public void C1(Intent intent) {
        Wg6.c(intent, "intent");
        HomeDashboardPresenter homeDashboardPresenter = this.h;
        if (homeDashboardPresenter != null) {
            if (homeDashboardPresenter != null) {
                homeDashboardPresenter.T(intent);
            } else {
                Wg6.n("mHomeDashboardPresenter");
                throw null;
            }
        }
        HomeProfilePresenter homeProfilePresenter = this.k;
        if (homeProfilePresenter == null) {
            return;
        }
        if (homeProfilePresenter != null) {
            homeProfilePresenter.g0(intent);
        } else {
            Wg6.n("mHomeProfilePresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.i;
        if (homeDianaCustomizePresenter != null) {
            if (homeDianaCustomizePresenter == null) {
                Wg6.n("mHomeDianaCustomizePresenter");
                throw null;
            } else if (homeDianaCustomizePresenter.k0()) {
                HomeDianaCustomizePresenter homeDianaCustomizePresenter2 = this.i;
                if (homeDianaCustomizePresenter2 != null) {
                    homeDianaCustomizePresenter2.g0();
                    return true;
                }
                Wg6.n("mHomeDianaCustomizePresenter");
                throw null;
            }
        }
        return false;
    }

    @DexReplace
    @Override // com.fossil.Zy5
    public void L0(FossilDeviceSerialPatternUtil.DEVICE device, int i2, boolean z2) { // called by HomePresenter.Gi.invokeSuspend
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = z;
        local.d(str, "updateFragmentList with deviceType " + device + " appMode " + i2);
        if (!z2) {
            Cl5.c.c();
        }
        T6(z2);
        if (i2 != 1) {
            a7(device);
        } else {
            b7();
        }
        U6();
        if (alarmToAdd != null) {
            HomeAlertsPresenter ap = HomeFragment.this.l;
            if (ap != null) {
                ap.alarmToAdd = alarmToAdd;
            }
            HomeAlertsHybridPresenter ahp = HomeFragment.this.m;
            if (ahp != null) {
                ahp.alarmToAdd = alarmToAdd;
            }
            alarmToAdd = null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Yy5 obj) {
        Y6((Yy5) obj);
    }

    @DexIgnore
    @Override // com.fossil.Zy5
    public void O3() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.I(childFragmentManager);
        }
    }

    @DexIgnore
    public final void Q6() {
        Resources resources = PortfolioApp.get.instance().getResources();
        Wg6.b(resources, "PortfolioApp.instance.resources");
        int applyDimension = (int) TypedValue.applyDimension(1, 28.0f, resources.getDisplayMetrics());
        G37<R75> g37 = this.u;
        if (g37 != null) {
            R75 a2 = g37.a();
            if (a2 != null) {
                View childAt = a2.q.getChildAt(0);
                if (childAt != null) {
                    BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) childAt;
                    int childCount = bottomNavigationMenuView.getChildCount();
                    for (int i2 = 0; i2 < childCount; i2++) {
                        @SuppressLint("ResourceType") View findViewById = bottomNavigationMenuView.getChildAt(i2).findViewById(2131362608);
                        Wg6.b(findViewById, "icon");
                        findViewById.getLayoutParams().width = applyDimension;
                        findViewById.getLayoutParams().height = applyDimension;
                    }
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type com.google.android.material.bottomnavigation.BottomNavigationMenuView");
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        switch (str.hashCode()) {
            case -1944405936:
                if (str.equals("FIRMWARE_UPDATE_FAIL") && i2 == 2131363373) {
                    UpdateFirmwareActivity.a aVar = UpdateFirmwareActivity.C;
                    FragmentActivity requireActivity = requireActivity();
                    Wg6.b(requireActivity, "requireActivity()");
                    aVar.b(requireActivity, PortfolioApp.get.instance().J(), false);
                    return;
                }
                return;
            case 986357734:
                if (!str.equals("FEEDBACK_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363291) {
                    FLogger.INSTANCE.getLocal().d(z, "Cancel Zendesk feedback");
                    return;
                } else if (i2 != 2131363373) {
                    return;
                } else {
                    if (!Wr4.a.a().n()) {
                        FLogger.INSTANCE.getLocal().d(z, "Go to Zendesk feedback");
                        Yy5 yy5 = this.t;
                        if (yy5 != null) {
                            yy5.u("Feedback - From app [Fossil] - [Android]");
                            return;
                        } else {
                            Wg6.n("mPresenter");
                            throw null;
                        }
                    } else {
                        String a2 = M47.a(M47.Ci.REPAIR_CENTER, null);
                        Wg6.b(a2, "URLHelper.buildStaticPag\u2026Page.REPAIR_CENTER, null)");
                        d7(a2);
                        return;
                    }
                }
            case 1390226280:
                if (!str.equals("HAPPINESS_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363291) {
                    FLogger.INSTANCE.getLocal().d(z, "Send Zendesk feedback");
                    S37 s37 = S37.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    Wg6.b(childFragmentManager, "childFragmentManager");
                    s37.A(childFragmentManager);
                    return;
                } else if (i2 == 2131363373) {
                    FLogger.INSTANCE.getLocal().d(z, "Open app rating dialog");
                    S37 s372 = S37.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    Wg6.b(childFragmentManager2, "childFragmentManager");
                    s372.j(childFragmentManager2);
                    return;
                } else {
                    return;
                }
            case 1431920636:
                if (!str.equals("APP_RATING_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363291) {
                    FLogger.INSTANCE.getLocal().d(z, "Stay in app");
                    return;
                } else if (i2 == 2131363373) {
                    FLogger.INSTANCE.getLocal().d(z, "Go to Play Store");
                    PortfolioApp instance = PortfolioApp.get.instance();
                    AppHelper.Ai ai = AppHelper.g;
                    String packageName = instance.getPackageName();
                    Wg6.b(packageName, "packageName");
                    ai.k(instance, packageName);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    @DexIgnore
    public final HomeDianaCustomizePresenter R6() {
        HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.i;
        if (homeDianaCustomizePresenter != null) {
            return homeDianaCustomizePresenter;
        }
        Wg6.n("mHomeDianaCustomizePresenter");
        throw null;
    }

    @DexIgnore
    public final HomeHybridCustomizePresenter S6() {
        HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.j;
        if (homeHybridCustomizePresenter != null) {
            return homeHybridCustomizePresenter;
        }
        Wg6.n("mHomeHybridCustomizePresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zy5
    public void T1() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = z;
        StringBuilder sb = new StringBuilder();
        sb.append("onStartUpdateFw currentTab ");
        Yy5 yy5 = this.t;
        if (yy5 != null) {
            sb.append(yy5.p());
            local.d(str, sb.toString());
            Yy5 yy52 = this.t;
            if (yy52 != null) {
                c7(yy52.p());
                HomeDashboardPresenter homeDashboardPresenter = this.h;
                if (homeDashboardPresenter == null) {
                    return;
                }
                if (homeDashboardPresenter != null) {
                    homeDashboardPresenter.V();
                } else {
                    Wg6.n("mHomeDashboardPresenter");
                    throw null;
                }
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void T6(boolean z2) {
        String d = ThemeManager.l.a().d("nonBrandSurface");
        Typeface f = ThemeManager.l.a().f("nonBrandTextStyle11");
        G37<R75> g37 = this.u;
        if (g37 != null) {
            R75 a2 = g37.a();
            if (a2 != null) {
                if (d != null) {
                    a2.q.setBackgroundColor(Color.parseColor(d));
                }
                BottomNavigationView bottomNavigationView = a2.q;
                Wg6.b(bottomNavigationView, "bottomNavigation");
                bottomNavigationView.getMenu().findItem(2131361983).setVisible(z2);
                BottomNavigationView bottomNavigationView2 = a2.q;
                Wg6.b(bottomNavigationView2, "bottomNavigation");
                Menu menu = bottomNavigationView2.getMenu();
                Wg6.b(menu, "bottomNavigation.menu");
                int size = menu.size();
                for (int i2 = 0; i2 < size; i2++) {
                    MenuItem item = menu.getItem(i2);
                    Wg6.b(item, "getItem(index)");
                    G37<R75> g372 = this.u;
                    if (g372 != null) {
                        R75 a3 = g372.a();
                        BottomNavigationView bottomNavigationView3 = a3 != null ? a3.q : null;
                        if (bottomNavigationView3 != null) {
                            bottomNavigationView3.findViewById(item.getItemId()).setOnLongClickListener(Ci.b);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.n("mBinding");
                        throw null;
                    }
                }
                Hh6 hh6 = new Hh6();
                hh6.element = W6.d(requireContext(), 2131099938);
                Hh6 hh62 = new Hh6();
                hh62.element = W6.d(requireContext(), 2131099967);
                String d2 = ThemeManager.l.a().d("nonBrandDisableCalendarDay");
                String d3 = ThemeManager.l.a().d("primaryColor");
                if (d2 != null) {
                    hh6.element = Color.parseColor(d2);
                }
                if (d3 != null) {
                    hh62.element = Color.parseColor(d3);
                }
                BottomNavigationView bottomNavigationView4 = a2.q;
                Wg6.b(bottomNavigationView4, "bottomNavigation");
                bottomNavigationView4.setItemIconTintList(null);
                a2.q.setOnNavigationItemSelectedListener(new Bi(hh6, hh62, this, d, z2, f));
            }
            Q6();
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void U6() {
        W6();
        Yy5 yy5 = this.t;
        if (yy5 != null) {
            int p = yy5.p();
            if (p == 0) {
                G37<R75> g37 = this.u;
                if (g37 != null) {
                    R75 a2 = g37.a();
                    if (a2 != null) {
                        BottomNavigationView bottomNavigationView = a2.q;
                        Wg6.b(bottomNavigationView, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView.setSelectedItemId(2131362185);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            } else if (p == 1) {
                G37<R75> g372 = this.u;
                if (g372 != null) {
                    R75 a3 = g372.a();
                    if (a3 != null) {
                        BottomNavigationView bottomNavigationView2 = a3.q;
                        Wg6.b(bottomNavigationView2, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView2.setSelectedItemId(2131361983);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            } else if (p == 2) {
                G37<R75> g373 = this.u;
                if (g373 != null) {
                    R75 a4 = g373.a();
                    if (a4 != null) {
                        BottomNavigationView bottomNavigationView3 = a4.q;
                        Wg6.b(bottomNavigationView3, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView3.setSelectedItemId(2131362173);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            } else if (p == 3) {
                G37<R75> g374 = this.u;
                if (g374 != null) {
                    R75 a5 = g374.a();
                    if (a5 != null) {
                        BottomNavigationView bottomNavigationView4 = a5.q;
                        Wg6.b(bottomNavigationView4, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView4.setSelectedItemId(2131361886);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            } else if (p == 4) {
                G37<R75> g375 = this.u;
                if (g375 != null) {
                    R75 a6 = g375.a();
                    if (a6 != null) {
                        BottomNavigationView bottomNavigationView5 = a6.q;
                        Wg6.b(bottomNavigationView5, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView5.setSelectedItemId(2131362967);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            }
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void V6(String str) {
        Wg6.c(str, "presetId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = z;
        StringBuilder sb = new StringBuilder();
        sb.append("navigate to preset id ");
        sb.append(str);
        sb.append(" isPresenterInitialize ");
        sb.append(this.i != null);
        local.d(str2, sb.toString());
        if (!TextUtils.isEmpty(str)) {
            HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.i;
            if (homeDianaCustomizePresenter == null) {
                this.w = str;
            } else if (homeDianaCustomizePresenter != null) {
                homeDianaCustomizePresenter.r(str);
            } else {
                Wg6.n("mHomeDianaCustomizePresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void W6() {
        Yy5 yy5 = this.t;
        if (yy5 == null) {
            return;
        }
        if (yy5 != null) {
            int p = yy5.p();
            int size = this.v.size();
            int i2 = 0;
            while (i2 < size) {
                if (this.v.get(i2) instanceof Aw5) {
                    Fragment fragment = this.v.get(i2);
                    if (fragment != null) {
                        ((Aw5) fragment).b2(i2 == p);
                    } else {
                        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.VisibleChangeListener");
                    }
                }
                i2++;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ou6.Ai
    public void X5(InAppNotification inAppNotification) {
        Yy5 yy5 = this.t;
        if (yy5 == null) {
            Wg6.n("mPresenter");
            throw null;
        } else if (inAppNotification != null) {
            yy5.o(inAppNotification);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void X6(Menu menu, Typeface typeface, int i2) {
        if (typeface != null) {
            int size = menu.size();
            for (int i3 = 0; i3 < size; i3++) {
                MenuItem item = menu.getItem(i3);
                Wg6.b(item, "getItem(index)");
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(item.getTitle());
                spannableStringBuilder.setSpan(new Z47(typeface), 0, spannableStringBuilder.length(), 0);
                spannableStringBuilder.setSpan(new ForegroundColorSpan(i2), 0, spannableStringBuilder.length(), 0);
                item.setTitle(spannableStringBuilder);
            }
        }
    }

    @DexIgnore
    public void Y6(Yy5 yy5) {
        Wg6.c(yy5, "presenter");
        this.t = yy5;
    }

    @DexIgnore
    public final void Z6(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = z;
        local.d(str, "showBottomTab currentDashboardTab=" + i2);
        if (i2 == 0) {
            G37<R75> g37 = this.u;
            if (g37 != null) {
                R75 a2 = g37.a();
                if (a2 != null) {
                    BottomNavigationView bottomNavigationView = a2.q;
                    Wg6.b(bottomNavigationView, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView.setSelectedItemId(2131362185);
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        } else if (i2 == 1) {
            G37<R75> g372 = this.u;
            if (g372 != null) {
                R75 a3 = g372.a();
                if (a3 != null) {
                    BottomNavigationView bottomNavigationView2 = a3.q;
                    Wg6.b(bottomNavigationView2, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView2.setSelectedItemId(2131361983);
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        } else if (i2 == 2) {
            G37<R75> g373 = this.u;
            if (g373 != null) {
                R75 a4 = g373.a();
                if (a4 != null) {
                    BottomNavigationView bottomNavigationView3 = a4.q;
                    Wg6.b(bottomNavigationView3, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView3.setSelectedItemId(2131362173);
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        } else if (i2 == 3) {
            G37<R75> g374 = this.u;
            if (g374 != null) {
                R75 a5 = g374.a();
                if (a5 != null) {
                    BottomNavigationView bottomNavigationView4 = a5.q;
                    Wg6.b(bottomNavigationView4, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView4.setSelectedItemId(2131361886);
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        } else if (i2 == 4) {
            G37<R75> g375 = this.u;
            if (g375 != null) {
                R75 a6 = g375.a();
                if (a6 != null) {
                    BottomNavigationView bottomNavigationView5 = a6.q;
                    Wg6.b(bottomNavigationView5, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView5.setSelectedItemId(2131362967);
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a7(FossilDeviceSerialPatternUtil.DEVICE device) {
        FLogger.INSTANCE.getLocal().d(z, "Inside .showMainFlow");
        HomeDashboardFragment a2 = HomeDashboardFragment.Y.a();
        Fragment Z = getChildFragmentManager().Z(Nv4.l.a());
        Fragment Z2 = getChildFragmentManager().Z("HomeDianaCustomizeFragment");
        Fragment Z3 = getChildFragmentManager().Z("HomeHybridCustomizeFragment");
        Fragment Z4 = getChildFragmentManager().Z("HomeAlertsFragment");
        Fragment Z5 = getChildFragmentManager().Z("HomeAlertsHybridFragment");
        Fragment Z6 = getChildFragmentManager().Z("HomeProfileFragment");
        Fragment Z7 = getChildFragmentManager().Z("HomeUpdateFirmwareFragment");
        if (Z2 == null) {
            Z2 = HomeDianaCustomizeFragment.E.a(this.w);
            this.w = "";
        }
        if (Z == null) {
            Z = Nv4.l.b();
        }
        if (Z3 == null) {
            Z3 = HomeHybridCustomizeFragment.x.a();
        }
        if (Z4 == null) {
            Z4 = HomeAlertsFragment.s.a();
        }
        if (Z5 == null) {
            Z5 = HomeAlertsHybridFragment.l.a();
        }
        if (Z6 == null) {
            Z6 = HomeProfileFragment.m.a();
        }
        if (Z7 == null) {
            Z7 = Tn6.t.a();
        }
        this.v.clear();
        this.v.add(a2);
        this.v.add(Z);
        if (DeviceHelper.o.w(device)) {
            this.v.add(Z2);
            this.v.add(Z4);
        } else {
            this.v.add(Z3);
            this.v.add(Z5);
        }
        this.v.add(Z6);
        this.v.add(Z7);
        G37<R75> g37 = this.u;
        if (g37 != null) {
            R75 a3 = g37.a();
            if (a3 != null) {
                try {
                    ViewPager2 viewPager2 = a3.s;
                    Wg6.b(viewPager2, "binding.rvTabs");
                    viewPager2.setAdapter(new G67(getChildFragmentManager(), this.v));
                    if (a3.s.getChildAt(0) != null) {
                        View childAt = a3.s.getChildAt(0);
                        if (childAt != null) {
                            ((RecyclerView) childAt).setItemViewCacheSize(4);
                        } else {
                            throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                        }
                    }
                    ViewPager2 viewPager22 = a3.s;
                    Wg6.b(viewPager22, "binding.rvTabs");
                    viewPager22.setUserInputEnabled(false);
                    Cd6 cd6 = Cd6.a;
                } catch (Exception e) {
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        HomeActivity.a aVar = HomeActivity.B;
                        Wg6.b(activity, "it");
                        Yy5 yy5 = this.t;
                        if (yy5 != null) {
                            aVar.a(activity, Integer.valueOf(yy5.p()));
                            Cd6 cd62 = Cd6.a;
                        } else {
                            Wg6.n("mPresenter");
                            throw null;
                        }
                    }
                }
            }
            Iface iface = PortfolioApp.get.instance().getIface();
            if (Z2 != null) {
                HomeDianaCustomizeFragment homeDianaCustomizeFragment = (HomeDianaCustomizeFragment) Z2;
                if (Z3 != null) {
                    HomeHybridCustomizeFragment homeHybridCustomizeFragment = (HomeHybridCustomizeFragment) Z3;
                    if (Z6 != null) {
                        HomeProfileFragment homeProfileFragment = (HomeProfileFragment) Z6;
                        if (Z4 != null) {
                            HomeAlertsFragment homeAlertsFragment = (HomeAlertsFragment) Z4;
                            if (Z5 != null) {
                                HomeAlertsHybridFragment homeAlertsHybridFragment = (HomeAlertsHybridFragment) Z5;
                                if (Z7 != null) {
                                    iface.p1(new Oz5(a2, homeDianaCustomizeFragment, homeHybridCustomizeFragment, homeProfileFragment, homeAlertsFragment, homeAlertsHybridFragment, (Tn6) Z7)).a(this);
                                    return;
                                }
                                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment");
                            }
                            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
                        }
                        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
                    }
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void b7() {
        FLogger.INSTANCE.getLocal().d(z, "Inside .showNoActiveDeviceFlow");
        HomeDashboardFragment a2 = HomeDashboardFragment.Y.a();
        Fragment Z = getChildFragmentManager().Z("HomeDianaCustomizeFragment");
        Fragment Z2 = getChildFragmentManager().Z("HomeHybridCustomizeFragment");
        Fragment Z3 = getChildFragmentManager().Z("HomeAlertsFragment");
        Fragment Z4 = getChildFragmentManager().Z("HomeAlertsHybridFragment");
        Fragment Z5 = getChildFragmentManager().Z("HomeProfileFragment");
        Fragment Z6 = getChildFragmentManager().Z("HomeUpdateFirmwareFragment");
        Fragment Z7 = getChildFragmentManager().Z(Nv4.l.a());
        if (Z == null) {
            Z = HomeDianaCustomizeFragment.E.a(this.w);
            this.w = "";
        }
        if (Z7 == null) {
            Z7 = Nv4.l.b();
        }
        if (Z2 == null) {
            Z2 = HomeHybridCustomizeFragment.x.a();
        }
        if (Z3 == null) {
            Z3 = HomeAlertsFragment.s.a();
        }
        if (Z4 == null) {
            Z4 = HomeAlertsHybridFragment.l.a();
        }
        if (Z5 == null) {
            Z5 = HomeProfileFragment.m.a();
        }
        if (Z6 == null) {
            Z6 = Tn6.t.a();
        }
        this.v.clear();
        this.v.add(a2);
        this.v.add(Z7);
        this.v.add(Z2);
        this.v.add(Z3);
        this.v.add(Z5);
        this.v.add(Z6);
        G37<R75> g37 = this.u;
        if (g37 != null) {
            R75 a3 = g37.a();
            if (a3 != null) {
                ViewPager2 viewPager2 = a3.s;
                Wg6.b(viewPager2, "binding.rvTabs");
                viewPager2.setAdapter(new G67(getChildFragmentManager(), this.v));
                if (a3.s.getChildAt(0) != null) {
                    View childAt = a3.s.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setItemViewCacheSize(4);
                    } else {
                        throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
                ViewPager2 viewPager22 = a3.s;
                Wg6.b(viewPager22, "binding.rvTabs");
                viewPager22.setUserInputEnabled(false);
            }
            Iface iface = PortfolioApp.get.instance().getIface();
            if (Z != null) {
                HomeDianaCustomizeFragment homeDianaCustomizeFragment = (HomeDianaCustomizeFragment) Z;
                if (Z2 != null) {
                    HomeHybridCustomizeFragment homeHybridCustomizeFragment = (HomeHybridCustomizeFragment) Z2;
                    if (Z5 != null) {
                        HomeProfileFragment homeProfileFragment = (HomeProfileFragment) Z5;
                        if (Z3 != null) {
                            HomeAlertsFragment homeAlertsFragment = (HomeAlertsFragment) Z3;
                            if (Z4 != null) {
                                HomeAlertsHybridFragment homeAlertsHybridFragment = (HomeAlertsHybridFragment) Z4;
                                if (Z6 != null) {
                                    iface.p1(new Oz5(a2, homeDianaCustomizeFragment, homeHybridCustomizeFragment, homeProfileFragment, homeAlertsFragment, homeAlertsHybridFragment, (Tn6) Z6)).a(this);
                                    return;
                                }
                                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment");
                            }
                            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
                        }
                        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
                    }
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void c7(int i2) {
        if (i2 == 1) {
            G37<R75> g37 = this.u;
            if (g37 != null) {
                R75 a2 = g37.a();
                if (a2 != null) {
                    ViewPager2 viewPager2 = a2.s;
                    Wg6.b(viewPager2, "mBinding.get()!!.rvTabs");
                    viewPager2.setDescendantFocusability(262144);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        } else {
            G37<R75> g372 = this.u;
            if (g372 != null) {
                R75 a3 = g372.a();
                if (a3 != null) {
                    ViewPager2 viewPager22 = a3.s;
                    Wg6.b(viewPager22, "mBinding.get()!!.rvTabs");
                    viewPager22.setDescendantFocusability(393216);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        }
        Yy5 yy5 = this.t;
        if (yy5 != null) {
            if (!yy5.q()) {
                G37<R75> g373 = this.u;
                if (g373 != null) {
                    R75 a4 = g373.a();
                    if (a4 != null) {
                        a4.s.j(i2, false);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            } else if (i2 > 0) {
                G37<R75> g374 = this.u;
                if (g374 != null) {
                    R75 a5 = g374.a();
                    if (a5 != null) {
                        a5.s.j(5, false);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            } else {
                G37<R75> g375 = this.u;
                if (g375 != null) {
                    R75 a6 = g375.a();
                    if (a6 != null) {
                        a6.s.j(i2, false);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            }
            W6();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void d7(String str) {
        J6(new Intent("android.intent.action.VIEW", Uri.parse(str)), z);
    }

    @DexIgnore
    @Override // com.fossil.Zy5
    public void o0(int i2) {
        HomeDashboardPresenter homeDashboardPresenter = this.h;
        if (homeDashboardPresenter == null) {
            return;
        }
        if (homeDashboardPresenter != null) {
            homeDashboardPresenter.a0(i2);
        } else {
            Wg6.n("mHomeDashboardPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zy5, androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = z;
        local.d(str, "onActivityResult " + i2 + ' ' + i2);
        HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.i;
        if (homeDianaCustomizePresenter != null) {
            if (homeDianaCustomizePresenter != null) {
                homeDianaCustomizePresenter.l0(i2, i3, intent);
            } else {
                Wg6.n("mHomeDianaCustomizePresenter");
                throw null;
            }
        }
        HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.j;
        if (homeHybridCustomizePresenter == null) {
            return;
        }
        if (homeHybridCustomizePresenter != null) {
            homeHybridCustomizePresenter.Q(i2, i3, intent);
        } else {
            Wg6.n("mHomeHybridCustomizePresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        R75 r75 = (R75) Aq0.f(layoutInflater, 2131558570, viewGroup, false, A6());
        this.u = new G37<>(this, r75);
        Wg6.b(r75, "binding");
        return r75.n();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        Yy5 yy5 = this.t;
        if (yy5 != null) {
            yy5.s();
            Yy5 yy52 = this.t;
            if (yy52 != null) {
                yy52.n();
                super.onDestroy();
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Yy5 yy5 = this.t;
        if (yy5 != null) {
            yy5.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Yy5 yy5 = this.t;
        if (yy5 != null) {
            yy5.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        Q6();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Ts0 a2 = Vs0.e(activity).a(Y67.class);
            Wg6.b(a2, "ViewModelProviders.of(ac\u2026ardViewModel::class.java)");
            Y67 y67 = (Y67) a2;
            this.x = y67;
            if (y67 != null) {
                y67.a().h(activity, new Di(this));
            } else {
                Wg6.n("mHomeDashboardViewModel");
                throw null;
            }
        }
        Bundle arguments = getArguments();
        if (arguments != null && arguments.getBoolean("KEY_DIANA_REQUIRE")) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.u(childFragmentManager);
        }
        PortfolioApp.get.instance().K().h(getViewLifecycleOwner(), new Ei(this));
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Zy5
    public void z3(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        Wg6.c(zendeskFeedbackConfiguration, "configuration");
        Context context = getContext();
        if (context != null) {
            Intent intent = new Intent(context, ContactZendeskActivity.class);
            intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
            startActivityForResult(intent, 1007);
        }
    }
}
