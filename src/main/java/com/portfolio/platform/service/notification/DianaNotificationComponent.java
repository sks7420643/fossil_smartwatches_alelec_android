package com.portfolio.platform.service.notification;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.Telephony;
import android.service.notification.StatusBarNotification;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.D47;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.I24;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.L44;
import com.fossil.Mn5;
import com.fossil.Qq7;
import com.fossil.Vt7;
import com.fossil.Wr5;
import com.fossil.Wt7;
import com.fossil.Yk7;
import com.fossil.Yn7;
import com.fossil.Zk7;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Gg6;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Mj6;
import com.mapped.PermissionUtils;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import android.content.SharedPreferences;
import com.portfolio.platform.AlelecPrefs;

import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.graphics.drawable.LayerDrawable;
import com.mapped.AppWrapper;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.util.NotificationAppHelper;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexWrap;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaNotificationComponent {
    @DexIgnore
    public static /* final */ HashMap<String, Ci> o; // = new HashMap<>();
    @DexIgnore
    public static /* final */ HashMap<String, Ji> p;
    @DexIgnore
    public static /* final */ Ai q; // = new Ai(null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Di b; // = new Di(this.k);
    @DexIgnore
    public /* final */ ConcurrentHashMap<String, Long> c; // = new ConcurrentHashMap<>();
    @DexIgnore
    public /* final */ Yk7 d; // = Zk7.a(Oi.INSTANCE);
    @DexIgnore
    public /* final */ Yk7 e; // = Zk7.a(Pi.INSTANCE);
    @DexIgnore
    public int f; // = 1380;
    @DexIgnore
    public int g; // = 1140;
    @DexIgnore
    public Hi h;
    @DexIgnore
    public Fi i;
    @DexIgnore
    public /* final */ Queue<NotificationBaseObj> j; // = L44.a(I24.create(20));
    @DexIgnore
    public /* final */ An4 k;
    @DexIgnore
    public /* final */ DNDSettingsDatabase l;
    @DexIgnore
    public /* final */ QuickResponseRepository m;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final HashMap<String, Ci> a() {
            if (DianaNotificationComponent.o.isEmpty()) {
                int i = 1;
                for (DianaNotificationObj.AApplicationName t : DianaNotificationObj.AApplicationName.Companion.getSUPPORTED_ICON_NOTIFICATION_APPS()) {
                    String packageName = t.getPackageName();
                    if (!Wg6.a(packageName, DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) && !Wg6.a(packageName, DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName()) && !Wg6.a(packageName, DianaNotificationObj.AApplicationName.Companion.getGOOGLE_CALENDAR().getPackageName()) && !Wg6.a(packageName, DianaNotificationObj.AApplicationName.Companion.getHANGOUTS().getPackageName()) && !Wg6.a(packageName, DianaNotificationObj.AApplicationName.Companion.getGMAIL().getPackageName()) && !Wg6.a(packageName, DianaNotificationObj.AApplicationName.Companion.getWHATSAPP().getPackageName())) {
                        DianaNotificationComponent.o.put(t.getPackageName(), new Ci(i, t, false, false));
                    } else {
                        DianaNotificationComponent.o.put(t.getPackageName(), new Ci(i, t, false, true));
                    }
                    i++;
                }
            }
            return DianaNotificationComponent.o;
        }
    }

    @DexIgnore
    public enum Bi {
        ACTIVE,
        SILENT,
        SKIP
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public /* final */ DianaNotificationObj.AApplicationName a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public Ci(int i, DianaNotificationObj.AApplicationName aApplicationName, boolean z, boolean z2) {
            Wg6.c(aApplicationName, "notificationApp");
            this.a = aApplicationName;
            this.b = z2;
        }

        @DexIgnore
        public final DianaNotificationObj.AApplicationName a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di {
        @DexIgnore
        public /* final */ List<Ei> a; // = new ArrayList();
        @DexIgnore
        public int b; // = this.c.z();
        @DexIgnore
        public /* final */ An4 c;

        @DexIgnore
        public Di(An4 an4) {
            Wg6.c(an4, "mSharedPref");
            this.c = an4;
        }

        @DexIgnore
        public final Ei a(Ei ei) {
            Ei t;
            boolean z;
            synchronized (this) {
                Wg6.c(ei, "notificationStatus");
                if (this.a.contains(ei)) {
                    ei = null;
                } else if (Vt7.j(ei.e(), Telephony.Sms.getDefaultSmsPackage(PortfolioApp.get.instance()), true)) {
                    Iterator<Ei> it = this.a.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        Ei next = it.next();
                        Ei t2 = next;
                        if (!Vt7.j(t2.i(), ei.i(), true) || (t2.k() != ei.k() && ei.k() - t2.k() > 1)) {
                            z = false;
//                            continue;
                        } else {
                            z = true;
//                            continue;
                        }
                        if (z) {
                            t = next;
                            break;
                        }
                    }
                    Ei t3 = t;
                    if (t3 != null) {
                        t3.w(ei.g());
                        t3.B(ei.k());
                        return null;
                    }
                    ei.t(b());
                    this.a.add(ei);
                    e(b() + 1);
                } else {
                    ei.t(b());
                    this.a.add(ei);
                    e(b() + 1);
                }
                return ei;
            }
        }

        @DexIgnore
        public final int b() {
            int i = this.b;
            if (i < 0) {
                return 0;
            }
            return i;
        }

        @DexIgnore
        public final List<Ei> c(String str, String str2) {
            ArrayList arrayList;
            synchronized (this) {
                Wg6.c(str, "realId");
                Wg6.c(str2, "packageName");
                List<Ei> list = this.a;
                arrayList = new ArrayList();
                for (Ei t : list) {
                    Ei t2 = t;
                    if (Wg6.a(t2.g(), str) && Wg6.a(t2.e(), str2)) {
                        arrayList.add(t);
                    }
                }
                this.a.removeAll(arrayList);
            }
            return arrayList;
        }

        @DexIgnore
        public final List<Ei> d(String str) {
            ArrayList arrayList;
            synchronized (this) {
                Wg6.c(str, "name");
                List<Ei> list = this.a;
                arrayList = new ArrayList();
                for (Ei t : list) {
                    Ei t2 = t;
                    if (PhoneNumberUtils.compare(str, t2.h()) && Wg6.a(t2.e(), DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName())) {
                        arrayList.add(t);
                    }
                }
                this.a.removeAll(arrayList);
            }
            return arrayList;
        }

        @DexIgnore
        public final void e(int i) {
            if (i >= Integer.MAX_VALUE) {
                i = 0;
            }
            this.b = i;
            this.c.x1(i);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, staticConstructorAction = DexAction.APPEND)
    public static class Ei {
        @DexIgnore
        public String a;
        @DexIgnore
        public int b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public long h;
        @DexIgnore
        public boolean i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public int k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public boolean m;
        @DexAdd
        public boolean dnd = false;

        @DexIgnore
        public Ei() {
            this.a = "";
            this.c = "";
            this.d = "";
            this.e = "";
            this.f = "";
            this.g = "";
        }

        @DexAdd
        private boolean checkDND(Notification notification) {
            // Return true if the notification should be hidden for DND
            String category = notification.category;
            NotificationManager notificationManager = (NotificationManager)PortfolioApp.get.instance().getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int filter = notificationManager.getCurrentInterruptionFilter();

                FLogger.INSTANCE.getLocal().d("DianaNotificationComponent", "checkDND: filter = " + notificationManager.getCurrentInterruptionFilter());
                FLogger.INSTANCE.getLocal().d("DianaNotificationComponent", "checkDND: category = " + category);
                switch (filter) {
                    case NotificationManager.INTERRUPTION_FILTER_NONE:
                        return true;

                    case NotificationManager.INTERRUPTION_FILTER_ALARMS:
                        if (!TextUtils.equals(Notification.CATEGORY_ALARM, category)) {
                            return true;
                        }
                        break;

                    case NotificationManager.INTERRUPTION_FILTER_PRIORITY:
                        String[] activeCategories = NotificationManager.Policy.priorityCategoriesToString(notificationManager.getNotificationPolicy().priorityCategories).split(",");
                        StringBuilder log = new StringBuilder("checkDND: activeCategories = ");
                        boolean ret = true;
                        for (String active : activeCategories) {
                            log.append(active).append(",");
                            if (TextUtils.equals(active, category)) {
                                ret = false;
                            }
                        }
                        FLogger.INSTANCE.getLocal().d("DianaNotificationComponent",log.toString());

                        if (category == null && notification.priority >= Notification.PRIORITY_HIGH) {
                            FLogger.INSTANCE.getLocal().d("DianaNotificationComponent", "checkDND: priority = " + notification.priority + "dndHighSettingEnabled: " + dndHighSettingEnabled);
                            if (dndHighSettingEnabled) {
                                ret = false;
                            }
                        }

                        return ret;

                    case NotificationManager.INTERRUPTION_FILTER_UNKNOWN:
                    case NotificationManager.INTERRUPTION_FILTER_ALL:
                    default:
                        return false;
                }
            }
            return false;
        }

        @DexAdd
        public class AlelecSettingsListener implements SharedPreferences.OnSharedPreferenceChangeListener {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
                Context ctx = PortfolioApp.get.instance().getApplicationContext();
                dndSettingEnabled = AlelecPrefs.getAndroidDND(ctx);
                dndHighSettingEnabled = AlelecPrefs.getAndroidDND_HIGH(ctx);
                emptyNotificationsEnabled = AlelecPrefs.getEmptyNotifications(ctx);
            }
        }

        @DexAdd
        @SuppressWarnings("WeakerAccess")
        public AlelecSettingsListener dndSettingsListener = null;
        @DexAdd
        @SuppressWarnings("WeakerAccess")
        public boolean dndSettingEnabled = false;
        @DexAdd
        @SuppressWarnings("WeakerAccess")
        public static boolean dndHighSettingEnabled = false;
        @DexAdd
        @SuppressWarnings("WeakerAccess")
        public boolean emptyNotificationsEnabled = false;

        @DexAdd
        public Ei(StatusBarNotification statusBarNotification) {
            this(statusBarNotification, (Void) null);
            this.dnd = checkDND(statusBarNotification.getNotification());

            // Grab icon if needed

            String packageName = this.c;

            Context context = PortfolioApp.get.instance().getApplicationContext();
            File iconFile = new File(context.getExternalFilesDir("appIcons"), packageName + ".icon");

            synchronized (fileLock) {
                if (!iconFile.exists()) {
                    try {
                        PackageManager manager = context.getPackageManager();
                        Resources resources = manager.getResourcesForApplication(packageName);
                        // Icon small = statusBarNotification.getNotification().getSmallIcon();
                        Drawable drawable;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                            try {
                                drawable = statusBarNotification.getNotification().getSmallIcon().loadDrawable(context);
                            } catch (Exception unused) {
                                drawable = statusBarNotification.getNotification().getLargeIcon().loadDrawable(context);
                            }
                        } else {
                            drawable = resources.getDrawable(statusBarNotification.getNotification().icon);
                        }

                        int notifIconWidth = 24;
                        int notifIconHeight = 24;

                        byte[] notifIcon = drawableToPixels(drawable, notifIconWidth, notifIconHeight, 0, false);
                        if (notifIcon == null) {
                            notifIcon = drawableToPixels(drawable, notifIconWidth, notifIconHeight, 0xff000000, false);
                        }
                        if (notifIcon == null) {
                            notifIcon = drawableToPixels(drawable, notifIconWidth, notifIconHeight, 0xffffffff, true);
                        }
                        if (notifIcon != null) {

                            // Convert to RLE with header and footer
                            byte[] rle = rle_encode(notifIcon);
                            notifIcon = new byte[rle.length + 4];
                            notifIcon[0] = (byte) notifIconWidth;
                            notifIcon[1] = (byte) notifIconHeight;
                            notifIcon[notifIcon.length - 1] = (byte) 0xff;
                            notifIcon[notifIcon.length - 2] = (byte) 0xff;
                            System.arraycopy(rle, 0, notifIcon, 2, rle.length);

                            // Write to file for next time

                            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(iconFile));
                            bos.write(notifIcon);
                            bos.flush();
                            bos.close();
                        }

                        // Send Filters to watch
                        NotificationAppHelper notificationAppHelper = NotificationAppHelper.b;
                        // In NotificationAppHelper find function that returns List<AppWrapper> taking one arg context
                        List<AppWrapper> allApps = (List<AppWrapper>) notificationAppHelper.g(context);
                        List<AppWrapper> apps = new ArrayList<>();
                        for (AppWrapper a : allApps) {
                            InstalledApp installedApp2 = a.getInstalledApp();
                            if (installedApp2 != null) {
                                Boolean isSelected = installedApp2.isSelected();
                                // wd4.a(isSelected, "it.isSelected");
                                if (isSelected) {
                                    apps.add(a);
                                }
                            }
                        }

                        List<AppNotificationFilter> j = new ArrayList<>();

                        // From NotificationAppsPresenter, search for "setRuleToDevice, showProgressDialog", about 9 lines below.

                        //local2.d(str2, "mListAppWrapper.size = " + this.g.size());
                        j.addAll(D47.b(apps, false));

                        // In NotificationAppHelper find function that returns List<AppNotificationFilter> taking one two int args
                        List<AppNotificationFilter> phone_sms_filters = notificationAppHelper.h(0, 0);
                        j.addAll(phone_sms_filters);

                        // And from the line below the previous one in NotificationAppsPresenter...
                        PortfolioApp.get.instance().E1(new AppNotificationFilterSettings(j, System.currentTimeMillis()), PortfolioApp.get.instance().J());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }

        @DexAdd
        public byte[] drawableToPixels(Drawable drawable, int width, int height, int background, boolean invert) {

            Bitmap bitmap = null;
            // Draw icon onto 24 x 24 bitmap

            // if (drawable instanceof BitmapDrawable) {
            //     bitmap = ((BitmapDrawable) drawable).getBitmap();
            // } else
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (drawable instanceof AdaptiveIconDrawable) {
                    Drawable backgroundDr = ((AdaptiveIconDrawable) drawable).getBackground();
                    Drawable foregroundDr = ((AdaptiveIconDrawable) drawable).getForeground();

                    Drawable[] drr = new Drawable[2];
                    drr[0] = backgroundDr;
                    drr[1] = foregroundDr;

                    bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bitmap);
                    LayerDrawable layerDrawable = new LayerDrawable(drr);
                    layerDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                    layerDrawable.draw(canvas);
                }
            }
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);

                drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                drawable.draw(canvas);
            }
            if (bitmap != null) {

                // Convert to greyscale
                Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas c = new Canvas(bmpGrayscale);

                if ((background & 0xFF000000) != 0) {
                    Paint paint = new Paint();
                    paint.setColor(background);
                    c.drawRect(0F, 0F, (float) width, (float) height, paint);
                }

                Paint paint = new Paint();
                ColorMatrix cm = new ColorMatrix();
                cm.setSaturation(0);
                ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
                paint.setColorFilter(f);

                c.drawBitmap(bitmap, 0, 0, paint);
                // scaled.recycle();

                // Downsample to 2 bit image
                int[] pixels = new int[width * height];
                bmpGrayscale.getPixels(pixels, 0, width, 0, 0, width, height);
                byte[] b_pixels = new byte[pixels.length];

                int n_light_pix = 0;
                for (int i = 0; i < pixels.length; i++) {
                    int pix = pixels[i];
                    //todo invert
                    b_pixels[i] = (byte) (pix >> 6 & 0x03);
                    if (invert) {
                        b_pixels[i] = (byte)(3 - b_pixels[i]);
                    }
                    if (b_pixels[i] > 0) {
                        n_light_pix++;
                    }
                }
                bitmap.recycle();
                bmpGrayscale.recycle();
                if ((100.0 * n_light_pix / pixels.length) < 5) {
                    // under 5% pixels visible, need to invert the background and try again
                    return null;
                }
                return b_pixels;
            }
            return null;
        }

        @DexAdd
        public static byte[] rle_encode(byte[] chars) {
            try {
                if (chars == null || chars.length == 0) return null;

                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                Writer writer = new OutputStreamWriter(bout);

                // StringBuilder builder = new StringBuilder();
                byte current = chars[0];
                int count = 1;

                for (int i = 1; i < chars.length; i++) {
                    if (current != chars[i]) {
                        if (count > 0) {
                            writer.write(count);
                            writer.write(current);
                        }
                        current = chars[i];
                        count = 1;
                    } else {
                        count++;
                        if (count == 254) {
                            writer.write(count);
                            writer.write(current);
                            count = 0;
                        }
                    }
                }
                if (count > 1) writer.write(count);
                writer.write(current);
                writer.flush();
                return bout.toByteArray();
            } catch (IOException unused) {
                return null;
            }
        }

        @DexAdd
        private static final Object fileLock = new Object();

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        @DexEdit
        public Ei(StatusBarNotification statusBarNotification, @DexIgnore Void tag) {
            this();
            String obj;
            Bundle bundle;
            DianaNotificationObj.AApplicationName a2;
            Wg6.c(statusBarNotification, "sbn");
            this.a = b((long) statusBarNotification.getId(), statusBarNotification.getTag());
            String packageName = statusBarNotification.getPackageName();
            Wg6.b(packageName, "sbn.packageName");
            this.c = packageName;
            Ci ci = DianaNotificationComponent.q.a().get(this.c);
            String N = (ci == null || (a2 = ci.a()) == null || (N = a2.getAppName()) == null) ? PortfolioApp.get.instance().N(this.c) : N;
            CharSequence charSequence = statusBarNotification.getNotification().extras.getCharSequence("android.title");
            String obj2 = (charSequence != null ? charSequence : N).toString();
            this.d = obj2;
            x(obj2);
            CharSequence charSequence2 = statusBarNotification.getNotification().extras.getCharSequence("android.bigText");
            if (!(charSequence2 == null || Vt7.l(charSequence2))) {
                Notification notification = statusBarNotification.getNotification();
                obj = String.valueOf((notification == null || (bundle = notification.extras) == null) ? null : bundle.getCharSequence("android.bigText"));
            } else {
                String charSequence3 = statusBarNotification.getNotification().extras.getCharSequence("android.text").toString();
                obj = (charSequence3 == null ? "" : charSequence3).toString();
            }
            z(obj);
            String str = statusBarNotification.getNotification().category;
            this.g = str == null ? "" : str;
            this.i = r(statusBarNotification);
            this.j = p(statusBarNotification);
            this.h = statusBarNotification.getNotification().when;
            this.k = statusBarNotification.getNotification().priority;
            this.l = statusBarNotification.getNotification().extras.getInt("android.progressMax", 0) != 0;
            if (n(statusBarNotification)) {
                this.m = true;
                CharSequence charSequence4 = statusBarNotification.getNotification().tickerText;
                if (charSequence4 != null) {
                    int length = charSequence4.length();
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            i2 = -1;
                            break;
                        } else if (Wg6.a(String.valueOf(charSequence4.charAt(i2)), ":")) {
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (i2 != -1) {
                        String obj3 = charSequence4.subSequence(0, i2).toString();
                        if (obj3 != null) {
                            x(Wt7.u0(obj3).toString());
                            String obj4 = charSequence4.subSequence(i2 + 1, charSequence4.length()).toString();
                            if (obj4 != null) {
                                z(Wt7.u0(obj4).toString());
                                return;
                            }
                            throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
                        }
                        throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
            }
        }

        @DexIgnore
        public final void A(String str) {
            Wg6.c(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void B(long j2) {
            this.h = j2;
        }

        @DexIgnore
        public final String a(String str) {
            String replace = new Mj6("\\p{C}").replace(str, "");
            int length = replace.length() - 1;
            boolean z = false;
            int i2 = 0;
            while (i2 <= length) {
                boolean z2 = replace.charAt(!z ? i2 : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i2++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            return replace.subSequence(i2, length + 1).toString();
        }

        @DexIgnore
        public final String b(long j2, String str) {
            return j2 + ':' + str;
        }

        @DexIgnore
        public final String c() {
            return this.g;
        }

        @DexIgnore
        public final int d() {
            return this.b;
        }

        @DexIgnore
        public final String e() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof Ei)) {
                return false;
            }
            Ei ei = (Ei) obj;
            return Wg6.a(this.a, ei.a) && this.h == ei.h;
        }

        @DexIgnore
        public final int f() {
            return this.k;
        }

        @DexIgnore
        public final String g() {
            return this.a;
        }

        @DexIgnore
        public final String h() {
            return this.e;
        }

        @DexIgnore
        public int hashCode() {
            return 0;
        }

        @DexIgnore
        public final String i() {
            return this.f;
        }

        @DexIgnore
        public final String j() {
            return this.d;
        }

        @DexIgnore
        public final long k() {
            return this.h;
        }

        @DexIgnore
        public final boolean l() {
            return this.l;
        }

        @DexIgnore
        public final boolean m() {
            return this.m;
        }

        @DexIgnore
        public final boolean n(StatusBarNotification statusBarNotification) {
            String tag = statusBarNotification.getTag();
            if (tag == null || (!Wt7.u(tag, "sms", true) && !Wt7.u(tag, "mms", true) && !Wt7.u(tag, "rcs", true))) {
                return TextUtils.equals(statusBarNotification.getNotification().category, "msg");
            }
            return true;
        }

        @DexIgnore
        public final boolean o() {
            return this.j;
        }

        @DexIgnore
        public final boolean p(StatusBarNotification statusBarNotification) {
            return (statusBarNotification.getNotification().flags & 2) == 2;
        }

        @DexIgnore
        public final boolean q() {
            return this.i;
        }

        @DexIgnore
        public final boolean r(StatusBarNotification statusBarNotification) {
            return (statusBarNotification.getNotification().flags & 512) == 512;
        }

        @DexIgnore
        public final void s(boolean z) {
            this.l = z;
        }

        @DexIgnore
        public final void t(int i2) {
            this.b = i2;
        }

        @DexIgnore
        public final void u(boolean z) {
            this.j = z;
        }

        @DexIgnore
        public final void v(String str) {
            Wg6.c(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void w(String str) {
            Wg6.c(str, "<set-?>");
            this.a = str;
        }

        @DexIgnore
        public final void x(String str) {
            Wg6.c(str, "value");
            this.e = a(str);
        }

        @DexIgnore
        public final void y(boolean z) {
            this.i = z;
        }

        @DexIgnore
        public final void z(String str) {
            Wg6.c(str, "value");
            this.f = a(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Fi extends ContentObserver {
        @DexIgnore
        public Bii a; // = new Bii(this, 0, null, 0, 0, 15, null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
        public /*static*/ final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    Fi fi = this.this$0;
                    Bii d = fi.d();
                    if (d == null) {
                        d = new Bii(this.this$0, 0, null, 0, 0, 15, null);
                    }
                    fi.a = d;
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class Bii {
            @DexIgnore
            public /* final */ long a;
            @DexIgnore
            public /* final */ String b;
            @DexIgnore
            public /* final */ int c;

            @DexIgnore
            public Bii(Fi fi, long j, String str, int i, long j2) {
                Wg6.c(str, "number");
                this.a = j;
                this.b = str;
                this.c = i;
            }

            @DexIgnore
            /* JADX INFO: this call moved to the top of the method (can break code semantics) */
            public /* synthetic */ Bii(Fi fi, long j, String str, int i, long j2, int i2, Qg6 qg6) {
                this(fi, (i2 & 1) != 0 ? -1 : j, (i2 & 2) != 0 ? "" : str, (i2 & 4) != 0 ? 0 : i, (i2 & 8) != 0 ? 0 : j2);
            }

            @DexIgnore
            public final long a() {
                return this.a;
            }

            @DexIgnore
            public final String b() {
                return this.b;
            }

            @DexIgnore
            public final int c() {
                return this.c;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$processMissedCall$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
        public /*static*/ final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    Bii d = this.this$0.d();
                    if (d != null) {
                        Bii bii = this.this$0.a;
                        if (bii != null) {
                            if (bii.a() < d.a()) {
                                if (d.c() == 3) {
                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                    String s = DianaNotificationComponent.this.s();
                                    local.d(s, "processMissedCall - number = " + d.b());
                                    DianaNotificationComponent.this.U(d.b(), new Date(), Gi.MISSED);
                                } else if (d.c() == 5 || d.c() == 6 || d.c() == 1) {
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String s2 = DianaNotificationComponent.this.s();
                                    local2.d(s2, "processHangUpCall - number = " + d.b());
                                    DianaNotificationComponent.this.U(d.b(), new Date(), Gi.PICKED);
                                }
                            }
                            this.this$0.a = d;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Fi() {
            super(null);
            PermissionUtils.Ai ai = PermissionUtils.a;
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            Wg6.b(applicationContext, "PortfolioApp.instance.applicationContext");
            if (ai.k(applicationContext)) {
                Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Aii(this, null), 3, null);
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x007c, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x007d, code lost:
            com.fossil.So7.a(r9, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0080, code lost:
            throw r1;
         */
        @DexIgnore
        @android.annotation.SuppressLint({"MissingPermission"})
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final com.portfolio.platform.service.notification.DianaNotificationComponent.Fi.Bii d() {
            /*
                r10 = this;
                r8 = 0
                com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get     // Catch:{ Exception -> 0x0081 }
                com.portfolio.platform.PortfolioApp r0 = r0.instance()     // Catch:{ Exception -> 0x0081 }
                android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0081 }
                android.net.Uri r1 = android.provider.CallLog.Calls.CONTENT_URI     // Catch:{ Exception -> 0x0081 }
                r2 = 4
                java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0081 }
                r3 = 0
                java.lang.String r4 = "_id"
                r2[r3] = r4     // Catch:{ Exception -> 0x0081 }
                r3 = 1
                java.lang.String r4 = "number"
                r2[r3] = r4     // Catch:{ Exception -> 0x0081 }
                r3 = 2
                java.lang.String r4 = "date"
                r2[r3] = r4     // Catch:{ Exception -> 0x0081 }
                r3 = 3
                java.lang.String r4 = "type"
                r2[r3] = r4     // Catch:{ Exception -> 0x0081 }
                r3 = 0
                r4 = 0
                java.lang.String r5 = "date DESC LIMIT 1"
                android.database.Cursor r9 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0081 }
                if (r9 == 0) goto L_0x0078
                boolean r0 = r9.moveToFirst()     // Catch:{ all -> 0x007a }
                if (r0 == 0) goto L_0x006f
                java.lang.String r0 = "_id"
                int r0 = r9.getColumnIndex(r0)     // Catch:{ all -> 0x007a }
                long r2 = r9.getLong(r0)     // Catch:{ all -> 0x007a }
                java.lang.String r0 = "number"
                int r0 = r9.getColumnIndex(r0)     // Catch:{ all -> 0x007a }
                java.lang.String r4 = r9.getString(r0)     // Catch:{ all -> 0x007a }
                java.lang.String r0 = "type"
                int r0 = r9.getColumnIndex(r0)     // Catch:{ all -> 0x007a }
                int r5 = r9.getInt(r0)     // Catch:{ all -> 0x007a }
                java.lang.String r0 = "date"
                int r0 = r9.getColumnIndex(r0)     // Catch:{ all -> 0x007a }
                long r6 = r9.getLong(r0)     // Catch:{ all -> 0x007a }
                r9.close()     // Catch:{ all -> 0x007a }
                com.portfolio.platform.service.notification.DianaNotificationComponent$Fi$Bii r0 = new com.portfolio.platform.service.notification.DianaNotificationComponent$Fi$Bii     // Catch:{ all -> 0x007a }
                java.lang.String r1 = "number"
                com.mapped.Wg6.b(r4, r1)     // Catch:{ all -> 0x007a }
                r1 = r10
                r0.<init>(r1, r2, r4, r5, r6)     // Catch:{ all -> 0x007a }
                r1 = 0
                com.fossil.So7.a(r9, r1)
            L_0x006e:
                return r0
            L_0x006f:
                r9.close()
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
                r0 = 0
                com.fossil.So7.a(r9, r0)
            L_0x0078:
                r0 = r8
                goto L_0x006e
            L_0x007a:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x007c }
            L_0x007c:
                r1 = move-exception
                com.fossil.So7.a(r9, r0)
                throw r1
            L_0x0081:
                r0 = move-exception
                r0.printStackTrace()
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.portfolio.platform.service.notification.DianaNotificationComponent r2 = com.portfolio.platform.service.notification.DianaNotificationComponent.this
                java.lang.String r2 = r2.s()
                java.lang.String r0 = r0.getMessage()
                r1.e(r2, r0)
                r0 = r8
                goto L_0x006e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.notification.DianaNotificationComponent.Fi.d():com.portfolio.platform.service.notification.DianaNotificationComponent$Fi$Bii");
        }

        @DexIgnore
        public final void e() {
            PermissionUtils.Ai ai = PermissionUtils.a;
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            Wg6.b(applicationContext, "PortfolioApp.instance.applicationContext");
            if (!ai.k(applicationContext)) {
                FLogger.INSTANCE.getLocal().d(DianaNotificationComponent.this.s(), "processMissedCall() is not executed because permissions has not granted");
            } else {
                Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Cii(this, null), 3, null);
            }
        }

        @DexIgnore
        public void onChange(boolean z) {
            e();
        }

        @DexIgnore
        public void onChange(boolean z, Uri uri) {
            e();
        }
    }

    @DexIgnore
    public enum Gi {
        RINGING,
        PICKED,
        MISSED,
        END
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi extends Ei {
        @DexIgnore
        public String n;
        @DexIgnore
        public boolean o;

        @DexIgnore
        public Hi() {
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public Hi(String str, String str2, String str3, String str4, Date date) {
            this();
            Wg6.c(str, "packageName");
            Wg6.c(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
            Wg6.c(str3, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            Wg6.c(str4, "message");
            Wg6.c(date, GoalPhase.COLUMN_START_DATE);
            w(b(date.getTime(), null));
            v(str);
            A(str2);
            x(str2);
            z(str4);
            B(date.getTime());
            this.n = str3;
        }

        @DexIgnore
        public final Hi C() {
            Hi hi = new Hi();
            hi.w(g());
            hi.t(d());
            hi.v(e());
            hi.x(h());
            hi.z(i());
            hi.A(j());
            hi.B(k());
            hi.s(l());
            hi.y(q());
            hi.u(o());
            hi.n = this.n;
            hi.o = this.o;
            return hi;
        }

        @DexIgnore
        public final boolean D() {
            return this.o;
        }

        @DexIgnore
        public final String E() {
            return this.n;
        }

        @DexIgnore
        public final void F(boolean z) {
            this.o = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii extends Ei {
        @DexIgnore
        public String n;

        @DexIgnore
        public Ii() {
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public Ii(String str, String str2, String str3, long j) {
            this();
            Wg6.c(str, RemoteFLogger.MESSAGE_SENDER_KEY);
            Wg6.c(str2, "message");
            Wg6.c(str3, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            w(b(j, null));
            v(DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName());
            A(str);
            x(str);
            z(str2);
            this.n = str3;
            B(j);
        }

        @DexIgnore
        public final String C() {
            return this.n;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji {
        @DexIgnore
        public String a;

        @DexIgnore
        public Ji() {
            this.a = "";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public Ji(String str, String str2) {
            this();
            Wg6.c(str, "packageName");
            Wg6.c(str2, "tag");
            this.a = str2;
        }

        @DexIgnore
        public final boolean a(String str) {
            Wg6.c(str, "realId");
            return Wt7.u(str, this.a, true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {459}, m = "checkConditionsToSendToDevice")
    public static final class Ki extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(DianaNotificationComponent dianaNotificationComponent, Xe6 xe6) {
            super(xe6);
            this.this$0 = dianaNotificationComponent;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.l(null, null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {390, 395, Action.ActivityTracker.TAG_ACTIVITY}, m = "handleNewLogic")
    public static final class Li extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Li(DianaNotificationComponent dianaNotificationComponent, Xe6 xe6) {
            super(xe6);
            this.this$0 = dianaNotificationComponent;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.t(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {338}, m = "handleNotificationAdded")
    public static final class Mi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Mi(DianaNotificationComponent dianaNotificationComponent, Xe6 xe6) {
            super(xe6);
            this.this$0 = dianaNotificationComponent;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$initializeInCalls$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
    public static final class Ni extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ni(DianaNotificationComponent dianaNotificationComponent, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaNotificationComponent;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ni ni = new Ni(this.this$0, xe6);
            ni.p$ = (Il6) obj;
            throw null;
            //return ni;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ni) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x009b, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x009c, code lost:
            com.fossil.So7.a(r1, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x009f, code lost:
            throw r2;
         */
        @DexIgnore
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
                r10 = this;
                com.fossil.Yn7.d()
                int r0 = r10.label
                if (r0 != 0) goto L_0x00c5
                com.fossil.El7.b(r11)
                com.portfolio.platform.service.notification.DianaNotificationComponent r0 = r10.this$0
                java.util.concurrent.ConcurrentHashMap r0 = com.portfolio.platform.service.notification.DianaNotificationComponent.a(r0)
                r0.clear()
                com.mapped.PermissionUtils$Ai r0 = com.mapped.PermissionUtils.a
                com.portfolio.platform.PortfolioApp$inner r1 = com.portfolio.platform.PortfolioApp.get
                com.portfolio.platform.PortfolioApp r1 = r1.instance()
                android.content.Context r1 = r1.getApplicationContext()
                java.lang.String r2 = "PortfolioApp.instance.applicationContext"
                com.mapped.Wg6.b(r1, r2)
                boolean r0 = r0.k(r1)
                if (r0 != 0) goto L_0x002d
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x002c:
                return r0
            L_0x002d:
                long r6 = java.lang.System.currentTimeMillis()
                com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get     // Catch:{ Exception -> 0x00a0 }
                com.portfolio.platform.PortfolioApp r0 = r0.instance()     // Catch:{ Exception -> 0x00a0 }
                android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x00a0 }
                android.net.Uri r1 = android.provider.CallLog.Calls.CONTENT_URI     // Catch:{ Exception -> 0x00a0 }
                r2 = 3
                java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x00a0 }
                r3 = 0
                java.lang.String r4 = "number"
                r2[r3] = r4     // Catch:{ Exception -> 0x00a0 }
                r3 = 1
                java.lang.String r4 = "date"
                r2[r3] = r4     // Catch:{ Exception -> 0x00a0 }
                r3 = 2
                java.lang.String r4 = "type"
                r2[r3] = r4     // Catch:{ Exception -> 0x00a0 }
                java.lang.String r3 = "date >= ? AND type IN(?,?)"
                r4 = 3
                java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00a0 }
                r5 = 0
                r8 = 900000(0xdbba0, double:4.44659E-318)
                long r6 = r6 - r8
                java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x00a0 }
                r4[r5] = r6     // Catch:{ Exception -> 0x00a0 }
                r5 = 1
                java.lang.String r6 = "1"
                r4[r5] = r6     // Catch:{ Exception -> 0x00a0 }
                r5 = 2
                java.lang.String r6 = "3"
                r4[r5] = r6     // Catch:{ Exception -> 0x00a0 }
                java.lang.String r5 = "date DESC"
                android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00a0 }
                if (r1 == 0) goto L_0x00b7
            L_0x0071:
                boolean r0 = r1.moveToNext()     // Catch:{ all -> 0x0099 }
                if (r0 == 0) goto L_0x00bb
                java.lang.String r0 = "number"
                int r0 = r1.getColumnIndex(r0)     // Catch:{ all -> 0x0099 }
                java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0099 }
                java.lang.String r2 = "date"
                int r2 = r1.getColumnIndex(r2)     // Catch:{ all -> 0x0099 }
                long r2 = r1.getLong(r2)     // Catch:{ all -> 0x0099 }
                com.portfolio.platform.service.notification.DianaNotificationComponent r4 = r10.this$0     // Catch:{ all -> 0x0099 }
                java.util.concurrent.ConcurrentHashMap r4 = com.portfolio.platform.service.notification.DianaNotificationComponent.a(r4)     // Catch:{ all -> 0x0099 }
                java.lang.Long r2 = com.fossil.Ao7.f(r2)     // Catch:{ all -> 0x0099 }
                r4.putIfAbsent(r0, r2)     // Catch:{ all -> 0x0099 }
                goto L_0x0071
            L_0x0099:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x009b }
            L_0x009b:
                r2 = move-exception
                com.fossil.So7.a(r1, r0)
                throw r2
            L_0x00a0:
                r0 = move-exception
                r0.printStackTrace()
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.portfolio.platform.service.notification.DianaNotificationComponent r2 = r10.this$0
                java.lang.String r2 = r2.s()
                java.lang.String r0 = r0.getMessage()
                r1.e(r2, r0)
            L_0x00b7:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
                goto L_0x002c
            L_0x00bb:
                r1.close()
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
                r0 = 0
                com.fossil.So7.a(r1, r0)
                goto L_0x00b7
            L_0x00c5:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.notification.DianaNotificationComponent.Ni.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

//    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
//    public static final class Oi extends Qq7 implements Gg6<AudioManager> {
//        @DexIgnore
//        public static /* final */ Oi INSTANCE; // = new Oi();
//
//        @DexIgnore
//        public Oi() {
//            super(0);
//        }
//
//        @DexIgnore
//        @Override // com.mapped.Gg6
//        public final AudioManager invoke() {
//            Object systemService = PortfolioApp.get.instance().getSystemService("audio");
//            if (systemService != null) {
//                return (AudioManager) systemService;
//            }
//            throw new Rc6("null cannot be cast to non-null type android.media.AudioManager");
//        }
//    }
//
//    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
//    public static final class Pi extends Qq7 implements Gg6<NotificationManager> {
//        @DexIgnore
//        public static /* final */ Pi INSTANCE; // = new Pi();
//
//        @DexIgnore
//        public Pi() {
//            super(0);
//        }
//
//        @DexIgnore
//        @Override // com.mapped.Gg6
//        public final NotificationManager invoke() {
//            Object systemService = PortfolioApp.get.instance().getSystemService("notification");
//            if (systemService != null) {
//                return (NotificationManager) systemService;
//            }
//            throw new Rc6("null cannot be cast to non-null type android.app.NotificationManager");
//        }
//    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$onNotificationAppRemoved$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
    public static final class Qi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ StatusBarNotification $sbn;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Qi(DianaNotificationComponent dianaNotificationComponent, StatusBarNotification statusBarNotification, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaNotificationComponent;
            this.$sbn = statusBarNotification;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Qi qi = new Qi(this.this$0, this.$sbn, xe6);
            qi.p$ = (Il6) obj;
            throw null;
            //return qi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Qi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                Ei ei = new Ei(this.$sbn);
                Ci ci = DianaNotificationComponent.q.a().get(ei.e());
                if (ci != null) {
                    ei.x(ci.a().getAppName());
                }
                if (this.this$0.G(this.$sbn)) {
                    this.this$0.S(new Ei(this.$sbn));
                } else {
                    this.this$0.T(new Ei(this.$sbn));
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processAppNotification$1", f = "DianaNotificationComponent.kt", l = {193}, m = "invokeSuspend")
    public static final class Ri extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ StatusBarNotification $sbn;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ri(DianaNotificationComponent dianaNotificationComponent, StatusBarNotification statusBarNotification, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaNotificationComponent;
            this.$sbn = statusBarNotification;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ri ri = new Ri(this.this$0, this.$sbn, xe6);
            ri.p$ = (Il6) obj;
            throw null;
            //return ri;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ri) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Ei ei = new Ei(this.$sbn);
                String str = this.$sbn.getNotification().category;
                DianaNotificationComponent dianaNotificationComponent = this.this$0;
                this.L$0 = il6;
                this.L$1 = ei;
                this.label = 1;
                if (dianaNotificationComponent.t(ei, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Ei ei2 = (Ei) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processPhoneCall$1", f = "DianaNotificationComponent.kt", l = {143, 169}, m = "invokeSuspend")
    public static final class Si extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $phoneNumber;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startTime;
        @DexIgnore
        public /* final */ /* synthetic */ Gi $state;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Si(DianaNotificationComponent dianaNotificationComponent, Gi gi, String str, Date date, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaNotificationComponent;
            this.$state = gi;
            this.$phoneNumber = str;
            this.$startTime = date;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Si si = new Si(this.this$0, this.$state, this.$phoneNumber, this.$startTime, xe6);
            si.p$ = (Il6) obj;
            throw null;
            //return si;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Si) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String s = this.this$0.s();
                local.d(s, "process phone call state " + this.$state + " phoneNumber " + this.$phoneNumber);
                int i2 = Wr5.a[this.$state.ordinal()];
                if (i2 == 1) {
                    String W = this.this$0.W(this.$phoneNumber);
                    Hi hi = new Hi(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), W, this.$phoneNumber, "Incoming Call", this.$startTime);
                    this.this$0.Z(this.$startTime.getTime());
                    hi.F(this.this$0.c.containsKey(this.$phoneNumber));
                    this.this$0.c.put(this.$phoneNumber, Ao7.f(this.$startTime.getTime()));
                    if (!this.this$0.y(this.$phoneNumber, true)) {
                        return Cd6.a;
                    }
                    DianaNotificationComponent dianaNotificationComponent = this.this$0;
                    Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                    Wg6.b(applicationContext, "PortfolioApp.instance.applicationContext");
                    dianaNotificationComponent.b0(applicationContext);
                    DianaNotificationComponent dianaNotificationComponent2 = this.this$0;
                    Context applicationContext2 = PortfolioApp.get.instance().getApplicationContext();
                    Wg6.b(applicationContext2, "PortfolioApp.instance.applicationContext");
                    dianaNotificationComponent2.X(applicationContext2);
                    Hi hi2 = this.this$0.h;
                    if (hi2 != null) {
                        this.this$0.T(hi2);
                    }
                    this.this$0.h = hi.C();
                    DianaNotificationComponent dianaNotificationComponent3 = this.this$0;
                    this.L$0 = il6;
                    this.L$1 = W;
                    this.L$2 = hi;
                    this.label = 1;
                    if (dianaNotificationComponent3.u(hi, this) == d) {
                        return d;
                    }
                } else if (i2 == 2) {
                    Hi hi3 = this.this$0.h;
                    if (hi3 != null && Wg6.a(hi3.E(), this.$phoneNumber)) {
                        Hi hi4 = new Hi(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), "", this.$phoneNumber, "", this.$startTime);
                        hi4.w(hi3.g());
                        hi4.t(hi3.d());
                        this.this$0.T(hi4);
                        this.this$0.h = null;
                    }
                    DianaNotificationComponent dianaNotificationComponent4 = this.this$0;
                    Context applicationContext3 = PortfolioApp.get.instance().getApplicationContext();
                    Wg6.b(applicationContext3, "PortfolioApp.instance.applicationContext");
                    dianaNotificationComponent4.b0(applicationContext3);
                } else if (i2 == 3) {
                    if (!this.this$0.y(this.$phoneNumber, true)) {
                        return Cd6.a;
                    }
                    Hi hi5 = this.this$0.h;
                    if (hi5 != null && Wg6.a(hi5.E(), this.$phoneNumber)) {
                        Hi hi6 = new Hi(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), "", this.$phoneNumber, "", this.$startTime);
                        hi6.t(hi5.d());
                        hi6.w(hi5.g());
                        this.this$0.T(hi6);
                        this.this$0.h = null;
                    }
                    String W2 = this.this$0.W(this.$phoneNumber);
                    DianaNotificationComponent dianaNotificationComponent5 = this.this$0;
                    String packageName = DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName();
                    String str = this.$phoneNumber;
                    Calendar instance = Calendar.getInstance();
                    Wg6.b(instance, "Calendar.getInstance()");
                    Date time = instance.getTime();
                    Wg6.b(time, "Calendar.getInstance().time");
                    Hi hi7 = new Hi(packageName, W2, str, "Missed Call", time);
                    this.L$0 = il6;
                    this.L$1 = W2;
                    this.label = 2;
                    if (dianaNotificationComponent5.u(hi7, this) == d) {
                        return d;
                    }
                }
                return Cd6.a;
            } else if (i == 1) {
                Hi hi8 = (Hi) this.L$2;
                String str2 = (String) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return Cd6.a;
            } else if (i == 2) {
                String str3 = (String) this.L$1;
                Il6 il63 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            DianaNotificationComponent dianaNotificationComponent6 = this.this$0;
            Context applicationContext4 = PortfolioApp.get.instance().getApplicationContext();
            Wg6.b(applicationContext4, "PortfolioApp.instance.applicationContext");
            dianaNotificationComponent6.b0(applicationContext4);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processSmsMms$1", f = "DianaNotificationComponent.kt", l = {182}, m = "invokeSuspend")
    public static final class Ti extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $content;
        @DexIgnore
        public /* final */ /* synthetic */ String $phoneNumber;
        @DexIgnore
        public /* final */ /* synthetic */ long $receivedTime;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ti(DianaNotificationComponent dianaNotificationComponent, String str, String str2, long j, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaNotificationComponent;
            this.$phoneNumber = str;
            this.$content = str2;
            this.$receivedTime = j;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ti ti = new Ti(this.this$0, this.$phoneNumber, this.$content, this.$receivedTime, xe6);
            ti.p$ = (Il6) obj;
            throw null;
            //return ti;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ti) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                FLogger.INSTANCE.getLocal().d(this.this$0.s(), "Process SMS/MMS by old solution");
                if (!this.this$0.y(this.$phoneNumber, false)) {
                    return Cd6.a;
                }
                String W = this.this$0.W(this.$phoneNumber);
                DianaNotificationComponent dianaNotificationComponent = this.this$0;
                Ii ii = new Ii(W, this.$content, this.$phoneNumber, this.$receivedTime);
                this.L$0 = il6;
                this.L$1 = W;
                this.label = 1;
                if (dianaNotificationComponent.u(ii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                String str = (String) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    /*
    static {
        HashMap<String, Ji> hashMap = new HashMap<>();
        hashMap.put("com.facebook.orca", new Ji("com.facebook.orca", "SMS"));
        p = hashMap;
    }
    */

    @DexIgnore
    public DianaNotificationComponent(An4 an4, DNDSettingsDatabase dNDSettingsDatabase, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(dNDSettingsDatabase, "mDNDndSettingsDatabase");
        Wg6.c(quickResponseRepository, "mQuickResponseRepository");
        Wg6.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.k = an4;
        this.l = dNDSettingsDatabase;
        this.m = quickResponseRepository;
        this.n = notificationSettingsDatabase;
        String simpleName = DianaNotificationComponent.class.getSimpleName();
        Wg6.b(simpleName, "DianaNotificationComponent::class.java.simpleName");
        this.a = simpleName;
        q.a();
    }

    @DexIgnore
    public final boolean A(String str) {
        List<ContactGroup> allContactGroups = Mn5.p.a().d().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups.isEmpty()) {
            return false;
        }
        for (ContactGroup contactGroup : allContactGroups) {
            Wg6.b(contactGroup, "contactGroup");
            List<Contact> contacts = contactGroup.getContacts();
            Wg6.b(contacts, "contactGroup.contacts");
            if (!contacts.isEmpty()) {
                Contact contact = contactGroup.getContacts().get(0);
                Wg6.b(contact, "contactGroup.contacts[0]");
                if (Vt7.j(contact.getDisplayName(), str, true)) {
                    return true;
                }
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        if (r0 != 2) goto L_0x0019;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean B(java.lang.String r7) {
        /*
            r6 = this;
            r2 = 1
            r1 = 0
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r0 = r6.n
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao r0 = r0.getNotificationSettingsDao()
            com.portfolio.platform.data.model.NotificationSettingsModel r0 = r0.getNotificationSettingsWithIsCallNoLiveData(r1)
            if (r0 == 0) goto L_0x004c
            int r0 = r0.getSettingsType()
            if (r0 == 0) goto L_0x0019
            if (r0 == r2) goto L_0x0047
            r3 = 2
            if (r0 == r3) goto L_0x001a
        L_0x0019:
            r1 = r2
        L_0x001a:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r6.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "isAllowMessageFromContactName() - from sender "
            r4.append(r5)
            r4.append(r7)
            java.lang.String r5 = " is "
            r4.append(r5)
            r4.append(r1)
            java.lang.String r5 = ", type = type = "
            r4.append(r5)
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r2.d(r3, r0)
            return r1
        L_0x0047:
            boolean r1 = r6.A(r7)
            goto L_0x001a
        L_0x004c:
            r0 = r1
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.notification.DianaNotificationComponent.B(java.lang.String):boolean");
    }

    @DexIgnore
    public final boolean C(Ei ei, Ci ci) {
        return ci.b() || ei.f() > -2;
    }

    @DexIgnore
    public final boolean D(Ei ei) {
        FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByCallDND()");
        Object systemService = PortfolioApp.get.instance().getSystemService("notification");
        if (systemService != null) {
            NotificationManager notificationManager = (NotificationManager) systemService;
            if ((notificationManager.getNotificationPolicy().priorityCategories & 8) == 0) {
                FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByCallDND() - Don't allow any calls");
                return true;
            }
            int i2 = notificationManager.getNotificationPolicy().priorityCallSenders;
            if (i2 == Math.abs(1)) {
                Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                Wg6.b(applicationContext, "PortfolioApp.instance.applicationContext");
                return !J(ei, o(applicationContext));
            } else if (i2 == Math.abs(2)) {
                Context applicationContext2 = PortfolioApp.get.instance().getApplicationContext();
                Wg6.b(applicationContext2, "PortfolioApp.instance.applicationContext");
                return !J(ei, r(applicationContext2));
            } else if (i2 != Math.abs(0)) {
                return true;
            } else {
                FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByCallDND() - Allow calls");
                return false;
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type android.app.NotificationManager");
        }
    }

    @DexIgnore
    public final boolean E(Ei ei) {
        FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByMessageDND()");
        Object systemService = PortfolioApp.get.instance().getSystemService("notification");
        if (systemService != null) {
            NotificationManager notificationManager = (NotificationManager) systemService;
            if ((notificationManager.getNotificationPolicy().priorityCategories & 4) == 0) {
                FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByMessageDND() - Don't allow any messages");
                return true;
            }
            int i2 = notificationManager.getNotificationPolicy().priorityMessageSenders;
            if (i2 == Math.abs(1)) {
                Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                Wg6.b(applicationContext, "PortfolioApp.instance.applicationContext");
                return !J(ei, o(applicationContext));
            } else if (i2 == Math.abs(2)) {
                Context applicationContext2 = PortfolioApp.get.instance().getApplicationContext();
                Wg6.b(applicationContext2, "PortfolioApp.instance.applicationContext");
                return !J(ei, r(applicationContext2));
            } else if (i2 != Math.abs(0)) {
                return true;
            } else {
                FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByMessageDND() - Allow messages");
                return false;
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type android.app.NotificationManager");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0116, code lost:
        if (r4 >= r2) goto L_0x0118;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean F() {
        /*
        // Method dump skipped, instructions count: 325
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.notification.DianaNotificationComponent.F():boolean");
    }

    @DexIgnore
    public final boolean G(StatusBarNotification statusBarNotification) {
        if (Build.VERSION.SDK_INT < 26) {
            return Wg6.a(statusBarNotification.getPackageName(), "com.android.server.telecom");
        }
        if (statusBarNotification.getGroupKey() == null) {
            return false;
        }
        String groupKey = statusBarNotification.getGroupKey();
        Wg6.b(groupKey, "sbn.groupKey");
        return Wt7.v(groupKey, "MissedCall", false, 2, null);
    }

    @DexIgnore
    public final boolean H() {
        return p().getStreamVolume(5) == 0;
    }

    @DexIgnore
    public final boolean I() {
        return p().getStreamVolume(2) == 0;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0012  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean J(com.portfolio.platform.service.notification.DianaNotificationComponent.Ei r6, java.util.List<com.mapped.Lc6<java.lang.String, java.lang.String>> r7) {
        /*
            r5 = this;
            java.lang.String r2 = r6.h()
            java.lang.String r3 = r6.j()
            java.util.Iterator r4 = r7.iterator()
        L_0x000c:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x003f
            java.lang.Object r0 = r4.next()
            com.mapped.Lc6 r0 = (com.mapped.Lc6) r0
            java.lang.Object r1 = r0.getFirst()
            java.lang.String r1 = (java.lang.String) r1
            boolean r1 = com.mapped.Wg6.a(r1, r2)
            if (r1 != 0) goto L_0x0030
            java.lang.Object r0 = r0.getSecond()
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = com.mapped.Wg6.a(r0, r3)
            if (r0 == 0) goto L_0x000c
        L_0x0030:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r5.a
            java.lang.String r2 = "isSenderInContactList() - Found contact"
            r0.d(r1, r2)
            r0 = 1
        L_0x003e:
            return r0
        L_0x003f:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r5.a
            java.lang.String r2 = "isSenderInContactList() - Not found contact"
            r0.d(r1, r2)
            r0 = 0
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.notification.DianaNotificationComponent.J(com.portfolio.platform.service.notification.DianaNotificationComponent$Ei, java.util.List):boolean");
    }

    @DexIgnore
    public final void K() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "onDeviceConnected, notificationList = " + this.j);
        Queue<NotificationBaseObj> queue = this.j;
        Wg6.b(queue, "mDeviceNotifications");
        synchronized (queue) {
            for (NotificationBaseObj notificationBaseObj : this.j) {
                PortfolioApp instance = PortfolioApp.get.instance();
                String n2 = n();
                Wg6.b(notificationBaseObj, "notification");
                instance.h1(n2, notificationBaseObj);
            }
            Cd6 cd6 = Cd6.a;
        }
    }

    @DexIgnore
    public final Rm6 L(StatusBarNotification statusBarNotification) {
        Wg6.c(statusBarNotification, "sbn");
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Qi(this, statusBarNotification, null), 3, null);
    }

    @DexIgnore
    public final void M(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "onNotificationRemoved, id = " + i2);
        Y(i2);
    }

    @DexIgnore
    public final void N(int i2, boolean z) {
        NotificationBaseObj t;
        boolean z2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "onNotificationSentResult, id = " + i2 + ", isSuccess = " + z);
        if (z) {
            Queue<NotificationBaseObj> queue = this.j;
            Wg6.b(queue, "mDeviceNotifications");
            synchronized (queue) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = this.a;
                local2.d(str2, "onNotificationSentResult, to silent notification, id = " + i2);
                Queue<NotificationBaseObj> queue2 = this.j;
                Wg6.b(queue2, "mDeviceNotifications");
                Iterator<NotificationBaseObj> it = queue2.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    NotificationBaseObj next = it.next();
                    if (next.getUid() == i2) {
                        z2 = true;
//                        continue;
                    } else {
                        z2 = false;
//                        continue;
                    }
                    if (z2) {
                        t = next;
                        break;
                    }
                }
                NotificationBaseObj t2 = t;
                if (t2 != null) {
                    t2.toExistedNotification();
                    Cd6 cd6 = Cd6.a;
                }
            }
        }
    }

    @DexIgnore
    public final void O() {
        FLogger.INSTANCE.getLocal().d(this.a, "onServiceConnected");
        v();
    }

    @DexIgnore
    public final void P() {
        this.c.clear();
    }

    @DexIgnore
    public final void Q(Ei ei, NotificationBaseObj.ANotificationType aNotificationType) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "  Notification Posted: " + ei.e());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local2.d(str2, "  Id: " + ei.g());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = this.a;
        local3.d(str3, "  Sender: " + ei.h());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = this.a;
        local4.d(str4, "  Title: " + ei.j());
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = this.a;
        local5.d(str5, "  Text: " + ei.i());
        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
        String str6 = this.a;
        local6.d(str6, "  Summary: " + ei.q());
        ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
        String str7 = this.a;
        local7.d(str7, "  IsOngoing: " + ei.o());
        ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
        String str8 = this.a;
        local8.d(str8, "  When: " + ei.k());
        ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
        String str9 = this.a;
        local9.d(str9, "  Priority: " + ei.f());
        ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
        String str10 = this.a;
        StringBuilder sb = new StringBuilder();
        sb.append("  Type: ");
        sb.append(aNotificationType != null ? aNotificationType.name() : null);
        local10.d(str10, sb.toString());
        ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
        String str11 = this.a;
        local11.d(str11, "  IsDownloadEvent: " + ei.l());
    }

    @DexIgnore
    public final Rm6 R(StatusBarNotification statusBarNotification) {
        Wg6.c(statusBarNotification, "sbn");
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Ri(this, statusBarNotification, null), 3, null);
    }

    @DexIgnore
    public final void S(Ei ei) {
        synchronized (this) {
            Wg6.c(ei, "notification");
            for (Ei t : this.b.d(ei.i())) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.a;
                local.d(str, "sendNotificationFromQueue, found notification " + ((Object) t));
                throw null;
//                DianaNotificationObj dianaNotificationObj = new DianaNotificationObj(t.d(), NotificationBaseObj.ANotificationType.REMOVED, new DianaNotificationObj.AApplicationName("", ei.e(), "", NotificationBaseObj.ANotificationType.REMOVED), t.j(), t.h(), -1, t.i(), Hm7.i(NotificationBaseObj.ANotificationFlag.IMPORTANT), Long.valueOf(t.k()), null, null, 1536, null);
//                Y(t.d());
//                PortfolioApp.get.instance().h1(n(), dianaNotificationObj);
            }
        }
    }

    @DexIgnore
    public final void T(Ei ei) {
        synchronized (this) {
            Wg6.c(ei, "notification");
            FLogger.INSTANCE.getLocal().d(this.a, "processNotificationRemoved");
            for (Ei t : this.b.c(ei.g(), ei.e())) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.a;
                local.d(str, "sendNotificationFromQueue, found notification " + ((Object) t));
                throw null;
//                DianaNotificationObj dianaNotificationObj = new DianaNotificationObj(t.d(), NotificationBaseObj.ANotificationType.REMOVED, new DianaNotificationObj.AApplicationName("", ei.e(), "", NotificationBaseObj.ANotificationType.REMOVED), t.j(), t.h(), -1, t.i(), Hm7.i(NotificationBaseObj.ANotificationFlag.IMPORTANT), Long.valueOf(t.k()), null, null, 1536, null);
//                Y(t.d());
//                PortfolioApp.get.instance().h1(n(), dianaNotificationObj);
            }
        }
    }

    @DexIgnore
    public final Rm6 U(String str, Date date, Gi gi) {
        Wg6.c(str, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
        Wg6.c(date, SampleRaw.COLUMN_START_TIME);
        Wg6.c(gi, "state");
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Si(this, gi, str, date, null), 3, null);
    }

    @DexIgnore
    public final Rm6 V(String str, String str2, long j2) {
        Wg6.c(str, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
        Wg6.c(str2, "content");
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Ti(this, str, str2, j2, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00bf  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String W(java.lang.String r8) {
        /*
        // Method dump skipped, instructions count: 235
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.notification.DianaNotificationComponent.W(java.lang.String):java.lang.String");
    }

    @DexIgnore
    public final void X(Context context) {
        if (PermissionUtils.a.k(context)) {
            this.i = new Fi();
            ContentResolver contentResolver = context.getContentResolver();
            Uri uri = CallLog.Calls.CONTENT_URI;
            Fi fi = this.i;
            if (fi != null) {
                contentResolver.registerContentObserver(uri, false, fi);
                FLogger.INSTANCE.getLocal().d(this.a, "registerPhoneCallObserver success");
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void Y(int i2) {
        FLogger.INSTANCE.getLocal().d(this.a, "removeNotification");
        Queue<NotificationBaseObj> queue = this.j;
        Wg6.b(queue, "mDeviceNotifications");
        synchronized (queue) {
            Iterator<NotificationBaseObj> it = this.j.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().getUid() == i2) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = this.a;
                        local.d(str, "notification removed, id = " + i2);
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
            Cd6 cd6 = Cd6.a;
        }
    }

    @DexIgnore
    public final void Z(long j2) {
        Iterator<Map.Entry<String, Long>> it = this.c.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Long> next = it.next();
            Wg6.b(next, "iterator.next()");
            Long value = next.getValue();
            Wg6.b(value, "item.value");
            if (j2 - value.longValue() >= 900000) {
                it.remove();
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x019e  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0257  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a0(com.portfolio.platform.service.notification.DianaNotificationComponent.Ei r17, com.portfolio.platform.service.notification.DianaNotificationComponent.Ci r18, boolean r19, com.mapped.Xe6<? super com.mapped.Cd6> r20) {
        /*
        // Method dump skipped, instructions count: 622
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.notification.DianaNotificationComponent.a0(com.portfolio.platform.service.notification.DianaNotificationComponent$Ei, com.portfolio.platform.service.notification.DianaNotificationComponent$Ci, boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void b0(Context context) {
        FLogger.INSTANCE.getLocal().d(this.a, "unregisterPhoneCallObserver");
        try {
            Fi fi = this.i;
            if (fi != null) {
                context.getContentResolver().unregisterContentObserver(fi);
                this.i = null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.d(str, "unregisterPhoneCallObserver e=" + e2);
        }
    }

    @DexIgnore
    public final Bi k(Ei ei, Ci ci) {
        FLogger.INSTANCE.getLocal().d(this.a, "applyDND()");
        if (Build.VERSION.SDK_INT >= 23) {
            Object systemService = PortfolioApp.get.instance().getSystemService("notification");
            if (systemService != null) {
                NotificationManager notificationManager = (NotificationManager) systemService;
                int currentInterruptionFilter = notificationManager.getCurrentInterruptionFilter();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.a;
                local.d(str, "currentInterruptionFilter = " + currentInterruptionFilter);
                if (currentInterruptionFilter == 2) {
                    return (Wg6.a(ci.a().getPackageName(), DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) || Wg6.a(ci.a().getPackageName(), DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName())) ? ei instanceof Hi ? ((notificationManager.getNotificationPolicy().priorityCategories & 16) == 0 || !((Hi) ei).D()) ? !D(ei) ? !I() ? Bi.ACTIVE : Bi.SILENT : Bi.SILENT : !I() ? Bi.ACTIVE : Bi.SILENT : D(ei) ? Bi.SILENT : Bi.ACTIVE : (Wg6.a(ci.a().getPackageName(), DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName()) || ei.m()) ? !E(ei) ? !H() ? Bi.ACTIVE : Bi.SILENT : Bi.SILENT : Wg6.a(ei.c(), Alarm.TABLE_NAME) ? (w() || x()) ? Bi.SILENT : Bi.ACTIVE : Bi.SILENT;
                }
                if (currentInterruptionFilter == 3) {
                    return Bi.SILENT;
                }
                if (currentInterruptionFilter == 4) {
                    return (x() || !Wg6.a(ei.c(), Alarm.TABLE_NAME)) ? Bi.SILENT : Bi.ACTIVE;
                }
                if (Wg6.a(ci.a().getPackageName(), DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) || Wg6.a(ci.a().getPackageName(), DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName())) {
                    return I() ? Bi.SILENT : Bi.ACTIVE;
                }
                if (Wg6.a(ei.c(), Alarm.TABLE_NAME)) {
                    return x() ? Bi.SILENT : Bi.ACTIVE;
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type android.app.NotificationManager");
            }
        }
        return H() ? Bi.SILENT : Bi.ACTIVE;
    }

    @DexAdd
    public class AlelecSettingsListener implements SharedPreferences.OnSharedPreferenceChangeListener {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            Context ctx = PortfolioApp.get.instance().getApplicationContext();
            dndSettingEnabled = AlelecPrefs.getAndroidDND(ctx);
            dndHighSettingEnabled = AlelecPrefs.getAndroidDND_HIGH(ctx);
            emptyNotificationsEnabled = AlelecPrefs.getEmptyNotifications(ctx);
        }
    }

    @DexAdd
    @SuppressWarnings("WeakerAccess")
    public AlelecSettingsListener dndSettingsListener = null;
    @DexAdd
    @SuppressWarnings("WeakerAccess")
    public boolean dndSettingEnabled = false;
    @DexAdd
    @SuppressWarnings("WeakerAccess")
    public static boolean dndHighSettingEnabled = false;
    @DexAdd
    @SuppressWarnings("WeakerAccess")
    public boolean emptyNotificationsEnabled = false;

    @DexAdd
    public final /* synthetic */ Object l(Ei eVar, Ci cVar, boolean z, Xe6<? super Cd6> xe6) {
        // This should wrap the function which contains Flogger ... "sendToDevice() - isSupportedIcon "
        if (dndSettingsListener == null) {
            Context context = PortfolioApp.get.instance().getApplicationContext();
            dndSettingsListener = new AlelecSettingsListener();
            AlelecPrefs.registerAndroidDND(context, dndSettingsListener );
            AlelecPrefs.registerEmptyNotifications(context, dndSettingsListener );
            dndSettingEnabled = AlelecPrefs.getAndroidDND(context);
            dndHighSettingEnabled = AlelecPrefs.getAndroidDND_HIGH(context);
            emptyNotificationsEnabled = AlelecPrefs.getEmptyNotifications(context);
        }
        FLogger.INSTANCE.getLocal().v(this.a, "emptyNotificationsEnabled: " + emptyNotificationsEnabled + ", dndSettingEnabled: " + dndSettingEnabled + ", notif.dnd: " + eVar.dnd);

        if (eVar.dnd && dndSettingEnabled) {
            FLogger.INSTANCE.getLocal().v(this.a, "Skip this notification from " + eVar.c() + " as DND is enabled");
            return Cd6.a;
        }
        if (emptyNotificationsEnabled && TextUtils.isEmpty(eVar.f)) {
            // Allow sending notifications with empty message
            eVar.f = " ";
        }

        return l(eVar, cVar, z, xe6, null);
    }

    @DexEdit
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object l(com.portfolio.platform.service.notification.DianaNotificationComponent.Ei r6, com.portfolio.platform.service.notification.DianaNotificationComponent.Ci r7, boolean r8, com.mapped.Xe6<? super com.mapped.Cd6> r9, @DexIgnore Void tag) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.service.notification.DianaNotificationComponent.Ki
            if (r0 == 0) goto L_0x0038
            r0 = r9
            com.portfolio.platform.service.notification.DianaNotificationComponent$Ki r0 = (com.portfolio.platform.service.notification.DianaNotificationComponent.Ki) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0047
            if (r3 != r4) goto L_0x003f
            java.lang.Object r0 = r1.L$3
            com.portfolio.platform.service.notification.DianaNotificationComponent$Ei r0 = (com.portfolio.platform.service.notification.DianaNotificationComponent.Ei) r0
            boolean r0 = r1.Z$0
            java.lang.Object r0 = r1.L$2
            com.portfolio.platform.service.notification.DianaNotificationComponent$Ci r0 = (com.portfolio.platform.service.notification.DianaNotificationComponent.Ci) r0
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.service.notification.DianaNotificationComponent$Ei r0 = (com.portfolio.platform.service.notification.DianaNotificationComponent.Ei) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.service.notification.DianaNotificationComponent r0 = (com.portfolio.platform.service.notification.DianaNotificationComponent) r0
            com.fossil.El7.b(r2)
        L_0x0035:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0037:
            return r0
        L_0x0038:
            com.portfolio.platform.service.notification.DianaNotificationComponent$Ki r0 = new com.portfolio.platform.service.notification.DianaNotificationComponent$Ki
            r0.<init>(r5, r9)
            r1 = r0
            goto L_0x0014
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            com.fossil.El7.b(r2)
            com.portfolio.platform.service.notification.DianaNotificationComponent$Di r2 = r5.b
            com.portfolio.platform.service.notification.DianaNotificationComponent$Ei r2 = r2.a(r6)
            if (r2 == 0) goto L_0x0082
            boolean r3 = r5.F()
            if (r3 != 0) goto L_0x006c
            java.lang.String r3 = r2.h()
            boolean r3 = com.fossil.Vt7.l(r3)
            if (r3 == 0) goto L_0x006f
            java.lang.String r3 = r2.i()
            boolean r3 = com.fossil.Vt7.l(r3)
            if (r3 == 0) goto L_0x006f
        L_0x006c:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
            goto L_0x0037
        L_0x006f:
            r1.L$0 = r5
            r1.L$1 = r6
            r1.L$2 = r7
            r1.Z$0 = r8
            r1.L$3 = r2
            r1.label = r4
            java.lang.Object r1 = r5.a0(r2, r7, r8, r1)
            if (r1 != r0) goto L_0x0035
            goto L_0x0037
        L_0x0082:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r5.a
            java.lang.String r2 = "processNotificationAdd() - this notification existed."
            r0.d(r1, r2)
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.notification.DianaNotificationComponent.l(com.portfolio.platform.service.notification.DianaNotificationComponent$Ei, com.portfolio.platform.service.notification.DianaNotificationComponent$Ci, boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0002: ARITH  (r0v1 int) = (r3v0 int) / (60 int)), (':' char), (wrap: int : 0x0011: ARITH  (r0v3 int) = (r3v0 int) % (60 int))] */
    public final String m(int i2) {
        if (i2 < 0) {
            return "invalid time";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(i2 / 60);
        sb.append(':');
        sb.append(i2 % 60);
        return sb.toString();
    }

    @DexIgnore
    public final String n() {
        return PortfolioApp.get.instance().J();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x006a, code lost:
        if (r0 != null) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x006c, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x006f, code lost:
        return r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0070, code lost:
        if (r0 != null) goto L_0x006c;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0078  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.mapped.Lc6<java.lang.String, java.lang.String>> o(android.content.Context r9) {
        /*
            r8 = this;
            r6 = 0
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r8.a
            java.lang.String r2 = "getAllContacts()"
            r0.d(r1, r2)
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            android.content.ContentResolver r0 = r9.getContentResolver()     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            android.net.Uri r1 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            r2 = 3
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            r3 = 0
            java.lang.String r4 = "contact_id"
            r2[r3] = r4     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            r3 = 1
            java.lang.String r4 = "display_name"
            r2[r3] = r4     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            r3 = 2
            java.lang.String r4 = "data1"
            r2[r3] = r4     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            if (r0 == 0) goto L_0x0070
        L_0x0034:
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0057 }
            if (r1 == 0) goto L_0x0070
            java.lang.String r1 = "display_name"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x0057 }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0057 }
            java.lang.String r2 = "data1"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x0057 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0057 }
            com.mapped.Lc6 r3 = new com.mapped.Lc6     // Catch:{ Exception -> 0x0057 }
            r3.<init>(r1, r2)     // Catch:{ Exception -> 0x0057 }
            r7.add(r3)     // Catch:{ Exception -> 0x0057 }
            goto L_0x0034
        L_0x0057:
            r1 = move-exception
        L_0x0058:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x007f }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ all -> 0x007f }
            java.lang.String r3 = r8.a     // Catch:{ all -> 0x007f }
            java.lang.String r4 = r1.getMessage()     // Catch:{ all -> 0x007f }
            r2.e(r3, r4)     // Catch:{ all -> 0x007f }
            r1.printStackTrace()     // Catch:{ all -> 0x007f }
            if (r0 == 0) goto L_0x006f
        L_0x006c:
            r0.close()
        L_0x006f:
            return r7
        L_0x0070:
            if (r0 == 0) goto L_0x006f
            goto L_0x006c
        L_0x0073:
            r0 = move-exception
            r1 = r0
            r2 = r6
        L_0x0076:
            if (r2 == 0) goto L_0x007b
            r2.close()
        L_0x007b:
            throw r1
        L_0x007c:
            r1 = move-exception
            r0 = r6
            goto L_0x0058
        L_0x007f:
            r1 = move-exception
            r2 = r0
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.notification.DianaNotificationComponent.o(android.content.Context):java.util.List");
    }

    @DexIgnore
    public final AudioManager p() {
        return (AudioManager) this.d.getValue();
    }

    @DexIgnore
    public final NotificationManager q() {
        return (NotificationManager) this.e.getValue();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0072, code lost:
        if (r0 != null) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0074, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0077, code lost:
        return r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0078, code lost:
        if (r0 != null) goto L_0x0074;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0080  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.mapped.Lc6<java.lang.String, java.lang.String>> r(android.content.Context r10) {
        /*
            r9 = this;
            r6 = 0
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r9.a
            java.lang.String r2 = "getStaredContacts()"
            r0.d(r1, r2)
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            android.content.ContentResolver r0 = r10.getContentResolver()     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            android.net.Uri r1 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r2 = 3
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r3 = 0
            java.lang.String r4 = "contact_id"
            r2[r3] = r4     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r3 = 1
            java.lang.String r4 = "display_name"
            r2[r3] = r4     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r3 = 2
            java.lang.String r4 = "data1"
            r2[r3] = r4     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            java.lang.String r3 = "starred = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r5 = 0
            java.lang.String r8 = "1"
            r4[r5] = r8     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            if (r0 == 0) goto L_0x0078
        L_0x003c:
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x005f }
            if (r1 == 0) goto L_0x0078
            java.lang.String r1 = "display_name"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x005f }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x005f }
            java.lang.String r2 = "data1"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x005f }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x005f }
            com.mapped.Lc6 r3 = new com.mapped.Lc6     // Catch:{ Exception -> 0x005f }
            r3.<init>(r1, r2)     // Catch:{ Exception -> 0x005f }
            r7.add(r3)     // Catch:{ Exception -> 0x005f }
            goto L_0x003c
        L_0x005f:
            r1 = move-exception
        L_0x0060:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0087 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ all -> 0x0087 }
            java.lang.String r3 = r9.a     // Catch:{ all -> 0x0087 }
            java.lang.String r4 = r1.getMessage()     // Catch:{ all -> 0x0087 }
            r2.e(r3, r4)     // Catch:{ all -> 0x0087 }
            r1.printStackTrace()     // Catch:{ all -> 0x0087 }
            if (r0 == 0) goto L_0x0077
        L_0x0074:
            r0.close()
        L_0x0077:
            return r7
        L_0x0078:
            if (r0 == 0) goto L_0x0077
            goto L_0x0074
        L_0x007b:
            r0 = move-exception
            r1 = r0
            r2 = r6
        L_0x007e:
            if (r2 == 0) goto L_0x0083
            r2.close()
        L_0x0083:
            throw r1
        L_0x0084:
            r1 = move-exception
            r0 = r6
            goto L_0x0060
        L_0x0087:
            r1 = move-exception
            r2 = r0
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.notification.DianaNotificationComponent.r(android.content.Context):java.util.List");
    }

    @DexIgnore
    public final String s() {
        return this.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01cf  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object t(com.portfolio.platform.service.notification.DianaNotificationComponent.Ei r13, com.mapped.Xe6<? super com.mapped.Cd6> r14) {
        /*
        // Method dump skipped, instructions count: 697
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.notification.DianaNotificationComponent.t(com.portfolio.platform.service.notification.DianaNotificationComponent$Ei, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object u(com.portfolio.platform.service.notification.DianaNotificationComponent.Ei r14, com.mapped.Xe6<? super com.mapped.Cd6> r15) {
        /*
        // Method dump skipped, instructions count: 361
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.notification.DianaNotificationComponent.u(com.portfolio.platform.service.notification.DianaNotificationComponent$Ei, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public final Rm6 v() {
        return Gu7.d(Jv7.a(Bw7.b()), null, null, new Ni(this, null), 3, null);
    }

    @DexIgnore
    public final boolean w() {
        if ((q().getNotificationPolicy().priorityCategories & 32) == 0) {
            FLogger.INSTANCE.getLocal().d(this.a, "isAlarmBlockedByDND() - Don't allow any alarms");
            return true;
        }
        FLogger.INSTANCE.getLocal().d(this.a, "isAlarmBlockedByDND() - Allow alarms");
        return false;
    }

    @DexIgnore
    public final boolean x() {
        return p().getStreamVolume(4) == 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        if (r0 != 2) goto L_0x0019;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean y(java.lang.String r7, boolean r8) {
        /*
            r6 = this;
            r2 = 1
            r1 = 0
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r0 = r6.n
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao r0 = r0.getNotificationSettingsDao()
            com.portfolio.platform.data.model.NotificationSettingsModel r0 = r0.getNotificationSettingsWithIsCallNoLiveData(r8)
            if (r0 == 0) goto L_0x004c
            int r0 = r0.getSettingsType()
            if (r0 == 0) goto L_0x0019
            if (r0 == r2) goto L_0x0047
            r3 = 2
            if (r0 == r3) goto L_0x001a
        L_0x0019:
            r1 = r2
        L_0x001a:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r6.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "isAllowCallOrMessageEvent() - from sender "
            r4.append(r5)
            r4.append(r7)
            java.lang.String r5 = " is "
            r4.append(r5)
            r4.append(r1)
            java.lang.String r5 = " because type = type = "
            r4.append(r5)
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r2.d(r3, r0)
            return r1
        L_0x0047:
            boolean r1 = r6.z(r7)
            goto L_0x001a
        L_0x004c:
            r0 = r1
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.notification.DianaNotificationComponent.y(java.lang.String, boolean):boolean");
    }

    @DexIgnore
    public final boolean z(String str) {
        List<ContactGroup> allContactGroups = Mn5.p.a().d().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups.isEmpty()) {
            return false;
        }
        for (ContactGroup contactGroup : allContactGroups) {
            if (contactGroup.getContactWithPhoneNumber(str) != null) {
                return true;
            }
        }
        return false;
    }
}
