package com.fossil;

import android.content.Context;
import com.google.gson.Gson;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.AccessGroup;
import com.portfolio.platform.manager.SoLibraryLoader;
import java.nio.charset.StandardCharsets;
import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public class Ng5 {
    @DexIgnore
    public static Ng5 b;
    @DexIgnore
    public Gson a; // = new Gson();

    @DexReplace
    public static Ng5 a() {
        if (b == null) {
            synchronized (Ng5.class) {
                try {
                    if (b == null) {
                        b = new Ng5();
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return b;
    }


    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0013, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r0 = a(r2, 4);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    @DexReplace
    public Access a(Context context) {
        Access access;
        synchronized (this) {
            try {
                access = a(context, 0);
            } catch (Exception e) {
                e.printStackTrace();
                return new Access(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            }
        }
        return access;
    }


    @DexReplace
    public final Access a(Context context, int i) throws Exception {
        PortfolioApp.inject_key_for = Thread.currentThread();
        int i2;
        Random random = new Random();
        SoLibraryLoader a2 = SoLibraryLoader.a();
        int nextInt = random.nextInt(1000);
        while (true) {
            i2 = nextInt + 1;
            if (SoLibraryLoader.a().a(i2)) {
                break;
            }
            nextInt = random.nextInt(1000);
        }
        String str = new String(a2.getData(i2, i), StandardCharsets.UTF_8);
        String str2 = new String(a2.find(context, i2), StandardCharsets.UTF_8);
        String str3 = a2.a(str, i2);
        String str4 = a2.a(str2, i2);
        String str5 = null;
        try {
            str5 = a2.a(str3, str4.trim());
        } catch (Exception e) {
            e.printStackTrace();
            PortfolioApp.inject_key_for = null;
            return new Access(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }
        AccessGroup accessGroup = (AccessGroup) this.a.a(str5, AccessGroup.class);
        PortfolioApp.inject_key_for = null;
        if (accessGroup == null) {
            return new Access(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }
        if (i != 0) {
            if (i == 1) {
                return accessGroup.getOreo();
            }
            if (i != 4) {
                return accessGroup.getLollipop();
            }
        }
        return accessGroup.getPie();
    }
}
