package com.fossil;

import com.mapped.Qg6;
import com.mapped.Xj6;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum N60 {
    UNKNOWN(false, Yp0.f.i()),
    DIANA(false, Yp0.f.b()),
    SE1(true, Yp0.f.g()),
    SLIM(true, Yp0.f.h()),
    MINI(true, Yp0.f.e()),
    HELLAS(false, Yp0.f.c()),
    SE0(true, Yp0.f.g()),
    WEAR_OS(false, new Type[0]),
    IVY(false, Yp0.f.b());
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ Type[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final N60 a(String str) {
            throw null;
//            return !M60.t.a(str) ? N60.UNKNOWN : Xj6.c(str, "D", false, 2, null) ? N60.DIANA : Xj6.c(str, "W", false, 2, null) ? N60.SE1 : Xj6.c(str, "L", false, 2, null) ? N60.SLIM : Xj6.c(str, "M", false, 2, null) ? N60.MINI : Xj6.c(str, "Y", false, 2, null) ? N60.HELLAS : Xj6.c(str, "Z", false, 2, null) ? N60.SE0 : Xj6.c(str, "V", false, 2, null) ? N60.IVY : N60.UNKNOWN;
        }

        @DexIgnore
        public final N60 a(String str, String str2) {
            throw null;
//            if (Xj6.c(str, "DN", false, 2, null)) {
//                return N60.DIANA;
//            }
//            if (!Xj6.c(str, "HW", false, 2, null)) {
//                return Xj6.c(str, "HL", false, 2, null) ? N60.SLIM : Xj6.c(str, "HM", false, 2, null) ? N60.MINI : Xj6.c(str, "AW", false, 2, null) ? N60.HELLAS : Xj6.c(str, "IV", false, 2, null) ? N60.IVY : a(str2);
//            }
//            N60 a = a(str2);
//            return a == N60.UNKNOWN ? N60.SE1 : a;
        }
    }

    @DexIgnore
    N60(boolean z, Type[] typeArr) {
        this.a = z;
        this.b = typeArr;
    }

    @DexIgnore
    public final Type[] a() {
        return this.b;
    }

    @DexIgnore
    public final boolean b() {
        return this.a;
    }
}
