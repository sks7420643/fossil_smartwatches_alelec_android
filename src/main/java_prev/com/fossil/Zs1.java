package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.fossil.fitness.AlgorithmManager;
import com.fossil.fitness.DataCaching;
import com.mapped.Cd6;
import com.mapped.Kc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.SoLibraryLoader;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class Zs1 {
    @DexIgnore
    public static long a; // = 1800000;
    @DexIgnore
    public static /* final */ Zs1 b; // = new Zs1();

    @DexIgnore
    public final long a(Bg0 bg0) {
        throw null;
//        int i = O71.a[bg0.ordinal()];
//        if (i == 1) {
//            return Wl0.h.c();
//        }
//        if (i == 2) {
//            return Yl1.h.c();
//        }
//        if (i == 3) {
//            return Xn1.h.c();
//        }
//        if (i == 4) {
//            return Ak1.h.c();
//        }
//        throw new Kc6();
    }

    @DexReplace
    public final void a(Context context, int i) throws IllegalArgumentException, IllegalStateException {
        Object obj;
        if (BluetoothAdapter.getDefaultAdapter() == null) {
            throw new IllegalStateException("Bluetooth is not supported on this hardware platform.");
        } else if (context == context.getApplicationContext()) {
            U31 u31 = U31.g;
            try {
                PortfolioApp.inject_key_for = Thread.currentThread();
                obj = SoLibraryLoader.a().a(context, i);
                U31.g.a(context);
                T11 t11 = T11.a;
                Zz0.d.a(context);
                BluetoothLeAdapter.k.a(context);
                System.loadLibrary("FitnessAlgorithm");
                System.loadLibrary("EllipticCurveCrypto");
                System.loadLibrary("EInkImageFilter");
                File file = new File(context.getFilesDir(), "msl");
                file.mkdirs();
                if (file.exists()) {
                    AlgorithmManager.defaultManager().setDataCaching(new DataCaching(file.getAbsolutePath(), new byte[0]));
                }
                AlgorithmManager defaultManager = AlgorithmManager.defaultManager();
                Wg6.a((Object) defaultManager, "AlgorithmManager.defaultManager()");
                defaultManager.setDebugEnabled(false);
                Wg6.a(obj, "isValid");
            } catch (Exception e) {
                obj = Cd6.a;
            } finally {
                PortfolioApp.inject_key_for = null;
            }
            u31.a(obj);
        } else {
            throw new IllegalArgumentException("Invalid application context.");
        }
    }

    @DexIgnore
    public final void a(Dh0 dh0) {
        U31.g.a(dh0);
    }

    @DexIgnore
    public final void a(String str) {
        U31.g.a(str);
        Yj0.e.a().c = str;
    }

    @DexIgnore
    public final boolean a() {
        return (Wg6.a(U31.g.f(), true) ^ true) || U31.g.a() != null;
    }

    @DexIgnore
    public final long b() {
        return a;
    }

    @DexIgnore
    public final String c() {
        return null;
    }

    @DexIgnore
    public final String d() {
        return null;
    }

    @DexIgnore
    public final String e() {
        return U31.g.e();
    }
}
