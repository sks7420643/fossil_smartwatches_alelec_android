package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import com.fossil.fitness.ActiveMinute;
import com.fossil.fitness.Calorie;
import com.fossil.fitness.Distance;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.GoalTracking;
import com.fossil.fitness.HeartRate;
import com.fossil.fitness.Resting;
import com.fossil.fitness.SleepSession;
import com.fossil.fitness.Step;
import com.fossil.fitness.WorkoutSession;
import com.fossil.fitness.WorkoutState;
import com.fossil.fitness.WorkoutType;
import com.mapped.Ej6;
import com.mapped.K70;
import com.mapped.Rc6;
import com.mapped.Rh6;
import com.mapped.S60;
import com.mapped.Wg6;
import com.mapped.Xj6;
import com.mapped.Yj6;
import io.flutter.plugin.common.StandardMessageCodec;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Yz0 {
    @DexIgnore
    public static final byte a(Set<? extends K70> set) {
        byte b = (byte) 0;
        Iterator<K70> it = (Iterator<K70>) set.iterator();
        while (it.hasNext()) {
            b = (byte) (it.next().b() | b);
        }
        return b;
    }

    @DexIgnore
    public static final double a(long j) {
        return ((double) j) / ((double) 1000);
    }

    @DexIgnore
    public static final float a(float f, int i) {
        float pow = (float) Math.pow((double) 10.0f, (double) i);
        return ((float) Rh6.b(f * pow)) / pow;
    }

    @DexIgnore
    public static final SharedPreferences a(J91 j91) {
        String name = j91.name();
        Context a = U31.g.a();
        if (a != null) {
            return a.getSharedPreferences(name, 0);
        }
        return null;
    }

    @DexIgnore
    public static final String a(byte b) {
        Qg7.a(16);
        String num = Integer.toString(b & 255, 16);
        Wg6.a((Object) num, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
        Locale e = B21.x.e();
        if (num != null) {
            String upperCase = num.toUpperCase(e);
            Wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase(locale)");
            return Yj6.a(upperCase, 2, '0');
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final String a(int i) {
        Qg7.a(16);
        String l = Long.toString(((long) i) & 4294967295L, 16);
        Wg6.a((Object) l, "java.lang.Long.toString(this, checkRadix(radix))");
        Locale e = B21.x.e();
        if (l != null) {
            String upperCase = l.toUpperCase(e);
            Wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase(locale)");
            return Yj6.a(upperCase, 8, '0');
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final String a(Enum<?> r2) {
        String name = r2.name();
        Locale e = B21.x.e();
        if (name != null) {
            String lowerCase = name.toLowerCase(e);
            Wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            return lowerCase;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final String a(String str) {
        if ((str.length() > 0) && Ph7.f(str) == ((char) 0)) {
            return str;
        }
        return str + (char) 0;
    }

    @DexIgnore
    public static /* synthetic */ String a(String str, int i, Charset charset, CodingErrorAction codingErrorAction, int i2) {
        if ((i2 & 2) != 0) {
            charset = Ej6.a;
        }
        if ((i2 & 4) != 0) {
            codingErrorAction = CodingErrorAction.IGNORE;
            Wg6.a((Object) codingErrorAction, "CodingErrorAction.IGNORE");
        }
        CharsetDecoder newDecoder = charset.newDecoder();
        Wg6.a((Object) newDecoder, "charset.newDecoder()");
        byte[] bytes = str.getBytes(charset);
        Wg6.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
        int min = Math.min(i, bytes.length);
        ByteBuffer wrap = ByteBuffer.wrap(bytes, 0, min);
        Wg6.a((Object) wrap, "ByteBuffer.wrap(stringByteArray, 0, lengthInByte)");
        CharBuffer allocate = CharBuffer.allocate(min);
        Wg6.a((Object) allocate, "CharBuffer.allocate(lengthInByte)");
        newDecoder.onMalformedInput(codingErrorAction);
        newDecoder.decode(wrap, allocate, true);
        newDecoder.flush(allocate);
        char[] array = allocate.array();
        Wg6.a((Object) array, "charBuffer.array()");
        return new String(array, 0, allocate.position());
    }

    @DexIgnore
    public static final String a(short s) {
        Qg7.a(16);
        String num = Integer.toString(65535 & s, 16);
        Wg6.a((Object) num, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
        Locale e = B21.x.e();
        if (num != null) {
            String upperCase = num.toUpperCase(e);
            Wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase(locale)");
            return Yj6.a(upperCase, 4, '0');
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static /* synthetic */ String a(byte[] bArr, String str, int i) {
        int i2 = 0;
        if ((i & 1) != 0) {
            str = "";
        }
        if (bArr.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        while (i2 < bArr.length - 1) {
            sb.append(a(bArr[i2]));
            sb.append(str);
            i2++;
        }
        if (i2 == bArr.length - 1) {
            sb.append(a(bArr[i2]));
        }
        String sb2 = sb.toString();
        Wg6.a((Object) sb2, "sb.toString()");
        return sb2;
    }

    @DexIgnore
    public static final <T> ArrayList<T> a(ArrayList<T> arrayList, ArrayList<T> arrayList2) {
        ArrayList<T> arrayList3 = new ArrayList<>(arrayList);
        arrayList3.addAll(arrayList2);
        return arrayList3;
    }

    @DexIgnore
    public static final JSONArray a(K60[] k60Arr) {
        JSONArray jSONArray = new JSONArray();
        for (K60 k60 : k60Arr) {
            jSONArray.put(k60.a());
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONArray a(Qk1[] qk1Arr) {
        JSONArray jSONArray = new JSONArray();
        for (Qk1 qk1 : qk1Arr) {
            jSONArray.put(qk1.a);
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONArray a(FitnessData[] fitnessDataArr) {
        ArrayList<Short> values;
        JSONArray jSONArray = new JSONArray();
        for (FitnessData fitnessData : fitnessDataArr) {
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray2 = new JSONArray();
            ArrayList<WorkoutSession> workouts = fitnessData.getWorkouts();
            if (workouts != null) {
                for (WorkoutSession t : workouts) {
                    JSONObject jSONObject2 = new JSONObject();
                    R51 r51 = R51.I4;
                    Wg6.a((Object) t, "item");
                    a(jSONObject2, r51, Long.valueOf(t.getId()));
                    a(jSONObject2, R51.V, Integer.valueOf(t.getStartTime()));
                    a(jSONObject2, R51.W, Integer.valueOf(t.getEndTime()));
                    a(jSONObject2, R51.R5, Integer.valueOf(t.getGpsDataPoints().size()));
                    jSONArray2.put(jSONObject2);
                }
            }
            JSONArray jSONArray3 = new JSONArray();
            ArrayList<SleepSession> sleeps = fitnessData.getSleeps();
            if (sleeps != null) {
                for (SleepSession t2 : sleeps) {
                    JSONObject jSONObject3 = new JSONObject();
                    R51 r512 = R51.V;
                    Wg6.a((Object) t2, "item");
                    a(jSONObject3, r512, Integer.valueOf(t2.getStartTime()));
                    a(jSONObject3, R51.W, Integer.valueOf(t2.getEndTime()));
                    jSONArray3.put(jSONObject3);
                }
            }
            a(jSONObject, R51.d0, jSONArray2);
            a(jSONObject, R51.c0, jSONArray3);
            a(jSONObject, R51.X, Integer.valueOf(fitnessData.getTimezoneOffsetInSecond()));
            R51 r513 = R51.b0;
            ActiveMinute activeMinute = fitnessData.getActiveMinute();
            a(jSONObject, r513, Integer.valueOf((activeMinute != null ? Integer.valueOf(activeMinute.getTotal()) : null).intValue()));
            R51 r514 = R51.Y;
            Distance distance = fitnessData.getDistance();
            a(jSONObject, r514, Double.valueOf((distance != null ? Double.valueOf(distance.getTotal()) : null).doubleValue()));
            a(jSONObject, R51.W, Integer.valueOf(fitnessData.getEndTime()));
            a(jSONObject, R51.V, Integer.valueOf(fitnessData.getStartTime()));
            R51 r515 = R51.a0;
            Step step = fitnessData.getStep();
            a(jSONObject, r515, Integer.valueOf((step != null ? Integer.valueOf(step.getTotal()) : null).intValue()));
            R51 r516 = R51.Z;
            Calorie calorie = fitnessData.getCalorie();
            a(jSONObject, r516, Integer.valueOf((calorie != null ? Integer.valueOf(calorie.getTotal()) : null).intValue()));
            R51 r517 = R51.e0;
            HeartRate heartrate = fitnessData.getHeartrate();
            a(jSONObject, r517, (heartrate == null || (values = heartrate.getValues()) == null) ? JSONObject.NULL : Integer.valueOf(values.size()));
            R51 r518 = R51.f0;
            ArrayList<Resting> resting = fitnessData.getResting();
            a(jSONObject, r518, Integer.valueOf((resting != null ? Integer.valueOf(resting.size()) : null).intValue()));
            R51 r519 = R51.g0;
            ArrayList<GoalTracking> goals = fitnessData.getGoals();
            a(jSONObject, r519, Integer.valueOf((goals != null ? Integer.valueOf(goals.size()) : null).intValue()));
            jSONArray.put(jSONObject);
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONArray a(S60[] s60Arr) {
        JSONArray jSONArray = new JSONArray();
        for (S60 s60 : s60Arr) {
            jSONArray.put(s60.b());
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONArray a(String[] strArr) {
        JSONArray jSONArray = new JSONArray();
        for (String str : strArr) {
            jSONArray.put(str);
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONArray a(UUID[] uuidArr) {
        JSONArray jSONArray = new JSONArray();
        for (UUID uuid : uuidArr) {
            jSONArray.put(uuid.toString());
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONObject a(JSONObject jSONObject, R51 r51, Object obj) {
        JSONObject put = null;
        try {
            put = jSONObject.put(a(r51), obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Wg6.a((Object) put, "this.put(key.lowerCaseName, value)");
        return put;
    }

    @DexIgnore
    public static final JSONObject a(JSONObject jSONObject, JSONObject jSONObject2) {
        JSONObject jSONObject3 = new JSONObject();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            try {
                jSONObject3.putOpt(next, jSONObject.get(next));
            } catch (JSONException e) {
                Wl0.h.a(e);
            }
        }
        Iterator<String> keys2 = jSONObject2.keys();
        while (keys2.hasNext()) {
            String next2 = keys2.next();
            try {
                jSONObject3.putOpt(next2, jSONObject2.get(next2));
            } catch (JSONException e2) {
                Wl0.h.a(e2);
            }
        }
        return jSONObject3;
    }

    @DexIgnore
    public static /* synthetic */ JSONObject a(JSONObject jSONObject, JSONObject jSONObject2, boolean z, int i) {
        if ((i & 2) != 0) {
            z = true;
        }
        Iterator<String> keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            if (z || !jSONObject.has(next)) {
                try {
                    jSONObject.putOpt(next, jSONObject2.get(next));
                } catch (JSONException e) {
                    Wl0.h.a(e);
                }
            }
        }
        return jSONObject;
    }

    @DexIgnore
    public static final byte[] a(byte[] bArr, byte[] bArr2) {
        if (bArr2.length == 0) {
            byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
            Wg6.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
            return copyOf;
        }
        byte[] bArr3 = new byte[(bArr.length + bArr2.length)];
        System.arraycopy(bArr, 0, bArr3, 0, bArr.length);
        System.arraycopy(bArr2, 0, bArr3, bArr.length, bArr2.length);
        return bArr3;
    }

    @DexIgnore
    public static final byte[][] a(byte[] bArr, int i) {
        if (i <= 0) {
            return new byte[][]{bArr};
        }
        ArrayList arrayList = new ArrayList();
        Jf7 a = Qf7.a(Qf7.d(0, bArr.length), i);
        int first = a.getFirst();
        int last = a.getLast();
        int a2 = a.a();
        if (a2 < 0 ? first >= last : first <= last) {
            while (true) {
                arrayList.add(S97.a(bArr, first, Math.min(first + i, bArr.length)));
                if (first == last) {
                    break;
                }
                first += a2;
            }
        }
        Object[] array = arrayList.toArray(new byte[0][]);
        if (array != null) {
            return (byte[][]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public static final int b(short s) {
        return s < 0 ? s + 65536 : s;
    }

    @DexIgnore
    public static final long b(int i) {
        long j = (long) i;
        return i < 0 ? j + 4294967296L : j;
    }

    @DexIgnore
    public static final WorkoutState b(String str) {
        if (Xj6.b(str, "started", true)) {
            return WorkoutState.START;
        }
        if (Xj6.b(str, "paused", true)) {
            return WorkoutState.PAUSE;
        }
        if (Xj6.b(str, "resumed", true)) {
            return WorkoutState.RESUME;
        }
        if (Xj6.b(str, "end", true)) {
            return WorkoutState.END;
        }
        return null;
    }

    @DexIgnore
    public static final short b(byte b) {
        return b < 0 ? (short) (b + StandardMessageCodec.NULL) : (short) b;
    }

    @DexIgnore
    public static final byte[] b(byte[] bArr, int i) {
        long b = b(i);
        for (int length = bArr.length - 1; length >= 0; length--) {
            long b2 = ((long) b(bArr[length])) + b;
            long j = (long) 256;
            b = b2 / j;
            bArr[length] = (byte) ((byte) ((int) (b2 % j)));
            if (b == 0) {
                break;
            }
        }
        return bArr;
    }

    @DexIgnore
    public static final WorkoutType c(int i) {
        WorkoutType workoutType;
        WorkoutType[] values = WorkoutType.values();
        int length = values.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                workoutType = null;
                break;
            }
            workoutType = values[i2];
            if (workoutType.getValue() == i) {
                break;
            }
            i2++;
        }
        return workoutType != null ? workoutType : WorkoutType.UNKNOWN;
    }
}
