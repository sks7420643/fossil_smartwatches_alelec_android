package com.fossil;

import com.facebook.internal.BoltsMeasurementEventListener;
import com.facebook.internal.FetchedAppGateKeepersManager;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wr1 extends K60 {
    @DexIgnore
    public long a;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ Ci1 e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public String j;
    @DexIgnore
    public M60 k;
    @DexIgnore
    public Sn0 l;
    @DexIgnore
    public JSONObject m;

    @DexIgnore
    public /* synthetic */ Wr1(String str, Ci1 ci1, String str2, String str3, String str4, boolean z, String str5, M60 m60, Sn0 sn0, JSONObject jSONObject, int i2) {
        throw null;
//        str5 = (i2 & 64) != 0 ? "" : str5;
//        m60 = (i2 & 128) != 0 ? new M60("", str2, "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262136) : m60;
//        Sn0 sn02 = (i2 & 256) != 0 ? new Sn0("", "", "", 0, "", null, 32) : sn0;
//        jSONObject = (i2 & 512) != 0 ? new JSONObject() : jSONObject;
//        String str6 = str2 + str4 + str3 + str + Yz0.a(ci1);
//        this.a = System.currentTimeMillis();
//        this.d = str;
//        this.e = ci1;
//        this.f = str2;
//        this.g = str3;
//        this.h = str4;
//        this.i = z;
//        this.j = str5;
//        this.k = m60;
//        this.l = sn02;
//        this.m = jSONObject;
//        this.c = Yh0.a("UUID.randomUUID().toString()");
    }

    @DexIgnore
    @Override // com.fossil.K60
    public JSONObject a() {
        throw null;
//        JSONObject put = new JSONObject().put("timestamp", Yz0.a(this.a)).put("line_number", this.b);
//        Wg6.a((Object) put, "JSONObject().put(LogEntr\u2026.LINE_NUMBER, lineNumber)");
//        put.put("phase_uuid", this.h).put("phase_name", this.g).put("entry_uuid", this.c).put(BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, this.d).put("type", Yz0.a(this.e)).put("is_success", this.i).put("value", this.m).put("user_id", this.l.c).put(Constants.SERIAL_NUMBER, this.k.getSerialNumber()).put("model_number", this.k.getModelNumber()).put(Constants.FIRMWARE_VERSION, this.k.getFirmwareVersion()).put("phone_model", this.l.b).put("os", this.l.f).put(Constants.OS_VERSION, this.l.a).put(FetchedAppGateKeepersManager.APPLICATION_SDK_VERSION, this.l.e).put("session_uuid", this.j).put(LegacySecondTimezoneSetting.COLUMN_TIMEZONE_OFFSET, this.l.d);
//        return put;
    }

    @DexIgnore
    public final String getMacAddress() {
        return this.f;
    }
}
