package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.welcome.CompatibleModelsActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rv6 extends BaseFragment implements Qv6, View.OnClickListener {
    @DexIgnore
    public Pv6 f;
    @DexIgnore
    public Qw6<G75> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ G75 a;

        @DexIgnore
        public b(G75 g75) {
            this.a = g75;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            FlexibleButton flexibleButton = this.a.r;
            Wg6.a((Object) flexibleButton, "it.fbGetStarted");
            flexibleButton.setEnabled(z);
            this.a.r.a(z ? "flexible_button_secondary" : "flexible_button_disabled");
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public Rv6() {
        String b2 = ThemeManager.l.a().b("nonBrandBlack");
        this.h = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
    }

    @DexIgnore
    @Override // com.fossil.Qv6
    public void K0() {
        CompatibleModelsActivity.a aVar = CompatibleModelsActivity.y;
        Context requireContext = requireContext();
        Wg6.a((Object) requireContext, "requireContext()");
        aVar.a(requireContext);
    }

    @DexIgnore
    @Override // com.fossil.Qv6
    public void R0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            SignUpActivity.a aVar = SignUpActivity.D;
            Wg6.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qv6
    public void Z() {
        Bx6 bx6 = Bx6.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.a((Object) childFragmentManager, "childFragmentManager");
        bx6.e(childFragmentManager);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qv6
    public void a(Spanned spanned) {
        FlexibleTextView flexibleTextView;
        Wg6.b(spanned, "message");
        if (isActive()) {
            Qw6<G75> qw6 = this.g;
            if (qw6 != null) {
                G75 a2 = qw6.a();
                if (a2 != null && (flexibleTextView = a2.u) != null) {
                    flexibleTextView.setText(spanned);
                    return;
                }
                return;
            }
            Wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(Pv6 pv6) {
        Wg6.b(pv6, "presenter");
        this.f = pv6;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean e1() {
        return false;
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131362256) {
                Pv6 pv6 = this.f;
                if (pv6 != null) {
                    pv6.h();
                } else {
                    Wg6.d("mPresenter");
                    throw null;
                }
            } else if (id == 2131362708) {
                Pv6 pv62 = this.f;
                if (pv62 != null) {
                    pv62.i();
                } else {
                    Wg6.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.b(layoutInflater, "inflater");
        Qw6<G75> qw6 = new Qw6<>(this, (G75) Qb.a(layoutInflater, 2131558639, viewGroup, false, a1()));
        this.g = qw6;
        if (qw6 != null) {
            G75 a2 = qw6.a();
            if (a2 != null) {
                Wg6.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            Wg6.a();
            throw null;
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        ShimmerFrameLayout shimmerFrameLayout;
        super.onPause();
        Qw6<G75> qw6 = this.g;
        if (qw6 != null) {
            G75 a2 = qw6.a();
            if (!(a2 == null || (shimmerFrameLayout = a2.w) == null)) {
                shimmerFrameLayout.stopShimmerAnimation();
            }
            Pv6 pv6 = this.f;
            if (pv6 != null) {
                pv6.g();
                Jf5 c1 = c1();
                if (c1 != null) {
                    c1.a("");
                    return;
                }
                return;
            }
            Wg6.d("mPresenter");
            throw null;
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        ShimmerFrameLayout shimmerFrameLayout;
        super.onResume();
        Qw6<G75> qw6 = this.g;
        if (qw6 != null) {
            G75 a2 = qw6.a();
            if (!(a2 == null || (shimmerFrameLayout = a2.w) == null)) {
                shimmerFrameLayout.startShimmerAnimation();
            }
            Pv6 pv6 = this.f;
            if (pv6 != null) {
                pv6.f();
                Jf5 c1 = c1();
                if (c1 != null) {
                    c1.d();
                    return;
                }
                return;
            }
            Wg6.d("mPresenter");
            throw null;
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        Qw6<G75> qw6 = this.g;
        if (qw6 != null) {
            G75 a2 = qw6.a();
            if (a2 != null) {
                if (!Tm4.a.a().d()) {
                    ShimmerFrameLayout shimmerFrameLayout = a2.w;
                    Wg6.a((Object) shimmerFrameLayout, "it.shmNote");
                    shimmerFrameLayout.setVisibility(4);
                }
                a2.r.setOnClickListener(this);
                a2.v.setOnClickListener(this);
                if (Wg6.a((Object) Tm4.a.a().h(), (Object) "CN")) {
                    FlexibleCheckBox flexibleCheckBox = a2.q;
                    Wg6.a((Object) flexibleCheckBox, "it.cbTermsService");
                    flexibleCheckBox.setVisibility(0);
                    FlexibleTextView flexibleTextView = a2.u;
                    Wg6.a((Object) flexibleTextView, "it.ftvTermsService");
                    flexibleTextView.setVisibility(0);
                    FlexibleButton flexibleButton = a2.r;
                    Wg6.a((Object) flexibleButton, "it.fbGetStarted");
                    flexibleButton.setEnabled(false);
                    a2.r.a("flexible_button_disabled");
                    a2.u.setLinkTextColor(this.h);
                    FlexibleTextView flexibleTextView2 = a2.u;
                    Wg6.a((Object) flexibleTextView2, "it.ftvTermsService");
                    flexibleTextView2.setMovementMethod(new LinkMovementMethod());
                    a2.q.setOnCheckedChangeListener(new b(a2));
                } else {
                    FlexibleCheckBox flexibleCheckBox2 = a2.q;
                    Wg6.a((Object) flexibleCheckBox2, "it.cbTermsService");
                    flexibleCheckBox2.setVisibility(8);
                    FlexibleTextView flexibleTextView3 = a2.u;
                    Wg6.a((Object) flexibleTextView3, "it.ftvTermsService");
                    flexibleTextView3.setVisibility(8);
                    FlexibleButton flexibleButton2 = a2.r;
                    Wg6.a((Object) flexibleButton2, "it.fbGetStarted");
                    flexibleButton2.setEnabled(true);
                    a2.r.a("flexible_button_secondary");
                }
            }
            V("tutorial_view");
            return;
        }
        Wg6.d("mBinding");
        throw null;
    }
}
