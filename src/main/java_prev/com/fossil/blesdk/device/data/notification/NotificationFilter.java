package com.fossil.blesdk.device.data.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.B21;
import com.fossil.Bz0;
import com.fossil.Ik1;
import com.fossil.K60;
import com.fossil.Ng1;
import com.fossil.R51;
import com.fossil.S97;
import com.fossil.Ud7;
import com.fossil.Wl0;
import com.fossil.Yz0;
import com.mapped.NotificationHandMovingConfig;
import com.mapped.NotificationIconConfig;
import com.mapped.NotificationVibePattern;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationFilter extends K60 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public long a;
    @DexIgnore
    public byte b;
    @DexIgnore
    public String c;
    @DexIgnore
    public short d;
    @DexIgnore
    public NotificationHandMovingConfig e;
    @DexIgnore
    public NotificationIconConfig f;
    @DexIgnore
    public NotificationVibePattern g;
    @DexIgnore
    public /* final */ String h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<NotificationFilter> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public NotificationFilter createFromParcel(Parcel parcel) {
            return new NotificationFilter(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public NotificationFilter[] newArray(int i) {
            return new NotificationFilter[i];
        }
    }

    /*
    static {
        Ud7 ud7 = Ud7.a;
    }
    */

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ NotificationFilter(Parcel parcel, Qg6 qg6) {
//        this(r0);
        String readString = parcel.readString();
        if (readString != null) {
            Wg6.a((Object) readString, "parcel.readString()!!");
            this.a = parcel.readLong();
            this.b = parcel.readByte();
            String readString2 = parcel.readString();
            m73setSender(readString2 == null ? "" : readString2);
            m72setPriority((short) parcel.readInt());
            this.e = (NotificationHandMovingConfig) parcel.readParcelable(NotificationHandMovingConfig.class.getClassLoader());
            this.g = (NotificationVibePattern) parcel.readSerializable();
            this.f = (NotificationIconConfig) parcel.readParcelable(NotificationIconConfig.class.getClassLoader());
            return;
        }
        Wg6.a();
        throw null;
    }

    @DexIgnore
    public NotificationFilter(String str) {
        this.h = str;
        Ik1 ik1 = Ik1.a;
        Charset c2 = B21.x.c();
        if (str != null) {
            byte[] bytes = str.getBytes(c2);
            Wg6.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            throw null;
//            this.a = ik1.a(S97.a(bytes, (byte) 0), Ng1.CRC32);
//            this.c = "";
//            this.d = (short) 255;
//            return;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public NotificationFilter(String str, NotificationHandMovingConfig notificationHandMovingConfig, NotificationVibePattern notificationVibePattern) {
        this(str);
        this.e = notificationHandMovingConfig;
        this.g = notificationVibePattern;
    }

    @DexIgnore
    @Override // com.fossil.K60
    public JSONObject a() {
        JSONObject jSONObject = null;
        JSONObject jSONObject2 = new JSONObject();
        throw null;
//        try {
//            Yz0.a(jSONObject2, R51.V2, this.h);
//            Yz0.a(jSONObject2, R51.U2, Yz0.a((int) this.a));
//            Yz0.a(jSONObject2, R51.S, Byte.valueOf(this.b));
//            Yz0.a(jSONObject2, R51.h, this.c);
//            Yz0.a(jSONObject2, R51.P, Short.valueOf(this.d));
//            R51 r51 = R51.z2;
//            NotificationHandMovingConfig notificationHandMovingConfig = this.e;
//            Yz0.a(jSONObject2, r51, notificationHandMovingConfig != null ? notificationHandMovingConfig.a() : null);
//            R51 r512 = R51.A2;
//            NotificationVibePattern notificationVibePattern = this.g;
//            Yz0.a(jSONObject2, r512, notificationVibePattern != null ? Byte.valueOf(notificationVibePattern.a()) : null);
//            R51 r513 = R51.Z2;
//            NotificationIconConfig notificationIconConfig = this.f;
//            if (notificationIconConfig != null) {
//                jSONObject = notificationIconConfig.a();
//            }
//            Yz0.a(jSONObject2, r513, jSONObject);
//        } catch (JSONException e2) {
//            Wl0.h.a(e2);
//        }
//        return jSONObject2;
    }

    @DexIgnore
    public final byte[] a(byte b2, byte[] bArr) {
        if (bArr.length + 2 > 100) {
            return new byte[0];
        }
        byte[] array = ByteBuffer.allocate(bArr.length + 2).order(ByteOrder.LITTLE_ENDIAN).put(b2).put((byte) bArr.length).put(bArr).array();
        Wg6.a((Object) array, "ByteBuffer.allocate(HEAD\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final byte[] b() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte b2 = Bz0.c.a;
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.a).array();
        Wg6.a((Object) array, "ByteBuffer.allocate(4)\n \u2026                 .array()");
        throw null;
//        byteArrayOutputStream.write(a(b2, array));
//        byteArrayOutputStream.write(a(Bz0.d.a, new byte[]{this.b}));
//        if (this.c.length() > 0) {
//            byte b3 = Bz0.b.a;
//            String a2 = Yz0.a(this.c);
//            Charset c2 = B21.x.c();
//            if (a2 != null) {
//                byte[] bytes = a2.getBytes(c2);
//                Wg6.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
//                byteArrayOutputStream.write(a(b3, bytes));
//            } else {
//                throw new Rc6("null cannot be cast to non-null type java.lang.String");
//            }
//        }
//        short s = this.d;
//        if (s != ((short) -1)) {
//            byteArrayOutputStream.write(a(Bz0.f.a, new byte[]{(byte) s}));
//        }
//        NotificationHandMovingConfig notificationHandMovingConfig = this.e;
//        if (notificationHandMovingConfig != null) {
//            byteArrayOutputStream.write(a(Bz0.g.a, notificationHandMovingConfig.b()));
//        }
//        NotificationVibePattern notificationVibePattern = this.g;
//        if (notificationVibePattern != null) {
//            byteArrayOutputStream.write(a(Bz0.h.a, new byte[]{notificationVibePattern.a()}));
//        }
//        NotificationIconConfig notificationIconConfig = this.f;
//        if (notificationIconConfig != null) {
//            byteArrayOutputStream.write(a(Bz0.e.a, notificationIconConfig.c()));
//        }
//        byte[] byteArray = byteArrayOutputStream.toByteArray();
//        byte[] array2 = ByteBuffer.allocate(byteArray.length + 2).order(ByteOrder.LITTLE_ENDIAN).putShort((short) byteArray.length).put(byteArray).array();
//        Wg6.a((Object) array2, "ByteBuffer\n             \u2026\n                .array()");
//        return array2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(NotificationFilter.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            NotificationFilter notificationFilter = (NotificationFilter) obj;
            throw null;
//            return this.a == notificationFilter.a && this.b == notificationFilter.b && !(Wg6.a(this.c, notificationFilter.c) ^ true) && this.d == notificationFilter.d && !(Wg6.a(this.e, notificationFilter.e) ^ true) && this.g == notificationFilter.g && !(Wg6.a(this.f, notificationFilter.f) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.NotificationFilter");
    }

    @DexIgnore
    public final long getAppBundleCrc() {
        return this.a;
    }

    @DexIgnore
    public final String getAppPackageName() {
        return this.h;
    }

    @DexIgnore
    public final NotificationHandMovingConfig getHandMovingConfig() {
        return this.e;
    }

    @DexIgnore
    public final NotificationIconConfig getIconConfig() {
        return this.f;
    }

    @DexIgnore
    public final short getPriority() {
        return this.d;
    }

    @DexIgnore
    public final String getSender() {
        return this.c;
    }

    @DexIgnore
    public final NotificationVibePattern getVibePattern() {
        return this.g;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((Long.valueOf(this.a).hashCode() * 31) + this.b) * 31) + this.c.hashCode()) * 31) + this.d;
        NotificationHandMovingConfig notificationHandMovingConfig = this.e;
        if (notificationHandMovingConfig != null) {
            hashCode = (hashCode * 31) + notificationHandMovingConfig.hashCode();
        }
        NotificationVibePattern notificationVibePattern = this.g;
        if (notificationVibePattern != null) {
            hashCode = (hashCode * 31) + notificationVibePattern.hashCode();
        }
        NotificationIconConfig notificationIconConfig = this.f;
        return notificationIconConfig != null ? (hashCode * 31) + notificationIconConfig.hashCode() : hashCode;
    }

    @DexIgnore
    public final NotificationFilter setHandMovingConfig(NotificationHandMovingConfig notificationHandMovingConfig) {
        this.e = notificationHandMovingConfig;
        return this;
    }

    @DexIgnore
    public final NotificationFilter setIconConfig(NotificationIconConfig notificationIconConfig) {
        this.f = notificationIconConfig;
        return this;
    }

    @DexIgnore
    public final NotificationFilter setPriority(short s) {
        Ud7 ud7 = Ud7.a;
        if (s < 0 || 255 < s) {
            s = -1;
        }
        this.d = (short) s;
        return this;
    }

    @DexIgnore
    /* renamed from: setPriority  reason: collision with other method in class */
    public final void m72setPriority(short s) {
        Ud7 ud7 = Ud7.a;
        if (s < 0 || 255 < s) {
            s = -1;
        }
        this.d = (short) s;
    }

    @DexIgnore
    public final NotificationFilter setSender(String str) {
        this.c = Yz0.a(str, 97, null, null, 6);
        return this;
    }

    @DexIgnore
    /* renamed from: setSender  reason: collision with other method in class */
    public final void m73setSender(String str) {
        this.c = Yz0.a(str, 97, null, null, 6);
    }

    @DexIgnore
    public final NotificationFilter setVibePatternConfig(NotificationVibePattern notificationVibePattern) {
        this.g = notificationVibePattern;
        return this;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.h);
        }
        if (parcel != null) {
            parcel.writeLong(this.a);
        }
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
        if (parcel != null) {
            parcel.writeSerializable(this.g);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.f, i);
        }
    }
}
