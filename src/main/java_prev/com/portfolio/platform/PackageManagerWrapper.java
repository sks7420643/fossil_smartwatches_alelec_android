package com.portfolio.platform;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ChangedPackages;
import android.content.pm.FeatureInfo;
import android.content.pm.InstrumentationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.SharedLibraryInfo;
import android.content.pm.Signature;
import android.content.pm.VersionedPackage;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.UserHandle;
import android.text.TextUtils;

import com.misfit.frameworks.buttonservice.log.FLogger;

import java.util.List;


@SuppressWarnings("deprecation")
public class PackageManagerWrapper extends PackageManager {
    private final PackageManager pm;

    public static String API_KEY;
    public static String NAME;

    public PackageManagerWrapper(PackageManager pm) {
        this.pm = pm;
    }

    @Override
    public ApplicationInfo getApplicationInfo(String packageName, int flags) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getApplicationInfo");
        ApplicationInfo ai = pm.getApplicationInfo(packageName, flags);
//        if (flags == PackageManager.GET_META_DATA && (!TextUtils.isEmpty(API_KEY))) {
//            ai.metaData.putString("com.google.android.geo.GOOGLE_API_KEY", API_KEY);
//            ai.metaData.putString("com.google.android.geo.API_KEY", API_KEY);
//            ai.metaData.putString("GOOGLE_WEB_SERVICE_KEY", API_KEY);
//        }
        return ai;
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public static String bytesToHex(byte[] bytes) {
        char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    @Override
    public PackageInfo getPackageInfo(String packageName, int flags) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().e("PackageManagerWrapper", "getPackageInfo");
//        packageName = "com.fossil.wearables.fossil";
        PackageInfo pi = pm.getPackageInfo(packageName, flags);
        FLogger.INSTANCE.getLocal().e("PackageManagerWrapper", bytesToHex(pi.signatures[0].toByteArray()));
        byte[] sig = hexStringToByteArray("308201B33082011CA00302010202045612E0E1300D06092A864886F70D0101050500301E310B3009060355040613025553310F300D060355040A1306466F7373696C301E170D3135313030353230343331335A170D3430303932383230343331335A301E310B3009060355040613025553310F300D060355040A1306466F7373696C30819F300D06092A864886F70D010101050003818D0030818902818100B3936C168CBB055F04DE6BEDCE9EAD60952E0DD8D00198DED7DB708B31C14F7AFCFD78C2AD91CB020CC2A1639A553CAA16AF996B036FACEE3D06175B702D7E16DCE7B0FCE709A7B7B3A08E39E12A4579B73DDB2FF6AB24E0E3E57108094A1E3369EEBD8A19F953E927BA3ED8B0DD48ED0FCB2A8C0423C574CABE0FE19881F32F0203010001300D06092A864886F70D0101050500038181003416EEBF0441B04FB012BE9926E48275AFE13B60517CA59B05F81E52B22F1BBA287C44446712E83FFF23C4186CFCC9ED368DD182DE06822D49A37E1EF96DD4AB083FE51318B0959569610E7B8E1994E27B13F996EEDB3FE6C0253105D25C4857A44A2BF94F74B3636D81F280788FD9D336862014949B7CBC7CB8B3A352BFAF71");
        Signature use = new Signature(sig);
        pi.signatures = new Signature[1];
        pi.signatures[0] = use;
        return pi;
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public PackageInfo getPackageInfo(VersionedPackage versionedPackage, int flags) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getPackageInfo");
        return pm.getPackageInfo(versionedPackage, flags);
    }

    @Override
    public String[] currentToCanonicalPackageNames(String[] names) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "currentToCanonicalPackageNames");
        return pm.currentToCanonicalPackageNames(names);
    }

    @Override
    public String[] canonicalToCurrentPackageNames(String[] names) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "canonicalToCurrentPackageNames");
        return pm.canonicalToCurrentPackageNames(names);
    }

    @Override
    public Intent getLaunchIntentForPackage(String packageName) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getLaunchIntentForPackage");
        return pm.getLaunchIntentForPackage(packageName);
    }

    @Override
    public Intent getLeanbackLaunchIntentForPackage(String packageName) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getLeanbackLaunchIntentForPackage");
        return pm.getLeanbackLaunchIntentForPackage(packageName);
    }

    @Override
    public int[] getPackageGids(String packageName) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getPackageGids");
        return pm.getPackageGids(packageName);
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public int[] getPackageGids(String packageName, int flags) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getPackageGids");
        return pm.getPackageGids(packageName, flags);
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public int getPackageUid(String packageName, int flags) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getPackageUid");
        return pm.getPackageUid(packageName, flags);
    }

    @Override
    public PermissionInfo getPermissionInfo(String name, int flags) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getPermissionInfo");
        return pm.getPermissionInfo(name, flags);
    }

    @Override
    public List<PermissionInfo> queryPermissionsByGroup(String group, int flags) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "queryPermissionsByGroup");
        return pm.queryPermissionsByGroup(group, flags);
    }

    @Override
    public PermissionGroupInfo getPermissionGroupInfo(String name, int flags) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getPermissionGroupInfo");
        return pm.getPermissionGroupInfo(name, flags);
    }

    @Override
    public List<PermissionGroupInfo> getAllPermissionGroups(int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getAllPermissionGroups");
        return pm.getAllPermissionGroups(flags);
    }

    @Override
    public ActivityInfo getActivityInfo(ComponentName component, int flags) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getActivityInfo");
        return pm.getActivityInfo(component, flags);
    }

    @Override
    public ActivityInfo getReceiverInfo(ComponentName component, int flags) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getReceiverInfo");
        return pm.getReceiverInfo(component, flags);
    }

    @Override
    public ServiceInfo getServiceInfo(ComponentName component, int flags) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getServiceInfo");
        return pm.getServiceInfo(component, flags);
    }

    @Override
    public ProviderInfo getProviderInfo(ComponentName component, int flags) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getProviderInfo");
        return pm.getProviderInfo(component, flags);
    }

    @Override
    public List<PackageInfo> getInstalledPackages(int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getInstalledPackages");
        return pm.getInstalledPackages(flags);
    }

    @Override
    public List<PackageInfo> getPackagesHoldingPermissions(String[] permissions, int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getPackagesHoldingPermissions");
        return pm.getPackagesHoldingPermissions(permissions, flags);
    }

    @Override
    public int checkPermission(String permName, String pkgName) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "checkPermission");
        return pm.checkPermission(permName, pkgName);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public boolean isPermissionRevokedByPolicy(String permName, String pkgName) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "isPermissionRevokedByPolicy");
        return pm.isPermissionRevokedByPolicy(permName, pkgName);
    }

    @Override
    public boolean addPermission(PermissionInfo info) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "addPermission");
        return pm.addPermission(info);
    }

    @Override
    public boolean addPermissionAsync(PermissionInfo info) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "addPermissionAsync");
        return pm.addPermissionAsync(info);
    }

    @Override
    public void removePermission(String name) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "removePermission");
        pm.removePermission(name);
    }

    @Override
    public int checkSignatures(String pkg1, String pkg2) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "checkSignatures");
        return pm.checkSignatures(pkg1, pkg2);
    }

    @Override
    public int checkSignatures(int uid1, int uid2) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "checkSignatures");
        return pm.checkSignatures(uid1, uid2);
    }

    @Override
    public String[] getPackagesForUid(int uid) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getPackagesForUid");
        return pm.getPackagesForUid(uid);
    }

    @Override
    public String getNameForUid(int uid) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getNameForUid");
        return pm.getNameForUid(uid);
    }

    @Override
    public List<ApplicationInfo> getInstalledApplications(int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getInstalledApplications");
        return pm.getInstalledApplications(flags);
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public boolean isInstantApp() {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "isInstantApp");
        return pm.isInstantApp();
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public boolean isInstantApp(String packageName) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "isInstantApp");
        return pm.isInstantApp(packageName);
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public int getInstantAppCookieMaxBytes() {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getInstantAppCookieMaxBytes");
        return pm.getInstantAppCookieMaxBytes();
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public byte[] getInstantAppCookie() {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getInstantAppCookie");
        return pm.getInstantAppCookie();
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public void clearInstantAppCookie() {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "clearInstantAppCookie");
        pm.clearInstantAppCookie();
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public void updateInstantAppCookie(byte[] cookie) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "updateInstantAppCookie");
        pm.updateInstantAppCookie(cookie);
    }

    @Override
    public String[] getSystemSharedLibraryNames() {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getSystemSharedLibraryNames");
        return pm.getSystemSharedLibraryNames();
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public List<SharedLibraryInfo> getSharedLibraries(int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getSharedLibraries");
        return pm.getSharedLibraries(flags);
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public ChangedPackages getChangedPackages(int sequenceNumber) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getChangedPackages");
        return pm.getChangedPackages(sequenceNumber);
    }

    @Override
    public FeatureInfo[] getSystemAvailableFeatures() {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getSystemAvailableFeatures");
        return pm.getSystemAvailableFeatures();
    }

    @Override
    public boolean hasSystemFeature(String name) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "hasSystemFeature");
        return pm.hasSystemFeature(name);
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public boolean hasSystemFeature(String name, int version) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "hasSystemFeature");
        return pm.hasSystemFeature(name, version);
    }

    @Override
    public ResolveInfo resolveActivity(Intent intent, int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "resolveActivity");
        return pm.resolveActivity(intent, flags);
    }

    @Override
    public List<ResolveInfo> queryIntentActivities(Intent intent, int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "queryIntentActivities");
        return pm.queryIntentActivities(intent, flags);
    }

    @Override
    public List<ResolveInfo> queryIntentActivityOptions(ComponentName caller, Intent[] specifics, Intent intent, int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "queryIntentActivityOptions");
        return pm.queryIntentActivityOptions(caller, specifics, intent, flags);
    }

    @Override
    public List<ResolveInfo> queryBroadcastReceivers(Intent intent, int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "queryBroadcastReceivers");
        return pm.queryBroadcastReceivers(intent, flags);
    }

    @Override
    public ResolveInfo resolveService(Intent intent, int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "resolveService");
        return pm.resolveService(intent, flags);
    }

    @Override
    public List<ResolveInfo> queryIntentServices(Intent intent, int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "queryIntentServices");
        return pm.queryIntentServices(intent, flags);
    }

    @Override
    public List<ResolveInfo> queryIntentContentProviders(Intent intent, int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "queryIntentContentProviders");
        return pm.queryIntentContentProviders(intent, flags);
    }

    @Override
    public ProviderInfo resolveContentProvider(String name, int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "resolveContentProvider");
        return pm.resolveContentProvider(name, flags);
    }

    @Override
    public List<ProviderInfo> queryContentProviders(String processName, int uid, int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "queryContentProviders");
        return pm.queryContentProviders(processName, uid, flags);
    }

    @Override
    public InstrumentationInfo getInstrumentationInfo(ComponentName className, int flags) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getInstrumentationInfo");
        return pm.getInstrumentationInfo(className, flags);
    }

    @Override
    public List<InstrumentationInfo> queryInstrumentation(String targetPackage, int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "queryInstrumentation");
        return pm.queryInstrumentation(targetPackage, flags);
    }

    @Override
    public Drawable getDrawable(String packageName, int resid, ApplicationInfo appInfo) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getDrawable");
        return pm.getDrawable(packageName, resid, appInfo);
    }

    @Override
    public Drawable getActivityIcon(ComponentName activityName) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getActivityIcon");
        return pm.getActivityIcon(activityName);
    }

    @Override
    public Drawable getActivityIcon(Intent intent) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getActivityIcon");
        return pm.getActivityIcon(intent);
    }

    @Override
    public Drawable getActivityBanner(ComponentName activityName) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getActivityBanner");
        return pm.getActivityBanner(activityName);
    }

    @Override
    public Drawable getActivityBanner(Intent intent) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getActivityBanner");
        return pm.getActivityBanner(intent);
    }

    @Override
    public Drawable getDefaultActivityIcon() {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getDefaultActivityIcon");
        return pm.getDefaultActivityIcon();
    }

    @Override
    public Drawable getApplicationIcon(ApplicationInfo info) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getApplicationIcon");
        return pm.getApplicationIcon(info);
    }

    @Override
    public Drawable getApplicationIcon(String packageName) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getApplicationIcon");
        return pm.getApplicationIcon(packageName);
    }

    @Override
    public Drawable getApplicationBanner(ApplicationInfo info) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getApplicationBanner");
        return pm.getApplicationBanner(info);
    }

    @Override
    public Drawable getApplicationBanner(String packageName) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getApplicationBanner");
        return pm.getApplicationBanner(packageName);
    }

    @Override
    public Drawable getActivityLogo(ComponentName activityName) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getActivityLogo");
        return pm.getActivityLogo(activityName);
    }

    @Override
    public Drawable getActivityLogo(Intent intent) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getActivityLogo");
        return pm.getActivityLogo(intent);
    }

    @Override
    public Drawable getApplicationLogo(ApplicationInfo info) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getApplicationLogo");
        return pm.getApplicationLogo(info);
    }

    @Override
    public Drawable getApplicationLogo(String packageName) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getApplicationLogo");
        return pm.getApplicationLogo(packageName);
    }

    @Override
    public Drawable getUserBadgedIcon(Drawable icon, UserHandle user) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getUserBadgedIcon");
        return pm.getUserBadgedIcon(icon, user);
    }

    @Override
    public Drawable getUserBadgedDrawableForDensity(Drawable drawable, UserHandle user, Rect badgeLocation, int badgeDensity) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getUserBadgedDrawableForDensity");
        return pm.getUserBadgedDrawableForDensity(drawable, user, badgeLocation, badgeDensity);
    }

    @Override
    public CharSequence getUserBadgedLabel(CharSequence label, UserHandle user) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getUserBadgedLabel");
        return pm.getUserBadgedLabel(label, user);
    }

    @Override
    public CharSequence getText(String packageName, int resid, ApplicationInfo appInfo) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getText");
        return pm.getText(packageName, resid, appInfo);
    }

    @Override
    public XmlResourceParser getXml(String packageName, int resid, ApplicationInfo appInfo) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getXml");
        return pm.getXml(packageName, resid, appInfo);
    }

    @Override
    public CharSequence getApplicationLabel(ApplicationInfo info) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getApplicationLabel");
        return pm.getApplicationLabel(info);
    }

    @Override
    public Resources getResourcesForActivity(ComponentName activityName) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getResourcesForActivity");
        return pm.getResourcesForActivity(activityName);
    }

    @Override
    public Resources getResourcesForApplication(ApplicationInfo app) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getResourcesForApplication");
        return pm.getResourcesForApplication(app);
    }

    @Override
    public Resources getResourcesForApplication(String appPackageName) throws NameNotFoundException {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getResourcesForApplication");
        return pm.getResourcesForApplication(appPackageName);
    }

    @Override
    public void verifyPendingInstall(int id, int verificationCode) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "verifyPendingInstall");
        pm.verifyPendingInstall(id, verificationCode);
    }

    @Override
    public void extendVerificationTimeout(int id, int verificationCodeAtTimeout, long millisecondsToDelay) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "extendVerificationTimeout");
        pm.extendVerificationTimeout(id, verificationCodeAtTimeout, millisecondsToDelay);
    }

    @Override
    public void setInstallerPackageName(String targetPackage, String installerPackageName) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "setInstallerPackageName");
        pm.setInstallerPackageName(targetPackage, installerPackageName);
    }

    @Override
    public String getInstallerPackageName(String packageName) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getInstallerPackageName");
        return pm.getInstallerPackageName(packageName);
    }

    @Override
    public void addPackageToPreferred(String packageName) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "addPackageToPreferred");
        pm.addPackageToPreferred(packageName);
    }

    @Override
    public void removePackageFromPreferred(String packageName) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "removePackageFromPreferred");
        pm.removePackageFromPreferred(packageName);
    }

    @Override
    public List<PackageInfo> getPreferredPackages(int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getPreferredPackages");
        return pm.getPreferredPackages(flags);
    }

    @Override
    public void addPreferredActivity(IntentFilter filter, int match, ComponentName[] set, ComponentName activity) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "addPreferredActivity");
        pm.addPreferredActivity(filter, match, set, activity);
    }

    @Override
    public void clearPackagePreferredActivities(String packageName) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "clearPackagePreferredActivities");
        pm.clearPackagePreferredActivities(packageName);
    }

    @Override
    public int getPreferredActivities(List<IntentFilter> outFilters, List<ComponentName> outActivities, String packageName) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getPreferredActivities");
        return pm.getPreferredActivities(outFilters, outActivities, packageName);
    }

    @Override
    public void setComponentEnabledSetting(ComponentName componentName, int newState, int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "setComponentEnabledSetting");
        pm.setComponentEnabledSetting(componentName, newState, flags);
    }

    @Override
    public int getComponentEnabledSetting(ComponentName componentName) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getComponentEnabledSetting");
        return pm.getComponentEnabledSetting(componentName);
    }

    @Override
    public void setApplicationEnabledSetting(String packageName, int newState, int flags) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "setApplicationEnabledSetting");
        pm.setApplicationEnabledSetting(packageName, newState, flags);
    }

    @Override
    public int getApplicationEnabledSetting(String packageName) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getApplicationEnabledSetting");
        return pm.getApplicationEnabledSetting(packageName);
    }

    @Override
    public boolean isSafeMode() {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "isSafeMode");
        return pm.isSafeMode();
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public void setApplicationCategoryHint(String packageName, int categoryHint) {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "setApplicationCategoryHint");
        pm.setApplicationCategoryHint(packageName, categoryHint);
    }

    @Override
    public PackageInstaller getPackageInstaller() {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "getPackageInstaller");
        return pm.getPackageInstaller();
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public boolean canRequestPackageInstalls() {
        FLogger.INSTANCE.getLocal().i("PackageManagerWrapper", "canRequestPackageInstalls");
        return pm.canRequestPackageInstalls();
    }
}
