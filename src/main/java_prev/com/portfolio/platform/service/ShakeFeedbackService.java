package com.portfolio.platform.service;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.De5;
import com.fossil.Ms3;
import com.fossil.Qj7;
import com.fossil.Rj5;
import com.fossil.Rs7;
import com.fossil.Tk7;
import com.fossil.Vh7;
import com.fossil.We7;
import com.fossil.Xh7;
import com.fossil.Yd5;
import com.fossil.Zb7;
import com.fossil.Zi7;
import com.fossil.Zs1;
import com.google.gson.Gson;
import com.mapped.TimeUtils;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Ej6;
import com.mapped.Ff6;
import com.mapped.Hf6;
import com.mapped.Hh6;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Nc6;
import com.mapped.PermissionUtils;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.db.HwLogProvider;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.ui.debug.LogcatActivity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class ShakeFeedbackService {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public WeakReference<Context> a;
    @DexIgnore
    public Rj5 b;
    @DexIgnore
    public Ms3 c;
    @DexIgnore
    public Ms3 d;
    @DexIgnore
    public String e;
    @DexIgnore
    public int f; // = -1;
    @DexIgnore
    public int g; // = -1;
    @DexIgnore
    public /* final */ UserRepository h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Rj5.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore
            public Aii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.a.f = 1;
                PermissionUtils.Ai ai = PermissionUtils.a;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj == null) {
                        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                    } else if (ai.c((Activity) obj, 123)) {
                        this.a.a.d();
                        Ms3 a2 = this.a.a.c;
                        if (a2 != null) {
                            a2.dismiss();
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$2$1", f = "ShakeFeedbackService.kt", l = {126, 127}, m = "invokeSuspend")
            public static final class Aiii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Bitmap $bitmap;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Bii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$2$1$1", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
                public static final class Aiiii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Aiii aiii, Xe6 xe6) {
                        super(2, xe6);
                        this.this$0 = aiii;
                    }

                    @DexIgnore
                    @Override // com.fossil.Ob7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.b(xe6, "completion");
                        Aiiii aiiii = new Aiiii(this.this$0, xe6);
                        aiiii.p$ = (Il6) obj;
                        throw null;
//                        return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                        throw null;
//                        return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Ob7
                    public final Object invokeSuspend(Object obj) {
                        Ff6.a();
                        if (this.label == 0) {
                            Nc6.a(obj);
                            Ms3 a = this.this$0.this$0.a.a.c;
                            if (a != null) {
                                a.dismiss();
                                return Cd6.a;
                            }
                            Wg6.a();
                            throw null;
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Bii bii, Bitmap bitmap, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = bii;
                    this.$bitmap = bitmap;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$bitmap, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
//                    return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
//                    return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Il6 il6;
                    Tk7 c;
                    Aiiii aiiii;
                    Object a = Ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        Nc6.a(obj);
                        il6 = this.p$;
                        ShakeFeedbackService shakeFeedbackService = this.this$0.a.a;
                        Bitmap bitmap = this.$bitmap;
                        this.L$0 = il6;
                        this.label = 1;
                        if (shakeFeedbackService.a(bitmap, this) == a) {
                            return a;
                        }
                        c = Qj7.c();
                        aiiii = new Aiiii(this, null);
                        this.L$0 = il6;
                        this.label = 2;
                        if (Vh7.a(c, aiiii, this) == a) {
                        }
                    } else if (i == 1) {
                        il6 = (Il6) this.L$0;
                        Nc6.a(obj);
                        c = Qj7.c();
                        aiiii = new Aiiii(this, null);
                        this.L$0 = il6;
                        this.label = 2;
                        if (Vh7.a(c, aiiii, this) == a) {
                            return a;
                        }
                    } else if (i == 2) {
                        Il6 il62 = (Il6) this.L$0;
                        Nc6.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            public Bii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                PermissionUtils.Ai ai = PermissionUtils.a;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj == null) {
                        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                    } else if (ai.c((Activity) obj, 123)) {
                        WeakReference b2 = this.a.a.a;
                        if (b2 != null) {
                            Object obj2 = b2.get();
                            if (obj2 != null) {
                                Window window = ((Activity) obj2).getWindow();
                                Wg6.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                                View decorView = window.getDecorView();
                                Wg6.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                                View rootView = decorView.getRootView();
                                Wg6.a((Object) rootView, "v1");
                                rootView.setDrawingCacheEnabled(true);
                                Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                                Wg6.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                                rootView.setDrawingCacheEnabled(false);
                                throw null;
//                                Rm6 unused = Xh7.b(Zi7.a(Qj7.b()), null, null, new Aiii(this, createBitmap, null), 3, null);
//                                return;
                            }
                            throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                        }
                        Wg6.a();
                        throw null;
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$3$1", f = "ShakeFeedbackService.kt", l = {142, 143}, m = "invokeSuspend")
            public static final class Aiii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Bitmap $bitmap;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Cii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$3$1$1", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
                public static final class Aiiii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Aiii aiii, Xe6 xe6) {
                        super(2, xe6);
                        this.this$0 = aiii;
                    }

                    @DexIgnore
                    @Override // com.fossil.Ob7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.b(xe6, "completion");
                        Aiiii aiiii = new Aiiii(this.this$0, xe6);
                        aiiii.p$ = (Il6) obj;
                        throw null;
//                        return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                        throw null;
//                        return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Ob7
                    public final Object invokeSuspend(Object obj) {
                        Ff6.a();
                        if (this.label == 0) {
                            Nc6.a(obj);
                            Ms3 a = this.this$0.this$0.a.a.c;
                            if (a != null) {
                                a.dismiss();
                                return Cd6.a;
                            }
                            Wg6.a();
                            throw null;
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Cii cii, Bitmap bitmap, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = cii;
                    this.$bitmap = bitmap;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$bitmap, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
//                    return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
//                    return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Il6 il6;
                    Tk7 c;
                    Aiiii aiiii;
                    Object a = Ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        Nc6.a(obj);
                        il6 = this.p$;
                        ShakeFeedbackService shakeFeedbackService = this.this$0.a.a;
                        Bitmap bitmap = this.$bitmap;
                        this.L$0 = il6;
                        this.label = 1;
                        if (shakeFeedbackService.a(bitmap, this) == a) {
                            return a;
                        }
                        c = Qj7.c();
                        aiiii = new Aiiii(this, null);
                        this.L$0 = il6;
                        this.label = 2;
                        if (Vh7.a(c, aiiii, this) == a) {
                        }
                    } else if (i == 1) {
                        il6 = (Il6) this.L$0;
                        Nc6.a(obj);
                        c = Qj7.c();
                        aiiii = new Aiiii(this, null);
                        this.L$0 = il6;
                        this.label = 2;
                        if (Vh7.a(c, aiiii, this) == a) {
                            return a;
                        }
                    } else if (i == 2) {
                        Il6 il62 = (Il6) this.L$0;
                        Nc6.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            public Cii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.a.f = 0;
                PermissionUtils.Ai ai = PermissionUtils.a;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj == null) {
                        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                    } else if (ai.c((Activity) obj, 123)) {
                        WeakReference b2 = this.a.a.a;
                        if (b2 != null) {
                            Object obj2 = b2.get();
                            if (obj2 != null) {
                                Window window = ((Activity) obj2).getWindow();
                                Wg6.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                                View decorView = window.getDecorView();
                                Wg6.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                                View rootView = decorView.getRootView();
                                Wg6.a((Object) rootView, "v1");
                                rootView.setDrawingCacheEnabled(true);
                                Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                                Wg6.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                                rootView.setDrawingCacheEnabled(false);
                                throw null;
//                                Rm6 unused = Xh7.b(Zi7.a(Qj7.b()), null, null, new Aiii(this, createBitmap, null), 3, null);
//                                return;
                            }
                            throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                        }
                        Wg6.a();
                        throw null;
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Dii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$4$1", f = "ShakeFeedbackService.kt", l = {160}, m = "invokeSuspend")
            public static final class Aiii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Bitmap $bitmap;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Dii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Dii dii, Bitmap bitmap, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = dii;
                    this.$bitmap = bitmap;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$bitmap, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
//                    return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
//                    return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = Ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        Nc6.a(obj);
                        Il6 il6 = this.p$;
                        ShakeFeedbackService shakeFeedbackService = this.this$0.a.a;
                        Bitmap bitmap = this.$bitmap;
                        this.L$0 = il6;
                        this.label = 1;
                        if (shakeFeedbackService.a(bitmap, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        Nc6.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            public Dii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.a.f = 1;
                PermissionUtils.Ai ai = PermissionUtils.a;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj == null) {
                        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                    } else if (ai.c((Activity) obj, 123)) {
                        WeakReference b2 = this.a.a.a;
                        if (b2 != null) {
                            Object obj2 = b2.get();
                            if (obj2 != null) {
                                Window window = ((Activity) obj2).getWindow();
                                Wg6.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                                View decorView = window.getDecorView();
                                Wg6.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                                View rootView = decorView.getRootView();
                                Wg6.a((Object) rootView, "v1");
                                rootView.setDrawingCacheEnabled(true);
                                if (rootView.getDrawingCache() != null) {
                                    Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                                    Wg6.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                                    rootView.setDrawingCacheEnabled(false);
                                    throw null;
//                                    Rm6 unused = Xh7.b(Zi7.a(Qj7.b()), null, null, new Aiii(this, createBitmap, null), 3, null);
                                }
                                Ms3 a2 = this.a.a.c;
                                if (a2 != null) {
                                    a2.dismiss();
                                } else {
                                    Wg6.a();
                                    throw null;
                                }
                            } else {
                                throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                            }
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Eii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$5$1", f = "ShakeFeedbackService.kt", l = {180}, m = "invokeSuspend")
            public static final class Aiii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Bitmap $bitmap;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Eii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Eii eii, Bitmap bitmap, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = eii;
                    this.$bitmap = bitmap;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$bitmap, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
//                    return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
//                    return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = Ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        Nc6.a(obj);
                        Il6 il6 = this.p$;
                        ShakeFeedbackService shakeFeedbackService = this.this$0.a.a;
                        Bitmap bitmap = this.$bitmap;
                        this.L$0 = il6;
                        this.label = 1;
                        if (shakeFeedbackService.a(bitmap, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        Nc6.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            public Eii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.a.f = 3;
                PermissionUtils.Ai ai = PermissionUtils.a;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj == null) {
                        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                    } else if (ai.c((Activity) obj, 123)) {
                        WeakReference b2 = this.a.a.a;
                        if (b2 != null) {
                            Object obj2 = b2.get();
                            if (obj2 != null) {
                                Window window = ((Activity) obj2).getWindow();
                                Wg6.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                                View decorView = window.getDecorView();
                                Wg6.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                                View rootView = decorView.getRootView();
                                Wg6.a((Object) rootView, "v1");
                                rootView.setDrawingCacheEnabled(true);
                                if (rootView.getDrawingCache() != null) {
                                    Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                                    Wg6.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                                    rootView.setDrawingCacheEnabled(false);
                                    throw null;
//                                    Rm6 unused = Xh7.b(Zi7.a(Qj7.b()), null, null, new Aiii(this, createBitmap, null), 3, null);
                                }
                                Ms3 a2 = this.a.a.c;
                                if (a2 != null) {
                                    a2.dismiss();
                                } else {
                                    Wg6.a();
                                    throw null;
                                }
                            } else {
                                throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                            }
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Fii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$6$1", f = "ShakeFeedbackService.kt", l = {200}, m = "invokeSuspend")
            public static final class Aiii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Bitmap $bitmap;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Fii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Fii fii, Bitmap bitmap, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = fii;
                    this.$bitmap = bitmap;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$bitmap, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
//                    return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
//                    return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = Ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        Nc6.a(obj);
                        Il6 il6 = this.p$;
                        ShakeFeedbackService shakeFeedbackService = this.this$0.a.a;
                        Bitmap bitmap = this.$bitmap;
                        this.L$0 = il6;
                        this.label = 1;
                        if (shakeFeedbackService.a(bitmap, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        Nc6.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            public Fii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.a.f = 2;
                PermissionUtils.Ai ai = PermissionUtils.a;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj == null) {
                        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                    } else if (ai.c((Activity) obj, 123)) {
                        WeakReference b2 = this.a.a.a;
                        if (b2 != null) {
                            Object obj2 = b2.get();
                            if (obj2 != null) {
                                Window window = ((Activity) obj2).getWindow();
                                Wg6.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                                View decorView = window.getDecorView();
                                Wg6.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                                View rootView = decorView.getRootView();
                                Wg6.a((Object) rootView, "v1");
                                rootView.setDrawingCacheEnabled(true);
                                if (rootView.getDrawingCache() != null) {
                                    Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                                    Wg6.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                                    rootView.setDrawingCacheEnabled(false);
                                    throw null;
//                                    Rm6 unused = Xh7.b(Zi7.a(Qj7.b()), null, null, new Aiii(this, createBitmap, null), 3, null);
                                }
                                Ms3 a2 = this.a.a.c;
                                if (a2 != null) {
                                    a2.dismiss();
                                } else {
                                    Wg6.a();
                                    throw null;
                                }
                            } else {
                                throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                            }
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Gii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$7$1", f = "ShakeFeedbackService.kt", l = {213}, m = "invokeSuspend")
            public static final class Aiii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Gii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Gii gii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = gii;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
//                    return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
//                    return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = Ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        Nc6.a(obj);
                        Il6 il6 = this.p$;
                        ShakeFeedbackService shakeFeedbackService = this.this$0.a.a;
                        this.L$0 = il6;
                        this.label = 1;
                        try {
                            if (shakeFeedbackService.a(this) == a) {
                                return a;
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        Nc6.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            public Gii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.a.f = 1;
                PermissionUtils.Ai ai = PermissionUtils.a;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj == null) {
                        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                    } else if (ai.c((Activity) obj, 123)) {
                        throw null;
//                        Rm6 unused = Xh7.b(Zi7.a(Qj7.b()), null, null, new Aiii(this, null), 3, null);
//                        Ms3 a2 = this.a.a.c;
//                        if (a2 != null) {
//                            a2.dismiss();
//                        } else {
//                            Wg6.a();
//                            throw null;
//                        }
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Hii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore
            public Hii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                DebugActivity.a aVar = DebugActivity.M;
                WeakReference b = this.a.a.a;
                if (b != null) {
                    Object obj = b.get();
                    if (obj != null) {
                        Wg6.a(obj, "contextWeakReference!!.get()!!");
                        aVar.a((Context) obj);
                        Ms3 a2 = this.a.a.c;
                        if (a2 != null) {
                            a2.dismiss();
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    } else {
                        Wg6.a();
                        throw null;
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public Bi(ShakeFeedbackService shakeFeedbackService) {
            this.a = shakeFeedbackService;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0137  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x014d  */
//        @Override // com.fossil.Rj5.Ai
        public final void a() {
            Ms3 a2;
            WeakReference b = this.a.a;
            if (b == null) {
                Wg6.a();
                throw null;
            } else if (De5.a((Context) b.get()) && !this.a.c()) {
                if (this.a.c != null) {
                    Ms3 a3 = this.a.c;
                    if (a3 != null) {
                        a3.dismiss();
                    } else {
                        Wg6.a();
                        throw null;
                    }
                }
                WeakReference b2 = this.a.a;
                if (b2 != null) {
                    Object obj = b2.get();
                    if (obj != null) {
                        Object systemService = ((Context) obj).getSystemService("layout_inflater");
                        if (systemService != null) {
                            View inflate = ((LayoutInflater) systemService).inflate(2131558461, (ViewGroup) null);
                            ShakeFeedbackService shakeFeedbackService = this.a;
                            WeakReference b3 = this.a.a;
                            if (b3 != null) {
                                Object obj2 = b3.get();
                                if (obj2 != null) {
                                    shakeFeedbackService.c = new Ms3((Context) obj2);
                                    Ms3 a4 = this.a.c;
                                    if (a4 != null) {
                                        a4.setContentView(inflate);
                                        View findViewById = inflate.findViewById(2131363351);
                                        if (findViewById != null) {
                                            ((TextView) findViewById).setText("4.5.0");
                                            inflate.findViewById(2131363054).setOnClickListener(new Aii(this));
                                            inflate.findViewById(2131363053).setOnClickListener(new Bii(this));
                                            inflate.findViewById(2131363050).setOnClickListener(new Cii(this));
                                            inflate.findViewById(2131363052).setOnClickListener(new Dii(this));
                                            inflate.findViewById(2131363049).setOnClickListener(new Eii(this));
                                            inflate.findViewById(2131363055).setOnClickListener(new Fii(this));
                                            inflate.findViewById(2131363051).setOnClickListener(new Gii(this));
                                            View findViewById2 = inflate.findViewById(2131362783);
                                            WeakReference b4 = this.a.a;
                                            if (b4 != null) {
                                                if (!(b4.get() instanceof DebugActivity)) {
                                                    WeakReference b5 = this.a.a;
                                                    if (b5 == null) {
                                                        Wg6.a();
                                                        throw null;
                                                    } else if (!(b5.get() instanceof LogcatActivity)) {
                                                        Wg6.a((Object) findViewById2, "llOpenDebug");
                                                        findViewById2.setVisibility(0);
                                                        inflate.findViewById(2131362871).setOnClickListener(new Hii(this));
                                                        a2 = this.a.c;
                                                        if (a2 == null) {
                                                            a2.show();
                                                            return;
                                                        } else {
                                                            Wg6.a();
                                                            throw null;
                                                        }
                                                    }
                                                }
                                                Wg6.a((Object) findViewById2, "llOpenDebug");
                                                findViewById2.setVisibility(8);
                                                inflate.findViewById(2131362871).setOnClickListener(new Hii(this));
                                                a2 = this.a.c;
                                                if (a2 == null) {
                                                }
                                            } else {
                                                Wg6.a();
                                                throw null;
                                            }
                                        } else {
                                            throw new Rc6("null cannot be cast to non-null type android.widget.TextView");
                                        }
                                    } else {
                                        Wg6.a();
                                        throw null;
                                    }
                                } else {
                                    Wg6.a();
                                    throw null;
                                }
                            } else {
                                Wg6.a();
                                throw null;
                            }
                        } else {
                            throw new Rc6("null cannot be cast to non-null type android.view.LayoutInflater");
                        }
                    } else {
                        Wg6.a();
                        throw null;
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService", f = "ShakeFeedbackService.kt", l = {481}, m = "sendFeedbackEmail")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$13;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(ShakeFeedbackService shakeFeedbackService, Xe6 xe6) {
            super(xe6);
            this.this$0 = shakeFeedbackService;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Context a;

        @DexIgnore
        public Di(Context context) {
            this.a = context;
        }

        @DexIgnore
        public final void run() {
            Toast.makeText(this.a, "Can't zip file!", 1).show();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService", f = "ShakeFeedbackService.kt", l = {FacebookRequestErrorClassification.EC_TOO_MANY_USER_ACTION_CALLS}, m = "sendHardwareLog")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(ShakeFeedbackService shakeFeedbackService, Xe6 xe6) {
            super(xe6);
            this.this$0 = shakeFeedbackService;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            throw null;
//            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$sendLog$2", f = "ShakeFeedbackService.kt", l = {83}, m = "invokeSuspend")
    public static final class Fi extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(ShakeFeedbackService shakeFeedbackService, Bitmap bitmap, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = shakeFeedbackService;
            this.$bitmap = bitmap;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Fi fi = new Fi(this.this$0, this.$bitmap, xe6);
            fi.p$ = (Il6) obj;
            throw null;
//            return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Object a = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il6 = this.p$;
                ShakeFeedbackService shakeFeedbackService = this.this$0;
                Bitmap bitmap = this.$bitmap;
                this.L$0 = il6;
                this.label = 1;
                if (shakeFeedbackService.a(bitmap, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                Nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$1$1", f = "ShakeFeedbackService.kt", l = {267, 268}, m = "invokeSuspend")
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Bitmap $bitmap;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$1$1$1", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
//                    return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
//                    return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Ff6.a();
                    if (this.label == 0) {
                        Nc6.a(obj);
                        Ms3 c = this.this$0.this$0.a.d;
                        if (c != null) {
                            c.dismiss();
                            return Cd6.a;
                        }
                        Wg6.a();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Gi gi, Bitmap bitmap, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = gi;
                this.$bitmap = bitmap;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$bitmap, xe6);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Il6 il6;
                Tk7 c;
                Aiii aiii;
                Object a = Ff6.a();
                int i = this.label;
                if (i == 0) {
                    Nc6.a(obj);
                    il6 = this.p$;
                    ShakeFeedbackService shakeFeedbackService = this.this$0.a;
                    Bitmap bitmap = this.$bitmap;
                    this.L$0 = il6;
                    this.label = 1;
                    if (shakeFeedbackService.a(bitmap, this) == a) {
                        return a;
                    }
                    c = Qj7.c();
                    aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 2;
                    throw null;
//                    if (Vh7.a(c, aiii, this) == a) {
//                    }
                } else if (i == 1) {
                    il6 = (Il6) this.L$0;
                    Nc6.a(obj);
                    c = Qj7.c();
                    aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 2;
                    throw null;
//                    if (Vh7.a(c, aiii, this) == a) {
//                        return a;
//                    }
                } else if (i == 2) {
                    Il6 il62 = (Il6) this.L$0;
                    Nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Gi(ShakeFeedbackService shakeFeedbackService) {
            this.a = shakeFeedbackService;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.g = 0;
            PermissionUtils.Ai ai = PermissionUtils.a;
            WeakReference b = this.a.a;
            if (b != null) {
                Object obj = b.get();
                if (obj == null) {
                    throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                } else if (ai.c((Activity) obj, 123)) {
                    WeakReference b2 = this.a.a;
                    if (b2 != null) {
                        Object obj2 = b2.get();
                        if (obj2 != null) {
                            Window window = ((Activity) obj2).getWindow();
                            Wg6.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                            View decorView = window.getDecorView();
                            Wg6.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                            View rootView = decorView.getRootView();
                            Wg6.a((Object) rootView, "v1");
                            rootView.setDrawingCacheEnabled(true);
                            Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                            Wg6.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                            rootView.setDrawingCacheEnabled(false);
                            throw null;
//                            Rm6 unused = Xh7.b(Zi7.a(Qj7.b()), null, null, new Aii(this, createBitmap, null), 3, null);
//                            return;
                        }
                        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                    }
                    Wg6.a();
                    throw null;
                }
            } else {
                Wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2$1", f = "ShakeFeedbackService.kt", l = {284, 285}, m = "invokeSuspend")
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Bitmap $bitmap;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Hi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$2$1$1", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
//                    return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
//                    return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Ff6.a();
                    if (this.label == 0) {
                        Nc6.a(obj);
                        Ms3 c = this.this$0.this$0.a.d;
                        if (c != null) {
                            c.dismiss();
                            return Cd6.a;
                        }
                        Wg6.a();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Hi hi, Bitmap bitmap, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = hi;
                this.$bitmap = bitmap;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$bitmap, xe6);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Il6 il6;
                Tk7 c;
                Aiii aiii;
                Object a = Ff6.a();
                int i = this.label;
                if (i == 0) {
                    Nc6.a(obj);
                    il6 = this.p$;
                    ShakeFeedbackService shakeFeedbackService = this.this$0.a;
                    Bitmap bitmap = this.$bitmap;
                    this.L$0 = il6;
                    this.label = 1;
                    if (shakeFeedbackService.a(bitmap, this) == a) {
                        return a;
                    }
                    c = Qj7.c();
                    aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 2;
                    throw null;
//                    if (Vh7.a(c, aiii, this) == a) {
//                    }
                } else if (i == 1) {
                    il6 = (Il6) this.L$0;
                    Nc6.a(obj);
                    c = Qj7.c();
                    aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 2;
                    throw null;
//                    if (Vh7.a(c, aiii, this) == a) {
//                        return a;
//                    }
                } else if (i == 2) {
                    Il6 il62 = (Il6) this.L$0;
                    Nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Hi(ShakeFeedbackService shakeFeedbackService) {
            this.a = shakeFeedbackService;
        }

        @DexIgnore
        public final void onClick(View view) {
            PermissionUtils.Ai ai = PermissionUtils.a;
            WeakReference b = this.a.a;
            if (b != null) {
                Object obj = b.get();
                if (obj == null) {
                    throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                } else if (ai.c((Activity) obj, 123)) {
                    WeakReference b2 = this.a.a;
                    if (b2 != null) {
                        Object obj2 = b2.get();
                        if (obj2 != null) {
                            Window window = ((Activity) obj2).getWindow();
                            Wg6.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                            View decorView = window.getDecorView();
                            Wg6.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                            View rootView = decorView.getRootView();
                            Wg6.a((Object) rootView, "v1");
                            rootView.setDrawingCacheEnabled(true);
                            Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                            Wg6.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                            rootView.setDrawingCacheEnabled(false);
                            throw null;
//                            Rm6 unused = Xh7.b(Zi7.a(Qj7.b()), null, null, new Aii(this, createBitmap, null), 3, null);
//                            return;
                        }
                        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                    }
                    Wg6.a();
                    throw null;
                }
            } else {
                Wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService", f = "ShakeFeedbackService.kt", l = {590, Action.Bolt.TURN_OFF}, m = "takeScreenShot")
    public static final class Ii extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(ShakeFeedbackService shakeFeedbackService, Xe6 xe6) {
            super(xe6);
            this.this$0 = shakeFeedbackService;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((Bitmap) null, this);
        }
    }

    /*
    static {
        new Ai(null);
        String simpleName = ShakeFeedbackService.class.getSimpleName();
        Wg6.a((Object) simpleName, "ShakeFeedbackService::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public ShakeFeedbackService(UserRepository userRepository) {
        String file;
        Wg6.b(userRepository, "mUserRepository");
        this.h = userRepository;
        if (b()) {
            file = Environment.getExternalStorageDirectory().toString();
        } else {
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            Wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            file = applicationContext.getFilesDir().toString();
        }
        this.e = file;
        this.e = Wg6.a(file, (Object) "/com.fossil.wearables.fossil/");
    }

    @DexReplace
    public final Uri a(String str) {
        if (Build.VERSION.SDK_INT < 24) {
            return Uri.parse("file://" + str);
        } else if (this.a == null || TextUtils.isEmpty(str)) {
            return null;
        } else {
            WeakReference<Context> weakReference = this.a;
            if (weakReference != null) {
                Context context = weakReference.get();
                if (context != null) {
                    return FileProvider.getUriForFile(context, context.getPackageName() + "%s.provider", new File(str));
                }
                Wg6.a();
                throw null;
            }
            Wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final File a(String str, File file) {
        File[] listFiles;
        File file2;
        if (file == null || (listFiles = file.listFiles()) == null) {
            return null;
        }
        if (!(!(listFiles.length == 0))) {
            return null;
        }
        FLogger.INSTANCE.getLocal().e(i, ".sendFeedbackEmail - files.length=" + listFiles.length);
        try {
            file2 = new File(Environment.getExternalStorageDirectory(), str + ".zip");
            try {
                ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(file2));
                for (File file3 : listFiles) {
                    byte[] bArr = new byte[1024];
                    FileInputStream fileInputStream = new FileInputStream(file3);
                    Wg6.a((Object) file3, "file");
                    zipOutputStream.putNextEntry(new ZipEntry(file3.getName()));
                    Hh6 hh6 = new Hh6();
                    while (true) {
                        int read = fileInputStream.read(bArr);
                        hh6.element = read;
                        if (!(read > 0)) {
                            break;
                        }
                        zipOutputStream.write(bArr, 0, hh6.element);
                    }
                    fileInputStream.close();
                }
                zipOutputStream.close();
                return file2;
            } catch (IOException e2) {
                FLogger.INSTANCE.getLocal().e(i, ".sendFeedbackEmail - read sdk log ex=" + e2);
                return file2;
            }
        } catch (Throwable e3) {
            file2 = null;
            FLogger.INSTANCE.getLocal().e(i, ".sendFeedbackEmail - read sdk log ex=" + e3);
            return file2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0114, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0115, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0118, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0119, code lost:
        r0 = null;
        r4 = null;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0114 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0027] */
    public final File a(ArrayList<String> arrayList) {
        ZipOutputStream zipOutputStream;
        File file = null;
        Throwable th;
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2 = null;
        FLogger.INSTANCE.getLocal().d(i, "zipDataFile, total: " + arrayList.size() + " files");
        try {
            file = new File(Environment.getExternalStorageDirectory(), "DebugDataLog.zip");
            ZipOutputStream zipOutputStream2 = new ZipOutputStream(new FileOutputStream(file));
            try {
                Iterator<String> it = arrayList.iterator();
                while (it.hasNext()) {
                    byte[] bArr = new byte[1024];
                    File file2 = new File(it.next());
                    if (file2.exists()) {
                        try {
                            fileInputStream = new FileInputStream(file2);
                            try {
                                zipOutputStream2.putNextEntry(new ZipEntry(file2.getName()));
                                FLogger.INSTANCE.getLocal().d(i, "zipDataFile - Processing file=" + file2.getName() + ", length=" + file2.length());
                                Hh6 hh6 = new Hh6();
                                while (true) {
                                    int read = fileInputStream.read(bArr);
                                    hh6.element = read;
                                    if (!(read > 0)) {
                                        break;
                                    }
                                    zipOutputStream2.write(bArr, 0, hh6.element);
                                }
                                zipOutputStream2.closeEntry();
                            } catch (Exception e2) {
                                try {
                                    FLogger.INSTANCE.getLocal().e(i, ".zipDataFile - ZipException=" + e2);
                                    zipOutputStream2.closeEntry();
                                    Rs7.a((InputStream) fileInputStream);
                                } catch (Throwable th2) {
                                    th = th2;
                                    fileInputStream2 = fileInputStream;
                                    zipOutputStream2.closeEntry();
                                    Rs7.a((InputStream) fileInputStream2);
                                    throw th;
                                }
                            }
                        } catch (Exception e3) {
                            fileInputStream = null;
                            FLogger.INSTANCE.getLocal().e(i, ".zipDataFile - ZipException=" + e3);
                            zipOutputStream2.closeEntry();
                            Rs7.a((InputStream) fileInputStream);
                        } catch (Throwable th3) {
                            th = th3;
                            zipOutputStream2.closeEntry();
                            Rs7.a((InputStream) fileInputStream2);
                            throw th;
                        }
                        Rs7.a((InputStream) fileInputStream);
                    }
                }
                Rs7.a((OutputStream) zipOutputStream2);
            } catch (Exception e4) {
                zipOutputStream = zipOutputStream2;
                try {
                    FLogger.INSTANCE.getLocal().e(i, ".zipDataFile - ex=" + e4);
                    Rs7.a((OutputStream) zipOutputStream);
                    return file;
                } catch (Throwable th4) {
                    th = th4;
                    zipOutputStream2 = zipOutputStream;
                    th = th;
                    Rs7.a((OutputStream) zipOutputStream2);
                    throw th;
                }
            } catch (Throwable th5) {
                th = th5;
                Rs7.a((OutputStream) zipOutputStream2);
                throw th;
            }
        } catch (Exception e5) {
            zipOutputStream = null;
            FLogger.INSTANCE.getLocal().e(i, ".zipDataFile - ex=" + e5);
            Rs7.a((OutputStream) zipOutputStream);
            return file;
        } catch (Throwable th6) {
        }
        return file;
    }

    @DexIgnore
    public final Object a(Context context, Xe6<? super Cd6> xe6) {
        if (context != null) {
            Window window = ((Activity) context).getWindow();
            Wg6.a((Object) window, "(context as Activity).window");
            View decorView = window.getDecorView();
            Wg6.a((Object) decorView, "(context as Activity).window.decorView");
            View rootView = decorView.getRootView();
            Wg6.a((Object) rootView, "v1");
            rootView.setDrawingCacheEnabled(true);
            Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
            Wg6.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
            throw null;
//            Object a2 = Vh7.a(Qj7.b(), new Fi(this, createBitmap, null), xe6);
//            return a2 == Ff6.a() ? a2 : Cd6.a;
        }
        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01ba  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01e5  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01fc  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    public final /* synthetic */ Object a(Bitmap bitmap, Xe6<? super Cd6> xe6) {
        Ii ii;
        int i2;
        File file;
        FileOutputStream fileOutputStream;
        String format;
        File file2;
        ShakeFeedbackService shakeFeedbackService;
        OutputStream outputStream;
        File file3;
        Bitmap bitmap2;
        WeakReference<Context> weakReference;
        String userId;
        Exception e = null;
        if (xe6 instanceof Ii) {
            Ii ii2 = (Ii) xe6;
            int i3 = ii2.label;
            if ((Integer.MIN_VALUE & i3) != 0) {
                ii2.label = i3 + RecyclerView.UNDEFINED_DURATION;
                ii = ii2;
                Object obj = ii.result;
                Object a2 = Ff6.a();
                i2 = ii.label;
                if (i2 != 0) {
                    Nc6.a(obj);
                    String str = this.e;
                    if (str != null) {
                        file = new File(str);
                        try {
                            file.mkdir();
                            File file4 = new File(this.e, "screenshot.jpg");
                            if (file4.exists()) {
                                file4.delete();
                            }
                            file4.createNewFile();
                            fileOutputStream = new FileOutputStream(file4);
                            try {
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
                                fileOutputStream.flush();
                                fileOutputStream.close();
                            } catch (IOException e2) {
                                e2.printStackTrace();
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String str2 = i;
                                local.e(str2, ".ScreenShootTask - doInBackground ex=" + e);
                                We7 we7 = We7.a;
                                format = String.format("logCat_%s.txt", Arrays.copyOf(new Object[]{Hf6.a(System.currentTimeMillis() / ((long) 1000))}, 1));
                                Wg6.a((Object) format, "java.lang.String.format(format, *args)");
                                file2 = new File(this.e, format);
                                Runtime runtime = Runtime.getRuntime();
                                runtime.exec("logcat -v time -d -f " + file2.getAbsolutePath());
                                UserRepository userRepository = this.h;
                                ii.L$0 = this;
                                ii.L$1 = bitmap;
                                ii.L$2 = fileOutputStream;
                                ii.L$3 = file;
                                ii.L$4 = format;
                                ii.L$5 = file2;
                                ii.label = 1;
                                obj = userRepository.getCurrentUser(ii);
                                if (obj == a2) {
                                }
                            }
                        } catch (IOException e3) {
                            e = e3;
                            fileOutputStream = null;
                            e.printStackTrace();
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str22 = i;
                            local2.e(str22, ".ScreenShootTask - doInBackground ex=" + e);
                            We7 we72 = We7.a;
                            format = String.format("logCat_%s.txt", Arrays.copyOf(new Object[]{Hf6.a(System.currentTimeMillis() / ((long) 1000))}, 1));
                            Wg6.a((Object) format, "java.lang.String.format(format, *args)");
                            file2 = new File(this.e, format);
                            Runtime runtime2 = Runtime.getRuntime();
                            try {
                                runtime2.exec("logcat -v time -d -f " + file2.getAbsolutePath());
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                            UserRepository userRepository2 = this.h;
                            ii.L$0 = this;
                            ii.L$1 = bitmap;
                            ii.L$2 = fileOutputStream;
                            ii.L$3 = file;
                            ii.L$4 = format;
                            ii.L$5 = file2;
                            ii.label = 1;
                            obj = userRepository2.getCurrentUser(ii);
                            if (obj == a2) {
                            }
                        }
                        We7 we722 = We7.a;
                        format = String.format("logCat_%s.txt", Arrays.copyOf(new Object[]{Hf6.a(System.currentTimeMillis() / ((long) 1000))}, 1));
                        Wg6.a((Object) format, "java.lang.String.format(format, *args)");
                        file2 = new File(this.e, format);
                        try {
                            Runtime runtime22 = Runtime.getRuntime();
                            runtime22.exec("logcat -v time -d -f " + file2.getAbsolutePath());
                        } catch (IOException e4) {
                            e4.printStackTrace();
                        }
                        UserRepository userRepository22 = this.h;
                        ii.L$0 = this;
                        ii.L$1 = bitmap;
                        ii.L$2 = fileOutputStream;
                        ii.L$3 = file;
                        ii.L$4 = format;
                        ii.L$5 = file2;
                        ii.label = 1;
                        obj = userRepository22.getCurrentUser(ii);
                        if (obj == a2) {
                            return a2;
                        }
                        shakeFeedbackService = this;
                        outputStream = fileOutputStream;
                        file3 = file2;
                        bitmap2 = bitmap;
                        MFUser mFUser = (MFUser) obj;
                        if (mFUser != null) {
                        }
                        Yd5 yd5 = Yd5.a;
                        weakReference = shakeFeedbackService.a;
                        if (weakReference == null) {
                        }
                    } else {
                        Wg6.a();
                        throw null;
                    }
                } else if (i2 == 1) {
                    file = (File) ii.L$3;
                    outputStream = (OutputStream) ii.L$2;
                    bitmap2 = (Bitmap) ii.L$1;
                    shakeFeedbackService = (ShakeFeedbackService) ii.L$0;
                    Nc6.a(obj);
                    file3 = (File) ii.L$5;
                    format = (String) ii.L$4;
                    MFUser mFUser2 = (MFUser) obj;
                    String str3 = (mFUser2 != null || (userId = mFUser2.getUserId()) == null) ? "" : userId;
                    Yd5 yd52 = Yd5.a;
                    weakReference = shakeFeedbackService.a;
                    if (weakReference == null) {
                        Context context = weakReference.get();
                        if (context != null) {
                            Wg6.a((Object) context, "contextWeakReference!!.get()!!");
                            Context context2 = context;
                            String str4 = shakeFeedbackService.e;
                            if (str4 == null) {
                                str4 = "";
                            }
                            List<String> a3 = yd52.a(context2, str3, str4);
                            StringBuilder sb = new StringBuilder();
                            String str5 = shakeFeedbackService.e;
                            if (str5 != null) {
                                sb.append(str5);
                                sb.append("screenshot.jpg");
                                String sb2 = sb.toString();
                                ArrayList arrayList = new ArrayList();
                                arrayList.add(file3);
                                List<File> a4 = shakeFeedbackService.a();
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String str6 = i;
                                local3.d(str6, "Number of app logs=" + a4.size());
                                arrayList.addAll(a4);
                                ii.L$0 = shakeFeedbackService;
                                ii.L$1 = bitmap2;
                                ii.L$2 = outputStream;
                                ii.L$3 = file;
                                ii.L$4 = format;
                                ii.L$5 = file3;
                                ii.L$6 = str3;
                                ii.L$7 = a3;
                                ii.L$8 = sb2;
                                ii.L$9 = arrayList;
                                ii.L$10 = a4;
                                ii.label = 2;
                                if (shakeFeedbackService.a(sb2, a3, arrayList, ii) == a2) {
                                    return a2;
                                }
                            } else {
                                Wg6.a();
                                throw null;
                            }
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    } else {
                        Wg6.a();
                        throw null;
                    }
                } else if (i2 == 2) {
                    List list = (List) ii.L$10;
                    ArrayList arrayList2 = (ArrayList) ii.L$9;
                    String str7 = (String) ii.L$8;
                    List list2 = (List) ii.L$7;
                    String str8 = (String) ii.L$6;
                    File file5 = (File) ii.L$5;
                    String str9 = (String) ii.L$4;
                    File file6 = (File) ii.L$3;
                    OutputStream outputStream2 = (OutputStream) ii.L$2;
                    Bitmap bitmap3 = (Bitmap) ii.L$1;
                    ShakeFeedbackService shakeFeedbackService2 = (ShakeFeedbackService) ii.L$0;
                    Nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }
        ii = new Ii(this, xe6);
        Object obj2 = ii.result;
        Object a22 = Ff6.a();
        i2 = ii.label;
        if (i2 != 0) {
        }
        return Cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004c A[Catch:{ Exception -> 0x0177 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    public final /* synthetic */ Object a(Xe6<? super Cd6> xe6) throws IOException {
        Ei ei;
        int i2;
        ShakeFeedbackService shakeFeedbackService = null;
        WeakReference<Context> weakReference;
        if (xe6 instanceof Ei) {
            Ei ei2 = (Ei) xe6;
            int i3 = ei2.label;
            if ((i3 & RecyclerView.UNDEFINED_DURATION) != 0) {
                ei2.label = i3 + RecyclerView.UNDEFINED_DURATION;
                ei = ei2;
                Object obj = ei.result;
                Object a2 = Ff6.a();
                i2 = ei.label;
                if (i2 != 0) {
                    Nc6.a(obj);
                    WeakReference<Context> weakReference2 = this.a;
                    if (weakReference2 != null) {
                        HwLogProvider instance = HwLogProvider.getInstance(weakReference2.get());
                        Wg6.a((Object) instance, "HwLogProvider.getInstanc\u2026extWeakReference!!.get())");
                        List<HardwareLog> allHardwareLogs = instance.getAllHardwareLogs();
                        String a3 = new Gson().a(allHardwareLogs);
                        String str = this.e;
                        if (str != null) {
                            File file = new File(str);
                            file.mkdir();
                            String str2 = this.e;
                            We7 we7 = We7.a;
                            String format = String.format("hwlog_%s.txt", Arrays.copyOf(new Object[]{Hf6.a(System.currentTimeMillis())}, 1));
                            Wg6.a((Object) format, "java.lang.String.format(format, *args)");
                            File file2 = new File(str2, format);
                            if (file2.exists()) {
                                file2.delete();
                            }
                            file2.createNewFile();
                            FileOutputStream fileOutputStream = new FileOutputStream(file2);
                            Wg6.a((Object) a3, "jsonLog");
                            Charset charset = Ej6.a;
                            if (a3 != null) {
                                byte[] bytes = a3.getBytes(charset);
                                Wg6.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                                fileOutputStream.write(bytes);
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                Yd5 yd5 = Yd5.a;
                                WeakReference<Context> weakReference3 = this.a;
                                if (weakReference3 != null) {
                                    Context context = weakReference3.get();
                                    if (context != null) {
                                        Wg6.a((Object) context, "contextWeakReference!!.get()!!");
                                        Context context2 = context;
                                        String str3 = this.e;
                                        if (str3 == null) {
                                            str3 = "";
                                        }
                                        String a4 = yd5.a(context2, str3);
                                        ArrayList arrayList = new ArrayList();
                                        if (a4 != null) {
                                            arrayList.add(a4);
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String str4 = i;
                                            local.d(str4, "HwLogFile=" + file2.getAbsolutePath());
                                            ArrayList arrayList2 = new ArrayList();
                                            arrayList2.add(file2);
                                            ei.L$0 = this;
                                            ei.L$1 = allHardwareLogs;
                                            ei.L$2 = a3;
                                            ei.L$3 = fileOutputStream;
                                            ei.L$4 = file;
                                            ei.L$5 = file2;
                                            ei.L$6 = a4;
                                            ei.L$7 = arrayList;
                                            ei.L$8 = arrayList2;
                                            ei.label = 1;
                                            if (a(null, arrayList, arrayList2, ei) == a2) {
                                                return a2;
                                            }
                                            shakeFeedbackService = this;
                                        } else {
                                            Wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        Wg6.a();
                                        throw null;
                                    }
                                } else {
                                    Wg6.a();
                                    throw null;
                                }
                            } else {
                                throw new Rc6("null cannot be cast to non-null type java.lang.String");
                            }
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    } else {
                        Wg6.a();
                        throw null;
                    }
                } else if (i2 == 1) {
                    ArrayList arrayList3 = (ArrayList) ei.L$8;
                    ArrayList arrayList4 = (ArrayList) ei.L$7;
                    String str5 = (String) ei.L$6;
                    File file3 = (File) ei.L$5;
                    File file4 = (File) ei.L$4;
                    FileOutputStream fileOutputStream2 = (FileOutputStream) ei.L$3;
                    String str6 = (String) ei.L$2;
                    List list = (List) ei.L$1;
                    shakeFeedbackService = (ShakeFeedbackService) ei.L$0;
                    try {
                        Nc6.a(obj);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str7 = i;
                        local2.e(str7, ".sendHardwareLog - ex=" + e2);
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                weakReference = shakeFeedbackService.a;
                if (weakReference == null) {
                    HwLogProvider.getInstance(weakReference.get()).setHardwareLogRead();
                    return Cd6.a;
                }
                Wg6.a();
                throw null;
            }
        }
        ei = new Ei(this, xe6);
        Object obj2 = ei.result;
        Object a22 = Ff6.a();
        i2 = ei.label;
        if (i2 != 0) {
        }
        weakReference = shakeFeedbackService.a;
        if (weakReference == null) {
        }
        throw null;
//
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    public final /* synthetic */ Object a(String str, List<String> list, List<? extends File> list2, Xe6<? super Cd6> xe6) {
        Ci ci;
        int i2;
        Intent intent;
        ShakeFeedbackService shakeFeedbackService;
        Intent intent2;
        Uri uri;
        String str2 = null;
        Object a2 = null;
        Intent intent3 = null;
        StringBuilder sb = null;
        long j;
        WeakReference<Context> weakReference;
        if (xe6 instanceof Ci) {
            Ci ci2 = (Ci) xe6;
            int i3 = ci2.label;
            if ((Integer.MIN_VALUE & i3) != 0) {
                ci2.label = i3 + RecyclerView.UNDEFINED_DURATION;
                ci = ci2;
                Object obj = ci.result;
                Object a3 = Ff6.a();
                i2 = ci.label;
                if (i2 != 0) {
                    Nc6.a(obj);
                    intent = new Intent("android.intent.action.SEND_MULTIPLE");
                    intent.setType("vnd.android.cursor.dir/email");
                    if (!PortfolioApp.get.e()) {
                        intent.putExtra("android.intent.extra.EMAIL", new String[]{"help@misfit.com"});
                    } else if (this.g != -1) {
                        intent.putExtra("android.intent.extra.EMAIL", new String[]{"fossiluat@fossil.com", "bugs@fossil.com", "sw-qa@fossil.com", "minh@fossil.com", "sw-q-android@fossil.com", "sw-diana@fossil.com", "diana-bugs-sm@fossil.com", "diana-bugs-sm@fossil.com", "sw-sa@fossil.com"});
                    } else {
                        int i4 = this.f;
                        if (i4 == 3) {
                            intent.putExtra("android.intent.extra.EMAIL", new String[]{"bugs@fossil.com", "buddy-challenge-sm@fossil.com"});
                        } else if (i4 == 0) {
                            intent.putExtra("android.intent.extra.EMAIL", new String[]{"fossiluat@fossil.com", "bugs@fossil.com", "sw-qa@fossil.com", "minh@fossil.com", "sw-q-android@fossil.com", "sw-diana@fossil.com", "diana-bugs-sm@fossil.com", "diana-bugs-sm@fossil.com", "sw-sa@fossil.com"});
                        } else if (i4 == 2) {
                            intent.putExtra("android.intent.extra.EMAIL", new String[]{"bugs@fossil.com", "sw-qa@fossil.com", "minh@fossil.com", "dungdna@fossil.com", "sw-q-android@fossil.com", "sw-diana@fossil.com", "diana-bugs-sm@fossil.com", "diana-bugs-sm@fossil.com", "sw-sa@fossil.com", "fossiluat@fossil.com"});
                        } else {
                            intent.putExtra("android.intent.extra.EMAIL", new String[]{"bugs@fossil.com", "sw-qa@fossil.com", "dungdna@fossil.com", "sw-portfolio-android@fossil.com", "sw-q-android@fossil.com", "diana-bugs-sm@fossil.com", "sw-sa@fossil.com"});
                        }
                    }
                    ArrayList<? extends Parcelable> arrayList = new ArrayList<>();
                    ArrayList<String> arrayList2 = new ArrayList<>();
                    long j2 = 0;
                    if (!(!list.isEmpty()) || !PortfolioApp.get.e()) {
                        uri = null;
                    } else {
                        uri = null;
                        for (String str3 : list) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String str4 = i;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(".sendFeedbackEmail - databaseFile=");
                            sb2.append(str3);
                            local.e(str4, sb2.toString());
                            if (str3 == null || (uri = a(str3)) == null) {
                                j = j2;
                            } else {
                                throw null;
//                                arrayList.add(uri);
//                                arrayList2.add(str3);
//                                String path = uri.getPath();
//                                if (path != null) {
//                                    j = new File(path).length() + j2;
//                                } else {
//                                    Wg6.a();
//                                    throw null;
//                                }
                            }
                            j2 = j;
                        }
                    }
                    StringBuilder sb3 = new StringBuilder();
                    Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                    Wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                    File filesDir = applicationContext.getFilesDir();
                    Wg6.a((Object) filesDir, "PortfolioApp.instance.applicationContext.filesDir");
                    sb3.append(filesDir.getAbsolutePath());
                    sb3.append(File.separator);
                    sb3.append(Zs1.b.d());
                    File file = new File(sb3.toString());
                    File a4 = a("NewSDKLog", file);
                    if (a4 != null) {
                        String absolutePath = a4.getAbsolutePath();
                        Wg6.a((Object) absolutePath, "newSdkZipFile.absolutePath");
                        Uri a5 = a(absolutePath);
                        if (a5 != null) {
                            throw null;
//                            arrayList.add(a5);
//                            arrayList2.add(a4.getAbsolutePath());
//                            j2 += a4.length();
                        }
                    }
                    String c2 = Zs1.b.c();
                    if (!TextUtils.isEmpty(c2)) {
                        StringBuilder sb4 = new StringBuilder();
                        Context applicationContext2 = PortfolioApp.get.instance().getApplicationContext();
                        Wg6.a((Object) applicationContext2, "PortfolioApp.instance.applicationContext");
                        File filesDir2 = applicationContext2.getFilesDir();
                        Wg6.a((Object) filesDir2, "PortfolioApp.instance.applicationContext.filesDir");
                        sb4.append(filesDir2.getAbsolutePath());
                        sb4.append(File.separator);
                        sb4.append(c2);
                        File file2 = new File(sb4.toString());
                        arrayList2.add(file2.getAbsolutePath());
                        j2 += file2.length();
                    }
                    if (!TextUtils.isEmpty(str)) {
                        if (str != null) {
                            uri = a(str);
                            if (uri != null) {
                                throw null;
//                                arrayList.add(uri);
//                                arrayList2.add(str);
//                                String path2 = uri.getPath();
//                                if (path2 != null) {
//                                    j2 += new File(path2).length();
//                                } else {
//                                    Wg6.a();
//                                    throw null;
//                                }
                            }
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    }
                    for (File file3 : list2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str5 = i;
                        local2.d(str5, "sendFeedbackEmail - logFile=" + file3.getName() + ", length=" + file3.length());
                        String absolutePath2 = file3.getAbsolutePath();
                        Wg6.a((Object) absolutePath2, "log.absolutePath");
                        Uri a6 = a(absolutePath2);
                        if (a6 != null) {
                            j2 += file3.length();
                            throw null;
//                            arrayList.add(a6);
//                            arrayList2.add(file3.getAbsolutePath());
                        }
                    }
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str6 = i;
                    local3.d(str6, ".sendFeedbackEmail - dataFeedbackSize=" + j2);
                    if (j2 > 1048576) {
                        File a7 = a(arrayList2);
                        arrayList.clear();
                        if (a7 == null) {
                            FLogger.INSTANCE.getLocal().d(i, "Can't zip file");
                            WeakReference<Context> weakReference2 = this.a;
                            throw null;
//                            Activity activity = weakReference2 != null ? weakReference2.get() : null;
//                            if (activity != null) {
//                                activity.runOnUiThread(new Di(activity));
//                                return Cd6.a;
//                            }
//                            throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                        }
                        String absolutePath3 = a7.getAbsolutePath();
                        Wg6.a((Object) absolutePath3, "file.absolutePath");
                        Uri a8 = a(absolutePath3);
                        if (a8 != null) {
                            throw null;
//                            arrayList.add(a8);
                        }
                    }
                    intent.putParcelableArrayListExtra("android.intent.extra.STREAM", arrayList);
                    int i5 = this.g;
                    if (i5 == -1) {
                        int i6 = this.f;
                        if (i6 == 3) {
                            intent.putExtra("android.intent.extra.SUBJECT", "[BC]-[Android - Feedback]");
                        } else if (i6 == 0) {
                            StringBuilder sb5 = new StringBuilder();
                            sb5.append("[BETA - ");
                            sb5.append(PortfolioApp.get.instance().h());
                            sb5.append(" ");
                            sb5.append(PortfolioApp.get.e() ? "Staging] - " : "] - ");
                            sb5.append("[Android - Feedback]");
                            intent.putExtra("android.intent.extra.SUBJECT", sb5.toString());
                        } else {
                            StringBuilder sb6 = new StringBuilder();
                            sb6.append("[");
                            sb6.append(PortfolioApp.get.instance().h());
                            sb6.append(" ");
                            sb6.append(PortfolioApp.get.e() ? "Staging] - " : "] - ");
                            sb6.append("[Android Feedback] -");
                            We7 we7 = We7.a;
                            String format = String.format(" on %s (%s)", Arrays.copyOf(new Object[]{Hf6.a(System.currentTimeMillis() / ((long) 1000)), TimeUtils.e(new Date())}, 2));
                            Wg6.a((Object) format, "java.lang.String.format(format, *args)");
                            sb6.append(format);
                            intent.putExtra("android.intent.extra.SUBJECT", sb6.toString());
                        }
                    } else if (i5 == 0) {
                        intent.putExtra("android.intent.extra.SUBJECT", "GATT CONNECTION");
                    } else if (i5 == 1) {
                        intent.putExtra("android.intent.extra.SUBJECT", "HID CONNECTION");
                    }
                    str2 = "android.intent.extra.TEXT";
                    try {
                        StringBuilder sb7 = new StringBuilder();
                        sb7.append("[");
                        sb7.append(PortfolioApp.get.instance().h());
                        sb7.append(" ");
                        sb7.append(PortfolioApp.get.e() ? "Staging" : "");
                        sb7.append("]\n");
                        AppHelper b2 = AppHelper.g.b();
                        WeakReference<Context> weakReference3 = this.a;
                        if (weakReference3 != null) {
                            Context context = weakReference3.get();
                            if (context != null) {
                                Wg6.a((Object) context, "contextWeakReference!!.get()!!");
                                int i7 = this.f;
                                int i8 = this.g;
                                ci.L$0 = this;
                                ci.L$1 = str;
                                ci.L$2 = list;
                                ci.L$3 = list2;
                                ci.L$4 = intent;
                                ci.L$5 = arrayList;
                                ci.L$6 = arrayList2;
                                ci.J$0 = j2;
                                ci.L$7 = uri;
                                ci.L$8 = file;
                                ci.L$9 = a4;
                                ci.L$10 = c2;
                                ci.L$11 = intent;
                                ci.L$12 = "android.intent.extra.TEXT";
                                ci.L$13 = sb7;
                                ci.label = 1;
                                a2 = b2.a(context, i7, i8, ci);
                                if (a2 == a3) {
                                    return a3;
                                }
                                intent3 = intent;
                                sb = sb7;
                                shakeFeedbackService = this;
                                intent2 = intent;
                            } else {
                                Wg6.a();
                                throw null;
                            }
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    } catch (Exception e2) {
                        throw null;
//                        e = e2;
//                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
//                        String str7 = i;
//                        local4.d(str7, ".sendFeedbackEmail - ex=" + e);
//                        shakeFeedbackService = this;
//                        intent2 = intent;
//                        weakReference = shakeFeedbackService.a;
//                        if (weakReference != null) {
//                        }
//                        return Cd6.a;
                    }
                } else if (i2 == 1) {
                    StringBuilder sb8 = (StringBuilder) ci.L$13;
                    String str8 = (String) ci.L$12;
                    intent3 = (Intent) ci.L$11;
                    String str9 = (String) ci.L$10;
                    File file4 = (File) ci.L$9;
                    File file5 = (File) ci.L$8;
                    Uri uri2 = (Uri) ci.L$7;
                    long j3 = ci.J$0;
                    ArrayList arrayList3 = (ArrayList) ci.L$6;
                    ArrayList arrayList4 = (ArrayList) ci.L$5;
                    intent2 = (Intent) ci.L$4;
                    List list3 = (List) ci.L$3;
                    List list4 = (List) ci.L$2;
                    String str10 = (String) ci.L$1;
                    shakeFeedbackService = (ShakeFeedbackService) ci.L$0;
                    try {
                        Nc6.a(obj);
                        sb = sb8;
                        a2 = obj;
                        str2 = str8;
                    } catch (Exception e3) {
                        Exception e = e3;
                        throw null;
//                        this = shakeFeedbackService;
//                        intent = intent2;
//                        ILocalFLogger local42 = FLogger.INSTANCE.getLocal();
//                        String str72 = i;
//                        local42.d(str72, ".sendFeedbackEmail - ex=" + e);
//                        shakeFeedbackService = this;
//                        intent2 = intent;
//                        weakReference = shakeFeedbackService.a;
//                        if (weakReference != null) {
//                        }
//                        return Cd6.a;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                sb.append((String) a2);
                intent3.putExtra(str2, sb.toString());
                weakReference = shakeFeedbackService.a;
                if (weakReference != null) {
                    if (weakReference == null) {
                        Wg6.a();
                        throw null;
                    } else if (weakReference.get() != null) {
                        WeakReference<Context> weakReference4 = shakeFeedbackService.a;
                        if (weakReference4 != null) {
                            Context context2 = weakReference4.get();
                            if (context2 != null) {
                                Context context3 = context2;
                                WeakReference<Context> weakReference5 = shakeFeedbackService.a;
                                if (weakReference5 != null) {
                                    Context context4 = weakReference5.get();
                                    if (context4 != null) {
                                        context3.startActivity(Intent.createChooser(intent2, context4.getString(2131887481)));
                                    } else {
                                        Wg6.a();
                                        throw null;
                                    }
                                } else {
                                    Wg6.a();
                                    throw null;
                                }
                            } else {
                                Wg6.a();
                                throw null;
                            }
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    }
                }
                return Cd6.a;
            }
        }
        ci = new Ci(this, xe6);
        Object obj2 = ci.result;
        Object a32 = Ff6.a();
        i2 = ci.label;
        if (i2 != 0) {
        }
        try {
            sb.append((String) a2);
            intent3.putExtra(str2, sb.toString());
        } catch (Exception e4) {
            throw null;
//            Exception e = e4;
//            this = shakeFeedbackService;
//            intent = intent2;
//            ILocalFLogger local422 = FLogger.INSTANCE.getLocal();
//            String str722 = i;
//            local422.d(str722, ".sendFeedbackEmail - ex=" + e);
//            shakeFeedbackService = this;
//            intent2 = intent;
//            weakReference = shakeFeedbackService.a;
//            if (weakReference != null) {
//            }
//            return Cd6.a;
        }
        throw null;
//        weakReference = shakeFeedbackService.a;
//        if (weakReference != null) {
//        }
//        return Cd6.a;
    }

    @DexIgnore
    public final List<File> a() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(FLogger.INSTANCE.getLocal().exportAppLogs());
        arrayList2.addAll(FLogger.INSTANCE.getRemote().exportAppLogs());
        arrayList2.addAll(MicroAppEventLogger.exportLogFiles());
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            File file = (File) it.next();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            StringBuilder sb = new StringBuilder();
            sb.append("Exporting ");
            Wg6.a((Object) file, "file");
            sb.append(file.getName());
            sb.append(", size=");
            sb.append(file.length());
            local.d(str, sb.toString());
            try {
                File file2 = new File(this.e, file.getName());
                if (file2.exists()) {
                    file2.delete();
                } else {
                    file2.createNewFile();
                }
                FileChannel channel = new FileInputStream(file).getChannel();
                Wg6.a((Object) channel, "FileInputStream(file).channel");
                FileChannel channel2 = new FileOutputStream(file2).getChannel();
                Wg6.a((Object) channel2, "FileOutputStream(exportFile).channel");
                channel2.transferFrom(channel, 0, channel.size());
                channel.close();
                channel2.close();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = i;
                local2.d(str2, "Done exporting " + file2.getName() + ", size=" + file2.length());
                arrayList.add(file2);
            } catch (Exception e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = i;
                local3.e(str3, "Error while exporting log files - e=" + e2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final void a(Context context) {
        throw null;
//        synchronized (this) {
//            Wg6.b(context, Constants.ACTIVITY);
//            this.a = new WeakReference<>(context);
//            Object systemService = context.getSystemService("sensor");
//            if (systemService != null) {
//                SensorManager sensorManager = (SensorManager) systemService;
//                Rj5 rj5 = new Rj5(new Bi(this));
//                this.b = rj5;
//                if (rj5 != null) {
//                    rj5.a(sensorManager);
//                }
//            } else {
//                throw new Rc6("null cannot be cast to non-null type android.hardware.SensorManager");
//            }
//        }
    }

    @DexIgnore
    public final boolean b() {
        return Wg6.a((Object) "mounted", (Object) Environment.getExternalStorageState());
    }

    @DexIgnore
    public final boolean c() {
        Ms3 ms3;
        Ms3 ms32 = this.c;
        if (ms32 != null) {
            if (ms32 == null) {
                Wg6.a();
                throw null;
            } else if (ms32.isShowing() && (ms3 = this.d) != null) {
                if (ms3 == null) {
                    Wg6.a();
                    throw null;
                } else if (ms3.isShowing()) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final void d() {
        WeakReference<Context> weakReference = this.a;
        if (weakReference == null) {
            Wg6.a();
            throw null;
        } else if (De5.a(weakReference.get()) && !c()) {
            Ms3 ms3 = this.d;
            if (ms3 != null) {
                if (ms3 != null) {
                    ms3.dismiss();
                } else {
                    Wg6.a();
                    throw null;
                }
            }
            WeakReference<Context> weakReference2 = this.a;
            if (weakReference2 != null) {
                Context context = weakReference2.get();
                if (context != null) {
                    Object systemService = context.getSystemService("layout_inflater");
                    if (systemService != null) {
                        View inflate = ((LayoutInflater) systemService).inflate(2131558462, (ViewGroup) null);
                        WeakReference<Context> weakReference3 = this.a;
                        if (weakReference3 != null) {
                            Context context2 = weakReference3.get();
                            if (context2 != null) {
                                Ms3 ms32 = new Ms3(context2);
                                this.d = ms32;
                                ms32.setContentView(inflate);
                                View findViewById = inflate.findViewById(2131363351);
                                if (findViewById != null) {
                                    ((TextView) findViewById).setText("4.5.0");
                                    inflate.findViewById(2131361942).setOnClickListener(new Gi(this));
                                    inflate.findViewById(2131361944).setOnClickListener(new Hi(this));
                                    Ms3 ms33 = this.d;
                                    if (ms33 != null) {
                                        ms33.show();
                                    } else {
                                        Wg6.a();
                                        throw null;
                                    }
                                } else {
                                    throw new Rc6("null cannot be cast to non-null type android.widget.TextView");
                                }
                            } else {
                                Wg6.a();
                                throw null;
                            }
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    } else {
                        throw new Rc6("null cannot be cast to non-null type android.view.LayoutInflater");
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            } else {
                Wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void e() {
        synchronized (this) {
            Rj5 rj5 = this.b;
            if (rj5 != null) {
                rj5.a();
            }
            if (this.c != null) {
                Ms3 ms3 = this.c;
                if (ms3 != null) {
                    ms3.dismiss();
                    this.c = null;
                } else {
                    Wg6.a();
                    throw null;
                }
            }
            if (this.d != null) {
                Ms3 ms32 = this.d;
                if (ms32 != null) {
                    ms32.dismiss();
                    this.d = null;
                } else {
                    Wg6.a();
                    throw null;
                }
            }
            this.a = null;
        }
    }
}
