package com.portfolio.platform.uirenew.customview.imagecropper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Hr5;
import com.fossil.Ir5;
import com.fossil.Kr5;
import com.fossil.Ub;
import com.fossil.imagefilters.FilterType;
import com.mapped.Lw4;
import com.mapped.Nw4;
import com.mapped.X24;
import com.portfolio.platform.uirenew.customview.imagecropper.CropOverlayView;
import java.lang.ref.WeakReference;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
public class CropImageView extends FrameLayout {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public int B;
    @DexIgnore
    public g C;
    @DexIgnore
    public f D;
    @DexIgnore
    public h E;
    @DexIgnore
    public i F;
    @DexIgnore
    public e G;
    @DexIgnore
    public Uri H;
    @DexIgnore
    public int I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public RectF M;
    @DexIgnore
    public int N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public Uri P;
    @DexIgnore
    public WeakReference<Ir5> Q;
    @DexIgnore
    public WeakReference<Hr5> R;
    @DexIgnore
    public Bitmap S;
    @DexIgnore
    public /* final */ ImageView a;
    @DexIgnore
    public /* final */ CropOverlayView b;
    @DexIgnore
    public /* final */ Matrix c;
    @DexIgnore
    public /* final */ Matrix d;
    @DexIgnore
    public /* final */ float[] e;
    @DexIgnore
    public /* final */ float[] f;
    @DexIgnore
    public Kr5 g;
    @DexIgnore
    public Bitmap h;
    @DexIgnore
    public Bitmap i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public k v;
    @DexIgnore
    public FilterType w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements CropOverlayView.b {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.uirenew.customview.imagecropper.CropOverlayView.b
        public void a(boolean z) {
            CropImageView.this.a(z, true);
            g a2 = CropImageView.this.C;
            if (a2 != null && !z) {
                a2.a(CropImageView.this.getCropRect());
            }
            f b = CropImageView.this.D;
            if (b != null && z) {
                b.a(CropImageView.this.getCropRect());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ Uri b;
        @DexIgnore
        public /* final */ Bitmap c;
        @DexIgnore
        public /* final */ Uri d;
        @DexIgnore
        public /* final */ Rect e;
        @DexIgnore
        public /* final */ Rect f;

        @DexIgnore
        public b(Bitmap bitmap, Uri uri, Bitmap bitmap2, Uri uri2, Exception exc, float[] fArr, Rect rect, Rect rect2, int i, int i2) {
            this.a = bitmap;
            this.b = uri;
            this.c = bitmap2;
            this.d = uri2;
            this.e = rect;
            this.f = rect2;
        }
    }

    @DexIgnore
    public enum c {
        RECTANGLE,
        OVAL
    }

    @DexIgnore
    public enum d {
        OFF,
        ON_TOUCH,
        ON
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(CropImageView cropImageView, b bVar);
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        void a(Rect rect);
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(Rect rect);
    }

    @DexIgnore
    public interface h {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore
    public interface i {
        @DexIgnore
        void a(CropImageView cropImageView, Uri uri, Exception exc);
    }

    @DexIgnore
    public enum j {
        NONE,
        SAMPLING,
        RESIZE_INSIDE,
        RESIZE_FIT,
        RESIZE_EXACT
    }

    @DexIgnore
    public enum k {
        FIT_CENTER,
        CENTER,
        CENTER_CROP,
        CENTER_INSIDE
    }

    @DexIgnore
    public CropImageView(Context context) {
        this(context, null);
    }

    @DexIgnore
    @SuppressLint("ResourceType")
    /* JADX INFO: finally extract failed */
    public CropImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Nw4 nw4;
        Bundle bundleExtra;
        this.c = new Matrix();
        this.d = new Matrix();
        this.e = new float[8];
        this.f = new float[8];
        this.w = FilterType.ATKINSON_DITHERING;
        this.x = false;
        this.y = true;
        this.z = true;
        this.A = true;
        this.I = 1;
        this.J = 1.0f;
        Intent intent = context instanceof Activity ? ((Activity) context).getIntent() : null;
        Nw4 nw42 = (intent == null || (bundleExtra = intent.getBundleExtra("CROP_IMAGE_EXTRA_BUNDLE")) == null) ? null : (Nw4) bundleExtra.getParcelable("CROP_IMAGE_EXTRA_OPTIONS");
        if (nw42 == null) {
            Nw4 nw43 = new Nw4();
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.CropImageView, 0, 0);
                try {
                    throw null;
//                    nw43.q = obtainStyledAttributes.getBoolean(10, nw43.q);
//                    nw43.r = obtainStyledAttributes.getInteger(0, nw43.r);
//                    nw43.s = obtainStyledAttributes.getInteger(1, nw43.s);
//                    nw43.e = k.values()[obtainStyledAttributes.getInt(26, nw43.e.ordinal())];
//                    nw43.h = obtainStyledAttributes.getBoolean(2, nw43.h);
//                    nw43.i = obtainStyledAttributes.getBoolean(24, nw43.i);
//                    nw43.j = obtainStyledAttributes.getInteger(19, nw43.j);
//                    nw43.a = c.values()[obtainStyledAttributes.getInt(27, nw43.a.ordinal())];
//                    nw43.d = d.values()[obtainStyledAttributes.getInt(13, nw43.d.ordinal())];
//                    nw43.b = obtainStyledAttributes.getDimension(30, nw43.b);
//                    nw43.c = obtainStyledAttributes.getDimension(31, nw43.c);
//                    nw43.p = obtainStyledAttributes.getFloat(16, nw43.p);
//                    nw43.t = obtainStyledAttributes.getDimension(8, nw43.t);
//                    nw43.u = obtainStyledAttributes.getInteger(9, nw43.u);
//                    nw43.v = obtainStyledAttributes.getDimension(7, nw43.v);
//                    nw43.w = obtainStyledAttributes.getDimension(6, nw43.w);
//                    nw43.x = obtainStyledAttributes.getDimension(5, nw43.x);
//                    nw43.y = obtainStyledAttributes.getInteger(4, nw43.y);
//                    nw43.z = obtainStyledAttributes.getDimension(15, nw43.z);
//                    nw43.A = obtainStyledAttributes.getInteger(14, nw43.A);
//                    nw43.B = obtainStyledAttributes.getInteger(3, nw43.B);
//                    nw43.f = obtainStyledAttributes.getBoolean(28, this.y);
//                    nw43.g = obtainStyledAttributes.getBoolean(29, this.z);
//                    nw43.v = obtainStyledAttributes.getDimension(7, nw43.v);
//                    nw43.C = (int) obtainStyledAttributes.getDimension(23, (float) nw43.C);
//                    nw43.D = (int) obtainStyledAttributes.getDimension(22, (float) nw43.D);
//                    nw43.E = (int) obtainStyledAttributes.getFloat(21, (float) nw43.E);
//                    nw43.F = (int) obtainStyledAttributes.getFloat(20, (float) nw43.F);
//                    nw43.G = (int) obtainStyledAttributes.getFloat(18, (float) nw43.G);
//                    nw43.H = (int) obtainStyledAttributes.getFloat(17, (float) nw43.H);
//                    nw43.X = obtainStyledAttributes.getBoolean(11, nw43.X);
//                    nw43.Y = obtainStyledAttributes.getBoolean(11, nw43.Y);
//                    this.x = obtainStyledAttributes.getBoolean(25, this.x);
//                    if (obtainStyledAttributes.hasValue(0) && obtainStyledAttributes.hasValue(0) && !obtainStyledAttributes.hasValue(10)) {
//                        nw43.q = true;
//                    }
//                    obtainStyledAttributes.recycle();
//                    nw4 = nw43;
                } catch (Throwable th) {
                    obtainStyledAttributes.recycle();
                    throw th;
                }
            } else {
                nw4 = nw43;
            }
        } else {
            nw4 = nw42;
        }
        nw4.a();
        this.v = nw4.e;
        this.A = nw4.h;
        this.B = nw4.j;
        this.y = nw4.f;
        this.z = nw4.g;
        this.q = nw4.X;
        this.r = nw4.Y;
        View inflate = LayoutInflater.from(context).inflate(2131558459, (ViewGroup) this, true);
        ImageView imageView = (ImageView) inflate.findViewById(2131361803);
        this.a = imageView;
        imageView.setScaleType(ImageView.ScaleType.MATRIX);
        CropOverlayView cropOverlayView = (CropOverlayView) inflate.findViewById(2131361798);
        this.b = cropOverlayView;
        cropOverlayView.setCropWindowChangeListener(new a());
        this.b.setInitialAttributeValues(nw4);
        h();
    }

    @DexIgnore
    public static int a(int i2, int i3, int i4) {
        return i2 == 1073741824 ? i3 : i2 == Integer.MIN_VALUE ? Math.min(i4, i3) : i4;
    }

    @DexIgnore
    public Bitmap a(int i2, int i3, j jVar) {
        if (this.h == null) {
            return null;
        }
        this.a.clearAnimation();
        int i4 = 0;
        throw null;
//        int i5 = jVar != j.NONE ? i2 : 0;
//        if (jVar != j.NONE) {
//            i4 = i3;
//        }
//        return Lw4.a((this.H == null || (this.I <= 1 && jVar != j.SAMPLING)) ? Lw4.a(this.h, getCropPoints(), this.p, this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY(), this.q, this.r).a : Lw4.a(getContext(), this.H, getCropPoints(), this.p, this.h.getWidth() * this.I, this.h.getHeight() * this.I, this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY(), i5, i4, this.q, this.r).a, i5, i4, jVar);
    }

    @DexIgnore
    public void a() {
        this.E = null;
        this.D = null;
        this.C = null;
        this.G = null;
        this.F = null;
    }

    @DexIgnore
    public final void a(float f2, float f3, boolean z2, boolean z3) {
        float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (this.h != null && f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && f3 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.c.invert(this.d);
            RectF cropWindowRect = this.b.getCropWindowRect();
            this.d.mapRect(cropWindowRect);
            this.c.reset();
            this.c.postTranslate((f2 - ((float) this.h.getWidth())) / 2.0f, (f3 - ((float) this.h.getHeight())) / 2.0f);
            f();
            int i2 = this.p;
            if (i2 > 0) {
                this.c.postRotate((float) i2, Lw4.b(this.e), Lw4.c(this.e));
                f();
            }
            float min = Math.min(f2 / Lw4.h(this.e), f3 / Lw4.d(this.e));
            k kVar = this.v;
            if (kVar == k.FIT_CENTER || ((kVar == k.CENTER_INSIDE && min < 1.0f) || (min > 1.0f && this.A))) {
                this.c.postScale(min, min, Lw4.b(this.e), Lw4.c(this.e));
                f();
            }
            float f5 = this.q ? -this.J : this.J;
            float f6 = this.r ? -this.J : this.J;
            this.c.postScale(f5, f6, Lw4.b(this.e), Lw4.c(this.e));
            f();
            this.c.mapRect(cropWindowRect);
            if (z2) {
                this.K = f2 > Lw4.h(this.e) ? 0.0f : Math.max(Math.min((f2 / 2.0f) - cropWindowRect.centerX(), -Lw4.e(this.e)), ((float) getWidth()) - Lw4.f(this.e)) / f5;
                if (f3 <= Lw4.d(this.e)) {
                    f4 = Math.max(Math.min((f3 / 2.0f) - cropWindowRect.centerY(), -Lw4.g(this.e)), ((float) getHeight()) - Lw4.a(this.e)) / f6;
                }
                this.L = f4;
            } else {
                this.K = Math.min(Math.max(this.K * f5, -cropWindowRect.left), (-cropWindowRect.right) + f2) / f5;
                this.L = Math.min(Math.max(this.L * f6, -cropWindowRect.top), (-cropWindowRect.bottom) + f3) / f6;
            }
            this.c.postTranslate(this.K * f5, this.L * f6);
            cropWindowRect.offset(f5 * this.K, f6 * this.L);
            this.b.setCropWindowRect(cropWindowRect);
            f();
            this.b.invalidate();
            if (z3) {
                this.g.a(this.e, this.c);
                this.a.startAnimation(this.g);
            } else {
                this.a.setImageMatrix(this.c);
            }
            a(false);
        }
    }

    @DexIgnore
    public void a(int i2) {
        if (this.h != null) {
            int i3 = i2 < 0 ? (i2 % 360) + 360 : i2 % 360;
            boolean z2 = !this.b.c() && ((i3 > 45 && i3 < 135) || (i3 > 215 && i3 < 305));
            Lw4.c.set(this.b.getCropWindowRect());
            RectF rectF = Lw4.c;
            float height = (z2 ? rectF.height() : rectF.width()) / 2.0f;
            RectF rectF2 = Lw4.c;
            float width = (z2 ? rectF2.width() : rectF2.height()) / 2.0f;
            if (z2) {
                boolean z3 = this.q;
                this.q = this.r;
                this.r = z3;
            }
            this.c.invert(this.d);
            Lw4.d[0] = Lw4.c.centerX();
            Lw4.d[1] = Lw4.c.centerY();
            float[] fArr = Lw4.d;
            fArr[2] = 0.0f;
            fArr[3] = 0.0f;
            fArr[4] = 1.0f;
            fArr[5] = 0.0f;
            this.d.mapPoints(fArr);
            this.p = (i3 + this.p) % 360;
            a((float) getWidth(), (float) getHeight(), true, false);
            this.c.mapPoints(Lw4.e, Lw4.d);
            float[] fArr2 = Lw4.e;
            double pow = Math.pow((double) (fArr2[4] - fArr2[2]), 2.0d);
            float[] fArr3 = Lw4.e;
            float sqrt = (float) (((double) this.J) / Math.sqrt(pow + Math.pow((double) (fArr3[5] - fArr3[3]), 2.0d)));
            this.J = sqrt;
            this.J = Math.max(sqrt, 1.0f);
            a((float) getWidth(), (float) getHeight(), true, false);
            this.c.mapPoints(Lw4.e, Lw4.d);
            float[] fArr4 = Lw4.e;
            double pow2 = Math.pow((double) (fArr4[4] - fArr4[2]), 2.0d);
            float[] fArr5 = Lw4.e;
            double sqrt2 = Math.sqrt(pow2 + Math.pow((double) (fArr5[5] - fArr5[3]), 2.0d));
            float f2 = (float) (((double) height) * sqrt2);
            float f3 = (float) (sqrt2 * ((double) width));
            RectF rectF3 = Lw4.c;
            float[] fArr6 = Lw4.e;
            rectF3.set(fArr6[0] - f2, fArr6[1] - f3, f2 + fArr6[0], f3 + fArr6[1]);
            this.b.f();
            this.b.setCropWindowRect(Lw4.c);
            a((float) getWidth(), (float) getHeight(), true, false);
            a(false, false);
            this.b.a();
        }
    }

    @DexIgnore
    public void a(int i2, int i3) {
        this.b.setAspectRatioX(i2);
        this.b.setAspectRatioY(i3);
        setFixedAspectRatio(true);
    }

    @DexIgnore
    public void a(int i2, int i3, j jVar, Uri uri, Bitmap.CompressFormat compressFormat, int i4) {
        Bitmap bitmap = this.h;
        if (bitmap != null) {
            this.a.clearAnimation();
            WeakReference<Hr5> weakReference = this.R;
            Hr5 hr5 = weakReference != null ? weakReference.get() : null;
            if (hr5 != null) {
                hr5.cancel(true);
            }
            throw null;
//            int i5 = jVar != j.NONE ? i2 : 0;
//            int i6 = jVar != j.NONE ? i3 : 0;
//            int width = bitmap.getWidth();
//            int i7 = this.I;
//            int height = bitmap.getHeight();
//            int i8 = this.I;
//            if (this.H == null || (i8 <= 1 && jVar != j.SAMPLING)) {
//                this.R = new WeakReference<>(new Hr5(this, bitmap, getCropPoints(), this.p, this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY(), i5, i6, this.q, this.r, jVar, uri, compressFormat, i4));
//            } else {
//                this.R = new WeakReference<>(new Hr5(this, this.H, getCropPoints(), this.p, i7 * width, height * i8, this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY(), i5, i6, this.q, this.r, jVar, uri, compressFormat, i4));
//            }
//            this.R.get().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
//            h();
        }
    }

    @DexIgnore
    public void a(Bitmap bitmap) {
        this.a.clearAnimation();
        if (this.h != null && (this.u > 0 || this.H != null)) {
            this.h.recycle();
        }
        this.h = null;
        this.h = bitmap;
        this.a.setImageBitmap(bitmap);
    }

    @DexIgnore
    public final void a(Bitmap bitmap, int i2, Uri uri, int i3, int i4) {
        Bitmap bitmap2 = this.h;
        if (bitmap2 == null || !bitmap2.equals(bitmap)) {
            this.a.clearAnimation();
            b();
            this.S = bitmap.copy(bitmap.getConfig(), true);
            this.h = bitmap;
            this.a.setImageBitmap(bitmap);
            this.H = uri;
            this.u = i2;
            this.I = i3;
            this.p = i4;
            a((float) getWidth(), (float) getHeight(), true, false);
            CropOverlayView cropOverlayView = this.b;
            if (cropOverlayView != null) {
                cropOverlayView.f();
                g();
            }
        }
    }

    @DexIgnore
    public void a(Bitmap bitmap, Ub ub) {
        int i2;
        Bitmap bitmap2;
        if (bitmap == null || ub == null) {
            i2 = 0;
            bitmap2 = bitmap;
        } else {
            Lw4.Bi a2 = Lw4.a(bitmap, ub);
            bitmap2 = a2.a;
            i2 = a2.b;
            this.j = i2;
        }
        this.b.setInitialCropWindowRect(null);
        a(bitmap2, 0, null, 1, i2);
    }

    @DexIgnore
    public void a(Uri uri, FilterType filterType) {
        if (uri != null) {
            this.w = filterType;
            WeakReference<Ir5> weakReference = this.Q;
            Ir5 ir5 = weakReference != null ? weakReference.get() : null;
            if (ir5 != null) {
                ir5.cancel(true);
            }
            b();
            this.M = null;
            this.N = 0;
            this.b.setInitialCropWindowRect(null);
            WeakReference<Ir5> weakReference2 = new WeakReference<>(new Ir5(this, uri, filterType));
            this.Q = weakReference2;
            weakReference2.get().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            h();
        }
    }

    @DexIgnore
    public void a(Hr5.Ai ai) {
        this.R = null;
        h();
        e eVar = this.G;
        if (eVar != null) {
            eVar.a(this, new b(this.h, this.H, ai.a, ai.b, ai.c, getCropPoints(), getCropRect(), getWholeImageRect(), this.p, ai.d));
        }
    }

    @DexIgnore
    public void a(Ir5.Ai ai) {
        this.Q = null;
        h();
        if (ai.e == null) {
            this.j = ai.d;
            if (this.i == null) {
                Bitmap bitmap = ai.b;
                this.i = bitmap.copy(bitmap.getConfig(), false);
            }
            a(ai.f, 0, ai.a, ai.c, ai.d);
        }
        i iVar = this.F;
        if (iVar != null) {
            iVar.a(this, ai.a, ai.e);
        }
    }

    @DexIgnore
    public final void a(boolean z2) {
        if (this.h != null && !z2) {
            this.b.a((float) getWidth(), (float) getHeight(), (((float) this.I) * 100.0f) / Lw4.h(this.f), (((float) this.I) * 100.0f) / Lw4.d(this.f));
        }
        this.b.a(z2 ? null : this.e, getWidth(), getHeight());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e1  */
    public final void a(boolean z2, boolean z3) {
        float f2;
        int width = getWidth();
        int height = getHeight();
        if (this.h != null && width > 0 && height > 0) {
            RectF cropWindowRect = this.b.getCropWindowRect();
            if (z2) {
                if (cropWindowRect.left < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || cropWindowRect.top < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || cropWindowRect.right > ((float) width) || cropWindowRect.bottom > ((float) height)) {
                    a((float) width, (float) height, false, false);
                }
            } else if (this.A || this.J > 1.0f) {
                if (this.J < ((float) this.B)) {
                    float f3 = (float) width;
                    if (cropWindowRect.width() < f3 * 0.5f) {
                        float f4 = (float) height;
                        if (cropWindowRect.height() < 0.5f * f4) {
                            f2 = Math.min((float) this.B, Math.min(f3 / ((cropWindowRect.width() / this.J) / 0.64f), f4 / ((cropWindowRect.height() / this.J) / 0.64f)));
                            if (this.J > 1.0f) {
                                float f5 = (float) width;
                                if (cropWindowRect.width() > 0.65f * f5 || cropWindowRect.height() > ((float) height) * 0.65f) {
                                    f2 = Math.max(1.0f, Math.min(f5 / ((cropWindowRect.width() / this.J) / 0.51f), ((float) height) / ((cropWindowRect.height() / this.J) / 0.51f)));
                                }
                            }
                            if (!this.A) {
                                f2 = 1.0f;
                            }
                            if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && f2 != this.J) {
                                if (z3) {
                                    if (this.g == null) {
                                        this.g = new Kr5(this.a, this.b);
                                    }
                                    this.g.b(this.e, this.c);
                                }
                                this.J = f2;
                                a((float) width, (float) height, true, z3);
                            }
                        }
                    }
                }
                f2 = 0.0f;
                if (this.J > 1.0f) {
                }
                if (!this.A) {
                }
                if (z3) {
                }
                this.J = f2;
                a((float) width, (float) height, true, z3);
            }
            h hVar = this.E;
            if (hVar != null && !z2) {
                hVar.a();
            }
        }
    }

    @DexIgnore
    public final void b() {
        if (this.h != null && (this.u > 0 || this.H != null)) {
            this.h.recycle();
        }
        this.h = null;
        this.u = 0;
        this.H = null;
        this.I = 1;
        this.p = 0;
        this.J = 1.0f;
        this.K = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.L = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.c.reset();
        this.P = null;
        this.a.setImageBitmap(null);
        g();
    }

    @DexIgnore
    public void b(int i2, int i3) {
        this.b.a(i2, i3);
    }

    @DexIgnore
    public void b(int i2, int i3, j jVar) {
        a(i2, i3, jVar, null, null, 0);
    }

    @DexIgnore
    public void c(int i2, int i3) {
        this.b.b(i2, i3);
    }

    @DexIgnore
    public boolean c() {
        return this.b.c();
    }

    @DexIgnore
    public boolean d() {
        return this.q;
    }

    @DexIgnore
    public boolean e() {
        return this.r;
    }

    @DexIgnore
    public final void f() {
        float[] fArr = this.e;
        fArr[0] = 0.0f;
        fArr[1] = 0.0f;
        fArr[2] = (float) this.h.getWidth();
        float[] fArr2 = this.e;
        fArr2[3] = 0.0f;
        fArr2[4] = (float) this.h.getWidth();
        this.e[5] = (float) this.h.getHeight();
        float[] fArr3 = this.e;
        fArr3[6] = 0.0f;
        fArr3[7] = (float) this.h.getHeight();
        this.c.mapPoints(this.e);
        float[] fArr4 = this.f;
        fArr4[0] = 0.0f;
        fArr4[1] = 0.0f;
        fArr4[2] = 100.0f;
        fArr4[3] = 0.0f;
        fArr4[4] = 100.0f;
        fArr4[5] = 100.0f;
        fArr4[6] = 0.0f;
        fArr4[7] = 100.0f;
        this.c.mapPoints(fArr4);
    }

    @DexIgnore
    public final void g() {
        CropOverlayView cropOverlayView = this.b;
        if (cropOverlayView != null) {
            throw null;
//            cropOverlayView.setVisibility((!this.y || this.h == null) ? 4 : 0);
        }
    }

    @DexIgnore
    public Pair<Integer, Integer> getAspectRatio() {
        return new Pair<>(Integer.valueOf(this.b.getAspectRatioX()), Integer.valueOf(this.b.getAspectRatioY()));
    }

    @DexIgnore
    public Bitmap getBitmap() {
        return this.h;
    }

    @DexIgnore
    public float[] getCropPoints() {
        RectF cropWindowRect = this.b.getCropWindowRect();
        float f2 = cropWindowRect.left;
        float f3 = cropWindowRect.top;
        float f4 = cropWindowRect.right;
        float f5 = cropWindowRect.bottom;
        float[] fArr = {f2, f3, f4, f3, f4, f5, f2, f5};
        this.c.invert(this.d);
        this.d.mapPoints(fArr);
        for (int i2 = 0; i2 < 8; i2++) {
            fArr[i2] = fArr[i2] * ((float) this.I);
        }
        return fArr;
    }

    @DexIgnore
    public Rect getCropRect() {
        int i2 = this.I;
        Bitmap bitmap = this.h;
        if (bitmap == null) {
            return null;
        }
        return Lw4.a(getCropPoints(), bitmap.getWidth() * i2, i2 * bitmap.getHeight(), this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY());
    }

    @DexIgnore
    public c getCropShape() {
        return this.b.getCropShape();
    }

    @DexIgnore
    public RectF getCropWindowRect() {
        CropOverlayView cropOverlayView = this.b;
        if (cropOverlayView == null) {
            return null;
        }
        return cropOverlayView.getCropWindowRect();
    }

    @DexIgnore
    public Bitmap getCroppedImage() {
        throw null;
//        return a(0, 0, j.NONE);
    }

    @DexIgnore
    public void getCroppedImageAsync() {
        throw null;
//        b(0, 0, j.NONE);
    }

    @DexIgnore
    public d getGuidelines() {
        return this.b.getGuidelines();
    }

    @DexIgnore
    public int getImageResource() {
        return this.u;
    }

    @DexIgnore
    public Uri getImageUri() {
        return this.H;
    }

    @DexIgnore
    public Bitmap getInitializeBitmap() {
        return this.i;
    }

    @DexIgnore
    public int getLoadedSampleSize() {
        return this.I;
    }

    @DexIgnore
    public int getMaxZoom() {
        return this.B;
    }

    @DexIgnore
    public int getRotatedDegrees() {
        return this.p;
    }

    @DexIgnore
    public k getScaleType() {
        return this.v;
    }

    @DexIgnore
    public Rect getWholeImageRect() {
        int i2 = this.I;
        Bitmap bitmap = this.h;
        if (bitmap == null) {
            return null;
        }
        return new Rect(0, 0, bitmap.getWidth() * i2, i2 * bitmap.getHeight());
    }

    @DexIgnore
    public final void h() {
        if (!this.z) {
            return;
        }
        if (this.h != null || this.Q == null) {
            WeakReference<Hr5> weakReference = this.R;
        }
    }

    @DexReplace
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        super.onLayout(z2, i2, i3, i4, i5);
        if (this.s <= 0 || this.t <= 0) {
            a(true);
            return;
        }
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.width = this.s;
        layoutParams.height = this.t;
        setLayoutParams(layoutParams);
        if (this.h != null) {
            float f2 = (float) (i4 - i2);
            float f3 = (float) (i5 - i3);
            a(f2, f3, true, false);
            if (this.M != null) {
                int i6 = this.N;
                if (i6 != this.j) {
                    this.p = i6;
                    a(f2, f3, true, false);
                }
                this.c.mapRect(this.M);
                this.b.setCropWindowRect(this.M);
                a(false, false);
                this.b.a();
                this.M = null;
            } else if (this.O) {
                this.O = false;
                a(false, false);
            }
        } else {
            a(true);
        }
        this.b.maximiseCrop();
    }


    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        super.onMeasure(i2, i3);
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        Bitmap bitmap = this.h;
        if (bitmap != null) {
            if (size2 == 0) {
                size2 = bitmap.getHeight();
            }
            double width = size < this.h.getWidth() ? ((double) size) / ((double) this.h.getWidth()) : Double.POSITIVE_INFINITY;
            double height = size2 < this.h.getHeight() ? ((double) size2) / ((double) this.h.getHeight()) : Double.POSITIVE_INFINITY;
            if (width == Double.POSITIVE_INFINITY && height == Double.POSITIVE_INFINITY) {
                i5 = this.h.getWidth();
                i4 = this.h.getHeight();
            } else if (width <= height) {
                i4 = (int) (width * ((double) this.h.getHeight()));
                i5 = size;
            } else {
                i5 = (int) (((double) this.h.getWidth()) * height);
                i4 = size2;
            }
            int a2 = a(mode, size, i5);
            int a3 = a(mode2, size2, i4);
            this.s = a2;
            this.t = a3;
            setMeasuredDimension(a2, a3);
            return;
        }
        setMeasuredDimension(size, size2);
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            if (this.Q == null && this.H == null && this.h == null && this.u == 0) {
                FilterType filterType = (FilterType) bundle.getSerializable("FILTER_TYPE");
                Uri uri = (Uri) bundle.getParcelable("LOADED_IMAGE_URI");
                if (uri != null) {
                    String string = bundle.getString("LOADED_IMAGE_STATE_BITMAP_KEY");
                    if (string != null) {
                        Pair<String, WeakReference<Bitmap>> pair = Lw4.g;
                        Bitmap bitmap = (pair == null || !((String) pair.first).equals(string)) ? null : (Bitmap) ((WeakReference) Lw4.g.second).get();
                        Lw4.g = null;
                        if (bitmap != null && !bitmap.isRecycled()) {
                            a(bitmap, 0, uri, bundle.getInt("LOADED_SAMPLE_SIZE"), 0);
                        }
                    }
                    if (this.H == null) {
                        a(uri, filterType);
                    }
                } else {
                    int i2 = bundle.getInt("LOADED_IMAGE_RESOURCE");
                    if (i2 > 0) {
                        setImageResource(i2);
                    } else {
                        Uri uri2 = (Uri) bundle.getParcelable("LOADING_IMAGE_URI");
                        if (uri2 != null) {
                            a(uri2, filterType);
                        }
                    }
                }
                int i3 = bundle.getInt("DEGREES_ROTATED");
                this.N = i3;
                this.p = i3;
                Rect rect = (Rect) bundle.getParcelable("INITIAL_CROP_RECT");
                if (rect != null && (rect.width() > 0 || rect.height() > 0)) {
                    this.b.setInitialCropWindowRect(rect);
                }
                RectF rectF = (RectF) bundle.getParcelable("CROP_WINDOW_RECT");
                if (rectF != null && (rectF.width() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || rectF.height() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
                    this.M = rectF;
                }
                throw null;
//                this.b.setCropShape(c.valueOf(bundle.getString("CROP_SHAPE")));
//                this.A = bundle.getBoolean("CROP_AUTO_ZOOM_ENABLED");
//                this.B = bundle.getInt("CROP_MAX_ZOOM");
//                this.q = bundle.getBoolean("CROP_FLIP_HORIZONTALLY");
//                this.r = bundle.getBoolean("CROP_FLIP_VERTICALLY");
            }
            super.onRestoreInstanceState(bundle.getParcelable("instanceState"));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Ir5 ir5;
        if (this.H == null && this.h == null && this.u < 1) {
            return super.onSaveInstanceState();
        }
        Bundle bundle = new Bundle();
        Uri uri = this.H;
        if (this.x && uri == null && this.u < 1) {
            uri = Lw4.a(getContext(), this.h, this.P);
            this.P = uri;
        }
        if (!(uri == null || this.h == null)) {
            String uuid = UUID.randomUUID().toString();
            Lw4.g = new Pair<>(uuid, new WeakReference(this.h));
            bundle.putString("LOADED_IMAGE_STATE_BITMAP_KEY", uuid);
        }
        WeakReference<Ir5> weakReference = this.Q;
        if (!(weakReference == null || (ir5 = weakReference.get()) == null)) {
            bundle.putParcelable("LOADING_IMAGE_URI", ir5.a());
        }
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putParcelable("LOADED_IMAGE_URI", uri);
        bundle.putSerializable("FILTER_TYPE", this.w);
        bundle.putInt("LOADED_IMAGE_RESOURCE", this.u);
        bundle.putInt("LOADED_SAMPLE_SIZE", this.I);
        bundle.putInt("DEGREES_ROTATED", this.p);
        bundle.putParcelable("INITIAL_CROP_RECT", this.b.getInitialCropWindowRect());
        Lw4.c.set(this.b.getCropWindowRect());
        this.c.invert(this.d);
        this.d.mapRect(Lw4.c);
        bundle.putParcelable("CROP_WINDOW_RECT", Lw4.c);
        bundle.putString("CROP_SHAPE", this.b.getCropShape().name());
        bundle.putBoolean("CROP_AUTO_ZOOM_ENABLED", this.A);
        bundle.putInt("CROP_MAX_ZOOM", this.B);
        bundle.putBoolean("CROP_FLIP_HORIZONTALLY", this.q);
        bundle.putBoolean("CROP_FLIP_VERTICALLY", this.r);
        return bundle;
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.O = i4 > 0 && i5 > 0;
    }

    @DexIgnore
    public void setAutoZoomEnabled(boolean z2) {
        if (this.A != z2) {
            this.A = z2;
            a(false, false);
            this.b.invalidate();
        }
    }

    @DexIgnore
    public void setCropRect(Rect rect) {
        this.b.setInitialCropWindowRect(rect);
    }

    @DexIgnore
    public void setCropShape(c cVar) {
        this.b.setCropShape(cVar);
    }

    @DexIgnore
    public void setFixedAspectRatio(boolean z2) {
        this.b.setFixedAspectRatio(z2);
    }

    @DexIgnore
    public void setFlippedHorizontally(boolean z2) {
        if (this.q != z2) {
            this.q = z2;
            a((float) getWidth(), (float) getHeight(), true, false);
        }
    }

    @DexIgnore
    public void setFlippedVertically(boolean z2) {
        if (this.r != z2) {
            this.r = z2;
            a((float) getWidth(), (float) getHeight(), true, false);
        }
    }

    @DexIgnore
    public void setGuidelines(d dVar) {
        this.b.setGuidelines(dVar);
    }

    @DexIgnore
    public void setImageBitmap(Bitmap bitmap) {
        this.b.setInitialCropWindowRect(null);
        a(bitmap, 0, null, 1, 0);
    }

    @DexIgnore
    public void setImageResource(int i2) {
        if (i2 != 0) {
            this.b.setInitialCropWindowRect(null);
            a(BitmapFactory.decodeResource(getResources(), i2), i2, null, 1, 0);
        }
    }

    @DexIgnore
    public void setMaxZoom(int i2) {
        if (this.B != i2 && i2 > 0) {
            this.B = i2;
            a(false, false);
            this.b.invalidate();
        }
    }

    @DexIgnore
    public void setMultiTouchEnabled(boolean z2) {
        if (this.b.b(z2)) {
            a(false, false);
            this.b.invalidate();
        }
    }

    @DexIgnore
    public void setOnCropImageCompleteListener(e eVar) {
        this.G = eVar;
    }

    @DexIgnore
    public void setOnCropWindowChangedListener(h hVar) {
        this.E = hVar;
    }

    @DexIgnore
    public void setOnSetCropOverlayMovedListener(f fVar) {
        this.D = fVar;
    }

    @DexIgnore
    public void setOnSetCropOverlayReleasedListener(g gVar) {
        this.C = gVar;
    }

    @DexIgnore
    public void setOnSetImageUriCompleteListener(i iVar) {
        this.F = iVar;
    }

    @DexIgnore
    public void setRotatedDegrees(int i2) {
        int i3 = this.p;
        if (i3 != i2) {
            a(i2 - i3);
        }
    }

    @DexIgnore
    public void setSaveBitmapToInstanceState(boolean z2) {
        this.x = z2;
    }

    @DexIgnore
    public void setScaleType(k kVar) {
        if (kVar != this.v) {
            this.v = kVar;
            this.J = 1.0f;
            this.L = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.K = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.b.f();
            requestLayout();
        }
    }

    @DexIgnore
    public void setShowCropOverlay(boolean z2) {
        if (this.y != z2) {
            this.y = z2;
            g();
        }
    }

    @DexIgnore
    public void setShowProgressBar(boolean z2) {
        if (this.z != z2) {
            this.z = z2;
            h();
        }
    }

    @DexIgnore
    public void setSnapRadius(float f2) {
        if (f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.b.setSnapRadius(f2);
        }
    }
}
