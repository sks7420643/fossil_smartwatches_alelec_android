package com.portfolio.platform.uirenew.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.Zr5;
import com.mapped.Qg6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.Calendar;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

import static android.provider.AlarmClock.ACTION_SET_ALARM;
import static android.provider.AlarmClock.ACTION_SHOW_ALARMS;
import static android.provider.AlarmClock.EXTRA_DAYS;
import static android.provider.AlarmClock.EXTRA_HOUR;
import static android.provider.AlarmClock.EXTRA_MINUTES;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public HomePresenter y;

    @DexIgnore
    HomeActivity(){}

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Context context, Integer num, int i, Object obj) {
            if ((i & 2) != 0) {
                num = 0;
            }
            aVar.a(context, num);
        }

        @SuppressLint("WrongConstant")
        @DexIgnore
        public final void a(Context context, Integer num) {
            Wg6.b(context, "context");
            Intent intent = new Intent(context, HomeActivity.class);
            intent.addFlags(268468224);
            intent.putExtra("OUT_STATE_DASHBOARD_CURRENT_TAB", num);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        HomePresenter homePresenter = this.y;
        if (homePresenter != null) {
            homePresenter.a(i, i2, intent);
        } else {
            Wg6.d("mPresenter");
            throw null;
        }
    }

    @SuppressLint("ResourceType")
    @DexReplace
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().b(2131362149);
        if (homeFragment == null) {
            homeFragment = HomeFragment.y.a();
            a(homeFragment, 2131362149);
        }
        PortfolioApp.get.instance().getIface().a(new Zr5(homeFragment)).a(this);

        // Ref https://github.com/carlosperate/LightUpDroid-Alarm/blob/master/app/src/main/java/com/embeddedlog/LightUpDroid/HandleApiCalls.java
        Intent intent = getIntent();
        if (intent != null) {
            HomePresenter homePresenter = this.y;
            int TAB_ALERTS = 3;
            if (ACTION_SET_ALARM.equals(intent.getAction())) {
                homePresenter.a(TAB_ALERTS); // setTab 3
                Alarm alarm = createAlarm(intent);
                HomeFragment.alarmToAdd = alarm;

            } else if (ACTION_SHOW_ALARMS.equals(intent.getAction())) {
                homePresenter.a(TAB_ALERTS); // setTab 3
                return;
            //} else if (ACTION_SET_TIMER.equals(intent.getAction())) {
            //    handleSetTimer(intent);
            }
        }
        if (bundle != null) {
            HomePresenter homePresenter = this.y;
            if (homePresenter != null) {
                homePresenter.a(bundle.getInt("OUT_STATE_DASHBOARD_CURRENT_TAB", 0));
            } else {
                Wg6.d("mPresenter");
                throw null;
            }
        } else {
            /*Intent intent = getIntent();*/
            if (intent != null && intent.getExtras() != null) {
                Intent intent2 = getIntent();
                if (intent2 != null) {
                    Bundle extras = intent2.getExtras();
                    if (extras == null) {
                        Wg6.a();
                        throw null;
                    } else if (extras.containsKey("OUT_STATE_DASHBOARD_CURRENT_TAB")) {
                        HomePresenter homePresenter2 = this.y;
                        if (homePresenter2 != null) {
                            Intent intent3 = getIntent();
                            if (intent3 != null) {
                                Bundle extras2 = intent3.getExtras();
                                if (extras2 != null) {
                                    homePresenter2.a(extras2.getInt("OUT_STATE_DASHBOARD_CURRENT_TAB", 0));
                                } else {
                                    Wg6.a();
                                    throw null;
                                }
                            } else {
                                Wg6.a();
                                throw null;
                            }
                        } else {
                            Wg6.d("mPresenter");
                            throw null;
                        }
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            }
        }
    }

    @DexAdd
    Alarm createAlarm(Intent intent) {
        final int hour = intent.getIntExtra(EXTRA_HOUR, -1);
        final int minutes;
        if (intent.hasExtra(EXTRA_MINUTES)) {
            minutes = intent.getIntExtra(EXTRA_MINUTES, -1);
        } else {
            minutes = 0;
        }

        StringBuilder var12 = new StringBuilder();
        String uri = "__autoadd__";
        var12.append(uri);
        var12.append(':');
        Calendar var9 = Calendar.getInstance();
        String createdAt = TimeUtils.f(var9.getTime());
        var9 = Calendar.getInstance();
        Wg6.a(var9, "Calendar.getInstance()");
        var12.append(var9.getTimeInMillis());
        uri = var12.toString();


        // TODO get title / message from assistant?
        String title = "Alarm";
        String message = "";

        int[] days;
        days = getDaysFromIntent(intent);

        boolean isRepeated = days.length != 0;

        Boolean isActive = true;
        // wg6.a(var3, "createdAt");
        String updatedAt = createdAt;
        int pinType = 0;
        Alarm var19 = new Alarm((String)null, uri, title, message, hour, minutes % 60, days, isActive, isRepeated, createdAt, updatedAt, pinType);//, 1024, (er4)null);

        return var19;

    }

    @DexAdd
    private int[] getDaysFromIntent(Intent intent) {
        final ArrayList<Integer> days = intent.getIntegerArrayListExtra(EXTRA_DAYS);
        if (days != null) {
            final int[] daysArray = new int[days.size()];
            for (int i = 0; i < days.size(); i++) {
                daysArray[i] = days.get(i);
            }
            return daysArray;
        } else {
            // API says to use an ArrayList<Integer> but we allow the user to use a int[] too.
            final int[] daysArray = intent.getIntArrayExtra(EXTRA_DAYS);
            if (daysArray == null) {
                return new int[0];
            }
            return daysArray;
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.b(bundle, "outState");
        HomePresenter homePresenter = this.y;
        if (homePresenter != null) {
            bundle.putInt("OUT_STATE_DASHBOARD_CURRENT_TAB", homePresenter.h());
            super.onSaveInstanceState(bundle);
            return;
        }
        Wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onStart() {
        super.onStart();
        a(MFDeviceService.class, ButtonService.class);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onStop() {
        super.onStop();
        b(MFDeviceService.class, ButtonService.class);
    }
}
