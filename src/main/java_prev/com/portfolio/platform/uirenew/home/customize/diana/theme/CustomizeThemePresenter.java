package com.portfolio.platform.uirenew.home.customize.diana.theme;

import android.os.Environment;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.Ea7;
import com.fossil.Oe7;
import com.fossil.Q26;
import com.fossil.Qj7;
import com.fossil.R26;
import com.fossil.Ti7;
import com.fossil.Tk7;
import com.fossil.Vh7;
import com.fossil.Xh7;
import com.fossil.Zb7;
import com.fossil.Zd;
import com.fossil.Zi7;
import com.mapped.Ai4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.CustomizeThemeFragment;
import com.mapped.Ff6;
import com.mapped.Hf6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Nc6;
import com.mapped.PermissionUtils;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.WatchFaceHelper;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.mapped.Xj6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeThemePresenter extends Q26 {
    @DexIgnore
    public DianaCustomizeViewModel e;
    @DexIgnore
    public LiveData<List<WatchFace>> f;
    @DexIgnore
    public List<WatchFace> g; // = new ArrayList();
    @DexIgnore
    public int h;
    @DexIgnore
    public String i;
    @DexIgnore
    public DianaPreset j;
    @DexIgnore
    public String k;
    @DexIgnore
    public Zd<String> l; // = new Ci(this);
    @DexIgnore
    public Zd<List<WatchFace>> m; // = new Fi(this);
    @DexIgnore
    public /* final */ R26 n;
    @DexIgnore
    public /* final */ WatchFaceRepository o;
    @DexIgnore
    public /* final */ DianaPresetRepository p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$checkToRemovePhoto$1", f = "CustomizeThemePresenter.kt", l = {241}, m = "invokeSuspend")
    public static final class Bi extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceWrapper $watchFaceWrapper;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$checkToRemovePhoto$1$1", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super DianaPreset>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super DianaPreset> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    return this.this$0.this$0.p.getActivePresetBySerial(PortfolioApp.get.instance().c());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(CustomizeThemePresenter customizeThemePresenter, WatchFaceWrapper watchFaceWrapper, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = customizeThemePresenter;
            this.$watchFaceWrapper = watchFaceWrapper;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$watchFaceWrapper, xe6);
            bi.p$ = (Il6) obj;
            throw null;
//            return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Object a;
            CustomizeThemePresenter customizeThemePresenter;
            Object a2 = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il6 = this.p$;
                if (this.this$0.j == null) {
                    this.this$0.n.b();
                    CustomizeThemePresenter customizeThemePresenter2 = this.this$0;
                    Ti7 d = customizeThemePresenter2.c();
                    Aii aii = new Aii(this, null);
                    this.L$0 = il6;
                    this.L$1 = customizeThemePresenter2;
                    this.label = 1;
                    a = Vh7.a(d, aii, this);
                    if (a == a2) {
                        return a2;
                    }
                    customizeThemePresenter = customizeThemePresenter2;
                } else {
                    CustomizeThemePresenter customizeThemePresenter3 = this.this$0;
                    customizeThemePresenter3.a(customizeThemePresenter3.j, this.$watchFaceWrapper);
                    return Cd6.a;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                Nc6.a(obj);
                customizeThemePresenter = (CustomizeThemePresenter) this.L$1;
                a = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            customizeThemePresenter.j = (DianaPreset) a;
            this.this$0.n.a();
            CustomizeThemePresenter customizeThemePresenter4 = this.this$0;
            customizeThemePresenter4.a(customizeThemePresenter4.j, this.$watchFaceWrapper);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemePresenter a;

        @DexIgnore
        public Ci(CustomizeThemePresenter customizeThemePresenter) {
            this.a = customizeThemePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeThemePresenter", "onLiveDataChanged SelectedCustomizeTheme value=" + str);
            if (str != null) {
                WatchFaceWrapper e = CustomizeThemePresenter.e(this.a).e(str);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("CustomizeThemePresenter", " show watchface " + e);
                if (e != null) {
                    this.a.n.a(str, e.getType());
                    this.a.a(e.getType());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$invalidateBackgroundDataWatchFace$2", f = "CustomizeThemePresenter.kt", l = {FacebookRequestErrorClassification.EC_INVALID_TOKEN}, m = "invokeSuspend")
    public static final class Di extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$invalidateBackgroundDataWatchFace$2$1", f = "CustomizeThemePresenter.kt", l = {FacebookRequestErrorClassification.EC_INVALID_TOKEN}, m = "invokeSuspend")
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Object a = Ff6.a();
                int i = this.label;
                if (i == 0) {
                    Nc6.a(obj);
                    Il6 il6 = this.p$;
                    WatchFaceRepository k = this.this$0.this$0.o;
                    String c = PortfolioApp.get.instance().c();
                    this.L$0 = il6;
                    this.label = 1;
                    if (k.getWatchFacesFromServer(c, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    Nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(CustomizeThemePresenter customizeThemePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = customizeThemePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
//            return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Object a = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il6 = this.p$;
                Ti7 b = Qj7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Vh7.a(b, aii, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                Nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$removePhotoWatchFace$1", f = "CustomizeThemePresenter.kt", l = {Action.Music.MUSIC_END_ACTION}, m = "invokeSuspend")
    public static final class Ei extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceWrapper $watchFaceWrapper;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$removePhotoWatchFace$1$1", f = "CustomizeThemePresenter.kt", l = {215, 223, 228}, m = "invokeSuspend")
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $id;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$removePhotoWatchFace$1$1$2", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
//                    return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
//                    return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Ff6.a();
                    if (this.label == 0) {
                        Nc6.a(obj);
                        this.this$0.this$0.this$0.n.b(this.this$0.this$0.$watchFaceWrapper);
                        this.this$0.this$0.this$0.n.a();
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$removePhotoWatchFace$1$1$3", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Biii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Biii biii = new Biii(this.this$0, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
//                    return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
//                    return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Ff6.a();
                    if (this.label == 0) {
                        Nc6.a(obj);
                        this.this$0.this$0.this$0.n.a();
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, String str, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
                this.$id = str;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$id, xe6);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:16:0x006c  */
            /* JADX WARNING: Removed duplicated region for block: B:25:0x00ee  */
            /* JADX WARNING: Removed duplicated region for block: B:44:0x0190  */
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Il6 il6 = null;
                String c = null;
                List<WatchFace> watchFacesWithType = null;
                List<WatchFace> list;
                String str;
                Il6 il62;
                Tk7 h;
                Aiii aiii;
                Tk7 h2;
                Biii biii;
                Exception exc;
                Object a = Ff6.a();
                int i = this.label;
                if (i == 0) {
                    Nc6.a(obj);
                    il6 = this.p$;
                    this.this$0.this$0.i = this.$id;
                    c = PortfolioApp.get.instance().c();
                    watchFacesWithType = this.this$0.this$0.o.getWatchFacesWithType(c, Ai4.BACKGROUND);
                    if (!watchFacesWithType.isEmpty()) {
                        String id = ((WatchFace) Ea7.d((List) watchFacesWithType)).getId();
                        List<DianaPreset> allPresets = this.this$0.this$0.p.getAllPresets(c);
                        Oe7 oe7 = new Oe7();
                        oe7.element = false;
                        for (DianaPreset t : allPresets) {
                            if (Wg6.a((Object) t.getWatchFaceId(), (Object) this.$id)) {
                                oe7.element = true;
                                t.setWatchFaceId(id);
                            }
                        }
                        if (oe7.element) {
                            DianaPresetRepository f = this.this$0.this$0.p;
                            this.L$0 = il6;
                            this.L$1 = c;
                            this.L$2 = watchFacesWithType;
                            this.L$3 = id;
                            this.L$4 = allPresets;
                            this.L$5 = oe7;
                            this.label = 1;
                            if (f.upsertPresetList(allPresets, this) == a) {
                                return a;
                            }
                            list = watchFacesWithType;
                            str = c;
                            watchFacesWithType = list;
                            c = str;
                        }
                    }
                    this.this$0.this$0.o.deleteWatchFace(this.$id, Ai4.PHOTO);
                    String str2 = this.$id + Constants.PHOTO_IMAGE_NAME_SUFFIX;
                    String str3 = this.$id + Constants.PHOTO_BINARY_NAME_SUFFIX;
                    this.this$0.this$0.o.deleteBackgroundPhoto(str2, str3);
                    h = this.this$0.this$0.d();
                    aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.L$1 = c;
                    this.L$2 = watchFacesWithType;
                    this.L$3 = str2;
                    this.L$4 = str3;
                    this.label = 2;
                    if (Vh7.a(h, aiii, this) == a) {
                    }
                    return Cd6.a;
                } else if (i == 1) {
                    Oe7 oe72 = (Oe7) this.L$5;
                    List list2 = (List) this.L$4;
                    String str4 = (String) this.L$3;
                    list = (List) this.L$2;
                    String str5 = (String) this.L$1;
                    Il6 il63 = (Il6) this.L$0;
                    try {
                        Nc6.a(obj);
                        str = str5;
                        il6 = il63;
                        watchFacesWithType = list;
                        c = str;
                    } catch (Exception e) {
                        e = e;
                        il62 = il63;
                        h2 = this.this$0.this$0.d();
                        biii = new Biii(this, null);
                        this.L$0 = il62;
                        this.L$1 = e;
                        this.label = 3;
                        if (Vh7.a(h2, biii, this) != a) {
                        }
                    }
                    try {
                        this.this$0.this$0.o.deleteWatchFace(this.$id, Ai4.PHOTO);
                        String str22 = this.$id + Constants.PHOTO_IMAGE_NAME_SUFFIX;
                        String str32 = this.$id + Constants.PHOTO_BINARY_NAME_SUFFIX;
                        this.this$0.this$0.o.deleteBackgroundPhoto(str22, str32);
                        h = this.this$0.this$0.d();
                        aiii = new Aiii(this, null);
                        this.L$0 = il6;
                        this.L$1 = c;
                        this.L$2 = watchFacesWithType;
                        this.L$3 = str22;
                        this.L$4 = str32;
                        this.label = 2;
                        if (Vh7.a(h, aiii, this) == a) {
                            return a;
                        }
                    } catch (Exception e2) {
                        Exception e = e2;
                        il62 = il6;
                        h2 = this.this$0.this$0.d();
                        biii = new Biii(this, null);
                        this.L$0 = il62;
                        this.L$1 = e;
                        this.label = 3;
                        if (Vh7.a(h2, biii, this) != a) {
                            return a;
                        }
                        exc = e;
                        FLogger.INSTANCE.getLocal().e("CustomizeThemePresenter", exc.getMessage());
                        exc.printStackTrace();
                        return Cd6.a;
                    }
                    return Cd6.a;
                } else if (i == 2) {
                    String str6 = (String) this.L$4;
                    String str7 = (String) this.L$3;
                    List list3 = (List) this.L$2;
                    String str8 = (String) this.L$1;
                    il62 = (Il6) this.L$0;
                    try {
                        Nc6.a(obj);
                    } catch (Exception e3) {
                        Exception e = e3;
                    }
                    return Cd6.a;
                } else if (i == 3) {
                    exc = (Exception) this.L$1;
                    Il6 il64 = (Il6) this.L$0;
                    Nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                FLogger.INSTANCE.getLocal().e("CustomizeThemePresenter", exc.getMessage());
                exc.printStackTrace();
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(CustomizeThemePresenter customizeThemePresenter, WatchFaceWrapper watchFaceWrapper, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = customizeThemePresenter;
            this.$watchFaceWrapper = watchFaceWrapper;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$watchFaceWrapper, xe6);
            ei.p$ = (Il6) obj;
            throw null;
//            return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Object a = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il6 = this.p$;
                this.this$0.n.b();
                String id = this.$watchFaceWrapper.getId();
                Ti7 d = this.this$0.c();
                Aii aii = new Aii(this, id, null);
                this.L$0 = il6;
                this.L$1 = id;
                this.label = 1;
                if (Vh7.a(d, aii, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                String str = (String) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                Nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Zd<List<? extends WatchFace>> {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemePresenter a;

        @DexIgnore
        public Fi(CustomizeThemePresenter customizeThemePresenter) {
            this.a = customizeThemePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<? extends WatchFace> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeThemePresenter", "loadWatchFace " + list);
            CustomizeThemePresenter customizeThemePresenter = this.a;
            Wg6.a((Object) list, "it");
            customizeThemePresenter.b((List<WatchFace>)list);
            this.a.n.v(true);
        }

    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$showCurrentWatchFace$1", f = "CustomizeThemePresenter.kt", l = {150, 151}, m = "invokeSuspend")
    public static final class Gi extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $watchFaceList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$showCurrentWatchFace$1$backgroundWrappers$1", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super List<? extends WatchFaceWrapper>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $backgroundList;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Gi gi, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = gi;
                this.$backgroundList = list;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$backgroundList, xe6);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends WatchFaceWrapper>> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    List list = this.$backgroundList;
                    DianaPreset a = CustomizeThemePresenter.e(this.this$0.this$0).a().a();
                    return WatchFaceHelper.a(list, a != null ? a.getComplications() : null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$showCurrentWatchFace$1$photoWrappers$1", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Zb7 implements Coroutine<Il6, Xe6<? super List<? extends WatchFaceWrapper>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $photoList;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Gi gi, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = gi;
                this.$photoList = list;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Bii bii = new Bii(this.this$0, this.$photoList, xe6);
                bii.p$ = (Il6) obj;
                throw null;
//                return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends WatchFaceWrapper>> xe6) {
                throw null;
//                return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    List list = this.$photoList;
                    DianaPreset a = CustomizeThemePresenter.e(this.this$0.this$0).a().a();
                    return WatchFaceHelper.a(list, a != null ? a.getComplications() : null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(CustomizeThemePresenter customizeThemePresenter, List list, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = customizeThemePresenter;
            this.$watchFaceList = list;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Gi gi = new Gi(this.this$0, this.$watchFaceList, xe6);
            gi.p$ = (Il6) obj;
            throw null;
//            return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v71, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00a9  */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x01dd  */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            boolean z;
            ArrayList arrayList;
            Object a;
            Il6 il6;
            List list;
            String watchFaceId;
            Object a2;
            List<WatchFaceWrapper> list2;
            WatchFaceWrapper e;
            Object a3 = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il62 = this.p$;
                FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "showCurrentWatchFace cacheList " + this.this$0.g + " updatelist " + this.$watchFaceList);
                if (Wg6.a(this.this$0.g, this.$watchFaceList)) {
                    return Cd6.a;
                }
                List list3 = this.$watchFaceList;
                ArrayList arrayList2 = new ArrayList();
                for (Object obj2 : list3) {
                    if (Hf6.a(((WatchFace) obj2).getWatchFaceType() == Ai4.PHOTO.getValue()).booleanValue()) {
                        arrayList2.add(obj2);
                    }
                }
                if (arrayList2.size() >= this.this$0.h) {
                    z = true;
                } else if (arrayList2.isEmpty()) {
                    DianaPreset a4 = this.this$0.j;
                    if (!(a4 == null || (watchFaceId = a4.getWatchFaceId()) == null)) {
                        this.this$0.a(watchFaceId);
                    }
                    this.this$0.g = this.$watchFaceList;
                    return Cd6.a;
                } else {
                    String c = this.this$0.i;
                    DianaPreset a5 = CustomizeThemePresenter.e(this.this$0).a().a();
                    z = Wg6.a((Object) c, (Object) (a5 != null ? a5.getWatchFaceId() : null));
                }
                this.this$0.h = arrayList2.size();
                if (z) {
                    FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "showCurrentWatchFace() needUpdate == true");
                    List list4 = this.$watchFaceList;
                    arrayList = new ArrayList();
                    for (Object obj3 : list4) {
                        if (Hf6.a(((WatchFace) obj3).getWatchFaceType() == Ai4.BACKGROUND.getValue()).booleanValue()) {
                            arrayList.add(obj3);
                        }
                    }
                    Ti7 d = this.this$0.c();
                    Aii aii = new Aii(this, arrayList, null);
                    this.L$0 = il62;
                    this.L$1 = arrayList2;
                    this.L$2 = arrayList;
                    this.label = 1;
                    a = Vh7.a(d, aii, this);
                    if (a == a3) {
                        return a3;
                    }
                    il6 = il62;
                    list = arrayList2;
                    List<WatchFaceWrapper> list5 = (List) a;
                    Ti7 d2 = this.this$0.c();
                    Bii bii = new Bii(this, list, null);
                    this.L$0 = il6;
                    this.L$1 = list;
                    this.L$2 = arrayList;
                    this.L$3 = list5;
                    this.label = 2;
                    a2 = Vh7.a(d2, bii, this);
                    if (a2 != a3) {
                    }
                }
                return Cd6.a;
            } else if (i == 1) {
                list = (List) this.L$1;
                il6 = (Il6) this.L$0;
                Nc6.a(obj);
                arrayList = (ArrayList) this.L$2;
                a = obj;
                List<WatchFaceWrapper> list52 = (List) a;
                Ti7 d22 = this.this$0.c();
                Bii bii2 = new Bii(this, list, null);
                this.L$0 = il6;
                this.L$1 = list;
                this.L$2 = arrayList;
                this.L$3 = list52;
                this.label = 2;
                a2 = Vh7.a(d22, bii2, this);
                if (a2 != a3) {
                    return a3;
                }
                list2 = list52;
            } else if (i == 2) {
                List list6 = (List) this.L$2;
                List list7 = (List) this.L$1;
                Il6 il63 = (Il6) this.L$0;
                Nc6.a(obj);
                a2 = obj;
                list2 = (List) this.L$3;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List<WatchFaceWrapper> list8 = (List) a2;
            this.this$0.a(list2);
            this.this$0.n.a(list2, Ai4.BACKGROUND);
            this.this$0.n.a(list8, Ai4.PHOTO);
            if (this.this$0.g.size() <= this.$watchFaceList.size()) {
                String a6 = CustomizeThemePresenter.e(this.this$0).g().a();
                if (!(a6 == null || (e = CustomizeThemePresenter.e(this.this$0).e(a6)) == null)) {
                    this.this$0.n.a(a6, e.getType());
                    this.this$0.a(e.getType());
                }
            } else if (!list8.isEmpty()) {
                this.this$0.c((WatchFaceWrapper) Ea7.f((List) list8));
            } else if (!list2.isEmpty()) {
                this.this$0.c((WatchFaceWrapper) Ea7.f((List) list2));
            }
            this.this$0.g = this.$watchFaceList;
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $currentPreset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ WatchFace $activeWatchFace;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Hi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Hi hi, WatchFace watchFace, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = hi;
                this.$activeWatchFace = watchFace;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$activeWatchFace, xe6);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    WatchFace watchFace = this.$activeWatchFace;
                    if (watchFace == null || watchFace.getWatchFaceType() != Ai4.PHOTO.getValue()) {
                        this.this$0.this$0.n.G0();
                    } else {
                        this.this$0.this$0.n.O0();
                    }
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(DianaPreset dianaPreset, Xe6 xe6, CustomizeThemePresenter customizeThemePresenter) {
            super(2, xe6);
            this.$currentPreset = dianaPreset;
            this.this$0 = customizeThemePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Hi hi = new Hi(this.$currentPreset, xe6, this.this$0);
            hi.p$ = (Il6) obj;
            throw null;
//            return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Object a = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il6 = this.p$;
                CustomizeThemePresenter customizeThemePresenter = this.this$0;
                customizeThemePresenter.j = customizeThemePresenter.p.getActivePresetBySerial(PortfolioApp.get.instance().c());
                String watchFaceId = this.$currentPreset.getWatchFaceId();
                WatchFace watchFaceWithId = this.this$0.o.getWatchFaceWithId(watchFaceId);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("CustomizeThemePresenter", "currentPreset observer activeWatchFace " + watchFaceWithId);
                Tk7 c = Qj7.c();
                Aii aii = new Aii(this, watchFaceWithId, null);
                this.L$0 = il6;
                this.L$1 = watchFaceId;
                this.L$2 = watchFaceWithId;
                this.label = 1;
                if (Vh7.a(c, aii, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                WatchFace watchFace = (WatchFace) this.L$2;
                String str = (String) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                Nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$updateSelectedPhotoWatchFaces$1", f = "CustomizeThemePresenter.kt", l = {108, 110}, m = "invokeSuspend")
    public static final class Ii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $watchFaceId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super WatchFaceWrapper>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ WatchFace $it;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ii this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(WatchFace watchFace, Xe6 xe6, Ii ii) {
                super(2, xe6);
                this.$it = watchFace;
                this.this$0 = ii;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.$it, xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super WatchFaceWrapper> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    WatchFace watchFace = this.$it;
                    DianaPreset a = CustomizeThemePresenter.e(this.this$0.this$0).a().a();
                    return WatchFaceHelper.b(watchFace, a != null ? a.getComplications() : null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$updateSelectedPhotoWatchFaces$1$watchFace$1", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Zb7 implements Coroutine<Il6, Xe6<? super WatchFace>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ii this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ii ii, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ii;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
//                return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super WatchFace> xe6) {
                throw null;
//                return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    return this.this$0.this$0.o.getWatchFaceWithId(this.this$0.$watchFaceId);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(CustomizeThemePresenter customizeThemePresenter, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = customizeThemePresenter;
            this.$watchFaceId = str;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Ii ii = new Ii(this.this$0, this.$watchFaceId, xe6);
            ii.p$ = (Il6) obj;
            throw null;
//            return ii;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Ii) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x005c  */
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Object a;
            Il6 il6;
            WatchFace watchFace;
            Object a2;
            Object a3 = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il62 = this.p$;
                if (!Wg6.a((Object) this.this$0.k, (Object) this.$watchFaceId)) {
                    this.this$0.k = this.$watchFaceId;
                    Ti7 d = this.this$0.c();
                    Bii bii = new Bii(this, null);
                    this.L$0 = il62;
                    this.label = 1;
                    a = Vh7.a(d, bii, this);
                    if (a == a3) {
                        return a3;
                    }
                    il6 = il62;
                    watchFace = (WatchFace) a;
                    if (watchFace != null) {
                    }
                }
                return Cd6.a;
            } else if (i == 1) {
                Nc6.a(obj);
                il6 = (Il6) this.L$0;
                a = obj;
                watchFace = (WatchFace) a;
                if (watchFace != null) {
                    Ti7 d2 = this.this$0.c();
                    Aii aii = new Aii(watchFace, null, this);
                    this.L$0 = il6;
                    this.L$1 = watchFace;
                    this.L$2 = watchFace;
                    this.label = 2;
                    a2 = Vh7.a(d2, aii, this);
                    if (a2 == a3) {
                        return a3;
                    }
                }
                return Cd6.a;
            } else if (i == 2) {
                WatchFace watchFace2 = (WatchFace) this.L$2;
                WatchFace watchFace3 = (WatchFace) this.L$1;
                Il6 il63 = (Il6) this.L$0;
                Nc6.a(obj);
                a2 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            WatchFaceWrapper watchFaceWrapper = (WatchFaceWrapper) a2;
            if (!CustomizeThemePresenter.e(this.this$0).j().contains(watchFaceWrapper)) {
                CustomizeThemePresenter.e(this.this$0).j().add(watchFaceWrapper);
            }
            this.this$0.c(watchFaceWrapper);
            return Cd6.a;
        }
    }

    /*
    static {
        new Ai(null);
    }
    */

    @DexIgnore
    public CustomizeThemePresenter(R26 r26, WatchFaceRepository watchFaceRepository, DianaPresetRepository dianaPresetRepository) {
        Wg6.b(r26, "mView");
        Wg6.b(watchFaceRepository, "watchFaceRepository");
        Wg6.b(dianaPresetRepository, "mDianaPresetRepository");
        this.n = r26;
        this.o = watchFaceRepository;
        this.p = dianaPresetRepository;
    }

    @DexIgnore
    public static final /* synthetic */ DianaCustomizeViewModel e(CustomizeThemePresenter customizeThemePresenter) {
        DianaCustomizeViewModel dianaCustomizeViewModel = customizeThemePresenter.e;
        if (dianaCustomizeViewModel != null) {
            return dianaCustomizeViewModel;
        }
        Wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final void a(Ai4 ai4) {
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "navigateToActiveWatchFace()");
        String str = this.i;
        throw null;
//        if (!(str == null || Xj6.a(str))) {
//            this.i = null;
//            this.n.O0();
//        } else if (ai4 == Ai4.BACKGROUND) {
//            this.n.G0();
//        } else if (ai4 == Ai4.PHOTO) {
//            this.n.O0();
//        }
    }

    @DexIgnore
    public final void a(DianaPreset dianaPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "updateCurrentPreset=" + dianaPreset);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            dianaCustomizeViewModel.a(dianaPreset);
        } else {
            Wg6.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void a(DianaPreset dianaPreset, WatchFaceWrapper watchFaceWrapper) {
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "showRemovePhotoDialog()");
        if (Wg6.a((Object) (dianaPreset != null ? dianaPreset.getWatchFaceId() : null), (Object) watchFaceWrapper.getId())) {
            this.n.g0();
        } else {
            this.n.z0();
        }
    }

    @DexIgnore
    @Override // com.fossil.Q26
    public void a(WatchFaceWrapper watchFaceWrapper) {
        Wg6.b(watchFaceWrapper, "watchFaceWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "checkToRemovePhoto watchFaceWrapper = " + watchFaceWrapper);
        throw null;
//        Rm6 unused = Xh7.b(e(), null, null, new Bi(this, watchFaceWrapper, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Q26
    public void a(DianaCustomizeViewModel dianaCustomizeViewModel) {
        Wg6.b(dianaCustomizeViewModel, "viewModel");
        this.e = dianaCustomizeViewModel;
    }

    @DexIgnore
    @Override // com.fossil.Q26
    public void a(String str) {
        Wg6.b(str, "watchFaceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "updateSelectedPhotoWatchFaces - watchFaceId=" + str);
        throw null;
//        Rm6 unused = Xh7.b(e(), null, null, new Ii(this, str, null), 3, null);
    }

    @DexIgnore
    public final void a(List<WatchFaceWrapper> list) {
        boolean z = false;
        for (WatchFaceWrapper t : list) {
            if (t.getBackground() != null || t.getCombination() == null || t.getTopComplication() == null || t.getBottomComplication() == null || t.getLeftComplication() == null || t.getRightComplication() == null) {
                z = true;
            }
        }
        if (z) {
            FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "Some watchface files are missing, retry download for those files");
            throw null;
//            Rm6 unused = Xh7.b(e(), null, null, new Di(this, null), 3, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Q26
    public void b(WatchFaceWrapper watchFaceWrapper) {
        Wg6.b(watchFaceWrapper, "watchFaceWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "removePhotoWatchFace, watchFaceWrapper = " + watchFaceWrapper);
        throw null;
//        Rm6 unused = Xh7.b(e(), null, null, new Ei(this, watchFaceWrapper, null), 3, null);
    }

    @DexIgnore
    public final void b(List<WatchFace> list) {
        throw null;
//        Rm6 unused = Xh7.b(e(), null, null, new Gi(this, list, null), 3, null);
    }

    @DexReplace
    @Override // com.fossil.Q26
    public void c(WatchFaceWrapper watchFaceWrapper) {
        Wg6.b(watchFaceWrapper, "watchFaceWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "setWatchFace " + watchFaceWrapper);

        try {
            File file = new File(Environment.getExternalStoragePublicDirectory("Fossil"), watchFaceWrapper.getId() + ".png");
            if (!file.exists()) {
                CustomizeThemeFragment frag = (CustomizeThemeFragment)this.n;
                // Ask for storage permissions if not already granted
                PermissionUtils.a.c(frag.getActivity(), 255); // function with: "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"

                File dir = file.getParentFile();
                if (!dir.exists()) {
                    dir.mkdir();
                }
                String WatchFaceDir = FileUtils.getDirectory(PortfolioApp.get.instance().getApplicationContext(), FileType.WATCH_FACE);
                File source = new File( WatchFaceDir + File.separator + watchFaceWrapper.getId());

                if (!source.exists()) {
                    for (File f: new File(WatchFaceDir).listFiles()) {
                        if (f.getName().contains(watchFaceWrapper.getId()) && f.getName().toLowerCase().endsWith(".png")) {
                            source = f;
                            break;
                        }
                    }
                }
                if (source.exists()) {
                    InputStream in = new FileInputStream(source);
                    OutputStream out = new FileOutputStream(file);

                    // Copy the bits from instream to outstream
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();
                }
            }
        } catch (NullPointerException | IOException ex) {
            ex.printStackTrace();
        }

        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            DianaPreset a = dianaCustomizeViewModel.a().a();
            if (a != null) {
                a.setWatchFaceId(watchFaceWrapper.getId());
                Wg6.a((Object) a, "currentPreset");
                a(a);
                return;
            }
            return;
        }
        Wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "onStart");
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            MutableLiveData<String> g2 = dianaCustomizeViewModel.g();
            R26 r26 = this.n;
            if (r26 != null) {
                g2.a((CustomizeThemeFragment) r26, this.l);
                LiveData<List<WatchFace>> watchFacesLiveDataWithSerial = this.o.getWatchFacesLiveDataWithSerial(PortfolioApp.get.instance().c());
                this.f = watchFacesLiveDataWithSerial;
                if (watchFacesLiveDataWithSerial != null) {
                    watchFacesLiveDataWithSerial.a((LifecycleOwner) this.n, this.m);
                }
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.e;
                if (dianaCustomizeViewModel2 != null) {
                    DianaPreset a = dianaCustomizeViewModel2.a().a();
                    if (a != null) {
                        throw null;
//                        Rm6 unused = Xh7.b(Zi7.a(Qj7.b()), null, null, new Hi(a, null, this), 3, null);
//                        return;
                    }
                    return;
                }
                Wg6.d("mDianaCustomizeViewModel");
                throw null;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment");
        }
        Wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "stop()");
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            dianaCustomizeViewModel.g().b(this.l);
            LiveData<List<WatchFace>> liveData = this.f;
            if (liveData != null) {
                liveData.b(this.m);
                return;
            }
            return;
        }
        Wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexReplace
    @Override // com.fossil.Q26
    public void h() {
        ArrayList arrayList;
        List<WatchFace> a;
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "checkAddingPhoto()");
        LiveData<List<WatchFace>> liveData = this.f;
        if (liveData == null || (a = liveData.a()) == null) {
            arrayList = null;
        } else {
            ArrayList arrayList2 = new ArrayList();
            for (WatchFace t : a) {
                if (t.getWatchFaceType() == Ai4.PHOTO.getValue()) {
                    arrayList2.add(t);
                }
            }
            arrayList = arrayList2;
        }
        /* Remove the 20 watch-face limit */
//        if (arrayList == null || arrayList.size() != 20) {
            this.n.M0();
//        } else {
//            this.n.L();
//        }
    }

    @DexIgnore
    public void i() {
        this.n.a(this);
    }
}
