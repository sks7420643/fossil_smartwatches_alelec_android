package com.portfolio.platform.uirenew.home.customize.diana.theme.preview;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.Bf5;
import com.fossil.Fz5;
import com.fossil.He;
import com.fossil.Je;
import com.fossil.Qb;
import com.fossil.Qw6;
import com.fossil.Rj4;
import com.fossil.Zd;
import com.fossil.imagefilters.FilterType;
import com.fossil.wearables.fossil.R;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.WatchFacePreviewFragmentBinding;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel;
import com.portfolio.platform.view.CustomizeWidget;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexWrap;

@DexEdit(defaultAction = DexAction.ADD, staticConstructorAction = DexAction.APPEND)
public final class PreviewFragment extends BaseFragment {
    @DexIgnore
    public static /* final */ Ai j; // = new Ai(null);
    @DexIgnore
    public Rj4 f;
    @DexIgnore
    public PreviewViewModel g;
    @DexIgnore
    public Qw6<WatchFacePreviewFragmentBinding> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore
    public PreviewFragment() {}

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final PreviewFragment a(ArrayList<Fz5> arrayList, FilterType filterType) {
            Wg6.b(arrayList, "complications");
            Wg6.b(filterType, "filterType");
            PreviewFragment previewFragment = new PreviewFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("COMPLICATIONS_ARG", arrayList);
            bundle.putSerializable("FILTER_TYPE_ARG", filterType);
            previewFragment.setArguments(bundle);
            return previewFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Zd<PreviewViewModel.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ PreviewFragment a;

        @DexIgnore
        public Bi(PreviewFragment previewFragment) {
            this.a = previewFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(PreviewViewModel.Bi bi) {
            ImageView imageView;
            WatchFacePreviewFragmentBinding a2 = this.a.g1();
            if (a2 != null && (imageView = a2.t) != null) {
                imageView.setImageDrawable(bi.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Zd<PreviewViewModel.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ PreviewFragment a;

        @DexIgnore
        public Ci(PreviewFragment previewFragment) {
            this.a = previewFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(PreviewViewModel.Ci ci) {
            int parseColor;
            this.a.a();
            if (ci.c()) {
                FLogger.INSTANCE.getLocal().d("PreviewFragment", "Receive loadComplicationSettingResult Success");
                try {
                    parseColor = Color.parseColor(ci.a());
                } catch (Exception e) {
                    FLogger.INSTANCE.getLocal().e("PreviewFragment", e.getMessage());
                    e.printStackTrace();
                    parseColor = Color.parseColor("#FFFFFF");
                }
                this.a.a(ci.b(), Integer.valueOf(parseColor));
                return;
            }
            FLogger.INSTANCE.getLocal().d("PreviewFragment", "Receive loadComplicationSettingResult Fail");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Zd<PreviewViewModel.Di> {
        @DexIgnore
        public /* final */ /* synthetic */ PreviewFragment a;

        @DexIgnore
        public Di(PreviewFragment previewFragment) {
            this.a = previewFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(PreviewViewModel.Di di) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                this.a.a();
                if (di.b()) {
                    FLogger.INSTANCE.getLocal().d("PreviewFragment", "Receive saveThemeResult Success");
                    Intent intent = new Intent();
                    intent.putExtra("WATCH_FACE_ID", di.a());
                    activity.setResult(-1, intent);
                    activity.finish();
                    return;
                }
                FLogger.INSTANCE.getLocal().d("PreviewFragment", "Receive saveThemeResult Fail");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PreviewFragment a;

        @DexIgnore
        public Ei(PreviewFragment previewFragment) {
            this.a = previewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.e1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PreviewFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public Fi(PreviewFragment previewFragment, int i) {
            this.a = previewFragment;
            this.b = i;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.b();
            PreviewFragment.b(this.a).c(this.b);
        }
    }

    @DexIgnore
    public static final /* synthetic */ PreviewViewModel b(PreviewFragment previewFragment) {
        PreviewViewModel previewViewModel = previewFragment.g;
        if (previewViewModel != null) {
            return previewViewModel;
        }
        Wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void a(Drawable drawable, Integer num) {
        String c;
        FLogger.INSTANCE.getLocal().d("PreviewFragment", "showComplications()");
        WatchFacePreviewFragmentBinding g1 = g1();
        if (g1 != null) {
            PreviewViewModel previewViewModel = this.g;
            if (previewViewModel != null) {
                ArrayList<Fz5> a = previewViewModel.a();
                if (a != null) {
                    for (Fz5 t : a) {
                        if ((!Wg6.a((Object) t.a(), (Object) "empty")) && (c = t.c()) != null) {
                            switch (c.hashCode()) {
                                case -1383228885:
                                    if (c.equals("bottom")) {
                                        CustomizeWidget customizeWidget = g1.x;
                                        Wg6.a((Object) customizeWidget, "it.wcBottom");
                                        a(customizeWidget, t.a(), t.d(), num, drawable);
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 115029:
                                    if (c.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                        CustomizeWidget customizeWidget2 = g1.A;
                                        Wg6.a((Object) customizeWidget2, "it.wcTop");
                                        a(customizeWidget2, t.a(), t.d(), num, drawable);
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 3317767:
                                    if (c.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                        CustomizeWidget customizeWidget3 = g1.z;
                                        Wg6.a((Object) customizeWidget3, "it.wcStart");
                                        a(customizeWidget3, t.a(), t.d(), num, drawable);
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 108511772:
                                    if (c.equals("right")) {
                                        CustomizeWidget customizeWidget4 = g1.y;
                                        Wg6.a((Object) customizeWidget4, "it.wcEnd");
                                        a(customizeWidget4, t.a(), t.d(), num, drawable);
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                        }
                    }
                    return;
                }
                return;
            }
            Wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void a(CustomizeWidget customizeWidget, String str, String str2, Integer num, Drawable drawable) {
        FLogger.INSTANCE.getLocal().d("PreviewFragment", "updateComplicationButton()");
        customizeWidget.setVisibility(View.VISIBLE);
        customizeWidget.b(str);
        if (str2 != null) {
            customizeWidget.setBottomContent(str2);
        }
        if (num != null) {
            customizeWidget.setDefaultColorRes(Integer.valueOf(num.intValue()));
        }
        if (drawable != null) {
            customizeWidget.setBackgroundDrawableCus(drawable);
        }
        customizeWidget.h();
    }

    @DexIgnore
    public final void b(Bundle bundle) {
        Bundle arguments;
        ArrayList<Fz5> parcelableArrayList;
        Serializable serializable = null;
        FLogger.INSTANCE.getLocal().d("PreviewFragment", "initialize()");
        if (!(!bundle.containsKey("COMPLICATIONS_ARG") || (arguments = getArguments()) == null || (parcelableArrayList = arguments.getParcelableArrayList("COMPLICATIONS_ARG")) == null)) {
            PreviewViewModel previewViewModel = this.g;
            if (previewViewModel != null) {
                previewViewModel.a(parcelableArrayList);
                @SuppressLint("ResourceType") int dimensionPixelSize = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165420);
                b();
                PreviewViewModel previewViewModel2 = this.g;
                if (previewViewModel2 != null) {
                    previewViewModel2.b(dimensionPixelSize);
                } else {
                    Wg6.d("mViewModel");
                    throw null;
                }
            } else {
                Wg6.d("mViewModel");
                throw null;
            }
        }
        if (bundle.containsKey("FILTER_TYPE_ARG")) {
            PreviewViewModel previewViewModel3 = this.g;
            if (previewViewModel3 != null) {
                Bundle arguments2 = getArguments();
                if (arguments2 != null) {
                    serializable = arguments2.getSerializable("FILTER_TYPE_ARG");
                }
                if (serializable != null) {
                    previewViewModel3.a((FilterType) serializable);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type com.fossil.imagefilters.FilterType");
            }
            Wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean e1() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null) {
            return true;
        }
        fragmentManager.E();
        return true;
    }

    @DexIgnore
    public final void f1() {
        PreviewViewModel previewViewModel = this.g;
        if (previewViewModel != null) {
            previewViewModel.c().a(getViewLifecycleOwner(), new Bi(this));
            PreviewViewModel previewViewModel2 = this.g;
            if (previewViewModel2 != null) {
                previewViewModel2.d().a(getViewLifecycleOwner(), new Ci(this));
                PreviewViewModel previewViewModel3 = this.g;
                if (previewViewModel3 != null) {
                    previewViewModel3.e().a(getViewLifecycleOwner(), new Di(this));
                } else {
                    Wg6.d("mViewModel");
                    throw null;
                }
            } else {
                Wg6.d("mViewModel");
                throw null;
            }
        } else {
            Wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final WatchFacePreviewFragmentBinding g1() {
        Qw6<WatchFacePreviewFragmentBinding> qw6 = this.h;
        if (qw6 != null) {
            return qw6.a();
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (bundle != null) {
            b(bundle);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.portfolio.platform.uirenew.BaseFragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().s().a(this);
        FragmentActivity requireActivity = requireActivity();
        Rj4 rj4 = this.f;
        if (rj4 != null) {
            He a = Je.a(requireActivity, rj4).a(PreviewViewModel.class);
            Wg6.a((Object) a, "ViewModelProviders.of(re\u2026iewViewModel::class.java)");
            this.g = (PreviewViewModel) a;
            return;
        }
        Wg6.d("viewModelFactory");
        throw null;
    }

    @DexAdd
    private Drawable wc_start_image = null;
    @DexAdd
    private Drawable wc_end_image = null;
    @DexAdd
    private Drawable wc_top_image = null;
    @DexAdd
    private Drawable wc_bottom_image = null;

    public enum SelectedComplication {
        CIRCLE,
        BLANK,
        NONE
    }
    public static SelectedComplication selectedComplication = SelectedComplication.CIRCLE;

    @DexAdd
    private void setComplicationCircle() {
        WatchFacePreviewFragmentBinding binding = g1();
        binding.wc_start.setBackgroundRes(R.drawable.bg_widget_control_transparent);
        binding.wc_end.setBackgroundRes(R.drawable.bg_widget_control_transparent);
        binding.wc_top.setBackgroundRes(R.drawable.bg_widget_control_transparent);
        binding.wc_bottom.setBackgroundRes(R.drawable.bg_widget_control_transparent);

        if (wc_start_image != null) binding.wc_start.setBackgroundDrawableCus(wc_start_image);
        if (wc_end_image != null) binding.wc_end.setBackgroundDrawableCus(wc_end_image);
        if (wc_top_image != null) binding.wc_top.setBackgroundDrawableCus(wc_top_image);
        if (wc_bottom_image != null) binding.wc_bottom.setBackgroundDrawableCus(wc_bottom_image);

        selectedComplication = SelectedComplication.CIRCLE;
    }

    @DexAdd
    private void setComplicationBlank() {
        WatchFacePreviewFragmentBinding binding = g1();
        if (binding.wc_start.k0 != null) wc_start_image = binding.wc_start.k0;
        if (binding.wc_end.k0 != null) wc_end_image = binding.wc_end.k0;
        if (binding.wc_top.k0 != null) wc_top_image = binding.wc_top.k0;
        if (binding.wc_bottom.k0 != null) wc_bottom_image = binding.wc_bottom.k0;

        binding.wc_start.setBackgroundRes(R.drawable.bg_widget_complication_blank);
        binding.wc_end.setBackgroundRes(R.drawable.bg_widget_complication_blank);
        binding.wc_top.setBackgroundRes(R.drawable.bg_widget_complication_blank);
        binding.wc_bottom.setBackgroundRes(R.drawable.bg_widget_complication_blank);

        selectedComplication = SelectedComplication.BLANK;
    }

    @DexAdd
    private void setComplicationNone() {
        WatchFacePreviewFragmentBinding binding = g1();
        if (binding.wc_start.k0 != null) wc_start_image = binding.wc_start.k0;
        if (binding.wc_end.k0 != null) wc_end_image = binding.wc_end.k0;
        if (binding.wc_top.k0 != null) wc_top_image = binding.wc_top.k0;
        if (binding.wc_bottom.k0 != null) wc_bottom_image = binding.wc_bottom.k0;

        binding.wc_start.setBackgroundRes(R.drawable.bg_widget_control_transparent);
        binding.wc_end.setBackgroundRes(R.drawable.bg_widget_control_transparent);
        binding.wc_top.setBackgroundRes(R.drawable.bg_widget_control_transparent);
        binding.wc_bottom.setBackgroundRes(R.drawable.bg_widget_control_transparent);

        selectedComplication = SelectedComplication.NONE;
    }

    @DexWrap
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View ret = onCreateView(layoutInflater, viewGroup, bundle);
        WatchFacePreviewFragmentBinding binding = g1();

        binding.complication_selection.setOnCheckedChangeListener((group, checkedId) -> {
            switch(checkedId) {
                case R.id.tv_complication_circle:
                    PreviewFragment.this.setComplicationCircle();
                    break;
                case R.id.tv_complication_blank:
                    PreviewFragment.this.setComplicationBlank();
                    break;
                case R.id.tv_complication_none:
                    PreviewFragment.this.setComplicationNone();
                    break;
            }
        });

//        binding.complication_selection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
//        {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId)
//            {
//                switch(checkedId) {
//                    case R.id.tv_complication_circle:
//                        PreviewFragment.this.setComplicationCircle();
//                        break;
//                    case R.id.tv_complication_blank:
//                        PreviewFragment.this.setComplicationBlank();
//                        break;
//                    case R.id.tv_complication_none:
//                        PreviewFragment.this.setComplicationNone();
//                        break;
//                }
//            }
//        });
//
//        binding.complication_circle.setOnClickListener(view -> PreviewFragment.this.setComplicationCircle());
//        binding.complication_blank.setOnClickListener(view -> PreviewFragment.this.setComplicationBlank());
//        binding.complication_none.setOnClickListener(view -> PreviewFragment.this.setComplicationNone());

//        binding.complication_circle.setOnCheckedChangeListener((view, isChecked) -> {
//            if (view.isShown() && isChecked) PreviewFragment.this.setComplicationCircle();
//        });
//        binding.complication_blank.setOnCheckedChangeListener((view, isChecked) -> {
//                    if (view.isShown() && isChecked) PreviewFragment.this.setComplicationBlank();
//        });
//        binding.complication_none.setOnCheckedChangeListener((view, isChecked) -> {
//                    if (view.isShown() && isChecked) PreviewFragment.this.setComplicationNone();
//        });
        selectedComplication = SelectedComplication.CIRCLE;
        return ret;
    }
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ac  */
    /*
    @Override // androidx.fragment.app.Fragment
    @SuppressLint("ResourceType")
    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int dimensionPixelSize;
        PreviewViewModel previewViewModel;
        TextView textView = null;
        TextView textView2;
        Wg6.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        WatchFacePreviewFragmentBinding watchFacePreviewFragmentBinding = g1();
        this.h = new Qw6<>(this, watchFacePreviewFragmentBinding);
        WatchFacePreviewFragmentBinding g1 = g1();
        if (!(g1 == null || (textView2 = g1.v) == null)) {
            Bf5.a(textView2, new Ei(this));
        }
        f1();
        Drawable c = W6.c(PortfolioApp.get.instance(), 2131231251);
        if (c != null) {
            c.getIntrinsicHeight();
            Drawable c2 = W6.c(PortfolioApp.get.instance(), 2131231251);
            Integer valueOf = c2 != null ? Integer.valueOf(c2.getMinimumWidth()) : null;
            if (valueOf != null) {
                dimensionPixelSize = valueOf.intValue();
                WatchFacePreviewFragmentBinding g12 = g1();
                if (!(g12 == null || (textView = g12.u) == null)) {
                    Bf5.a(textView, new Fi(this, dimensionPixelSize));
                }
                previewViewModel = this.g;
                if (previewViewModel == null) {
                    previewViewModel.a(dimensionPixelSize);
                    Bundle arguments = getArguments();
                    if (arguments != null) {
                        Wg6.a((Object) arguments, "it");
                        b(arguments);
                    }
                    Wg6.a((Object) watchFacePreviewFragmentBinding, "binding");
                    return watchFacePreviewFragmentBinding.d();
                }
                Wg6.d("mViewModel");
                throw null;
            }
        }
        dimensionPixelSize = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165418);
        WatchFacePreviewFragmentBinding g122 = g1();
        Bf5.a(textView, new Fi(this, dimensionPixelSize));
        previewViewModel = this.g;
        if (previewViewModel == null) {
        }
        throw null;
    }
    */

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.portfolio.platform.uirenew.BaseFragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        PreviewViewModel previewViewModel = this.g;
        if (previewViewModel != null) {
            bundle.putSerializable("FILTER_TYPE_ARG", previewViewModel.b());
            PreviewViewModel previewViewModel2 = this.g;
            if (previewViewModel2 != null) {
                bundle.putParcelableArrayList("COMPLICATIONS_ARG", previewViewModel2.a());
            } else {
                Wg6.d("mViewModel");
                throw null;
            }
        } else {
            Wg6.d("mViewModel");
            throw null;
        }
    }
}
