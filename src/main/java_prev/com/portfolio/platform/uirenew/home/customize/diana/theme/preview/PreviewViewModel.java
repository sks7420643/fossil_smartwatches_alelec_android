package com.portfolio.platform.uirenew.home.customize.diana.theme.preview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.Fz5;
import com.fossil.He;
import com.fossil.Hg5;
import com.fossil.Ie;
import com.fossil.Qj7;
import com.fossil.Ti7;
import com.fossil.Vh7;
import com.fossil.Xh7;
import com.fossil.Zb7;
import com.fossil.Zi7;
import com.fossil.imagefilters.EInkImageFilter;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.FilterType;
import com.fossil.imagefilters.Format;
import com.fossil.imagefilters.OutputSettings;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Ff6;
import com.mapped.FileHelper;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Lw4;
import com.mapped.Nc6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.mapped.Yj6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.BitmapUtils;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.model.diana.preset.Data;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.MetaData;
import com.portfolio.platform.data.model.diana.preset.RingStyle;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewFragment;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class PreviewViewModel extends He {
    @DexIgnore
    public MutableLiveData<Bi> a; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Di> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Ci> c; // = new MutableLiveData<>();
    @DexIgnore
    public FilterType d; // = FilterType.ORDERED_DITHERING;
    @DexIgnore
    public ArrayList<Fz5> e;
    @DexIgnore
    public Bitmap f;
    @DexIgnore
    public /* final */ WatchFaceRepository g;
    @DexIgnore
    public /* final */ RingStyleRepository h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ BitmapDrawable a;

        @DexIgnore
        public Bi(BitmapDrawable bitmapDrawable) {
            Wg6.b(bitmapDrawable, ResourceManager.DRAWABLE);
            this.a = bitmapDrawable;
        }

        @DexIgnore
        public final BitmapDrawable a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public /* final */ Drawable a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public Ci(Drawable drawable, String str, boolean z) {
            this.a = drawable;
            this.b = str;
            this.c = z;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ci(Drawable drawable, String str, boolean z, int i, Qg6 qg6) {
            this((i & 1) != 0 ? null : drawable, (i & 2) != 0 ? null : str, z);
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final Drawable b() {
            return this.a;
        }

        @DexIgnore
        public final boolean c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public Di(String str, boolean z) {
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Di(String str, boolean z, int i, Qg6 qg6) {
            this((i & 1) != 0 ? null : str, z);
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$loadBackgroundPhoto$1", f = "PreviewViewModel.kt", l = {62}, m = "invokeSuspend")
    public static final class Ei extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $size;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PreviewViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$loadBackgroundPhoto$1$drawable$1", f = "PreviewViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super BitmapDrawable>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $previewPhotoDir;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, String str, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
                this.$previewPhotoDir = str;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$previewPhotoDir, xe6);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super BitmapDrawable> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    BitmapUtils bitmapUtils = BitmapUtils.INSTANCE;
                    String str = this.$previewPhotoDir;
                    int i = this.this$0.$size;
                    Bitmap decodeBitmapFromDirectory = bitmapUtils.decodeBitmapFromDirectory(str, i, i);
                    if (decodeBitmapFromDirectory == null) {
                        return null;
                    }
                    this.this$0.this$0.f = decodeBitmapFromDirectory;
                    EInkImageFilter create = EInkImageFilter.create();
                    Wg6.a((Object) create, "EInkImageFilter.create()");
                    FilterResult apply = create.apply(decodeBitmapFromDirectory, this.this$0.this$0.b(), false, false, new OutputSettings(480, 480, Format.RAW, false, false));
                    Wg6.a((Object) apply, "algorithm.apply(bitmap, \u2026e, false, outputSettings)");
                    Bitmap preview = apply.getPreview();
                    int i2 = this.this$0.$size;
                    return new BitmapDrawable(PortfolioApp.get.instance().getResources(), Bitmap.createScaledBitmap(preview, i2, i2, false));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(PreviewViewModel previewViewModel, int i, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = previewViewModel;
            this.$size = i;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$size, xe6);
            ei.p$ = (Il6) obj;
            throw null;
//            return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Object a;
            String str;
            Object a2 = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il6 = this.p$;
                Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                String str2 = FileUtils.getDirectory(applicationContext, FileType.WATCH_FACE) + File.separator + "previewPhoto";
                Ti7 b = Qj7.b();
                Aii aii = new Aii(this, str2, null);
                this.L$0 = il6;
                this.L$1 = applicationContext;
                this.L$2 = str2;
                this.label = 1;
                a = Vh7.a(b, aii, this);
                if (a == a2) {
                    return a2;
                }
                str = str2;
            } else if (i == 1) {
                Context context = (Context) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                Nc6.a(obj);
                str = (String) this.L$2;
                a = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            BitmapDrawable bitmapDrawable = (BitmapDrawable) a;
            if (bitmapDrawable != null) {
                FLogger.INSTANCE.getLocal().d("PreviewViewModel", "load photo success");
                Hg5.b().a(str + this.$size + this.$size, bitmapDrawable);
                this.this$0.c().b(new Bi(bitmapDrawable));
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$loadComplicationSetting$1", f = "PreviewViewModel.kt", l = {121}, m = "invokeSuspend")
    public static final class Fi extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $size;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PreviewViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$loadComplicationSetting$1$1", f = "PreviewViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    DianaComplicationRingStyle ringStylesBySerial = this.this$0.this$0.h.getRingStylesBySerial(PortfolioApp.get.instance().c());
                    if (ringStylesBySerial != null) {
                        FileHelper fileHelper = FileHelper.a;
                        String previewUrl = ringStylesBySerial.getData().getPreviewUrl();
                        int i = this.this$0.$size;
                        Drawable b = fileHelper.b(previewUrl, i, i, FileType.WATCH_FACE);
                        String unselectedForegroundColor = ringStylesBySerial.getMetaData().getUnselectedForegroundColor();
                        if (unselectedForegroundColor == null || b == null) {
                            this.this$0.this$0.d().a(new Ci(null, null, false, 3, null));
                        } else {
                            FLogger.INSTANCE.getLocal().d("PreviewViewModel", "loadComplicationSetting() success");
                            this.this$0.this$0.d().a(new Ci(b, unselectedForegroundColor, true));
                        }
                    } else {
                        this.this$0.this$0.d().a(new Ci(null, null, false, 3, null));
                    }
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(PreviewViewModel previewViewModel, int i, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = previewViewModel;
            this.$size = i;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Fi fi = new Fi(this.this$0, this.$size, xe6);
            fi.p$ = (Il6) obj;
            throw null;
//            return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Object a = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il6 = this.p$;
                Ti7 b = Qj7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Vh7.a(b, aii, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                Nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$saveTheme$1", f = "PreviewViewModel.kt", l = {94}, m = "invokeSuspend")
    public static final class Gi extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $size;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PreviewViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Bitmap $srcBitmap;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bitmap bitmap, Xe6 xe6, Gi gi) {
                super(2, xe6);
                this.$srcBitmap = bitmap;
                this.this$0 = gi;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.$srcBitmap, xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    String valueOf = String.valueOf(System.currentTimeMillis());
                    String str = valueOf + Constants.PHOTO_IMAGE_NAME_SUFFIX;
                    String str2 = valueOf + Constants.PHOTO_BINARY_NAME_SUFFIX;
                    String str3 = FileUtils.getDirectory(PortfolioApp.get.instance().getApplicationContext(), FileType.WATCH_FACE) + File.separator;
                    EInkImageFilter create = EInkImageFilter.create();
                    Wg6.a((Object) create, "EInkImageFilter.create()");
                    FilterResult apply = create.apply(this.$srcBitmap, this.this$0.this$0.b(), false, false, OutputSettings.BACKGROUND);
                    Wg6.a((Object) apply, "algorithm.apply(srcBitma\u2026utputSettings.BACKGROUND)");
                    Bitmap bitmap = this.$srcBitmap;
                    FilterResult apply2 = create.apply(bitmap.copy(bitmap.getConfig(), true), this.this$0.this$0.b(), false, false, OutputSettings.BACKGROUND);
                    Wg6.a((Object) apply2, "algorithm.apply(srcBitma\u2026utputSettings.BACKGROUND)");
                    Bitmap a = Lw4.a(apply2.getPreview());
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(PortfolioApp.get.instance().getResources(), a);
                    Hg5.b().a(str3 + valueOf + this.this$0.$size + this.this$0.$size, bitmapDrawable);
                    FLogger.INSTANCE.getLocal().d("PreviewViewModel", "save theme to repository");
                    WatchFaceRepository c = this.this$0.this$0.g;
                    Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                    Wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                    Wg6.a((Object) a, "bitmap");
                    byte[] data = apply.getData();
                    Wg6.a((Object) data, "thumbnailResult.data");
                    c.saveBackgroundPhoto(applicationContext, str3, str, a, str2, data);
                    PreviewViewModel previewViewModel = this.this$0.this$0;
                    String str4 = File.separator;
                    Wg6.a((Object) str4, "File.separator");
                    throw null;
//                    previewViewModel.a(Yj6.c(str3, str4, (String) null, 2, (Object) null), str, str2);
//                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(PreviewViewModel previewViewModel, int i, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = previewViewModel;
            this.$size = i;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Gi gi = new Gi(this.this$0, this.$size, xe6);
            gi.p$ = (Il6) obj;
            throw null;
//            return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Object a = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il6 = this.p$;
                if (this.this$0.f == null) {
                    FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveTheme() error mBitmap == null");
                    this.this$0.e().a(new Di(null, false, 1, null));
                    return Cd6.a;
                }
                Bitmap a2 = this.this$0.f;
                if (a2 != null) {
                    if (a2.isRecycled()) {
                        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveRingStyle fail bitmap is recycledObject?");
                        this.this$0.e().a(new Di(null, false, 1, null));
                    } else {
                        Ti7 b = Qj7.b();
                        Aii aii = new Aii(a2, null, this);
                        this.L$0 = il6;
                        this.L$1 = a2;
                        this.label = 1;
                        if (Vh7.a(b, aii, this) == a) {
                            return a;
                        }
                    }
                }
            } else if (i == 1) {
                Bitmap bitmap = (Bitmap) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                Nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    /*
    static {
        new Ai(null);
        System.loadLibrary("EInkImageFilter");
    }
    */

    @DexIgnore
    public PreviewViewModel(WatchFaceRepository watchFaceRepository, RingStyleRepository ringStyleRepository) {
        Wg6.b(watchFaceRepository, "watchFaceRepository");
        Wg6.b(ringStyleRepository, "ringStyleRepository");
        this.g = watchFaceRepository;
        this.h = ringStyleRepository;
    }

    @DexIgnore
    public final ArrayList<Fz5> a() {
        return this.e;
    }

    @DexIgnore
    public final ArrayList<RingStyleItem> a(DianaComplicationRingStyle dianaComplicationRingStyle) {
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "createPhotoRingStyleList()");
        String url = dianaComplicationRingStyle.getData().getUrl();
        String previewUrl = dianaComplicationRingStyle.getData().getPreviewUrl();
        String selectedBackgroundColor = dianaComplicationRingStyle.getMetaData().getSelectedBackgroundColor();
        String unselectedBackgroundColor = dianaComplicationRingStyle.getMetaData().getUnselectedBackgroundColor();
        String selectedForegroundColor = dianaComplicationRingStyle.getMetaData().getSelectedForegroundColor();
        String unselectedForegroundColor = dianaComplicationRingStyle.getMetaData().getUnselectedForegroundColor();
        ArrayList<Fz5> arrayList = this.e;
        if (arrayList == null) {
            return null;
        }
        if (arrayList == null) {
            Wg6.a();
            throw null;
        } else if (!(!arrayList.isEmpty())) {
            return null;
        } else {
            ArrayList<RingStyleItem> arrayList2 = new ArrayList<>();
            ArrayList<Fz5> arrayList3 = this.e;
            if (arrayList3 != null) {
                Iterator<Fz5> it = arrayList3.iterator();
                while (it.hasNext()) {
                    String c2 = it.next().c();
                    if (c2 != null) {
                        switch (c2.hashCode()) {
                            case -1383228885:
                                if (c2.equals("bottom")) {
                                    arrayList2.add(new RingStyleItem("bottom", new RingStyle("bottomRing", new Data(previewUrl, url), new MetaData(selectedForegroundColor, selectedBackgroundColor, unselectedForegroundColor, unselectedBackgroundColor))));
                                    break;
                                } else {
                                    continue;
                                }
                            case 115029:
                                if (c2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                    arrayList2.add(new RingStyleItem(ViewHierarchy.DIMENSION_TOP_KEY, new RingStyle("topRing", new Data(previewUrl, url), new MetaData(selectedForegroundColor, selectedBackgroundColor, unselectedForegroundColor, unselectedBackgroundColor))));
                                    break;
                                } else {
                                    continue;
                                }
                            case 3317767:
                                if (c2.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                    arrayList2.add(new RingStyleItem(ViewHierarchy.DIMENSION_LEFT_KEY, new RingStyle("leftRing", new Data(previewUrl, url), new MetaData(selectedForegroundColor, selectedBackgroundColor, unselectedForegroundColor, unselectedBackgroundColor))));
                                    break;
                                } else {
                                    continue;
                                }
                            case 108511772:
                                if (c2.equals("right")) {
                                    arrayList2.add(new RingStyleItem("right", new RingStyle("rightRing", new Data(previewUrl, url), new MetaData(selectedForegroundColor, selectedBackgroundColor, unselectedForegroundColor, unselectedBackgroundColor))));
                                    break;
                                } else {
                                    continue;
                                }
                        }
                    }
                }
            }
            return arrayList2;
        }
    }

    @DexIgnore
    public final void a(int i) {
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "loadBackgroundPhoto()");
        throw null;
//        Rm6 unused = Xh7.b(Ie.a(this), null, null, new Ei(this, i, null), 3, null);
    }

    @DexIgnore
    public final void a(FilterType filterType) {
        Wg6.b(filterType, "<set-?>");
        this.d = filterType;
    }

    @DexReplace
    public final void a(String directory, String filename, String binaryName) { // saveRingStyle
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveRingStyle");
        DianaComplicationRingStyle ringStylesBySerial = this.h.getRingStylesBySerial(PortfolioApp.get.instance().c());
        if (ringStylesBySerial != null) {
            if (PreviewFragment.selectedComplication == PreviewFragment.SelectedComplication.NONE) {
                ringStylesBySerial.data = new Data(null, null);
                ringStylesBySerial.metaData = new MetaData(
                        /* selectedForegroundColor */ "#242424",
                        /* selectedBackgroundColor */ "#FFFFFF",
                        /* unselectedForegroundColor */ "#FFFFFF",
                        /* unselectedBackgroundColor */ "#636363"
                );
            } else if (PreviewFragment.selectedComplication == PreviewFragment.SelectedComplication.BLANK) {
                ringStylesBySerial.data = new Data(
                        /* previewUrl */ "file:///android_asset/background/blank_complication.png",
                        /* url        */ "file:///android_asset/background/blank_complication.bin");
                ringStylesBySerial.metaData = new MetaData(
                        /* selectedForegroundColor */ "#242424",
                        /* selectedBackgroundColor */ "#FFFFFF",
                        /* unselectedForegroundColor */ "#FFFFFF",
                        /* unselectedBackgroundColor */ "#636363"
                );
            }

            FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveRingStyle success: " + ringStylesBySerial);
            this.g.insertWatchFace(directory, filename, binaryName, a(ringStylesBySerial));
            this.b.a(new Di(filename, true));
            return;
        }
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveRingStyle fail");
        this.b.a(new Di(null, false, 1, null));
    }

    @DexIgnore
    public final void a(ArrayList<Fz5> arrayList) {
        this.e = arrayList;
    }

    @DexIgnore
    public final FilterType b() {
        return this.d;
    }

    @DexIgnore
    public final void b(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("PreviewViewModel", "loadComplicationSetting() size=" + i);
        throw null;
//        Rm6 unused = Xh7.b(Ie.a(this), null, null, new Fi(this, i, null), 3, null);
    }

    @DexIgnore
    public final MutableLiveData<Bi> c() {
        return this.a;
    }

    @DexIgnore
    public final void c(int i) {
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveTheme()");
        throw null;
//        Rm6 unused = Xh7.b(Ie.a(this), null, null, new Gi(this, i, null), 3, null);
    }

    @DexIgnore
    public final MutableLiveData<Ci> d() {
        return this.c;
    }

    @DexIgnore
    public final MutableLiveData<Di> e() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.He
    public void onCleared() {
        throw null;
//        Zi7.a(Ie.a(this), null, 1, null);
//        Bitmap bitmap = this.f;
//        if (bitmap != null) {
//            bitmap.recycle();
//        }
//        super.onCleared();
    }
}
