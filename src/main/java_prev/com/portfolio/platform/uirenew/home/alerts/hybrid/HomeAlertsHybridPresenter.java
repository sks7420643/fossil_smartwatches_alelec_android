package com.portfolio.platform.uirenew.home.alerts.hybrid;

import android.content.Context;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Aa7;
import com.fossil.Ec5;
import com.fossil.Ib5;
import com.fossil.K07;
import com.fossil.Pw5;
import com.fossil.Qw5;
import com.fossil.Ti7;
import com.fossil.Vh7;
import com.fossil.Xg5;
import com.fossil.Xh7;
import com.fossil.Zb7;
import com.fossil.Zd;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Ff6;
import com.mapped.HomeAlertsFragment;
import com.mapped.HomeAlertsHybridFragment;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Nc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Ue6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexAppend;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;
import lanchon.dexpatcher.annotation.DexWrap;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeAlertsHybridPresenter extends Pw5 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ Ai o; // = new Ai(null);
    @DexIgnore
    public LiveData<String> e; // = PortfolioApp.get.instance().d();
    @DexIgnore
    public ArrayList<Alarm> f; // = new ArrayList<>();
    @DexIgnore
    public boolean g;
    @DexIgnore
    public volatile boolean h;
    @DexIgnore
    public /* final */ Qw5 i;
    @DexIgnore
    public /* final */ AlarmHelper j;
    @DexIgnore
    public /* final */ SetAlarms k;
    @DexIgnore
    public /* final */ AlarmsRepository l;
    @DexIgnore
    public /* final */ An4 m;

    // Ensure changes are reflected in HomeAlertsPresenter
    @DexAdd
    public Alarm alarmToAdd = null;
    @DexAdd
    public boolean closeOnFinish = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return HomeAlertsHybridPresenter.n;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ei<SetAlarms.Di, SetAlarms.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ Alarm b;

        @DexIgnore
        public Bi(HomeAlertsHybridPresenter homeAlertsHybridPresenter, Alarm alarm) {
            this.a = homeAlertsHybridPresenter;
            this.b = alarm;
        }

        @DexIgnore
        public void a(SetAlarms.Bi bi) {
            Wg6.b(bi, "errorValue");
            this.a.i.a();
            int c = bi.c();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsHybridPresenter.o.a();
            local.d(a2, "enableAlarm() - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.a.i.c();
                } else if (!(c == 1112 || c == 1113)) {
                    this.a.i.z();
                }
                this.a.b(bi.a(), false);
                return;
            }
            List<Ib5> convertBLEPermissionErrorCode = Ib5.convertBLEPermissionErrorCode(bi.b());
            Wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            Qw5 h = this.a.i;
            Object[] array = convertBLEPermissionErrorCode.toArray(new Ib5[0]);
            if (array != null) {
                Ib5[] ib5Arr = (Ib5[]) array;
                h.a((Ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                this.a.b(bi.a(), false);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetAlarms.Di di) {
            Wg6.b(di, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsHybridPresenter.o.a();
            local.d(a2, "enableAlarm - onSuccess: alarmUri = " + di.a().getUri() + ", alarmId = " + di.a().getId());
            this.a.i.a();
            this.a.b(this.b, true);
        }

        // Ensure changes are reflected in HomeAlertsPresenter
        @DexAppend
        public void a(SetAlarms.Di di) {
            if (this.a.closeOnFinish) {
                HomeAlertsHybridFragment f = (HomeAlertsHybridFragment) this.a.i;
                FragmentActivity activity = f.getActivity();
                if (activity != null) {
                    activity.finish();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridPresenter a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1", f = "HomeAlertsHybridPresenter.kt", l = {62}, m = "invokeSuspend")
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $deviceId;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1$allAlarms$1", f = "HomeAlertsHybridPresenter.kt", l = {63, 68}, m = "invokeSuspend")
            public static final class Aiii extends Zb7 implements Coroutine<Il6, Xe6<? super List<Alarm>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
//                    return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super List<Alarm>> xe6) {
                    throw null;
//                    return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Il6 il6;
                    Object a = Ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        Nc6.a(obj);
                        il6 = this.p$;
                        AlarmHelper b = this.this$0.this$0.a.j;
                        this.L$0 = il6;
                        this.label = 1;
                        if (b.a(this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        il6 = (Il6) this.L$0;
                        Nc6.a(obj);
                    } else if (i == 2) {
                        Il6 il62 = (Il6) this.L$0;
                        Nc6.a(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (!this.this$0.this$0.a.h) {
                        this.this$0.this$0.a.h = true;
                        AlarmHelper b2 = this.this$0.this$0.a.j;
                        Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                        Wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                        b2.d(applicationContext);
                    }
                    AlarmsRepository c = this.this$0.this$0.a.l;
                    this.L$0 = il6;
                    this.label = 2;
                    Object allAlarmIgnoreDeletePinType = c.getAllAlarmIgnoreDeletePinType(this);
                    return allAlarmIgnoreDeletePinType == a ? a : allAlarmIgnoreDeletePinType;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Biii<T> implements Comparator<T> {
                @DexIgnore
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    throw null;
//                    return Ue6.a(Integer.valueOf(t.getTotalMinutes()), Integer.valueOf(t2.getTotalMinutes()));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, String str, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$deviceId = str;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$deviceId, xe6);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            // Ensure changes are reflected in HomeAlertsPresenter
            @DexWrap
            @Override
            public final Object invokeSuspend(Object obj) {
                int i = this.label;
                Object ret = invokeSuspend(obj);
                if (i == 1) {
                    // alarms are loaded
                    HomeAlertsHybridPresenter ap = this.this$0.a;
                    if (ap.alarmToAdd != null) {
                        Alarm alarmToAdd = ap.alarmToAdd;
                        ap.alarmToAdd = null;
                        ap.closeOnFinish = true;

                        ArrayList<Alarm> list = ap.f;
                        list.add(alarmToAdd);
                        ap.a(alarmToAdd, true); // function with 'enableAlarm - alarmTotalMinu'
                    }
                }
                return ret;
            }

//            @DexIgnore
//            @Override // com.fossil.Ob7
//            public final Object invokeSuspend(Object obj) {
//                Object a;
//                Object a2 = Ff6.a();
//                int i = this.label;
//                if (i == 0) {
//                    Nc6.a(obj);
//                    Il6 il6 = this.p$;
//                    boolean b = DeviceHelper.o.e().b(this.$deviceId);
//                    this.this$0.a.i.G(b);
//                    if (b) {
//                        Ti7 a3 = this.this$0.a.c();
//                        Aiii aiii = new Aiii(this, null);
//                        this.L$0 = il6;
//                        this.Z$0 = b;
//                        this.label = 1;
//                        a = Vh7.a(a3, aiii, this);
//                        if (a == a2) {
//                            return a2;
//                        }
//                    }
//                    HomeAlertsHybridPresenter homeAlertsHybridPresenter = this.this$0.a;
//                    homeAlertsHybridPresenter.g = homeAlertsHybridPresenter.m.I();
//                    this.this$0.a.i.o(this.this$0.a.g);
//                    this.this$0.a.i.B(this.this$0.a.g);
//                    return Cd6.a;
//                } else if (i == 1) {
//                    Il6 il62 = (Il6) this.L$0;
//                    Nc6.a(obj);
//                    a = obj;
//                } else {
//                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
//                }
//                List<Alarm> list = (List) a;
//                ILocalFLogger local = FLogger.INSTANCE.getLocal();
//                String a4 = HomeAlertsHybridPresenter.o.a();
//                local.d(a4, "GetAlarms onSuccess: size = " + list.size());
//                this.this$0.a.f.clear();
//                for (Alarm alarm : list) {
//                    this.this$0.a.f.add(Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
//                }
//                ArrayList d = this.this$0.a.f;
//                if (d.size() > 1) {
//                    Aa7.a(d, new Biii());
//                }
//                this.this$0.a.i.f(this.this$0.a.f);
//                HomeAlertsHybridPresenter homeAlertsHybridPresenter2 = this.this$0.a;
//                homeAlertsHybridPresenter2.g = homeAlertsHybridPresenter2.m.I();
//                this.this$0.a.i.o(this.this$0.a.g);
//                this.this$0.a.i.B(this.this$0.a.g);
//                return Cd6.a;
//            }
        }

        @DexIgnore
        public Ci(HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
            this.a = homeAlertsHybridPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            if (str == null || str.length() == 0) {
                this.a.i.a(true);
            } else {
                throw null;
//                Rm6 unused = Xh7.b(this.a.e(), null, null, new Aii(this, str, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridPresenter a;

        @DexIgnore
        public Di(HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
            this.a = homeAlertsHybridPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            Qw5 unused = this.a.i;
        }
    }

    /*
    static {
        String simpleName = HomeAlertsHybridPresenter.class.getSimpleName();
        Wg6.a((Object) simpleName, "HomeAlertsHybridPresenter::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public HomeAlertsHybridPresenter(Qw5 qw5, AlarmHelper alarmHelper, SetAlarms setAlarms, AlarmsRepository alarmsRepository, An4 an4) {
        Wg6.b(qw5, "mView");
        Wg6.b(alarmHelper, "mAlarmHelper");
        Wg6.b(setAlarms, "mSetAlarms");
        Wg6.b(alarmsRepository, "mAlarmRepository");
        Wg6.b(an4, "mSharedPreferencesManager");
        this.i = qw5;
        this.j = alarmHelper;
        this.k = setAlarms;
        this.l = alarmsRepository;
        this.m = an4;
    }

    @DexIgnore
    @Override // com.fossil.Pw5
    public void a(Alarm alarm) {
        String a = this.e.a();
        if (a == null || a.length() == 0) {
            FLogger.INSTANCE.getLocal().d(n, "Current Active Device Serial Is Empty");
        } else if (alarm != null || this.f.size() < 32) {
            Qw5 qw5 = this.i;
            String a2 = this.e.a();
            if (a2 != null) {
                Wg6.a((Object) a2, "mActiveSerial.value!!");
                qw5.a(a2, this.f, alarm);
                return;
            }
            Wg6.a();
            throw null;
        } else {
            this.i.r();
        }
    }

    @DexIgnore
    @Override // com.fossil.Pw5
    public void a(Alarm alarm, boolean z) {
        Wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        String a = this.e.a();
        if (!(a == null || a.length() == 0)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = n;
            local.d(str, "enableAlarm - alarmTotalMinues: " + alarm.getTotalMinutes() + " - enable: " + z);
            alarm.setActive(z);
            this.i.b();
            SetAlarms setAlarms = this.k;
            String a2 = this.e.a();
            if (a2 != null) {
                Wg6.a((Object) a2, "mActiveSerial.value!!");
                setAlarms.a(new SetAlarms.Ci(a2, this.f, alarm), new Bi(this, alarm));
                return;
            }
            Wg6.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(n, "enableAlarm - Current Active Device Serial Is Empty");
    }

    @DexIgnore
    public final void b(Alarm alarm, boolean z) {
        Wg6.b(alarm, "editAlarm");
        Iterator<Alarm> it = this.f.iterator();
        while (it.hasNext()) {
            Alarm next = it.next();
            if (Wg6.a((Object) next.getUri(), (Object) alarm.getUri())) {
                ArrayList<Alarm> arrayList = this.f;
                throw null;
//                arrayList.set(arrayList.indexOf(next), Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
//                if (!z) {
//                    break;
//                }
//                j();
            }
        }
        this.i.f(this.f);
    }

    @DexIgnore
    @Override // com.fossil.Cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(n, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.k.f();
        PortfolioApp.get.b(this);
        LiveData<String> liveData = this.e;
        Qw5 qw5 = this.i;
        if (qw5 != null) {
            liveData.a((HomeAlertsHybridFragment) qw5, new Ci(this));
            BleCommandResultManager.d.a(CommunicateMode.SET_LIST_ALARM);
            Qw5 qw52 = this.i;
            throw null;
//            qw52.d(!Xg5.a(Xg5.b, ((HomeAlertsHybridFragment) qw52).getContext(), Xg5.Ai.NOTIFICATION_HYBRID, false, false, false, (Integer) null, 56, (Object) null));
//            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
    }

    @DexIgnore
    @Override // com.fossil.Cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(n, "stop");
        this.e.b(new Di(this));
        this.k.g();
        PortfolioApp.get.c(this);
    }

    @DexIgnore
    @Override // com.fossil.Pw5
    public void h() {
        Xg5 xg5 = Xg5.b;
        Qw5 qw5 = this.i;
        if (qw5 != null) {
            throw null;
//            Xg5.a(xg5, ((HomeAlertsHybridFragment) qw5).getContext(), Xg5.Ai.NOTIFICATION_HYBRID, false, false, false, (Integer) null, 60, (Object) null);
//            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
    }

    @DexIgnore
    @Override // com.fossil.Pw5
    public void i() {
        boolean z = !this.g;
        this.g = z;
        this.m.d(z);
        this.i.o(this.g);
        this.i.B(this.g);
    }

    @DexIgnore
    public final void j() {
        FLogger.INSTANCE.getLocal().d(n, "onSetAlarmsSuccess");
        this.j.d(PortfolioApp.get.instance());
        PortfolioApp instance = PortfolioApp.get.instance();
        String a = this.e.a();
        if (a != null) {
            Wg6.a((Object) a, "mActiveSerial.value!!");
            instance.j(a);
            return;
        }
        Wg6.a();
        throw null;
    }

    @DexIgnore
    public void k() {
        this.i.a(this);
    }

    @DexIgnore
    @K07
    public final void onSetAlarmEventEndComplete(Ec5 ec5) {
        FLogger.INSTANCE.getLocal().d(n, "onSetAlarmEventEndComplete()");
        if (ec5 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = n;
            local.d(str, "onSetAlarmEventEndComplete() - event = " + ec5);
            if (ec5.b()) {
                String a = ec5.a();
                Iterator<Alarm> it = this.f.iterator();
                while (it.hasNext()) {
                    Alarm next = it.next();
                    if (Wg6.a((Object) next.getUri(), (Object) a)) {
                        next.setActive(false);
                    }
                }
                this.i.t();
            }
        }
    }
}
