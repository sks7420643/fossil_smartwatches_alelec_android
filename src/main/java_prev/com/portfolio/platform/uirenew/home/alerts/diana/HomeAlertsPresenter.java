package com.portfolio.platform.uirenew.home.alerts.diana;

import android.content.Context;
import android.text.SpannableString;
import android.text.format.DateFormat;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Aa7;
import com.fossil.Ea7;
import com.fossil.Ec5;
import com.fossil.Gl4;
import com.fossil.Ib5;
import com.fossil.K07;
import com.fossil.Mt5;
import com.fossil.Mx6;
import com.fossil.Nw5;
import com.fossil.Ql4;
import com.fossil.Rl4;
import com.fossil.Rs5;
import com.fossil.Ss5;
import com.fossil.Ti7;
import com.fossil.Vh7;
import com.fossil.Vu5;
import com.fossil.We7;
import com.fossil.Xe5;
import com.fossil.Xg5;
import com.fossil.Xh7;
import com.fossil.Zb7;
import com.fossil.Zd;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.mapped.An4;
import com.mapped.AppWrapper;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Ff6;
import com.mapped.HomeAlertsFragment;
import com.mapped.Il6;
import com.mapped.Jh6;
import com.mapped.Jm4;
import com.mapped.Lf6;
import com.mapped.Nc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rl6;
import com.mapped.Rm6;
import com.mapped.Ue6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexAppend;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;
import lanchon.dexpatcher.annotation.DexWrap;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeAlertsPresenter extends Rs5 {
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ Ai x; // = new Ai(null);
    @DexIgnore
    public LiveData<String> e; // = PortfolioApp.get.instance().d();
    @DexIgnore
    public /* final */ LiveData<List<DNDScheduledTimeModel>> f; // = this.v.getDNDScheduledTimeDao().getListDNDScheduledTime();
    @DexIgnore
    public ArrayList<Alarm> g; // = new ArrayList<>();
    @DexIgnore
    public boolean h;
    @DexIgnore
    public volatile boolean i;
    @DexIgnore
    public List<AppWrapper> j; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> k; // = new ArrayList();
    @DexIgnore
    public /* final */ Ss5 l;  // HomeAlertsFragment
    @DexIgnore
    public /* final */ Rl4 m;
    @DexIgnore
    public /* final */ AlarmHelper n;
    @DexIgnore
    public /* final */ Nw5 o;
    @DexIgnore
    public /* final */ Vu5 p;
    @DexIgnore
    public /* final */ Mt5 q;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase r;
    @DexIgnore
    public /* final */ SetAlarms s;
    @DexIgnore
    public /* final */ AlarmsRepository t;
    @DexIgnore
    public /* final */ An4 u;
    @DexIgnore
    public /* final */ DNDSettingsDatabase v;

    // Ensure changes are reflected in HomeAlertsHybridPresenter
    @DexAdd
    public Alarm alarmToAdd = null;
    @DexAdd
    public boolean closeOnFinish = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return HomeAlertsPresenter.w;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ei<SetAlarms.Di, SetAlarms.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ Alarm b;

        @DexIgnore
        public Bi(HomeAlertsPresenter homeAlertsPresenter, Alarm alarm) {
            this.a = homeAlertsPresenter;
            this.b = alarm;
        }

        @DexIgnore
        public void a(SetAlarms.Bi bi) {
            Wg6.b(bi, "errorValue");
            this.a.l.a();
            int c = bi.c();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsPresenter.x.a();
            local.d(a2, "enableAlarm() - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.a.l.c();
                } else if (!(c == 1112 || c == 1113)) {
                    this.a.l.z();
                }
                this.a.b(bi.a(), false);
                return;
            }
            List<Ib5> convertBLEPermissionErrorCode = Ib5.convertBLEPermissionErrorCode(bi.b());
            Wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            Ss5 o = this.a.l;
            Object[] array = convertBLEPermissionErrorCode.toArray(new Ib5[0]);
            if (array != null) {
                Ib5[] ib5Arr = (Ib5[]) array;
                o.a((Ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                this.a.b(bi.a(), false);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetAlarms.Di di) {
            Wg6.b(di, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsPresenter.x.a();
            local.d(a2, "enableAlarm - onSuccess: alarmUri = " + di.a().getUri() + ", alarmId = " + di.a().getId());
            this.a.l.a();
            this.a.b(this.b, true);
        }

        // Ensure changes are reflected in HomeAlertsHybridPresenter
        @DexAppend
        public void a(SetAlarms.Di di) {
            if (this.a.closeOnFinish) {
                HomeAlertsFragment f = (HomeAlertsFragment) this.a.l;
                FragmentActivity activity = f.getActivity();
                if (activity != null) {
                    activity.finish();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Ql4.Di<Mt5.Ci, Ql4.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ci(HomeAlertsPresenter homeAlertsPresenter) {
            this.a = homeAlertsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(Mt5.Ci ci) {
            FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), ".Inside mSaveAppsNotification onSuccess");
            String a2 = Jm4.a(PortfolioApp.get.instance(), 2131886497);
            Ss5 o = this.a.l;
            Wg6.a((Object) a2, "notificationAppOverView");
            o.l(a2);
            this.a.l();
        }

        @DexIgnore
        public void a(Ql4.Ai ai) {
            FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), ".Inside mSaveAppsNotification onError");
            String a2 = Jm4.a(PortfolioApp.get.instance(), 2131886942);
            Ss5 o = this.a.l;
            Wg6.a((Object) a2, "notificationAppOverView");
            o.l(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1", f = "HomeAlertsPresenter.kt", l = {435, 437}, m = "invokeSuspend")
    public static final class Di extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1", f = "HomeAlertsPresenter.kt", l = {356, 362, 371, 379}, m = "invokeSuspend")
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super List<? extends AppNotificationFilter>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $notificationSettings;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii implements Runnable {
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii a;

                    @DexIgnore
                    public Aiiii(Aiii aiii) {
                        this.a = aiii;
                    }

                    @DexIgnore
                    public final void run() {
                        this.a.this$0.this$0.this$0.r.getNotificationSettingsDao().insertListNotificationSettings(this.a.$notificationSettings);
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, List list, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                    this.$notificationSettings = list;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$notificationSettings, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
//                    return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
//                    return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Ff6.a();
                    if (this.label == 0) {
                        Nc6.a(obj);
                        this.this$0.this$0.this$0.r.runInTransaction(new Aiiii(this));
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$2", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Biii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Jh6 $contactMessageAppFilters;
                @DexIgnore
                public /* final */ /* synthetic */ List<NotificationSettingsModel> $listNotificationSettings;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Aii aii, List list, Jh6 jh6, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                    this.$listNotificationSettings = list;
                    this.$contactMessageAppFilters = jh6;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Biii biii = new Biii(this.this$0, this.$listNotificationSettings, this.$contactMessageAppFilters, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
//                    return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
//                    return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Ff6.a();
                    if (this.label == 0) {
                        Nc6.a(obj);
                        for (NotificationSettingsModel notificationSettingsModel : this.$listNotificationSettings) {
                            int component2 = notificationSettingsModel.component2();
                            if (notificationSettingsModel.component3()) {
                                String a = this.this$0.this$0.this$0.a(component2);
                                FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "CALL settingsTypeName=" + a);
                                if (component2 == 0) {
                                    FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL());
                                    DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                    throw null;
//                                    this.$contactMessageAppFilters.element.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
                                } else if (component2 == 1) {
                                    int size = this.this$0.this$0.this$0.k.size();
                                    for (int i = 0; i < size; i++) {
                                        ContactGroup contactGroup = (ContactGroup) this.this$0.this$0.this$0.k.get(i);
                                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                        String a2 = HomeAlertsPresenter.x.a();
                                        StringBuilder sb = new StringBuilder();
                                        sb.append("mListAppNotificationFilter add PHONE item - ");
                                        sb.append(i);
                                        sb.append(" name = ");
                                        Contact contact = contactGroup.getContacts().get(0);
                                        Wg6.a((Object) contact, "item.contacts[0]");
                                        sb.append(contact.getDisplayName());
                                        local.d(a2, sb.toString());
                                        DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                        FNotification fNotification = new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType());
                                        List<Contact> contacts = contactGroup.getContacts();
                                        Wg6.a((Object) contacts, "item.contacts");
                                        if (!contacts.isEmpty()) {
                                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification);
                                            Contact contact2 = contactGroup.getContacts().get(0);
                                            Wg6.a((Object) contact2, "item.contacts[0]");
                                            appNotificationFilter.setSender(contact2.getDisplayName());
                                            throw null;
//                                            this.$contactMessageAppFilters.element.add(appNotificationFilter);
                                        }
                                    }
                                }
                            } else {
                                String a3 = this.this$0.this$0.this$0.a(component2);
                                FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "MESSAGE settingsTypeName=" + a3);
                                if (component2 == 0) {
                                    FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getMESSAGES());
                                    DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                    throw null;
//                                    this.$contactMessageAppFilters.element.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
                                } else if (component2 == 1) {
                                    int size2 = this.this$0.this$0.this$0.k.size();
                                    for (int i2 = 0; i2 < size2; i2++) {
                                        ContactGroup contactGroup2 = (ContactGroup) this.this$0.this$0.this$0.k.get(i2);
                                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                        String a4 = HomeAlertsPresenter.x.a();
                                        StringBuilder sb2 = new StringBuilder();
                                        sb2.append("mListAppNotificationFilter add MESSAGE item - ");
                                        sb2.append(i2);
                                        sb2.append(" name = ");
                                        Contact contact3 = contactGroup2.getContacts().get(0);
                                        Wg6.a((Object) contact3, "item.contacts[0]");
                                        sb2.append(contact3.getDisplayName());
                                        local2.d(a4, sb2.toString());
                                        DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                        FNotification fNotification2 = new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType());
                                        List<Contact> contacts2 = contactGroup2.getContacts();
                                        Wg6.a((Object) contacts2, "item.contacts");
                                        if (!contacts2.isEmpty()) {
                                            AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification2);
                                            Contact contact4 = contactGroup2.getContacts().get(0);
                                            Wg6.a((Object) contact4, "item.contacts[0]");
                                            appNotificationFilter2.setSender(contact4.getDisplayName());
                                            throw null;
//                                            this.$contactMessageAppFilters.element.add(appNotificationFilter2);
                                        }
                                    }
                                }
                            }
                        }
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$listNotificationSettings$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Ciii extends Zb7 implements Coroutine<Il6, Xe6<? super List<? extends NotificationSettingsModel>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Ciii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Ciii ciii = new Ciii(this.this$0, xe6);
                    ciii.p$ = (Il6) obj;
                    throw null;
//                    return ciii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super List<? extends NotificationSettingsModel>> xe6) {
                    throw null;
//                    return ((Ciii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Ff6.a();
                    if (this.label == 0) {
                        Nc6.a(obj);
                        return this.this$0.this$0.this$0.r.getNotificationSettingsDao().getListNotificationSettingsNoLiveData();
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends AppNotificationFilter>> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:16:0x005e  */
            /* JADX WARNING: Removed duplicated region for block: B:22:0x00b1  */
            /* JADX WARNING: Removed duplicated region for block: B:30:0x0146  */
            /* JADX WARNING: Removed duplicated region for block: B:33:0x018b  */
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Object a;
                Il6 il6;
                Jh6 jh6;
                CoroutineUseCase.Ci ci;
                Object a2;
                Il6 il62;
                CoroutineUseCase.Ci ci2;
                Jh6 jh62;
                List list;
                Jh6 jh63 = null;
                Object a3 = Ff6.a();
                int i = this.label;
                if (i == 0) {
                    Nc6.a(obj);
                    Il6 il63 = this.p$;
                    Jh6 jh64 = new Jh6();
                    jh64.element = null;
                    Vu5 i2 = this.this$0.this$0.p;
                    this.L$0 = il63;
                    this.L$1 = jh64;
                    this.label = 1;
                    a = Gl4.a(i2, null, this);
                    if (a == a3) {
                        return a3;
                    }
                    il6 = il63;
                    jh6 = jh64;
                    ci = (CoroutineUseCase.Ci) a;
                    if (!(ci instanceof Vu5.Di)) {
                    }
                } else if (i == 1) {
                    Nc6.a(obj);
                    il6 = (Il6) this.L$0;
                    jh6 = (Jh6) this.L$1;
                    a = obj;
                    ci = (CoroutineUseCase.Ci) a;
                    if (!(ci instanceof Vu5.Di)) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a4 = HomeAlertsPresenter.x.a();
                        StringBuilder sb = new StringBuilder();
                        sb.append("GetAllContactGroup onSuccess, size = ");
                        Vu5.Di di = (Vu5.Di) ci;
                        sb.append(di.a().size());
                        local.d(a4, sb.toString());
                        jh6.element = new ArrayList();
                        this.this$0.this$0.k.clear();
                        this.this$0.this$0.k = Ea7.d((Collection) di.a());
                        Ti7 c = this.this$0.this$0.c();
                        Ciii ciii = new Ciii(this, null);
                        this.L$0 = il6;
                        this.L$1 = jh6;
                        this.L$2 = ci;
                        this.label = 2;
                        a2 = Vh7.a(c, ciii, this);
                        if (a2 == a3) {
                            return a3;
                        }
                        il62 = il6;
                        ci2 = ci;
                        jh62 = jh6;
                        list = (List) a2;
                        if (!list.isEmpty()) {
                        }
                        jh63 = jh62;
                    } else {
                        if (ci instanceof Vu5.Bi) {
                            FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "GetAllContactGroup onError");
                        }
                        return jh6.element;
                    }
                } else if (i != 2) {
                    if (i == 3) {
                        NotificationSettingsModel notificationSettingsModel = (NotificationSettingsModel) this.L$6;
                        NotificationSettingsModel notificationSettingsModel2 = (NotificationSettingsModel) this.L$5;
                        List list2 = (List) this.L$4;
                    } else if (i != 4) {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    List list3 = (List) this.L$3;
                    CoroutineUseCase.Ci ci3 = (CoroutineUseCase.Ci) this.L$2;
                    jh63 = (Jh6) this.L$1;
                    Il6 il64 = (Il6) this.L$0;
                    Nc6.a(obj);
                } else {
                    jh62 = (Jh6) this.L$1;
                    il62 = (Il6) this.L$0;
                    Nc6.a(obj);
                    a2 = obj;
                    ci2 = (CoroutineUseCase.Ci) this.L$2;
                    list = (List) a2;
                    if (!list.isEmpty()) {
                        ArrayList arrayList = new ArrayList();
                        NotificationSettingsModel notificationSettingsModel3 = new NotificationSettingsModel("AllowCallsFrom", 0, true);
                        NotificationSettingsModel notificationSettingsModel4 = new NotificationSettingsModel("AllowMessagesFrom", 0, false);
                        arrayList.add(notificationSettingsModel3);
                        arrayList.add(notificationSettingsModel4);
                        Ti7 c2 = this.this$0.this$0.c();
                        Aiii aiii = new Aiii(this, arrayList, null);
                        this.L$0 = il62;
                        this.L$1 = jh62;
                        this.L$2 = ci2;
                        this.L$3 = list;
                        this.L$4 = arrayList;
                        this.L$5 = notificationSettingsModel3;
                        this.L$6 = notificationSettingsModel4;
                        this.label = 3;
                        if (Vh7.a(c2, aiii, this) == a3) {
                            return a3;
                        }
                    } else {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a5 = HomeAlertsPresenter.x.a();
                        local2.d(a5, "listNotificationSettings.size = " + list.size());
                        Ti7 a6 = this.this$0.this$0.b();
                        Biii biii = new Biii(this, list, jh62, null);
                        this.L$0 = il62;
                        this.L$1 = jh62;
                        this.L$2 = ci2;
                        this.L$3 = list;
                        this.label = 4;
                        if (Vh7.a(a6, biii, this) == a3) {
                            return a3;
                        }
                    }
                    jh63 = jh62;
                }
                jh6 = jh63;
                return jh6.element;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$otherAppDeffer$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Zb7 implements Coroutine<Il6, Xe6<? super List<? extends AppNotificationFilter>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
//                return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends AppNotificationFilter>> xe6) {
                throw null;
//                return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    throw null;
//                    return Mx6.a(this.this$0.this$0.j(), false, 1, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(HomeAlertsPresenter homeAlertsPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeAlertsPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
//            return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v15, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x00d0  */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            long currentTimeMillis;
            Rl6 a;
            Object c;
            Il6 il6;
            ArrayList arrayList;
            Rl6 rl6;
            List list;
            Object c2;
            Object a2 = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il62 = this.p$;
                ArrayList arrayList2 = new ArrayList();
                currentTimeMillis = System.currentTimeMillis();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = HomeAlertsPresenter.x.a();
                local.d(a3, "filter notification, time start=" + currentTimeMillis);
                arrayList2.clear();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a4 = HomeAlertsPresenter.x.a();
                local2.d(a4, "mListAppWrapper.size = " + this.this$0.j().size());
                throw null;
//                Rl6 a5 = Xh7.a(il62, this.this$0.b(), null, new Bii(this, null), 2, null);
//                a = Xh7.a(il62, this.this$0.b(), null, new Aii(this, null), 2, null);
//                this.L$0 = il62;
//                this.L$1 = arrayList2;
//                this.J$0 = currentTimeMillis;
//                this.L$2 = a5;
//                this.L$3 = a;
//                this.L$4 = arrayList2;
//                this.label = 1;
//                c = a5.c(this);
//                if (c == a2) {
//                    return a2;
//                }
//                il6 = il62;
//                arrayList = arrayList2;
//                rl6 = a5;
//                list = arrayList2;
//                arrayList.addAll((Collection) c);
//                this.L$0 = il6;
//                this.L$1 = list;
//                this.J$0 = currentTimeMillis;
//                this.L$2 = rl6;
//                this.L$3 = a;
//                this.label = 2;
//                c2 = a.c(this);
//                if (c2 == a2) {
//                }
            } else if (i == 1) {
                a = (Rl6) this.L$3;
                rl6 = (Rl6) this.L$2;
                long j = this.J$0;
                list = (List) this.L$1;
                il6 = (Il6) this.L$0;
                Nc6.a(obj);
                c = obj;
                arrayList = (ArrayList)this.L$4;
                currentTimeMillis = j;
                arrayList.addAll((Collection) c);
                this.L$0 = il6;
                this.L$1 = list;
                this.J$0 = currentTimeMillis;
                this.L$2 = rl6;
                this.L$3 = a;
                this.label = 2;
                c2 = a.c(this);
                if (c2 == a2) {
                    return a2;
                }
            } else if (i == 2) {
                Rl6 rl62 = (Rl6) this.L$3;
                Rl6 rl63 = (Rl6) this.L$2;
                currentTimeMillis = this.J$0;
                Il6 il63 = (Il6) this.L$0;
                Nc6.a(obj);
                c2 = obj;
                list = (List) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list2 = (List) c2;
            if (list2 != null) {
                list.addAll(list2);
                long b = PortfolioApp.get.instance().b(new AppNotificationFilterSettings(list, System.currentTimeMillis()), PortfolioApp.get.instance().c());
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a6 = HomeAlertsPresenter.x.a();
                local3.d(a6, "filter notification, time end= " + b);
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String a7 = HomeAlertsPresenter.x.a();
                local4.d(a7, "delayTime =" + (b - currentTimeMillis) + " milliseconds");
            }
            return Cd6.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1", f = "HomeAlertsPresenter.kt", l = {85}, m = "invokeSuspend")
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii implements CoroutineUseCase.Ei<Nw5.Ai, CoroutineUseCase.Ai> {
                @DexIgnore
                public /* final */ /* synthetic */ Aii a;

                @DexIgnore
                /* JADX WARN: Incorrect args count in method signature: ()V */
                public Aiii(Aii aii) {
                    this.a = aii;
                }

                @DexIgnore
                /* renamed from: a */
                public void onSuccess(Nw5.Ai ai) {
                    String sb;
                    Wg6.b(ai, "responseValue");
                    FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "GetApps onSuccess");
                    ArrayList<AppWrapper> arrayList = new ArrayList();
                    arrayList.addAll(ai.a());
                    if (this.a.this$0.a.u.F()) {
                        ArrayList arrayList2 = new ArrayList();
                        for (AppWrapper appWrapper : arrayList) {
                            InstalledApp installedApp = appWrapper.getInstalledApp();
                            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                            if (isSelected == null) {
                                Wg6.a();
                                throw null;
                            } else if (!isSelected.booleanValue()) {
                                arrayList2.add(appWrapper);
                            }
                        }
                        this.a.this$0.a.j().clear();
                        this.a.this$0.a.j().addAll(arrayList);
                        if (!arrayList2.isEmpty()) {
                            this.a.this$0.a.a(arrayList2);
                            return;
                        }
                        String a2 = Jm4.a(PortfolioApp.get.instance(), 2131886497);
                        Ss5 o = this.a.this$0.a.l;
                        Wg6.a((Object) a2, "notificationAppOverView");
                        o.l(a2);
                        HomeAlertsPresenter homeAlertsPresenter = this.a.this$0.a;
                        if (homeAlertsPresenter.a(homeAlertsPresenter.j(), arrayList)) {
                            this.a.this$0.a.l();
                            return;
                        }
                        return;
                    }
                    this.a.this$0.a.j().clear();
                    int i = 0;
                    for (AppWrapper appWrapper2 : arrayList) {
                        InstalledApp installedApp2 = appWrapper2.getInstalledApp();
                        Boolean isSelected2 = installedApp2 != null ? installedApp2.isSelected() : null;
                        if (isSelected2 == null) {
                            Wg6.a();
                            throw null;
                        } else if (isSelected2.booleanValue()) {
                            this.a.this$0.a.j().add(appWrapper2);
                            i++;
                        }
                    }
                    if (i == ai.a().size()) {
                        sb = Jm4.a(PortfolioApp.get.instance(), 2131886497);
                    } else if (i == 0) {
                        sb = Jm4.a(PortfolioApp.get.instance(), 2131886942);
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(i);
                        sb2.append('/');
                        sb2.append(arrayList.size());
                        sb = sb2.toString();
                    }
                    Ss5 o2 = this.a.this$0.a.l;
                    Wg6.a((Object) sb, "notificationAppOverView");
                    o2.l(sb);
                }

                @DexIgnore
                public void a(CoroutineUseCase.Ai ai) {
                    Wg6.b(ai, "errorValue");
                    FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "GetApps onError");
                }
            }

//            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
//            public static final class Biii<T> implements Zd<List<? extends DNDScheduledTimeModel>> {
//                @DexIgnore
//                public /* final */ /* synthetic */ Aii a;
//
//                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
//                @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3$1", f = "HomeAlertsPresenter.kt", l = {166}, m = "invokeSuspend")
//                public static final class Aiiii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
//                    @DexIgnore
//                    public /* final */ /* synthetic */ List $lDndScheduledTimeModel;
//                    @DexIgnore
//                    public Object L$0;
//                    @DexIgnore
//                    public int label;
//                    @DexIgnore
//                    public Il6 p$;
//                    @DexIgnore
//                    public /* final */ /* synthetic */ Biii this$0;
//
//                    @DexIgnore
//                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
//                    public Aiiii(Biii biii, List list, Xe6 xe6) {
//                        super(2, xe6);
//                        this.this$0 = biii;
//                        this.$lDndScheduledTimeModel = list;
//                    }
//
//                    @DexIgnore
//                    @Override // com.fossil.Ob7
//                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
//                        Wg6.b(xe6, "completion");
//                        Aiiii aiiii = new Aiiii(this.this$0, this.$lDndScheduledTimeModel, xe6);
//                        aiiii.p$ = (Il6) obj;
//                        return aiiii;
//                    }
//
//                    @DexIgnore
//                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
//                    @Override // com.mapped.Coroutine
//                    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
//                        return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
//                    }
//
//                    @DexIgnore
//                    @Override // com.fossil.Ob7
//                    public final Object invokeSuspend(Object obj) {
//                        Object a = Ff6.a();
//                        int i = this.label;
//                        if (i == 0) {
//                            Nc6.a(obj);
//                            Il6 il6 = this.p$;
//                            Ti7 c = this.this$0.a.this$0.a.c();
//                            HomeAlertsPresenter$e$a$b$a$a homeAlertsPresenter$e$a$b$a$a = new HomeAlertsPresenter$e$a$b$a$a(this, null);
//                            this.L$0 = il6;
//                            this.label = 1;
//                            if (Vh7.a(c, homeAlertsPresenter$e$a$b$a$a, this) == a) {
//                                return a;
//                            }
//                        } else if (i == 1) {
//                            Il6 il62 = (Il6) this.L$0;
//                            Nc6.a(obj);
//                        } else {
//                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
//                        }
//                        return Cd6.a;
//                    }
//                }
//
//                @DexIgnore
//                public Biii(Aii aii) {
//                    this.a = aii;
//                }
//
//                @DexIgnore
//                /* renamed from: a */
//                public final void onChanged(List<DNDScheduledTimeModel> list) {
//                    if (list == null || list.isEmpty()) {
//                        ArrayList arrayList = new ArrayList();
//                        DNDScheduledTimeModel dNDScheduledTimeModel = new DNDScheduledTimeModel("Start", 1380, 0);
//                        DNDScheduledTimeModel dNDScheduledTimeModel2 = new DNDScheduledTimeModel("End", 1140, 1);
//                        arrayList.add(dNDScheduledTimeModel);
//                        arrayList.add(dNDScheduledTimeModel2);
//                        Rm6 unused = Xh7.b(this.a.this$0.a.e(), null, null, new Aiiii(this, arrayList, null), 3, null);
//                        return;
//                    }
//                    for (T t : list) {
//                        if (t.getScheduledTimeType() == 0) {
//                            this.a.this$0.a.l.b(this.a.this$0.a.b(t.getMinutes()));
//                        } else {
//                            this.a.this$0.a.l.a(this.a.this$0.a.b(t.getMinutes()));
//                        }
//                    }
//                }
//            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$allAlarms$1", f = "HomeAlertsPresenter.kt", l = {86, 91}, m = "invokeSuspend")
            public static final class Ciii extends Zb7 implements Coroutine<Il6, Xe6<? super List<Alarm>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Ciii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    Ciii ciii = new Ciii(this.this$0, xe6);
                    ciii.p$ = (Il6) obj;
                    throw null;
//                    return ciii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super List<Alarm>> xe6) {
                    throw null;
//                    return ((Ciii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Il6 il6;
                    Object a = Ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        Nc6.a(obj);
                        il6 = this.p$;
                        AlarmHelper d = this.this$0.this$0.a.n;
                        this.L$0 = il6;
                        this.label = 1;
                        if (d.a(this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        il6 = (Il6) this.L$0;
                        Nc6.a(obj);
                    } else if (i == 2) {
                        Il6 il62 = (Il6) this.L$0;
                        Nc6.a(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (!this.this$0.this$0.a.i) {
                        this.this$0.this$0.a.i = true;
                        AlarmHelper d2 = this.this$0.this$0.a.n;
                        Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                        Wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                        d2.d(applicationContext);
                    }
                    AlarmsRepository e = this.this$0.this$0.a.t;
                    this.L$0 = il6;
                    this.label = 2;
                    Object allAlarmIgnoreDeletePinType = e.getAllAlarmIgnoreDeletePinType(this);
                    return allAlarmIgnoreDeletePinType == a ? a : allAlarmIgnoreDeletePinType;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Diii<T> implements Comparator<T> {
                @DexIgnore
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    throw null;
//                    return Ue6.a(Integer.valueOf(t.getTotalMinutes()), Integer.valueOf(t2.getTotalMinutes()));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            // Ensure changes are reflected in HomeAlertsHybridPresenter
            @DexWrap
            @Override
            public final Object invokeSuspend(Object obj) {
                int i = this.label;
                Object ret = invokeSuspend(obj);
                if (i == 1) {
                    // alarms are loaded
                    HomeAlertsPresenter ap = this.this$0.a;
                    if (ap.alarmToAdd != null) {
                        Alarm alarmToAdd = ap.alarmToAdd;
                        ap.alarmToAdd = null;
                        ap.closeOnFinish = true;

                        ArrayList<Alarm> list = ap.g;
                        list.add(alarmToAdd);
                        ap.a(alarmToAdd, true); // function with 'enableAlarm - alarmTotalMinu'
                    }
                }
                return ret;
            }

//            @DexIgnore
//            @Override // com.fossil.Ob7
//            public final Object invokeSuspend(Object obj) {
//                Object a;
//                Object a2 = Ff6.a();
//                int i = this.label;
//                if (i == 0) {
//                    Nc6.a(obj);
//                    Il6 il6 = this.p$;
//                    Ti7 c = this.this$0.a.c();
//                    Ciii ciii = new Ciii(this, null);
//                    this.L$0 = il6;
//                    this.label = 1;
//                    a = Vh7.a(c, ciii, this);
//                    if (a == a2) {
//                        return a2;
//                    }
//                } else if (i == 1) {
//                    Il6 il62 = (Il6) this.L$0;
//                    Nc6.a(obj);
//                    a = obj;
//                } else {
//                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
//                }
//                List<Alarm> list = (List) a;
//                ILocalFLogger local = FLogger.INSTANCE.getLocal();
//                String a3 = HomeAlertsPresenter.x.a();
//                local.d(a3, "GetAlarms onSuccess: size = " + list.size());
//                this.this$0.a.g.clear();
//                for (Alarm alarm : list) {
//                    this.this$0.a.g.add(Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
//                }
//                ArrayList f = this.this$0.a.g;
//                if (f.size() > 1) {
//                    Aa7.a(f, new Diii());
//                }
//                this.this$0.a.l.f(this.this$0.a.g);
//                this.this$0.a.o.a((CoroutineUseCase.Bi) null, new Aiii(this));
//                HomeAlertsPresenter homeAlertsPresenter = this.this$0.a;
//                homeAlertsPresenter.h = homeAlertsPresenter.u.J();
//                this.this$0.a.l.z(this.this$0.a.h);
//                throw null;
////                this.this$0.a.f.a((LifecycleOwner) this.this$0.a.l, new Biii(this));
////                return Cd6.a;
//            }
        }

        @DexIgnore
        public Ei(HomeAlertsPresenter homeAlertsPresenter) {
            this.a = homeAlertsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            if (str == null || str.length() == 0) {
                this.a.l.a(true);
            } else {
                throw null;
//                Rm6 unused = Xh7.b(this.a.e(), null, null, new Aii(this, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;

        @DexIgnore
        public Fi(HomeAlertsPresenter homeAlertsPresenter) {
            this.a = homeAlertsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            Ss5 unused = this.a.l;
        }
    }

//    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
//    public static final class Gi<T> implements Zd<List<? extends DNDScheduledTimeModel>> {
//        @DexIgnore
//        public /* final */ /* synthetic */ HomeAlertsPresenter a;
//
//        @DexIgnore
//        public Gi(HomeAlertsPresenter homeAlertsPresenter) {
//            this.a = homeAlertsPresenter;
//        }
//
//        @DexIgnore
//        /* renamed from: a */
//        public final void onChanged(List<DNDScheduledTimeModel> list) {
//            Ss5 unused = this.a.l;
//        }
//    }

    /*
    static {
        String simpleName = HomeAlertsPresenter.class.getSimpleName();
        Wg6.a((Object) simpleName, "HomeAlertsPresenter::class.java.simpleName");
        w = simpleName;
    }
    */

    @DexIgnore
    public HomeAlertsPresenter(Ss5 ss5, Rl4 rl4, AlarmHelper alarmHelper, Nw5 nw5, Vu5 vu5, Mt5 mt5, NotificationSettingsDatabase notificationSettingsDatabase, SetAlarms setAlarms, AlarmsRepository alarmsRepository, An4 an4, DNDSettingsDatabase dNDSettingsDatabase) {
        Wg6.b(ss5, "mView");
        Wg6.b(rl4, "mUseCaseHandler");
        Wg6.b(alarmHelper, "mAlarmHelper");
        Wg6.b(nw5, "mGetApps");
        Wg6.b(vu5, "mGetAllContactGroup");
        Wg6.b(mt5, "mSaveAppsNotification");
        Wg6.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        Wg6.b(setAlarms, "mSetAlarms");
        Wg6.b(alarmsRepository, "mAlarmRepository");
        Wg6.b(an4, "mSharedPreferencesManager");
        Wg6.b(dNDSettingsDatabase, "mDNDSettingsDatabase");
        this.l = ss5;
        this.m = rl4;
        this.n = alarmHelper;
        this.o = nw5;
        this.p = vu5;
        this.q = mt5;
        this.r = notificationSettingsDatabase;
        this.s = setAlarms;
        this.t = alarmsRepository;
        this.u = an4;
        this.v = dNDSettingsDatabase;
    }

    @DexIgnore
    public final String a(int i2) {
        if (i2 == 0) {
            String a = Jm4.a(PortfolioApp.get.instance(), 2131886089);
            Wg6.a((Object) a, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return a;
        } else if (i2 != 1) {
            String a2 = Jm4.a(PortfolioApp.get.instance(), 2131886091);
            Wg6.a((Object) a2, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return a2;
        } else {
            String a3 = Jm4.a(PortfolioApp.get.instance(), 2131886090);
            Wg6.a((Object) a3, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return a3;
        }
    }

    @DexIgnore
    @Override // com.fossil.Rs5
    public void a(Alarm alarm) {
        String a = this.e.a();
        if (a == null || a.length() == 0) {
            FLogger.INSTANCE.getLocal().d("HomeAlertsFragment", "Current Active Device Serial Is Empty");
        } else if (alarm != null || this.g.size() < 32) {
            Ss5 ss5 = this.l;
            String a2 = this.e.a();
            if (a2 != null) {
                Wg6.a((Object) a2, "mActiveSerial.value!!");
                ss5.a(a2, this.g, alarm); // Start AlarmActivity
                return;
            }
            Wg6.a();
            throw null;
        } else {
            this.l.r();
        }
    }

    @DexIgnore
    @Override // com.fossil.Rs5
    public void a(Alarm alarm, boolean z) {
        Wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        String a = this.e.a();
        if (!(a == null || a.length() == 0)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "enableAlarm - alarmTotalMinue: " + alarm.getTotalMinutes() + " - enable: " + z);
            alarm.setActive(z);
            this.l.b();
            SetAlarms setAlarms = this.s;
            String a2 = this.e.a();
            if (a2 != null) {
                Wg6.a((Object) a2, "mActiveSerial.value!!");
                setAlarms.a(new SetAlarms.Ci(a2, this.g, alarm), new Bi(this, alarm));
                return;
            }
            Wg6.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(w, "enableAlarm - Current Active Device Serial Is Empty");
    }

    @DexIgnore
    public final void a(List<AppWrapper> list) {
        Wg6.b(list, "listAppWrapperNotEnabled");
        for (AppWrapper appWrapper : list) {
            InstalledApp installedApp = appWrapper.getInstalledApp();
            if (installedApp != null) {
                installedApp.setSelected(true);
            }
        }
        this.m.a(this.q, new Mt5.Bi(list), new Ci(this));
    }

    @DexIgnore
    public final boolean a(List<AppWrapper> list, List<AppWrapper> list2) {
        AppWrapper t2;
        Wg6.b(list, "listDatabaseAppWrapper");
        Wg6.b(list2, "listAppWrapper");
        if (list.size() != list2.size()) {
            return true;
        }
        boolean z = false;
        for (AppWrapper t3 : list) {
            Iterator<AppWrapper> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                AppWrapper next = it.next();
                InstalledApp installedApp = next.getInstalledApp();
                String identifier = installedApp != null ? installedApp.getIdentifier() : null;
                InstalledApp installedApp2 = t3.getInstalledApp();
                if (Wg6.a((Object) identifier, (Object) (installedApp2 != null ? installedApp2.getIdentifier() : null))) {
                    t2 = next;
                    break;
                }
            }
            AppWrapper t4 = t2;
            if (t4 != null) {
                InstalledApp installedApp3 = t4.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                InstalledApp installedApp4 = t3.getInstalledApp();
                if (!(!Wg6.a(isSelected, installedApp4 != null ? installedApp4.isSelected() : null))) {
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final SpannableString b(int i2) {
        int i3 = 12;
        int i4 = i2 / 60;
        int i5 = i2 % 60;
        if (DateFormat.is24HourFormat(PortfolioApp.get.instance())) {
            StringBuilder sb = new StringBuilder();
            We7 we7 = We7.a;
            Locale locale = Locale.US;
            Wg6.a((Object) locale, "Locale.US");
            String format = String.format(locale, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i4)}, 1));
            Wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
            sb.append(format);
            sb.append(':');
            We7 we72 = We7.a;
            Locale locale2 = Locale.US;
            Wg6.a((Object) locale2, "Locale.US");
            String format2 = String.format(locale2, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i5)}, 1));
            Wg6.a((Object) format2, "java.lang.String.format(locale, format, *args)");
            sb.append(format2);
            return new SpannableString(sb.toString());
        } else if (i2 < 720) {
            if (i4 != 0) {
                i3 = i4;
            }
            Xe5 xe5 = Xe5.b;
            StringBuilder sb2 = new StringBuilder();
            We7 we73 = We7.a;
            Locale locale3 = Locale.US;
            Wg6.a((Object) locale3, "Locale.US");
            String format3 = String.format(locale3, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i3)}, 1));
            Wg6.a((Object) format3, "java.lang.String.format(locale, format, *args)");
            sb2.append(format3);
            sb2.append(':');
            We7 we74 = We7.a;
            Locale locale4 = Locale.US;
            Wg6.a((Object) locale4, "Locale.US");
            String format4 = String.format(locale4, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i5)}, 1));
            Wg6.a((Object) format4, "java.lang.String.format(locale, format, *args)");
            sb2.append(format4);
            String sb3 = sb2.toString();
            String a = Jm4.a(PortfolioApp.get.instance(), 2131886102);
            Wg6.a((Object) a, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
            return xe5.a(sb3, a, 1.0f);
        } else {
            if (i4 > 12) {
                i3 = i4 - 12;
            }
            Xe5 xe52 = Xe5.b;
            StringBuilder sb4 = new StringBuilder();
            We7 we75 = We7.a;
            Locale locale5 = Locale.US;
            Wg6.a((Object) locale5, "Locale.US");
            String format5 = String.format(locale5, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i3)}, 1));
            Wg6.a((Object) format5, "java.lang.String.format(locale, format, *args)");
            sb4.append(format5);
            sb4.append(':');
            We7 we76 = We7.a;
            Locale locale6 = Locale.US;
            Wg6.a((Object) locale6, "Locale.US");
            String format6 = String.format(locale6, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i5)}, 1));
            Wg6.a((Object) format6, "java.lang.String.format(locale, format, *args)");
            sb4.append(format6);
            String sb5 = sb4.toString();
            String a2 = Jm4.a(PortfolioApp.get.instance(), 2131886104);
            Wg6.a((Object) a2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
            return xe52.a(sb5, a2, 1.0f);
        }
    }

    @DexIgnore
    public final void b(Alarm alarm, boolean z) {
        Wg6.b(alarm, "editAlarm");
        Iterator<Alarm> it = this.g.iterator();
        while (it.hasNext()) {
            Alarm next = it.next();
            if (Wg6.a((Object) next.getUri(), (Object) alarm.getUri())) {
                ArrayList<Alarm> arrayList = this.g;
                throw null;
//                arrayList.set(arrayList.indexOf(next), Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
//                if (!z) {
//                    break;
//                }
//                k();
            }
        }
        this.l.f(this.g);
    }

    @DexIgnore
    @Override // com.fossil.Cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(w, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.s.f();
        PortfolioApp.get.b(this);
        LiveData<String> liveData = this.e;
        Ss5 ss5 = this.l;
        if (ss5 != null) {
            liveData.a((HomeAlertsFragment) ss5, new Ei(this));
            BleCommandResultManager.d.a(CommunicateMode.SET_LIST_ALARM);
            Ss5 ss52 = this.l;
            throw null;
//            ss52.d(!Xg5.a(Xg5.b, ((HomeAlertsFragment) ss52).getContext(), Xg5.Ai.NOTIFICATION_DIANA, false, false, false, (Integer) null, 56, (Object) null));
//            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
    }

    @DexIgnore
    @Override // com.fossil.Cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(w, "stop");
        throw null;
//        this.e.b(new Fi(this));
//        this.f.b(new Gi(this));
//        this.s.g();
//        PortfolioApp.get.c(this);
    }

    @DexIgnore
    @Override // com.fossil.Rs5
    public void h() {
        Xg5 xg5 = Xg5.b;
        Ss5 ss5 = this.l;
        if (ss5 != null) {
            throw null;
//            Xg5.a(xg5, ((HomeAlertsFragment) ss5).getContext(), Xg5.Ai.NOTIFICATION_DIANA, false, false, false, (Integer) null, 60, (Object) null);
//            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
    }

    @DexIgnore
    @Override // com.fossil.Rs5
    public void i() {
        boolean z = !this.h;
        this.h = z;
        this.u.e(z);
        this.l.z(this.h);
    }

    @DexIgnore
    public final List<AppWrapper> j() {
        return this.j;
    }

    @DexIgnore
    public final void k() {
        FLogger.INSTANCE.getLocal().d(w, "onSetAlarmsSuccess");
        this.n.d(PortfolioApp.get.instance());
        String a = this.e.a();
        if (a != null) {
            PortfolioApp instance = PortfolioApp.get.instance();
            Wg6.a((Object) a, "it");
            instance.j(a);
        }
    }

    @DexIgnore
    public final void l() {
        Xg5 xg5 = Xg5.b;
        Ss5 ss5 = this.l;
        throw null;
//        if (ss5 == null) {
//            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
//        } else if (Xg5.a(xg5, ((HomeAlertsFragment) ss5).getContext(), Xg5.Ai.SET_BLE_COMMAND, false, false, false, (Integer) null, 60, (Object) null)) {
//            throw null;
//            Rm6 unused = Xh7.b(e(), null, null, new Di(this, null), 3, null);
//        }
    }

    @DexIgnore
    public void m() {
        this.l.a(this);
    }

    @DexIgnore
    @K07
    public final void onSetAlarmEventEndComplete(Ec5 ec5) {
        if (ec5 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "onSetAlarmEventEndComplete() - event = " + ec5);
            if (ec5.b()) {
                String a = ec5.a();
                Iterator<Alarm> it = this.g.iterator();
                while (it.hasNext()) {
                    Alarm next = it.next();
                    if (Wg6.a((Object) next.getUri(), (Object) a)) {
                        next.setActive(false);
                    }
                }
                this.l.t();
            }
        }
    }
}
