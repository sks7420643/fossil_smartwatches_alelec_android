package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;

import com.facebook.share.internal.VideoUploader;
import com.fossil.Ea7;
import com.fossil.Et5;
import com.fossil.Ft5;
import com.fossil.Gl4;
import com.fossil.Ib5;
import com.fossil.Mt5;
import com.fossil.Mx6;
import com.fossil.Nw5;
import com.fossil.Ql4;
import com.fossil.Rl4;
import com.fossil.Ti7;
import com.fossil.Vh7;
import com.fossil.Vu5;
import com.fossil.Xg5;
import com.fossil.Xh7;
import com.fossil.Zb7;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.mapped.An4;
import com.mapped.AppWrapper;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Ff6;
import com.mapped.Hf6;
import com.mapped.Il6;
import com.mapped.Jm4;
import com.mapped.Lf6;
import com.mapped.Nc6;
import com.mapped.PermissionUtils;
import com.mapped.Qd6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.service.BleCommandResultManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexPrepend;
import lanchon.dexpatcher.annotation.DexWrap;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationAppsPresenter extends Et5 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public boolean e; // = this.p.F();
    @DexIgnore
    public List<InstalledApp> f; // = new ArrayList();
    @DexIgnore
    public List<AppWrapper> g; // = new ArrayList();
    @DexIgnore
    public List<AppNotificationFilter> h; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> i; // = new ArrayList();
    @DexIgnore
    public /* final */ Bi j; // = new Bi();
    @DexIgnore
    public /* final */ Ft5 k;
    @DexIgnore
    public /* final */ Rl4 l;
    @DexIgnore
    public /* final */ Nw5 m;
    @DexIgnore
    public /* final */ Vu5 n;
    @DexIgnore
    public /* final */ Mt5 o;
    @DexIgnore
    public /* final */ An4 p;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase q;
    @DexAdd
    public boolean ListRestored;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return NotificationAppsPresenter.r;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi implements BleCommandResultManager.Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public /*static*/ final class Aii implements Ql4.Di<Mt5.Ci, Ql4.Ai> {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Aii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(Mt5.Ci ci) {
                FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), ".Inside mSaveAppsNotification onSuccess");
                NotificationAppsPresenter.this.k.a();
                NotificationAppsPresenter.this.k.close();
            }

            @DexIgnore
            public void a(Ql4.Ai ai) {
                FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), ".Inside mSaveAppsNotification onError");
                NotificationAppsPresenter.this.k.a();
                NotificationAppsPresenter.this.k.close();
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.b(communicateMode, "communicateMode");
            Wg6.b(intent, "intent");
            FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "SetNotificationFilterReceiver");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationAppsPresenter.s.a();
            local.d(a2, "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
            if (communicateMode != CommunicateMode.SET_NOTIFICATION_FILTERS) {
                return;
            }
            if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "onReceive - success");
                NotificationAppsPresenter.this.p.a(NotificationAppsPresenter.this.j());
                NotificationAppsPresenter.this.l.a(NotificationAppsPresenter.this.o, new Mt5.Bi(NotificationAppsPresenter.this.k()), new Aii(this));
                return;
            }
            FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "onReceive - failed");
            int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
            ArrayList<Integer> arrayList = integerArrayListExtra != null ? integerArrayListExtra : new ArrayList<>(intExtra2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = NotificationAppsPresenter.s.a();
            local2.d(a3, "permissionErrorCodes=" + arrayList + " , size=" + arrayList.size());
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = NotificationAppsPresenter.s.a();
                local3.d(a4, "error code " + i + " =" + arrayList.get(i));
            }
            if (intExtra2 != 1101) {
                if (intExtra2 == 1924) {
                    NotificationAppsPresenter.this.k.l();
                    return;
                } else if (intExtra2 == 8888) {
                    NotificationAppsPresenter.this.k.c();
                    return;
                } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                    return;
                }
            }
            List<Ib5> convertBLEPermissionErrorCode = Ib5.convertBLEPermissionErrorCode(arrayList);
            Wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
            Ft5 l = NotificationAppsPresenter.this.k;
            Object[] array = convertBLEPermissionErrorCode.toArray(new Ib5[0]);
            if (array != null) {
                Ib5[] ib5Arr = (Ib5[]) array;
                l.a((Ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1", f = "NotificationAppsPresenter.kt", l = {130, 135, 184, 192, 201, 210}, m = "invokeSuspend")
    public static final class Ci extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$1", f = "NotificationAppsPresenter.kt", l = {131}, m = "invokeSuspend")
        public static final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Object a = Ff6.a();
                int i = this.label;
                if (i == 0) {
                    Nc6.a(obj);
                    Il6 il6 = this.p$;
                    PortfolioApp instance = PortfolioApp.get.instance();
                    this.L$0 = il6;
                    this.label = 1;
                    if (instance.k(this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    Nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$6", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $notificationSettings;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ Bii a;

                @DexIgnore
                public Aiii(Bii bii) {
                    this.a = bii;
                }

                @DexIgnore
                public final void run() {
                    this.a.this$0.this$0.q.getNotificationSettingsDao().insertListNotificationSettings(this.a.$notificationSettings);
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ci ci, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$notificationSettings = list;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Bii bii = new Bii(this.this$0, this.$notificationSettings, xe6);
                bii.p$ = (Il6) obj;
                throw null;
//                return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
//                return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    this.this$0.this$0.q.runInTransaction(new Aiii(this));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$7", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Cii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List<NotificationSettingsModel> $listNotificationSettings;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Ci ci, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$listNotificationSettings = list;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Cii cii = new Cii(this.this$0, this.$listNotificationSettings, xe6);
                cii.p$ = (Il6) obj;
                throw null;
//                return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
//                return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    for (NotificationSettingsModel notificationSettingsModel : this.$listNotificationSettings) {
                        int component2 = notificationSettingsModel.component2();
                        if (notificationSettingsModel.component3()) {
                            String a = this.this$0.this$0.a(component2);
                            FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "CALL settingsTypeName=" + a);
                            if (component2 == 0) {
                                FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL());
                                DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                this.this$0.this$0.h.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
                            } else if (component2 == 1) {
                                int size = this.this$0.this$0.i.size();
                                for (int i = 0; i < size; i++) {
                                    ContactGroup contactGroup = (ContactGroup) this.this$0.this$0.i.get(i);
                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                    String a2 = NotificationAppsPresenter.s.a();
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("mListAppNotificationFilter add PHONE item - ");
                                    sb.append(i);
                                    sb.append(" name = ");
                                    Contact contact = contactGroup.getContacts().get(0);
                                    Wg6.a((Object) contact, "item.contacts[0]");
                                    sb.append(contact.getDisplayName());
                                    local.d(a2, sb.toString());
                                    DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType()));
                                    Contact contact2 = contactGroup.getContacts().get(0);
                                    Wg6.a((Object) contact2, "item.contacts[0]");
                                    appNotificationFilter.setSender(contact2.getDisplayName());
                                    this.this$0.this$0.h.add(appNotificationFilter);
                                }
                            }
                        } else {
                            String a3 = this.this$0.this$0.a(component2);
                            FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "MESSAGE settingsTypeName=" + a3);
                            if (component2 == 0) {
                                FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getMESSAGES());
                                DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                this.this$0.this$0.h.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
                            } else if (component2 == 1) {
                                int size2 = this.this$0.this$0.i.size();
                                for (int i2 = 0; i2 < size2; i2++) {
                                    ContactGroup contactGroup2 = (ContactGroup) this.this$0.this$0.i.get(i2);
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String a4 = NotificationAppsPresenter.s.a();
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append("mListAppNotificationFilter add MESSAGE item - ");
                                    sb2.append(i2);
                                    sb2.append(" name = ");
                                    Contact contact3 = contactGroup2.getContacts().get(0);
                                    Wg6.a((Object) contact3, "item.contacts[0]");
                                    sb2.append(contact3.getDisplayName());
                                    local2.d(a4, sb2.toString());
                                    DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType()));
                                    Contact contact4 = contactGroup2.getContacts().get(0);
                                    Wg6.a((Object) contact4, "item.contacts[0]");
                                    appNotificationFilter2.setSender(contact4.getDisplayName());
                                    this.this$0.this$0.h.add(appNotificationFilter2);
                                }
                            }
                        }
                    }
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$getAllContactGroupsResponse$1", f = "NotificationAppsPresenter.kt", l = {186}, m = "invokeSuspend")
        public static final class Dii extends Zb7 implements Coroutine<Il6, Xe6<? super CoroutineUseCase.Ci>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Dii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Dii dii = new Dii(this.this$0, xe6);
                dii.p$ = (Il6) obj;
                throw null;
//                return dii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super CoroutineUseCase.Ci> xe6) {
                throw null;
//                return ((Dii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Object a = Ff6.a();
                int i = this.label;
                if (i == 0) {
                    Nc6.a(obj);
                    Il6 il6 = this.p$;
                    Vu5 c = this.this$0.this$0.n;
                    this.L$0 = il6;
                    this.label = 1;
                    Object a2 = Gl4.a(c, null, this);
                    return a2 == a ? a : a2;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    Nc6.a(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$getAppResponse$1", f = "NotificationAppsPresenter.kt", l = {136}, m = "invokeSuspend")
        public static final class Eii extends Zb7 implements Coroutine<Il6, Xe6<? super CoroutineUseCase.Ci>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Eii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Eii eii = new Eii(this.this$0, xe6);
                eii.p$ = (Il6) obj;
                throw null;
//                return eii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super CoroutineUseCase.Ci> xe6) {
                throw null;
//                return ((Eii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Object a = Ff6.a();
                int i = this.label;
                if (i == 0) {
                    Nc6.a(obj);
                    Il6 il6 = this.p$;
                    Nw5 d = this.this$0.this$0.m;
                    this.L$0 = il6;
                    this.label = 1;
                    Object a2 = Gl4.a(d, null, this);
                    return a2 == a ? a : a2;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    Nc6.a(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$listNotificationSettings$1", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Fii extends Zb7 implements Coroutine<Il6, Xe6<? super List<? extends NotificationSettingsModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Fii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Fii fii = new Fii(this.this$0, xe6);
                fii.p$ = (Il6) obj;
                throw null;
//                return fii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends NotificationSettingsModel>> xe6) {
                throw null;
//                return ((Fii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    return this.this$0.this$0.q.getNotificationSettingsDao().getListNotificationSettingsNoLiveData();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(NotificationAppsPresenter notificationAppsPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationAppsPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
//            return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:104:0x030d, code lost:
            if (r0 == false) goto L_0x0331;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0054  */
        /* JADX WARNING: Removed duplicated region for block: B:113:0x0357  */
        /* JADX WARNING: Removed duplicated region for block: B:126:0x038e  */
        /* JADX WARNING: Removed duplicated region for block: B:129:0x03ad  */
        /* JADX WARNING: Removed duplicated region for block: B:132:0x03f8  */
        /* JADX WARNING: Removed duplicated region for block: B:136:0x0413  */
        /* JADX WARNING: Removed duplicated region for block: B:137:0x0416  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00a4  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x0111  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x01b5  */
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Il6 il6;
            Object a;
            Il6 il62;
            CoroutineUseCase.Ci ci;
            Object a2;
            Il6 il63;
            throw null;
//            T t;
//            boolean z;
//            T t2;
//            CoroutineUseCase.Ci ci2;
//            Object a3;
//            Il6 il64;
//            CoroutineUseCase.Ci ci3;
//            List list;
//            Object a4 = Ff6.a();
//            switch (this.label) {
//                case 0:
//                    Nc6.a(obj);
//                    il6 = this.p$;
//                    if (!PortfolioApp.get.instance().v().U()) {
//                        Ti7 a5 = this.this$0.b();
//                        Aii aii = new Aii(null);
//                        this.L$0 = il6;
//                        this.label = 1;
//                        if (Vh7.a(a5, aii, this) == a4) {
//                            return a4;
//                        }
//                    }
//                    Ti7 a6 = this.this$0.b();
//                    Eii eii = new Eii(this, null);
//                    this.L$0 = il6;
//                    this.label = 2;
//                    a = Vh7.a(a6, eii, this);
//                    if (a != a4) {
//                        return a4;
//                    }
//                    il62 = il6;
//                    ci = (CoroutineUseCase.Ci) a;
//                    if (!(ci instanceof Nw5.Ai)) {
//                        this.this$0.k.a();
//                        int size = this.this$0.k().size();
//                        Nw5.Ai ai = (Nw5.Ai) ci;
//                        int size2 = ai.a().size();
//                        if (size > size2) {
//                            NotificationAppsPresenter notificationAppsPresenter = this.this$0;
//                            List<AppWrapper> k = notificationAppsPresenter.k();
//                            ArrayList arrayList = new ArrayList();
//                            for (T t3 : k) {
//                                T t4 = t3;
//                                Iterator<T> it = ai.a().iterator();
//                                while (true) {
//                                    if (it.hasNext()) {
//                                        T next = it.next();
//                                        InstalledApp installedApp = next.getInstalledApp();
//                                        String identifier = installedApp != null ? installedApp.getIdentifier() : null;
//                                        InstalledApp installedApp2 = t4.getInstalledApp();
//                                        if (Hf6.a(Wg6.a((Object) identifier, (Object) (installedApp2 != null ? installedApp2.getIdentifier() : null))).booleanValue()) {
//                                            t2 = next;
//                                        }
//                                    } else {
//                                        t2 = null;
//                                    }
//                                }
//                                if (Hf6.a(t2 != null).booleanValue()) {
//                                    arrayList.add(t3);
//                                }
//                            }
//                            notificationAppsPresenter.a(Ea7.d((Collection) arrayList));
//                        } else if (size < size2) {
//                            List<AppWrapper> a7 = ai.a();
//                            ArrayList arrayList2 = new ArrayList();
//                            for (T t5 : a7) {
//                                T t6 = t5;
//                                Iterator<T> it2 = this.this$0.k().iterator();
//                                while (true) {
//                                    if (it2.hasNext()) {
//                                        T next2 = it2.next();
//                                        T t7 = next2;
//                                        InstalledApp installedApp3 = t6.getInstalledApp();
//                                        String identifier2 = installedApp3 != null ? installedApp3.getIdentifier() : null;
//                                        InstalledApp installedApp4 = t7.getInstalledApp();
//                                        if (Hf6.a(Wg6.a((Object) identifier2, (Object) (installedApp4 != null ? installedApp4.getIdentifier() : null))).booleanValue()) {
//                                            t = next2;
//                                        }
//                                    } else {
//                                        t = null;
//                                    }
//                                }
//                                if (Hf6.a(t == null).booleanValue()) {
//                                    arrayList2.add(t5);
//                                }
//                            }
//                            this.this$0.k().addAll(arrayList2);
//                        }
//                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
//                        String a8 = NotificationAppsPresenter.s.a();
//                        local.d(a8, "GetApps onSuccess size " + ai.a().size() + " isAllAppEnabled " + this.this$0.j());
//                        this.this$0.f.clear();
//                        for (AppWrapper appWrapper : this.this$0.k()) {
//                            InstalledApp installedApp5 = appWrapper.getInstalledApp();
//                            if (installedApp5 != null) {
//                                Boolean isSelected = installedApp5.isSelected();
//                                Wg6.a((Object) isSelected, "it.isSelected");
//                                if (isSelected.booleanValue()) {
//                                    this.this$0.f.add(installedApp5);
//                                }
//                            }
//                        }
//                        if (this.this$0.j()) {
//                            this.this$0.c(true);
//                        }
//                        if (!this.this$0.j()) {
//                            List<AppWrapper> k2 = this.this$0.k();
//                            if (!(k2 instanceof Collection) || !k2.isEmpty()) {
//                                Iterator<T> it3 = k2.iterator();
//                                while (true) {
//                                    if (it3.hasNext()) {
//                                        InstalledApp installedApp6 = it3.next().getInstalledApp();
//                                        if (installedApp6 != null) {
//                                            Boolean isSelected2 = installedApp6.isSelected();
//                                            Wg6.a((Object) isSelected2, "it.installedApp!!.isSelected");
//                                            if (isSelected2.booleanValue()) {
//                                                z = true;
//                                                break;
//                                            }
//                                        } else {
//                                            Wg6.a();
//                                            throw null;
//                                        }
//                                    }
//                                }
//                            }
//                            z = false;
//                            break;
//                        }
//                        Ft5 l = this.this$0.k;
//                        if (l != null) {
//                            Context context = ((Gt5) l).getContext();
//                            if (context != null) {
//                                Hf6.a(Xg5.a(Xg5.b, context, Xg5.Ai.NOTIFICATION_APPS, false, false, false, (Integer) null, 60, (Object) null));
//                            }
//                            this.this$0.k.k(this.this$0.k());
//                        } else {
//                            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
//                        }
//                    } else if (ci instanceof CoroutineUseCase.Ai) {
//                        FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "GetApps onError");
//                        this.this$0.k.a();
//                    }
//                    Ti7 a9 = this.this$0.b();
//                    Dii dii = new Dii(this, null);
//                    this.L$0 = il62;
//                    this.label = 3;
//                    a2 = Vh7.a(a9, dii, this);
//                    if (a2 != a4) {
//                        return a4;
//                    }
//                    il63 = il62;
//                    ci2 = (CoroutineUseCase.Ci) a2;
//                    if (!(ci2 instanceof Vu5.Di)) {
//                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
//                        String a10 = NotificationAppsPresenter.s.a();
//                        StringBuilder sb = new StringBuilder();
//                        sb.append("GetAllContactGroup onSuccess, size = ");
//                        Vu5.Di di = (Vu5.Di) ci2;
//                        sb.append(di.a().size());
//                        local2.d(a10, sb.toString());
//                        this.this$0.i.clear();
//                        this.this$0.i = Ea7.d((Collection) di.a());
//                        Ti7 b = this.this$0.c();
//                        Fii fii = new Fii(this, null);
//                        this.L$0 = il63;
//                        this.L$1 = ci2;
//                        this.label = 4;
//                        a3 = Vh7.a(b, fii, this);
//                        if (a3 == a4) {
//                            return a4;
//                        }
//                        il64 = il63;
//                        ci3 = ci2;
//                        list = (List) a3;
//                        if (!list.isEmpty()) {
//                            ArrayList arrayList3 = new ArrayList();
//                            NotificationSettingsModel notificationSettingsModel = new NotificationSettingsModel("AllowCallsFrom", 0, true);
//                            NotificationSettingsModel notificationSettingsModel2 = new NotificationSettingsModel("AllowMessagesFrom", 0, false);
//                            arrayList3.add(notificationSettingsModel);
//                            arrayList3.add(notificationSettingsModel2);
//                            Ti7 b2 = this.this$0.c();
//                            Bii bii = new Bii(this, arrayList3, null);
//                            this.L$0 = il64;
//                            this.L$1 = ci3;
//                            this.L$2 = list;
//                            this.L$3 = arrayList3;
//                            this.L$4 = notificationSettingsModel;
//                            this.L$5 = notificationSettingsModel2;
//                            this.label = 5;
//                            if (Vh7.a(b2, bii, this) == a4) {
//                                return a4;
//                            }
//                        } else {
//                            this.this$0.h.clear();
//                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
//                            String a11 = NotificationAppsPresenter.s.a();
//                            local3.d(a11, "listNotificationSettings.size = " + list.size());
//                            Ti7 a12 = this.this$0.b();
//                            Cii cii = new Cii(this, list, null);
//                            this.L$0 = il64;
//                            this.L$1 = ci3;
//                            this.L$2 = list;
//                            this.label = 6;
//                            if (Vh7.a(a12, cii, this) == a4) {
//                                return a4;
//                            }
//                        }
//                        this.this$0.k.j(this.this$0.j());
//                        return Cd6.a;
//                    }
//                    if (ci2 instanceof Vu5.Bi) {
//                        FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "GetAllContactGroup onError");
//                    }
//                    this.this$0.k.j(this.this$0.j());
//                    return Cd6.a;
//                case 1:
//                    il6 = (Il6) this.L$0;
//                    Nc6.a(obj);
//                    Ti7 a62 = this.this$0.b();
//                    Eii eii2 = new Eii(this, null);
//                    this.L$0 = il6;
//                    this.label = 2;
//                    a = Vh7.a(a62, eii2, this);
//                    if (a != a4) {
//                    }
//                    break;
//                case 2:
//                    Nc6.a(obj);
//                    il62 = (Il6) this.L$0;
//                    a = obj;
//                    ci = (CoroutineUseCase.Ci) a;
//                    if (!(ci instanceof Nw5.Ai)) {
//                    }
//                    Ti7 a92 = this.this$0.b();
//                    Dii dii2 = new Dii(this, null);
//                    this.L$0 = il62;
//                    this.label = 3;
//                    a2 = Vh7.a(a92, dii2, this);
//                    if (a2 != a4) {
//                    }
//                    break;
//                case 3:
//                    Nc6.a(obj);
//                    il63 = (Il6) this.L$0;
//                    a2 = obj;
//                    ci2 = (CoroutineUseCase.Ci) a2;
//                    if (!(ci2 instanceof Vu5.Di)) {
//                    }
//                    break;
//                case 4:
//                    il64 = (Il6) this.L$0;
//                    Nc6.a(obj);
//                    ci3 = (CoroutineUseCase.Ci) this.L$1;
//                    a3 = obj;
//                    list = (List) a3;
//                    if (!list.isEmpty()) {
//                    }
//                    this.this$0.k.j(this.this$0.j());
//                    return Cd6.a;
//                case 5:
//                    NotificationSettingsModel notificationSettingsModel3 = (NotificationSettingsModel) this.L$5;
//                    NotificationSettingsModel notificationSettingsModel4 = (NotificationSettingsModel) this.L$4;
//                    List list2 = (List) this.L$3;
//                    List list3 = (List) this.L$2;
//                    CoroutineUseCase.Ci ci4 = (CoroutineUseCase.Ci) this.L$1;
//                    Il6 il65 = (Il6) this.L$0;
//                    Nc6.a(obj);
//                    this.this$0.k.j(this.this$0.j());
//                    return Cd6.a;
//                case 6:
//                    List list32 = (List) this.L$2;
//                    CoroutineUseCase.Ci ci42 = (CoroutineUseCase.Ci) this.L$1;
//                    Il6 il652 = (Il6) this.L$0;
//                    Nc6.a(obj);
//                    this.this$0.k.j(this.this$0.j());
//                    return Cd6.a;
//                default:
//                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
//            }
        }
    }

    /*
    static {
        String simpleName = NotificationAppsPresenter.class.getSimpleName();
        Wg6.a((Object) simpleName, "NotificationAppsPresenter::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public NotificationAppsPresenter(Ft5 ft5, Rl4 rl4, Nw5 nw5, Vu5 vu5, Mt5 mt5, An4 an4, NotificationSettingsDatabase notificationSettingsDatabase) {
        Wg6.b(ft5, "mView");
        Wg6.b(rl4, "mUseCaseHandler");
        Wg6.b(nw5, "mGetApps");
        Wg6.b(vu5, "mGetAllContactGroup");
        Wg6.b(mt5, "mSaveAppsNotification");
        Wg6.b(an4, "mSharedPreferencesManager");
        Wg6.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.k = ft5;
        this.l = rl4;
        this.m = nw5;
        this.n = vu5;
        this.o = mt5;
        this.p = an4;
        this.q = notificationSettingsDatabase;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "init mIsAllAppToggleEnable " + this.e);
    }

    @DexIgnore
    public final String a(int i2) {
        if (i2 == 0) {
            String a = Jm4.a(PortfolioApp.get.instance(), 2131886089);
            Wg6.a((Object) a, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return a;
        } else if (i2 != 1) {
            String a2 = Jm4.a(PortfolioApp.get.instance(), 2131886091);
            Wg6.a((Object) a2, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return a2;
        } else {
            String a3 = Jm4.a(PortfolioApp.get.instance(), 2131886090);
            Wg6.a((Object) a3, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return a3;
        }
    }

    @DexIgnore
    @Override // com.fossil.Et5
    public void a(AppWrapper appWrapper, boolean z) {
        AppWrapper t;
        boolean z2;
        InstalledApp installedApp;
        Wg6.b(appWrapper, "appWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        StringBuilder sb = new StringBuilder();
        sb.append("setAppState: appName = ");
        InstalledApp installedApp2 = appWrapper.getInstalledApp();
        sb.append(installedApp2 != null ? installedApp2.getTitle() : null);
        sb.append(", selected = ");
        sb.append(z);
        local.d(str, sb.toString());
        Iterator<AppWrapper> it = this.g.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            AppWrapper next = it.next();
            if (Wg6.a(next.getUri(), appWrapper.getUri())) {
                t = next;
                break;
            }
        }
        AppWrapper t2 = t;
        if (!(t2 == null || (installedApp = t2.getInstalledApp()) == null)) {
            installedApp.setSelected(z);
        }
        if (b(z) && this.e != z) {
            this.e = z;
            this.k.j(z);
        } else if (!z && this.e) {
            this.e = false;
            this.k.j(false);
        }
        if (!this.e) {
            List<AppWrapper> list = this.g;
            if (!(list instanceof Collection) || !list.isEmpty()) {
                Iterator<AppWrapper> it2 = list.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        z2 = false;
                        break;
                    }
                    InstalledApp installedApp3 = it2.next().getInstalledApp();
                    if (installedApp3 != null) {
                        Boolean isSelected = installedApp3.isSelected();
                        Wg6.a((Object) isSelected, "it.installedApp!!.isSelected");
                        if (isSelected.booleanValue()) {
                            z2 = true;
                            break;
                        }
                    } else {
                        Wg6.a();
                        throw null;
                    }
                }
            } else {
                z2 = false;
            }
            if (!z2) {
                return;
            }
        }
        Ft5 ft5 = this.k;
        if (ft5 != null) {
            Context context = ((NotificationAppsFragment) ft5).getContext();
            if (context != null) {
                throw null;
//                Xg5.a(Xg5.b, context, Xg5.Ai.NOTIFICATION_APPS, false, false, false, (Integer) null, 60, (Object) null);
//                return;
            }
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
    }

    @DexIgnore
    public final void a(List<AppWrapper> list) {
        Wg6.b(list, "<set-?>");
        this.g = list;
    }

    @DexIgnore
    @Override // com.fossil.Et5
    public void a(boolean z) {
        boolean z2 = true;
        FLogger.INSTANCE.getLocal().d(r, "setEnableAllAppsToggle: enable = " + z);
        this.e = z;
        if (!z && b(true)) {
            c(false);
        } else if (z) {
            c(true);
        } else {
            z2 = false;
        }
        if (z2) {
            this.k.k(this.g);
        }
        if (this.e) {
            Xg5 xg5 = Xg5.b;
            Ft5 ft5 = this.k;
            if (ft5 != null) {
                throw null;
//                Xg5.a(xg5, ((Gt5) ft5).getContext(), Xg5.Ai.NOTIFICATION_APPS, false, false, false, (Integer) null, 60, (Object) null);
//                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
        }
    }

    @DexIgnore
    public final boolean b(boolean z) {
        int i2;
        int i3;
        List<AppWrapper> list = this.g;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            i2 = 0;
            for (AppWrapper t : list) {
                InstalledApp installedApp = t.getInstalledApp();
                Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                if (isSelected != null) {
                    if (isSelected.booleanValue() == z && t.getUri() != null) {
                        int i4 = i2 + 1;
                        if (i4 >= 0) {
                            i2 = i4;
                        } else {
                            Qd6.b();
                            throw null;
                        }
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            }
        } else {
            i2 = 0;
        }
        List<AppWrapper> list2 = this.g;
        if (!(list2 instanceof Collection) || !list2.isEmpty()) {
            Iterator<AppWrapper> it = list2.iterator();
            i3 = 0;
            while (it.hasNext()) {
                if (it.next().getUri() != null) {
                    int i5 = i3 + 1;
                    if (i5 >= 0) {
                        i3 = i5;
                    } else {
                        Qd6.b();
                        throw null;
                    }
                }
            }
        } else {
            i3 = 0;
        }
        return i2 == i3;
    }

    @DexIgnore
    public final void c(boolean z) {
        InstalledApp installedApp;
        for (AppWrapper appWrapper : this.g) {
            if (!(appWrapper.getUri() == null || (installedApp = appWrapper.getInstalledApp()) == null)) {
                installedApp.setSelected(z);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(r, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        m();
        this.k.b();
        throw null;
//        Rm6 unused = Xh7.b(e(), null, null, new Ci(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(r, "stop");
        o();
        this.k.a();
    }

    @DexPrepend
    public void h() {  // function with "closeAndSave, nothing changed"
        List<AppWrapper> apps = this.g;
        StringBuilder sb = new StringBuilder();
        for (AppWrapper a : apps) {
            if (a.getInstalledApp().isSelected()) {
                sb.append(a.getInstalledApp().identifier);
                sb.append(",");
            }
        }
        try {
            File dir = Environment.getExternalStoragePublicDirectory("Fossil");
            NotificationAppsFragment frag = (NotificationAppsFragment) this.k;  // NotificationAppsFragment
            // Ask for storage permissions if not already granted
            PermissionUtils.a.c(frag.getActivity(), 255); // function with: "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"
            if (!dir.exists()) {
                dir.mkdir();
            }
            if (dir.exists()) {
                File enabledNotifications = new File(Environment.getExternalStoragePublicDirectory("Fossil"), "enabled_notifications.csv");
                FileOutputStream os = null;
                os = new FileOutputStream(enabledNotifications);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(os);
                outputStreamWriter.write(sb.toString());
                outputStreamWriter.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

//    @DexIgnore
//    @Override // com.fossil.Et5
//    public void h() {
//        if (!l()) {
//            this.p.a(this.e);
//            FLogger.INSTANCE.getLocal().d(r, "closeAndSave, nothing changed");
//            this.k.close();
//            return;
//        }
//        Xg5 xg5 = Xg5.b;
//        Ft5 ft5 = this.k;
//        throw null;
////        if (ft5 == null) {
////            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
////        } else if (Xg5.a(xg5, ((Gt5) ft5).getContext(), Xg5.Ai.SET_BLE_COMMAND, false, false, false, (Integer) null, 60, (Object) null)) {
////            FLogger.INSTANCE.getLocal().d(r, "setRuleToDevice, showProgressDialog");
////            this.k.b();
////            long currentTimeMillis = System.currentTimeMillis();
////            ILocalFLogger local = FLogger.INSTANCE.getLocal();
////            String str = r;
////            local.d(str, "filter notification, time start=" + currentTimeMillis);
////            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
////            String str2 = r;
////            local2.d(str2, "mListAppWrapper.size = " + this.g.size());
////            this.h.addAll(Mx6.a(this.g, false, 1, null));
////            long b = PortfolioApp.get.instance().b(new AppNotificationFilterSettings(this.h, System.currentTimeMillis()), PortfolioApp.get.instance().c());
////            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
////            String str3 = r;
////            local3.d(str3, "filter notification, time end= " + b);
////            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
////            String str4 = r;
////            local4.d(str4, "delayTime =" + (b - currentTimeMillis) + " mili seconds");
////        }
//    }

    @DexIgnore
    @Override // com.fossil.Et5
    public void i() {
        this.k.a();
        this.k.close();
    }

    @DexIgnore
    public final boolean j() {
        return this.e;
    }

    @DexIgnore
    public final List<AppWrapper> k() {
        return this.g;
    }


    @DexWrap
    public final boolean l() {
        if (ListRestored) {
            return true;
        }
        return l();
    }

//    @DexIgnore
//    /* JADX WARNING: Removed duplicated region for block: B:14:0x0049  */
//    public final boolean l() {
//        InstalledApp t;
//        ArrayList<InstalledApp> arrayList = new ArrayList();
//        for (AppWrapper appWrapper : this.g) {
//            InstalledApp installedApp = appWrapper.getInstalledApp();
//            if (installedApp != null) {
//                Boolean isSelected = installedApp.isSelected();
//                Wg6.a((Object) isSelected, "it.isSelected");
//                if (isSelected.booleanValue()) {
//                    arrayList.add(installedApp);
//                }
//            }
//        }
//        if (arrayList.size() != this.f.size()) {
//            return true;
//        }
//        for (InstalledApp installedApp2 : arrayList) {
//            Iterator<InstalledApp> it = this.f.iterator();
//            while (true) {
//                if (!it.hasNext()) {
//                    t = null;
//                    break;
//                }
//                InstalledApp next = it.next();
//                if (Wg6.a((Object) next.getIdentifier(), (Object) installedApp2.getIdentifier())) {
//                    t = next;
//                    break;
//                }
//            }
//            InstalledApp t2 = t;
//            if (t2 == null || (!Wg6.a(t2.isSelected(), installedApp2.isSelected()))) {
//                return true;
//            }
//            throw null;
////            while (r4.hasNext()) {
////            }
//        }
//        return false;
//    }

    @DexIgnore
    public final void m() {
        FLogger.INSTANCE.getLocal().d(r, "registerBroadcastReceiver");
        BleCommandResultManager.d.a(this.j, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public void n() {
        this.k.a(this);
    }

    @DexIgnore
    public final void o() {
        FLogger.INSTANCE.getLocal().d(r, "unregisterBroadcastReceiver");
        BleCommandResultManager.d.b(this.j, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }
}
