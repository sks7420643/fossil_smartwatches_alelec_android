package com.portfolio.platform.uirenew.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.Bo6;
import com.fossil.Bx6;
import com.fossil.Eo6;
import com.fossil.Gh6;
import com.fossil.Gs5;
import com.fossil.He;
import com.fossil.Hh6;
import com.fossil.Ho5;
import com.fossil.Iy6;
import com.fossil.Je;
import com.fossil.Lq4;
import com.fossil.M25;
import com.fossil.Qb;
import com.fossil.Qe5;
import com.fossil.Qr5;
import com.fossil.Qw6;
import com.fossil.Qz6;
import com.fossil.Ro5;
import com.fossil.Rr5;
import com.fossil.Tm4;
import com.fossil.Vx6;
import com.fossil.Xz6;
import com.fossil.Zd;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mapped.AlertDialogFragment;
import com.mapped.Cd6;
import com.mapped.HomeAlertsFragment;
import com.mapped.HomeAlertsHybridFragment;
import com.mapped.HomeDashboardFragment;
import com.mapped.HomeDianaCustomizeFragment;
import com.mapped.HomeHybridCustomizeFragment;
import com.mapped.HomeProfileFragment;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD, staticConstructorAction = DexAction.APPEND)
public final class HomeFragment extends Ho5 implements Rr5, AlertDialogFragment.Gi, Bo6.Ai {
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ Ai y; // = new Ai(null);
    @DexIgnore
    public HomeDashboardPresenter g;
    @DexIgnore
    public HomeDianaCustomizePresenter h;
    @DexIgnore
    public HomeHybridCustomizePresenter i;
    @DexIgnore
    public HomeProfilePresenter j;
    @DexIgnore
    public HomeAlertsPresenter p;
    @DexIgnore
    public HomeAlertsHybridPresenter q;
    @DexIgnore
    public Hh6 r;
    @DexIgnore
    public Qr5 s;
    @DexIgnore
    public Qw6<M25> t;
    @DexIgnore
    public /* final */ ArrayList<Fragment> u; // = new ArrayList<>();
    @DexIgnore
    public Xz6 v;
    @DexIgnore
    public HashMap w;

    @DexAdd
    public static Alarm alarmToAdd = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final HomeFragment a() {
            return new HomeFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements BottomNavigationView.d {
        @DexIgnore
        public /* final */ /* synthetic */ com.mapped.Hh6 a;
        @DexIgnore
        public /* final */ /* synthetic */ com.mapped.Hh6 b;
        @DexIgnore
        public /* final */ /* synthetic */ HomeFragment c;
        @DexIgnore
        public /* final */ /* synthetic */ Typeface d;

        @DexIgnore
        public Bi(com.mapped.Hh6 hh6, com.mapped.Hh6 hh62, HomeFragment homeFragment, String str, boolean z, Typeface typeface) {
            this.a = hh6;
            this.b = hh62;
            this.c = homeFragment;
            this.d = typeface;
        }

        @SuppressLint("ResourceType")
        @DexIgnore
        @Override // com.google.android.material.bottomnavigation.BottomNavigationView.d
        public final boolean a(MenuItem menuItem) {
            int i;
            Wg6.b(menuItem, "item");
            M25 m25 = (M25) HomeFragment.a(this.c).a();
            BottomNavigationView bottomNavigationView = m25 != null ? m25.q : null;
            if (bottomNavigationView != null) {
                Wg6.a((Object) bottomNavigationView, "mBinding.get()?.bottomNavigation!!");
                Menu menu = bottomNavigationView.getMenu();
                Wg6.a((Object) menu, "mBinding.get()?.bottomNavigation!!.menu");
                this.c.a(menu, this.d, this.a.element);
                menu.findItem(2131362176).setIcon(2131231084);
                Drawable c2 = W6.c(this.c.requireContext(), 2131231009);
                if (c2 != null) {
                    c2.setColorFilter(W6.a(PortfolioApp.get.instance(), 2131099820), PorterDuff.Mode.SRC_ATOP);
                }
                menuItem.setIcon(c2);
                MenuItem findItem = menu.findItem(2131361975);
                Wg6.a((Object) findItem, "menu.findItem(R.id.buddyChallengeFragment)");
                findItem.setIcon(c2);
                menu.findItem(2131362164).setIcon(2131231059);
                menu.findItem(2131362926).setIcon(2131231141);
                menu.findItem(2131361885).setIcon(2131230987);
                MenuItem findItem2 = menu.findItem(2131361885);
                Wg6.a((Object) findItem2, "menu.findItem(R.id.alertsFragment)");
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(findItem2.getTitle());
                spannableStringBuilder.setSpan(new ForegroundColorSpan(this.a.element), 0, spannableStringBuilder.length(), 0);
                menu.findItem(2131361885).setTitle(spannableStringBuilder);
                switch (menuItem.getItemId()) {
                    case 2131361885:
                        Drawable c3 = W6.c(this.c.requireContext(), 2131230988);
                        int i2 = this.b.element;
                        if (c3 != null) {
                            c3.setColorFilter(i2, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(c3);
                        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder2.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder2.length(), 0);
                        menu.findItem(2131361885).setTitle(spannableStringBuilder2);
                        i = 3;
                        break;
                    case 2131361975:
                        Drawable c4 = W6.c(this.c.requireContext(), 2131231011);
                        int i3 = this.b.element;
                        if (c4 != null) {
                            c4.setColorFilter(i3, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(c4);
                        SpannableStringBuilder spannableStringBuilder3 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder3.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder3.length(), 0);
                        menu.findItem(2131361975).setTitle(spannableStringBuilder3);
                        i = 1;
                        break;
                    case 2131362164:
                        Drawable c5 = W6.c(this.c.requireContext(), 2131231060);
                        int i4 = this.b.element;
                        if (c5 != null) {
                            c5.setColorFilter(i4, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(c5);
                        SpannableStringBuilder spannableStringBuilder4 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder4.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder4.length(), 0);
                        menu.findItem(2131362164).setTitle(spannableStringBuilder4);
                        i = 2;
                        break;
                    case 2131362176:
                        Drawable c6 = W6.c(this.c.requireContext(), 2131231085);
                        int i5 = this.b.element;
                        if (c6 != null) {
                            c6.setColorFilter(i5, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(c6);
                        SpannableStringBuilder spannableStringBuilder5 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder5.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder5.length(), 0);
                        menu.findItem(2131362176).setTitle(spannableStringBuilder5);
                        i = 0;
                        break;
                    case 2131362926:
                        Drawable c7 = W6.c(this.c.requireContext(), 2131231142);
                        int i6 = this.b.element;
                        if (c7 != null) {
                            c7.setColorFilter(i6, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(c7);
                        SpannableStringBuilder spannableStringBuilder6 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder6.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder6.length(), 0);
                        menu.findItem(2131362926).setTitle(spannableStringBuilder6);
                        i = 4;
                        break;
                    default:
                        i = 0;
                        break;
                }
                if (this.c.i != null && DeviceHelper.o.g(PortfolioApp.get.instance().c())) {
                    this.c.h1().b(i);
                }
                if (this.c.h != null && DeviceHelper.o.f(PortfolioApp.get.instance().c())) {
                    this.c.g1().b(i);
                }
                FLogger.INSTANCE.getLocal().d(HomeFragment.x, "show tab with tab=" + i);
                HomeFragment.b(this.c).a(i);
                this.c.o(i);
                return true;
            }
            Wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnLongClickListener {
        @DexIgnore
        public static /* final */ Ci a; // = new Ci();

        @DexIgnore
        public final boolean onLongClick(View view) {
            Wg6.a((Object) view, "item");
            view.setHapticFeedbackEnabled(false);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Zd<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeFragment a;

        @DexIgnore
        public Di(HomeFragment homeFragment) {
            this.a = homeFragment;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0071, code lost:
            if (((r0 == null || (r0 = r0.q) == null || (r0 = r0.getMenu()) == null || (r0 = r0.findItem(2131361975)) == null) ? false : r0.isVisible()) != false) goto L_0x0025;
         */
        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Integer num) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String l1 = HomeFragment.x;
            local.d(l1, "dashboardTab: " + num);
            if (num != null) {
                if (num.intValue() == 1) {
                    M25 m25 = (M25) HomeFragment.a(this.a).a();
                }
                this.a.n(num.intValue());
                HomeFragment.b(this.a).a(num.intValue());
                this.a.o(num.intValue());
            }
            num = 0;
            this.a.n(num.intValue());
            HomeFragment.b(this.a).a(num.intValue());
            this.a.o(num.intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeFragment a;

        @DexIgnore
        public Ei(HomeFragment homeFragment) {
            this.a = homeFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String l1 = HomeFragment.x;
            local.d(l1, "activeDeviceSerialLiveData onChange " + str);
            Qr5 b = HomeFragment.b(this.a);
            if (str != null) {
                b.a(str);
            } else {
                Wg6.a();
                throw null;
            }
        }
    }

    /*
    static {
        String simpleName = HomeFragment.class.getSimpleName();
        Wg6.a((Object) simpleName, "HomeFragment::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    public HomeFragment() {
        Eo6.b.a();
    }

    @DexIgnore
    public static final /* synthetic */ Qw6 a(HomeFragment homeFragment) {
        Qw6<M25> qw6 = homeFragment.t;
        if (qw6 != null) {
            return qw6;
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Qr5 b(HomeFragment homeFragment) {
        Qr5 qr5 = homeFragment.s;
        if (qr5 != null) {
            return qr5;
        }
        Wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void T(boolean z) {
        String b = ThemeManager.l.a().b("nonBrandSurface");
        Typeface c = ThemeManager.l.a().c("nonBrandTextStyle11");
        Qw6<M25> qw6 = this.t;
        if (qw6 != null) {
            M25 a = qw6.a();
            if (a != null) {
                if (b != null) {
                    a.q.setBackgroundColor(Color.parseColor(b));
                }
                BottomNavigationView bottomNavigationView = a.q;
                Wg6.a((Object) bottomNavigationView, "bottomNavigation");
                bottomNavigationView.getMenu().findItem(2131361975).setVisible(z);
                BottomNavigationView bottomNavigationView2 = a.q;
                Wg6.a((Object) bottomNavigationView2, "bottomNavigation");
                Menu menu = bottomNavigationView2.getMenu();
                Wg6.a((Object) menu, "bottomNavigation.menu");
                int size = menu.size();
                int i2 = 0;
                while (i2 < size) {
                    MenuItem item = menu.getItem(i2);
                    Wg6.a((Object) item, "getItem(index)");
                    Qw6<M25> qw62 = this.t;
                    if (qw62 != null) {
                        M25 a2 = qw62.a();
                        BottomNavigationView bottomNavigationView3 = a2 != null ? a2.q : null;
                        if (bottomNavigationView3 != null) {
                            bottomNavigationView3.findViewById(item.getItemId()).setOnLongClickListener(Ci.a);
                            i2++;
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    } else {
                        Wg6.d("mBinding");
                        throw null;
                    }
                }
                com.mapped.Hh6 hh6 = new com.mapped.Hh6();
                hh6.element = W6.a(requireContext(), 2131099942);
                com.mapped.Hh6 hh62 = new com.mapped.Hh6();
                hh62.element = W6.a(requireContext(), 2131099971);
                String b2 = ThemeManager.l.a().b("nonBrandDisableCalendarDay");
                String b3 = ThemeManager.l.a().b("primaryColor");
                if (b2 != null) {
                    hh6.element = Color.parseColor(b2);
                }
                if (b3 != null) {
                    hh62.element = Color.parseColor(b3);
                }
                BottomNavigationView bottomNavigationView4 = a.q;
                Wg6.a((Object) bottomNavigationView4, "bottomNavigation");
                bottomNavigationView4.setItemIconTintList(null);
                a.q.setOnNavigationItemSelectedListener(new Bi(hh6, hh62, this, b, z, c));
            }
            f1();
            return;
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Rr5
    public void W() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        StringBuilder sb = new StringBuilder();
        sb.append("onStartUpdateFw currentTab ");
        Qr5 qr5 = this.s;
        if (qr5 != null) {
            sb.append(qr5.h());
            local.d(str, sb.toString());
            Qr5 qr52 = this.s;
            if (qr52 != null) {
                o(qr52.h());
                HomeDashboardPresenter homeDashboardPresenter = this.g;
                if (homeDashboardPresenter == null) {
                    return;
                }
                if (homeDashboardPresenter != null) {
                    homeDashboardPresenter.m();
                } else {
                    Wg6.d("mHomeDashboardPresenter");
                    throw null;
                }
            } else {
                Wg6.d("mPresenter");
                throw null;
            }
        } else {
            Wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void Y(String str) {
        a(new Intent("android.intent.action.VIEW", Uri.parse(str)), x);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Ho5
    public void Z0() {
        HashMap hashMap = this.w;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Rr5
    public void a(Intent intent) {
        Wg6.b(intent, "intent");
        HomeDashboardPresenter homeDashboardPresenter = this.g;
        if (homeDashboardPresenter != null) {
            if (homeDashboardPresenter != null) {
                homeDashboardPresenter.a(intent);
            } else {
                Wg6.d("mHomeDashboardPresenter");
                throw null;
            }
        }
        HomeProfilePresenter homeProfilePresenter = this.j;
        if (homeProfilePresenter == null) {
            return;
        }
        if (homeProfilePresenter != null) {
            homeProfilePresenter.b(intent);
        } else {
            Wg6.d("mHomeProfilePresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void a(Menu menu, Typeface typeface, int i2) {
        if (typeface != null) {
            int size = menu.size();
            for (int i3 = 0; i3 < size; i3++) {
                MenuItem item = menu.getItem(i3);
                Wg6.a((Object) item, "getItem(index)");
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(item.getTitle());
                spannableStringBuilder.setSpan(new Iy6(typeface), 0, spannableStringBuilder.length(), 0);
                spannableStringBuilder.setSpan(new ForegroundColorSpan(i2), 0, spannableStringBuilder.length(), 0);
                item.setTitle(spannableStringBuilder);
            }
        }
    }

    @DexIgnore
    public void a(Qr5 qr5) {
        Wg6.b(qr5, "presenter");
        this.s = qr5;
    }

    @DexIgnore
    public final void a(FossilDeviceSerialPatternUtil.DEVICE device) {
        FLogger.INSTANCE.getLocal().d(x, "Inside .showMainFlow");
        HomeDashboardFragment a = HomeDashboardFragment.X.a();
        Fragment b = getChildFragmentManager().b(Lq4.p.a());
        Fragment b2 = getChildFragmentManager().b("HomeDianaCustomizeFragment");
        Fragment b3 = getChildFragmentManager().b("HomeHybridCustomizeFragment");
        Fragment b4 = getChildFragmentManager().b("HomeAlertsFragment");
        Fragment b5 = getChildFragmentManager().b("HomeAlertsHybridFragment");
        Fragment b6 = getChildFragmentManager().b("HomeProfileFragment");
        Fragment b7 = getChildFragmentManager().b("HomeUpdateFirmwareFragment");
        if (b2 == null) {
            b2 = HomeDianaCustomizeFragment.w.a();
        }
        if (b == null) {
            b = Lq4.p.b();
        }
        if (b3 == null) {
            b3 = HomeHybridCustomizeFragment.w.a();
        }
        if (b4 == null) {
            b4 = HomeAlertsFragment.r.a();
        }
        if (b5 == null) {
            b5 = HomeAlertsHybridFragment.p.a();
        }
        if (b6 == null) {
            b6 = HomeProfileFragment.q.a();
        }
        if (b7 == null) {
            b7 = Gh6.s.a();
        }
        this.u.clear();
        this.u.add(a);
        this.u.add(b);
        if (DeviceHelper.o.a(device)) {
            this.u.add(b2);
            this.u.add(b4);
        } else {
            this.u.add(b3);
            this.u.add(b5);
        }
        this.u.add(b6);
        this.u.add(b7);
        Qw6<M25> qw6 = this.t;
        if (qw6 != null) {
            M25 a2 = qw6.a();
            if (a2 != null) {
                try {
                    ViewPager2 viewPager2 = a2.s;
                    Wg6.a((Object) viewPager2, "binding.rvTabs");
                    viewPager2.setAdapter(new Qz6(getChildFragmentManager(), this.u));
                    if (a2.s.getChildAt(0) != null) {
                        View childAt = a2.s.getChildAt(0);
                        if (childAt != null) {
                            ((RecyclerView) childAt).setItemViewCacheSize(4);
                        } else {
                            throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                        }
                    }
                    ViewPager2 viewPager22 = a2.s;
                    Wg6.a((Object) viewPager22, "binding.rvTabs");
                    viewPager22.setUserInputEnabled(false);
                    Cd6 cd6 = Cd6.a;
                } catch (Exception e) {
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        HomeActivity.a aVar = HomeActivity.z;
                        Wg6.a((Object) activity, "it");
                        Qr5 qr5 = this.s;
                        if (qr5 != null) {
                            aVar.a(activity, Integer.valueOf(qr5.h()));
                            Cd6 cd62 = Cd6.a;
                        } else {
                            Wg6.d("mPresenter");
                            throw null;
                        }
                    }
                }
            }
            Iface iface = PortfolioApp.get.instance().getIface();
            if (b2 != null) {
                HomeDianaCustomizeFragment homeDianaCustomizeFragment = (HomeDianaCustomizeFragment) b2;
                if (b3 != null) {
                    HomeHybridCustomizeFragment homeHybridCustomizeFragment = (HomeHybridCustomizeFragment) b3;
                    if (b6 != null) {
                        HomeProfileFragment homeProfileFragment = (HomeProfileFragment) b6;
                        if (b4 != null) {
                            HomeAlertsFragment homeAlertsFragment = (HomeAlertsFragment) b4;
                            if (b5 != null) {
                                HomeAlertsHybridFragment homeAlertsHybridFragment = (HomeAlertsHybridFragment) b5;
                                if (b7 != null) {
                                    iface.a(new Gs5(a, homeDianaCustomizeFragment, homeHybridCustomizeFragment, homeProfileFragment, homeAlertsFragment, homeAlertsHybridFragment, (Gh6) b7)).a(this);
                                    return;
                                }
                                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment");
                            }
                            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
                        }
                        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
                    }
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexReplace
    @Override // com.fossil.Rr5
    public void a(FossilDeviceSerialPatternUtil.DEVICE device, int i2, boolean z) { // called by HomePresenter.Gi.invokeSuspend
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "updateFragmentList with deviceType " + device + " appMode " + i2);
        if (!z) {
            Qe5.c.a();
        }
        T(z);
        if (i2 != 1) {
            a(device);
        } else {
            k1();
        }
        i1();
        if (alarmToAdd != null) {
            HomeAlertsPresenter ap = HomeFragment.this.p;
            if (ap != null) {
                ap.alarmToAdd = alarmToAdd;
            }
            HomeAlertsHybridPresenter ahp = HomeFragment.this.q;
            if (ahp != null) {
                ahp.alarmToAdd = alarmToAdd;
            }
            alarmToAdd = null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Bo6.Ai
    public void a(InAppNotification inAppNotification) {
        Qr5 qr5 = this.s;
        if (qr5 == null) {
            Wg6.d("mPresenter");
            throw null;
        } else if (inAppNotification != null) {
            qr5.a(inAppNotification);
        } else {
            Wg6.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ho5, com.mapped.AlertDialogFragment.Gi
    public void a(String str, int i2, Intent intent) {
        Wg6.b(str, "tag");
        switch (str.hashCode()) {
            case -1944405936:
                if (str.equals("FIRMWARE_UPDATE_FAIL") && i2 == 2131363307) {
                    UpdateFirmwareActivity.a aVar = UpdateFirmwareActivity.A;
                    FragmentActivity requireActivity = requireActivity();
                    Wg6.a((Object) requireActivity, "requireActivity()");
                    aVar.a(requireActivity, PortfolioApp.get.instance().c(), false);
                    return;
                }
                return;
            case 986357734:
                if (!str.equals("FEEDBACK_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363229) {
                    FLogger.INSTANCE.getLocal().d(x, "Cancel Zendesk feedback");
                    return;
                } else if (i2 != 2131363307) {
                    return;
                } else {
                    if (!Tm4.a.a().l()) {
                        FLogger.INSTANCE.getLocal().d(x, "Go to Zendesk feedback");
                        Qr5 qr5 = this.s;
                        if (qr5 != null) {
                            qr5.b("Feedback - From app [Fossil] - [Android]");
                            return;
                        } else {
                            Wg6.d("mPresenter");
                            throw null;
                        }
                    } else {
                        String a = Vx6.a(Vx6.Ci.REPAIR_CENTER, null);
                        Wg6.a((Object) a, "URLHelper.buildStaticPag\u2026Page.REPAIR_CENTER, null)");
                        Y(a);
                        return;
                    }
                }
            case 1390226280:
                if (!str.equals("HAPPINESS_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363229) {
                    FLogger.INSTANCE.getLocal().d(x, "Send Zendesk feedback");
                    Bx6 bx6 = Bx6.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    Wg6.a((Object) childFragmentManager, "childFragmentManager");
                    bx6.o(childFragmentManager);
                    return;
                } else if (i2 == 2131363307) {
                    FLogger.INSTANCE.getLocal().d(x, "Open app rating dialog");
                    Bx6 bx62 = Bx6.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    Wg6.a((Object) childFragmentManager2, "childFragmentManager");
                    bx62.f(childFragmentManager2);
                    return;
                } else {
                    return;
                }
            case 1431920636:
                if (!str.equals("APP_RATING_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363229) {
                    FLogger.INSTANCE.getLocal().d(x, "Stay in app");
                    return;
                } else if (i2 == 2131363307) {
                    FLogger.INSTANCE.getLocal().d(x, "Go to Play Store");
                    PortfolioApp instance = PortfolioApp.get.instance();
                    AppHelper.Ai ai = AppHelper.g;
                    String packageName = instance.getPackageName();
                    Wg6.a((Object) packageName, "packageName");
                    ai.b(instance, packageName);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    @DexIgnore
    @Override // com.fossil.Rr5
    public void b(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        Wg6.b(zendeskFeedbackConfiguration, "configuration");
        Context context = getContext();
        if (context != null) {
            Intent intent = new Intent(context, ContactZendeskActivity.class);
            intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
            startActivityForResult(intent, 1007);
        }
    }

    @DexIgnore
    @Override // com.fossil.Rr5
    public void c(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        StringBuilder sb = new StringBuilder();
        sb.append("onUpdateFwComplete currentTab ");
        Qr5 qr5 = this.s;
        if (qr5 != null) {
            sb.append(qr5.h());
            local.d(str, sb.toString());
            HomeDashboardPresenter homeDashboardPresenter = this.g;
            if (homeDashboardPresenter != null) {
                if (homeDashboardPresenter != null) {
                    homeDashboardPresenter.b(z);
                } else {
                    Wg6.d("mHomeDashboardPresenter");
                    throw null;
                }
            }
            Qr5 qr52 = this.s;
            if (qr52 != null) {
                o(qr52.h());
                if (!z && isActive()) {
                    Bx6 bx6 = Bx6.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    Wg6.a((Object) childFragmentManager, "childFragmentManager");
                    bx6.W(childFragmentManager);
                    return;
                }
                return;
            }
            Wg6.d("mPresenter");
            throw null;
        }
        Wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Rr5
    public void f(int i2) {
        HomeDashboardPresenter homeDashboardPresenter = this.g;
        if (homeDashboardPresenter == null) {
            return;
        }
        if (homeDashboardPresenter != null) {
            homeDashboardPresenter.c(i2);
        } else {
            Wg6.d("mHomeDashboardPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void f1() {
        int i2 = 0;
        Resources resources = getResources();
        Wg6.a((Object) resources, "resources");
        int applyDimension = (int) TypedValue.applyDimension(1, 28.0f, resources.getDisplayMetrics());
        Qw6<M25> qw6 = this.t;
        if (qw6 != null) {
            M25 a = qw6.a();
            if (a != null) {
                View childAt = a.q.getChildAt(0);
                if (childAt != null) {
                    BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) childAt;
                    int childCount = bottomNavigationMenuView.getChildCount() - 1;
                    if (childCount >= 0) {
                        while (true) {
                            @SuppressLint("ResourceType") View findViewById = bottomNavigationMenuView.getChildAt(i2).findViewById(2131362579);
                            Wg6.a((Object) findViewById, "icon");
                            findViewById.getLayoutParams().width = applyDimension;
                            findViewById.getLayoutParams().height = applyDimension;
                            if (i2 != childCount) {
                                i2++;
                            } else {
                                return;
                            }
                        }
                    }
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.google.android.material.bottomnavigation.BottomNavigationMenuView");
                }
            } else {
                Wg6.a();
                throw null;
            }
        } else {
            Wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final HomeDianaCustomizePresenter g1() {
        HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.h;
        if (homeDianaCustomizePresenter != null) {
            return homeDianaCustomizePresenter;
        }
        Wg6.d("mHomeDianaCustomizePresenter");
        throw null;
    }

    @DexIgnore
    public final HomeHybridCustomizePresenter h1() {
        HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.i;
        if (homeHybridCustomizePresenter != null) {
            return homeHybridCustomizePresenter;
        }
        Wg6.d("mHomeHybridCustomizePresenter");
        throw null;
    }

    @DexIgnore
    public final void i1() {
        j1();
        Qr5 qr5 = this.s;
        if (qr5 != null) {
            int h2 = qr5.h();
            if (h2 == 0) {
                Qw6<M25> qw6 = this.t;
                if (qw6 != null) {
                    M25 a = qw6.a();
                    if (a != null) {
                        BottomNavigationView bottomNavigationView = a.q;
                        Wg6.a((Object) bottomNavigationView, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView.setSelectedItemId(2131362176);
                        return;
                    }
                    Wg6.a();
                    throw null;
                }
                Wg6.d("mBinding");
                throw null;
            } else if (h2 == 1) {
                Qw6<M25> qw62 = this.t;
                if (qw62 != null) {
                    M25 a2 = qw62.a();
                    if (a2 != null) {
                        BottomNavigationView bottomNavigationView2 = a2.q;
                        Wg6.a((Object) bottomNavigationView2, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView2.setSelectedItemId(2131361975);
                        return;
                    }
                    Wg6.a();
                    throw null;
                }
                Wg6.d("mBinding");
                throw null;
            } else if (h2 == 2) {
                Qw6<M25> qw63 = this.t;
                if (qw63 != null) {
                    M25 a3 = qw63.a();
                    if (a3 != null) {
                        BottomNavigationView bottomNavigationView3 = a3.q;
                        Wg6.a((Object) bottomNavigationView3, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView3.setSelectedItemId(2131362164);
                        return;
                    }
                    Wg6.a();
                    throw null;
                }
                Wg6.d("mBinding");
                throw null;
            } else if (h2 == 3) {
                Qw6<M25> qw64 = this.t;
                if (qw64 != null) {
                    M25 a4 = qw64.a();
                    if (a4 != null) {
                        BottomNavigationView bottomNavigationView4 = a4.q;
                        Wg6.a((Object) bottomNavigationView4, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView4.setSelectedItemId(2131361885);
                        return;
                    }
                    Wg6.a();
                    throw null;
                }
                Wg6.d("mBinding");
                throw null;
            } else if (h2 == 4) {
                Qw6<M25> qw65 = this.t;
                if (qw65 != null) {
                    M25 a5 = qw65.a();
                    if (a5 != null) {
                        BottomNavigationView bottomNavigationView5 = a5.q;
                        Wg6.a((Object) bottomNavigationView5, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView5.setSelectedItemId(2131362926);
                        return;
                    }
                    Wg6.a();
                    throw null;
                }
                Wg6.d("mBinding");
                throw null;
            }
        } else {
            Wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void j1() {
        Qr5 qr5 = this.s;
        if (qr5 == null) {
            return;
        }
        if (qr5 != null) {
            int h2 = qr5.h();
            int size = this.u.size();
            int i2 = 0;
            while (i2 < size) {
                if (this.u.get(i2) instanceof Ro5) {
                    Fragment fragment = this.u.get(i2);
                    if (fragment != null) {
                        ((Ro5) fragment).r(i2 == h2);
                    } else {
                        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.VisibleChangeListener");
                    }
                }
                i2++;
            }
            return;
        }
        Wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void k1() {
        FLogger.INSTANCE.getLocal().d(x, "Inside .showNoActiveDeviceFlow");
        HomeDashboardFragment a = HomeDashboardFragment.X.a();
        Fragment b = getChildFragmentManager().b("HomeDianaCustomizeFragment");
        Fragment b2 = getChildFragmentManager().b("HomeHybridCustomizeFragment");
        Fragment b3 = getChildFragmentManager().b("HomeAlertsFragment");
        Fragment b4 = getChildFragmentManager().b("HomeAlertsHybridFragment");
        Fragment b5 = getChildFragmentManager().b("HomeProfileFragment");
        Fragment b6 = getChildFragmentManager().b("HomeUpdateFirmwareFragment");
        Fragment b7 = getChildFragmentManager().b(Lq4.p.a());
        if (b == null) {
            b = HomeDianaCustomizeFragment.w.a();
        }
        if (b7 == null) {
            b7 = Lq4.p.b();
        }
        if (b2 == null) {
            b2 = HomeHybridCustomizeFragment.w.a();
        }
        if (b3 == null) {
            b3 = HomeAlertsFragment.r.a();
        }
        if (b4 == null) {
            b4 = HomeAlertsHybridFragment.p.a();
        }
        if (b5 == null) {
            b5 = HomeProfileFragment.q.a();
        }
        if (b6 == null) {
            b6 = Gh6.s.a();
        }
        this.u.clear();
        this.u.add(a);
        this.u.add(b7);
        this.u.add(b2);
        this.u.add(b3);
        this.u.add(b5);
        this.u.add(b6);
        Qw6<M25> qw6 = this.t;
        if (qw6 != null) {
            M25 a2 = qw6.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.s;
                Wg6.a((Object) viewPager2, "binding.rvTabs");
                viewPager2.setAdapter(new Qz6(getChildFragmentManager(), this.u));
                if (a2.s.getChildAt(0) != null) {
                    View childAt = a2.s.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setItemViewCacheSize(4);
                    } else {
                        throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
                ViewPager2 viewPager22 = a2.s;
                Wg6.a((Object) viewPager22, "binding.rvTabs");
                viewPager22.setUserInputEnabled(false);
            }
            Iface iface = PortfolioApp.get.instance().getIface();
            if (b != null) {
                HomeDianaCustomizeFragment homeDianaCustomizeFragment = (HomeDianaCustomizeFragment) b;
                if (b2 != null) {
                    HomeHybridCustomizeFragment homeHybridCustomizeFragment = (HomeHybridCustomizeFragment) b2;
                    if (b5 != null) {
                        HomeProfileFragment homeProfileFragment = (HomeProfileFragment) b5;
                        if (b3 != null) {
                            HomeAlertsFragment homeAlertsFragment = (HomeAlertsFragment) b3;
                            if (b4 != null) {
                                HomeAlertsHybridFragment homeAlertsHybridFragment = (HomeAlertsHybridFragment) b4;
                                if (b6 != null) {
                                    iface.a(new Gs5(a, homeDianaCustomizeFragment, homeHybridCustomizeFragment, homeProfileFragment, homeAlertsFragment, homeAlertsHybridFragment, (Gh6) b6)).a(this);
                                    return;
                                }
                                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment");
                            }
                            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
                        }
                        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
                    }
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void n(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "showBottomTab currentDashboardTab=" + i2);
        if (i2 == 0) {
            Qw6<M25> qw6 = this.t;
            if (qw6 != null) {
                M25 a = qw6.a();
                if (a != null) {
                    BottomNavigationView bottomNavigationView = a.q;
                    Wg6.a((Object) bottomNavigationView, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView.setSelectedItemId(2131362176);
                    return;
                }
                Wg6.a();
                throw null;
            }
            Wg6.d("mBinding");
            throw null;
        } else if (i2 == 1) {
            Qw6<M25> qw62 = this.t;
            if (qw62 != null) {
                M25 a2 = qw62.a();
                if (a2 != null) {
                    BottomNavigationView bottomNavigationView2 = a2.q;
                    Wg6.a((Object) bottomNavigationView2, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView2.setSelectedItemId(2131361975);
                    return;
                }
                Wg6.a();
                throw null;
            }
            Wg6.d("mBinding");
            throw null;
        } else if (i2 == 2) {
            Qw6<M25> qw63 = this.t;
            if (qw63 != null) {
                M25 a3 = qw63.a();
                if (a3 != null) {
                    BottomNavigationView bottomNavigationView3 = a3.q;
                    Wg6.a((Object) bottomNavigationView3, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView3.setSelectedItemId(2131362164);
                    return;
                }
                Wg6.a();
                throw null;
            }
            Wg6.d("mBinding");
            throw null;
        } else if (i2 == 3) {
            Qw6<M25> qw64 = this.t;
            if (qw64 != null) {
                M25 a4 = qw64.a();
                if (a4 != null) {
                    BottomNavigationView bottomNavigationView4 = a4.q;
                    Wg6.a((Object) bottomNavigationView4, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView4.setSelectedItemId(2131361885);
                    return;
                }
                Wg6.a();
                throw null;
            }
            Wg6.d("mBinding");
            throw null;
        } else if (i2 == 4) {
            Qw6<M25> qw65 = this.t;
            if (qw65 != null) {
                M25 a5 = qw65.a();
                if (a5 != null) {
                    BottomNavigationView bottomNavigationView5 = a5.q;
                    Wg6.a((Object) bottomNavigationView5, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView5.setSelectedItemId(2131362926);
                    return;
                }
                Wg6.a();
                throw null;
            }
            Wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void o(int i2) {
        if (i2 == 1) {
            Qw6<M25> qw6 = this.t;
            if (qw6 != null) {
                M25 a = qw6.a();
                if (a != null) {
                    ViewPager2 viewPager2 = a.s;
                    Wg6.a((Object) viewPager2, "mBinding.get()!!.rvTabs");
                    viewPager2.setDescendantFocusability(262144);
                } else {
                    Wg6.a();
                    throw null;
                }
            } else {
                Wg6.d("mBinding");
                throw null;
            }
        } else {
            Qw6<M25> qw62 = this.t;
            if (qw62 != null) {
                M25 a2 = qw62.a();
                if (a2 != null) {
                    ViewPager2 viewPager22 = a2.s;
                    Wg6.a((Object) viewPager22, "mBinding.get()!!.rvTabs");
                    viewPager22.setDescendantFocusability(393216);
                } else {
                    Wg6.a();
                    throw null;
                }
            } else {
                Wg6.d("mBinding");
                throw null;
            }
        }
        Qr5 qr5 = this.s;
        if (qr5 != null) {
            if (!qr5.i()) {
                Qw6<M25> qw63 = this.t;
                if (qw63 != null) {
                    M25 a3 = qw63.a();
                    if (a3 != null) {
                        a3.s.a(i2, false);
                    } else {
                        Wg6.a();
                        throw null;
                    }
                } else {
                    Wg6.d("mBinding");
                    throw null;
                }
            } else if (i2 > 0) {
                Qw6<M25> qw64 = this.t;
                if (qw64 != null) {
                    M25 a4 = qw64.a();
                    if (a4 != null) {
                        a4.s.a(5, false);
                    } else {
                        Wg6.a();
                        throw null;
                    }
                } else {
                    Wg6.d("mBinding");
                    throw null;
                }
            } else {
                Qw6<M25> qw65 = this.t;
                if (qw65 != null) {
                    M25 a5 = qw65.a();
                    if (a5 != null) {
                        a5.s.a(i2, false);
                    } else {
                        Wg6.a();
                        throw null;
                    }
                } else {
                    Wg6.d("mBinding");
                    throw null;
                }
            }
            j1();
            return;
        }
        Wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Rr5, androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "onActivityResult " + i2 + ' ' + i2);
        HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.h;
        if (homeDianaCustomizePresenter != null) {
            if (homeDianaCustomizePresenter != null) {
                homeDianaCustomizePresenter.a(i2, i3, intent);
            } else {
                Wg6.d("mHomeDianaCustomizePresenter");
                throw null;
            }
        }
        HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.i;
        if (homeHybridCustomizePresenter == null) {
            return;
        }
        if (homeHybridCustomizePresenter != null) {
            homeHybridCustomizePresenter.a(i2, i3, intent);
        } else {
            Wg6.d("mHomeHybridCustomizePresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        M25 m25 = (M25) Qb.a(layoutInflater, 2131558569, viewGroup, false, a1());
        this.t = new Qw6<>(this, m25);
        Wg6.a((Object) m25, "binding");
        return m25.d();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        Qr5 qr5 = this.s;
        if (qr5 != null) {
            qr5.j();
            super.onDestroy();
            return;
        }
        Wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Qr5 qr5 = this.s;
        if (qr5 != null) {
            qr5.g();
        } else {
            Wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Qr5 qr5 = this.s;
        if (qr5 != null) {
            qr5.f();
        } else {
            Wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        f1();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            He a = Je.a(activity).a(Xz6.class);
            Wg6.a((Object) a, "ViewModelProviders.of(ac\u2026ardViewModel::class.java)");
            Xz6 xz6 = (Xz6) a;
            this.v = xz6;
            if (xz6 != null) {
                xz6.a().a(activity, new Di(this));
            } else {
                Wg6.d("mHomeDashboardViewModel");
                throw null;
            }
        }
        PortfolioApp.get.instance().d().a(getViewLifecycleOwner(), new Ei(this));
    }

    @DexIgnore
    @Override // com.fossil.Rr5
    public void v0() {
        if (isActive()) {
            Bx6 bx6 = Bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.a((Object) childFragmentManager, "childFragmentManager");
            bx6.s(childFragmentManager);
        }
    }
}
