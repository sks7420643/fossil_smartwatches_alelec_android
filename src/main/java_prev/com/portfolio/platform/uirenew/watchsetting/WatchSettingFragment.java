package com.portfolio.platform.uirenew.watchsetting;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Aw;
import com.fossil.Bx6;
import com.fossil.El4;
import com.fossil.He;
import com.fossil.Ho5;
import com.fossil.Ib5;
import com.fossil.Iw;
import com.fossil.Je;
import com.fossil.Jf5;
import com.fossil.Mn4;
import com.fossil.Qb;
import com.fossil.Qw6;
import com.fossil.Rj4;
import com.fossil.Vt4;
import com.fossil.Zd;
import com.mapped.AlertDialogFragment;
import com.mapped.Jm4;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.WatchSettingFragmentBinding;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.AlelecPrefs;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardActivity;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexAppend;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
public final class WatchSettingFragment extends Ho5 implements AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public Qw6<WatchSettingFragmentBinding> g;
    @DexIgnore
    public WatchSettingViewModel h;
    @DexIgnore
    public String i; // = ThemeManager.l.a().b("primaryText");
    @DexIgnore
    public Iw j;
    @DexIgnore
    public Rj4 p;
    @DexIgnore
    public HashMap q;

    @DexIgnore
    WatchSettingFragment () {}

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return WatchSettingFragment.r;
        }

        @DexIgnore
        public final WatchSettingFragment b() {
            return new WatchSettingFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Zd<El4.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public Bi(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(El4.Ai ai) {
            if (ai != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = WatchSettingFragment.s.a();
                local.d(a2, "loadingState start " + ai.a() + " stop " + ai.b());
                if (ai.a()) {
                    this.a.g();
                }
                if (ai.b()) {
                    this.a.f();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Zd<El4.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public Ci(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(El4.Bi bi) {
            if (!bi.a().isEmpty()) {
                WatchSettingFragment watchSettingFragment = this.a;
                Object[] array = bi.a().toArray(new Ib5[0]);
                if (array != null) {
                    Ib5[] ib5Arr = (Ib5[]) array;
                    watchSettingFragment.a((Ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Zd<WatchSettingViewModel.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public Di(WatchSettingFragment watchSettingFragment, String str) {
            this.a = watchSettingFragment;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(WatchSettingViewModel.Ci ci) {
            if (ci != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = WatchSettingFragment.s.a();
                local.d(a2, "onUIState changed, modelWrapper=" + ci);
                if (ci.k() != null) {
                    WatchSettingFragment watchSettingFragment = this.a;
                    WatchSettingViewModel.Di k = ci.k();
                    if (k != null) {
                        watchSettingFragment.a(k);
                    } else {
                        Wg6.a();
                        throw null;
                    }
                }
                if (ci.a()) {
                    this.a.v();
                }
                if (ci.l() != null) {
                    WatchSettingFragment watchSettingFragment2 = this.a;
                    String l = ci.l();
                    if (l != null) {
                        watchSettingFragment2.e0(l);
                    } else {
                        Wg6.a();
                        throw null;
                    }
                }
                Integer m = ci.m();
                if (m != null) {
                    m.intValue();
                    WatchSettingFragment watchSettingFragment3 = this.a;
                    Integer m2 = ci.m();
                    if (m2 != null) {
                        watchSettingFragment3.n(m2.intValue());
                    } else {
                        Wg6.a();
                        throw null;
                    }
                }
                if (ci.f() != null) {
                    WatchSettingFragment watchSettingFragment4 = this.a;
                    Lc6<Integer, String> f = ci.f();
                    if (f != null) {
                        int intValue = f.getFirst().intValue();
                        Lc6<Integer, String> f2 = ci.f();
                        if (f2 != null) {
                            watchSettingFragment4.a(intValue, f2.getSecond());
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    } else {
                        Wg6.a();
                        throw null;
                    }
                }
                if (ci.d()) {
                    this.a.a0(this.b);
                }
                if (ci.c() != null) {
                    WatchSettingFragment watchSettingFragment5 = this.a;
                    String c = ci.c();
                    if (c != null) {
                        watchSettingFragment5.Z(c);
                    } else {
                        Wg6.a();
                        throw null;
                    }
                }
                if (ci.i() != null) {
                    WatchSettingFragment watchSettingFragment6 = this.a;
                    String i = ci.i();
                    if (i != null) {
                        watchSettingFragment6.d0(i);
                    } else {
                        Wg6.a();
                        throw null;
                    }
                }
                if (ci.e() != null) {
                    WatchSettingFragment watchSettingFragment7 = this.a;
                    String e = ci.e();
                    if (e != null) {
                        watchSettingFragment7.b0(e);
                    } else {
                        Wg6.a();
                        throw null;
                    }
                }
                if (ci.b() != null) {
                    WatchSettingFragment watchSettingFragment8 = this.a;
                    String b2 = ci.b();
                    if (b2 != null) {
                        watchSettingFragment8.Y(b2);
                    } else {
                        Wg6.a();
                        throw null;
                    }
                }
                if (ci.h() != null) {
                    WatchSettingFragment watchSettingFragment9 = this.a;
                    String h = ci.h();
                    if (h != null) {
                        watchSettingFragment9.c0(h);
                    } else {
                        Wg6.a();
                        throw null;
                    }
                }
                if (ci.g()) {
                    this.a.c();
                }
                if (ci.j()) {
                    this.a.g1();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Zd<WatchSettingViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public Ei(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(WatchSettingViewModel.Ai ai) {
            boolean b = ai.b();
            boolean c = ai.c();
            if (b) {
                String a2 = c ? Jm4.a(PortfolioApp.get.instance(), 2131886253) : Jm4.a(PortfolioApp.get.instance(), 2131886252);
                Bx6 bx6 = Bx6.c;
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                Wg6.a((Object) childFragmentManager, "childFragmentManager");
                Wg6.a((Object) a2, "title");
                String a3 = Jm4.a(PortfolioApp.get.instance(), 2131886238);
                Wg6.a((Object) a3, "LanguageHelper.getString\u2026ourCurrentChallengeFirst)");
                bx6.a(childFragmentManager, a2, a3, ai.a());
            } else if (!c) {
                Bx6 bx62 = Bx6.c;
                FragmentManager childFragmentManager2 = this.a.getChildFragmentManager();
                Wg6.a((Object) childFragmentManager2, "childFragmentManager");
                bx62.b(childFragmentManager2, WatchSettingFragment.a(this.a).h());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public Fi(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchSettingFragment.a(this.a).a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public Gi(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingFragment.s.a();
            local.d(a2, "getWatchSerial=" + WatchSettingFragment.a(this.a).l());
            if (this.a.isActive() && !TextUtils.isEmpty(WatchSettingFragment.a(this.a).l())) {
                FindDeviceActivity.a aVar = FindDeviceActivity.z;
                FragmentActivity requireActivity = this.a.requireActivity();
                Wg6.a((Object) requireActivity, "requireActivity()");
                String l = WatchSettingFragment.a(this.a).l();
                if (l != null) {
                    aVar.a(requireActivity, l);
                } else {
                    Wg6.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public Hi(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.isActive() && !TextUtils.isEmpty(WatchSettingFragment.a(this.a).l())) {
                CalibrationActivity.a aVar = CalibrationActivity.z;
                FragmentActivity requireActivity = this.a.requireActivity();
                Wg6.a((Object) requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public Ii(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchSettingFragment.a(this.a).a(100);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public Ji(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchSettingFragment.a(this.a).a(50);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public Ki(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchSettingFragment.a(this.a).a(25);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public Li(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public Mi(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchSettingFragment.a(this.a).e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragmentBinding a;
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment b;

        @DexIgnore
        public Ni(WatchSettingFragmentBinding watchSettingFragmentBinding, WatchSettingFragment watchSettingFragment, WatchSettingViewModel.Di di) {
            this.a = watchSettingFragmentBinding;
            this.b = watchSettingFragment;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            Wg6.b(str, "serial");
            Wg6.b(str2, "filePath");
            this.b.f1().a(str2).a(this.a.z);
        }
    }

    /*
    static {
        String simpleName = WatchSettingFragment.class.getSimpleName();
        Wg6.a((Object) simpleName, "WatchSettingFragment::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ WatchSettingViewModel a(WatchSettingFragment watchSettingFragment) {
        WatchSettingViewModel watchSettingViewModel = watchSettingFragment.h;
        if (watchSettingViewModel != null) {
            return watchSettingViewModel;
        }
        Wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void Y(String str) {
        Wg6.b(str, "serial");
        if (isActive()) {
            Bx6 bx6 = Bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.a((Object) childFragmentManager, "childFragmentManager");
            bx6.b(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void Z(String str) {
        Wg6.b(str, "serial");
        if (isActive()) {
            Bx6 bx6 = Bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.a((Object) childFragmentManager, "childFragmentManager");
            bx6.e(str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Ho5
    public void Z0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final String a(boolean z, int i2) {
        String a = Jm4.a(PortfolioApp.get.instance(), z ? 2131887143 : 2131887120);
        if (!z || i2 <= 0) {
            Wg6.a((Object) a, "connectedString");
            return a;
        }
        return a + ", " + i2 + '%';
    }

    @DexIgnore
    public final void a(int i2, String str) {
        Wg6.b(str, "message");
        if (isActive()) {
            Bx6 bx6 = Bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
    }

    @DexAppend
    public final void a(WatchSettingViewModel.Di di) {
        if (isActive()) {
            if (this.g != null) {
                WatchSettingFragmentBinding a2 = this.g.a();
                if (a2 != null) {
                    TextView r2 = a2.secret_key_value;
                    Wg6.a(r2, "it.secret_key_value");
                    r2.setText(PortfolioApp.get.instance().secretKey);
                }
            }
        }
    }
    /*
    @SuppressLint("WrongConstant")
    @DexIgnore
    public final void a(WatchSettingViewModel.Di di) {
        Wg6.b(di, "watchSetting");
        FLogger.INSTANCE.getLocal().d(r, "updateDeviceInfo");
        if (isActive()) {
            Qw6<WatchSettingFragmentBinding> qw6 = this.g;
            if (qw6 != null) {
                WatchSettingFragmentBinding a = qw6.a();
                if (a != null) {
                    FlexibleTextView flexibleTextView = a.P;
                    Wg6.a((Object) flexibleTextView, "it.tvSerialValue");
                    flexibleTextView.setText(di.a().getDeviceId());
                    FlexibleTextView flexibleTextView2 = a.K;
                    Wg6.a((Object) flexibleTextView2, "it.tvFwVersionValue");
                    flexibleTextView2.setText(di.a().getFirmwareRevision());
                    FlexibleTextView flexibleTextView3 = a.H;
                    Wg6.a((Object) flexibleTextView3, "it.tvDeviceName");
                    flexibleTextView3.setText(di.b());
                    boolean d = di.d();
                    if (d) {
                        Boolean c = di.c();
                        if (c != null && c.booleanValue()) {
                            Integer vibrationStrength = di.a().getVibrationStrength();
                            if (vibrationStrength != null) {
                                n(vibrationStrength.intValue());
                            } else {
                                Wg6.a();
                                throw null;
                            }
                        }
                        if (di.e()) {
                            FlexibleTextView flexibleTextView4 = a.H;
                            Wg6.a((Object) flexibleTextView4, "it.tvDeviceName");
                            flexibleTextView4.setAlpha(1.0f);
                            a.H.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, W6.c(PortfolioApp.get.instance(), 2131231071), (Drawable) null);
                            FlexibleTextView flexibleTextView5 = a.G;
                            Wg6.a((Object) flexibleTextView5, "it.tvConnectionStatus");
                            flexibleTextView5.setAlpha(1.0f);
                            FlexibleTextView flexibleTextView6 = a.G;
                            Wg6.a((Object) flexibleTextView6, "it.tvConnectionStatus");
                            flexibleTextView6.setText(a(true, di.a().getBatteryLevel()));
                            if (di.a().getBatteryLevel() >= 0) {
                                int batteryLevel = di.a().getBatteryLevel();
                                a.G.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, W6.c(PortfolioApp.get.instance(), (batteryLevel >= 0 && 25 >= batteryLevel) ? 2131231004 : (25 <= batteryLevel && 50 >= batteryLevel) ? 2131231006 : (50 <= batteryLevel && 75 >= batteryLevel) ? 2131231008 : 2131231002), (Drawable) null);
                            }
                            FlexibleButton flexibleButton = a.q;
                            Wg6.a((Object) flexibleButton, "it.btActive");
                            flexibleButton.setVisibility(0);
                            FlexibleButton flexibleButton2 = a.s;
                            Wg6.a((Object) flexibleButton2, "it.btConnect");
                            flexibleButton2.setVisibility(8);
                            FlexibleTextView flexibleTextView7 = a.E;
                            Wg6.a((Object) flexibleTextView7, "it.tvCalibration");
                            flexibleTextView7.setAlpha(1.0f);
                            FlexibleButton flexibleButton3 = a.w;
                            Wg6.a((Object) flexibleButton3, "it.fbVibrationHigh");
                            flexibleButton3.setEnabled(true);
                            FlexibleButton flexibleButton4 = a.x;
                            Wg6.a((Object) flexibleButton4, "it.fbVibrationLow");
                            flexibleButton4.setEnabled(true);
                            FlexibleButton flexibleButton5 = a.y;
                            Wg6.a((Object) flexibleButton5, "it.fbVibrationMedium");
                            flexibleButton5.setEnabled(true);
                            FlexibleTextView flexibleTextView8 = a.E;
                            Wg6.a((Object) flexibleTextView8, "it.tvCalibration");
                            flexibleTextView8.setEnabled(true);
                        } else {
                            a.H.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                            String str = this.i;
                            if (str != null) {
                                a.H.setTextColor(Color.parseColor(str));
                            }
                            FlexibleTextView flexibleTextView9 = a.H;
                            Wg6.a((Object) flexibleTextView9, "it.tvDeviceName");
                            flexibleTextView9.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView10 = a.G;
                            Wg6.a((Object) flexibleTextView10, "it.tvConnectionStatus");
                            flexibleTextView10.setText(a(false, di.a().getBatteryLevel()));
                            FlexibleTextView flexibleTextView11 = a.G;
                            Wg6.a((Object) flexibleTextView11, "it.tvConnectionStatus");
                            flexibleTextView11.setAlpha(0.4f);
                            FlexibleButton flexibleButton6 = a.q;
                            Wg6.a((Object) flexibleButton6, "it.btActive");
                            flexibleButton6.setVisibility(8);
                            FlexibleButton flexibleButton7 = a.s;
                            Wg6.a((Object) flexibleButton7, "it.btConnect");
                            flexibleButton7.setVisibility(0);
                            FlexibleButton flexibleButton8 = a.s;
                            Wg6.a((Object) flexibleButton8, "it.btConnect");
                            flexibleButton8.setText(Jm4.a(PortfolioApp.get.instance(), 2131887180));
                            FlexibleTextView flexibleTextView12 = a.E;
                            Wg6.a((Object) flexibleTextView12, "it.tvCalibration");
                            flexibleTextView12.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView13 = a.E;
                            Wg6.a((Object) flexibleTextView13, "it.tvCalibration");
                            flexibleTextView13.setEnabled(false);
                        }
                    } else if (!d) {
                        a.w.a("flexible_button_secondary");
                        a.y.a("flexible_button_secondary");
                        a.x.a("flexible_button_secondary");
                        a.H.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                        String str2 = this.i;
                        if (str2 != null) {
                            a.H.setTextColor(Color.parseColor(str2));
                        }
                        FlexibleTextView flexibleTextView14 = a.H;
                        Wg6.a((Object) flexibleTextView14, "it.tvDeviceName");
                        flexibleTextView14.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView15 = a.G;
                        Wg6.a((Object) flexibleTextView15, "it.tvConnectionStatus");
                        flexibleTextView15.setText(a(false, di.a().getBatteryLevel()));
                        FlexibleTextView flexibleTextView16 = a.G;
                        Wg6.a((Object) flexibleTextView16, "it.tvConnectionStatus");
                        flexibleTextView16.setAlpha(0.4f);
                        FlexibleButton flexibleButton9 = a.q;
                        Wg6.a((Object) flexibleButton9, "it.btActive");
                        flexibleButton9.setVisibility(8);
                        FlexibleButton flexibleButton10 = a.s;
                        Wg6.a((Object) flexibleButton10, "it.btConnect");
                        flexibleButton10.setText(Jm4.a(PortfolioApp.get.instance(), 2131887181));
                        FlexibleTextView flexibleTextView17 = a.E;
                        Wg6.a((Object) flexibleTextView17, "it.tvCalibration");
                        flexibleTextView17.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView18 = a.E;
                        Wg6.a((Object) flexibleTextView18, "it.tvCalibration");
                        flexibleTextView18.setEnabled(false);
                    }
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(di.a().getDeviceId()).setSerialPrefix(DeviceHelper.o.b(di.a().getDeviceId())).setType(Constants.DeviceType.TYPE_LARGE);
                    ImageView imageView = a.z;
                    Wg6.a((Object) imageView, "it.ivDevice");
                    type.setPlaceHolder(imageView, DeviceHelper.o.b(di.a().getDeviceId(), DeviceHelper.Bi.SMALL)).setImageCallback(new Ni(a, this, di)).download();
                    return;
                }
                return;
            }
            Wg6.d("mBinding");
            throw null;
        }
    }
    */

    @DexIgnore
    @Override // com.fossil.Ho5, com.mapped.AlertDialogFragment.Gi
    public void a(String str, int i2, Intent intent) {
        Wg6.b(str, "tag");
        switch (str.hashCode()) {
            case -2051261777:
                if (str.equals("REMOVE_DEVICE_WORKOUT")) {
                    String stringExtra = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra == null) {
                        return;
                    }
                    if (i2 == 2131363229) {
                        WatchSettingViewModel watchSettingViewModel = this.h;
                        if (watchSettingViewModel != null) {
                            watchSettingViewModel.g(stringExtra);
                            return;
                        } else {
                            Wg6.d("mViewModel");
                            throw null;
                        }
                    } else if (i2 == 2131363307) {
                        WatchSettingViewModel watchSettingViewModel2 = this.h;
                        if (watchSettingViewModel2 != null) {
                            watchSettingViewModel2.c(stringExtra);
                            return;
                        } else {
                            Wg6.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -1138109835:
                if (str.equals("SWITCH_DEVICE_ERASE_FAIL")) {
                    String stringExtra2 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra2 != null && i2 == 2131363307) {
                        WatchSettingViewModel watchSettingViewModel3 = this.h;
                        if (watchSettingViewModel3 != null) {
                            watchSettingViewModel3.e(stringExtra2);
                            return;
                        } else {
                            Wg6.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -693701870:
                if (str.equals("CONFIRM_REMOVE_DEVICE")) {
                    if (i2 == 2131363307) {
                        WatchSettingViewModel watchSettingViewModel4 = this.h;
                        if (watchSettingViewModel4 != null) {
                            watchSettingViewModel4.n();
                            return;
                        } else {
                            Wg6.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -454228492:
                if (str.equals("REMOVE_DEVICE_SYNC_FAIL")) {
                    String stringExtra3 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra3 != null && i2 == 2131363307) {
                        WatchSettingViewModel watchSettingViewModel5 = this.h;
                        if (watchSettingViewModel5 != null) {
                            watchSettingViewModel5.b(stringExtra3);
                            return;
                        } else {
                            Wg6.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 39550276:
                if (str.equals("SWITCH_DEVICE_SYNC_FAIL")) {
                    String stringExtra4 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra4 != null && i2 == 2131363307) {
                        WatchSettingViewModel watchSettingViewModel6 = this.h;
                        if (watchSettingViewModel6 != null) {
                            watchSettingViewModel6.f(stringExtra4);
                            return;
                        } else {
                            Wg6.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 603997695:
                if (str.equals("SWITCH_DEVICE_WORKOUT")) {
                    String stringExtra5 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra5 == null) {
                        return;
                    }
                    if (i2 == 2131363229) {
                        WatchSettingViewModel watchSettingViewModel7 = this.h;
                        if (watchSettingViewModel7 != null) {
                            watchSettingViewModel7.h(stringExtra5);
                            return;
                        } else {
                            Wg6.d("mViewModel");
                            throw null;
                        }
                    } else if (i2 == 2131363307) {
                        WatchSettingViewModel watchSettingViewModel8 = this.h;
                        if (watchSettingViewModel8 != null) {
                            watchSettingViewModel8.d(stringExtra5);
                            return;
                        } else {
                            Wg6.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 1970588827:
                if (str.equals("LEAVE_CHALLENGE")) {
                    a();
                    if (i2 == 2131363307) {
                        Mn4 mn4 = intent != null ? (Mn4) intent.getParcelableExtra("CHALLENGE") : null;
                        if (mn4 != null) {
                            b(mn4);
                            return;
                        }
                        return;
                    }
                    return;
                }
                break;
        }
        super.a(str, i2, intent);
    }

    @DexIgnore
    public final void a0(String str) {
        Wg6.b(str, "serial");
        if (getActivity() != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
            Context requireContext = requireContext();
            Wg6.a((Object) requireContext, "requireContext()");
            throw null;
//            TroubleshootingActivity.a.a(aVar, requireContext, str, false, false, 12, null);
        }
    }

    @DexIgnore
    public final void b(Mn4 mn4) {
        long b = Vt4.a.b();
        Date m = mn4.m();
        if (b > (m != null ? m.getTime() : 0)) {
            BCOverviewLeaderBoardActivity.y.a(this, mn4, null);
        } else {
            BCWaitingChallengeDetailActivity.z.a(this, mn4, "joined_challenge", -1, false);
        }
    }

    @DexIgnore
    public final void b0(String str) {
        Wg6.b(str, "serial");
        if (isActive()) {
            Bx6 bx6 = Bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.a((Object) childFragmentManager, "childFragmentManager");
            bx6.d(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void c() {
        if (isActive()) {
            Bx6 bx6 = Bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.a((Object) childFragmentManager, "childFragmentManager");
            bx6.n(childFragmentManager);
        }
    }

    @DexIgnore
    public final void c0(String str) {
        Wg6.b(str, "serial");
        if (isActive()) {
            Bx6 bx6 = Bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.a((Object) childFragmentManager, "childFragmentManager");
            bx6.c(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void d0(String str) {
        Wg6.b(str, "serial");
        if (isActive()) {
            Bx6 bx6 = Bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.a((Object) childFragmentManager, "childFragmentManager");
            bx6.f(str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String d1() {
        return r;
    }

    @DexIgnore
    public final void e0(String str) {
        Wg6.b(str, "lastSync");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = r;
        local.d(str2, "updateLastSync, lastSync=" + str);
        if (isActive()) {
            Qw6<WatchSettingFragmentBinding> qw6 = this.g;
            if (qw6 != null) {
                WatchSettingFragmentBinding a = qw6.a();
                if (a != null) {
                    FlexibleTextView flexibleTextView = a.M;
                    Wg6.a((Object) flexibleTextView, "it.tvLastSyncValue");
                    flexibleTextView.setText(str);
                    FlexibleTextView flexibleTextView2 = a.M;
                    Wg6.a((Object) flexibleTextView2, "it.tvLastSyncValue");
                    flexibleTextView2.setSelected(true);
                    return;
                }
                return;
            }
            Wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean e1() {
        return false;
    }

    @DexIgnore
    public final void f() {
        FLogger.INSTANCE.getLocal().d(r, "stopLoading");
        a();
    }

    @DexIgnore
    public final Iw f1() {
        Iw iw = this.j;
        if (iw != null) {
            return iw;
        }
        Wg6.d("mRequestManager");
        throw null;
    }

    @DexIgnore
    public final void g() {
        FLogger.INSTANCE.getLocal().d(r, "startLoading");
        b();
    }

    @DexIgnore
    public final void g1() {
        FlexibleButton flexibleButton;
        Qw6<WatchSettingFragmentBinding> qw6 = this.g;
        if (qw6 != null) {
            WatchSettingFragmentBinding a = qw6.a();
            if (a != null && (flexibleButton = a.s) != null) {
                flexibleButton.setText(Jm4.a(PortfolioApp.get.instance(), 2131887180));
                return;
            }
            return;
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void n(int i2) {
        Qw6<WatchSettingFragmentBinding> qw6 = this.g;
        if (qw6 != null) {
            WatchSettingFragmentBinding a = qw6.a();
            if (a != null) {
                a.w.a("flexible_button_secondary");
                a.y.a("flexible_button_secondary");
                a.x.a("flexible_button_secondary");
                if (i2 == 25) {
                    a.x.a("flexible_button_primary");
                } else if (i2 == 50) {
                    a.y.a("flexible_button_primary");
                } else if (i2 == 100) {
                    a.w.a("flexible_button_primary");
                }
            }
        } else {
            Wg6.d("mBinding");
            throw null;
        }
    }

    @SuppressLint("WrongConstant")
    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        WatchSettingFragmentBinding watchSettingFragmentBinding = (WatchSettingFragmentBinding) Qb.a(layoutInflater, 2131558637, viewGroup, false, a1());
        PortfolioApp.get.instance().getIface().n().a(this);
        Rj4 rj4 = this.p;
        if (rj4 != null) {
            He a = Je.a(this, rj4).a(WatchSettingViewModel.class);
            Wg6.a((Object) a, "ViewModelProviders.of(th\u2026ingViewModel::class.java)");
            WatchSettingViewModel watchSettingViewModel = (WatchSettingViewModel) a;
            this.h = watchSettingViewModel;
            if (watchSettingViewModel != null) {
                watchSettingViewModel.o();
                Bundle arguments = getArguments();
                Object obj = arguments != null ? arguments.get("SERIAL") : null;
                if (obj != null) {
                    String str = (String) obj;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = r;
                    local.d(str2, "serial=" + str);
                    WatchSettingViewModel watchSettingViewModel2 = this.h;
                    if (watchSettingViewModel2 != null) {
                        watchSettingViewModel2.i(str);
                        WatchSettingViewModel watchSettingViewModel3 = this.h;
                        if (watchSettingViewModel3 != null) {
                            watchSettingViewModel3.b().a(getViewLifecycleOwner(), new Bi(this));
                            WatchSettingViewModel watchSettingViewModel4 = this.h;
                            if (watchSettingViewModel4 != null) {
                                watchSettingViewModel4.d().a(getViewLifecycleOwner(), new Ci(this));
                                WatchSettingViewModel watchSettingViewModel5 = this.h;
                                if (watchSettingViewModel5 != null) {
                                    watchSettingViewModel5.k().a(getViewLifecycleOwner(), new Di(this, str));
                                    WatchSettingViewModel watchSettingViewModel6 = this.h;
                                    if (watchSettingViewModel6 != null) {
                                        if (watchSettingViewModel6.r()) {
                                            FlexibleTextView flexibleTextView = watchSettingFragmentBinding.R;
                                            Wg6.a((Object) flexibleTextView, "bindingLocal.tvVibration");
                                            flexibleTextView.setVisibility(0);
                                            FlexibleButton flexibleButton = watchSettingFragmentBinding.x;
                                            Wg6.a((Object) flexibleButton, "bindingLocal.fbVibrationLow");
                                            flexibleButton.setVisibility(0);
                                            FlexibleButton flexibleButton2 = watchSettingFragmentBinding.y;
                                            Wg6.a((Object) flexibleButton2, "bindingLocal.fbVibrationMedium");
                                            flexibleButton2.setVisibility(0);
                                            FlexibleButton flexibleButton3 = watchSettingFragmentBinding.w;
                                            Wg6.a((Object) flexibleButton3, "bindingLocal.fbVibrationHigh");
                                            flexibleButton3.setVisibility(0);
                                        } else {
                                            FlexibleTextView flexibleTextView2 = watchSettingFragmentBinding.R;
                                            Wg6.a((Object) flexibleTextView2, "bindingLocal.tvVibration");
                                            flexibleTextView2.setVisibility(8);
                                            FlexibleButton flexibleButton4 = watchSettingFragmentBinding.x;
                                            Wg6.a((Object) flexibleButton4, "bindingLocal.fbVibrationLow");
                                            flexibleButton4.setVisibility(8);
                                            FlexibleButton flexibleButton5 = watchSettingFragmentBinding.y;
                                            Wg6.a((Object) flexibleButton5, "bindingLocal.fbVibrationMedium");
                                            flexibleButton5.setVisibility(8);
                                            FlexibleButton flexibleButton6 = watchSettingFragmentBinding.w;
                                            Wg6.a((Object) flexibleButton6, "bindingLocal.fbVibrationHigh");
                                            flexibleButton6.setVisibility(8);
                                        }
                                        WatchSettingViewModel watchSettingViewModel7 = this.h;
                                        if (watchSettingViewModel7 != null) {
                                            watchSettingViewModel7.g().a(getViewLifecycleOwner(), new Ei(this));
                                            Qw6<WatchSettingFragmentBinding> qw6 = new Qw6<>(this, watchSettingFragmentBinding);
                                            this.g = qw6;
                                            if (qw6 != null) {
                                                WatchSettingFragmentBinding a2 = qw6.a();
                                                if (a2 != null) {
                                                    Wg6.a((Object) a2, "mBinding.get()!!");
                                                    return a2.d();
                                                }
                                                Wg6.a();
                                                throw null;
                                            }
                                            Wg6.d("mBinding");
                                            throw null;
                                        }
                                        Wg6.d("mViewModel");
                                        throw null;
                                    }
                                    Wg6.d("mViewModel");
                                    throw null;
                                }
                                Wg6.d("mViewModel");
                                throw null;
                            }
                            Wg6.d("mViewModel");
                            throw null;
                        }
                        Wg6.d("mViewModel");
                        throw null;
                    }
                    Wg6.d("mViewModel");
                    throw null;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.String");
            }
            Wg6.d("mViewModel");
            throw null;
        }
        Wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        WatchSettingViewModel watchSettingViewModel = this.h;
        if (watchSettingViewModel != null) {
            watchSettingViewModel.p();
            Jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
                return;
            }
            return;
        }
        Wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        WatchSettingViewModel watchSettingViewModel = this.h;
        if (watchSettingViewModel != null) {
            watchSettingViewModel.o();
            Jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        Wg6.d("mViewModel");
        throw null;
    }

    @DexAdd
    public static final class ClickCopy implements View.OnClickListener {
        public /* final */ /* synthetic */ WatchSettingFragment a;

        public ClickCopy(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        public final void onClick(View view) {
            String text = ((com.portfolio.platform.view.FlexibleTextView)view).getText().toString();
            ClipboardManager clipboard = (ClipboardManager) a.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("fossil", text);
            if (clipboard != null) {
                clipboard.setPrimaryClip(clip);
                Toast.makeText(a.getContext(), "Copied: " + text, Toast.LENGTH_SHORT).show();

            }

        }
    }

    @DexReplace
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View _view, Bundle bundle) {
        ConstraintLayout constraintLayout;
        ConstraintLayout constraintLayout2;
        Wg6.b(_view, "view");
        super.onViewCreated(_view, bundle);
        Iw a = Aw.a(this);
        Wg6.a((Object) a, "Glide.with(this)");
        this.j = a;
        Qw6<WatchSettingFragmentBinding> qw6 = this.g;
        if (qw6 != null) {
            WatchSettingFragmentBinding a2 = qw6.a();
            if (a2 != null) {
                a2.N.setOnClickListener(new Fi(this));
                a2.I.setOnClickListener(new Gi(this));
                a2.E.setOnClickListener(new Hi(this));
                String b = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
                if (!TextUtils.isEmpty(b) && (constraintLayout2 = a2.u) != null) {
                    constraintLayout2.setBackgroundColor(Color.parseColor(b));
                }
                String b2 = ThemeManager.l.a().b("nonBrandSurface");
                if (!TextUtils.isEmpty(b2) && (constraintLayout = a2.t) != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(b2));
                }
                a2.w.setOnClickListener(new Ii(this));
                a2.y.setOnClickListener(new Ji(this));
                a2.x.setOnClickListener(new Ki(this));
                a2.r.setOnClickListener(new Li(this));
                a2.s.setOnClickListener(new Mi(this));

//                a2.fw_version_value.setOnClickListener(new ClickCopy(this));
//                a2.serial_value.setOnClickListener(new ClickCopy(this));
                a2.secret_key_value.setOnClickListener(new ClickCopy(this));

                Context context = getContext();
                a2.switchAndroidDND.setChecked(AlelecPrefs.getAndroidDND(context));
                a2.switchAndroidDND.setOnClickListener(view -> {
                    FlexibleSwitchCompat sw = (FlexibleSwitchCompat)view;
                    AlelecPrefs.setAndroidDND(getContext(), sw.isChecked());
                    FLogger.INSTANCE.getLocal().v(r, "setAndroidDND: " + sw.isChecked());
                });
                a2.switchAndroidDND_HIGH.setChecked(AlelecPrefs.getAndroidDND_HIGH(context));
                a2.switchAndroidDND_HIGH.setOnClickListener(view -> {
                    FlexibleSwitchCompat sw = (FlexibleSwitchCompat)view;
                    AlelecPrefs.setAndroidDND_HIGH(getContext(), sw.isChecked());
                    FLogger.INSTANCE.getLocal().v(r, "setAndroidDND_HIGH: " + sw.isChecked());
                });
                a2.switchEmptyNotifications.setChecked(AlelecPrefs.getEmptyNotifications(context));
                a2.switchEmptyNotifications.setOnClickListener(view -> {
                    FlexibleSwitchCompat sw = (FlexibleSwitchCompat)view;
                    AlelecPrefs.setEmptyNotifications(getContext(), sw.isChecked());
                    FLogger.INSTANCE.getLocal().v(r, "setEmptyNotifications: " + sw.isChecked());
                });
                a2.switchAutoSync.setChecked(AlelecPrefs.getAutoSync(context));
                a2.switchAutoSync.setOnClickListener(view -> {
                    FlexibleSwitchCompat sw = (FlexibleSwitchCompat)view;
                    AlelecPrefs.setAutoSync(getContext(), sw.isChecked());
                    FLogger.INSTANCE.getLocal().v(r, "setAutoSync: " + sw.isChecked());
                });
            }
            V("watch_setting_view");
            return;
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void v() {
        FragmentActivity activity;
        FLogger.INSTANCE.getLocal().d(r, VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        if (isActive() && (activity = getActivity()) != null) {
            activity.finish();
        }
    }
}
