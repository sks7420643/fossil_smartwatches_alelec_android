package com.portfolio.platform;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

import lanchon.dexpatcher.annotation.DexAdd;

@DexAdd
public class AlelecPrefs {

    private static String PREFS_KEY = "alelec_prefs";

    private static final String PREFS_DND = "android_dnd";
    private static final String PREFS_DND_HIGH = "android_dnd_high";
    private static final String PREFS_EmptyNotification = "enpty_notifications";
    private static final String PREFS_AutoSync = "auto_sync";

    private static final Object registerLock = new Object();
    private static SettingsListener registered = null;

    private static ArrayList<SharedPreferences.OnSharedPreferenceChangeListener> listenerDND = new ArrayList<>();
    private static ArrayList<SharedPreferences.OnSharedPreferenceChangeListener> listenerEmptyNotification = new ArrayList<>();
    private static ArrayList<SharedPreferences.OnSharedPreferenceChangeListener> listenerAutoSync = new ArrayList<>();

    static class SettingsListener implements SharedPreferences.OnSharedPreferenceChangeListener {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            switch(s) {
                case PREFS_DND:
                case PREFS_DND_HIGH:
                    for (SharedPreferences.OnSharedPreferenceChangeListener listener: listenerDND) {
                        listener.onSharedPreferenceChanged(sharedPreferences, s);
                    }
                    break;
                case PREFS_EmptyNotification:
                    for (SharedPreferences.OnSharedPreferenceChangeListener listener: listenerEmptyNotification) {
                        listener.onSharedPreferenceChanged(sharedPreferences, s);
                    }
                    break;
                case PREFS_AutoSync:
                    for (SharedPreferences.OnSharedPreferenceChangeListener listener: listenerAutoSync) {
                        listener.onSharedPreferenceChanged(sharedPreferences, s);
                    }
                    break;
            }
        }
    }

    private static void registerListener(Context context) {
        synchronized (registerLock) {
            if (registered == null) {
                registered = new SettingsListener();
                getPrefs(context).registerOnSharedPreferenceChangeListener(registered);
            }
        }
    }

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(AlelecPrefs.PREFS_KEY, Context.MODE_PRIVATE);
    }

    public static void setAndroidDND_HIGH(Context context, Boolean on) {
        getPrefs(context).edit().putBoolean(PREFS_DND_HIGH, on).apply();
    }

    public static Boolean getAndroidDND_HIGH(Context context) {
        return getPrefs(context).getBoolean(PREFS_DND_HIGH, true);
    }

    public static void setAndroidDND(Context context, Boolean on) {
        getPrefs(context).edit().putBoolean(PREFS_DND, on).apply();
    }

    public static Boolean getAndroidDND(Context context) {
        return getPrefs(context).getBoolean(PREFS_DND, true);
    }

    public static void registerAndroidDND(Context context, SharedPreferences.OnSharedPreferenceChangeListener listener) {
        registerListener(context);
        listenerDND.add(listener);
    }

    public static void setEmptyNotifications(Context context, Boolean on) {
        getPrefs(context).edit().putBoolean(PREFS_EmptyNotification, on).apply();
    }

    public static Boolean getEmptyNotifications(Context context) {
        return getPrefs(context).getBoolean(PREFS_EmptyNotification, true);
    }

    public static void registerEmptyNotifications(Context context, SharedPreferences.OnSharedPreferenceChangeListener listener) {
        registerListener(context);
        listenerEmptyNotification.add(listener);
    }

    public static void setAutoSync(Context context, Boolean on) {
        getPrefs(context).edit().putBoolean(PREFS_AutoSync, on).apply();
    }

    public static Boolean getAutoSync(Context context) {
        return getPrefs(context).getBoolean(PREFS_AutoSync, true);
    }

    public static void registerAutoSync(Context context, SharedPreferences.OnSharedPreferenceChangeListener listener) {
        registerListener(context);
        listenerAutoSync.add(listener);
    }

}
