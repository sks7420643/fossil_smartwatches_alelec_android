package com.mapped;

import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomizeWidget;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexEdit(defaultAction = DexAction.IGNORE)
public abstract class WatchFacePreviewFragmentBinding extends ViewDataBinding {
    @DexIgnore
    public /* final */ CustomizeWidget A;
    @DexIgnore
    public /* final */ View q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ TextView u;
    @DexIgnore
    public /* final */ TextView v;
    @DexIgnore
    public /* final */ TextView w;
    @DexIgnore
    public /* final */ CustomizeWidget x;
    @DexIgnore
    public /* final */ CustomizeWidget y;
    @DexIgnore
    public /* final */ CustomizeWidget z;
    @DexAdd
    public RadioGroup complication_selection;

    @DexAdd
    public /* final */ CustomizeWidget wc_bottom;
    @DexAdd
    public /* final */ CustomizeWidget wc_end;
    @DexAdd
    public /* final */ CustomizeWidget wc_start;
    @DexAdd
    public /* final */ CustomizeWidget wc_top;

    @DexAdd
    public WatchFacePreviewFragmentBinding(Object obj, View view, int i, View view2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ImageView imageView, TextView textView, TextView textView2, TextView textView3, CustomizeWidget customizeWidget, CustomizeWidget customizeWidget2, CustomizeWidget customizeWidget3, CustomizeWidget customizeWidget4,
            RadioGroup complication_selection) {
        super(obj, view, i);
        this.q = view2;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = imageView;
        this.u = textView;
        this.v = textView2;
        this.w = textView3;
        this.x = customizeWidget;   //bottom
        this.y = customizeWidget2;  //end
        this.z = customizeWidget3;  //start
        this.A = customizeWidget4;  //top
        // duplicate of the above 4 for convenient naming
        this.wc_bottom = customizeWidget;   //bottom
        this.wc_end = customizeWidget2;  //end
        this.wc_start = customizeWidget3;  //start
        this.wc_top = customizeWidget4;  //top
        // new widgets
        this.complication_selection = complication_selection;
    }
}
