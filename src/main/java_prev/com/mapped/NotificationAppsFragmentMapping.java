package com.mapped;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Pb;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE, staticConstructorAction = DexAction.APPEND)
public class NotificationAppsFragmentMapping extends NotificationAppsFragmentBinding {
    @DexIgnore
    public static /* final */ ViewDataBinding.i A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B;
    @DexIgnore
    public long z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        B = sparseIntArray;
        sparseIntArray.put(2131362656, 1);
        B.put(2131362517, 2);
        B.put(2131362281, 3);
        B.put(2131362653, 4);
        B.put(2131362028, 5);
        B.put(2131362336, 6);
        B.put(2131363102, 7);
        B.put(2131362995, 8);
    }
    */

    static {
        B.put(R.id.ftv_restore, 9);
    }

    @DexReplace
    public NotificationAppsFragmentMapping(Pb pb, View view) {
        this(pb, view, ViewDataBinding.a(pb, view, 10, A, B));
    }

    @DexReplace
    public NotificationAppsFragmentMapping(Pb pb, View view, Object[] objArr) {
        super(pb, view, 0, (ConstraintLayout) objArr[5], (FlexibleEditText) objArr[3], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[2], (RTLImageView) objArr[4], (RTLImageView) objArr[1], (ConstraintLayout) objArr[0], (RecyclerView) objArr[8], (FlexibleSwitchCompat) objArr[7], (FlexibleTextView) objArr[9]);
        this.z = -1;
        ((NotificationAppsFragmentBinding) this).w.setTag(null);
        a(view);
        f();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            return this.z != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }
}
