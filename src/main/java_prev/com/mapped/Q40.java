package com.mapped;

import android.os.Parcelable;
import com.fossil.Ks0;
import com.fossil.M01;
import com.fossil.M60;
import com.fossil.To0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Q40 extends Parcelable {

    @DexIgnore
    public enum Ai {
        BOND_NONE,
        BONDING,
        BONDED;
        
        @DexIgnore
        public static /* final */ Aii b; // = new Aii(null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
            }

            @DexIgnore
            public final Ai a(int i) {
                return i != 11 ? i != 12 ? Ai.BOND_NONE : Ai.BONDED : Ai.BONDING;
            }

            @DexIgnore
            public final Ai a(M01 m01) {
                throw null;
//                int i = To0.a[m01.ordinal()];
//                if (i == 1) {
//                    return Ai.BOND_NONE;
//                }
//                if (i == 2) {
//                    return Ai.BONDING;
//                }
//                if (i == 3) {
//                    return Ai.BONDED;
//                }
//                throw new Kc6();
            }
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void onDeviceStateChanged(Q40 q40, Ci ci, Ci ci2);

        @DexIgnore
        void onEventReceived(Q40 q40, D90 d90);
    }

    @DexIgnore
    public enum Ci {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        UPGRADING_FIRMWARE,
        DISCONNECTING
    }

    @DexIgnore
    public enum Di {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        DISCONNECTING;
        
        @DexIgnore
        public static /* final */ Ks0 f; // = new Ks0(null);
    }

    @DexIgnore
    <T> T a(Class<T> cls);

    @DexIgnore
    void a(Bi bi);

    @DexIgnore
    Ai getBondState();

    @DexIgnore
    Ci getState();

    @DexIgnore
    boolean isActive();

    @DexIgnore
    Zb0<Cd6> k();

    @DexIgnore
    M60 n();
}
