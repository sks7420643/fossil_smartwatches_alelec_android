package com.mapped;

import android.graphics.RectF;
import com.fossil.Nr5;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ow4 {
    @DexIgnore
    public /* final */ RectF a; // = new RectF();
    @DexIgnore
    public /* final */ RectF b; // = new RectF();
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float k; // = 1.0f;
    @DexIgnore
    public float l; // = 1.0f;

    @DexIgnore
    public static boolean a(float f2, float f3, float f4, float f5, float f6) {
        return Math.abs(f2 - f4) <= f6 && Math.abs(f3 - f5) <= f6;
    }

    @DexIgnore
    public static boolean a(float f2, float f3, float f4, float f5, float f6, float f7) {
        return f2 > f4 && f2 < f6 && f3 > f5 && f3 < f7;
    }

    @DexIgnore
    public static boolean b(float f2, float f3, float f4, float f5, float f6, float f7) {
        return f2 > f4 && f2 < f5 && Math.abs(f3 - f6) <= f7;
    }

    @DexIgnore
    public static boolean c(float f2, float f3, float f4, float f5, float f6, float f7) {
        return Math.abs(f2 - f4) <= f7 && f3 > f5 && f3 < f6;
    }

    @DexIgnore
    public final Nr5.Bi a(float f2, float f3) {  // Set initial touch drag point, select crop drag handle
        float width = this.a.width() / 6.0f;
        RectF rectF = this.a;
        float f4 = rectF.left;
        float height = rectF.height() / 6.0f;
        float f5 = this.a.top;
        float f6 = f5 + height;
        float f7 = (height * 5.0f) + f5;
        return f2 < f4 + width ? f3 < f6 ? Nr5.Bi.TOP_LEFT : f3 < f7 ? Nr5.Bi.LEFT : Nr5.Bi.BOTTOM_LEFT : f2 < (width * 5.0f) + f4 ? f3 < f6 ? Nr5.Bi.TOP : f3 < f7 ? Nr5.Bi.CENTER : Nr5.Bi.BOTTOM : f3 < f6 ? Nr5.Bi.TOP_RIGHT : f3 < f7 ? Nr5.Bi.RIGHT : Nr5.Bi.BOTTOM_RIGHT;
    }

    @DexIgnore
    public final Nr5.Bi a(float f2, float f3, float f4) {
        RectF rectF = this.a;
        if (a(f2, f3, rectF.left, rectF.top, f4)) {
            return Nr5.Bi.TOP_LEFT;
        }
        RectF rectF2 = this.a;
        if (a(f2, f3, rectF2.right, rectF2.top, f4)) {
            return Nr5.Bi.TOP_RIGHT;
        }
        RectF rectF3 = this.a;
        if (a(f2, f3, rectF3.left, rectF3.bottom, f4)) {
            return Nr5.Bi.BOTTOM_LEFT;
        }
        RectF rectF4 = this.a;
        if (a(f2, f3, rectF4.right, rectF4.bottom, f4)) {
            return Nr5.Bi.BOTTOM_RIGHT;
        }
        RectF rectF5 = this.a;
        if (a(f2, f3, rectF5.left, rectF5.top, rectF5.right, rectF5.bottom) && a()) {
            return Nr5.Bi.CENTER;
        }
        RectF rectF6 = this.a;
        if (b(f2, f3, rectF6.left, rectF6.right, rectF6.top, f4)) {
            return Nr5.Bi.TOP;
        }
        RectF rectF7 = this.a;
        if (b(f2, f3, rectF7.left, rectF7.right, rectF7.bottom, f4)) {
            return Nr5.Bi.BOTTOM;
        }
        RectF rectF8 = this.a;
        if (c(f2, f3, rectF8.left, rectF8.top, rectF8.bottom, f4)) {
            return Nr5.Bi.LEFT;
        }
        RectF rectF9 = this.a;
        if (c(f2, f3, rectF9.right, rectF9.top, rectF9.bottom, f4)) {
            return Nr5.Bi.RIGHT;
        }
        RectF rectF10 = this.a;
        if (!a(f2, f3, rectF10.left, rectF10.top, rectF10.right, rectF10.bottom) || a()) {
            return null;
        }
        return Nr5.Bi.CENTER;
    }

    @DexIgnore
    public Nr5 a(float f2, float f3, float f4, CropImageView.c cVar) {
        throw null;
//        Nr5.Bi a2 = cVar == CropImageView.c.OVAL ? a(f2, f3) : a(f2, f3, f4);
//        if (a2 != null) {
//            return new Nr5(a2, this, f2, f3);
//        }
//        return null;
    }

    @DexIgnore
    public void a(float f2, float f3, float f4, float f5) {
        this.e = f2;
        this.f = f3;
        this.k = f4;
        this.l = f5;
    }

    @DexIgnore
    public void a(int i2, int i3) {
        this.i = (float) i2;
        this.j = (float) i3;
    }

    @DexIgnore
    public void a(RectF rectF) {  // set new crop handle rect
        this.a.set(rectF);
    }

    @DexIgnore
    public void a(Nw4 nw4) {
        this.c = (float) nw4.C;
        this.d = (float) nw4.D;
        this.g = (float) nw4.E;
        this.h = (float) nw4.F;
        this.i = (float) nw4.G;
        this.j = (float) nw4.H;
    }

    @DexIgnore
    public final boolean a() {
        return !i();
    }

    @DexIgnore
    public float b() {
        return Math.min(this.f, this.j / this.l);
    }

    @DexIgnore
    public void b(int i2, int i3) {
        this.g = (float) i2;
        this.h = (float) i3;
    }

    @DexIgnore
    public float c() {
        return Math.min(this.e, this.i / this.k);
    }

    @DexIgnore
    public float d() {
        return Math.max(this.d, this.h / this.l);
    }

    @DexIgnore
    public float e() {
        return Math.max(this.c, this.g / this.k);
    }

    @DexIgnore
    public RectF f() {
        this.b.set(this.a);
        return this.b;
    }

    @DexIgnore
    public float g() {
        return this.l;
    }

    @DexIgnore
    public float h() {
        return this.k;
    }

    @DexIgnore
    public boolean i() {
        return this.a.width() >= 100.0f && this.a.height() >= 100.0f;
    }
}
