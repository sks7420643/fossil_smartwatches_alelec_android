package com.mapped;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import com.fossil.Hc7;
import com.fossil.Hg5;
import com.fossil.Je5;
import com.fossil.Mg5;
import com.fossil.Mo7;
import com.fossil.Rs7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.BitmapUtils;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.portfolio.platform.PortfolioApp;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class FileHelper {
    @DexIgnore
    public static /* final */ FileHelper a = new FileHelper();

    @DexReplace
    public final Bitmap a(String path, int i, int i2, FileType fileType) { // loadBitmap
        if (TextUtils.isEmpty(path)) {
            return null;
        }
        try {
            InputStream stream;
            AssetFileDescriptor af = assetPath(path);
            if (af != null) {
                stream = af.createInputStream();
            } else {
                stream = new FileInputStream(new File(FileUtils.getDirectory(PortfolioApp.get.instance().getApplicationContext(), fileType) + File.separator + Mg5.a(path)));
            }
            return Je5.a(stream, i, i2);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final Drawable a(String str, int i, int i2, int i3, FileType fileType) {
        String str2;
        Bitmap bitmap;
        Wg6.b(fileType, "type");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (i3 == Ai4.BACKGROUND.getValue()) {
            str2 = str;
        } else if (i3 == Ai4.PHOTO.getValue()) {
            str2 = str + i + i2;
        } else {
            str2 = null;
        }
        BitmapDrawable b = Hg5.b().b(str2);
        if (b != null) {
            return b;
        }
        try {
            if (i3 == Ai4.BACKGROUND.getValue()) {
                bitmap = a(str, i, i2, fileType);
            } else if (i3 == Ai4.PHOTO.getValue()) {
                BitmapUtils bitmapUtils = BitmapUtils.INSTANCE;
                if (str != null) {
                    bitmap = bitmapUtils.decodeBitmapFromDirectory(str, i, i2);
                } else {
                    Wg6.a();
                    throw null;
                }
            } else {
                bitmap = null;
            }
            if (bitmap == null) {
                return null;
            }
            BitmapDrawable bitmapDrawable = new BitmapDrawable(PortfolioApp.get.instance().getResources(), Bitmap.createScaledBitmap(bitmap, i, i2, false));
            Hg5.b().a(str2, bitmapDrawable);
            return bitmapDrawable;
        } catch (Exception e) {
            return null;
        }
    }

    @DexAdd
    public AssetFileDescriptor assetPath(String path) throws IOException {
        Uri uri = Uri.parse(path);
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return null;
        }
        AssetManager am = PortfolioApp.get.instance().getAssets();
        String ap = uri.getPath().substring("android_asset".length()+2);
        FLogger.INSTANCE.getLocal().d("FileHelper", "open asset " + ap);
        return am.openFd(ap);
    }

    @DexAdd
    public String urlToName(String url) {
        return TextUtils.isEmpty(url) ? "" : url.substring(url.lastIndexOf(File.separator) + 1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0071, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0072, code lost:
        com.fossil.Hc7.a(r3, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0075, code lost:
        throw r2;
     */
    @DexReplace
    public final String a(String str, FileType fileType) { // readFile
        String str2;
        Wg6.b(str, "nameFile");
        Wg6.b(fileType, "type");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FileHelper", "open nameFile " + str);
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            InputStream inputStream = null;
            AssetFileDescriptor ap = assetPath(str);
            if (ap != null) {
                inputStream = ap.createInputStream();
            } else {
                String directory = FileUtils.getDirectory(PortfolioApp.get.instance().getApplicationContext(), fileType);
                inputStream = new FileInputStream(new File(directory + File.separator + urlToName(str)));
            }
            str2 = Base64.encodeToString(Rs7.b(inputStream), 0);
            Hc7.a(inputStream, null);
        } catch (Exception e) {
            e.printStackTrace();
            str2 = "";
        }
        Wg6.a((Object) str2, "try {\n                va\u2026         \"\"\n            }");
        return str2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0023 A[Catch:{ IOException -> 0x0030 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x002c A[Catch:{ IOException -> 0x0030 }] */
    public final boolean a(Bitmap bitmap, String str) throws Throwable {
        FileOutputStream fileOutputStream;
        FileOutputStream fileOutputStream2;
        Wg6.b(bitmap, "body");
        Wg6.b(str, "path");
        try {
            fileOutputStream2 = new FileOutputStream(str);
            try {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream2);
                fileOutputStream2.flush();
                try {
                    fileOutputStream2.close();
                    return true;
                } catch (IOException e) {
                    return false;
                }
            } catch (IOException e2) {
                fileOutputStream = fileOutputStream2;
                if (fileOutputStream != null) {
                }
                return false;
            } catch (Throwable th) {
                th = th;
                if (fileOutputStream2 != null) {
                }
                throw th;
            }
        } catch (IOException e3) {
            fileOutputStream = null;
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return false;
        } catch (Throwable th2) {
            Throwable th = th2;
            fileOutputStream2 = null;
            if (fileOutputStream2 != null) {
                try {
                    fileOutputStream2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x004c  */
    public final boolean a(Mo7 mo7, String str) throws Throwable {
        InputStream inputStream;
        FileOutputStream fileOutputStream;
        InputStream inputStream2;
        FileOutputStream fileOutputStream2;
        Wg6.b(mo7, "body");
        Wg6.b(str, "path");
        try {
            byte[] bArr = new byte[4096];
            inputStream2 = mo7.byteStream();
            try {
                fileOutputStream = new FileOutputStream(str);
                if (inputStream2 != null) {
                    while (true) {
                        try {
                            int read = inputStream2.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(bArr, 0, read);
                        } catch (IOException e) {
                            inputStream = inputStream2;
                            if (inputStream != null) {
                                inputStream.close();
                            }
                            if (fileOutputStream != null) {
                                fileOutputStream.close();
                            }
                            return false;
                        } catch (Throwable th) {
                            th = th;
                            fileOutputStream2 = fileOutputStream;
                            if (inputStream2 != null) {
                                inputStream2.close();
                            }
                            if (fileOutputStream2 != null) {
                                fileOutputStream2.close();
                            }
                            throw th;
                        }
                    }
                }
                fileOutputStream.flush();
                if (inputStream2 != null) {
                    try {
                        inputStream2.close();
                    } catch (IOException e2) {
                        return false;
                    }
                }
                fileOutputStream.close();
                return true;
            } catch (IOException e3) {
                fileOutputStream = null;
                inputStream = inputStream2;
                if (inputStream != null) {
                }
                if (fileOutputStream != null) {
                }
                return false;
            } catch (Throwable th2) {
                Throwable th = th2;
                fileOutputStream = null;
                fileOutputStream2 = fileOutputStream;
                if (inputStream2 != null) {
                }
                if (fileOutputStream2 != null) {
                }
                throw th;
            }
        } catch (IOException e4) {
            inputStream = null;
            fileOutputStream = null;
            if (inputStream != null) {
            }
            if (fileOutputStream != null) {
            }
            return false;
        } catch (Throwable th3) {
            inputStream2 = null;
            fileOutputStream2 = null;
            Throwable th = th3;
            if (inputStream2 != null) {
            }
            if (fileOutputStream2 != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            if (str != null) {
                return new File(str).exists();
            }
            Wg6.a();
            throw null;
        } catch (Exception e) {
            return false;
        }
    }

    @DexIgnore
    public final Drawable b(String str, int i, int i2, FileType fileType) {
        BitmapDrawable bitmapDrawable;
        Wg6.b(fileType, "type");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        BitmapDrawable b = Hg5.b().b(str);
        if (b != null) {
            return b;
        }
        try {
            bitmapDrawable = new BitmapDrawable(PortfolioApp.get.instance().getResources(), a(str, i, i2, fileType));
            Hg5.b().a(str, bitmapDrawable);
        } catch (Exception e) {
            e.printStackTrace();
            bitmapDrawable = null;
        }
        return bitmapDrawable;
    }
}
