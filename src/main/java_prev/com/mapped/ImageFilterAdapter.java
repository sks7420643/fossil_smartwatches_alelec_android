package com.mapped;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.F36;
import com.fossil.imagefilters.FilterType;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;
import lanchon.dexpatcher.annotation.DexWrap;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ImageFilterAdapter extends RecyclerView.g<ImageFilterAdapter.Ci> {
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ ArrayList<Bi> b;
    @DexIgnore
    public Di c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ FilterType b;

        @DexIgnore
        public Bi(Bitmap bitmap, FilterType filterType) {
            Wg6.b(bitmap, "image");
            Wg6.b(filterType, "type");
            this.a = bitmap;
            this.b = filterType;
        }

        @DexIgnore
        public final Bitmap a() {
            return this.a;
        }

        @DexIgnore
        public final FilterType b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Bi) {
                    Bi bi = (Bi) obj;
                    if (!Wg6.a(this.a, bi.a) || !Wg6.a(this.b, bi.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            Bitmap bitmap = this.a;
            int hashCode = bitmap != null ? bitmap.hashCode() : 0;
            FilterType filterType = this.b;
            if (filterType != null) {
                i = filterType.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "ImageFilter(image=" + this.a + ", type=" + this.b + ")";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends RecyclerView.ViewHolder {
        @DexIgnore
        public ImageView a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public /* final */ /* synthetic */ ImageFilterAdapter d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ci a;

            @DexIgnore
            public Aii(Ci ci) {
                this.a = ci;
            }

            @DexIgnore
            public final void onClick(View view) {
                Di c;
                if (!(this.a.d.getItemCount() <= this.a.getAdapterPosition() || this.a.getAdapterPosition() == -1 || (c = this.a.d.c()) == null)) {
                    Object obj = this.a.d.b.get(this.a.getAdapterPosition());
                    Wg6.a(obj, "mFilterList[adapterPosition]");
                    c.a((Bi) obj);
                }
                Ci ci = this.a;
                ci.d.a(ci.getAdapterPosition());
            }
        }

        @DexAdd
        public Ci(ImageFilterAdapter imageFilterAdapter, View view) {
            this(imageFilterAdapter, view, null);
            Aii listener = new Aii(this);
            this.b.setOnClickListener(listener);
            this.b.setTextColor(view.getResources().getColor(R.color.secondaryText));
        }

        @DexEdit
        @SuppressLint("ResourceType")
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(ImageFilterAdapter imageFilterAdapter, View view, @DexIgnore Void _) {
            super(view);
            Wg6.b(view, "view");
            this.d = imageFilterAdapter;
            View findViewById = view.findViewById(2131362641);
            Wg6.a((Object) findViewById, "view.findViewById(R.id.iv_background_preview)");
            this.a = (ImageView) findViewById;
            View findViewById2 = view.findViewById(2131363304);
            Wg6.a((Object) findViewById2, "view.findViewById(R.id.tv_name)");
            this.b = (TextView) findViewById2;
            View findViewById3 = view.findViewById(2131363381);
            Wg6.a((Object) findViewById3, "view.findViewById(R.id.v_background_selected)");
            this.c = findViewById3;
            this.a.setOnClickListener(new Aii(this));
        }

        @DexIgnore
        public final void a(Bi bi, int i) {
            String a2;
            Wg6.b(bi, "imageFilter");
            this.a.setImageBitmap(bi.a());
            throw null;
//            switch (F36.a[bi.b().ordinal()]) {
//                case 1:
//                    View view = ((RecyclerView.ViewHolder) this).itemView;
//                    Wg6.a((Object) view, "itemView");
//                    a2 = Jm4.a(view.getContext(), 2131886521);
//                    break;
//                case 2:
//                    View view2 = ((RecyclerView.ViewHolder) this).itemView;
//                    Wg6.a((Object) view2, "itemView");
//                    a2 = Jm4.a(view2.getContext(), 2131886517);
//                    break;
//                case 3:
//                    View view3 = ((RecyclerView.ViewHolder) this).itemView;
//                    Wg6.a((Object) view3, "itemView");
//                    a2 = Jm4.a(view3.getContext(), 2131886519);
//                    break;
//                case 4:
//                    View view4 = ((RecyclerView.ViewHolder) this).itemView;
//                    Wg6.a((Object) view4, "itemView");
//                    a2 = Jm4.a(view4.getContext(), 2131886518);
//                    break;
//                case 5:
//                    View view5 = ((RecyclerView.ViewHolder) this).itemView;
//                    Wg6.a((Object) view5, "itemView");
//                    a2 = Jm4.a(view5.getContext(), 2131886522);
//                    break;
//                case 6:
//                    View view6 = ((RecyclerView.ViewHolder) this).itemView;
//                    Wg6.a((Object) view6, "itemView");
//                    a2 = Jm4.a(view6.getContext(), 2131886520);
//                    break;
//                default:
//                    a2 = "";
//                    break;
//            }
//            this.b.setText(a2);
//            if (i == this.d.a) {
//                this.c.setVisibility(0);
//            } else {
//                this.c.setVisibility(8);
//            }
        }
    }

    @DexIgnore
    public interface Di {
        @DexIgnore
        void a(Bi bi);
    }

    /*
    static {
        new Ai(null);
    }
    */

    @DexIgnore
    public ImageFilterAdapter(ArrayList<Bi> arrayList, Di di) {
        Wg6.b(arrayList, "mFilterList");
        this.b = arrayList;
        this.c = di;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ImageFilterAdapter(ArrayList arrayList, Di di, int i, Qg6 qg6) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : di);
    }

    @DexIgnore
    public final void a(int i) {
        try {
            if (this.a != i) {
                int i2 = this.a;
                this.a = i;
                notifyItemChanged(i);
                notifyItemChanged(i2);
            }
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().e("ImageFilterAdapter", e.getMessage());
            e.printStackTrace();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(Ci ci, int i) {
        Wg6.b(ci, "holder");
        if (getItemCount() > i && i != -1) {
            Bi bi = this.b.get(i);
            Wg6.a((Object) bi, "mFilterList[position]");
            ci.a(bi, i);
        }
    }

    @DexIgnore
    public final void a(Di di) {
        this.c = di;
    }

    @DexIgnore
    public final void a(List<Bi> list) {
        Wg6.b(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ImageFilterAdapter", "setData size = " + list.size());
        this.b.clear();
        this.b.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final Di c() {
        return this.c;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public Ci onCreateViewHolder(ViewGroup viewGroup, int i) {
        Wg6.b(viewGroup, "parent");
        @SuppressLint("ResourceType") View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558681, viewGroup, false);
        Wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026na_filter, parent, false)");
        return new Ci(this, inflate);
    }
}
