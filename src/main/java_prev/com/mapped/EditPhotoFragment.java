package com.mapped;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bf5;
import com.fossil.Bx6;
import com.fossil.Fz5;
import com.fossil.He;
import com.fossil.Ho5;
import com.fossil.Je;
import com.fossil.Qb;
import com.fossil.Qw6;
import com.fossil.Rj4;
import com.fossil.W05;
import com.fossil.Zd;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.FilterType;
import com.mapped.AlertDialogFragment;
import com.mapped.ImageFilterAdapter;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EditPhotoFragment extends Ho5 implements ImageFilterAdapter.Di, CropImageView.i, CropImageView.g, FragmentManager.g, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ Ai t; // = new Ai(null);
    @DexIgnore
    public EditPhotoViewModel g;
    @DexIgnore
    public Qw6<W05> h;
    @DexIgnore
    public Uri i;
    @DexIgnore
    public ImageFilterAdapter j;
    @DexIgnore
    public /* final */ ArrayList<FilterType> p;
    @DexIgnore
    public FilterType q;
    @DexIgnore
    public Rj4 r;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final EditPhotoFragment a(Uri uri, ArrayList<Fz5> arrayList) {
            Wg6.b(uri, "uri");
            Wg6.b(arrayList, "complications");
            EditPhotoFragment editPhotoFragment = new EditPhotoFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("MAGE_URI_ARG", uri);
            bundle.putParcelableArrayList("COMPLICATIONS_ARG", arrayList);
            editPhotoFragment.setArguments(bundle);
            return editPhotoFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Zd<EditPhotoViewModel.Ei> {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment a;

        @DexIgnore
        public Bi(EditPhotoFragment editPhotoFragment) {
            this.a = editPhotoFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(EditPhotoViewModel.Ei ei) {
            TextView textView;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("EditPhotoFragment", "Crop image: result = " + ei);
            this.a.a();
            W05 b = this.a.i1();
            if (!(b == null || (textView = b.w) == null)) {
                textView.setClickable(true);
            }
            if (ei.a() && this.a.getActivity() != null) {
                if (EditPhotoFragment.f(this.a).c() != null) {
                    EditPhotoFragment editPhotoFragment = this.a;
                    PreviewFragment.Ai ai = PreviewFragment.j;
                    ArrayList<Fz5> c = EditPhotoFragment.f(editPhotoFragment).c();
                    if (c != null) {
                        editPhotoFragment.a(ai.a(c, this.a.q), "PreviewFragment", 2131362149);
                    } else {
                        Wg6.a();
                        throw null;
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "Can not initialize PreviewFragment without complications");
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Zd<EditPhotoViewModel.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment a;

        @DexIgnore
        public Ci(EditPhotoFragment editPhotoFragment) {
            this.a = editPhotoFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(EditPhotoViewModel.Bi bi) {
            List<FilterResult> a2 = bi.a();
            FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(a2));
            this.a.a();
            if (!a2.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                for (FilterResult filterResult : bi.a()) {
                    Bitmap preview = filterResult.getPreview();
                    Wg6.a((Object) preview, "filter.preview");
                    Bitmap a3 = Lw4.a(preview);
                    Wg6.a((Object) a3, "circleBitmap");
                    FilterType type = filterResult.getType();
                    Wg6.a((Object) type, "filter.type");
                    arrayList.add(new ImageFilterAdapter.Bi(a3, type));
                }
                this.a.q = ((ImageFilterAdapter.Bi) arrayList.get(0)).b();
                ImageFilterAdapter c = this.a.j;
                if (c != null) {
                    c.a(arrayList);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Zd<EditPhotoViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment a;

        @DexIgnore
        public Di(EditPhotoFragment editPhotoFragment) {
            this.a = editPhotoFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(EditPhotoViewModel.Ai ai) {
            CropImageView cropImageView;
            FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(ai.a()));
            FilterResult a2 = ai.a();
            W05 b = this.a.i1();
            if (!(b == null || (cropImageView = b.q) == null)) {
                cropImageView.a(a2.getPreview());
            }
            EditPhotoFragment editPhotoFragment = this.a;
            FilterType type = a2.getType();
            Wg6.a((Object) type, "it.type");
            editPhotoFragment.q = type;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment a;

        @DexIgnore
        public Ei(EditPhotoFragment editPhotoFragment, W05 w05) {
            this.a = editPhotoFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment a;

        @DexIgnore
        public Fi(EditPhotoFragment editPhotoFragment, W05 w05) {
            this.a = editPhotoFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.i != null) {
                this.a.g1();
            } else {
                FLogger.INSTANCE.getLocal().e("EditPhotoFragment", "Can not crop image cause of null imageURI");
            }
        }
    }

    @DexIgnore
    public EditPhotoFragment() {
        ArrayList<FilterType> arrayList = new ArrayList<>();
        arrayList.add(FilterType.ATKINSON_DITHERING);
        arrayList.add(FilterType.BURKES_DITHERING);
        arrayList.add(FilterType.DIRECT_MAPPING);
        arrayList.add(FilterType.JAJUNI_DITHERING);
        arrayList.add(FilterType.ORDERED_DITHERING);
        arrayList.add(FilterType.SIERRA_DITHERING);
        this.p = arrayList;
        FilterType filterType = arrayList.get(0);
        Wg6.a((Object) filterType, "mFilterList[0]");
        this.q = filterType;
    }

    @DexIgnore
    public static final /* synthetic */ EditPhotoViewModel f(EditPhotoFragment editPhotoFragment) {
        EditPhotoViewModel editPhotoViewModel = editPhotoFragment.g;
        if (editPhotoViewModel != null) {
            return editPhotoViewModel;
        }
        Wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Ho5
    public void Z0() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.customview.imagecropper.CropImageView.g
    public void a(Rect rect) {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "onCropOverlayReleased()");
        j1();
    }

    @DexIgnore
    @Override // com.mapped.ImageFilterAdapter.Di
    public void a(ImageFilterAdapter.Bi bi) {
        W05 i1;
        CropImageView cropImageView;
        Bitmap initializeBitmap;
        Wg6.b(bi, "imageFilter");
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(bi.b()));
        if (this.q != bi.b() && (i1 = i1()) != null && (cropImageView = i1.q) != null && (initializeBitmap = cropImageView.getInitializeBitmap()) != null) {
            EditPhotoViewModel editPhotoViewModel = this.g;
            if (editPhotoViewModel != null) {
                editPhotoViewModel.a(initializeBitmap, bi.b());
            } else {
                Wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.customview.imagecropper.CropImageView.i
    public void a(CropImageView cropImageView, Uri uri, Exception exc) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("EditPhotoFragment", "onSetImageUriComplete, uri = " + uri + ", error = " + exc);
        if (isActive()) {
            if (exc == null) {
                j1();
                return;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("EditPhotoFragment", "error = " + exc);
            Bx6 bx6 = Bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.a((Object) childFragmentManager, "childFragmentManager");
            bx6.G(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ho5, com.mapped.AlertDialogFragment.Gi
    public void a(String str, int i2, Intent intent) {
        Wg6.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("EditPhotoFragment", "onDialogFragmentResult tag = " + str);
        if ((str.length() == 0) || getActivity() == null) {
            return;
        }
        if (str.hashCode() == 407101428 && str.equals("PROCESS_IMAGE_ERROR")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
                return;
            }
            return;
        }
        super.a(str, i2, intent);
    }

    @DexIgnore
    public final void b(Bundle bundle) {
        ArrayList<Fz5> arrayList = null;
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "initialize()");
        if (bundle.containsKey("MAGE_URI_ARG")) {
            Bundle arguments = getArguments();
            this.i = arguments != null ? (Uri) arguments.getParcelable("MAGE_URI_ARG") : null;
        }
        if (bundle.containsKey("COMPLICATIONS_ARG")) {
            EditPhotoViewModel editPhotoViewModel = this.g;
            if (editPhotoViewModel != null) {
                Bundle arguments2 = getArguments();
                if (arguments2 != null) {
                    arrayList = arguments2.getParcelableArrayList("COMPLICATIONS_ARG");
                }
                editPhotoViewModel.a(arrayList);
                return;
            }
            Wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void f1() {
        EditPhotoViewModel editPhotoViewModel = this.g;
        if (editPhotoViewModel != null) {
            editPhotoViewModel.d().a(getViewLifecycleOwner(), new Bi(this));
            EditPhotoViewModel editPhotoViewModel2 = this.g;
            if (editPhotoViewModel2 != null) {
                editPhotoViewModel2.b().a(getViewLifecycleOwner(), new Ci(this));
                EditPhotoViewModel editPhotoViewModel3 = this.g;
                if (editPhotoViewModel3 != null) {
                    editPhotoViewModel3.a().a(getViewLifecycleOwner(), new Di(this));
                } else {
                    Wg6.d("mViewModel");
                    throw null;
                }
            } else {
                Wg6.d("mViewModel");
                throw null;
            }
        } else {
            Wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void g1() {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "cropImage()");
        EditPhotoViewModel.Di h1 = h1();
        if (h1 != null) {
            b();
            EditPhotoViewModel editPhotoViewModel = this.g;
            if (editPhotoViewModel != null) {
                editPhotoViewModel.a(h1, this.q);
            } else {
                Wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final EditPhotoViewModel.Di h1() {
        CropImageView cropImageView;
        ConstraintLayout constraintLayout;
        W05 i1 = i1();
        if (!(i1 == null || (constraintLayout = i1.r) == null)) {
            constraintLayout.invalidate();
        }
        W05 i12 = i1();
        if (!(i12 == null || (cropImageView = i12.q) == null)) {
            Wg6.a((Object) cropImageView, "it");
            if (cropImageView.getInitializeBitmap() != null) {
                cropImageView.clearAnimation();
                float[] fArr = new float[8];
                float[] cropPoints = cropImageView.getCropPoints();
                Wg6.a((Object) cropPoints, "it.cropPoints");
                int i2 = 0;
                for (float f : cropPoints) {
                    fArr[i2] = f / ((float) cropImageView.getLoadedSampleSize());
                    i2++;
                }
                Bitmap initializeBitmap = cropImageView.getInitializeBitmap();
                Wg6.a((Object) initializeBitmap, "it.initializeBitmap");
                int rotatedDegrees = cropImageView.getRotatedDegrees();
                boolean c = cropImageView.c();
                Object obj = cropImageView.getAspectRatio().first;
                Wg6.a(obj, "it.aspectRatio.first");
                int intValue = ((Number) obj).intValue();
                Object obj2 = cropImageView.getAspectRatio().second;
                Wg6.a(obj2, "it.aspectRatio.second");
                return new EditPhotoViewModel.Di(initializeBitmap, fArr, rotatedDegrees, c, intValue, ((Number) obj2).intValue(), cropImageView.d(), cropImageView.e());
            }
        }
        return null;
    }

    @DexIgnore
    public final W05 i1() {
        Qw6<W05> qw6 = this.h;
        if (qw6 != null) {
            return qw6.a();
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void j1() {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "updateFilterList()");
        EditPhotoViewModel.Di h1 = h1();
        if (h1 != null) {
            EditPhotoViewModel editPhotoViewModel = this.g;
            if (editPhotoViewModel != null) {
                editPhotoViewModel.a(h1, this.p);
            } else {
                Wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (bundle != null) {
            b(bundle);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentManager.g
    public void onBackStackChanged() {
        CropImageView cropImageView;
        CropImageView cropImageView2;
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "onBackStackChanged()");
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.a((Object) childFragmentManager, "childFragmentManager");
        if (childFragmentManager.t() == 0) {
            Qw6<W05> qw6 = this.h;
            if (qw6 != null) {
                W05 a = qw6.a();
                if (a != null && (cropImageView2 = a.q) != null) {
                    cropImageView2.setOnSetImageUriCompleteListener(this);
                    cropImageView2.setOnSetCropOverlayReleasedListener(this);
                    return;
                }
                return;
            }
            Wg6.d("mBinding");
            throw null;
        }
        Qw6<W05> qw62 = this.h;
        if (qw62 != null) {
            W05 a2 = qw62.a();
            if (a2 != null && (cropImageView = a2.q) != null) {
                cropImageView.a();
                return;
            }
            return;
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().j().a(this);
        FragmentActivity requireActivity = requireActivity();
        Rj4 rj4 = this.r;
        if (rj4 != null) {
            He a = Je.a(requireActivity, rj4).a(EditPhotoViewModel.class);
            Wg6.a((Object) a, "ViewModelProviders.of(re\u2026otoViewModel::class.java)");
            this.g = (EditPhotoViewModel) a;
            Bundle arguments = getArguments();
            if (arguments != null) {
                Wg6.a((Object) arguments, "it");
                b(arguments);
            }
            getChildFragmentManager().a((FragmentManager.g) this);
            return;
        }
        Wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.b(layoutInflater, "inflater");
        W05 w05 = (W05) Qb.a(LayoutInflater.from(getContext()), 2131558550, null, true, a1());
        this.h = new Qw6<>(this, w05);
        this.j = new ImageFilterAdapter(null, this, 1, null);
        Qw6<W05> qw6 = this.h;
        if (qw6 != null) {
            W05 a = qw6.a();
            if (a != null) {
                TextView textView = a.v;
                Wg6.a((Object) textView, "fragmentBinding.tvCancel");
                Bf5.a(textView, new Ei(this, w05));
                TextView textView2 = a.w;
                Wg6.a((Object) textView2, "fragmentBinding.tvPreview");
                Bf5.a(textView2, new Fi(this, w05));
                if (this.i != null) {
                    a.q.setOnSetImageUriCompleteListener(this);
                    CropImageView cropImageView = a.q;
                    if (cropImageView != null) {
                        cropImageView.setOnSetCropOverlayReleasedListener(this);
                    }
                    b();
                    a.q.a(this.i, this.q);
                } else {
                    a();
                }
                RecyclerView recyclerView = w05.u;
                recyclerView.setHasFixedSize(true);
                recyclerView.setItemAnimator(null);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                recyclerView.setAdapter(this.j);
            }
            f1();
            Wg6.a((Object) w05, "binding");
            return w05.d();
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        getChildFragmentManager().b((FragmentManager.g) this);
        super.onDestroy();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Ho5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        CropImageView cropImageView;
        Qw6<W05> qw6 = this.h;
        if (qw6 != null) {
            W05 a = qw6.a();
            if (!(a == null || (cropImageView = a.q) == null)) {
                cropImageView.a();
            }
            ImageFilterAdapter imageFilterAdapter = this.j;
            if (imageFilterAdapter != null) {
                imageFilterAdapter.a((ImageFilterAdapter.Di) null);
            }
            super.onDestroyView();
            Z0();
            return;
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putParcelable("MAGE_URI_ARG", this.i);
        EditPhotoViewModel editPhotoViewModel = this.g;
        if (editPhotoViewModel != null) {
            bundle.putParcelableArrayList("COMPLICATIONS_ARG", editPhotoViewModel.c());
        } else {
            Wg6.d("mViewModel");
            throw null;
        }
    }
}
