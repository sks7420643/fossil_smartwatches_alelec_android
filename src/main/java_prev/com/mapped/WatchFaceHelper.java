package com.mapped;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;

import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.Mg5;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.background.BackgroundImgData;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.MetaData;
import com.portfolio.platform.data.model.diana.preset.RingStyle;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceHelper {
    @DexReplace
    public static final BackgroundConfig a(final WatchFace watchFace, final List<DianaPresetComplicationSetting> list) {
        Wg6.b((Object)watchFace, "$this$toBackgroundConfig");
        final int watchFaceType = watchFace.getWatchFaceType();
        final int value = Ai4.BACKGROUND.getValue();
        String a = "";
        String s = null;
        Label_0107: {
            if (watchFaceType == value) {
                final String url = watchFace.getBackground().getData().getUrl();
                if (url != null) {
                    final FileHelper a2 = FileHelper.a;
//                    final String a3 = Mg5.a(url); // urlToName, moved into FileHelper
//                    Wg6.a((Object)a3, "StringHelper.fileNameFromUrl(it)");
                    if (url != null && !TextUtils.isEmpty(url)) {
                        s = a2.a(url, FileType.WATCH_FACE);
                    }
                    if (s != null) {
                        break Label_0107;
                    }
                }
            }
            else {
                s = watchFace.getBackground().getData().getUrl();
                if (s != null) {
                    break Label_0107;
                }
            }
            s = "";
        }
        String s2;
        String s3;
        String s4;
        String s5;
        if (list != null) {
            final Iterator<DianaPresetComplicationSetting> iterator = list.iterator();
            String a4 = "";
            String a6;
            String a5 = a6 = a4;
            while (true) {
                s2 = a4;
                s3 = a;
                s4 = a5;
                s5 = a6;
                if (!iterator.hasNext()) {
                    break;
                }
                final DianaPresetComplicationSetting dianaPresetComplicationSetting = iterator.next();
                final String component1 = dianaPresetComplicationSetting.component1();
                final String component2 = dianaPresetComplicationSetting.component2();
                final int hashCode = component1.hashCode();
                RingStyleItem ringStyleItem = null;
                final RingStyleItem ringStyleItem2 = null;
                final RingStyleItem ringStyleItem3 = null;
                final RingStyleItem ringStyleItem4 = null;
                final RingStyleItem ringStyleItem5 = null;
                final RingStyleItem ringStyleItem6 = null;
                final RingStyleItem ringStyleItem7 = null;
                final RingStyleItem ringStyleItem8 = null;
                switch (hashCode) {
                    default: {
                        continue;
                    }
                    case 108511772: {
                        if (!component1.equals("right") || !(Wg6.a((Object)component2, (Object)"empty") ^ true)) {
                            continue;
                        }
                        final ArrayList ringStyleItems = watchFace.getRingStyleItems();
                        if (ringStyleItems != null) {
                            final Iterator<RingStyleItem> iterator2 = ringStyleItems.iterator();
                            RingStyleItem next;
                            do {
                                next = ringStyleItem8;
                                if (!iterator2.hasNext()) {
                                    break;
                                }
                                next = iterator2.next();
                            } while (!Wg6.a((Object)next.getPosition(), (Object)"right"));
                            ringStyleItem = next;
                        }
                        if (ringStyleItem != null) {
                            final FileHelper a7 = FileHelper.a;
                            final String a8 = ringStyleItem.getRingStyle().getData().getUrl();
                            if (a8 != null && !TextUtils.isEmpty(a8)) {
                                Wg6.a((Object) a8, "StringHelper.fileNameFro\u2026Style.ringStyle.data.url)");
                                a4 = a7.a(a8, FileType.WATCH_FACE);
                            }
                            continue;
                        }
                        continue;
                    }
                    case 3317767: {
                        if (!component1.equals("left") || !(Wg6.a((Object)component2, (Object)"empty") ^ true)) {
                            continue;
                        }
                        final ArrayList ringStyleItems2 = watchFace.getRingStyleItems();
                        RingStyleItem ringStyleItem9 = ringStyleItem3;
                        if (ringStyleItems2 != null) {
                            final Iterator<RingStyleItem> iterator3 = ringStyleItems2.iterator();
                            RingStyleItem next2;
                            do {
                                next2 = ringStyleItem2;
                                if (!iterator3.hasNext()) {
                                    break;
                                }
                                next2 = iterator3.next();
                            } while (!Wg6.a((Object)next2.getPosition(), (Object)"left"));
                            ringStyleItem9 = next2;
                        }
                        if (ringStyleItem9 != null) {
                            final FileHelper a9 = FileHelper.a;
                            final String a10 = ringStyleItem9.getRingStyle().getData().getUrl();
                            if (a10 != null && !TextUtils.isEmpty(a10)) {
                                Wg6.a((Object) a10, "StringHelper.fileNameFro\u2026Style.ringStyle.data.url)");
                                a6 = a9.a(a10, FileType.WATCH_FACE);
                            }
                            continue;
                        }
                        continue;
                    }
                    case 115029: {
                        if (!component1.equals("top") || !(Wg6.a((Object)component2, (Object)"empty") ^ true)) {
                            continue;
                        }
                        final ArrayList ringStyleItems3 = watchFace.getRingStyleItems();
                        RingStyleItem ringStyleItem10 = ringStyleItem5;
                        if (ringStyleItems3 != null) {
                            final Iterator<RingStyleItem> iterator4 = ringStyleItems3.iterator();
                            RingStyleItem next3;
                            do {
                                next3 = ringStyleItem4;
                                if (!iterator4.hasNext()) {
                                    break;
                                }
                                next3 = iterator4.next();
                            } while (!Wg6.a((Object)next3.getPosition(), (Object)"top"));
                            ringStyleItem10 = next3;
                        }
                        if (ringStyleItem10 != null) {
                            final FileHelper a11 = FileHelper.a;
                            final String a12 = ringStyleItem10.getRingStyle().getData().getUrl();
                            if (a12 != null && !TextUtils.isEmpty(a12)) {
                                Wg6.a((Object) a12, "StringHelper.fileNameFro\u2026Style.ringStyle.data.url)");
                                a = a11.a(a12, FileType.WATCH_FACE);
                            }
                            continue;
                        }
                        continue;
                    }
                    case -1383228885: {
                        if (!component1.equals("bottom") || !(Wg6.a((Object)component2, (Object)"empty") ^ true)) {
                            continue;
                        }
                        final ArrayList ringStyleItems4 = watchFace.getRingStyleItems();
                        RingStyleItem ringStyleItem11 = ringStyleItem7;
                        if (ringStyleItems4 != null) {
                            final Iterator<RingStyleItem> iterator5 = ringStyleItems4.iterator();
                            RingStyleItem next4;
                            do {
                                next4 = ringStyleItem6;
                                if (!iterator5.hasNext()) {
                                    break;
                                }
                                next4 = iterator5.next();
                            } while (!Wg6.a((Object)next4.getPosition(), (Object)"bottom"));
                            ringStyleItem11 = next4;
                        }
                        if (ringStyleItem11 != null) {
                            final FileHelper a13 = FileHelper.a;
                            final String a14 = ringStyleItem11.getRingStyle().getData().getUrl();
                            if (a14 != null && !TextUtils.isEmpty(a14)) {
                                Wg6.a((Object) a14, "StringHelper.fileNameFro\u2026Style.ringStyle.data.url)");
                                a5 = a13.a(a14, FileType.WATCH_FACE);
                            }
                            continue;
                        }
                        continue;
                    }
                }
            }
        }
        else {
            s2 = "";
            s5 = (s4 = s2);
            s3 = a;
        }
        return new BackgroundConfig(System.currentTimeMillis(), new BackgroundImgData("main_bg", s), new BackgroundImgData("top_bg", s3), new BackgroundImgData("right_bg", s2), new BackgroundImgData("bottom_bg", s4), new BackgroundImgData("left_bg", s5));
    }

    @DexIgnore
    public static final WatchFaceWrapper.MetaData a(MetaData metaData) {
        Integer num;
        Integer num2;
        Integer num3;
        Integer num4 = null;
        Wg6.b(metaData, "$this$toColorMetaData");
        try {
            num = Integer.valueOf(Color.parseColor(metaData.getSelectedForegroundColor()));
        } catch (Exception e) {
            e.printStackTrace();
            num = null;
        }
        try {
            num2 = Integer.valueOf(Color.parseColor(metaData.getSelectedBackgroundColor()));
        } catch (Exception e2) {
            e2.printStackTrace();
            num2 = null;
        }
        try {
            num3 = Integer.valueOf(Color.parseColor(metaData.getUnselectedForegroundColor()));
        } catch (Exception e3) {
            e3.printStackTrace();
            num3 = null;
        }
        try {
            num4 = Integer.valueOf(Color.parseColor(metaData.getUnselectedBackgroundColor()));
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        return new WatchFaceWrapper.MetaData(num, num2, num3, num4);
    }

    @DexIgnore
    public static final List<WatchFaceWrapper> a(List<WatchFace> list, List<DianaPresetComplicationSetting> list2) {
        Wg6.b(list, "$this$toListOfWatchFaceWrappers");
        ArrayList arrayList = new ArrayList();
        for (WatchFace watchFace : list) {
            arrayList.add(b(watchFace, list2));
        }
        return arrayList;
    }

    @DexIgnore
    @SuppressLint("ResourceType")
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0194  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0088  */
    public static final WatchFaceWrapper b(WatchFace watchFace, List<DianaPresetComplicationSetting> list) {
        int dimensionPixelSize;
        ArrayList<RingStyleItem> ringStyleItems;
        Drawable drawable = null;
        Drawable drawable2 = null;
        WatchFaceWrapper.MetaData metaData = null;
        Drawable drawable3 = null;
        WatchFaceWrapper.MetaData metaData2 = null;
        WatchFaceWrapper.MetaData metaData3 = null;
        WatchFaceWrapper.MetaData metaData4 = null;
        Drawable drawable4 = null;
        Wg6.b(watchFace, "$this$toWatchFaceWrapper");
        String id = watchFace.getId();
        Drawable c = W6.c(PortfolioApp.get.instance(), 2131231251);
        if (c != null) {
            c.getIntrinsicHeight();
            Drawable c2 = W6.c(PortfolioApp.get.instance(), 2131231251);
            Integer valueOf = c2 != null ? Integer.valueOf(c2.getMinimumWidth()) : null;
            if (valueOf != null) {
                dimensionPixelSize = valueOf.intValue();
                int dimensionPixelSize2 = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165379);
                Drawable b = FileHelper.a.b(watchFace.getPreviewUrl(), dimensionPixelSize2, dimensionPixelSize2, FileType.WATCH_FACE);
                Drawable a = FileHelper.a.a(watchFace.getBackground().getData().getPreviewUrl(), dimensionPixelSize, dimensionPixelSize, watchFace.getWatchFaceType(), FileType.WATCH_FACE);
                int dimensionPixelSize3 = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165420);
                ringStyleItems = watchFace.getRingStyleItems();
                if (ringStyleItems == null) {
                    WatchFaceWrapper.MetaData metaData5 = null;
                    WatchFaceWrapper.MetaData metaData6 = null;
                    metaData = null;
                    drawable2 = null;
                    drawable = null;
                    Drawable drawable5 = null;
                    Drawable drawable6 = null;
                    metaData3 = null;
                    for (RingStyleItem t : ringStyleItems) {
                        String component1 = t.component1();
                        RingStyle component2 = t.component2();
                        switch (component1.hashCode()) {
                            case -1383228885:
                                if (!component1.equals("bottom")) {
                                    break;
                                } else {
                                    drawable = FileHelper.a.b(component2.getData().getPreviewUrl(), dimensionPixelSize3, dimensionPixelSize3, FileType.WATCH_FACE);
                                    MetaData metadata = component2.getMetadata();
                                    if (metadata == null) {
                                        break;
                                    } else {
                                        metaData5 = a(metadata);
                                        break;
                                    }
                                }
                            case 115029:
                                if (!component1.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                    break;
                                } else {
                                    drawable6 = FileHelper.a.b(component2.getData().getPreviewUrl(), dimensionPixelSize3, dimensionPixelSize3, FileType.WATCH_FACE);
                                    MetaData metadata2 = component2.getMetadata();
                                    if (metadata2 == null) {
                                        break;
                                    } else {
                                        metaData = a(metadata2);
                                        break;
                                    }
                                }
                            case 3317767:
                                if (!component1.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                    break;
                                } else {
                                    drawable2 = FileHelper.a.b(component2.getData().getPreviewUrl(), dimensionPixelSize3, dimensionPixelSize3, FileType.WATCH_FACE);
                                    MetaData metadata3 = component2.getMetadata();
                                    if (metadata3 == null) {
                                        break;
                                    } else {
                                        metaData3 = a(metadata3);
                                        break;
                                    }
                                }
                            case 108511772:
                                if (!component1.equals("right")) {
                                    break;
                                } else {
                                    drawable5 = FileHelper.a.b(component2.getData().getPreviewUrl(), dimensionPixelSize3, dimensionPixelSize3, FileType.WATCH_FACE);
                                    MetaData metadata4 = component2.getMetadata();
                                    if (metadata4 == null) {
                                        break;
                                    } else {
                                        metaData6 = a(metadata4);
                                        break;
                                    }
                                }
                        }
                        metaData5 = metaData5;
                        metaData = metaData;
                        drawable2 = drawable2;
                        drawable5 = drawable5;
                        drawable6 = drawable6;
                    }
                    drawable3 = drawable6;
                    metaData2 = metaData5;
                    metaData4 = metaData6;
                    drawable4 = drawable5;
                } else {
                    drawable = null;
                    drawable2 = null;
                    metaData = null;
                    drawable3 = null;
                    metaData2 = null;
                    metaData3 = null;
                    metaData4 = null;
                    drawable4 = null;
                }
                return WatchFaceWrapper.Companion.createBackgroundWrapper(id, b, a, drawable3, drawable4, drawable, drawable2, metaData, metaData4, metaData2, metaData3, watchFace.getName(), a(watchFace, list), Ai4.Companion.a(watchFace.getWatchFaceType()));
            }
        }
        dimensionPixelSize = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165418);
        int dimensionPixelSize22 = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165379);
        Drawable b2 = FileHelper.a.b(watchFace.getPreviewUrl(), dimensionPixelSize22, dimensionPixelSize22, FileType.WATCH_FACE);
        Drawable a2 = FileHelper.a.a(watchFace.getBackground().getData().getPreviewUrl(), dimensionPixelSize, dimensionPixelSize, watchFace.getWatchFaceType(), FileType.WATCH_FACE);
        int dimensionPixelSize32 = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165420);
        ringStyleItems = watchFace.getRingStyleItems();
        if (ringStyleItems == null) {
        }
        return WatchFaceWrapper.Companion.createBackgroundWrapper(id, b2, a2, drawable3, drawable4, drawable, drawable2, metaData, metaData4, metaData2, metaData3, watchFace.getName(), a(watchFace, list), Ai4.Companion.a(watchFace.getWatchFaceType()));
    }
}
