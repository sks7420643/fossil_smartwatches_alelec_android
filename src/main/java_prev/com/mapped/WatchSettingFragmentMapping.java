package com.mapped;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.Pb;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE, staticConstructorAction = DexAction.APPEND)
public class WatchSettingFragmentMapping extends WatchSettingFragmentBinding {
    @DexIgnore
    public static /* final */ ViewDataBinding.i T; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray U;
    @DexIgnore
    public long S;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        U = sparseIntArray;
        sparseIntArray.put(2131361931, 1);
        U.put(2131363342, 2);
        U.put(2131362058, 3);
        U.put(2131362661, 4);
        U.put(2131363257, 5);
        U.put(2131363235, 6);
        U.put(2131361929, 7);
        U.put(2131361934, 8);
        U.put(2131362166, 9);
        U.put(2131362105, 10);
        U.put(2131363280, 11);
        U.put(2131363281, 12);
        U.put(2131363076, 13);
        U.put(2131363272, 14);
        U.put(2131363273, 15);
        U.put(2131363077, 16);
        U.put(2131363326, 17);
        U.put(2131363327, 18);
        U.put(2131363352, 19);
        U.put(2131362270, 20);
        U.put(2131362271, 21);
        U.put(2131362269, 22);
        U.put(2131363268, 23);
        U.put(2131363078, 24);
        U.put(2131363225, 25);
        U.put(2131363226, 26);
        U.put(2131363319, 27);
    }
    */

    static {
        U.put(com.fossil.wearables.fossil.R.id.tv_key_value, 28);

        U.put(com.fossil.wearables.fossil.R.id.switch_android_dnd, 29);
        U.put(com.fossil.wearables.fossil.R.id.switch_android_dnd_high, 30);
        U.put(com.fossil.wearables.fossil.R.id.switch_empty_notifications, 31);
        U.put(com.fossil.wearables.fossil.R.id.switch_auto_sync, 32);
    }

    @DexReplace
    public WatchSettingFragmentMapping(Pb pb, View view) {
        this(pb, view, ViewDataBinding.a(pb, view, 33, T, U));
    }

    @DexReplace
    public WatchSettingFragmentMapping(Pb pb, View view, Object[] objArr) {
        super(
                pb,
                view,
                0,
                (FlexibleButton) objArr[7],
                (RTLImageView) objArr[1],
                (FlexibleButton) objArr[8],
                (ConstraintLayout) objArr[3],
                (ConstraintLayout) objArr[10],
                (CardView) objArr[9],
                (FlexibleButton) objArr[22],
                (FlexibleButton) objArr[20],
                (FlexibleButton) objArr[21],
                (ImageView) objArr[4],
                (ScrollView) objArr[0],
                (View) objArr[13],
                (View) objArr[16],
                (View) objArr[24],
                (FlexibleTextView) objArr[25],
                (FlexibleTextView) objArr[26],
                (FlexibleTextView) objArr[6],
                (FlexibleTextView) objArr[5],
                (FlexibleTextView) objArr[23],
                (FlexibleTextView) objArr[14],
                (FlexibleTextView) objArr[15],
                (FlexibleTextView) objArr[11],
                (FlexibleTextView) objArr[12],
                (FlexibleTextView) objArr[27],
                (FlexibleTextView) objArr[17],
                (FlexibleTextView) objArr[18],
                (FlexibleTextView) objArr[2],
                (FlexibleTextView) objArr[19],
                (FlexibleTextView) objArr[28],
                (FlexibleSwitchCompat) objArr[29],
                (FlexibleSwitchCompat) objArr[30],
                (FlexibleSwitchCompat) objArr[31],
                (FlexibleSwitchCompat) objArr[32]
        );
        this.S = -1;
        ((WatchSettingFragmentBinding) this).A.setTag(null);
        a(view);
        f();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.S = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            return this.S != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.S = 1;
        }
        g();
    }
}
