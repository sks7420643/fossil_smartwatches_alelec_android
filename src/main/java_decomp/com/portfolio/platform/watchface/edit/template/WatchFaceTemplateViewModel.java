package com.portfolio.platform.watchface.edit.template;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Fb7;
import com.fossil.Gu7;
import com.fossil.Hq4;
import com.fossil.Js0;
import com.fossil.Ko7;
import com.fossil.U08;
import com.fossil.Us0;
import com.fossil.W08;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceTemplateViewModel extends Hq4 {
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public List<Fb7> i; // = new ArrayList();
    @DexIgnore
    public String j; // = "";
    @DexIgnore
    public /* final */ MutableLiveData<Fb7> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Js0<Lc6<List<Fb7>, Integer>> l; // = new Js0<>();
    @DexIgnore
    public /* final */ U08 m; // = W08.b(false, 1, null);
    @DexIgnore
    public /* final */ WFAssetRepository n;
    @DexIgnore
    public /* final */ FileRepository o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel$1", f = "WatchFaceTemplateViewModel.kt", l = {25}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceTemplateViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel$1$1", f = "WatchFaceTemplateViewModel.kt", l = {27, 32}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r0v19, types: [java.util.List] */
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0040 A[LOOP:0: B:10:0x003a->B:12:0x0040, LOOP_END] */
            /* JADX WARNING: Removed duplicated region for block: B:18:0x00ae  */
            /* JADX WARNING: Unknown variable types count: 1 */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                /*
                    r9 = this;
                    r2 = 0
                    r5 = 1
                    r4 = 2
                    java.lang.Object r6 = com.fossil.Yn7.d()
                    int r0 = r9.label
                    if (r0 == 0) goto L_0x0056
                    if (r0 == r5) goto L_0x0025
                    if (r0 != r4) goto L_0x001d
                    java.lang.Object r0 = r9.L$1
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r0 = r9.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r10)
                L_0x001a:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x001c:
                    return r0
                L_0x001d:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0025:
                    java.lang.Object r0 = r9.L$1
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r1 = r9.L$0
                    r3 = r1
                    com.mapped.Il6 r3 = (com.mapped.Il6) r3
                    com.fossil.El7.b(r10)
                    r1 = r0
                    r5 = r10
                L_0x0033:
                    r0 = r5
                    java.lang.Iterable r0 = (java.lang.Iterable) r0
                    java.util.Iterator r5 = r0.iterator()
                L_0x003a:
                    boolean r0 = r5.hasNext()
                    if (r0 == 0) goto L_0x0076
                    java.lang.Object r0 = r5.next()
                    com.fossil.P77 r0 = (com.fossil.P77) r0
                    com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel$Ai r7 = r9.this$0
                    com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel r7 = r7.this$0
                    com.portfolio.platform.data.source.FileRepository r7 = com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel.o(r7)
                    com.fossil.Fb7 r0 = com.fossil.Dc7.a(r0, r7)
                    r1.add(r0)
                    goto L_0x003a
                L_0x0056:
                    com.fossil.El7.b(r10)
                    com.mapped.Il6 r3 = r9.p$
                    java.util.ArrayList r1 = new java.util.ArrayList
                    r1.<init>()
                    com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel$Ai r0 = r9.this$0
                    com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel r0 = r0.this$0
                    com.portfolio.platform.watchface.data.source.WFAssetRepository r0 = com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel.n(r0)
                    r9.L$0 = r3
                    r9.L$1 = r1
                    r9.label = r5
                    java.lang.Object r5 = r0.b(r9)
                    if (r5 != r6) goto L_0x0033
                    r0 = r6
                    goto L_0x001c
                L_0x0076:
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel$Ai r5 = r9.this$0
                    com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel r5 = r5.this$0
                    java.lang.String r5 = com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel.p(r5)
                    java.lang.StringBuilder r7 = new java.lang.StringBuilder
                    r7.<init>()
                    java.lang.String r8 = "uiBackgroundTemplatesLive - assets: "
                    r7.append(r8)
                    int r8 = r1.size()
                    r7.append(r8)
                    java.lang.String r7 = r7.toString()
                    r0.e(r5, r7)
                    com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel$Ai r0 = r9.this$0
                    com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel r0 = r0.this$0
                    r9.L$0 = r3
                    r9.L$1 = r1
                    r9.label = r4
                    r3 = r9
                    r5 = r2
                    java.lang.Object r0 = com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel.x(r0, r1, r2, r3, r4, r5)
                    if (r0 != r6) goto L_0x001a
                    r0 = r6
                    goto L_0x001c
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel.Ai.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(WatchFaceTemplateViewModel watchFaceTemplateViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceTemplateViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel$syncPickedTemplate$1", f = "WatchFaceTemplateViewModel.kt", l = {68}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $wfId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceTemplateViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(WatchFaceTemplateViewModel watchFaceTemplateViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceTemplateViewModel;
            this.$wfId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$wfId, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                WatchFaceTemplateViewModel watchFaceTemplateViewModel = this.this$0;
                String str = this.$wfId;
                this.L$0 = il6;
                this.label = 1;
                if (WatchFaceTemplateViewModel.x(watchFaceTemplateViewModel, null, str, this, 1, null) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel", f = "WatchFaceTemplateViewModel.kt", l = {95}, m = "updateAssetsAndCurrentIndex")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceTemplateViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(WatchFaceTemplateViewModel watchFaceTemplateViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = watchFaceTemplateViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.w(null, null, this);
        }
    }

    @DexIgnore
    public WatchFaceTemplateViewModel(WFAssetRepository wFAssetRepository, FileRepository fileRepository) {
        Wg6.c(wFAssetRepository, "assetRepository");
        Wg6.c(fileRepository, "fileRepository");
        this.n = wFAssetRepository;
        this.o = fileRepository;
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ai(this, null), 3, null);
        String simpleName = WatchFaceTemplateViewModel.class.getSimpleName();
        Wg6.b(simpleName, "WatchFaceTemplateViewModel::class.java.simpleName");
        this.h = simpleName;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Object x(WatchFaceTemplateViewModel watchFaceTemplateViewModel, List list, String str, Xe6 xe6, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            list = null;
        }
        if ((i2 & 2) != 0) {
            str = null;
        }
        return watchFaceTemplateViewModel.w(list, str, xe6);
    }

    @DexIgnore
    public final LiveData<Lc6<List<Fb7>, Integer>> q() {
        return this.l;
    }

    @DexIgnore
    public final LiveData<Fb7> r() {
        return this.k;
    }

    @DexIgnore
    public final void s(Fb7 fb7) {
        Wg6.c(fb7, "wf");
        this.k.l(fb7);
    }

    @DexIgnore
    public final void t(String str) {
        if (!Wg6.a(this.j, str)) {
            this.j = str;
        }
    }

    @DexIgnore
    public final void u(List<Fb7> list) {
        if (!Wg6.a(this.i, list)) {
            this.i = list;
        }
    }

    @DexIgnore
    public final void v(String str) {
        Wg6.c(str, "wfId");
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Bi(this, str, null), 3, null);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003f A[Catch:{ all -> 0x0139 }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004d A[Catch:{ all -> 0x0139 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0083 A[Catch:{ all -> 0x0139 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object w(java.util.List<com.fossil.Fb7> r11, java.lang.String r12, com.mapped.Xe6<? super com.mapped.Cd6> r13) {
        /*
        // Method dump skipped, instructions count: 318
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel.w(java.util.List, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }
}
