package com.portfolio.platform.watchface.edit.complication.complication_selector;

import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Hq4;
import com.fossil.Hs0;
import com.fossil.Ik5;
import com.fossil.Jj5;
import com.fossil.Ko7;
import com.fossil.Or0;
import com.fossil.Us0;
import com.fossil.Vt7;
import com.fossil.Ym5;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceComplicationViewModel extends Hq4 {
    @DexIgnore
    public /* final */ MutableLiveData<Ci> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Bi> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Complication> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<List<Complication>> l; // = Or0.c(null, 0, new Di(this, null), 3, null);
    @DexIgnore
    public /* final */ MutableLiveData<List<DianaAppSetting>> m;
    @DexIgnore
    public /* final */ ComplicationRepository n;
    @DexIgnore
    public /* final */ DianaAppSettingRepository o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel$1", f = "WatchFaceComplicationViewModel.kt", l = {48}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(WatchFaceComplicationViewModel watchFaceComplicationViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceComplicationViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v4, types: [androidx.lifecycle.MutableLiveData] */
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            MutableLiveData<List<DianaAppSetting>> u;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                u = this.this$0.u();
                DianaAppSettingRepository dianaAppSettingRepository = this.this$0.o;
                this.L$0 = il6;
                this.L$1 = u;
                this.label = 1;
                obj = dianaAppSettingRepository.getAllDianaAppSettings("complication", this);
                if (obj == d) {
                    return d;
                }
            } else if (i == 1) {
                u = (MutableLiveData) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            u.l(obj);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;

        @DexIgnore
        public Bi(String str, String str2) {
            Wg6.c(str, "complicationId");
            Wg6.c(str2, MicroAppSetting.SETTING);
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Bi) {
                    Bi bi = (Bi) obj;
                    if (!Wg6.a(this.a, bi.a) || !Wg6.a(this.b, bi.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.a;
            int hashCode = str != null ? str.hashCode() : 0;
            String str2 = this.b;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "SettingScreenNavigation(complicationId=" + this.a + ", setting=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public String a;

        @DexIgnore
        public Ci() {
            this(null, 1, null);
        }

        @DexIgnore
        public Ci(String str) {
            Wg6.c(str, "content");
            this.a = str;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ci(String str, int i, Qg6 qg6) {
            this((i & 1) != 0 ? "" : str);
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || ((obj instanceof Ci) && Wg6.a(this.a, ((Ci) obj).a));
        }

        @DexIgnore
        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "SettingValue(content=" + this.a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel$complicationsLiveData$1", f = "WatchFaceComplicationViewModel.kt", l = {39, 39}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Hs0<List<? extends Complication>>, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Hs0 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(WatchFaceComplicationViewModel watchFaceComplicationViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceComplicationViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Hs0) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Hs0<List<? extends Complication>> hs0, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(hs0, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r6) {
            /*
                r5 = this;
                r4 = 2
                r2 = 1
                java.lang.Object r3 = com.fossil.Yn7.d()
                int r0 = r5.label
                if (r0 == 0) goto L_0x0038
                if (r0 == r2) goto L_0x0020
                if (r0 != r4) goto L_0x0018
                java.lang.Object r0 = r5.L$0
                com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                com.fossil.El7.b(r6)
            L_0x0015:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r5.L$1
                com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                java.lang.Object r1 = r5.L$0
                com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                com.fossil.El7.b(r6)
                r2 = r1
            L_0x002c:
                r5.L$0 = r2
                r5.label = r4
                java.lang.Object r0 = r0.b(r6, r5)
                if (r0 != r3) goto L_0x0015
                r0 = r3
                goto L_0x0017
            L_0x0038:
                com.fossil.El7.b(r6)
                com.fossil.Hs0 r1 = r5.p$
                com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel r0 = r5.this$0
                com.portfolio.platform.data.source.ComplicationRepository r0 = com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel.n(r0)
                r5.L$0 = r1
                r5.L$1 = r1
                r5.label = r2
                java.lang.Object r6 = r0.getAllComplicationRaw(r5)
                if (r6 != r3) goto L_0x0051
                r0 = r3
                goto L_0x0017
            L_0x0051:
                r0 = r1
                r2 = r1
                goto L_0x002c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel.Di.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel$getComplication$1", f = "WatchFaceComplicationViewModel.kt", l = {88}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $complicationId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel$getComplication$1$complication$1", f = "WatchFaceComplicationViewModel.kt", l = {89}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Complication>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Complication> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    ComplicationRepository complicationRepository = this.this$0.this$0.n;
                    String str = this.this$0.$complicationId;
                    this.L$0 = il6;
                    this.label = 1;
                    Object complicationById = complicationRepository.getComplicationById(str, this);
                    return complicationById == d ? d : complicationById;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(WatchFaceComplicationViewModel watchFaceComplicationViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceComplicationViewModel;
            this.$complicationId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$complicationId, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchFaceComplicationViewModel", "getComplication, complicationId = " + this.$complicationId);
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.w().o((Complication) g);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel$getComplicationSetting$1", f = "WatchFaceComplicationViewModel.kt", l = {100}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $complicationId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel$getComplicationSetting$1$complicationSetting$1", f = "WatchFaceComplicationViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super DianaAppSetting>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super DianaAppSetting> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                T t;
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    List<DianaAppSetting> e = this.this$0.this$0.u().e();
                    if (e == null) {
                        return null;
                    }
                    Iterator<T> it = e.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        if (Ao7.a(Wg6.a(next.getAppId(), this.this$0.$complicationId)).booleanValue()) {
                            t = next;
                            break;
                        }
                    }
                    return t;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(WatchFaceComplicationViewModel watchFaceComplicationViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceComplicationViewModel;
            this.$complicationId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, this.$complicationId, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            String a2;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchFaceComplicationViewModel", "getComplicationSetting - complicationId = " + this.$complicationId);
                if (!Ik5.d.d(this.$complicationId)) {
                    this.this$0.y().o(new Ci(null, 1, null));
                    return Cd6.a;
                }
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            DianaAppSetting dianaAppSetting = (DianaAppSetting) g;
            if (dianaAppSetting == null || !(!Vt7.l(dianaAppSetting.getSetting()))) {
                a2 = Ik5.d.a(this.$complicationId);
            } else {
                Gson gson = new Gson();
                try {
                    String str = this.$complicationId;
                    int hashCode = str.hashCode();
                    if (hashCode != -829740640) {
                        if (hashCode == 134170930 && str.equals("second-timezone")) {
                            a2 = ((SecondTimezoneSetting) gson.k(dianaAppSetting.getSetting(), SecondTimezoneSetting.class)).getTimeZoneName();
                        }
                    } else if (str.equals("commute-time")) {
                        a2 = ((CommuteTimeSetting) gson.k(dianaAppSetting.getSetting(), CommuteTimeSetting.class)).getAddress();
                    }
                    a2 = "";
                } catch (Exception e) {
                    FLogger.INSTANCE.getLocal().e("WatchFaceComplicationViewModel", e.getMessage());
                    e.printStackTrace();
                    a2 = Ik5.d.a(this.$complicationId);
                }
            }
            this.this$0.y().o(new Ci(a2));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel$navigateToSettingScreen$1", f = "WatchFaceComplicationViewModel.kt", l = {72}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super String>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $complicationId;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(String str, Xe6 xe6, Gi gi) {
                super(2, xe6);
                this.$complicationId = str;
                this.this$0 = gi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$complicationId, xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super String> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                T t;
                String setting;
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    List<DianaAppSetting> e = this.this$0.this$0.u().e();
                    if (e != null) {
                        Iterator<T> it = e.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t = null;
                                break;
                            }
                            T next = it.next();
                            if (Ao7.a(Wg6.a(next.getAppId(), this.$complicationId)).booleanValue()) {
                                t = next;
                                break;
                            }
                        }
                        T t2 = t;
                        return (t2 == null || (setting = t2.getSetting()) == null) ? "" : setting;
                    }
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(WatchFaceComplicationViewModel watchFaceComplicationViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceComplicationViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.this$0, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            String complicationId;
            Object g;
            String str;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Complication e = this.this$0.w().e();
                if (!(e == null || (complicationId = e.getComplicationId()) == null)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchFaceComplicationViewModel", "navigateToSettingScreen, complicationId = " + complicationId);
                    Dv7 b = Bw7.b();
                    Aii aii = new Aii(complicationId, null, this);
                    this.L$0 = il6;
                    this.L$1 = complicationId;
                    this.label = 1;
                    g = Eu7.g(b, aii, this);
                    if (g == d) {
                        return d;
                    }
                    str = complicationId;
                }
                return Cd6.a;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                str = (String) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.x().l(new Bi(str, (String) g));
            return Cd6.a;
        }
    }

    @DexIgnore
    public WatchFaceComplicationViewModel(ComplicationRepository complicationRepository, DianaAppSettingRepository dianaAppSettingRepository) {
        Wg6.c(complicationRepository, "mComplicationRepository");
        Wg6.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        this.n = complicationRepository;
        this.o = dianaAppSettingRepository;
        new MutableLiveData();
        this.m = new MutableLiveData<>();
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Ai(this, null), 2, null);
    }

    @DexIgnore
    public final void A(String str, Parcelable parcelable) {
        Object obj;
        Wg6.c(str, "appId");
        Wg6.c(parcelable, "data");
        ArrayList arrayList = new ArrayList();
        List<DianaAppSetting> e = this.m.e();
        if (e == null) {
            e = new ArrayList<>();
        }
        arrayList.addAll(e);
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            Object next = it.next();
            if (Wg6.a(((DianaAppSetting) next).getAppId(), str)) {
                obj = next;
                break;
            }
        }
        DianaAppSetting dianaAppSetting = (DianaAppSetting) obj;
        if (dianaAppSetting != null) {
            dianaAppSetting.setSetting(Jj5.a(parcelable));
        } else {
            String e2 = Ym5.e(24);
            Wg6.b(e2, "StringHelper.randomUUID(24)");
            arrayList.add(new DianaAppSetting(e2, str, "complication", Jj5.a(parcelable)));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceComplicationViewModel", "setting is changed " + arrayList);
        this.m.l(arrayList);
    }

    @DexIgnore
    public final void p(String str) {
        Wg6.c(str, "complicationId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceComplicationViewModel", "buildData, complicationId = " + str);
        s(str);
        t(str);
    }

    @DexIgnore
    public final void q() {
        List<Complication> e = this.l.e();
        if (e != null) {
            Iterator<T> it = e.iterator();
            while (it.hasNext()) {
                if (!Ik5.d.f(it.next().getComplicationId())) {
                    this.j.l(Boolean.FALSE);
                    return;
                }
            }
        }
        this.j.l(Boolean.TRUE);
    }

    @DexIgnore
    public final MutableLiveData<Boolean> r() {
        return this.j;
    }

    @DexIgnore
    public final Rm6 s(String str) {
        return Gu7.d(Us0.a(this), null, null, new Ei(this, str, null), 3, null);
    }

    @DexIgnore
    public final Rm6 t(String str) {
        return Gu7.d(Us0.a(this), null, null, new Fi(this, str, null), 3, null);
    }

    @DexIgnore
    public final MutableLiveData<List<DianaAppSetting>> u() {
        return this.m;
    }

    @DexIgnore
    public final LiveData<List<Complication>> v() {
        return this.l;
    }

    @DexIgnore
    public final MutableLiveData<Complication> w() {
        return this.k;
    }

    @DexIgnore
    public final MutableLiveData<Bi> x() {
        return this.i;
    }

    @DexIgnore
    public final MutableLiveData<Ci> y() {
        return this.h;
    }

    @DexIgnore
    public final Rm6 z() {
        return Gu7.d(Us0.a(this), null, null, new Gi(this, null), 3, null);
    }
}
