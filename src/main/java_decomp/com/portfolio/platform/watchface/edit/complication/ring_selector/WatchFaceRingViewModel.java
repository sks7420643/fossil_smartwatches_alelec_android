package com.portfolio.platform.watchface.edit.complication.ring_selector;

import androidx.lifecycle.LiveData;
import com.fossil.Bw7;
import com.fossil.Hq4;
import com.fossil.Hs0;
import com.fossil.Ko7;
import com.fossil.Or0;
import com.fossil.P77;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceRingViewModel extends Hq4 {
    @DexIgnore
    public /* final */ LiveData<List<P77>> h; // = Or0.c(Bw7.b(), 0, new Ai(this, null), 2, null);
    @DexIgnore
    public /* final */ WFAssetRepository i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.complication.ring_selector.WatchFaceRingViewModel$ringsLiveData$1", f = "WatchFaceRingViewModel.kt", l = {12, 12}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Hs0<List<? extends P77>>, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Hs0 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceRingViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(WatchFaceRingViewModel watchFaceRingViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceRingViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Hs0) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Hs0<List<? extends P77>> hs0, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(hs0, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r6) {
            /*
                r5 = this;
                r4 = 2
                r2 = 1
                java.lang.Object r3 = com.fossil.Yn7.d()
                int r0 = r5.label
                if (r0 == 0) goto L_0x0038
                if (r0 == r2) goto L_0x0020
                if (r0 != r4) goto L_0x0018
                java.lang.Object r0 = r5.L$0
                com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                com.fossil.El7.b(r6)
            L_0x0015:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r5.L$1
                com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                java.lang.Object r1 = r5.L$0
                com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                com.fossil.El7.b(r6)
                r2 = r0
            L_0x002c:
                r5.L$0 = r1
                r5.label = r4
                java.lang.Object r0 = r2.b(r6, r5)
                if (r0 != r3) goto L_0x0015
                r0 = r3
                goto L_0x0017
            L_0x0038:
                com.fossil.El7.b(r6)
                com.fossil.Hs0 r0 = r5.p$
                com.portfolio.platform.watchface.edit.complication.ring_selector.WatchFaceRingViewModel r1 = r5.this$0
                com.portfolio.platform.watchface.data.source.WFAssetRepository r1 = com.portfolio.platform.watchface.edit.complication.ring_selector.WatchFaceRingViewModel.n(r1)
                r5.L$0 = r0
                r5.L$1 = r0
                r5.label = r2
                java.lang.Object r6 = r1.f(r5)
                if (r6 != r3) goto L_0x0051
                r0 = r3
                goto L_0x0017
            L_0x0051:
                r1 = r0
                r2 = r0
                goto L_0x002c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.complication.ring_selector.WatchFaceRingViewModel.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public WatchFaceRingViewModel(WFAssetRepository wFAssetRepository) {
        Wg6.c(wFAssetRepository, "mRepository");
        this.i = wFAssetRepository;
    }

    @DexIgnore
    public final LiveData<List<P77>> o() {
        return this.h;
    }
}
