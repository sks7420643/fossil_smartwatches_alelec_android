package com.portfolio.platform.watchface.edit.photobackground.data.source;

import com.fossil.El7;
import com.fossil.G97;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WFBackgroundPhotoRemote {
    @DexIgnore
    public /* final */ ApiServiceV2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRemote$deleteBackgroundPhotos$2", f = "WFBackgroundPhotoRemote.kt", l = {20}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $deletedPhotoIds;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(WFBackgroundPhotoRemote wFBackgroundPhotoRemote, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = wFBackgroundPhotoRemote;
            this.$deletedPhotoIds = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Ai(this.this$0, this.$deletedPhotoIds, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Ai) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                Ku3 ku3 = this.$deletedPhotoIds;
                this.label = 1;
                Object deleteBackgroundPhotos = apiServiceV2.deleteBackgroundPhotos(ku3, this);
                return deleteBackgroundPhotos == d ? d : deleteBackgroundPhotos;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRemote$fetchBackgroundPhotos$2", f = "WFBackgroundPhotoRemote.kt", l = {12}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<G97>>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(WFBackgroundPhotoRemote wFBackgroundPhotoRemote, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = wFBackgroundPhotoRemote;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Bi(this.this$0, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<ApiResponse<G97>>> xe6) {
            throw null;
            //return ((Bi) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                this.label = 1;
                Object fetchBackgroundPhotos = apiServiceV2.fetchBackgroundPhotos(this);
                return fetchBackgroundPhotos == d ? d : fetchBackgroundPhotos;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRemote$upsertBackgroundPhoto$2", f = "WFBackgroundPhotoRemote.kt", l = {16}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<G97>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $photoData;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(WFBackgroundPhotoRemote wFBackgroundPhotoRemote, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = wFBackgroundPhotoRemote;
            this.$photoData = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Ci(this.this$0, this.$photoData, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<ApiResponse<G97>>> xe6) {
            throw null;
            //return ((Ci) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                Ku3 ku3 = this.$photoData;
                this.label = 1;
                Object upsertBackgroundPhotos = apiServiceV2.upsertBackgroundPhotos(ku3, this);
                return upsertBackgroundPhotos == d ? d : upsertBackgroundPhotos;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public WFBackgroundPhotoRemote(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        this.a = apiServiceV2;
    }

    @DexIgnore
    public final Object b(Ku3 ku3, Xe6<? super Ap4<Ku3>> xe6) {
        return ResponseKt.d(new Ai(this, ku3, null), xe6);
    }

    @DexIgnore
    public final Object c(Xe6<? super Ap4<ApiResponse<G97>>> xe6) {
        return ResponseKt.d(new Bi(this, null), xe6);
    }

    @DexIgnore
    public final Object d(Ku3 ku3, Xe6<? super Ap4<ApiResponse<G97>>> xe6) {
        return ResponseKt.d(new Ci(this, ku3, null), xe6);
    }
}
