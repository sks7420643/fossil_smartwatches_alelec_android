package com.portfolio.platform.watchface.edit.sticker;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Hq4;
import com.fossil.Ko7;
import com.fossil.Pm7;
import com.fossil.S87;
import com.fossil.U37;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.WatchFaceStickerStorage;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceStickerViewModel extends Hq4 {
    @DexIgnore
    public /* final */ MutableLiveData<List<S87.Bi>> h;
    @DexIgnore
    public /* final */ LiveData<List<S87.Bi>> i;
    @DexIgnore
    public /* final */ MutableLiveData<U37<Boolean>> j;
    @DexIgnore
    public /* final */ LiveData<U37<Boolean>> k;
    @DexIgnore
    public /* final */ WFAssetRepository l;
    @DexIgnore
    public /* final */ FileRepository m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel$1", f = "WatchFaceStickerViewModel.kt", l = {36}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceStickerViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(WatchFaceStickerViewModel watchFaceStickerViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceStickerViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.h.l(Pm7.h0(WatchFaceStickerStorage.c.f()));
                WatchFaceStickerViewModel watchFaceStickerViewModel = this.this$0;
                this.L$0 = il6;
                this.label = 1;
                if (watchFaceStickerViewModel.r(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel$checkAndDownloadMissingResources$2", f = "WatchFaceStickerViewModel.kt", l = {43, 57, 60}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceStickerViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(WatchFaceStickerViewModel watchFaceStickerViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceStickerViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v30, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0060  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00ec  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0189  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x01a2  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x01a5  */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 434
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel", f = "WatchFaceStickerViewModel.kt", l = {67}, m = "loadMissingBitmap")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceStickerViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(WatchFaceStickerViewModel watchFaceStickerViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = watchFaceStickerViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(this);
        }
    }

    @DexIgnore
    public WatchFaceStickerViewModel(WFAssetRepository wFAssetRepository, FileRepository fileRepository) {
        Wg6.c(wFAssetRepository, "wfAssetRepository");
        Wg6.c(fileRepository, "fileRepository");
        this.l = wFAssetRepository;
        this.m = fileRepository;
        MutableLiveData<List<S87.Bi>> mutableLiveData = new MutableLiveData<>();
        this.h = mutableLiveData;
        this.i = mutableLiveData;
        MutableLiveData<U37<Boolean>> mutableLiveData2 = new MutableLiveData<>(new U37(Boolean.FALSE));
        this.j = mutableLiveData2;
        this.k = mutableLiveData2;
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    public final /* synthetic */ Object r(Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new Bi(this, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final LiveData<U37<Boolean>> s() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<List<S87.Bi>> t() {
        return this.i;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object u(com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r6 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel.Ci
            if (r0 == 0) goto L_0x0095
            r0 = r7
            com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel$Ci r0 = (com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel.Ci) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0095
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x00a4
            if (r3 != r4) goto L_0x009c
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel r0 = (com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel) r0
            com.fossil.El7.b(r1)
            r6 = r0
        L_0x0027:
            r0 = r1
            java.lang.Iterable r0 = (java.lang.Iterable) r0
            java.util.Iterator r2 = r0.iterator()
        L_0x002e:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00b7
            java.lang.Object r0 = r2.next()
            com.fossil.P77 r0 = (com.fossil.P77) r0
            com.fossil.Q77 r1 = r0.c()
            java.lang.String r1 = r1.a()
            java.lang.String r1 = com.fossil.Ym5.b(r1)
            com.portfolio.platform.data.source.FileRepository r3 = r6.m
            java.lang.String r4 = "fileName"
            com.mapped.Wg6.b(r1, r4)
            com.misfit.frameworks.buttonservice.model.FileType r4 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            java.io.File r1 = r3.getFileByName(r1, r4)
            if (r1 == 0) goto L_0x00b5
            java.lang.String r1 = r1.getAbsolutePath()
        L_0x0059:
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeFile(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "load missing bitmap, file="
            r4.append(r5)
            com.fossil.Q77 r5 = r0.c()
            java.lang.String r5 = r5.a()
            r4.append(r5)
            java.lang.String r5 = ", bitmap="
            r4.append(r5)
            r4.append(r1)
            java.lang.String r5 = "WatchFaceStickerViewModel"
            java.lang.String r4 = r4.toString()
            r3.d(r5, r4)
            if (r1 == 0) goto L_0x002e
            com.mapped.WatchFaceStickerStorage r3 = com.mapped.WatchFaceStickerStorage.c
            java.lang.String r0 = r0.f()
            r3.a(r0, r1)
            goto L_0x002e
        L_0x0095:
            com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel$Ci r0 = new com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel$Ci
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x009c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x00a4:
            com.fossil.El7.b(r1)
            com.portfolio.platform.watchface.data.source.WFAssetRepository r1 = r6.l
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r1 = r1.g(r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
        L_0x00b4:
            return r0
        L_0x00b5:
            r1 = 0
            goto L_0x0059
        L_0x00b7:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
            goto L_0x00b4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel.u(com.mapped.Xe6):java.lang.Object");
    }
}
