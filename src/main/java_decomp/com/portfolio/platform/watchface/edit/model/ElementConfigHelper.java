package com.portfolio.platform.watchface.edit.model;

import android.graphics.Bitmap;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Gm7;
import com.fossil.Mb7;
import com.fossil.Pb7;
import com.fossil.R87;
import com.fossil.S87;
import com.fossil.Tb7;
import com.fossil.V87;
import com.fossil.Vb7;
import com.fossil.X87;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.WatchFaceFontStorage;
import com.mapped.WatchFaceStickerStorage;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ElementConfigHelper {
    @DexIgnore
    public static /* final */ ElementConfigHelper a; // = new ElementConfigHelper();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.model.ElementConfigHelper", f = "ElementConfigHelper.kt", l = {126, 127, 129}, m = "buildComplicationConfig")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ElementConfigHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(ElementConfigHelper elementConfigHelper, Xe6 xe6) {
            super(xe6);
            this.this$0 = elementConfigHelper;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.model.ElementConfigHelper", f = "ElementConfigHelper.kt", l = {49}, m = "buildFromThemeData")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ElementConfigHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(ElementConfigHelper elementConfigHelper, Xe6 xe6) {
            super(xe6);
            this.this$0 = elementConfigHelper;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, null, null, null, null, this);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x016a  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x01c3  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01c7  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0261  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0269  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x027a  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x028e  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x029b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.Sb7 r27, com.portfolio.platform.watchface.data.source.WFAssetRepository r28, com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository r29, com.portfolio.platform.data.source.UserRepository r30, com.portfolio.platform.manager.CustomizeRealDataManager r31, com.mapped.Xe6<? super com.fossil.S87.Ai> r32) {
        /*
        // Method dump skipped, instructions count: 690
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.model.ElementConfigHelper.a(com.fossil.Sb7, com.portfolio.platform.watchface.data.source.WFAssetRepository, com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository, com.portfolio.platform.data.source.UserRepository, com.portfolio.platform.manager.CustomizeRealDataManager, com.mapped.Xe6):java.lang.Object");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v7, types: [java.lang.Iterable] */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0114 A[SYNTHETIC] */
    public final java.lang.Object b(com.fossil.Ub7 r23, com.portfolio.platform.watchface.data.source.WFAssetRepository r24, com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository r25, com.portfolio.platform.data.source.UserRepository r26, com.portfolio.platform.manager.CustomizeRealDataManager r27, com.mapped.Xe6<? super java.util.List<? extends com.fossil.S87>> r28) {
        /*
        // Method dump skipped, instructions count: 279
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.model.ElementConfigHelper.b(com.fossil.Ub7, com.portfolio.platform.watchface.data.source.WFAssetRepository, com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository, com.portfolio.platform.data.source.UserRepository, com.portfolio.platform.manager.CustomizeRealDataManager, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final S87.Bi c(Vb7 vb7) {
        Pb7 b = vb7.b();
        S87.Bi e = WatchFaceStickerStorage.c.e(b.d());
        if (e != null) {
            e.q(vb7.a());
            e.d(V87.c(b.b()));
            e.o(b.a());
            e.p(b.a());
            return e;
        }
        WatchFaceStickerStorage watchFaceStickerStorage = WatchFaceStickerStorage.c;
        String d = b.d();
        Bitmap a2 = vb7.a();
        if (a2 != null) {
            watchFaceStickerStorage.a(d, a2);
            String d2 = b.d();
            String c = vb7.c();
            if (c == null) {
                c = "";
            }
            return new S87.Bi(R87.b(b.c()), Gm7.b(new X87(d2, c)), null, V87.c(b.b()), 0, 0, 52, null);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final S87.Ci d(Tb7 tb7) {
        Mb7 a2 = tb7.a();
        return new S87.Ci(a2.f(), WatchFaceFontStorage.d.i(a2.e()), a2.d(), R87.b(a2.c()), V87.c(a2.b()), a2.a(), a2.a());
    }
}
