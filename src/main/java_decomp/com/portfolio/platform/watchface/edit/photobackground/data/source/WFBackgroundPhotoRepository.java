package com.portfolio.platform.watchface.edit.photobackground.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WFBackgroundPhotoRepository {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ WFBackgroundPhotoRemote b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository", f = "WFBackgroundPhotoRepository.kt", l = {129}, m = "clean")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(WFBackgroundPhotoRepository wFBackgroundPhotoRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFBackgroundPhotoRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository", f = "WFBackgroundPhotoRepository.kt", l = {121}, m = "deletePhotoLocally")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(WFBackgroundPhotoRepository wFBackgroundPhotoRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFBackgroundPhotoRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository", f = "WFBackgroundPhotoRepository.kt", l = {85, 89, 94}, m = "deletePhotos")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(WFBackgroundPhotoRepository wFBackgroundPhotoRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFBackgroundPhotoRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository", f = "WFBackgroundPhotoRepository.kt", l = {113}, m = "displayPhotos")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(WFBackgroundPhotoRepository wFBackgroundPhotoRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFBackgroundPhotoRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository", f = "WFBackgroundPhotoRepository.kt", l = {28, 38}, m = "fetchBackgroundPhotos")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(WFBackgroundPhotoRepository wFBackgroundPhotoRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFBackgroundPhotoRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository", f = "WFBackgroundPhotoRepository.kt", l = {117}, m = "insertPhotos")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(WFBackgroundPhotoRepository wFBackgroundPhotoRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFBackgroundPhotoRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository", f = "WFBackgroundPhotoRepository.kt", l = {119}, m = "insertPhotos")
    public static final class Gi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(WFBackgroundPhotoRepository wFBackgroundPhotoRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFBackgroundPhotoRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository", f = "WFBackgroundPhotoRepository.kt", l = {125}, m = "pendingBackgroundPhotos")
    public static final class Hi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(WFBackgroundPhotoRepository wFBackgroundPhotoRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFBackgroundPhotoRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository", f = "WFBackgroundPhotoRepository.kt", l = {115}, m = "photoWithId")
    public static final class Ii extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(WFBackgroundPhotoRepository wFBackgroundPhotoRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFBackgroundPhotoRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository", f = "WFBackgroundPhotoRepository.kt", l = {127}, m = "removedBackgroundPhotos")
    public static final class Ji extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(WFBackgroundPhotoRepository wFBackgroundPhotoRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFBackgroundPhotoRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.j(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository", f = "WFBackgroundPhotoRepository.kt", l = {123}, m = "updateDeletedPinType")
    public static final class Ki extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(WFBackgroundPhotoRepository wFBackgroundPhotoRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFBackgroundPhotoRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository", f = "WFBackgroundPhotoRepository.kt", l = {55, 65}, m = "upsertPhotos")
    public static final class Li extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFBackgroundPhotoRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Li(WFBackgroundPhotoRepository wFBackgroundPhotoRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFBackgroundPhotoRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.l(null, this);
        }
    }

    @DexIgnore
    public WFBackgroundPhotoRepository(WFBackgroundPhotoRemote wFBackgroundPhotoRemote) {
        Wg6.c(wFBackgroundPhotoRemote, "remote");
        this.b = wFBackgroundPhotoRemote;
        String simpleName = WFBackgroundPhotoRepository.class.getSimpleName();
        Wg6.b(simpleName, "WFBackgroundPhotoRepository::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.mapped.Xe6<? super com.mapped.Cd6> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Ai
            if (r0 == 0) goto L_0x0033
            r0 = r6
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Ai r0 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository r0 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.H97 r0 = r0.getWFBackgroundDao()
            r0.a()
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Ai r0 = new com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Ai
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r1)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.a(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(java.lang.String r6, com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Bi
            if (r0 == 0) goto L_0x003a
            r0 = r7
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Bi r0 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Bi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003a
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0049
            if (r3 != r4) goto L_0x0041
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository r1 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.H97 r0 = r0.getWFBackgroundDao()
            r0.b(r6)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0039:
            return r0
        L_0x003a:
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Bi r0 = new com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Bi
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0041:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0049:
            com.fossil.El7.b(r2)
            com.portfolio.platform.manager.EncryptedDatabaseManager r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.b(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0125  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0165  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0185  */
    /* JADX WARNING: Removed duplicated region for block: B:54:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(java.util.List<java.lang.String> r19, com.mapped.Xe6<? super com.fossil.Kz4<com.mapped.Ku3>> r20) {
        /*
        // Method dump skipped, instructions count: 429
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.c(java.util.List, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.mapped.Xe6<? super java.util.List<com.fossil.G97>> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Di
            if (r0 == 0) goto L_0x0032
            r0 = r6
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Di r0 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Di) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0040
            if (r3 != r4) goto L_0x0038
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository r0 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.H97 r0 = r0.getWFBackgroundDao()
            java.util.List r0 = r0.f()
        L_0x0031:
            return r0
        L_0x0032:
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Di r0 = new com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Di
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0038:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0040:
            com.fossil.El7.b(r1)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.d(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.G97>>> r12) {
        /*
        // Method dump skipped, instructions count: 283
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.e(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object f(java.lang.String r11, java.lang.String r12, com.mapped.Xe6<? super java.lang.Long> r13) {
        /*
            r10 = this;
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r8 = 1
            boolean r0 = r13 instanceof com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Fi
            if (r0 == 0) goto L_0x0050
            r0 = r13
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Fi r0 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Fi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0050
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r1 = r2.label
            if (r1 == 0) goto L_0x005f
            if (r1 != r8) goto L_0x0057
            java.lang.Object r0 = r2.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r2.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository r2 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository) r2
            com.fossil.El7.b(r3)
            r2 = r3
            r7 = r0
        L_0x0031:
            r0 = r2
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.H97 r9 = r0.getWFBackgroundDao()
            com.fossil.G97 r0 = new com.fossil.G97
            java.lang.String r2 = ""
            java.lang.String r3 = ""
            java.lang.String r4 = ""
            java.lang.String r5 = ""
            java.lang.String r6 = ""
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            long r0 = r9.c(r0)
            java.lang.Long r0 = com.fossil.Ao7.f(r0)
        L_0x004f:
            return r0
        L_0x0050:
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Fi r0 = new com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Fi
            r0.<init>(r10, r13)
            r2 = r0
            goto L_0x0014
        L_0x0057:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005f:
            com.fossil.El7.b(r3)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r2.L$0 = r10
            r2.L$1 = r11
            r2.L$2 = r12
            r2.label = r8
            java.lang.Object r2 = r1.v(r2)
            if (r2 == r0) goto L_0x004f
            r7 = r12
            r1 = r11
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.f(java.lang.String, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g(java.util.List<com.fossil.G97> r6, com.mapped.Xe6<? super java.lang.Long[]> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Gi
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Gi r0 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Gi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository r1 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.H97 r0 = r0.getWFBackgroundDao()
            java.lang.Long[] r0 = r0.insert(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Gi r0 = new com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Gi
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.El7.b(r2)
            com.portfolio.platform.manager.EncryptedDatabaseManager r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.g(java.util.List, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object h(com.mapped.Xe6<? super java.util.List<com.fossil.G97>> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Hi
            if (r0 == 0) goto L_0x0032
            r0 = r6
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Hi r0 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Hi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0040
            if (r3 != r4) goto L_0x0038
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository r0 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.H97 r0 = r0.getWFBackgroundDao()
            java.util.List r0 = r0.e()
        L_0x0031:
            return r0
        L_0x0032:
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Hi r0 = new com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Hi
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0038:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0040:
            com.fossil.El7.b(r1)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.h(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object i(java.lang.String r6, com.mapped.Xe6<? super com.fossil.G97> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Ii
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Ii r0 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Ii) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository r1 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.H97 r0 = r0.getWFBackgroundDao()
            com.fossil.G97 r0 = r0.g(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Ii r0 = new com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Ii
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.El7.b(r2)
            com.portfolio.platform.manager.EncryptedDatabaseManager r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.i(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object j(com.mapped.Xe6<? super java.util.List<com.fossil.G97>> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Ji
            if (r0 == 0) goto L_0x0032
            r0 = r6
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Ji r0 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Ji) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0040
            if (r3 != r4) goto L_0x0038
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository r0 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.H97 r0 = r0.getWFBackgroundDao()
            java.util.List r0 = r0.d()
        L_0x0031:
            return r0
        L_0x0032:
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Ji r0 = new com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Ji
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0038:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0040:
            com.fossil.El7.b(r1)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.j(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object k(java.lang.String r6, com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Ki
            if (r0 == 0) goto L_0x003a
            r0 = r7
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Ki r0 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.Ki) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003a
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0049
            if (r3 != r4) goto L_0x0041
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository r1 = (com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.H97 r0 = r0.getWFBackgroundDao()
            r0.h(r6)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0039:
            return r0
        L_0x003a:
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Ki r0 = new com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository$Ki
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0041:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0049:
            com.fossil.El7.b(r2)
            com.portfolio.platform.manager.EncryptedDatabaseManager r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.k(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object l(java.util.List<com.fossil.F97> r12, com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.G97>>> r13) {
        /*
        // Method dump skipped, instructions count: 300
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository.l(java.util.List, com.mapped.Xe6):java.lang.Object");
    }
}
