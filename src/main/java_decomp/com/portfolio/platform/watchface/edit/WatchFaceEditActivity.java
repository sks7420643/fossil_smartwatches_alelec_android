package com.portfolio.platform.watchface.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.B77;
import com.fossil.Fc7;
import com.fossil.Nw3;
import com.fossil.Xq0;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceEditActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            Wg6.c(context, "context");
            Wg6.c(str, "watchFaceId");
            Intent intent = new Intent(context, WatchFaceEditActivity.class);
            intent.putExtra("preset_id", str);
            context.startActivity(intent);
        }

        @DexIgnore
        public final void b(Context context, boolean z) {
            Wg6.c(context, "context");
            Intent intent = new Intent(context, WatchFaceEditActivity.class);
            intent.putExtra("from_faces", true);
            intent.putExtra("sharing_flow_extra", z);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Fragment Y = getSupportFragmentManager().Y(Nw3.container);
        if (Y != null) {
            Y.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.portfolio.platform.ui.BaseActivity
    public void onBackPressed() {
        WatchFaceEditFragment watchFaceEditFragment = (WatchFaceEditFragment) getSupportFragmentManager().Z(WatchFaceEditFragment.D.a());
        if (watchFaceEditFragment != null) {
            watchFaceEditFragment.onBackPressed();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558850);
        D(System.currentTimeMillis());
        if (bundle == null) {
            Xq0 j = getSupportFragmentManager().j();
            j.s(Nw3.container, Fc7.b(Fc7.a, WatchFaceEditFragment.class, getIntent().getStringExtra("preset_id"), getIntent().getIntExtra("TAB_EXTRA", 2), null, getIntent().getBooleanExtra("from_faces", false), getIntent().getBooleanExtra("sharing_flow_extra", false), 8, null), WatchFaceEditFragment.D.a());
            j.j();
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onDestroy() {
        B77.a.h("wf_editing_session", p(), System.currentTimeMillis());
        super.onDestroy();
    }
}
