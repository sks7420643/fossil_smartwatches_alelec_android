package com.portfolio.platform.watchface.utils;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.El7;
import com.fossil.Fb7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.L77;
import com.fossil.P77;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WFCommon {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ WFCommon b; // = new WFCommon();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFCommon this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(WFCommon wFCommon, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFCommon;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Rm6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ FileRepository $fileRepository;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetRepository $wfAssetRepository;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object c;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WFAssetRepository wFAssetRepository = this.this$0.$wfAssetRepository;
                    this.L$0 = il6;
                    this.label = 1;
                    c = wFAssetRepository.c(this);
                    if (c == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    c = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                Kz4 kz4 = (Kz4) c;
                if (kz4.c() != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    WFCommon wFCommon = WFCommon.b;
                    String str = WFCommon.a;
                    local.e(str, "templates: " + ((List) kz4.c()));
                    for (P77 p77 : (Iterable) kz4.c()) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        WFCommon wFCommon2 = WFCommon.b;
                        String str2 = WFCommon.a;
                        local2.e(str2, "downloading template: " + p77.c().a());
                        FileRepository.asyncDownloadFromUrl$default(this.this$0.$fileRepository, p77.c().a(), FileType.WATCH_FACE, null, 4, null);
                    }
                }
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object e;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WFAssetRepository wFAssetRepository = this.this$0.$wfAssetRepository;
                    this.L$0 = il6;
                    this.label = 1;
                    e = wFAssetRepository.e(this);
                    if (e == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    e = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                Kz4 kz4 = (Kz4) e;
                if (kz4.c() != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    WFCommon wFCommon = WFCommon.b;
                    String str = WFCommon.a;
                    local.e(str, "tickers: " + ((List) kz4.c()));
                    for (P77 p77 : (Iterable) kz4.c()) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        WFCommon wFCommon2 = WFCommon.b;
                        String str2 = WFCommon.a;
                        local2.e(str2, "downloading ticker: " + p77.c().a());
                        FileRepository.asyncDownloadFromUrl$default(this.this$0.$fileRepository, p77.c().a(), FileType.WATCH_FACE, null, 4, null);
                    }
                }
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d;
                Object d2 = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WFAssetRepository wFAssetRepository = this.this$0.$wfAssetRepository;
                    this.L$0 = il6;
                    this.label = 1;
                    d = wFAssetRepository.d(this);
                    if (d == d2) {
                        return d2;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    d = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                Kz4 kz4 = (Kz4) d;
                if (kz4.c() != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    WFCommon wFCommon = WFCommon.b;
                    String str = WFCommon.a;
                    local.e(str, "rings: " + ((List) kz4.c()));
                    for (P77 p77 : (Iterable) kz4.c()) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        WFCommon wFCommon2 = WFCommon.b;
                        String str2 = WFCommon.a;
                        local2.e(str2, "downloading ring: " + p77.c().a());
                        FileRepository.asyncDownloadFromUrl$default(this.this$0.$fileRepository, p77.c().a(), FileType.WATCH_FACE, null, 4, null);
                    }
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(WFAssetRepository wFAssetRepository, FileRepository fileRepository, Xe6 xe6) {
            super(2, xe6);
            this.$wfAssetRepository = wFAssetRepository;
            this.$fileRepository = fileRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.$wfAssetRepository, this.$fileRepository, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Rm6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                WFCommon wFCommon = WFCommon.b;
                local.e(WFCommon.a, "start fetching assets");
                Rm6 unused = Gu7.d(il6, null, null, new Aii(this, null), 3, null);
                Rm6 unused2 = Gu7.d(il6, null, null, new Bii(this, null), 3, null);
                return Gu7.d(il6, null, null, new Cii(this, null), 3, null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFCommon this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(WFCommon wFCommon, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFCommon;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(null, null, null, this);
        }
    }

    /*
    static {
        String simpleName = WFCommon.class.getSimpleName();
        Wg6.b(simpleName, "WFCommon::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.watchface.utils.WFCommon */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Object f(WFCommon wFCommon, WFBackgroundPhotoRepository wFBackgroundPhotoRepository, FileRepository fileRepository, List list, Xe6 xe6, int i, Object obj) {
        if ((i & 4) != 0) {
            list = null;
        }
        return wFCommon.e(wFBackgroundPhotoRepository, fileRepository, list, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object b(com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository r11, com.portfolio.platform.data.source.FileRepository r12, com.mapped.Xe6<? super java.lang.Boolean> r13) {
        /*
        // Method dump skipped, instructions count: 259
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.utils.WFCommon.b(com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository, com.portfolio.platform.data.source.FileRepository, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object c(WFAssetRepository wFAssetRepository, FileRepository fileRepository, Xe6<? super Rm6> xe6) {
        return Jv7.e(new Bi(wFAssetRepository, fileRepository, null), xe6);
    }

    @DexIgnore
    public final Fb7 d() {
        return new Fb7("-metadata.priority", "", "", "", L77.BACKGROUND_TEMPLATE, "", null, null, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01a2  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01ba  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0203  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0252  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository r14, com.portfolio.platform.data.source.FileRepository r15, java.util.List<com.fossil.G97> r16, com.mapped.Xe6<? super com.mapped.Cd6> r17) {
        /*
        // Method dump skipped, instructions count: 619
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.utils.WFCommon.e(com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository, com.portfolio.platform.data.source.FileRepository, java.util.List, com.mapped.Xe6):java.lang.Object");
    }
}
