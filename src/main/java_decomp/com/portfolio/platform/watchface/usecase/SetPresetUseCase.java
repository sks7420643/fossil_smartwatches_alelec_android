package com.portfolio.platform.watchface.usecase;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Mo5;
import com.fossil.Oo5;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.CustomizeConfigurationExt;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetPresetUseCase extends CoroutineUseCase<Ci, Di, Bi> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ Ai e; // = new Ai();
    @DexIgnore
    public /* final */ Ei f; // = new Ei();
    @DexIgnore
    public Mo5 g;
    @DexIgnore
    public /* final */ DianaPresetRepository h;
    @DexIgnore
    public /* final */ FileRepository i;
    @DexIgnore
    public /* final */ An4 j;
    @DexIgnore
    public /* final */ DianaAppSettingRepository k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai implements BleCommandResultManager.Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.usecase.SetPresetUseCase$ApplyThemeReceiver$receive$1", f = "SetPresetUseCase.kt", l = {57}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                List<Oo5> a2;
                Object allDianaAppSettings;
                Gson gson;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Mo5 mo5 = SetPresetUseCase.this.g;
                    if (mo5 != null) {
                        a2 = mo5.a();
                        if (a2 != null) {
                            Gson gson2 = new Gson();
                            DianaAppSettingRepository dianaAppSettingRepository = SetPresetUseCase.this.k;
                            this.L$0 = il6;
                            this.L$1 = a2;
                            this.L$2 = gson2;
                            this.label = 1;
                            allDianaAppSettings = dianaAppSettingRepository.getAllDianaAppSettings("watch_app", this);
                            if (allDianaAppSettings == d) {
                                return d;
                            }
                            gson = gson2;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else if (i == 1) {
                    a2 = (List) this.L$1;
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    gson = (Gson) this.L$2;
                    allDianaAppSettings = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                PortfolioApp.get.instance().M1(CustomizeConfigurationExt.l(a2, gson, (List) allDianaAppSettings), PortfolioApp.get.instance().J());
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            if (SetPresetUseCase.this.s() && communicateMode == CommunicateMode.APPLY_THEME_SESSION) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetPresetUseCase", "onReceive - ApplyThemeReceiver - isExecuted " + SetPresetUseCase.this.s() + " communicateMode=" + communicateMode);
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("SetPresetUseCase", "apply theme success,start set watch apps preset " + SetPresetUseCase.this.g);
                    if (SetPresetUseCase.this.g != null) {
                        Rm6 unused = Gu7.d(SetPresetUseCase.this.g(), null, null, new Aii(this, null), 3, null);
                        return;
                    }
                    SetPresetUseCase.this.v(false);
                    SetPresetUseCase.this.w();
                    SetPresetUseCase.this.i(new Bi(-1, new ArrayList()));
                    return;
                }
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("SetPresetUseCase", "onReceive - failed erorCode " + intExtra + " permissionErrorCodes " + integerArrayListExtra);
                SetPresetUseCase.this.w();
                SetPresetUseCase.this.i(new Bi(intExtra, integerArrayListExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public Bi(int i, ArrayList<Integer> arrayList) {
            Wg6.c(arrayList, "mBLEErrorCodes");
            this.a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Ci(String str) {
            Wg6.c(str, "presetId");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ei implements BleCommandResultManager.Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Mo5 $currentPreset;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Mo5 mo5, Xe6 xe6, Ei ei) {
                super(2, xe6);
                this.$currentPreset = mo5;
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$currentPreset, xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r0v16, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r1v10, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r0v25, types: [java.util.List] */
            /* JADX WARNING: Removed duplicated region for block: B:13:0x0062  */
            /* JADX WARNING: Removed duplicated region for block: B:19:0x0088  */
            /* JADX WARNING: Removed duplicated region for block: B:22:0x00d9  */
            /* JADX WARNING: Removed duplicated region for block: B:26:0x0116  */
            /* JADX WARNING: Unknown variable types count: 3 */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                /*
                // Method dump skipped, instructions count: 285
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.usecase.SetPresetUseCase.Ei.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            if (SetPresetUseCase.this.s() && communicateMode == CommunicateMode.SET_WATCH_APPS) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetPresetUseCase", "onReceive - SetWatchAppReceiver - isExecuted " + SetPresetUseCase.this.s() + " communicateMode=" + communicateMode);
                Mo5 mo5 = SetPresetUseCase.this.g;
                if (mo5 != null) {
                    Rm6 unused = Gu7.d(SetPresetUseCase.this.g(), null, null, new Aii(mo5, null, this), 3, null);
                }
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.FAILED.ordinal()) {
                    SetPresetUseCase.this.j.Z0(PortfolioApp.get.instance().J(), -1, false);
                }
                SetPresetUseCase.this.w();
                SetPresetUseCase.this.v(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.usecase.SetPresetUseCase", f = "SetPresetUseCase.kt", l = {119, 126, 133}, m = "run")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ SetPresetUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(SetPresetUseCase setPresetUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = setPresetUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(null, this);
        }
    }

    @DexIgnore
    public SetPresetUseCase(DianaPresetRepository dianaPresetRepository, FileRepository fileRepository, An4 an4, WFAssetRepository wFAssetRepository, DianaAppSettingRepository dianaAppSettingRepository) {
        Wg6.c(dianaPresetRepository, "mDianaPresetRepository");
        Wg6.c(fileRepository, "mFileRepository");
        Wg6.c(an4, "mSharedPrefs");
        Wg6.c(wFAssetRepository, "wfAssetRepository");
        Wg6.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        this.h = dianaPresetRepository;
        this.i = fileRepository;
        this.j = an4;
        this.k = dianaAppSettingRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "SetPresetUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ci ci, Xe6 xe6) {
        return u(ci, xe6);
    }

    @DexIgnore
    public final boolean s() {
        return this.d;
    }

    @DexIgnore
    public final void t() {
        BleCommandResultManager.d.e(this.e, CommunicateMode.APPLY_THEME_SESSION);
        BleCommandResultManager.d.e(this.f, CommunicateMode.SET_WATCH_APPS);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0142  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0193  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object u(com.portfolio.platform.watchface.usecase.SetPresetUseCase.Ci r14, com.mapped.Xe6<java.lang.Object> r15) {
        /*
        // Method dump skipped, instructions count: 406
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.usecase.SetPresetUseCase.u(com.portfolio.platform.watchface.usecase.SetPresetUseCase$Ci, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void v(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final void w() {
        BleCommandResultManager.d.j(this.e, CommunicateMode.APPLY_THEME_SESSION);
        BleCommandResultManager.d.j(this.f, CommunicateMode.SET_WATCH_APPS);
    }
}
