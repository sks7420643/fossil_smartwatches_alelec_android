package com.portfolio.platform.watchface.faces;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.Ds0;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hq4;
import com.fossil.Jn5;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Nm0;
import com.fossil.Oa7;
import com.fossil.Po4;
import com.fossil.Qv5;
import com.fossil.S37;
import com.fossil.Tg5;
import com.fossil.Ts0;
import com.fossil.U37;
import com.fossil.Uh5;
import com.fossil.Va7;
import com.fossil.Vs0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.watchface.edit.WatchFaceEditActivity;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryActivity;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceListFragment extends Qv5 {
    @DexIgnore
    public static /* final */ Ai t; // = new Ai(null);
    @DexIgnore
    public Po4 h;
    @DexIgnore
    public WatchFaceListViewModel i;
    @DexIgnore
    public Oa7 j;
    @DexIgnore
    public Tg5 k;
    @DexIgnore
    public String l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final WatchFaceListFragment a(String str, String str2, boolean z) {
            Wg6.c(str, "watchFaceId");
            Wg6.c(str2, "type");
            WatchFaceListFragment watchFaceListFragment = new WatchFaceListFragment();
            watchFaceListFragment.setArguments(Nm0.a(new Lc6("WATCH_FACE_ID", str), new Lc6("WATCH_FACE_ID_TYPE", str2), new Lc6("sharing_flow_extra", Boolean.valueOf(z))));
            return watchFaceListFragment;
        }

        @DexIgnore
        public final WatchFaceListFragment b(String str, String str2) {
            Wg6.c(str, "id");
            Wg6.c(str2, "token");
            Bundle bundle = new Bundle();
            bundle.putString("EXTRA_WATCH_FACE_SHARED_ID", str);
            bundle.putString("EXTRA_WATCH_FACE_SHARED_TOKEN", str2);
            bundle.putBoolean("sharing_flow_extra", true);
            WatchFaceListFragment watchFaceListFragment = new WatchFaceListFragment();
            watchFaceListFragment.setArguments(bundle);
            return watchFaceListFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment a;

        @DexIgnore
        public Bi(WatchFaceListFragment watchFaceListFragment) {
            this.a = watchFaceListFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            boolean z = true;
            int i = 0;
            int intValue = t.intValue();
            FLogger.INSTANCE.getLocal().d("WatchFaceListFragment", "watchFaceListSize=" + intValue);
            boolean z2 = intValue != 0;
            Tg5 tg5 = this.a.k;
            if (tg5 != null) {
                ViewPager2 viewPager2 = tg5.j;
                Wg6.b(viewPager2, "mBinding!!.viewPager");
                int currentItem = viewPager2.getCurrentItem();
                Tg5 tg52 = this.a.k;
                if (tg52 != null) {
                    ViewPager2 viewPager22 = tg52.j;
                    Wg6.b(viewPager22, "mBinding!!.viewPager");
                    viewPager22.setVisibility(z2 ? 0 : 8);
                    Tg5 tg53 = this.a.k;
                    if (tg53 != null) {
                        RTLImageView rTLImageView = tg53.e;
                        Wg6.b(rTLImageView, "mBinding!!.fbPreview");
                        if (!z2 || currentItem != 0) {
                            z = false;
                        }
                        rTLImageView.setVisibility(z ? 0 : 8);
                        Tg5 tg54 = this.a.k;
                        if (tg54 != null) {
                            Group group = tg54.c;
                            Wg6.b(group, "mBinding!!.emptyGroup");
                            if (!(!z2)) {
                                i = 8;
                            }
                            group.setVisibility(i);
                            return;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment a;

        @DexIgnore
        public Ci(WatchFaceListFragment watchFaceListFragment) {
            this.a = watchFaceListFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            Boolean bool = (Boolean) t.a();
            if (bool != null) {
                boolean booleanValue = bool.booleanValue();
                Tg5 tg5 = this.a.k;
                if (tg5 != null) {
                    ViewPager2 viewPager2 = tg5.j;
                    Wg6.b(viewPager2, "mBinding!!.viewPager");
                    if (viewPager2.getCurrentItem() == 0) {
                        Tg5 tg52 = this.a.k;
                        if (tg52 != null) {
                            ViewPager2 viewPager22 = tg52.j;
                            Wg6.b(viewPager22, "mBinding!!.viewPager");
                            viewPager22.setUserInputEnabled(booleanValue);
                            return;
                        }
                        Wg6.i();
                        throw null;
                    }
                    return;
                }
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment a;

        @DexIgnore
        public Di(WatchFaceListFragment watchFaceListFragment) {
            this.a = watchFaceListFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            T t2 = t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceListFragment", "update new id " + ((String) t2));
            this.a.l = t2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment a;

        @DexIgnore
        public Ei(WatchFaceListFragment watchFaceListFragment) {
            this.a = watchFaceListFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            String str = (String) t.a();
            if (str != null) {
                WatchFaceListFragment.O6(this.a).x(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment a;

        @DexIgnore
        public Fi(WatchFaceListFragment watchFaceListFragment) {
            this.a = watchFaceListFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            String str = (String) t.a();
            if (str != null) {
                WatchFaceListFragment.O6(this.a).y(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment b;

        @DexIgnore
        public Gi(WatchFaceListFragment watchFaceListFragment) {
            this.b = watchFaceListFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment b;

        @DexIgnore
        public Hi(WatchFaceListFragment watchFaceListFragment) {
            this.b = watchFaceListFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceListFragment", "preview watch face id " + this.b.l);
            if (Jn5.c(Jn5.b, this.b.getContext(), Jn5.Ai.SET_BLE_COMMAND, false, false, false, null, 60, null)) {
                WatchFaceListFragment.O6(this.b).E(this.b.l);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.faces.WatchFaceListFragment$onActivityCreated$12$1", f = "WatchFaceListFragment.kt", l = {160}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ii this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ii ii, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ii;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object D;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WatchFaceListViewModel O6 = WatchFaceListFragment.O6(this.this$0.b);
                    this.L$0 = il6;
                    this.label = 1;
                    D = O6.D(this);
                    if (D == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    D = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (!((Boolean) D).booleanValue()) {
                    WatchFaceEditActivity.a aVar = WatchFaceEditActivity.A;
                    Context requireContext = this.this$0.b.requireContext();
                    Wg6.b(requireContext, "requireContext()");
                    aVar.b(requireContext, this.this$0.b.m);
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Ii(WatchFaceListFragment watchFaceListFragment) {
            this.b = watchFaceListFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            Rm6 unused = Gu7.d(Ds0.a(this.b), null, null, new Aii(this, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment b;

        @DexIgnore
        public Ji(WatchFaceListFragment watchFaceListFragment) {
            this.b = watchFaceListFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchFaceGalleryActivity.a aVar = WatchFaceGalleryActivity.B;
            Context requireContext = this.b.requireContext();
            Wg6.b(requireContext, "requireContext()");
            aVar.a(requireContext);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki<T> implements Ls0<Hq4.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment a;

        @DexIgnore
        public Ki(WatchFaceListFragment watchFaceListFragment) {
            this.a = watchFaceListFragment;
        }

        @DexIgnore
        public final void a(Hq4.Ci ci) {
            if (!ci.a().isEmpty()) {
                WatchFaceListFragment watchFaceListFragment = this.a;
                Object[] array = ci.a().toArray(new Uh5[0]);
                if (array != null) {
                    Uh5[] uh5Arr = (Uh5[]) array;
                    watchFaceListFragment.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Ci ci) {
            a(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li<T> implements Ls0<Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment a;

        @DexIgnore
        public Li(WatchFaceListFragment watchFaceListFragment) {
            this.a = watchFaceListFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(Object obj) {
            if (this.a.isActive()) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
                FragmentActivity requireActivity = this.a.requireActivity();
                Wg6.b(requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi<T> implements Ls0<Hq4.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment a;

        @DexIgnore
        public Mi(WatchFaceListFragment watchFaceListFragment) {
            this.a = watchFaceListFragment;
        }

        @DexIgnore
        public final void a(Hq4.Ai ai) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.B0(childFragmentManager, ai.b());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment a;

        @DexIgnore
        public Ni(WatchFaceListFragment watchFaceListFragment) {
            this.a = watchFaceListFragment;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceListFragment", "update new id " + str);
            Oa7 N6 = WatchFaceListFragment.N6(this.a);
            Wg6.b(str, "it");
            N6.t(str);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment a;

        @DexIgnore
        public Oi(WatchFaceListFragment watchFaceListFragment) {
            this.a = watchFaceListFragment;
        }

        @DexIgnore
        public final void a(String str) {
            HomeActivity.a aVar = HomeActivity.B;
            FragmentActivity requireActivity = this.a.requireActivity();
            Wg6.b(requireActivity, "requireActivity()");
            Wg6.b(str, "presetId");
            aVar.c(requireActivity, str, this.a.m);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi<T> implements Ls0<Hq4.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment a;

        @DexIgnore
        public Pi(WatchFaceListFragment watchFaceListFragment) {
            this.a = watchFaceListFragment;
        }

        @DexIgnore
        public final void a(Hq4.Bi bi) {
            if (bi.a()) {
                this.a.b();
            }
            if (bi.b()) {
                this.a.a();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Bi bi) {
            a(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Qi extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListFragment a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Qi(WatchFaceListFragment watchFaceListFragment) {
            this.a = watchFaceListFragment;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void c(int i) {
            int i2 = 0;
            super.c(i);
            Tg5 tg5 = this.a.k;
            if (tg5 != null) {
                RTLImageView rTLImageView = tg5.e;
                Wg6.b(rTLImageView, "mBinding!!.fbPreview");
                if (!(i == 0)) {
                    i2 = 8;
                }
                rTLImageView.setVisibility(i2);
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public static final /* synthetic */ Oa7 N6(WatchFaceListFragment watchFaceListFragment) {
        Oa7 oa7 = watchFaceListFragment.j;
        if (oa7 != null) {
            return oa7;
        }
        Wg6.n("mSharedViewModel");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ WatchFaceListViewModel O6(WatchFaceListFragment watchFaceListFragment) {
        WatchFaceListViewModel watchFaceListViewModel = watchFaceListFragment.i;
        if (watchFaceListViewModel != null) {
            return watchFaceListViewModel;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "WatchFaceListFragment";
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        String str;
        String str2;
        super.onActivityCreated(bundle);
        PortfolioApp.get.instance().getIface().q0().b(this);
        Po4 po4 = this.h;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(WatchFaceListViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026istViewModel::class.java)");
            this.i = (WatchFaceListViewModel) a2;
            FragmentActivity requireActivity = requireActivity();
            if (requireActivity != null) {
                this.j = ((WatchFaceListActivity) requireActivity).M();
                Bundle arguments = getArguments();
                if (arguments != null) {
                    String string = arguments.getString("EXTRA_WATCH_FACE_SHARED_ID");
                    if (string == null) {
                        string = "";
                    }
                    Wg6.b(string, "it.getString(EXTRA_WATCH_FACE_SHARED_ID) ?: \"\"");
                    String string2 = arguments.getString("EXTRA_WATCH_FACE_SHARED_TOKEN");
                    if (string2 == null) {
                        string2 = "";
                    }
                    Wg6.b(string2, "it.getString(EXTRA_WATCH_FACE_SHARED_TOKEN) ?: \"\"");
                    this.m = arguments.getBoolean("sharing_flow_extra");
                    WatchFaceListViewModel watchFaceListViewModel = this.i;
                    if (watchFaceListViewModel != null) {
                        watchFaceListViewModel.z(string, string2);
                    } else {
                        Wg6.n("mViewModel");
                        throw null;
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchFaceListFragment", "onActivityCreated sharingFlow " + this.m);
                Bundle arguments2 = getArguments();
                if (arguments2 == null || (str = arguments2.getString("WATCH_FACE_ID")) == null) {
                    str = "";
                }
                Wg6.b(str, "arguments?.getString(Con\u2026ants.WATCH_FACE_ID) ?: \"\"");
                Bundle arguments3 = getArguments();
                if (arguments3 == null || (str2 = arguments3.getString("WATCH_FACE_ID_TYPE")) == null) {
                    str2 = "";
                }
                Wg6.b(str2, "arguments?.getString(Con\u2026WATCH_FACE_ID_TYPE) ?: \"\"");
                Va7 va7 = new Va7(this, str, str2);
                Qi qi = new Qi(this);
                Tg5 tg5 = this.k;
                if (tg5 != null) {
                    ViewPager2 viewPager2 = tg5.j;
                    viewPager2.setOverScrollMode(2);
                    viewPager2.setUserInputEnabled(false);
                    viewPager2.setAdapter(va7);
                    viewPager2.g(qi);
                    WatchFaceListViewModel watchFaceListViewModel2 = this.i;
                    if (watchFaceListViewModel2 != null) {
                        LiveData<Integer> C = watchFaceListViewModel2.C();
                        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
                        Wg6.b(viewLifecycleOwner, "viewLifecycleOwner");
                        C.h(viewLifecycleOwner, new Bi(this));
                        WatchFaceListViewModel watchFaceListViewModel3 = this.i;
                        if (watchFaceListViewModel3 != null) {
                            watchFaceListViewModel3.l().h(getViewLifecycleOwner(), new Ki(this));
                            WatchFaceListViewModel watchFaceListViewModel4 = this.i;
                            if (watchFaceListViewModel4 != null) {
                                watchFaceListViewModel4.m().h(getViewLifecycleOwner(), new Li(this));
                                WatchFaceListViewModel watchFaceListViewModel5 = this.i;
                                if (watchFaceListViewModel5 != null) {
                                    watchFaceListViewModel5.h().h(getViewLifecycleOwner(), new Mi(this));
                                    WatchFaceListViewModel watchFaceListViewModel6 = this.i;
                                    if (watchFaceListViewModel6 != null) {
                                        watchFaceListViewModel6.B().h(getViewLifecycleOwner(), new Ni(this));
                                        WatchFaceListViewModel watchFaceListViewModel7 = this.i;
                                        if (watchFaceListViewModel7 != null) {
                                            watchFaceListViewModel7.A().h(getViewLifecycleOwner(), new Oi(this));
                                            WatchFaceListViewModel watchFaceListViewModel8 = this.i;
                                            if (watchFaceListViewModel8 != null) {
                                                watchFaceListViewModel8.j().h(getViewLifecycleOwner(), new Pi(this));
                                                Tg5 tg52 = this.k;
                                                if (tg52 != null) {
                                                    tg52.g.setOnClickListener(new Gi(this));
                                                    Tg5 tg53 = this.k;
                                                    if (tg53 != null) {
                                                        tg53.e.setOnClickListener(new Hi(this));
                                                        Tg5 tg54 = this.k;
                                                        if (tg54 != null) {
                                                            tg54.d.setOnClickListener(new Ii(this));
                                                            Tg5 tg55 = this.k;
                                                            if (tg55 != null) {
                                                                tg55.b.setOnClickListener(new Ji(this));
                                                                Oa7 oa7 = this.j;
                                                                if (oa7 != null) {
                                                                    LiveData<U37<Boolean>> s2 = oa7.s();
                                                                    LifecycleOwner viewLifecycleOwner2 = getViewLifecycleOwner();
                                                                    Wg6.b(viewLifecycleOwner2, "viewLifecycleOwner");
                                                                    s2.h(viewLifecycleOwner2, new Ci(this));
                                                                    Oa7 oa72 = this.j;
                                                                    if (oa72 != null) {
                                                                        LiveData<String> q = oa72.q();
                                                                        LifecycleOwner viewLifecycleOwner3 = getViewLifecycleOwner();
                                                                        Wg6.b(viewLifecycleOwner3, "viewLifecycleOwner");
                                                                        q.h(viewLifecycleOwner3, new Di(this));
                                                                        Oa7 oa73 = this.j;
                                                                        if (oa73 != null) {
                                                                            LiveData<U37<String>> p = oa73.p();
                                                                            LifecycleOwner viewLifecycleOwner4 = getViewLifecycleOwner();
                                                                            Wg6.b(viewLifecycleOwner4, "viewLifecycleOwner");
                                                                            p.h(viewLifecycleOwner4, new Ei(this));
                                                                            Oa7 oa74 = this.j;
                                                                            if (oa74 != null) {
                                                                                LiveData<U37<String>> r = oa74.r();
                                                                                LifecycleOwner viewLifecycleOwner5 = getViewLifecycleOwner();
                                                                                Wg6.b(viewLifecycleOwner5, "viewLifecycleOwner");
                                                                                r.h(viewLifecycleOwner5, new Fi(this));
                                                                                return;
                                                                            }
                                                                            Wg6.n("mSharedViewModel");
                                                                            throw null;
                                                                        }
                                                                        Wg6.n("mSharedViewModel");
                                                                        throw null;
                                                                    }
                                                                    Wg6.n("mSharedViewModel");
                                                                    throw null;
                                                                }
                                                                Wg6.n("mSharedViewModel");
                                                                throw null;
                                                            }
                                                            Wg6.i();
                                                            throw null;
                                                        }
                                                        Wg6.i();
                                                        throw null;
                                                    }
                                                    Wg6.i();
                                                    throw null;
                                                }
                                                Wg6.i();
                                                throw null;
                                            }
                                            Wg6.n("mViewModel");
                                            throw null;
                                        }
                                        Wg6.n("mViewModel");
                                        throw null;
                                    }
                                    Wg6.n("mViewModel");
                                    throw null;
                                }
                                Wg6.n("mViewModel");
                                throw null;
                            }
                            Wg6.n("mViewModel");
                            throw null;
                        }
                        Wg6.n("mViewModel");
                        throw null;
                    }
                    Wg6.n("mViewModel");
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.watchface.faces.WatchFaceListActivity");
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Tg5 c = Tg5.c(layoutInflater);
        this.k = c;
        if (c != null) {
            ConstraintLayout b = c.b();
            Wg6.b(b, "mBinding!!.root");
            return b;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
