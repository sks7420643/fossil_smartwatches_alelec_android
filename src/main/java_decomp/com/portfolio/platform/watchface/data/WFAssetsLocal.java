package com.portfolio.platform.watchface.data;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WFAssetsLocal {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.data.WFAssetsLocal", f = "WFAssetsLocal.kt", l = {20}, m = "assetWithName")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetsLocal this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(WFAssetsLocal wFAssetsLocal, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFAssetsLocal;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.data.WFAssetsLocal", f = "WFAssetsLocal.kt", l = {12}, m = "assetsWithType")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetsLocal this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(WFAssetsLocal wFAssetsLocal, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFAssetsLocal;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.data.WFAssetsLocal", f = "WFAssetsLocal.kt", l = {22}, m = "deleteWithType")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetsLocal this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(WFAssetsLocal wFAssetsLocal, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFAssetsLocal;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.data.WFAssetsLocal", f = "WFAssetsLocal.kt", l = {10}, m = "insert")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetsLocal this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(WFAssetsLocal wFAssetsLocal, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFAssetsLocal;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(null, this);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.L77 r6, java.lang.String r7, com.mapped.Xe6<? super com.fossil.P77> r8) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.watchface.data.WFAssetsLocal.Ai
            if (r0 == 0) goto L_0x003d
            r0 = r8
            com.portfolio.platform.watchface.data.WFAssetsLocal$Ai r0 = (com.portfolio.platform.watchface.data.WFAssetsLocal.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003d
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r1 = r2.label
            if (r1 == 0) goto L_0x004c
            if (r1 != r4) goto L_0x0044
            java.lang.Object r0 = r2.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r2.L$1
            com.fossil.L77 r1 = (com.fossil.L77) r1
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.watchface.data.WFAssetsLocal r2 = (com.portfolio.platform.watchface.data.WFAssetsLocal) r2
            com.fossil.El7.b(r3)
            r2 = r3
            r7 = r0
        L_0x0031:
            r0 = r2
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.U77 r0 = r0.getWFTemplateDao()
            com.fossil.P77 r0 = r0.c(r1, r7)
        L_0x003c:
            return r0
        L_0x003d:
            com.portfolio.platform.watchface.data.WFAssetsLocal$Ai r0 = new com.portfolio.platform.watchface.data.WFAssetsLocal$Ai
            r0.<init>(r5, r8)
            r2 = r0
            goto L_0x0014
        L_0x0044:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004c:
            com.fossil.El7.b(r3)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r2.L$0 = r5
            r2.L$1 = r6
            r2.L$2 = r7
            r2.label = r4
            java.lang.Object r2 = r1.v(r2)
            if (r2 == r0) goto L_0x003c
            r1 = r6
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.data.WFAssetsLocal.a(com.fossil.L77, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.fossil.L77 r6, com.mapped.Xe6<? super java.util.List<com.fossil.P77>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.watchface.data.WFAssetsLocal.Bi
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.watchface.data.WFAssetsLocal$Bi r0 = (com.portfolio.platform.watchface.data.WFAssetsLocal.Bi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            com.fossil.L77 r0 = (com.fossil.L77) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.watchface.data.WFAssetsLocal r1 = (com.portfolio.platform.watchface.data.WFAssetsLocal) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.U77 r0 = r0.getWFTemplateDao()
            java.util.List r0 = r0.b(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.watchface.data.WFAssetsLocal$Bi r0 = new com.portfolio.platform.watchface.data.WFAssetsLocal$Bi
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.El7.b(r2)
            com.portfolio.platform.manager.EncryptedDatabaseManager r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.data.WFAssetsLocal.b(com.fossil.L77, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.fossil.L77 r6, com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.watchface.data.WFAssetsLocal.Ci
            if (r0 == 0) goto L_0x003a
            r0 = r7
            com.portfolio.platform.watchface.data.WFAssetsLocal$Ci r0 = (com.portfolio.platform.watchface.data.WFAssetsLocal.Ci) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003a
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0049
            if (r3 != r4) goto L_0x0041
            java.lang.Object r0 = r1.L$1
            com.fossil.L77 r0 = (com.fossil.L77) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.watchface.data.WFAssetsLocal r1 = (com.portfolio.platform.watchface.data.WFAssetsLocal) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.U77 r0 = r0.getWFTemplateDao()
            r0.a(r6)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0039:
            return r0
        L_0x003a:
            com.portfolio.platform.watchface.data.WFAssetsLocal$Ci r0 = new com.portfolio.platform.watchface.data.WFAssetsLocal$Ci
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0041:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0049:
            com.fossil.El7.b(r2)
            com.portfolio.platform.manager.EncryptedDatabaseManager r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.data.WFAssetsLocal.c(com.fossil.L77, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(java.util.List<com.fossil.P77> r6, com.mapped.Xe6<? super java.lang.Long[]> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.watchface.data.WFAssetsLocal.Di
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.watchface.data.WFAssetsLocal$Di r0 = (com.portfolio.platform.watchface.data.WFAssetsLocal.Di) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.watchface.data.WFAssetsLocal r1 = (com.portfolio.platform.watchface.data.WFAssetsLocal) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.U77 r0 = r0.getWFTemplateDao()
            java.lang.Long[] r0 = r0.insert(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.watchface.data.WFAssetsLocal$Di r0 = new com.portfolio.platform.watchface.data.WFAssetsLocal$Di
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.El7.b(r2)
            com.portfolio.platform.manager.EncryptedDatabaseManager r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.data.WFAssetsLocal.d(java.util.List, com.mapped.Xe6):java.lang.Object");
    }
}
