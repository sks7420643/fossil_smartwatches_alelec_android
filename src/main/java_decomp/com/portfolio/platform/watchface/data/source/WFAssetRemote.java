package com.portfolio.platform.watchface.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.P77;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WFAssetRemote {
    @DexIgnore
    public /* final */ ApiServiceV2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.data.source.WFAssetRemote", f = "WFAssetRemote.kt", l = {16}, m = "fetchBackgroundTemplates")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(WFAssetRemote wFAssetRemote, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFAssetRemote;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.data.source.WFAssetRemote$fetchBackgroundTemplates$response$1", f = "WFAssetRemote.kt", l = {16}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<P77>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $sortCondition;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(WFAssetRemote wFAssetRemote, String str, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = wFAssetRemote;
            this.$sortCondition = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Bi(this.this$0, this.$sortCondition, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<ApiResponse<P77>>> xe6) {
            throw null;
            //return ((Bi) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                String str = this.$sortCondition;
                this.label = 1;
                Object fetchWFBackgroundTemplates = apiServiceV2.fetchWFBackgroundTemplates(str, 100, this);
                return fetchWFBackgroundTemplates == d ? d : fetchWFBackgroundTemplates;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.data.source.WFAssetRemote", f = "WFAssetRemote.kt", l = {41}, m = "fetchRings")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(WFAssetRemote wFAssetRemote, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFAssetRemote;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.data.source.WFAssetRemote$fetchRings$response$1", f = "WFAssetRemote.kt", l = {41}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<P77>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $sortQuery;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(WFAssetRemote wFAssetRemote, String str, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = wFAssetRemote;
            this.$sortQuery = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Di(this.this$0, this.$sortQuery, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<ApiResponse<P77>>> xe6) {
            throw null;
            //return ((Di) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                String str = this.$sortQuery;
                this.label = 1;
                Object wFRings = apiServiceV2.getWFRings(str, 100, this);
                return wFRings == d ? d : wFRings;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.data.source.WFAssetRemote", f = "WFAssetRemote.kt", l = {28}, m = "fetchTickers")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(WFAssetRemote wFAssetRemote, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFAssetRemote;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.data.source.WFAssetRemote$fetchTickers$response$1", f = "WFAssetRemote.kt", l = {28}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<P77>>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(WFAssetRemote wFAssetRemote, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = wFAssetRemote;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Fi(this.this$0, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<ApiResponse<P77>>> xe6) {
            throw null;
            //return ((Fi) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                this.label = 1;
                Object fetchWFTicker = apiServiceV2.fetchWFTicker(100, this);
                return fetchWFTicker == d ? d : fetchWFTicker;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public WFAssetRemote(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        this.a = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.P77>>> r9) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.portfolio.platform.watchface.data.source.WFAssetRemote.Ai
            if (r0 == 0) goto L_0x0044
            r0 = r9
            com.portfolio.platform.watchface.data.source.WFAssetRemote$Ai r0 = (com.portfolio.platform.watchface.data.source.WFAssetRemote.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0044
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0053
            if (r0 != r5) goto L_0x004b
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.watchface.data.source.WFAssetRemote r0 = (com.portfolio.platform.watchface.data.source.WFAssetRemote) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x002d:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x006d
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x0043:
            return r0
        L_0x0044:
            com.portfolio.platform.watchface.data.source.WFAssetRemote$Ai r0 = new com.portfolio.platform.watchface.data.source.WFAssetRemote$Ai
            r0.<init>(r8, r9)
            r1 = r0
            goto L_0x0015
        L_0x004b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0053:
            com.fossil.El7.b(r2)
            com.portfolio.platform.watchface.data.source.WFAssetRemote$Bi r0 = new com.portfolio.platform.watchface.data.source.WFAssetRemote$Bi
            java.lang.String r2 = "-metadata.priority"
            r0.<init>(r8, r2, r4)
            r1.L$0 = r8
            java.lang.String r2 = "-metadata.priority"
            r1.L$1 = r2
            r1.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x0043
        L_0x006d:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x008a
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0043
        L_0x008a:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.data.source.WFAssetRemote.b(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.P77>>> r9) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.portfolio.platform.watchface.data.source.WFAssetRemote.Ci
            if (r0 == 0) goto L_0x0044
            r0 = r9
            com.portfolio.platform.watchface.data.source.WFAssetRemote$Ci r0 = (com.portfolio.platform.watchface.data.source.WFAssetRemote.Ci) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0044
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0053
            if (r0 != r5) goto L_0x004b
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.watchface.data.source.WFAssetRemote r0 = (com.portfolio.platform.watchface.data.source.WFAssetRemote) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x002d:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x006d
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x0043:
            return r0
        L_0x0044:
            com.portfolio.platform.watchface.data.source.WFAssetRemote$Ci r0 = new com.portfolio.platform.watchface.data.source.WFAssetRemote$Ci
            r0.<init>(r8, r9)
            r1 = r0
            goto L_0x0015
        L_0x004b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0053:
            com.fossil.El7.b(r2)
            com.portfolio.platform.watchface.data.source.WFAssetRemote$Di r0 = new com.portfolio.platform.watchface.data.source.WFAssetRemote$Di
            java.lang.String r2 = "-metadata.priority"
            r0.<init>(r8, r2, r4)
            r1.L$0 = r8
            java.lang.String r2 = "-metadata.priority"
            r1.L$1 = r2
            r1.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x0043
        L_0x006d:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x008a
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0043
        L_0x008a:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.data.source.WFAssetRemote.c(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.P77>>> r9) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.portfolio.platform.watchface.data.source.WFAssetRemote.Ei
            if (r0 == 0) goto L_0x003f
            r0 = r9
            com.portfolio.platform.watchface.data.source.WFAssetRemote$Ei r0 = (com.portfolio.platform.watchface.data.source.WFAssetRemote.Ei) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003f
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x004d
            if (r3 != r5) goto L_0x0045
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.watchface.data.source.WFAssetRemote r0 = (com.portfolio.platform.watchface.data.source.WFAssetRemote) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0028:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0061
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x003e:
            return r0
        L_0x003f:
            com.portfolio.platform.watchface.data.source.WFAssetRemote$Ei r0 = new com.portfolio.platform.watchface.data.source.WFAssetRemote$Ei
            r0.<init>(r8, r9)
            goto L_0x0014
        L_0x0045:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004d:
            com.fossil.El7.b(r1)
            com.portfolio.platform.watchface.data.source.WFAssetRemote$Fi r1 = new com.portfolio.platform.watchface.data.source.WFAssetRemote$Fi
            r1.<init>(r8, r4)
            r0.L$0 = r8
            r0.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r1, r0)
            if (r0 != r2) goto L_0x0028
            r0 = r2
            goto L_0x003e
        L_0x0061:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x007e
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x003e
        L_0x007e:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.data.source.WFAssetRemote.d(com.mapped.Xe6):java.lang.Object");
    }
}
