package com.portfolio.platform.watchface.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.L77;
import com.fossil.P77;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.watchface.data.WFAssetsLocal;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WFAssetRepository {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ WFAssetsLocal b;
    @DexIgnore
    public /* final */ WFAssetRemote c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.data.source.WFAssetRepository", f = "WFAssetRepository.kt", l = {24, 30, 34}, m = "fetchBackgroundTemplates")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(WFAssetRepository wFAssetRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFAssetRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.data.source.WFAssetRepository", f = "WFAssetRepository.kt", l = {82, 90}, m = "fetchRings")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(WFAssetRepository wFAssetRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFAssetRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.data.source.WFAssetRepository", f = "WFAssetRepository.kt", l = {53, 60, 64}, m = "fetchTickers")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(WFAssetRepository wFAssetRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = wFAssetRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(this);
        }
    }

    @DexIgnore
    public WFAssetRepository(WFAssetsLocal wFAssetsLocal, WFAssetRemote wFAssetRemote) {
        Wg6.c(wFAssetsLocal, "local");
        Wg6.c(wFAssetRemote, "remote");
        this.b = wFAssetsLocal;
        this.c = wFAssetRemote;
        String simpleName = WFAssetRepository.class.getSimpleName();
        Wg6.b(simpleName, "WFAssetRepository::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public final Object a(L77 l77, String str, Xe6<? super P77> xe6) {
        return this.b.a(l77, str, xe6);
    }

    @DexIgnore
    public final Object b(Xe6<? super List<P77>> xe6) {
        return this.b.b(L77.BACKGROUND_TEMPLATE, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0070 A[LOOP:0: B:18:0x006a->B:20:0x0070, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.P77>>> r12) {
        /*
        // Method dump skipped, instructions count: 349
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.data.source.WFAssetRepository.c(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.P77>>> r11) {
        /*
        // Method dump skipped, instructions count: 288
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.data.source.WFAssetRepository.d(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0070 A[LOOP:0: B:18:0x006a->B:20:0x0070, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.P77>>> r12) {
        /*
        // Method dump skipped, instructions count: 349
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.data.source.WFAssetRepository.e(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object f(Xe6<? super List<P77>> xe6) {
        return this.b.b(L77.RING_COMPLICATION, xe6);
    }

    @DexIgnore
    public final Object g(Xe6<? super List<P77>> xe6) {
        return this.b.b(L77.STICKER, xe6);
    }
}
