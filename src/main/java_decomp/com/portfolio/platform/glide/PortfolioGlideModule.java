package com.portfolio.platform.glide;

import android.content.Context;
import com.fossil.Oa1;
import com.fossil.Pj5;
import com.fossil.Qj5;
import com.fossil.Rj5;
import com.fossil.Sj5;
import com.fossil.Ua1;
import com.fossil.Xj5;
import com.fossil.Yj5;
import com.mapped.Uy;
import com.mapped.Wg6;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioGlideModule extends Uy {
    @DexIgnore
    @Override // com.fossil.Pi1, com.fossil.Ri1
    public void b(Context context, Oa1 oa1, Ua1 ua1) {
        Wg6.c(context, "context");
        Wg6.c(oa1, "glide");
        Wg6.c(ua1, "registry");
        super.b(context, oa1, ua1);
        ua1.d(Qj5.class, InputStream.class, new Pj5.Bi());
        ua1.d(Sj5.class, InputStream.class, new Rj5.Bi());
        ua1.d(Yj5.class, InputStream.class, new Xj5.Bi());
    }

    @DexIgnore
    @Override // com.mapped.Uy
    public boolean c() {
        return false;
    }
}
