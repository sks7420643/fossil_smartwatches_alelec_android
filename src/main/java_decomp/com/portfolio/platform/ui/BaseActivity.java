package com.portfolio.platform.ui;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.fossil.Bw7;
import com.fossil.D57;
import com.fossil.Gu5;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Pn5;
import com.fossil.V78;
import com.fossil.Xk0;
import com.fossil.Xq0;
import com.mapped.AlertDialogFragment;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.ServiceUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.service.MFDeviceService;
import java.lang.reflect.Method;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"Registered"})
public class BaseActivity extends AppCompatActivity implements AlertDialogFragment.Gi, V78.Ai {
    @DexIgnore
    public static IButtonConnectivity y;
    @DexIgnore
    public static /* final */ Ai z; // = new Ai(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public long e;
    @DexIgnore
    public /* final */ Handler f; // = new Handler();
    @DexIgnore
    public View g;
    @DexIgnore
    public TextView h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public UserRepository j;
    @DexIgnore
    public An4 k;
    @DexIgnore
    public DeviceRepository l;
    @DexIgnore
    public MigrationManager m;
    @DexIgnore
    public Gu5 s;
    @DexIgnore
    public String t;
    @DexIgnore
    public D57 u;
    @DexIgnore
    public /* final */ Di v; // = new Di(this);
    @DexIgnore
    public /* final */ Ei w; // = new Ei(this);
    @DexIgnore
    public /* final */ Runnable x; // = new Bi(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final IButtonConnectivity a() {
            return BaseActivity.y;
        }

        @DexIgnore
        public final void b(IButtonConnectivity iButtonConnectivity) {
            BaseActivity.y = iButtonConnectivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements CoroutineUseCase.Ei<Gu5.Ei, Gu5.Bi> {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Aii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void a(Gu5.Bi bi) {
                b(bi);
            }

            @DexIgnore
            public void b(Gu5.Bi bi) {
                Wg6.c(bi, "errorValue");
                this.a.b.K("Disconnected");
            }

            @DexIgnore
            public void c(Gu5.Ei ei) {
                Wg6.c(ei, "responseValue");
                this.a.b.K(String.valueOf(ei.a()));
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void onSuccess(Gu5.Ei ei) {
                c(ei);
            }
        }

        @DexIgnore
        public Bi(BaseActivity baseActivity) {
            this.b = baseActivity;
        }

        @DexIgnore
        public final void run() {
            if (!TextUtils.isEmpty(this.b.t)) {
                Gu5 o = this.b.o();
                String str = this.b.t;
                if (str != null) {
                    o.e(new Gu5.Di(str), new Aii(this));
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity b;

        @DexIgnore
        public Ci(BaseActivity baseActivity) {
            this.b = baseActivity;
        }

        @DexIgnore
        public final void run() {
            Fragment Z;
            try {
                if (this.b.u == null && (Z = this.b.getSupportFragmentManager().Z("ProgressDialogFragment")) != null) {
                    this.b.u = (D57) Z;
                }
                if (this.b.u != null) {
                    FLogger.INSTANCE.getLocal().d("ProgressDialogFragment", "hideLoadingDialog dismissAllowingStateLoss");
                    D57 d57 = this.b.u;
                    if (d57 != null) {
                        d57.dismissAllowingStateLoss();
                        this.b.u = null;
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String r = this.b.r();
                local.d(r, "Exception when dismiss progress dialog=" + e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements ServiceConnection {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.ui.BaseActivity$mButtonServiceConnection$1$onServiceConnected$1", f = "BaseActivity.kt", l = {141, 143}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:21:0x005e  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 0
                    r2 = 2
                    r4 = 1
                    java.lang.Object r1 = com.fossil.Yn7.d()
                    int r0 = r6.label
                    if (r0 == 0) goto L_0x0060
                    if (r0 == r4) goto L_0x0044
                    if (r0 != r2) goto L_0x003c
                    java.lang.Object r0 = r6.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r7)     // Catch:{ Exception -> 0x007f }
                    r0 = r7
                L_0x0017:
                    com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0     // Catch:{ Exception -> 0x007f }
                    if (r0 == 0) goto L_0x0032
                    com.portfolio.platform.ui.BaseActivity$Ai r1 = com.portfolio.platform.ui.BaseActivity.z     // Catch:{ Exception -> 0x007f }
                    com.misfit.frameworks.buttonservice.IButtonConnectivity r1 = r1.a()     // Catch:{ Exception -> 0x007f }
                    if (r1 == 0) goto L_0x0032
                    com.portfolio.platform.ui.BaseActivity$Ai r1 = com.portfolio.platform.ui.BaseActivity.z     // Catch:{ Exception -> 0x007f }
                    com.misfit.frameworks.buttonservice.IButtonConnectivity r1 = r1.a()     // Catch:{ Exception -> 0x007f }
                    if (r1 == 0) goto L_0x007b
                    java.lang.String r0 = r0.getUserId()     // Catch:{ Exception -> 0x007f }
                    r1.updateUserId(r0)     // Catch:{ Exception -> 0x007f }
                L_0x0032:
                    com.portfolio.platform.ui.BaseActivity$Di r0 = r6.this$0     // Catch:{ Exception -> 0x007f }
                    com.portfolio.platform.ui.BaseActivity r0 = r0.a     // Catch:{ Exception -> 0x007f }
                    r0.z()     // Catch:{ Exception -> 0x007f }
                L_0x0039:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x003b:
                    return r0
                L_0x003c:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0044:
                    java.lang.Object r0 = r6.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r7)
                L_0x004b:
                    com.portfolio.platform.ui.BaseActivity$Di r2 = r6.this$0
                    com.portfolio.platform.ui.BaseActivity r2 = r2.a
                    com.portfolio.platform.data.source.UserRepository r2 = r2.q()
                    r6.L$0 = r0
                    r0 = 2
                    r6.label = r0
                    java.lang.Object r0 = r2.getCurrentUser(r6)
                    if (r0 != r1) goto L_0x0017
                    r0 = r1
                    goto L_0x003b
                L_0x0060:
                    com.fossil.El7.b(r7)
                    com.mapped.Il6 r0 = r6.p$
                    com.portfolio.platform.PortfolioApp$inner r2 = com.portfolio.platform.PortfolioApp.get
                    com.portfolio.platform.ui.BaseActivity$Ai r3 = com.portfolio.platform.ui.BaseActivity.z
                    com.misfit.frameworks.buttonservice.IButtonConnectivity r3 = r3.a()
                    if (r3 == 0) goto L_0x00a6
                    r6.L$0 = r0
                    r6.label = r4
                    java.lang.Object r2 = r2.m(r3, r6)
                    if (r2 != r1) goto L_0x004b
                    r0 = r1
                    goto L_0x003b
                L_0x007b:
                    com.mapped.Wg6.i()
                    throw r5
                L_0x007f:
                    r0 = move-exception
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                    com.portfolio.platform.ui.BaseActivity$Di r2 = r6.this$0
                    com.portfolio.platform.ui.BaseActivity r2 = r2.a
                    java.lang.String r2 = r2.r()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r4 = ".onServiceConnected(), ex="
                    r3.append(r4)
                    r3.append(r0)
                    java.lang.String r3 = r3.toString()
                    r1.e(r2, r3)
                    r0.printStackTrace()
                    goto L_0x0039
                L_0x00a6:
                    com.mapped.Wg6.i()
                    throw r5
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.BaseActivity.Di.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di(BaseActivity baseActivity) {
            this.a = baseActivity;
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Wg6.c(componentName, "name");
            Wg6.c(iBinder, Constants.SERVICE);
            FLogger.INSTANCE.getLocal().d(this.a.r(), "Button service connected");
            BaseActivity.z.b(IButtonConnectivity.Stub.asInterface(iBinder));
            this.a.B(true);
            Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Aii(this, null), 3, null);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            Wg6.c(componentName, "name");
            FLogger.INSTANCE.getLocal().d(this.a.r(), "Button service disconnected");
            this.a.B(false);
            BaseActivity.z.b(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements ServiceConnection {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei(BaseActivity baseActivity) {
            this.a = baseActivity;
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Wg6.c(componentName, "name");
            Wg6.c(iBinder, Constants.SERVICE);
            FLogger.INSTANCE.getLocal().d(this.a.r(), "Misfit service connected");
            MFDeviceService.b bVar = (MFDeviceService.b) iBinder;
            this.a.E(bVar.a());
            PortfolioApp.get.n(bVar);
            this.a.C(true);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            Wg6.c(componentName, "name");
            this.a.C(false);
            this.a.E(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity b;

        @DexIgnore
        public Fi(BaseActivity baseActivity) {
            this.b = baseActivity;
        }

        @DexIgnore
        public final void run() {
            FragmentManager supportFragmentManager = this.b.getSupportFragmentManager();
            Wg6.b(supportFragmentManager, "supportFragmentManager");
            if (supportFragmentManager.d0() > 1) {
                this.b.getSupportFragmentManager().H0();
            } else {
                this.b.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ BaseActivity b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        public Gi(BaseActivity baseActivity, String str, boolean z) {
            this.b = baseActivity;
            this.c = str;
            this.d = z;
        }

        @DexIgnore
        public final void run() {
            try {
                if (this.b.isDestroyed() || this.b.isFinishing()) {
                    FLogger.INSTANCE.getLocal().d(this.b.r(), "Activity is destroy or finishing, no need to show dialog");
                    return;
                }
                this.b.u = D57.d.a(this.c);
                D57 d57 = this.b.u;
                if (d57 != null) {
                    d57.setCancelable(this.d);
                    Xq0 j = this.b.getSupportFragmentManager().j();
                    Wg6.b(j, "supportFragmentManager.beginTransaction()");
                    D57 d572 = this.b.u;
                    if (d572 != null) {
                        j.d(d572, "ProgressDialogFragment");
                        j.i();
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String r = this.b.r();
                local.d(r, "Exception when showing progress dialog=" + e);
            }
        }
    }

    @DexIgnore
    public BaseActivity() {
        String simpleName = getClass().getSimpleName();
        Wg6.b(simpleName, "this.javaClass.simpleName");
        this.b = simpleName;
    }

    @DexIgnore
    public static /* synthetic */ void I(BaseActivity baseActivity, boolean z2, String str, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                str = "";
            }
            baseActivity.H(z2, str);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: showLoadingDialog");
    }

    @DexIgnore
    public final void A() {
        View view = this.g;
        if (view != null && this.i) {
            if (view != null) {
                ViewParent parent = view.getParent();
                if (parent != null) {
                    ((ViewGroup) parent).removeView(this.g);
                    this.i = false;
                    this.f.removeCallbacks(this.x);
                } else {
                    throw new Rc6("null cannot be cast to non-null type android.view.ViewGroup");
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
        try {
            Gu5 gu5 = this.s;
            if (gu5 != null) {
                gu5.s();
            } else {
                Wg6.n("mGetRssi");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void B(boolean z2) {
        this.d = z2;
    }

    @DexIgnore
    public final void C(boolean z2) {
        this.c = z2;
    }

    @DexIgnore
    @Override // com.fossil.V78.Ai
    public void C4(int i2, List<String> list) {
        Wg6.c(list, "perms");
    }

    @DexIgnore
    public final void D(long j2) {
        this.e = j2;
    }

    @DexIgnore
    public final void E(MFDeviceService mFDeviceService) {
    }

    @DexIgnore
    public final void F() {
        I(this, false, null, 2, null);
    }

    @DexIgnore
    public final void G(String str) {
        Wg6.c(str, "title");
        H(false, str);
    }

    @DexIgnore
    public final void H(boolean z2, String str) {
        Wg6.c(str, "title");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProgressDialogFragment", "showLoadingDialog: cancelable = " + z2);
        t();
        this.f.post(new Gi(this, str, z2));
    }

    @DexIgnore
    public final <T extends Service> void J(Class<? extends T>... clsArr) {
        Wg6.c(clsArr, "services");
        FLogger.INSTANCE.getLocal().d(this.b, "unbindServices()");
        for (Class<? extends T> cls : clsArr) {
            if (Wg6.a(cls, MFDeviceService.class)) {
                if (this.c) {
                    FLogger.INSTANCE.getLocal().d(this.b, "Unbinding from mIsMisfitServiceBound");
                    ServiceUtils.a.g(this, this.w);
                    this.c = false;
                }
            } else if (Wg6.a(cls, ButtonService.class) && this.d) {
                FLogger.INSTANCE.getLocal().d(this.b, "Unbinding from mButtonServiceBound");
                ServiceUtils.a.g(this, this.v);
                this.d = false;
            }
        }
    }

    @DexIgnore
    public final void K(String str) {
        Wg6.c(str, "rssiInfo");
        TextView textView = this.h;
        if (textView != null) {
            textView.setText(str);
            this.f.postDelayed(this.x, 1000);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.b;
        local.d(str2, "Inside .onDialogFragmentResult tag=" + str);
        if (str.hashCode() == 2009556792 && str.equals(InAppPermission.NOTIFICATION_ACCESS) && i2 == 2131362442) {
            w();
        }
    }

    @DexIgnore
    public void finish() {
        super.finish();
        if (u()) {
            overridePendingTransition(2130772013, 2130772016);
        }
    }

    @DexIgnore
    public final void i(Fragment fragment, int i2) {
        Wg6.c(fragment, "fragment");
        k(fragment, null, i2);
    }

    @DexIgnore
    public final void j(Fragment fragment, String str) {
        Wg6.c(fragment, "fragment");
        Wg6.c(str, "tag");
        k(fragment, str, 2131362158);
    }

    @DexIgnore
    public final void k(Fragment fragment, String str, int i2) {
        Wg6.c(fragment, "fragment");
        Xq0 j2 = getSupportFragmentManager().j();
        j2.s(i2, fragment, str);
        j2.f(str);
        j2.i();
    }

    @DexIgnore
    @Override // com.fossil.V78.Ai
    public void k1(int i2, List<String> list) {
        Wg6.c(list, "perms");
    }

    @DexIgnore
    @TargetApi(23)
    public final void l(boolean z2) {
        if (Build.VERSION.SDK_INT >= 23) {
            Window window = getWindow();
            window.clearFlags(67108864);
            window.addFlags(RecyclerView.UNDEFINED_DURATION);
            Wg6.b(window, "window");
            View decorView = window.getDecorView();
            Wg6.b(decorView, "window.decorView");
            int systemUiVisibility = decorView.getSystemUiVisibility();
            if (z2) {
                decorView.setSystemUiVisibility(systemUiVisibility & -8193);
            } else {
                decorView.setSystemUiVisibility(systemUiVisibility | 8192);
            }
        }
    }

    @DexIgnore
    public <T extends Service> void m(Class<? extends T>... clsArr) {
        Wg6.c(clsArr, "services");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.b;
        local.d(str, "Service Tracking - startAndBindService " + clsArr);
        int length = clsArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            Class<? extends T> cls = clsArr[i2];
            if (Wg6.a(cls, MFDeviceService.class)) {
                ServiceUtils.a.c(this, MFDeviceService.class, this.w, 0);
            } else if (Wg6.a(cls, ButtonService.class)) {
                ServiceUtils.a.c(this, ButtonService.class, this.v, 1);
            }
        }
    }

    @DexIgnore
    public final DeviceRepository n() {
        DeviceRepository deviceRepository = this.l;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        Wg6.n("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final Gu5 o() {
        Gu5 gu5 = this.s;
        if (gu5 != null) {
            return gu5;
        }
        Wg6.n("mGetRssi");
        throw null;
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity
    public void onBackPressed() {
        runOnUiThread(new Fi(this));
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onCreate(Bundle bundle) {
        PortfolioApp.get.instance().getIface().u1(this);
        super.onCreate(bundle);
        if (u()) {
            overridePendingTransition(2130772014, 2130772015);
        }
        Pn5.o.a().p();
        Window window = getWindow();
        Wg6.b(window, "window");
        View decorView = window.getDecorView();
        Wg6.b(decorView, "window.decorView");
        decorView.setSystemUiVisibility(3328);
        AnalyticsHelper.f.g();
    }

    @DexIgnore
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Wg6.c(menuItem, "item");
        if (menuItem.getItemId() == 16908332) {
            v();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onPause() {
        super.onPause();
        try {
            PortfolioApp.get.l(this);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.b;
            local.e(str, "Inside " + this.b + ".onPause - exception=" + e2);
        }
        if (!PortfolioApp.get.instance().z0()) {
            An4 an4 = this.k;
            if (an4 == null) {
                Wg6.n("mSharePrefs");
                throw null;
            } else if (an4.i0()) {
                A();
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onResume() {
        super.onResume();
        PortfolioApp.get.h(this);
        l(false);
        if (PortfolioApp.get.f()) {
            Pn5.o.a().p();
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onStart() {
        super.onStart();
        FLogger.INSTANCE.getLocal().d(this.b, "onStart()");
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d(this.b, "onStop()");
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        super.onTrimMemory(i2);
        System.runFinalization();
    }

    @DexIgnore
    public final long p() {
        return this.e;
    }

    @DexIgnore
    public final UserRepository q() {
        UserRepository userRepository = this.j;
        if (userRepository != null) {
            return userRepository;
        }
        Wg6.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final String r() {
        return this.b;
    }

    @DexIgnore
    public final int s() {
        try {
            Method method = Context.class.getMethod("getThemeResId", new Class[0]);
            Wg6.b(method, "currentClass.getMethod(\"getThemeResId\")");
            method.setAccessible(true);
            Object invoke = method.invoke(this, new Object[0]);
            if (invoke != null) {
                return ((Integer) invoke).intValue();
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Int");
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d(this.b, "Failed to get theme resource ID");
            return 0;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void startActivityForResult(Intent intent, int i2) {
        if (intent == null) {
            intent = new Intent();
        }
        super.startActivityForResult(intent, i2);
    }

    @DexIgnore
    public final void t() {
        FLogger.INSTANCE.getLocal().d("ProgressDialogFragment", "hideLoadingDialog");
        this.f.post(new Ci(this));
    }

    @DexIgnore
    public final boolean u() {
        return s() == 2131951631;
    }

    @DexIgnore
    public final void v() {
        try {
            Xk0.e(this);
        } catch (IllegalArgumentException e2) {
            finish();
        }
    }

    @DexIgnore
    public final void w() {
        startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
    }

    @DexIgnore
    public final void x() {
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, getPackageName(), null));
        startActivity(intent);
    }

    @DexIgnore
    public final void y() {
        if (Build.VERSION.SDK_INT >= 29) {
            startActivity(new Intent("android.settings.panel.action.INTERNET_CONNECTIVITY"));
        } else {
            startActivity(new Intent("android.settings.SETTINGS"));
        }
    }

    @DexIgnore
    public final void z() {
        synchronized (this) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.b;
            StringBuilder sb = new StringBuilder();
            sb.append("needToUpdateBLEWhenUpgradeLegacy - isNeedToUpdateBLE=");
            An4 an4 = this.k;
            if (an4 != null) {
                sb.append(an4.o0());
                local.d(str, sb.toString());
                An4 an42 = this.k;
                if (an42 == null) {
                    Wg6.n("mSharePrefs");
                    throw null;
                } else if (an42.o0()) {
                    try {
                        MigrationManager migrationManager = this.m;
                        if (migrationManager != null) {
                            migrationManager.w();
                        } else {
                            Wg6.n("mMigrationManager");
                            throw null;
                        }
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = this.b;
                        local2.e(str2, "needToUpdateBLEWhenUpgradeLegacy - e=" + e2);
                    }
                }
            } else {
                Wg6.n("mSharePrefs");
                throw null;
            }
        }
    }
}
