package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UnlinkDeviceUseCase extends CoroutineUseCase<Bi, Di, Ci> {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public Bi d;
    @DexIgnore
    public /* final */ DeviceRepository e;
    @DexIgnore
    public /* final */ HybridPresetRepository f;
    @DexIgnore
    public /* final */ PortfolioApp g;
    @DexIgnore
    public /* final */ WatchFaceRepository h;
    @DexIgnore
    public /* final */ DianaPresetRepository i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return UnlinkDeviceUseCase.j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Bi(String str) {
            Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public Ci(String str, ArrayList<Integer> arrayList, String str2) {
            Wg6.c(str, "lastErrorCode");
            this.a = str;
            this.b = arrayList;
            this.c = str2;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$1", f = "UnlinkDeviceUseCase.kt", l = {60, 66, 68}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UnlinkDeviceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(UnlinkDeviceUseCase unlinkDeviceUseCase, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = unlinkDeviceUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0068  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x007f  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0117  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 366
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase.Ei.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = UnlinkDeviceUseCase.class.getSimpleName();
        Wg6.b(simpleName, "UnlinkDeviceUseCase::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public UnlinkDeviceUseCase(DeviceRepository deviceRepository, HybridPresetRepository hybridPresetRepository, com.portfolio.platform.data.source.DianaPresetRepository dianaPresetRepository, PortfolioApp portfolioApp, WatchFaceRepository watchFaceRepository, DianaPresetRepository dianaPresetRepository2) {
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(hybridPresetRepository, "mHybridPresetRepository");
        Wg6.c(dianaPresetRepository, "mDianaPresetRepository");
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(watchFaceRepository, "watchFaceRepository");
        Wg6.c(dianaPresetRepository2, "dianaPresetRepository");
        this.e = deviceRepository;
        this.f = hybridPresetRepository;
        this.g = portfolioApp;
        this.h = watchFaceRepository;
        this.i = dianaPresetRepository2;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return j;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return t(bi, xe6);
    }

    @DexIgnore
    public final Rm6 s() {
        return Gu7.d(g(), null, null, new Ei(this, null), 3, null);
    }

    @DexIgnore
    public Object t(Bi bi, Xe6<Object> xe6) {
        FLogger.INSTANCE.getLocal().d(j, "running UseCase");
        if (bi == null) {
            return new Ci("", null, "");
        }
        this.d = bi;
        String a2 = bi.a();
        String J = this.g.J();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "remove device " + a2 + " currentActive " + J);
        s();
        return new Object();
    }
}
