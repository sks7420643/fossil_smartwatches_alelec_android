package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.D26;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jo5;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Lo5;
import com.fossil.Mo5;
import com.fossil.St5;
import com.fossil.Tt5;
import com.fossil.Ul5;
import com.fossil.Ut5;
import com.fossil.V36;
import com.fossil.Vt7;
import com.fossil.Yn7;
import com.fossil.Zh5;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.U40;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaSyncUseCase extends CoroutineUseCase<Tt5, Ut5, St5> {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ Ai t; // = new Ai(null);
    @DexIgnore
    public /* final */ Bi d; // = new Bi(this);
    @DexIgnore
    public /* final */ An4 e;
    @DexIgnore
    public /* final */ V36 f;
    @DexIgnore
    public /* final */ PortfolioApp g;
    @DexIgnore
    public /* final */ DeviceRepository h;
    @DexIgnore
    public /* final */ FileRepository i;
    @DexIgnore
    public /* final */ DianaPresetRepository j;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase k;
    @DexIgnore
    public /* final */ D26 l;
    @DexIgnore
    public /* final */ VerifySecretKeyUseCase m;
    @DexIgnore
    public /* final */ UpdateFirmwareUsecase n;
    @DexIgnore
    public /* final */ AnalyticsHelper o;
    @DexIgnore
    public /* final */ AlarmsRepository p;
    @DexIgnore
    public /* final */ DianaAppSettingRepository q;
    @DexIgnore
    public /* final */ WorkoutSettingRepository r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return DianaSyncUseCase.s;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements BleCommandResultManager.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncUseCase a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(DianaSyncUseCase dianaSyncUseCase) {
            this.a = dianaSyncUseCase;
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && Vt7.j(stringExtra, this.a.g.J(), true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                if (intExtra == 1) {
                    FLogger.INSTANCE.getLocal().d(DianaSyncUseCase.t.a(), "sync success, remove device now");
                    BleCommandResultManager.d.j(this, CommunicateMode.SYNC);
                    this.a.j(new Ut5());
                } else if (intExtra == 2 || intExtra == 4) {
                    BleCommandResultManager.d.j(this, CommunicateMode.SYNC);
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>();
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = DianaSyncUseCase.t.a();
                    local.d(a2, "sync fail due to " + intExtra2);
                    if (intExtra2 != 1101) {
                        if (intExtra2 == 1603) {
                            this.a.i(new St5(Zh5.FAIL_DUE_TO_USER_DENY_STOP_WORKOUT, null));
                            return;
                        } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                            this.a.i(new St5(Zh5.FAIL_DUE_TO_SYNC_FAIL, null));
                            return;
                        }
                    }
                    this.a.i(new St5(Zh5.FAIL_DUE_TO_LACK_PERMISSION, integerArrayListExtra));
                } else if (intExtra == 5) {
                    FLogger.INSTANCE.getLocal().d(DianaSyncUseCase.t.a(), "sync pending due to workout");
                    this.a.i(new St5(Zh5.FAIL_DUE_TO_PENDING_WORKOUT, null));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase", f = "DianaSyncUseCase.kt", l = {278, Action.Selfie.SELFIE_END_ACTION, 306}, m = "removeBuddyChallengeOnAllPresets")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(DianaSyncUseCase dianaSyncUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = dianaSyncUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.t(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$removeBuddyChallengeOnAllPresets$3", f = "DianaSyncUseCase.kt", l = {306}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<Mo5>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $presets;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(DianaSyncUseCase dianaSyncUseCase, List list, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaSyncUseCase;
            this.$presets = list;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$presets, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Kz4<List<Mo5>>> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                DianaPresetRepository dianaPresetRepository = this.this$0.j;
                List<Jo5> f = Lo5.f(this.$presets, this.this$0.i);
                this.L$0 = il6;
                this.label = 1;
                Object r = dianaPresetRepository.r(f, this);
                return r == d ? d : r;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$removeBuddyChallengeOnAllPresets$presets$1", f = "DianaSyncUseCase.kt", l = {278}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Mo5>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(DianaSyncUseCase dianaSyncUseCase, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaSyncUseCase;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$serial, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super List<? extends Mo5>> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                DianaPresetRepository dianaPresetRepository = this.this$0.j;
                String str = this.$serial;
                this.L$0 = il6;
                this.label = 1;
                Object o = dianaPresetRepository.o(str, this);
                return o == d ? d : o;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase", f = "DianaSyncUseCase.kt", l = {110, 112, 113, 155, 165, DateTimeConstants.HOURS_PER_WEEK, 169, 176, 180}, m = "run")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(DianaSyncUseCase dianaSyncUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = dianaSyncUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$run$isAA$1", f = "DianaSyncUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        public Gi(Xe6 xe6) {
            super(2, xe6);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return Ao7.a(SoLibraryLoader.f().c(PortfolioApp.get.instance()) != null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase", f = "DianaSyncUseCase.kt", l = {240}, m = "setRuleNotificationFilterToDevice")
    public static final class Hi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(DianaSyncUseCase dianaSyncUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = dianaSyncUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.v(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements CoroutineUseCase.Ei<VerifySecretKeyUseCase.Ci, VerifySecretKeyUseCase.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncUseCase a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;
        @DexIgnore
        public /* final */ /* synthetic */ UserProfile e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements CoroutineUseCase.Ei<UpdateFirmwareUsecase.Di, UpdateFirmwareUsecase.Ci> {
            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void a(UpdateFirmwareUsecase.Ci ci) {
                b(ci);
            }

            @DexIgnore
            public void b(UpdateFirmwareUsecase.Ci ci) {
                Wg6.c(ci, "errorValue");
            }

            @DexIgnore
            public void c(UpdateFirmwareUsecase.Di di) {
                Wg6.c(di, "responseValue");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void onSuccess(UpdateFirmwareUsecase.Di di) {
                c(di);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1$onSuccess$1", f = "DianaSyncUseCase.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ii this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ii ii, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ii;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    Ii ii = this.this$0;
                    ii.a.w(ii.c, ii.d, ii.e, ii.b);
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public Ii(DianaSyncUseCase dianaSyncUseCase, String str, int i, boolean z, UserProfile userProfile) {
            this.a = dianaSyncUseCase;
            this.b = str;
            this.c = i;
            this.d = z;
            this.e = userProfile;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(VerifySecretKeyUseCase.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(VerifySecretKeyUseCase.Bi bi) {
            Wg6.c(bi, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = DianaSyncUseCase.t.a();
            local.d(a2, "verify secret key fail, stop sync " + bi.a());
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String str = this.b;
            String a3 = DianaSyncUseCase.t.a();
            remote.i(component, session, str, a3, "[Sync] Verify secret key failed by " + bi.a());
            if (bi.a() == U40.REQUEST_UNSUPPORTED.getCode() && this.e.getOriginalSyncMode() == 12) {
                this.a.n.e(new UpdateFirmwareUsecase.Bi(this.b, true), new Aii());
            }
            this.a.s(this.e.getOriginalSyncMode(), this.b, 2);
        }

        @DexIgnore
        public void c(VerifySecretKeyUseCase.Ci ci) {
            Wg6.c(ci, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = DianaSyncUseCase.t.a();
            local.d(a2, "secret key " + ci.a() + ", start sync");
            FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.b, DianaSyncUseCase.t.a(), "[Sync] Verify secret key success, start sync");
            PortfolioApp.get.instance().I1(ci.a(), this.b);
            Rm6 unused = Gu7.d(this.a.g(), null, null, new Bii(this, null), 3, null);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(VerifySecretKeyUseCase.Ci ci) {
            c(ci);
        }
    }

    /*
    static {
        String simpleName = DianaSyncUseCase.class.getSimpleName();
        Wg6.b(simpleName, "DianaSyncUseCase::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public DianaSyncUseCase(An4 an4, V36 v36, PortfolioApp portfolioApp, DeviceRepository deviceRepository, FileRepository fileRepository, DianaPresetRepository dianaPresetRepository, NotificationSettingsDatabase notificationSettingsDatabase, D26 d26, VerifySecretKeyUseCase verifySecretKeyUseCase, UpdateFirmwareUsecase updateFirmwareUsecase, AnalyticsHelper analyticsHelper, AlarmsRepository alarmsRepository, DianaAppSettingRepository dianaAppSettingRepository, WorkoutSettingRepository workoutSettingRepository) {
        Wg6.c(an4, "mSharedPrefs");
        Wg6.c(v36, "mGetApps");
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(fileRepository, "mFileRepository");
        Wg6.c(dianaPresetRepository, "mDianaPresetRepository");
        Wg6.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        Wg6.c(d26, "mGetAllContactGroup");
        Wg6.c(verifySecretKeyUseCase, "mVerifySecretKeyUseCase");
        Wg6.c(updateFirmwareUsecase, "mUpdateFirmwareUseCase");
        Wg6.c(analyticsHelper, "mAnalyticsHelper");
        Wg6.c(alarmsRepository, "mAlarmsRepository");
        Wg6.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        Wg6.c(workoutSettingRepository, "mWorkoutSettingRepository");
        this.e = an4;
        this.f = v36;
        this.g = portfolioApp;
        this.h = deviceRepository;
        this.i = fileRepository;
        this.j = dianaPresetRepository;
        this.k = notificationSettingsDatabase;
        this.l = d26;
        this.m = verifySecretKeyUseCase;
        this.n = updateFirmwareUsecase;
        this.o = analyticsHelper;
        this.p = alarmsRepository;
        this.q = dianaAppSettingRepository;
        this.r = workoutSettingRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return s;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Tt5 tt5, Xe6 xe6) {
        return u(tt5, xe6);
    }

    @DexIgnore
    public final void s(int i2, String str, int i3) {
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String h2 = h();
        local.d(h2, "broadcastSyncStatus serial=" + str + ' ' + i3);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", i3);
        intent.putExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), i2);
        intent.putExtra("SERIAL", str);
        BleCommandResultManager bleCommandResultManager = BleCommandResultManager.d;
        CommunicateMode communicateMode = CommunicateMode.SYNC;
        bleCommandResultManager.h(communicateMode, new BleCommandResultManager.Ai(communicateMode, str, intent));
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v26, types: [java.util.List] */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0134, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().e(com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase.s, "removeBuddyChallengeOnAllPresets - start reset active watchApp");
        r4 = new com.google.gson.Gson();
        r12 = r9.q;
        r11.L$0 = r9;
        r11.L$1 = r8;
        r11.L$2 = r7;
        r11.L$3 = r13;
        r11.L$4 = r5;
        r11.L$5 = r14;
        r11.L$6 = r3;
        r11.L$7 = r2;
        r11.L$8 = r10;
        r11.L$9 = r6;
        r11.L$10 = r4;
        r11.L$11 = r6;
        r11.label = 2;
        r10 = r12.getAllDianaAppSettings("watch_app", r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x016b, code lost:
        if (r10 != r15) goto L_0x019f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x019f, code lost:
        r3 = r4;
        r12 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        return r15;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0174  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object t(java.lang.String r19, com.mapped.Xe6<? super com.mapped.Cd6> r20) {
        /*
        // Method dump skipped, instructions count: 419
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase.t(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:103:0x03fa  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x03fe  */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x059b  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x05a2  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x05b2  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x05b5  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x05db  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x05e9  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x05ee  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0066 A[Catch:{ Exception -> 0x00ac }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071 A[Catch:{ Exception -> 0x00ac }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0126 A[Catch:{ Exception -> 0x0262 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x015b  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x017e A[Catch:{ Exception -> 0x05cf }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01b2  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01b6  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01ec  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x01fb  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x025e  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x026f  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x02b3 A[SYNTHETIC, Splitter:B:79:0x02b3] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x02e0  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x02fe  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0337  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x036a  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x036e  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0397  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x039b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object u(com.fossil.Tt5 r20, com.mapped.Xe6<java.lang.Object> r21) {
        /*
        // Method dump skipped, instructions count: 1546
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase.u(com.fossil.Tt5, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object v(com.mapped.Xe6<? super com.mapped.Cd6> r9) {
        /*
            r8 = this;
            r7 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase.Hi
            if (r0 == 0) goto L_0x0049
            r0 = r9
            com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$Hi r0 = (com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase.Hi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0049
            int r1 = r1 + r3
            r0.label = r1
            r5 = r0
        L_0x0014:
            java.lang.Object r1 = r5.result
            java.lang.Object r6 = com.fossil.Yn7.d()
            int r0 = r5.label
            if (r0 == 0) goto L_0x0057
            if (r0 != r7) goto L_0x004f
            java.lang.Object r0 = r5.L$0
            com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase r0 = (com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0028:
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings r1 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings
            java.util.List r0 = (java.util.List) r0
            long r2 = java.lang.System.currentTimeMillis()
            r1.<init>(r0, r2)
            com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r0 = r0.instance()
            com.portfolio.platform.PortfolioApp$inner r2 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r2 = r2.instance()
            java.lang.String r2 = r2.J()
            r0.s1(r1, r2)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0048:
            return r0
        L_0x0049:
            com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$Hi r5 = new com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$Hi
            r5.<init>(r8, r9)
            goto L_0x0014
        L_0x004f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0057:
            com.fossil.El7.b(r1)
            com.portfolio.platform.util.NotificationAppHelper r0 = com.portfolio.platform.util.NotificationAppHelper.b
            com.fossil.V36 r1 = r8.f
            com.fossil.D26 r2 = r8.l
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r3 = r8.k
            com.mapped.An4 r4 = r8.e
            r5.L$0 = r8
            r5.label = r7
            java.lang.Object r0 = r0.c(r1, r2, r3, r4, r5)
            if (r0 != r6) goto L_0x0028
            r0 = r6
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase.v(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void w(int i2, boolean z, UserProfile userProfile, String str) {
        Wg6.c(userProfile, "userProfile");
        Wg6.c(str, "serial");
        if (f() != null) {
            BleCommandResultManager.d.e(this.d, CommunicateMode.SYNC);
        }
        userProfile.setNewDevice(z);
        userProfile.setSyncMode(i2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String h2 = h();
        local.d(h2, ".startDeviceSync - syncMode=" + i2);
        if (!this.e.z0()) {
            SKUModel skuModelBySerialPrefix = this.h.getSkuModelBySerialPrefix(DeviceHelper.o.m(str));
            this.e.t1(Boolean.TRUE);
            this.o.o(i2, skuModelBySerialPrefix);
            Ul5 c = AnalyticsHelper.f.c("sync_session");
            AnalyticsHelper.f.a("sync_session", c);
            c.i();
        }
        if (!PortfolioApp.get.instance().T1(str, userProfile)) {
            s(userProfile.getOriginalSyncMode(), str, 2);
        }
    }

    @DexIgnore
    public final void x(int i2, boolean z, UserProfile userProfile, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = s;
        local.d(str2, "verifySecretKey " + str);
        s(userProfile.getOriginalSyncMode(), str, 0);
        this.m.e(new VerifySecretKeyUseCase.Ai(str), new Ii(this, str, i2, z, userProfile));
    }
}
