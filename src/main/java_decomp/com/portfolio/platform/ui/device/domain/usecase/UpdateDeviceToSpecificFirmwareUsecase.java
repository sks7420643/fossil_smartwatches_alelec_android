package com.portfolio.platform.ui.device.domain.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.mapped.An4;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UpdateDeviceToSpecificFirmwareUsecase extends CoroutineUseCase<Ai, Ci, Bi> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ DeviceRepository d;
    @DexIgnore
    public /* final */ An4 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ FirmwareData b;

        @DexIgnore
        public Ai(String str, FirmwareData firmwareData) {
            Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            Wg6.c(firmwareData, "firmwareData");
            this.a = str;
            this.b = firmwareData;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final FirmwareData b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ai {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.UpdateDeviceToSpecificFirmwareUsecase", f = "UpdateDeviceToSpecificFirmwareUsecase.kt", l = {47}, m = "run")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateDeviceToSpecificFirmwareUsecase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(UpdateDeviceToSpecificFirmwareUsecase updateDeviceToSpecificFirmwareUsecase, Xe6 xe6) {
            super(xe6);
            this.this$0 = updateDeviceToSpecificFirmwareUsecase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, this);
        }
    }

    /*
    static {
        String simpleName = UpdateFirmwareUsecase.class.getSimpleName();
        Wg6.b(simpleName, "UpdateFirmwareUsecase::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public UpdateDeviceToSpecificFirmwareUsecase(DeviceRepository deviceRepository, An4 an4) {
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.d = deviceRepository;
        this.e = an4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return f;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ai ai, Xe6 xe6) {
        return m(ai, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0044 A[Catch:{ Exception -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object m(com.portfolio.platform.ui.device.domain.usecase.UpdateDeviceToSpecificFirmwareUsecase.Ai r10, com.mapped.Xe6<java.lang.Object> r11) {
        /*
        // Method dump skipped, instructions count: 376
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.UpdateDeviceToSpecificFirmwareUsecase.m(com.portfolio.platform.ui.device.domain.usecase.UpdateDeviceToSpecificFirmwareUsecase$Ai, com.mapped.Xe6):java.lang.Object");
    }
}
