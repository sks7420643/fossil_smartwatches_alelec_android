package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.El7;
import com.fossil.Gt5;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Q27;
import com.fossil.Ry6;
import com.fossil.Sy6;
import com.fossil.Ty6;
import com.fossil.Uy6;
import com.fossil.Vt7;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.EmptyFirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.misfit.frameworks.buttonservice.model.SkipFirmwareData;
import com.misfit.frameworks.buttonservice.model.pairing.PairingAuthorizeResponse;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.LabelRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LinkDeviceUseCase extends CoroutineUseCase<Hi, Ji, Ii> {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ Ci t; // = new Ci(null);
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public Device f;
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public boolean h;
    @DexIgnore
    public Ki i;
    @DexIgnore
    public /* final */ Gi j; // = new Gi();
    @DexIgnore
    public /* final */ DeviceRepository k;
    @DexIgnore
    public /* final */ DownloadFirmwareByDeviceModelUsecase l;
    @DexIgnore
    public /* final */ An4 m;
    @DexIgnore
    public /* final */ UserRepository n;
    @DexIgnore
    public /* final */ Q27 o;
    @DexIgnore
    public /* final */ Cj4 p;
    @DexIgnore
    public /* final */ LabelRepository q;
    @DexIgnore
    public /* final */ FileRepository r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ji {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Ai(String str) {
            Wg6.c(str, "serial");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ji {
        @DexIgnore
        public /* final */ boolean a;

        @DexIgnore
        public Bi(String str, boolean z) {
            Wg6.c(str, "serial");
            this.a = z;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public /* synthetic */ Ci(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return LinkDeviceUseCase.s;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Ii {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(int i, String str, String str2) {
            super(i, str, str2);
            Wg6.c(str, "deviceId");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends Ii {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(String str, String str2) {
            super(-1, str, str2);
            Wg6.c(str, "deviceId");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends Ji {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Fi(String str) {
            Wg6.c(str, "serial");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Gi implements BleCommandResultManager.Bi {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Gi() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), FailureCode.UNKNOWN_ERROR);
            ServiceActionResult serviceActionResult = ServiceActionResult.values()[intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), ServiceActionResult.UNALLOWED_ACTION.ordinal())];
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LinkDeviceUseCase.t.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + LinkDeviceUseCase.this.B() + ", isSuccess=" + intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1));
            if (!TextUtils.isEmpty(stringExtra) && Vt7.j(stringExtra, LinkDeviceUseCase.this.z(), true) && communicateMode == CommunicateMode.LINK && LinkDeviceUseCase.this.B()) {
                switch (Gt5.a[serviceActionResult.ordinal()]) {
                    case 1:
                        LinkDeviceUseCase linkDeviceUseCase = LinkDeviceUseCase.this;
                        Bundle extras = intent.getExtras();
                        if (extras != null) {
                            String string = extras.getString("device_model", "");
                            Wg6.b(string, "intent.extras!!.getStrin\u2026nstants.DEVICE_MODEL, \"\")");
                            linkDeviceUseCase.K(string);
                            LinkDeviceUseCase linkDeviceUseCase2 = LinkDeviceUseCase.this;
                            if (stringExtra != null) {
                                linkDeviceUseCase2.j(new Ai(stringExtra));
                                LinkDeviceUseCase.this.H();
                                return;
                            }
                            Wg6.i();
                            throw null;
                        }
                        Wg6.i();
                        throw null;
                    case 2:
                        LinkDeviceUseCase linkDeviceUseCase3 = LinkDeviceUseCase.this;
                        if (stringExtra != null) {
                            linkDeviceUseCase3.j(new Fi(stringExtra));
                            return;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    case 3:
                        LinkDeviceUseCase linkDeviceUseCase4 = LinkDeviceUseCase.this;
                        if (stringExtra != null) {
                            linkDeviceUseCase4.j(new Mi(stringExtra, true));
                            return;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    case 4:
                        LinkDeviceUseCase linkDeviceUseCase5 = LinkDeviceUseCase.this;
                        if (stringExtra != null) {
                            linkDeviceUseCase5.j(new Mi(stringExtra, false));
                            return;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    case 5:
                        LinkDeviceUseCase linkDeviceUseCase6 = LinkDeviceUseCase.this;
                        if (stringExtra != null) {
                            linkDeviceUseCase6.j(new Bi(stringExtra, true));
                            return;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    case 6:
                        LinkDeviceUseCase linkDeviceUseCase7 = LinkDeviceUseCase.this;
                        if (stringExtra != null) {
                            linkDeviceUseCase7.j(new Bi(stringExtra, false));
                            return;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    case 7:
                        FLogger.INSTANCE.getLocal().d(LinkDeviceUseCase.t.a(), "onReceive(), ASK_FOR_LINK_SERVER");
                        Bundle extras2 = intent.getExtras();
                        if (extras2 != null) {
                            MisfitDeviceProfile misfitDeviceProfile = (MisfitDeviceProfile) extras2.getParcelable("device");
                            if (misfitDeviceProfile != null) {
                                LinkDeviceUseCase.this.M(misfitDeviceProfile.getDeviceSerial());
                                LinkDeviceUseCase.this.L(misfitDeviceProfile.getAddress());
                                LinkDeviceUseCase.this.J(new Device(misfitDeviceProfile.getDeviceSerial(), misfitDeviceProfile.getAddress(), misfitDeviceProfile.getDeviceModel(), misfitDeviceProfile.getFirmwareVersion(), misfitDeviceProfile.getBatteryLevel(), 50, false, 64, null));
                                Device w = LinkDeviceUseCase.this.w();
                                if (w != null) {
                                    w.appendAdditionalInfo(misfitDeviceProfile);
                                }
                                LinkDeviceUseCase.this.u();
                                return;
                            }
                            LinkDeviceUseCase linkDeviceUseCase8 = LinkDeviceUseCase.this;
                            String z = linkDeviceUseCase8.z();
                            if (z != null) {
                                linkDeviceUseCase8.i(new Di(FailureCode.UNKNOWN_ERROR, z, "No device profile"));
                                PortfolioApp instance = PortfolioApp.get.instance();
                                String z2 = LinkDeviceUseCase.this.z();
                                if (z2 != null) {
                                    instance.s(z2);
                                    return;
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    case 8:
                        FLogger.INSTANCE.getLocal().d(LinkDeviceUseCase.t.a(), "onReceive(), ASK_FOR_LABEL_FILE");
                        LinkDeviceUseCase linkDeviceUseCase9 = LinkDeviceUseCase.this;
                        Wg6.b(stringExtra, "serial");
                        linkDeviceUseCase9.v(stringExtra);
                        return;
                    case 9:
                        LinkDeviceUseCase.this.I(false);
                        PortfolioApp instance2 = PortfolioApp.get.instance();
                        if (stringExtra != null) {
                            instance2.n1(stringExtra, LinkDeviceUseCase.this.y());
                            LinkDeviceUseCase linkDeviceUseCase10 = LinkDeviceUseCase.this;
                            Device w2 = linkDeviceUseCase10.w();
                            if (w2 != null) {
                                linkDeviceUseCase10.j(new Li(w2));
                                return;
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    case 10:
                        LinkDeviceUseCase.this.I(false);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a3 = LinkDeviceUseCase.t.a();
                        local2.d(a3, "Pair device failed due to " + intExtra + ", remove this device on button service");
                        try {
                            PortfolioApp instance3 = PortfolioApp.get.instance();
                            String z3 = LinkDeviceUseCase.this.z();
                            if (z3 != null) {
                                instance3.a1(z3);
                                PortfolioApp instance4 = PortfolioApp.get.instance();
                                String z4 = LinkDeviceUseCase.this.z();
                                if (z4 != null) {
                                    instance4.Z0(z4);
                                    if (intExtra > 1000) {
                                        LinkDeviceUseCase linkDeviceUseCase11 = LinkDeviceUseCase.this;
                                        String z5 = linkDeviceUseCase11.z();
                                        if (z5 != null) {
                                            linkDeviceUseCase11.i(new Di(intExtra, z5, ""));
                                            return;
                                        } else {
                                            Wg6.i();
                                            throw null;
                                        }
                                    } else {
                                        LinkDeviceUseCase linkDeviceUseCase12 = LinkDeviceUseCase.this;
                                        String z6 = linkDeviceUseCase12.z();
                                        if (z6 != null) {
                                            linkDeviceUseCase12.N(new Ki(intExtra, z6, ""));
                                            LinkDeviceUseCase linkDeviceUseCase13 = LinkDeviceUseCase.this;
                                            Ki A = linkDeviceUseCase13.A();
                                            if (A != null) {
                                                linkDeviceUseCase13.i(A);
                                                LinkDeviceUseCase.this.N(null);
                                                return;
                                            }
                                            Wg6.i();
                                            throw null;
                                        }
                                        Wg6.i();
                                        throw null;
                                    }
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } catch (Exception e) {
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String a4 = LinkDeviceUseCase.t.a();
                            local3.d(a4, "Pair device failed, remove this device on button service exception=" + e.getMessage());
                        }
                    default:
                        return;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public Hi(String str, String str2) {
            Wg6.c(str, "device");
            Wg6.c(str2, "macAddress");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ii implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public Ii(int i, String str, String str2) {
            Wg6.c(str, "deviceId");
            this.a = i;
            this.b = str;
            this.c = str2;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ji implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki extends Ii {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(int i, String str, String str2) {
            super(i, str, str2);
            Wg6.c(str, "deviceId");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li extends Ji {
        @DexIgnore
        public /* final */ Device a;

        @DexIgnore
        public Li(Device device) {
            Wg6.c(device, "device");
            this.a = device;
        }

        @DexIgnore
        public final Device a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi extends Ji {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public Mi(String str, boolean z) {
            Wg6.c(str, "serial");
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements CoroutineUseCase.Ei<Uy6, Sy6> {
        @DexIgnore
        public /* final */ /* synthetic */ LinkDeviceUseCase a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ni(LinkDeviceUseCase linkDeviceUseCase) {
            this.a = linkDeviceUseCase;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(Sy6 sy6) {
            b(sy6);
        }

        @DexIgnore
        public void b(Sy6 sy6) {
            Wg6.c(sy6, "errorValue");
            FLogger.INSTANCE.getLocal().d(LinkDeviceUseCase.t.a(), " get device setting fail!!");
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.API;
            FLogger.Session session = FLogger.Session.PAIR;
            String z = this.a.z();
            if (z != null) {
                String a2 = LinkDeviceUseCase.t.a();
                StringBuilder sb = new StringBuilder();
                sb.append("Get device setting of ");
                String z2 = this.a.z();
                if (z2 != null) {
                    sb.append(z2);
                    sb.append(", server error=");
                    sb.append(sy6.a());
                    sb.append(", error = ");
                    sb.append(ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.LINK_DEVICE, ErrorCodeBuilder.Component.APP, ErrorCodeBuilder.AppError.NETWORK_ERROR));
                    remote.e(component, session, z, a2, sb.toString());
                    LinkDeviceUseCase linkDeviceUseCase = this.a;
                    int a3 = sy6.a();
                    String z3 = this.a.z();
                    if (z3 != null) {
                        linkDeviceUseCase.N(new Ki(a3, z3, ""));
                        PortfolioApp instance = PortfolioApp.get.instance();
                        String z4 = this.a.z();
                        if (z4 != null) {
                            instance.M0(z4, PairingResponse.CREATOR.buildPairingLinkServerResponse(false, sy6.a()));
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public void c(Uy6 uy6) {
            Wg6.c(uy6, "responseValue");
            FLogger.INSTANCE.getLocal().d(LinkDeviceUseCase.t.a(), " get device setting success");
            this.a.F();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(Uy6 uy6) {
            c(uy6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$downloadLabelFile$1", f = "LinkDeviceUseCase.kt", l = {285, 294}, m = "invokeSuspend")
    public static final class Oi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LinkDeviceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Oi(LinkDeviceUseCase linkDeviceUseCase, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = linkDeviceUseCase;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Oi oi = new Oi(this.this$0, this.$serial, xe6);
            oi.p$ = (Il6) obj;
            throw null;
            //return oi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Oi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0068  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0129  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 372
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.Oi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Device $deviceModel;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LinkDeviceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Pi(Device device, Xe6 xe6, LinkDeviceUseCase linkDeviceUseCase) {
            super(2, xe6);
            this.$deviceModel = device;
            this.this$0 = linkDeviceUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Pi pi = new Pi(this.$deviceModel, xe6, this.this$0);
            pi.p$ = (Il6) obj;
            throw null;
            //return pi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Pi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x005f  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00c4  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0118  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x021d  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0249  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 760
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.Pi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase", f = "LinkDeviceUseCase.kt", l = {174}, m = "run")
    public static final class Qi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ LinkDeviceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Qi(LinkDeviceUseCase linkDeviceUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = linkDeviceUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.G(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ri implements CoroutineUseCase.Ei<DownloadFirmwareByDeviceModelUsecase.Ci, DownloadFirmwareByDeviceModelUsecase.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ LinkDeviceUseCase a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1", f = "LinkDeviceUseCase.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ DownloadFirmwareByDeviceModelUsecase.Ci $responseValue;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ri this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ri ri, DownloadFirmwareByDeviceModelUsecase.Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ri;
                this.$responseValue = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$responseValue, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    String a2 = this.$responseValue.a();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a3 = LinkDeviceUseCase.t.a();
                    local.d(a3, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + a2);
                    FirmwareData a4 = UpdateFirmwareUsecase.g.a(this.this$0.a.m, this.this$0.a.x());
                    if (a4 == null) {
                        a4 = new EmptyFirmwareData();
                    }
                    PortfolioApp instance = PortfolioApp.get.instance();
                    String z = this.this$0.a.z();
                    if (z != null) {
                        instance.M0(z, PairingResponse.CREATOR.buildPairingUpdateFWResponse(a4));
                        return Cd6.a;
                    }
                    Wg6.i();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ri(LinkDeviceUseCase linkDeviceUseCase) {
            this.a = linkDeviceUseCase;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(DownloadFirmwareByDeviceModelUsecase.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(DownloadFirmwareByDeviceModelUsecase.Bi bi) {
            Wg6.c(bi, "errorValue");
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.API;
            FLogger.Session session = FLogger.Session.PAIR;
            String z = this.a.z();
            if (z != null) {
                String a2 = LinkDeviceUseCase.t.a();
                StringBuilder sb = new StringBuilder();
                sb.append(" downloadFw FAILED!!!, latestFwVersion=");
                String z2 = this.a.z();
                if (z2 != null) {
                    sb.append(z2);
                    sb.append(" but device is DianaEV1!!!");
                    remote.e(component, session, z, a2, sb.toString());
                    FLogger.INSTANCE.getLocal().e(LinkDeviceUseCase.t.a(), "checkFirmware - downloadFw FAILED!!!");
                    LinkDeviceUseCase linkDeviceUseCase = this.a;
                    String z3 = linkDeviceUseCase.z();
                    if (z3 != null) {
                        linkDeviceUseCase.i(new Ei(z3, ""));
                        PortfolioApp instance = PortfolioApp.get.instance();
                        String z4 = this.a.z();
                        if (z4 != null) {
                            instance.s(z4);
                            this.a.I(false);
                            return;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        public void c(DownloadFirmwareByDeviceModelUsecase.Ci ci) {
            Wg6.c(ci, "responseValue");
            Rm6 unused = Gu7.d(this.a.g(), null, null, new Aii(this, ci, null), 3, null);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(DownloadFirmwareByDeviceModelUsecase.Ci ci) {
            c(ci);
        }
    }

    /*
    static {
        String simpleName = LinkDeviceUseCase.class.getSimpleName();
        Wg6.b(simpleName, "LinkDeviceUseCase::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public LinkDeviceUseCase(DeviceRepository deviceRepository, DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase, An4 an4, UserRepository userRepository, Q27 q27, Cj4 cj4, LabelRepository labelRepository, FileRepository fileRepository) {
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(downloadFirmwareByDeviceModelUsecase, "mDownloadFwByDeviceModel");
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(q27, "mDecryptValueKeyStoreUseCase");
        Wg6.c(cj4, "mDeviceSettingFactory");
        Wg6.c(labelRepository, "mLabelRepository");
        Wg6.c(fileRepository, "mFileRepository");
        this.k = deviceRepository;
        this.l = downloadFirmwareByDeviceModelUsecase;
        this.m = an4;
        this.n = userRepository;
        this.o = q27;
        this.p = cj4;
        this.q = labelRepository;
        this.r = fileRepository;
    }

    @DexIgnore
    public final Ki A() {
        return this.i;
    }

    @DexIgnore
    public final boolean B() {
        return this.h;
    }

    @DexIgnore
    public final void C(ShineDevice shineDevice, CoroutineUseCase.Ei<? super Ji, ? super Ii> ei) {
        Wg6.c(shineDevice, "closestDevice");
        Wg6.c(ei, "caseCallback");
        FLogger.INSTANCE.getLocal().d(s, "onRetrieveLinkAction");
        this.h = true;
        this.d = shineDevice.getSerial();
        this.e = shineDevice.getMacAddress();
        l(ei);
    }

    @DexIgnore
    public final void D() {
        BleCommandResultManager.d.e(this.j, CommunicateMode.LINK);
    }

    @DexIgnore
    public final void E(long j2) {
        synchronized (this) {
            FLogger.INSTANCE.getLocal().d(s, "requestAuthorizeDevice()");
            PortfolioApp instance = PortfolioApp.get.instance();
            String str = this.d;
            if (str != null) {
                instance.M0(str, new PairingAuthorizeResponse(j2));
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void F() {
        synchronized (this) {
            Device device = this.f;
            if (device != null) {
                Rm6 unused = Gu7.d(g(), null, null, new Pi(device, null, this), 3, null);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object G(com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.Hi r8, com.mapped.Xe6<java.lang.Object> r9) {
        /*
            r7 = this;
            r6 = 0
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 1
            boolean r0 = r9 instanceof com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.Qi
            if (r0 == 0) goto L_0x0032
            r0 = r9
            com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$Qi r0 = (com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.Qi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r5) goto L_0x0039
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$Hi r0 = (com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.Hi) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase r0 = (com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase) r0
            com.fossil.El7.b(r2)     // Catch:{ Exception -> 0x0098 }
        L_0x002c:
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
        L_0x0031:
            return r0
        L_0x0032:
            com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$Qi r0 = new com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$Qi
            r0.<init>(r7, r9)
            r1 = r0
            goto L_0x0015
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.s
            java.lang.String r4 = "running UseCase"
            r2.d(r3, r4)
            if (r8 != 0) goto L_0x005f
            com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$Ii r0 = new com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$Ii
            r1 = 600(0x258, float:8.41E-43)
            java.lang.String r2 = ""
            java.lang.String r3 = ""
            r0.<init>(r1, r2, r3)
            goto L_0x0031
        L_0x005f:
            r7.h = r5
            java.lang.String r2 = r8.a()
            r7.d = r2
            java.lang.String r2 = ""
            r7.e = r2
            r8.b()
            java.lang.String r2 = r8.b()
            r7.e = r2
            com.portfolio.platform.PortfolioApp$inner r2 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r2 = r2.instance()
            java.lang.String r3 = r7.d
            if (r3 == 0) goto L_0x0094
            java.lang.String r4 = r7.e
            if (r4 == 0) goto L_0x0090
            r1.L$0 = r7
            r1.L$1 = r8
            r5 = 1
            r1.label = r5
            java.lang.Object r1 = r2.L0(r3, r4, r1)
            if (r1 != r0) goto L_0x002c
            goto L_0x0031
        L_0x0090:
            com.mapped.Wg6.i()
            throw r6
        L_0x0094:
            com.mapped.Wg6.i()
            throw r6
        L_0x0098:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.s
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Error inside "
            r3.append(r4)
            java.lang.String r4 = com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.s
            r3.append(r4)
            java.lang.String r4 = ".connectDevice - e="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.e(r2, r0)
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase.G(com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$Hi, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void H() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "sendLatestFWResponseToDevice(), deviceModel=" + this.g + ", isSkipOTA=" + this.m.y0());
        if (PortfolioApp.get.instance().z0() || !this.m.y0()) {
            this.l.e(new DownloadFirmwareByDeviceModelUsecase.Ai(this.g), new Ri(this));
            return;
        }
        PortfolioApp instance = PortfolioApp.get.instance();
        String str2 = this.d;
        if (str2 != null) {
            instance.M0(str2, PairingResponse.CREATOR.buildPairingUpdateFWResponse(new SkipFirmwareData()));
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void I(boolean z) {
        this.h = z;
    }

    @DexIgnore
    public final void J(Device device) {
        this.f = device;
    }

    @DexIgnore
    public final void K(String str) {
        Wg6.c(str, "<set-?>");
        this.g = str;
    }

    @DexIgnore
    public final void L(String str) {
        this.e = str;
    }

    @DexIgnore
    public final void M(String str) {
        this.d = str;
    }

    @DexIgnore
    public final void N(Ki ki) {
        this.i = ki;
    }

    @DexIgnore
    public final void O() {
        BleCommandResultManager.d.j(this.j, CommunicateMode.LINK);
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return s;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Hi hi, Xe6 xe6) {
        return G(hi, xe6);
    }

    @DexIgnore
    public final void u() {
        synchronized (this) {
            Cj4 cj4 = this.p;
            String str = this.d;
            if (str != null) {
                CoroutineUseCase<Ty6, Uy6, Sy6> a2 = cj4.a(str);
                String str2 = this.d;
                if (str2 != null) {
                    a2.e(new Ty6(str2, Ry6.PAIR), new Ni(this));
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void v(String str) {
        synchronized (this) {
            Wg6.c(str, "serial");
            this.h = true;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = s;
            local.d(str2, "downloadLabelFile, serial = " + str);
            Rm6 unused = Gu7.d(g(), null, null, new Oi(this, str, null), 3, null);
        }
    }

    @DexIgnore
    public final Device w() {
        return this.f;
    }

    @DexIgnore
    public final String x() {
        return this.g;
    }

    @DexIgnore
    public final String y() {
        return this.e;
    }

    @DexIgnore
    public final String z() {
        return this.d;
    }
}
