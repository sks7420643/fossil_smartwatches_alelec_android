package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.UserSettingDao;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetActivityGoalUserCase extends CoroutineUseCase<Bi, Di, Ci> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ Ai j; // = new Ai(null);
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ BleCommandResultManager.Bi e; // = new Ei(this);
    @DexIgnore
    public /* final */ PortfolioApp f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ UserSettingDao h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return SetActivityGoalUserCase.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ ActivitySettings b;

        @DexIgnore
        public Bi(String str, ActivitySettings activitySettings) {
            Wg6.c(str, "serial");
            Wg6.c(activitySettings, com.fossil.wearables.fsl.fitness.ActivitySettings.TABLE_NAME);
            this.a = str;
            this.b = activitySettings;
        }

        @DexIgnore
        public final ActivitySettings a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public Ci(int i, String str, ArrayList<Integer> arrayList) {
            this.a = i;
            this.b = str;
            this.c = arrayList;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ci(int i, String str, ArrayList arrayList, int i2, Qg6 qg6) {
            this(i, str, (i2 & 4) != 0 ? null : arrayList);
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.c;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements BleCommandResultManager.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ SetActivityGoalUserCase a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase$mSetGoalReceiver$1$receive$1", f = "SetActivityGoalUserCase.kt", l = {50, 59}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isGoalRingEnabled;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, boolean z, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
                this.$isGoalRingEnabled = z;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$isGoalRingEnabled, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x0042  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x00e5  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                /*
                // Method dump skipped, instructions count: 238
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase.Ei.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei(SetActivityGoalUserCase setActivityGoalUserCase) {
            this.a = setActivityGoalUserCase;
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            Bundle extras = intent.getExtras();
            boolean z = extras != null ? extras.getBoolean(ButtonService.GOAL_RING_ENABLED, true) : true;
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            if (communicateMode == CommunicateMode.SET_STEP_GOAL && Wg6.a(stringExtra, this.a.d)) {
                if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    Rm6 unused = Gu7.d(this.a.g(), null, null, new Aii(this, z, null), 3, null);
                } else if (intExtra == ServiceActionResult.FAILED.ordinal()) {
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                    int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), FailureCode.UNKNOWN_ERROR);
                    if (intExtra2 == 1101) {
                        this.a.i(new Ci(125, "", integerArrayListExtra));
                    } else if (intExtra2 == 1505) {
                        this.a.i(new Ci(123, "", null, 4, null));
                    } else if (intExtra2 != 1920) {
                        this.a.i(new Ci(600, "", null, 4, null));
                    } else {
                        this.a.i(new Ci(124, "", null, 4, null));
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase", f = "SetActivityGoalUserCase.kt", l = {85}, m = "sendUserSettingToServer")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ SetActivityGoalUserCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(SetActivityGoalUserCase setActivityGoalUserCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = setActivityGoalUserCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(null, this);
        }
    }

    /*
    static {
        String simpleName = SetActivityGoalUserCase.class.getSimpleName();
        Wg6.b(simpleName, "SetActivityGoalUserCase::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public SetActivityGoalUserCase(PortfolioApp portfolioApp, UserRepository userRepository, UserSettingDao userSettingDao) {
        Wg6.c(portfolioApp, "mPortfolioApp");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(userSettingDao, "mUserSettingDao");
        this.f = portfolioApp;
        this.g = userRepository;
        this.h = userSettingDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return i;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return r(bi, xe6);
    }

    @DexIgnore
    public final void q() {
        FLogger.INSTANCE.getLocal().d(i, "registerReceiver");
        BleCommandResultManager.d.j(this.e, CommunicateMode.SET_STEP_GOAL);
        BleCommandResultManager.d.e(this.e, CommunicateMode.SET_STEP_GOAL);
        BleCommandResultManager.d.g(CommunicateMode.SET_STEP_GOAL);
    }

    @DexIgnore
    public Object r(Bi bi, Xe6<Object> xe6) {
        if (bi == null) {
            return new Ci(600, "", null, 4, null);
        }
        FLogger.INSTANCE.getLocal().d(i, "running UseCase");
        this.d = bi.b();
        this.f.K1(bi.b(), bi.a().getCurrentStepGoal(), bi.a().getCurrentCaloriesGoal(), bi.a().getCurrentActiveTimeGoal());
        return new Object();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    @android.annotation.SuppressLint({"VisibleForTests"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object s(com.portfolio.platform.data.model.UserSettings r6, com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r5 = this;
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 1
            boolean r0 = r7 instanceof com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase.Fi
            if (r0 == 0) goto L_0x0054
            r0 = r7
            com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase$Fi r0 = (com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase.Fi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0054
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0063
            if (r3 != r4) goto L_0x005b
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.data.model.UserSettings r0 = (com.portfolio.platform.data.model.UserSettings) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase r1 = (com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase) r1
            com.fossil.El7.b(r2)
            r6 = r0
            r5 = r1
        L_0x002d:
            r0 = r2
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            if (r0 == 0) goto L_0x0051
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x007a
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r1 = r0.a()
            com.portfolio.platform.data.model.UserSettings r1 = (com.portfolio.platform.data.model.UserSettings) r1
            if (r1 == 0) goto L_0x0044
            r2 = 0
            r1.setPinType(r2)
        L_0x0044:
            com.portfolio.platform.data.source.UserSettingDao r1 = r5.h
            java.lang.Object r0 = r0.a()
            if (r0 == 0) goto L_0x0075
            com.portfolio.platform.data.model.UserSettings r0 = (com.portfolio.platform.data.model.UserSettings) r0
            r1.addOrUpdateUserSetting(r0)
        L_0x0051:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0053:
            return r0
        L_0x0054:
            com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase$Fi r0 = new com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase$Fi
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x005b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0063:
            com.fossil.El7.b(r2)
            com.portfolio.platform.data.source.UserRepository r2 = r5.g
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r2 = r2.sendUserSettingToServer(r6, r1)
            if (r2 != r0) goto L_0x002d
            goto L_0x0053
        L_0x0075:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        L_0x007a:
            boolean r0 = r0 instanceof com.fossil.Hq5
            if (r0 == 0) goto L_0x0051
            r6.setPinType(r4)
            com.portfolio.platform.data.source.UserSettingDao r0 = r5.h
            r0.addOrUpdateUserSetting(r6)
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase.s(com.portfolio.platform.data.model.UserSettings, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void t() {
        FLogger.INSTANCE.getLocal().d(i, "unRegisterReceiver");
        BleCommandResultManager.d.j(this.e, CommunicateMode.SET_STEP_GOAL);
    }
}
