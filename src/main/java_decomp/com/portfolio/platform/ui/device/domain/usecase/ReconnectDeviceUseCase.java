package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ReconnectDeviceUseCase extends CoroutineUseCase<Ci, Ei, Di> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ Ai h; // = new Ai(null);
    @DexIgnore
    public String d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ Bi f; // = new Bi();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ReconnectDeviceUseCase.g;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi implements BleCommandResultManager.Bi {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            boolean z = false;
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ReconnectDeviceUseCase.h.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + ReconnectDeviceUseCase.this.n() + ", isSuccess=" + intExtra);
            if (communicateMode == CommunicateMode.FORCE_CONNECT && ReconnectDeviceUseCase.this.n()) {
                ReconnectDeviceUseCase.this.q(false);
                if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    ReconnectDeviceUseCase.this.j(new Ei());
                    return;
                }
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra2);
                }
                ReconnectDeviceUseCase.this.i(new Di(FailureCode.FAILED_TO_CONNECT, intExtra2, integerArrayListExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Ci(String str) {
            Wg6.c(str, "serial");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public Di(int i, int i2, ArrayList<Integer> arrayList) {
            Wg6.c(arrayList, "errorCodes");
            this.a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase", f = "ReconnectDeviceUseCase.kt", l = {58}, m = "run")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ReconnectDeviceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(ReconnectDeviceUseCase reconnectDeviceUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = reconnectDeviceUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.p(null, this);
        }
    }

    /*
    static {
        String simpleName = ReconnectDeviceUseCase.class.getSimpleName();
        Wg6.b(simpleName, "ReconnectDeviceUseCase::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return g;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ci ci, Xe6 xe6) {
        return p(ci, xe6);
    }

    @DexIgnore
    public final boolean n() {
        return this.e;
    }

    @DexIgnore
    public final void o() {
        BleCommandResultManager.d.e(this.f, CommunicateMode.FORCE_CONNECT);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object p(com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase.Ci r7, com.mapped.Xe6<java.lang.Object> r8) {
        /*
            r6 = this;
            r2 = 0
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 1
            boolean r0 = r8 instanceof com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase.Fi
            if (r0 == 0) goto L_0x0032
            r0 = r8
            com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase$Fi r0 = (com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase.Fi) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0032
            int r1 = r1 + r4
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r4 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0041
            if (r0 != r5) goto L_0x0039
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase$Ci r0 = (com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase.Ci) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase r0 = (com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase) r0
            com.fossil.El7.b(r4)     // Catch:{ Exception -> 0x006d }
        L_0x002c:
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
        L_0x0031:
            return r0
        L_0x0032:
            com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase$Fi r0 = new com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase$Fi
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0015
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r4)
            r6.e = r5
            if (r7 == 0) goto L_0x0067
            java.lang.String r0 = r7.a()
        L_0x004c:
            r6.d = r0
            com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r0 = r0.instance()
            java.lang.String r4 = r6.d
            if (r4 == 0) goto L_0x0069
            r1.L$0 = r6
            r1.L$1 = r7
            r2 = 1
            r1.label = r2
            java.lang.Object r0 = r0.G(r4, r1)
            if (r0 != r3) goto L_0x002c
            r0 = r3
            goto L_0x0031
        L_0x0067:
            r0 = r2
            goto L_0x004c
        L_0x0069:
            com.mapped.Wg6.i()
            throw r2
        L_0x006d:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase.g
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Error inside "
            r3.append(r4)
            java.lang.String r4 = com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase.g
            r3.append(r4)
            java.lang.String r4 = ".connectDevice - e="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.e(r2, r0)
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase.p(com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase$Ci, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void q(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public final void r() {
        BleCommandResultManager.d.j(this.f, CommunicateMode.FORCE_CONNECT);
    }
}
