package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import android.text.TextUtils;
import android.util.SparseArray;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.St5;
import com.fossil.Tt5;
import com.fossil.Ul5;
import com.fossil.Ut5;
import com.fossil.Vt7;
import com.fossil.Yn7;
import com.fossil.Zh5;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.util.NotificationAppHelper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridSyncUseCase extends CoroutineUseCase<Tt5, Ut5, St5> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public /* final */ Ai d; // = new Ai(this);
    @DexIgnore
    public /* final */ MicroAppRepository e;
    @DexIgnore
    public /* final */ An4 f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ PortfolioApp h;
    @DexIgnore
    public /* final */ HybridPresetRepository i;
    @DexIgnore
    public /* final */ NotificationsRepository j;
    @DexIgnore
    public /* final */ AnalyticsHelper k;
    @DexIgnore
    public /* final */ AlarmsRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements BleCommandResultManager.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ HybridSyncUseCase a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(HybridSyncUseCase hybridSyncUseCase) {
            this.a = hybridSyncUseCase;
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && Vt7.j(stringExtra, this.a.h.J(), true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                if (intExtra == 1) {
                    FLogger.INSTANCE.getLocal().d(HybridSyncUseCase.m, "sync success, remove device now");
                    BleCommandResultManager.d.j(this, CommunicateMode.SYNC);
                    this.a.j(new Ut5());
                } else if (intExtra == 2 || intExtra == 4) {
                    BleCommandResultManager.d.j(this, CommunicateMode.SYNC);
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>();
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = HybridSyncUseCase.m;
                    local.d(str, "sync fail due to " + intExtra2);
                    if (intExtra2 == 1101 || intExtra2 == 1112 || intExtra2 == 1113) {
                        this.a.i(new St5(Zh5.FAIL_DUE_TO_LACK_PERMISSION, integerArrayListExtra));
                    } else {
                        this.a.i(new St5(Zh5.FAIL_DUE_TO_SYNC_FAIL, null));
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase", f = "HybridSyncUseCase.kt", l = {85, 93, 132}, m = "run")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public boolean Z$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ HybridSyncUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(HybridSyncUseCase hybridSyncUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = hybridSyncUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.q(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase$run$isAA$1", f = "HybridSyncUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        public Ci(Xe6 xe6) {
            super(2, xe6);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return Ao7.a(SoLibraryLoader.f().c(PortfolioApp.get.instance()) != null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase$saveNotificationSettingToDevice$1", f = "HybridSyncUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ SparseArray $data;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isMovemberModel;
        @DexIgnore
        public /* final */ /* synthetic */ String $serialNumber;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridSyncUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(HybridSyncUseCase hybridSyncUseCase, SparseArray sparseArray, boolean z, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = hybridSyncUseCase;
            this.$data = sparseArray;
            this.$isMovemberModel = z;
            this.$serialNumber = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$data, this.$isMovemberModel, this.$serialNumber, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                AppNotificationFilterSettings b = NotificationAppHelper.b.b(this.$data, this.$isMovemberModel);
                PortfolioApp.get.instance().s1(b, this.$serialNumber);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String h = this.this$0.h();
                local.d(h, "saveNotificationSettingToDevice, total: " + b.getNotificationFilters().size() + " items");
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = HybridSyncUseCase.class.getSimpleName();
        Wg6.b(simpleName, "HybridSyncUseCase::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public HybridSyncUseCase(MicroAppRepository microAppRepository, An4 an4, DeviceRepository deviceRepository, PortfolioApp portfolioApp, HybridPresetRepository hybridPresetRepository, NotificationsRepository notificationsRepository, AnalyticsHelper analyticsHelper, AlarmsRepository alarmsRepository) {
        Wg6.c(microAppRepository, "mMicroAppRepository");
        Wg6.c(an4, "mSharedPrefs");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(hybridPresetRepository, "mPresetRepository");
        Wg6.c(notificationsRepository, "mNotificationsRepository");
        Wg6.c(analyticsHelper, "mAnalyticsHelper");
        Wg6.c(alarmsRepository, "mAlarmsRepository");
        this.e = microAppRepository;
        this.f = an4;
        this.g = deviceRepository;
        this.h = portfolioApp;
        this.i = hybridPresetRepository;
        this.j = notificationsRepository;
        this.k = analyticsHelper;
        this.l = alarmsRepository;
    }

    @DexIgnore
    public static /* synthetic */ void p(HybridSyncUseCase hybridSyncUseCase, String str, Integer num, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            num = 2;
        }
        hybridSyncUseCase.o(str, num);
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "HybridSyncUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Tt5 tt5, Xe6 xe6) {
        return q(tt5, xe6);
    }

    @DexIgnore
    public final void o(String str, Integer num) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, "broadcastSyncFail serial=" + str);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", num);
        intent.putExtra("SERIAL", str);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0055 A[Catch:{ Exception -> 0x0111 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0091 A[Catch:{ Exception -> 0x0111 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0179  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01c3  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01ff  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x022e  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x024a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object q(com.fossil.Tt5 r15, com.mapped.Xe6<java.lang.Object> r16) {
        /*
        // Method dump skipped, instructions count: 987
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase.q(com.fossil.Tt5, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Rm6 r(SparseArray<List<BaseFeatureModel>> sparseArray, String str, boolean z) {
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Di(this, sparseArray, z, str, null), 3, null);
    }

    @DexIgnore
    public final void s(int i2, boolean z, UserProfile userProfile, String str) {
        if (f() != null) {
            BleCommandResultManager.d.e(this.d, CommunicateMode.SYNC);
        }
        userProfile.setNewDevice(z);
        userProfile.setSyncMode(i2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, ".startDeviceSync - syncMode=" + i2);
        if (!this.f.z0()) {
            SKUModel skuModelBySerialPrefix = this.g.getSkuModelBySerialPrefix(DeviceHelper.o.m(str));
            this.f.t1(Boolean.TRUE);
            this.k.o(i2, skuModelBySerialPrefix);
            Ul5 c = AnalyticsHelper.f.c("sync_session");
            AnalyticsHelper.f.a("sync_session", c);
            c.i();
        }
        if (!PortfolioApp.get.instance().T1(str, userProfile)) {
            p(this, str, null, 2, null);
        } else if (i2 == 13) {
            this.f.Z0(str, System.currentTimeMillis(), false);
        }
    }
}
