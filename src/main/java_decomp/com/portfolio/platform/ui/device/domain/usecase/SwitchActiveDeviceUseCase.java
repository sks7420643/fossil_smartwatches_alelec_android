package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import android.os.Bundle;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Ry6;
import com.fossil.Sy6;
import com.fossil.Ty6;
import com.fossil.Uy6;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SwitchActiveDeviceUseCase extends CoroutineUseCase<Bi, Di, Ci> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai n; // = new Ai(null);
    @DexIgnore
    public Bi d;
    @DexIgnore
    public Device e;
    @DexIgnore
    public Ci f;
    @DexIgnore
    public /* final */ BleCommandResultManager.Bi g; // = new Ii(this);
    @DexIgnore
    public /* final */ UserRepository h;
    @DexIgnore
    public /* final */ DeviceRepository i;
    @DexIgnore
    public /* final */ Cj4 j;
    @DexIgnore
    public /* final */ PortfolioApp k;
    @DexIgnore
    public /* final */ An4 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return SwitchActiveDeviceUseCase.m;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Bi(String str, int i) {
            Wg6.c(str, "newActiveSerial");
            this.a = str;
            this.b = i;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public Ci(int i, ArrayList<Integer> arrayList, String str) {
            this.a = i;
            this.b = arrayList;
            this.c = str;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ Device a;

        @DexIgnore
        public Di(Device device) {
            this.a = device;
        }

        @DexIgnore
        public final Device a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$1", f = "SwitchActiveDeviceUseCase.kt", l = {155}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SwitchActiveDeviceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(SwitchActiveDeviceUseCase switchActiveDeviceUseCase, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = switchActiveDeviceUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object X1;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = SwitchActiveDeviceUseCase.n.a();
                StringBuilder sb = new StringBuilder();
                sb.append("doSwitchDevice serial ");
                Bi v = this.this$0.v();
                if (v != null) {
                    sb.append(v.b());
                    local.d(a2, sb.toString());
                    PortfolioApp instance = PortfolioApp.get.instance();
                    Bi v2 = this.this$0.v();
                    if (v2 != null) {
                        String b = v2.b();
                        this.L$0 = il6;
                        this.label = 1;
                        X1 = instance.X1(b, this);
                        if (X1 == d) {
                            return d;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                X1 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (!((Boolean) X1).booleanValue()) {
                this.this$0.C();
                this.this$0.i(new Ci(116, null, ""));
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1", f = "SwitchActiveDeviceUseCase.kt", l = {181, 184}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $newActiveDeviceSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SwitchActiveDeviceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(SwitchActiveDeviceUseCase switchActiveDeviceUseCase, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = switchActiveDeviceUseCase;
            this.$newActiveDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, this.$newActiveDeviceSerial, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0053  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00da  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
            // Method dump skipped, instructions count: 274
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.Fi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements CoroutineUseCase.Ei<Uy6, Sy6> {
        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(Sy6 sy6) {
            b(sy6);
        }

        @DexIgnore
        public void b(Sy6 sy6) {
            Wg6.c(sy6, "errorValue");
            FLogger.INSTANCE.getLocal().d(SwitchActiveDeviceUseCase.n.a(), "getDeviceSetting fail");
        }

        @DexIgnore
        public void c(Uy6 uy6) {
            Wg6.c(uy6, "responseValue");
            FLogger.INSTANCE.getLocal().d(SwitchActiveDeviceUseCase.n.a(), "getDeviceSetting success");
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(Uy6 uy6) {
            c(uy6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$linkServer$2", f = "SwitchActiveDeviceUseCase.kt", l = {234, 237}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends Boolean, ? extends Integer>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SwitchActiveDeviceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(SwitchActiveDeviceUseCase switchActiveDeviceUseCase, MisfitDeviceProfile misfitDeviceProfile, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = switchActiveDeviceUseCase;
            this.$currentDeviceProfile = misfitDeviceProfile;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, this.$currentDeviceProfile, this.$serial, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends Boolean, ? extends Integer>> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00b8, code lost:
            if (r0 != null) goto L_0x00ba;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x00ba, code lost:
            if (r0 != null) goto L_0x00bc;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0045  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x0170  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 514
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.Hi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements BleCommandResultManager.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ SwitchActiveDeviceUseCase a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1", f = "SwitchActiveDeviceUseCase.kt", l = {75}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
            @DexIgnore
            public /* final */ /* synthetic */ String $serial;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ii this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ii ii, String str, MisfitDeviceProfile misfitDeviceProfile, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ii;
                this.$serial = str;
                this.$currentDeviceProfile = misfitDeviceProfile;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$serial, this.$currentDeviceProfile, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object x;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.this$0.a;
                    String str = this.$serial;
                    Wg6.b(str, "serial");
                    MisfitDeviceProfile misfitDeviceProfile = this.$currentDeviceProfile;
                    if (misfitDeviceProfile != null) {
                        this.L$0 = il6;
                        this.label = 1;
                        x = switchActiveDeviceUseCase.x(str, misfitDeviceProfile, this);
                        if (x == d) {
                            return d;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    x = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                Lc6 lc6 = (Lc6) x;
                boolean booleanValue = ((Boolean) lc6.component1()).booleanValue();
                int intValue = ((Number) lc6.component2()).intValue();
                PortfolioApp instance = PortfolioApp.get.instance();
                String str2 = this.$serial;
                Wg6.b(str2, "serial");
                instance.Y1(str2, booleanValue, intValue);
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2", f = "SwitchActiveDeviceUseCase.kt", l = {95, 99}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
            @DexIgnore
            public /* final */ /* synthetic */ String $serial;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ii this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ii ii, MisfitDeviceProfile misfitDeviceProfile, String str, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ii;
                this.$currentDeviceProfile = misfitDeviceProfile;
                this.$serial = str;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, this.$currentDeviceProfile, this.$serial, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0067  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x0069
                    if (r0 == r6) goto L_0x0034
                    if (r0 != r7) goto L_0x002c
                    java.lang.Object r0 = r8.L$1
                    com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
                    java.lang.Object r0 = r8.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r9)
                L_0x0019:
                    com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$Ii r0 = r8.this$0
                    com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase r0 = r0.a
                    com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$Di r1 = new com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$Di
                    com.portfolio.platform.data.model.Device r2 = r0.u()
                    r1.<init>(r2)
                    r0.j(r1)
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x002b:
                    return r0
                L_0x002c:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0034:
                    java.lang.Object r0 = r8.L$1
                    com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
                    int r2 = r8.I$0
                    java.lang.Object r1 = r8.L$0
                    com.mapped.Il6 r1 = (com.mapped.Il6) r1
                    com.fossil.El7.b(r9)
                    r3 = r0
                L_0x0042:
                    r0 = r2
                L_0x0043:
                    com.portfolio.platform.PortfolioApp$inner r2 = com.portfolio.platform.PortfolioApp.get
                    com.portfolio.platform.PortfolioApp r2 = r2.instance()
                    java.lang.String r5 = r8.$serial
                    java.lang.String r6 = "serial"
                    com.mapped.Wg6.b(r5, r6)
                    r2.J0(r5)
                    com.portfolio.platform.util.DeviceUtils$Ai r2 = com.portfolio.platform.util.DeviceUtils.h
                    com.portfolio.platform.util.DeviceUtils r2 = r2.a()
                    r8.L$0 = r1
                    r8.I$0 = r0
                    r8.L$1 = r3
                    r8.label = r7
                    java.lang.Object r0 = r2.d(r8)
                    if (r0 != r4) goto L_0x0019
                    r0 = r4
                    goto L_0x002b
                L_0x0069:
                    com.fossil.El7.b(r9)
                    com.mapped.Il6 r1 = r8.p$
                    com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r0 = r8.$currentDeviceProfile
                    com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj r0 = r0.getVibrationStrength()
                    int r0 = r0.getVibrationStrengthLevel()
                    int r0 = com.fossil.Tk5.c(r0)
                    com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$Ii r2 = r8.this$0
                    com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase r2 = r2.a
                    com.portfolio.platform.data.source.DeviceRepository r2 = com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.n(r2)
                    java.lang.String r3 = r8.$serial
                    java.lang.String r5 = "serial"
                    com.mapped.Wg6.b(r3, r5)
                    com.portfolio.platform.data.model.Device r3 = r2.getDeviceBySerial(r3)
                    if (r3 == 0) goto L_0x0043
                    java.lang.Integer r2 = r3.getVibrationStrength()
                    if (r2 != 0) goto L_0x00b8
                L_0x0097:
                    java.lang.Integer r2 = com.fossil.Ao7.e(r0)
                    r3.setVibrationStrength(r2)
                    com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$Ii r2 = r8.this$0
                    com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase r2 = r2.a
                    com.portfolio.platform.data.source.DeviceRepository r2 = com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.n(r2)
                    r8.L$0 = r1
                    r8.I$0 = r0
                    r8.L$1 = r3
                    r8.label = r6
                    r5 = 0
                    java.lang.Object r2 = r2.updateDevice(r3, r5, r8)
                    if (r2 != r4) goto L_0x00bf
                    r0 = r4
                    goto L_0x002b
                L_0x00b8:
                    int r2 = r2.intValue()
                    if (r2 == r0) goto L_0x0043
                    goto L_0x0097
                L_0x00bf:
                    r2 = r0
                    goto L_0x0042
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase.Ii.Bii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ii(SwitchActiveDeviceUseCase switchActiveDeviceUseCase) {
            this.a = switchActiveDeviceUseCase;
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            if (communicateMode == CommunicateMode.SWITCH_DEVICE) {
                Bi v = this.a.v();
                if (!Wg6.a(stringExtra, v != null ? v.b() : null)) {
                    return;
                }
                if (intExtra == ServiceActionResult.ASK_FOR_LINK_SERVER.ordinal()) {
                    Bundle extras = intent.getExtras();
                    if (extras != null) {
                        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Aii(this, stringExtra, (MisfitDeviceProfile) extras.getParcelable("device"), null), 3, null);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.a.C();
                    FLogger.INSTANCE.getLocal().d(SwitchActiveDeviceUseCase.n.a(), "Switch device  success");
                    Bundle extras2 = intent.getExtras();
                    if (extras2 != null) {
                        MisfitDeviceProfile misfitDeviceProfile = (MisfitDeviceProfile) extras2.getParcelable("device");
                        if (misfitDeviceProfile != null) {
                            if (misfitDeviceProfile.getHeartRateMode() != HeartRateMode.NONE) {
                                this.a.l.k1(misfitDeviceProfile.getHeartRateMode());
                            }
                            SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.a;
                            Wg6.b(stringExtra, "serial");
                            switchActiveDeviceUseCase.t(stringExtra);
                            Rm6 unused2 = Gu7.d(Jv7.a(Bw7.b()), null, null, new Bii(this, misfitDeviceProfile, stringExtra, null), 3, null);
                            return;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                } else if (intExtra == ServiceActionResult.FAILED.ordinal()) {
                    this.a.C();
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>();
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = SwitchActiveDeviceUseCase.n.a();
                    local.d(a2, "stop current workout fail due to " + intExtra2);
                    if (intExtra2 != 1101) {
                        if (intExtra2 == 1928) {
                            Ci w = this.a.w();
                            if (w == null || this.a.i(w) == null) {
                                this.a.i(new Ci(116, null, ""));
                                return;
                            }
                            return;
                        } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                            this.a.i(new Ci(117, null, ""));
                            return;
                        }
                    }
                    this.a.i(new Ci(113, integerArrayListExtra, ""));
                }
            }
        }
    }

    /*
    static {
        String simpleName = SwitchActiveDeviceUseCase.class.getSimpleName();
        Wg6.b(simpleName, "SwitchActiveDeviceUseCase::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public SwitchActiveDeviceUseCase(UserRepository userRepository, DeviceRepository deviceRepository, Cj4 cj4, PortfolioApp portfolioApp, An4 an4) {
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(cj4, "mDeviceSettingFactory");
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(an4, "mSharePrefs");
        this.h = userRepository;
        this.i = deviceRepository;
        this.j = cj4;
        this.k = portfolioApp;
        this.l = an4;
    }

    @DexIgnore
    public final void A(Device device) {
        this.e = device;
    }

    @DexIgnore
    public final void B(Ci ci) {
        this.f = ci;
    }

    @DexIgnore
    public final void C() {
        FLogger.INSTANCE.getLocal().d(m, "unregisterReceiver ");
        BleCommandResultManager.d.j(this.g, CommunicateMode.SWITCH_DEVICE);
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return m;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return z(bi, xe6);
    }

    @DexIgnore
    public final Rm6 r() {
        return Gu7.d(g(), null, null, new Ei(this, null), 3, null);
    }

    @DexIgnore
    public final Rm6 s(String str) {
        return Gu7.d(g(), null, null, new Fi(this, str, null), 3, null);
    }

    @DexIgnore
    public final void t(String str) {
        Wg6.c(str, "serial");
        FLogger.INSTANCE.getLocal().d(m, "getDeviceSetting start");
        this.j.a(str).e(new Ty6(str, Ry6.SWITCH), new Gi());
    }

    @DexIgnore
    public final Device u() {
        return this.e;
    }

    @DexIgnore
    public final Bi v() {
        return this.d;
    }

    @DexIgnore
    public final Ci w() {
        return this.f;
    }

    @DexIgnore
    public final /* synthetic */ Object x(String str, MisfitDeviceProfile misfitDeviceProfile, Xe6<? super Lc6<Boolean, Integer>> xe6) {
        return Eu7.g(Bw7.b(), new Hi(this, misfitDeviceProfile, str, null), xe6);
    }

    @DexIgnore
    public final void y() {
        FLogger.INSTANCE.getLocal().d(m, "registerReceiver ");
        BleCommandResultManager.d.j(this.g, CommunicateMode.SWITCH_DEVICE);
        BleCommandResultManager.d.e(this.g, CommunicateMode.SWITCH_DEVICE);
    }

    @DexIgnore
    public Object z(Bi bi, Xe6<Object> xe6) {
        if (bi == null) {
            return new Ci(600, null, "");
        }
        this.d = bi;
        String J = this.k.J();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "run with mode " + bi.a() + " currentActive " + J);
        if (bi.a() != 4) {
            r();
        } else {
            s(bi.b());
        }
        return new Object();
    }
}
