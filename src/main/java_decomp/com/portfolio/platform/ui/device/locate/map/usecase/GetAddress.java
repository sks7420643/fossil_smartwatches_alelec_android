package com.portfolio.platform.ui.device.locate.map.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Um5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Jf6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetAddress extends CoroutineUseCase<Ai, Ci, Bi> {
    @DexIgnore
    public /* final */ GoogleApiService d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ double a;
        @DexIgnore
        public /* final */ double b;

        @DexIgnore
        public Ai(double d, double d2) {
            this.a = d;
            this.b = d2;
        }

        @DexIgnore
        public final double a() {
            return this.a;
        }

        @DexIgnore
        public final double b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ai {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Ci(String str) {
            Wg6.c(str, "address");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress", f = "GetAddress.kt", l = {20}, m = "run")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ GetAddress this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(GetAddress getAddress, Xe6 xe6) {
            super(xe6);
            this.this$0 = getAddress;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.n(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$run$response$1", f = "GetAddress.kt", l = {20}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ai $requestValues;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ GetAddress this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(GetAddress getAddress, Ai ai, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = getAddress;
            this.$requestValues = ai;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Ei(this.this$0, this.$requestValues, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Ei) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Double d = null;
            Object d2 = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                GoogleApiService googleApiService = this.this$0.d;
                StringBuilder sb = new StringBuilder();
                Ai ai = this.$requestValues;
                sb.append(ai != null ? Ao7.c(ai.a()) : null);
                sb.append(',');
                Ai ai2 = this.$requestValues;
                if (ai2 != null) {
                    d = Ao7.c(ai2.b());
                }
                sb.append(d);
                String sb2 = sb.toString();
                Locale a2 = Um5.a();
                Wg6.b(a2, "LanguageHelper.getLocale()");
                String language = a2.getLanguage();
                Wg6.b(language, "LanguageHelper.getLocale().language");
                this.label = 1;
                Object address = googleApiService.getAddress(sb2, language, this);
                return address == d2 ? d2 : address;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public GetAddress(GoogleApiService googleApiService) {
        Wg6.c(googleApiService, "mGoogleService");
        this.d = googleApiService;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "GetAddress";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ai ai, Xe6 xe6) {
        return n(ai, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object n(com.portfolio.platform.ui.device.locate.map.usecase.GetAddress.Ai r8, com.mapped.Xe6<java.lang.Object> r9) {
        /*
            r7 = this;
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.portfolio.platform.ui.device.locate.map.usecase.GetAddress.Di
            if (r0 == 0) goto L_0x006b
            r0 = r9
            com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Di r0 = (com.portfolio.platform.ui.device.locate.map.usecase.GetAddress.Di) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x006b
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x007a
            if (r0 != r6) goto L_0x0072
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Ai r0 = (com.portfolio.platform.ui.device.locate.map.usecase.GetAddress.Ai) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.ui.device.locate.map.usecase.GetAddress r0 = (com.portfolio.platform.ui.device.locate.map.usecase.GetAddress) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x002d:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x00ab
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            com.mapped.Ku3 r0 = (com.mapped.Ku3) r0
            if (r0 == 0) goto L_0x009d
            java.lang.String r1 = "results"
            com.mapped.Fu3 r0 = r0.q(r1)
        L_0x0043:
            if (r0 == 0) goto L_0x00a5
            int r1 = r0.size()
            if (r1 <= 0) goto L_0x00a5
            r1 = 0
            com.google.gson.JsonElement r0 = r0.m(r1)
            com.mapped.Ku3 r0 = (com.mapped.Ku3) r0
            if (r0 == 0) goto L_0x00bb
            java.lang.String r1 = "formatted_address"
            com.google.gson.JsonElement r0 = r0.p(r1)
        L_0x005a:
            if (r0 == 0) goto L_0x009f
            java.lang.String r1 = r0.f()
            java.lang.String r0 = "value.asString"
            com.mapped.Wg6.b(r1, r0)
            com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Ci r0 = new com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Ci
            r0.<init>(r1)
        L_0x006a:
            return r0
        L_0x006b:
            com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Di r0 = new com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Di
            r0.<init>(r7, r9)
            r1 = r0
            goto L_0x0015
        L_0x0072:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x007a:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = "GetAddress"
            java.lang.String r5 = "executeUseCase"
            r0.d(r2, r5)
            com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Ei r0 = new com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Ei
            r0.<init>(r7, r8, r4)
            r1.L$0 = r7
            r1.L$1 = r8
            r1.label = r6
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x006a
        L_0x009d:
            r0 = r4
            goto L_0x0043
        L_0x009f:
            com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Bi r0 = new com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Bi
            r0.<init>()
            goto L_0x006a
        L_0x00a5:
            com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Bi r0 = new com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Bi
            r0.<init>()
            goto L_0x006a
        L_0x00ab:
            boolean r0 = r0 instanceof com.fossil.Hq5
            if (r0 == 0) goto L_0x00b5
            com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Bi r0 = new com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Bi
            r0.<init>()
            goto L_0x006a
        L_0x00b5:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        L_0x00bb:
            r0 = r4
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.locate.map.usecase.GetAddress.n(com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$Ai, com.mapped.Xe6):java.lang.Object");
    }
}
