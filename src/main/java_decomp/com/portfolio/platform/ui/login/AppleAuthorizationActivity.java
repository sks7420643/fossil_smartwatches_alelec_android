package com.portfolio.platform.ui.login;

import android.content.Intent;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.fragment.app.FragmentActivity;
import com.fossil.Vt7;
import com.google.gson.Gson;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.BaseWebViewActivity;
import com.portfolio.platform.data.AppleAuth;
import com.portfolio.platform.data.SignUpSocialAuth;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppleAuthorizationActivity extends BaseWebViewActivity {
    @DexIgnore
    public static /* final */ a E; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(FragmentActivity fragmentActivity, String str) {
            Wg6.c(fragmentActivity, Constants.ACTIVITY);
            Wg6.c(str, "authorizationUrl");
            Intent intent = new Intent(fragmentActivity, AppleAuthorizationActivity.class);
            intent.putExtra("urlToLoad", str);
            fragmentActivity.startActivityForResult(intent, 3535);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends WebViewClient {
        @DexIgnore
        public /* final */ /* synthetic */ AppleAuthorizationActivity a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(AppleAuthorizationActivity appleAuthorizationActivity) {
            this.a = appleAuthorizationActivity;
        }

        @DexIgnore
        @Override // android.webkit.WebViewClient
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            boolean z = false;
            if (webResourceRequest != null) {
                try {
                    FLogger.INSTANCE.getLocal().d(this.a.r(), "request = " + webResourceRequest.getUrl());
                    String queryParameter = webResourceRequest.getUrl().getQueryParameter("id_token");
                    if (!(queryParameter == null || Vt7.l(queryParameter))) {
                        if (this.a.getIntent() == null) {
                            this.a.setIntent(new Intent());
                        }
                        SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                        signUpSocialAuth.setService("apple");
                        signUpSocialAuth.setToken(queryParameter);
                        String queryParameter2 = webResourceRequest.getUrl().getQueryParameter("user");
                        if (queryParameter2 == null || Vt7.l(queryParameter2)) {
                            z = true;
                        }
                        if (!z) {
                            AppleAuth appleAuth = (AppleAuth) new Gson().k(queryParameter2, AppleAuth.class);
                            signUpSocialAuth.setEmail(appleAuth.getEmail());
                            signUpSocialAuth.setLastName(appleAuth.getName().getLastName());
                            signUpSocialAuth.setFirstName(appleAuth.getName().getFirstName());
                        }
                        this.a.getIntent().putExtra("USER_INFO_EXTRA", signUpSocialAuth);
                        this.a.setResult(-1, this.a.getIntent());
                        z = true;
                    } else {
                        z = Wg6.a(webResourceRequest.getUrl().getQueryParameter("error"), "user_cancelled_authorize");
                    }
                } catch (Exception e) {
                    FLogger.INSTANCE.getLocal().d(this.a.r(), "Get authorization info with error: " + e.getMessage());
                }
            }
            if (!z) {
                return super.shouldOverrideUrlLoading(webView, webResourceRequest);
            }
            this.a.finish();
            return true;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.BaseWebViewActivity
    public WebViewClient M() {
        return new b(this);
    }
}
