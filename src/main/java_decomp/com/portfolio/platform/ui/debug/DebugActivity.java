package com.portfolio.platform.ui.debug;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Hr7;
import com.fossil.Im7;
import com.fossil.Ir7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Pm7;
import com.fossil.Q88;
import com.fossil.Ss5;
import com.fossil.Ts0;
import com.fossil.Um5;
import com.fossil.Ve0;
import com.fossil.Vi5;
import com.fossil.Vs0;
import com.fossil.Vs5;
import com.fossil.Ws5;
import com.fossil.Wt7;
import com.fossil.Xs5;
import com.fossil.Yn7;
import com.fossil.Ys5;
import com.mapped.AlertDialogFragment;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.E90;
import com.mapped.Er4;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareFactory;
import com.misfit.frameworks.buttonservice.model.customrequest.ForceBackgroundRequest;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DebugFirmwareData;
import com.portfolio.platform.data.model.DebugForceBackgroundRequestData;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.ui.device.domain.usecase.UpdateDeviceToSpecificFirmwareUsecase;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.viewmodel.FirmwareDebugViewModel;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DebugActivity extends BaseActivity implements Ss5.Bi {
    @DexIgnore
    public static /* final */ a O; // = new a(null);
    @DexIgnore
    public An4 A;
    @DexIgnore
    public UpdateDeviceToSpecificFirmwareUsecase B;
    @DexIgnore
    public FirmwareFileRepository C;
    @DexIgnore
    public GuestApiService D;
    @DexIgnore
    public DianaPresetRepository E;
    @DexIgnore
    public ShakeFeedbackService F;
    @DexIgnore
    public Cj4 G;
    @DexIgnore
    public /* final */ Ss5 H; // = new Ss5();
    @DexIgnore
    public /* final */ ArrayList<Ys5> I; // = new ArrayList<>();
    @DexIgnore
    public FirmwareDebugViewModel J;
    @DexIgnore
    public String K; // = "";
    @DexIgnore
    public HeartRateMode L;
    @DexIgnore
    public boolean M;
    @DexIgnore
    public /* final */ e N; // = new e(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            Wg6.c(context, "context");
            context.startActivity(new Intent(context, DebugActivity.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements InputFilter {
        @DexIgnore
        public double b;
        @DexIgnore
        public double c;

        @DexIgnore
        public b(double d2, double d3) {
            this.b = d2;
            this.c = d3;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0016 A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean a(double r4, double r6, double r8) {
            /*
                r3 = this;
                r0 = 1
                int r1 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
                if (r1 <= 0) goto L_0x000e
                int r1 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
                if (r1 < 0) goto L_0x0016
                int r1 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
                if (r1 > 0) goto L_0x0016
            L_0x000d:
                return r0
            L_0x000e:
                int r1 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
                if (r1 < 0) goto L_0x0016
                int r1 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
                if (r1 <= 0) goto L_0x000d
            L_0x0016:
                r0 = 0
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.debug.DebugActivity.b.a(double, double, double):boolean");
        }

        @DexIgnore
        public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
            Wg6.c(charSequence, "source");
            Wg6.c(spanned, "dest");
            try {
                StringBuilder sb = new StringBuilder();
                String obj = spanned.toString();
                if (obj != null) {
                    String substring = obj.substring(0, i3);
                    Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    sb.append(substring);
                    String obj2 = spanned.toString();
                    int length = spanned.toString().length();
                    if (obj2 != null) {
                        String substring2 = obj2.substring(i4, length);
                        Wg6.b(substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                        sb.append(substring2);
                        String sb2 = sb.toString();
                        StringBuilder sb3 = new StringBuilder();
                        if (sb2 != null) {
                            String substring3 = sb2.substring(0, i3);
                            Wg6.b(substring3, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                            sb3.append(substring3);
                            sb3.append(charSequence.toString());
                            int length2 = sb2.length();
                            if (sb2 != null) {
                                String substring4 = sb2.substring(i3, length2);
                                Wg6.b(substring4, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                                sb3.append(substring4);
                                if (a(this.b, this.c, Double.parseDouble(sb3.toString()))) {
                                    return null;
                                }
                                return "";
                            }
                            throw new Rc6("null cannot be cast to non-null type java.lang.String");
                        }
                        throw new Rc6("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.debug.DebugActivity", f = "DebugActivity.kt", l = {268}, m = "loadFirmware")
    public static final class c extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(DebugActivity debugActivity, Xe6 xe6) {
            super(xe6);
            this.this$0 = debugActivity;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c0(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.debug.DebugActivity$loadFirmware$repoResponse$1", f = "DebugActivity.kt", l = {268}, m = "invokeSuspend")
    public static final class d extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<Firmware>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $model;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(DebugActivity debugActivity, String str, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = debugActivity;
            this.$model = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new d(this.this$0, this.$model, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<ApiResponse<Firmware>>> xe6) {
            throw null;
            //return ((d) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                GuestApiService W = this.this$0.W();
                String P = PortfolioApp.get.instance().P();
                String str = this.$model;
                this.label = 1;
                Object firmwares$default = GuestApiService.DefaultImpls.getFirmwares$default(W, P, str, "android", false, this, 8, null);
                return firmwares$default == d ? d : firmwares$default;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements BleCommandResultManager.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(DebugActivity debugActivity) {
            this.a = debugActivity;
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            T t;
            T t2;
            ArrayList<Er4> a2;
            T t3;
            T t4;
            T t5;
            ArrayList<Er4> a3;
            T t6;
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            if (communicateMode == CommunicateMode.SET_HEART_RATE_MODE) {
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.a.a0().k1(this.a.X());
                    Iterator<T> it = this.a.S().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t4 = null;
                            break;
                        }
                        T next = it.next();
                        if (Wg6.a(next.b(), "OTHER")) {
                            t4 = next;
                            break;
                        }
                    }
                    T t7 = t4;
                    if (t7 == null || (a3 = t7.a()) == null) {
                        t5 = null;
                    } else {
                        Iterator<T> it2 = a3.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                t6 = null;
                                break;
                            }
                            T next2 = it2.next();
                            if (Wg6.a(next2.a(), "SWITCH HEART RATE MODE")) {
                                t6 = next2;
                                break;
                            }
                        }
                        t5 = t6;
                    }
                    Xs5 xs5 = (Xs5) t5;
                    if (xs5 != null) {
                        xs5.g(this.a.X().name());
                    }
                    this.a.Q().notifyDataSetChanged();
                } else {
                    DebugActivity debugActivity = this.a;
                    HeartRateMode r = debugActivity.a0().r(PortfolioApp.get.instance().J());
                    Wg6.b(r, "mSharedPreferencesManage\u2026tance.activeDeviceSerial)");
                    debugActivity.e0(r);
                }
                this.a.t();
            } else if (communicateMode == CommunicateMode.SET_FRONT_LIGHT_ENABLE) {
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.a.a0().g1(this.a.Y());
                } else {
                    DebugActivity debugActivity2 = this.a;
                    debugActivity2.f0(debugActivity2.a0().f0(PortfolioApp.get.instance().J()));
                    Iterator<T> it3 = this.a.S().iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            t = null;
                            break;
                        }
                        T next3 = it3.next();
                        if (Wg6.a(next3.b(), "OTHER")) {
                            t = next3;
                            break;
                        }
                    }
                    T t8 = t;
                    if (t8 == null || (a2 = t8.a()) == null) {
                        t2 = null;
                    } else {
                        Iterator<T> it4 = a2.iterator();
                        while (true) {
                            if (!it4.hasNext()) {
                                t3 = null;
                                break;
                            }
                            T next4 = it4.next();
                            if (Wg6.a(next4.a(), "FRONT LIGHT ENABLE")) {
                                t3 = next4;
                                break;
                            }
                        }
                        t2 = t3;
                    }
                    Ws5 ws5 = (Ws5) t2;
                    if (ws5 != null) {
                        ws5.g(this.a.Y());
                    }
                    this.a.Q().notifyDataSetChanged();
                }
                this.a.t();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.debug.DebugActivity$onCreate$4", f = "DebugActivity.kt", l = {}, m = "invokeSuspend")
    public static final class f extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends Ko7 implements Hg6<Xe6<? super List<? extends DebugFirmwareData>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(Xe6 xe6, f fVar) {
                super(1, xe6);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                return new a(xe6, this.this$0);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Hg6
            public final Object invoke(Xe6<? super List<? extends DebugFirmwareData>> xe6) {
                throw null;
                //return ((a) create(xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    DebugActivity debugActivity = this.this$0.this$0;
                    String str = debugActivity.K;
                    this.label = 1;
                    Object c0 = debugActivity.c0(str, this);
                    return c0 == d ? d : c0;
                } else if (i == 1) {
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(DebugActivity debugActivity, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = debugActivity;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            f fVar = new f(this.this$0, this.$activeDeviceSerial, xe6);
            fVar.p$ = (Il6) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((f) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                Device deviceBySerial = this.this$0.n().getDeviceBySerial(this.$activeDeviceSerial);
                if (deviceBySerial != null) {
                    DebugActivity debugActivity = this.this$0;
                    String sku = deviceBySerial.getSku();
                    if (sku == null) {
                        sku = "";
                    }
                    debugActivity.K = sku;
                    Firmware k = this.this$0.a0().k(this.this$0.K);
                    if (k != null) {
                        DebugActivity.M(this.this$0).f(k);
                    }
                    if (!DebugActivity.M(this.this$0).c()) {
                        DebugActivity.M(this.this$0).d(new a(null, this));
                    }
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements Ls0<Firmware> {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;

        @DexIgnore
        public g(DebugActivity debugActivity) {
            this.a = debugActivity;
        }

        @DexIgnore
        public final void a(Firmware firmware) {
            T t;
            ArrayList<Er4> a2;
            T t2;
            T t3 = null;
            if (firmware != null) {
                Iterator<T> it = this.a.S().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (Wg6.a(next.b(), "OTHER")) {
                        t = next;
                        break;
                    }
                }
                T t4 = t;
                if (!(t4 == null || (a2 = t4.a()) == null)) {
                    Iterator<T> it2 = a2.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            t2 = null;
                            break;
                        }
                        T next2 = it2.next();
                        if (Wg6.a(next2.a(), "CONSIDER AS LATEST BUNDLE FIRMWARE")) {
                            t2 = next2;
                            break;
                        }
                    }
                    t3 = t2;
                }
                if (t3 != null) {
                    t3.e("CONSIDER AS LATEST BUNDLE FIRMWARE: " + firmware.getVersionNumber());
                }
                this.a.Q().notifyDataSetChanged();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Firmware firmware) {
            a(firmware);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements Ls0<List<? extends DebugFirmwareData>> {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;

        @DexIgnore
        public h(DebugActivity debugActivity) {
            this.a = debugActivity;
        }

        @DexIgnore
        public final void a(List<DebugFirmwareData> list) {
            T t;
            ArrayList<Er4> a2;
            ArrayList<Er4> a3;
            if (list != null && (!list.isEmpty())) {
                Iterator<T> it = this.a.S().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (Wg6.a(next.b(), "FIRMWARE")) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (!(t2 == null || (a3 = t2.a()) == null)) {
                    a3.clear();
                }
                for (T t3 : list) {
                    Firmware firmware = t3.getFirmware();
                    int state = t3.getState();
                    String str = state != 1 ? state != 2 ? "" : "Downloaded" : "Downloading";
                    String versionNumber = firmware.getVersionNumber();
                    Wg6.b(versionNumber, "firmware.versionNumber");
                    Xs5 xs5 = new Xs5("FIRMWARE", versionNumber, str);
                    xs5.c(t3);
                    if (!(t2 == null || (a2 = t2.a()) == null)) {
                        a2.add(xs5);
                    }
                }
                this.a.Q().notifyDataSetChanged();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends DebugFirmwareData> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$1", f = "DebugActivity.kt", l = {Action.Presenter.PREVIOUS, Action.Presenter.BLACKOUT, 304}, m = "invokeSuspend")
    public static final class i extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$1$1", f = "DebugActivity.kt", l = {}, m = "invokeSuspend")
        public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(i iVar, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                a aVar = new a(this.this$0, xe6);
                aVar.p$ = (Il6) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    Toast.makeText(this.this$0.this$0, "Log will be sent after 1 second", 1).show();
                    this.this$0.this$0.finish();
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(DebugActivity debugActivity, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = debugActivity;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            i iVar = new i(this.this$0, xe6);
            iVar.p$ = (Il6) obj;
            throw null;
            //return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((i) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0053  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 3
                r5 = 2
                r4 = 1
                java.lang.Object r1 = com.fossil.Yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0055
                if (r0 == r4) goto L_0x0040
                if (r0 == r5) goto L_0x0023
                if (r0 != r6) goto L_0x001b
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
            L_0x0018:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x001a:
                return r0
            L_0x001b:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0023:
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
            L_0x002a:
                com.fossil.Jx7 r2 = com.fossil.Bw7.c()
                com.portfolio.platform.ui.debug.DebugActivity$i$a r3 = new com.portfolio.platform.ui.debug.DebugActivity$i$a
                r4 = 0
                r3.<init>(r7, r4)
                r7.L$0 = r0
                r7.label = r6
                java.lang.Object r0 = com.fossil.Eu7.g(r2, r3, r7)
                if (r0 != r1) goto L_0x0018
                r0 = r1
                goto L_0x001a
            L_0x0040:
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
            L_0x0047:
                r7.L$0 = r0
                r7.label = r5
                r2 = 1000(0x3e8, double:4.94E-321)
                java.lang.Object r2 = com.fossil.Uv7.a(r2, r7)
                if (r2 != r1) goto L_0x002a
                r0 = r1
                goto L_0x001a
            L_0x0055:
                com.fossil.El7.b(r8)
                com.mapped.Il6 r0 = r7.p$
                com.portfolio.platform.ui.debug.DebugActivity r2 = r7.this$0
                com.portfolio.platform.service.ShakeFeedbackService r2 = r2.Z()
                com.portfolio.platform.ui.debug.DebugActivity r3 = r7.this$0
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r2 = r2.t(r3, r7)
                if (r2 != r1) goto L_0x0047
                r0 = r1
                goto L_0x001a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.debug.DebugActivity.i.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity b;
        @DexIgnore
        public /* final */ /* synthetic */ EditText c;
        @DexIgnore
        public /* final */ /* synthetic */ EditText d;
        @DexIgnore
        public /* final */ /* synthetic */ EditText e;
        @DexIgnore
        public /* final */ /* synthetic */ EditText f;
        @DexIgnore
        public /* final */ /* synthetic */ Ve0 g;

        @DexIgnore
        public j(DebugActivity debugActivity, EditText editText, EditText editText2, EditText editText3, EditText editText4, Ve0 ve0) {
            this.b = debugActivity;
            this.c = editText;
            this.d = editText2;
            this.e = editText3;
            this.f = editText4;
            this.g = ve0;
        }

        @DexIgnore
        public final void onClick(View view) {
            int i;
            int i2;
            int i3;
            int i4;
            EditText editText = this.c;
            Wg6.b(editText, "etDelay");
            String obj = editText.getText().toString();
            EditText editText2 = this.d;
            Wg6.b(editText2, "etDuration");
            String obj2 = editText2.getText().toString();
            EditText editText3 = this.e;
            Wg6.b(editText3, "etRepeat");
            String obj3 = editText3.getText().toString();
            EditText editText4 = this.f;
            Wg6.b(editText4, "etDelayEachTime");
            String obj4 = editText4.getText().toString();
            try {
                i = Integer.parseInt(obj);
            } catch (Exception e2) {
                i = 0;
            }
            try {
                i2 = Integer.parseInt(obj2);
            } catch (Exception e3) {
                i2 = 0;
            }
            try {
                i3 = Integer.parseInt(obj3);
            } catch (Exception e4) {
                i3 = 0;
            }
            try {
                i4 = Integer.parseInt(obj4);
            } catch (Exception e5) {
                i4 = 0;
            }
            if (i > 65535 || i2 > 65535 || i3 > 65535 || i4 > 65535) {
                Toast.makeText(this.b, "Value should be in range [0, 65535]", 0).show();
                return;
            }
            PortfolioApp.get.instance().R1(i, i2, i3, i4);
            this.g.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity b;
        @DexIgnore
        public /* final */ /* synthetic */ EditText c;

        @DexIgnore
        public k(DebugActivity debugActivity, EditText editText) {
            this.b = debugActivity;
            this.c = editText;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            String obj = this.c.getText().toString();
            if (obj != null) {
                Integer valueOf = Integer.valueOf(Wt7.u0(obj).toString());
                Wg6.b(valueOf, "Integer.valueOf(input.text.toString().trim())");
                int intValue = valueOf.intValue();
                this.b.a0().Q1(intValue);
                for (T t : this.b.S()) {
                    if (Wg6.a(t.b(), "SIMULATION")) {
                        for (T t2 : t.a()) {
                            if (Wg6.a(t2.d(), "TRIGGER LOW BATTERY EVENT")) {
                                if (t2 != null) {
                                    Hr7 hr7 = Hr7.a;
                                    String format = String.format("Battery level: %d", Arrays.copyOf(new Object[]{Integer.valueOf(intValue)}, 1));
                                    Wg6.b(format, "java.lang.String.format(format, *args)");
                                    t2.g(format);
                                    this.b.Q().notifyDataSetChanged();
                                    return;
                                }
                                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithText");
                            }
                        }
                        throw new NoSuchElementException("Collection contains no element matching the predicate.");
                    }
                }
                throw new NoSuchElementException("Collection contains no element matching the predicate.");
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Ve0 b;

        @DexIgnore
        public l(Ve0 ve0) {
            this.b = ve0;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0065, code lost:
            if (com.mapped.Wg6.d(java.lang.Integer.valueOf(com.fossil.Wt7.u0(r3).toString()).intValue(), 100) <= 0) goto L_0x0067;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void afterTextChanged(android.text.Editable r6) {
            /*
                r5 = this;
                r3 = -1
                r0 = 1
                r1 = 0
                java.lang.String r2 = "s"
                com.mapped.Wg6.c(r6, r2)
                int r2 = r6.length()
                if (r2 != 0) goto L_0x0020
                r2 = r0
            L_0x000f:
                if (r2 == 0) goto L_0x0022
                com.fossil.Ve0 r0 = r5.b
                android.widget.Button r0 = r0.e(r3)
                java.lang.String r2 = "dialog.getButton(AlertDialog.BUTTON_POSITIVE)"
                com.mapped.Wg6.b(r0, r2)
                r0.setEnabled(r1)
            L_0x001f:
                return
            L_0x0020:
                r2 = r1
                goto L_0x000f
            L_0x0022:
                com.fossil.Ve0 r2 = r5.b
                android.widget.Button r2 = r2.e(r3)
                java.lang.String r3 = "dialog.getButton(AlertDialog.BUTTON_POSITIVE)"
                com.mapped.Wg6.b(r2, r3)
                java.lang.String r3 = r6.toString()
                if (r3 == 0) goto L_0x0075
                java.lang.CharSequence r3 = com.fossil.Wt7.u0(r3)
                java.lang.String r3 = r3.toString()
                java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
                int r3 = r3.intValue()
                int r3 = com.mapped.Wg6.d(r3, r0)
                if (r3 < 0) goto L_0x0073
                java.lang.String r3 = r6.toString()
                if (r3 == 0) goto L_0x006b
                java.lang.CharSequence r3 = com.fossil.Wt7.u0(r3)
                java.lang.String r3 = r3.toString()
                java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
                int r3 = r3.intValue()
                r4 = 100
                int r3 = com.mapped.Wg6.d(r3, r4)
                if (r3 > 0) goto L_0x0073
            L_0x0067:
                r2.setEnabled(r0)
                goto L_0x001f
            L_0x006b:
                com.mapped.Rc6 r0 = new com.mapped.Rc6
                java.lang.String r1 = "null cannot be cast to non-null type kotlin.CharSequence"
                r0.<init>(r1)
                throw r0
            L_0x0073:
                r0 = r1
                goto L_0x0067
            L_0x0075:
                com.mapped.Rc6 r0 = new com.mapped.Rc6
                java.lang.String r1 = "null cannot be cast to non-null type kotlin.CharSequence"
                r0.<init>(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.debug.DebugActivity.l.afterTextChanged(android.text.Editable):void");
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Wg6.c(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Wg6.c(charSequence, "s");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Firmware b;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity c;
        @DexIgnore
        public /* final */ /* synthetic */ DebugFirmwareData d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ m this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super File>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(a aVar, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    a aVar = new a(this.this$0, xe6);
                    aVar.p$ = (Il6) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super File> xe6) {
                    throw null;
                    //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        FirmwareFileRepository V = this.this$0.this$0.c.V();
                        String versionNumber = this.this$0.this$0.b.getVersionNumber();
                        Wg6.b(versionNumber, "firmware.versionNumber");
                        String downloadUrl = this.this$0.this$0.b.getDownloadUrl();
                        Wg6.b(downloadUrl, "firmware.downloadUrl");
                        String checksum = this.this$0.this$0.b.getChecksum();
                        Wg6.b(checksum, "firmware.checksum");
                        this.L$0 = il6;
                        this.label = 1;
                        Object downloadFirmware = V.downloadFirmware(versionNumber, downloadUrl, checksum, this);
                        return downloadFirmware == d ? d : downloadFirmware;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(m mVar, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = mVar;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                a aVar = new a(this.this$0, xe6);
                aVar.p$ = (Il6) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                DebugFirmwareData debugFirmwareData;
                int i;
                Object d = Yn7.d();
                int i2 = this.label;
                if (i2 == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Dv7 a2 = Bw7.a();
                    a aVar = new a(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    g = Eu7.g(a2, aVar, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i2 == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (((File) g) != null) {
                    debugFirmwareData = this.this$0.d;
                    i = 2;
                } else {
                    debugFirmwareData = this.this$0.d;
                    i = 0;
                }
                debugFirmwareData.setState(i);
                DebugActivity.M(this.this$0.c).e();
                return Cd6.a;
            }
        }

        @DexIgnore
        public m(Firmware firmware, DebugActivity debugActivity, DebugFirmwareData debugFirmwareData) {
            this.b = firmware;
            this.c = debugActivity;
            this.d = debugFirmwareData;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new a(this, null), 3, null);
            this.d.setState(1);
            DebugActivity.M(this.c).e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements DialogInterface.OnClickListener {
        @DexIgnore
        public static /* final */ n b; // = new n();

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity b;
        @DexIgnore
        public /* final */ /* synthetic */ Firmware c;

        @DexIgnore
        public o(DebugActivity debugActivity, Firmware firmware) {
            this.b = debugActivity;
            this.c = firmware;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            this.b.d0(this.c);
            dialogInterface.dismiss();
            this.b.finish();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements DialogInterface.OnClickListener {
        @DexIgnore
        public static /* final */ p b; // = new p();

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity b;
        @DexIgnore
        public /* final */ /* synthetic */ Firmware c;

        @DexIgnore
        public q(DebugActivity debugActivity, Firmware firmware) {
            this.b = debugActivity;
            this.c = firmware;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            this.b.a0().Q0(this.c, this.b.K);
            DebugActivity.M(this.b).f(this.c);
            dialogInterface.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r implements DialogInterface.OnClickListener {
        @DexIgnore
        public static /* final */ r b; // = new r();

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore
    public static final /* synthetic */ FirmwareDebugViewModel M(DebugActivity debugActivity) {
        FirmwareDebugViewModel firmwareDebugViewModel = debugActivity.J;
        if (firmwareDebugViewModel != null) {
            return firmwareDebugViewModel;
        }
        Wg6.n("mFirmwareViewModel");
        throw null;
    }

    @DexIgnore
    public final Lc6<String, Long> P(Date date) {
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "calendar");
        instance.setTime(date);
        long timeInMillis = instance.getTimeInMillis();
        switch (instance.get(7)) {
            case 1:
                return new Lc6<>(Um5.c(PortfolioApp.get.instance(), 2131886770), Long.valueOf(timeInMillis));
            case 2:
                return new Lc6<>(Um5.c(PortfolioApp.get.instance(), 2131886769), Long.valueOf(timeInMillis));
            case 3:
                return new Lc6<>(Um5.c(PortfolioApp.get.instance(), 2131886772), Long.valueOf(timeInMillis));
            case 4:
                return new Lc6<>(Um5.c(PortfolioApp.get.instance(), 2131886774), Long.valueOf(timeInMillis));
            case 5:
                return new Lc6<>(Um5.c(PortfolioApp.get.instance(), 2131886773), Long.valueOf(timeInMillis));
            case 6:
                return new Lc6<>(Um5.c(PortfolioApp.get.instance(), 2131886768), Long.valueOf(timeInMillis));
            case 7:
                return new Lc6<>(Um5.c(PortfolioApp.get.instance(), 2131886771), Long.valueOf(timeInMillis));
            default:
                return new Lc6<>(Um5.c(PortfolioApp.get.instance(), 2131886770), Long.valueOf(timeInMillis));
        }
    }

    @DexIgnore
    public final Ss5 Q() {
        return this.H;
    }

    @DexIgnore
    public final LinkedList<Lc6<String, Long>> R() {
        Calendar instance = Calendar.getInstance();
        LinkedList<Lc6<String, Long>> linkedList = new LinkedList<>();
        for (int i2 = 0; i2 <= 31; i2++) {
            Wg6.b(instance, "calendar");
            Date time = instance.getTime();
            Wg6.b(time, "calendar.time");
            linkedList.add(P(time));
            instance.add(5, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String r2 = r();
            local.d(r2, "getListDay - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.BaseActivity, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        if (Wg6.a(str, "CONFIRM_CLEAR_DATA") && i2 == 2131362279) {
            DebugClearDataWarningActivity.d(this);
        } else if (Wg6.a(str, "SWITCH HEART RATE MODE") && i2 == 2131362279 && intent != null && intent.hasExtra("EXTRA_RADIO_GROUPS_RESULTS")) {
            HashMap hashMap = (HashMap) intent.getSerializableExtra("EXTRA_RADIO_GROUPS_RESULTS");
        }
    }

    @DexIgnore
    public final ArrayList<Ys5> S() {
        return this.I;
    }

    @DexIgnore
    public final LinkedList<Lc6<String, Long>> T() {
        Calendar instance = Calendar.getInstance();
        LinkedList<Lc6<String, Long>> linkedList = new LinkedList<>();
        for (int i2 = 0; i2 <= 12; i2++) {
            Wg6.b(instance, "calendar");
            Date time = instance.getTime();
            Wg6.b(time, "calendar.time");
            linkedList.add(P(time));
            instance.add(2, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String r2 = r();
            local.d(r2, "getListMonth - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    public final LinkedList<Lc6<String, Long>> U() {
        Calendar instance = Calendar.getInstance();
        LinkedList<Lc6<String, Long>> linkedList = new LinkedList<>();
        for (int i2 = 0; i2 <= 52; i2++) {
            Wg6.b(instance, "calendar");
            Date time = instance.getTime();
            Wg6.b(time, "calendar.time");
            linkedList.add(b0(time));
            instance.add(5, -7);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String r2 = r();
            local.d(r2, "getListWeek - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    public final FirmwareFileRepository V() {
        FirmwareFileRepository firmwareFileRepository = this.C;
        if (firmwareFileRepository != null) {
            return firmwareFileRepository;
        }
        Wg6.n("mFirmwareFileRepository");
        throw null;
    }

    @DexIgnore
    public final GuestApiService W() {
        GuestApiService guestApiService = this.D;
        if (guestApiService != null) {
            return guestApiService;
        }
        Wg6.n("mGuestApiService");
        throw null;
    }

    @DexIgnore
    public final HeartRateMode X() {
        HeartRateMode heartRateMode = this.L;
        if (heartRateMode != null) {
            return heartRateMode;
        }
        Wg6.n("mHeartRateMode");
        throw null;
    }

    @DexIgnore
    public final boolean Y() {
        return this.M;
    }

    @DexIgnore
    public final ShakeFeedbackService Z() {
        ShakeFeedbackService shakeFeedbackService = this.F;
        if (shakeFeedbackService != null) {
            return shakeFeedbackService;
        }
        Wg6.n("mShakeFeedbackService");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ss5.Bi
    public void a(String str, int i2, int i3, Object obj, Bundle bundle) {
        DebugFirmwareData debugFirmwareData;
        Wg6.c(str, "tagName");
        switch (str.hashCode()) {
            case -2017027426:
                if (str.equals("BLUETOOTH SETTING")) {
                    Intent intent = new Intent();
                    intent.setAction("android.settings.BLUETOOTH_SETTINGS");
                    startActivity(intent);
                    return;
                }
                return;
            case -1935069302:
                if (str.equals("WEATHER_WATCH_APP_TAP")) {
                    PortfolioApp.inner inner = PortfolioApp.get;
                    inner.g(new Vi5(inner.instance().J(), E90.WEATHER_WATCH_APP.ordinal(), new Bundle()));
                    return;
                }
                return;
            case -1822588397:
                if (str.equals("TRIGGER LOW BATTERY EVENT")) {
                    Ve0.Ai ai = new Ve0.Ai(this);
                    EditText editText = new EditText(this);
                    editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                    editText.setInputType(2);
                    ai.p(editText);
                    ai.o("Custom dialog");
                    ai.g("Enter battery level between 1-100");
                    ai.l("Done", new k(this, editText));
                    Ve0 a2 = ai.a();
                    Wg6.b(a2, "dialogBuilder.create()");
                    a2.show();
                    Button e2 = a2.e(-1);
                    Wg6.b(e2, "dialog.getButton(AlertDialog.BUTTON_POSITIVE)");
                    e2.setEnabled(false);
                    editText.addTextChangedListener(new l(a2));
                    return;
                }
                return;
            case -1367489317:
                if (str.equals("SKIP OTA")) {
                    An4 an4 = this.A;
                    if (an4 != null) {
                        Boolean valueOf = bundle != null ? Boolean.valueOf(bundle.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf != null) {
                            an4.S1(valueOf.booleanValue());
                            return;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.n("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -1276486892:
                if (str.equals("DISABLE AUTO SYNC")) {
                    An4 an42 = this.A;
                    if (an42 != null) {
                        Boolean valueOf2 = bundle != null ? Boolean.valueOf(bundle.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf2 != null) {
                            an42.N0(!valueOf2.booleanValue());
                            return;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.n("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -907588345:
                if (str.equals("CONSIDER AS LATEST BUNDLE FIRMWARE")) {
                    An4 an43 = this.A;
                    if (an43 != null) {
                        Boolean valueOf3 = bundle != null ? Boolean.valueOf(bundle.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf3 != null) {
                            an43.S0(valueOf3.booleanValue());
                            return;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.n("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -833549689:
                if (str.equals("RESET_DELAY_OTA_TIMESTAMP")) {
                    An4 an44 = this.A;
                    if (an44 != null) {
                        an44.X0(PortfolioApp.get.instance().J(), -1);
                        return;
                    } else {
                        Wg6.n("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -558521180:
                if (str.equals("FRONT LIGHT ENABLE") && bundle != null && bundle.containsKey("DEBUG_BUNDLE_IS_CHECKED")) {
                    boolean z = bundle.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false);
                    F();
                    if (PortfolioApp.get.instance().w1(PortfolioApp.get.instance().J(), z) == ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD()) {
                        t();
                        return;
                    }
                    return;
                }
                return;
            case -276828739:
                if (str.equals("CLEAR DATA")) {
                    AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558479);
                    fi.e(2131362384, "ARE YOU SURE?");
                    fi.e(2131362383, "** This will NOT happen on production **");
                    fi.e(2131362279, "OK");
                    fi.b(2131362279);
                    AlertDialogFragment f2 = fi.f("CONFIRM_CLEAR_DATA");
                    Wg6.b(f2, "AlertDialogFragment.Buil\u2026Utils.CONFIRM_CLEAR_DATA)");
                    f2.setCancelable(true);
                    f2.setStyle(0, 2131951933);
                    f2.show(getSupportFragmentManager(), "CONFIRM_CLEAR_DATA");
                    return;
                }
                return;
            case -75588064:
                if (str.equals("GENERATE PRESET DATA")) {
                    R();
                    U();
                    T();
                    return;
                }
                return;
            case -62411653:
                if (str.equals("SHOW ALL DEVICES")) {
                    An4 an45 = this.A;
                    if (an45 != null) {
                        Boolean valueOf4 = bundle != null ? Boolean.valueOf(bundle.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf4 != null) {
                            an45.R1(valueOf4.booleanValue());
                            DeviceHelper.o.j().l();
                            return;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.n("mSharedPreferencesManager");
                    throw null;
                }
                return;
            case 2560667:
                if (str.equals("SYNC")) {
                    PortfolioApp instance = PortfolioApp.get.instance();
                    Cj4 cj4 = this.G;
                    if (cj4 != null) {
                        instance.S1(cj4, false, 12);
                        return;
                    } else {
                        Wg6.n("mDeviceSettingFactory");
                        throw null;
                    }
                } else {
                    return;
                }
            case 227289531:
                if (str.equals("FIRMWARE") && (debugFirmwareData = (DebugFirmwareData) obj) != null) {
                    if (debugFirmwareData.getState() == 2) {
                        h0(debugFirmwareData.getFirmware());
                        return;
                    } else if (debugFirmwareData.getState() == 0) {
                        g0(debugFirmwareData);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 334768719:
                if (str.equals("RESET DEVICE SETTING IN FIRMWARE TO DEFAULT")) {
                    PortfolioApp.get.instance().c1();
                    return;
                }
                return;
            case 345524076:
                if (str.equals("CUSTOMIZE THEME")) {
                    ThemesActivity.A.a(this);
                    return;
                }
                return;
            case 437897810:
                if (str.equals("FORCE_BACKGROUND_REQUEST") && bundle != null && bundle.containsKey("DEBUG_BUNDLE_SPINNER_SELECTED_POS")) {
                    int i4 = bundle.getInt("DEBUG_BUNDLE_SPINNER_SELECTED_POS");
                    List b2 = Ir7.b(obj);
                    Object obj2 = b2 != null ? b2.get(i4) : null;
                    if (!(obj2 instanceof DebugForceBackgroundRequestData)) {
                        obj2 = null;
                    }
                    DebugForceBackgroundRequestData debugForceBackgroundRequestData = (DebugForceBackgroundRequestData) obj2;
                    if (debugForceBackgroundRequestData != null) {
                        PortfolioApp.get.instance().f1(PortfolioApp.get.instance().J(), new ForceBackgroundRequest(debugForceBackgroundRequestData.getBackgroundRequestType()));
                        return;
                    }
                    return;
                }
                return;
            case 596314607:
                if (str.equals("DISABLE HW_LOG SYNC")) {
                    An4 an46 = this.A;
                    if (an46 != null) {
                        Boolean valueOf5 = bundle != null ? Boolean.valueOf(bundle.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf5 != null) {
                            an46.i1(!valueOf5.booleanValue());
                            return;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.n("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case 967694544:
                if (str.equals("SIMULATE DISCONNECTION")) {
                    Ve0.Ai ai2 = new Ve0.Ai(this);
                    Object systemService = getSystemService("layout_inflater");
                    if (systemService != null) {
                        View inflate = ((LayoutInflater) systemService).inflate(2131558819, (ViewGroup) null);
                        ai2.p(inflate);
                        Ve0 a3 = ai2.a();
                        Wg6.b(a3, "builder.create()");
                        a3.show();
                        EditText editText2 = (EditText) inflate.findViewById(2131362230);
                        EditText editText3 = (EditText) inflate.findViewById(2131362232);
                        EditText editText4 = (EditText) inflate.findViewById(2131362242);
                        EditText editText5 = (EditText) inflate.findViewById(2131362231);
                        FlexibleButton flexibleButton = (FlexibleButton) inflate.findViewById(2131361972);
                        Wg6.b(editText2, "etDelay");
                        double d2 = (double) 0;
                        double d3 = (double) 65535;
                        editText2.setFilters(new InputFilter[]{new b(d2, d3)});
                        Wg6.b(editText3, "etDuration");
                        editText3.setFilters(new InputFilter[]{new b(d2, d3)});
                        Wg6.b(editText4, "etRepeat");
                        editText4.setFilters(new InputFilter[]{new b(d2, d3)});
                        Wg6.b(editText5, "etDelayEachTime");
                        editText5.setFilters(new InputFilter[]{new b(d2, (double) 4294967)});
                        flexibleButton.setOnClickListener(new j(this, editText2, editText3, editText4, editText5, a3));
                        return;
                    }
                    throw new Rc6("null cannot be cast to non-null type android.view.LayoutInflater");
                }
                return;
            case 999402770:
                if (str.equals("WATCH APP MUSIC CONTROL")) {
                    finish();
                    return;
                }
                return;
            case 1053818587:
                if (str.equals("APPLY NEW NOTIFICATION FILTER")) {
                    An4 an47 = this.A;
                    if (an47 != null) {
                        Boolean valueOf6 = bundle != null ? Boolean.valueOf(bundle.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf6 != null) {
                            an47.J1(valueOf6.booleanValue());
                            return;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.n("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case 1484036056:
                if (str.equals("RESET UAPP LOG FILES")) {
                    MicroAppEventLogger.resetLogFiles();
                    return;
                }
                return;
            case 1642625733:
                if (str.equals("GENERATE HEART RATE DATA")) {
                    finish();
                    return;
                }
                return;
            case 1977879337:
                if (str.equals("VIEW LOG")) {
                    LogcatActivity.N(this);
                    return;
                }
                return;
            case 2029483660:
                if (str.equals("SEND LOG")) {
                    Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new i(this, null), 3, null);
                    return;
                }
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final An4 a0() {
        An4 an4 = this.A;
        if (an4 != null) {
            return an4;
        }
        Wg6.n("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ss5.Bi
    public void b(String str, int i2, int i3, Object obj, Bundle bundle) {
        DebugFirmwareData debugFirmwareData;
        Wg6.c(str, "tagName");
        if (str.hashCode() == 227289531 && str.equals("FIRMWARE") && (debugFirmwareData = (DebugFirmwareData) obj) != null) {
            FirmwareFileRepository firmwareFileRepository = this.C;
            if (firmwareFileRepository != null) {
                String versionNumber = debugFirmwareData.getFirmware().getVersionNumber();
                Wg6.b(versionNumber, "it.firmware.versionNumber");
                String checksum = debugFirmwareData.getFirmware().getChecksum();
                Wg6.b(checksum, "it.firmware.checksum");
                if (firmwareFileRepository.isDownloaded(versionNumber, checksum)) {
                    i0(debugFirmwareData.getFirmware());
                } else {
                    FLogger.INSTANCE.getLocal().d(r(), "Firmware hasn't been downloaded. Could not be set as latest firmware.");
                }
            } else {
                Wg6.n("mFirmwareFileRepository");
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r1v2 int), ('-' char), (r0v2 int)] */
    public final Lc6<String, Long> b0(Date date) {
        Calendar instance = Calendar.getInstance(Locale.US);
        Wg6.b(instance, "calendar");
        instance.setTime(date);
        instance.set(7, instance.getFirstDayOfWeek());
        int i2 = instance.get(5);
        instance.set(7, instance.getFirstDayOfWeek() + 6);
        int i3 = instance.get(5);
        StringBuilder sb = new StringBuilder();
        sb.append(i2);
        sb.append('-');
        sb.append(i3);
        return new Lc6<>(sb.toString(), Long.valueOf(date.getTime()));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object c0(java.lang.String r11, com.mapped.Xe6<? super java.util.List<com.portfolio.platform.data.model.DebugFirmwareData>> r12) {
        /*
        // Method dump skipped, instructions count: 309
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.debug.DebugActivity.c0(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void d0(Firmware firmware) {
        String J2 = PortfolioApp.get.instance().J();
        FirmwareData createFirmwareData = FirmwareFactory.getInstance().createFirmwareData(firmware.getVersionNumber(), firmware.getDeviceModel(), firmware.getChecksum());
        Wg6.b(createFirmwareData, "FirmwareFactory.getInsta\u2026Model, firmware.checksum)");
        UpdateDeviceToSpecificFirmwareUsecase.Ai ai = new UpdateDeviceToSpecificFirmwareUsecase.Ai(J2, createFirmwareData);
        UpdateDeviceToSpecificFirmwareUsecase updateDeviceToSpecificFirmwareUsecase = this.B;
        if (updateDeviceToSpecificFirmwareUsecase != null) {
            updateDeviceToSpecificFirmwareUsecase.e(ai, null);
        } else {
            Wg6.n("mOTAToSpecificFirmwareUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void e0(HeartRateMode heartRateMode) {
        Wg6.c(heartRateMode, "<set-?>");
        this.L = heartRateMode;
    }

    @DexIgnore
    public final void f0(boolean z) {
        this.M = z;
    }

    @DexIgnore
    public final void g0(DebugFirmwareData debugFirmwareData) {
        Ve0.Ai ai = new Ve0.Ai(this);
        Firmware firmware = debugFirmwareData.getFirmware();
        ai.o("Confirm Download");
        ai.g("Are you sure you want download to firmware " + firmware.getVersionNumber() + '?');
        ai.l("Confirm", new m(firmware, this, debugFirmwareData));
        ai.h("Cancel", n.b);
        ai.a();
        ai.q();
    }

    @DexIgnore
    public final void h0(Firmware firmware) {
        Ve0.Ai ai = new Ve0.Ai(this);
        ai.o("Confirm OTA");
        ai.g("Are you sure you want OTA to firmware " + firmware.getVersionNumber() + '?');
        ai.l("Confirm", new o(this, firmware));
        ai.h("Cancel", p.b);
        ai.a();
        ai.q();
    }

    @DexIgnore
    public final void i0(Firmware firmware) {
        Ve0.Ai ai = new Ve0.Ai(this);
        ai.o("Confirm Latest Firmware");
        ai.g("Are you sure you want to use firmware " + firmware.getVersionNumber() + " as the bundle latest firmware?");
        ai.l("Confirm", new q(this, firmware));
        ai.h("Cancel", r.b);
        ai.a();
        ai.q();
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558430);
        PortfolioApp.get.instance().getIface().A(this);
        String J2 = PortfolioApp.get.instance().J();
        An4 an4 = this.A;
        if (an4 != null) {
            HeartRateMode r2 = an4.r(J2);
            Wg6.b(r2, "mSharedPreferencesManage\u2026eMode(activeDeviceSerial)");
            this.L = r2;
            An4 an42 = this.A;
            if (an42 != null) {
                this.M = an42.f0(J2);
                ArrayList arrayList = new ArrayList();
                arrayList.add(new Er4("VIEW LOG", "VIEW LOG"));
                arrayList.add(new Er4("SEND LOG", "SEND LOG"));
                arrayList.add(new Er4("RESET UAPP LOG FILES", "RESET UAPP LOG FILES"));
                this.I.add(new Ys5("LOG", "LOG", arrayList, false, 8, null));
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add(new Er4("CLEAR DATA", "CLEAR DATA"));
                arrayList2.add(new Er4("GENERATE PRESET DATA", "GENERATE PRESET DATA"));
                this.I.add(new Ys5("DATA", "DATA", arrayList2, false, 8, null));
                ArrayList arrayList3 = new ArrayList();
                arrayList3.add(new Er4("BLUETOOTH SETTING", "BLUETOOTH SETTING"));
                this.I.add(new Ys5("ANDROID SETTINGS", "ANDROID SETTINGS", arrayList3, false, 8, null));
                ArrayList arrayList4 = new ArrayList();
                arrayList4.add(new Er4("START MINDFULNESS PRACTICE", "START MINDFULNESS PRACTICE"));
                arrayList4.add(new Er4("WATCH APP MUSIC CONTROL", "WATCH APP MUSIC CONTROL"));
                arrayList4.add(new Er4("SIMULATE DISCONNECTION", "SIMULATE DISCONNECTION"));
                Hr7 hr7 = Hr7.a;
                An4 an43 = this.A;
                if (an43 != null) {
                    String format = String.format("Battery level: %d", Arrays.copyOf(new Object[]{Integer.valueOf(an43.M())}, 1));
                    Wg6.b(format, "java.lang.String.format(format, *args)");
                    arrayList4.add(new Xs5("TRIGGER LOW BATTERY EVENT", "TRIGGER LOW BATTERY EVENT", format));
                    this.I.add(new Ys5("SIMULATION", "SIMULATION", arrayList4, false, 8, null));
                    ArrayList arrayList5 = new ArrayList();
                    arrayList5.add(new Er4("FIRMWARE_LOADING", "FIRMWARE_LOADING"));
                    this.I.add(new Ys5("FIRMWARE", "FIRMWARE", arrayList5, false, 8, null));
                    ArrayList arrayList6 = new ArrayList();
                    arrayList6.add(new Er4("SYNC", "SYNC"));
                    List<DebugForceBackgroundRequestData> i2 = Hm7.i(new DebugForceBackgroundRequestData("Notification Filter Background", ForceBackgroundRequest.BackgroundRequestType.SET_NOTIFICATION_FILTER), new DebugForceBackgroundRequestData("Alarms Background", ForceBackgroundRequest.BackgroundRequestType.SET_MULTI_ALARM), new DebugForceBackgroundRequestData("Device Configuration Background", ForceBackgroundRequest.BackgroundRequestType.SET_CONFIG_FILE));
                    ArrayList arrayList7 = new ArrayList(Im7.m(i2, 10));
                    for (DebugForceBackgroundRequestData debugForceBackgroundRequestData : i2) {
                        arrayList7.add(debugForceBackgroundRequestData.getVisibleName());
                    }
                    Vs5 vs5 = new Vs5("FORCE_BACKGROUND_REQUEST", "FORCE_BACKGROUND_REQUEST", Pm7.j0(arrayList7), "Send");
                    vs5.c(i2);
                    Cd6 cd6 = Cd6.a;
                    arrayList6.add(vs5);
                    arrayList6.add(new Er4("WEATHER_WATCH_APP_TAP", "WEATHER_WATCH_APP_TAP"));
                    arrayList6.add(new Er4("RESET DEVICE SETTING IN FIRMWARE TO DEFAULT", "RESET DEVICE SETTING IN FIRMWARE TO DEFAULT"));
                    arrayList6.add(new Er4("RESET_DELAY_OTA_TIMESTAMP", "RESET_DELAY_OTA_TIMESTAMP"));
                    An4 an44 = this.A;
                    if (an44 != null) {
                        arrayList6.add(new Ws5("SKIP OTA", "SKIP OTA", an44.y0()));
                        An4 an45 = this.A;
                        if (an45 != null) {
                            arrayList6.add(new Ws5("SHOW ALL DEVICES", "SHOW ALL DEVICES", an45.x0()));
                            An4 an46 = this.A;
                            if (an46 != null) {
                                arrayList6.add(new Ws5("DISABLE HW_LOG SYNC", "DISABLE HW_LOG SYNC", !an46.h0()));
                                An4 an47 = this.A;
                                if (an47 != null) {
                                    arrayList6.add(new Ws5("DISABLE AUTO SYNC", "DISABLE AUTO SYNC", !an47.X()));
                                    An4 an48 = this.A;
                                    if (an48 != null) {
                                        arrayList6.add(new Ws5("SHOW DISPLAY DEVICE INFO", "SHOW DISPLAY DEVICE INFO", an48.i0()));
                                        An4 an49 = this.A;
                                        if (an49 != null) {
                                            arrayList6.add(new Ws5("CONSIDER AS LATEST BUNDLE FIRMWARE", "CONSIDER AS LATEST BUNDLE FIRMWARE:No Firmware", an49.Y()));
                                            An4 an410 = this.A;
                                            if (an410 != null) {
                                                arrayList6.add(new Ws5("APPLY NEW NOTIFICATION FILTER", "APPLY NEW NOTIFICATION FILTER", an410.q0()));
                                                if (FossilDeviceSerialPatternUtil.isDianaDevice(J2)) {
                                                    arrayList6.add(new Ws5("FRONT LIGHT ENABLE", "FRONT LIGHT ENABLE", this.M));
                                                    HeartRateMode heartRateMode = this.L;
                                                    if (heartRateMode != null) {
                                                        arrayList6.add(new Xs5("SWITCH HEART RATE MODE", "SWITCH HEART RATE MODE", heartRateMode.name()));
                                                    } else {
                                                        Wg6.n("mHeartRateMode");
                                                        throw null;
                                                    }
                                                }
                                                this.I.add(new Ys5("OTHER", "OTHER", arrayList6, false, 8, null));
                                                ArrayList arrayList8 = new ArrayList();
                                                arrayList8.add(new Er4("CUSTOMIZE THEME", "CUSTOMIZE THEME"));
                                                this.I.add(new Ys5("THEME", "THEME", arrayList8, false, 8, null));
                                                Ss5 ss5 = this.H;
                                                ss5.k(this.I);
                                                ss5.l(this);
                                                Cd6 cd62 = Cd6.a;
                                                RecyclerView recyclerView = (RecyclerView) findViewById(2131363033);
                                                recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                                                recyclerView.setAdapter(this.H);
                                                RecyclerView.g adapter = recyclerView.getAdapter();
                                                if (adapter != null) {
                                                    adapter.notifyDataSetChanged();
                                                    Cd6 cd63 = Cd6.a;
                                                    Ts0 a2 = Vs0.e(this).a(FirmwareDebugViewModel.class);
                                                    Wg6.b(a2, "ViewModelProviders.of(th\u2026bugViewModel::class.java)");
                                                    this.J = (FirmwareDebugViewModel) a2;
                                                    h hVar = new h(this);
                                                    FirmwareDebugViewModel firmwareDebugViewModel = this.J;
                                                    if (firmwareDebugViewModel != null) {
                                                        firmwareDebugViewModel.a().h(this, hVar);
                                                        g gVar = new g(this);
                                                        FirmwareDebugViewModel firmwareDebugViewModel2 = this.J;
                                                        if (firmwareDebugViewModel2 != null) {
                                                            firmwareDebugViewModel2.b().h(this, gVar);
                                                            if (!TextUtils.isEmpty(J2)) {
                                                                Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new f(this, J2, null), 3, null);
                                                                return;
                                                            }
                                                            return;
                                                        }
                                                        Wg6.n("mFirmwareViewModel");
                                                        throw null;
                                                    }
                                                    Wg6.n("mFirmwareViewModel");
                                                    throw null;
                                                }
                                                Wg6.i();
                                                throw null;
                                            }
                                            Wg6.n("mSharedPreferencesManager");
                                            throw null;
                                        }
                                        Wg6.n("mSharedPreferencesManager");
                                        throw null;
                                    }
                                    Wg6.n("mSharedPreferencesManager");
                                    throw null;
                                }
                                Wg6.n("mSharedPreferencesManager");
                                throw null;
                            }
                            Wg6.n("mSharedPreferencesManager");
                            throw null;
                        }
                        Wg6.n("mSharedPreferencesManager");
                        throw null;
                    }
                    Wg6.n("mSharedPreferencesManager");
                    throw null;
                }
                Wg6.n("mSharedPreferencesManager");
                throw null;
            }
            Wg6.n("mSharedPreferencesManager");
            throw null;
        }
        Wg6.n("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onPause() {
        super.onPause();
        BleCommandResultManager.d.j(this.N, CommunicateMode.SET_HEART_RATE_MODE, CommunicateMode.SET_FRONT_LIGHT_ENABLE);
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onResume() {
        super.onResume();
        BleCommandResultManager.d.e(this.N, CommunicateMode.SET_HEART_RATE_MODE, CommunicateMode.SET_FRONT_LIGHT_ENABLE);
        BleCommandResultManager.d.g(CommunicateMode.SET_HEART_RATE_MODE, CommunicateMode.SET_FRONT_LIGHT_ENABLE);
    }
}
