package com.portfolio.platform.ui.user.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LoginSocialUseCase extends CoroutineUseCase<Bi, Ci, Ai> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public Ai(int i, String str) {
            Wg6.c(str, "errorMessage");
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public Bi(String str, String str2, String str3) {
            Wg6.c(str, Constants.SERVICE);
            Wg6.c(str2, "token");
            Wg6.c(str3, "clientId");
            this.a = str;
            this.b = str2;
            this.c = str3;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
        @DexIgnore
        public Ci(MFUser.Auth auth) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.user.usecase.LoginSocialUseCase", f = "LoginSocialUseCase.kt", l = {23}, m = "run")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ LoginSocialUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(LoginSocialUseCase loginSocialUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = loginSocialUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, this);
        }
    }

    /*
    static {
        String simpleName = LoginSocialUseCase.class.getSimpleName();
        Wg6.b(simpleName, "LoginSocialUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public LoginSocialUseCase(UserRepository userRepository) {
        Wg6.c(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return e;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return m(bi, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object m(com.portfolio.platform.ui.user.usecase.LoginSocialUseCase.Bi r10, com.mapped.Xe6<java.lang.Object> r11) {
        /*
            r9 = this;
            r8 = 600(0x258, float:8.41E-43)
            r7 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r11 instanceof com.portfolio.platform.ui.user.usecase.LoginSocialUseCase.Di
            if (r0 == 0) goto L_0x0045
            r0 = r11
            com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$Di r0 = (com.portfolio.platform.ui.user.usecase.LoginSocialUseCase.Di) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0045
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0016:
            java.lang.Object r2 = r1.result
            java.lang.Object r4 = com.fossil.Yn7.d()
            int r0 = r1.label
            java.lang.String r3 = ""
            if (r0 == 0) goto L_0x0054
            if (r0 != r7) goto L_0x004c
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$Bi r0 = (com.portfolio.platform.ui.user.usecase.LoginSocialUseCase.Bi) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.ui.user.usecase.LoginSocialUseCase r0 = (com.portfolio.platform.ui.user.usecase.LoginSocialUseCase) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x0030:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x008a
            com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$Ci r1 = new com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$Ci
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.model.MFUser$Auth r0 = (com.portfolio.platform.data.model.MFUser.Auth) r0
            r1.<init>(r0)
            r0 = r1
        L_0x0044:
            return r0
        L_0x0045:
            com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$Di r0 = new com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$Di
            r0.<init>(r9, r11)
            r1 = r0
            goto L_0x0016
        L_0x004c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0054:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.ui.user.usecase.LoginSocialUseCase.e
            java.lang.String r5 = "running UseCase"
            r0.d(r2, r5)
            if (r10 != 0) goto L_0x006e
            com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$Ai r0 = new com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$Ai
            java.lang.String r1 = ""
            r0.<init>(r8, r1)
            goto L_0x0044
        L_0x006e:
            com.portfolio.platform.data.source.UserRepository r0 = r9.d
            java.lang.String r2 = r10.b()
            java.lang.String r5 = r10.c()
            java.lang.String r6 = r10.a()
            r1.L$0 = r9
            r1.L$1 = r10
            r1.label = r7
            java.lang.Object r0 = r0.loginWithSocial(r2, r5, r6, r1)
            if (r0 != r4) goto L_0x0030
            r0 = r4
            goto L_0x0044
        L_0x008a:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00c8
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.ui.user.usecase.LoginSocialUseCase.e
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Inside .run failed with http code="
            r4.append(r5)
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            int r5 = r0.a()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.d(r2, r4)
            int r2 = r0.a()
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00d1
            java.lang.String r0 = r0.getMessage()
            if (r0 == 0) goto L_0x00d1
        L_0x00c0:
            com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$Ai r1 = new com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$Ai
            r1.<init>(r2, r0)
            r0 = r1
            goto L_0x0044
        L_0x00c8:
            com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$Ai r0 = new com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$Ai
            java.lang.String r1 = ""
            r0.<init>(r8, r1)
            goto L_0x0044
        L_0x00d1:
            r0 = r3
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.user.usecase.LoginSocialUseCase.m(com.portfolio.platform.ui.user.usecase.LoginSocialUseCase$Bi, com.mapped.Xe6):java.lang.Object");
    }
}
