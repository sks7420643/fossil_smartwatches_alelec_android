package com.portfolio.platform.ui.user.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DownloadUserInfoUseCase extends CoroutineUseCase<Bi, Ci, Ai> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public Ai(int i, String str) {
            Wg6.c(str, "errorMessage");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ MFUser a;

        @DexIgnore
        public Ci(MFUser mFUser) {
            this.a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase", f = "DownloadUserInfoUseCase.kt", l = {22}, m = "run")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DownloadUserInfoUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(DownloadUserInfoUseCase downloadUserInfoUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = downloadUserInfoUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, this);
        }
    }

    /*
    static {
        String simpleName = DownloadUserInfoUseCase.class.getSimpleName();
        Wg6.b(simpleName, "DownloadUserInfoUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public DownloadUserInfoUseCase(UserRepository userRepository) {
        Wg6.c(userRepository, "userRepository");
        this.d = userRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return e;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return m(bi, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object m(com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase.Bi r7, com.mapped.Xe6<java.lang.Object> r8) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase.Di
            if (r0 == 0) goto L_0x0041
            r0 = r8
            com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$Di r0 = (com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase.Di) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0041
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0050
            if (r0 != r5) goto L_0x0048
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$Bi r0 = (com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase.Bi) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase r0 = (com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x002c:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0070
            com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$Ci r1 = new com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$Ci
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            r1.<init>(r0)
            r0 = r1
        L_0x0040:
            return r0
        L_0x0041:
            com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$Di r0 = new com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$Di
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0014
        L_0x0048:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0050:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase.e
            java.lang.String r4 = "running UseCase"
            r0.d(r2, r4)
            com.portfolio.platform.data.source.UserRepository r0 = r6.d
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r5
            java.lang.Object r0 = r0.loadUserInfo(r1)
            if (r0 != r3) goto L_0x002c
            r0 = r3
            goto L_0x0040
        L_0x0070:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x0090
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            int r2 = r0.a()
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x008d
            java.lang.String r0 = r0.getMessage()
            if (r0 == 0) goto L_0x008d
        L_0x0086:
            com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$Ai r1 = new com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$Ai
            r1.<init>(r2, r0)
            r0 = r1
            goto L_0x0040
        L_0x008d:
            java.lang.String r0 = ""
            goto L_0x0086
        L_0x0090:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase.m(com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase$Bi, com.mapped.Xe6):java.lang.Object");
    }
}
