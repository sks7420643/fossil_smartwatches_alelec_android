package com.portfolio.platform.ui.heartrate.domain.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FetchDailyHeartRateSummaries extends CoroutineUseCase<Ai, CoroutineUseCase.Di, CoroutineUseCase.Ai> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository d;
    @DexIgnore
    public /* final */ UserRepository e;
    @DexIgnore
    public /* final */ FitnessDataRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public Ai(Date date) {
            Wg6.c(date, "date");
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries", f = "FetchDailyHeartRateSummaries.kt", l = {27, 55, 57}, m = "run")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FetchDailyHeartRateSummaries this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries, Xe6 xe6) {
            super(xe6);
            this.this$0 = fetchDailyHeartRateSummaries;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, this);
        }
    }

    /*
    static {
        String simpleName = FetchDailyHeartRateSummaries.class.getSimpleName();
        Wg6.b(simpleName, "FetchDailyHeartRateSumma\u2026es::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public FetchDailyHeartRateSummaries(HeartRateSummaryRepository heartRateSummaryRepository, UserRepository userRepository, FitnessDataRepository fitnessDataRepository) {
        Wg6.c(heartRateSummaryRepository, "mHeartRateSummariesRepository");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(fitnessDataRepository, "mFitnessDataRepository");
        this.d = heartRateSummaryRepository;
        this.e = userRepository;
        this.f = fitnessDataRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return g;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ai ai, Xe6 xe6) {
        return m(ai, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object m(com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries.Ai r21, com.mapped.Xe6<? super com.mapped.Cd6> r22) {
        /*
        // Method dump skipped, instructions count: 516
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries.m(com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries$Ai, com.mapped.Xe6):java.lang.Object");
    }
}
