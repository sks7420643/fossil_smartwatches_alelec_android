package com.portfolio.platform.ui.view.chart.overview;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Dl5;
import com.fossil.G78;
import com.fossil.Hm7;
import com.fossil.Mn7;
import com.fossil.Mv5;
import com.fossil.Pm7;
import com.fossil.Um5;
import com.fossil.Wt7;
import com.mapped.Lc6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class OverviewDayChart extends BarChart {
    @DexIgnore
    public ObjectAnimator x0;
    @DexIgnore
    public ObjectAnimator y0;
    @DexIgnore
    public Mv5 z0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ OverviewDayChart a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(OverviewDayChart overviewDayChart) {
            this.a = overviewDayChart;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationCancel");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = this.a.getTAG();
            StringBuilder sb = new StringBuilder();
            sb.append("changeModel - onAnimationEnd -- isRunning=");
            ObjectAnimator objectAnimator = this.a.x0;
            sb.append(objectAnimator != null ? Boolean.valueOf(objectAnimator.isRunning()) : null);
            local.d(tag, sb.toString());
            OverviewDayChart overviewDayChart = this.a;
            Mv5 mv5 = overviewDayChart.z0;
            if (mv5 != null) {
                overviewDayChart.I(mv5);
                ObjectAnimator objectAnimator2 = this.a.y0;
                if (objectAnimator2 != null) {
                    objectAnimator2.start();
                    return;
                }
                return;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationRepeat");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationStart");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return Mn7.c(t.c(), t2.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return Mn7.c(t.c(), t2.c());
        }
    }

    @DexIgnore
    public OverviewDayChart(Context context) {
        this(context, null);
    }

    @DexIgnore
    public OverviewDayChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewDayChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewDayChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    @DexIgnore
    public static /* synthetic */ ObjectAnimator O(OverviewDayChart overviewDayChart, int i, int i2, int i3, int i4, int i5, Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewDayChart.N(i, i2, i3, i4);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createInAnim");
    }

    @DexIgnore
    public static /* synthetic */ ObjectAnimator Q(OverviewDayChart overviewDayChart, int i, int i2, int i3, int i4, int i5, Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewDayChart.P(i, i2, i3, i4);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createOutAnim");
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void I(Mv5 mv5) {
        Wg6.c(mv5, DeviceRequestsHelper.DEVICE_INFO_MODEL);
        setMChartModel((BarChart.c) mv5);
        setMMaxValue(getMChartModel().d());
        setMNumberBar(getMChartModel().b().size() <= 1 ? 24 : getMChartModel().b().size());
    }

    @DexIgnore
    public final int M(List<BarChart.a> list) {
        if (list.isEmpty()) {
            return 0;
        }
        int i = Integer.MIN_VALUE;
        for (T t : list) {
            if (!t.d().isEmpty()) {
                ArrayList<BarChart.b> arrayList = t.d().get(0);
                Wg6.b(arrayList, "model.mListOfBarPoints[0]");
                Iterator<T> it = arrayList.iterator();
                while (it.hasNext()) {
                    int e = it.next().e();
                    if (e > i) {
                        i = e;
                    }
                }
            }
        }
        return i;
    }

    @DexIgnore
    public final ObjectAnimator N(int i, int i2, int i3, int i4) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, PropertyValuesHolder.ofInt("maxValue", i4 * i, i), PropertyValuesHolder.ofInt("barAlpha", i2, i3));
        Wg6.b(ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026his, inMaxValue, inAlpha)");
        ofPropertyValuesHolder.setDuration(200L);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    public final ObjectAnimator P(int i, int i2, int i3, int i4) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, PropertyValuesHolder.ofInt("maxValue", i, i4 * i), PropertyValuesHolder.ofInt("barAlpha", i2, i3));
        Wg6.b(ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026s, outMaxValue, outAlpha)");
        ofPropertyValuesHolder.setDuration(200L);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    public final int R(BarChart.a aVar, int i) {
        T t;
        int i2;
        if (i < 0) {
            return 255;
        }
        ArrayList<BarChart.b> arrayList = aVar.d().get(0);
        Wg6.b(arrayList, "barModel.mListOfBarPoints[0]");
        Iterator<T> it = arrayList.iterator();
        if (!it.hasNext()) {
            t = null;
        } else {
            T next = it.next();
            if (it.hasNext()) {
                int e = next.e();
                while (true) {
                    next = it.next();
                    e = next.e();
                    if (e >= e) {
                        e = e;
                        next = next;
                    }
                    if (!it.hasNext()) {
                        break;
                    }
                }
            }
            t = next;
        }
        T t2 = t;
        double e2 = (double) (((float) (t2 != null ? t2.e() : 0)) / ((float) i));
        if (e2 >= 0.75d) {
            i2 = 255;
        } else {
            i2 = (int) ((e2 < 0.5d || e2 >= 0.75d) ? (e2 < 0.25d || e2 >= 0.5d) ? 63.75d : 127.5d : 191.25d);
        }
        return i2;
    }

    @DexIgnore
    public final void S() {
        if (!TextUtils.isEmpty(getMBackgroundColor())) {
            String d = ThemeManager.l.a().d(getMBackgroundColor());
            if (!TextUtils.isEmpty(d)) {
                setBackgroundColor(Color.parseColor(d));
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart, com.portfolio.platform.ui.view.chart.base.BaseChart
    public void a() {
        super.a();
        setMNumberBar(24);
        S();
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void c(Canvas canvas) {
        Bitmap t;
        Wg6.c(canvas, "canvas");
        super.c(canvas);
        Iterator<BarChart.a> it = getMChartModel().b().iterator();
        while (it.hasNext()) {
            BarChart.a next = it.next();
            ArrayList<BarChart.b> arrayList = next.d().get(0);
            Wg6.b(arrayList, "item.mListOfBarPoints[0]");
            List b0 = Pm7.b0(arrayList, new c());
            if ((!b0.isEmpty()) && next.e() && (t = BarChart.t(this, getMLegendIconRes(), 0, 2, null)) != null) {
                RectF a2 = ((BarChart.b) b0.get(0)).a();
                canvas.drawBitmap(t, a2.left + ((a2.width() - ((float) t.getWidth())) * 0.5f), a2.bottom + ((float) getMTextMargin()), new Paint(1));
                t.recycle();
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart, com.portfolio.platform.ui.view.chart.base.BaseChart
    public void f(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        m();
        n();
        x(canvas);
        u(canvas);
        S();
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void p(float f, float f2) {
        Rect rect = new Rect();
        getMLegendPaint().getTextBounds("gh", 0, 2, rect);
        float mLegendHeight = ((float) (getMLegendHeight() + rect.height())) * 0.5f;
        int size = getMLegendTexts().size();
        if (size < 1) {
            getMTextPoint().clear();
            return;
        }
        float f3 = size == 1 ? 0.0f : (f2 - f) / ((float) (size - 1));
        float mBarWidth = f + (getMBarWidth() * 0.5f);
        int i = 0;
        for (T t : getMLegendTexts()) {
            if (i >= 0) {
                T t2 = t;
                if (Wt7.v(t2, G78.ANY_NON_NULL_MARKER, false, 2, null)) {
                    String str = (String) Wt7.Y(t2, new String[]{G78.ANY_NON_NULL_MARKER}, false, 0, 6, null).get(0);
                    getMLegendPaint().getTextBounds(str, 0, str.length(), rect);
                    getMTextPoint().add(new Lc6<>(t2, new PointF(((((float) i) * f3) + mBarWidth) - (((float) rect.width()) * 0.5f), mLegendHeight)));
                } else {
                    getMLegendPaint().getTextBounds((String) t2, 0, t2.length(), rect);
                    getMTextPoint().add(new Lc6<>(t2, new PointF(((((float) i) * f3) + mBarWidth) - (((float) rect.width()) * 0.5f), mLegendHeight)));
                }
                i++;
            } else {
                Hm7.l();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void r(Mv5 mv5) {
        synchronized (this) {
            Wg6.c(mv5, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            StringBuilder sb = new StringBuilder();
            sb.append("changeModel - model=");
            sb.append(mv5);
            sb.append(", mOutAnim.isRunning=");
            ObjectAnimator objectAnimator = this.x0;
            sb.append(objectAnimator != null ? Boolean.valueOf(objectAnimator.isRunning()) : null);
            sb.append(", mInAnim.isRunning=");
            ObjectAnimator objectAnimator2 = this.y0;
            sb.append(objectAnimator2 != null ? Boolean.valueOf(objectAnimator2.isRunning()) : null);
            local.d(tag, sb.toString());
            ObjectAnimator objectAnimator3 = this.x0;
            Boolean valueOf = objectAnimator3 != null ? Boolean.valueOf(objectAnimator3.isRunning()) : null;
            ObjectAnimator objectAnimator4 = this.y0;
            Boolean valueOf2 = objectAnimator4 != null ? Boolean.valueOf(objectAnimator4.isRunning()) : null;
            if (Wg6.a(valueOf, Boolean.TRUE) || Wg6.a(valueOf2, Boolean.TRUE)) {
                if (Wg6.a(mv5, this.z0)) {
                    FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - model == mTempModel");
                    return;
                }
                this.z0 = mv5;
                if (Wg6.a(valueOf, Boolean.TRUE)) {
                    FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - outRunning == true");
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String tag2 = getTAG();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("changeModel - outRunning == true - mMaxValue=");
                    Mv5 mv52 = this.z0;
                    if (mv52 != null) {
                        sb2.append(((BarChart.c) mv52).d());
                        local2.d(tag2, sb2.toString());
                        Mv5 mv53 = this.z0;
                        if (mv53 != null) {
                            this.y0 = O(this, ((BarChart.c) mv53).d(), 0, 255, 0, 8, null);
                        } else {
                            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                        }
                    } else {
                        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - inRunning == true");
                    ObjectAnimator objectAnimator5 = this.y0;
                    if (objectAnimator5 != null) {
                        objectAnimator5.cancel();
                    }
                    int mMaxValue = getMMaxValue();
                    int d = getMChartModel().d();
                    int mBarAlpha = getMBarAlpha();
                    if (d <= 0) {
                        d = 1;
                    }
                    int i = mMaxValue / d;
                    Mv5 mv54 = this.z0;
                    if (mv54 != null) {
                        I(mv54);
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String tag3 = getTAG();
                        local3.d(tag3, "changeModel - inRunning == true -- tempStartMaxValue=" + mMaxValue + ", tempEndMaxValue=" + mMaxValue + ", tempAlpha=" + mMaxValue + ", tempMaxRate=" + mMaxValue + ", newMaxValue=" + getMChartModel().d());
                        ObjectAnimator N = N(getMChartModel().d(), mBarAlpha, 255, i);
                        this.y0 = N;
                        if (N != null) {
                            N.start();
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            } else if (Wg6.a(getMChartModel(), mv5)) {
                FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - mChartModel == model");
            } else {
                this.z0 = mv5;
                this.x0 = Q(this, getMMaxValue(), 255, 0, 0, 8, null);
                Mv5 mv55 = this.z0;
                if (mv55 != null) {
                    this.y0 = O(this, ((BarChart.c) mv55).d(), 0, 255, 0, 8, null);
                    ObjectAnimator objectAnimator6 = this.x0;
                    if (objectAnimator6 != null) {
                        objectAnimator6.addListener(new a(this));
                    }
                    ObjectAnimator objectAnimator7 = this.x0;
                    if (objectAnimator7 != null) {
                        objectAnimator7.start();
                    }
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                }
            }
        }
    }

    @DexIgnore
    public void setGraphPreviewColor(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setGraphPreviewColor color=" + i);
        getMGraph().invalidate();
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void u(Canvas canvas) {
        Object obj;
        Wg6.c(canvas, "canvas");
        getMGraphPaint().setColor(getMDefaultColor());
        int M = M(getMChartModel().b());
        Iterator<BarChart.a> it = getMChartModel().b().iterator();
        while (it.hasNext()) {
            BarChart.a next = it.next();
            ArrayList<BarChart.b> arrayList = next.d().get(0);
            Wg6.b(arrayList, "item.mListOfBarPoints[0]");
            List b0 = Pm7.b0(arrayList, new b());
            Paint mGraphPaint = getMGraphPaint();
            Wg6.b(next, "item");
            mGraphPaint.setAlpha(R(next, M));
            Iterator it2 = b0.iterator();
            if (!it2.hasNext()) {
                obj = null;
            } else {
                Object next2 = it2.next();
                if (it2.hasNext()) {
                    int e = ((BarChart.b) next2).e();
                    while (true) {
                        next2 = it2.next();
                        int e2 = ((BarChart.b) next2).e();
                        if (e >= e2) {
                            e2 = e;
                            next2 = next2;
                        }
                        if (!it2.hasNext()) {
                            break;
                        }
                        e = e2;
                    }
                }
                obj = next2;
            }
            BarChart.b bVar = (BarChart.b) obj;
            if (bVar != null) {
                canvas.drawRoundRect(bVar.a(), getMBarRadius(), getMBarRadius(), getMGraphPaint());
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void x(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        float width = (float) canvas.getWidth();
        Iterator<Lc6<Integer, PointF>> it = getMGraphLegendPoint().iterator();
        while (it.hasNext()) {
            Lc6<Integer, PointF> next = it.next();
            Rect rect = new Rect();
            float intValue = (float) next.getFirst().intValue();
            String valueOf = String.valueOf((int) intValue);
            float f = (float) 1000;
            if (intValue >= f) {
                float f2 = intValue / f;
                valueOf = Dl5.b(f2, 1) + Um5.c(PortfolioApp.get.instance(), 2131886674);
            }
            getMLegendPaint().getTextBounds(valueOf, 0, valueOf.length(), rect);
            float f3 = next.getSecond().y;
            canvas.drawLine(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3, width, f3, getMLegendLinePaint());
            canvas.drawText(valueOf, (width - getMGraphLegendMargin()) - ((float) rect.width()), getMGraphLegendMargin() + f3 + ((float) rect.height()), getMLegendPaint());
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void y(Canvas canvas) {
        Wg6.c(canvas, "canvas");
    }
}
