package com.portfolio.platform.ui.view.chart.overview;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Jl5;
import com.fossil.K37;
import com.fossil.Nv5;
import com.fossil.Rh5;
import com.fossil.Wt7;
import com.mapped.Kc6;
import com.mapped.Lc6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OverviewSleepDayChart extends BarChart {
    @DexIgnore
    public Integer A0;
    @DexIgnore
    public Integer B0;
    @DexIgnore
    public Integer C0;
    @DexIgnore
    public int D0;
    @DexIgnore
    public float x0;
    @DexIgnore
    public float y0;
    @DexIgnore
    public float z0;

    @DexIgnore
    public OverviewSleepDayChart(Context context) {
        this(context, null);
    }

    @DexIgnore
    public OverviewSleepDayChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewSleepDayChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewSleepDayChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        Resources.Theme theme;
        TypedArray obtainStyledAttributes;
        this.D0 = -1;
        if (attributeSet != null && context != null && (theme = context.getTheme()) != null && (obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, X24.OverviewSleepDayChart, 0, 0)) != null) {
            try {
                this.x0 = obtainStyledAttributes.getFloat(0, 0.1f);
                this.y0 = obtainStyledAttributes.getFloat(2, 0.33f);
                this.z0 = obtainStyledAttributes.getFloat(1, 0.6f);
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = getTAG();
                local.d(tag, "constructor - e=" + e);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public void J(String str, int i) {
        Wg6.c(str, "keyColor");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setGraphPreviewColor keyColor=" + str + " color=" + i);
        int hashCode = str.hashCode();
        if (hashCode != -1991609013) {
            if (hashCode != -1521618862) {
                if (hashCode == -219887775 && str.equals("lightSleep")) {
                    this.B0 = Integer.valueOf(i);
                }
            } else if (str.equals("awakeSleep")) {
                this.A0 = Integer.valueOf(i);
            }
        } else if (str.equals("deepSleep")) {
            this.C0 = Integer.valueOf(i);
        }
        getMGraph().invalidate();
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void m() {
        float f;
        float mGraphHeight;
        float mGraphHeight2;
        float mGraphHeight3;
        float mGraphHeight4;
        float f2;
        float mGraphHeight5;
        float mGraphHeight6;
        float mGraphHeight7;
        float mGraphHeight8;
        float f3;
        ArrayList<BarChart.a> b = getMChartModel().b();
        if (b.size() != 0) {
            setMGoalLinePath(new Path());
            setMGoalIconPoint(new PointF());
            setMGoalIconShow(false);
            RectF rectF = new RectF(getMBarMargin(), getMSafeAreaHeight(), ((float) getMGraphWidth()) - getMBarMargin(), (float) getMGraphHeight());
            getMGraphHeight();
            float f4 = rectF.left;
            ArrayList<ArrayList<BarChart.b>> d = b.get(0).d();
            Iterator<ArrayList<BarChart.b>> it = d.iterator();
            int i = 0;
            while (it.hasNext()) {
                ArrayList<BarChart.b> next = it.next();
                int d2 = next.get(0).d() + i;
                i = d.indexOf(next) != 0 ? d2 + 10 : d2;
            }
            float f5 = (rectF.right - rectF.left) / ((float) i);
            Iterator<ArrayList<BarChart.b>> it2 = d.iterator();
            float f6 = 0.0f;
            float f7 = f4;
            while (it2.hasNext()) {
                ArrayList<BarChart.b> next2 = it2.next();
                float mGraphHeight9 = ((float) getMGraphHeight()) * this.x0;
                int size = next2.size();
                float f8 = f6;
                int i2 = 0;
                while (i2 < size) {
                    if (i2 < next2.size() - 1) {
                        f8 = (((float) (next2.get(i2 + 1).b() - next2.get(i2).b())) * f5) + f7;
                        int i3 = Nv5.b[next2.get(i2).c().ordinal()];
                        if (i3 != 1) {
                            if (i3 == 2) {
                                mGraphHeight7 = (float) getMGraphHeight();
                                mGraphHeight8 = (float) getMGraphHeight();
                                f3 = this.y0;
                            } else if (i3 == 3) {
                                mGraphHeight7 = (float) getMGraphHeight();
                                mGraphHeight8 = (float) getMGraphHeight();
                                f3 = this.z0;
                            } else {
                                throw new Kc6();
                            }
                            mGraphHeight5 = (mGraphHeight7 - (f3 * mGraphHeight8)) - mGraphHeight9;
                        } else {
                            mGraphHeight5 = ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.x0);
                        }
                        int i4 = Nv5.c[next2.get(i2).c().ordinal()];
                        if (i4 == 1) {
                            mGraphHeight6 = (float) getMGraphHeight();
                        } else if (i4 == 2 || i4 == 3) {
                            mGraphHeight6 = ((float) getMGraphHeight()) - mGraphHeight9;
                        } else {
                            throw new Kc6();
                        }
                        next2.get(i2).f(new RectF(f7, mGraphHeight5, f8, mGraphHeight6));
                        f = f8;
                    } else if (i2 == next2.size() - 1) {
                        f8 = (((float) (next2.get(i2).d() - next2.get(i2).b())) * f5) + f7;
                        int i5 = Nv5.d[next2.get(i2).c().ordinal()];
                        if (i5 != 1) {
                            if (i5 == 2) {
                                mGraphHeight3 = (float) getMGraphHeight();
                                mGraphHeight4 = (float) getMGraphHeight();
                                f2 = this.y0;
                            } else if (i5 == 3) {
                                mGraphHeight3 = (float) getMGraphHeight();
                                mGraphHeight4 = (float) getMGraphHeight();
                                f2 = this.z0;
                            } else {
                                throw new Kc6();
                            }
                            mGraphHeight = (mGraphHeight3 - (mGraphHeight4 * f2)) - mGraphHeight9;
                        } else {
                            mGraphHeight = ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.x0);
                        }
                        int i6 = Nv5.e[next2.get(i2).c().ordinal()];
                        if (i6 == 1) {
                            mGraphHeight2 = (float) getMGraphHeight();
                        } else if (i6 == 2 || i6 == 3) {
                            mGraphHeight2 = ((float) getMGraphHeight()) - mGraphHeight9;
                        } else {
                            throw new Kc6();
                        }
                        next2.get(next2.size() - 1).f(new RectF(f7, mGraphHeight, f8, mGraphHeight2));
                        f = f7;
                    } else {
                        f = f7;
                    }
                    f8 = f8;
                    i2++;
                    f7 = f;
                }
                float f9 = (((float) 10) * f5) + f8;
                f6 = f9;
                f7 = f9;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void n() {
        if (getMGoalType() == Rh5.TOTAL_SLEEP && getMChartModel().b().size() != 0) {
            RectF rectF = new RectF(getMBarMargin(), getMSafeAreaHeight(), ((float) getMGraphWidth()) - getMBarMargin(), (float) getMGraphHeight());
            ArrayList<BarChart.a> b = getMChartModel().b();
            int c = getMChartModel().c();
            ArrayList<ArrayList<BarChart.b>> d = b.get(0).d();
            Iterator<ArrayList<BarChart.b>> it = d.iterator();
            int i = 0;
            int i2 = c;
            while (it.hasNext()) {
                ArrayList<BarChart.b> next = it.next();
                int d2 = next.get(0).d() + i;
                if (d.indexOf(next) != 0) {
                    i = d2 + 10;
                    i2 += 10;
                } else {
                    i = d2;
                }
            }
            float f = (((float) i2) * (rectF.right - rectF.left)) / ((float) i);
            Iterator<ArrayList<BarChart.b>> it2 = d.iterator();
            while (it2.hasNext()) {
                ArrayList<BarChart.b> next2 = it2.next();
                if (f <= next2.get(next2.size() - 1).a().right) {
                    Iterator<BarChart.b> it3 = next2.iterator();
                    while (it3.hasNext()) {
                        BarChart.b next3 = it3.next();
                        if (f >= next3.a().left && f <= next3.a().right) {
                            getMGoalLinePath().moveTo(f, getMSafeAreaHeight() - 10.0f);
                            getMGoalLinePath().lineTo(f, next3.a().top);
                            getMGoalIconPoint().set(f - (((float) getMGoalIconSize()) * 0.5f), (getMSafeAreaHeight() * 0.5f) - 20.0f);
                            setMGoalIconShow(false);
                            return;
                        }
                    }
                    continue;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void p(float f, float f2) {
        if (getMChartModel().b().size() != 0) {
            ArrayList<ArrayList<BarChart.b>> d = getMChartModel().b().get(0).d();
            Iterator<ArrayList<BarChart.b>> it = d.iterator();
            while (it.hasNext()) {
                ArrayList<BarChart.b> next = it.next();
                BarChart.b bVar = next.get(0);
                Wg6.b(bVar, "session[0]");
                BarChart.b bVar2 = bVar;
                BarChart.b bVar3 = next.get(next.size() - 1);
                Wg6.b(bVar3, "session[session.size - 1]");
                BarChart.b bVar4 = bVar3;
                long j = (long) 1000;
                long e = ((long) bVar2.e()) * j;
                long d2 = (j * (((long) bVar4.d()) - 1) * ((long) 60)) + e;
                Rect rect = new Rect();
                int i = this.D0;
                String r = i != -1 ? TimeUtils.r(e, i) : TimeUtils.n(e);
                int i2 = this.D0;
                String r2 = i2 != -1 ? TimeUtils.r(d2, i2) : TimeUtils.n(d2);
                Jl5 jl5 = Jl5.b;
                Wg6.b(r, "startTimeString");
                String k = jl5.k(r);
                Jl5 jl52 = Jl5.b;
                Wg6.b(r2, "endTimeString");
                String k2 = jl52.k(r2);
                getMLegendPaint().getTextBounds(k, 0, Wt7.A(k), rect);
                float mTextMargin = ((float) getMTextMargin()) + ((float) rect.height());
                if (bVar4.a().right - bVar2.a().left > ((float) (rect.right * 2))) {
                    getMTextPoint().add(new Lc6<>(k, new PointF(bVar2.a().left, mTextMargin)));
                    getMTextPoint().add(new Lc6<>(k2, new PointF((bVar4.a().right - f) - ((float) rect.right), mTextMargin)));
                } else if (d.indexOf(next) == d.size() - 1) {
                    getMTextPoint().add(new Lc6<>(k2, new PointF((bVar4.a().right - f) - ((float) rect.right), mTextMargin)));
                } else {
                    getMTextPoint().add(new Lc6<>(k, new PointF(bVar2.a().left, mTextMargin)));
                }
            }
        }
    }

    @DexIgnore
    public final void setTimeZoneOffsetInSecond(int i) {
        this.D0 = i;
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void u(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        canvas.drawRect(new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.x0), (float) getMGraphWidth(), (((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.x0)) + ((float) 2)), getMLegendLinePaint());
        Iterator<BarChart.a> it = getMChartModel().b().iterator();
        while (it.hasNext()) {
            Iterator<ArrayList<BarChart.b>> it2 = it.next().d().iterator();
            while (it2.hasNext()) {
                Iterator<BarChart.b> it3 = it2.next().iterator();
                while (it3.hasNext()) {
                    BarChart.b next = it3.next();
                    int i = Nv5.a[next.c().ordinal()];
                    if (i == 1) {
                        Paint mGraphPaint = getMGraphPaint();
                        Integer num = this.A0;
                        mGraphPaint.setColor(num != null ? num.intValue() : getMLowestColor());
                        K37.a(canvas, next.a().left, next.a().top, next.a().right, next.a().bottom, getMBarRadius(), getMBarRadius(), false, false, true, true, getMGraphPaint());
                    } else if (i == 2) {
                        Paint mGraphPaint2 = getMGraphPaint();
                        Integer num2 = this.B0;
                        mGraphPaint2.setColor(num2 != null ? num2.intValue() : getMDefaultColor());
                        K37.a(canvas, next.a().left, next.a().top, next.a().right, next.a().bottom, getMBarRadius(), getMBarRadius(), true, true, false, false, getMGraphPaint());
                    } else if (i == 3) {
                        Paint mGraphPaint3 = getMGraphPaint();
                        Integer num3 = this.C0;
                        mGraphPaint3.setColor(num3 != null ? num3.intValue() : getMHighestColor());
                        K37.a(canvas, next.a().left, next.a().top, next.a().right, next.a().bottom, getMBarRadius(), getMBarRadius(), true, true, false, false, getMGraphPaint());
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void y(Canvas canvas) {
        Wg6.c(canvas, "canvas");
    }
}
