package com.portfolio.platform.ui.view.chart.base;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Em7;
import com.fossil.Hm7;
import com.fossil.K37;
import com.fossil.Lv5;
import com.fossil.Mn7;
import com.fossil.Mv5;
import com.fossil.Pm7;
import com.fossil.Rh5;
import com.mapped.A;
import com.mapped.C;
import com.mapped.Kc6;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BarChart extends BaseChart {
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public PointF E;
    @DexIgnore
    public boolean F;
    @DexIgnore
    public int G;
    @DexIgnore
    public int H;
    @DexIgnore
    public ArrayList<PointF> I;
    @DexIgnore
    public boolean J;
    @DexIgnore
    public float K;
    @DexIgnore
    public int L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public String O;
    @DexIgnore
    public Path P;
    @DexIgnore
    public String Q;
    @DexIgnore
    public float R;
    @DexIgnore
    public ArrayList<String> S;
    @DexIgnore
    public int T;
    @DexIgnore
    public float U;
    @DexIgnore
    public String V;
    @DexIgnore
    public int W;
    @DexIgnore
    public String a0;
    @DexIgnore
    public ArrayList<Lc6<String, PointF>> b0;
    @DexIgnore
    public float c0;
    @DexIgnore
    public ArrayList<Lc6<Integer, PointF>> d0;
    @DexIgnore
    public float e0;
    @DexIgnore
    public float f0;
    @DexIgnore
    public float g0;
    @DexIgnore
    public float h0;
    @DexIgnore
    public float i0;
    @DexIgnore
    public float j0;
    @DexIgnore
    public float k0;
    @DexIgnore
    public int l0;
    @DexIgnore
    public int m0;
    @DexIgnore
    public int n0;
    @DexIgnore
    public boolean o0;
    @DexIgnore
    public String p0;
    @DexIgnore
    public c q0;
    @DexIgnore
    public Rh5 r0;
    @DexIgnore
    public Paint s0;
    @DexIgnore
    public Paint t0;
    @DexIgnore
    public Paint u0;
    @DexIgnore
    public Paint v0;
    @DexIgnore
    public d w;
    @DexIgnore
    public Paint w0;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public int a;
        @DexIgnore
        public ArrayList<ArrayList<b>> b;
        @DexIgnore
        public long c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public a() {
            this(0, null, 0, false, 15, null);
        }

        @DexIgnore
        public a(int i, ArrayList<ArrayList<b>> arrayList, long j, boolean z) {
            Wg6.c(arrayList, "mListOfBarPoints");
            this.a = i;
            this.b = arrayList;
            this.c = j;
            this.d = z;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(int i, ArrayList arrayList, long j, boolean z, int i2, Qg6 qg6) {
            this((i2 & 1) != 0 ? -1 : i, (i2 & 2) != 0 ? Hm7.c(Hm7.c(new b(0, null, 0, 0, null, 23, null))) : arrayList, (i2 & 4) != 0 ? -1 : j, (i2 & 8) != 0 ? false : z);
        }

        @DexIgnore
        public final boolean a(ArrayList<ArrayList<b>> arrayList) {
            if (this.b.size() != arrayList.size()) {
                return false;
            }
            int i = 0;
            for (T t : this.b) {
                if (i >= 0) {
                    T t2 = t;
                    if (t2.size() != arrayList.get(i).size()) {
                        return false;
                    }
                    int i2 = 0;
                    for (Object obj : t2) {
                        if (i2 < 0) {
                            Hm7.l();
                            throw null;
                        } else if (!Wg6.a((b) obj, arrayList.get(i).get(i2))) {
                            return false;
                        } else {
                            i2++;
                        }
                    }
                    i++;
                } else {
                    Hm7.l();
                    throw null;
                }
            }
            return true;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final long c() {
            return this.c;
        }

        @DexIgnore
        public final ArrayList<ArrayList<b>> d() {
            return this.b;
        }

        @DexIgnore
        public final boolean e() {
            return this.d;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.c == aVar.c && this.d == aVar.d && a(aVar.b);
        }

        @DexIgnore
        public int hashCode() {
            return (((((this.a * 31) + this.b.hashCode()) * 31) + C.a(this.c)) * 31) + A.a(this.d);
        }

        @DexIgnore
        public String toString() {
            return "{goal=" + this.a + ", index=" + this.c + ", points=" + this.b + '}';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public int a;
        @DexIgnore
        public e b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public RectF e;

        @DexIgnore
        public b() {
            this(0, null, 0, 0, null, 31, null);
        }

        @DexIgnore
        public b(int i, e eVar, int i2, int i3, RectF rectF) {
            Wg6.c(eVar, "mState");
            Wg6.c(rectF, "mBound");
            this.a = i;
            this.b = eVar;
            this.c = i2;
            this.d = i3;
            this.e = rectF;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(int i, e eVar, int i2, int i3, RectF rectF, int i4, Qg6 qg6) {
            this((i4 & 1) != 0 ? -1 : i, (i4 & 2) != 0 ? e.DEFAULT : eVar, (i4 & 4) != 0 ? 0 : i2, (i4 & 8) == 0 ? i3 : 0, (i4 & 16) != 0 ? new RectF() : rectF);
        }

        @DexIgnore
        public final RectF a() {
            return this.e;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final e c() {
            return this.b;
        }

        @DexIgnore
        public final int d() {
            return this.c;
        }

        @DexIgnore
        public final int e() {
            return this.d;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return this.d == bVar.d && this.c == bVar.c && this.a == bVar.a && this.b.getMValue() == bVar.b.getMValue();
        }

        @DexIgnore
        public final void f(RectF rectF) {
            Wg6.c(rectF, "<set-?>");
            this.e = rectF;
        }

        @DexIgnore
        public final void g(int i) {
            this.a = i;
        }

        @DexIgnore
        public final void h(e eVar) {
            Wg6.c(eVar, "<set-?>");
            this.b = eVar;
        }

        @DexIgnore
        public int hashCode() {
            return (((((((this.a * 31) + this.b.hashCode()) * 31) + this.c) * 31) + this.d) * 31) + this.e.hashCode();
        }

        @DexIgnore
        public final void i(int i) {
            this.c = i;
        }

        @DexIgnore
        public final void j(int i) {
            this.d = i;
        }

        @DexIgnore
        public String toString() {
            return "{index=" + this.a + ", state=" + this.b + ", mTotal=" + this.c + ", value=" + this.d + ", bound=" + this.e + '}';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Mv5 {
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public ArrayList<a> d;

        @DexIgnore
        public c() {
            this(0, 0, null, 7, null);
        }

        @DexIgnore
        public c(int i, int i2, ArrayList<a> arrayList) {
            Wg6.c(arrayList, "mData");
            this.b = i;
            this.c = i2;
            this.d = arrayList;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(int i, int i2, ArrayList arrayList, int i3, Qg6 qg6) {
            this((i3 & 1) != 0 ? -1 : i, (i3 & 2) != 0 ? -1 : i2, (i3 & 4) != 0 ? new ArrayList() : arrayList);
        }

        @DexIgnore
        public final boolean a(ArrayList<a> arrayList) {
            if (this.d.size() != arrayList.size()) {
                return false;
            }
            int i = 0;
            for (T t : this.d) {
                if (i < 0) {
                    Hm7.l();
                    throw null;
                } else if (!Wg6.a(t, arrayList.get(i))) {
                    return false;
                } else {
                    i++;
                }
            }
            return true;
        }

        @DexIgnore
        public final ArrayList<a> b() {
            return this.d;
        }

        @DexIgnore
        public final int c() {
            return this.c;
        }

        @DexIgnore
        public final int d() {
            return this.b;
        }

        @DexIgnore
        public final void e(int i) {
            this.c = i;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return this.b == cVar.b && this.c == cVar.c && a(cVar.d);
        }

        @DexIgnore
        public final void f(int i) {
            this.b = i;
        }

        @DexIgnore
        public int hashCode() {
            return (((this.b * 31) + this.c) * 31) + this.d.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "ChartModel:{max=" + this.b + ", goal=" + this.c + ", data=" + this.d + '}';
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(long j);
    }

    @DexIgnore
    public enum e {
        LOWEST(0),
        DEFAULT(1),
        HIGHEST(2);
        
        @DexIgnore
        public static /* final */ a Companion; // = new a(null);
        @DexIgnore
        public int mValue;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(Qg6 qg6) {
                this();
            }
        }

        @DexIgnore
        public e(int i) {
            this.mValue = i;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ e(int i, int i2, Qg6 qg6) {
            this((i2 & 1) != 0 ? 0 : i);
        }

        @DexIgnore
        public final int getMValue() {
            return this.mValue;
        }

        @DexIgnore
        public final void setMValue(int i) {
            this.mValue = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return Mn7.c(t2.c(), t.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return Mn7.c(t2.c(), t.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return Mn7.c(t.c(), t2.c());
        }
    }

    @DexIgnore
    public BarChart(Context context) {
        this(context, null);
    }

    @DexIgnore
    public BarChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public BarChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public BarChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        Resources.Theme theme;
        TypedArray obtainStyledAttributes;
        String[] stringArray;
        this.C = -1;
        this.D = 10;
        this.E = new PointF();
        this.G = -1;
        this.H = 10;
        this.I = new ArrayList<>();
        this.K = 2.0f;
        this.M = 2.0f;
        this.N = 2.0f;
        this.O = "";
        this.P = new Path();
        this.Q = "";
        this.R = 2.0f;
        this.S = new ArrayList<>();
        this.T = -1;
        this.U = 14.0f;
        this.V = "";
        this.W = 10;
        this.a0 = "";
        this.b0 = new ArrayList<>();
        this.d0 = new ArrayList<>();
        this.e0 = 50.0f;
        this.g0 = 10.0f;
        this.h0 = 5.0f;
        this.i0 = 30.0f;
        this.j0 = 30.0f;
        this.k0 = 30.0f;
        this.l0 = -1;
        this.n0 = 255;
        this.p0 = "";
        this.q0 = new c(0, 0, null, 7, null);
        this.r0 = Rh5.ACTIVE_TIME;
        if (!(attributeSet == null || context == null || (theme = context.getTheme()) == null || (obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, X24.BaseChart, 0, 0)) == null)) {
            try {
                this.x = obtainStyledAttributes.getColor(0, 0);
                this.y = obtainStyledAttributes.getColor(14, 0);
                this.z = obtainStyledAttributes.getColor(20, 0);
                this.A = obtainStyledAttributes.getColor(2, 0);
                this.B = obtainStyledAttributes.getColor(13, 0);
                this.C = obtainStyledAttributes.getResourceId(5, 10);
                this.D = obtainStyledAttributes.getDimensionPixelSize(6, 10);
                this.G = obtainStyledAttributes.getResourceId(27, 10);
                this.H = obtainStyledAttributes.getDimensionPixelSize(28, 10);
                this.K = (float) obtainStyledAttributes.getDimensionPixelSize(10, 2);
                this.L = obtainStyledAttributes.getColor(7, 0);
                this.M = (float) obtainStyledAttributes.getDimensionPixelSize(9, 2);
                this.N = (float) obtainStyledAttributes.getDimensionPixelSize(8, 2);
                String string = obtainStyledAttributes.getString(11);
                this.O = string == null ? "" : string;
                this.U = (float) obtainStyledAttributes.getDimensionPixelSize(31, 14);
                String string2 = obtainStyledAttributes.getString(29);
                this.V = string2 == null ? "" : string2;
                this.W = obtainStyledAttributes.getDimensionPixelSize(30, 10);
                this.T = obtainStyledAttributes.getResourceId(16, -1);
                String string3 = obtainStyledAttributes.getString(17);
                this.Q = string3 == null ? "" : string3;
                this.R = (float) obtainStyledAttributes.getDimensionPixelSize(18, 2);
                this.c0 = (float) obtainStyledAttributes.getDimensionPixelSize(12, 0);
                obtainStyledAttributes.getDimensionPixelSize(25, -1);
                this.e0 = (float) obtainStyledAttributes.getDimensionPixelSize(24, 50);
                this.f0 = (float) obtainStyledAttributes.getDimensionPixelSize(32, 0);
                this.g0 = (float) obtainStyledAttributes.getDimensionPixelSize(33, 10);
                this.h0 = (float) obtainStyledAttributes.getDimensionPixelSize(23, 5);
                float dimensionPixelSize = (float) obtainStyledAttributes.getDimensionPixelSize(21, 30);
                this.j0 = dimensionPixelSize;
                this.k0 = (float) obtainStyledAttributes.getDimensionPixelSize(22, (int) dimensionPixelSize);
                this.i0 = (float) obtainStyledAttributes.getDimensionPixelSize(26, 30);
                setMLegendHeight(obtainStyledAttributes.getDimensionPixelSize(15, 30));
                this.o0 = obtainStyledAttributes.getBoolean(3, false);
                String string4 = obtainStyledAttributes.getString(4);
                this.a0 = string4 == null ? "" : string4;
                String string5 = obtainStyledAttributes.getString(1);
                this.p0 = string5 == null ? "" : string5;
                int resourceId = obtainStyledAttributes.getResourceId(19, -1);
                if (resourceId != -1) {
                    Resources resources = getResources();
                    String[] strArr = (resources == null || (stringArray = resources.getStringArray(resourceId)) == null) ? new String[0] : stringArray;
                    Wg6.b(strArr, "(resources?.getStringArr\u2026         ?: emptyArray())");
                    ArrayList<String> arrayList = new ArrayList<>();
                    Em7.b0(strArr, arrayList);
                    this.S = arrayList;
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = getTAG();
                local.d(tag, "constructor - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        a();
    }

    @DexIgnore
    public static /* synthetic */ void F(BarChart barChart, int i, int i2, int i3, int i4, String str, Rh5 rh5, int i5, Object obj) {
        int i6 = -1;
        if (obj == null) {
            int i7 = (i5 & 1) != 0 ? -1 : i;
            int i8 = (i5 & 2) != 0 ? -1 : i2;
            int i9 = (i5 & 4) != 0 ? -1 : i3;
            if ((i5 & 8) == 0) {
                i6 = i4;
            }
            barChart.E(i7, i8, i9, i6, (i5 & 16) != 0 ? null : str, (i5 & 32) != 0 ? Rh5.ACTIVE_TIME : rh5);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: setDataRes");
    }

    @DexIgnore
    public static /* synthetic */ void H(BarChart barChart, ArrayList arrayList, boolean z2, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                z2 = false;
            }
            barChart.G(arrayList, z2);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: setLegendTexts");
    }

    @DexIgnore
    public static /* synthetic */ Bitmap t(BarChart barChart, int i, int i2, int i3, Object obj) {
        if (obj == null) {
            if ((i3 & 2) != 0) {
                i2 = -1;
            }
            return barChart.s(i, i2);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createBitmapByRes");
    }

    @DexIgnore
    public void A(Canvas canvas) {
        Bitmap s;
        Wg6.c(canvas, "canvas");
        if (this.J && (s = s(this.G, this.H)) != null) {
            for (T t : this.I) {
                float f2 = ((PointF) t).x;
                float f3 = ((PointF) t).y;
                Paint paint = this.u0;
                if (paint != null) {
                    canvas.drawBitmap(s, f2, f3, paint);
                } else {
                    Wg6.n("mGraphIconPaint");
                    throw null;
                }
            }
            s.recycle();
        }
    }

    @DexIgnore
    public final RectF B(RectF rectF, float f2) {
        return new RectF(rectF.left - f2, rectF.top - f2, rectF.right + f2, rectF.bottom + f2);
    }

    @DexIgnore
    public final void C() {
        String d2;
        Typeface f2;
        String d3;
        Paint paint = new Paint(1);
        this.s0 = paint;
        if (paint != null) {
            paint.setAntiAlias(true);
            Paint paint2 = this.s0;
            if (paint2 != null) {
                paint2.setStyle(Paint.Style.FILL);
                Paint paint3 = this.s0;
                if (paint3 != null) {
                    Paint paint4 = new Paint(paint3);
                    this.t0 = paint4;
                    if (paint4 != null) {
                        paint4.setStrokeWidth(this.K);
                        Paint paint5 = this.t0;
                        if (paint5 != null) {
                            paint5.setColor(this.L);
                            Paint paint6 = this.t0;
                            if (paint6 != null) {
                                paint6.setStyle(Paint.Style.STROKE);
                                Paint paint7 = this.t0;
                                if (paint7 != null) {
                                    paint7.setStrokeCap(Paint.Cap.ROUND);
                                    Paint paint8 = this.t0;
                                    if (paint8 != null) {
                                        paint8.setStrokeJoin(Paint.Join.ROUND);
                                        Paint paint9 = this.t0;
                                        if (paint9 != null) {
                                            paint9.setPathEffect(new DashPathEffect(new float[]{this.M, this.N}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                                            Paint paint10 = new Paint(1);
                                            this.u0 = paint10;
                                            if (paint10 != null) {
                                                paint10.setColorFilter(new PorterDuffColorFilter(this.x, PorterDuff.Mode.SRC_IN));
                                                this.v0 = new Paint(1);
                                                if (!TextUtils.isEmpty(this.V) && (d3 = ThemeManager.l.a().d(this.V)) != null) {
                                                    Paint paint11 = this.v0;
                                                    if (paint11 != null) {
                                                        paint11.setColor(Color.parseColor(d3));
                                                    } else {
                                                        Wg6.n("mLegendPaint");
                                                        throw null;
                                                    }
                                                }
                                                Paint paint12 = this.v0;
                                                if (paint12 != null) {
                                                    paint12.setAntiAlias(true);
                                                    Paint paint13 = this.v0;
                                                    if (paint13 != null) {
                                                        paint13.setStyle(Paint.Style.FILL);
                                                        Paint paint14 = this.v0;
                                                        if (paint14 != null) {
                                                            paint14.setStrokeWidth(this.R);
                                                            Paint paint15 = this.v0;
                                                            if (paint15 != null) {
                                                                paint15.setTextSize(this.U);
                                                                if (!TextUtils.isEmpty(this.a0) && (f2 = ThemeManager.l.a().f(this.a0)) != null) {
                                                                    Paint paint16 = this.v0;
                                                                    if (paint16 != null) {
                                                                        paint16.setTypeface(f2);
                                                                    } else {
                                                                        Wg6.n("mLegendPaint");
                                                                        throw null;
                                                                    }
                                                                }
                                                                Paint paint17 = this.v0;
                                                                if (paint17 != null) {
                                                                    this.w0 = new Paint(paint17);
                                                                    if (!TextUtils.isEmpty(this.Q) && (d2 = ThemeManager.l.a().d(this.Q)) != null) {
                                                                        Paint paint18 = this.w0;
                                                                        if (paint18 != null) {
                                                                            paint18.setColor(Color.parseColor(d2));
                                                                        } else {
                                                                            Wg6.n("mLegendLinePaint");
                                                                            throw null;
                                                                        }
                                                                    }
                                                                } else {
                                                                    Wg6.n("mLegendPaint");
                                                                    throw null;
                                                                }
                                                            } else {
                                                                Wg6.n("mLegendPaint");
                                                                throw null;
                                                            }
                                                        } else {
                                                            Wg6.n("mLegendPaint");
                                                            throw null;
                                                        }
                                                    } else {
                                                        Wg6.n("mLegendPaint");
                                                        throw null;
                                                    }
                                                } else {
                                                    Wg6.n("mLegendPaint");
                                                    throw null;
                                                }
                                            } else {
                                                Wg6.n("mGraphIconPaint");
                                                throw null;
                                            }
                                        } else {
                                            Wg6.n("mGraphGoalLinePaint");
                                            throw null;
                                        }
                                    } else {
                                        Wg6.n("mGraphGoalLinePaint");
                                        throw null;
                                    }
                                } else {
                                    Wg6.n("mGraphGoalLinePaint");
                                    throw null;
                                }
                            } else {
                                Wg6.n("mGraphGoalLinePaint");
                                throw null;
                            }
                        } else {
                            Wg6.n("mGraphGoalLinePaint");
                            throw null;
                        }
                    } else {
                        Wg6.n("mGraphGoalLinePaint");
                        throw null;
                    }
                } else {
                    Wg6.n("mGraphPaint");
                    throw null;
                }
            } else {
                Wg6.n("mGraphPaint");
                throw null;
            }
        } else {
            Wg6.n("mGraphPaint");
            throw null;
        }
    }

    @DexIgnore
    public void D(String str, String str2) {
        Wg6.c(str, "reachGoal");
        Wg6.c(str2, "nonBrandNonReachGoal");
        String d2 = ThemeManager.l.a().d(str);
        String d3 = ThemeManager.l.a().d(str2);
        if (d2 != null) {
            int parseColor = Color.parseColor(d2);
            this.A = parseColor;
            this.L = parseColor;
            this.x = parseColor;
            Paint paint = this.t0;
            if (paint != null) {
                paint.setColor(parseColor);
            } else {
                Wg6.n("mGraphGoalLinePaint");
                throw null;
            }
        }
        if (d3 != null) {
            this.y = Color.parseColor(d3);
        }
    }

    @DexIgnore
    public final void E(int i, int i2, int i3, int i4, String str, Rh5 rh5) {
        Wg6.c(rh5, "goalType");
        if (i == -1) {
            i = this.x;
        }
        this.x = i;
        if (i2 == -1) {
            i2 = this.z;
        }
        this.z = i2;
        if (i3 == -1) {
            i3 = this.A;
        }
        this.A = i3;
        if (i4 == -1) {
            i4 = this.B;
        }
        this.B = i4;
        this.O = str != null ? str : this.O;
        if (rh5 == Rh5.ACTIVE_TIME) {
            rh5 = this.r0;
        }
        this.r0 = rh5;
        if (str == null) {
            str = this.O;
        }
        this.O = str;
    }

    @DexIgnore
    public final void G(ArrayList<String> arrayList, boolean z2) {
        Wg6.c(arrayList, "array");
        this.S.clear();
        this.S.addAll(arrayList);
        if (z2) {
            getMLegend().invalidate();
        }
    }

    @DexIgnore
    public void I(Mv5 mv5) {
        Wg6.c(mv5, DeviceRequestsHelper.DEVICE_INFO_MODEL);
        c cVar = (c) mv5;
        this.q0 = cVar;
        this.m0 = cVar.d();
        if (this.l0 == -1) {
            this.l0 = this.q0.b().size();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void a() {
        super.a();
        C();
        if (isInEditMode()) {
            this.q0 = new c(200, 200, Hm7.c(new a(200, Hm7.c(Hm7.c(new b(-1, e.DEFAULT, 0, 40, new RectF()), new b(-1, e.LOWEST, 0, 40, new RectF()), new b(-1, e.HIGHEST, 0, 40, new RectF()))), 0, false, 12, null), new a(200, Hm7.c(Hm7.c(new b(-1, e.DEFAULT, 0, 40, new RectF()), new b(-1, e.LOWEST, 0, 80, new RectF()), new b(-1, e.HIGHEST, 0, 50, new RectF()))), 0, false, 12, null), new a(150, Hm7.c(Hm7.c(new b(-1, e.DEFAULT, 0, 60, new RectF()), new b(-1, e.LOWEST, 0, 100, new RectF()), new b(-1, e.HIGHEST, 0, 40, new RectF()))), 0, false, 12, null), new a(150, Hm7.c(Hm7.c(new b(-1, e.DEFAULT, 0, 80, new RectF()), new b(-1, e.LOWEST, 0, 30, new RectF()), new b(-1, e.HIGHEST, 0, 50, new RectF()))), 0, false, 12, null), new a(150, Hm7.c(Hm7.c(new b(-1, e.DEFAULT, 0, 40, new RectF()), new b(-1, e.LOWEST, 0, 90, new RectF()), new b(-1, e.HIGHEST, 0, 20, new RectF()))), 0, false, 12, null), new a(180, Hm7.c(Hm7.c(new b(-1, e.DEFAULT, 0, 40, new RectF()), new b(-1, e.LOWEST, 0, 40, new RectF()), new b(-1, e.HIGHEST, 0, 40, new RectF()))), 0, false, 12, null), new a(180, Hm7.c(Hm7.c(new b(-1, e.DEFAULT, 0, 70, new RectF()), new b(-1, e.LOWEST, 0, 30, new RectF()), new b(-1, e.HIGHEST, 0, 50, new RectF()))), 0, false, 12, null)));
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void f(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        super.f(canvas);
        m();
        n();
        v(canvas);
        u(canvas);
        x(canvas);
    }

    @DexIgnore
    public final int getMActiveColor() {
        return this.x;
    }

    @DexIgnore
    public final String getMBackgroundColor() {
        return this.p0;
    }

    @DexIgnore
    public final int getMBarAlpha() {
        return this.n0;
    }

    @DexIgnore
    public final float getMBarMargin() {
        return this.j0;
    }

    @DexIgnore
    public final float getMBarMarginEnd() {
        return this.k0;
    }

    @DexIgnore
    public final float getMBarRadius() {
        return this.h0;
    }

    @DexIgnore
    public final float getMBarSpace() {
        return this.i0;
    }

    @DexIgnore
    public final d getMBarTouchListener() {
        return this.w;
    }

    @DexIgnore
    public final float getMBarWidth() {
        return this.g0;
    }

    @DexIgnore
    public final c getMChartModel() {
        return this.q0;
    }

    @DexIgnore
    public final int getMDefaultColor() {
        return this.A;
    }

    @DexIgnore
    public final PointF getMGoalIconPoint() {
        return this.E;
    }

    @DexIgnore
    public final boolean getMGoalIconShow() {
        return this.F;
    }

    @DexIgnore
    public final int getMGoalIconSize() {
        return this.D;
    }

    @DexIgnore
    public final Path getMGoalLinePath() {
        return this.P;
    }

    @DexIgnore
    public final Rh5 getMGoalType() {
        return this.r0;
    }

    @DexIgnore
    public final int getMGoalnonBrandLineColor() {
        return this.L;
    }

    @DexIgnore
    public final Paint getMGraphGoalLinePaint() {
        Paint paint = this.t0;
        if (paint != null) {
            return paint;
        }
        Wg6.n("mGraphGoalLinePaint");
        throw null;
    }

    @DexIgnore
    public final float getMGraphLegendMargin() {
        return this.c0;
    }

    @DexIgnore
    public final ArrayList<Lc6<Integer, PointF>> getMGraphLegendPoint() {
        return this.d0;
    }

    @DexIgnore
    public final Paint getMGraphPaint() {
        Paint paint = this.s0;
        if (paint != null) {
            return paint;
        }
        Wg6.n("mGraphPaint");
        throw null;
    }

    @DexIgnore
    public final int getMHighestColor() {
        return this.B;
    }

    @DexIgnore
    public final int getMInActiveColor() {
        return this.y;
    }

    @DexIgnore
    public final boolean getMIsFlexibleSize() {
        return this.o0;
    }

    @DexIgnore
    public final int getMLegendIconRes() {
        return this.T;
    }

    @DexIgnore
    public final Paint getMLegendLinePaint() {
        Paint paint = this.w0;
        if (paint != null) {
            return paint;
        }
        Wg6.n("mLegendLinePaint");
        throw null;
    }

    @DexIgnore
    public final Paint getMLegendPaint() {
        Paint paint = this.v0;
        if (paint != null) {
            return paint;
        }
        Wg6.n("mLegendPaint");
        throw null;
    }

    @DexIgnore
    public final ArrayList<String> getMLegendTexts() {
        return this.S;
    }

    @DexIgnore
    public final int getMLowestColor() {
        return this.z;
    }

    @DexIgnore
    public final int getMMaxValue() {
        return this.m0;
    }

    @DexIgnore
    public final int getMNumberBar() {
        return this.l0;
    }

    @DexIgnore
    public final float getMSafeAreaHeight() {
        return this.e0;
    }

    @DexIgnore
    public final ArrayList<PointF> getMStarIconPoint() {
        return this.I;
    }

    @DexIgnore
    public final boolean getMStarIconShow() {
        return this.J;
    }

    @DexIgnore
    public final int getMStarIconSize() {
        return this.H;
    }

    @DexIgnore
    public final String getMTextColor() {
        return this.V;
    }

    @DexIgnore
    public final int getMTextMargin() {
        return this.W;
    }

    @DexIgnore
    public final ArrayList<Lc6<String, PointF>> getMTextPoint() {
        return this.b0;
    }

    @DexIgnore
    public final float getMTextSize() {
        return this.U;
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public boolean i(MotionEvent motionEvent) {
        Wg6.c(motionEvent, Constants.EVENT);
        PointF pointF = new PointF(motionEvent.getX(), motionEvent.getY());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onGraphOverlayTouchEvent - pointF=" + pointF);
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                return false;
            }
            Iterator<a> it = this.q0.b().iterator();
            while (it.hasNext()) {
                a next = it.next();
                Iterator<ArrayList<b>> it2 = next.d().iterator();
                while (it2.hasNext()) {
                    ArrayList<b> next2 = it2.next();
                    if (this.r0 == Rh5.TOTAL_SLEEP) {
                        if (BaseChart.v.a(pointF, B(new RectF(next2.get(0).a().left, next2.get(next2.size() - 1).a().top, next2.get(0).a().right, next2.get(0).a().bottom), this.f0))) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String tag2 = getTAG();
                            local2.d(tag2, "onGraphOverlayTouchEvent for total sleep - ACTION_UP = TRUE, index=" + next.c());
                            d dVar = this.w;
                            if (dVar != null) {
                                dVar.a(next.c());
                            }
                        }
                    } else {
                        Iterator<b> it3 = next2.iterator();
                        while (it3.hasNext()) {
                            if (BaseChart.v.a(pointF, B(it3.next().a(), this.f0))) {
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String tag3 = getTAG();
                                local3.d(tag3, "onGraphOverlayTouchEvent for normal chart item- ACTION_UP = TRUE, index=" + next.c());
                                d dVar2 = this.w;
                                if (dVar2 != null) {
                                    dVar2.a(next.c());
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void k(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        super.k(canvas);
        o();
        y(canvas);
        z(canvas);
    }

    @DexIgnore
    public void m() {
        float f2;
        ArrayList<a> b2 = this.q0.b();
        RectF rectF = new RectF(this.j0, this.e0, ((float) getMGraphWidth()) - this.k0, (float) getMGraphHeight());
        float height = rectF.height();
        float width = rectF.width();
        if (this.o0) {
            float f3 = this.i0;
            int i = this.l0;
            this.g0 = (width - (f3 * ((float) (i - 1)))) / ((float) i);
        } else {
            this.i0 = q(width);
        }
        float f4 = this.f0;
        float f5 = this.i0;
        if (f4 > 0.5f * f5) {
            this.f0 = 0.5f * f5;
        }
        float f6 = rectF.left;
        float f7 = (float) 10;
        PointF pointF = new PointF(rectF.right + f7, rectF.top);
        PointF pointF2 = new PointF(rectF.right + f7, (rectF.bottom + rectF.top) * 0.5f);
        this.P = new Path();
        this.E = new PointF();
        this.F = false;
        this.I.clear();
        this.J = false;
        this.d0.clear();
        Iterator<a> it = b2.iterator();
        boolean z2 = true;
        boolean z3 = true;
        float f8 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (it.hasNext()) {
            ArrayList<b> arrayList = it.next().d().get(0);
            Wg6.b(arrayList, "item.mListOfBarPoints[0]");
            List<b> b02 = Pm7.b0(arrayList, new f());
            float f9 = this.g0 + f6;
            float f10 = 0.0f;
            for (b bVar : b02) {
                float e2 = f10 + ((float) bVar.e());
                float f11 = (e2 * height) / ((float) this.m0);
                if (f11 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    f2 = this.h0;
                    if (f11 < f2) {
                        f8 = ((float) getMGraphHeight()) - f2;
                        bVar.f(new RectF(f6, f8, f9, (float) getMGraphHeight()));
                        f10 = e2;
                    }
                }
                f2 = f11;
                f8 = ((float) getMGraphHeight()) - f2;
                bVar.f(new RectF(f6, f8, f9, (float) getMGraphHeight()));
                f10 = e2;
            }
            z2 = (f9 <= pointF2.x || f8 >= pointF2.y) ? z2 : false;
            if (f9 > pointF.x && f8 < pointF.y) {
                z3 = false;
            }
            f6 = f9 + this.i0;
        }
        if (z2) {
            this.d0.add(new Lc6<>(Integer.valueOf(this.m0 / 2), pointF2));
        }
        if (z3) {
            this.d0.add(new Lc6<>(Integer.valueOf(this.m0), pointF));
            return;
        }
        pointF.set(pointF.x, this.e0 - 10.0f);
        this.d0.add(new Lc6<>(Integer.valueOf(this.m0), pointF));
    }

    @DexIgnore
    public void n() {
        boolean z2;
        float f2;
        ArrayList<a> b2 = this.q0.b();
        int c2 = this.q0.c();
        RectF rectF = new RectF(this.j0, this.e0, ((float) getMGraphWidth()) - this.k0, (float) getMGraphHeight());
        float height = rectF.height();
        float f3 = rectF.left;
        Iterator<a> it = b2.iterator();
        boolean z3 = false;
        float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f5 = 0.0f;
        float f6 = f3;
        while (it.hasNext()) {
            ArrayList<b> arrayList = it.next().d().get(0);
            Wg6.b(arrayList, "item.mListOfBarPoints[0]");
            List<b> b02 = Pm7.b0(arrayList, new g());
            float f7 = this.g0 + f6;
            float f8 = 0.0f;
            for (b bVar : b02) {
                float e2 = f8 + ((float) bVar.e());
                float f9 = (e2 * height) / ((float) this.m0);
                if (f9 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    f2 = this.h0;
                    if (f9 < f2) {
                        f4 = ((float) getMGraphHeight()) - f2;
                        bVar.f(new RectF(f6, f4, f7, (float) getMGraphHeight()));
                        f8 = e2;
                    }
                }
                f2 = f9;
                f4 = ((float) getMGraphHeight()) - f2;
                bVar.f(new RectF(f6, f4, f7, (float) getMGraphHeight()));
                f8 = e2;
            }
            float f10 = f8 + f5;
            if (f10 < ((float) c2) || z3) {
                z2 = z3;
            } else {
                float f11 = (f6 + f7) * 0.5f;
                this.P.moveTo(f11, this.e0 - 10.0f);
                this.P.lineTo(f11, f4);
                this.E.set(f11 - (((float) this.D) * 0.5f), (this.e0 * 0.5f) - 20.0f);
                this.F = true;
                z2 = true;
            }
            z3 = z2;
            f5 = f10;
            f6 = this.i0 + f7;
        }
    }

    @DexIgnore
    public final void o() {
        RectF rectF = new RectF(this.j0, this.e0, ((float) getMGraphWidth()) - this.k0, (float) getMGraphHeight());
        this.b0.clear();
        p(rectF.left, rectF.right);
    }

    @DexIgnore
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    public void p(float f2, float f3) {
        Rect rect = new Rect();
        Paint paint = this.v0;
        if (paint != null) {
            paint.getTextBounds("12 am", 0, 5, rect);
            float height = ((float) this.W) + ((float) rect.height());
            this.b0.add(new Lc6<>("12 am", new PointF(f2, height)));
            this.b0.add(new Lc6<>("12 am", new PointF(f3 - ((float) rect.width()), height)));
            Paint paint2 = this.v0;
            if (paint2 != null) {
                paint2.getTextBounds("12 pm", 0, 5, rect);
                this.b0.add(new Lc6<>("12 pm", new PointF(((f2 + f3) - ((float) rect.width())) * 0.5f, height)));
                return;
            }
            Wg6.n("mLegendPaint");
            throw null;
        }
        Wg6.n("mLegendPaint");
        throw null;
    }

    @DexIgnore
    public float q(float f2) {
        float f3 = this.g0;
        int i = this.l0;
        return (f2 - (f3 * ((float) i))) / ((float) (i - 1));
    }

    @DexIgnore
    public void r(Mv5 mv5) {
        synchronized (this) {
            Wg6.c(mv5, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "changeModel - model=" + mv5);
            if (!Wg6.a(this.q0, mv5)) {
                I(mv5);
                e();
            }
        }
    }

    @DexIgnore
    public Bitmap s(int i, int i2) {
        Exception e2;
        int i3;
        int i4;
        Bitmap bitmap = null;
        if (i == -1) {
            return null;
        }
        try {
            Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), i);
            if (decodeResource == null) {
                return decodeResource;
            }
            if (i2 == -1) {
                try {
                    int width = decodeResource.getWidth();
                    i4 = decodeResource.getHeight();
                    i3 = width;
                } catch (Exception e3) {
                    e2 = e3;
                    bitmap = decodeResource;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String tag = getTAG();
                    local.d(tag, "createBitmapByRes - e=" + e2);
                    return bitmap;
                }
            } else {
                i3 = i2;
                i4 = i2;
            }
            return Bitmap.createScaledBitmap(decodeResource, i3, i4, false);
        } catch (Exception e4) {
            e2 = e4;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String tag2 = getTAG();
            local2.d(tag2, "createBitmapByRes - e=" + e2);
            return bitmap;
        }
    }

    @DexIgnore
    public final void setBarAlpha(int i) {
        this.n0 = i;
        Paint paint = this.s0;
        if (paint != null) {
            paint.setAlpha(i);
            Paint paint2 = this.t0;
            if (paint2 != null) {
                paint2.setAlpha(this.n0);
                Paint paint3 = this.u0;
                if (paint3 != null) {
                    paint3.setAlpha(this.n0);
                    e();
                    return;
                }
                Wg6.n("mGraphIconPaint");
                throw null;
            }
            Wg6.n("mGraphGoalLinePaint");
            throw null;
        }
        Wg6.n("mGraphPaint");
        throw null;
    }

    @DexIgnore
    public final void setMActiveColor(int i) {
        this.x = i;
    }

    @DexIgnore
    public final void setMBackgroundColor(String str) {
        Wg6.c(str, "<set-?>");
        this.p0 = str;
    }

    @DexIgnore
    public final void setMBarAlpha(int i) {
        this.n0 = i;
    }

    @DexIgnore
    public final void setMBarMargin(float f2) {
        this.j0 = f2;
    }

    @DexIgnore
    public final void setMBarMarginEnd(float f2) {
        this.k0 = f2;
    }

    @DexIgnore
    public final void setMBarRadius(float f2) {
        this.h0 = f2;
    }

    @DexIgnore
    public final void setMBarSpace(float f2) {
        this.i0 = f2;
    }

    @DexIgnore
    public final void setMBarTouchListener(d dVar) {
        this.w = dVar;
    }

    @DexIgnore
    public final void setMBarWidth(float f2) {
        this.g0 = f2;
    }

    @DexIgnore
    public final void setMChartModel(c cVar) {
        Wg6.c(cVar, "<set-?>");
        this.q0 = cVar;
    }

    @DexIgnore
    public final void setMDefaultColor(int i) {
        this.A = i;
    }

    @DexIgnore
    public final void setMGoalIconPoint(PointF pointF) {
        Wg6.c(pointF, "<set-?>");
        this.E = pointF;
    }

    @DexIgnore
    public final void setMGoalIconShow(boolean z2) {
        this.F = z2;
    }

    @DexIgnore
    public final void setMGoalIconSize(int i) {
        this.D = i;
    }

    @DexIgnore
    public final void setMGoalLinePath(Path path) {
        Wg6.c(path, "<set-?>");
        this.P = path;
    }

    @DexIgnore
    public final void setMGoalType(Rh5 rh5) {
        Wg6.c(rh5, "<set-?>");
        this.r0 = rh5;
    }

    @DexIgnore
    public final void setMGoalnonBrandLineColor(int i) {
        this.L = i;
    }

    @DexIgnore
    public final void setMGraphGoalLinePaint(Paint paint) {
        Wg6.c(paint, "<set-?>");
        this.t0 = paint;
    }

    @DexIgnore
    public final void setMGraphLegendMargin(float f2) {
        this.c0 = f2;
    }

    @DexIgnore
    public final void setMGraphLegendPoint(ArrayList<Lc6<Integer, PointF>> arrayList) {
        Wg6.c(arrayList, "<set-?>");
        this.d0 = arrayList;
    }

    @DexIgnore
    public final void setMGraphPaint(Paint paint) {
        Wg6.c(paint, "<set-?>");
        this.s0 = paint;
    }

    @DexIgnore
    public final void setMHighestColor(int i) {
        this.B = i;
    }

    @DexIgnore
    public final void setMInActiveColor(int i) {
        this.y = i;
    }

    @DexIgnore
    public final void setMIsFlexibleSize(boolean z2) {
        this.o0 = z2;
    }

    @DexIgnore
    public final void setMLegendIconRes(int i) {
        this.T = i;
    }

    @DexIgnore
    public final void setMLegendLinePaint(Paint paint) {
        Wg6.c(paint, "<set-?>");
        this.w0 = paint;
    }

    @DexIgnore
    public final void setMLegendPaint(Paint paint) {
        Wg6.c(paint, "<set-?>");
        this.v0 = paint;
    }

    @DexIgnore
    public final void setMLegendTexts(ArrayList<String> arrayList) {
        Wg6.c(arrayList, "<set-?>");
        this.S = arrayList;
    }

    @DexIgnore
    public final void setMLowestColor(int i) {
        this.z = i;
    }

    @DexIgnore
    public final void setMMaxValue(int i) {
        this.m0 = i;
    }

    @DexIgnore
    public final void setMNumberBar(int i) {
        this.l0 = i;
    }

    @DexIgnore
    public final void setMSafeAreaHeight(float f2) {
        this.e0 = f2;
    }

    @DexIgnore
    public final void setMStarIconPoint(ArrayList<PointF> arrayList) {
        Wg6.c(arrayList, "<set-?>");
        this.I = arrayList;
    }

    @DexIgnore
    public final void setMStarIconShow(boolean z2) {
        this.J = z2;
    }

    @DexIgnore
    public final void setMStarIconSize(int i) {
        this.H = i;
    }

    @DexIgnore
    public final void setMTextColor(String str) {
        Wg6.c(str, "<set-?>");
        this.V = str;
    }

    @DexIgnore
    public final void setMTextMargin(int i) {
        this.W = i;
    }

    @DexIgnore
    public final void setMTextPoint(ArrayList<Lc6<String, PointF>> arrayList) {
        Wg6.c(arrayList, "<set-?>");
        this.b0 = arrayList;
    }

    @DexIgnore
    public final void setMTextSize(float f2) {
        this.U = f2;
    }

    @DexIgnore
    public final void setMaxValue(int i) {
        this.m0 = i;
        e();
    }

    @DexIgnore
    public final void setOnTouchListener(d dVar) {
        Wg6.c(dVar, "barTouchListener");
        this.w = dVar;
    }

    @DexIgnore
    public void u(Canvas canvas) {
        int i;
        Wg6.c(canvas, "canvas");
        Iterator<a> it = this.q0.b().iterator();
        while (it.hasNext()) {
            ArrayList<b> arrayList = it.next().d().get(0);
            Wg6.b(arrayList, "item.mListOfBarPoints[0]");
            Iterator it2 = Pm7.b0(arrayList, new h()).iterator();
            while (true) {
                if (it2.hasNext()) {
                    b bVar = (b) it2.next();
                    if (bVar.e() != 0) {
                        Paint paint = this.s0;
                        if (paint != null) {
                            int i2 = Lv5.a[bVar.c().ordinal()];
                            if (i2 == 1) {
                                i = this.z;
                            } else if (i2 == 2) {
                                i = this.A;
                            } else if (i2 == 3) {
                                i = this.B;
                            } else {
                                throw new Kc6();
                            }
                            paint.setColor(i);
                            float f2 = bVar.a().left;
                            float f3 = bVar.a().top;
                            float f4 = bVar.a().right;
                            float f5 = bVar.a().bottom;
                            float f6 = this.h0;
                            Paint paint2 = this.s0;
                            if (paint2 != null) {
                                K37.a(canvas, f2, f3, f4, f5, f6, f6, true, true, false, false, paint2);
                            } else {
                                Wg6.n("mGraphPaint");
                                throw null;
                            }
                        } else {
                            Wg6.n("mGraphPaint");
                            throw null;
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void v(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        w(canvas);
        A(canvas);
    }

    @DexIgnore
    public void w(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        if (this.F) {
            Path path = this.P;
            Paint paint = this.t0;
            if (paint != null) {
                canvas.drawPath(path, paint);
                Bitmap s = s(this.C, this.D);
                if (s != null) {
                    PointF pointF = this.E;
                    float f2 = pointF.x;
                    float f3 = pointF.y;
                    Paint paint2 = this.u0;
                    if (paint2 != null) {
                        canvas.drawBitmap(s, f2, f3, paint2);
                        s.recycle();
                        return;
                    }
                    Wg6.n("mGraphIconPaint");
                    throw null;
                }
                return;
            }
            Wg6.n("mGraphGoalLinePaint");
            throw null;
        }
    }

    @DexIgnore
    public void x(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        Iterator<Lc6<Integer, PointF>> it = this.d0.iterator();
        while (it.hasNext()) {
            Lc6<Integer, PointF> next = it.next();
            int intValue = next.getFirst().intValue();
            float f2 = next.getSecond().x;
            float f3 = next.getSecond().y;
            Paint paint = this.v0;
            if (paint != null) {
                canvas.drawText(String.valueOf(intValue), f2, f3, paint);
            } else {
                Wg6.n("mLegendPaint");
                throw null;
            }
        }
    }

    @DexIgnore
    public void y(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        float f2 = this.R;
        float width = (float) canvas.getWidth();
        float f3 = this.R;
        Paint paint = this.w0;
        if (paint != null) {
            canvas.drawLine(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2 * 0.5f, width, f3 * 0.5f, paint);
        } else {
            Wg6.n("mLegendLinePaint");
            throw null;
        }
    }

    @DexIgnore
    public void z(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        Iterator<Lc6<String, PointF>> it = this.b0.iterator();
        while (it.hasNext()) {
            Lc6<String, PointF> next = it.next();
            String first = next.getFirst();
            float f2 = next.getSecond().x;
            float f3 = next.getSecond().y;
            Paint paint = this.v0;
            if (paint != null) {
                canvas.drawText(first, f2, f3, paint);
            } else {
                Wg6.n("mLegendPaint");
                throw null;
            }
        }
    }
}
