package com.portfolio.platform.ui.stats.activity.month.domain.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FetchSummaries extends CoroutineUseCase<Ai, CoroutineUseCase.Di, CoroutineUseCase.Ai> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ SummariesRepository d;
    @DexIgnore
    public /* final */ FitnessDataRepository e;
    @DexIgnore
    public /* final */ UserRepository f;
    @DexIgnore
    public /* final */ ActivitiesRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public Ai(Date date) {
            Wg6.c(date, "date");
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries", f = "FetchSummaries.kt", l = {29, 57, 58, 61}, m = "run")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FetchSummaries this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(FetchSummaries fetchSummaries, Xe6 xe6) {
            super(xe6);
            this.this$0 = fetchSummaries;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, this);
        }
    }

    /*
    static {
        String simpleName = FetchSummaries.class.getSimpleName();
        Wg6.b(simpleName, "FetchSummaries::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public FetchSummaries(SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, ActivitiesRepository activitiesRepository) {
        Wg6.c(summariesRepository, "mSummariesRepository");
        Wg6.c(fitnessDataRepository, "mFitnessDataRepository");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(activitiesRepository, "mActivitiesRepository");
        this.d = summariesRepository;
        this.e = fitnessDataRepository;
        this.f = userRepository;
        this.g = activitiesRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return h;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ai ai, Xe6 xe6) {
        return m(ai, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0171  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0262  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object m(com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries.Ai r23, com.mapped.Xe6<? super com.mapped.Cd6> r24) {
        /*
        // Method dump skipped, instructions count: 629
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries.m(com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries$Ai, com.mapped.Xe6):java.lang.Object");
    }
}
