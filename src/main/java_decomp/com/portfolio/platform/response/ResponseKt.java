package com.portfolio.platform.response;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Em7;
import com.fossil.Hq5;
import com.fossil.Kq5;
import com.fossil.Q88;
import com.fossil.W18;
import com.google.gson.Gson;
import com.mapped.Ap4;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.model.ServerError;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ResponseKt {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.response.ResponseKt", f = "Response.kt", l = {15}, m = "handleRequest")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;

        @DexIgnore
        public Ai(Xe6 xe6) {
            super(xe6);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return ResponseKt.d(null, this);
        }
    }

    @DexIgnore
    public static final <T> Hq5<T> a(Throwable th) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("RepoResponse", "create=" + th.getMessage());
        return th instanceof SocketTimeoutException ? new Hq5<>(MFNetworkReturnCode.CLIENT_TIMEOUT, null, th, null, null, 24, null) : th instanceof UnknownHostException ? new Hq5<>(601, null, th, null, null, 24, null) : new Hq5<>(600, null, th, null, null, 24, null);
    }

    @DexIgnore
    public static final <T> Ap4<T> b(Q88<T> q88) {
        Wg6.c(q88, "$this$createRepoResponse");
        try {
            return c(q88);
        } catch (Throwable th) {
            return a(th);
        }
    }

    @DexIgnore
    public static final <T> Ap4<T> c(Q88<T> q88) {
        String f;
        String str;
        Hq5 hq5;
        Integer code;
        String f2;
        boolean z = true;
        Integer num = null;
        if (q88.e()) {
            T a2 = q88.a();
            if (q88.g().c() != null) {
                FLogger.INSTANCE.getLocal().d("RepoResponse", "cacheResponse valid");
            } else {
                z = false;
            }
            if (q88.g().A() != null) {
                Response A = q88.g().A();
                if (A == null) {
                    Wg6.i();
                    throw null;
                } else if (A.f() != 304) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("networkResponse valid httpCode ");
                    Response A2 = q88.g().A();
                    if (A2 != null) {
                        num = Integer.valueOf(A2.f());
                    }
                    sb.append(num);
                    local.d("RepoResponse", sb.toString());
                    z = false;
                }
            }
            FLogger.INSTANCE.getLocal().d("RepoResponse", "isFromCache=" + z);
            return new Kq5(a2, z);
        }
        int b = q88.b();
        if (Em7.B(new Integer[]{504, 503, 500, 401, Integer.valueOf((int) MFNetworkReturnCode.RATE_LIMIT_EXEEDED), 601, Integer.valueOf((int) MFNetworkReturnCode.CLIENT_TIMEOUT), 413}, Integer.valueOf(b))) {
            ServerError serverError = new ServerError();
            serverError.setCode(Integer.valueOf(b));
            W18 d = q88.d();
            if (d == null || (f2 = d.string()) == null) {
                f2 = q88.f();
            }
            serverError.setMessage(f2);
            return new Hq5(b, serverError, null, null, null, 24, null);
        }
        W18 d2 = q88.d();
        if (d2 == null || (f = d2.string()) == null) {
            f = q88.f();
        }
        try {
            ServerError serverError2 = (ServerError) new Gson().k(f, ServerError.class);
            hq5 = (serverError2 == null || ((code = serverError2.getCode()) != null && code.intValue() == 0)) ? new Hq5(q88.b(), null, null, f, null, 16, null) : new Hq5(q88.b(), serverError2, null, null, null, 24, null);
        } catch (Exception e) {
            W18 d3 = q88.d();
            if (d3 == null || (str = d3.string()) == null) {
                str = f;
            }
            hq5 = new Hq5(q88.b(), new ServerError(b, str), null, null, null, 24, null);
        }
        return hq5;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T> java.lang.Object d(com.mapped.Hg6<? super com.mapped.Xe6<? super com.fossil.Q88<T>>, ? extends java.lang.Object> r8, com.mapped.Xe6<? super com.mapped.Ap4<T>> r9) {
        /*
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = 24
            r2 = 0
            boolean r0 = r9 instanceof com.portfolio.platform.response.ResponseKt.Ai
            if (r0 == 0) goto L_0x0031
            r0 = r9
            com.portfolio.platform.response.ResponseKt$Ai r0 = (com.portfolio.platform.response.ResponseKt.Ai) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0031
            int r1 = r1 + r4
            r0.label = r1
        L_0x0016:
            java.lang.Object r3 = r0.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x003f
            if (r4 != r5) goto L_0x0037
            java.lang.Object r0 = r0.L$0
            com.mapped.Hg6 r0 = (com.mapped.Hg6) r0
            com.fossil.El7.b(r3)     // Catch:{ Exception -> 0x004f }
            r0 = r3
        L_0x002a:
            com.fossil.Q88 r0 = (com.fossil.Q88) r0     // Catch:{ Exception -> 0x004f }
            com.mapped.Ap4 r0 = b(r0)     // Catch:{ Exception -> 0x004f }
        L_0x0030:
            return r0
        L_0x0031:
            com.portfolio.platform.response.ResponseKt$Ai r0 = new com.portfolio.platform.response.ResponseKt$Ai
            r0.<init>(r9)
            goto L_0x0016
        L_0x0037:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003f:
            com.fossil.El7.b(r3)
            r0.L$0 = r8
            r3 = 1
            r0.label = r3
            java.lang.Object r0 = r8.invoke(r0)
            if (r0 != r1) goto L_0x002a
            r0 = r1
            goto L_0x0030
        L_0x004f:
            r3 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r4 = "exception handleRequest "
            r1.append(r4)
            r1.append(r3)
            java.lang.String r4 = "RepoResponse"
            java.lang.String r1 = r1.toString()
            r0.d(r4, r1)
            boolean r0 = r3 instanceof java.net.SocketTimeoutException
            if (r0 == 0) goto L_0x007b
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            r1 = 408(0x198, float:5.72E-43)
            r4 = r2
            r5 = r2
            r7 = r2
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0030
        L_0x007b:
            boolean r0 = r3 instanceof java.net.UnknownHostException
            if (r0 == 0) goto L_0x008a
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            r1 = 601(0x259, float:8.42E-43)
            r4 = r2
            r5 = r2
            r7 = r2
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0030
        L_0x008a:
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            r1 = 600(0x258, float:8.41E-43)
            r4 = r2
            r5 = r2
            r7 = r2
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.response.ResponseKt.d(com.mapped.Hg6, com.mapped.Xe6):java.lang.Object");
    }
}
