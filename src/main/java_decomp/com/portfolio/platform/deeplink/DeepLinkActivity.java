package com.portfolio.platform.deeplink;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.fossil.B77;
import com.fossil.Hh5;
import com.fossil.Hq4;
import com.fossil.Ls0;
import com.fossil.Po4;
import com.fossil.Ts0;
import com.fossil.Vs0;
import com.mapped.AlertDialogFragment;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.watchface.faces.WatchFaceListActivity;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeepLinkActivity extends BaseActivity implements AlertDialogFragment.Gi {
    @DexIgnore
    public Po4 A;
    @DexIgnore
    public Hh5 B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Ls0<Hq4.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ DeepLinkActivity a;

        @DexIgnore
        public a(DeepLinkActivity deepLinkActivity) {
            this.a = deepLinkActivity;
        }

        @DexIgnore
        public final void a(Hq4.Bi bi) {
            if (bi.a()) {
                this.a.F();
            }
            if (bi.b()) {
                this.a.t();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Bi bi) {
            a(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Ls0<Hh5.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ DeepLinkActivity a;
        @DexIgnore
        public /* final */ /* synthetic */ Uri b;

        @DexIgnore
        public b(DeepLinkActivity deepLinkActivity, Uri uri) {
            this.a = deepLinkActivity;
            this.b = uri;
        }

        @DexIgnore
        public final void a(Hh5.Ai ai) {
            if (ai.b()) {
                this.a.finish();
                WatchFaceGalleryActivity.B.b(this.a, this.b);
            }
            if (ai.c()) {
                this.a.finish();
                WatchFaceListActivity.C.b(this.a, this.b);
            }
            if (ai.d()) {
                this.a.finish();
                WelcomeActivity.B.c(this.a);
            }
            if (ai.a()) {
                this.a.finish();
                HomeActivity.B.d(this.a);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hh5.Ai ai) {
            a(ai);
        }
    }

    /*
    static {
        Wg6.b(DeepLinkActivity.class.getSimpleName(), "DeepLinkActivity::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        PortfolioApp.get.instance().getIface().S1().a(this);
        Po4 po4 = this.A;
        if (po4 != null) {
            Ts0 a2 = Vs0.f(this, po4).a(Hh5.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026inkViewModel::class.java)");
            Hh5 hh5 = (Hh5) a2;
            this.B = hh5;
            if (hh5 != null) {
                hh5.j().h(this, new a(this));
                Intent intent = getIntent();
                Uri data = intent != null ? intent.getData() : null;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String r = r();
                local.d(r, "uri " + data);
                if (data != null) {
                    String lastPathSegment = data.getLastPathSegment();
                    if (lastPathSegment != null) {
                        B77 b77 = B77.a;
                        Wg6.b(lastPathSegment, "it");
                        b77.f(lastPathSegment);
                    }
                    Hh5 hh52 = this.B;
                    if (hh52 != null) {
                        hh52.t().h(this, new b(this, data));
                        Hh5 hh53 = this.B;
                        if (hh53 != null) {
                            hh53.p(data);
                        } else {
                            Wg6.n("mViewModel");
                            throw null;
                        }
                    } else {
                        Wg6.n("mViewModel");
                        throw null;
                    }
                }
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModelFactory");
            throw null;
        }
    }
}
