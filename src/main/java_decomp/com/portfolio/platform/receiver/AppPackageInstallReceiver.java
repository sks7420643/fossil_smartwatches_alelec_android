package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bw7;
import com.fossil.D26;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.V36;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.DeviceHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppPackageInstallReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public NotificationsRepository a;
    @DexIgnore
    public An4 b;
    @DexIgnore
    public Cj4 c;
    @DexIgnore
    public V36 d;
    @DexIgnore
    public D26 e;
    @DexIgnore
    public NotificationSettingsDatabase f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.receiver.AppPackageInstallReceiver$onReceive$1", f = "AppPackageInstallReceiver.kt", l = {67}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $packageName;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AppPackageInstallReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(AppPackageInstallReceiver appPackageInstallReceiver, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = appPackageInstallReceiver;
            this.$packageName = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, this.$packageName, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                FLogger.INSTANCE.getLocal().d(AppPackageInstallReceiver.g, "new app installed, start full sync");
                AppFilter appFilter = new AppFilter();
                appFilter.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_DIANA.ordinal());
                appFilter.setType(this.$packageName);
                this.this$0.b().saveAppFilter(appFilter);
                AppPackageInstallReceiver appPackageInstallReceiver = this.this$0;
                this.L$0 = il6;
                this.L$1 = appFilter;
                this.label = 1;
                if (appPackageInstallReceiver.c(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                AppFilter appFilter2 = (AppFilter) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.receiver.AppPackageInstallReceiver", f = "AppPackageInstallReceiver.kt", l = {73}, m = "setRuleNotificationFilterToDevice")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ AppPackageInstallReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(AppPackageInstallReceiver appPackageInstallReceiver, Xe6 xe6) {
            super(xe6);
            this.this$0 = appPackageInstallReceiver;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    /*
    static {
        String simpleName = AppPackageRemoveReceiver.class.getSimpleName();
        Wg6.b(simpleName, "AppPackageRemoveReceiver::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public AppPackageInstallReceiver() {
        PortfolioApp.get.instance().getIface().l(this);
    }

    @DexIgnore
    public final NotificationsRepository b() {
        NotificationsRepository notificationsRepository = this.a;
        if (notificationsRepository != null) {
            return notificationsRepository;
        }
        Wg6.n("mNotificationsRepository");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object c(com.mapped.Xe6<? super com.mapped.Cd6> r10) {
        /*
            r9 = this;
            r8 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.receiver.AppPackageInstallReceiver.Bi
            if (r0 == 0) goto L_0x004a
            r0 = r10
            com.portfolio.platform.receiver.AppPackageInstallReceiver$Bi r0 = (com.portfolio.platform.receiver.AppPackageInstallReceiver.Bi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x004a
            int r1 = r1 + r3
            r0.label = r1
            r5 = r0
        L_0x0015:
            java.lang.Object r1 = r5.result
            java.lang.Object r6 = com.fossil.Yn7.d()
            int r0 = r5.label
            if (r0 == 0) goto L_0x0058
            if (r0 != r8) goto L_0x0050
            java.lang.Object r0 = r5.L$0
            com.portfolio.platform.receiver.AppPackageInstallReceiver r0 = (com.portfolio.platform.receiver.AppPackageInstallReceiver) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0029:
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings r1 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings
            java.util.List r0 = (java.util.List) r0
            long r2 = java.lang.System.currentTimeMillis()
            r1.<init>(r0, r2)
            com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r0 = r0.instance()
            com.portfolio.platform.PortfolioApp$inner r2 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r2 = r2.instance()
            java.lang.String r2 = r2.J()
            r0.s1(r1, r2)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0049:
            return r0
        L_0x004a:
            com.portfolio.platform.receiver.AppPackageInstallReceiver$Bi r5 = new com.portfolio.platform.receiver.AppPackageInstallReceiver$Bi
            r5.<init>(r9, r10)
            goto L_0x0015
        L_0x0050:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0058:
            com.fossil.El7.b(r1)
            com.portfolio.platform.util.NotificationAppHelper r0 = com.portfolio.platform.util.NotificationAppHelper.b
            com.fossil.V36 r1 = r9.d
            if (r1 == 0) goto L_0x008b
            com.fossil.D26 r2 = r9.e
            if (r2 == 0) goto L_0x0085
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r3 = r9.f
            if (r3 == 0) goto L_0x007f
            com.mapped.An4 r4 = r9.b
            if (r4 == 0) goto L_0x0079
            r5.L$0 = r9
            r5.label = r8
            java.lang.Object r0 = r0.c(r1, r2, r3, r4, r5)
            if (r0 != r6) goto L_0x0029
            r0 = r6
            goto L_0x0049
        L_0x0079:
            java.lang.String r0 = "mSharedPreferencesManager"
            com.mapped.Wg6.n(r0)
            throw r7
        L_0x007f:
            java.lang.String r0 = "mNotificationSettingsDatabase"
            com.mapped.Wg6.n(r0)
            throw r7
        L_0x0085:
            java.lang.String r0 = "mGetAllContactGroup"
            com.mapped.Wg6.n(r0)
            throw r7
        L_0x008b:
            java.lang.String r0 = "mGetApps"
            com.mapped.Wg6.n(r0)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.receiver.AppPackageInstallReceiver.c(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        Uri data;
        An4 an4 = this.b;
        if (an4 != null) {
            boolean W = an4.W();
            String J = PortfolioApp.get.instance().J();
            String encodedSchemeSpecificPart = (intent == null || (data = intent.getData()) == null) ? null : data.getEncodedSchemeSpecificPart();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            StringBuilder sb = new StringBuilder();
            sb.append("onReceive action ");
            sb.append(intent != null ? intent.getAction() : null);
            sb.append(" new app install ");
            sb.append(encodedSchemeSpecificPart);
            sb.append(", isAllAppEnable ");
            sb.append(W);
            local.d(str, sb.toString());
            if (W && DeviceHelper.o.x(J) && !TextUtils.isEmpty(encodedSchemeSpecificPart)) {
                Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Ai(this, encodedSchemeSpecificPart, null), 3, null);
                return;
            }
            return;
        }
        Wg6.n("mSharedPreferencesManager");
        throw null;
    }
}
