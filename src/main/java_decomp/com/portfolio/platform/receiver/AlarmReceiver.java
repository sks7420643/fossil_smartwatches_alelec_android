package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AlarmHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmReceiver extends BroadcastReceiver {
    @DexIgnore
    public UserRepository a;
    @DexIgnore
    public An4 b;
    @DexIgnore
    public DeviceRepository c;
    @DexIgnore
    public AlarmHelper d;
    @DexIgnore
    public AlarmsRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.receiver.AlarmReceiver$onReceive$1", f = "AlarmReceiver.kt", l = {56, 59, 104}, m = "invokeSuspend")
    public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public /* final */ /* synthetic */ Context $context;
        @DexIgnore
        public /* final */ /* synthetic */ Intent $intent;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public long J$0;
        @DexIgnore
        public long J$1;
        @DexIgnore
        public long J$2;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(AlarmReceiver alarmReceiver, int i, Intent intent, Context context, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = alarmReceiver;
            this.$action = i;
            this.$intent = intent;
            this.$context = context;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            a aVar = new a(this.this$0, this.$action, this.$intent, this.$context, xe6);
            aVar.p$ = (Il6) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00e0  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x015f  */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x029f  */
        /* JADX WARNING: Removed duplicated region for block: B:74:0x0313  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r19) {
            /*
            // Method dump skipped, instructions count: 929
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.receiver.AlarmReceiver.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public final AlarmHelper a() {
        AlarmHelper alarmHelper = this.d;
        if (alarmHelper != null) {
            return alarmHelper;
        }
        Wg6.n("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public final AlarmsRepository b() {
        AlarmsRepository alarmsRepository = this.e;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        Wg6.n("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository c() {
        DeviceRepository deviceRepository = this.c;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        Wg6.n("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final An4 d() {
        An4 an4 = this.b;
        if (an4 != null) {
            return an4;
        }
        Wg6.n("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final UserRepository e() {
        UserRepository userRepository = this.a;
        if (userRepository != null) {
            return userRepository;
        }
        Wg6.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        Wg6.c(context, "context");
        PortfolioApp.get.instance().getIface().i(this);
        FLogger.INSTANCE.getLocal().d("AlarmReceiver", "onReceive");
        if (intent != null) {
            int intExtra = intent.getIntExtra("DEF_ALARM_RECEIVER_ACTION", -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmReceiver", "onReceive - action=" + intExtra);
            Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new a(this, intExtra, intent, context, null), 3, null);
        }
    }
}
