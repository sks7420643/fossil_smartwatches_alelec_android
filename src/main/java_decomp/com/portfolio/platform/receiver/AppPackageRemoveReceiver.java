package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.util.NotificationAppHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppPackageRemoveReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public NotificationsRepository a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.receiver.AppPackageRemoveReceiver$onReceive$1", f = "AppPackageRemoveReceiver.kt", l = {}, m = "invokeSuspend")
    public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $packageName;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AppPackageRemoveReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(AppPackageRemoveReceiver appPackageRemoveReceiver, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = appPackageRemoveReceiver;
            this.$packageName = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            a aVar = new a(this.this$0, this.$packageName, xe6);
            aVar.p$ = (Il6) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                AppFilter f = NotificationAppHelper.b.f(this.$packageName, MFDeviceFamily.DEVICE_FAMILY_SAM);
                AppFilter f2 = NotificationAppHelper.b.f(this.$packageName, MFDeviceFamily.DEVICE_FAMILY_DIANA);
                if (f != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = AppPackageRemoveReceiver.b;
                    local.d(str, "remove app " + this.$packageName);
                    this.this$0.b().removeAppFilter(f);
                }
                if (f2 != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = AppPackageRemoveReceiver.b;
                    local2.d(str2, "remove app " + this.$packageName);
                    this.this$0.b().removeAppFilter(f2);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = AppPackageRemoveReceiver.class.getSimpleName();
        Wg6.b(simpleName, "AppPackageRemoveReceiver::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public AppPackageRemoveReceiver() {
        PortfolioApp.get.instance().getIface().s(this);
    }

    @DexIgnore
    public final NotificationsRepository b() {
        NotificationsRepository notificationsRepository = this.a;
        if (notificationsRepository != null) {
            return notificationsRepository;
        }
        Wg6.n("notificationsRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String schemeSpecificPart;
        Wg6.c(context, "context");
        Wg6.c(intent, "intent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "onReceive " + intent.getAction());
        if (Wg6.a("android.intent.action.PACKAGE_FULLY_REMOVED", intent.getAction())) {
            Uri data = intent.getData();
            String str2 = (data == null || (schemeSpecificPart = data.getSchemeSpecificPart()) == null) ? "" : schemeSpecificPart;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b;
            local2.d(str3, "package removed " + str2);
            Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new a(this, str2, null), 3, null);
        }
    }
}
