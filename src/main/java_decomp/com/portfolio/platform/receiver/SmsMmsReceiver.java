package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import com.fossil.Bw7;
import com.fossil.Cm5;
import com.fossil.Dm5;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jh6;
import com.mapped.Lf6;
import com.mapped.PduParser;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.model.NotificationInfo;
import com.portfolio.platform.manager.LightAndHapticsManager;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import com.portfolio.platform.util.NotificationAppHelper;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SmsMmsReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public DianaNotificationComponent a;
    @DexIgnore
    public An4 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.receiver.SmsMmsReceiver$onReceive$1", f = "SmsMmsReceiver.kt", l = {}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $notificationInfo;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Jh6 jh6, Xe6 xe6) {
            super(2, xe6);
            this.$notificationInfo = jh6;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.$notificationInfo, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                String defaultSmsPackage = Telephony.Sms.getDefaultSmsPackage(PortfolioApp.get.instance());
                if (defaultSmsPackage == null || NotificationAppHelper.b.i(defaultSmsPackage)) {
                    FLogger.INSTANCE.getLocal().d(SmsMmsReceiver.c, "onReceive() - SMS app filter is assigned - ignore");
                } else {
                    LightAndHapticsManager.i.a().h(this.$notificationInfo.element);
                    FLogger.INSTANCE.getLocal().d(SmsMmsReceiver.c, "onReceive) - SMS app filter is not assigned - add to Queue");
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = SmsMmsReceiver.class.getSimpleName();
        Wg6.b(simpleName, "SmsMmsReceiver::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        Dm5 h;
        Wg6.c(context, "context");
        Wg6.c(intent, "intent");
        Jh6 jh6 = new Jh6();
        jh6.element = null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = c;
        StringBuilder sb = new StringBuilder();
        sb.append("SmsMmsReceiver : ");
        String action = intent.getAction();
        if (action != null) {
            sb.append(action);
            local.d(str, sb.toString());
            An4 an4 = this.b;
            if (an4 == null) {
                Wg6.n("mSharePref");
                throw null;
            } else if (an4.q0()) {
                FLogger.INSTANCE.getLocal().d(c, "SmsMmsReceiver user new solution");
            } else {
                String str2 = "";
                if (TextUtils.isEmpty(intent.getAction()) || !Wg6.a(intent.getAction(), "android.provider.Telephony.WAP_PUSH_RECEIVED")) {
                    try {
                        SmsMessage[] messagesFromIntent = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                        if (messagesFromIntent != null) {
                            if (!(messagesFromIntent.length == 0)) {
                                for (SmsMessage smsMessage : messagesFromIntent) {
                                    Wg6.b(smsMessage, "message");
                                    String messageBody = smsMessage.getMessageBody();
                                    String originatingAddress = smsMessage.getOriginatingAddress();
                                    jh6.element = (T) new NotificationInfo(NotificationSource.TEXT, originatingAddress, messageBody, "");
                                    if (!(TextUtils.isEmpty(messageBody) || TextUtils.isEmpty(originatingAddress))) {
                                        break;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        FLogger.INSTANCE.getLocal().e(c, "onReceive() - SMS - error " + e);
                    }
                } else {
                    try {
                        if (intent.hasExtra("data") && (h = new PduParser(intent.getByteArrayExtra("data")).h()) != null && h.a() != null) {
                            Cm5 a2 = h.a();
                            Wg6.b(a2, "pdu.from");
                            String d = a2.d();
                            FLogger.INSTANCE.getLocal().d(c, "onReceive() - MMS - originatingAddress = " + d);
                            jh6.element = (T) new NotificationInfo(NotificationSource.TEXT, d, "", "");
                        }
                    } catch (Exception e2) {
                        FLogger.INSTANCE.getLocal().e(c, "onReceive() - MMS - error " + e2);
                    }
                }
                if (jh6.element != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = c;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("onReceive() - SMSMMS - sender info ");
                    String senderInfo = jh6.element.getSenderInfo();
                    if (senderInfo != null) {
                        sb2.append(senderInfo);
                        local2.d(str3, sb2.toString());
                        if (!FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.get.instance().J())) {
                            An4 an42 = this.b;
                            if (an42 == null) {
                                Wg6.n("mSharePref");
                                throw null;
                            } else if (an42.Z()) {
                                FLogger.INSTANCE.getLocal().d(c, "onReceive() - Blocked by DND mode");
                            } else {
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String str4 = c;
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("onReceive() - SMS-MMS - sender info ");
                                String senderInfo2 = jh6.element.getSenderInfo();
                                if (senderInfo2 != null) {
                                    sb3.append(senderInfo2);
                                    local3.d(str4, sb3.toString());
                                    Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Ai(jh6, null), 3, null);
                                    return;
                                }
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            if (jh6.element.getSenderInfo() != null) {
                                str2 = jh6.element.getSenderInfo();
                            }
                            DianaNotificationComponent dianaNotificationComponent = this.a;
                            if (dianaNotificationComponent == null) {
                                Wg6.n("mDianaNotificationComponent");
                                throw null;
                            } else if (str2 != null) {
                                String body = jh6.element.getBody();
                                Wg6.b(body, "notificationInfo.body");
                                Calendar instance = Calendar.getInstance();
                                Wg6.b(instance, "Calendar.getInstance()");
                                dianaNotificationComponent.V(str2, body, instance.getTimeInMillis());
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
        } else {
            Wg6.i();
            throw null;
        }
    }
}
