package com.portfolio.platform.util;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.Ao7;
import com.fossil.El7;
import com.fossil.Fu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Qq7;
import com.fossil.Vt7;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.mapped.An4;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rl6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceUtils {
    @DexIgnore
    public static DeviceUtils f;
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ Ai h; // = new Ai(null);
    @DexIgnore
    public DeviceRepository a;
    @DexIgnore
    public An4 b;
    @DexIgnore
    public UserRepository c;
    @DexIgnore
    public GuestApiService d;
    @DexIgnore
    public FirmwareFileRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final DeviceUtils a() {
            DeviceUtils b;
            synchronized (this) {
                if (DeviceUtils.h.b() == null) {
                    DeviceUtils.h.c(new DeviceUtils(null));
                }
                b = DeviceUtils.h.b();
                if (b == null) {
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.util.DeviceUtils");
                }
            }
            return b;
        }

        @DexIgnore
        public final DeviceUtils b() {
            return DeviceUtils.f;
        }

        @DexIgnore
        public final void c(DeviceUtils deviceUtils) {
            DeviceUtils.f = deviceUtils;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.util.DeviceUtils", f = "DeviceUtils.kt", l = {57, 79, 84}, m = "downloadActiveDeviceFirmware")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceUtils this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(DeviceUtils deviceUtils, Xe6 xe6) {
            super(xe6);
            this.this$0 = deviceUtils;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2", f = "DeviceUtils.kt", l = {100}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ap4 $repoResponse;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceUtils this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Qq7 implements Hg6<Firmware, Rl6<? extends Boolean>> {
            @DexIgnore
            public /* final */ /* synthetic */ Il6 $this_withContext;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2$2$1", f = "DeviceUtils.kt", l = {96}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Firmware $it;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Firmware firmware, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                    this.$it = firmware;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$it, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        DeviceUtils deviceUtils = this.this$0.this$0.this$0;
                        Firmware firmware = this.$it;
                        this.L$0 = il6;
                        this.label = 1;
                        Object e = deviceUtils.e(firmware, this);
                        return e == d ? d : e;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Il6 il6) {
                super(1);
                this.this$0 = ci;
                this.$this_withContext = il6;
            }

            @DexIgnore
            public final Rl6<Boolean> invoke(Firmware firmware) {
                Wg6.c(firmware, "it");
                return Gu7.b(this.$this_withContext, null, null, new Aiii(this, firmware, null), 3, null);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Hg6
            public /* bridge */ /* synthetic */ Rl6<? extends Boolean> invoke(Firmware firmware) {
                return invoke(firmware);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(DeviceUtils deviceUtils, Ap4 ap4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = deviceUtils;
            this.$repoResponse = ap4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$repoResponse, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0066  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x011c  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0120  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r18) {
            /*
            // Method dump skipped, instructions count: 307
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.util.DeviceUtils.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$repoResponse$1", f = "DeviceUtils.kt", l = {79}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<Firmware>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $model;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceUtils this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(DeviceUtils deviceUtils, String str, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = deviceUtils;
            this.$model = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Di(this.this$0, this.$model, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<ApiResponse<Firmware>>> xe6) {
            throw null;
            //return ((Di) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                GuestApiService g = this.this$0.g();
                String P = PortfolioApp.get.instance().P();
                String str = this.$model;
                Wg6.b(str, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                this.label = 1;
                Object firmwares = g.getFirmwares(P, str, "android", false, this);
                return firmwares == d ? d : firmwares;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.util.DeviceUtils", f = "DeviceUtils.kt", l = {133}, m = "downloadDetailFirmware")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceUtils this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(DeviceUtils deviceUtils, Xe6 xe6) {
            super(xe6);
            this.this$0 = deviceUtils;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.util.DeviceUtils$isDeviceDianaEV1Java$1", f = "DeviceUtils.kt", l = {}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceSerial;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceUtils this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(DeviceUtils deviceUtils, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = deviceUtils;
            this.$deviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, this.$deviceSerial, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return Ao7.a(this.this$0.h(this.$deviceSerial));
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.util.DeviceUtils", f = "DeviceUtils.kt", l = {200}, m = "isLatestFirmware")
    public static final class Gi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceUtils this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(DeviceUtils deviceUtils, Xe6 xe6) {
            super(xe6);
            this.this$0 = deviceUtils;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.j(null, null, this);
        }
    }

    /*
    static {
        String simpleName = DeviceUtils.class.getSimpleName();
        Wg6.b(simpleName, "DeviceUtils::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public DeviceUtils() {
        PortfolioApp.get.instance().getIface().Q(this);
    }

    @DexIgnore
    public /* synthetic */ DeviceUtils(Qg6 qg6) {
        this();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01cc  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x020d  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0211  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.mapped.Xe6<? super com.mapped.Cd6> r14) {
        /*
        // Method dump skipped, instructions count: 545
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.util.DeviceUtils.d(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0184  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.portfolio.platform.data.model.Firmware r14, com.mapped.Xe6<? super java.lang.Boolean> r15) {
        /*
        // Method dump skipped, instructions count: 397
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.util.DeviceUtils.e(com.portfolio.platform.data.model.Firmware, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.mapped.Lc6<java.lang.String, java.lang.String> f(java.lang.String r8, com.portfolio.platform.data.model.Device r9) {
        /*
            r7 = this;
            r1 = 0
            java.lang.String r0 = "deviceSerial"
            com.mapped.Wg6.c(r8, r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.util.DeviceUtils.g
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "fetchFirmwaresInfo - deviceSerial="
            r3.append(r4)
            r3.append(r8)
            java.lang.String r4 = ", activeDevice="
            r3.append(r4)
            r3.append(r9)
            java.lang.String r3 = r3.toString()
            r0.d(r2, r3)
            if (r9 != 0) goto L_0x0034
            com.portfolio.platform.data.source.DeviceRepository r0 = r7.a
            if (r0 == 0) goto L_0x003c
            com.portfolio.platform.data.model.Device r9 = r0.getDeviceBySerial(r8)
        L_0x0034:
            if (r9 != 0) goto L_0x0042
            com.mapped.Lc6 r0 = new com.mapped.Lc6
            r0.<init>(r1, r1)
        L_0x003b:
            return r0
        L_0x003c:
            java.lang.String r0 = "mDeviceRepository"
            com.mapped.Wg6.n(r0)
            throw r1
        L_0x0042:
            java.lang.String r2 = r9.getFirmwareRevision()
            java.lang.String r0 = "release"
            java.lang.String r3 = "release"
            r4 = 1
            boolean r0 = com.fossil.Vt7.j(r0, r3, r4)
            if (r0 != 0) goto L_0x005b
            com.mapped.An4 r0 = r7.b
            if (r0 == 0) goto L_0x00bd
            boolean r0 = r0.Y()
            if (r0 != 0) goto L_0x0096
        L_0x005b:
            com.fossil.Mn5$Ai r0 = com.fossil.Mn5.p
            com.fossil.Mn5 r0 = r0.a()
            com.fossil.Jp5 r0 = r0.g()
            java.lang.String r3 = r9.getSku()
            com.portfolio.platform.data.model.Firmware r0 = r0.e(r3)
        L_0x006d:
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.portfolio.platform.util.DeviceUtils.g
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "fetchFirmwaresInfo - latestFw="
            r5.append(r6)
            r5.append(r0)
            java.lang.String r5 = r5.toString()
            r3.d(r4, r5)
            if (r0 == 0) goto L_0x00c3
            java.lang.String r0 = r0.getVersionNumber()
        L_0x008f:
            com.mapped.Lc6 r1 = new com.mapped.Lc6
            r1.<init>(r2, r0)
            r0 = r1
            goto L_0x003b
        L_0x0096:
            com.mapped.An4 r0 = r7.b
            if (r0 == 0) goto L_0x00b7
            java.lang.String r3 = r9.getSku()
            com.portfolio.platform.data.model.Firmware r0 = r0.k(r3)
            if (r0 != 0) goto L_0x006d
            com.fossil.Mn5$Ai r0 = com.fossil.Mn5.p
            com.fossil.Mn5 r0 = r0.a()
            com.fossil.Jp5 r0 = r0.g()
            java.lang.String r3 = r9.getSku()
            com.portfolio.platform.data.model.Firmware r0 = r0.e(r3)
            goto L_0x006d
        L_0x00b7:
            java.lang.String r0 = "mSharePrefs"
            com.mapped.Wg6.n(r0)
            throw r1
        L_0x00bd:
            java.lang.String r0 = "mSharePrefs"
            com.mapped.Wg6.n(r0)
            throw r1
        L_0x00c3:
            r0 = r1
            goto L_0x008f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.util.DeviceUtils.f(java.lang.String, com.portfolio.platform.data.model.Device):com.mapped.Lc6");
    }

    @DexIgnore
    public final GuestApiService g() {
        GuestApiService guestApiService = this.d;
        if (guestApiService != null) {
            return guestApiService;
        }
        Wg6.n("mGuestApiService");
        throw null;
    }

    @DexIgnore
    public final boolean h(String str) {
        String firmwareRevision;
        Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        if (!FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
            return false;
        }
        DeviceRepository deviceRepository = this.a;
        if (deviceRepository != null) {
            Device deviceBySerial = deviceRepository.getDeviceBySerial(str);
            if (deviceBySerial == null || (firmwareRevision = deviceBySerial.getFirmwareRevision()) == null) {
                return false;
            }
            return Vt7.r(firmwareRevision, "DN0.0.0.", true);
        }
        Wg6.n("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final boolean i(String str) {
        Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        return ((Boolean) Fu7.b(null, new Fi(this, str, null), 1, null)).booleanValue();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0143  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01ac  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object j(java.lang.String r15, com.portfolio.platform.data.model.Device r16, com.mapped.Xe6<? super java.lang.Boolean> r17) {
        /*
        // Method dump skipped, instructions count: 497
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.util.DeviceUtils.j(java.lang.String, com.portfolio.platform.data.model.Device, com.mapped.Xe6):java.lang.Object");
    }
}
