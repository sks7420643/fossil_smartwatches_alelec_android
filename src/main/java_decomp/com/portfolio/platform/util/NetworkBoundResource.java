package com.portfolio.platform.util;

import androidx.lifecycle.LiveData;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.H47;
import com.fossil.Hq5;
import com.fossil.Js0;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Kq5;
import com.fossil.Ls0;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.ServerError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class NetworkBoundResource<ResultType, RequestType> {
    @DexIgnore
    public boolean isFromCache; // = true;
    @DexIgnore
    public /* final */ Js0<H47<ResultType>> result;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.util.NetworkBoundResource$1", f = "NetworkBoundResource.kt", l = {34, 36}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NetworkBoundResource this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.util.NetworkBoundResource$1$1", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ LiveData $dbSource;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii<T> implements Ls0<S> {
                @DexIgnore
                public /* final */ /* synthetic */ Aii a;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii<T> implements Ls0<S> {
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii a;

                    @DexIgnore
                    public Aiiii(Aiii aiii) {
                        this.a = aiii;
                    }

                    @DexIgnore
                    @Override // com.fossil.Ls0
                    public final void onChanged(ResultType resulttype) {
                        this.a.a.this$0.this$0.setValue(H47.e.d(resulttype));
                    }
                }

                @DexIgnore
                public Aiii(Aii aii) {
                    this.a = aii;
                }

                @DexIgnore
                @Override // com.fossil.Ls0
                public final void onChanged(ResultType resulttype) {
                    this.a.this$0.this$0.result.q(this.a.$dbSource);
                    if (this.a.this$0.this$0.shouldFetch(resulttype)) {
                        Aii aii = this.a;
                        aii.this$0.this$0.fetchFromNetwork(aii.$dbSource);
                        return;
                    }
                    this.a.this$0.this$0.result.p(this.a.$dbSource, new Aiiii(this));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, LiveData liveData, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
                this.$dbSource = liveData;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$dbSource, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.result.p(this.$dbSource, new Aiii(this));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(NetworkBoundResource networkBoundResource, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = networkBoundResource;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0046  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 2
                r2 = 1
                java.lang.Object r3 = com.fossil.Yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0048
                if (r0 == r2) goto L_0x0024
                if (r0 != r6) goto L_0x001c
                java.lang.Object r0 = r7.L$1
                androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
            L_0x0019:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x001b:
                return r0
            L_0x001c:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0024:
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
                r2 = r0
                r1 = r8
            L_0x002d:
                r0 = r1
                androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                com.fossil.Jx7 r1 = com.fossil.Bw7.c()
                com.portfolio.platform.util.NetworkBoundResource$Ai$Aii r4 = new com.portfolio.platform.util.NetworkBoundResource$Ai$Aii
                r5 = 0
                r4.<init>(r7, r0, r5)
                r7.L$0 = r2
                r7.L$1 = r0
                r7.label = r6
                java.lang.Object r0 = com.fossil.Eu7.g(r1, r4, r7)
                if (r0 != r3) goto L_0x0019
                r0 = r3
                goto L_0x001b
            L_0x0048:
                com.fossil.El7.b(r8)
                com.mapped.Il6 r0 = r7.p$
                com.portfolio.platform.util.NetworkBoundResource r1 = r7.this$0
                r7.L$0 = r0
                r7.label = r2
                java.lang.Object r1 = r1.loadFromDb(r7)
                if (r1 != r3) goto L_0x005b
                r0 = r3
                goto L_0x001b
            L_0x005b:
                r2 = r0
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.util.NetworkBoundResource.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1", f = "NetworkBoundResource.kt", l = {58, 65, 68, 76, 76, 79, 85, 88, 98, 111}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ LiveData $dbSource;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NetworkBoundResource this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$1", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii<T> implements Ls0<S> {
                @DexIgnore
                public /* final */ /* synthetic */ Aii a;

                @DexIgnore
                public Aiii(Aii aii) {
                    this.a = aii;
                }

                @DexIgnore
                @Override // com.fossil.Ls0
                public final void onChanged(ResultType resulttype) {
                    this.a.this$0.this$0.setValue(H47.e.c(resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.result.p(this.this$0.$dbSource, new Aiii(this));
                    this.this$0.this$0.result.q(this.this$0.$dbSource);
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$2", f = "NetworkBoundResource.kt", l = {71}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii<T> implements Ls0<S> {
                @DexIgnore
                public /* final */ /* synthetic */ Bii a;

                @DexIgnore
                public Aiii(Bii bii) {
                    this.a = bii;
                }

                @DexIgnore
                @Override // com.fossil.Ls0
                public final void onChanged(ResultType resulttype) {
                    this.a.this$0.this$0.setValue(H47.e.d(resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object loadFromDb;
                Js0 js0;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    FLogger.INSTANCE.getLocal().d("NetworkBoundResource", "set value dbSource fetched from network response null");
                    Js0 js02 = this.this$0.this$0.result;
                    NetworkBoundResource networkBoundResource = this.this$0.this$0;
                    this.L$0 = il6;
                    this.L$1 = js02;
                    this.label = 1;
                    loadFromDb = networkBoundResource.loadFromDb(this);
                    if (loadFromDb == d) {
                        return d;
                    }
                    js0 = js02;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    js0 = (Js0) this.L$1;
                    loadFromDb = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                js0.p((LiveData) loadFromDb, new Aiii(this));
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$3", f = "NetworkBoundResource.kt", l = {79, 79}, m = "invokeSuspend")
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Ap4 $response;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Bi bi, Ap4 ap4, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$response = ap4;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, this.$response, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 2
                    r4 = 1
                    java.lang.Object r3 = com.fossil.Yn7.d()
                    int r0 = r6.label
                    if (r0 == 0) goto L_0x0038
                    if (r0 == r4) goto L_0x0020
                    if (r0 != r5) goto L_0x0018
                    java.lang.Object r0 = r6.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r7)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r6.L$1
                    com.portfolio.platform.util.NetworkBoundResource r0 = (com.portfolio.platform.util.NetworkBoundResource) r0
                    java.lang.Object r1 = r6.L$0
                    com.mapped.Il6 r1 = (com.mapped.Il6) r1
                    com.fossil.El7.b(r7)
                    r2 = r1
                L_0x002c:
                    r6.L$0 = r2
                    r6.label = r5
                    java.lang.Object r0 = r0.saveCallResult(r7, r6)
                    if (r0 != r3) goto L_0x0015
                    r0 = r3
                    goto L_0x0017
                L_0x0038:
                    com.fossil.El7.b(r7)
                    com.mapped.Il6 r2 = r6.p$
                    com.portfolio.platform.util.NetworkBoundResource$Bi r0 = r6.this$0
                    com.portfolio.platform.util.NetworkBoundResource r1 = r0.this$0
                    com.mapped.Ap4 r0 = r6.$response
                    com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
                    r6.L$0 = r2
                    r6.L$1 = r1
                    r6.label = r4
                    java.lang.Object r7 = r1.processResponse(r0, r6)
                    if (r7 != r3) goto L_0x0053
                    r0 = r3
                    goto L_0x0017
                L_0x0053:
                    r0 = r1
                    goto L_0x002c
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.util.NetworkBoundResource.Bi.Cii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$4", f = "NetworkBoundResource.kt", l = {85, 85}, m = "invokeSuspend")
        public static final class Dii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Ap4 $response;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Dii(Bi bi, Ap4 ap4, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$response = ap4;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Dii dii = new Dii(this.this$0, this.$response, xe6);
                dii.p$ = (Il6) obj;
                throw null;
                //return dii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Dii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 2
                    r4 = 1
                    java.lang.Object r3 = com.fossil.Yn7.d()
                    int r0 = r6.label
                    if (r0 == 0) goto L_0x0038
                    if (r0 == r4) goto L_0x0020
                    if (r0 != r5) goto L_0x0018
                    java.lang.Object r0 = r6.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r7)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r6.L$1
                    com.portfolio.platform.util.NetworkBoundResource r0 = (com.portfolio.platform.util.NetworkBoundResource) r0
                    java.lang.Object r1 = r6.L$0
                    com.mapped.Il6 r1 = (com.mapped.Il6) r1
                    com.fossil.El7.b(r7)
                    r2 = r1
                L_0x002c:
                    r6.L$0 = r2
                    r6.label = r5
                    java.lang.Object r0 = r0.saveCallResult(r7, r6)
                    if (r0 != r3) goto L_0x0015
                    r0 = r3
                    goto L_0x0017
                L_0x0038:
                    com.fossil.El7.b(r7)
                    com.mapped.Il6 r2 = r6.p$
                    com.portfolio.platform.util.NetworkBoundResource$Bi r0 = r6.this$0
                    com.portfolio.platform.util.NetworkBoundResource r1 = r0.this$0
                    com.mapped.Ap4 r0 = r6.$response
                    com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
                    r6.L$0 = r2
                    r6.L$1 = r1
                    r6.label = r4
                    java.lang.Object r7 = r1.processResponse(r0, r6)
                    if (r7 != r3) goto L_0x0053
                    r0 = r3
                    goto L_0x0017
                L_0x0053:
                    r0 = r1
                    goto L_0x002c
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.util.NetworkBoundResource.Bi.Dii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$5", f = "NetworkBoundResource.kt", l = {93}, m = "invokeSuspend")
        public static final class Eii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii<T> implements Ls0<S> {
                @DexIgnore
                public /* final */ /* synthetic */ Eii a;

                @DexIgnore
                public Aiii(Eii eii) {
                    this.a = eii;
                }

                @DexIgnore
                @Override // com.fossil.Ls0
                public final void onChanged(ResultType resulttype) {
                    this.a.this$0.this$0.setValue(H47.e.d(resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Eii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Eii eii = new Eii(this.this$0, xe6);
                eii.p$ = (Il6) obj;
                throw null;
                //return eii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Eii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object loadFromDb;
                Js0 js0;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    FLogger.INSTANCE.getLocal().d("NetworkBoundResource", "set value dbSource fetched from network success");
                    Js0 js02 = this.this$0.this$0.result;
                    NetworkBoundResource networkBoundResource = this.this$0.this$0;
                    this.L$0 = il6;
                    this.L$1 = js02;
                    this.label = 1;
                    loadFromDb = networkBoundResource.loadFromDb(this);
                    if (loadFromDb == d) {
                        return d;
                    }
                    js0 = js02;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    js0 = (Js0) this.L$1;
                    loadFromDb = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                js0.p((LiveData) loadFromDb, new Aiii(this));
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$6", f = "NetworkBoundResource.kt", l = {99}, m = "invokeSuspend")
        public static final class Fii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii<T> implements Ls0<S> {
                @DexIgnore
                public /* final */ /* synthetic */ Fii a;

                @DexIgnore
                public Aiii(Fii fii) {
                    this.a = fii;
                }

                @DexIgnore
                @Override // com.fossil.Ls0
                public final void onChanged(ResultType resulttype) {
                    this.a.this$0.this$0.setValue(H47.e.c(resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Fii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Fii fii = new Fii(this.this$0, xe6);
                fii.p$ = (Il6) obj;
                throw null;
                //return fii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Fii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object loadFromDb;
                Js0 js0;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Js0 js02 = this.this$0.this$0.result;
                    NetworkBoundResource networkBoundResource = this.this$0.this$0;
                    this.L$0 = il6;
                    this.L$1 = js02;
                    this.label = 1;
                    loadFromDb = networkBoundResource.loadFromDb(this);
                    if (loadFromDb == d) {
                        return d;
                    }
                    js0 = js02;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    loadFromDb = obj;
                    js0 = (Js0) this.L$1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                js0.p((LiveData) loadFromDb, new Aiii(this));
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$7", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
        public static final class Gii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Ap4 $response;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii<T> implements Ls0<S> {
                @DexIgnore
                public /* final */ /* synthetic */ Gii a;

                @DexIgnore
                public Aiii(Gii gii) {
                    this.a = gii;
                }

                @DexIgnore
                @Override // com.fossil.Ls0
                public final void onChanged(ResultType resulttype) {
                    String message;
                    Gii gii = this.a;
                    NetworkBoundResource networkBoundResource = gii.this$0.this$0;
                    H47.Ai ai = H47.e;
                    int a2 = ((Hq5) gii.$response).a();
                    ServerError c = ((Hq5) this.a.$response).c();
                    if (c == null || (message = c.getUserMessage()) == null) {
                        ServerError c2 = ((Hq5) this.a.$response).c();
                        message = c2 != null ? c2.getMessage() : null;
                    }
                    if (message == null) {
                        message = "";
                    }
                    networkBoundResource.setValue(ai.b(a2, message, resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Gii(Bi bi, Ap4 ap4, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$response = ap4;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Gii gii = new Gii(this.this$0, this.$response, xe6);
                gii.p$ = (Il6) obj;
                throw null;
                //return gii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Gii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.result.p(this.this$0.$dbSource, new Aiii(this));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$response$1", f = "NetworkBoundResource.kt", l = {65}, m = "invokeSuspend")
        public static final class Hii extends Ko7 implements Hg6<Xe6<? super Q88<RequestType>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Hii(Bi bi, Xe6 xe6) {
                super(1, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                return new Hii(this.this$0, xe6);
            }

            @DexIgnore
            @Override // com.mapped.Hg6
            public final Object invoke(Object obj) {
                throw null;
                //return ((Hii) create((Xe6) obj)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    NetworkBoundResource networkBoundResource = this.this$0.this$0;
                    this.label = 1;
                    Object createCall = networkBoundResource.createCall(this);
                    return createCall == d ? d : createCall;
                } else if (i == 1) {
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(NetworkBoundResource networkBoundResource, LiveData liveData, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = networkBoundResource;
            this.$dbSource = liveData;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$dbSource, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x006c  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00bf  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00de  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x0117  */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x014a  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x0180  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x019a  */
        /* JADX WARNING: Removed duplicated region for block: B:63:0x01d1  */
        /* JADX WARNING: Removed duplicated region for block: B:65:0x01d8  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0027  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
            // Method dump skipped, instructions count: 502
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.util.NetworkBoundResource.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public NetworkBoundResource() {
        Js0<H47<ResultType>> js0 = new Js0<>();
        this.result = js0;
        js0.o(H47.e.a(null));
        this.isFromCache = true;
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    private final void fetchFromNetwork(LiveData<ResultType> liveData) {
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Bi(this, liveData, null), 3, null);
    }

    @DexIgnore
    public static /* synthetic */ Object processResponse$suspendImpl(NetworkBoundResource networkBoundResource, Kq5 kq5, Xe6 xe6) {
        Object a2 = kq5.a();
        if (a2 != null) {
            return a2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    private final void setValue(H47<? extends ResultType> h47) {
        if (!Wg6.a(this.result.e(), h47)) {
            this.result.o(h47);
        }
    }

    @DexIgnore
    public final LiveData<H47<ResultType>> asLiveData() {
        Js0<H47<ResultType>> js0 = this.result;
        if (js0 != null) {
            return js0;
        }
        throw new Rc6("null cannot be cast to non-null type androidx.lifecycle.LiveData<com.portfolio.platform.util.Resource<ResultType>>");
    }

    @DexIgnore
    public abstract Object createCall(Xe6<? super Q88<RequestType>> xe6);

    @DexIgnore
    public abstract Object loadFromDb(Xe6<? super LiveData<ResultType>> xe6);

    @DexIgnore
    public void onFetchFailed(Throwable th) {
    }

    @DexIgnore
    public Object processContinueFetching(RequestType requesttype, Xe6<? super Boolean> xe6) {
        return Ao7.a(false);
    }

    @DexIgnore
    public Object processResponse(Kq5<RequestType> kq5, Xe6<? super RequestType> xe6) {
        return processResponse$suspendImpl(this, kq5, xe6);
    }

    @DexIgnore
    public abstract Object saveCallResult(RequestType requesttype, Xe6<? super Cd6> xe6);

    @DexIgnore
    public abstract boolean shouldFetch(ResultType resulttype);
}
