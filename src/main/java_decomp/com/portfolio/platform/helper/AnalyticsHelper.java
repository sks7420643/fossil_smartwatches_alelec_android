package com.portfolio.platform.helper;

import android.app.Activity;
import android.os.Bundle;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.Bw7;
import com.fossil.Ej5;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Sl5;
import com.fossil.Tl5;
import com.fossil.Ul5;
import com.fossil.Vl5;
import com.fossil.Yn7;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Mj6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.zendesk.sdk.network.impl.DeviceInfo;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AnalyticsHelper {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static FirebaseAnalytics c;
    @DexIgnore
    public static AnalyticsHelper d;
    @DexIgnore
    public static /* final */ HashMap<String, Ul5> e; // = new HashMap<>();
    @DexIgnore
    public static /* final */ Ai f; // = new Ai(null);
    @DexIgnore
    public volatile String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(String str, Ul5 ul5) {
            Wg6.c(str, "tracingKey");
            Wg6.c(ul5, "trace");
            i().put(str, ul5);
        }

        @DexIgnore
        public final Sl5 b(String str) {
            Wg6.c(str, "eventName");
            return new Sl5(g(), str);
        }

        @DexIgnore
        public final Ul5 c(String str) {
            Wg6.c(str, "traceName");
            AnalyticsHelper g = g();
            return Wg6.a("view_appearance", str) ? new Vl5(g, str, g.g()) : new Ul5(g, str, g.g());
        }

        @DexIgnore
        public final Tl5 d() {
            return new Tl5(g());
        }

        @DexIgnore
        public final Vl5 e() {
            AnalyticsHelper g = g();
            return new Vl5(g, "view_appearance", g.g());
        }

        @DexIgnore
        public final Ul5 f(String str) {
            Wg6.c(str, "tracingKey");
            return i().get(str);
        }

        @DexIgnore
        public final AnalyticsHelper g() {
            AnalyticsHelper j;
            synchronized (this) {
                if (j() == null) {
                    l(new AnalyticsHelper(null));
                }
                j = j();
                if (j == null) {
                    Wg6.i();
                    throw null;
                }
            }
            return j;
        }

        @DexIgnore
        public final String h(String str) {
            Wg6.c(str, "input");
            return new Mj6("[^a-zA-Z0-9]+").replace(str, LocaleConverter.LOCALE_DELIMITER);
        }

        @DexIgnore
        public final HashMap<String, Ul5> i() {
            return AnalyticsHelper.e;
        }

        @DexIgnore
        public final AnalyticsHelper j() {
            return AnalyticsHelper.d;
        }

        @DexIgnore
        public final void k(String str) {
            Wg6.c(str, "tracingKey");
            i().remove(str);
        }

        @DexIgnore
        public final void l(AnalyticsHelper analyticsHelper) {
            AnalyticsHelper.d = analyticsHelper;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.helper.AnalyticsHelper$doLogEvent$1", f = "AnalyticsHelper.kt", l = {}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bundle $data;
        @DexIgnore
        public /* final */ /* synthetic */ String $event;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(String str, Bundle bundle, Xe6 xe6) {
            super(2, xe6);
            this.$event = str;
            this.$data = bundle;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.$event, this.$data, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                FirebaseAnalytics firebaseAnalytics = AnalyticsHelper.c;
                if (firebaseAnalytics != null) {
                    firebaseAnalytics.a(this.$event, this.$data);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = AnalyticsHelper.class.getSimpleName();
        Wg6.b(simpleName, "AnalyticsHelper::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public AnalyticsHelper() {
        c = FirebaseAnalytics.getInstance(PortfolioApp.get.instance());
    }

    @DexIgnore
    public /* synthetic */ AnalyticsHelper(Qg6 qg6) {
        this();
    }

    @DexIgnore
    public final Map<String, String> e(String str, String str2, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("Style_Number", str);
        hashMap.put("Device_Name", str2);
        hashMap.put("Type", String.valueOf(i));
        return hashMap;
    }

    @DexIgnore
    public final void f(String str, Bundle bundle) {
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Bi(str, bundle, null), 3, null);
    }

    @DexIgnore
    public final String g() {
        return this.a;
    }

    @DexIgnore
    public final void h(String str, String str2, String str3) {
        Wg6.c(str, "serialPrefix");
        Wg6.c(str2, "activeDeviceName");
        Wg6.c(str3, "firmwareVersion");
        Tl5 d2 = f.d();
        d2.a("serial_number_prefix", str);
        d2.a(DeviceInfo.DEVICE_INFO_DEVICE_NAME, str2);
        d2.a(Constants.FIRMWARE_VERSION, str3);
        d2.b();
    }

    @DexIgnore
    public final void i(String str) {
        Wg6.c(str, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "Inside .analyticLogEvent event=" + str);
        f(str, null);
    }

    @DexIgnore
    public final void j(String str, String str2) {
        Wg6.c(str, Constants.EVENT);
        Wg6.c(str2, "value");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b;
        local.d(str3, "Inside .analyticLogEvent event=" + str + ", value=" + str2);
        Bundle bundle = new Bundle();
        bundle.putString(str, Ej5.b(str2));
        f(str, bundle);
    }

    @DexIgnore
    public final void k(String str, String str2, String str3) {
        Wg6.c(str, Constants.EVENT);
        Wg6.c(str2, "paramName");
        Wg6.c(str3, "paramValue");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = b;
        local.d(str4, "Inside .analyticLogEvent event=" + str + ", name=" + str2 + ", value=" + str3);
        Bundle bundle = new Bundle();
        bundle.putString(str2, Ej5.b(str3));
        f(str, bundle);
    }

    @DexIgnore
    public final void l(String str, Map<String, ? extends Object> map) {
        Wg6.c(str, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "Inside .analyticLogEvent event=" + str + ", value=" + map);
        if (map != null && (!map.isEmpty())) {
            Bundle bundle = new Bundle();
            for (Map.Entry<String, ? extends Object> entry : map.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                if (value instanceof Integer) {
                    bundle.putInt(key, ((Number) value).intValue());
                } else if (value instanceof Long) {
                    bundle.putLong(key, ((Number) value).longValue());
                } else if (value instanceof Boolean) {
                    bundle.putBoolean(key, ((Boolean) value).booleanValue());
                } else if (value instanceof Float) {
                    bundle.putFloat(key, ((Number) value).floatValue());
                } else if (value != null) {
                    bundle.putString(key, Ej5.b((String) value));
                } else {
                    throw new Rc6("null cannot be cast to non-null type kotlin.String");
                }
            }
            f(str, bundle);
        }
    }

    @DexIgnore
    public final void m(String str, Activity activity) {
        FirebaseAnalytics firebaseAnalytics;
        if (!(str == null || str.length() == 0) && activity != null && (firebaseAnalytics = c) != null) {
            firebaseAnalytics.setCurrentScreen(activity, str, activity.getClass().getSimpleName() + " - " + System.currentTimeMillis());
        }
    }

    @DexIgnore
    public final void n(String str, String str2, int i) {
        Wg6.c(str, "styleNumber");
        Wg6.c(str2, "deviceName");
        l("sync_error", e(str, str2, i));
    }

    @DexIgnore
    public final void o(int i, SKUModel sKUModel) {
        if (sKUModel != null) {
            String sku = sKUModel.getSku();
            if (sku != null) {
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                l("sync_start", e(sku, deviceName, i));
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void p(String str, String str2, int i) {
        Wg6.c(str, "styleNumber");
        Wg6.c(str2, "deviceName");
        l("sync_success", e(str, str2, i));
    }

    @DexIgnore
    public final void q(String str, String str2) {
        Wg6.c(str, "propertyName");
        Wg6.c(str2, "value");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b;
        local.d(str3, "User property " + str + " with value=" + str2);
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.d(str, Ej5.b(str2));
        }
    }

    @DexIgnore
    public final void r(Map<String, String> map) {
        if (map != null && (!map.isEmpty())) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = b;
                local.d(str, "User property " + key + " with value=" + value);
                FirebaseAnalytics firebaseAnalytics = c;
                if (firebaseAnalytics != null) {
                    firebaseAnalytics.d(key, Ej5.b(value));
                }
            }
        }
    }

    @DexIgnore
    public final void s(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "setAnalyticEnable: " + z);
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.b(z);
        }
    }

    @DexIgnore
    public final void t() {
        q("Auth", "None");
    }

    @DexIgnore
    public final void u(boolean z) {
        q("Optin_Emails", z ? "Yes" : "No");
    }

    @DexIgnore
    public final void v(boolean z) {
        q("Optin_Usage", z ? "Yes" : "No");
    }

    @DexIgnore
    public final void w(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "set userId: " + str);
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.c(str);
        }
        this.a = str;
    }
}
