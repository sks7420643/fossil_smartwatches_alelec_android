package com.portfolio.platform.helper;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.receiver.AlarmReceiver;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmHelper {
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ An4 a;
    @DexIgnore
    public /* final */ UserRepository b;
    @DexIgnore
    public /* final */ AlarmsRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final long a(long j) {
            int i;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmHelper", "getEndTimeFromAlarmMinute - duration=" + j);
            Calendar instance = Calendar.getInstance();
            if (instance.get(9) == 1) {
                int i2 = instance.get(10);
                i = i2 == 12 ? 12 : i2 + 12;
            } else {
                i = instance.get(10);
            }
            long j2 = ((long) ((((i * 60) + instance.get(12)) * 60) + instance.get(13))) * 1000;
            long j3 = j2 <= j ? j - j2 : LogBuilder.MAX_INTERVAL - (j2 - j);
            long currentTimeMillis = System.currentTimeMillis() + j;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("AlarmHelper", "getEndTimeFromAlarmMinute - duration=" + j3);
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("AlarmHelper", "getEndTimeFromAlarmMinute - currentSecond=" + instance.get(13));
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("AlarmHelper", "getEndTimeFromAlarmMinute - alarmEnd=" + new Date(currentTimeMillis));
            return currentTimeMillis;
        }

        @DexIgnore
        public final int b(String str) {
            Wg6.c(str, "day");
            String lowerCase = str.toLowerCase();
            Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
            switch (lowerCase.hashCode()) {
                case 101661:
                    if (lowerCase.equals("fri")) {
                        return 6;
                    }
                    break;
                case 108300:
                    if (lowerCase.equals("mon")) {
                        return 2;
                    }
                    break;
                case 113638:
                    if (lowerCase.equals("sat")) {
                        return 7;
                    }
                    break;
                case 114252:
                    if (lowerCase.equals("sun")) {
                        return 1;
                    }
                    break;
                case 114817:
                    if (lowerCase.equals("thu")) {
                        return 5;
                    }
                    break;
                case 115204:
                    if (lowerCase.equals("tue")) {
                        return 3;
                    }
                    break;
                case 117590:
                    if (lowerCase.equals("wed")) {
                        return 4;
                    }
                    break;
            }
            return -1;
        }

        @DexIgnore
        public final boolean c(Alarm alarm) {
            Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            if (!alarm.isRepeated()) {
                if (!alarm.isActive()) {
                    return false;
                }
                Calendar instance = Calendar.getInstance();
                Wg6.b(instance, "calendar");
                instance.setTime(TimeUtils.q0(alarm.getUpdatedAt()));
                int i = instance.get(9);
                int i2 = instance.get(10);
                if (i == 1 && i2 < 12) {
                    i2 += 12;
                }
                Calendar instance2 = Calendar.getInstance();
                Wg6.b(instance2, "instanceCalendar");
                long timeInMillis = instance2.getTimeInMillis();
                TimeUtils.T(instance2);
                Wg6.b(instance2, "DateHelper.getStartOfDay(instanceCalendar)");
                long timeInMillis2 = instance2.getTimeInMillis();
                long millisecond = alarm.getMillisecond();
                if ((((long) ((((i2 * 60) + instance.get(12)) * 60) + instance.get(13))) * 1000) + 1 > millisecond) {
                    return false;
                }
                if (timeInMillis - timeInMillis2 > millisecond) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.helper.AlarmHelper$endJobScheduler$1", f = "AlarmHelper.kt", l = {209, 216}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $currentMinute;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(AlarmHelper alarmHelper, int i, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = alarmHelper;
            this.$currentMinute = i;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$currentMinute, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
            	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
            	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
            */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0080  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x009e A[EDGE_INSN: B:28:0x009e->B:24:0x009e ?: BREAK  , SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x003a  */
        @Override // com.fossil.Zn7
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
                r10 = this;
                r9 = 2
                r8 = 1
                java.lang.Object r7 = com.fossil.Yn7.d()
                int r0 = r10.label
                if (r0 == 0) goto L_0x0087
                if (r0 == r8) goto L_0x0072
                if (r0 != r9) goto L_0x006a
                java.lang.Object r0 = r10.L$3
                java.util.Iterator r0 = (java.util.Iterator) r0
                java.lang.Object r1 = r10.L$2
                com.portfolio.platform.data.source.local.alarm.Alarm r1 = (com.portfolio.platform.data.source.local.alarm.Alarm) r1
                java.lang.Object r2 = r10.L$1
                java.util.List r2 = (java.util.List) r2
                java.lang.Object r3 = r10.L$0
                com.mapped.Il6 r3 = (com.mapped.Il6) r3
                com.fossil.El7.b(r11)
                r4 = r2
                r6 = r1
                r5 = r0
            L_0x0024:
                com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get
                com.fossil.Qi5 r1 = new com.fossil.Qi5
                java.lang.String r2 = r6.getUri()
                r1.<init>(r8, r2)
                r0.g(r1)
                r2 = r3
                r1 = r4
            L_0x0034:
                boolean r0 = r5.hasNext()
                if (r0 == 0) goto L_0x009e
                java.lang.Object r0 = r5.next()
                com.portfolio.platform.data.source.local.alarm.Alarm r0 = (com.portfolio.platform.data.source.local.alarm.Alarm) r0
                int r3 = r0.getTotalMinutes()
                int r4 = r10.$currentMinute
                if (r3 != r4) goto L_0x0034
                boolean r3 = r0.isRepeated()
                if (r3 != 0) goto L_0x0034
                r3 = 0
                r0.setActive(r3)
                com.portfolio.platform.helper.AlarmHelper r3 = r10.this$0
                com.portfolio.platform.data.source.AlarmsRepository r3 = r3.c()
                r10.L$0 = r2
                r10.L$1 = r1
                r10.L$2 = r0
                r10.L$3 = r5
                r10.label = r9
                java.lang.Object r3 = r3.updateAlarm(r0, r10)
                if (r3 != r7) goto L_0x00a1
                r0 = r7
            L_0x0069:
                return r0
            L_0x006a:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0072:
                java.lang.Object r0 = r10.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r11)
                r2 = r0
                r1 = r11
            L_0x007b:
                r0 = r1
                java.util.List r0 = (java.util.List) r0
                if (r0 == 0) goto L_0x009e
                java.util.Iterator r3 = r0.iterator()
                r1 = r0
                r5 = r3
                goto L_0x0034
            L_0x0087:
                com.fossil.El7.b(r11)
                com.mapped.Il6 r0 = r10.p$
                com.portfolio.platform.helper.AlarmHelper r1 = r10.this$0
                com.portfolio.platform.data.source.AlarmsRepository r1 = r1.c()
                r10.L$0 = r0
                r10.label = r8
                java.lang.Object r1 = r1.getActiveAlarms(r10)
                if (r1 != r7) goto L_0x00a5
                r0 = r7
                goto L_0x0069
            L_0x009e:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
                goto L_0x0069
            L_0x00a1:
                r3 = r2
                r4 = r1
                r6 = r0
                goto L_0x0024
            L_0x00a5:
                r2 = r0
                goto L_0x007b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.helper.AlarmHelper.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.helper.AlarmHelper$startJobScheduler$1", f = "AlarmHelper.kt", l = {126, 184}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Context $context;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public long J$0;
        @DexIgnore
        public long J$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(AlarmHelper alarmHelper, Context context, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = alarmHelper;
            this.$context = context;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$context, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:117:0x0253  */
        /* JADX WARNING: Removed duplicated region for block: B:154:0x030b  */
        /* JADX WARNING: Removed duplicated region for block: B:164:0x0219 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:167:0x021f A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0074  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x009b  */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x00ba  */
        /* JADX WARNING: Removed duplicated region for block: B:51:0x00e4  */
        /* JADX WARNING: Removed duplicated region for block: B:97:0x0197  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r21) {
            /*
            // Method dump skipped, instructions count: 953
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.helper.AlarmHelper.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.helper.AlarmHelper", f = "AlarmHelper.kt", l = {250, 272}, m = "validateAlarms")
    public static final class Di extends Jf6 {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public long J$1;
        @DexIgnore
        public long J$2;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(AlarmHelper alarmHelper, Xe6 xe6) {
            super(xe6);
            this.this$0 = alarmHelper;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.j(this);
        }
    }

    @DexIgnore
    public AlarmHelper(An4 an4, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(alarmsRepository, "mAlarmsRepository");
        this.a = an4;
        this.b = userRepository;
        this.c = alarmsRepository;
    }

    @DexIgnore
    public final void a(Context context) {
        Wg6.c(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "cancelJobScheduler");
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.setAction("com.portfolio.platform.ALARM_RECEIVER");
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 101, intent, 134217728);
        if (!(broadcast == null || alarmManager == null)) {
            alarmManager.cancel(broadcast);
        }
        AlarmManager alarmManager2 = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        Intent intent2 = new Intent(context, AlarmReceiver.class);
        intent2.setAction("com.portfolio.platform.ALARM_RECEIVER");
        PendingIntent broadcast2 = PendingIntent.getBroadcast(context, 102, intent2, 134217728);
        if (alarmManager2 != null) {
            alarmManager2.cancel(broadcast2);
        }
    }

    @DexIgnore
    public final void b() {
        int i = Calendar.getInstance().get(12) + (Calendar.getInstance().get(11) * 60);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AlarmHelper", "endJobScheduler - currentMinute=" + i);
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Bi(this, i, null), 3, null);
    }

    @DexIgnore
    public final AlarmsRepository c() {
        return this.c;
    }

    @DexIgnore
    public final UserRepository d() {
        return this.b;
    }

    @DexIgnore
    public final void e(Context context) {
        Wg6.c(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startExactReplaceBatteryAlarm");
        Calendar instance = Calendar.getInstance();
        instance.set(11, 9);
        instance.set(12, 0);
        instance.set(13, 0);
        Wg6.b(instance, "triggerCalendar");
        long timeInMillis = instance.getTimeInMillis();
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("REQUEST_CODE", 1);
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 1, intent, 134217728);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        if (alarmManager != null) {
            if (Build.VERSION.SDK_INT >= 23) {
                alarmManager.setExactAndAllowWhileIdle(0, timeInMillis, broadcast);
            } else {
                alarmManager.setExact(0, timeInMillis, broadcast);
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmHelper", "startExactReplaceBatteryAlarm - triggerAtMillis=" + timeInMillis);
        }
    }

    @DexIgnore
    public final void f(Context context) {
        Wg6.c(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startHWLogScheduler");
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        if (alarmManager != null) {
            Intent intent = new Intent(context, AlarmReceiver.class);
            intent.putExtra("REQUEST_CODE", 10);
            PendingIntent broadcast = PendingIntent.getBroadcast(context, 10, intent, 134217728);
            alarmManager.cancel(broadcast);
            Calendar instance = Calendar.getInstance();
            instance.set(11, 10);
            instance.set(12, 0);
            instance.set(13, 0);
            Wg6.b(instance, "triggerCalendar");
            long timeInMillis = instance.getTimeInMillis();
            alarmManager.setInexactRepeating(0, timeInMillis, LogBuilder.MAX_INTERVAL, broadcast);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmHelper", "startHWLogScheduler - setInexactRepeating: triggerAtMillis=" + timeInMillis + ", interval=" + LogBuilder.MAX_INTERVAL);
        }
    }

    @DexIgnore
    public final void g(Context context) {
        Wg6.c(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startJobScheduler");
        a(context);
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Ci(this, context, null), 3, null);
    }

    @DexIgnore
    public final void h(Context context) {
        Wg6.c(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "startRemindSyncAlarm");
        Calendar instance = Calendar.getInstance();
        if (Calendar.getInstance().get(11) >= 15) {
            instance.add(6, 1);
        }
        instance.set(11, 15);
        instance.set(12, 0);
        instance.set(13, 0);
        Bundle bundle = new Bundle();
        bundle.putInt("DEF_ALARM_RECEIVER_ACTION", 2);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtras(bundle);
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 20, intent, 134217728);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        if (alarmManager != null) {
            try {
                alarmManager.cancel(broadcast);
                Wg6.b(instance, "calendar");
                alarmManager.setRepeating(0, instance.getTimeInMillis(), LogBuilder.MAX_INTERVAL, broadcast);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("AlarmHelper", "schedule SyncAlarm at = " + new Date(instance.getTimeInMillis()));
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("AlarmHelper", "startSyncAlarm() - Exception: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public final void i(Context context) {
        Wg6.c(context, "context");
        FLogger.INSTANCE.getLocal().d("AlarmHelper", "stopReplaceBatteryAlarm");
        this.a.E1("");
        Intent intent = new Intent(context, AlarmReceiver.class);
        Intent intent2 = new Intent(context, AlarmReceiver.class);
        intent.putExtra("REQUEST_CODE", 0);
        intent2.putExtra("REQUEST_CODE", 1);
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, intent, 134217728);
        PendingIntent broadcast2 = PendingIntent.getBroadcast(context, 1, intent2, 134217728);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        if (alarmManager != null) {
            alarmManager.cancel(broadcast);
            alarmManager.cancel(broadcast2);
            FLogger.INSTANCE.getLocal().d("AlarmHelper", "stopReplaceBatteryAlarm success");
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x01a9  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01d1  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01d5  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0248  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01a3 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object j(com.mapped.Xe6<? super com.mapped.Cd6> r27) {
        /*
        // Method dump skipped, instructions count: 593
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.helper.AlarmHelper.j(com.mapped.Xe6):java.lang.Object");
    }
}
