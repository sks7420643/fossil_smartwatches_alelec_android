package com.portfolio.platform.helper;

import com.google.gson.JsonElement;
import com.mapped.Cd6;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConvertDateTimeToLong implements Hu3<Long> {
    @DexIgnore
    public Long a(JsonElement jsonElement, Type type, Gu3 gu3) {
        long j = 0;
        Wg6.c(jsonElement, "json");
        String f = jsonElement.f();
        Wg6.b(f, "dateAsString");
        if (f.length() == 0) {
            return 0L;
        }
        try {
            DateTime S = TimeUtils.S(f);
            Wg6.b(S, "DateHelper.getServerDate\u2026ffsetParsed(dateAsString)");
            j = S.getMillis();
        } catch (Exception e) {
            try {
                DateTime M = TimeUtils.M(jsonElement.f());
                Wg6.b(M, "DateHelper.getLocalDateT\u2026DateFormat(json.asString)");
                j = M.getMillis();
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("deserialize - json=");
                sb.append(jsonElement.f());
                sb.append(", e=");
                e2.printStackTrace();
                sb.append(Cd6.a);
                local.e("GsonConvertDateTimeToLong", sb.toString());
            }
        }
        return Long.valueOf(j);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hu3
    public /* bridge */ /* synthetic */ Long deserialize(JsonElement jsonElement, Type type, Gu3 gu3) {
        return a(jsonElement, type, gu3);
    }
}
