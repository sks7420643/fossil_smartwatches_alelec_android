package com.portfolio.platform.helper;

import com.google.gson.JsonElement;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConvertDate implements Hu3<Date> {
    @DexIgnore
    public Date a(JsonElement jsonElement, Type type, Gu3 gu3) {
        Wg6.c(jsonElement, "json");
        Wg6.c(type, "typeOfT");
        Wg6.c(gu3, "context");
        String f = jsonElement.f();
        Wg6.b(f, "dateAsString");
        if (f.length() == 0) {
            return new Date(0);
        }
        try {
            Date date = TimeUtils.R(DateTimeZone.getDefault(), f).toDate();
            Wg6.b(date, "DateHelper.getServerDate\u2026), dateAsString).toDate()");
            return date;
        } catch (Exception e) {
            try {
                SimpleDateFormat simpleDateFormat = TimeUtils.a.get();
                if (simpleDateFormat != null) {
                    Date parse = simpleDateFormat.parse(jsonElement.f());
                    if (parse != null) {
                        return parse;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("GsonConvertDate", "deserialize - json=" + jsonElement.f() + ", e=" + e2);
                e2.printStackTrace();
                return new Date(0);
            }
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hu3
    public /* bridge */ /* synthetic */ Date deserialize(JsonElement jsonElement, Type type, Gu3 gu3) {
        return a(jsonElement, type, gu3);
    }
}
