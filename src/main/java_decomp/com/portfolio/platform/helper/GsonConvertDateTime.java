package com.portfolio.platform.helper;

import com.fossil.Jj4;
import com.fossil.Kj4;
import com.google.gson.JsonElement;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.Pu3;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConvertDateTime implements Hu3<DateTime>, Pu3<DateTime> {
    @DexIgnore
    public DateTime a(JsonElement jsonElement, Type type, Gu3 gu3) {
        Wg6.c(jsonElement, "json");
        Wg6.c(type, "typeOfT");
        Wg6.c(gu3, "context");
        String f = jsonElement.f();
        Wg6.b(f, "dateAsString");
        if (f.length() == 0) {
            return new DateTime(0);
        }
        try {
            DateTime R = TimeUtils.R(DateTimeZone.getDefault(), f);
            Wg6.b(R, "DateHelper.getServerDate\u2026tDefault(), dateAsString)");
            return R;
        } catch (Exception e) {
            try {
                DateTime M = TimeUtils.M(jsonElement.f());
                Wg6.b(M, "DateHelper.getLocalDateT\u2026DateFormat(json.asString)");
                return M;
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("GsonConvertDateTime", "deserialize - json=" + jsonElement.f() + ", e=" + e2);
                e2.printStackTrace();
                return new DateTime(0);
            }
        }
    }

    @DexIgnore
    public JsonElement b(DateTime dateTime, Type type, Kj4 kj4) {
        String s0;
        Wg6.c(type, "typeOfSrc");
        Wg6.c(kj4, "context");
        if (dateTime == null) {
            s0 = "";
        } else {
            s0 = TimeUtils.s0(DateTimeZone.UTC, dateTime);
            Wg6.b(s0, "DateHelper.printServerDa\u2026at(DateTimeZone.UTC, src)");
        }
        return new Jj4(s0);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hu3
    public /* bridge */ /* synthetic */ DateTime deserialize(JsonElement jsonElement, Type type, Gu3 gu3) {
        return a(jsonElement, type, gu3);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.reflect.Type, com.fossil.Kj4] */
    @Override // com.mapped.Pu3
    public /* bridge */ /* synthetic */ JsonElement serialize(DateTime dateTime, Type type, Kj4 kj4) {
        return b(dateTime, type, kj4);
    }
}
