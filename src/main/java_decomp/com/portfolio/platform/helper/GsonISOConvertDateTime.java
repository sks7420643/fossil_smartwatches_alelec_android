package com.portfolio.platform.helper;

import com.fossil.Jj4;
import com.fossil.Kj4;
import com.google.gson.JsonElement;
import com.mapped.Cd6;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.Pu3;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonISOConvertDateTime implements Hu3<Date>, Pu3<Date> {
    @DexIgnore
    public Date a(JsonElement jsonElement, Type type, Gu3 gu3) {
        String f;
        if (!(jsonElement == null || (f = jsonElement.f()) == null)) {
            try {
                SimpleDateFormat simpleDateFormat = TimeUtils.p.get();
                if (simpleDateFormat != null) {
                    Date parse = simpleDateFormat.parse(f);
                    SimpleDateFormat simpleDateFormat2 = TimeUtils.n.get();
                    if (simpleDateFormat2 != null) {
                        SimpleDateFormat simpleDateFormat3 = simpleDateFormat2;
                        SimpleDateFormat simpleDateFormat4 = TimeUtils.n.get();
                        if (simpleDateFormat4 != null) {
                            Date parse2 = simpleDateFormat3.parse(simpleDateFormat4.format(parse));
                            Wg6.b(parse2, "DateHelper.LOCAL_DATE_SP\u2026MAT.get()!!.format(date))");
                            return parse2;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            } catch (ParseException e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("deserialize - json=");
                sb.append(jsonElement.f());
                sb.append(", e=");
                e.printStackTrace();
                sb.append(Cd6.a);
                local.e("GsonISOConvertDateTime", sb.toString());
            }
        }
        return new Date(0);
    }

    @DexIgnore
    public JsonElement b(Date date, Type type, Kj4 kj4) {
        String format;
        if (date == null) {
            format = "";
        } else {
            SimpleDateFormat simpleDateFormat = TimeUtils.p.get();
            if (simpleDateFormat != null) {
                format = simpleDateFormat.format(date);
                Wg6.b(format, "DateHelper.SERVER_DATE_I\u2026ORMAT.get()!!.format(src)");
            } else {
                Wg6.i();
                throw null;
            }
        }
        return new Jj4(format);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hu3
    public /* bridge */ /* synthetic */ Date deserialize(JsonElement jsonElement, Type type, Gu3 gu3) {
        return a(jsonElement, type, gu3);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.reflect.Type, com.fossil.Kj4] */
    @Override // com.mapped.Pu3
    public /* bridge */ /* synthetic */ JsonElement serialize(Date date, Type type, Kj4 kj4) {
        return b(date, type, kj4);
    }
}
