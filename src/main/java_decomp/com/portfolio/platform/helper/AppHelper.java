package com.portfolio.platform.helper;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.model.PlaceFields;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Ko7;
import com.fossil.Q27;
import com.fossil.Vt7;
import com.fossil.Wk5;
import com.fossil.Wt7;
import com.fossil.Yn7;
import com.google.gson.reflect.TypeToken;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.SecondTimezoneRaw;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppHelper {
    @DexIgnore
    public static AppHelper e;
    @DexIgnore
    public static /* final */ ArrayList<Ringtone> f; // = new ArrayList<>();
    @DexIgnore
    public static /* final */ Ai g; // = new Ai(null);
    @DexIgnore
    public An4 a;
    @DexIgnore
    public DeviceRepository b;
    @DexIgnore
    public UserRepository c;
    @DexIgnore
    public Q27 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends TypeToken<List<? extends SecondTimezoneRaw>> {
        }

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @SuppressLint({"HardwareIds"})
        public final String a(String str) {
            Wg6.c(str, ButtonService.USER_ID);
            if (TextUtils.isEmpty(str)) {
                String str2 = Build.SERIAL;
                Wg6.b(str2, "Build.SERIAL");
                return str2;
            }
            return Build.SERIAL + ":" + str;
        }

        @DexIgnore
        public final ArrayList<Bi> b() {
            PortfolioApp instance = PortfolioApp.get.instance();
            ArrayList<Bi> arrayList = new ArrayList<>();
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            List<ResolveInfo> queryIntentActivities = instance.getPackageManager().queryIntentActivities(intent, 0);
            PackageManager packageManager = instance.getPackageManager();
            for (ResolveInfo resolveInfo : queryIntentActivities) {
                if (!Vt7.j(resolveInfo.loadLabel(packageManager).toString(), "Contacts", true) && !Vt7.j(resolveInfo.loadLabel(packageManager).toString(), "Phone", true)) {
                    Bi bi = new Bi(resolveInfo.loadLabel(instance.getPackageManager()).toString());
                    String str = resolveInfo.activityInfo.packageName;
                    Wg6.b(str, "item.activityInfo.packageName");
                    bi.d(str);
                    try {
                        ApplicationInfo applicationInfo = packageManager.getApplicationInfo(bi.b(), 0);
                        Wg6.b(applicationInfo, "packageManager.getApplic\u2026ionInfo(newInfo.pname, 0)");
                        if (applicationInfo.icon != 0) {
                            bi.e(Uri.parse("android.resource://" + bi.b() + "/" + applicationInfo.icon));
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    Iterator<Bi> it = arrayList.iterator();
                    boolean z = false;
                    while (it.hasNext()) {
                        if (Wg6.a(it.next().b(), bi.b())) {
                            z = true;
                        }
                    }
                    if (!z) {
                        arrayList.add(bi);
                    }
                }
            }
            return arrayList;
        }

        @DexIgnore
        public final AppHelper c() {
            AppHelper f;
            synchronized (this) {
                if (AppHelper.g.f() == null) {
                    AppHelper.g.l(new AppHelper(null));
                }
                f = AppHelper.g.f();
                if (f == null) {
                    Wg6.i();
                    throw null;
                }
            }
            return f;
        }

        @DexIgnore
        public final ArrayList<Ringtone> d() {
            return AppHelper.f;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0077, code lost:
            if (r0 != null) goto L_0x0079;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0079, code lost:
            r0.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0081, code lost:
            if (r0 != null) goto L_0x0079;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
            return d();
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0088  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.util.List<com.portfolio.platform.data.model.Ringtone> e() {
            /*
                r6 = this;
                r2 = 0
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r1 = "AppHelper"
                java.lang.String r3 = "getListRingtoneFromRingtonePhone"
                r0.d(r1, r3)
                java.util.ArrayList r0 = r6.d()
                boolean r0 = r0.isEmpty()
                r0 = r0 ^ 1
                if (r0 == 0) goto L_0x001f
                java.util.ArrayList r0 = r6.d()
            L_0x001e:
                return r0
            L_0x001f:
                android.media.RingtoneManager r0 = new android.media.RingtoneManager     // Catch:{ Exception -> 0x008c, all -> 0x0084 }
                com.portfolio.platform.PortfolioApp$inner r1 = com.portfolio.platform.PortfolioApp.get     // Catch:{ Exception -> 0x008c, all -> 0x0084 }
                com.portfolio.platform.PortfolioApp r1 = r1.instance()     // Catch:{ Exception -> 0x008c, all -> 0x0084 }
                android.content.Context r1 = r1.getApplicationContext()     // Catch:{ Exception -> 0x008c, all -> 0x0084 }
                r0.<init>(r1)     // Catch:{ Exception -> 0x008c, all -> 0x0084 }
                r1 = 1
                r0.setType(r1)     // Catch:{ Exception -> 0x008c, all -> 0x0084 }
                android.database.Cursor r0 = r0.getCursor()     // Catch:{ Exception -> 0x008c, all -> 0x0084 }
                if (r0 == 0) goto L_0x0081
            L_0x0038:
                boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x005a }
                if (r1 == 0) goto L_0x0081
                r1 = 1
                java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x005a }
                r2 = 0
                java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x005a }
                java.util.ArrayList r3 = r6.d()     // Catch:{ Exception -> 0x005a }
                com.portfolio.platform.data.model.Ringtone r4 = new com.portfolio.platform.data.model.Ringtone     // Catch:{ Exception -> 0x005a }
                java.lang.String r5 = "title"
                com.mapped.Wg6.b(r1, r5)     // Catch:{ Exception -> 0x005a }
                r4.<init>(r1, r2)     // Catch:{ Exception -> 0x005a }
                r3.add(r4)     // Catch:{ Exception -> 0x005a }
                goto L_0x0038
            L_0x005a:
                r1 = move-exception
            L_0x005b:
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x008f }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ all -> 0x008f }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x008f }
                r3.<init>()     // Catch:{ all -> 0x008f }
                java.lang.String r4 = "getListRingtoneFromRingtonePhone - Exception="
                r3.append(r4)     // Catch:{ all -> 0x008f }
                r3.append(r1)     // Catch:{ all -> 0x008f }
                java.lang.String r1 = "AppHelper"
                java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x008f }
                r2.e(r1, r3)     // Catch:{ all -> 0x008f }
                if (r0 == 0) goto L_0x007c
            L_0x0079:
                r0.close()
            L_0x007c:
                java.util.ArrayList r0 = r6.d()
                goto L_0x001e
            L_0x0081:
                if (r0 == 0) goto L_0x007c
                goto L_0x0079
            L_0x0084:
                r0 = move-exception
                r1 = r0
            L_0x0086:
                if (r2 == 0) goto L_0x008b
                r2.close()
            L_0x008b:
                throw r1
            L_0x008c:
                r1 = move-exception
                r0 = r2
                goto L_0x005b
            L_0x008f:
                r1 = move-exception
                r2 = r0
                goto L_0x0086
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.helper.AppHelper.Ai.e():java.util.List");
        }

        @DexIgnore
        public final AppHelper f() {
            return AppHelper.e;
        }

        @DexIgnore
        public final ArrayList<Bi> g() {
            return b();
        }

        @DexIgnore
        public final SecondTimezoneSetting h(String str, int i) {
            Wg6.c(str, PlaceFields.LOCATION);
            ArrayList<SecondTimezoneSetting> i2 = i();
            ArrayList<SecondTimezoneSetting> arrayList = new ArrayList();
            for (T t : i2) {
                if (Wg6.a(t.getCityCode(), str)) {
                    arrayList.add(t);
                }
            }
            for (SecondTimezoneSetting secondTimezoneSetting : arrayList) {
                if (ConversionUtils.INSTANCE.getTimezoneRawOffsetById(secondTimezoneSetting.getTimeZoneId()) == i) {
                    return secondTimezoneSetting;
                }
            }
            return null;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0084, code lost:
            if (r1 != null) goto L_0x0086;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0081  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0099  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x009e  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.util.ArrayList<com.portfolio.platform.data.model.setting.SecondTimezoneSetting> i() {
            /*
                r10 = this;
                r0 = 0
                java.util.ArrayList r4 = new java.util.ArrayList
                r4.<init>()
                com.portfolio.platform.PortfolioApp$inner r1 = com.portfolio.platform.PortfolioApp.get     // Catch:{ Exception -> 0x00ad, all -> 0x00a8 }
                com.portfolio.platform.PortfolioApp r1 = r1.instance()     // Catch:{ Exception -> 0x00ad, all -> 0x00a8 }
                android.content.res.Resources r1 = r1.getResources()     // Catch:{ Exception -> 0x00ad, all -> 0x00a8 }
                r2 = 2131820551(0x7f110007, float:1.927382E38)
                java.io.InputStream r3 = r1.openRawResource(r2)     // Catch:{ Exception -> 0x00ad, all -> 0x00a8 }
                java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00a5, all -> 0x00a2 }
                java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00a5, all -> 0x00a2 }
                r2.<init>(r3)     // Catch:{ Exception -> 0x00a5, all -> 0x00a2 }
                r1.<init>(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x00a2 }
                com.google.gson.stream.JsonReader r0 = new com.google.gson.stream.JsonReader     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                r0.<init>(r1)     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                com.google.gson.Gson r2 = new com.google.gson.Gson     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                r2.<init>()     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                com.portfolio.platform.helper.AppHelper$Ai$Aii r5 = new com.portfolio.platform.helper.AppHelper$Ai$Aii     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                r5.<init>()     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                java.lang.reflect.Type r5 = r5.getType()     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                java.lang.Object r0 = r2.i(r0, r5)     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                java.util.List r0 = (java.util.List) r0     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                java.lang.String r2 = "rawSecondTimezoneList"
                com.mapped.Wg6.b(r0, r2)     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                r5 = 10
                int r5 = com.fossil.Im7.m(r0, r5)     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                r2.<init>(r5)     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                java.util.Iterator r5 = r0.iterator()     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
            L_0x004e:
                boolean r0 = r5.hasNext()     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                if (r0 == 0) goto L_0x008a
                java.lang.Object r0 = r5.next()     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                com.portfolio.platform.data.model.SecondTimezoneRaw r0 = (com.portfolio.platform.data.model.SecondTimezoneRaw) r0     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                com.portfolio.platform.data.model.setting.SecondTimezoneSetting r6 = new com.portfolio.platform.data.model.setting.SecondTimezoneSetting     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                java.lang.String r7 = r0.getCityName()     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                java.lang.String r8 = r0.getTimeZoneId()     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                r9 = 0
                java.lang.String r0 = r0.getCityCode()     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                r6.<init>(r7, r8, r9, r0)     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                r2.add(r6)     // Catch:{ Exception -> 0x0070, all -> 0x0093 }
                goto L_0x004e
            L_0x0070:
                r0 = move-exception
            L_0x0071:
                r2 = r3
            L_0x0072:
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x00b1 }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ all -> 0x00b1 }
                java.lang.String r3 = "AppHelper"
                java.lang.String r5 = "exception when close stream."
                r0.d(r3, r5)     // Catch:{ all -> 0x00b1 }
                if (r2 == 0) goto L_0x0084
                r2.close()
            L_0x0084:
                if (r1 == 0) goto L_0x0089
            L_0x0086:
                r1.close()
            L_0x0089:
                return r4
            L_0x008a:
                r4.addAll(r2)
                if (r3 == 0) goto L_0x0086
                r3.close()
                goto L_0x0086
            L_0x0093:
                r2 = move-exception
                r0 = r1
            L_0x0095:
                r4 = r2
                r5 = r0
            L_0x0097:
                if (r3 == 0) goto L_0x009c
                r3.close()
            L_0x009c:
                if (r5 == 0) goto L_0x00a1
                r5.close()
            L_0x00a1:
                throw r4
            L_0x00a2:
                r1 = move-exception
                r2 = r1
                goto L_0x0095
            L_0x00a5:
                r1 = move-exception
                r1 = r0
                goto L_0x0071
            L_0x00a8:
                r1 = move-exception
                r4 = r1
                r5 = r0
                r3 = r0
                goto L_0x0097
            L_0x00ad:
                r1 = move-exception
                r1 = r0
                r2 = r0
                goto L_0x0072
            L_0x00b1:
                r0 = move-exception
                r4 = r0
                r5 = r1
                r3 = r2
                goto L_0x0097
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.helper.AppHelper.Ai.i():java.util.ArrayList");
        }

        @DexIgnore
        public final boolean j(Context context, String str) {
            Wg6.c(context, "context");
            Wg6.c(str, "packageName");
            try {
                context.getPackageManager().getPackageInfo(str, 0);
                return true;
            } catch (PackageManager.NameNotFoundException e) {
                return false;
            }
        }

        @DexIgnore
        public final void k(Context context, String str) {
            Wg6.c(context, "context");
            Wg6.c(str, "packageName");
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + str));
            try {
                intent.setFlags(SQLiteDatabase.CREATE_IF_NECESSARY);
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("AppHelper", "openMarketApp - ex=" + e);
            }
        }

        @DexIgnore
        public final void l(AppHelper appHelper) {
            AppHelper.e = appHelper;
        }

        @DexIgnore
        public final String m() {
            try {
                String sb = new StringBuilder("4.6.0").delete(Wt7.F("4.6.0", '-', 0, false, 6, null), 5).toString();
                Wg6.b(sb, "stringBuilder.delete(pos\u2026N_NAME.length).toString()");
                return sb;
            } catch (Exception e) {
                return "4.6.0";
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public String a; // = "";
        @DexIgnore
        public Uri b;
        @DexIgnore
        public String c;

        @DexIgnore
        public Bi(String str) {
            Wg6.c(str, "appname");
            this.c = str;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }

        @DexIgnore
        public final Uri c() {
            return this.b;
        }

        @DexIgnore
        public final void d(String str) {
            Wg6.c(str, "<set-?>");
            this.a = str;
        }

        @DexIgnore
        public final void e(Uri uri) {
            this.b = uri;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || ((obj instanceof Bi) && Wg6.a(this.c, ((Bi) obj).c));
        }

        @DexIgnore
        public int hashCode() {
            String str = this.c;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "PInfo(appname=" + this.c + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.helper.AppHelper$getAppLogInfo$2", f = "AppHelper.kt", l = {68}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super AppLogInfo>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AppHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(AppHelper appHelper, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = appHelper;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super AppLogInfo> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object currentUser;
            String str;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                UserRepository h = this.this$0.h();
                this.L$0 = il6;
                this.label = 1;
                currentUser = h.getCurrentUser(this);
                if (currentUser == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                currentUser = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) currentUser;
            if (mFUser == null || (str = mFUser.getUserId()) == null) {
                str = "";
            }
            String m = Wg6.a("release", "release") ? "4.6.0" : AppHelper.g.m();
            String str2 = Wk5.c.a(PortfolioApp.get.instance()) + ':' + Build.MODEL;
            String str3 = Build.MODEL;
            String str4 = Build.VERSION.RELEASE;
            Wg6.b(str4, "Build.VERSION.RELEASE");
            String sDKVersion = ButtonService.Companion.getSDKVersion();
            Wg6.b(str3, "phoneModel");
            return new AppLogInfo(str, m, "android", str4, str2, sDKVersion, str3);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.helper.AppHelper", f = "AppHelper.kt", l = {96, 100}, m = "getBuildInfo")
    public static final class Di extends Jf6 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ AppHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(AppHelper appHelper, Xe6 xe6) {
            super(xe6);
            this.this$0 = appHelper;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g(null, 0, 0, this);
        }
    }

    @DexIgnore
    public AppHelper() {
        PortfolioApp.get.instance().getIface().K1(this);
    }

    @DexIgnore
    public /* synthetic */ AppHelper(Qg6 qg6) {
        this();
    }

    @DexIgnore
    public final ActiveDeviceInfo d() {
        String str;
        String sku;
        String J = PortfolioApp.get.instance().J();
        DeviceRepository deviceRepository = this.b;
        if (deviceRepository != null) {
            Device deviceBySerial = deviceRepository.getDeviceBySerial(J);
            String str2 = (deviceBySerial == null || (sku = deviceBySerial.getSku()) == null) ? "" : sku;
            if (deviceBySerial == null || (str = deviceBySerial.getFirmwareRevision()) == null) {
                str = "";
            }
            return new ActiveDeviceInfo(J, str2, str);
        }
        Wg6.n("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final Object e(Xe6<? super AppLogInfo> xe6) {
        return Eu7.g(Bw7.b(), new Ci(this, null), xe6);
    }

    @DexIgnore
    public final String f() {
        return Wg6.a("release", "release") ? "4.6.0" : g.m();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:105:0x084f  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x016d A[LOOP:0: B:32:0x0167->B:34:0x016d, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x01be  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0207  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0249  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x024f  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0270  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x02a3  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x02d2  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x046f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x048b  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x069b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g(android.content.Context r17, int r18, int r19, com.mapped.Xe6<? super java.lang.String> r20) {
        /*
        // Method dump skipped, instructions count: 2141
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.helper.AppHelper.g(android.content.Context, int, int, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final UserRepository h() {
        UserRepository userRepository = this.c;
        if (userRepository != null) {
            return userRepository;
        }
        Wg6.n("mUserRepository");
        throw null;
    }
}
