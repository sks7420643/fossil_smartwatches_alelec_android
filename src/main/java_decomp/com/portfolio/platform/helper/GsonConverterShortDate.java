package com.portfolio.platform.helper;

import com.fossil.Jj4;
import com.fossil.Kj4;
import com.google.gson.JsonElement;
import com.mapped.Cd6;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.Pu3;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConverterShortDate implements Hu3<Date>, Pu3<Date> {
    @DexIgnore
    public Date a(JsonElement jsonElement, Type type, Gu3 gu3) {
        Wg6.c(jsonElement, "json");
        Wg6.c(type, "typeOfT");
        Wg6.c(gu3, "context");
        String f = jsonElement.f();
        if (f != null) {
            try {
                SimpleDateFormat simpleDateFormat = TimeUtils.a.get();
                if (simpleDateFormat != null) {
                    Date parse = simpleDateFormat.parse(f);
                    if (parse != null) {
                        return parse;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            } catch (ParseException e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("deserialize - json=");
                sb.append(f);
                sb.append(", e=");
                e.printStackTrace();
                sb.append(Cd6.a);
                local.e("GsonConverterShortDate", sb.toString());
            }
        }
        return new Date(0);
    }

    @DexIgnore
    public JsonElement b(Date date, Type type, Kj4 kj4) {
        String format;
        Wg6.c(type, "typeOfSrc");
        Wg6.c(kj4, "context");
        if (date == null) {
            format = "";
        } else {
            SimpleDateFormat simpleDateFormat = TimeUtils.a.get();
            if (simpleDateFormat != null) {
                format = simpleDateFormat.format(date);
                Wg6.b(format, "DateHelper.SHORT_DATE_FO\u2026ATTER.get()!!.format(src)");
            } else {
                Wg6.i();
                throw null;
            }
        }
        return new Jj4(format);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hu3
    public /* bridge */ /* synthetic */ Date deserialize(JsonElement jsonElement, Type type, Gu3 gu3) {
        return a(jsonElement, type, gu3);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.reflect.Type, com.fossil.Kj4] */
    @Override // com.mapped.Pu3
    public /* bridge */ /* synthetic */ JsonElement serialize(Date date, Type type, Kj4 kj4) {
        return b(date, type, kj4);
    }
}
