package com.portfolio.platform.helper;

import com.fossil.Hj4;
import com.google.gson.JsonElement;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.TimeUtils;
import java.lang.reflect.Type;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class GsonConverterShortDateTime implements Hu3<DateTime> {
    @DexIgnore
    public DateTime a(JsonElement jsonElement, Type type, Gu3 gu3) throws Hj4 {
        String f = jsonElement.f();
        if (f.isEmpty()) {
            return null;
        }
        return TimeUtils.M(f);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hu3
    public /* bridge */ /* synthetic */ DateTime deserialize(JsonElement jsonElement, Type type, Gu3 gu3) throws Hj4 {
        return a(jsonElement, type, gu3);
    }
}
