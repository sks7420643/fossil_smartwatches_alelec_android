package com.portfolio.platform.helper;

import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Bw7;
import com.fossil.Eu7;
import com.fossil.Ko7;
import com.fossil.Rh5;
import com.fossil.Rk5;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.mapped.An4;
import com.mapped.Bv6;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Ph4;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityIntensities;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessHelper {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static /* final */ Ai c; // = new Ai(null);
    @DexIgnore
    public /* final */ An4 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final int a(long j) {
            if (j < ((long) 70)) {
                return 0;
            }
            return j < ((long) ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL) ? 1 : 2;
        }

        @DexIgnore
        public final String b(String str, MFSleepSession mFSleepSession) {
            Wg6.c(str, ButtonService.USER_ID);
            Wg6.c(mFSleepSession, "sleepSession");
            String value = Ph4.values()[mFSleepSession.getSource()].getValue();
            Wg6.b(value, "FitnessSourceType.values\u2026leepSession.source].value");
            if (value != null) {
                String lowerCase = value.toLowerCase();
                Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                return str + ":" + lowerCase + ":" + mFSleepSession.getRealEndTime();
            }
            throw new Rc6("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final int c(GoalTrackingSummary goalTrackingSummary) {
            if (goalTrackingSummary != null) {
                return goalTrackingSummary.getGoalTarget();
            }
            return 8;
        }

        @DexIgnore
        public final int d(ActivitySummary activitySummary, Rh5 rh5) {
            Wg6.c(rh5, "goalType");
            int i = Rk5.a[rh5.ordinal()];
            if (i == 1) {
                return activitySummary != null ? activitySummary.getStepGoal() : VideoUploader.RETRY_DELAY_UNIT_MS;
            }
            if (i != 2) {
                return i != 3 ? VideoUploader.RETRY_DELAY_UNIT_MS : activitySummary != null ? activitySummary.getCaloriesGoal() : ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL;
            }
            if (activitySummary != null) {
                return activitySummary.getActiveTimeGoal();
            }
            return 30;
        }

        @DexIgnore
        public final int e(MFSleepDay mFSleepDay) {
            if (mFSleepDay != null) {
                return mFSleepDay.getGoalMinutes();
            }
            return 480;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.helper.FitnessHelper$getCurrentSteps$2", f = "FitnessHelper.kt", l = {69, 80}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Float>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $includeRealTimeStep;
        @DexIgnore
        public float F$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FitnessHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(FitnessHelper fitnessHelper, Date date, boolean z, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = fitnessHelper;
            this.$date = date;
            this.$includeRealTimeStep = z;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$date, this.$includeRealTimeStep, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Float> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0097 A[SYNTHETIC, Splitter:B:19:0x0097] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00f1  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
            // Method dump skipped, instructions count: 263
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.helper.FitnessHelper.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = FitnessHelper.class.getSimpleName();
        Wg6.b(simpleName, "FitnessHelper::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public FitnessHelper(An4 an4) {
        Wg6.c(an4, "mSharedPreferencesManager");
        this.a = an4;
    }

    @DexIgnore
    public final List<ActivitySample> b(List<ActivitySample> list, String str) {
        Wg6.c(list, "activitySamples");
        Wg6.c(str, ButtonService.USER_ID);
        long d = d(new Date());
        long j = 0;
        long j2 = 0;
        for (ActivitySample activitySample : list) {
            DateTime component4 = activitySample.component4();
            j += (long) activitySample.component5();
            j2 = component4.getMillis();
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "addRealTimeStepToActivitySample - steps=" + j + ", realTimeSteps=" + d);
        if (j < d) {
            try {
                ActivityIntensities activityIntensities = new ActivityIntensities();
                double d2 = (double) (d - j);
                activityIntensities.setIntensity(d2);
                long j3 = j2 + 1;
                Date date = new Date(j3);
                DateTime dateTime = new DateTime(j3);
                DateTime dateTime2 = new DateTime(j2 + ((long) 100));
                int c0 = TimeUtils.c0();
                String value = Ph4.Device.getValue();
                Wg6.b(value, "FitnessSourceType.Device.value");
                list.add(new ActivitySample(str, date, dateTime, dateTime2, d2, 0.0d, 0.0d, 0, activityIntensities, c0, value, j3, j3, j3));
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = b;
                local2.e(str3, "addRealTimeStepToActivitySample - e=" + e);
                e.printStackTrace();
            }
        }
        return list;
    }

    @DexIgnore
    public final Object c(Date date, boolean z, Xe6<? super Float> xe6) {
        return Eu7.g(Bw7.b(), new Bi(this, date, z, null), xe6);
    }

    @DexIgnore
    public final long d(Date date) {
        long j;
        String[] g;
        Wg6.c(date, "date");
        String L = this.a.L();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "getRealTimeStep - data=" + L + ", date=" + TimeUtils.z(date));
        if (!TextUtils.isEmpty(L) && (g = Bv6.g(L, LocaleConverter.LOCALE_DELIMITER)) != null && g.length == 2) {
            try {
                if (TimeUtils.m0(date, new Date(Long.parseLong(g[0])))) {
                    j = Long.parseLong(g[1]);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = b;
                    local2.d(str2, "XXX- getRealTimeStep - date=" + TimeUtils.z(date) + " steps " + j);
                    return j;
                }
            } catch (Exception e) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = b;
                local3.e(str3, "getRealTimeStep - e=" + e);
                e.printStackTrace();
            }
        }
        j = 0;
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        String str22 = b;
        local22.d(str22, "XXX- getRealTimeStep - date=" + TimeUtils.z(date) + " steps " + j);
        return j;
    }

    @DexIgnore
    public final void e(Date date, long j) {
        Wg6.c(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "saveRealTimeStep - date=" + date + ", steps=" + j);
        if (j >= 0) {
            String L = this.a.L();
            if (!TextUtils.isEmpty(L)) {
                String[] g = Bv6.g(L, LocaleConverter.LOCALE_DELIMITER);
                if (g != null && g.length == 2) {
                    try {
                        long parseLong = Long.parseLong(g[0]);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = b;
                        local2.d(str2, "saveRealTimeStep - lastSampleRawTimeStamp=" + date + ", savedRealTimeStepTimeStamp=" + new Date(parseLong));
                        if (TimeUtils.m0(new Date(parseLong), date)) {
                            An4 an4 = this.a;
                            an4.L1(String.valueOf(date.getTime()) + LocaleConverter.LOCALE_DELIMITER + j);
                            return;
                        }
                        FLogger.INSTANCE.getLocal().d(b, "saveRealTimeStep - Different date, clear realTimeStepStamp");
                        An4 an42 = this.a;
                        an42.L1(String.valueOf(date.getTime()) + LocaleConverter.LOCALE_DELIMITER + j);
                    } catch (Exception e) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str3 = b;
                        local3.e(str3, "saveRealTimeStep - e=" + e);
                        e.printStackTrace();
                    }
                }
            } else {
                An4 an43 = this.a;
                an43.L1(String.valueOf(date.getTime()) + LocaleConverter.LOCALE_DELIMITER + j);
            }
        }
    }
}
