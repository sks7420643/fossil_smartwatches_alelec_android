package com.portfolio.platform.viewmodel;

import androidx.lifecycle.MutableLiveData;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Ts0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.DebugFirmwareData;
import com.portfolio.platform.data.model.Firmware;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FirmwareDebugViewModel extends Ts0 {
    @DexIgnore
    public /* final */ MutableLiveData<List<DebugFirmwareData>> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Firmware> b; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.viewmodel.FirmwareDebugViewModel$loadFirmware$1", f = "FirmwareDebugViewModel.kt", l = {16}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Hg6 $block;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FirmwareDebugViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(FirmwareDebugViewModel firmwareDebugViewModel, Hg6 hg6, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = firmwareDebugViewModel;
            this.$block = hg6;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, this.$block, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object invoke;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Hg6 hg6 = this.$block;
                this.L$0 = il6;
                this.label = 1;
                invoke = hg6.invoke(this);
                if (invoke == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                invoke = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.a().l((List) invoke);
            return Cd6.a;
        }
    }

    @DexIgnore
    public final MutableLiveData<List<DebugFirmwareData>> a() {
        return this.a;
    }

    @DexIgnore
    public final MutableLiveData<Firmware> b() {
        return this.b;
    }

    @DexIgnore
    public final boolean c() {
        if (this.a.e() != null) {
            List<DebugFirmwareData> e = this.a.e();
            if (e != null) {
                Wg6.b(e, "firmwares.value!!");
                if (!e.isEmpty()) {
                    return true;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
        return false;
    }

    @DexIgnore
    public final Rm6 d(Hg6<? super Xe6<? super List<DebugFirmwareData>>, ? extends Object> hg6) {
        Wg6.c(hg6, "block");
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Ai(this, hg6, null), 3, null);
    }

    @DexIgnore
    public final void e() {
        MutableLiveData<List<DebugFirmwareData>> mutableLiveData = this.a;
        mutableLiveData.l(mutableLiveData.e());
        MutableLiveData<Firmware> mutableLiveData2 = this.b;
        mutableLiveData2.l(mutableLiveData2.e());
    }

    @DexIgnore
    public final void f(Firmware firmware) {
        Wg6.c(firmware, "firmware");
        this.b.l(firmware);
    }
}
