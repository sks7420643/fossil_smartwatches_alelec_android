package com.portfolio.platform.service.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gl7;
import com.fossil.Hm7;
import com.fossil.Hr7;
import com.fossil.Ko7;
import com.fossil.Tk5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Jh6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.SyncDataExtensions;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.service.syncmodel.WrapperTapEventSummary;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridSyncDataProcessing {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ HybridSyncDataProcessing b; // = new HybridSyncDataProcessing();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ List<WrapperTapEventSummary> b;
        @DexIgnore
        public /* final */ List<ActivitySample> c;
        @DexIgnore
        public /* final */ List<GFitSample> d;
        @DexIgnore
        public /* final */ List<ActivitySummary> e;
        @DexIgnore
        public /* final */ List<MFSleepSession> f;

        @DexIgnore
        public Ai(long j, List<WrapperTapEventSummary> list, List<ActivitySample> list2, List<GFitSample> list3, List<ActivitySummary> list4, List<MFSleepSession> list5) {
            Wg6.c(list, "tapEventSummaryList");
            Wg6.c(list2, "sampleRawList");
            Wg6.c(list3, "gFitSampleList");
            Wg6.c(list4, "summaryList");
            Wg6.c(list5, "sleepSessionList");
            this.a = j;
            this.b = list;
            this.c = list2;
            this.d = list3;
            this.e = list4;
            this.f = list5;
        }

        @DexIgnore
        public final List<GFitSample> a() {
            return this.d;
        }

        @DexIgnore
        public final long b() {
            return this.a;
        }

        @DexIgnore
        public final List<ActivitySample> c() {
            return this.c;
        }

        @DexIgnore
        public final List<MFSleepSession> d() {
            return this.f;
        }

        @DexIgnore
        public final List<ActivitySummary> e() {
            return this.e;
        }

        @DexIgnore
        public final List<WrapperTapEventSummary> f() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing", f = "HybridSyncDataProcessing.kt", l = {308}, m = "processGoalTrackingData")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ HybridSyncDataProcessing this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(HybridSyncDataProcessing hybridSyncDataProcessing, Xe6 xe6) {
            super(xe6);
            this.this$0 = hybridSyncDataProcessing;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(null, null, null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing", f = "HybridSyncDataProcessing.kt", l = {122, 133, 148, 156, 166, 176, 183}, m = "saveSyncResult")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$13;
        @DexIgnore
        public Object L$14;
        @DexIgnore
        public Object L$15;
        @DexIgnore
        public Object L$16;
        @DexIgnore
        public Object L$17;
        @DexIgnore
        public Object L$18;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ HybridSyncDataProcessing this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(HybridSyncDataProcessing hybridSyncDataProcessing, Xe6 xe6) {
            super(xe6);
            this.this$0 = hybridSyncDataProcessing;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g(null, null, null, null, null, null, null, null, null, null, null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2", f = "HybridSyncDataProcessing.kt", l = {137}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Rm6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ai $finalResult;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository $thirdPartyRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2$1", f = "HybridSyncDataProcessing.kt", l = {138, 139}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Rm6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $listGFitSample;
            @DexIgnore
            public /* final */ /* synthetic */ List $listSleepSession;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, List list, List list2, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
                this.$listGFitSample = list;
                this.$listSleepSession = list2;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$listGFitSample, this.$listSleepSession, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Rm6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Il6 il6;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il62 = this.p$;
                    ThirdPartyRepository thirdPartyRepository = this.this$0.$thirdPartyRepository;
                    List<GFitSample> list = this.$listGFitSample;
                    List<GFitHeartRate> e = Hm7.e();
                    List<GFitWorkoutSession> e2 = Hm7.e();
                    List<MFSleepSession> list2 = this.$listSleepSession;
                    this.L$0 = il62;
                    this.label = 1;
                    if (thirdPartyRepository.saveData(list, null, e, e2, list2, this) == d) {
                        return d;
                    }
                    il6 = il62;
                } else if (i == 1) {
                    il6 = (Il6) this.L$0;
                    El7.b(obj);
                } else if (i == 2) {
                    Il6 il63 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ThirdPartyRepository thirdPartyRepository2 = this.this$0.$thirdPartyRepository;
                this.L$0 = il6;
                this.label = 2;
                Object uploadData$default = ThirdPartyRepository.uploadData$default(thirdPartyRepository2, null, this, 1, null);
                return uploadData$default == d ? d : uploadData$default;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(Ai ai, ThirdPartyRepository thirdPartyRepository, Xe6 xe6) {
            super(2, xe6);
            this.$finalResult = ai;
            this.$thirdPartyRepository = thirdPartyRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.$finalResult, this.$thirdPartyRepository, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Rm6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                List<GFitSample> a2 = this.$finalResult.a();
                List<MFSleepSession> d2 = this.$finalResult.d();
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, a2, d2, null);
                this.L$0 = il6;
                this.L$1 = a2;
                this.L$2 = d2;
                this.label = 1;
                Object g = Eu7.g(b, aii, this);
                return g == d ? d : g;
            } else if (i == 1) {
                List list = (List) this.L$2;
                List list2 = (List) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3", f = "HybridSyncDataProcessing.kt", l = {192}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super ActivityStatistic>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivitiesRepository $activityRepository;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSessionsRepository $sleepSessionsRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository $sleepSummariesRepository;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $startDate;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository $summaryRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3$1", f = "HybridSyncDataProcessing.kt", l = {193, 194, 196, 197, 198}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super ActivityStatistic>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super ActivityStatistic> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:18:0x0076  */
            /* JADX WARNING: Removed duplicated region for block: B:22:0x00b3  */
            /* JADX WARNING: Removed duplicated region for block: B:26:0x00ec  */
            /* JADX WARNING: Removed duplicated region for block: B:30:0x012a  */
            /* JADX WARNING: Removed duplicated region for block: B:31:0x012d  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r14) {
                /*
                // Method dump skipped, instructions count: 306
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.usecase.HybridSyncDataProcessing.Ei.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(List list, Jh6 jh6, Jh6 jh62, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, SleepSessionsRepository sleepSessionsRepository, SleepSummariesRepository sleepSummariesRepository, Xe6 xe6) {
            super(2, xe6);
            this.$fitnessDataList = list;
            this.$startDate = jh6;
            this.$endDate = jh62;
            this.$activityRepository = activitiesRepository;
            this.$summaryRepository = summariesRepository;
            this.$sleepSessionsRepository = sleepSessionsRepository;
            this.$sleepSummariesRepository = sleepSummariesRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.$fitnessDataList, this.$startDate, this.$endDate, this.$activityRepository, this.$summaryRepository, this.$sleepSessionsRepository, this.$sleepSummariesRepository, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super ActivityStatistic> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                for (FitnessDataWrapper fitnessDataWrapper : this.$fitnessDataList) {
                    if (fitnessDataWrapper.getStartLongTime() < this.$startDate.element.getMillis()) {
                        this.$startDate.element = (T) fitnessDataWrapper.getStartTimeTZ();
                    }
                    if (fitnessDataWrapper.getEndLongTime() > this.$endDate.element.getMillis()) {
                        this.$endDate.element = (T) fitnessDataWrapper.getEndTimeTZ();
                    }
                }
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                Object g = Eu7.g(b, aii, this);
                return g == d ? d : g;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    /*
    static {
        String simpleName = HybridSyncDataProcessing.class.getSimpleName();
        Wg6.b(simpleName, "HybridSyncDataProcessing::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final String a(List<ActivitySample> list) {
        if (list.isEmpty()) {
            return "empty\n";
        }
        StringBuilder sb = new StringBuilder();
        for (ActivitySample activitySample : list) {
            sb.append("[");
            sb.append("startTime:");
            sb.append(activitySample.getStartTime());
            sb.append(", step:");
            sb.append(activitySample.getSteps());
            sb.append("]");
        }
        String sb2 = sb.toString();
        Wg6.b(sb2, "buffer.toString()");
        return sb2;
    }

    @DexIgnore
    public final String b(List<MFSleepSession> list) {
        if (list.isEmpty()) {
            return "empty\n";
        }
        StringBuilder sb = new StringBuilder();
        for (MFSleepSession mFSleepSession : list) {
            sb.append(mFSleepSession.toString());
            sb.append("\n");
            sb.append("State: ");
            String sleepStates = mFSleepSession.getSleepStates();
            int length = sleepStates.length();
            for (int i = 0; i < length; i++) {
                sb.append(String.valueOf(sleepStates.charAt(i)));
                sb.append(" -- ");
            }
            sb.append("\n");
        }
        sb.append("\n");
        String sb2 = sb.toString();
        Wg6.b(sb2, "buffer.toString()");
        return sb2;
    }

    @DexIgnore
    public final Ai c(String str, List<FitnessDataWrapper> list, MFUser mFUser, UserProfile userProfile, long j, long j2, PortfolioApp portfolioApp) {
        Wg6.c(str, "serial");
        Wg6.c(list, "syncData");
        Wg6.c(mFUser, "user");
        Wg6.c(userProfile, "userProfile");
        Wg6.c(portfolioApp, "portfolioApp");
        FLogger.INSTANCE.getLocal().d(a, ".buildSyncResult(), get all data files, synctime=" + j + ", data=" + list);
        Ai ai = new Ai(j2, new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList());
        if (!list.isEmpty()) {
            portfolioApp.p(CommunicateMode.SYNC, str, "Calculating sleep and activity...");
            Gl7<List<ActivitySample>, List<ActivitySummary>, List<GFitSample>> d = SyncDataExtensions.d(list, str, mFUser.getUserId(), 1000 * j);
            List<ActivitySample> first = d.getFirst();
            List<ActivitySummary> second = d.getSecond();
            List<GFitSample> third = d.getThird();
            List<MFSleepSession> e = SyncDataExtensions.e(list, str);
            List<WrapperTapEventSummary> c = SyncDataExtensions.c(list);
            int size = e.size();
            double d2 = 0.0d;
            double d3 = 0.0d;
            double d4 = 0.0d;
            double d5 = 0.0d;
            for (T t : first) {
                d4 += t.getCalories();
                d3 += t.getDistance();
                d2 += t.getSteps();
                t.getActiveTime();
                Boolean p0 = TimeUtils.p0(t.getDate());
                Wg6.b(p0, "DateHelper.isToday(it.date)");
                if (p0.booleanValue()) {
                    d5 += t.getSteps();
                    t.getActiveTime();
                }
            }
            Hr7 hr7 = Hr7.a;
            String format = String.format("Done calculating sleep: total %s sleep session(s)", Arrays.copyOf(new Object[]{String.valueOf(size)}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            f(str, format);
            Hr7 hr72 = Hr7.a;
            String format2 = String.format("After calculation: steps=%s, todayStep=%s, realTimeSteps=%s, lastRealtimeSteps=%s. Calories=%s. DistanceWrapper=%s. Taps=%s", Arrays.copyOf(new Object[]{Double.valueOf(d2), Double.valueOf(d5), Long.valueOf(j2), Long.valueOf(userProfile.getCurrentSteps()), Double.valueOf(d4), Double.valueOf(d3), d(c)}, 7));
            Wg6.b(format2, "java.lang.String.format(format, *args)");
            f(str, format2);
            FLogger.INSTANCE.getLocal().d(a, "Release=" + Tk5.d());
            if (!Tk5.d()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = a;
                StringBuilder sb = new StringBuilder();
                sb.append("onSyncCompleted - Sleep session details: ");
                sb.append(e.isEmpty() ? b(e) : ", no sleep data");
                local.d(str2, sb.toString());
                FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Minute data details: " + a(first));
                FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Tap event details: " + d(c));
            }
            return new Ai(j2, c, first, third, second, e);
        }
        f(str, "Sync data is empty");
        return ai;
    }

    @DexIgnore
    public final String d(List<? extends WrapperTapEventSummary> list) {
        if (list == null || list.isEmpty()) {
            return "null\n";
        }
        StringBuilder sb = new StringBuilder();
        for (WrapperTapEventSummary wrapperTapEventSummary : list) {
            sb.append("[startTime:");
            sb.append(wrapperTapEventSummary.startTime);
            sb.append(", timezoneOffsetInSecond=");
            sb.append(wrapperTapEventSummary.timezoneOffsetInSecond);
            sb.append(", goalTrackingIds=");
            sb.append(wrapperTapEventSummary.goalId);
            sb.append("]");
            sb.append("\n");
        }
        String sb2 = sb.toString();
        Wg6.b(sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object e(java.util.List<? extends com.portfolio.platform.service.syncmodel.WrapperTapEventSummary> r19, java.lang.String r20, com.portfolio.platform.data.source.HybridPresetRepository r21, com.portfolio.platform.data.source.GoalTrackingRepository r22, com.portfolio.platform.data.source.UserRepository r23, com.mapped.Xe6<? super com.mapped.Cd6> r24) {
        /*
        // Method dump skipped, instructions count: 367
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.usecase.HybridSyncDataProcessing.e(java.util.List, java.lang.String, com.portfolio.platform.data.source.HybridPresetRepository, com.portfolio.platform.data.source.GoalTrackingRepository, com.portfolio.platform.data.source.UserRepository, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void f(String str, String str2) {
        PortfolioApp.get.instance().p(CommunicateMode.SYNC, str, str2);
        FLogger.INSTANCE.getLocal().d(a, str2);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str, a, str2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:103:0x04e6  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x04ea  */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0597  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x05c0  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x06eb  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x070f  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x072c  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:192:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:195:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:196:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x019f  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0245  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x028b  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0301 A[Catch:{ Exception -> 0x0742 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0379  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0024  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x041c  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0446  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g(com.portfolio.platform.service.usecase.HybridSyncDataProcessing.Ai r37, java.lang.String r38, com.portfolio.platform.data.source.SleepSessionsRepository r39, com.portfolio.platform.data.source.SummariesRepository r40, com.portfolio.platform.data.source.SleepSummariesRepository r41, com.portfolio.platform.data.source.FitnessDataRepository r42, com.portfolio.platform.data.source.ActivitiesRepository r43, com.portfolio.platform.data.source.HybridPresetRepository r44, com.portfolio.platform.data.source.GoalTrackingRepository r45, com.portfolio.platform.data.source.ThirdPartyRepository r46, com.portfolio.platform.data.source.UserRepository r47, com.portfolio.platform.PortfolioApp r48, com.portfolio.platform.helper.FitnessHelper r49, com.mapped.Xe6<? super com.mapped.Cd6> r50) {
        /*
        // Method dump skipped, instructions count: 1910
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.usecase.HybridSyncDataProcessing.g(com.portfolio.platform.service.usecase.HybridSyncDataProcessing$Ai, java.lang.String, com.portfolio.platform.data.source.SleepSessionsRepository, com.portfolio.platform.data.source.SummariesRepository, com.portfolio.platform.data.source.SleepSummariesRepository, com.portfolio.platform.data.source.FitnessDataRepository, com.portfolio.platform.data.source.ActivitiesRepository, com.portfolio.platform.data.source.HybridPresetRepository, com.portfolio.platform.data.source.GoalTrackingRepository, com.portfolio.platform.data.source.ThirdPartyRepository, com.portfolio.platform.data.source.UserRepository, com.portfolio.platform.PortfolioApp, com.portfolio.platform.helper.FitnessHelper, com.mapped.Xe6):java.lang.Object");
    }
}
