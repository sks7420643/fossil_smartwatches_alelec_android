package com.portfolio.platform.service.microapp;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import com.facebook.applinks.AppLinkData;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Ga3;
import com.fossil.Gu7;
import com.fossil.Ha3;
import com.fossil.Ht3;
import com.fossil.Ia3;
import com.fossil.Ja3;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Kr5;
import com.fossil.Mr5;
import com.fossil.N62;
import com.fossil.Na3;
import com.fossil.Nt3;
import com.fossil.Pu5;
import com.fossil.Yn7;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.gson.Gson;
import com.google.maps.model.TravelMode;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc3;
import com.mapped.Lf6;
import com.mapped.Mc3;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.mapped.Wv2;
import com.mapped.Xe6;
import com.mapped.Yv2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.Mapping;
import com.misfit.frameworks.buttonservice.model.microapp.CommuteTimeETAMicroAppResponse;
import com.misfit.frameworks.buttonservice.model.microapp.CommuteTimeTravelMicroAppResponse;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeService extends Kr5 implements Ga3, LocationListener {
    @DexIgnore
    public static /* final */ int A; // = A;
    @DexIgnore
    public static /* final */ float B; // = 50.0f;
    @DexIgnore
    public static /* final */ a C; // = new a(null);
    @DexIgnore
    public static /* final */ String y;
    @DexIgnore
    public static /* final */ int z; // = 120000;
    @DexIgnore
    public Location e;
    @DexIgnore
    public Location f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public String h;
    @DexIgnore
    public long i;
    @DexIgnore
    public CommuteTimeSetting j;
    @DexIgnore
    public LocationManager k;
    @DexIgnore
    public String l;
    @DexIgnore
    public Handler m;
    @DexIgnore
    public Wv2 s;
    @DexIgnore
    public Na3 t;
    @DexIgnore
    public LocationRequest u;
    @DexIgnore
    public Ia3 v;
    @DexIgnore
    public Yv2 w;
    @DexIgnore
    public Pu5 x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return CommuteTimeService.y;
        }

        @DexIgnore
        public final void b(Context context, Bundle bundle, Mr5 mr5) {
            Wg6.c(context, "context");
            Wg6.c(bundle, Mapping.COLUMN_EXTRA_INFO);
            Wg6.c(mr5, "listener");
            Intent intent = new Intent(context, CommuteTimeService.class);
            intent.putExtras(bundle);
            context.startService(intent);
            Kr5.e(mr5);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Yv2 {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        @Override // com.mapped.Yv2
        public void onLocationResult(LocationResult locationResult) {
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "onLocationResult");
            super.onLocationResult(locationResult);
            CommuteTimeService commuteTimeService = this.a;
            if (locationResult != null) {
                commuteTimeService.D(locationResult.c());
                if (this.a.w() != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = CommuteTimeService.C.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("onLocationResult lastLocation=");
                    Location w = this.a.w();
                    if (w != null) {
                        sb.append(w);
                        local.d(a2, sb.toString());
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "onLocationResult lastLocation is null");
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.microapp.CommuteTimeService$getDurationTime$1", f = "CommuteTimeService.kt", l = {}, m = "invokeSuspend")
    public static final class c extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Location $location;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(CommuteTimeService commuteTimeService, Location location, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = commuteTimeService;
            this.$location = location;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            c cVar = new c(this.this$0, this.$location, xe6);
            cVar.p$ = (Il6) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((c) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                if (this.this$0.j != null) {
                    CommuteTimeSetting commuteTimeSetting = this.this$0.j;
                    if (commuteTimeSetting != null) {
                        if (!(commuteTimeSetting.getAddress().length() == 0)) {
                            Pu5 v = this.this$0.v();
                            if (v != null) {
                                CommuteTimeSetting commuteTimeSetting2 = this.this$0.j;
                                if (commuteTimeSetting2 != null) {
                                    String address = commuteTimeSetting2.getAddress();
                                    TravelMode travelMode = TravelMode.DRIVING;
                                    CommuteTimeSetting commuteTimeSetting3 = this.this$0.j;
                                    if (commuteTimeSetting3 != null) {
                                        long a2 = v.a(address, travelMode, commuteTimeSetting3.getAvoidTolls(), this.$location.getLatitude(), this.$location.getLongitude());
                                        if (a2 != -1) {
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String a3 = CommuteTimeService.C.a();
                                            local.d(a3, "getDurationTime duration " + a2);
                                            this.this$0.i = a2;
                                            if (this.this$0.i < ((long) 60)) {
                                                this.this$0.i = 60;
                                            }
                                            this.this$0.A();
                                        } else {
                                            this.this$0.a();
                                        }
                                    } else {
                                        Wg6.i();
                                        throw null;
                                    }
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<TResult> implements Ht3<Location> {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            @Override // com.fossil.Ht3
            public final void onComplete(Nt3<Location> nt3) {
                Wg6.c(nt3, "task");
                if (!nt3.q() || nt3.m() == null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = CommuteTimeService.C.a();
                    local.d(a2, "getLastLocation:exception" + nt3.l());
                    this.a.b.a();
                    return;
                }
                CommuteTimeService commuteTimeService = this.a.b;
                Location w = commuteTimeService.w();
                if (w != null) {
                    commuteTimeService.D(commuteTimeService.y(w, nt3.m()));
                    long currentTimeMillis = System.currentTimeMillis();
                    Location w2 = this.a.b.w();
                    if (w2 == null) {
                        Wg6.i();
                        throw null;
                    } else if (currentTimeMillis - w2.getTime() > ((long) CommuteTimeService.A)) {
                        FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "Runnable after 5s over 5 mins not trust");
                        this.a.b.a();
                    } else {
                        CommuteTimeService commuteTimeService2 = this.a.b;
                        Location w3 = commuteTimeService2.w();
                        if (w3 != null) {
                            commuteTimeService2.u(w3);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        public d(CommuteTimeService commuteTimeService) {
            this.b = commuteTimeService;
        }

        @DexIgnore
        public final void run() {
            Nt3<Location> s;
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "Runnable after 5s");
            if (this.b.e == null && this.b.k != null) {
                LocationManager locationManager = this.b.k;
                if (locationManager != null) {
                    locationManager.removeUpdates(this.b);
                    if (W6.a(PortfolioApp.get.instance(), "android.permission.ACCESS_FINE_LOCATION") == 0 && W6.a(PortfolioApp.get.instance(), "android.permission.ACCESS_COARSE_LOCATION") == 0 && LocationUtils.isLocationEnable(PortfolioApp.get.instance())) {
                        CommuteTimeService commuteTimeService = this.b;
                        LocationManager locationManager2 = commuteTimeService.k;
                        if (locationManager2 != null) {
                            commuteTimeService.D(locationManager2.getLastKnownLocation(this.b.l));
                            if (this.b.w() != null) {
                                Wv2 wv2 = this.b.s;
                                if (wv2 != null && (s = wv2.s()) != null) {
                                    s.b(new a(this));
                                    return;
                                }
                                return;
                            }
                            this.b.a();
                            return;
                        }
                        Wg6.i();
                        throw null;
                    }
                    FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "Runnable after 5s permission not granted");
                    this.b.a();
                    return;
                }
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<TResult> implements Mc3<Ja3> {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        public e(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        public final void a(Ja3 ja3) {
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "All location settings are satisfied");
            if (W6.a(this.a, "android.permission.ACCESS_FINE_LOCATION") == 0 || W6.a(this.a, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
                Wv2 wv2 = this.a.s;
                if (wv2 != null) {
                    wv2.u(this.a.u, this.a.w, Looper.myLooper());
                    return;
                }
                return;
            }
            this.a.a();
            this.a.E(false);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Mc3
        public /* bridge */ /* synthetic */ void onSuccess(Ja3 ja3) {
            a(ja3);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Lc3 {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        public f(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        @Override // com.mapped.Lc3
        public final void onFailure(Exception exc) {
            Wg6.c(exc, "exception");
            int statusCode = ((N62) exc).getStatusCode();
            if (statusCode == 6) {
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "Location settings are not satisfied. Attempting to upgrade location settings ");
                this.a.a();
            } else if (statusCode == 8502) {
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "Location settings are inadequate, and cannot be fixed here. Fix in Settings.");
                this.a.E(false);
                this.a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<TResult> implements Ht3<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        public g(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        @Override // com.fossil.Ht3
        public final void onComplete(Nt3<Void> nt3) {
            Wg6.c(nt3, "it");
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.C.a(), "stopLocationUpdates success");
            this.a.E(false);
        }
    }

    /*
    static {
        String simpleName = CommuteTimeService.class.getSimpleName();
        Wg6.b(simpleName, "CommuteTimeService::class.java.simpleName");
        y = simpleName;
    }
    */

    @DexIgnore
    public final void A() {
        FLogger.INSTANCE.getLocal().d(y, "playHands");
        CommuteTimeSetting commuteTimeSetting = this.j;
        if (commuteTimeSetting == null) {
            Wg6.i();
            throw null;
        } else if (Wg6.a(commuteTimeSetting.getFormat(), "travel")) {
            C();
        } else {
            B();
        }
    }

    @DexIgnore
    public final void B() {
        FLogger.INSTANCE.getLocal().d(y, "playHandsETA");
        long currentTimeMillis = System.currentTimeMillis();
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "calendar");
        instance.setTimeInMillis(currentTimeMillis + (this.i * ((long) 1000)));
        int i2 = instance.get(11) % 12;
        int i3 = instance.get(12);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.d(str, "playHandsETA - duration=" + this.i + ", hour=" + i2 + ", minute=" + i3);
        try {
            PortfolioApp instance2 = PortfolioApp.get.instance();
            String str2 = this.h;
            if (str2 != null) {
                instance2.i1(str2, new CommuteTimeETAMicroAppResponse(i2, i3));
                a();
                return;
            }
            Wg6.i();
            throw null;
        } catch (IllegalArgumentException e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = y;
            local2.d(str3, "playHandsETA exception exception=" + e2.getMessage());
        }
    }

    @DexIgnore
    public final void C() {
        FLogger.INSTANCE.getLocal().d(y, "playHandsMinute");
        int round = Math.round(((float) this.i) / 60.0f);
        try {
            PortfolioApp instance = PortfolioApp.get.instance();
            String str = this.h;
            if (str != null) {
                instance.i1(str, new CommuteTimeTravelMicroAppResponse(round));
                a();
                return;
            }
            Wg6.i();
            throw null;
        } catch (IllegalArgumentException e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = y;
            local.d(str2, "playHandsMinute exception exception=" + e2.getMessage());
        }
    }

    @DexIgnore
    public final void D(Location location) {
        this.f = location;
    }

    @DexIgnore
    public final void E(boolean z2) {
        this.g = z2;
    }

    @DexIgnore
    public final void F() {
        FLogger.INSTANCE.getLocal().d(y, "startLocationUpdates");
        if (W6.a(PortfolioApp.get.instance(), "android.permission.ACCESS_FINE_LOCATION") == 0 && W6.a(PortfolioApp.get.instance(), "android.permission.ACCESS_COARSE_LOCATION") == 0 && LocationUtils.isLocationEnable(PortfolioApp.get.instance())) {
            this.g = true;
            Na3 na3 = this.t;
            if (na3 != null) {
                Nt3<Ja3> s2 = na3.s(this.v);
                s2.f(new e(this));
                s2.d(new f(this));
                return;
            }
            Wg6.i();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(y, "startLocationUpdates permission not granted");
        a();
        this.g = false;
    }

    @DexIgnore
    public final void G() {
        Nt3<Void> t2;
        if (!this.g) {
            FLogger.INSTANCE.getLocal().d(y, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }
        Wv2 wv2 = this.s;
        if (wv2 != null && (t2 = wv2.t(this.w)) != null) {
            t2.b(new g(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.Kr5
    public void a() {
        FLogger.INSTANCE.getLocal().d(y, VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        super.a();
        stopSelf();
    }

    @DexIgnore
    @Override // com.fossil.Kr5
    public void b() {
        FLogger.INSTANCE.getLocal().d(y, "forceStop");
        a();
    }

    @DexIgnore
    @Override // com.fossil.Kr5
    public IBinder onBind(Intent intent) {
        Wg6.c(intent, "intent");
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        FLogger.INSTANCE.getLocal().d(y, "onCreate");
        PortfolioApp.get.instance().getIface().t1(this);
        this.m = new Handler();
        this.h = PortfolioApp.get.instance().J();
        this.s = Ha3.a(this);
        this.t = Ha3.b(this);
        s();
        t();
        r();
        x();
        F();
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(y, "onDestroy");
        super.onDestroy();
        G();
        Handler handler = this.m;
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            LocationManager locationManager = this.k;
            if (locationManager == null) {
                return;
            }
            if (locationManager != null) {
                locationManager.removeUpdates(this);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ga3
    public void onLocationChanged(Location location) {
        FLogger.INSTANCE.getLocal().d(y, "onLocationChanged");
        if (!(this.j == null || location == null || this.e != null)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = y;
            local.d(str, "onLocationChanged location=lat: " + location.getLatitude() + " long: " + location.getLongitude());
            this.e = location;
            if (location != null) {
                u(location);
            } else {
                Wg6.i();
                throw null;
            }
        }
        LocationManager locationManager = this.k;
        if (locationManager == null) {
            return;
        }
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public void onProviderDisabled(String str) {
        Wg6.c(str, "provider");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = y;
        local.d(str2, "onProviderDisabled provider=" + str);
    }

    @DexIgnore
    public void onProviderEnabled(String str) {
        Wg6.c(str, "provider");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = y;
        local.d(str2, "onProviderEnabled provider=" + str);
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        Bundle extras;
        Wg6.c(intent, "intent");
        FLogger.INSTANCE.getLocal().d(y, "onStartCommand");
        this.b = Action.MicroAppAction.SHOW_COMMUTE;
        super.d();
        if (this.j != null || (extras = intent.getExtras()) == null) {
            return 2;
        }
        String string = extras.getString(Constants.EXTRA_INFO);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        StringBuilder sb = new StringBuilder();
        sb.append("onStartCommand json=");
        if (string != null) {
            sb.append(string);
            local.d(str, sb.toString());
            this.j = (CommuteTimeSetting) new Gson().k(string, CommuteTimeSetting.class);
            Handler handler = this.m;
            if (handler != null) {
                handler.postDelayed(new d(this), 5000);
                return 2;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public void onStatusChanged(String str, int i2, Bundle bundle) {
        Wg6.c(str, "provider");
        Wg6.c(bundle, AppLinkData.ARGUMENTS_EXTRAS_KEY);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = y;
        local.d(str2, "onStatusChanged status=" + i2);
    }

    @DexIgnore
    public final void r() {
        Ia3.Ai ai = new Ia3.Ai();
        LocationRequest locationRequest = this.u;
        if (locationRequest != null) {
            if (locationRequest != null) {
                ai.a(locationRequest);
            } else {
                Wg6.i();
                throw null;
            }
        }
        this.v = ai.b();
    }

    @DexIgnore
    public final void s() {
        this.w = new b(this);
    }

    @DexIgnore
    public final void t() {
        FLogger.INSTANCE.getLocal().d(y, "createLocationRequest");
        LocationRequest locationRequest = new LocationRequest();
        this.u = locationRequest;
        if (locationRequest != null) {
            locationRequest.A(1000);
            LocationRequest locationRequest2 = this.u;
            if (locationRequest2 != null) {
                locationRequest2.k(1000);
                LocationRequest locationRequest3 = this.u;
                if (locationRequest3 != null) {
                    locationRequest3.F(100);
                    LocationRequest locationRequest4 = this.u;
                    if (locationRequest4 != null) {
                        locationRequest4.L(B);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void u(Location location) {
        Wg6.c(location, PlaceFields.LOCATION);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.d(str, "getDurationTime location long=" + location.getLongitude() + " lat=" + location.getLatitude());
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new c(this, location, null), 3, null);
    }

    @DexIgnore
    public final Pu5 v() {
        Pu5 pu5 = this.x;
        if (pu5 != null) {
            return pu5;
        }
        Wg6.n("mDurationUtils");
        throw null;
    }

    @DexIgnore
    public final Location w() {
        return this.f;
    }

    @DexIgnore
    public final void x() {
        boolean z2;
        FLogger.INSTANCE.getLocal().d(y, "initLocationManager");
        if (W6.a(PortfolioApp.get.instance(), "android.permission.ACCESS_FINE_LOCATION") == 0 || W6.a(PortfolioApp.get.instance(), "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            Criteria criteria = new Criteria();
            criteria.setPowerRequirement(1);
            Object systemService = getSystemService(PlaceFields.LOCATION);
            if (systemService != null) {
                LocationManager locationManager = (LocationManager) systemService;
                this.k = locationManager;
                if (locationManager == null) {
                    z2 = false;
                } else if (locationManager != null) {
                    z2 = locationManager.isProviderEnabled("network");
                } else {
                    Wg6.i();
                    throw null;
                }
                if (z2) {
                    criteria.setAccuracy(2);
                } else {
                    criteria.setAccuracy(1);
                }
                LocationManager locationManager2 = this.k;
                if (locationManager2 != null) {
                    String bestProvider = locationManager2.getBestProvider(criteria, true);
                    this.l = bestProvider;
                    if (this.k != null && bestProvider != null) {
                        FLogger.INSTANCE.getLocal().d(y, "mLocationManager");
                        LocationManager locationManager3 = this.k;
                        if (locationManager3 != null) {
                            locationManager3.requestLocationUpdates(this.l, 0, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type android.location.LocationManager");
            }
        } else {
            a();
        }
    }

    @DexIgnore
    public final Location y(Location location, Location location2) {
        boolean z2 = true;
        Wg6.c(location, PlaceFields.LOCATION);
        FLogger.INSTANCE.getLocal().d(y, "isBetterLocation location long=" + location.getLongitude() + " lat=" + location.getLatitude() + " time=" + location.getTime());
        if (location2 == null) {
            FLogger.INSTANCE.getLocal().d(y, "isBetterLocation currentBestLocation null");
            return location;
        }
        FLogger.INSTANCE.getLocal().d(y, "isBetterLocation currentBestLocation long=" + location2.getLongitude() + " lat=" + location2.getLatitude() + " time=" + location2.getTime());
        long time = location.getTime() - location2.getTime();
        boolean z3 = time > ((long) z);
        boolean z4 = time < ((long) (-z));
        boolean z5 = time > 0;
        if (z3) {
            FLogger.INSTANCE.getLocal().d(y, "isBetterLocation isSignificantlyNewer");
            return location;
        } else if (z4) {
            FLogger.INSTANCE.getLocal().d(y, "isBetterLocation isSignificantlyOlder");
            return location2;
        } else {
            FLogger.INSTANCE.getLocal().d(y, "isBetterLocation accuracy location=" + location.getAccuracy() + " currentBestLocation=" + location2.getAccuracy());
            int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
            boolean z6 = accuracy > 0;
            boolean z7 = accuracy < 0;
            if (accuracy <= 200) {
                z2 = false;
            }
            boolean z8 = z(location.getProvider(), location2.getProvider());
            if (z7) {
                FLogger.INSTANCE.getLocal().d(y, "isBetterLocation isMoreAccurate");
                return location;
            } else if (z5 && !z6) {
                FLogger.INSTANCE.getLocal().d(y, "isBetterLocation isNewer && isLessAccurate=false");
                return location;
            } else if (!z5 || z2 || !z8) {
                return location2;
            } else {
                FLogger.INSTANCE.getLocal().d(y, "isBetterLocation isNewer && isSignificantlyLessAccurate=false && isFromSameProvider");
                return location;
            }
        }
    }

    @DexIgnore
    public final boolean z(String str, String str2) {
        FLogger.INSTANCE.getLocal().d(y, "isSameProvider");
        return str == null ? str2 == null : Wg6.a(str, str2);
    }
}
