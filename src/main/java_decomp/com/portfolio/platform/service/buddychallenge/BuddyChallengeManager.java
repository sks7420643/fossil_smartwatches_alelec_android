package com.portfolio.platform.service.buddychallenge;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Hz4;
import com.fossil.Im7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Ks4;
import com.fossil.Kz4;
import com.fossil.Lt4;
import com.fossil.Ms4;
import com.fossil.Pm7;
import com.fossil.Ps4;
import com.fossil.Ss4;
import com.fossil.Tt4;
import com.fossil.U08;
import com.fossil.Um5;
import com.fossil.W08;
import com.fossil.Ws1;
import com.fossil.Ws4;
import com.fossil.Xs1;
import com.fossil.Xs4;
import com.fossil.Yn7;
import com.fossil.Ys1;
import com.fossil.Zs1;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.EncryptedData;
import com.misfit.frameworks.buttonservice.model.watchapp.response.buddychallenge.BCChallengeInfoWatchAppInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.buddychallenge.BCListChallengeWatchAppInfo;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BuddyChallengeManager {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ Ai h; // = new Ai(null);
    @DexIgnore
    public Il6 a;
    @DexIgnore
    public U08 b; // = W08.b(false, 1, null);
    @DexIgnore
    public /* final */ PortfolioApp c;
    @DexIgnore
    public /* final */ Tt4 d;
    @DexIgnore
    public /* final */ FriendRepository e;
    @DexIgnore
    public /* final */ An4 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return BuddyChallengeManager.g;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BuddyChallengeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(BuddyChallengeManager buddyChallengeManager, String str, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = buddyChallengeManager;
            this.$challengeId = str;
            this.$serial = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$challengeId, this.$serial, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object p;
            Long f;
            Long f2;
            Long f3;
            Long f4;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Tt4 tt4 = this.this$0.d;
                List<String> i2 = Hm7.i(this.$challengeId);
                this.L$0 = il6;
                this.label = 1;
                p = tt4.p(i2, 1, 3, false, this);
                if (p == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                p = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list = (List) ((Kz4) p).c();
            if (list != null && (!list.isEmpty())) {
                Ks4 ks4 = (Ks4) Pm7.F(list);
                ArrayList arrayList = new ArrayList();
                List<Ms4> d2 = ks4.d();
                List<Ms4> j0 = d2 != null ? Pm7.j0(d2) : null;
                List<Ms4> b = ks4.b();
                if (!(b == null || j0 == null)) {
                    Ao7.a(j0.addAll(b));
                }
                if (j0 != null) {
                    for (Ms4 ms4 : j0) {
                        Integer n = ms4.n();
                        long j = 0;
                        long longValue = (n == null || (f4 = Ao7.f((long) n.intValue())) == null) ? 0 : f4.longValue();
                        Integer m = ms4.m();
                        long longValue2 = (m == null || (f3 = Ao7.f((long) m.intValue())) == null) ? 0 : f3.longValue();
                        Integer b2 = ms4.b();
                        long longValue3 = (b2 == null || (f2 = Ao7.f((long) b2.intValue())) == null) ? 0 : f2.longValue();
                        Integer a2 = ms4.a();
                        if (!(a2 == null || (f = Ao7.f((long) a2.intValue())) == null)) {
                            j = f.longValue();
                        }
                        String c = Wg6.a(PortfolioApp.get.instance().l0(), ms4.d()) ? Um5.c(PortfolioApp.get.instance(), 2131886250) : Hz4.a.a(ms4.c(), ms4.e(), ms4.i());
                        Wg6.b(c, "name");
                        Integer h = ms4.h();
                        arrayList.add(new Zs1(c, h != null ? h.intValue() : -1, new Ys1(longValue - longValue2, longValue3 - j)));
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = BuddyChallengeManager.h.a();
                local.e(a3, "getChallengeInfo - watchPlayers: " + arrayList);
                String a4 = ks4.a();
                Object[] array = arrayList.toArray(new Zs1[0]);
                if (array != null) {
                    PortfolioApp.get.instance().g1(new BCChallengeInfoWatchAppInfo(new Ws1(a4, "BC", (Zs1[]) array)), this.$serial);
                } else {
                    throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BuddyChallengeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(BuddyChallengeManager buddyChallengeManager, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = buddyChallengeManager;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$serial, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object w;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Tt4 tt4 = this.this$0.d;
                this.L$0 = il6;
                this.label = 1;
                w = tt4.w(this);
                if (w == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                w = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List<Ps4> list = (List) w;
            if (list != null) {
                ArrayList arrayList = new ArrayList(Im7.m(list, 10));
                for (Ps4 ps4 : list) {
                    String f = ps4.f();
                    String g = ps4.g();
                    if (g == null) {
                        g = "";
                    }
                    arrayList.add(new Xs1(f, g, ""));
                }
                PortfolioApp.get.instance().g1(new BCListChallengeWatchAppInfo(arrayList), this.$serial);
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BuddyChallengeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(BuddyChallengeManager buddyChallengeManager, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = buddyChallengeManager;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$challengeId, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0062  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0086  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00a1  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x010c  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0110  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 276
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.Di.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BuddyChallengeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(BuddyChallengeManager buddyChallengeManager, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = buddyChallengeManager;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$challengeId, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object t;
            Ps4 a2;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Tt4 tt4 = this.this$0.d;
                String str = this.$challengeId;
                this.L$0 = il6;
                this.label = 1;
                t = tt4.t(str, new String[]{"waiting", "running"}, this);
                if (t == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                t = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Kz4 kz4 = (Kz4) t;
            if (!(kz4.c() == null || (a2 = this.this$0.d.a(this.$challengeId)) == null)) {
                a2.u(Ao7.e(((List) kz4.c()).size()));
                this.this$0.d.B(a2);
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BuddyChallengeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(BuddyChallengeManager buddyChallengeManager, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = buddyChallengeManager;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, this.$challengeId, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0063  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                r2 = 3
                r11 = 2
                r4 = 0
                r10 = 1
                java.lang.Object r9 = com.fossil.Yn7.d()
                int r0 = r12.label
                if (r0 == 0) goto L_0x009e
                if (r0 == r10) goto L_0x0026
                if (r0 != r11) goto L_0x001e
                java.lang.Object r0 = r12.L$1
                java.lang.String r0 = (java.lang.String) r0
                java.lang.Object r0 = r12.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r13)
            L_0x001b:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x001d:
                return r0
            L_0x001e:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0026:
                java.lang.Object r0 = r12.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r13)
            L_0x002d:
                com.portfolio.platform.PortfolioApp$inner r1 = com.portfolio.platform.PortfolioApp.get
                com.portfolio.platform.PortfolioApp r1 = r1.instance()
                boolean r1 = r1.L()
                com.portfolio.platform.PortfolioApp$inner r2 = com.portfolio.platform.PortfolioApp.get
                com.portfolio.platform.PortfolioApp r2 = r2.instance()
                java.lang.String r2 = r2.J()
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$Ai r5 = com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.h
                java.lang.String r5 = r5.a()
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r7 = "onChallengeStarted - consider retry sync data for buddy challenge - activeStatus: "
                r6.append(r7)
                r6.append(r1)
                java.lang.String r6 = r6.toString()
                r3.e(r5, r6)
                if (r1 == 0) goto L_0x001b
                int r3 = r2.length()
                if (r3 <= 0) goto L_0x006a
                r4 = r10
            L_0x006a:
                if (r4 == 0) goto L_0x001b
                com.portfolio.platform.PortfolioApp$inner r3 = com.portfolio.platform.PortfolioApp.get
                com.portfolio.platform.PortfolioApp r3 = r3.instance()
                boolean r3 = r3.A0(r2)
                if (r3 != 0) goto L_0x001b
                com.portfolio.platform.service.buddychallenge.BuddyChallengeManager r3 = r12.this$0
                com.mapped.An4 r3 = com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.d(r3)
                java.lang.Long r3 = r3.d()
                long r4 = r3.longValue()
                r6 = 0
                int r3 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r3 <= 0) goto L_0x001b
                com.portfolio.platform.service.buddychallenge.BuddyChallengeManager r3 = r12.this$0
                r12.L$0 = r0
                r12.Z$0 = r1
                r12.L$1 = r2
                r12.label = r11
                java.lang.Object r0 = r3.u(r12)
                if (r0 != r9) goto L_0x001b
                r0 = r9
                goto L_0x001d
            L_0x009e:
                com.fossil.El7.b(r13)
                com.mapped.Il6 r8 = r12.p$
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$Ai r1 = com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.h
                java.lang.String r1 = r1.a()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r5 = "onChallengeStarted - challengeId: "
                r3.append(r5)
                java.lang.String r5 = r12.$challengeId
                r3.append(r5)
                java.lang.String r3 = r3.toString()
                r0.e(r1, r3)
                com.portfolio.platform.service.buddychallenge.BuddyChallengeManager r0 = r12.this$0
                com.fossil.Tt4 r0 = com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.c(r0)
                java.lang.String[] r1 = new java.lang.String[r10]
                java.lang.String r3 = r12.$challengeId
                r1[r4] = r3
                java.util.List r1 = com.fossil.Hm7.i(r1)
                r12.L$0 = r8
                r12.label = r10
                r6 = 8
                r7 = 0
                r3 = r2
                r5 = r12
                java.lang.Object r0 = com.fossil.Tt4.q(r0, r1, r2, r3, r4, r5, r6, r7)
                if (r0 != r9) goto L_0x00e7
                r0 = r9
                goto L_0x001d
            L_0x00e7:
                r0 = r8
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.Fi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ws4 $this_run;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BuddyChallengeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(Ws4 ws4, Xe6 xe6, BuddyChallengeManager buddyChallengeManager) {
            super(2, xe6);
            this.$this_run = ws4;
            this.this$0 = buddyChallengeManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.$this_run, xe6, this.this$0);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                FriendRepository friendRepository = this.this$0.e;
                Lt4 d = this.$this_run.d();
                String c = d != null ? d.c() : null;
                if (c != null) {
                    Xs4 j = friendRepository.j(c);
                    if (j != null && j.c() == 1) {
                        j.k(0);
                        long n = this.this$0.e.n(j);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = BuddyChallengeManager.h.a();
                        local.e(a2, "onFriendResponse - friend: " + j + " - rowid: " + n);
                    }
                    return Cd6.a;
                }
                Wg6.i();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BuddyChallengeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(BuddyChallengeManager buddyChallengeManager, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = buddyChallengeManager;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, this.$challengeId, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0055  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                r7 = 0
                r2 = 3
                r4 = 0
                r11 = 2
                r10 = 1
                java.lang.Object r9 = com.fossil.Yn7.d()
                int r0 = r12.label
                if (r0 == 0) goto L_0x0057
                if (r0 == r10) goto L_0x0034
                if (r0 != r11) goto L_0x002c
                java.lang.Object r0 = r12.L$1
                com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
                java.lang.Object r1 = r12.L$0
                com.mapped.Il6 r1 = (com.mapped.Il6) r1
                com.fossil.El7.b(r13)
            L_0x001c:
                com.portfolio.platform.service.buddychallenge.BuddyChallengeManager r1 = r12.this$0
                java.lang.Object r0 = r0.c()
                java.util.List r0 = (java.util.List) r0
                java.lang.String r2 = r12.$challengeId
                com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.a(r1, r0, r2, r10)
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x002b:
                return r0
            L_0x002c:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0034:
                java.lang.Object r0 = r12.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r13)
                r3 = r0
                r1 = r13
            L_0x003d:
                r0 = r1
                com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
                com.portfolio.platform.service.buddychallenge.BuddyChallengeManager r1 = r12.this$0
                com.fossil.Tt4 r2 = com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.c(r1)
                r12.L$0 = r3
                r12.L$1 = r0
                r12.label = r11
                r3 = 5
                r5 = r12
                r6 = r11
                java.lang.Object r1 = com.fossil.Tt4.s(r2, r3, r4, r5, r6, r7)
                if (r1 != r9) goto L_0x001c
                r0 = r9
                goto L_0x002b
            L_0x0057:
                com.fossil.El7.b(r13)
                com.mapped.Il6 r8 = r12.p$
                com.portfolio.platform.service.buddychallenge.BuddyChallengeManager r0 = r12.this$0
                com.fossil.Tt4 r0 = com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.c(r0)
                java.lang.String r1 = r12.$challengeId
                java.util.List r1 = com.fossil.Gm7.b(r1)
                r12.L$0 = r8
                r12.label = r10
                r6 = 8
                r3 = r2
                r5 = r12
                java.lang.Object r1 = com.fossil.Tt4.q(r0, r1, r2, r3, r4, r5, r6, r7)
                if (r1 != r9) goto L_0x0078
                r0 = r9
                goto L_0x002b
            L_0x0078:
                r3 = r8
                goto L_0x003d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.Hi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Lt4 $this_run;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BuddyChallengeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(Lt4 lt4, Xe6 xe6, BuddyChallengeManager buddyChallengeManager) {
            super(2, xe6);
            this.$this_run = lt4;
            this.this$0 = buddyChallengeManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ii ii = new Ii(this.$this_run, xe6, this.this$0);
            ii.p$ = (Il6) obj;
            throw null;
            //return ii;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ii) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                String c = this.$this_run.c();
                String e = this.$this_run.e();
                if (e == null) {
                    e = "";
                }
                String b = this.$this_run.b();
                if (b == null) {
                    b = "";
                }
                String d = this.$this_run.d();
                if (d == null) {
                    d = "";
                }
                String a2 = this.$this_run.a();
                if (a2 == null) {
                    a2 = "";
                }
                Xs4 xs4 = new Xs4(c, e, b, d, null, a2, false, 0, 2);
                long n = this.this$0.e.n(xs4);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = BuddyChallengeManager.h.a();
                local.e(a3, "onReceivedRequest - friend: " + xs4 + " - rowId: " + n);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ EncryptedData $encryptedData;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BuddyChallengeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(BuddyChallengeManager buddyChallengeManager, EncryptedData encryptedData, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = buddyChallengeManager;
            this.$encryptedData = encryptedData;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ji ji = new Ji(this.this$0, this.$encryptedData, xe6);
            ji.p$ = (Il6) obj;
            throw null;
            //return ji;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ji) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object F;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                if (this.$encryptedData != null) {
                    Tt4 tt4 = this.this$0.d;
                    EncryptedData encryptedData = this.$encryptedData;
                    this.L$0 = il6;
                    this.label = 1;
                    F = tt4.F(encryptedData, this);
                    if (F == d) {
                        return d;
                    }
                }
                return Cd6.a;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                F = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = BuddyChallengeManager.h.a();
            local.e(a2, "processEncryptedData - result - " + ((Kz4) F));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BuddyChallengeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(BuddyChallengeManager buddyChallengeManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = buddyChallengeManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BuddyChallengeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Li(BuddyChallengeManager buddyChallengeManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = buddyChallengeManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(this);
        }
    }

    /*
    static {
        String simpleName = BuddyChallengeManager.class.getSimpleName();
        Wg6.b(simpleName, "BuddyChallengeManager::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public BuddyChallengeManager(PortfolioApp portfolioApp, Tt4 tt4, FriendRepository friendRepository, An4 an4) {
        Wg6.c(portfolioApp, "mPortfolioApp");
        Wg6.c(tt4, "mChallengeRepository");
        Wg6.c(friendRepository, "friendRepository");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.c = portfolioApp;
        this.d = tt4;
        this.e = friendRepository;
        this.f = an4;
    }

    @DexIgnore
    public static /* synthetic */ Object t(BuddyChallengeManager buddyChallengeManager, String str, boolean z, Xe6 xe6, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return buddyChallengeManager.s(str, z, xe6);
    }

    @DexIgnore
    public final void f() {
        Il6 il6 = this.a;
        if (il6 != null) {
            Jv7.d(il6, null, 1, null);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00ac  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void g(java.util.List<com.fossil.Ks4> r9, java.lang.String r10, boolean r11) {
        /*
            r8 = this;
            r2 = 0
            r5 = 0
            r6 = 1
            if (r9 == 0) goto L_0x000b
            boolean r0 = r9.isEmpty()
            if (r0 == 0) goto L_0x0075
        L_0x000b:
            r0 = r6
        L_0x000c:
            if (r0 != 0) goto L_0x0074
            java.lang.Object r0 = com.fossil.Pm7.F(r9)
            com.fossil.Ks4 r0 = (com.fossil.Ks4) r0
            java.util.List r1 = r0.d()
            if (r1 == 0) goto L_0x0077
            java.util.List r1 = com.fossil.Pm7.j0(r1)
        L_0x001e:
            java.util.List r3 = r0.b()
            if (r1 == 0) goto L_0x007b
            java.util.Iterator r4 = r1.iterator()
        L_0x0028:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0079
            java.lang.Object r1 = r4.next()
            r0 = r1
            com.fossil.Ms4 r0 = (com.fossil.Ms4) r0
            java.lang.String r0 = r0.d()
            com.portfolio.platform.PortfolioApp r7 = r8.c
            java.lang.String r7 = r7.l0()
            boolean r0 = com.mapped.Wg6.a(r0, r7)
            if (r0 == 0) goto L_0x0028
            r0 = r1
        L_0x0046:
            com.fossil.Ms4 r0 = (com.fossil.Ms4) r0
            if (r0 == 0) goto L_0x007b
            r2 = r0
        L_0x004b:
            if (r2 == 0) goto L_0x00ac
            java.lang.Integer r0 = r2.h()
            if (r0 == 0) goto L_0x00a3
            int r0 = r0.intValue()
        L_0x0057:
            java.lang.Integer r1 = r2.n()
            if (r1 == 0) goto L_0x00a5
            int r1 = r1.intValue()
        L_0x0061:
            if (r11 == 0) goto L_0x00a7
        L_0x0063:
            r4 = r1
            r3 = r0
            r5 = r6
        L_0x0066:
            com.fossil.Xr4 r0 = com.fossil.Xr4.a
            com.portfolio.platform.PortfolioApp r1 = r8.c
            java.lang.String r2 = r1.l0()
            com.portfolio.platform.PortfolioApp r6 = r8.c
            r1 = r10
            r0.c(r1, r2, r3, r4, r5, r6)
        L_0x0074:
            return
        L_0x0075:
            r0 = r5
            goto L_0x000c
        L_0x0077:
            r1 = r2
            goto L_0x001e
        L_0x0079:
            r0 = r2
            goto L_0x0046
        L_0x007b:
            if (r3 == 0) goto L_0x004b
            java.util.Iterator r3 = r3.iterator()
        L_0x0081:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x00af
            java.lang.Object r1 = r3.next()
            r0 = r1
            com.fossil.Ms4 r0 = (com.fossil.Ms4) r0
            java.lang.String r0 = r0.d()
            com.portfolio.platform.PortfolioApp r4 = r8.c
            java.lang.String r4 = r4.l0()
            boolean r0 = com.mapped.Wg6.a(r0, r4)
            if (r0 == 0) goto L_0x0081
            r0 = r1
        L_0x009f:
            com.fossil.Ms4 r0 = (com.fossil.Ms4) r0
            r2 = r0
            goto L_0x004b
        L_0x00a3:
            r0 = r5
            goto L_0x0057
        L_0x00a5:
            r1 = r5
            goto L_0x0061
        L_0x00a7:
            if (r0 == r6) goto L_0x0063
        L_0x00a9:
            r4 = r1
            r3 = r0
            goto L_0x0066
        L_0x00ac:
            r1 = r6
            r0 = r6
            goto L_0x00a9
        L_0x00af:
            r0 = r2
            goto L_0x009f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.g(java.util.List, java.lang.String, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x003d, code lost:
        if (com.fossil.Jv7.g(r0) == false) goto L_0x003f;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.mapped.Il6 h() {
        /*
            r5 = this;
            r1 = 0
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r0.getLocal()
            java.lang.String r3 = com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.g
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r0 = "coroutine - "
            r4.append(r0)
            com.mapped.Il6 r0 = r5.a
            r4.append(r0)
            java.lang.String r0 = " - isActive: "
            r4.append(r0)
            com.mapped.Il6 r0 = r5.a
            if (r0 == 0) goto L_0x0057
            boolean r0 = com.fossil.Jv7.g(r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
        L_0x0029:
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r2.e(r3, r0)
            com.mapped.Il6 r0 = r5.a
            if (r0 == 0) goto L_0x003f
            if (r0 == 0) goto L_0x0059
            boolean r0 = com.fossil.Jv7.g(r0)
            if (r0 != 0) goto L_0x0052
        L_0x003f:
            com.fossil.Jx7 r0 = com.fossil.Bw7.c()
            com.mapped.Il6 r0 = com.fossil.Jv7.a(r0)
            r2 = 1
            com.fossil.Uu7 r2 = com.fossil.Ux7.b(r1, r2, r1)
            com.mapped.Il6 r0 = com.fossil.Jv7.h(r0, r2)
            r5.a = r0
        L_0x0052:
            com.mapped.Il6 r0 = r5.a
            if (r0 == 0) goto L_0x005d
            return r0
        L_0x0057:
            r0 = r1
            goto L_0x0029
        L_0x0059:
            com.mapped.Wg6.i()
            throw r1
        L_0x005d:
            com.mapped.Wg6.i()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.h():com.mapped.Il6");
    }

    @DexIgnore
    public final void i(String str, String str2) {
        Wg6.c(str, "serial");
        Wg6.c(str2, "challengeId");
        Rm6 unused = Gu7.d(h(), Bw7.b(), null, new Bi(this, str2, str, null), 2, null);
    }

    @DexIgnore
    public final void j(String str) {
        Wg6.c(str, "serial");
        Rm6 unused = Gu7.d(h(), Bw7.b(), null, new Ci(this, str, null), 2, null);
    }

    @DexIgnore
    public final void k(Ws4 ws4) {
        Ss4 b2;
        Ss4 b3;
        String a2;
        Ss4 b4;
        String a3;
        Ss4 b5;
        String a4;
        Wg6.c(ws4, "notification");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.e(str, "handleNewNotification() - notification = " + ws4);
        String e2 = ws4.e();
        switch (e2.hashCode()) {
            case -1559094286:
                if (e2.equals("Title_Notification_End_Challenge") && (b2 = ws4.b()) != null) {
                    l(b2.a(), b2.d());
                    return;
                }
                return;
            case -764698119:
                if (e2.equals("Title_Notification_Start_Challenge") && (b3 = ws4.b()) != null && (a2 = b3.a()) != null) {
                    n(a2);
                    return;
                }
                return;
            case -473527954:
                if (e2.equals("Title_Respond_Invitation_Challenge") && (b4 = ws4.b()) != null && (a3 = b4.a()) != null) {
                    m(a3);
                    return;
                }
                return;
            case 238453777:
                if (e2.equals("Title_Notification_Reached_Goal_Challenge") && (b5 = ws4.b()) != null && (a4 = b5.a()) != null) {
                    p(a4);
                    return;
                }
                return;
            case 634854495:
                if (e2.equals("Title_Accepted_Friend_Request")) {
                    o(ws4);
                    return;
                }
                return;
            case 1433363166:
                if (e2.equals("Title_Send_Friend_Request")) {
                    q(ws4);
                    return;
                }
                return;
            case 1669546642:
                e2.equals("Title_Notification_Remind_End_Challenge");
                return;
            case 1898363949:
                e2.equals("Title_Send_Invitation_Challenge");
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void l(String str, String str2) {
        Rm6 unused = Gu7.d(h(), Bw7.b(), null, new Di(this, str, null), 2, null);
    }

    @DexIgnore
    public final void m(String str) {
        Rm6 unused = Gu7.d(h(), Bw7.b(), null, new Ei(this, str, null), 2, null);
    }

    @DexIgnore
    public final void n(String str) {
        Rm6 unused = Gu7.d(h(), Bw7.b(), null, new Fi(this, str, null), 2, null);
    }

    @DexIgnore
    public final void o(Ws4 ws4) {
        Lt4 d2 = ws4.d();
        if ((d2 != null ? d2.c() : null) != null) {
            Rm6 unused = Gu7.d(h(), Bw7.b(), null, new Gi(ws4, null, this), 2, null);
        }
    }

    @DexIgnore
    public final void p(String str) {
        Rm6 unused = Gu7.d(h(), Bw7.b(), null, new Hi(this, str, null), 2, null);
    }

    @DexIgnore
    public final void q(Ws4 ws4) {
        Lt4 d2 = ws4.d();
        if (d2 != null) {
            if (d2.c().length() > 0) {
                Rm6 unused = Gu7.d(h(), Bw7.b(), null, new Ii(d2, null, this), 2, null);
            }
        }
    }

    @DexIgnore
    public final void r(String str, EncryptedData encryptedData) {
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = g;
        local.e(str2, "processEncryptedData - serial: " + str + " - encryptedData: " + encryptedData);
        Rm6 unused = Gu7.d(h(), Bw7.b(), null, new Ji(this, encryptedData, null), 2, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00b0 A[Catch:{ all -> 0x015a }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object s(java.lang.String r13, boolean r14, com.mapped.Xe6<? super com.mapped.Cd6> r15) {
        /*
        // Method dump skipped, instructions count: 354
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.s(java.lang.String, boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object u(com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.Li
            if (r0 == 0) goto L_0x003e
            r0 = r7
            com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$Li r0 = (com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.Li) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003e
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x004c
            if (r3 != r5) goto L_0x0044
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.service.buddychallenge.BuddyChallengeManager r0 = (com.portfolio.platform.service.buddychallenge.BuddyChallengeManager) r0
            com.fossil.El7.b(r1)
            r6 = r0
        L_0x0027:
            r0 = r1
            com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
            java.lang.Object r0 = r0.c()
            com.fossil.Pt4 r0 = (com.fossil.Pt4) r0
            if (r0 == 0) goto L_0x003b
            com.portfolio.platform.PortfolioApp r1 = r6.c
            java.lang.String r0 = r0.a()
            r1.m1(r0)
        L_0x003b:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x003d:
            return r0
        L_0x003e:
            com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$Li r0 = new com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$Li
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x0044:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004c:
            com.fossil.El7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.g
            java.lang.String r4 = "setSyncData"
            r1.e(r3, r4)
            com.fossil.Tt4 r1 = r6.d
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r1 = r1.v(r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.buddychallenge.BuddyChallengeManager.u(com.mapped.Xe6):java.lang.Object");
    }
}
