package com.portfolio.platform.service.watchapp.commute;

import android.location.Location;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.ShareConstants;
import com.fossil.Bw7;
import com.fossil.By7;
import com.fossil.Ds5;
import com.fossil.Du1;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hr7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Lr7;
import com.fossil.Q88;
import com.fossil.Ul5;
import com.fossil.Um5;
import com.fossil.Vt7;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Dd0;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Kc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.diana.commutetime.Address;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.diana.commutetime.TrafficRequest;
import com.portfolio.platform.data.model.diana.commutetime.TrafficResponse;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.AnalyticsHelper;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppCommuteTimeManager {
    @DexIgnore
    public static WatchAppCommuteTimeManager q;
    @DexIgnore
    public static /* final */ Ai r; // = new Ai(null);
    @DexIgnore
    public PortfolioApp a;
    @DexIgnore
    public ApiServiceV2 b;
    @DexIgnore
    public LocationSource c;
    @DexIgnore
    public UserRepository d;
    @DexIgnore
    public DianaAppSettingRepository e;
    @DexIgnore
    public An4 f;
    @DexIgnore
    public /* final */ Gson g;
    @DexIgnore
    public String h;
    @DexIgnore
    public TrafficResponse i;
    @DexIgnore
    public Address j;
    @DexIgnore
    public AddressWrapper k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public AddressWrapper m;
    @DexIgnore
    public Bi n;
    @DexIgnore
    public Bi o;
    @DexIgnore
    public boolean p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final WatchAppCommuteTimeManager a() {
            WatchAppCommuteTimeManager watchAppCommuteTimeManager;
            synchronized (this) {
                if (WatchAppCommuteTimeManager.q == null) {
                    WatchAppCommuteTimeManager.q = new WatchAppCommuteTimeManager(null);
                }
                watchAppCommuteTimeManager = WatchAppCommuteTimeManager.q;
                if (watchAppCommuteTimeManager == null) {
                    Wg6.i();
                    throw null;
                }
            }
            return watchAppCommuteTimeManager;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi implements LocationSource.LocationListener {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$Listener$onLocationResult$1", f = "WatchAppCommuteTimeManager.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Location $location;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Location location, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$location = location;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$location, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    WatchAppCommuteTimeManager.this.v(this.$location.getLatitude(), this.$location.getLongitude());
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.LocationSource.LocationListener
        public void onLocationResult(Location location) {
            Wg6.c(location, PlaceFields.LOCATION);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppCommuteTimeManager", "onLocationResult lastLocation=" + location);
            Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), Bw7.c(), null, new Aii(this, location, null), 2, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1", f = "WatchAppCommuteTimeManager.kt", l = {112}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $destinationAlias;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppCommuteTimeManager this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1$1", f = "WatchAppCommuteTimeManager.kt", l = {114, 134}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x0043  */
            /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r14) {
                /*
                // Method dump skipped, instructions count: 405
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager.Ci.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(WatchAppCommuteTimeManager watchAppCommuteTimeManager, String str, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchAppCommuteTimeManager;
            this.$destinationAlias = str;
            this.$serial = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$destinationAlias, this.$serial, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (By7.c(DeviceAppResponse.LIFE_TIME, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Ko7 implements Hg6<Xe6<? super Q88<TrafficResponse>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xe6 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Location $location$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ TrafficRequest $trafficRequest;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppCommuteTimeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(TrafficRequest trafficRequest, Xe6 xe6, WatchAppCommuteTimeManager watchAppCommuteTimeManager, Location location, Xe6 xe62) {
            super(1, xe6);
            this.$trafficRequest = trafficRequest;
            this.this$0 = watchAppCommuteTimeManager;
            this.$location$inlined = location;
            this.$continuation$inlined = xe62;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Di(this.$trafficRequest, xe6, this.this$0, this.$location$inlined, this.$continuation$inlined);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<TrafficResponse>> xe6) {
            throw null;
            //return ((Di) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 j = this.this$0.j();
                TrafficRequest trafficRequest = this.$trafficRequest;
                this.label = 1;
                Object trafficStatus = j.getTrafficStatus(trafficRequest, this);
                return trafficStatus == d ? d : trafficStatus;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager", f = "WatchAppCommuteTimeManager.kt", l = {192}, m = "getDurationTime")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppCommuteTimeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(WatchAppCommuteTimeManager watchAppCommuteTimeManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = watchAppCommuteTimeManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager", f = "WatchAppCommuteTimeManager.kt", l = {145, 165}, m = "getDurationTimeBaseOnLocation")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppCommuteTimeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(WatchAppCommuteTimeManager watchAppCommuteTimeManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = watchAppCommuteTimeManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(this);
        }
    }

    @DexIgnore
    public WatchAppCommuteTimeManager() {
        this.g = new Gson();
        this.n = new Bi();
        this.o = new Bi();
        PortfolioApp.get.instance().getIface().c1(this);
    }

    @DexIgnore
    public /* synthetic */ WatchAppCommuteTimeManager(Qg6 qg6) {
        this();
    }

    @DexIgnore
    public final void g(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppCommuteTimeManager", "getCommuteTimeForWatchApp serial " + str + " destinationAlias " + str2);
        this.h = str;
        this.p = false;
        Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Ci(this, str2, str, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071 A[Catch:{ Exception -> 0x02a6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x02a1  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object h(android.location.Location r17, com.mapped.Xe6<? super com.mapped.Cd6> r18) {
        /*
        // Method dump skipped, instructions count: 693
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager.h(android.location.Location, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0167  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01b6  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x01b9  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01bc  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01bf  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x020b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object i(com.mapped.Xe6<? super com.mapped.Cd6> r15) {
        /*
        // Method dump skipped, instructions count: 596
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager.i(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final ApiServiceV2 j() {
        ApiServiceV2 apiServiceV2 = this.b;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        Wg6.n("mApiServiceV2");
        throw null;
    }

    @DexIgnore
    public final DianaAppSettingRepository k() {
        DianaAppSettingRepository dianaAppSettingRepository = this.e;
        if (dianaAppSettingRepository != null) {
            return dianaAppSettingRepository;
        }
        Wg6.n("mDianaAppSettingRepository");
        throw null;
    }

    @DexIgnore
    public final Float l(Address address, Address address2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppCommuteTimeManager", "getSimpleDistance between " + address + " and " + address2);
        float[] fArr = new float[1];
        Location.distanceBetween(address2.getLat(), address2.getLng(), address.getLat(), address.getLng(), fArr);
        return Float.valueOf(fArr[0]);
    }

    @DexIgnore
    public final String m(String str) {
        switch (str.hashCode()) {
            case -1078030475:
                if (str.equals("medium")) {
                    PortfolioApp portfolioApp = this.a;
                    if (portfolioApp != null) {
                        String c2 = Um5.c(portfolioApp, 2131887588);
                        Wg6.b(c2, "LanguageHelper.getString\u2026ng.traffic_status_medium)");
                        return c2;
                    }
                    Wg6.n("mPortfolioApp");
                    throw null;
                }
                break;
            case -284840886:
                if (str.equals("unknown")) {
                    PortfolioApp portfolioApp2 = this.a;
                    if (portfolioApp2 != null) {
                        String c3 = Um5.c(portfolioApp2, 2131887589);
                        Wg6.b(c3, "LanguageHelper.getString\u2026g.traffic_status_unknown)");
                        return c3;
                    }
                    Wg6.n("mPortfolioApp");
                    throw null;
                }
                break;
            case 99152071:
                if (str.equals("heavy")) {
                    PortfolioApp portfolioApp3 = this.a;
                    if (portfolioApp3 != null) {
                        String c4 = Um5.c(portfolioApp3, 2131887586);
                        Wg6.b(c4, "LanguageHelper.getString\u2026ing.traffic_status_heavy)");
                        return c4;
                    }
                    Wg6.n("mPortfolioApp");
                    throw null;
                }
                break;
            case 102970646:
                if (str.equals("light")) {
                    PortfolioApp portfolioApp4 = this.a;
                    if (portfolioApp4 != null) {
                        String c5 = Um5.c(portfolioApp4, 2131887587);
                        Wg6.b(c5, "LanguageHelper.getString\u2026ing.traffic_status_light)");
                        return c5;
                    }
                    Wg6.n("mPortfolioApp");
                    throw null;
                }
                break;
        }
        return "--";
    }

    @DexIgnore
    public final void n(LocationSource.ErrorState errorState) {
        Ul5 f2 = AnalyticsHelper.f.f("commute-time");
        if (f2 != null) {
            String str = this.h;
            if (str != null) {
                f2.d(str, false, ErrorCodeBuilder.AppError.NOT_FETCH_CURRENT_LOCATION.getValue());
            } else {
                Wg6.i();
                throw null;
            }
        }
        AnalyticsHelper.f.k("commute-time");
    }

    @DexIgnore
    public final void o(String str, String str2, int i2) {
        Wg6.c(str, "serial");
        Wg6.c(str2, ShareConstants.DESTINATION);
        if (i2 == Du1.START.ordinal()) {
            FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "process start commute time");
            g(str, str2);
        } else if (i2 == Du1.STOP.ordinal()) {
            FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "process stop commute time");
            JSONObject jSONObject = new JSONObject();
            AddressWrapper addressWrapper = this.m;
            jSONObject.put("des", addressWrapper != null ? addressWrapper.getId() : null);
            jSONObject.put("type", "end");
            jSONObject.put("end_type", "user_exit");
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
            String jSONObject2 = jSONObject.toString();
            Wg6.b(jSONObject2, "jsonObject.toString()");
            remote.i(component, session, str, "WatchAppCommuteTimeManager", jSONObject2);
            FLogger.INSTANCE.getRemote().summary(this.p ? 0 : FailureCode.USER_CANCELLED, FLogger.Component.APP, FLogger.Session.DIANA_COMMUTE_TIME, str, "WatchAppCommuteTimeManager");
            this.m = null;
            t();
            u();
        }
    }

    @DexIgnore
    public final void p() {
        String c2;
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "sendArriveToDestinationResponse");
        AddressWrapper addressWrapper = this.m;
        if (addressWrapper != null) {
            try {
                this.p = true;
                int i2 = Ds5.a[addressWrapper.getType().ordinal()];
                if (i2 == 1) {
                    PortfolioApp portfolioApp = this.a;
                    if (portfolioApp != null) {
                        c2 = Um5.c(portfolioApp, 2131886353);
                    } else {
                        Wg6.n("mPortfolioApp");
                        throw null;
                    }
                } else if (i2 == 2) {
                    PortfolioApp portfolioApp2 = this.a;
                    if (portfolioApp2 != null) {
                        c2 = Um5.c(portfolioApp2, 2131886351);
                    } else {
                        Wg6.n("mPortfolioApp");
                        throw null;
                    }
                } else if (i2 == 3) {
                    Hr7 hr7 = Hr7.a;
                    PortfolioApp portfolioApp3 = this.a;
                    if (portfolioApp3 != null) {
                        String c3 = Um5.c(portfolioApp3, 2131886350);
                        Wg6.b(c3, "LanguageHelper.getString\u2026Popup___ArrivedToAddress)");
                        c2 = String.format(c3, Arrays.copyOf(new Object[]{addressWrapper.getName()}, 1));
                        Wg6.b(c2, "java.lang.String.format(format, *args)");
                    } else {
                        Wg6.n("mPortfolioApp");
                        throw null;
                    }
                } else {
                    throw new Kc6();
                }
                Wg6.b(c2, "message");
                CommuteTimeWatchAppMessage commuteTimeWatchAppMessage = new CommuteTimeWatchAppMessage(c2, Dd0.END);
                PortfolioApp instance = PortfolioApp.get.instance();
                String str = this.h;
                if (str != null) {
                    instance.g1(commuteTimeWatchAppMessage, str);
                    JSONObject jSONObject = new JSONObject();
                    AddressWrapper addressWrapper2 = this.m;
                    jSONObject.put("des", addressWrapper2 != null ? addressWrapper2.getId() : null);
                    jSONObject.put("type", "end");
                    jSONObject.put("end_type", "arrived");
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
                    String str2 = this.h;
                    if (str2 != null) {
                        String jSONObject2 = jSONObject.toString();
                        Wg6.b(jSONObject2, "jsonObject.toString()");
                        remote.i(component, session, str2, "WatchAppCommuteTimeManager", jSONObject2);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            } catch (IllegalArgumentException e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppCommuteTimeManager", "sendArriveToDestinationResponse exception exception=" + e2.getMessage());
            }
        }
    }

    @DexIgnore
    public final void q(String str, long j2, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppCommuteTimeManager", "sendCommuteTimeAppResponse destination " + str + ", durationInTraffic " + j2 + ", trafficStatus " + str2);
        AddressWrapper addressWrapper = this.m;
        if (addressWrapper == null) {
            return;
        }
        if (addressWrapper == null) {
            Wg6.i();
            throw null;
        } else if (!TextUtils.isEmpty(addressWrapper.getName())) {
            int b2 = Lr7.b(((float) j2) / 60.0f);
            if (Vt7.j(str2, "not_found", true)) {
                PortfolioApp portfolioApp = this.a;
                if (portfolioApp != null) {
                    String c2 = Um5.c(portfolioApp, 2131886348);
                    Wg6.b(c2, "message");
                    CommuteTimeWatchAppMessage commuteTimeWatchAppMessage = new CommuteTimeWatchAppMessage(c2, Dd0.END);
                    PortfolioApp instance = PortfolioApp.get.instance();
                    String str3 = this.h;
                    if (str3 != null) {
                        instance.g1(commuteTimeWatchAppMessage, str3);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.n("mPortfolioApp");
                    throw null;
                }
            } else {
                try {
                    CommuteTimeWatchAppInfo commuteTimeWatchAppInfo = new CommuteTimeWatchAppInfo(str, b2, m(str2));
                    PortfolioApp instance2 = PortfolioApp.get.instance();
                    String str4 = this.h;
                    if (str4 != null) {
                        instance2.g1(commuteTimeWatchAppInfo, str4);
                        Ul5 f2 = AnalyticsHelper.f.f("commute-time");
                        if (f2 != null) {
                            String str5 = this.h;
                            if (str5 != null) {
                                f2.d(str5, this.l, "");
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                        AnalyticsHelper.f.k("commute-time");
                        return;
                    }
                    Wg6.i();
                    throw null;
                } catch (IllegalArgumentException e2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("WatchAppCommuteTimeManager", "sendCommuteTimeAppResponse exception exception=" + e2.getMessage());
                }
            }
        }
    }

    @DexIgnore
    public final void r() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "startMonitoringLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            PortfolioApp portfolioApp = this.a;
            if (portfolioApp != null) {
                LocationSource.observerLocation$default(locationSource, portfolioApp, this.o, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 8, null);
            } else {
                Wg6.n("mPortfolioApp");
                throw null;
            }
        } else {
            Wg6.n("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void s() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "startSignificantLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            PortfolioApp portfolioApp = this.a;
            if (portfolioApp != null) {
                LocationSource.observerLocation$default(locationSource, portfolioApp, this.n, true, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 8, null);
            } else {
                Wg6.n("mPortfolioApp");
                throw null;
            }
        } else {
            Wg6.n("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void t() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "stopMonitoringLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            locationSource.unObserverLocation(this.o, false);
        } else {
            Wg6.n("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void u() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "stopSignificantLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            locationSource.unObserverLocation(this.n, true);
        } else {
            Wg6.n("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void v(double d2, double d3) {
        Float f2;
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "updateObserver updateObserver=" + d2 + ',' + d3);
        AddressWrapper addressWrapper = this.m;
        if (addressWrapper == null) {
            f2 = null;
        } else if (addressWrapper != null) {
            double lat = addressWrapper.getLat();
            AddressWrapper addressWrapper2 = this.m;
            if (addressWrapper2 != null) {
                f2 = l(new Address(lat, addressWrapper2.getLng()), new Address(d2, d3));
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "updateObserver distance=" + f2);
        JSONObject jSONObject = new JSONObject();
        AddressWrapper addressWrapper3 = this.m;
        jSONObject.put("des", addressWrapper3 != null ? addressWrapper3.getId() : null);
        jSONObject.put("type", "loc_update");
        jSONObject.put("dis_to_des", f2);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
        String str = this.h;
        if (str != null) {
            String jSONObject2 = jSONObject.toString();
            Wg6.b(jSONObject2, "jsonObject.toString()");
            remote.i(component, session, str, "WatchAppCommuteTimeManager", jSONObject2);
            if (f2 != null) {
                if (f2 == null) {
                    Wg6.i();
                    throw null;
                } else if (f2.floatValue() < ((float) 2000)) {
                    if (f2 == null) {
                        Wg6.i();
                        throw null;
                    } else if (f2.floatValue() < ((float) 100)) {
                        t();
                        u();
                        p();
                        return;
                    } else {
                        u();
                        r();
                        return;
                    }
                }
            }
            t();
            s();
            return;
        }
        Wg6.i();
        throw null;
    }
}
