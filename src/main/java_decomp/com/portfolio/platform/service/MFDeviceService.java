package com.portfolio.platform.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import com.fossil.Aj5;
import com.fossil.Ao7;
import com.fossil.Bj5;
import com.fossil.Bw7;
import com.fossil.Ct0;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Kq5;
import com.fossil.Lo4;
import com.fossil.Mn5;
import com.fossil.Ph5;
import com.fossil.Rl5;
import com.fossil.S27;
import com.fossil.Tk5;
import com.fossil.Vi5;
import com.fossil.Vt7;
import com.fossil.Wi5;
import com.fossil.Yn7;
import com.fossil.fitness.FitnessData;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.fossil.wearables.fsl.location.LocationProvider;
import com.google.gson.Gson;
import com.mapped.An4;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.PermissionUtils;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.ServiceUtils;
import com.mapped.TimeTickReceiver;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.FitnessHelper;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import com.portfolio.platform.receiver.RestartServiceReceiver;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.service.buddychallenge.BuddyChallengeManager;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.service.workout.WorkoutTetherGpsManager;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MFDeviceService extends Service {
    @DexIgnore
    public static /* final */ String Y;
    @DexIgnore
    public static /* final */ String Z;
    @DexIgnore
    public static /* final */ a a0; // = new a(null);
    @DexIgnore
    public S27 A;
    @DexIgnore
    public VerifySecretKeyUseCase B;
    @DexIgnore
    public FitnessHelper C;
    @DexIgnore
    public Rl5 D;
    @DexIgnore
    public BuddyChallengeManager E;
    @DexIgnore
    public WorkoutTetherGpsManager F;
    @DexIgnore
    public MusicControlComponent G;
    @DexIgnore
    public MisfitDeviceProfile H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public /* final */ TimeTickReceiver J; // = new TimeTickReceiver();
    @DexIgnore
    public /* final */ Gson K; // = new Gson();
    @DexIgnore
    public /* final */ i L; // = new i(this);
    @DexIgnore
    public boolean M;
    @DexIgnore
    public Handler N;
    @DexIgnore
    public /* final */ Runnable O; // = new m(this);
    @DexIgnore
    public /* final */ d P; // = new d(this);
    @DexIgnore
    public /* final */ e Q; // = new e(this);
    @DexIgnore
    public /* final */ k R; // = new k();
    @DexIgnore
    public /* final */ o S; // = new o();
    @DexIgnore
    public /* final */ j T; // = new j();
    @DexIgnore
    public /* final */ f U; // = new f();
    @DexIgnore
    public /* final */ h V; // = new h();
    @DexIgnore
    public /* final */ n W; // = new n();
    @DexIgnore
    public /* final */ c X; // = new c(this);
    @DexIgnore
    public /* final */ b b; // = new b();
    @DexIgnore
    public An4 c;
    @DexIgnore
    public DeviceRepository d;
    @DexIgnore
    public ActivitiesRepository e;
    @DexIgnore
    public SummariesRepository f;
    @DexIgnore
    public SleepSessionsRepository g;
    @DexIgnore
    public SleepSummariesRepository h;
    @DexIgnore
    public UserRepository i;
    @DexIgnore
    public HybridPresetRepository j;
    @DexIgnore
    public MicroAppRepository k;
    @DexIgnore
    public HeartRateSampleRepository l;
    @DexIgnore
    public HeartRateSummaryRepository m;
    @DexIgnore
    public WorkoutSessionRepository s;
    @DexIgnore
    public FitnessDataRepository t;
    @DexIgnore
    public GoalTrackingRepository u;
    @DexIgnore
    public U04 v;
    @DexIgnore
    public AnalyticsHelper w;
    @DexIgnore
    public PortfolioApp x;
    @DexIgnore
    public LinkStreamingManager y;
    @DexIgnore
    public ThirdPartyRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return MFDeviceService.Z;
        }

        @DexIgnore
        public final String b() {
            return MFDeviceService.Y;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends Binder {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        public final MFDeviceService a() {
            return MFDeviceService.this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            if (intent != null) {
                String stringExtra = intent.getStringExtra("message");
                MFDeviceService mFDeviceService = this.a;
                Wg6.b(stringExtra, "message");
                mFDeviceService.f(stringExtra);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1", f = "MFDeviceService.kt", l = {239, 291, 292, 321, 331, 336, 342, 350, 356, 363, 383, 393, 438, 477, 522, 540}, m = "invokeSuspend")
        public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Intent $intent;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public int I$2;
            @DexIgnore
            public int I$3;
            @DexIgnore
            public int I$4;
            @DexIgnore
            public int I$5;
            @DexIgnore
            public int I$6;
            @DexIgnore
            public long J$0;
            @DexIgnore
            public long J$1;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$10;
            @DexIgnore
            public Object L$11;
            @DexIgnore
            public Object L$12;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public Object L$9;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$10", f = "MFDeviceService.kt", l = {635}, m = "invokeSuspend")
            public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $serial;
                @DexIgnore
                public /* final */ /* synthetic */ String $uiVersion;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(a aVar, String str, String str2, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                    this.$uiVersion = str;
                    this.$serial = str2;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    a aVar = new a(this.this$0, this.$uiVersion, this.$serial, xe6);
                    aVar.p$ = (Il6) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object C;
                    PortfolioApp portfolioApp;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        PortfolioApp o = this.this$0.this$0.a.o();
                        String str = this.$uiVersion;
                        this.L$0 = il6;
                        this.L$1 = o;
                        this.label = 1;
                        C = o.C(str, this);
                        if (C == d) {
                            return d;
                        }
                        portfolioApp = o;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        portfolioApp = (PortfolioApp) this.L$1;
                        C = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    boolean booleanValue = ((Boolean) C).booleanValue();
                    String str2 = this.$serial;
                    Wg6.b(str2, "serial");
                    portfolioApp.F0(str2, booleanValue);
                    return Cd6.a;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b implements CoroutineUseCase.Ei<VerifySecretKeyUseCase.Ci, VerifySecretKeyUseCase.Bi> {
                @DexIgnore
                public /* final */ /* synthetic */ a a;
                @DexIgnore
                public /* final */ /* synthetic */ String b;

                @DexIgnore
                public b(a aVar, String str) {
                    this.a = aVar;
                    this.b = str;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // com.portfolio.platform.CoroutineUseCase.Ei
                public /* bridge */ /* synthetic */ void a(VerifySecretKeyUseCase.Bi bi) {
                    b(bi);
                }

                @DexIgnore
                public void b(VerifySecretKeyUseCase.Bi bi) {
                    Wg6.c(bi, "errorValue");
                    PortfolioApp o = this.a.this$0.a.o();
                    String str = this.b;
                    Wg6.b(str, "serial");
                    o.e1(str, null);
                }

                @DexIgnore
                public void c(VerifySecretKeyUseCase.Ci ci) {
                    Wg6.c(ci, "responseValue");
                    PortfolioApp o = this.a.this$0.a.o();
                    String str = this.b;
                    Wg6.b(str, "serial");
                    o.e1(str, ci.a());
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // com.portfolio.platform.CoroutineUseCase.Ei
                public /* bridge */ /* synthetic */ void onSuccess(VerifySecretKeyUseCase.Ci ci) {
                    c(ci);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$8", f = "MFDeviceService.kt", l = {574}, m = "invokeSuspend")
            public static final class c extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $secretKey;
                @DexIgnore
                public /* final */ /* synthetic */ String $serial;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(a aVar, String str, String str2, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                    this.$serial = str;
                    this.$secretKey = str2;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    c cVar = new c(this.this$0, this.$serial, this.$secretKey, xe6);
                    cVar.p$ = (Il6) obj;
                    throw null;
                    //return cVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((c) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object updateDeviceSecretKey;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        DeviceRepository q = this.this$0.this$0.a.q();
                        String str = this.$serial;
                        if (str != null) {
                            String str2 = this.$secretKey;
                            if (str2 != null) {
                                this.L$0 = il6;
                                this.label = 1;
                                updateDeviceSecretKey = q.updateDeviceSecretKey(str, str2, this);
                                if (updateDeviceSecretKey == d) {
                                    return d;
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        updateDeviceSecretKey = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (((Ap4) updateDeviceSecretKey) instanceof Kq5) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String b = MFDeviceService.a0.b();
                        local.d(b, "update secret key " + this.$secretKey + " for " + this.$serial + " to server success");
                        PortfolioApp.get.instance().K0(this.$serial, true);
                    } else {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String b2 = MFDeviceService.a0.b();
                        local2.d(b2, "update secret key " + this.$secretKey + " for " + this.$serial + " to server failed");
                        PortfolioApp.get.instance().K0(this.$serial, false);
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$9", f = "MFDeviceService.kt", l = {599, 603}, m = "invokeSuspend")
            public static final class d extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $secretKey;
                @DexIgnore
                public /* final */ /* synthetic */ String $serial;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public Object L$3;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public d(a aVar, String str, String str2, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                    this.$serial = str;
                    this.$secretKey = str2;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    d dVar = new d(this.this$0, this.$serial, this.$secretKey, xe6);
                    dVar.p$ = (Il6) obj;
                    throw null;
                    //return dVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((d) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
                @Override // com.fossil.Zn7
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                    /*
                        r9 = this;
                        r8 = 2
                        r2 = 1
                        java.lang.Object r3 = com.fossil.Yn7.d()
                        int r0 = r9.label
                        if (r0 == 0) goto L_0x0081
                        if (r0 == r2) goto L_0x002f
                        if (r0 != r8) goto L_0x0027
                        java.lang.Object r0 = r9.L$3
                        java.lang.String r0 = (java.lang.String) r0
                        java.lang.Object r0 = r9.L$2
                        com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                        java.lang.Object r0 = r9.L$1
                        com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                        java.lang.Object r0 = r9.L$0
                        com.mapped.Il6 r0 = (com.mapped.Il6) r0
                        com.fossil.El7.b(r10)
                        r0 = r10
                    L_0x0022:
                        com.portfolio.platform.CoroutineUseCase$Ci r0 = (com.portfolio.platform.CoroutineUseCase.Ci) r0
                    L_0x0024:
                        com.mapped.Cd6 r0 = com.mapped.Cd6.a
                    L_0x0026:
                        return r0
                    L_0x0027:
                        java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                        java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                        r0.<init>(r1)
                        throw r0
                    L_0x002f:
                        java.lang.Object r0 = r9.L$0
                        com.mapped.Il6 r0 = (com.mapped.Il6) r0
                        com.fossil.El7.b(r10)
                        r2 = r0
                        r1 = r10
                    L_0x0038:
                        r0 = r1
                        com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                        if (r0 == 0) goto L_0x0024
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder
                        r1.<init>()
                        java.lang.String r4 = r0.getUserId()
                        r1.append(r4)
                        r4 = 58
                        r1.append(r4)
                        java.lang.String r4 = r9.$serial
                        r1.append(r4)
                        java.lang.String r1 = r1.toString()
                        com.portfolio.platform.service.MFDeviceService$d$a r4 = r9.this$0
                        com.portfolio.platform.service.MFDeviceService$d r4 = r4.this$0
                        com.portfolio.platform.service.MFDeviceService r4 = r4.a
                        com.fossil.S27 r4 = r4.r()
                        com.fossil.Bi5 r5 = new com.fossil.Bi5
                        r5.<init>()
                        java.lang.String r6 = r9.$secretKey
                        if (r6 == 0) goto L_0x009c
                        com.fossil.S27$Ai r7 = new com.fossil.S27$Ai
                        r7.<init>(r1, r5, r6)
                        r9.L$0 = r2
                        r9.L$1 = r0
                        r9.L$2 = r0
                        r9.L$3 = r1
                        r9.label = r8
                        java.lang.Object r0 = com.fossil.Jq4.a(r4, r7, r9)
                        if (r0 != r3) goto L_0x0022
                        r0 = r3
                        goto L_0x0026
                    L_0x0081:
                        com.fossil.El7.b(r10)
                        com.mapped.Il6 r0 = r9.p$
                        com.portfolio.platform.service.MFDeviceService$d$a r1 = r9.this$0
                        com.portfolio.platform.service.MFDeviceService$d r1 = r1.this$0
                        com.portfolio.platform.service.MFDeviceService r1 = r1.a
                        com.portfolio.platform.data.source.UserRepository r1 = r1.F()
                        r9.L$0 = r0
                        r9.label = r2
                        java.lang.Object r1 = r1.getCurrentUser(r9)
                        if (r1 != r3) goto L_0x00a1
                        r0 = r3
                        goto L_0x0026
                    L_0x009c:
                        com.mapped.Wg6.i()
                        r0 = 0
                        throw r0
                    L_0x00a1:
                        r2 = r0
                        goto L_0x0038
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.MFDeviceService.d.a.d.invokeSuspend(java.lang.Object):java.lang.Object");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class e extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<Void>>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ int $batteryLevel$inlined;
                @DexIgnore
                public /* final */ /* synthetic */ Device $device$inlined;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public e(Xe6 xe6, a aVar, int i, Device device) {
                    super(2, xe6);
                    this.this$0 = aVar;
                    this.$batteryLevel$inlined = i;
                    this.$device$inlined = device;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    e eVar = new e(xe6, this.this$0, this.$batteryLevel$inlined, this.$device$inlined);
                    eVar.p$ = (Il6) obj;
                    throw null;
                    //return eVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Ap4<Void>> xe6) {
                    throw null;
                    //return ((e) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        DeviceRepository q = this.this$0.this$0.a.q();
                        Device device = this.$device$inlined;
                        this.L$0 = il6;
                        this.label = 1;
                        Object updateDevice = q.updateDevice(device, true, this);
                        return updateDevice == d ? d : updateDevice;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, Intent intent, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = dVar;
                this.$intent = intent;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                a aVar = new a(this.this$0, this.$intent, xe6);
                aVar.p$ = (Il6) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r7v35, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r5v176, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r5v206 */
            /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r4v318 int), ('.' char), (r5v121 int)] */
            /* JADX WARNING: Removed duplicated region for block: B:116:0x0778  */
            /* JADX WARNING: Removed duplicated region for block: B:14:0x00bb  */
            /* JADX WARNING: Removed duplicated region for block: B:168:0x09fa  */
            /* JADX WARNING: Removed duplicated region for block: B:204:0x0cdf  */
            /* JADX WARNING: Removed duplicated region for block: B:21:0x013b  */
            /* JADX WARNING: Removed duplicated region for block: B:236:0x0f98  */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x0148  */
            /* JADX WARNING: Removed duplicated region for block: B:250:0x1026  */
            /* JADX WARNING: Removed duplicated region for block: B:39:0x038f  */
            /* JADX WARNING: Removed duplicated region for block: B:473:0x18f0 A[SYNTHETIC, Splitter:B:473:0x18f0] */
            /* JADX WARNING: Removed duplicated region for block: B:48:0x04e2  */
            /* JADX WARNING: Removed duplicated region for block: B:509:0x19f2  */
            /* JADX WARNING: Removed duplicated region for block: B:511:0x1a6c  */
            /* JADX WARNING: Removed duplicated region for block: B:513:0x1abf  */
            /* JADX WARNING: Removed duplicated region for block: B:625:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:627:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:628:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:629:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:638:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x006a  */
            /* JADX WARNING: Unknown variable types count: 2 */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r54) {
                /*
                // Method dump skipped, instructions count: 8250
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.MFDeviceService.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new a(this, intent, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1$onReceive$1", f = "MFDeviceService.kt", l = {673}, m = "invokeSuspend")
        public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Intent $intent;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a implements Lo4.Bi {
                @DexIgnore
                public /* final */ /* synthetic */ a a;
                @DexIgnore
                public /* final */ /* synthetic */ String b;

                @DexIgnore
                public a(a aVar, String str) {
                    this.a = aVar;
                    this.b = str;
                }

                @DexIgnore
                @Override // com.fossil.Lo4.Bi
                public void a(Location location, int i) {
                    MFDeviceService mFDeviceService = this.a.this$0.a;
                    String str = this.b;
                    Wg6.b(str, "serial");
                    mFDeviceService.L(str, location, i);
                    if (location != null) {
                        Lo4.h(this.a.this$0.a.o()).p(this);
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, Intent intent, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = eVar;
                this.$intent = intent;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                a aVar = new a(this.this$0, this.$intent, xe6);
                aVar.p$ = (Il6) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:14:0x007c  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r12) {
                /*
                // Method dump skipped, instructions count: 505
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.MFDeviceService.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new a(this, intent, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            FLogger.INSTANCE.getLocal().d(MFDeviceService.a0.b(), "Device app event receive");
            if (intent != null) {
                PortfolioApp.get.g(new Vi5(intent.getStringExtra(Constants.SERIAL_NUMBER), intent.getIntExtra(Constants.MICRO_APP_ID, -1), intent.getExtras()));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.MFDeviceService$executeGetWatchParamsFlow$1", f = "MFDeviceService.kt", l = {795, 797, 799}, m = "invokeSuspend")
    public static final class g extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ float $currentWPVersion;
        @DexIgnore
        public /* final */ /* synthetic */ int $majorVersion;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(MFDeviceService mFDeviceService, String str, int i, float f, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = mFDeviceService;
            this.$serial = str;
            this.$majorVersion = i;
            this.$currentWPVersion = f;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            g gVar = new g(this.this$0, this.$serial, this.$majorVersion, this.$currentWPVersion, xe6);
            gVar.p$ = (Il6) obj;
            throw null;
            //return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((g) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0068  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                r7 = 3
                r6 = 2
                r5 = 1
                java.lang.Object r3 = com.fossil.Yn7.d()
                int r0 = r8.label
                if (r0 == 0) goto L_0x004d
                if (r0 == r5) goto L_0x0027
                if (r0 == r6) goto L_0x0011
                if (r0 != r7) goto L_0x001f
            L_0x0011:
                java.lang.Object r0 = r8.L$1
                com.portfolio.platform.data.model.WatchParameterResponse r0 = (com.portfolio.platform.data.model.WatchParameterResponse) r0
                java.lang.Object r0 = r8.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r9)
            L_0x001c:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x001e:
                return r0
            L_0x001f:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0027:
                java.lang.Object r0 = r8.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r9)
                r2 = r0
                r1 = r9
            L_0x0030:
                r0 = r1
                com.portfolio.platform.data.model.WatchParameterResponse r0 = (com.portfolio.platform.data.model.WatchParameterResponse) r0
                if (r0 == 0) goto L_0x0068
                com.portfolio.platform.service.MFDeviceService r1 = r8.this$0
                com.fossil.Rl5 r1 = r1.H()
                java.lang.String r4 = r8.$serial
                float r5 = r8.$currentWPVersion
                r8.L$0 = r2
                r8.L$1 = r0
                r8.label = r6
                java.lang.Object r0 = r1.c(r4, r5, r0, r8)
                if (r0 != r3) goto L_0x001c
                r0 = r3
                goto L_0x001e
            L_0x004d:
                com.fossil.El7.b(r9)
                com.mapped.Il6 r0 = r8.p$
                com.portfolio.platform.service.MFDeviceService r1 = r8.this$0
                com.portfolio.platform.data.source.DeviceRepository r1 = r1.q()
                java.lang.String r2 = r8.$serial
                int r4 = r8.$majorVersion
                r8.L$0 = r0
                r8.label = r5
                java.lang.Object r1 = r1.getLatestWatchParamFromServer(r2, r4, r8)
                if (r1 != r3) goto L_0x0080
                r0 = r3
                goto L_0x001e
            L_0x0068:
                com.portfolio.platform.service.MFDeviceService r1 = r8.this$0
                com.fossil.Rl5 r1 = r1.H()
                java.lang.String r4 = r8.$serial
                float r5 = r8.$currentWPVersion
                r8.L$0 = r2
                r8.L$1 = r0
                r8.label = r7
                java.lang.Object r0 = r1.b(r4, r5, r8)
                if (r0 != r3) goto L_0x001c
                r0 = r3
                goto L_0x001e
            L_0x0080:
                r2 = r0
                goto L_0x0030
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.MFDeviceService.g.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            if (intent != null) {
                int intExtra = intent.getIntExtra(Constants.DAILY_STEPS, 0);
                int intExtra2 = intent.getIntExtra(Constants.DAILY_POINTS, 0);
                Calendar instance = Calendar.getInstance();
                Wg6.b(instance, "Calendar.getInstance()");
                long longExtra = intent.getLongExtra(Constants.UPDATED_TIME, instance.getTimeInMillis());
                FLogger.INSTANCE.getLocal().d(MFDeviceService.a0.b(), ".saveSyncResult - Update heartbeat step by heartbeat receiver");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = MFDeviceService.a0.b();
                local.i(b, "Heartbeat data received, dailySteps=" + intExtra + ", dailyPoints=" + intExtra2 + ", receivedTime=" + longExtra);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements ServiceConnection {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.MFDeviceService$mButtonConnectivity$1$onServiceConnected$1", f = "MFDeviceService.kt", l = {DateTimeConstants.HOURS_PER_WEEK}, m = "invokeSuspend")
        public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ IBinder $service;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(IBinder iBinder, Xe6 xe6) {
                super(2, xe6);
                this.$service = iBinder;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                a aVar = new a(this.$service, xe6);
                aVar.p$ = (Il6) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    PortfolioApp.inner inner = PortfolioApp.get;
                    IButtonConnectivity asInterface = IButtonConnectivity.Stub.asInterface(this.$service);
                    Wg6.b(asInterface, "IButtonConnectivity.Stub.asInterface(service)");
                    this.L$0 = il6;
                    this.label = 1;
                    if (inner.m(asInterface, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Wg6.c(componentName, "name");
            Wg6.c(iBinder, Constants.SERVICE);
            FLogger.INSTANCE.getLocal().d(MFDeviceService.a0.b(), "Connection to the BLE has been established");
            this.a.M = true;
            Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new a(iBinder, null), 3, null);
            this.a.T();
            this.a.R();
            FossilNotificationBar.c.d(this.a);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            Wg6.c(componentName, "name");
            this.a.M = false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            FLogger.INSTANCE.getLocal().d(MFDeviceService.a0.b(), "Micro app cancel event receive");
            if (intent != null) {
                PortfolioApp.get.g(new Wi5(intent.getStringExtra(Constants.SERIAL_NUMBER), intent.getIntExtra(Constants.MICRO_APP_ID, -1), intent.getIntExtra(Constants.VARIANT_ID, -1), intent.getIntExtra("gesture", -1)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            FLogger.INSTANCE.getLocal().d(MFDeviceService.a0.b(), "Notification event receive");
            if (intent != null) {
                PortfolioApp.get.g(new Aj5(intent.getIntExtra(ButtonService.NOTIFICATION_ID, -1), intent.getBooleanExtra(ButtonService.NOTIFICATION_RESULT, false)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.MFDeviceService$onLocationUpdated$1", f = "MFDeviceService.kt", l = {}, m = "invokeSuspend")
    public static final class l extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ DeviceLocation $deviceLocation;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(DeviceLocation deviceLocation, Xe6 xe6) {
            super(2, xe6);
            this.$deviceLocation = deviceLocation;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            l lVar = new l(this.$deviceLocation, xe6);
            lVar.p$ = (Il6) obj;
            throw null;
            //return lVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((l) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                Mn5.p.a().i().saveDeviceLocation(this.$deviceLocation);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService b;

        @DexIgnore
        public m(MFDeviceService mFDeviceService) {
            this.b = mFDeviceService;
        }

        @DexIgnore
        public final void run() {
            if (Vt7.j("release", "debug", true) && Wg6.a(this.b.o().Q(), Ph5.PORTFOLIO.getName())) {
                this.b.o().T0();
                this.b.P();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            intent.setAction("SCAN_DEVICE_FOUND");
            Ct0.b(context).d(intent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            FLogger.INSTANCE.getLocal().d(MFDeviceService.a0.b(), "Streaming event receive");
            if (intent != null) {
                int intExtra = intent.getIntExtra("gesture", -1);
                PortfolioApp.get.g(new Bj5(intent.getStringExtra(Constants.SERIAL_NUMBER), intExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.MFDeviceService$updateDeviceData$1", f = "MFDeviceService.kt", l = {991, 1000}, m = "invokeSuspend")
    public static final class p extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MisfitDeviceProfile $deviceProfile;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isNeedUpdateRemote;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(MFDeviceService mFDeviceService, String str, boolean z, MisfitDeviceProfile misfitDeviceProfile, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = mFDeviceService;
            this.$serial = str;
            this.$isNeedUpdateRemote = z;
            this.$deviceProfile = misfitDeviceProfile;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            p pVar = new p(this.this$0, this.$serial, this.$isNeedUpdateRemote, this.$deviceProfile, xe6);
            pVar.p$ = (Il6) obj;
            throw null;
            //return pVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((p) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x00bd  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 578
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.MFDeviceService.p.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.MFDeviceService$updatePairedDeviceToButtonService$1", f = "MFDeviceService.kt", l = {906}, m = "invokeSuspend")
    public static final class q extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(MFDeviceService mFDeviceService, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = mFDeviceService;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            q qVar = new q(this.this$0, xe6);
            qVar.p$ = (Il6) obj;
            throw null;
            //return qVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((q) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object currentUser;
            List<Device> list;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                List<Device> allDevice = this.this$0.q().getAllDevice();
                if (!allDevice.isEmpty()) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = MFDeviceService.a0.b();
                    local.d(b, "Get all device success devicesList=" + allDevice);
                    UserRepository F = this.this$0.F();
                    this.L$0 = il6;
                    this.L$1 = allDevice;
                    this.label = 1;
                    currentUser = F.getCurrentUser(this);
                    if (currentUser == d) {
                        return d;
                    }
                    list = allDevice;
                }
                return Cd6.a;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                list = (List) this.L$1;
                currentUser = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((MFUser) currentUser) != null) {
                for (Device device : list) {
                    this.this$0.o().F1(device.component1(), device.component2());
                }
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ MisfitDeviceProfile $this_run;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public r(MisfitDeviceProfile misfitDeviceProfile, Xe6 xe6, MFDeviceService mFDeviceService, String str) {
            super(2, xe6);
            this.$this_run = misfitDeviceProfile;
            this.this$0 = mFDeviceService;
            this.$serial$inlined = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            r rVar = new r(this.$this_run, xe6, this.this$0, this.$serial$inlined);
            rVar.p$ = (Il6) obj;
            throw null;
            //return rVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((r) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Integer vibrationStrength;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Device deviceBySerial = this.this$0.q().getDeviceBySerial(this.$serial$inlined);
                int c = Tk5.c(this.$this_run.getVibrationStrength().getVibrationStrengthLevel());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = MFDeviceService.a0.b();
                local.d(b, "newVibrationLvl: " + c + " - device: " + deviceBySerial);
                if (deviceBySerial != null && ((vibrationStrength = deviceBySerial.getVibrationStrength()) == null || vibrationStrength.intValue() != c)) {
                    deviceBySerial.setVibrationStrength(Ao7.e(c));
                    DeviceRepository q = this.this$0.q();
                    this.L$0 = il6;
                    this.L$1 = deviceBySerial;
                    this.I$0 = c;
                    this.label = 1;
                    if (q.updateDevice(deviceBySerial, false, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                Device device = (Device) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = MFDeviceService.class.getSimpleName();
        Wg6.b(simpleName, "MFDeviceService::class.java.simpleName");
        Y = simpleName;
        StringBuilder sb = new StringBuilder();
        Package r1 = MFDeviceService.class.getPackage();
        if (r1 != null) {
            Wg6.b(r1, "MFDeviceService::class.java.`package`!!");
            sb.append(r1.getName());
            sb.append(".location_updated");
            Z = sb.toString();
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    public static /* synthetic */ void j(MFDeviceService mFDeviceService, String str, int i2, int i3, ArrayList arrayList, Integer num, int i4, Object obj) {
        mFDeviceService.i(str, i2, i3, (i4 & 8) != 0 ? new ArrayList(i3) : arrayList, (i4 & 16) != 0 ? 10 : num);
    }

    @DexIgnore
    public final An4 A() {
        An4 an4 = this.c;
        if (an4 != null) {
            return an4;
        }
        Wg6.n("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final SleepSessionsRepository B() {
        SleepSessionsRepository sleepSessionsRepository = this.g;
        if (sleepSessionsRepository != null) {
            return sleepSessionsRepository;
        }
        Wg6.n("mSleepSessionsRepository");
        throw null;
    }

    @DexIgnore
    public final SleepSummariesRepository C() {
        SleepSummariesRepository sleepSummariesRepository = this.h;
        if (sleepSummariesRepository != null) {
            return sleepSummariesRepository;
        }
        Wg6.n("mSleepSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final SummariesRepository D() {
        SummariesRepository summariesRepository = this.f;
        if (summariesRepository != null) {
            return summariesRepository;
        }
        Wg6.n("mSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final ThirdPartyRepository E() {
        ThirdPartyRepository thirdPartyRepository = this.z;
        if (thirdPartyRepository != null) {
            return thirdPartyRepository;
        }
        Wg6.n("mThirdPartyRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository F() {
        UserRepository userRepository = this.i;
        if (userRepository != null) {
            return userRepository;
        }
        Wg6.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final VerifySecretKeyUseCase G() {
        VerifySecretKeyUseCase verifySecretKeyUseCase = this.B;
        if (verifySecretKeyUseCase != null) {
            return verifySecretKeyUseCase;
        }
        Wg6.n("mVerifySecretKeyUseCase");
        throw null;
    }

    @DexIgnore
    public final Rl5 H() {
        Rl5 rl5 = this.D;
        if (rl5 != null) {
            return rl5;
        }
        Wg6.n("mWatchParamHelper");
        throw null;
    }

    @DexIgnore
    public final WorkoutSessionRepository I() {
        WorkoutSessionRepository workoutSessionRepository = this.s;
        if (workoutSessionRepository != null) {
            return workoutSessionRepository;
        }
        Wg6.n("mWorkoutSessionRepository");
        throw null;
    }

    @DexIgnore
    public final WorkoutTetherGpsManager J() {
        WorkoutTetherGpsManager workoutTetherGpsManager = this.F;
        if (workoutTetherGpsManager != null) {
            return workoutTetherGpsManager;
        }
        Wg6.n("mWorkoutTetherGpsManager");
        throw null;
    }

    @DexIgnore
    public final void K(SKUModel sKUModel, int i2, int i3) {
        if (sKUModel != null) {
            String str = "";
            if (i3 == 1) {
                AnalyticsHelper analyticsHelper = this.w;
                if (analyticsHelper != null) {
                    String sku = sKUModel.getSku();
                    if (sku == null) {
                        sku = "";
                    }
                    String deviceName = sKUModel.getDeviceName();
                    if (deviceName != null) {
                        str = deviceName;
                    }
                    analyticsHelper.p(sku, str, i2);
                } else {
                    Wg6.n("mAnalyticsHelper");
                    throw null;
                }
            } else if (i3 == 2) {
                AnalyticsHelper analyticsHelper2 = this.w;
                if (analyticsHelper2 != null) {
                    String sku2 = sKUModel.getSku();
                    if (sku2 == null) {
                        sku2 = "";
                    }
                    String deviceName2 = sKUModel.getDeviceName();
                    if (deviceName2 == null) {
                        deviceName2 = str;
                    }
                    analyticsHelper2.n(sku2, deviceName2, i2);
                } else {
                    Wg6.n("mAnalyticsHelper");
                    throw null;
                }
            }
            An4 an4 = this.c;
            if (an4 != null) {
                an4.t1(Boolean.FALSE);
            } else {
                Wg6.n("mSharedPreferencesManager");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void L(String str, Location location, int i2) {
        Wg6.c(str, "serial");
        if (i2 >= 0) {
            if (location != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = Y;
                local.d(str2, "Inside " + Y + ".onLocationUpdated - location=[lat:" + location.getLatitude() + ", lon:" + location.getLongitude() + ", accuracy:" + location.getAccuracy() + "]");
                if (location.getAccuracy() <= 500.0f) {
                    try {
                        DeviceLocation deviceLocation = new DeviceLocation(str, location.getLatitude(), location.getLongitude(), System.currentTimeMillis());
                        h(str, deviceLocation);
                        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new l(deviceLocation, null), 3, null);
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str3 = Y;
                        local2.e(str3, "Error inside " + Y + ".onLocationUpdated - e=" + e2);
                    }
                }
            }
        } else if (i2 != -1) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = Y;
            local3.e(str4, "Error inside " + Y + ".onLocationUpdated - code=" + i2);
        }
    }

    @DexIgnore
    public final void M() {
        if (this.I) {
            FLogger.INSTANCE.getLocal().e(Y, "Return from device service register receiver");
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = Y;
        local.i(str, "Inside " + Y + ".registerReceiver " + l(ButtonService.Companion.getACTION_SERVICE_BLE_RESPONSE()));
        registerReceiver(this.P, new IntentFilter(l(ButtonService.Companion.getACTION_SERVICE_BLE_RESPONSE())));
        registerReceiver(this.X, new IntentFilter(l(ButtonService.Companion.getACTION_ANALYTIC_EVENT())));
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = Y;
        local2.i(str2, "Inside " + Y + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_STREAMING_EVENT());
        registerReceiver(this.S, new IntentFilter(l(ButtonService.Companion.getACTION_SERVICE_STREAMING_EVENT())));
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = Y;
        local3.i(str3, "Inside " + Y + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_MICRO_APP_CANCEL_EVENT());
        registerReceiver(this.T, new IntentFilter(l(ButtonService.Companion.getACTION_SERVICE_MICRO_APP_CANCEL_EVENT())));
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = Y;
        local4.i(str4, "Inside " + Y + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_DEVICE_APP_EVENT());
        registerReceiver(this.U, new IntentFilter(l(ButtonService.Companion.getACTION_SERVICE_DEVICE_APP_EVENT())));
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = Y;
        local5.i(str5, "Inside " + Y + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_HEARTBEAT_DATA());
        registerReceiver(this.V, new IntentFilter(l(ButtonService.Companion.getACTION_SERVICE_HEARTBEAT_DATA())));
        registerReceiver(this.Q, new IntentFilter(l(ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE())));
        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
        String str6 = Y;
        local6.i(str6, "Inside " + Y + ".registerReceiver " + ButtonService.Companion.getACTION_NOTIFICATION_SENT());
        registerReceiver(this.R, new IntentFilter(l(ButtonService.Companion.getACTION_NOTIFICATION_SENT())));
        registerReceiver(this.W, new IntentFilter(l(ButtonService.Companion.getACTION_SCAN_DEVICE_FOUND())));
        registerReceiver(this.J, new IntentFilter("android.intent.action.TIME_TICK"));
        this.I = true;
    }

    @DexIgnore
    public final List<FitnessDataWrapper> N(List<? extends FitnessData> list, String str, DateTime dateTime) {
        Wg6.c(list, "syncData");
        Wg6.c(str, "serial");
        Wg6.c(dateTime, "syncTime");
        ArrayList arrayList = new ArrayList();
        for (T t2 : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = Y;
            local.d(str2, "saveFitnessData=" + ((Object) t2));
            arrayList.add(new FitnessDataWrapper(t2, str, dateTime));
        }
        return arrayList;
    }

    @DexIgnore
    public final void O(MisfitDeviceProfile misfitDeviceProfile) {
        this.H = misfitDeviceProfile;
    }

    @DexIgnore
    public final void P() {
        Q();
        FLogger.INSTANCE.getLocal().d(Y, "Start reading realtime step timeoutHandler 30 mins");
        Handler handler = new Handler(getMainLooper());
        this.N = handler;
        if (handler != null) {
            handler.postDelayed(this.O, 1800000);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void Q() {
        if (this.N != null) {
            FLogger.INSTANCE.getLocal().d(Y, "Stop reading realtime step timeoutHandler");
            Handler handler = this.N;
            if (handler != null) {
                handler.removeCallbacks(this.O);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void R() {
        boolean b2 = FossilNotificationListenerService.w.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = Y;
        local.d(str, "triggerReconnectNotificationService() - isConnectedNotificationManager = " + b2);
        if (!b2 && PermissionUtils.a.h()) {
            FossilNotificationListenerService.w.d();
        }
    }

    @DexIgnore
    public final Rm6 S(MisfitDeviceProfile misfitDeviceProfile, String str, boolean z2) {
        Wg6.c(str, "serial");
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new p(this, str, z2, misfitDeviceProfile, null), 3, null);
    }

    @DexIgnore
    public final Rm6 T() {
        return Gu7.d(Jv7.a(Bw7.b()), null, null, new q(this, null), 3, null);
    }

    @DexIgnore
    public final void U(String str) {
        double d2 = 0.0d;
        Wg6.c(str, "serial");
        LocationProvider i2 = Mn5.p.a().i();
        DeviceLocation deviceLocation = i2.getDeviceLocation(str);
        double latitude = deviceLocation != null ? deviceLocation.getLatitude() : 0.0d;
        if (deviceLocation != null) {
            d2 = deviceLocation.getLongitude();
        }
        i2.saveDeviceLocation(new DeviceLocation(str, latitude, d2, System.currentTimeMillis()));
    }

    @DexIgnore
    public final void V(MisfitDeviceProfile misfitDeviceProfile, String str) {
        Wg6.c(str, "serial");
        if (misfitDeviceProfile != null) {
            boolean isDefaultValue = misfitDeviceProfile.getVibrationStrength().isDefaultValue();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = Y;
            local.d(str2, "updateVibrationStrengthLevel - isDefaultValue: " + isDefaultValue);
            if (!isDefaultValue) {
                Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new r(misfitDeviceProfile, null, this, str), 3, null);
            }
        }
    }

    @DexIgnore
    public final void f(String str) {
        Wg6.c(str, Constants.EVENT);
        AnalyticsHelper.f.g().i(str);
    }

    @DexIgnore
    public final void g(String str, Map<String, String> map) {
        Wg6.c(str, Constants.EVENT);
        AnalyticsHelper.f.g().l(str, map);
    }

    @DexIgnore
    public final void h(String str, DeviceLocation deviceLocation) {
        Intent intent = new Intent();
        intent.putExtra("SERIAL", str);
        intent.putExtra("device_location", deviceLocation);
        intent.setAction(Z);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void i(String str, int i2, int i3, ArrayList<Integer> arrayList, Integer num) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = Y;
        local.i(str2, "Broadcast sync status=" + i2);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", i2);
        intent.putExtra("LAST_ERROR_CODE", i3);
        intent.putExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), num);
        intent.putIntegerArrayListExtra("LIST_ERROR_CODE", arrayList);
        intent.putExtra("SERIAL", str);
        BleCommandResultManager bleCommandResultManager = BleCommandResultManager.d;
        CommunicateMode communicateMode = CommunicateMode.SYNC;
        bleCommandResultManager.h(communicateMode, new BleCommandResultManager.Ai(communicateMode, str, intent));
    }

    @DexIgnore
    public final void k(String str, Bundle bundle) {
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new g(this, str, bundle.getInt(ButtonService.WATCH_PARAMS_MAJOR), bundle.getFloat(ButtonService.CURRENT_WATCH_PARAMS_VERSION), null), 3, null);
    }

    @DexIgnore
    public final String l(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = Y;
        local.d(str2, "Key=" + getPackageName() + str);
        return getPackageName() + str;
    }

    @DexIgnore
    public final MisfitDeviceProfile m() {
        return this.H;
    }

    @DexIgnore
    public final ActivitiesRepository n() {
        ActivitiesRepository activitiesRepository = this.e;
        if (activitiesRepository != null) {
            return activitiesRepository;
        }
        Wg6.n("mActivitiesRepository");
        throw null;
    }

    @DexIgnore
    public final PortfolioApp o() {
        PortfolioApp portfolioApp = this.x;
        if (portfolioApp != null) {
            return portfolioApp;
        }
        Wg6.n("mApp");
        throw null;
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        FLogger.INSTANCE.getLocal().i(Y, "on misfit service bind");
        return this.b;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        PortfolioApp.get.instance().getIface().Z(this);
        M();
        FLogger.INSTANCE.getLocal().d(Y, "Service TRacking - MFDeviceService onCreate");
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.SERVICE;
        String str = Y;
        remote.i(component, session, "", str, "[MFDeviceService] onCreate " + System.currentTimeMillis());
    }

    @DexIgnore
    public void onDestroy() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = Y;
        local.d(str, "Inside " + Y + ".onDestroy");
        super.onDestroy();
        if (this.M) {
            ServiceUtils.a.g(this, this.L);
        }
        unregisterReceiver(this.P);
        unregisterReceiver(this.X);
        unregisterReceiver(this.S);
        unregisterReceiver(this.Q);
        unregisterReceiver(this.W);
        unregisterReceiver(this.U);
        unregisterReceiver(this.T);
        unregisterReceiver(this.V);
        unregisterReceiver(this.J);
        unregisterReceiver(this.R);
        this.I = false;
        BuddyChallengeManager buddyChallengeManager = this.E;
        if (buddyChallengeManager != null) {
            buddyChallengeManager.f();
        } else {
            Wg6.n("mBuddyChallengeManager");
            throw null;
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        if (Vt7.k(intent != null ? intent.getAction() : null, com.misfit.frameworks.buttonservice.utils.Constants.START_FOREGROUND_ACTION, false, 2, null)) {
            FLogger.INSTANCE.getLocal().d(Y, "Service Tracking - onStartCommand() - Start to bring service to foreground");
            FossilNotificationBar.c.c(this, this, false);
        } else {
            FLogger.INSTANCE.getLocal().d(Y, "Service Tracking - onStartCommand() - Stop service");
            FossilNotificationBar.c.c(this, this, true);
        }
        if (!this.M) {
            ServiceUtils.a.b(this, ButtonService.class, this.L, 1);
            FLogger.INSTANCE.getLocal().d(Y, "Service Tracking - onStartCommand() - Bound to service");
        }
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.SERVICE;
        String str = Y;
        remote.i(component, session, "", str, "[MFDeviceService] onStartCommand " + System.currentTimeMillis());
        return 1;
    }

    @DexIgnore
    public void onTaskRemoved(Intent intent) {
        super.onTaskRemoved(intent);
        FLogger.INSTANCE.getLocal().d(Y, "onTaskRemoved()");
        stopSelf();
        sendBroadcast(new Intent(this, RestartServiceReceiver.class));
    }

    @DexIgnore
    public final BuddyChallengeManager p() {
        BuddyChallengeManager buddyChallengeManager = this.E;
        if (buddyChallengeManager != null) {
            return buddyChallengeManager;
        }
        Wg6.n("mBuddyChallengeManager");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository q() {
        DeviceRepository deviceRepository = this.d;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        Wg6.n("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final S27 r() {
        S27 s27 = this.A;
        if (s27 != null) {
            return s27;
        }
        Wg6.n("mEncryptUseCase");
        throw null;
    }

    @DexIgnore
    public final FitnessDataRepository s() {
        FitnessDataRepository fitnessDataRepository = this.t;
        if (fitnessDataRepository != null) {
            return fitnessDataRepository;
        }
        Wg6.n("mFitnessDataRepository");
        throw null;
    }

    @DexIgnore
    public final FitnessHelper t() {
        FitnessHelper fitnessHelper = this.C;
        if (fitnessHelper != null) {
            return fitnessHelper;
        }
        Wg6.n("mFitnessHelper");
        throw null;
    }

    @DexIgnore
    public final GoalTrackingRepository u() {
        GoalTrackingRepository goalTrackingRepository = this.u;
        if (goalTrackingRepository != null) {
            return goalTrackingRepository;
        }
        Wg6.n("mGoalTrackingRepository");
        throw null;
    }

    @DexIgnore
    public final HeartRateSampleRepository v() {
        HeartRateSampleRepository heartRateSampleRepository = this.l;
        if (heartRateSampleRepository != null) {
            return heartRateSampleRepository;
        }
        Wg6.n("mHeartRateSampleRepository");
        throw null;
    }

    @DexIgnore
    public final HeartRateSummaryRepository w() {
        HeartRateSummaryRepository heartRateSummaryRepository = this.m;
        if (heartRateSummaryRepository != null) {
            return heartRateSummaryRepository;
        }
        Wg6.n("mHeartRateSummaryRepository");
        throw null;
    }

    @DexIgnore
    public final MicroAppRepository x() {
        MicroAppRepository microAppRepository = this.k;
        if (microAppRepository != null) {
            return microAppRepository;
        }
        Wg6.n("mMicroAppRepository");
        throw null;
    }

    @DexIgnore
    public final MusicControlComponent y() {
        MusicControlComponent musicControlComponent = this.G;
        if (musicControlComponent != null) {
            return musicControlComponent;
        }
        Wg6.n("mMusicControlComponent");
        throw null;
    }

    @DexIgnore
    public final HybridPresetRepository z() {
        HybridPresetRepository hybridPresetRepository = this.j;
        if (hybridPresetRepository != null) {
            return hybridPresetRepository;
        }
        Wg6.n("mPresetRepository");
        throw null;
    }
}
