package com.portfolio.platform.service;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.PixelCopy;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ar5;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Mx3;
import com.fossil.Pk5;
import com.fossil.Ux7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hh6;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.PermissionUtils;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.debug.DebugActivity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ShakeFeedbackService {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public WeakReference<Context> a;
    @DexIgnore
    public Ar5 b;
    @DexIgnore
    public Mx3 c;
    @DexIgnore
    public Mx3 d;
    @DexIgnore
    public String e;
    @DexIgnore
    public int f; // = -1;
    @DexIgnore
    public int g; // = -1;
    @DexIgnore
    public /* final */ Il6 h; // = Jv7.a(Ux7.b(null, 1, null).plus(Bw7.b()));
    @DexIgnore
    public /* final */ UserRepository i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Ar5.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Aii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.a.f = 1;
                if (this.b.a.a != null) {
                    WeakReference weakReference = this.b.a.a;
                    if (weakReference == null) {
                        Wg6.i();
                        throw null;
                    } else if (weakReference.get() != null) {
                        PermissionUtils.Ai ai = PermissionUtils.a;
                        WeakReference weakReference2 = this.b.a.a;
                        if (weakReference2 != null) {
                            Object obj = weakReference2.get();
                            if (obj == null) {
                                throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                            } else if (ai.s((Activity) obj, 123)) {
                                this.b.a.u();
                                Mx3 mx3 = this.b.a.c;
                                if (mx3 != null) {
                                    mx3.dismiss();
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Bii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.a.k();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Cii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.a.f = 0;
                this.b.a.k();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Dii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Dii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.a.f = 1;
                this.b.a.k();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Eii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Eii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.a.f = 3;
                this.b.a.k();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Fii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Fii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.a.f = 2;
                this.b.a.k();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Gii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$7$1", f = "ShakeFeedbackService.kt", l = {144}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Gii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Gii gii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = gii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        ShakeFeedbackService shakeFeedbackService = this.this$0.b.a;
                        this.L$0 = il6;
                        this.label = 1;
                        if (shakeFeedbackService.s(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            public Gii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.a.f = 1;
                if (this.b.a.a != null) {
                    WeakReference weakReference = this.b.a.a;
                    if (weakReference == null) {
                        Wg6.i();
                        throw null;
                    } else if (weakReference.get() != null) {
                        PermissionUtils.Ai ai = PermissionUtils.a;
                        WeakReference weakReference2 = this.b.a.a;
                        if (weakReference2 != null) {
                            Object obj = weakReference2.get();
                            if (obj == null) {
                                throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                            } else if (ai.s((Activity) obj, 123)) {
                                Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Aiii(this, null), 3, null);
                                Mx3 mx3 = this.b.a.c;
                                if (mx3 != null) {
                                    mx3.dismiss();
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Hii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Hii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.a.a != null) {
                    WeakReference weakReference = this.b.a.a;
                    if (weakReference == null) {
                        Wg6.i();
                        throw null;
                    } else if (weakReference.get() != null) {
                        DebugActivity.a aVar = DebugActivity.O;
                        WeakReference weakReference2 = this.b.a.a;
                        if (weakReference2 != null) {
                            Object obj = weakReference2.get();
                            if (obj != null) {
                                Wg6.b(obj, "contextWeakReference!!.get()!!");
                                aVar.a((Context) obj);
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                }
                Mx3 mx3 = this.b.a.c;
                if (mx3 != null) {
                    mx3.dismiss();
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        public Ai(ShakeFeedbackService shakeFeedbackService) {
            this.a = shakeFeedbackService;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:38:0x013b  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0151  */
        @Override // com.fossil.Ar5.Ai
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a() {
            /*
            // Method dump skipped, instructions count: 381
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.ShakeFeedbackService.Ai.a():void");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService", f = "ShakeFeedbackService.kt", l = {401}, m = "sendFeedbackEmail")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$13;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(ShakeFeedbackService shakeFeedbackService, Xe6 xe6) {
            super(xe6);
            this.this$0 = shakeFeedbackService;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.r(null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Context b;

        @DexIgnore
        public Ci(Context context) {
            this.b = context;
        }

        @DexIgnore
        public final void run() {
            Toast.makeText(this.b, "Can't zip file!", 1).show();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService", f = "ShakeFeedbackService.kt", l = {261}, m = "sendHardwareLog")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(ShakeFeedbackService shakeFeedbackService, Xe6 xe6) {
            super(xe6);
            this.this$0 = shakeFeedbackService;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService b;

        @DexIgnore
        public Ei(ShakeFeedbackService shakeFeedbackService) {
            this.b = shakeFeedbackService;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.g = 0;
            this.b.k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService b;

        @DexIgnore
        public Fi(ShakeFeedbackService shakeFeedbackService) {
            this.b = shakeFeedbackService;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements PixelCopy.OnPixelCopyFinishedListener {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService a;
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$startScreenShot$1$1", f = "ShakeFeedbackService.kt", l = {492}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Gi gi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = gi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Gi gi = this.this$0;
                    ShakeFeedbackService shakeFeedbackService = gi.a;
                    Bitmap bitmap = gi.b;
                    Wg6.b(bitmap, "bitmap");
                    this.L$0 = il6;
                    this.label = 1;
                    if (shakeFeedbackService.x(bitmap, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Gi(ShakeFeedbackService shakeFeedbackService, Bitmap bitmap) {
            this.a = shakeFeedbackService;
            this.b = bitmap;
        }

        @DexIgnore
        public final void onPixelCopyFinished(int i) {
            if (i == 0) {
                Rm6 unused = Gu7.d(this.a.h, null, null, new Aii(this, null), 3, null);
            } else {
                FLogger.INSTANCE.getLocal().d(ShakeFeedbackService.j, "prepareScreenShot - Can not extract image from current view");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$startScreenShot$2", f = "ShakeFeedbackService.kt", l = {504}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(ShakeFeedbackService shakeFeedbackService, Bitmap bitmap, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = shakeFeedbackService;
            this.$bitmap = bitmap;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, this.$bitmap, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ShakeFeedbackService shakeFeedbackService = this.this$0;
                Bitmap bitmap = this.$bitmap;
                Wg6.b(bitmap, "bitmap");
                this.L$0 = il6;
                this.label = 1;
                if (shakeFeedbackService.x(bitmap, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService", f = "ShakeFeedbackService.kt", l = {540, 552, 553}, m = "takeScreenShot")
    public static final class Ii extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(ShakeFeedbackService shakeFeedbackService, Xe6 xe6) {
            super(xe6);
            this.this$0 = shakeFeedbackService;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.x(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$takeScreenShot$2", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
    public static final class Ji extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(ShakeFeedbackService shakeFeedbackService, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = shakeFeedbackService;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ji ji = new Ji(this.this$0, xe6);
            ji.p$ = (Il6) obj;
            throw null;
            //return ji;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ji) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                Mx3 mx3 = this.this$0.c;
                if (mx3 == null) {
                    return null;
                }
                mx3.dismiss();
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = ShakeFeedbackService.class.getSimpleName();
        Wg6.b(simpleName, "ShakeFeedbackService::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public ShakeFeedbackService(UserRepository userRepository) {
        String file;
        Wg6.c(userRepository, "mUserRepository");
        this.i = userRepository;
        if (o()) {
            file = Environment.getExternalStorageDirectory().toString();
        } else {
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            Wg6.b(applicationContext, "PortfolioApp.instance.applicationContext");
            file = applicationContext.getFilesDir().toString();
        }
        this.e = file;
        this.e = Wg6.h(file, "/com.fossil.wearables.fossil/");
    }

    @DexIgnore
    public final void k() {
        WeakReference<Context> weakReference = this.a;
        if (weakReference == null) {
            return;
        }
        if (weakReference == null) {
            Wg6.i();
            throw null;
        } else if (weakReference.get() != null) {
            PermissionUtils.Ai ai = PermissionUtils.a;
            WeakReference<Context> weakReference2 = this.a;
            if (weakReference2 != null) {
                Context context = weakReference2.get();
                if (context == null) {
                    throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                } else if (ai.s((Activity) context, 123)) {
                    v();
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final List<File> l() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(FLogger.INSTANCE.getLocal().exportAppLogs());
        arrayList2.addAll(FLogger.INSTANCE.getRemote().exportAppLogs());
        arrayList2.addAll(MicroAppEventLogger.exportLogFiles());
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            File file = (File) it.next();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = j;
            StringBuilder sb = new StringBuilder();
            sb.append("Exporting ");
            Wg6.b(file, "file");
            sb.append(file.getName());
            sb.append(", size=");
            sb.append(file.length());
            local.d(str, sb.toString());
            try {
                File file2 = new File(this.e, file.getName());
                if (file2.exists()) {
                    file2.delete();
                } else {
                    file2.createNewFile();
                }
                FileChannel channel = new FileInputStream(file).getChannel();
                Wg6.b(channel, "FileInputStream(file).channel");
                FileChannel channel2 = new FileOutputStream(file2).getChannel();
                Wg6.b(channel2, "FileOutputStream(exportFile).channel");
                channel2.transferFrom(channel, 0, channel.size());
                channel.close();
                channel2.close();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = j;
                local2.d(str2, "Done exporting " + file2.getName() + ", size=" + file2.length());
                arrayList.add(file2);
            } catch (Exception e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = j;
                local3.e(str3, "Error while exporting log files - e=" + e2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final Uri m(String str) {
        if (Build.VERSION.SDK_INT < 24) {
            return Uri.parse("file://" + str);
        } else if (this.a == null || TextUtils.isEmpty(str)) {
            return null;
        } else {
            WeakReference<Context> weakReference = this.a;
            if (weakReference != null) {
                Context context = weakReference.get();
                if (context != null) {
                    return FileProvider.getUriForFile(context, "com.fossil.wearables.fossil.provider", new File(str));
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void n(Context context) {
        synchronized (this) {
            Wg6.c(context, Constants.ACTIVITY);
            this.a = new WeakReference<>(context);
            Object systemService = context.getSystemService("sensor");
            if (systemService != null) {
                SensorManager sensorManager = (SensorManager) systemService;
                Ar5 ar5 = new Ar5(new Ai(this));
                this.b = ar5;
                if (ar5 != null) {
                    ar5.b(sensorManager);
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type android.hardware.SensorManager");
            }
        }
    }

    @DexIgnore
    public final boolean o() {
        return Wg6.a("mounted", Environment.getExternalStorageState());
    }

    @DexIgnore
    public final boolean p() {
        Mx3 mx3;
        Mx3 mx32 = this.c;
        if (mx32 != null) {
            if (mx32 == null) {
                Wg6.i();
                throw null;
            } else if (mx32.isShowing() && (mx3 = this.d) != null) {
                if (mx3 == null) {
                    Wg6.i();
                    throw null;
                } else if (mx3.isShowing()) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final File q(String str, File file) {
        File[] listFiles;
        File file2;
        if (file == null || (listFiles = file.listFiles()) == null) {
            return null;
        }
        if (!(!(listFiles.length == 0))) {
            return null;
        }
        FLogger.INSTANCE.getLocal().e(j, ".sendFeedbackEmail - files.length=" + listFiles.length);
        try {
            file2 = new File(Environment.getExternalStorageDirectory(), str + ".zip");
            try {
                ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(file2));
                for (File file3 : listFiles) {
                    byte[] bArr = new byte[1024];
                    FileInputStream fileInputStream = new FileInputStream(file3);
                    Wg6.b(file3, "file");
                    zipOutputStream.putNextEntry(new ZipEntry(file3.getName()));
                    Hh6 hh6 = new Hh6();
                    while (true) {
                        int read = fileInputStream.read(bArr);
                        hh6.element = read;
                        if (!(read > 0)) {
                            break;
                        }
                        zipOutputStream.write(bArr, 0, hh6.element);
                    }
                    fileInputStream.close();
                }
                zipOutputStream.close();
                return file2;
            } catch (IOException e2) {
                e = e2;
                FLogger.INSTANCE.getLocal().e(j, ".sendFeedbackEmail - read sdk log ex=" + e);
                return file2;
            }
        } catch (IOException e3) {
            e = e3;
            file2 = null;
            FLogger.INSTANCE.getLocal().e(j, ".sendFeedbackEmail - read sdk log ex=" + e);
            return file2;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object r(java.lang.String r25, java.util.List<java.lang.String> r26, java.util.List<? extends java.io.File> r27, com.mapped.Xe6<? super com.mapped.Cd6> r28) {
        /*
        // Method dump skipped, instructions count: 1524
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.ShakeFeedbackService.r(java.lang.String, java.util.List, java.util.List, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004c A[Catch:{ Exception -> 0x0177 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object s(com.mapped.Xe6<? super com.mapped.Cd6> r15) {
        /*
        // Method dump skipped, instructions count: 417
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.ShakeFeedbackService.s(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object t(Context context, Xe6<? super Cd6> xe6) {
        v();
        return Cd6.a;
    }

    @DexIgnore
    public final void u() {
        WeakReference<Context> weakReference = this.a;
        if (weakReference == null) {
            Wg6.i();
            throw null;
        } else if (Pk5.a(weakReference.get()) && !p()) {
            Mx3 mx3 = this.d;
            if (mx3 != null) {
                if (mx3 != null) {
                    mx3.dismiss();
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            WeakReference<Context> weakReference2 = this.a;
            if (weakReference2 != null) {
                Context context = weakReference2.get();
                if (context != null) {
                    Object systemService = context.getSystemService("layout_inflater");
                    if (systemService != null) {
                        View inflate = ((LayoutInflater) systemService).inflate(2131558462, (ViewGroup) null);
                        WeakReference<Context> weakReference3 = this.a;
                        if (weakReference3 != null) {
                            Context context2 = weakReference3.get();
                            if (context2 != null) {
                                Mx3 mx32 = new Mx3(context2);
                                this.d = mx32;
                                mx32.setContentView(inflate);
                                View findViewById = inflate.findViewById(2131363419);
                                if (findViewById != null) {
                                    ((TextView) findViewById).setText("4.6.0");
                                    inflate.findViewById(2131361947).setOnClickListener(new Ei(this));
                                    inflate.findViewById(2131361949).setOnClickListener(new Fi(this));
                                    Mx3 mx33 = this.d;
                                    if (mx33 != null) {
                                        mx33.show();
                                    } else {
                                        Wg6.i();
                                        throw null;
                                    }
                                } else {
                                    throw new Rc6("null cannot be cast to non-null type android.widget.TextView");
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        throw new Rc6("null cannot be cast to non-null type android.view.LayoutInflater");
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void v() {
        WeakReference<Context> weakReference = this.a;
        if (weakReference != null) {
            Context context = weakReference.get();
            if (context != null) {
                Window window = ((Activity) context).getWindow();
                Wg6.b(window, "(contextWeakReference!!.get() as Activity).window");
                View decorView = window.getDecorView();
                Wg6.b(decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                View rootView = decorView.getRootView();
                if (Build.VERSION.SDK_INT >= 26) {
                    Wg6.b(rootView, "view");
                    Bitmap createBitmap = Bitmap.createBitmap(rootView.getWidth(), rootView.getHeight(), Bitmap.Config.ARGB_8888);
                    int[] iArr = new int[2];
                    rootView.getLocationInWindow(iArr);
                    WeakReference<Context> weakReference2 = this.a;
                    if (weakReference2 != null) {
                        Context context2 = weakReference2.get();
                        if (context2 != null) {
                            PixelCopy.request(((Activity) context2).getWindow(), new Rect(iArr[0], iArr[1], iArr[0] + rootView.getWidth(), rootView.getHeight() + iArr[1]), createBitmap, new Gi(this, createBitmap), new Handler(Looper.getMainLooper()));
                            return;
                        }
                        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.b(rootView, "view");
                rootView.setDrawingCacheEnabled(true);
                if (rootView.getDrawingCache() != null) {
                    Bitmap createBitmap2 = Bitmap.createBitmap(rootView.getDrawingCache());
                    rootView.setDrawingCacheEnabled(false);
                    Rm6 unused = Gu7.d(this.h, null, null, new Hi(this, createBitmap2, null), 3, null);
                    return;
                }
                return;
            }
            throw new Rc6("null cannot be cast to non-null type android.app.Activity");
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void w() {
        synchronized (this) {
            Ar5 ar5 = this.b;
            if (ar5 != null) {
                ar5.c();
            }
            if (this.c != null) {
                Mx3 mx3 = this.c;
                if (mx3 != null) {
                    mx3.dismiss();
                    this.c = null;
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            if (this.d != null) {
                Mx3 mx32 = this.d;
                if (mx32 != null) {
                    mx32.dismiss();
                    this.d = null;
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            this.a = null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x01bc  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x026e  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x029a  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x02c2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object x(android.graphics.Bitmap r26, com.mapped.Xe6<? super com.mapped.Cd6> r27) {
        /*
        // Method dump skipped, instructions count: 726
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.ShakeFeedbackService.x(android.graphics.Bitmap, com.mapped.Xe6):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0114, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0115, code lost:
        r1 = null;
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0118, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0119, code lost:
        r3 = null;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0118 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0027] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.io.File y(java.util.ArrayList<java.lang.String> r15) {
        /*
        // Method dump skipped, instructions count: 288
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.ShakeFeedbackService.y(java.util.ArrayList):java.io.File");
    }
}
