package com.portfolio.platform.service.workout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Handler;
import android.os.PowerManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.places.model.PlaceFields;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hr7;
import com.fossil.Jn5;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Mq1;
import com.fossil.Pq1;
import com.fossil.U08;
import com.fossil.Ux7;
import com.fossil.W08;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Dd0;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.response.workout.ResumeWorkoutInfoData;
import com.misfit.frameworks.buttonservice.model.watchapp.response.workout.StartWorkoutInfoData;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutTetherGpsManager {
    @DexIgnore
    public Location a;
    @DexIgnore
    public /* final */ PowerManager b;
    @DexIgnore
    public Ai c;
    @DexIgnore
    public volatile boolean d;
    @DexIgnore
    public Handler e;
    @DexIgnore
    public Runnable f;
    @DexIgnore
    public /* final */ U08 g;
    @DexIgnore
    public /* final */ Il6 h;
    @DexIgnore
    public /* final */ Bi i;
    @DexIgnore
    public /* final */ PortfolioApp j;
    @DexIgnore
    public /* final */ LocationSource k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements LocationSource.LocationListener {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutTetherGpsManager a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(WorkoutTetherGpsManager workoutTetherGpsManager) {
            this.a = workoutTetherGpsManager;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.LocationSource.LocationListener
        public void onLocationResult(Location location) {
            Wg6.c(location, PlaceFields.LOCATION);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutTetherGpsManager", "onLocationChanged location=" + location);
            if (this.a.a == null) {
                long currentTimeMillis = System.currentTimeMillis() - location.getTime();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("WorkoutTetherGpsManager", "delta of first location from android " + currentTimeMillis);
                if (currentTimeMillis > ((long) 2000)) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("WorkoutTetherGpsManager", "Ignore this outdated location " + location);
                    return;
                }
                this.a.a = location;
            }
            this.a.j.P1(this.a.j.J(), location);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutTetherGpsManager a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(WorkoutTetherGpsManager workoutTetherGpsManager) {
            this.a = workoutTetherGpsManager;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (Wg6.a(intent != null ? intent.getAction() : null, "android.os.action.POWER_SAVE_MODE_CHANGED")) {
                FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "mPowerSavingModeChangedReceiver");
                this.a.m();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutTetherGpsManager b;

        @DexIgnore
        public Ci(WorkoutTetherGpsManager workoutTetherGpsManager) {
            this.b = workoutTetherGpsManager;
        }

        @DexIgnore
        public final void run() {
            this.b.t();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.workout.WorkoutTetherGpsManager$startLocationObserver$1", f = "WorkoutTetherGpsManager.kt", l = {261}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutTetherGpsManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(WorkoutTetherGpsManager workoutTetherGpsManager, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = workoutTetherGpsManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            U08 u08;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                u08 = this.this$0.g;
                this.L$0 = il6;
                this.L$1 = u08;
                this.label = 1;
                if (u08.a(null, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                u08 = (U08) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            try {
                boolean isObservingLocation = this.this$0.k.isObservingLocation(this.this$0.c, false);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherGpsManager", "startLocationObserver isObserving " + isObservingLocation);
                this.this$0.e.removeCallbacksAndMessages(null);
                if (!isObservingLocation) {
                    this.this$0.a = null;
                    FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "startLocationObserver start observing location");
                    this.this$0.k.observerLocation(this.this$0.j, this.this$0.c, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                Cd6 cd6 = Cd6.a;
                u08.b(null);
                return Cd6.a;
            } catch (Throwable th) {
                u08.b(null);
                throw th;
            }
        }
    }

    @DexIgnore
    public WorkoutTetherGpsManager(PortfolioApp portfolioApp, LocationSource locationSource) {
        Wg6.c(portfolioApp, "mPortfolioApp");
        Wg6.c(locationSource, "mLocationSource");
        this.j = portfolioApp;
        this.k = locationSource;
        Object systemService = PortfolioApp.get.instance().getSystemService("power");
        if (systemService != null) {
            this.b = (PowerManager) systemService;
            this.c = new Ai(this);
            this.e = new Handler();
            this.f = new Ci(this);
            this.g = W08.b(false, 1, null);
            this.h = Jv7.a(Bw7.c().plus(Ux7.b(null, 1, null)));
            this.i = new Bi(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.os.PowerManager");
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "autoResumeWorkoutTracking ");
        this.d = true;
        q();
        m();
        n();
    }

    @DexIgnore
    public final String j(boolean z, boolean z2) {
        boolean m = Jn5.b.m(PortfolioApp.get.instance(), Jn5.Ci.LOCATION_BACKGROUND);
        boolean isPowerSaveMode = this.b.isPowerSaveMode();
        PortfolioApp instance = PortfolioApp.get.instance();
        String str = "";
        if (!z || !m) {
            str = " " + instance.getResources().getString(2131886730);
        }
        if (!z2) {
            if (str.length() > 0) {
                str = str + ',';
            }
            str = str + ' ' + instance.getResources().getString(2131886731);
        }
        if (isPowerSaveMode) {
            if (str.length() > 0) {
                str = str + ',';
            }
            str = str + ' ' + instance.getResources().getString(2131886732);
        }
        FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "watchContent " + str);
        if (!(str.length() > 0)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(' ');
        Hr7 hr7 = Hr7.a;
        String string = instance.getResources().getString(2131886733);
        Wg6.b(string, "context.resources.getStr\u2026AllowBrandAppToVisualize)");
        String Q = instance.Q();
        if (Q != null) {
            String upperCase = Q.toUpperCase();
            Wg6.b(upperCase, "(this as java.lang.String).toUpperCase()");
            String format = String.format(string, Arrays.copyOf(new Object[]{upperCase}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            sb.append(format);
            return sb.toString();
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final Lc6<String, String> k() {
        String str;
        String str2;
        boolean m = Jn5.b.m(PortfolioApp.get.instance(), Jn5.Ci.LOCATION_FINE);
        boolean m2 = Jn5.b.m(PortfolioApp.get.instance(), Jn5.Ci.LOCATION_BACKGROUND);
        boolean m3 = Jn5.b.m(PortfolioApp.get.instance(), Jn5.Ci.LOCATION_SERVICE);
        boolean isPowerSaveMode = this.b.isPowerSaveMode();
        PortfolioApp instance = PortfolioApp.get.instance();
        Hr7 hr7 = Hr7.a;
        String string = instance.getResources().getString(2131886733);
        Wg6.b(string, "context.resources.getStr\u2026AllowBrandAppToVisualize)");
        String Q = instance.Q();
        if (Q != null) {
            String upperCase = Q.toUpperCase();
            Wg6.b(upperCase, "(this as java.lang.String).toUpperCase()");
            String format = String.format(string, Arrays.copyOf(new Object[]{upperCase}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "postWorkoutNotification isFinePermissionGranted " + m + " isBackgroundLocationGranted " + m2 + " isLocationTurnOn " + m3 + " isPowerSaving " + isPowerSaveMode);
            if (!m || !m2) {
                str = instance.getResources().getString(2131886730);
                Wg6.b(str, "context.resources.getStr\u2026LocationPermissionAlways)");
                str2 = str + ' ' + format;
            } else if (m && m2 && !m3) {
                str = instance.getResources().getString(2131886731);
                Wg6.b(str, "context.resources.getStr\u2026__TurnOnLocationServices)");
                str2 = str + "  " + format;
            } else if (!m || !m2 || !m3 || !isPowerSaveMode) {
                str = instance.getResources().getString(2131886722);
                Wg6.b(str, "context.resources.getStr\u2026Text__GpsWorkoutTracking)");
                str2 = instance.getResources().getString(2131886720);
                Wg6.b(str2, "context.resources.getStr\u2026rkoutSessionLocationData)");
            } else {
                str = instance.getResources().getString(2131886732);
                Wg6.b(str, "context.resources.getStr\u2026__TurnOffPowerSavingMode)");
                str2 = str + ' ' + format;
            }
            return new Lc6<>(str, str2);
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final boolean l() {
        return this.d;
    }

    @DexIgnore
    public final void m() {
        Lc6<String, String> k2 = k();
        FossilNotificationBar.c.e(this.j, k2.getFirst(), k2.getSecond());
    }

    @DexIgnore
    public final void n() {
        try {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGED");
            PortfolioApp.get.instance().registerReceiver(this.i, intentFilter);
        } catch (Exception e2) {
        }
    }

    @DexIgnore
    public final void o(Mq1 mq1) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherGpsManager", "resumeWorkoutTracking " + mq1);
        if (mq1 != null) {
            if (mq1.isRequiredGPS()) {
                FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "resumeWorkoutTracking gpsRequired");
                i();
            }
            PortfolioApp.get.instance().g1(new ResumeWorkoutInfoData(mq1, "", Dd0.SUCCESS), PortfolioApp.get.instance().J());
        }
    }

    @DexIgnore
    public final void p() {
        boolean isObservingLocation = this.k.isObservingLocation(this.c, false);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherGpsManager", "scheduleStopGpsListener isObserving " + isObservingLocation);
        this.e.removeCallbacksAndMessages(null);
        if (isObservingLocation) {
            this.e.postDelayed(this.f, 3600000);
        }
    }

    @DexIgnore
    public final Rm6 q() {
        return Gu7.d(this.h, null, null, new Di(this, null), 3, null);
    }

    @DexIgnore
    public final void r(String str, Pq1 pq1) {
        boolean z = true;
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherGpsManager", "startWorkoutTracking serial " + str + " request " + pq1);
        if (pq1 == null || pq1.isRequiredGPS()) {
            u();
            n();
            boolean m = Jn5.b.m(PortfolioApp.get.instance(), Jn5.Ci.LOCATION_FINE);
            boolean m2 = Jn5.b.m(PortfolioApp.get.instance(), Jn5.Ci.LOCATION_SERVICE);
            this.d = true;
            if (m && m2) {
                q();
            }
            m();
            String j2 = j(m, m2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("WorkoutTetherGpsManager", "startWorkoutTracking watchErrorContent " + j2);
            if (j2.length() <= 0) {
                z = false;
            }
            if (z) {
                PortfolioApp instance = PortfolioApp.get.instance();
                if (pq1 != null) {
                    instance.g1(new StartWorkoutInfoData(pq1, j2, Dd0.ERROR), str);
                    return;
                }
                return;
            }
            PortfolioApp instance2 = PortfolioApp.get.instance();
            if (pq1 != null) {
                instance2.g1(new StartWorkoutInfoData(pq1, "", Dd0.SUCCESS), str);
                return;
            }
            return;
        }
        PortfolioApp.get.instance().g1(new StartWorkoutInfoData(pq1, "", Dd0.SUCCESS), str);
        s();
    }

    @DexIgnore
    public final void s() {
        FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "stopLocationObserver");
        this.a = null;
        FossilNotificationBar.c.d(PortfolioApp.get.instance());
        this.k.unObserverLocation(this.c, false);
    }

    @DexIgnore
    public final void t() {
        FLogger.INSTANCE.getLocal().d("WorkoutTetherGpsManager", "stopWorkoutTracking");
        this.d = false;
        s();
        u();
    }

    @DexIgnore
    public final void u() {
        try {
            PortfolioApp.get.instance().unregisterReceiver(this.i);
        } catch (Exception e2) {
        }
    }
}
