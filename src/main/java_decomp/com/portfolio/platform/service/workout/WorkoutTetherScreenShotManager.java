package com.portfolio.platform.service.workout;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Lifecycle;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hr7;
import com.fossil.Im7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.P47;
import com.fossil.U08;
import com.fossil.Uu7;
import com.fossil.Ux7;
import com.fossil.V37;
import com.fossil.W08;
import com.fossil.Xq0;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Fd;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Md;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint;
import com.portfolio.platform.data.model.diana.workout.WorkoutRouterGpsWrapper;
import com.portfolio.platform.data.model.diana.workout.WorkoutScreenshotWrapper;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import io.flutter.embedding.android.FlutterFragment;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.dart.DartExecutor;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.JSONMethodCodec;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.platform.PlatformViewsController;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutTetherScreenShotManager implements Fd {
    @DexIgnore
    public static /* final */ String v; // = (PortfolioApp.get.instance().getFilesDir() + File.separator + "%s");
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public U08 b; // = W08.b(false, 1, null);
    @DexIgnore
    public /* final */ LinkedBlockingDeque<WorkoutScreenshotWrapper> c; // = new LinkedBlockingDeque<>();
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ Uu7 e;
    @DexIgnore
    public /* final */ Il6 f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public FlutterEngine h;
    @DexIgnore
    public MethodChannel i;
    @DexIgnore
    public FragmentActivity j;
    @DexIgnore
    public FrameLayout k;
    @DexIgnore
    public FlutterFragment l;
    @DexIgnore
    public int m;
    @DexIgnore
    public /* final */ Gson s;
    @DexIgnore
    public /* final */ WorkoutSessionRepository t;
    @DexIgnore
    public /* final */ FileRepository u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return WorkoutTetherScreenShotManager.v;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.workout.WorkoutTetherScreenShotManager$enqueue$1", f = "WorkoutTetherScreenShotManager.kt", l = {206}, m = "invokeSuspend")
    public static final class b extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutScreenshotWrapper $job;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutTetherScreenShotManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WorkoutTetherScreenShotManager workoutTetherScreenShotManager, WorkoutScreenshotWrapper workoutScreenshotWrapper, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = workoutTetherScreenShotManager;
            this.$job = workoutScreenshotWrapper;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            b bVar = new b(this.this$0, this.$job, xe6);
            bVar.p$ = (Il6) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((b) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            U08 u08;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.c.add(this.$job);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", this.$job.getWorkoutId() + " added to mQueue");
                u08 = this.this$0.b;
                this.L$0 = il6;
                this.L$1 = u08;
                this.label = 1;
                if (u08.a(null, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                u08 = (U08) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            try {
                this.this$0.r();
                Cd6 cd6 = Cd6.a;
                u08.b(null);
                return Cd6.a;
            } catch (Throwable th) {
                u08.b(null);
                throw th;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutScreenshotWrapper $job$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ JSONObject $mock$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutTetherScreenShotManager this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements MethodChannel.Result {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(a aVar, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    a aVar = new a(this.this$0, xe6);
                    aVar.p$ = (Il6) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        WorkoutTetherScreenShotManager workoutTetherScreenShotManager = this.this$0.a.this$0;
                        workoutTetherScreenShotManager.m--;
                        this.this$0.a.this$0.d = null;
                        this.this$0.a.this$0.g = false;
                        this.this$0.a.this$0.r();
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Object $result;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, Object obj, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                    this.$result = obj;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    b bVar = new b(this.this$0, this.$result, xe6);
                    bVar.p$ = (Il6) obj;
                    throw null;
                    //return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((b) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Object obj2 = this.$result;
                        if (obj2 != null) {
                            String string = ((JSONObject) obj2).getString("imageData");
                            FileRepository fileRepository = this.this$0.a.this$0.u;
                            String workoutId = this.this$0.a.$job$inlined.getWorkoutId();
                            Hr7 hr7 = Hr7.a;
                            String format = String.format(WorkoutTetherScreenShotManager.w.a(), Arrays.copyOf(new Object[]{this.this$0.a.$job$inlined.getWorkoutId()}, 1));
                            Wg6.b(format, "java.lang.String.format(format, *args)");
                            String saveLocalDataFile = fileRepository.saveLocalDataFile(string, workoutId, format);
                            FLogger.INSTANCE.getLocal().d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.this$0.a.$job$inlined.getWorkoutId() + " success insert to DB, uri " + saveLocalDataFile);
                            WorkoutSessionRepository workoutSessionRepository = this.this$0.a.this$0.t;
                            String workoutId2 = this.this$0.a.$job$inlined.getWorkoutId();
                            this.L$0 = il6;
                            this.L$1 = string;
                            this.L$2 = saveLocalDataFile;
                            this.label = 1;
                            if (workoutSessionRepository.insertWorkoutTetherScreenShot(workoutId2, saveLocalDataFile, this) == d) {
                                return d;
                            }
                        } else {
                            throw new Rc6("null cannot be cast to non-null type org.json.JSONObject");
                        }
                    } else if (i == 1) {
                        String str = (String) this.L$2;
                        String str2 = (String) this.L$1;
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    WorkoutTetherScreenShotManager workoutTetherScreenShotManager = this.this$0.a.this$0;
                    workoutTetherScreenShotManager.m--;
                    this.this$0.a.this$0.d = null;
                    this.this$0.a.this$0.g = false;
                    this.this$0.a.this$0.r();
                    return Cd6.a;
                }
            }

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void error(String str, String str2, Object obj) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.a.$job$inlined.getWorkoutId() + " error " + str2 + ' ');
                Rm6 unused = Gu7.d(this.a.this$0.f, null, null, new a(this, null), 3, null);
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void notImplemented() {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.a.$job$inlined.getWorkoutId() + " notImplemented ");
                this.a.this$0.d = null;
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void success(Object obj) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.a.$job$inlined.getWorkoutId() + " success");
                Rm6 unused = Gu7.d(this.a.this$0.f, null, null, new b(this, obj, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(Xe6 xe6, WorkoutTetherScreenShotManager workoutTetherScreenShotManager, WorkoutScreenshotWrapper workoutScreenshotWrapper, JSONObject jSONObject) {
            super(2, xe6);
            this.this$0 = workoutTetherScreenShotManager;
            this.$job$inlined = workoutScreenshotWrapper;
            this.$mock$inlined = jSONObject;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            c cVar = new c(xe6, this.this$0, this.$job$inlined, this.$mock$inlined);
            cVar.p$ = (Il6) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((c) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                if (this.this$0.l == null || this.this$0.k == null) {
                    this.this$0.q();
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + this.$job$inlined.getWorkoutId() + " invoke flutter module to take screenshot");
                MethodChannel methodChannel = this.this$0.i;
                if (methodChannel != null) {
                    methodChannel.invokeMethod("takeSnapShot", this.$mock$inlined, new a(this));
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public WorkoutTetherScreenShotManager(WorkoutSessionRepository workoutSessionRepository, FileRepository fileRepository) {
        DartExecutor dartExecutor;
        BinaryMessenger binaryMessenger = null;
        Wg6.c(workoutSessionRepository, "mWorkoutSessionRepository");
        Wg6.c(fileRepository, "mFileRepository");
        this.t = workoutSessionRepository;
        this.u = fileRepository;
        Uu7 b2 = Ux7.b(null, 1, null);
        this.e = b2;
        this.f = Jv7.a(b2.plus(Bw7.b()));
        this.g = true;
        FlutterEngine b3 = V37.c.b();
        this.h = b3;
        if (!(b3 == null || (dartExecutor = b3.getDartExecutor()) == null)) {
            binaryMessenger = dartExecutor.getBinaryMessenger();
        }
        this.i = new MethodChannel(binaryMessenger, "detailTracking/screenShotMap", JSONMethodCodec.INSTANCE);
        this.s = new Gson();
    }

    @DexIgnore
    @Md(Lifecycle.a.ON_STOP)
    public final void cancel() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherScreenShotManager", "onActivity onStop currentTaskId " + this.d);
        this.c.clear();
        this.d = null;
        this.m = 0;
        s();
    }

    @DexIgnore
    public final void q() {
        FLogger.INSTANCE.getLocal().d("WorkoutTetherScreenShotManager", "attachPlaceHolderView");
        FragmentActivity fragmentActivity = this.j;
        if (fragmentActivity != null) {
            FlutterFragment flutterFragment = (FlutterFragment) fragmentActivity.getSupportFragmentManager().Z("TAG_FLUTTER_FRAGMENT");
            this.l = flutterFragment;
            if (flutterFragment == null || this.k == null) {
                FrameLayout frameLayout = new FrameLayout(fragmentActivity);
                this.k = frameLayout;
                if (frameLayout != null) {
                    frameLayout.setId(View.generateViewId());
                    ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams((int) P47.b(350.0f), (int) P47.b(200.0f));
                    marginLayoutParams.setMargins(0, Constants.BOLT_RSSI_ERROR, 0, 0);
                    FrameLayout frameLayout2 = this.k;
                    if (frameLayout2 != null) {
                        frameLayout2.setLayoutParams(new FrameLayout.LayoutParams(marginLayoutParams));
                        View findViewById = fragmentActivity.findViewById(2131362158);
                        if (findViewById != null) {
                            ((ViewGroup) findViewById).addView(this.k);
                            this.l = FlutterFragment.withCachedEngine("screenshot_engine_id").build();
                            Xq0 j2 = fragmentActivity.getSupportFragmentManager().j();
                            FrameLayout frameLayout3 = this.k;
                            if (frameLayout3 != null) {
                                int id = frameLayout3.getId();
                                FlutterFragment flutterFragment2 = this.l;
                                if (flutterFragment2 != null) {
                                    j2.b(id, flutterFragment2, "TAG_FLUTTER_FRAGMENT");
                                    j2.h();
                                    return;
                                }
                                throw new Rc6("null cannot be cast to non-null type androidx.fragment.app.Fragment");
                            }
                            Wg6.i();
                            throw null;
                        }
                        throw new Rc6("null cannot be cast to non-null type android.view.ViewGroup");
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void r() {
        synchronized (this) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutTetherScreenShotManager", "dequeue queueSize " + this.c.size() + " currentJobRunning " + this.m);
            if (this.c.size() > 0 && this.m == 0) {
                WorkoutScreenshotWrapper pop = this.c.pop();
                pop.setResetState(this.g);
                Wg6.b(pop, "jobToRun");
                u(pop);
            }
        }
    }

    @DexIgnore
    public final void s() {
        PlatformViewsController platformViewsController;
        FLogger.INSTANCE.getLocal().d("WorkoutTetherScreenShotManager", "detachPlaceHolderView");
        FragmentActivity fragmentActivity = this.j;
        if (fragmentActivity != null) {
            if (this.l != null) {
                Xq0 j2 = fragmentActivity.getSupportFragmentManager().j();
                FlutterFragment flutterFragment = this.l;
                if (flutterFragment != null) {
                    j2.q(flutterFragment);
                    j2.h();
                } else {
                    throw new Rc6("null cannot be cast to non-null type androidx.fragment.app.Fragment");
                }
            }
            if (this.k != null) {
                View findViewById = fragmentActivity.findViewById(2131362158);
                if (findViewById != null) {
                    ((ViewGroup) findViewById).removeView(this.k);
                } else {
                    throw new Rc6("null cannot be cast to non-null type android.view.ViewGroup");
                }
            }
            FlutterFragment flutterFragment2 = this.l;
            if (flutterFragment2 != null) {
                flutterFragment2.onDestroyView();
            }
            FlutterEngine flutterEngine = this.h;
            if (!(flutterEngine == null || (platformViewsController = flutterEngine.getPlatformViewsController()) == null)) {
                platformViewsController.detachFromView();
            }
        }
        this.j = null;
        this.l = null;
        this.k = null;
    }

    @DexIgnore
    public final void t(WorkoutScreenshotWrapper workoutScreenshotWrapper) {
        synchronized (this) {
            Wg6.c(workoutScreenshotWrapper, "job");
            if (Wg6.a(this.d, workoutScreenshotWrapper.getWorkoutId()) || this.c.contains(workoutScreenshotWrapper)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutTetherScreenShotManager", workoutScreenshotWrapper.getWorkoutId() + " is already added");
                return;
            }
            Rm6 unused = Gu7.d(this.f, null, null, new b(this, workoutScreenshotWrapper, null), 3, null);
        }
    }

    @DexIgnore
    public final void u(WorkoutScreenshotWrapper workoutScreenshotWrapper) {
        JSONObject jSONObject = new JSONObject(this.s.t(workoutScreenshotWrapper));
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutTetherScreenShotManager", "generateScreenShot of " + workoutScreenshotWrapper.getWorkoutId() + " resetState " + workoutScreenshotWrapper.getResetState());
        this.m = this.m + 1;
        this.d = workoutScreenshotWrapper.getWorkoutId();
        if (this.j != null) {
            Rm6 unused = Gu7.d(Jv7.a(Bw7.c()), null, null, new c(null, this, workoutScreenshotWrapper, jSONObject), 3, null);
        }
    }

    @DexIgnore
    public final String v(String str) {
        Wg6.c(str, "workoutId");
        Hr7 hr7 = Hr7.a;
        String format = String.format(v, Arrays.copyOf(new Object[]{str}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final void w(FragmentActivity fragmentActivity) {
        Wg6.c(fragmentActivity, Constants.ACTIVITY);
        this.j = fragmentActivity;
    }

    @DexIgnore
    public final WorkoutScreenshotWrapper x(WorkoutSession workoutSession) {
        ArrayList arrayList;
        Wg6.c(workoutSession, "workoutSession");
        String id = workoutSession.getId();
        int b2 = (int) P47.b(350.0f);
        int b3 = (int) P47.b(200.0f);
        List<WorkoutGpsPoint> workoutGpsPoints = workoutSession.getWorkoutGpsPoints();
        if (workoutGpsPoints != null) {
            arrayList = new ArrayList(Im7.m(workoutGpsPoints, 10));
            Iterator<T> it = workoutGpsPoints.iterator();
            while (it.hasNext()) {
                arrayList.add(new WorkoutRouterGpsWrapper(it.next()));
            }
        } else {
            arrayList = null;
        }
        return new WorkoutScreenshotWrapper(id, b2, b3, true, arrayList);
    }
}
