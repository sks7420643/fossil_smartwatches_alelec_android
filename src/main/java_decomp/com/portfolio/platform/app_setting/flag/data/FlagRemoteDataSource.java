package com.portfolio.platform.app_setting.flag.data;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Jf6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlagRemoteDataSource {
    @DexIgnore
    public /* final */ ApiServiceV2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource", f = "FlagRemoteDataSource.kt", l = {21}, m = "fetchFlags")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FlagRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(FlagRemoteDataSource flagRemoteDataSource, Xe6 xe6) {
            super(xe6);
            this.this$0 = flagRemoteDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource$fetchFlags$response$1", f = "FlagRemoteDataSource.kt", l = {21}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeSerial;
        @DexIgnore
        public /* final */ /* synthetic */ String $agent;
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ FlagRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(FlagRemoteDataSource flagRemoteDataSource, String str, String str2, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = flagRemoteDataSource;
            this.$activeSerial = str;
            this.$agent = str2;
            this.$jsonObject = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Bi(this.this$0, this.$activeSerial, this.$agent, this.$jsonObject, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Bi) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                String str = this.$activeSerial;
                String str2 = this.$agent;
                Ku3 ku3 = this.$jsonObject;
                this.label = 1;
                Object featureFlag = apiServiceV2.featureFlag(str, str2, ku3, this);
                return featureFlag == d ? d : featureFlag;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public FlagRemoteDataSource(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        this.a = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(java.lang.String r11, java.lang.String r12, java.lang.String[] r13, com.mapped.Xe6<? super com.mapped.Ap4<com.mapped.Ku3>> r14) {
        /*
            r10 = this;
            r9 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 0
            boolean r0 = r14 instanceof com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource.Ai
            if (r0 == 0) goto L_0x0053
            r0 = r14
            com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource$Ai r0 = (com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0053
            int r1 = r1 + r3
            r0.label = r1
            r6 = r0
        L_0x0015:
            java.lang.Object r1 = r6.result
            java.lang.Object r7 = com.fossil.Yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x0062
            if (r0 != r9) goto L_0x005a
            java.lang.Object r0 = r6.L$5
            com.mapped.Ku3 r0 = (com.mapped.Ku3) r0
            java.lang.Object r0 = r6.L$4
            com.mapped.Fu3 r0 = (com.mapped.Fu3) r0
            java.lang.Object r0 = r6.L$3
            java.lang.String[] r0 = (java.lang.String[]) r0
            java.lang.Object r0 = r6.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r6.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r6.L$0
            com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource r0 = (com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x003d:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x009e
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
        L_0x0052:
            return r1
        L_0x0053:
            com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource$Ai r0 = new com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource$Ai
            r0.<init>(r10, r14)
            r6 = r0
            goto L_0x0015
        L_0x005a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0062:
            com.fossil.El7.b(r1)
            com.mapped.Fu3 r8 = new com.mapped.Fu3
            r8.<init>()
            int r1 = r13.length
            r0 = 0
        L_0x006c:
            if (r0 >= r1) goto L_0x0076
            r2 = r13[r0]
            r8.l(r2)
            int r0 = r0 + 1
            goto L_0x006c
        L_0x0076:
            com.mapped.Ku3 r4 = new com.mapped.Ku3
            r4.<init>()
            java.lang.String r0 = "flags"
            r4.k(r0, r8)
            com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource$Bi r0 = new com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource$Bi
            r1 = r10
            r2 = r11
            r3 = r12
            r0.<init>(r1, r2, r3, r4, r5)
            r6.L$0 = r10
            r6.L$1 = r11
            r6.L$2 = r12
            r6.L$3 = r13
            r6.L$4 = r8
            r6.L$5 = r4
            r6.label = r9
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r0, r6)
            if (r0 != r7) goto L_0x003d
            r1 = r7
            goto L_0x0052
        L_0x009e:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00ba
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            com.fossil.Hq5 r1 = new com.fossil.Hq5
            int r2 = r0.a()
            com.portfolio.platform.data.model.ServerError r3 = r0.c()
            java.lang.Throwable r4 = r0.d()
            r7 = 24
            r6 = r5
            r8 = r5
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            goto L_0x0052
        L_0x00ba:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource.b(java.lang.String, java.lang.String, java.lang.String[], com.mapped.Xe6):java.lang.Object");
    }
}
