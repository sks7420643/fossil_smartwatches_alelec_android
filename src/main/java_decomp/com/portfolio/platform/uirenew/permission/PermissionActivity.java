package com.portfolio.platform.uirenew.permission;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.fossil.Ez6;
import com.fossil.Gz6;
import com.fossil.Jz6;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PermissionData;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PermissionActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public Jz6 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void b(a aVar, Context context, ArrayList arrayList, boolean z, boolean z2, Integer num, int i, Object obj) {
            boolean z3 = false;
            boolean z4 = (i & 4) != 0 ? false : z;
            if ((i & 8) == 0) {
                z3 = z2;
            }
            aVar.a(context, arrayList, z4, z3, (i & 16) != 0 ? null : num);
        }

        @DexIgnore
        public final void a(Context context, ArrayList<PermissionData> arrayList, boolean z, boolean z2, Integer num) {
            Wg6.c(context, "context");
            Wg6.c(arrayList, "listPermissions");
            Intent intent = new Intent(context, PermissionActivity.class);
            intent.putExtra("PERMISSION_SKIP_ABLE_FLAG", z);
            intent.putExtra("EXTRA_LIST_PERMISSIONS", arrayList);
            intent.setFlags(536870912);
            if (z2 || z) {
                int i = 111;
                if (num != null) {
                    i = num.intValue();
                }
                ((Activity) context).startActivityForResult(intent, i);
                return;
            }
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        if (Build.VERSION.SDK_INT < 26) {
            setRequestedOrientation(1);
        }
        boolean booleanExtra = getIntent().getBooleanExtra("PERMISSION_SKIP_ABLE_FLAG", false);
        ArrayList parcelableArrayListExtra = getIntent().getParcelableArrayListExtra("EXTRA_LIST_PERMISSIONS");
        if (parcelableArrayListExtra != null) {
            Ez6 ez6 = (Ez6) getSupportFragmentManager().Y(2131362158);
            if (booleanExtra) {
                if (ez6 == null) {
                    ez6 = Ez6.m.c(booleanExtra);
                    k(ez6, Ez6.m.a(), 2131362158);
                }
            } else if (ez6 == null) {
                ez6 = Ez6.m.b();
                k(ez6, Ez6.m.a(), 2131362158);
            }
            Iface iface = PortfolioApp.get.instance().getIface();
            if (ez6 != null) {
                iface.u(new Gz6(ez6, parcelableArrayListExtra)).a(this);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.permission.PermissionContract.View");
        }
        Wg6.i();
        throw null;
    }
}
