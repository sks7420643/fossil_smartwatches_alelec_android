package com.portfolio.platform.uirenew.onboarding.profilesetup;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import com.fossil.B47;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Hq5;
import com.fossil.Ko7;
import com.fossil.Kq5;
import com.fossil.M47;
import com.fossil.Qh5;
import com.fossil.Sl5;
import com.fossil.Um5;
import com.fossil.Vt7;
import com.fossil.Wr4;
import com.fossil.Yn7;
import com.fossil.Yw6;
import com.fossil.Zw6;
import com.mapped.An4;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jh6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.ui.user.information.domain.usecase.GetUser;
import com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase;
import com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.LocalDate;
import org.joda.time.Years;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileSetupPresenter extends Yw6 {
    @DexIgnore
    public static /* final */ String E;
    @DexIgnore
    public /* final */ DeviceRepository A;
    @DexIgnore
    public /* final */ ServerSettingRepository B;
    @DexIgnore
    public /* final */ FlagRepository C;
    @DexIgnore
    public /* final */ An4 D;
    @DexIgnore
    public SignUpEmailUseCase e;
    @DexIgnore
    public SignUpSocialUseCase f;
    @DexIgnore
    public GetUser g;
    @DexIgnore
    public AnalyticsHelper h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public MFUser j;
    @DexIgnore
    public String k; // = "";
    @DexIgnore
    public String l;
    @DexIgnore
    public String m;
    @DexIgnore
    public String n;
    @DexIgnore
    public String o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public Calendar u; // = Calendar.getInstance();
    @DexIgnore
    public SignUpSocialAuth v;
    @DexIgnore
    public SignUpEmailAuth w;
    @DexIgnore
    public /* final */ Zw6 x;
    @DexIgnore
    public /* final */ GetRecommendedGoalUseCase y;
    @DexIgnore
    public /* final */ UserRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $dataLocationSharingPrivacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpSocialAuth $it;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $privacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $termOfUseVersionLatest;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii implements ServerSettingDataSource.OnGetServerSettingList {
                @DexIgnore
                public /* final */ /* synthetic */ Aii a;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii implements CoroutineUseCase.Ei<SignUpSocialUseCase.Ci, SignUpSocialUseCase.Bi> {
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii a;

                    @DexIgnore
                    public Aiiii(Aiii aiii) {
                        this.a = aiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // com.portfolio.platform.CoroutineUseCase.Ei
                    public /* bridge */ /* synthetic */ void a(SignUpSocialUseCase.Bi bi) {
                        b(bi);
                    }

                    @DexIgnore
                    public void b(SignUpSocialUseCase.Bi bi) {
                        Wg6.c(bi, "errorValue");
                        this.a.a.this$0.this$0.x.h();
                        this.a.a.this$0.this$0.x.T5(bi.a(), bi.b());
                    }

                    @DexIgnore
                    public void c(SignUpSocialUseCase.Ci ci) {
                        Wg6.c(ci, "responseValue");
                        PortfolioApp.get.instance().getIface().c0(this.a.a.this$0.this$0);
                        Ai ai = this.a.a.this$0;
                        ai.this$0.Q(ai.$it.getService());
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // com.portfolio.platform.CoroutineUseCase.Ei
                    public /* bridge */ /* synthetic */ void onSuccess(SignUpSocialUseCase.Ci ci) {
                        c(ci);
                    }
                }

                @DexIgnore
                public Aiii(Aii aii) {
                    this.a = aii;
                }

                @DexIgnore
                @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
                public void onFailed(int i) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = ProfileSetupPresenter.E;
                    local.e(str, "getServerSettingList - onFailed. ErrorCode = " + i);
                    this.a.this$0.this$0.x.h();
                    this.a.this$0.this$0.x.T5(i, "");
                }

                @DexIgnore
                @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
                public void onSuccess(ServerSettingList serverSettingList) {
                    List<ServerSetting> serverSettings;
                    FLogger.INSTANCE.getLocal().d(ProfileSetupPresenter.E, "getServerSettingList - onSuccess");
                    if (!(serverSettingList == null || (serverSettings = serverSettingList.getServerSettings()) == null)) {
                        for (T t : serverSettings) {
                            if (t != null) {
                                if (Wg6.a(t.getObjectId(), "dataLocationSharingPrivacyVersionLatest")) {
                                    this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                                if (Wg6.a(t.getObjectId(), "privacyVersionLatest")) {
                                    this.a.this$0.$privacyVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                                if (Wg6.a(t.getObjectId(), "tosVersionLatest")) {
                                    this.a.this$0.$termOfUseVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                            }
                        }
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = ProfileSetupPresenter.E;
                    local.d(str, "dataLocationSharingPrivacyVersionLatest=" + ((String) this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element) + " privacyVersionLatest=" + ((String) this.a.this$0.$privacyVersionLatest.element) + " termOfUseVersionLatest=" + ((String) this.a.this$0.$termOfUseVersionLatest.element));
                    ArrayList<String> arrayList = new ArrayList<>();
                    if (this.a.this$0.this$0.t) {
                        arrayList.add(this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element);
                    }
                    this.a.this$0.$it.setAcceptedLocationDataSharing(arrayList);
                    ArrayList<String> arrayList2 = new ArrayList<>();
                    if (this.a.this$0.this$0.s) {
                        arrayList2.add(this.a.this$0.$privacyVersionLatest.element);
                    }
                    this.a.this$0.$it.setAcceptedPrivacies(arrayList2);
                    ArrayList<String> arrayList3 = new ArrayList<>();
                    if (this.a.this$0.this$0.r) {
                        arrayList3.add(this.a.this$0.$termOfUseVersionLatest.element);
                    }
                    this.a.this$0.$it.setAcceptedTermsOfService(arrayList3);
                    this.a.this$0.this$0.W().e(new SignUpSocialUseCase.Ai(this.a.this$0.$it), new Aiiii(this));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.B.getServerSettingList(new Aiii(this));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Jh6 jh6, Jh6 jh62, Jh6 jh63, SignUpSocialAuth signUpSocialAuth, Xe6 xe6, ProfileSetupPresenter profileSetupPresenter) {
            super(2, xe6);
            this.$dataLocationSharingPrivacyVersionLatest = jh6;
            this.$privacyVersionLatest = jh62;
            this.$termOfUseVersionLatest = jh63;
            this.$it = signUpSocialAuth;
            this.this$0 = profileSetupPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.$dataLocationSharingPrivacyVersionLatest, this.$privacyVersionLatest, this.$termOfUseVersionLatest, this.$it, xe6, this.this$0);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 h = this.this$0.h();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(h, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $dataLocationSharingPrivacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpEmailAuth $it;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $privacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $termOfUseVersionLatest;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii implements ServerSettingDataSource.OnGetServerSettingList {
                @DexIgnore
                public /* final */ /* synthetic */ Aii a;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii implements CoroutineUseCase.Ei<SignUpEmailUseCase.Ci, SignUpEmailUseCase.Bi> {
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii a;

                    @DexIgnore
                    public Aiiii(Aiii aiii) {
                        this.a = aiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // com.portfolio.platform.CoroutineUseCase.Ei
                    public /* bridge */ /* synthetic */ void a(SignUpEmailUseCase.Bi bi) {
                        b(bi);
                    }

                    @DexIgnore
                    public void b(SignUpEmailUseCase.Bi bi) {
                        Wg6.c(bi, "errorValue");
                        this.a.a.this$0.this$0.x.h();
                        this.a.a.this$0.this$0.x.T5(bi.a(), bi.b());
                    }

                    @DexIgnore
                    public void c(SignUpEmailUseCase.Ci ci) {
                        Wg6.c(ci, "responseValue");
                        PortfolioApp.get.instance().getIface().c0(this.a.a.this$0.this$0);
                        ProfileSetupPresenter profileSetupPresenter = this.a.a.this$0.this$0;
                        String lowerCase = "Email".toLowerCase();
                        Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                        profileSetupPresenter.Q(lowerCase);
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // com.portfolio.platform.CoroutineUseCase.Ei
                    public /* bridge */ /* synthetic */ void onSuccess(SignUpEmailUseCase.Ci ci) {
                        c(ci);
                    }
                }

                @DexIgnore
                public Aiii(Aii aii) {
                    this.a = aii;
                }

                @DexIgnore
                @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
                public void onFailed(int i) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = ProfileSetupPresenter.E;
                    local.e(str, "getServerSettingList - onFailed. ErrorCode = " + i);
                    this.a.this$0.this$0.x.h();
                    this.a.this$0.this$0.x.T5(i, "");
                }

                @DexIgnore
                @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
                public void onSuccess(ServerSettingList serverSettingList) {
                    List<ServerSetting> serverSettings;
                    FLogger.INSTANCE.getLocal().d(ProfileSetupPresenter.E, "getServerSettingList - onSuccess");
                    if (!(serverSettingList == null || (serverSettings = serverSettingList.getServerSettings()) == null)) {
                        for (T t : serverSettings) {
                            if (t != null) {
                                if (Wg6.a(t.getObjectId(), "dataLocationSharingPrivacyVersionLatest")) {
                                    this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                                if (Wg6.a(t.getObjectId(), "privacyVersionLatest")) {
                                    this.a.this$0.$privacyVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                                if (Wg6.a(t.getObjectId(), "tosVersionLatest")) {
                                    this.a.this$0.$termOfUseVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                            }
                        }
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = ProfileSetupPresenter.E;
                    local.d(str, "dataLocationSharingPrivacyVersionLatest=" + ((String) this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element) + " privacyVersionLatest=" + ((String) this.a.this$0.$privacyVersionLatest.element) + " termOfUseVersionLatest=" + ((String) this.a.this$0.$termOfUseVersionLatest.element));
                    ArrayList<String> arrayList = new ArrayList<>();
                    if (this.a.this$0.this$0.t) {
                        arrayList.add(this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element);
                    }
                    this.a.this$0.$it.setAcceptedLocationDataSharing(arrayList);
                    ArrayList<String> arrayList2 = new ArrayList<>();
                    if (this.a.this$0.this$0.s) {
                        arrayList2.add(this.a.this$0.$privacyVersionLatest.element);
                    }
                    this.a.this$0.$it.setAcceptedPrivacies(arrayList2);
                    ArrayList<String> arrayList3 = new ArrayList<>();
                    if (this.a.this$0.this$0.r) {
                        arrayList3.add(this.a.this$0.$termOfUseVersionLatest.element);
                    }
                    this.a.this$0.$it.setAcceptedTermsOfService(arrayList3);
                    this.a.this$0.this$0.V().e(new SignUpEmailUseCase.Ai(this.a.this$0.$it), new Aiiii(this));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.B.getServerSettingList(new Aiii(this));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Jh6 jh6, Jh6 jh62, Jh6 jh63, SignUpEmailAuth signUpEmailAuth, Xe6 xe6, ProfileSetupPresenter profileSetupPresenter) {
            super(2, xe6);
            this.$dataLocationSharingPrivacyVersionLatest = jh6;
            this.$privacyVersionLatest = jh62;
            this.$termOfUseVersionLatest = jh63;
            this.$it = signUpEmailAuth;
            this.this$0 = profileSetupPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.$dataLocationSharingPrivacyVersionLatest, this.$privacyVersionLatest, this.$termOfUseVersionLatest, this.$it, xe6, this.this$0);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 h = this.this$0.h();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(h, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1", f = "ProfileSetupPresenter.kt", l = {MFNetworkReturnCode.ITEM_NAME_IN_USED, 419, 427}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $service;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1$1", f = "ProfileSetupPresenter.kt", l = {410, 411, 416, 417}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<UserSettings>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ap4<UserSettings>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:16:0x00af  */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00d2  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x0109  */
            /* JADX WARNING: Removed duplicated region for block: B:25:0x010c  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r13) {
                /*
                // Method dump skipped, instructions count: 274
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter.Ci.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1$currentUser$1", f = "ProfileSetupPresenter.kt", l = {419}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.z;
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii implements CoroutineUseCase.Ei<GetRecommendedGoalUseCase.Ci, GetRecommendedGoalUseCase.Ai> {
            @DexIgnore
            public /* final */ /* synthetic */ Ci a;

            @DexIgnore
            public Cii(Ci ci) {
                this.a = ci;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void a(GetRecommendedGoalUseCase.Ai ai) {
                b(ai);
            }

            @DexIgnore
            public void b(GetRecommendedGoalUseCase.Ai ai) {
                Wg6.c(ai, "errorValue");
                Ci ci = this.a;
                ci.this$0.g0(ci.$service);
            }

            @DexIgnore
            public void c(GetRecommendedGoalUseCase.Ci ci) {
                Wg6.c(ci, "responseValue");
                Ci ci2 = this.a;
                ci2.this$0.g0(ci2.$service);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void onSuccess(GetRecommendedGoalUseCase.Ci ci) {
                c(ci);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(ProfileSetupPresenter profileSetupPresenter, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileSetupPresenter;
            this.$service = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$service, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0062  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00cc  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00f0  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
            // Method dump skipped, instructions count: 243
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3", f = "ProfileSetupPresenter.kt", l = {157}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3$1", f = "ProfileSetupPresenter.kt", l = {157}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.z;
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(ProfileSetupPresenter profileSetupPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileSetupPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            ProfileSetupPresenter profileSetupPresenter;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ProfileSetupPresenter profileSetupPresenter2 = this.this$0;
                Dv7 i2 = profileSetupPresenter2.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.L$1 = profileSetupPresenter2;
                this.label = 1;
                g = Eu7.g(i2, aii, this);
                if (g == d) {
                    return d;
                }
                profileSetupPresenter = profileSetupPresenter2;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                profileSetupPresenter = (ProfileSetupPresenter) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            profileSetupPresenter.j = (MFUser) g;
            MFUser mFUser = this.this$0.j;
            if (mFUser != null) {
                this.this$0.x.n2(mFUser);
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$1", f = "ProfileSetupPresenter.kt", l = {578}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$1$response$1", f = "ProfileSetupPresenter.kt", l = {578}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<? extends MFUser>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ap4<? extends MFUser>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.z;
                    MFUser mFUser = this.this$0.$user;
                    this.L$0 = il6;
                    this.label = 1;
                    Object updateUser = userRepository.updateUser(mFUser, true, this);
                    return updateUser == d ? d : updateUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(ProfileSetupPresenter profileSetupPresenter, MFUser mFUser, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileSetupPresenter;
            this.$user = mFUser;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$user, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            String str;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.x.c6();
                this.this$0.x.i();
                Dv7 h = this.this$0.h();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(h, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Ap4 ap4 = (Ap4) g;
            if (ap4 instanceof Kq5) {
                FLogger.INSTANCE.getLocal().d(ProfileSetupPresenter.E, "updateAccount successfully");
                PortfolioApp.get.instance().getIface().c0(this.this$0);
                ProfileSetupPresenter profileSetupPresenter = this.this$0;
                String authType = this.$user.getAuthType();
                if (authType != null) {
                    profileSetupPresenter.Q(authType);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else if (ap4 instanceof Hq5) {
                FLogger.INSTANCE.getLocal().d(ProfileSetupPresenter.E, "updateAccount failed");
                Zw6 zw6 = this.this$0.x;
                Hq5 hq5 = (Hq5) ap4;
                int a2 = hq5.a();
                ServerError c = hq5.c();
                if (c == null || (str = c.getMessage()) == null) {
                    str = "";
                }
                zw6.T5(a2, str);
                this.this$0.x.h();
                this.this$0.x.k3();
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = ProfileSetupPresenter.class.getSimpleName();
        Wg6.b(simpleName, "ProfileSetupPresenter::class.java.simpleName");
        E = simpleName;
    }
    */

    @DexIgnore
    public ProfileSetupPresenter(Zw6 zw6, GetRecommendedGoalUseCase getRecommendedGoalUseCase, UserRepository userRepository, DeviceRepository deviceRepository, ServerSettingRepository serverSettingRepository, FlagRepository flagRepository, An4 an4) {
        Wg6.c(zw6, "mView");
        Wg6.c(getRecommendedGoalUseCase, "mGetRecommendedGoalUseCase");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(serverSettingRepository, "mServerSettingRepository");
        Wg6.c(flagRepository, "flagRepository");
        Wg6.c(an4, "shared");
        this.x = zw6;
        this.y = getRecommendedGoalUseCase;
        this.z = userRepository;
        this.A = deviceRepository;
        this.B = serverSettingRepository;
        this.C = flagRepository;
        this.D = an4;
    }

    @DexIgnore
    @Override // com.fossil.Yw6
    public void A() {
        Bundle bundle = new Bundle();
        Calendar instance = Calendar.getInstance();
        bundle.putInt("DAY", instance.get(5));
        bundle.putInt("MONTH", instance.get(2) + 1);
        bundle.putInt("YEAR", instance.get(1) - 32);
        this.x.z0(bundle);
    }

    @DexIgnore
    public final Rm6 Q(String str) {
        Wg6.c(str, Constants.SERVICE);
        return Gu7.d(k(), null, null, new Ci(this, str, null), 3, null);
    }

    @DexIgnore
    public SignUpEmailAuth R() {
        return this.w;
    }

    @DexIgnore
    public final List<String> S() {
        ArrayList arrayList = new ArrayList();
        if (this.p) {
            arrayList.add("Agree terms of use and privacy");
        }
        if (this.q) {
            arrayList.add("Allow Gather data usage");
        }
        if (this.r) {
            arrayList.add("Agree term of use");
        }
        if (this.s) {
            arrayList.add("Agree privacy");
        }
        if (this.t) {
            arrayList.add("Allow location data processing");
        }
        return arrayList;
    }

    @DexIgnore
    public final String T() {
        String a2 = M47.a(M47.Ci.PRIVACY, null);
        String str = Um5.c(PortfolioApp.get.instance(), 2131886962).toString();
        Wg6.b(a2, "privacyPolicyUrl");
        return Vt7.q(str, "privacy_policy", a2, false, 4, null);
    }

    @DexIgnore
    public final AnalyticsHelper U() {
        AnalyticsHelper analyticsHelper = this.h;
        if (analyticsHelper != null) {
            return analyticsHelper;
        }
        Wg6.n("mAnalyticsHelper");
        throw null;
    }

    @DexIgnore
    public final SignUpEmailUseCase V() {
        SignUpEmailUseCase signUpEmailUseCase = this.e;
        if (signUpEmailUseCase != null) {
            return signUpEmailUseCase;
        }
        Wg6.n("mSignUpEmailUseCase");
        throw null;
    }

    @DexIgnore
    public final SignUpSocialUseCase W() {
        SignUpSocialUseCase signUpSocialUseCase = this.f;
        if (signUpSocialUseCase != null) {
            return signUpSocialUseCase;
        }
        Wg6.n("mSignUpSocialUseCase");
        throw null;
    }

    @DexIgnore
    public final String X() {
        String a2 = M47.a(M47.Ci.PRIVACY, null);
        String str = Um5.c(PortfolioApp.get.instance(), 2131886963).toString();
        Wg6.b(a2, "privacyPolicyUrl");
        return Vt7.q(str, "privacy_policy", a2, false, 4, null);
    }

    @DexIgnore
    public SignUpSocialAuth Y() {
        return this.v;
    }

    @DexIgnore
    public final String Z() {
        String a2 = M47.a(M47.Ci.TERMS, null);
        String a3 = M47.a(M47.Ci.PRIVACY, null);
        String str = Um5.c(PortfolioApp.get.instance(), 2131886977).toString();
        Wg6.b(a2, "termOfUseUrl");
        String q2 = Vt7.q(str, "term_of_use_url", a2, false, 4, null);
        Wg6.b(a3, "privacyPolicyUrl");
        return Vt7.q(q2, "privacy_policy", a3, false, 4, null);
    }

    @DexIgnore
    public final String a0() {
        String a2 = M47.a(M47.Ci.TERMS, null);
        String str = Um5.c(PortfolioApp.get.instance(), 2131886964).toString();
        Wg6.b(a2, "termOfUseUrl");
        return Vt7.q(str, "term_of_use_url", a2, false, 4, null);
    }

    @DexIgnore
    public final boolean b0() {
        Calendar calendar = this.u;
        if (calendar == null) {
            return false;
        }
        if (calendar != null) {
            Years yearsBetween = Years.yearsBetween(LocalDate.fromCalendarFields(calendar), LocalDate.now());
            Wg6.b(yearsBetween, "age");
            return yearsBetween.getYears() >= 16;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final boolean c0() {
        if (this.k.length() == 0) {
            this.x.n0(false, false, "");
            return false;
        } else if (!B47.a(this.k)) {
            Zw6 zw6 = this.x;
            String c = Um5.c(PortfolioApp.get.instance(), 2131887009);
            Wg6.b(c, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            zw6.n0(false, true, c);
            return false;
        } else {
            this.x.n0(true, false, "");
            return true;
        }
    }

    @DexIgnore
    public final boolean d0() {
        String str = this.l;
        if (!(str == null || Vt7.l(str))) {
            this.x.L3(true);
            return true;
        }
        this.x.L3(false);
        return false;
    }

    @DexIgnore
    public final boolean e0() {
        String str = this.m;
        if (!(str == null || Vt7.l(str))) {
            this.x.p3(true);
            return true;
        }
        this.x.p3(false);
        return false;
    }

    @DexIgnore
    public boolean f0() {
        return this.i;
    }

    @DexIgnore
    public final void g0(String str) {
        Wg6.c(str, Constants.SERVICE);
        Sl5 b = AnalyticsHelper.f.b("user_signup");
        String lowerCase = "Source".toLowerCase();
        Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
        b.a(lowerCase, str);
        b.b();
        this.x.h();
        this.x.Q1();
    }

    @DexIgnore
    public void h0(SignUpEmailAuth signUpEmailAuth) {
        Wg6.c(signUpEmailAuth, "auth");
        this.w = signUpEmailAuth;
    }

    @DexIgnore
    public void i0(boolean z2) {
        this.i = z2;
    }

    @DexIgnore
    public void j0(SignUpSocialAuth signUpSocialAuth) {
        Wg6.c(signUpSocialAuth, "auth");
        this.v = signUpSocialAuth;
    }

    @DexIgnore
    public void k0() {
        this.x.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Zw6 zw6 = this.x;
        Spanned fromHtml = Html.fromHtml(Z());
        Wg6.b(fromHtml, "Html.fromHtml(getTermsOf\u2026AndPrivacyPolicyString())");
        zw6.w0(fromHtml);
        Zw6 zw62 = this.x;
        Spanned fromHtml2 = Html.fromHtml(a0());
        Wg6.b(fromHtml2, "Html.fromHtml(getTermsOfUseString())");
        zw62.H4(fromHtml2);
        Zw6 zw63 = this.x;
        Spanned fromHtml3 = Html.fromHtml(X());
        Wg6.b(fromHtml3, "Html.fromHtml(getPrivacyPolicyString())");
        zw63.Y2(fromHtml3);
        Zw6 zw64 = this.x;
        Spanned fromHtml4 = Html.fromHtml(T());
        Wg6.b(fromHtml4, "Html.fromHtml(getLocationDataString())");
        zw64.Q3(fromHtml4);
        this.x.f();
        if (!this.i) {
            SignUpSocialAuth signUpSocialAuth = this.v;
            if (signUpSocialAuth != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = E;
                local.d(str, "start - mSocialAuth=" + signUpSocialAuth);
                this.x.e3(signUpSocialAuth);
            }
            SignUpEmailAuth signUpEmailAuth = this.w;
            if (signUpEmailAuth != null) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = E;
                local2.d(str2, "start - mEmailAuth=" + signUpEmailAuth);
                this.x.I3(signUpEmailAuth);
            }
        } else if (this.j == null) {
            Rm6 unused = Gu7.d(k(), null, null, new Di(this, null), 3, null);
        }
    }

    @DexIgnore
    public final void l0(MFUser mFUser) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = E;
        local.d(str, "updateAccount - userId = " + mFUser.getUserId());
        Rm6 unused = Gu7.d(k(), null, null, new Ei(this, mFUser, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
    }

    @DexIgnore
    public final void m0() {
        if (!c0()) {
            this.x.c6();
        } else if (!d0()) {
            this.x.c6();
        } else if (!e0()) {
            this.x.c6();
        } else if (!b0()) {
            this.x.c6();
        } else if (!Wr4.a.a().m(S())) {
            this.x.c6();
        } else {
            this.x.k3();
        }
    }

    @DexIgnore
    @Override // com.fossil.Yw6
    public void n() {
        if (this.i) {
            MFUser mFUser = this.j;
            if (mFUser != null) {
                mFUser.setEmail(this.k);
                String str = this.l;
                if (str != null) {
                    mFUser.setFirstName(str);
                    String str2 = this.m;
                    if (str2 != null) {
                        mFUser.setLastName(str2);
                        String str3 = this.o;
                        if (str3 != null) {
                            mFUser.setBirthday(str3);
                            mFUser.setDiagnosticEnabled(this.q);
                            String str4 = this.n;
                            if (str4 == null) {
                                str4 = Qh5.OTHER.toString();
                            }
                            mFUser.setGender(str4);
                            l0(mFUser);
                            return;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str5 = E;
        local.d(str5, "create account socialAuth=" + this.v + " emailAuth=" + this.w);
        this.x.i();
        SignUpSocialAuth signUpSocialAuth = this.v;
        if (signUpSocialAuth != null) {
            signUpSocialAuth.setEmail(this.k);
            String str6 = this.n;
            if (str6 == null) {
                str6 = Qh5.OTHER.toString();
            }
            signUpSocialAuth.setGender(str6);
            String str7 = this.o;
            if (str7 != null) {
                signUpSocialAuth.setBirthday(str7);
                signUpSocialAuth.setClientId(AppHelper.g.a(""));
                String str8 = this.l;
                if (str8 != null) {
                    signUpSocialAuth.setFirstName(str8);
                    String str9 = this.m;
                    if (str9 != null) {
                        signUpSocialAuth.setLastName(str9);
                        signUpSocialAuth.setDiagnosticEnabled(this.q);
                        Jh6 jh6 = new Jh6();
                        jh6.element = "";
                        Jh6 jh62 = new Jh6();
                        jh62.element = "";
                        Jh6 jh63 = new Jh6();
                        jh63.element = "";
                        Rm6 unused = Gu7.d(k(), null, null, new Ai(jh6, jh62, jh63, signUpSocialAuth, null, this), 3, null);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
        SignUpEmailAuth signUpEmailAuth = this.w;
        if (signUpEmailAuth != null) {
            String str10 = this.n;
            if (str10 == null) {
                str10 = Qh5.OTHER.toString();
            }
            signUpEmailAuth.setGender(str10);
            String str11 = this.o;
            if (str11 != null) {
                signUpEmailAuth.setBirthday(str11);
                signUpEmailAuth.setClientId(AppHelper.g.a(""));
                String str12 = this.l;
                if (str12 != null) {
                    signUpEmailAuth.setFirstName(str12);
                    String str13 = this.m;
                    if (str13 != null) {
                        signUpEmailAuth.setLastName(str13);
                        signUpEmailAuth.setDiagnosticEnabled(this.q);
                        Jh6 jh64 = new Jh6();
                        jh64.element = "";
                        Jh6 jh65 = new Jh6();
                        jh65.element = "";
                        Jh6 jh66 = new Jh6();
                        jh66.element = "";
                        Rm6 unused2 = Gu7.d(k(), null, null, new Bi(jh64, jh65, jh66, signUpEmailAuth, null, this), 3, null);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Yw6
    public Calendar o() {
        Calendar calendar = this.u;
        Wg6.b(calendar, "mBirthdayCalendar");
        return calendar;
    }

    @DexIgnore
    @Override // com.fossil.Yw6
    public void p(Date date, Calendar calendar) {
        Wg6.c(date, "data");
        Wg6.c(calendar, "calendar");
        FLogger.INSTANCE.getLocal().d(E, "onBirthDayChanged");
        this.o = TimeUtils.k(date);
        this.u = calendar;
        Zw6 zw6 = this.x;
        String i2 = TimeUtils.i(date);
        Wg6.b(i2, "DateHelper.formatLocalDateMonth(data)");
        zw6.Z5(i2);
        if (!b0()) {
            Zw6 zw62 = this.x;
            String c = Um5.c(PortfolioApp.get.instance(), 2131886965);
            Wg6.b(c, "LanguageHelper.getString\u2026Text__YouCantUseTheAppIf)");
            zw62.K4(false, c);
        } else {
            this.x.K4(true, "");
        }
        m0();
    }

    @DexIgnore
    @Override // com.fossil.Yw6
    public void q(boolean z2) {
        this.q = z2;
        m0();
    }

    @DexIgnore
    @Override // com.fossil.Yw6
    public void r(String str) {
        Wg6.c(str, Constants.EMAIL);
        this.k = str;
        c0();
        m0();
    }

    @DexIgnore
    @Override // com.fossil.Yw6
    public void s(String str) {
        Wg6.c(str, Constants.PROFILE_KEY_FIRST_NAME);
        this.l = str;
        d0();
        m0();
    }

    @DexIgnore
    @Override // com.fossil.Yw6
    public void t(Qh5 qh5) {
        Wg6.c(qh5, "gender");
        this.n = qh5.toString();
        this.x.p4(qh5);
    }

    @DexIgnore
    @Override // com.fossil.Yw6
    public void u(String str) {
        Wg6.c(str, Constants.PROFILE_KEY_LAST_NAME);
        this.m = str;
        e0();
        m0();
    }

    @DexIgnore
    @Override // com.fossil.Yw6
    public void v(boolean z2) {
        this.t = z2;
        m0();
    }

    @DexIgnore
    @Override // com.fossil.Yw6
    public void w(boolean z2) {
        this.s = z2;
        m0();
    }

    @DexIgnore
    @Override // com.fossil.Yw6
    public void x(boolean z2) {
        this.r = z2;
        m0();
    }

    @DexIgnore
    @Override // com.fossil.Yw6
    public void y(boolean z2) {
        this.p = z2;
        m0();
    }

    @DexIgnore
    @Override // com.fossil.Yw6
    public void z() {
        Bundle bundle = new Bundle();
        bundle.putInt("DAY", this.u.get(5));
        bundle.putInt("MONTH", this.u.get(2) + 1);
        bundle.putInt("YEAR", this.u.get(1));
        this.x.z0(bundle);
    }
}
