package com.portfolio.platform.uirenew.onboarding.ota;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.fossil.Ao7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Pw6;
import com.fossil.Qw6;
import com.fossil.Ul5;
import com.fossil.Um5;
import com.fossil.Vt7;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.util.DeviceUtils;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UpdateFirmwarePresenter extends Pw6 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ Ai r; // = new Ai(null);
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public boolean f;
    @DexIgnore
    public Device g;
    @DexIgnore
    public Ul5 h;
    @DexIgnore
    public /* final */ Ei i; // = new Ei(this);
    @DexIgnore
    public /* final */ Di j; // = new Di(this);
    @DexIgnore
    public /* final */ Qw6 k;
    @DexIgnore
    public /* final */ DeviceRepository l;
    @DexIgnore
    public /* final */ Cj4 m;
    @DexIgnore
    public /* final */ An4 n;
    @DexIgnore
    public /* final */ UpdateFirmwareUsecase o;
    @DexIgnore
    public /* final */ DownloadFirmwareByDeviceModelUsecase p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return UpdateFirmwarePresenter.q;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ei<DownloadFirmwareByDeviceModelUsecase.Ci, DownloadFirmwareByDeviceModelUsecase.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ Device c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1", f = "UpdateFirmwarePresenter.kt", l = {214, 217}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ DownloadFirmwareByDeviceModelUsecase.Ci $responseValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1$isDianaEV1$1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        return Ao7.a(DeviceUtils.h.a().i(this.this$0.this$0.b));
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$checkFirmware$1$onSuccess$1$isLatestFw$1", f = "UpdateFirmwarePresenter.kt", l = {217}, m = "invokeSuspend")
            public static final class Biii extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Biii biii = new Biii(this.this$0, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
                    //return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
                    throw null;
                    //return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        DeviceUtils a2 = DeviceUtils.h.a();
                        Bi bi = this.this$0.this$0;
                        String str = bi.b;
                        Device device = bi.c;
                        this.L$0 = il6;
                        this.label = 1;
                        Object j = a2.j(str, device, this);
                        return j == d ? d : j;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, DownloadFirmwareByDeviceModelUsecase.Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$responseValue = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$responseValue, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:14:0x004f  */
            /* JADX WARNING: Removed duplicated region for block: B:21:0x00be  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r11) {
                /*
                    r10 = this;
                    r9 = 0
                    r8 = 2
                    r7 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r10.label
                    if (r0 == 0) goto L_0x006c
                    if (r0 == r7) goto L_0x0039
                    if (r0 != r8) goto L_0x0031
                    java.lang.Object r0 = r10.L$1
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r0 = r10.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r11)
                    r0 = r11
                L_0x001b:
                    java.lang.Boolean r0 = (java.lang.Boolean) r0
                    boolean r0 = r0.booleanValue()
                    if (r0 == 0) goto L_0x00b3
                    com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$Bi r0 = r10.this$0
                    com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter r0 = r0.a
                    com.fossil.Qw6 r0 = r0.L()
                    r0.t3()
                L_0x002e:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0030:
                    return r0
                L_0x0031:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0039:
                    java.lang.Object r0 = r10.L$1
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r1 = r10.L$0
                    com.mapped.Il6 r1 = (com.mapped.Il6) r1
                    com.fossil.El7.b(r11)
                    r3 = r0
                    r2 = r11
                L_0x0046:
                    r0 = r2
                    java.lang.Boolean r0 = (java.lang.Boolean) r0
                    boolean r0 = r0.booleanValue()
                    if (r0 != 0) goto L_0x00be
                    com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$Bi r2 = r10.this$0
                    com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter r2 = r2.a
                    com.fossil.Dv7 r2 = com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter.w(r2)
                    com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$Bi$Aii$Biii r5 = new com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$Bi$Aii$Biii
                    r5.<init>(r10, r9)
                    r10.L$0 = r1
                    r10.L$1 = r3
                    r10.Z$0 = r0
                    r10.label = r8
                    java.lang.Object r0 = com.fossil.Eu7.g(r2, r5, r10)
                    if (r0 != r4) goto L_0x001b
                    r0 = r4
                    goto L_0x0030
                L_0x006c:
                    com.fossil.El7.b(r11)
                    com.mapped.Il6 r1 = r10.p$
                    com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase$Ci r0 = r10.$responseValue
                    java.lang.String r0 = r0.a()
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$Ai r3 = com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter.r
                    java.lang.String r3 = r3.a()
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "checkFirmware - downloadFw SUCCESS, latestFwVersion="
                    r5.append(r6)
                    r5.append(r0)
                    java.lang.String r5 = r5.toString()
                    r2.d(r3, r5)
                    com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$Bi r2 = r10.this$0
                    com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter r2 = r2.a
                    com.fossil.Dv7 r2 = com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter.w(r2)
                    com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$Bi$Aii$Aiii r3 = new com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$Bi$Aii$Aiii
                    r3.<init>(r10, r9)
                    r10.L$0 = r1
                    r10.L$1 = r0
                    r10.label = r7
                    java.lang.Object r2 = com.fossil.Eu7.g(r2, r3, r10)
                    if (r2 != r4) goto L_0x00e5
                    r0 = r4
                    goto L_0x0030
                L_0x00b3:
                    com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$Bi r0 = r10.this$0
                    com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter r1 = r0.a
                    com.portfolio.platform.data.model.Device r0 = r0.c
                    r1.N(r0)
                    goto L_0x002e
                L_0x00be:
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$Ai r1 = com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter.r
                    java.lang.String r1 = r1.a()
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r4 = "checkFirmware - downloadFw SUCCESS, latestFwVersion="
                    r2.append(r4)
                    r2.append(r3)
                    java.lang.String r3 = " but device is DianaEV1!!!"
                    r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    r0.e(r1, r2)
                    goto L_0x002e
                L_0x00e5:
                    r3 = r0
                    goto L_0x0046
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter.Bi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Bi(UpdateFirmwarePresenter updateFirmwarePresenter, String str, Device device) {
            this.a = updateFirmwarePresenter;
            this.b = str;
            this.c = device;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(DownloadFirmwareByDeviceModelUsecase.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(DownloadFirmwareByDeviceModelUsecase.Bi bi) {
            Wg6.c(bi, "errorValue");
            FLogger.INSTANCE.getLocal().e(UpdateFirmwarePresenter.r.a(), "checkFirmware - downloadFw FAILED!!!");
            this.a.L().O1();
        }

        @DexIgnore
        public void c(DownloadFirmwareByDeviceModelUsecase.Ci ci) {
            Wg6.c(ci, "responseValue");
            Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, ci, null), 3, null);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(DownloadFirmwareByDeviceModelUsecase.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Device $device$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceId;
        @DexIgnore
        public /* final */ /* synthetic */ String $lastFwTemp;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Device>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Device> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.K().getDeviceBySerial(this.this$0.$deviceId);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(String str, String str2, Xe6 xe6, UpdateFirmwarePresenter updateFirmwarePresenter, Device device) {
            super(2, xe6);
            this.$deviceId = str;
            this.$lastFwTemp = str2;
            this.this$0 = updateFirmwarePresenter;
            this.$device$inlined = device;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.$deviceId, this.$lastFwTemp, xe6, this.this$0, this.$device$inlined);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 h = this.this$0.h();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(h, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Device device = (Device) g;
            FLogger.INSTANCE.getLocal().d(UpdateFirmwarePresenter.r.a(), "checkLastOTASuccess - getDeviceBySerial SUCCESS");
            if (device != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = UpdateFirmwarePresenter.r.a();
                local.d(a2, "checkLastOTASuccess - currentDeviceFw=" + device.getFirmwareRevision());
                if (!Vt7.j(this.$lastFwTemp, device.getFirmwareRevision(), true)) {
                    FLogger.INSTANCE.getLocal().d(UpdateFirmwarePresenter.r.a(), "Handle OTA complete on check last OTA success");
                    this.this$0.n.B0(this.$deviceId);
                    this.this$0.L().t3();
                } else {
                    this.this$0.H(this.$device$inlined);
                }
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements BleCommandResultManager.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di(UpdateFirmwarePresenter updateFirmwarePresenter) {
            this.a = updateFirmwarePresenter;
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            boolean booleanExtra = intent.getBooleanExtra("OTA_RESULT", false);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.r.a();
            local.d(a2, "otaCompleteReceiver - isSuccess=" + booleanExtra);
            this.a.L().A(booleanExtra);
            this.a.n.B0(PortfolioApp.get.instance().J());
            if (booleanExtra) {
                FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.a.e, UpdateFirmwarePresenter.r.a(), "[Sync Start] AUTO SYNC after OTA");
                PortfolioApp.get.instance().S1(this.a.m, false, 13);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei(UpdateFirmwarePresenter updateFirmwarePresenter) {
            this.a = updateFirmwarePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            if (otaEvent != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = UpdateFirmwarePresenter.r.a();
                local.d(a2, "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
                if (!TextUtils.isEmpty(otaEvent.getSerial()) && Vt7.j(otaEvent.getSerial(), PortfolioApp.get.instance().J(), true)) {
                    this.a.L().Z((int) (otaEvent.getProcess() * ((float) 10)));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1", f = "UpdateFirmwarePresenter.kt", l = {106}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1$1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Device>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $activeSerial;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, String str, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
                this.$activeSerial = str;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$activeSerial, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Device> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.K().getDeviceBySerial(this.$activeSerial);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(UpdateFirmwarePresenter updateFirmwarePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = updateFirmwarePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            String J;
            Object g;
            UpdateFirmwarePresenter updateFirmwarePresenter;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                J = PortfolioApp.get.instance().J();
                UpdateFirmwarePresenter updateFirmwarePresenter2 = this.this$0;
                Dv7 h = updateFirmwarePresenter2.h();
                Aii aii = new Aii(this, J, null);
                this.L$0 = il6;
                this.L$1 = J;
                this.L$2 = updateFirmwarePresenter2;
                this.label = 1;
                g = Eu7.g(h, aii, this);
                if (g == d) {
                    return d;
                }
                updateFirmwarePresenter = updateFirmwarePresenter2;
            } else if (i == 1) {
                J = (String) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                updateFirmwarePresenter = (UpdateFirmwarePresenter) this.L$2;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            updateFirmwarePresenter.g = (Device) g;
            boolean v0 = PortfolioApp.get.instance().v0();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.r.a();
            local.d(a2, "start - activeSerial=" + J + ", isDeviceOtaing=" + v0);
            if (!v0) {
                UpdateFirmwarePresenter updateFirmwarePresenter3 = this.this$0;
                updateFirmwarePresenter3.I(updateFirmwarePresenter3.g);
            }
            this.this$0.J();
            Ul5 c = AnalyticsHelper.f.c("ota_session");
            this.this$0.h = c;
            AnalyticsHelper.f.a("ota_session", c);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1", f = "UpdateFirmwarePresenter.kt", l = {248}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1$1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends String, ? extends String>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Gi gi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = gi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends String, ? extends String>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return DeviceUtils.h.a().f(this.this$0.$activeSerial, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements CoroutineUseCase.Ei<UpdateFirmwareUsecase.Di, UpdateFirmwareUsecase.Ci> {
            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void a(UpdateFirmwareUsecase.Ci ci) {
                b(ci);
            }

            @DexIgnore
            public void b(UpdateFirmwareUsecase.Ci ci) {
                Wg6.c(ci, "errorValue");
            }

            @DexIgnore
            public void c(UpdateFirmwareUsecase.Di di) {
                Wg6.c(di, "responseValue");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void onSuccess(UpdateFirmwareUsecase.Di di) {
                c(di);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(UpdateFirmwarePresenter updateFirmwarePresenter, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = updateFirmwarePresenter;
            this.$activeSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.this$0, this.$activeSerial, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Ul5 ul5;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 h = this.this$0.h();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(h, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Lc6 lc6 = (Lc6) g;
            String str = (String) lc6.component1();
            String str2 = (String) lc6.component2();
            if (!(str == null || str2 == null || (ul5 = this.this$0.h) == null)) {
                ul5.b("old_firmware", str);
                if (ul5 != null) {
                    ul5.b("new_firmware", str2);
                    if (ul5 != null) {
                        ul5.i();
                    }
                }
            }
            this.this$0.o.e(new UpdateFirmwareUsecase.Bi(this.$activeSerial, false, 2, null), new Bii());
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = UpdateFirmwarePresenter.class.getSimpleName();
        Wg6.b(simpleName, "UpdateFirmwarePresenter::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public UpdateFirmwarePresenter(Qw6 qw6, DeviceRepository deviceRepository, UserRepository userRepository, Cj4 cj4, An4 an4, UpdateFirmwareUsecase updateFirmwareUsecase, DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase) {
        Wg6.c(qw6, "mView");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(cj4, "mDeviceSettingFactory");
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(updateFirmwareUsecase, "mUpdateFirmwareUseCase");
        Wg6.c(downloadFirmwareByDeviceModelUsecase, "mDownloadFwByDeviceModel");
        this.k = qw6;
        this.l = deviceRepository;
        this.m = cj4;
        this.n = an4;
        this.o = updateFirmwareUsecase;
        this.p = downloadFirmwareByDeviceModelUsecase;
    }

    @DexIgnore
    public final void H(Device device) {
        String deviceId = device.getDeviceId();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "checkFirmware - deviceId=" + deviceId + ", sku=" + device.getSku() + ", fw=" + device.getFirmwareRevision());
        if (PortfolioApp.get.instance().z0() || !this.n.y0()) {
            DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase = this.p;
            String sku = device.getSku();
            if (sku == null) {
                sku = "";
            }
            downloadFirmwareByDeviceModelUsecase.e(new DownloadFirmwareByDeviceModelUsecase.Ai(sku), new Bi(this, deviceId, device));
            return;
        }
        this.k.t3();
    }

    @DexIgnore
    public final void I(Device device) {
        if (device != null) {
            String deviceId = device.getDeviceId();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = q;
            local.d(str, "checkLastOTASuccess - deviceId=" + deviceId + ", sku=" + device.getSku());
            if (TextUtils.isEmpty(deviceId)) {
                FLogger.INSTANCE.getLocal().e(q, "checkLastOTASuccess - DEVICE ID IS EMPTY!!!");
                return;
            }
            String Q = this.n.Q(deviceId);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = q;
            local2.d(str2, "checkLastOTASuccess - lastTempFw=" + Q);
            if (TextUtils.isEmpty(Q)) {
                H(device);
            } else {
                Rm6 unused = Gu7.d(k(), null, null, new Ci(deviceId, Q, null, this, device), 3, null);
            }
        }
    }

    @DexIgnore
    public final void J() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "serial=" + this.e + ", mCurrentDeviceType=" + deviceBySerial);
        if (DeviceHelper.o.w(deviceBySerial)) {
            explore.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886916));
            explore.setBackground(2131231353);
            explore2.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886917));
            explore2.setBackground(2131231351);
            explore3.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886914));
            explore3.setBackground(2131231354);
            explore4.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886915));
            explore4.setBackground(2131231350);
        } else {
            explore.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886923));
            explore.setBackground(2131231353);
            explore2.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886922));
            explore2.setBackground(2131231355);
            explore3.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886920));
            explore3.setBackground(2131231354);
            explore4.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886921));
            explore4.setBackground(2131231352);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.k.X(arrayList);
    }

    @DexIgnore
    public final DeviceRepository K() {
        return this.l;
    }

    @DexIgnore
    public final Qw6 L() {
        return this.k;
    }

    @DexIgnore
    public void M() {
        this.k.M5(this);
    }

    @DexIgnore
    public final void N(Device device) {
        Wg6.c(device, "Device");
        String deviceId = device.getDeviceId();
        boolean v0 = PortfolioApp.get.instance().v0();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "updateFirmware - activeSerial=" + deviceId + ", isDeviceOtaing=" + v0);
        if (!v0) {
            Rm6 unused = Gu7.d(k(), null, null, new Gi(this, deviceId, null), 3, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "start - mIsOnBoardingFlow=" + this.f);
        if (!this.f) {
            this.k.K0();
        }
        BleCommandResultManager.d.e(this.j, CommunicateMode.OTA);
        PortfolioApp instance = PortfolioApp.get.instance();
        Ei ei = this.i;
        instance.registerReceiver(ei, new IntentFilter(PortfolioApp.get.instance().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        this.k.f();
        BleCommandResultManager.d.g(CommunicateMode.OTA);
        Rm6 unused = Gu7.d(k(), null, null, new Fi(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        try {
            PortfolioApp.get.instance().unregisterReceiver(this.i);
            BleCommandResultManager.d.j(this.j, CommunicateMode.OTA);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = q;
            local.e(str, "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Pw6
    public void n() {
        BleCommandResultManager.d.g(CommunicateMode.OTA);
    }

    @DexIgnore
    @Override // com.fossil.Pw6
    public void o() {
        FLogger.INSTANCE.getLocal().d(q, "checkFwAgain");
        I(this.g);
    }

    @DexIgnore
    @Override // com.fossil.Pw6
    public void p(boolean z, String str) {
        Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        this.f = z;
        this.e = str;
    }

    @DexIgnore
    @Override // com.fossil.Pw6
    public boolean q() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.Pw6
    public void r() {
        if (DeviceHelper.o.x(PortfolioApp.get.instance().J())) {
            this.k.d0();
        } else {
            this.k.l();
        }
    }

    @DexIgnore
    @Override // com.fossil.Pw6
    public void s() {
        FLogger.INSTANCE.getLocal().d(q, "tryOtaAgain");
        Device device = this.g;
        if (device != null) {
            N(device);
        }
        this.k.V2();
    }
}
