package com.portfolio.platform.uirenew.onboarding.exploreWatch;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.Rv6;
import com.fossil.Tv5;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ExploreWatchActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public ExploreWatchPresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, boolean z) {
            Wg6.c(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ExploreWatchActivity", "start - isOnboarding=" + z);
            Intent intent = new Intent(context, ExploreWatchActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.portfolio.platform.ui.BaseActivity
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        boolean z = false;
        super.onCreate(bundle);
        setContentView(2131558439);
        Tv5 tv5 = (Tv5) getSupportFragmentManager().Y(2131362158);
        if (getIntent() != null) {
            z = getIntent().getBooleanExtra("IS_ONBOARDING_FLOW", false);
        }
        if (tv5 == null) {
            tv5 = Tv5.t.a(z);
            k(tv5, "ExploreWatchFragment", 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (tv5 != null) {
            iface.n1(new Rv6(tv5)).a(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchContract.View");
    }
}
