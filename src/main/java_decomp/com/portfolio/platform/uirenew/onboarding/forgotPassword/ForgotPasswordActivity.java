package com.portfolio.platform.uirenew.onboarding.forgotPassword;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import com.fossil.Aw6;
import com.fossil.Cw6;
import com.fossil.Uv5;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ForgotPasswordActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public Cw6 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            Wg6.c(context, "context");
            Wg6.c(str, Constants.EMAIL);
            Intent intent = new Intent(context, ForgotPasswordActivity.class);
            intent.putExtra("EMAIL", str);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        Intent intent = getIntent();
        String stringExtra = intent != null ? intent.getStringExtra("EMAIL") : null;
        Uv5 uv5 = (Uv5) getSupportFragmentManager().Y(2131362158);
        if (uv5 == null) {
            uv5 = Uv5.k.b();
            k(uv5, Uv5.k.a(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (uv5 != null) {
            iface.r0(new Aw6(uv5)).a(this);
            if (bundle == null) {
                Cw6 cw6 = this.A;
                if (cw6 != null) {
                    if (stringExtra == null) {
                        stringExtra = "";
                    }
                    cw6.p(stringExtra);
                    return;
                }
                Wg6.n("mPresenter");
                throw null;
            } else if (bundle.containsKey("EMAIL")) {
                Cw6 cw62 = this.A;
                if (cw62 != null) {
                    String string = bundle.getString("EMAIL");
                    if (string == null) {
                        string = "";
                    }
                    cw62.p(string);
                    return;
                }
                Wg6.n("mPresenter");
                throw null;
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordContract.View");
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle, PersistableBundle persistableBundle) {
        if (bundle != null) {
            Cw6 cw6 = this.A;
            if (cw6 != null) {
                cw6.s("EMAIL", bundle);
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
        super.onSaveInstanceState(bundle, persistableBundle);
    }
}
