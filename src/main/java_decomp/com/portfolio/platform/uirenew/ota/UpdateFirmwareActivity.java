package com.portfolio.platform.uirenew.ota;

import android.content.Context;
import android.content.Intent;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UpdateFirmwareActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ String B;
    @DexIgnore
    public static /* final */ a C; // = new a(null);
    @DexIgnore
    public UpdateFirmwarePresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return UpdateFirmwareActivity.B;
        }

        @DexIgnore
        public final void b(Context context, String str, boolean z) {
            Wg6.c(context, "context");
            Wg6.c(str, "serial");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = a();
            local.d(a2, "start isOnboarding=" + z + " serial " + str);
            Intent intent = new Intent(context, UpdateFirmwareActivity.class);
            intent.putExtra("IS_ONBOARDING_FLOW", z);
            intent.putExtra("SERIAL", str);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = UpdateFirmwareActivity.class.getSimpleName();
        if (simpleName != null) {
            Wg6.b(simpleName, "UpdateFirmwareActivity::class.java.simpleName!!");
            B = simpleName;
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.portfolio.platform.ui.BaseActivity
    public void onBackPressed() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x002b, code lost:
        if (r2 != null) goto L_0x002d;
     */
    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            r5 = 2131362158(0x7f0a016e, float:1.8344089E38)
            r1 = 0
            super.onCreate(r7)
            r0 = 2131558439(0x7f0d0027, float:1.8742194E38)
            r6.setContentView(r0)
            androidx.fragment.app.FragmentManager r0 = r6.getSupportFragmentManager()
            androidx.fragment.app.Fragment r0 = r0.Y(r5)
            com.fossil.Ox6 r0 = (com.fossil.Ox6) r0
            android.content.Intent r2 = r6.getIntent()
            java.lang.String r3 = ""
            if (r2 == 0) goto L_0x004f
            java.lang.String r4 = "IS_ONBOARDING_FLOW"
            boolean r1 = r2.getBooleanExtra(r4, r1)
            java.lang.String r4 = "SERIAL"
            java.lang.String r2 = r2.getStringExtra(r4)
            if (r2 == 0) goto L_0x004f
        L_0x002d:
            if (r0 != 0) goto L_0x0038
            com.fossil.Ox6$Ai r0 = com.fossil.Ox6.v
            com.fossil.Ox6 r0 = r0.a(r1, r2)
            r6.i(r0, r5)
        L_0x0038:
            com.portfolio.platform.PortfolioApp$inner r1 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r1 = r1.instance()
            com.mapped.Iface r1 = r1.getIface()
            com.fossil.Rw6 r2 = new com.fossil.Rw6
            r2.<init>(r0)
            com.fossil.Ow6 r0 = r1.L0(r2)
            r0.a(r6)
            return
        L_0x004f:
            r2 = r3
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity.onCreate(android.os.Bundle):void");
    }
}
