package com.portfolio.platform.uirenew.alarm.usecase;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Dj5;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Ul5;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAlarms extends CoroutineUseCase<Ci, Di, Bi> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ Ai j; // = new Ai(null);
    @DexIgnore
    public Ci d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ Ei f; // = new Ei();
    @DexIgnore
    public /* final */ PortfolioApp g;
    @DexIgnore
    public /* final */ AlarmsRepository h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return SetAlarms.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ Alarm a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public Bi(Alarm alarm, int i, ArrayList<Integer> arrayList) {
            Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            Wg6.c(arrayList, "mBLEErrorCodes");
            this.a = alarm;
            this.b = i;
            this.c = arrayList;
        }

        @DexIgnore
        public final Alarm a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ List<Alarm> b;
        @DexIgnore
        public /* final */ Alarm c;

        @DexIgnore
        public Ci(String str, List<Alarm> list, Alarm alarm) {
            Wg6.c(str, "deviceId");
            Wg6.c(list, "alarms");
            Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.a = str;
            this.b = list;
            this.c = alarm;
        }

        @DexIgnore
        public final Alarm a() {
            return this.c;
        }

        @DexIgnore
        public final List<Alarm> b() {
            return this.b;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ Alarm a;

        @DexIgnore
        public Di(Alarm alarm) {
            Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.a = alarm;
        }

        @DexIgnore
        public final Alarm a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ei implements BleCommandResultManager.Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.alarm.usecase.SetAlarms$SetAlarmsBroadcastReceiver$receive$1", f = "SetAlarms.kt", l = {38, 43, 45, 58}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Intent $intent;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Intent intent, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
                this.$intent = intent;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$intent, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:19:0x0060  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x00ab  */
            /* JADX WARNING: Removed duplicated region for block: B:35:0x0166  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r21) {
                /*
                // Method dump skipped, instructions count: 464
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.Ei.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            FLogger.INSTANCE.getLocal().d(SetAlarms.j.a(), "onReceive");
            if (communicateMode == CommunicateMode.SET_LIST_ALARM && SetAlarms.this.p()) {
                SetAlarms.this.v(false);
                Rm6 unused = Gu7.d(SetAlarms.this.g(), null, null, new Aii(this, intent, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.alarm.usecase.SetAlarms", f = "SetAlarms.kt", l = {96, 100}, m = "run")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ SetAlarms this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(SetAlarms setAlarms, Xe6 xe6) {
            super(xe6);
            this.this$0 = setAlarms;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.t(null, this);
        }
    }

    /*
    static {
        String simpleName = SetAlarms.class.getSimpleName();
        Wg6.b(simpleName, "SetAlarms::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public SetAlarms(PortfolioApp portfolioApp, AlarmsRepository alarmsRepository) {
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(alarmsRepository, "mAlarmsRepository");
        this.g = portfolioApp;
        this.h = alarmsRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return i;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ci ci, Xe6 xe6) {
        return t(ci, xe6);
    }

    @DexIgnore
    public final boolean p() {
        return this.e;
    }

    @DexIgnore
    public final Ci q() {
        Ci ci = this.d;
        if (ci != null) {
            return ci;
        }
        Wg6.n("mRequestValues");
        throw null;
    }

    @DexIgnore
    public final void r(Intent intent, Alarm alarm) {
        Wg6.c(intent, "intent");
        Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "processBLEErrorCodes() - errorCode=" + intExtra);
        ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
        if (integerArrayListExtra == null) {
            integerArrayListExtra = new ArrayList<>(intExtra);
        }
        Ul5 f2 = AnalyticsHelper.f.f("set_alarms");
        if (f2 != null) {
            f2.c(String.valueOf(intExtra));
        }
        AnalyticsHelper.f.k("set_alarms");
        i(new Bi(alarm, intExtra, integerArrayListExtra));
    }

    @DexIgnore
    public final void s() {
        BleCommandResultManager.d.e(this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object t(com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.Ci r11, com.mapped.Xe6<java.lang.Object> r12) {
        /*
        // Method dump skipped, instructions count: 241
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.alarm.usecase.SetAlarms.t(com.portfolio.platform.uirenew.alarm.usecase.SetAlarms$Ci, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void u(Ci ci) {
        List<Alarm> b = ci.b();
        ArrayList arrayList = new ArrayList();
        for (T t : b) {
            if (t.isActive()) {
                arrayList.add(t);
            }
        }
        this.g.o1(ci.c(), Dj5.a(arrayList));
    }

    @DexIgnore
    public final void v(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public final void w() {
        BleCommandResultManager.d.j(this.f, CommunicateMode.SET_LIST_ALARM);
    }
}
