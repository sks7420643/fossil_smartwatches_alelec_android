package com.portfolio.platform.uirenew.alarm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.Qx5;
import com.mapped.AlarmFragment;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public AlarmPresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str, ArrayList<Alarm> arrayList, Alarm alarm) {
            Wg6.c(context, "context");
            Wg6.c(str, "deviceId");
            Wg6.c(arrayList, "currentAlarms");
            Intent intent = new Intent(context, AlarmActivity.class);
            intent.putExtra("EXTRA_DEVICE_ID", str);
            intent.putExtra("EXTRA_ALARM", alarm);
            intent.putParcelableArrayListExtra("EXTRA_ALARMS", arrayList);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.portfolio.platform.ui.BaseActivity
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        String stringExtra = getIntent().getStringExtra("EXTRA_DEVICE_ID");
        Alarm alarm = (Alarm) getIntent().getParcelableExtra("EXTRA_ALARM");
        ArrayList parcelableArrayListExtra = getIntent().getParcelableArrayListExtra("EXTRA_ALARMS");
        AlarmFragment alarmFragment = (AlarmFragment) getSupportFragmentManager().Y(2131362158);
        if (alarmFragment == null) {
            alarmFragment = AlarmFragment.s.b();
            k(alarmFragment, AlarmFragment.s.a(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (alarmFragment != null) {
            Wg6.b(stringExtra, "deviceId");
            Wg6.b(parcelableArrayListExtra, "alarms");
            iface.L1(new Qx5(alarmFragment, stringExtra, parcelableArrayListExtra, alarm)).a(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.alarm.AlarmContract.View");
    }
}
