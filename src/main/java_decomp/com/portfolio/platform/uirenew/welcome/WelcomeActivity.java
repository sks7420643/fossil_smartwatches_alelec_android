package com.portfolio.platform.uirenew.welcome;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.H27;
import com.fossil.J27;
import com.mapped.Qg6;
import com.mapped.ServiceUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WelcomeActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public WelcomePresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            Wg6.c(context, "context");
            context.startActivity(new Intent(context, WelcomeActivity.class));
        }

        @DexIgnore
        public final void b(Context context) {
            Wg6.c(context, "context");
            Intent intent = new Intent(context, WelcomeActivity.class);
            intent.addFlags(268468224);
            context.startActivity(intent);
        }

        @DexIgnore
        public final void c(Context context) {
            Wg6.c(context, "context");
            Intent intent = new Intent(context, WelcomeActivity.class);
            intent.addFlags(268468224);
            intent.putExtra("KEY_DIANA_REQUIRE", true);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        H27 h27 = (H27) getSupportFragmentManager().Y(2131362158);
        if (h27 == null) {
            h27 = (!getIntent().hasExtra("KEY_DIANA_REQUIRE") || !getIntent().getBooleanExtra("KEY_DIANA_REQUIRE", false)) ? new H27() : H27.k.a();
            k(h27, "WelcomeFragment", 2131362158);
        }
        PortfolioApp.get.instance().getIface().w1(new J27(h27)).a(this);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onStart() {
        super.onStart();
        FLogger.INSTANCE.getLocal().e(r(), "Service Tracking - startForegroundService in WelcomeActivity");
        ServiceUtils.a.d(this, MFDeviceService.class, Constants.STOP_FOREGROUND_ACTION);
        ServiceUtils.a.d(this, ButtonService.class, Constants.STOP_FOREGROUND_ACTION);
    }
}
