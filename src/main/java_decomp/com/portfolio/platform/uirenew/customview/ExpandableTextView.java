package com.portfolio.platform.uirenew.customview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import androidx.appcompat.widget.AppCompatTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ExpandableTextView extends AppCompatTextView {
    @DexIgnore
    public /* final */ List<c> f;
    @DexIgnore
    public TimeInterpolator g;
    @DexIgnore
    public TimeInterpolator h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public int l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            ExpandableTextView.this.setHeight(((Integer) valueAnimator.getAnimatedValue()).intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends AnimatorListenerAdapter {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ExpandableTextView.this.j();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(ExpandableTextView expandableTextView);
    }

    @DexIgnore
    public ExpandableTextView(Context context) {
        this(context, null);
    }

    @DexIgnore
    public ExpandableTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public ExpandableTextView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.i = getMaxLines();
        this.f = new ArrayList();
        this.g = new AccelerateDecelerateInterpolator();
        this.h = new AccelerateDecelerateInterpolator();
    }

    @DexIgnore
    public TimeInterpolator getCollapseInterpolator() {
        return this.h;
    }

    @DexIgnore
    public TimeInterpolator getExpandInterpolator() {
        return this.g;
    }

    @DexIgnore
    public boolean h() {
        if (this.k || this.j || this.i < 0) {
            return false;
        }
        i();
        measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
        this.l = getMeasuredHeight();
        this.j = true;
        setMaxLines(Integer.MAX_VALUE);
        measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
        ValueAnimator ofInt = ValueAnimator.ofInt(this.l, getMeasuredHeight());
        ofInt.addUpdateListener(new a());
        ofInt.addListener(new b());
        ofInt.setInterpolator(this.g);
        ofInt.setDuration(750L).start();
        return true;
    }

    @DexIgnore
    public final void i() {
        for (c cVar : this.f) {
            cVar.a(this);
        }
    }

    @DexIgnore
    public void j() {
        setMaxHeight(Integer.MAX_VALUE);
        setMinHeight(0);
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.height = -2;
        setLayoutParams(layoutParams);
        this.k = true;
        this.j = false;
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatTextView
    public void onMeasure(int i2, int i3) {
        if (this.i == 0 && !this.k && !this.j) {
            i3 = View.MeasureSpec.makeMeasureSpec(0, 1073741824);
        }
        super.onMeasure(i2, i3);
    }

    @DexIgnore
    public void setCollapseInterpolator(TimeInterpolator timeInterpolator) {
        this.h = timeInterpolator;
    }

    @DexIgnore
    public void setExpandInterpolator(TimeInterpolator timeInterpolator) {
        this.g = timeInterpolator;
    }

    @DexIgnore
    public void setInterpolator(TimeInterpolator timeInterpolator) {
        this.g = timeInterpolator;
        this.h = timeInterpolator;
    }
}
