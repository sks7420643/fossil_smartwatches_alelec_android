package com.portfolio.platform.uirenew.customview;

import android.content.Context;
import android.text.Spannable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.At4;
import com.fossil.F57;
import com.fossil.Hm7;
import com.fossil.Hr7;
import com.fossil.Hz4;
import com.fossil.Jl5;
import com.fossil.Tj5;
import com.fossil.Ty4;
import com.fossil.Um5;
import com.fossil.Uy4;
import com.mapped.Wg6;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FriendsInView extends ConstraintLayout {
    @DexIgnore
    public ImageView A;
    @DexIgnore
    public FlexibleTextView B;
    @DexIgnore
    public F57.Bi C;
    @DexIgnore
    public Uy4 D;
    @DexIgnore
    public ImageView w;
    @DexIgnore
    public ImageView x;
    @DexIgnore
    public ImageView y;
    @DexIgnore
    public ImageView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FriendsInView(Context context) {
        super(context);
        Wg6.c(context, "context");
        G(context, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FriendsInView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attributeSet");
        G(context, attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FriendsInView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attributeSet");
        G(context, attributeSet);
    }

    @DexIgnore
    public final void G(Context context, AttributeSet attributeSet) {
        F57.Bi f = F57.a().f();
        Wg6.b(f, "TextDrawable.builder().round()");
        this.C = f;
        this.D = Uy4.d.b();
        View inflate = View.inflate(context, 2131558646, this);
        View findViewById = inflate.findViewById(2131362618);
        Wg6.b(findViewById, "view.findViewById(R.id.img1)");
        this.w = (ImageView) findViewById;
        View findViewById2 = inflate.findViewById(2131362619);
        Wg6.b(findViewById2, "view.findViewById(R.id.img2)");
        this.x = (ImageView) findViewById2;
        View findViewById3 = inflate.findViewById(2131362620);
        Wg6.b(findViewById3, "view.findViewById(R.id.img3)");
        this.y = (ImageView) findViewById3;
        View findViewById4 = inflate.findViewById(2131362621);
        Wg6.b(findViewById4, "view.findViewById(R.id.img4)");
        this.z = (ImageView) findViewById4;
        View findViewById5 = inflate.findViewById(2131362622);
        Wg6.b(findViewById5, "view.findViewById(R.id.img5)");
        this.A = (ImageView) findViewById5;
        View findViewById6 = inflate.findViewById(2131362406);
        Wg6.b(findViewById6, "view.findViewById(R.id.ftv_description)");
        this.B = (FlexibleTextView) findViewById6;
    }

    @DexIgnore
    public final void setData(List<At4> list) {
        String str;
        String e;
        String str2;
        Wg6.c(list, "friendsIn");
        if (list.isEmpty()) {
            setVisibility(8);
            return;
        }
        Wg6.b(Tj5.a(getContext()), "GlideApp.with(context)");
        int size = list.size();
        Iterator<T> it = list.iterator();
        int i = 0;
        while (true) {
            boolean hasNext = it.hasNext();
            String str3 = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
            if (hasNext) {
                T next = it.next();
                if (i >= 0) {
                    T t = next;
                    Hz4 hz4 = Hz4.a;
                    String b = t != null ? t.b() : null;
                    String d = t != null ? t.d() : null;
                    if (t == null || (str2 = t.e()) == null) {
                        str2 = str3;
                    }
                    String a2 = hz4.a(b, d, str2);
                    if (i == 0) {
                        ImageView imageView = this.w;
                        if (imageView != null) {
                            imageView.setVisibility(0);
                            ImageView imageView2 = this.w;
                            if (imageView2 != null) {
                                String a3 = t != null ? t.a() : null;
                                F57.Bi bi = this.C;
                                if (bi != null) {
                                    Uy4 uy4 = this.D;
                                    if (uy4 != null) {
                                        Ty4.b(imageView2, a3, a2, bi, uy4);
                                    } else {
                                        Wg6.n("colorGenerator");
                                        throw null;
                                    }
                                } else {
                                    Wg6.n("drawableBuilder");
                                    throw null;
                                }
                            } else {
                                Wg6.n("img1");
                                throw null;
                            }
                        } else {
                            Wg6.n("img1");
                            throw null;
                        }
                    } else if (i == 1) {
                        ImageView imageView3 = this.x;
                        if (imageView3 != null) {
                            imageView3.setVisibility(0);
                            ImageView imageView4 = this.x;
                            if (imageView4 != null) {
                                String a4 = t != null ? t.a() : null;
                                F57.Bi bi2 = this.C;
                                if (bi2 != null) {
                                    Uy4 uy42 = this.D;
                                    if (uy42 != null) {
                                        Ty4.b(imageView4, a4, a2, bi2, uy42);
                                    } else {
                                        Wg6.n("colorGenerator");
                                        throw null;
                                    }
                                } else {
                                    Wg6.n("drawableBuilder");
                                    throw null;
                                }
                            } else {
                                Wg6.n("img2");
                                throw null;
                            }
                        } else {
                            Wg6.n("img2");
                            throw null;
                        }
                    } else if (i == 2) {
                        ImageView imageView5 = this.y;
                        if (imageView5 != null) {
                            imageView5.setVisibility(0);
                            ImageView imageView6 = this.y;
                            if (imageView6 != null) {
                                String a5 = t != null ? t.a() : null;
                                F57.Bi bi3 = this.C;
                                if (bi3 != null) {
                                    Uy4 uy43 = this.D;
                                    if (uy43 != null) {
                                        Ty4.b(imageView6, a5, a2, bi3, uy43);
                                    } else {
                                        Wg6.n("colorGenerator");
                                        throw null;
                                    }
                                } else {
                                    Wg6.n("drawableBuilder");
                                    throw null;
                                }
                            } else {
                                Wg6.n("img3");
                                throw null;
                            }
                        } else {
                            Wg6.n("img3");
                            throw null;
                        }
                    } else if (i == 3) {
                        ImageView imageView7 = this.z;
                        if (imageView7 != null) {
                            imageView7.setVisibility(0);
                            ImageView imageView8 = this.z;
                            if (imageView8 != null) {
                                String a6 = t != null ? t.a() : null;
                                F57.Bi bi4 = this.C;
                                if (bi4 != null) {
                                    Uy4 uy44 = this.D;
                                    if (uy44 != null) {
                                        Ty4.b(imageView8, a6, a2, bi4, uy44);
                                    } else {
                                        Wg6.n("colorGenerator");
                                        throw null;
                                    }
                                } else {
                                    Wg6.n("drawableBuilder");
                                    throw null;
                                }
                            } else {
                                Wg6.n("img4");
                                throw null;
                            }
                        } else {
                            Wg6.n("img4");
                            throw null;
                        }
                    } else {
                        continue;
                    }
                    i++;
                } else {
                    Hm7.l();
                    throw null;
                }
            } else {
                if (size > 5) {
                    ImageView imageView9 = this.A;
                    if (imageView9 != null) {
                        imageView9.setVisibility(0);
                        ImageView imageView10 = this.A;
                        if (imageView10 != null) {
                            F57.Bi bi5 = this.C;
                            if (bi5 != null) {
                                StringBuilder sb = new StringBuilder();
                                sb.append('+');
                                sb.append(list.size() - 4);
                                String sb2 = sb.toString();
                                Uy4 uy45 = this.D;
                                if (uy45 != null) {
                                    imageView10.setImageDrawable(bi5.e(sb2, uy45.c()));
                                } else {
                                    Wg6.n("colorGenerator");
                                    throw null;
                                }
                            } else {
                                Wg6.n("drawableBuilder");
                                throw null;
                            }
                        } else {
                            Wg6.n("img5");
                            throw null;
                        }
                    } else {
                        Wg6.n("img5");
                        throw null;
                    }
                } else if (size == 5) {
                    ImageView imageView11 = this.A;
                    if (imageView11 != null) {
                        imageView11.setVisibility(0);
                        ImageView imageView12 = this.A;
                        if (imageView12 != null) {
                            At4 at4 = list.get(4);
                            String a7 = at4 != null ? at4.a() : null;
                            Hz4 hz42 = Hz4.a;
                            At4 at42 = list.get(4);
                            String b2 = at42 != null ? at42.b() : null;
                            At4 at43 = list.get(4);
                            String d2 = at43 != null ? at43.d() : null;
                            At4 at44 = list.get(4);
                            if (at44 == null || (str = at44.e()) == null) {
                                str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                            }
                            String a8 = hz42.a(b2, d2, str);
                            F57.Bi bi6 = this.C;
                            if (bi6 != null) {
                                Uy4 uy46 = this.D;
                                if (uy46 != null) {
                                    Ty4.b(imageView12, a7, a8, bi6, uy46);
                                } else {
                                    Wg6.n("colorGenerator");
                                    throw null;
                                }
                            } else {
                                Wg6.n("drawableBuilder");
                                throw null;
                            }
                        } else {
                            Wg6.n("img5");
                            throw null;
                        }
                    } else {
                        Wg6.n("img5");
                        throw null;
                    }
                }
                Hz4 hz43 = Hz4.a;
                At4 at45 = list.get(0);
                String b3 = at45 != null ? at45.b() : null;
                At4 at46 = list.get(0);
                String d3 = at46 != null ? at46.d() : null;
                At4 at47 = list.get(0);
                if (!(at47 == null || (e = at47.e()) == null)) {
                    str3 = e;
                }
                String a9 = hz43.a(b3, d3, str3);
                if (size > 1) {
                    Hr7 hr7 = Hr7.a;
                    String c = Um5.c(getContext(), 2131886209);
                    Wg6.b(c, "LanguageHelper.getString\u2026ameAndNumberOthersJoined)");
                    String format = String.format(c, Arrays.copyOf(new Object[]{a9, String.valueOf(size - 1)}, 2));
                    Wg6.b(format, "java.lang.String.format(format, *args)");
                    Spannable b4 = Jl5.b(Jl5.b, a9, format, 0, 4, null);
                    FlexibleTextView flexibleTextView = this.B;
                    if (flexibleTextView != null) {
                        flexibleTextView.setText(b4);
                        return;
                    } else {
                        Wg6.n("ftvDes");
                        throw null;
                    }
                } else {
                    Hr7 hr72 = Hr7.a;
                    String c2 = Um5.c(getContext(), 2131886210);
                    Wg6.b(c2, "LanguageHelper.getString\u2026ge_Label__UsernameJoined)");
                    String format2 = String.format(c2, Arrays.copyOf(new Object[]{a9}, 1));
                    Wg6.b(format2, "java.lang.String.format(format, *args)");
                    Spannable b5 = Jl5.b(Jl5.b, a9, format2, 0, 4, null);
                    FlexibleTextView flexibleTextView2 = this.B;
                    if (flexibleTextView2 != null) {
                        flexibleTextView2.setText(b5);
                        return;
                    } else {
                        Wg6.n("ftvDes");
                        throw null;
                    }
                }
            }
        }
    }
}
