package com.portfolio.platform.uirenew.watchsetting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchSettingActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ String A;
    @DexIgnore
    public static /* final */ a B; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return WatchSettingActivity.A;
        }

        @DexIgnore
        public final void b(Context context, String str) {
            Wg6.c(context, "context");
            Wg6.c(str, "serial");
            Intent intent = new Intent(context, WatchSettingActivity.class);
            intent.putExtra("SERIAL", str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = a();
            local.d(a2, "start with " + str);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = WatchSettingActivity.class.getSimpleName();
        Wg6.b(simpleName, "WatchSettingActivity::class.java.simpleName");
        A = simpleName;
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        WatchSettingFragment watchSettingFragment = (WatchSettingFragment) getSupportFragmentManager().Y(2131362158);
        if (watchSettingFragment == null) {
            watchSettingFragment = WatchSettingFragment.t.b();
            k(watchSettingFragment, WatchSettingFragment.t.a(), 2131362158);
        }
        if (bundle != null && bundle.containsKey("SERIAL")) {
            String string = bundle.getString("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String r = r();
            local.d(r, "retrieve serial from savedInstanceState " + string);
            if (string != null) {
                Bundle bundle2 = new Bundle();
                bundle2.putString("SERIAL", string);
                watchSettingFragment.setArguments(bundle2);
            }
        }
        if (getIntent() != null) {
            String stringExtra = getIntent().getStringExtra("SERIAL");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String r2 = r();
            local2.d(r2, "retrieve serial from intent " + stringExtra);
            Bundle bundle3 = new Bundle();
            bundle3.putString("SERIAL", stringExtra);
            watchSettingFragment.setArguments(bundle3);
        }
    }
}
