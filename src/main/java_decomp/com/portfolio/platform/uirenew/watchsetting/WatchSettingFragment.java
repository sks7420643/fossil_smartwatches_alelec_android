package com.portfolio.platform.uirenew.watchsetting;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.Hq4;
import com.fossil.Ls0;
import com.fossil.Oa1;
import com.fossil.Po4;
import com.fossil.Ps4;
import com.fossil.Qv5;
import com.fossil.S37;
import com.fossil.Ts0;
import com.fossil.Uh5;
import com.fossil.Um5;
import com.fossil.Vl5;
import com.fossil.Vs0;
import com.fossil.Wa1;
import com.fossil.Xy4;
import com.mapped.AlertDialogFragment;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.WatchSettingFragmentBinding;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardActivity;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchSettingFragment extends Qv5 implements AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ Ai t; // = new Ai(null);
    @DexIgnore
    public G37<WatchSettingFragmentBinding> h;
    @DexIgnore
    public WatchSettingViewModel i;
    @DexIgnore
    public String j; // = ThemeManager.l.a().d("primaryText");
    @DexIgnore
    public Wa1 k;
    @DexIgnore
    public Po4 l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return WatchSettingFragment.s;
        }

        @DexIgnore
        public final WatchSettingFragment b() {
            return new WatchSettingFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<Hq4.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public Bi(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void a(Hq4.Bi bi) {
            if (bi != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = WatchSettingFragment.t.a();
                local.d(a2, "loadingState start " + bi.a() + " stop " + bi.b());
                if (bi.a()) {
                    this.a.i();
                }
                if (bi.b()) {
                    this.a.h();
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Bi bi) {
            a(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<Hq4.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public Ci(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void a(Hq4.Ci ci) {
            if (!ci.a().isEmpty()) {
                WatchSettingFragment watchSettingFragment = this.a;
                Object[] array = ci.a().toArray(new Uh5[0]);
                if (array != null) {
                    Uh5[] uh5Arr = (Uh5[]) array;
                    watchSettingFragment.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Ci ci) {
            a(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<WatchSettingViewModel.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public Di(WatchSettingFragment watchSettingFragment, String str) {
            this.a = watchSettingFragment;
            this.b = str;
        }

        @DexIgnore
        public final void a(WatchSettingViewModel.Ci ci) {
            if (ci != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = WatchSettingFragment.t.a();
                local.d(a2, "onUIState changed, modelWrapper=" + ci);
                if (ci.k() != null) {
                    WatchSettingFragment watchSettingFragment = this.a;
                    WatchSettingViewModel.Di k = ci.k();
                    if (k != null) {
                        watchSettingFragment.w3(k);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                if (ci.a()) {
                    this.a.e0();
                }
                if (ci.l() != null) {
                    WatchSettingFragment watchSettingFragment2 = this.a;
                    String l = ci.l();
                    if (l != null) {
                        watchSettingFragment2.W6(l);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                Integer m = ci.m();
                if (m != null) {
                    m.intValue();
                    WatchSettingFragment watchSettingFragment3 = this.a;
                    Integer m2 = ci.m();
                    if (m2 != null) {
                        watchSettingFragment3.X6(m2.intValue());
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                if (ci.f() != null) {
                    WatchSettingFragment watchSettingFragment4 = this.a;
                    Lc6<Integer, String> f = ci.f();
                    if (f != null) {
                        int intValue = f.getFirst().intValue();
                        Lc6<Integer, String> f2 = ci.f();
                        if (f2 != null) {
                            watchSettingFragment4.o(intValue, f2.getSecond());
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                if (ci.d()) {
                    this.a.Q6(this.b);
                }
                if (ci.c() != null) {
                    WatchSettingFragment watchSettingFragment5 = this.a;
                    String c = ci.c();
                    if (c != null) {
                        watchSettingFragment5.P6(c);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                if (ci.i() != null) {
                    WatchSettingFragment watchSettingFragment6 = this.a;
                    String i = ci.i();
                    if (i != null) {
                        watchSettingFragment6.T6(i);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                if (ci.e() != null) {
                    WatchSettingFragment watchSettingFragment7 = this.a;
                    String e = ci.e();
                    if (e != null) {
                        watchSettingFragment7.R6(e);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                if (ci.b() != null) {
                    WatchSettingFragment watchSettingFragment8 = this.a;
                    String b2 = ci.b();
                    if (b2 != null) {
                        watchSettingFragment8.O6(b2);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                if (ci.h() != null) {
                    WatchSettingFragment watchSettingFragment9 = this.a;
                    String h = ci.h();
                    if (h != null) {
                        watchSettingFragment9.S6(h);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                if (ci.g()) {
                    this.a.c();
                }
                if (ci.j()) {
                    this.a.V6();
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(WatchSettingViewModel.Ci ci) {
            a(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<WatchSettingViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment a;

        @DexIgnore
        public Ei(WatchSettingFragment watchSettingFragment) {
            this.a = watchSettingFragment;
        }

        @DexIgnore
        public final void a(WatchSettingViewModel.Ai ai) {
            boolean b = ai.b();
            boolean c = ai.c();
            if (b) {
                String c2 = c ? Um5.c(PortfolioApp.get.instance(), 2131886257) : Um5.c(PortfolioApp.get.instance(), 2131886256);
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                Wg6.b(c2, "title");
                String c3 = Um5.c(PortfolioApp.get.instance(), 2131886242);
                Wg6.b(c3, "LanguageHelper.getString\u2026ourCurrentChallengeFirst)");
                s37.o(childFragmentManager, c2, c3, ai.a());
            } else if (!c) {
                S37 s372 = S37.c;
                FragmentManager childFragmentManager2 = this.a.getChildFragmentManager();
                Wg6.b(childFragmentManager2, "childFragmentManager");
                s372.d0(childFragmentManager2, WatchSettingFragment.K6(this.a).L());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(WatchSettingViewModel.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment b;

        @DexIgnore
        public Fi(WatchSettingFragment watchSettingFragment) {
            this.b = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchSettingFragment.K6(this.b).G(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment b;

        @DexIgnore
        public Gi(WatchSettingFragment watchSettingFragment) {
            this.b = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingFragment.t.a();
            local.d(a2, "getWatchSerial=" + WatchSettingFragment.K6(this.b).P());
            if (this.b.isActive() && !TextUtils.isEmpty(WatchSettingFragment.K6(this.b).P())) {
                FindDeviceActivity.a aVar = FindDeviceActivity.B;
                FragmentActivity requireActivity = this.b.requireActivity();
                Wg6.b(requireActivity, "requireActivity()");
                String P = WatchSettingFragment.K6(this.b).P();
                if (P != null) {
                    aVar.a(requireActivity, P);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment b;

        @DexIgnore
        public Hi(WatchSettingFragment watchSettingFragment) {
            this.b = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.isActive() && !TextUtils.isEmpty(WatchSettingFragment.K6(this.b).P())) {
                CalibrationActivity.a aVar = CalibrationActivity.B;
                FragmentActivity requireActivity = this.b.requireActivity();
                Wg6.b(requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment b;

        @DexIgnore
        public Ii(WatchSettingFragment watchSettingFragment) {
            this.b = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchSettingFragment.K6(this.b).f0(100);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment b;

        @DexIgnore
        public Ji(WatchSettingFragment watchSettingFragment) {
            this.b = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchSettingFragment.K6(this.b).f0(50);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment b;

        @DexIgnore
        public Ki(WatchSettingFragment watchSettingFragment) {
            this.b = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchSettingFragment.K6(this.b).f0(25);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment b;

        @DexIgnore
        public Li(WatchSettingFragment watchSettingFragment) {
            this.b = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment b;

        @DexIgnore
        public Mi(WatchSettingFragment watchSettingFragment) {
            this.b = watchSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchSettingFragment.K6(this.b).F();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragmentBinding a;
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingFragment b;

        @DexIgnore
        public Ni(WatchSettingFragmentBinding watchSettingFragmentBinding, WatchSettingFragment watchSettingFragment, WatchSettingViewModel.Di di) {
            this.a = watchSettingFragmentBinding;
            this.b = watchSettingFragment;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            Wg6.c(str, "serial");
            Wg6.c(str2, "filePath");
            this.b.N6().t(str2).F0(this.a.z);
        }
    }

    /*
    static {
        String simpleName = WatchSettingFragment.class.getSimpleName();
        Wg6.b(simpleName, "WatchSettingFragment::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ WatchSettingViewModel K6(WatchSettingFragment watchSettingFragment) {
        WatchSettingViewModel watchSettingViewModel = watchSettingFragment.i;
        if (watchSettingViewModel != null) {
            return watchSettingViewModel;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return s;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    public final String M6(boolean z, int i2) {
        String c = Um5.c(PortfolioApp.get.instance(), z ? 2131887198 : 2131887173);
        if (!z || i2 <= 0) {
            Wg6.b(c, "connectedString");
            return c;
        }
        return c + ", " + i2 + '%';
    }

    @DexIgnore
    public final Wa1 N6() {
        Wa1 wa1 = this.k;
        if (wa1 != null) {
            return wa1;
        }
        Wg6.n("mRequestManager");
        throw null;
    }

    @DexIgnore
    public final void O6(String str) {
        Wg6.c(str, "serial");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.e0(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void P6(String str) {
        Wg6.c(str, "serial");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.v0(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void Q6(String str) {
        Wg6.c(str, "serial");
        if (getActivity() != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            Context requireContext = requireContext();
            Wg6.b(requireContext, "requireContext()");
            TroubleshootingActivity.a.c(aVar, requireContext, str, false, false, 12, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        switch (str.hashCode()) {
            case -2051261777:
                if (str.equals("REMOVE_DEVICE_WORKOUT")) {
                    String stringExtra = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra == null) {
                        return;
                    }
                    if (i2 == 2131363291) {
                        WatchSettingViewModel watchSettingViewModel = this.i;
                        if (watchSettingViewModel != null) {
                            watchSettingViewModel.W(stringExtra);
                            return;
                        } else {
                            Wg6.n("mViewModel");
                            throw null;
                        }
                    } else if (i2 == 2131363373) {
                        WatchSettingViewModel watchSettingViewModel2 = this.i;
                        if (watchSettingViewModel2 != null) {
                            watchSettingViewModel2.S(stringExtra);
                            return;
                        } else {
                            Wg6.n("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -1138109835:
                if (str.equals("SWITCH_DEVICE_ERASE_FAIL")) {
                    String stringExtra2 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra2 != null && i2 == 2131363373) {
                        WatchSettingViewModel watchSettingViewModel3 = this.i;
                        if (watchSettingViewModel3 != null) {
                            watchSettingViewModel3.U(stringExtra2);
                            return;
                        } else {
                            Wg6.n("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -693701870:
                if (str.equals("CONFIRM_REMOVE_DEVICE")) {
                    if (i2 == 2131363373) {
                        WatchSettingViewModel watchSettingViewModel4 = this.i;
                        if (watchSettingViewModel4 != null) {
                            watchSettingViewModel4.Z();
                            return;
                        } else {
                            Wg6.n("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -454228492:
                if (str.equals("REMOVE_DEVICE_SYNC_FAIL")) {
                    String stringExtra3 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra3 != null && i2 == 2131363373) {
                        WatchSettingViewModel watchSettingViewModel5 = this.i;
                        if (watchSettingViewModel5 != null) {
                            watchSettingViewModel5.R(stringExtra3);
                            return;
                        } else {
                            Wg6.n("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 39550276:
                if (str.equals("SWITCH_DEVICE_SYNC_FAIL")) {
                    String stringExtra4 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra4 != null && i2 == 2131363373) {
                        WatchSettingViewModel watchSettingViewModel6 = this.i;
                        if (watchSettingViewModel6 != null) {
                            watchSettingViewModel6.V(stringExtra4);
                            return;
                        } else {
                            Wg6.n("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 603997695:
                if (str.equals("SWITCH_DEVICE_WORKOUT")) {
                    String stringExtra5 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra5 == null) {
                        return;
                    }
                    if (i2 == 2131363291) {
                        WatchSettingViewModel watchSettingViewModel7 = this.i;
                        if (watchSettingViewModel7 != null) {
                            watchSettingViewModel7.X(stringExtra5);
                            return;
                        } else {
                            Wg6.n("mViewModel");
                            throw null;
                        }
                    } else if (i2 == 2131363373) {
                        WatchSettingViewModel watchSettingViewModel8 = this.i;
                        if (watchSettingViewModel8 != null) {
                            watchSettingViewModel8.T(stringExtra5);
                            return;
                        } else {
                            Wg6.n("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 1970588827:
                if (str.equals("LEAVE_CHALLENGE")) {
                    a();
                    if (i2 == 2131363373) {
                        Ps4 ps4 = intent != null ? (Ps4) intent.getParcelableExtra("CHALLENGE") : null;
                        if (ps4 != null) {
                            U6(ps4);
                            return;
                        }
                        return;
                    }
                    return;
                }
                break;
        }
        super.R5(str, i2, intent);
    }

    @DexIgnore
    public final void R6(String str) {
        Wg6.c(str, "serial");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.u0(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void S6(String str) {
        Wg6.c(str, "serial");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.f0(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void T6(String str) {
        Wg6.c(str, "serial");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.w0(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void U6(Ps4 ps4) {
        long b = Xy4.a.b();
        Date m2 = ps4.m();
        if (b > (m2 != null ? m2.getTime() : 0)) {
            BCOverviewLeaderBoardActivity.A.a(this, ps4, null);
        } else {
            BCWaitingChallengeDetailActivity.B.a(this, ps4, "joined_challenge", -1, false);
        }
    }

    @DexIgnore
    public final void V6() {
        FlexibleButton flexibleButton;
        G37<WatchSettingFragmentBinding> g37 = this.h;
        if (g37 != null) {
            WatchSettingFragmentBinding a2 = g37.a();
            if (a2 != null && (flexibleButton = a2.s) != null) {
                flexibleButton.setText(Um5.c(PortfolioApp.get.instance(), 2131887184));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void W6(String str) {
        Wg6.c(str, "lastSync");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = s;
        local.d(str2, "updateLastSync, lastSync=" + str);
        if (isActive()) {
            G37<WatchSettingFragmentBinding> g37 = this.h;
            if (g37 != null) {
                WatchSettingFragmentBinding a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.M;
                    Wg6.b(flexibleTextView, "it.tvLastSyncValue");
                    flexibleTextView.setText(str);
                    FlexibleTextView flexibleTextView2 = a2.M;
                    Wg6.b(flexibleTextView2, "it.tvLastSyncValue");
                    flexibleTextView2.setSelected(true);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void X6(int i2) {
        G37<WatchSettingFragmentBinding> g37 = this.h;
        if (g37 != null) {
            WatchSettingFragmentBinding a2 = g37.a();
            if (a2 != null) {
                a2.w.d("flexible_button_secondary");
                a2.y.d("flexible_button_secondary");
                a2.x.d("flexible_button_secondary");
                if (i2 == 25) {
                    a2.x.d("flexible_button_primary");
                } else if (i2 == 50) {
                    a2.y.d("flexible_button_primary");
                } else if (i2 == 100) {
                    a2.w.d("flexible_button_primary");
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void c() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    public final void e0() {
        FragmentActivity activity;
        FLogger.INSTANCE.getLocal().d(s, VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        if (isActive() && (activity = getActivity()) != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d(s, "stopLoading");
        a();
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d(s, "startLoading");
        b();
    }

    @DexIgnore
    public final void o(int i2, String str) {
        Wg6.c(str, "message");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        WatchSettingFragmentBinding watchSettingFragmentBinding = (WatchSettingFragmentBinding) Aq0.f(layoutInflater, 2131558639, viewGroup, false, A6());
        PortfolioApp.get.instance().getIface().K0().a(this);
        Po4 po4 = this.l;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(WatchSettingViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ingViewModel::class.java)");
            WatchSettingViewModel watchSettingViewModel = (WatchSettingViewModel) a2;
            this.i = watchSettingViewModel;
            if (watchSettingViewModel != null) {
                watchSettingViewModel.b0();
                Bundle arguments = getArguments();
                Object obj = arguments != null ? arguments.get("SERIAL") : null;
                if (obj != null) {
                    String str = (String) obj;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = s;
                    local.d(str2, "serial=" + str);
                    WatchSettingViewModel watchSettingViewModel2 = this.i;
                    if (watchSettingViewModel2 != null) {
                        watchSettingViewModel2.a0(str);
                        WatchSettingViewModel watchSettingViewModel3 = this.i;
                        if (watchSettingViewModel3 != null) {
                            watchSettingViewModel3.j().h(getViewLifecycleOwner(), new Bi(this));
                            WatchSettingViewModel watchSettingViewModel4 = this.i;
                            if (watchSettingViewModel4 != null) {
                                watchSettingViewModel4.l().h(getViewLifecycleOwner(), new Ci(this));
                                WatchSettingViewModel watchSettingViewModel5 = this.i;
                                if (watchSettingViewModel5 != null) {
                                    watchSettingViewModel5.O().h(getViewLifecycleOwner(), new Di(this, str));
                                    WatchSettingViewModel watchSettingViewModel6 = this.i;
                                    if (watchSettingViewModel6 != null) {
                                        if (watchSettingViewModel6.g0()) {
                                            FlexibleTextView flexibleTextView = watchSettingFragmentBinding.R;
                                            Wg6.b(flexibleTextView, "bindingLocal.tvVibration");
                                            flexibleTextView.setVisibility(0);
                                            FlexibleButton flexibleButton = watchSettingFragmentBinding.x;
                                            Wg6.b(flexibleButton, "bindingLocal.fbVibrationLow");
                                            flexibleButton.setVisibility(0);
                                            FlexibleButton flexibleButton2 = watchSettingFragmentBinding.y;
                                            Wg6.b(flexibleButton2, "bindingLocal.fbVibrationMedium");
                                            flexibleButton2.setVisibility(0);
                                            FlexibleButton flexibleButton3 = watchSettingFragmentBinding.w;
                                            Wg6.b(flexibleButton3, "bindingLocal.fbVibrationHigh");
                                            flexibleButton3.setVisibility(0);
                                        } else {
                                            FlexibleTextView flexibleTextView2 = watchSettingFragmentBinding.R;
                                            Wg6.b(flexibleTextView2, "bindingLocal.tvVibration");
                                            flexibleTextView2.setVisibility(8);
                                            FlexibleButton flexibleButton4 = watchSettingFragmentBinding.x;
                                            Wg6.b(flexibleButton4, "bindingLocal.fbVibrationLow");
                                            flexibleButton4.setVisibility(8);
                                            FlexibleButton flexibleButton5 = watchSettingFragmentBinding.y;
                                            Wg6.b(flexibleButton5, "bindingLocal.fbVibrationMedium");
                                            flexibleButton5.setVisibility(8);
                                            FlexibleButton flexibleButton6 = watchSettingFragmentBinding.w;
                                            Wg6.b(flexibleButton6, "bindingLocal.fbVibrationHigh");
                                            flexibleButton6.setVisibility(8);
                                        }
                                        WatchSettingViewModel watchSettingViewModel7 = this.i;
                                        if (watchSettingViewModel7 != null) {
                                            watchSettingViewModel7.K().h(getViewLifecycleOwner(), new Ei(this));
                                            G37<WatchSettingFragmentBinding> g37 = new G37<>(this, watchSettingFragmentBinding);
                                            this.h = g37;
                                            if (g37 != null) {
                                                WatchSettingFragmentBinding a3 = g37.a();
                                                if (a3 != null) {
                                                    Wg6.b(a3, "mBinding.get()!!");
                                                    return a3.n();
                                                }
                                                Wg6.i();
                                                throw null;
                                            }
                                            Wg6.n("mBinding");
                                            throw null;
                                        }
                                        Wg6.n("mViewModel");
                                        throw null;
                                    }
                                    Wg6.n("mViewModel");
                                    throw null;
                                }
                                Wg6.n("mViewModel");
                                throw null;
                            }
                            Wg6.n("mViewModel");
                            throw null;
                        }
                        Wg6.n("mViewModel");
                        throw null;
                    }
                    Wg6.n("mViewModel");
                    throw null;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.String");
            }
            Wg6.n("mViewModel");
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        WatchSettingViewModel watchSettingViewModel = this.i;
        if (watchSettingViewModel != null) {
            watchSettingViewModel.c0();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        WatchSettingViewModel watchSettingViewModel = this.i;
        if (watchSettingViewModel != null) {
            watchSettingViewModel.b0();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ConstraintLayout constraintLayout;
        ConstraintLayout constraintLayout2;
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        Wa1 v = Oa1.v(this);
        Wg6.b(v, "Glide.with(this)");
        this.k = v;
        G37<WatchSettingFragmentBinding> g37 = this.h;
        if (g37 != null) {
            WatchSettingFragmentBinding a2 = g37.a();
            if (a2 != null) {
                a2.N.setOnClickListener(new Fi(this));
                a2.I.setOnClickListener(new Gi(this));
                a2.E.setOnClickListener(new Hi(this));
                String d = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
                if (!TextUtils.isEmpty(d) && (constraintLayout2 = a2.u) != null) {
                    constraintLayout2.setBackgroundColor(Color.parseColor(d));
                }
                String d2 = ThemeManager.l.a().d("nonBrandSurface");
                if (!TextUtils.isEmpty(d2) && (constraintLayout = a2.t) != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(d2));
                }
                a2.w.setOnClickListener(new Ii(this));
                a2.y.setOnClickListener(new Ji(this));
                a2.x.setOnClickListener(new Ki(this));
                a2.r.setOnClickListener(new Li(this));
                a2.s.setOnClickListener(new Mi(this));
            }
            E6("watch_setting_view");
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void w3(WatchSettingViewModel.Di di) {
        Wg6.c(di, "watchSetting");
        FLogger.INSTANCE.getLocal().d(s, "updateDeviceInfo");
        if (isActive()) {
            G37<WatchSettingFragmentBinding> g37 = this.h;
            if (g37 != null) {
                WatchSettingFragmentBinding a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.P;
                    Wg6.b(flexibleTextView, "it.tvSerialValue");
                    flexibleTextView.setText(di.a().getDeviceId());
                    FlexibleTextView flexibleTextView2 = a2.K;
                    Wg6.b(flexibleTextView2, "it.tvFwVersionValue");
                    flexibleTextView2.setText(di.a().getFirmwareRevision());
                    FlexibleTextView flexibleTextView3 = a2.H;
                    Wg6.b(flexibleTextView3, "it.tvDeviceName");
                    flexibleTextView3.setText(di.b());
                    boolean d = di.d();
                    if (d) {
                        Boolean c = di.c();
                        if (c != null && c.booleanValue()) {
                            Integer vibrationStrength = di.a().getVibrationStrength();
                            if (vibrationStrength != null) {
                                X6(vibrationStrength.intValue());
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                        if (di.e()) {
                            FlexibleTextView flexibleTextView4 = a2.H;
                            Wg6.b(flexibleTextView4, "it.tvDeviceName");
                            flexibleTextView4.setAlpha(1.0f);
                            a2.H.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, W6.f(PortfolioApp.get.instance(), 2131231075), (Drawable) null);
                            FlexibleTextView flexibleTextView5 = a2.G;
                            Wg6.b(flexibleTextView5, "it.tvConnectionStatus");
                            flexibleTextView5.setAlpha(1.0f);
                            FlexibleTextView flexibleTextView6 = a2.G;
                            Wg6.b(flexibleTextView6, "it.tvConnectionStatus");
                            flexibleTextView6.setText(M6(true, di.a().getBatteryLevel()));
                            if (di.a().getBatteryLevel() >= 0) {
                                int batteryLevel = di.a().getBatteryLevel();
                                a2.G.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, W6.f(PortfolioApp.get.instance(), (batteryLevel >= 0 && 25 >= batteryLevel) ? 2131231008 : (25 <= batteryLevel && 50 >= batteryLevel) ? 2131231010 : (50 <= batteryLevel && 75 >= batteryLevel) ? 2131231012 : 2131231006), (Drawable) null);
                            }
                            FlexibleButton flexibleButton = a2.q;
                            Wg6.b(flexibleButton, "it.btActive");
                            flexibleButton.setVisibility(0);
                            FlexibleButton flexibleButton2 = a2.s;
                            Wg6.b(flexibleButton2, "it.btConnect");
                            flexibleButton2.setVisibility(8);
                            FlexibleTextView flexibleTextView7 = a2.E;
                            Wg6.b(flexibleTextView7, "it.tvCalibration");
                            flexibleTextView7.setAlpha(1.0f);
                            FlexibleButton flexibleButton3 = a2.w;
                            Wg6.b(flexibleButton3, "it.fbVibrationHigh");
                            flexibleButton3.setEnabled(true);
                            FlexibleButton flexibleButton4 = a2.x;
                            Wg6.b(flexibleButton4, "it.fbVibrationLow");
                            flexibleButton4.setEnabled(true);
                            FlexibleButton flexibleButton5 = a2.y;
                            Wg6.b(flexibleButton5, "it.fbVibrationMedium");
                            flexibleButton5.setEnabled(true);
                            FlexibleTextView flexibleTextView8 = a2.E;
                            Wg6.b(flexibleTextView8, "it.tvCalibration");
                            flexibleTextView8.setEnabled(true);
                        } else {
                            a2.H.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                            String str = this.j;
                            if (str != null) {
                                a2.H.setTextColor(Color.parseColor(str));
                            }
                            FlexibleTextView flexibleTextView9 = a2.H;
                            Wg6.b(flexibleTextView9, "it.tvDeviceName");
                            flexibleTextView9.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView10 = a2.G;
                            Wg6.b(flexibleTextView10, "it.tvConnectionStatus");
                            flexibleTextView10.setText(M6(false, di.a().getBatteryLevel()));
                            FlexibleTextView flexibleTextView11 = a2.G;
                            Wg6.b(flexibleTextView11, "it.tvConnectionStatus");
                            flexibleTextView11.setAlpha(0.4f);
                            FlexibleButton flexibleButton6 = a2.q;
                            Wg6.b(flexibleButton6, "it.btActive");
                            flexibleButton6.setVisibility(8);
                            FlexibleButton flexibleButton7 = a2.s;
                            Wg6.b(flexibleButton7, "it.btConnect");
                            flexibleButton7.setVisibility(0);
                            FlexibleButton flexibleButton8 = a2.s;
                            Wg6.b(flexibleButton8, "it.btConnect");
                            flexibleButton8.setText(Um5.c(PortfolioApp.get.instance(), 2131887184));
                            FlexibleTextView flexibleTextView12 = a2.E;
                            Wg6.b(flexibleTextView12, "it.tvCalibration");
                            flexibleTextView12.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView13 = a2.E;
                            Wg6.b(flexibleTextView13, "it.tvCalibration");
                            flexibleTextView13.setEnabled(false);
                        }
                    } else if (!d) {
                        a2.w.d("flexible_button_secondary");
                        a2.y.d("flexible_button_secondary");
                        a2.x.d("flexible_button_secondary");
                        a2.H.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                        String str2 = this.j;
                        if (str2 != null) {
                            a2.H.setTextColor(Color.parseColor(str2));
                        }
                        FlexibleTextView flexibleTextView14 = a2.H;
                        Wg6.b(flexibleTextView14, "it.tvDeviceName");
                        flexibleTextView14.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView15 = a2.G;
                        Wg6.b(flexibleTextView15, "it.tvConnectionStatus");
                        flexibleTextView15.setText(M6(false, di.a().getBatteryLevel()));
                        FlexibleTextView flexibleTextView16 = a2.G;
                        Wg6.b(flexibleTextView16, "it.tvConnectionStatus");
                        flexibleTextView16.setAlpha(0.4f);
                        FlexibleButton flexibleButton9 = a2.q;
                        Wg6.b(flexibleButton9, "it.btActive");
                        flexibleButton9.setVisibility(8);
                        FlexibleButton flexibleButton10 = a2.s;
                        Wg6.b(flexibleButton10, "it.btConnect");
                        flexibleButton10.setText(Um5.c(PortfolioApp.get.instance(), 2131887185));
                        FlexibleTextView flexibleTextView17 = a2.E;
                        Wg6.b(flexibleTextView17, "it.tvCalibration");
                        flexibleTextView17.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView18 = a2.E;
                        Wg6.b(flexibleTextView18, "it.tvCalibration");
                        flexibleTextView18.setEnabled(false);
                    }
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(di.a().getDeviceId()).setSerialPrefix(DeviceHelper.o.m(di.a().getDeviceId())).setType(Constants.DeviceType.TYPE_LARGE);
                    ImageView imageView = a2.z;
                    Wg6.b(imageView, "it.ivDevice");
                    type.setPlaceHolder(imageView, DeviceHelper.o.i(di.a().getDeviceId(), DeviceHelper.Bi.SMALL)).setImageCallback(new Ni(a2, this, di)).download();
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }
}
