package com.portfolio.platform.uirenew.watchsetting.calibration;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ct0;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.K17;
import com.fossil.Ko7;
import com.fossil.L17;
import com.fossil.P17;
import com.fossil.Pl5;
import com.fossil.Uh5;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CalibrationPresenter extends K17 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ long u; // = TimeUnit.SECONDS.toMillis(15);
    @DexIgnore
    public static /* final */ long v; // = TimeUnit.SECONDS.toMillis(1);
    @DexIgnore
    public static /* final */ Ai w; // = new Ai(null);
    @DexIgnore
    public boolean e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = 3;
    @DexIgnore
    public /* final */ Ei h; // = new Ei(this, u, v);
    @DexIgnore
    public /* final */ Handler i; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ Runnable k; // = new Fi(this);
    @DexIgnore
    public /* final */ Gi l; // = new Gi(this);
    @DexIgnore
    public /* final */ Di m; // = new Di(this);
    @DexIgnore
    public /* final */ Ci n; // = new Ci(this);
    @DexIgnore
    public /* final */ PortfolioApp o;
    @DexIgnore
    public /* final */ L17 p;
    @DexIgnore
    public /* final */ UserRepository q;
    @DexIgnore
    public /* final */ Ct0 r;
    @DexIgnore
    public /* final */ Pl5 s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return CalibrationPresenter.t;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationPresenter$completeCalibration$1", f = "CalibrationPresenter.kt", l = {383}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CalibrationPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(CalibrationPresenter calibrationPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = calibrationPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Pl5 pl5;
            Object currentUser;
            String str;
            String str2;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.h.cancel();
                this.this$0.p.b();
                pl5 = this.this$0.s;
                String J = this.this$0.o.J();
                UserRepository userRepository = this.this$0.q;
                this.L$0 = il6;
                this.L$1 = pl5;
                this.L$2 = J;
                this.label = 1;
                currentUser = userRepository.getCurrentUser(this);
                if (currentUser == d) {
                    return d;
                }
                str = J;
            } else if (i == 1) {
                pl5 = (Pl5) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                str = (String) this.L$2;
                currentUser = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) currentUser;
            if (mFUser == null || (str2 = mFUser.getUserId()) == null) {
                str2 = "";
            }
            pl5.b(str, str2);
            this.this$0.f = 4;
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ CalibrationPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ci(CalibrationPresenter calibrationPresenter) {
            this.a = calibrationPresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            if (Wg6.a("android.bluetooth.adapter.action.STATE_CHANGED", intent.getAction())) {
                int intExtra = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", RecyclerView.UNDEFINED_DURATION);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = CalibrationPresenter.w.a();
                local.d(a2, "mBluetoothStateChangeReceiver - onReceive() - state = " + intExtra);
                if (intExtra == 10) {
                    FLogger.INSTANCE.getLocal().d(CalibrationPresenter.w.a(), "Bluetooth off");
                    this.a.m();
                    this.a.p.a();
                    this.a.p.r1();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ CalibrationPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di(CalibrationPresenter calibrationPresenter) {
            this.a = calibrationPresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = CalibrationPresenter.w.a();
            local.d(a2, "mConnectionStateChangeReceiver onReceive: status = " + intExtra);
            if (intExtra == ConnectionStateChange.GATT_OFF.ordinal()) {
                BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
                Wg6.b(defaultAdapter, "BluetoothAdapter.getDefaultAdapter()");
                if (!defaultAdapter.isEnabled()) {
                    this.a.p.a();
                    this.a.p.r1();
                    return;
                }
                this.a.m();
                this.a.H(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends CountDownTimer {
        @DexIgnore
        public /* final */ /* synthetic */ CalibrationPresenter a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(CalibrationPresenter calibrationPresenter, long j, long j2) {
            super(j, j2);
            this.a = calibrationPresenter;
        }

        @DexIgnore
        public void onFinish() {
            FLogger.INSTANCE.getLocal().d(CalibrationPresenter.w.a(), "CountDownTimer onFinish");
            this.a.s.e(true, this.a.o.J(), 0, CalibrationEnums.HandId.HOUR);
            cancel();
            start();
        }

        @DexIgnore
        public void onTick(long j) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CalibrationPresenter b;

        @DexIgnore
        public Fi(CalibrationPresenter calibrationPresenter) {
            this.b = calibrationPresenter;
        }

        @DexIgnore
        public final void run() {
            if (this.b.j) {
                this.b.p.a();
                this.b.M();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements BleCommandResultManager.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ CalibrationPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Gi(CalibrationPresenter calibrationPresenter) {
            this.a = calibrationPresenter;
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            boolean z = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal();
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = CalibrationPresenter.w.a();
            local.d(a2, "receiver mode=" + communicateMode + ", isSuccess=" + z);
            int i = P17.a[communicateMode.ordinal()];
            if (i == 1) {
                this.a.J(z);
            } else if (i == 2) {
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                this.a.G(z, integerArrayListExtra, intExtra);
            } else if (i == 3) {
                this.a.F(z, intExtra);
            } else if (i == 4) {
                this.a.I(z);
            } else if (i == 5) {
                this.a.H(z);
            }
        }
    }

    /*
    static {
        String simpleName = CalibrationPresenter.class.getSimpleName();
        Wg6.b(simpleName, "CalibrationPresenter::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public CalibrationPresenter(PortfolioApp portfolioApp, L17 l17, UserRepository userRepository, Ct0 ct0, Pl5 pl5) {
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(l17, "mView");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(ct0, "mLocalBroadcastManager");
        Wg6.c(pl5, "mWatchHelper");
        this.o = portfolioApp;
        this.p = l17;
        this.q = userRepository;
        this.r = ct0;
        this.s = pl5;
    }

    @DexIgnore
    public final void D(String str, String str2, String str3) {
        AnalyticsHelper.f.g().k(str, str2, str3);
    }

    @DexIgnore
    public final void E() {
        FLogger.INSTANCE.getLocal().d(t, "completeCalibration");
        Rm6 unused = Gu7.d(k(), null, null, new Bi(this, null), 3, null);
    }

    @DexIgnore
    public final void F(boolean z, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onApplyHandsComplete isSuccess=" + z);
        if (z) {
            D("device_calibrate_result", "errorCode", "N/A");
            return;
        }
        D("device_calibrate_result", "errorCode", String.valueOf(i2));
        this.p.a();
        M();
    }

    @DexIgnore
    public final void G(boolean z, ArrayList<Integer> arrayList, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.e(str, "onEnterCalibrationComplete isSuccess=" + z + ", lastErrorCode=" + i2);
        if (z) {
            this.s.i(this.o.J());
            return;
        }
        this.p.a();
        if (i2 != 1101 && i2 != 1112 && i2 != 1113) {
            M();
        } else if (arrayList != null) {
            List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(arrayList);
            Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026onErrorCode(errorCodes!!)");
            L17 l17 = this.p;
            Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
            if (array != null) {
                Uh5[] uh5Arr = (Uh5[]) array;
                l17.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void H(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onExitCalibrationComplete success=" + z);
        this.p.a();
        if (z) {
            this.p.e0();
        } else {
            M();
        }
    }

    @DexIgnore
    public final void I(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onMoveHandComplete isSuccess=" + z);
        if (!z) {
            this.s.a(this.o.J());
            M();
        }
    }

    @DexIgnore
    public final void J(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.e(str, "onResetHandComplete isSuccess=" + z);
        this.p.a();
        if (z) {
            P();
            this.h.start();
            return;
        }
        M();
    }

    @DexIgnore
    public final void K() {
        FLogger.INSTANCE.getLocal().d(t, "registerBroadcastReceiver()");
        BleCommandResultManager.d.e(this.l, CommunicateMode.ENTER_CALIBRATION, CommunicateMode.RESET_HAND, CommunicateMode.MOVE_HAND, CommunicateMode.APPLY_HAND_POSITION, CommunicateMode.EXIT_CALIBRATION);
        Ct0 ct0 = this.r;
        Di di = this.m;
        ct0.c(di, new IntentFilter(this.o.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        this.o.registerReceiver(this.n, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
    }

    @DexIgnore
    public void L() {
        this.p.M5(this);
    }

    @DexIgnore
    public final void M() {
        if (!this.e) {
            this.p.i4();
        }
    }

    @DexIgnore
    public final void N() {
        FLogger.INSTANCE.getLocal().d(t, "startCalibration");
        int i2 = this.f;
        if (i2 == 0) {
            this.p.F1(this.o.J());
        } else if (i2 == 1) {
            this.p.I4(this.o.J());
        } else if (i2 == 2) {
            this.p.x1(this.o.J());
        }
        if (!this.e) {
            this.p.b();
            FLogger.INSTANCE.getLocal().e(t, "mStartCalibration");
            this.s.c(this.o.J());
            O(30);
        }
    }

    @DexIgnore
    public final void O(int i2) {
        P();
        this.j = true;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "startSetConfigTimeOutTimer:  timeout = " + i2);
        this.i.postDelayed(this.k, ((long) i2) * 1000);
    }

    @DexIgnore
    public final void P() {
        FLogger.INSTANCE.getLocal().d(t, "stopSetConfigTimeOutTimer");
        this.j = false;
        this.i.removeCallbacks(this.k);
    }

    @DexIgnore
    public final void Q() {
        FLogger.INSTANCE.getLocal().d(t, "unregisterBroadcastReceiver()");
        try {
            BleCommandResultManager.d.j(this.l, CommunicateMode.ENTER_CALIBRATION, CommunicateMode.RESET_HAND, CommunicateMode.MOVE_HAND, CommunicateMode.APPLY_HAND_POSITION, CommunicateMode.EXIT_CALIBRATION);
            this.r.e(this.m);
            this.o.unregisterReceiver(this.n);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = t;
            local.d(str, "unregisterBroadcastReceiver() - ex = " + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(t, "start()");
        Q();
        K();
        N();
        BleCommandResultManager.d.g(CommunicateMode.ENTER_CALIBRATION);
        int i2 = DeviceHelper.o.e(this.o.J()) == MFDeviceFamily.DEVICE_FAMILY_SAM ? 3 : 2;
        this.g = i2;
        this.p.L5(this.f, i2);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(t, "stop");
        if (this.f != 3) {
            this.s.a(this.o.J());
        }
        Q();
        this.h.cancel();
        P();
    }

    @DexIgnore
    @Override // com.fossil.K17
    public void n() {
        FLogger.INSTANCE.getLocal().d(t, "back");
        int i2 = this.f;
        if (i2 == 0) {
            this.p.e0();
        } else if (i2 == 1) {
            this.f = 0;
            this.p.F1(this.o.J());
        } else if (i2 == 2) {
            this.f = 1;
            this.p.I4(this.o.J());
        } else if (i2 == 3) {
            if (DeviceHelper.o.e(this.o.J()) == MFDeviceFamily.DEVICE_FAMILY_SAM) {
                this.f = 2;
                this.p.x1(this.o.J());
            } else {
                this.f = 1;
                this.p.I4(this.o.J());
            }
        }
        this.p.L5(this.f, this.g);
    }

    @DexIgnore
    @Override // com.fossil.K17
    public List<DeviceHelper.Bi> o() {
        ArrayList arrayList = new ArrayList();
        MFDeviceFamily e2 = DeviceHelper.o.e(this.o.J());
        if (e2 == MFDeviceFamily.DEVICE_FAMILY_SAM || e2 == MFDeviceFamily.DEVICE_FAMILY_SAM_SLIM || e2 == MFDeviceFamily.DEVICE_FAMILY_SAM_MINI) {
            arrayList.add(DeviceHelper.Bi.HYBRID_WATCH_HOUR);
            arrayList.add(DeviceHelper.Bi.HYBRID_WATCH_MINUTE);
            arrayList.add(DeviceHelper.Bi.HYBRID_WATCH_SUBEYE);
        } else {
            arrayList.add(DeviceHelper.Bi.DIANA_WATCH_HOUR);
            arrayList.add(DeviceHelper.Bi.DIANA_WATCH_MINUTE);
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.K17
    public void p() {
        FLogger.INSTANCE.getLocal().d(t, "next");
        int i2 = this.f;
        if (i2 == 0) {
            D("device_calibrate", "Step", AppFilter.COLUMN_HOUR);
            this.f = 1;
            this.p.I4(this.o.J());
        } else if (i2 == 1) {
            D("device_calibrate", "Step", MFSleepGoal.COLUMN_MINUTE);
            if (DeviceHelper.o.e(this.o.J()) == MFDeviceFamily.DEVICE_FAMILY_SAM) {
                this.f = 2;
                this.p.x1(this.o.J());
            } else {
                this.f = 3;
                E();
            }
        } else if (i2 == 2) {
            D("device_calibrate", "Step", "subeye");
            this.f = 3;
            E();
        } else if (i2 == 3) {
            E();
        }
        this.p.L5(this.f, this.g);
    }

    @DexIgnore
    @Override // com.fossil.K17
    public void q(boolean z) {
        this.e = z;
    }

    @DexIgnore
    @Override // com.fossil.K17
    public void r(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "startMove: clockwise = " + z);
        int i2 = FossilDeviceSerialPatternUtil.isDianaDevice(this.o.J()) ? 1 : 2;
        int i3 = this.f;
        if (i3 == 0) {
            this.s.e(z, this.o.J(), i2, CalibrationEnums.HandId.HOUR);
        } else if (i3 == 1) {
            this.s.e(z, this.o.J(), i2, CalibrationEnums.HandId.MINUTE);
        } else if (i3 == 2) {
            this.s.e(z, this.o.J(), i2, CalibrationEnums.HandId.SUB_EYE);
        }
    }

    @DexIgnore
    @Override // com.fossil.K17
    public void s(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "startSmartMove: clockwise = " + z + ", mCalibrationStep = " + this.f);
        int i2 = this.f;
        if (i2 == 0) {
            this.s.f(z, this.o.J(), 2, CalibrationEnums.HandId.HOUR);
        } else if (i2 == 1) {
            this.s.f(z, this.o.J(), 2, CalibrationEnums.HandId.MINUTE);
        } else if (i2 == 2) {
            this.s.f(z, this.o.J(), 2, CalibrationEnums.HandId.SUB_EYE);
        }
    }

    @DexIgnore
    @Override // com.fossil.K17
    public void t() {
        FLogger.INSTANCE.getLocal().d(t, "stopSmartMove");
        this.s.g();
    }
}
