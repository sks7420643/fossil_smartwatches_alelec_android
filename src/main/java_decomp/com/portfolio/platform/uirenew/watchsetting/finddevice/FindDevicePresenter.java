package com.portfolio.platform.uirenew.watchsetting.finddevice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.model.PlaceFields;
import com.fossil.Ct0;
import com.fossil.El7;
import com.fossil.Gu5;
import com.fossil.Gu7;
import com.fossil.Jn5;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Ss0;
import com.fossil.U17;
import com.fossil.V17;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.FindDeviceFragment;
import com.mapped.GetLocation;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.LoadLocation;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.network.utils.NetworkUtils;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FindDevicePresenter extends U17 {
    @DexIgnore
    public static /* final */ long v; // = TimeUnit.SECONDS.toMillis(1);
    @DexIgnore
    public /* final */ MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<WatchSettingViewModel.Di> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<WatchSettingViewModel.Di> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public /* final */ Handler i;
    @DexIgnore
    public /* final */ Runnable j;
    @DexIgnore
    public /* final */ Ei k;
    @DexIgnore
    public /* final */ Di l;
    @DexIgnore
    public /* final */ Ct0 m;
    @DexIgnore
    public /* final */ DeviceRepository n;
    @DexIgnore
    public /* final */ An4 o;
    @DexIgnore
    public /* final */ V17 p;
    @DexIgnore
    public /* final */ GetLocation q;
    @DexIgnore
    public /* final */ LoadLocation r;
    @DexIgnore
    public /* final */ GetAddress s;
    @DexIgnore
    public /* final */ Gu5 t;
    @DexIgnore
    public /* final */ PortfolioApp u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ei<GetLocation.Ci, GetLocation.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public Ai(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(GetLocation.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(GetLocation.Bi bi) {
            Wg6.c(bi, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation onError");
            this.a.p.r6();
        }

        @DexIgnore
        public void c(GetLocation.Ci ci) {
            Wg6.c(ci, "responseValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation onSuccess");
            this.a.C(ci.a());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(GetLocation.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ei<GetAddress.Ci, GetAddress.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(GetAddress.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(GetAddress.Bi bi) {
            Wg6.c(bi, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetCityName onError");
        }

        @DexIgnore
        public void c(GetAddress.Ci ci) {
            Wg6.c(ci, "responseValue");
            String a2 = ci.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDevicePresenter", "GetCityName onSuccess - address: " + a2);
            this.a.p.L2(a2);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(GetAddress.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Ei<LoadLocation.Ci, LoadLocation.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ci(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(LoadLocation.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(LoadLocation.Bi bi) {
            Wg6.c(bi, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation onError");
            this.a.y();
        }

        @DexIgnore
        public void c(LoadLocation.Ci ci) {
            Wg6.c(ci, "responseValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation onSuccess");
            if (Wg6.a(this.a.B(), ci.a().getDeviceSerial())) {
                this.a.C(ci.a());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(LoadLocation.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            DeviceLocation deviceLocation = (DeviceLocation) intent.getSerializableExtra("device_location");
            String stringExtra = intent.getStringExtra("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDevicePresenter", "onReceive - location: " + deviceLocation + ",serial: " + stringExtra);
            if (Wg6.a(stringExtra, this.a.B()) && deviceLocation != null) {
                this.a.C(deviceLocation);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            if (Wg6.a(stringExtra, this.a.B()) && Wg6.a(stringExtra, this.a.z())) {
                this.a.A().l(stringExtra);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1$1", f = "FindDevicePresenter.kt", l = {66, 69}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $serial;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1$1$deviceModel$1", f = "FindDevicePresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Device>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Device> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        DeviceRepository deviceRepository = this.this$0.this$0.a.n;
                        String str = this.this$0.$serial;
                        Wg6.b(str, "serial");
                        return deviceRepository.getDeviceBySerial(str);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Biii extends Ko7 implements Coroutine<Il6, Xe6<? super String>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Xe6 xe6, Aii aii) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Biii biii = new Biii(xe6, this.this$0);
                    biii.p$ = (Il6) obj;
                    throw null;
                    //return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super String> xe6) {
                    throw null;
                    //return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        DeviceRepository deviceRepository = this.this$0.this$0.a.n;
                        String str = this.this$0.$serial;
                        Wg6.b(str, "serial");
                        return deviceRepository.getDeviceNameBySerial(str);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, String str, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
                this.$serial = str;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$serial, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:23:0x0091  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r11) {
                /*
                // Method dump skipped, instructions count: 238
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter.Fi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Fi(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        public final MutableLiveData<WatchSettingViewModel.Di> a(String str) {
            this.a.j.run();
            Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, str, null), 3, null);
            return this.a.f;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((String) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements CoroutineUseCase.Ei<Gu5.Ei, Gu5.Bi> {
            @DexIgnore
            public /* final */ /* synthetic */ Gi a;

            @DexIgnore
            public Aii(Gi gi) {
                this.a = gi;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void a(Gu5.Bi bi) {
                b(bi);
            }

            @DexIgnore
            public void b(Gu5.Bi bi) {
                Wg6.c(bi, "errorValue");
                FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "getRssi onError");
                this.a.b.G(-9999);
                this.a.b.J(-9999);
            }

            @DexIgnore
            public void c(Gu5.Ei ei) {
                Wg6.c(ei, "responseValue");
                int a2 = ei.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FindDevicePresenter", "getRssi onSuccess - rssi: " + a2);
                this.a.b.G(a2);
                this.a.b.J(a2);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void onSuccess(Gu5.Ei ei) {
                c(ei);
            }
        }

        @DexIgnore
        public Gi(FindDevicePresenter findDevicePresenter) {
            this.b = findDevicePresenter;
        }

        @DexIgnore
        public final void run() {
            String B = this.b.B();
            if (B != null) {
                this.b.t.e(new Gu5.Di(B), new Aii(this));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> implements Ls0<WatchSettingViewModel.Di> {
        @DexIgnore
        public /* final */ /* synthetic */ FindDevicePresenter a;

        @DexIgnore
        public Hi(FindDevicePresenter findDevicePresenter) {
            this.a = findDevicePresenter;
        }

        @DexIgnore
        public final void a(WatchSettingViewModel.Di di) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.C.a();
            local.d(a2, "observer device " + di);
            if (di != null) {
                this.a.p.w3(di);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(WatchSettingViewModel.Di di) {
            a(di);
        }
    }

    @DexIgnore
    public FindDevicePresenter(Ct0 ct0, DeviceRepository deviceRepository, An4 an4, V17 v17, GetLocation getLocation, LoadLocation loadLocation, GetAddress getAddress, Gu5 gu5, PortfolioApp portfolioApp) {
        Wg6.c(ct0, "mLocalBroadcastManager");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(v17, "mView");
        Wg6.c(getLocation, "mGetLocation");
        Wg6.c(loadLocation, "mLoadLocation");
        Wg6.c(getAddress, "mGetAddress");
        Wg6.c(gu5, "mGetRssi");
        Wg6.c(portfolioApp, "mApp");
        this.m = ct0;
        this.n = deviceRepository;
        this.o = an4;
        this.p = v17;
        this.q = getLocation;
        this.r = loadLocation;
        this.s = getAddress;
        this.t = gu5;
        this.u = portfolioApp;
        LiveData<WatchSettingViewModel.Di> c = Ss0.c(this.e, new Fi(this));
        Wg6.b(c, "Transformations.switchMa\u2026viceWrapperLiveData\n    }");
        this.g = c;
        this.i = new Handler();
        this.j = new Gi(this);
        this.k = new Ei(this);
        this.l = new Di(this);
    }

    @DexIgnore
    public final MutableLiveData<String> A() {
        return this.e;
    }

    @DexIgnore
    public String B() {
        return this.e.e();
    }

    @DexIgnore
    public final void C(DeviceLocation deviceLocation) {
        Wg6.c(deviceLocation, PlaceFields.LOCATION);
        this.p.I2(deviceLocation.getTimeStamp());
        double latitude = deviceLocation.getLatitude();
        double longitude = deviceLocation.getLongitude();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDevicePresenter", "handleLocation latitude=" + latitude + ", longitude=" + longitude);
        if (latitude != 0.0d && longitude != 0.0d) {
            this.p.E1(Double.valueOf(latitude), Double.valueOf(longitude));
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetCityName");
            this.s.e(new GetAddress.Ai(latitude, longitude), new Bi(this));
        }
    }

    @DexIgnore
    public final boolean D(Context context) {
        return Settings.Secure.getInt(context.getContentResolver(), "location_mode") != 0;
    }

    @DexIgnore
    public final void E() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation");
        this.r.e(new LoadLocation.Ai(), new Ci(this));
    }

    @DexIgnore
    public final void F() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "locateOnMap");
        if (!NetworkUtils.isNetworkAvailable(this.u)) {
            this.p.o(601, null);
            return;
        }
        Jn5 jn5 = Jn5.b;
        V17 v17 = this.p;
        if (v17 == null) {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.watchsetting.FindDeviceFragment");
        } else if (!Jn5.c(jn5, ((FindDeviceFragment) v17).getContext(), Jn5.Ai.FIND_DEVICE, false, false, false, null, 60, null)) {
            this.p.j1();
        } else {
            String B = B();
            if (B != null) {
                if (TextUtils.equals(B, z()) && x(B)) {
                    Context applicationContext = this.u.getApplicationContext();
                    Wg6.b(applicationContext, "mApp.applicationContext");
                    if (D(applicationContext)) {
                        E();
                        return;
                    }
                }
                y();
            }
        }
    }

    @DexIgnore
    public final void G(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public void H(String str) {
        Wg6.c(str, "serial");
        this.e.l(str);
    }

    @DexIgnore
    public void I() {
        this.p.M5(this);
    }

    @DexIgnore
    public final void J(int i2) {
        if (i2 != -9999) {
            this.p.W0(i2);
        }
        this.i.removeCallbacks(this.j);
        this.i.postDelayed(this.j, v);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        this.m.c(this.l, new IntentFilter(MFDeviceService.a0.a()));
        PortfolioApp portfolioApp = this.u;
        Ei ei = this.k;
        portfolioApp.registerReceiver(ei, new IntentFilter(this.u.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        LiveData<WatchSettingViewModel.Di> liveData = this.g;
        V17 v17 = this.p;
        if (v17 != null) {
            liveData.h((BaseFragment) v17, new Hi(this));
            this.t.p();
            if (Wg6.a(this.u.J(), this.e.e())) {
                FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "update RSSI only for active device");
                J(this.h);
                return;
            }
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        try {
            MutableLiveData<WatchSettingViewModel.Di> mutableLiveData = this.f;
            V17 v17 = this.p;
            if (v17 != null) {
                mutableLiveData.n((BaseFragment) v17);
                this.e.n((LifecycleOwner) this.p);
                this.g.n((LifecycleOwner) this.p);
                this.m.e(this.l);
                this.u.unregisterReceiver(this.k);
                this.t.s();
                this.i.removeCallbacksAndMessages(null);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("FindDevicePresenter", "stop with " + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.U17
    public void n(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDevicePresenter", "enableLocate: enable = " + z);
        o(z);
        this.p.f3(z, this.o.d0());
        if (z) {
            F();
        }
    }

    @DexIgnore
    @Override // com.fossil.U17
    public void o(boolean z) {
        this.o.a1(z);
    }

    @DexIgnore
    public final boolean x(String str) {
        IButtonConnectivity b = PortfolioApp.get.b();
        return b != null && b.getGattState(str) == 2;
    }

    @DexIgnore
    public final void y() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation");
        String B = B();
        if (B != null) {
            this.q.e(new GetLocation.Ai(B), new Ai(this));
        }
    }

    @DexIgnore
    public final String z() {
        return PortfolioApp.get.instance().J();
    }
}
