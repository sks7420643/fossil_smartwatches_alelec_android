package com.portfolio.platform.uirenew.watchsetting;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.text.format.DateUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.G17;
import com.fossil.Gu7;
import com.fossil.Hq4;
import com.fossil.Hr7;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Ps4;
import com.fossil.Ss0;
import com.fossil.St5;
import com.fossil.Tt4;
import com.fossil.Tt5;
import com.fossil.Uh5;
import com.fossil.Um5;
import com.fossil.Us0;
import com.fossil.Ut5;
import com.fossil.Vt7;
import com.fossil.Xy4;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Hx5;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchSettingViewModel extends Hq4 {
    @DexIgnore
    public static /* final */ String B;
    @DexIgnore
    public static /* final */ Bi C; // = new Bi(null);
    @DexIgnore
    public /* final */ PortfolioApp A;
    @DexIgnore
    public MutableLiveData<Ci> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Handler i; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public MutableLiveData<String> j;
    @DexIgnore
    public /* final */ LiveData<Device> k;
    @DexIgnore
    public Di l;
    @DexIgnore
    public Ci m;
    @DexIgnore
    public String n;
    @DexIgnore
    public /* final */ Runnable o;
    @DexIgnore
    public Ls0<Device> p;
    @DexIgnore
    public /* final */ MutableLiveData<Ai> q;
    @DexIgnore
    public /* final */ Ii r;
    @DexIgnore
    public /* final */ DeviceRepository s;
    @DexIgnore
    public /* final */ SetVibrationStrengthUseCase t;
    @DexIgnore
    public /* final */ UnlinkDeviceUseCase u;
    @DexIgnore
    public /* final */ Cj4 v;
    @DexIgnore
    public /* final */ An4 w;
    @DexIgnore
    public /* final */ SwitchActiveDeviceUseCase x;
    @DexIgnore
    public /* final */ ReconnectDeviceUseCase y;
    @DexIgnore
    public /* final */ Tt4 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public Ps4 c;

        @DexIgnore
        public Ai(boolean z, boolean z2, Ps4 ps4) {
            this.a = z;
            this.b = z2;
            this.c = ps4;
        }

        @DexIgnore
        public final Ps4 a() {
            return this.c;
        }

        @DexIgnore
        public final boolean b() {
            return this.a;
        }

        @DexIgnore
        public final boolean c() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!(this.a == ai.a && this.b == ai.b && Wg6.a(this.c, ai.c))) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 1;
            boolean z = this.a;
            if (z) {
                z = true;
            }
            boolean z2 = this.b;
            if (!z2) {
                i = z2 ? 1 : 0;
            }
            Ps4 ps4 = this.c;
            int hashCode = ps4 != null ? ps4.hashCode() : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            return (((i2 * 31) + i) * 31) + hashCode;
        }

        @DexIgnore
        public String toString() {
            return "CheckingChallengeWrapper(needToLeave=" + this.a + ", isSwitch=" + this.b + ", challenge=" + this.c + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public /* synthetic */ Bi(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return WatchSettingViewModel.B;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public Di b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public Lc6<Integer, String> e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;
        @DexIgnore
        public String i;
        @DexIgnore
        public String j;
        @DexIgnore
        public String k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public boolean m;

        @DexIgnore
        public Ci() {
            this(false, null, null, null, null, false, null, null, null, null, null, false, false, 8191, null);
        }

        @DexIgnore
        public Ci(boolean z, Di di, String str, Integer num, Lc6<Integer, String> lc6, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4) {
            this.a = z;
            this.b = di;
            this.c = str;
            this.d = num;
            this.e = lc6;
            this.f = z2;
            this.g = str2;
            this.h = str3;
            this.i = str4;
            this.j = str5;
            this.k = str6;
            this.l = z3;
            this.m = z4;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ci(boolean z, Di di, String str, Integer num, Lc6 lc6, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4, int i2, Qg6 qg6) {
            this((i2 & 1) != 0 ? false : z, (i2 & 2) != 0 ? null : di, (i2 & 4) != 0 ? null : str, (i2 & 8) != 0 ? null : num, (i2 & 16) != 0 ? null : lc6, (i2 & 32) != 0 ? false : z2, (i2 & 64) != 0 ? null : str2, (i2 & 128) != 0 ? null : str3, (i2 & 256) != 0 ? null : str4, (i2 & 512) != 0 ? null : str5, (i2 & 1024) == 0 ? str6 : null, (i2 & 2048) != 0 ? false : z3, (i2 & 4096) == 0 ? z4 : false);
        }

        @DexIgnore
        public static /* synthetic */ void q(Ci ci, boolean z, Di di, String str, Integer num, Lc6 lc6, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4, int i2, Object obj) {
            boolean z5 = false;
            boolean z6 = (i2 & 1) != 0 ? false : z;
            String str7 = null;
            Di di2 = (i2 & 2) != 0 ? null : di;
            String str8 = (i2 & 4) != 0 ? null : str;
            Integer num2 = (i2 & 8) != 0 ? null : num;
            Lc6 lc62 = (i2 & 16) != 0 ? null : lc6;
            boolean z7 = (i2 & 32) != 0 ? false : z2;
            String str9 = (i2 & 64) != 0 ? null : str2;
            String str10 = (i2 & 128) != 0 ? null : str3;
            String str11 = (i2 & 256) != 0 ? null : str4;
            String str12 = (i2 & 512) != 0 ? null : str5;
            if ((i2 & 1024) == 0) {
                str7 = str6;
            }
            boolean z8 = (i2 & 2048) != 0 ? false : z3;
            if ((i2 & 4096) == 0) {
                z5 = z4;
            }
            ci.p(z6, di2, str8, num2, lc62, z7, str9, str10, str11, str12, str7, z8, z5);
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.j;
        }

        @DexIgnore
        public final String c() {
            return this.g;
        }

        @DexIgnore
        public final boolean d() {
            return this.f;
        }

        @DexIgnore
        public final String e() {
            return this.i;
        }

        @DexIgnore
        public final Lc6<Integer, String> f() {
            return this.e;
        }

        @DexIgnore
        public final boolean g() {
            return this.l;
        }

        @DexIgnore
        public final String h() {
            return this.k;
        }

        @DexIgnore
        public final String i() {
            return this.h;
        }

        @DexIgnore
        public final boolean j() {
            return this.m;
        }

        @DexIgnore
        public final Di k() {
            return this.b;
        }

        @DexIgnore
        public final String l() {
            return this.c;
        }

        @DexIgnore
        public final Integer m() {
            return this.d;
        }

        @DexIgnore
        public final void n(Di di) {
            this.b = di;
        }

        @DexIgnore
        public final void o(String str) {
            this.c = str;
        }

        @DexIgnore
        public final void p(boolean z, Di di, String str, Integer num, Lc6<Integer, String> lc6, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4) {
            synchronized (this) {
                this.a = z;
                this.b = di;
                this.c = str;
                this.d = num;
                this.e = lc6;
                this.f = z2;
                this.g = str2;
                this.h = str3;
                this.i = str4;
                this.j = str5;
                this.k = str6;
                this.l = z3;
                this.m = z4;
            }
        }

        @DexIgnore
        public String toString() {
            String t = new Gson().t(this);
            Wg6.b(t, "Gson().toJson(this)");
            return t;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di {
        @DexIgnore
        public Device a;
        @DexIgnore
        public String b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public Boolean e;

        @DexIgnore
        public Di(Device device, String str, boolean z, boolean z2, Boolean bool) {
            Wg6.c(device, "deviceModel");
            Wg6.c(str, "deviceName");
            this.a = device;
            this.b = str;
            this.c = z;
            this.d = z2;
            this.e = bool;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Di(Device device, String str, boolean z, boolean z2, Boolean bool, int i, Qg6 qg6) {
            this(device, str, z, z2, (i & 16) != 0 ? null : bool);
        }

        @DexIgnore
        public final Device a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final Boolean c() {
            return this.e;
        }

        @DexIgnore
        public final boolean d() {
            return this.d;
        }

        @DexIgnore
        public final boolean e() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Di) {
                    Di di = (Di) obj;
                    if (!Wg6.a(this.a, di.a) || !Wg6.a(this.b, di.b) || this.c != di.c || this.d != di.d || !Wg6.a(this.e, di.e)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 1;
            int i2 = 0;
            Device device = this.a;
            int hashCode = device != null ? device.hashCode() : 0;
            String str = this.b;
            int hashCode2 = str != null ? str.hashCode() : 0;
            boolean z = this.c;
            if (z) {
                z = true;
            }
            boolean z2 = this.d;
            if (!z2) {
                i = z2 ? 1 : 0;
            }
            Boolean bool = this.e;
            if (bool != null) {
                i2 = bool.hashCode();
            }
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = z ? 1 : 0;
            return (((((((hashCode * 31) + hashCode2) * 31) + i3) * 31) + i) * 31) + i2;
        }

        @DexIgnore
        public String toString() {
            return "WatchSettingWrapper(deviceModel=" + this.a + ", deviceName=" + this.b + ", isConnected=" + this.c + ", isActive=" + this.d + ", shouldShowVibrationUI=" + this.e + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$checkToReconnectOrSwitchActiveDevice$1", f = "WatchSettingViewModel.kt", l = {MFNetworkReturnCode.BAD_REQUEST}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $currentActiveSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$checkToReconnectOrSwitchActiveDevice$1$challenge$1", f = "WatchSettingViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Ps4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ps4> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.z.g(new String[]{"running", "waiting"}, Xy4.a.a());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(WatchSettingViewModel watchSettingViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchSettingViewModel;
            this.$currentActiveSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$currentActiveSerial, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Boolean b = this.this$0.w.b();
                Wg6.b(b, "mSharedPreferencesManager.bcStatus()");
                if (b.booleanValue()) {
                    Dv7 b2 = Bw7.b();
                    Aii aii = new Aii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    g = Eu7.g(b2, aii, this);
                    if (g == d) {
                        return d;
                    }
                } else {
                    if (!Vt7.l(this.$currentActiveSerial)) {
                        this.this$0.d0(this.$currentActiveSerial, 0);
                    } else {
                        WatchSettingViewModel watchSettingViewModel = this.this$0;
                        Object e = watchSettingViewModel.j.e();
                        if (e != null) {
                            Wg6.b(e, "mSerialLiveData.value!!");
                            watchSettingViewModel.I((String) e, 1);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                    return Cd6.a;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Ps4 ps4 = (Ps4) g;
            if (ps4 != null) {
                Hq4.d(this.this$0, false, false, null, 6, null);
                this.this$0.q.l(new Ai(true, true, ps4));
            } else {
                this.this$0.w.m0(Ao7.a(false));
                if (!Vt7.l(this.$currentActiveSerial)) {
                    this.this$0.d0(this.$currentActiveSerial, 0);
                } else {
                    WatchSettingViewModel watchSettingViewModel2 = this.this$0;
                    Object e2 = watchSettingViewModel2.j.e();
                    if (e2 != null) {
                        Wg6.b(e2, "mSerialLiveData.value!!");
                        watchSettingViewModel2.I((String) e2, 1);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$checkingBeforeRemoveDevice$1", f = "WatchSettingViewModel.kt", l = {545}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isSwitch;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$checkingBeforeRemoveDevice$1$challenge$1", f = "WatchSettingViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Ps4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ps4> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.z.g(new String[]{"running", "waiting"}, Xy4.a.a());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(WatchSettingViewModel watchSettingViewModel, boolean z, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchSettingViewModel;
            this.$isSwitch = z;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, this.$isSwitch, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Boolean b = this.this$0.w.b();
                Wg6.b(b, "mSharedPreferencesManager.bcStatus()");
                if (b.booleanValue()) {
                    String J = PortfolioApp.get.instance().J();
                    Device device = (Device) this.this$0.k.e();
                    if (!Wg6.a(J, device != null ? device.getDeviceId() : null)) {
                        this.this$0.q.l(new Ai(false, this.$isSwitch, null));
                    } else {
                        Dv7 b2 = Bw7.b();
                        Aii aii = new Aii(this, null);
                        this.L$0 = il6;
                        this.L$1 = J;
                        this.label = 1;
                        g = Eu7.g(b2, aii, this);
                        if (g == d) {
                            return d;
                        }
                    }
                } else {
                    this.this$0.q.l(new Ai(false, this.$isSwitch, null));
                }
                Hq4.d(this.this$0, false, false, null, 6, null);
                return Cd6.a;
            } else if (i == 1) {
                String str = (String) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Ps4 ps4 = (Ps4) g;
            if (ps4 == null) {
                this.this$0.w.m0(Ao7.a(false));
                this.this$0.q.l(new Ai(false, this.$isSwitch, null));
            } else {
                this.this$0.q.l(new Ai(true, this.$isSwitch, ps4));
            }
            Hq4.d(this.this$0, false, false, null, 6, null);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements CoroutineUseCase.Ei<UnlinkDeviceUseCase.Di, UnlinkDeviceUseCase.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public Gi(WatchSettingViewModel watchSettingViewModel, String str) {
            this.a = watchSettingViewModel;
            this.b = str;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(UnlinkDeviceUseCase.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(UnlinkDeviceUseCase.Ci ci) {
            Wg6.c(ci, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.C.a();
            local.d(a2, "remove device " + this.a.j + ".value fail due to " + ci.b());
            Hq4.d(this.a, false, true, null, 5, null);
            String b2 = ci.b();
            switch (b2.hashCode()) {
                case -1767173543:
                    if (!b2.equals("UNLINK_FAIL_DUE_TO_STOP_WORKOUT_FAIL")) {
                        return;
                    }
                    break;
                case -1697024179:
                    if (b2.equals("UNLINK_FAIL_DUE_TO_LACK_PERMISSION")) {
                        if (ci.c() != null) {
                            List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(new ArrayList(ci.c()));
                            Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                            WatchSettingViewModel watchSettingViewModel = this.a;
                            Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                            if (array != null) {
                                Uh5[] uh5Arr = (Uh5[]) array;
                                watchSettingViewModel.e((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                            } else {
                                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                            }
                        }
                        this.a.J();
                        return;
                    }
                    return;
                case -643272338:
                    if (b2.equals("UNLINK_FAIL_ON_SERVER")) {
                        Integer num = (ci.c() == null || !(ci.c().isEmpty() ^ true)) ? 600 : ci.c().get(0);
                        Wg6.b(num, "if (errorValue.subErrorC\u2026                        }");
                        int intValue = num.intValue();
                        Ci ci2 = this.a.m;
                        String a3 = ci.a();
                        Ci.q(ci2, false, null, null, null, new Lc6(Integer.valueOf(intValue), a3 != null ? a3 : ""), false, null, null, null, null, null, false, false, 8175, null);
                        this.a.J();
                        return;
                    }
                    return;
                case 1447890910:
                    if (!b2.equals("UNLINK_FAIL_DUE_TO_SYNC_FAIL")) {
                        return;
                    }
                    break;
                case 1768665169:
                    if (b2.equals("UNLINK_FAIL_DUE_TO_PENDING_WORKOUT")) {
                        Ci.q(this.a.m, false, null, null, null, null, false, null, null, null, this.b, null, false, false, 7679, null);
                        this.a.J();
                        return;
                    }
                    return;
                default:
                    return;
            }
            Ci.q(this.a.m, false, null, null, null, null, false, null, null, null, null, this.b, false, false, 7167, null);
            this.a.J();
        }

        @DexIgnore
        public void c(UnlinkDeviceUseCase.Di di) {
            Wg6.c(di, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.C.a();
            local.d(a2, "removeDevice success serial=" + this.a.j + ".value");
            Hq4.d(this.a, false, true, null, 5, null);
            Ci.q(this.a.m, true, null, null, null, null, false, null, null, null, null, null, false, false, 8190, null);
            this.a.J();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(UnlinkDeviceUseCase.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements CoroutineUseCase.Ei<SwitchActiveDeviceUseCase.Di, SwitchActiveDeviceUseCase.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public Hi(WatchSettingViewModel watchSettingViewModel, String str) {
            this.a = watchSettingViewModel;
            this.b = str;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(SwitchActiveDeviceUseCase.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(SwitchActiveDeviceUseCase.Ci ci) {
            Wg6.c(ci, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.C.a();
            local.d(a2, "removeDevice switch to " + this.b + " fail due to " + ci.b());
            Hq4.d(this.a, false, true, null, 5, null);
            int b2 = ci.b();
            if (b2 == 113) {
                if (ci.c() != null) {
                    List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(new ArrayList(ci.c()));
                    Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                    WatchSettingViewModel watchSettingViewModel = this.a;
                    Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                    if (array != null) {
                        Uh5[] uh5Arr = (Uh5[]) array;
                        watchSettingViewModel.e((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    } else {
                        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                this.a.J();
            } else if (b2 == 114) {
                Integer num = (ci.c() == null || !(ci.c().isEmpty() ^ true)) ? 600 : ci.c().get(0);
                Wg6.b(num, "if (errorValue.subErrorC\u2026                        }");
                int intValue = num.intValue();
                Ci ci2 = this.a.m;
                String a3 = ci.a();
                Ci.q(ci2, false, null, null, null, new Lc6(Integer.valueOf(intValue), a3 != null ? a3 : ""), false, null, null, null, null, null, false, false, 8175, null);
                this.a.J();
            } else if (b2 == 117) {
                Ci.q(this.a.m, false, null, null, null, null, false, null, null, this.b, null, null, false, false, 7935, null);
                this.a.J();
            }
        }

        @DexIgnore
        public void c(SwitchActiveDeviceUseCase.Di di) {
            Wg6.c(di, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.C.a();
            local.d(a2, "removeDevice(), switch to newDevice=" + this.b + " success");
            Hq4.d(this.a, false, true, null, 5, null);
            this.a.a0(this.b);
            this.a.Q(di.a());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(SwitchActiveDeviceUseCase.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ii(WatchSettingViewModel watchSettingViewModel) {
            this.a = watchSettingViewModel;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Device device;
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.C.a();
            local.d(a2, "onConnectionStateChangeReceiver, serial=" + stringExtra);
            if (Wg6.a(stringExtra, (String) this.a.j.e()) && Vt7.j(stringExtra, this.a.A.J(), true)) {
                FLogger.INSTANCE.getLocal().d(WatchSettingViewModel.C.a(), "onConnectionStateChanged");
                LiveData liveData = this.a.k;
                if (!(liveData == null || (device = (Device) liveData.e()) == null)) {
                    this.a.Q(device);
                }
                Hq4.d(this.a, false, true, null, 5, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;

        @DexIgnore
        public Ji(WatchSettingViewModel watchSettingViewModel) {
            this.a = watchSettingViewModel;
        }

        @DexIgnore
        public final LiveData<Device> a(String str) {
            DeviceRepository deviceRepository = this.a.s;
            Wg6.b(str, "serial");
            return deviceRepository.getDeviceBySerialAsLiveData(str);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((String) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel b;

        @DexIgnore
        public Ki(WatchSettingViewModel watchSettingViewModel) {
            this.b = watchSettingViewModel;
        }

        @DexIgnore
        public final void run() {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li<T> implements Ls0<Device> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;

        @DexIgnore
        public Li(WatchSettingViewModel watchSettingViewModel) {
            this.a = watchSettingViewModel;
        }

        @DexIgnore
        public final void a(Device device) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.C.a();
            local.d(a2, "on device changed " + device);
            this.a.Q(device);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Device device) {
            a(device);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Device $device$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Device $this_run;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super String>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Mi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Mi mi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = mi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super String> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.s.getDeviceNameBySerial(this.this$0.$this_run.getDeviceId());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Mi(Device device, Xe6 xe6, WatchSettingViewModel watchSettingViewModel, Device device2) {
            super(2, xe6);
            this.$this_run = device;
            this.this$0 = watchSettingViewModel;
            this.$device$inlined = device2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Mi mi = new Mi(this.$this_run, xe6, this.this$0, this.$device$inlined);
            mi.p$ = (Il6) obj;
            throw null;
            //return mi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Mi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            String str;
            boolean z;
            Il6 il6;
            boolean z2;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il62 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = WatchSettingViewModel.C.a();
                local.d(a2, "onDeviceChanged - device: " + this.$device$inlined);
                String J = PortfolioApp.get.instance().J();
                boolean z3 = !FossilDeviceSerialPatternUtil.isSamSlimDevice(this.$this_run.getDeviceId()) && !FossilDeviceSerialPatternUtil.isDianaDevice(this.$this_run.getDeviceId());
                if (this.$this_run.getVibrationStrength() == null) {
                    this.$this_run.setVibrationStrength(Ao7.e(50));
                    Cd6 cd6 = Cd6.a;
                }
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il62;
                this.L$1 = J;
                this.Z$0 = z3;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
                str = J;
                z = z3;
                il6 = il62;
            } else if (i == 1) {
                boolean z4 = this.Z$0;
                El7.b(obj);
                g = obj;
                str = (String) this.L$1;
                z = z4;
                il6 = (Il6) this.L$0;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str2 = (String) g;
            if (this.$this_run.getBatteryLevel() > 100) {
                this.$this_run.setBatteryLevel(100);
            }
            if (Wg6.a(this.$this_run.getDeviceId(), str)) {
                try {
                    IButtonConnectivity b2 = PortfolioApp.get.b();
                    z2 = b2 != null && b2.getGattState(this.$this_run.getDeviceId()) == 2;
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = WatchSettingViewModel.C.a();
                    local2.e(a3, "exception when get gatt state of " + il6);
                    z2 = false;
                }
                this.this$0.l = new Di(this.$device$inlined, str2, z2, true, Ao7.a(z));
            } else {
                this.this$0.l = new Di(this.$device$inlined, str2, false, false, Ao7.a(z));
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String a4 = WatchSettingViewModel.C.a();
            local3.d(a4, "onDeviceChanged, mDeviceWrapper=" + this.this$0.l);
            Ci ci = this.this$0.m;
            Di di = this.this$0.l;
            if (di != null) {
                Ci.q(ci, false, di, null, null, null, false, null, null, null, null, null, false, false, 8189, null);
                this.this$0.J();
                this.this$0.i.removeCallbacksAndMessages(null);
                this.this$0.e0();
                return Cd6.a;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements CoroutineUseCase.Ei<SwitchActiveDeviceUseCase.Di, SwitchActiveDeviceUseCase.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public Ni(WatchSettingViewModel watchSettingViewModel, String str) {
            this.a = watchSettingViewModel;
            this.b = str;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(SwitchActiveDeviceUseCase.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(SwitchActiveDeviceUseCase.Ci ci) {
            Wg6.c(ci, "errorValue");
            Integer num = (ci.c() == null || !(ci.c().isEmpty() ^ true)) ? 600 : ci.c().get(0);
            Wg6.b(num, "if (errorValue.subErrorC\u2026                        }");
            int intValue = num.intValue();
            Hq4.d(this.a, false, true, null, 5, null);
            Ci ci2 = this.a.m;
            String a2 = ci.a();
            Ci.q(ci2, false, null, null, null, new Lc6(Integer.valueOf(intValue), a2 != null ? a2 : ""), false, null, null, null, null, null, false, false, 8175, null);
            this.a.J();
        }

        @DexIgnore
        public void c(SwitchActiveDeviceUseCase.Di di) {
            Wg6.c(di, "responseValue");
            Hq4.d(this.a, false, true, null, 5, null);
            this.a.a0(this.b);
            this.a.Q(di.a());
            this.a.Y();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(SwitchActiveDeviceUseCase.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi implements CoroutineUseCase.Ei<ReconnectDeviceUseCase.Ei, ReconnectDeviceUseCase.Di> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Oi(WatchSettingViewModel watchSettingViewModel) {
            this.a = watchSettingViewModel;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(ReconnectDeviceUseCase.Di di) {
            b(di);
        }

        @DexIgnore
        public void b(ReconnectDeviceUseCase.Di di) {
            Wg6.c(di, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.C.a();
            local.d(a2, "reconnectActiveDevice fail!! errorValue=" + di.a());
            Hq4.d(this.a, false, true, null, 5, null);
            int c = di.c();
            if (c == 1101 || c == 1112 || c == 1113) {
                List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(di.b());
                Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                WatchSettingViewModel watchSettingViewModel = this.a;
                Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                if (array != null) {
                    Uh5[] uh5Arr = (Uh5[]) array;
                    watchSettingViewModel.e((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    this.a.J();
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            Ci.q(this.a.m, false, null, null, null, null, true, null, null, null, null, null, false, false, 8159, null);
            this.a.J();
        }

        @DexIgnore
        public void c(ReconnectDeviceUseCase.Ei ei) {
            Wg6.c(ei, "responseValue");
            Hq4.d(this.a, false, true, null, 5, null);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(ReconnectDeviceUseCase.Ei ei) {
            c(ei);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi implements CoroutineUseCase.Ei<Ut5, St5> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public Pi(WatchSettingViewModel watchSettingViewModel, String str, int i) {
            this.a = watchSettingViewModel;
            this.b = str;
            this.c = i;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(St5 st5) {
            b(st5);
        }

        @DexIgnore
        public void b(St5 st5) {
            Wg6.c(st5, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.C.a();
            local.d(a2, "syncDevice fail - serial=" + this.b + " - errorCode=" + st5.a());
            int i = G17.a[st5.a().ordinal()];
            if (i == 1) {
                FLogger.INSTANCE.getLocal().d(WatchSettingViewModel.C.a(), "Sync of this device is in progress, keep waiting for result");
            } else if (i == 2) {
                if (st5.b() != null) {
                    List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(new ArrayList(st5.b()));
                    Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                    WatchSettingViewModel watchSettingViewModel = this.a;
                    Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                    if (array != null) {
                        Uh5[] uh5Arr = (Uh5[]) array;
                        watchSettingViewModel.e((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                        this.a.J();
                    } else {
                        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                Hq4.d(this.a, false, true, null, 5, null);
            } else if (i == 3) {
                int i2 = this.c;
                if (i2 == 0) {
                    Ci.q(this.a.m, false, null, null, null, null, false, this.b, null, null, null, null, false, false, 8127, null);
                    this.a.J();
                } else if (i2 == 1) {
                    Ci.q(this.a.m, false, null, null, null, null, false, null, null, null, this.b, null, false, false, 7679, null);
                    this.a.J();
                }
                Hq4.d(this.a, false, true, null, 5, null);
            } else if (i == 4) {
                int i3 = this.c;
                if (i3 == 0) {
                    Ci ci = this.a.m;
                    Object e = this.a.j.e();
                    if (e != null) {
                        Ci.q(ci, false, null, null, null, null, false, null, (String) e, null, null, null, false, false, 8063, null);
                        this.a.J();
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else if (i3 == 1) {
                    Ci ci2 = this.a.m;
                    Object e2 = this.a.j.e();
                    if (e2 != null) {
                        Ci.q(ci2, false, null, null, null, null, false, null, null, null, null, (String) e2, false, false, 7167, null);
                        this.a.J();
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                Hq4.d(this.a, false, true, null, 5, null);
            } else if (i != 5) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = WatchSettingViewModel.C.a();
                local2.d(a3, "errorValue.lastErrorCode=" + st5.a());
                Ci.q(this.a.m, false, null, null, null, null, true, null, null, null, null, null, false, false, 8159, null);
                this.a.J();
                Hq4.d(this.a, false, true, null, 5, null);
            } else {
                FLogger.INSTANCE.getLocal().d(WatchSettingViewModel.C.a(), "User deny stopping workout");
                String build = ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SYNC_CURRENT_DEVICE, ErrorCodeBuilder.Component.APP, ErrorCodeBuilder.AppError.USER_CANCELLED);
                int i4 = this.c;
                if (i4 == 0) {
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.SWITCH_DEVICE;
                    Object e3 = this.a.j.e();
                    if (e3 != null) {
                        Wg6.b(e3, "mSerialLiveData.value!!");
                        remote.e(component, session, (String) e3, WatchSettingViewModel.C.a(), build, ErrorCodeBuilder.Step.SYNC_CURRENT_DEVICE, "User deny stopping workout");
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else if (i4 == 1) {
                    FLogger.INSTANCE.getRemote().e(FLogger.Component.APP, FLogger.Session.REMOVE_DEVICE, this.b, WatchSettingViewModel.C.a(), build, ErrorCodeBuilder.Step.SYNC_CURRENT_DEVICE, "User deny stopping workout");
                }
                Hq4.d(this.a, false, true, null, 5, null);
            }
        }

        @DexIgnore
        public void c(Ut5 ut5) {
            Wg6.c(ut5, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.C.a();
            local.d(a2, "syncDevice success - serial=" + this.b);
            int i = this.c;
            if (i == 0) {
                WatchSettingViewModel watchSettingViewModel = this.a;
                Object e = watchSettingViewModel.j.e();
                if (e != null) {
                    Wg6.b(e, "mSerialLiveData.value!!");
                    watchSettingViewModel.I((String) e, 1);
                    return;
                }
                Wg6.i();
                throw null;
            } else if (i == 1) {
                WatchSettingViewModel watchSettingViewModel2 = this.a;
                Object e2 = watchSettingViewModel2.j.e();
                if (e2 != null) {
                    watchSettingViewModel2.H((String) e2);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(Ut5 ut5) {
            c(ut5);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Qi implements CoroutineUseCase.Ei<SetVibrationStrengthUseCase.Di, SetVibrationStrengthUseCase.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ Device a;
        @DexIgnore
        public /* final */ /* synthetic */ WatchSettingViewModel b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public Qi(Device device, String str, WatchSettingViewModel watchSettingViewModel, int i) {
            this.a = device;
            this.b = watchSettingViewModel;
            this.c = i;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(SetVibrationStrengthUseCase.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(SetVibrationStrengthUseCase.Ci ci) {
            Wg6.c(ci, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchSettingViewModel.C.a();
            local.d(a2, "updateVibrationLevel fail!! errorValue=" + ci.a());
            Hq4.d(this.b, false, true, null, 5, null);
            int c2 = ci.c();
            if (c2 != 1101) {
                if (c2 == 8888) {
                    Ci.q(this.b.m, false, null, null, null, null, false, null, null, null, null, null, true, false, 6143, null);
                    this.b.J();
                    return;
                } else if (!(c2 == 1112 || c2 == 1113)) {
                    Ci.q(this.b.m, false, null, null, null, null, true, null, null, null, null, null, false, false, 8159, null);
                    this.b.J();
                    return;
                }
            }
            List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(ci.b());
            Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            WatchSettingViewModel watchSettingViewModel = this.b;
            Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
            if (array != null) {
                Uh5[] uh5Arr = (Uh5[]) array;
                watchSettingViewModel.e((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                this.b.J();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public void c(SetVibrationStrengthUseCase.Di di) {
            Device a2;
            Wg6.c(di, "responseValue");
            Integer vibrationStrength = this.a.getVibrationStrength();
            Hq4.d(this.b, false, true, null, 5, null);
            Ci.q(this.b.m, false, null, null, Integer.valueOf(this.c), null, false, null, null, null, null, null, false, false, 8183, null);
            this.b.J();
            Di di2 = this.b.l;
            if (!(di2 == null || (a2 = di2.a()) == null)) {
                a2.setVibrationStrength(vibrationStrength);
            }
            FLogger.INSTANCE.getLocal().d(WatchSettingViewModel.C.a(), "updateVibrationLevel success");
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(SetVibrationStrengthUseCase.Di di) {
            c(di);
        }
    }

    /*
    static {
        String simpleName = WatchSettingViewModel.class.getSimpleName();
        Wg6.b(simpleName, "WatchSettingViewModel::class.java.simpleName");
        B = simpleName;
    }
    */

    @DexIgnore
    public WatchSettingViewModel(DeviceRepository deviceRepository, SetVibrationStrengthUseCase setVibrationStrengthUseCase, UnlinkDeviceUseCase unlinkDeviceUseCase, Cj4 cj4, An4 an4, SwitchActiveDeviceUseCase switchActiveDeviceUseCase, ReconnectDeviceUseCase reconnectDeviceUseCase, Tt4 tt4, PortfolioApp portfolioApp) {
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(setVibrationStrengthUseCase, "mSetVibrationStrengthUseCase");
        Wg6.c(unlinkDeviceUseCase, "mUnlinkDeviceUseCase");
        Wg6.c(cj4, "mDeviceSettingFactory");
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(switchActiveDeviceUseCase, "mSwitchActiveDeviceUseCase");
        Wg6.c(reconnectDeviceUseCase, "mReconnectDeviceUseCase");
        Wg6.c(tt4, "challengeRepository");
        Wg6.c(portfolioApp, "mApp");
        this.s = deviceRepository;
        this.t = setVibrationStrengthUseCase;
        this.u = unlinkDeviceUseCase;
        this.v = cj4;
        this.w = an4;
        this.x = switchActiveDeviceUseCase;
        this.y = reconnectDeviceUseCase;
        this.z = tt4;
        this.A = portfolioApp;
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        this.j = mutableLiveData;
        LiveData<Device> c = Ss0.c(mutableLiveData, new Ji(this));
        Wg6.b(c, "Transformations.switchMa\u2026lAsLiveData(serial)\n    }");
        this.k = c;
        this.m = new Ci(false, null, null, null, null, false, null, null, null, null, null, false, false, 8191, null);
        this.o = new Ki(this);
        this.p = new Li(this);
        this.q = new MutableLiveData<>();
        this.r = new Ii(this);
    }

    @DexIgnore
    public final void F() {
        FLogger.INSTANCE.getLocal().d(B, "checkToReconnectOrSwitchActiveDevice");
        String J = this.A.J();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = B;
        local.d(str, "activeSerial=" + J + ", mSerialLiveData=" + this.j.e());
        if (Wg6.a(this.j.e(), J)) {
            Y();
        } else if (!Hx5.b(PortfolioApp.get.instance())) {
            Ci.q(this.m, false, null, null, null, new Lc6(601, ""), false, null, null, null, null, null, false, false, 8175, null);
            J();
        } else {
            Hq4.d(this, true, false, null, 6, null);
            Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ei(this, J, null), 3, null);
        }
    }

    @DexIgnore
    public final void G(boolean z2) {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Fi(this, z2, null), 3, null);
    }

    @DexIgnore
    public final void H(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = B;
        local.d(str2, "removeDevice " + str);
        this.u.e(str != null ? new UnlinkDeviceUseCase.Bi(str) : null, new Gi(this, str));
    }

    @DexIgnore
    public final void I(String str, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = B;
        local.d(str2, "switch to device " + str + " mode " + i2);
        this.x.e(new SwitchActiveDeviceUseCase.Bi(str, i2), new Hi(this, str));
    }

    @DexIgnore
    public final void J() {
        this.m.n(M());
        this.m.o(N());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = B;
        local.d(str, ".emitUIState(), mUiModelWrapper=" + this.m);
        this.h.l(this.m);
    }

    @DexIgnore
    public final LiveData<Ai> K() {
        return this.q;
    }

    @DexIgnore
    public final String L() {
        String b;
        Di di = this.l;
        return (di == null || (b = di.b()) == null) ? "" : b;
    }

    @DexIgnore
    public final Di M() {
        return this.l;
    }

    @DexIgnore
    public final String N() {
        return this.n;
    }

    @DexIgnore
    public final MutableLiveData<Ci> O() {
        return this.h;
    }

    @DexIgnore
    public final String P() {
        return this.j.e();
    }

    @DexIgnore
    public final void Q(Device device) {
        if (device != null) {
            Rm6 unused = Gu7.d(Us0.a(this), null, null, new Mi(device, null, this, device), 3, null);
        }
    }

    @DexIgnore
    public final void R(String str) {
        Wg6.c(str, "serial");
        Hq4.d(this, true, false, null, 6, null);
        H(str);
    }

    @DexIgnore
    public final void S(String str) {
        Wg6.c(str, "serial");
        Hq4.d(this, true, false, null, 6, null);
        this.A.x(str, true);
    }

    @DexIgnore
    public final void T(String str) {
        Wg6.c(str, "serial");
        Hq4.d(this, true, false, null, 6, null);
        this.A.x(str, true);
    }

    @DexIgnore
    public final void U(String str) {
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = B;
        local.d(str2, "force switch to device " + str);
        Hq4.d(this, true, false, null, 6, null);
        this.x.e(new SwitchActiveDeviceUseCase.Bi(str, 4), new Ni(this, str));
    }

    @DexIgnore
    public final void V(String str) {
        Wg6.c(str, "serial");
        Hq4.d(this, true, false, null, 6, null);
        I(str, 2);
    }

    @DexIgnore
    public final void W(String str) {
        Wg6.c(str, "serial");
        Hq4.d(this, true, false, null, 6, null);
        this.A.x(str, false);
    }

    @DexIgnore
    public final void X(String str) {
        Wg6.c(str, "serial");
        Hq4.d(this, true, false, null, 6, null);
        this.A.x(str, false);
    }

    @DexIgnore
    public final void Y() {
        FLogger.INSTANCE.getLocal().d(B, "reconnectActiveDevice");
        Hq4.d(this, true, false, null, 6, null);
        ReconnectDeviceUseCase reconnectDeviceUseCase = this.y;
        String e = this.j.e();
        if (e != null) {
            Wg6.b(e, "mSerialLiveData.value!!");
            reconnectDeviceUseCase.e(new ReconnectDeviceUseCase.Ci(e), new Oi(this));
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void Z() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = B;
        local.d(str, "removeDevice serial=" + this.j + ".value");
        Hq4.d(this, true, false, null, 6, null);
        String J = this.A.J();
        if (!Wg6.a(this.j.e(), J) || Vt7.l(J)) {
            H(this.j.e());
        } else {
            d0(J, 1);
        }
    }

    @DexIgnore
    public final void a0(String str) {
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = B;
        local.d(str2, "setWatchSerial, serial=" + str);
        this.j.o(str);
    }

    @DexIgnore
    public final void b0() {
        Ci.q(this.m, false, null, null, null, null, false, null, null, null, null, null, false, false, 8191, null);
        this.t.s();
        this.y.o();
        this.x.y();
        BleCommandResultManager.d.g(CommunicateMode.FORCE_CONNECT, CommunicateMode.SET_VIBRATION_STRENGTH, CommunicateMode.SWITCH_DEVICE);
        PortfolioApp portfolioApp = this.A;
        Ii ii = this.r;
        portfolioApp.registerReceiver(ii, new IntentFilter(this.A.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        LiveData<Device> liveData = this.k;
        if (liveData != null) {
            liveData.i(this.p);
        }
    }

    @DexIgnore
    public final void c0() {
        try {
            this.x.C();
            LiveData<Device> liveData = this.k;
            if (liveData != null) {
                liveData.m(this.p);
            }
            this.y.r();
            this.t.v();
            this.A.unregisterReceiver(this.r);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = B;
            local.e(str, "stop with " + e);
        }
        this.i.removeCallbacksAndMessages(null);
    }

    @DexIgnore
    public final void d0(String str, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = B;
        local.d(str2, "syncDevice - serial=" + str + ", userAction=" + i2);
        this.v.b(str).e(new Tt5(!DeviceHelper.o.w(FossilDeviceSerialPatternUtil.getDeviceBySerial(str)) ? 10 : 15, str, false), new Pi(this, str, i2));
    }

    @DexIgnore
    public final void e0() {
        String relativeTimeSpanString;
        String e = this.j.e();
        if (e != null) {
            if (TextUtils.equals(PortfolioApp.get.instance().J(), e)) {
                PortfolioApp portfolioApp = this.A;
                Wg6.b(e, "it");
                if (portfolioApp.A0(e)) {
                    relativeTimeSpanString = this.A.getString(2131886784);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = B;
                    local.d(str, "updateSyncTime " + relativeTimeSpanString);
                    Ci.q(this.m, false, null, relativeTimeSpanString.toString(), null, null, false, null, null, null, null, null, false, false, 8187, null);
                    this.n = relativeTimeSpanString.toString();
                    J();
                    this.i.postDelayed(this.o, 60000);
                }
            }
            long C2 = this.w.C(e);
            if (((int) C2) == 0) {
                relativeTimeSpanString = "";
            } else if (System.currentTimeMillis() - C2 < 60000) {
                Hr7 hr7 = Hr7.a;
                String c = Um5.c(PortfolioApp.get.instance(), 2131887191);
                Wg6.b(c, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                String format = String.format(c, Arrays.copyOf(new Object[]{1}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                relativeTimeSpanString = format;
            } else {
                relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(C2, System.currentTimeMillis(), TimeUnit.SECONDS.toMillis(1));
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = B;
            local2.d(str2, "updateSyncTime " + relativeTimeSpanString);
            Ci.q(this.m, false, null, relativeTimeSpanString.toString(), null, null, false, null, null, null, null, null, false, false, 8187, null);
            this.n = relativeTimeSpanString.toString();
            J();
            this.i.postDelayed(this.o, 60000);
        }
    }

    @DexIgnore
    public final void f0(int i2) {
        Di di;
        Device a2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = B;
        local.d(str, "updateVibrationLevel " + i2 + " of " + this.j.e());
        String e = this.j.e();
        if (e != null && (di = this.l) != null && (a2 = di.a()) != null) {
            Integer vibrationStrength = a2.getVibrationStrength();
            if ((vibrationStrength == null || vibrationStrength.intValue() != i2) && !FossilDeviceSerialPatternUtil.isSamSlimDevice(e) && !FossilDeviceSerialPatternUtil.isDianaDevice(e)) {
                Hq4.d(this, true, false, null, 6, null);
                SetVibrationStrengthUseCase setVibrationStrengthUseCase = this.t;
                Wg6.b(e, "it");
                setVibrationStrengthUseCase.e(new SetVibrationStrengthUseCase.Bi(e, i2), new Qi(a2, e, this, i2));
            }
        }
    }

    @DexIgnore
    public final boolean g0() {
        String e = this.j.e();
        if (e != null) {
            return !FossilDeviceSerialPatternUtil.isSamSlimDevice(e) && !FossilDeviceSerialPatternUtil.isDianaDevice(e);
        }
        return true;
    }
}
