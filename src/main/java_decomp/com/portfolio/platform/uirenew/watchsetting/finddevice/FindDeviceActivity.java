package com.portfolio.platform.uirenew.watchsetting.finddevice;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.W17;
import com.mapped.FindDeviceFragment;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FindDeviceActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public FindDevicePresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            Wg6.c(context, "context");
            Wg6.c(str, "serial");
            Intent intent = new Intent(context, FindDeviceActivity.class);
            intent.putExtra("SERIAL", str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDeviceActivity", "start with " + str);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        FindDeviceFragment findDeviceFragment = (FindDeviceFragment) getSupportFragmentManager().Y(2131362158);
        if (findDeviceFragment == null) {
            findDeviceFragment = FindDeviceFragment.w.a();
            k(findDeviceFragment, "FindDeviceFragment", 2131362158);
        }
        PortfolioApp.get.instance().getIface().S(new W17(findDeviceFragment)).a(this);
        if (bundle != null && bundle.containsKey("SERIAL")) {
            String string = bundle.getString("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String r = r();
            local.d(r, "retrieve serial from savedInstanceState " + string);
            if (string != null) {
                FindDevicePresenter findDevicePresenter = this.A;
                if (findDevicePresenter != null) {
                    findDevicePresenter.H(string);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        }
        if (getIntent() != null) {
            String stringExtra = getIntent().getStringExtra("SERIAL");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String r2 = r();
            local2.d(r2, "retrieve serial from intent " + stringExtra);
            FindDevicePresenter findDevicePresenter2 = this.A;
            if (findDevicePresenter2 != null) {
                Wg6.b(stringExtra, "serial");
                findDevicePresenter2.H(stringExtra);
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.c(bundle, "outState");
        FindDevicePresenter findDevicePresenter = this.A;
        if (findDevicePresenter != null) {
            bundle.putString("SERIAL", findDevicePresenter.B());
            super.onSaveInstanceState(bundle);
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }
}
