package com.portfolio.platform.uirenew.splash;

import android.os.Bundle;
import com.fossil.C27;
import com.fossil.K07;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SplashScreenActivity extends BaseActivity {
    @DexIgnore
    public SplashPresenter A;

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.portfolio.platform.ui.BaseActivity
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        C27 c27 = (C27) getSupportFragmentManager().Y(2131362158);
        if (c27 == null) {
            c27 = C27.i.a();
            i(c27, 2131362158);
        }
        PortfolioApp.get.instance().getIface().r(new K07(c27)).a(this);
    }
}
