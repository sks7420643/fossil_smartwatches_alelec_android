package com.portfolio.platform.uirenew.pairing.usecase;

import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Sy6;
import com.fossil.Ty6;
import com.fossil.Uy6;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hx5;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.util.NotificationAppHelper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetHybridDeviceSettingUseCase extends CoroutineUseCase<Ty6, Uy6, Sy6> {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ HybridPresetRepository e;
    @DexIgnore
    public /* final */ MicroAppRepository f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ NotificationsRepository h;
    @DexIgnore
    public /* final */ CategoryRepository i;
    @DexIgnore
    public /* final */ AlarmsRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$saveNotificationSettingToDevice$1", f = "GetHybridDeviceSettingUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ SparseArray $data;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isMovemberModel;
        @DexIgnore
        public /* final */ /* synthetic */ String $serialNumber;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(SparseArray sparseArray, boolean z, String str, Xe6 xe6) {
            super(2, xe6);
            this.$data = sparseArray;
            this.$isMovemberModel = z;
            this.$serialNumber = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.$data, this.$isMovemberModel, this.$serialNumber, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                AppNotificationFilterSettings b = NotificationAppHelper.b.b(this.$data, this.$isMovemberModel);
                PortfolioApp.get.instance().s1(b, this.$serialNumber);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = GetHybridDeviceSettingUseCase.k;
                local.d(str, "saveNotificationSettingToDevice, total: " + b.getNotificationFilters().size() + " items");
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase$start$1", f = "GetHybridDeviceSettingUseCase.kt", l = {60, 61, 62, 63, 64, 76, 84, 100}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GetHybridDeviceSettingUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = getHybridDeviceSettingUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:116:0x03df  */
        /* JADX WARNING: Removed duplicated region for block: B:118:0x03e3  */
        /* JADX WARNING: Removed duplicated region for block: B:122:0x03eb  */
        /* JADX WARNING: Removed duplicated region for block: B:124:0x03ef  */
        /* JADX WARNING: Removed duplicated region for block: B:126:0x03f3  */
        /* JADX WARNING: Removed duplicated region for block: B:128:0x03f7  */
        /* JADX WARNING: Removed duplicated region for block: B:141:0x02da A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00f0  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0154  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x01ad  */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x022f  */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x0247  */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x026a  */
        /* JADX WARNING: Removed duplicated region for block: B:73:0x028d  */
        /* JADX WARNING: Removed duplicated region for block: B:87:0x02df  */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x005c  */
        /* JADX WARNING: Removed duplicated region for block: B:92:0x032d  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 1050
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = GetHybridDeviceSettingUseCase.class.getSimpleName();
        Wg6.b(simpleName, "GetHybridDeviceSettingUs\u2026se::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public GetHybridDeviceSettingUseCase(HybridPresetRepository hybridPresetRepository, MicroAppRepository microAppRepository, DeviceRepository deviceRepository, NotificationsRepository notificationsRepository, CategoryRepository categoryRepository, AlarmsRepository alarmsRepository) {
        Wg6.c(hybridPresetRepository, "mPresetRepository");
        Wg6.c(microAppRepository, "mMicroAppRepository");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(notificationsRepository, "mNotificationsRepository");
        Wg6.c(categoryRepository, "mCategoryRepository");
        Wg6.c(alarmsRepository, "mAlarmsRepository");
        this.e = hybridPresetRepository;
        this.f = microAppRepository;
        this.g = deviceRepository;
        this.h = notificationsRepository;
        this.i = categoryRepository;
        this.j = alarmsRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return k;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ty6 ty6, Xe6 xe6) {
        return v(ty6, xe6);
    }

    @DexIgnore
    public Object v(Ty6 ty6, Xe6<? super Cd6> xe6) {
        if (ty6 == null) {
            i(new Sy6(600, ""));
            return Cd6.a;
        }
        this.d = ty6.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        StringBuilder sb = new StringBuilder();
        sb.append("Inside .GetHybridDeviceSettingUseCase executing!!! with serial=");
        String str2 = this.d;
        if (str2 != null) {
            sb.append(str2);
            local.d(str, sb.toString());
            if (!Hx5.b(PortfolioApp.get.instance())) {
                i(new Sy6(601, ""));
                return Cd6.a;
            }
            if (!TextUtils.isEmpty(this.d)) {
                DeviceHelper.Ai ai = DeviceHelper.o;
                String str3 = this.d;
                if (str3 == null) {
                    Wg6.i();
                    throw null;
                } else if (ai.v(str3)) {
                    x();
                    return Cd6.a;
                }
            }
            i(new Sy6(600, ""));
            return Cd6.a;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Rm6 w(SparseArray<List<BaseFeatureModel>> sparseArray, String str, boolean z) {
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Ai(sparseArray, z, str, null), 3, null);
    }

    @DexIgnore
    public final Rm6 x() {
        return Gu7.d(g(), null, null, new Bi(this, null), 3, null);
    }
}
