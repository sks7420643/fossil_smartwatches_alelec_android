package com.portfolio.platform.uirenew.pairing.scanning;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.Ao7;
import com.fossil.Ct0;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Hy6;
import com.fossil.Iy6;
import com.fossil.Jn5;
import com.fossil.Ko7;
import com.fossil.My6;
import com.fossil.Rx6;
import com.fossil.Sl5;
import com.fossil.St5;
import com.fossil.Tt5;
import com.fossil.Uh5;
import com.fossil.Ul5;
import com.fossil.Um5;
import com.fossil.Ut5;
import com.fossil.Vt7;
import com.fossil.Wt7;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingPresenter extends Hy6 {
    @DexIgnore
    public static /* final */ Ai z; // = new Ai(null);
    @DexIgnore
    public ShineDevice e;
    @DexIgnore
    public /* final */ List<Lc6<ShineDevice, String>> f; // = new ArrayList();
    @DexIgnore
    public /* final */ HashMap<String, List<Integer>> g; // = new HashMap<>();
    @DexIgnore
    public Handler h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ ArrayList<Device> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<SKUModel> l; // = new ArrayList<>();
    @DexIgnore
    public Ul5 m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public /* final */ Di q; // = new Di(this);
    @DexIgnore
    public /* final */ Ei r; // = new Ei(this);
    @DexIgnore
    public /* final */ Iy6 s;
    @DexIgnore
    public /* final */ LinkDeviceUseCase t;
    @DexIgnore
    public /* final */ DeviceRepository u;
    @DexIgnore
    public /* final */ Cj4 v;
    @DexIgnore
    public /* final */ MusicControlComponent w;
    @DexIgnore
    public /* final */ SetNotificationUseCase x;
    @DexIgnore
    public /* final */ An4 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            String simpleName = PairingPresenter.class.getSimpleName();
            Wg6.b(simpleName, "PairingPresenter::class.java.simpleName");
            return simpleName;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi implements Runnable {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi() {
        }

        @DexIgnore
        public void run() {
            PairingPresenter.this.l0(true);
            if (PairingPresenter.this.c0().isEmpty()) {
                PairingPresenter.this.s.x4();
                return;
            }
            Iy6 iy6 = PairingPresenter.this.s;
            PairingPresenter pairingPresenter = PairingPresenter.this;
            iy6.o4(pairingPresenter.W(pairingPresenter.c0()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$1", f = "PairingPresenter.kt", l = {425}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ShineDevice $shineDevice;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$1$deviceName$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super String>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super String> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    DeviceRepository deviceRepository = this.this$0.this$0.u;
                    String serial = this.this$0.$shineDevice.getSerial();
                    Wg6.b(serial, "shineDevice.serial");
                    return deviceRepository.getDeviceNameBySerial(serial);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(PairingPresenter pairingPresenter, ShineDevice shineDevice, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = pairingPresenter;
            this.$shineDevice = shineDevice;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$shineDevice, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            T t;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 h = this.this$0.h();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(h, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str = (String) g;
            Iterator<T> it = this.this$0.c0().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (Ao7.a(Wg6.a(((ShineDevice) next.getFirst()).getSerial(), this.$shineDevice.getSerial())).booleanValue()) {
                    t = next;
                    break;
                }
            }
            T t2 = t;
            if (t2 == null) {
                this.this$0.c0().add(new Lc6<>(this.$shineDevice, str));
            } else {
                ((ShineDevice) t2.getFirst()).updateRssi(this.$shineDevice.getRssi());
            }
            Iy6 iy6 = this.this$0.s;
            PairingPresenter pairingPresenter = this.this$0;
            iy6.t2(pairingPresenter.W(pairingPresenter.c0()));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Ei<LinkDeviceUseCase.Ji, LinkDeviceUseCase.Ii> {
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1", f = "PairingPresenter.kt", l = {312}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ LinkDeviceUseCase.Ii $errorValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1$params$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super HashMap<String, String>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super HashMap<String, String>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        Aii aii = this.this$0;
                        return aii.this$0.a.f0(aii.$errorValue.a());
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, LinkDeviceUseCase.Ii ii, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
                this.$errorValue = ii;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$errorValue, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = PairingPresenter.z.a();
                    local.d(a2, "Inside .startPairClosestDevice pairDevice fail with error=" + this.$errorValue.b());
                    Dv7 h = this.this$0.a.h();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    g = Eu7.g(h, aiii, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                HashMap hashMap = (HashMap) g;
                if (hashMap.containsKey("Style_Number") && hashMap.containsKey("Device_Name")) {
                    this.this$0.a.U("pair_fail", hashMap);
                }
                this.this$0.a.s.a();
                LinkDeviceUseCase.Ii ii = this.$errorValue;
                if (ii instanceof LinkDeviceUseCase.Di) {
                    if (this.this$0.a.p) {
                        Iy6 iy6 = this.this$0.a.s;
                        ShineDevice shineDevice = this.this$0.a.e;
                        if (shineDevice != null) {
                            String serial = shineDevice.getSerial();
                            Wg6.b(serial, "mPairingDevice!!.serial");
                            iy6.i2(serial);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Iy6 iy62 = this.this$0.a.s;
                        ShineDevice shineDevice2 = this.this$0.a.e;
                        if (shineDevice2 != null) {
                            String serial2 = shineDevice2.getSerial();
                            Wg6.b(serial2, "mPairingDevice!!.serial");
                            iy62.l4(serial2);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                } else if (ii instanceof LinkDeviceUseCase.Ki) {
                    this.this$0.a.s.h4(this.$errorValue.b(), this.$errorValue.a(), this.$errorValue.c());
                } else if (ii instanceof LinkDeviceUseCase.Ei) {
                    this.this$0.a.s.D0();
                }
                String valueOf = String.valueOf(this.$errorValue.b());
                Sl5 b = AnalyticsHelper.f.b("setup_device_session_optional_error");
                b.a("error_code", valueOf);
                Ul5 Z = this.this$0.a.Z();
                if (Z != null) {
                    Z.a(b);
                }
                Ul5 Z2 = this.this$0.a.Z();
                if (Z2 != null) {
                    Z2.c(valueOf);
                }
                AnalyticsHelper.f.k("setup_device_session");
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1", f = "PairingPresenter.kt", l = {283, 289, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $devicePairingSerial;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Rm6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Bii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Bii bii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = bii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Rm6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    String str;
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        String deviceNameBySerial = this.this$0.this$0.a.u.getDeviceNameBySerial(this.this$0.$devicePairingSerial);
                        MisfitDeviceProfile m = DeviceHelper.o.j().m(this.this$0.$devicePairingSerial);
                        if (m == null || (str = m.getFirmwareVersion()) == null) {
                            str = "";
                        }
                        AnalyticsHelper.f.g().h(DeviceHelper.o.m(this.this$0.$devicePairingSerial), deviceNameBySerial, str);
                        return this.this$0.this$0.a.x.e(new SetNotificationUseCase.Ai(this.this$0.$devicePairingSerial), null);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1$params$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Biii extends Ko7 implements Coroutine<Il6, Xe6<? super HashMap<String, String>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Bii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Bii bii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = bii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Biii biii = new Biii(this.this$0, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
                    //return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super HashMap<String, String>> xe6) {
                    throw null;
                    //return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        Bii bii = this.this$0;
                        return bii.this$0.a.f0(bii.$devicePairingSerial);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Di di, String str, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
                this.$devicePairingSerial = str;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, this.$devicePairingSerial, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0044  */
            /* JADX WARNING: Removed duplicated region for block: B:15:0x0064  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x00ab  */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x00cd  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 0
                    r6 = 3
                    r5 = 2
                    r4 = 1
                    java.lang.Object r3 = com.fossil.Yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x00ae
                    if (r0 == r4) goto L_0x0066
                    if (r0 == r5) goto L_0x002f
                    if (r0 != r6) goto L_0x0027
                    java.lang.Object r0 = r8.L$1
                    java.util.HashMap r0 = (java.util.HashMap) r0
                    java.lang.Object r0 = r8.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r9)
                L_0x001d:
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$Di r0 = r8.this$0
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter r0 = r0.a
                    r0.j0()
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0026:
                    return r0
                L_0x0027:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x002f:
                    java.lang.Object r0 = r8.L$1
                    java.util.HashMap r0 = (java.util.HashMap) r0
                    java.lang.Object r1 = r8.L$0
                    com.mapped.Il6 r1 = (com.mapped.Il6) r1
                    com.fossil.El7.b(r9)
                L_0x003a:
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$Di r2 = r8.this$0
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter r2 = r2.a
                    com.fossil.Ul5 r2 = r2.Z()
                    if (r2 == 0) goto L_0x0049
                    java.lang.String r4 = ""
                    r2.c(r4)
                L_0x0049:
                    com.portfolio.platform.helper.AnalyticsHelper$Ai r2 = com.portfolio.platform.helper.AnalyticsHelper.f
                    java.lang.String r4 = "setup_device_session"
                    r2.k(r4)
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$Di r2 = r8.this$0
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter r2 = r2.a
                    com.portfolio.platform.service.musiccontrol.MusicControlComponent r2 = com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter.H(r2)
                    r8.L$0 = r1
                    r8.L$1 = r0
                    r8.label = r6
                    java.lang.Object r0 = r2.n(r8)
                    if (r0 != r3) goto L_0x001d
                    r0 = r3
                    goto L_0x0026
                L_0x0066:
                    java.lang.Object r0 = r8.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r9)
                    r2 = r0
                    r1 = r9
                L_0x006f:
                    r0 = r1
                    java.util.HashMap r0 = (java.util.HashMap) r0
                    java.lang.String r1 = "Style_Number"
                    boolean r1 = r0.containsKey(r1)
                    if (r1 == 0) goto L_0x0092
                    java.lang.String r1 = "Device_Name"
                    boolean r1 = r0.containsKey(r1)
                    if (r1 == 0) goto L_0x0092
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$Di r1 = r8.this$0
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter r1 = r1.a
                    java.lang.String r4 = "pair_success"
                    r1.U(r4, r0)
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$Di r1 = r8.this$0
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter r1 = r1.a
                    r1.V(r0)
                L_0x0092:
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$Di r1 = r8.this$0
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter r1 = r1.a
                    com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter.D(r1)
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$Di$Bii$Aiii r4 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$Di$Bii$Aiii
                    r4.<init>(r8, r7)
                    r8.L$0 = r2
                    r8.L$1 = r0
                    r8.label = r5
                    java.lang.Object r1 = com.fossil.Eu7.g(r1, r4, r8)
                    if (r1 != r3) goto L_0x00cd
                    r0 = r3
                    goto L_0x0026
                L_0x00ae:
                    com.fossil.El7.b(r9)
                    com.mapped.Il6 r0 = r8.p$
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$Di r1 = r8.this$0
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter r1 = r1.a
                    com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter.C(r1)
                    com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$Di$Bii$Biii r2 = new com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$Di$Bii$Biii
                    r2.<init>(r8, r7)
                    r8.L$0 = r0
                    r8.label = r4
                    java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r8)
                    if (r1 != r3) goto L_0x00d0
                    r0 = r3
                    goto L_0x0026
                L_0x00cd:
                    r1 = r2
                    goto L_0x003a
                L_0x00d0:
                    r2 = r0
                    goto L_0x006f
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter.Di.Bii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di(PairingPresenter pairingPresenter) {
            this.a = pairingPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(LinkDeviceUseCase.Ii ii) {
            b(ii);
        }

        @DexIgnore
        public void b(LinkDeviceUseCase.Ii ii) {
            Wg6.c(ii, "errorValue");
            Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, ii, null), 3, null);
        }

        @DexIgnore
        public void c(LinkDeviceUseCase.Ji ji) {
            Wg6.c(ji, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = PairingPresenter.z.a();
            local.d(a2, "pairDeviceCallback() pairDevice, isSkipOTA=" + this.a.y.y0() + ", response=" + ji.getClass().getSimpleName());
            if (ji instanceof LinkDeviceUseCase.Ai) {
                this.a.s.a();
                this.a.s.v2(((LinkDeviceUseCase.Ai) ji).a(), this.a.a0());
            } else if (ji instanceof LinkDeviceUseCase.Fi) {
                this.a.s.a();
                this.a.s.F2(((LinkDeviceUseCase.Fi) ji).a(), this.a.a0());
            } else if (ji instanceof LinkDeviceUseCase.Mi) {
                LinkDeviceUseCase.Mi mi = (LinkDeviceUseCase.Mi) ji;
                this.a.s.P3(mi.a(), this.a.a0(), mi.b());
            } else if (ji instanceof LinkDeviceUseCase.Bi) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = PairingPresenter.z.a();
                StringBuilder sb = new StringBuilder();
                sb.append("authorizeDeviceResponse, isStartTimer=");
                LinkDeviceUseCase.Bi bi = (LinkDeviceUseCase.Bi) ji;
                sb.append(bi.a());
                local2.d(a3, sb.toString());
                if (bi.a()) {
                    this.a.s.b4(true);
                    return;
                }
                this.a.p = false;
                this.a.s.b();
                this.a.s.b4(false);
            } else if (ji instanceof LinkDeviceUseCase.Li) {
                String deviceId = ((LinkDeviceUseCase.Li) ji).a().getDeviceId();
                PortfolioApp.get.instance().S1(this.a.v, false, 13);
                FLogger.INSTANCE.getRemote().i(FLogger.Component.API, FLogger.Session.PAIR, deviceId, PairingPresenter.z.a(), "Pair Success");
                Rm6 unused = Gu7.d(this.a.k(), null, null, new Bii(this, deviceId, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(LinkDeviceUseCase.Ji ji) {
            c(ji);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei(PairingPresenter pairingPresenter) {
            this.a = pairingPresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            T t;
            T t2;
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            ShineDevice shineDevice = (ShineDevice) intent.getParcelableExtra("device");
            if (shineDevice == null) {
                FLogger.INSTANCE.getLocal().d(PairingPresenter.z.a(), "scanReceiver - ShineDevice is NULL!!!");
                return;
            }
            String serial = shineDevice.getSerial();
            if (TextUtils.isEmpty(serial)) {
                FLogger.INSTANCE.getLocal().d(PairingPresenter.z.a(), "scanReceiver - ShineDeviceSerial is NULL => wearOS device=" + shineDevice);
                this.a.e0(shineDevice);
                return;
            }
            FLogger.INSTANCE.getLocal().d(PairingPresenter.z.a(), "scanReceiver - receive device serial=" + serial + " allSkuModelSize " + this.a.Y().size());
            DeviceHelper j = DeviceHelper.o.j();
            if (serial == null) {
                Wg6.i();
                throw null;
            } else if (j.n(serial, this.a.Y())) {
                int rssi = shineDevice.getRssi();
                this.a.k0(serial, rssi);
                Iterator<T> it = this.a.c0().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (Wg6.a(((ShineDevice) next.getFirst()).getSerial(), serial)) {
                        t = next;
                        break;
                    }
                }
                T t3 = t;
                Iterator<T> it2 = this.a.d0().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next2 = it2.next();
                    if (Wg6.a(next2.getDeviceId(), serial)) {
                        t2 = next2;
                        break;
                    }
                }
                T t4 = t2;
                if (t4 == null && t3 == null) {
                    FLogger.INSTANCE.getLocal().d(PairingPresenter.z.a(), "Add device " + serial + " to list");
                    this.a.T(shineDevice);
                } else if (t4 != null || t3 == null || this.a.b0()) {
                    FLogger.INSTANCE.getLocal().d(PairingPresenter.z.a(), "Device already in list, ignore it " + serial);
                } else {
                    FLogger.INSTANCE.getLocal().d(PairingPresenter.z.a(), "Pre-scan is not complete, update RSSI for scanned devices");
                    int X = this.a.X(serial);
                    ((ShineDevice) t3.getFirst()).updateRssi(X == Integer.MIN_VALUE ? rssi : X);
                    Iy6 iy6 = this.a.s;
                    PairingPresenter pairingPresenter = this.a;
                    iy6.t2(pairingPresenter.W(pairingPresenter.c0()));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1", f = "PairingPresenter.kt", l = {99, 101}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Device>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends Device>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.u.getAllDevice();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1$2", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends SKUModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends SKUModel>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.u.getSupportedSku();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(PairingPresenter pairingPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = pairingPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00e4  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0113  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 292
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter.Fi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements CoroutineUseCase.Ei<Ut5, St5> {
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public Gi(PairingPresenter pairingPresenter, String str) {
            this.a = pairingPresenter;
            this.b = str;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(St5 st5) {
            b(st5);
        }

        @DexIgnore
        public void b(St5 st5) {
            Wg6.c(st5, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = PairingPresenter.z.a();
            local.d(a2, "syncDevice fail - serial=" + this.b + " - errorCode=" + st5.a());
            this.a.s.a();
            int i = My6.a[st5.a().ordinal()];
            if (i != 1) {
                if (i == 2) {
                    this.a.s.s6(this.b);
                } else if (i == 3) {
                    this.a.s.v3(this.b);
                } else if (i != 4) {
                    this.a.s.K2(st5.a().ordinal(), this.b);
                } else {
                    FLogger.INSTANCE.getLocal().d(PairingPresenter.z.a(), "User deny stopping workout");
                }
            } else if (st5.b() != null) {
                List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(new ArrayList(st5.b()));
                Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                Iy6 iy6 = this.a.s;
                Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                if (array != null) {
                    Uh5[] uh5Arr = (Uh5[]) array;
                    iy6.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }

        @DexIgnore
        public void c(Ut5 ut5) {
            Wg6.c(ut5, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = PairingPresenter.z.a();
            local.d(a2, "syncDevice success - serial=" + this.b);
            this.a.x();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(Ut5 ut5) {
            c(ut5);
        }
    }

    @DexIgnore
    public PairingPresenter(Iy6 iy6, LinkDeviceUseCase linkDeviceUseCase, DeviceRepository deviceRepository, Cj4 cj4, MusicControlComponent musicControlComponent, SetNotificationUseCase setNotificationUseCase, An4 an4) {
        Wg6.c(iy6, "mPairingView");
        Wg6.c(linkDeviceUseCase, "mLinkDeviceUseCase");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(cj4, "mDeviceSettingFactory");
        Wg6.c(musicControlComponent, "mMusicControlComponent");
        Wg6.c(setNotificationUseCase, "mSetNotificationUseCase");
        Wg6.c(an4, "mSharePrefs");
        this.s = iy6;
        this.t = linkDeviceUseCase;
        this.u = deviceRepository;
        this.v = cj4;
        this.w = musicControlComponent;
        this.x = setNotificationUseCase;
        this.y = an4;
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public void A(ShineDevice shineDevice) {
        Wg6.c(shineDevice, "device");
        Jn5 jn5 = Jn5.b;
        Iy6 iy6 = this.s;
        if (iy6 == null) {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.pairing.PairingFragment");
        } else if (Jn5.c(jn5, ((Rx6) iy6).getContext(), Jn5.Ai.PAIR_DEVICE, false, false, false, null, 60, null)) {
            this.s.b();
            this.o = false;
            p0();
            this.e = shineDevice;
            String J = PortfolioApp.get.instance().J();
            if (!Vt7.l(J)) {
                q0(J);
            } else {
                x();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public void B() {
        this.t.H();
        Iy6 iy6 = this.s;
        ShineDevice shineDevice = this.e;
        if (shineDevice != null) {
            String serial = shineDevice.getSerial();
            Wg6.b(serial, "mPairingDevice!!.serial");
            iy6.v2(serial, this.j);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void T(ShineDevice shineDevice) {
        Wg6.c(shineDevice, "shineDevice");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = z.a();
        local.d(a2, "addDevice serial=" + shineDevice.getSerial());
        String serial = shineDevice.getSerial();
        Wg6.b(serial, "shineDevice.serial");
        int X = X(serial);
        if (X == Integer.MIN_VALUE) {
            X = shineDevice.getRssi();
        }
        shineDevice.updateRssi(X);
        Rm6 unused = Gu7.d(k(), null, null, new Ci(this, shineDevice, null), 3, null);
    }

    @DexIgnore
    public final void U(String str, Map<String, String> map) {
        Wg6.c(str, Constants.EVENT);
        Wg6.c(map, "values");
        AnalyticsHelper.f.g().l(str, map);
    }

    @DexIgnore
    public final void V(Map<String, String> map) {
        Wg6.c(map, "properties");
        AnalyticsHelper.f.g().r(map);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0010 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.mapped.Lc6<com.misfit.frameworks.buttonservice.model.ShineDevice, java.lang.String>> W(java.util.List<com.mapped.Lc6<com.misfit.frameworks.buttonservice.model.ShineDevice, java.lang.String>> r9) {
        /*
            r8 = this;
            r4 = 1
            r3 = 0
            java.lang.String r0 = "devices"
            com.mapped.Wg6.c(r9, r0)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.Iterator r6 = r9.iterator()
        L_0x0010:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x004c
            java.lang.Object r2 = r6.next()
            r1 = r2
            com.mapped.Lc6 r1 = (com.mapped.Lc6) r1
            java.lang.Object r0 = r1.getFirst()
            com.misfit.frameworks.buttonservice.model.ShineDevice r0 = (com.misfit.frameworks.buttonservice.model.ShineDevice) r0
            int r0 = r0.getRssi()
            r7 = -150(0xffffffffffffff6a, float:NaN)
            if (r0 > r7) goto L_0x0043
            java.lang.Object r0 = r1.getFirst()
            com.misfit.frameworks.buttonservice.model.ShineDevice r0 = (com.misfit.frameworks.buttonservice.model.ShineDevice) r0
            java.lang.String r0 = r0.getSerial()
            java.lang.String r1 = "it.first.serial"
            com.mapped.Wg6.b(r0, r1)
            int r0 = r0.length()
            if (r0 != 0) goto L_0x004a
            r0 = r4
        L_0x0041:
            if (r0 == 0) goto L_0x0051
        L_0x0043:
            r0 = r4
        L_0x0044:
            if (r0 == 0) goto L_0x0010
            r5.add(r2)
            goto L_0x0010
        L_0x004a:
            r0 = r3
            goto L_0x0041
        L_0x004c:
            java.util.List r0 = com.fossil.Pm7.j0(r5)
            return r0
        L_0x0051:
            r0 = r3
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter.W(java.util.List):java.util.List");
    }

    @DexIgnore
    public final int X(String str) {
        Wg6.c(str, "serial");
        if (this.g.containsKey(str)) {
            double d = 0.0d;
            List<Integer> list = this.g.get(str);
            if (list != null) {
                for (Integer num : list) {
                    d += (double) num.intValue();
                }
                int size = list.size();
                if (size <= 0) {
                    size = 1;
                }
                return (int) (d / ((double) size));
            }
        }
        return RecyclerView.UNDEFINED_DURATION;
    }

    @DexIgnore
    public final ArrayList<SKUModel> Y() {
        return this.l;
    }

    @DexIgnore
    public final Ul5 Z() {
        return this.m;
    }

    @DexIgnore
    public final boolean a0() {
        return this.j;
    }

    @DexIgnore
    public final boolean b0() {
        return this.n;
    }

    @DexIgnore
    public final List<Lc6<ShineDevice, String>> c0() {
        return this.f;
    }

    @DexIgnore
    public final ArrayList<Device> d0() {
        return this.k;
    }

    @DexIgnore
    public final void e0(ShineDevice shineDevice) {
        T t2;
        Iterator<T> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (Wg6.a(((ShineDevice) next.getFirst()).getMacAddress(), shineDevice.getMacAddress())) {
                t2 = next;
                break;
            }
        }
        if (t2 == null) {
            ArrayList<SKUModel> arrayList = this.l;
            ArrayList<SKUModel> arrayList2 = new ArrayList();
            for (T t3 : arrayList) {
                if (Wg6.a(t3.getGroupName(), "WearOS")) {
                    arrayList2.add(t3);
                }
            }
            if (!arrayList2.isEmpty()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = z.a();
                local.d(a2, "wearOS devices in DB=" + arrayList2);
                for (SKUModel sKUModel : arrayList2) {
                    String name = shineDevice.getName();
                    Wg6.b(name, "shineDevice.name");
                    if (Wt7.u(name, String.valueOf(sKUModel.getDeviceName()), true)) {
                        FLogger.INSTANCE.getLocal().d(z.a(), "Detected wearOS device is acceptable");
                        this.f.add(new Lc6<>(shineDevice, shineDevice.getName()));
                        this.s.t2(W(this.f));
                        return;
                    }
                }
            }
        }
    }

    @DexIgnore
    public final HashMap<String, String> f0(String str) {
        Wg6.c(str, "serial");
        HashMap<String, String> hashMap = new HashMap<>();
        SKUModel skuModelBySerialPrefix = this.u.getSkuModelBySerialPrefix(DeviceHelper.o.m(str));
        if (skuModelBySerialPrefix != null) {
            String sku = skuModelBySerialPrefix.getSku();
            if (sku != null) {
                hashMap.put("Style_Number", sku);
                String deviceName = skuModelBySerialPrefix.getDeviceName();
                if (deviceName != null) {
                    hashMap.put("Device_Name", deviceName);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
        return hashMap;
    }

    @DexIgnore
    public final boolean g0() {
        Locale locale = Locale.getDefault();
        Wg6.b(locale, "Locale.getDefault()");
        if (TextUtils.isEmpty(locale.getLanguage())) {
            return false;
        }
        Locale locale2 = Locale.getDefault();
        Wg6.b(locale2, "Locale.getDefault()");
        if (TextUtils.isEmpty(locale2.getCountry())) {
            return false;
        }
        StringBuilder sb = new StringBuilder();
        Locale locale3 = Locale.getDefault();
        Wg6.b(locale3, "Locale.getDefault()");
        sb.append(locale3.getLanguage());
        sb.append(LocaleConverter.LOCALE_DELIMITER);
        Locale locale4 = Locale.getDefault();
        Wg6.b(locale4, "Locale.getDefault()");
        sb.append(locale4.getCountry());
        String sb2 = sb.toString();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = z.a();
        local.d(a2, "language: " + sb2);
        return Vt7.j(sb2, "zh_CN", true) || Vt7.j(sb2, "zh_SG", true);
    }

    @DexIgnore
    public boolean h0() {
        return this.i;
    }

    @DexIgnore
    public void i0() {
        if (!this.i && this.o) {
            if (this.f.isEmpty()) {
                this.s.Z0();
                this.n = false;
                Handler handler = this.h;
                if (handler != null) {
                    handler.postDelayed(new Bi(), (long) 15000);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                this.s.o4(W(this.f));
            }
            o0();
        }
    }

    @DexIgnore
    public final void j0() {
        FLogger.INSTANCE.getLocal().d(z.a(), "onUserContinueToNextStep");
        this.s.a();
        if (DeviceHelper.o.x(PortfolioApp.get.instance().J())) {
            this.s.d0();
        } else {
            this.s.l();
        }
    }

    @DexIgnore
    public final void k0(String str, int i2) {
        Wg6.c(str, "serial");
        int i3 = (i2 == 0 || i2 == -999999) ? 0 : i2;
        if (this.g.containsKey(str)) {
            List<Integer> list = this.g.get(str);
            if (list != null && !list.contains(0)) {
                if (list.size() < 5) {
                    list.add(Integer.valueOf(i2));
                    return;
                }
                list.remove(0);
                list.add(Integer.valueOf(i2));
                return;
            }
            return;
        }
        this.g.put(str, Hm7.i(Integer.valueOf(i3)));
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Rm6 unused = Gu7.d(k(), null, null, new Fi(this, null), 3, null);
    }

    @DexIgnore
    public final void l0(boolean z2) {
        this.n = z2;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        Handler handler = this.h;
        if (handler != null) {
            if (handler != null) {
                handler.removeCallbacksAndMessages(null);
            } else {
                Wg6.i();
                throw null;
            }
        }
        p0();
        this.t.O();
        Ct0.b(PortfolioApp.get.instance()).e(this.r);
    }

    @DexIgnore
    public void m0(boolean z2) {
        this.i = z2;
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public void n() {
        FLogger.INSTANCE.getLocal().d(z.a(), "cancelPairDevice()");
        PortfolioApp instance = PortfolioApp.get.instance();
        ShineDevice shineDevice = this.e;
        if (shineDevice != null) {
            String serial = shineDevice.getSerial();
            Wg6.b(serial, "mPairingDevice!!.serial");
            instance.s(serial);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public void n0() {
        this.s.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public void o(String str) {
        Wg6.c(str, "serial");
        this.s.b();
        PortfolioApp.get.instance().x(str, true);
    }

    @DexIgnore
    public void o0() {
        try {
            IButtonConnectivity b = PortfolioApp.get.b();
            if (b != null) {
                b.deviceStartScan();
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public void p(String str) {
        Wg6.c(str, "serial");
        this.s.b();
        PortfolioApp.get.instance().x(str, false);
    }

    @DexIgnore
    public final void p0() {
        try {
            IButtonConnectivity b = PortfolioApp.get.b();
            if (b != null) {
                b.deviceStopScan();
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public ShineDevice q() {
        return this.e;
    }

    @DexIgnore
    public void q0(String str) {
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = z.a();
        local.d(a2, "syncDevice - serial=" + str);
        this.v.b(str).e(new Tt5(!DeviceHelper.o.w(FossilDeviceSerialPatternUtil.getDeviceBySerial(str)) ? 10 : 15, str, false), new Gi(this, str));
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public void r(int i2) {
        this.s.e0();
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public boolean s() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public void t() {
        this.s.x4();
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public void u(ShineDevice shineDevice) {
        Wg6.c(shineDevice, "device");
        this.s.y4(g0());
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public void v() {
        List<String> i2 = Hm7.i(Um5.c(PortfolioApp.get.instance(), 2131886893), Um5.c(PortfolioApp.get.instance(), 2131886894));
        this.p = true;
        this.s.G3(i2);
        this.t.E(30000);
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public void w(ShineDevice shineDevice) {
        Wg6.c(shineDevice, "pairingDevice");
        this.e = shineDevice;
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public void x() {
        this.s.b();
        ShineDevice shineDevice = this.e;
        if (shineDevice != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = z.a();
            local.d(a2, "pairDevice - serial=" + shineDevice.getSerial());
            String serial = shineDevice.getSerial();
            Wg6.b(serial, "it.serial");
            String macAddress = shineDevice.getMacAddress();
            Wg6.b(macAddress, "it.macAddress");
            LinkDeviceUseCase.Hi hi = new LinkDeviceUseCase.Hi(serial, macAddress);
            Ul5 c = AnalyticsHelper.f.c("setup_device_session");
            this.m = c;
            AnalyticsHelper.f.a("setup_device_session", c);
            PortfolioApp instance = PortfolioApp.get.instance();
            CommunicateMode communicateMode = CommunicateMode.LINK;
            instance.t(communicateMode, "", communicateMode, hi.a());
            this.t.e(hi, this.q);
            Ul5 ul5 = this.m;
            if (ul5 != null) {
                ul5.i();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public void y(boolean z2) {
        this.o = z2;
    }

    @DexIgnore
    @Override // com.fossil.Hy6
    public void z(boolean z2) {
        this.j = z2;
    }
}
