package com.portfolio.platform.uirenew.migration;

import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Hq4;
import com.fossil.Ko7;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Hx5;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.migration.MigrationHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MigrationViewModel extends Hq4 {
    @DexIgnore
    public /* final */ MigrationHelper h;
    @DexIgnore
    public /* final */ Cj4 i;
    @DexIgnore
    public /* final */ An4 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.migration.MigrationViewModel$migrate$1", f = "MigrationViewModel.kt", l = {25}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MigrationViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.migration.MigrationViewModel$migrate$1$resultCode$1", f = "MigrationViewModel.kt", l = {26}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Integer>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Integer> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    MigrationHelper migrationHelper = this.this$0.this$0.h;
                    this.L$0 = il6;
                    this.label = 1;
                    Object g = migrationHelper.g(this);
                    return g == d ? d : g;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(MigrationViewModel migrationViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = migrationViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                if (Hx5.b(PortfolioApp.get.instance())) {
                    this.this$0.j().l(new Hq4.Bi(true, false, null, 6, null));
                    Dv7 b = Bw7.b();
                    Aii aii = new Aii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    g = Eu7.g(b, aii, this);
                    if (g == d) {
                        return d;
                    }
                } else {
                    this.this$0.j().l(new Hq4.Bi(false, false, null, 6, null));
                    this.this$0.a(1, "");
                    return Cd6.a;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            int intValue = ((Number) g).intValue();
            this.this$0.j().l(new Hq4.Bi(false, false, null, 6, null));
            if (intValue == 0) {
                PortfolioApp.get.instance().S1(this.this$0.i, false, 13);
                this.this$0.j.G1(Ao7.a(true));
            }
            this.this$0.a(intValue, "");
            return Cd6.a;
        }
    }

    @DexIgnore
    public MigrationViewModel(MigrationHelper migrationHelper, Cj4 cj4, An4 an4) {
        Wg6.c(migrationHelper, "migrationHelper");
        Wg6.c(cj4, "mDeviceSettingFactory");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.h = migrationHelper;
        this.i = cj4;
        this.j = an4;
    }

    @DexIgnore
    public final void r() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ai(this, null), 3, null);
    }
}
