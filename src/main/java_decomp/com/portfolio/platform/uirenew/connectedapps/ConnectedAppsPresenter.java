package com.portfolio.platform.uirenew.connectedapps;

import android.app.Activity;
import android.util.Log;
import androidx.fragment.app.FragmentActivity;
import com.fossil.By5;
import com.fossil.Cy5;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Sh5;
import com.fossil.T62;
import com.fossil.X62;
import com.fossil.Xn6;
import com.fossil.Yn7;
import com.fossil.Z52;
import com.fossil.Z62;
import com.google.android.gms.common.api.Status;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Kk4;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ConnectedAppsPresenter extends By5 implements Kk4.Di {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public MFUser e;
    @DexIgnore
    public boolean f; // = true;
    @DexIgnore
    public /* final */ Cy5 g;
    @DexIgnore
    public /* final */ UserRepository h;
    @DexIgnore
    public /* final */ Kk4 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ConnectedAppsPresenter.j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends X62<Status> {
        @DexIgnore
        public /* final */ /* synthetic */ ConnectedAppsPresenter c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(ConnectedAppsPresenter connectedAppsPresenter, Activity activity, int i) {
            super(activity, i);
            this.c = connectedAppsPresenter;
        }

        @DexIgnore
        @Override // com.fossil.B72
        public /* bridge */ /* synthetic */ void c(Z62 z62) {
            e((Status) z62);
        }

        @DexIgnore
        @Override // com.fossil.X62
        public void d(Status status) {
            Wg6.c(status, "status");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ConnectedAppsPresenter.k.a();
            local.d(a2, "onGFLogout.onUnresolvableFailure(): isSuccess = " + status.D() + ", statusMessage = " + status.h() + ", statusCode = " + status.f());
            if (status.f() == 17 || status.f() == 5010) {
                this.c.x();
                return;
            }
            this.c.g.N2(true);
            this.c.g.I5(status.f());
        }

        @DexIgnore
        public void e(Status status) {
            Wg6.c(status, "status");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ConnectedAppsPresenter.k.a();
            local.d(a2, "onGFLogout.onSuccess(): isSuccess = " + status.D() + ", statusMessage = " + status.h());
            if (status.D()) {
                this.c.x();
                return;
            }
            this.c.g.N2(true);
            this.c.g.I5(status.f());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1", f = "ConnectedAppsPresenter.kt", l = {43}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ConnectedAppsPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1$1", f = "ConnectedAppsPresenter.kt", l = {44}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object currentUser;
                ConnectedAppsPresenter connectedAppsPresenter;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    ConnectedAppsPresenter connectedAppsPresenter2 = this.this$0.this$0;
                    UserRepository userRepository = connectedAppsPresenter2.h;
                    this.L$0 = il6;
                    this.L$1 = connectedAppsPresenter2;
                    this.label = 1;
                    currentUser = userRepository.getCurrentUser(this);
                    if (currentUser == d) {
                        return d;
                    }
                    connectedAppsPresenter = connectedAppsPresenter2;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    connectedAppsPresenter = (ConnectedAppsPresenter) this.L$1;
                    currentUser = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                connectedAppsPresenter.e = (MFUser) currentUser;
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(ConnectedAppsPresenter connectedAppsPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = connectedAppsPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            boolean e;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                e = this.this$0.i.e();
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.Z$0 = e;
                this.label = 1;
                if (Eu7.g(i2, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                boolean z = this.Z$0;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                e = z;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ConnectedAppsPresenter.k.a();
            local.d(a2, "starts isGFConnected=" + this.this$0.i.e());
            this.this$0.i.i(this.this$0);
            this.this$0.g.N2(e);
            if (!e && this.this$0.i.f() && !this.this$0.f) {
                this.this$0.i.k();
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ConnectedAppsPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<? extends MFUser>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ap4<? extends MFUser>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.h;
                    MFUser mFUser = this.this$0.$it;
                    this.L$0 = il6;
                    this.label = 1;
                    Object updateUser = userRepository.updateUser(mFUser, true, this);
                    return updateUser == d ? d : updateUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(MFUser mFUser, Xe6 xe6, ConnectedAppsPresenter connectedAppsPresenter) {
            super(2, xe6);
            this.$it = mFUser;
            this.this$0 = connectedAppsPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.$it, xe6, this.this$0);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(i2, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = ConnectedAppsPresenter.class.getSimpleName();
        Wg6.b(simpleName, "ConnectedAppsPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public ConnectedAppsPresenter(Cy5 cy5, UserRepository userRepository, PortfolioApp portfolioApp, Kk4 kk4) {
        Wg6.c(cy5, "mView");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(kk4, "mGoogleFitHelper");
        this.g = cy5;
        this.h = userRepository;
        this.i = kk4;
    }

    @DexIgnore
    @Override // com.mapped.Kk4.Di
    public void b() {
        FLogger.INSTANCE.getLocal().d(j, "onGFConnectionSuccess");
        String name = Sh5.GoogleFit.getName();
        Wg6.b(name, "Integration.GoogleFit.getName()");
        w(name);
        this.g.N2(true);
    }

    @DexIgnore
    @Override // com.mapped.Kk4.Di
    public void c(Z52 z52) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onGFConnectionFailed - Cause: " + z52);
        if (z52 == null) {
            return;
        }
        if (z52.k()) {
            this.f = false;
            this.g.f1(z52);
            return;
        }
        this.g.I5(z52.c());
    }

    @DexIgnore
    @Override // com.mapped.Kk4.Di
    public void f(T62<Status> t62) {
        if (t62 != null) {
            Cy5 cy5 = this.g;
            if (cy5 != null) {
                FragmentActivity activity = ((Xn6) cy5).getActivity();
                if (activity != null) {
                    t62.d(new Bi(this, activity, 3));
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.ConnectedAppsFragment");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Rm6 unused = Gu7.d(k(), null, null, new Ci(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        Log.d(j, "stop");
        MFUser mFUser = this.e;
        if (mFUser != null) {
            Rm6 unused = Gu7.d(k(), null, null, new Di(mFUser, null, this), 3, null);
        }
        this.i.i(null);
    }

    @DexIgnore
    @Override // com.fossil.By5
    public void n() {
        if (!PortfolioApp.get.instance().p0()) {
            this.g.I5(601);
            this.g.N2(this.i.e());
        } else if (this.i.e()) {
            this.i.g();
        } else if (this.i.d()) {
            this.i.a();
        } else {
            Kk4 kk4 = this.i;
            Cy5 cy5 = this.g;
            if (cy5 != null) {
                kk4.h((Xn6) cy5);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.ConnectedAppsFragment");
        }
    }

    @DexIgnore
    @Override // com.fossil.By5
    public void o() {
        this.i.a();
    }

    @DexIgnore
    public final void w(String str) {
        MFUser mFUser = this.e;
        if (mFUser != null) {
            List<String> integrationsList = mFUser.getIntegrationsList();
            if (integrationsList != null) {
                if (!integrationsList.contains(str)) {
                    integrationsList.add(str);
                }
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    mFUser2.setIntegrationsList(integrationsList);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void x() {
        this.i.b();
        String name = Sh5.GoogleFit.getName();
        Wg6.b(name, "Integration.GoogleFit.getName()");
        y(name);
        this.g.N2(false);
    }

    @DexIgnore
    public final void y(String str) {
        MFUser mFUser = this.e;
        if (mFUser != null) {
            List<String> integrationsList = mFUser.getIntegrationsList();
            if (integrationsList != null) {
                integrationsList.remove(str);
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    mFUser2.setIntegrationsList(integrationsList);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public void z() {
        this.g.M5(this);
    }
}
