package com.portfolio.platform.uirenew.troubleshooting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.U07;
import com.fossil.V07;
import com.fossil.X07;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TroubleshootingActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public X07 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void c(a aVar, Context context, String str, boolean z, boolean z2, int i, Object obj) {
            if ((i & 2) != 0) {
                str = "";
            }
            if ((i & 4) != 0) {
                z = false;
            }
            if ((i & 8) != 0) {
                z2 = false;
            }
            aVar.b(context, str, z, z2);
        }

        @DexIgnore
        public final void a(Context context) {
            Wg6.c(context, "context");
            context.startActivity(new Intent(context, TroubleshootingActivity.class));
        }

        @DexIgnore
        public final void b(Context context, String str, boolean z, boolean z2) {
            Wg6.c(context, "context");
            Wg6.c(str, "serial");
            Intent intent = new Intent(context, TroubleshootingActivity.class);
            if (TextUtils.isEmpty(str) || DeviceHelper.o.x(str)) {
                intent.putExtra("DEVICE_TYPE", false);
            } else {
                intent.putExtra("DEVICE_TYPE", true);
            }
            intent.putExtra("IS_PAIRING_FLOW", z);
            intent.putExtra("IS_ONBOARDING_FLOW", z2);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    public final boolean L(Intent intent) {
        if (intent.getExtras() == null) {
            return false;
        }
        Bundle extras = intent.getExtras();
        if (extras != null) {
            return extras.getBoolean("DEVICE_TYPE", false);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        boolean z = false;
        super.onCreate(bundle);
        setContentView(2131558439);
        U07 u07 = (U07) getSupportFragmentManager().Y(2131362158);
        Intent intent = getIntent();
        Wg6.b(intent, "intent");
        boolean L = L(intent);
        Intent intent2 = getIntent();
        Wg6.b(intent2, "intent");
        Bundle extras = intent2.getExtras();
        boolean z2 = extras != null ? extras.getBoolean("IS_PAIRING_FLOW", false) : false;
        Intent intent3 = getIntent();
        Wg6.b(intent3, "intent");
        Bundle extras2 = intent3.getExtras();
        if (extras2 != null) {
            z = extras2.getBoolean("IS_ONBOARDING_FLOW", false);
        }
        if (u07 == null) {
            u07 = U07.u.e(L, z2, z);
            k(u07, U07.u.d(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (u07 != null) {
            iface.H(new V07(u07)).a(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.troubleshooting.TroubleshootingContract.View");
    }
}
