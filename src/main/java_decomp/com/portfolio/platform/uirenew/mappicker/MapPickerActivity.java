package com.portfolio.platform.uirenew.mappicker;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.mapped.MapPickerFragment;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MapPickerActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void b(a aVar, Fragment fragment, double d, double d2, String str, int i, Object obj) {
            double d3 = 0.0d;
            double d4 = (i & 2) != 0 ? 0.0d : d;
            if ((i & 4) == 0) {
                d3 = d2;
            }
            aVar.a(fragment, d4, d3, (i & 8) != 0 ? "" : str);
        }

        @DexIgnore
        public final void a(Fragment fragment, double d, double d2, String str) {
            Wg6.c(fragment, "context");
            Wg6.c(str, "address");
            Intent intent = new Intent(fragment.getContext(), MapPickerActivity.class);
            intent.putExtra("latitude", d);
            intent.putExtra("longitude", d2);
            intent.putExtra("address", str);
            fragment.startActivityForResult(intent, 100);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Fragment Y = getSupportFragmentManager().Y(2131362158);
        if (Y != null) {
            Y.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        String str;
        double d;
        double d2;
        String stringExtra;
        double d3 = 0.0d;
        super.onCreate(bundle);
        setContentView(2131558439);
        if (((MapPickerFragment) getSupportFragmentManager().Y(2131362158)) == null) {
            Intent intent = getIntent();
            if (intent != null) {
                double doubleExtra = intent.hasExtra("latitude") ? intent.getDoubleExtra("latitude", 0.0d) : 0.0d;
                if (intent.hasExtra("longitude")) {
                    d3 = intent.getDoubleExtra("longitude", 0.0d);
                }
                str = (!intent.hasExtra("address") || (stringExtra = intent.getStringExtra("address")) == null) ? "" : stringExtra;
                d = doubleExtra;
                d2 = d3;
            } else {
                str = "";
                d = 0.0d;
                d2 = 0.0d;
            }
            k(MapPickerFragment.m.a(d, d2, str), "MapPickerFragment", 2131362158);
        }
    }
}
