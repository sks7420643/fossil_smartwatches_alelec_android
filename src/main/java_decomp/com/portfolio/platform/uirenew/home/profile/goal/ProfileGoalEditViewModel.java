package com.portfolio.platform.uirenew.home.profile.goal;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gl7;
import com.fossil.Gu7;
import com.fossil.Hq4;
import com.fossil.Hq5;
import com.fossil.Ip6;
import com.fossil.Ko7;
import com.fossil.Kq5;
import com.fossil.Rh5;
import com.fossil.U37;
import com.fossil.Uh5;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rl6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileGoalEditViewModel extends Hq4 {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ Bi v; // = new Bi(null);
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public /* final */ MutableLiveData<U37<Ei>> j;
    @DexIgnore
    public /* final */ LiveData<U37<Ei>> k;
    @DexIgnore
    public Ci l; // = new Ci(0, 0, 0, 0, 0, 31, null);
    @DexIgnore
    public Ci m; // = new Ci(0, 0, 0, 0, 0, 31, null);
    @DexIgnore
    public /* final */ MutableLiveData<Ci> n;
    @DexIgnore
    public /* final */ LiveData<Ci> o;
    @DexIgnore
    public /* final */ SummariesRepository p;
    @DexIgnore
    public /* final */ SleepSummariesRepository q;
    @DexIgnore
    public /* final */ GoalTrackingRepository r;
    @DexIgnore
    public /* final */ UserRepository s;
    @DexIgnore
    public /* final */ SetActivityGoalUserCase t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1", f = "ProfileGoalEditViewModel.kt", l = {61}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1$1", f = "ProfileGoalEditViewModel.kt", l = {78, 79, 80, 81, 82}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Ci>, Object> {
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public int I$2;
            @DexIgnore
            public int I$3;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1$1$activitySettings$1", f = "ProfileGoalEditViewModel.kt", l = {63}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super ActivitySettings>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super ActivitySettings> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object activitySettings;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        SummariesRepository summariesRepository = this.this$0.this$0.this$0.p;
                        this.L$0 = il6;
                        this.label = 1;
                        activitySettings = summariesRepository.getActivitySettings(this);
                        if (activitySettings == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        activitySettings = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    ActivitySettings activitySettings2 = (ActivitySettings) activitySettings;
                    return activitySettings2 != null ? activitySettings2 : new ActivitySettings(VideoUploader.RETRY_DELAY_UNIT_MS, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 30);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1$1$goalTracking$1", f = "ProfileGoalEditViewModel.kt", l = {73}, m = "invokeSuspend")
            public static final class Biii extends Ko7 implements Coroutine<Il6, Xe6<? super Integer>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Biii biii = new Biii(this.this$0, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
                    //return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Integer> xe6) {
                    throw null;
                    //return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object lastGoalSettings;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        GoalTrackingRepository goalTrackingRepository = this.this$0.this$0.this$0.r;
                        this.L$0 = il6;
                        this.label = 1;
                        lastGoalSettings = goalTrackingRepository.getLastGoalSettings(this);
                        if (lastGoalSettings == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        lastGoalSettings = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    Integer num = (Integer) lastGoalSettings;
                    return Ao7.e(num != null ? num.intValue() : 8);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$1$1$sleepGoal$1", f = "ProfileGoalEditViewModel.kt", l = {68}, m = "invokeSuspend")
            public static final class Ciii extends Ko7 implements Coroutine<Il6, Xe6<? super Integer>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Ciii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Ciii ciii = new Ciii(this.this$0, xe6);
                    ciii.p$ = (Il6) obj;
                    throw null;
                    //return ciii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Integer> xe6) {
                    throw null;
                    //return ((Ciii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object lastSleepGoal;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        SleepSummariesRepository sleepSummariesRepository = this.this$0.this$0.this$0.q;
                        this.L$0 = il6;
                        this.label = 1;
                        lastSleepGoal = sleepSummariesRepository.getLastSleepGoal(this);
                        if (lastSleepGoal == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        lastSleepGoal = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    Integer num = (Integer) lastSleepGoal;
                    return Ao7.e(num != null ? num.intValue() : 480);
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ci> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:15:0x0085  */
            /* JADX WARNING: Removed duplicated region for block: B:19:0x00bd  */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x00f2  */
            /* JADX WARNING: Removed duplicated region for block: B:27:0x0124  */
            /* JADX WARNING: Removed duplicated region for block: B:31:0x0160  */
            /* JADX WARNING: Removed duplicated region for block: B:32:0x0164  */
            /* JADX WARNING: Removed duplicated region for block: B:33:0x0167  */
            /* JADX WARNING: Removed duplicated region for block: B:34:0x016c  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r13) {
                /*
                // Method dump skipped, instructions count: 373
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel.Ai.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(ProfileGoalEditViewModel profileGoalEditViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileGoalEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            ProfileGoalEditViewModel profileGoalEditViewModel;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Hq4.d(this.this$0, true, false, null, 6, null);
                ProfileGoalEditViewModel profileGoalEditViewModel2 = this.this$0;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.L$1 = profileGoalEditViewModel2;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
                profileGoalEditViewModel = profileGoalEditViewModel2;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
                profileGoalEditViewModel = (ProfileGoalEditViewModel) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            profileGoalEditViewModel.m = (Ci) g;
            ProfileGoalEditViewModel profileGoalEditViewModel3 = this.this$0;
            profileGoalEditViewModel3.l = Ci.b(profileGoalEditViewModel3.m, 0, 0, 0, 0, 0, 31, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ProfileGoalEditViewModel.v.a();
            local.d(a2, "init - mUiModel: " + this.this$0.l);
            this.this$0.F();
            Hq4.d(this.this$0, false, true, null, 5, null);
            this.this$0.I();
            this.this$0.t.q();
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public /* synthetic */ Bi(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ProfileGoalEditViewModel.u;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;

        @DexIgnore
        public Ci() {
            this(0, 0, 0, 0, 0, 31, null);
        }

        @DexIgnore
        public Ci(int i, int i2, int i3, int i4, int i5) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
            this.e = i5;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ci(int i, int i2, int i3, int i4, int i5, int i6, Qg6 qg6) {
            this((i6 & 1) != 0 ? 0 : i, (i6 & 2) != 0 ? 0 : i2, (i6 & 4) != 0 ? 0 : i3, (i6 & 8) != 0 ? 0 : i4, (i6 & 16) == 0 ? i5 : 0);
        }

        @DexIgnore
        public static /* synthetic */ Ci b(Ci ci, int i, int i2, int i3, int i4, int i5, int i6, Object obj) {
            return ci.a((i6 & 1) != 0 ? ci.a : i, (i6 & 2) != 0 ? ci.b : i2, (i6 & 4) != 0 ? ci.c : i3, (i6 & 8) != 0 ? ci.d : i4, (i6 & 16) != 0 ? ci.e : i5);
        }

        @DexIgnore
        public final Ci a(int i, int i2, int i3, int i4, int i5) {
            return new Ci(i, i2, i3, i4, i5);
        }

        @DexIgnore
        public final Gl7<Boolean, Boolean, Boolean> c(Ci ci) {
            boolean z = true;
            Wg6.c(ci, "that");
            boolean a2 = Wg6.a(e(), ci.e());
            boolean z2 = this.d != ci.d;
            if (this.e == ci.e) {
                z = false;
            }
            return new Gl7<>(Boolean.valueOf(!a2), Boolean.valueOf(z2), Boolean.valueOf(z));
        }

        @DexIgnore
        public final int d() {
            return this.b;
        }

        @DexIgnore
        public final ActivitySettings e() {
            return new ActivitySettings(this.a, this.c, this.b);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ci) {
                    Ci ci = (Ci) obj;
                    if (!(this.a == ci.a && this.b == ci.b && this.c == ci.c && this.d == ci.d && this.e == ci.e)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final int f() {
            return this.c;
        }

        @DexIgnore
        public final int g() {
            return this.e;
        }

        @DexIgnore
        public final int h() {
            return this.d;
        }

        @DexIgnore
        public int hashCode() {
            return (((((((this.a * 31) + this.b) * 31) + this.c) * 31) + this.d) * 31) + this.e;
        }

        @DexIgnore
        public final int i() {
            return this.a;
        }

        @DexIgnore
        public final void j(int i) {
            this.b = i;
        }

        @DexIgnore
        public final void k(int i) {
            this.c = i;
        }

        @DexIgnore
        public final void l(int i) {
            this.e = i;
        }

        @DexIgnore
        public final void m(int i) {
            this.d = i;
        }

        @DexIgnore
        public final void n(int i) {
            this.a = i;
        }

        @DexIgnore
        public String toString() {
            return "UiModelWrapper(stepGoal=" + this.a + ", activeTimeGoal=" + this.b + ", caloriesGoal=" + this.c + ", sleepGoal=" + this.d + ", goalTracking=" + this.e + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Throwable {
        @DexIgnore
        public /* final */ int errorCode;
        @DexIgnore
        public /* final */ String errorMessage;

        @DexIgnore
        public Di(int i, String str) {
            super(str);
            this.errorCode = i;
            this.errorMessage = str;
        }

        @DexIgnore
        public final int getErrorCode() {
            return this.errorCode;
        }

        @DexIgnore
        public final String getErrorMessage() {
            return this.errorMessage;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ei {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ei {
            @DexIgnore
            public static /* final */ Aii a; // = new Aii();

            @DexIgnore
            public Aii() {
                super(null);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii extends Ei {
            @DexIgnore
            public static /* final */ Bii a; // = new Bii();

            @DexIgnore
            public Bii() {
                super(null);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii extends Ei {
            @DexIgnore
            public static /* final */ Cii a; // = new Cii();

            @DexIgnore
            public Cii() {
                super(null);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Dii extends Ei {
            @DexIgnore
            public /* final */ int a;
            @DexIgnore
            public /* final */ String b;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Dii(int i, String str) {
                super(null);
                Wg6.c(str, "message");
                this.a = i;
                this.b = str;
            }

            @DexIgnore
            public final int a() {
                return this.a;
            }

            @DexIgnore
            public final String b() {
                return this.b;
            }

            @DexIgnore
            public boolean equals(Object obj) {
                if (this != obj) {
                    if (obj instanceof Dii) {
                        Dii dii = (Dii) obj;
                        if (this.a != dii.a || !Wg6.a(this.b, dii.b)) {
                            return false;
                        }
                    }
                    return false;
                }
                return true;
            }

            @DexIgnore
            public int hashCode() {
                int i = this.a;
                String str = this.b;
                return (str != null ? str.hashCode() : 0) + (i * 31);
            }

            @DexIgnore
            public String toString() {
                return "ShowErrorDialog(code=" + this.a + ", message=" + this.b + ")";
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Eii extends Ei {
            @DexIgnore
            public /* final */ List<Uh5> a;

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends com.fossil.Uh5> */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Eii(List<? extends Uh5> list) {
                super(null);
                Wg6.c(list, "permissionCodes");
                this.a = list;
            }

            @DexIgnore
            public final List<Uh5> a() {
                return this.a;
            }

            @DexIgnore
            public boolean equals(Object obj) {
                return this == obj || ((obj instanceof Eii) && Wg6.a(this.a, ((Eii) obj).a));
            }

            @DexIgnore
            public int hashCode() {
                List<Uh5> list = this.a;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            @DexIgnore
            public String toString() {
                return "ShowPermissionPopups(permissionCodes=" + this.a + ")";
            }
        }

        @DexIgnore
        public Ei() {
        }

        @DexIgnore
        public /* synthetic */ Ei(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$checkUserUsingDefaultGoal$2", f = "ProfileGoalEditViewModel.kt", l = {255, 258}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(ProfileGoalEditViewModel profileGoalEditViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileGoalEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0067 A[RETURN, SYNTHETIC] */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 2
                r4 = 1
                java.lang.Object r3 = com.fossil.Yn7.d()
                int r0 = r6.label
                if (r0 == 0) goto L_0x0050
                if (r0 == r4) goto L_0x0024
                if (r0 != r5) goto L_0x001c
                java.lang.Object r0 = r6.L$1
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                java.lang.Object r0 = r6.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r7)
            L_0x0019:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x001b:
                return r0
            L_0x001c:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0024:
                java.lang.Object r0 = r6.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r7)
                r2 = r0
                r1 = r7
            L_0x002d:
                r0 = r1
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                if (r0 == 0) goto L_0x0067
                boolean r1 = r0.getUseDefaultGoals()
                if (r1 == 0) goto L_0x0019
                r1 = 0
                r0.setUseDefaultGoals(r1)
                com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel r1 = r6.this$0
                com.portfolio.platform.data.source.UserRepository r1 = com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel.u(r1)
                r6.L$0 = r2
                r6.L$1 = r0
                r6.label = r5
                java.lang.Object r0 = r1.updateUser(r0, r4, r6)
                if (r0 != r3) goto L_0x0019
                r0 = r3
                goto L_0x001b
            L_0x0050:
                com.fossil.El7.b(r7)
                com.mapped.Il6 r0 = r6.p$
                com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel r1 = r6.this$0
                com.portfolio.platform.data.source.UserRepository r1 = com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel.u(r1)
                r6.L$0 = r0
                r6.label = r4
                java.lang.Object r1 = r1.getCurrentUser(r6)
                if (r1 != r3) goto L_0x0069
                r0 = r3
                goto L_0x001b
            L_0x0067:
                r0 = 0
                goto L_0x001b
            L_0x0069:
                r2 = r0
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel.Fi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1", f = "ProfileGoalEditViewModel.kt", l = {304, 305, 306, 130, 131, 132, 135}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public boolean Z$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$1", f = "ProfileGoalEditViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Rl6<? extends Ap4<? extends ActivitySettings>>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $changed;
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isActivityGoalChanged$inlined;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$1$1", f = "ProfileGoalEditViewModel.kt", l = {305}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<? extends ActivitySettings>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Ap4<? extends ActivitySettings>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object updateActivitySettings;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        if (!this.this$0.$changed) {
                            return new Kq5(null, false, 2, null);
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = ProfileGoalEditViewModel.v.a();
                        local.d(a2, "onSave - isActivityGoalChanged = " + this.this$0.$isActivityGoalChanged$inlined);
                        SummariesRepository summariesRepository = this.this$0.this$0.this$0.p;
                        ActivitySettings e = this.this$0.this$0.this$0.l.e();
                        this.L$0 = il6;
                        this.L$1 = this;
                        this.label = 1;
                        updateActivitySettings = summariesRepository.updateActivitySettings(e, this);
                        if (updateActivitySettings == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Xe6 xe6 = (Xe6) this.L$1;
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        updateActivitySettings = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return (Ap4) updateActivitySettings;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(boolean z, Xe6 xe6, Gi gi, boolean z2) {
                super(2, xe6);
                this.$changed = z;
                this.this$0 = gi;
                this.$isActivityGoalChanged$inlined = z2;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$changed, xe6, this.this$0, this.$isActivityGoalChanged$inlined);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Rl6<? extends Ap4<? extends ActivitySettings>>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return Gu7.b(this.p$, null, null, new Aiii(this, null), 3, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$2", f = "ProfileGoalEditViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Rl6<? extends Ap4<? extends MFSleepSettings>>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $changed;
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isSleepGoalChanged$inlined;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$2$1", f = "ProfileGoalEditViewModel.kt", l = {305}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<? extends MFSleepSettings>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Bii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Bii bii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = bii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Ap4<? extends MFSleepSettings>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object updateLastSleepGoal;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        if (!this.this$0.$changed) {
                            return new Kq5(null, false, 2, null);
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = ProfileGoalEditViewModel.v.a();
                        local.d(a2, "onSave - isSleepGoalChanged = " + this.this$0.$isSleepGoalChanged$inlined);
                        SleepSummariesRepository sleepSummariesRepository = this.this$0.this$0.this$0.q;
                        int h = this.this$0.this$0.this$0.l.h();
                        this.L$0 = il6;
                        this.L$1 = this;
                        this.label = 1;
                        updateLastSleepGoal = sleepSummariesRepository.updateLastSleepGoal(h, this);
                        if (updateLastSleepGoal == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Xe6 xe6 = (Xe6) this.L$1;
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        updateLastSleepGoal = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return (Ap4) updateLastSleepGoal;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(boolean z, Xe6 xe6, Gi gi, boolean z2) {
                super(2, xe6);
                this.$changed = z;
                this.this$0 = gi;
                this.$isSleepGoalChanged$inlined = z2;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.$changed, xe6, this.this$0, this.$isSleepGoalChanged$inlined);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Rl6<? extends Ap4<? extends MFSleepSettings>>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return Gu7.b(this.p$, null, null, new Aiii(this, null), 3, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$3", f = "ProfileGoalEditViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super Rl6<? extends Ap4<? extends GoalSetting>>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $changed;
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isGoalTrackingChanged$inlined;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel$onSave$1$invokeSuspend$$inlined$launchUpdateGoalAsync$3$1", f = "ProfileGoalEditViewModel.kt", l = {305}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<? extends GoalSetting>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Cii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Cii cii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = cii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Ap4<? extends GoalSetting>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object updateGoalSetting;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        if (!this.this$0.$changed) {
                            return new Kq5(null, false, 2, null);
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = ProfileGoalEditViewModel.v.a();
                        local.d(a2, "onSave - isGoalTrackingChanged = " + this.this$0.$isGoalTrackingChanged$inlined);
                        GoalTrackingRepository goalTrackingRepository = this.this$0.this$0.this$0.r;
                        GoalSetting goalSetting = new GoalSetting(this.this$0.this$0.this$0.l.g());
                        this.L$0 = il6;
                        this.L$1 = this;
                        this.label = 1;
                        updateGoalSetting = goalTrackingRepository.updateGoalSetting(goalSetting, this);
                        if (updateGoalSetting == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Xe6 xe6 = (Xe6) this.L$1;
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        updateGoalSetting = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return (Ap4) updateGoalSetting;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(boolean z, Xe6 xe6, Gi gi, boolean z2) {
                super(2, xe6);
                this.$changed = z;
                this.this$0 = gi;
                this.$isGoalTrackingChanged$inlined = z2;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.$changed, xe6, this.this$0, this.$isGoalTrackingChanged$inlined);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Rl6<? extends Ap4<? extends GoalSetting>>> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return Gu7.b(this.p$, null, null, new Aiii(this, null), 3, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(ProfileGoalEditViewModel profileGoalEditViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileGoalEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.this$0, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x008f  */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0094  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00fd  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x016e  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0202  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0266  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x02c2  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x0343  */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x0348  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x034d  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x03af  */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x03bd  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x03c2  */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x03c9  */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x0037 A[Catch:{ Di -> 0x0362 }] */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r18) {
            /*
            // Method dump skipped, instructions count: 996
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel.Gi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements CoroutineUseCase.Ei<SetActivityGoalUserCase.Di, SetActivityGoalUserCase.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditViewModel a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Hi(ProfileGoalEditViewModel profileGoalEditViewModel) {
            this.a = profileGoalEditViewModel;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(SetActivityGoalUserCase.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(SetActivityGoalUserCase.Ci ci) {
            boolean z = false;
            Wg6.c(ci, "errorValue");
            FLogger.INSTANCE.getLocal().d(ProfileGoalEditViewModel.v.a(), "executeSetActivityUseCase failed, errorCode=" + ci.b());
            Hq4.d(this.a, false, true, null, 5, null);
            ArrayList<Integer> a2 = ci.a();
            if (a2 == null || a2.isEmpty()) {
                z = true;
            }
            if (!z) {
                List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(ci.a());
                Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026errorValue.bleErrorCodes)");
                this.a.S(convertBLEPermissionErrorCode);
                return;
            }
            ProfileGoalEditViewModel profileGoalEditViewModel = this.a;
            int b = ci.b();
            String c = ci.c();
            if (c == null) {
                c = "";
            }
            profileGoalEditViewModel.R(b, c);
        }

        @DexIgnore
        public void c(SetActivityGoalUserCase.Di di) {
            Wg6.c(di, "responseValue");
            FLogger.INSTANCE.getLocal().d(ProfileGoalEditViewModel.v.a(), "executeSetActivityUseCase success");
            Hq4.d(this.a, false, true, null, 5, null);
            this.a.N();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(SetActivityGoalUserCase.Di di) {
            c(di);
        }
    }

    /*
    static {
        String simpleName = ProfileGoalEditViewModel.class.getSimpleName();
        Wg6.b(simpleName, "ProfileGoalEditViewModel::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public ProfileGoalEditViewModel(SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, SetActivityGoalUserCase setActivityGoalUserCase) {
        Wg6.c(summariesRepository, "mSummariesRepository");
        Wg6.c(sleepSummariesRepository, "mSleepSummariesRepository");
        Wg6.c(goalTrackingRepository, "mGoalTrackingRepository");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(setActivityGoalUserCase, "mSetActivityGoalUserCase");
        this.p = summariesRepository;
        this.q = sleepSummariesRepository;
        this.r = goalTrackingRepository;
        this.s = userRepository;
        this.t = setActivityGoalUserCase;
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.get.instance().J());
        this.h = deviceBySerial;
        this.i = DeviceHelper.o.w(deviceBySerial);
        MutableLiveData<U37<Ei>> mutableLiveData = new MutableLiveData<>();
        this.j = mutableLiveData;
        this.k = mutableLiveData;
        MutableLiveData<Ci> mutableLiveData2 = new MutableLiveData<>(this.l);
        this.n = mutableLiveData2;
        this.o = mutableLiveData2;
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    public final <T> boolean D(Ap4<T> ap4) {
        String str;
        if (!(ap4 instanceof Hq5)) {
            return true;
        }
        Hq5 hq5 = (Hq5) ap4;
        int a2 = hq5.a();
        ServerError c = hq5.c();
        if (c == null || (str = c.getMessage()) == null) {
            str = "";
        }
        throw new Di(a2, str);
    }

    @DexIgnore
    public final /* synthetic */ Object E(Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new Fi(this, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final void F() {
        this.n.l(this.l);
    }

    @DexIgnore
    public final LiveData<U37<Ei>> G() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<Ci> H() {
        return this.o;
    }

    @DexIgnore
    public final void I() {
        this.j.l(new U37<>(Ei.Aii.a));
    }

    @DexIgnore
    public final boolean J() {
        return this.i;
    }

    @DexIgnore
    public final boolean K() {
        return !Wg6.a(this.l, this.m);
    }

    @DexIgnore
    public final void L() {
        if (K()) {
            Q();
        } else {
            N();
        }
    }

    @DexIgnore
    public final void M() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Gi(this, null), 3, null);
    }

    @DexIgnore
    public final void N() {
        this.j.l(new U37<>(Ei.Bii.a));
    }

    @DexIgnore
    public final void O() {
        String J = PortfolioApp.get.instance().J();
        if (!TextUtils.isEmpty(J)) {
            this.t.e(new SetActivityGoalUserCase.Bi(J, this.l.e()), new Hi(this));
        }
    }

    @DexIgnore
    public final void P(int i2, Rh5 rh5) {
        Wg6.c(rh5, "type");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "setSettingValue value=" + i2 + " type=" + rh5);
        int i3 = Ip6.a[rh5.ordinal()];
        if (i3 == 1) {
            this.l.n(i2);
        } else if (i3 == 2) {
            this.l.k(i2);
        } else if (i3 == 3) {
            this.l.j(i2);
        } else if (i3 == 4) {
            this.l.m(i2);
        } else if (i3 == 5) {
            this.l.l(i2);
        }
        F();
    }

    @DexIgnore
    public final void Q() {
        this.j.l(new U37<>(Ei.Cii.a));
    }

    @DexIgnore
    public final void R(int i2, String str) {
        this.j.l(new U37<>(new Ei.Dii(i2, str)));
    }

    @DexIgnore
    public final void S(List<? extends Uh5> list) {
        this.j.l(new U37<>(new Ei.Eii(list)));
    }

    @DexIgnore
    @Override // com.fossil.Ts0
    public void onCleared() {
        this.t.t();
    }
}
