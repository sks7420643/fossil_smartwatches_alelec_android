package com.portfolio.platform.uirenew.home.customize.diana.complications.search;

import android.content.Intent;
import android.os.Bundle;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.E96;
import com.fossil.F96;
import com.mapped.Iface;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationSearchActivity extends BaseActivity {
    @DexIgnore
    public ComplicationSearchPresenter A;

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        E96 e96 = (E96) getSupportFragmentManager().Y(2131362158);
        if (e96 == null) {
            e96 = E96.l.b();
            k(e96, E96.l.a(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (e96 != null) {
            iface.P(new F96(e96)).a(this);
            Intent intent = getIntent();
            Wg6.b(intent, "intent");
            Bundle extras = intent.getExtras();
            if (extras != null) {
                ComplicationSearchPresenter complicationSearchPresenter = this.A;
                if (complicationSearchPresenter != null) {
                    String string = extras.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string == null) {
                        string = "empty";
                    }
                    String string2 = extras.getString("bottom");
                    if (string2 == null) {
                        string2 = "empty";
                    }
                    String string3 = extras.getString("right");
                    if (string3 == null) {
                        string3 = "empty";
                    }
                    String string4 = extras.getString(ViewHierarchy.DIMENSION_LEFT_KEY);
                    if (string4 == null) {
                        string4 = "empty";
                    }
                    complicationSearchPresenter.D(string, string2, string3, string4);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
            if (bundle != null) {
                ComplicationSearchPresenter complicationSearchPresenter2 = this.A;
                if (complicationSearchPresenter2 != null) {
                    String string5 = bundle.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string5 == null) {
                        string5 = "empty";
                    }
                    String string6 = bundle.getString("bottom");
                    if (string6 == null) {
                        string6 = "empty";
                    }
                    String string7 = bundle.getString("right");
                    if (string7 == null) {
                        string7 = "empty";
                    }
                    String string8 = bundle.getString(ViewHierarchy.DIMENSION_LEFT_KEY);
                    if (string8 == null) {
                        string8 = "empty";
                    }
                    complicationSearchPresenter2.D(string5, string6, string7, string8);
                    return;
                }
                Wg6.n("mPresenter");
                throw null;
            }
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchContract.View");
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.c(bundle, "outState");
        ComplicationSearchPresenter complicationSearchPresenter = this.A;
        if (complicationSearchPresenter != null) {
            complicationSearchPresenter.A(bundle);
            if (bundle != null) {
                super.onSaveInstanceState(bundle);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }
}
