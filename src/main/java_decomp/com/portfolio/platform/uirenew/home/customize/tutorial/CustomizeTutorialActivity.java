package com.portfolio.platform.uirenew.home.customize.tutorial;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.S66;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeTutorialActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            Wg6.c(context, "context");
            Wg6.c(str, "watchAppId");
            Intent intent = new Intent(context, CustomizeTutorialActivity.class);
            intent.putExtra("EXTRA_WATCHAPP_ID", str);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        if (((S66) getSupportFragmentManager().Y(2131362158)) == null) {
            String stringExtra = getIntent().getStringExtra("EXTRA_WATCHAPP_ID");
            S66.Ai ai = S66.k;
            Wg6.b(stringExtra, "watchAppId");
            k(ai.a(stringExtra), "CustomizeTutorialFragment", 2131362158);
        }
    }
}
