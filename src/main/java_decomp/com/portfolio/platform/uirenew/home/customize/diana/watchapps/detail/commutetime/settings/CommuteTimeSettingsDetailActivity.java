package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.Da6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsDetailActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, Bundle bundle, int i) {
            Wg6.c(fragment, "fragment");
            Wg6.c(bundle, "bundle");
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsDetailActivity.class);
            intent.putExtra("KEY_BUNDLE_SETTING_ADDRESS", bundle);
            fragment.startActivityForResult(intent, i);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((Da6) getSupportFragmentManager().Y(2131362158)) == null) {
            Bundle bundleExtra = getIntent().getBundleExtra("KEY_BUNDLE_SETTING_ADDRESS");
            k(Da6.s.b((AddressWrapper) bundleExtra.getParcelable("KEY_SELECTED_ADDRESS"), bundleExtra.getStringArrayList("KEY_LIST_ADDRESS"), bundleExtra.getBoolean("KEY_HAVING_MAP_RESULT")), Da6.s.a(), 2131362158);
        }
    }
}
