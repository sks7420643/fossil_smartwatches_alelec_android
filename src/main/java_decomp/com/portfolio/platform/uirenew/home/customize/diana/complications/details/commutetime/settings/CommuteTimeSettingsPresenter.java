package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.text.TextUtils;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Mh5;
import com.fossil.R76;
import com.fossil.S76;
import com.fossil.Yn7;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jh6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsPresenter extends R76 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public PlacesClient e;
    @DexIgnore
    public Gson f; // = new Gson();
    @DexIgnore
    public CommuteTimeSetting g;
    @DexIgnore
    public ArrayList<String> h; // = new ArrayList<>();
    @DexIgnore
    public MFUser i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ S76 k;
    @DexIgnore
    public /* final */ An4 l;
    @DexIgnore
    public /* final */ UserRepository m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2", f = "CommuteTimeSettingsPresenter.kt", l = {155, 162}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $address;
        @DexIgnore
        public /* final */ /* synthetic */ String $displayInfo;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2$1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Jh6 $searchedRecent;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Jh6 jh6, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
                this.$searchedRecent = jh6;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$searchedRecent, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.l.I0(this.$searchedRecent.element);
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2$searchedRecent$1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<String>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<String>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.l.h();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(CommuteTimeSettingsPresenter commuteTimeSettingsPresenter, String str, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = commuteTimeSettingsPresenter;
            this.$address = str;
            this.$displayInfo = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, this.$address, this.$displayInfo, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0060  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 218
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2", f = "CommuteTimeSettingsPresenter.kt", l = {67, 68}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2$1", f = "CommuteTimeSettingsPresenter.kt", l = {67}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.m;
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2$recentSearchedAddress$1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<String>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<String>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.l.h();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(CommuteTimeSettingsPresenter commuteTimeSettingsPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = commuteTimeSettingsPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00b5  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                r7 = 0
                r6 = 2
                r5 = 1
                java.lang.Object r4 = com.fossil.Yn7.d()
                int r0 = r8.label
                if (r0 == 0) goto L_0x00b7
                if (r0 == r5) goto L_0x008d
                if (r0 != r6) goto L_0x0085
                java.lang.Object r0 = r8.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r9)
                r0 = r9
            L_0x0017:
                java.lang.String r1 = "withContext(IO) { mShare\u2026r.addressSearchedRecent }"
                com.mapped.Wg6.b(r0, r1)
                java.util.List r0 = (java.util.List) r0
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter r1 = r8.this$0
                java.util.ArrayList r1 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter.y(r1)
                r1.clear()
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter r1 = r8.this$0
                java.util.ArrayList r1 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter.y(r1)
                r1.addAll(r0)
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter r0 = r8.this$0
                com.portfolio.platform.data.model.MFUser r2 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter.A(r0)
                if (r2 == 0) goto L_0x0064
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter r0 = r8.this$0
                com.fossil.S76 r3 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter.C(r0)
                com.portfolio.platform.data.model.MFUser$Address r0 = r2.getAddresses()
                java.lang.String r1 = ""
                if (r0 == 0) goto L_0x00d5
                java.lang.String r0 = r0.getHome()
                if (r0 == 0) goto L_0x00d5
            L_0x004c:
                r3.k2(r0)
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter r0 = r8.this$0
                com.fossil.S76 r3 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter.C(r0)
                com.portfolio.platform.data.model.MFUser$Address r0 = r2.getAddresses()
                if (r0 == 0) goto L_0x00d9
                java.lang.String r0 = r0.getWork()
                if (r0 == 0) goto L_0x00d9
            L_0x0061:
                r3.Q4(r0)
            L_0x0064:
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter r0 = r8.this$0
                com.fossil.S76 r0 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter.C(r0)
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter r1 = r8.this$0
                java.util.ArrayList r1 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter.y(r1)
                r0.h0(r1)
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter r0 = r8.this$0
                com.fossil.S76 r0 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter.C(r0)
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter r1 = r8.this$0
                com.portfolio.platform.data.model.setting.CommuteTimeSetting r1 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter.x(r1)
                r0.s4(r1)
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0084:
                return r0
            L_0x0085:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x008d:
                java.lang.Object r0 = r8.L$1
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter r0 = (com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter) r0
                java.lang.Object r1 = r8.L$0
                com.mapped.Il6 r1 = (com.mapped.Il6) r1
                com.fossil.El7.b(r9)
                r3 = r0
                r2 = r9
            L_0x009a:
                r0 = r2
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter.D(r3, r0)
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter r0 = r8.this$0
                com.fossil.Dv7 r0 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter.w(r0)
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$Bi$Bii r2 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$Bi$Bii
                r2.<init>(r8, r7)
                r8.L$0 = r1
                r8.label = r6
                java.lang.Object r0 = com.fossil.Eu7.g(r0, r2, r8)
                if (r0 != r4) goto L_0x0017
                r0 = r4
                goto L_0x0084
            L_0x00b7:
                com.fossil.El7.b(r9)
                com.mapped.Il6 r1 = r8.p$
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter r0 = r8.this$0
                com.fossil.Dv7 r2 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter.w(r0)
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$Bi$Aii r3 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$Bi$Aii
                r3.<init>(r8, r7)
                r8.L$0 = r1
                r8.L$1 = r0
                r8.label = r5
                java.lang.Object r2 = com.fossil.Eu7.g(r2, r3, r8)
                if (r2 != r4) goto L_0x00db
                r0 = r4
                goto L_0x0084
            L_0x00d5:
                java.lang.String r0 = ""
                goto L_0x004c
            L_0x00d9:
                r0 = r1
                goto L_0x0061
            L_0x00db:
                r3 = r0
                goto L_0x009a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = CommuteTimeSettingsPresenter.class.getSimpleName();
        Wg6.b(simpleName, "CommuteTimeSettingsPrese\u2026er::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public CommuteTimeSettingsPresenter(S76 s76, An4 an4, UserRepository userRepository) {
        Wg6.c(s76, "mView");
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(userRepository, "mUserRepository");
        this.k = s76;
        this.l = an4;
        this.m = userRepository;
    }

    @DexIgnore
    public void E(String str) {
        CommuteTimeSetting commuteTimeSetting;
        Wg6.c(str, MicroAppSetting.SETTING);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = n;
        local.d(str2, "initSeting " + str);
        try {
            commuteTimeSetting = (CommuteTimeSetting) this.f.k(str, CommuteTimeSetting.class);
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = n;
            local2.d(str3, "exception when parse commute time setting " + e2);
            commuteTimeSetting = new CommuteTimeSetting(null, null, false, null, 15, null);
        }
        this.g = commuteTimeSetting;
        if (commuteTimeSetting == null) {
            this.g = new CommuteTimeSetting(null, null, false, null, 15, null);
        }
    }

    @DexIgnore
    public void F() {
        this.k.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        PlacesClient createClient = Places.createClient(PortfolioApp.get.instance());
        this.e = createClient;
        this.k.H(createClient);
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            if (!TextUtils.isEmpty(commuteTimeSetting.getAddress())) {
                this.k.w2(commuteTimeSetting.getAddress());
            }
            this.k.p6(commuteTimeSetting.getAvoidTolls());
        }
        Rm6 unused = Gu7.d(k(), null, null, new Bi(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        this.e = null;
    }

    @DexIgnore
    @Override // com.fossil.R76
    public void n() {
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            commuteTimeSetting.setAddress("");
        }
    }

    @DexIgnore
    @Override // com.fossil.R76
    public CommuteTimeSetting o() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.R76
    public void p() {
        String str;
        MFUser.Address addresses;
        MFUser.Address addresses2;
        MFUser mFUser = this.i;
        if (TextUtils.isEmpty((mFUser == null || (addresses2 = mFUser.getAddresses()) == null) ? null : addresses2.getHome())) {
            this.j = false;
            this.k.c2("Home", "");
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            MFUser mFUser2 = this.i;
            if (mFUser2 == null || (addresses = mFUser2.getAddresses()) == null || (str = addresses.getHome()) == null) {
                str = "";
            }
            commuteTimeSetting.setAddress(str);
        }
        this.k.x0(true);
    }

    @DexIgnore
    @Override // com.fossil.R76
    public void q() {
        String str;
        MFUser.Address addresses;
        if (this.g != null) {
            this.j = true;
            S76 s76 = this.k;
            MFUser mFUser = this.i;
            if (mFUser == null || (addresses = mFUser.getAddresses()) == null || (str = addresses.getHome()) == null) {
                str = "";
            }
            s76.c2("Home", str);
        }
    }

    @DexIgnore
    @Override // com.fossil.R76
    public void r(String str) {
        Wg6.c(str, "address");
        if (this.j) {
            this.j = false;
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null && !TextUtils.isEmpty(str)) {
            commuteTimeSetting.setAddress(str);
            this.k.w2(str);
        }
    }

    @DexIgnore
    @Override // com.fossil.R76
    public void s() {
        MFUser.Address addresses;
        MFUser mFUser = this.i;
        if (TextUtils.isEmpty((mFUser == null || (addresses = mFUser.getAddresses()) == null) ? null : addresses.getWork())) {
            this.j = false;
            this.k.c2("Other", "");
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            MFUser mFUser2 = this.i;
            if (mFUser2 != null) {
                MFUser.Address addresses2 = mFUser2.getAddresses();
                if (addresses2 != null) {
                    String work = addresses2.getWork();
                    if (work != null) {
                        commuteTimeSetting.setAddress(work);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
        this.k.x0(true);
    }

    @DexIgnore
    @Override // com.fossil.R76
    public void t() {
        String str;
        MFUser.Address addresses;
        if (this.g != null) {
            this.j = true;
            S76 s76 = this.k;
            MFUser mFUser = this.i;
            if (mFUser == null || (addresses = mFUser.getAddresses()) == null || (str = addresses.getWork()) == null) {
                str = "";
            }
            s76.c2("Other", str);
        }
    }

    @DexIgnore
    @Override // com.fossil.R76
    public void u(String str, String str2, Mh5 mh5, boolean z, String str3) {
        boolean z2 = true;
        Wg6.c(str, "displayInfo");
        Wg6.c(str2, "address");
        Wg6.c(mh5, "directionBy");
        Wg6.c(str3, "format");
        FLogger.INSTANCE.getLocal().d(n, "saveCommuteTimeSetting - displayInfo=" + str + ", address=" + str2 + ", directionBy=" + mh5.getType() + ", avoidTolls=" + z + ", format=" + str3);
        this.k.b();
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            if (str.length() == 0) {
                if (str2.length() != 0) {
                    z2 = false;
                }
                if (z2) {
                    commuteTimeSetting.setAddress(str);
                }
            } else {
                commuteTimeSetting.setAddress(str2);
            }
            commuteTimeSetting.setAvoidTolls(z);
            commuteTimeSetting.setFormat(str3);
        }
        Rm6 unused = Gu7.d(k(), null, null, new Ai(this, str2, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.R76
    public void v(String str) {
        Wg6.c(str, "format");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = n;
        local.d(str2, "updateFormatType, format = " + str);
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            commuteTimeSetting.setFormat(str);
        }
        this.k.s4(this.g);
    }
}
