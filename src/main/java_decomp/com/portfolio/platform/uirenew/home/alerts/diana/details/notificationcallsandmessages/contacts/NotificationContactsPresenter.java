package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Ao7;
import com.fossil.At0;
import com.fossil.D26;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.G26;
import com.fossil.Gu7;
import com.fossil.I16;
import com.fossil.J06;
import com.fossil.J16;
import com.fossil.Jn5;
import com.fossil.K16;
import com.fossil.Ko7;
import com.fossil.Mm7;
import com.fossil.Pm7;
import com.fossil.Tq4;
import com.fossil.Uq4;
import com.fossil.W37;
import com.fossil.Yn7;
import com.fossil.Zs0;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationContactsPresenter extends I16 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public /* final */ List<J06> e; // = new ArrayList();
    @DexIgnore
    public /* final */ List<J06> f; // = new ArrayList();
    @DexIgnore
    public /* final */ J16 g;
    @DexIgnore
    public /* final */ Uq4 h;
    @DexIgnore
    public /* final */ D26 i;
    @DexIgnore
    public /* final */ G26 j;
    @DexIgnore
    public /* final */ LoaderManager k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return NotificationContactsPresenter.l;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Tq4.Di<G26.Bi, Tq4.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(NotificationContactsPresenter notificationContactsPresenter) {
            this.a = notificationContactsPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void a(Tq4.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(Tq4.Ai ai) {
            FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.m.a(), ".Inside mSaveContactGroupsNotification onError");
            this.a.g.close();
        }

        @DexIgnore
        public void c(G26.Bi bi) {
            FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.m.a(), ".Inside mSaveContactGroupsNotification onSuccess");
            this.a.g.close();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void onSuccess(G26.Bi bi) {
            c(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1", f = "NotificationContactsPresenter.kt", l = {49}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$1", f = "NotificationContactsPresenter.kt", l = {50}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    PortfolioApp instance = PortfolioApp.get.instance();
                    this.L$0 = il6;
                    this.label = 1;
                    if (instance.V0(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements CoroutineUseCase.Ei<D26.Ci, D26.Ai> {
            @DexIgnore
            public /* final */ /* synthetic */ Ci a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1", f = "NotificationContactsPresenter.kt", l = {60}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ D26.Ci $responseValue;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Bii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1$1", f = "NotificationContactsPresenter.kt", l = {}, m = "invokeSuspend")
                public static final class Aiiii extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Aiii aiii, Xe6 xe6) {
                        super(2, xe6);
                        this.this$0 = aiii;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        Aiiii aiiii = new Aiiii(this.this$0, xe6);
                        aiiii.p$ = (Il6) obj;
                        throw null;
                        //return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
                        throw null;
                        //return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Object invokeSuspend(Object obj) {
                        Yn7.d();
                        if (this.label == 0) {
                            El7.b(obj);
                            for (T t : this.this$0.$responseValue.a()) {
                                List<Contact> contacts = t.getContacts();
                                Wg6.b(contacts, "it.contacts");
                                if (!contacts.isEmpty()) {
                                    Contact contact = t.getContacts().get(0);
                                    J06 j06 = new J06(contact, null, 2, null);
                                    j06.setAdded(true);
                                    Contact contact2 = j06.getContact();
                                    if (contact2 != null) {
                                        Wg6.b(contact, "contact");
                                        contact2.setDbRowId(contact.getDbRowId());
                                    }
                                    Contact contact3 = j06.getContact();
                                    if (contact3 != null) {
                                        Wg6.b(contact, "contact");
                                        contact3.setUseSms(contact.isUseSms());
                                    }
                                    Contact contact4 = j06.getContact();
                                    if (contact4 != null) {
                                        Wg6.b(contact, "contact");
                                        contact4.setUseCall(contact.isUseCall());
                                    }
                                    Wg6.b(contact, "contact");
                                    List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                                    Wg6.b(phoneNumbers, "contact.phoneNumbers");
                                    if (!phoneNumbers.isEmpty()) {
                                        PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                                        Wg6.b(phoneNumber, "contact.phoneNumbers[0]");
                                        if (!TextUtils.isEmpty(phoneNumber.getNumber())) {
                                            j06.setHasPhoneNumber(true);
                                            PhoneNumber phoneNumber2 = contact.getPhoneNumbers().get(0);
                                            Wg6.b(phoneNumber2, "contact.phoneNumbers[0]");
                                            j06.setPhoneNumber(phoneNumber2.getNumber());
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String a2 = NotificationContactsPresenter.m.a();
                                            StringBuilder sb = new StringBuilder();
                                            sb.append(".Inside loadContactData filter selected contact, phoneNumber=");
                                            PhoneNumber phoneNumber3 = contact.getPhoneNumbers().get(0);
                                            Wg6.b(phoneNumber3, "contact.phoneNumbers[0]");
                                            sb.append(phoneNumber3.getNumber());
                                            local.d(a2, sb.toString());
                                        }
                                    }
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String a3 = NotificationContactsPresenter.m.a();
                                    local2.d(a3, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                                    this.this$0.this$0.a.this$0.v().add(j06);
                                }
                            }
                            return Ao7.a(this.this$0.this$0.a.this$0.w().addAll(Pm7.j0(this.this$0.this$0.a.this$0.v())));
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Bii bii, D26.Ci ci, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = bii;
                    this.$responseValue = ci;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$responseValue, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Dv7 h = this.this$0.a.this$0.h();
                        Aiiii aiiii = new Aiiii(this, null);
                        this.L$0 = il6;
                        this.label = 1;
                        if (Eu7.g(h, aiiii, this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    this.this$0.a.this$0.g.j5(this.this$0.a.this$0.v(), W37.a.a());
                    this.this$0.a.this$0.k.d(0, new Bundle(), this.this$0.a.this$0);
                    return Cd6.a;
                }
            }

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Bii(Ci ci) {
                this.a = ci;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void a(D26.Ai ai) {
                b(ai);
            }

            @DexIgnore
            public void b(D26.Ai ai) {
                Wg6.c(ai, "errorValue");
                FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.m.a(), "GetAllContactGroup onError");
            }

            @DexIgnore
            public void c(D26.Ci ci) {
                Wg6.c(ci, "responseValue");
                FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.m.a(), "GetAllContactGroup onSuccess");
                Rm6 unused = Gu7.d(this.a.this$0.k(), null, null, new Aiii(this, ci, null), 3, null);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void onSuccess(D26.Ci ci) {
                c(ci);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(NotificationContactsPresenter notificationContactsPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationContactsPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                if (!PortfolioApp.get.instance().k0().s0()) {
                    Dv7 h = this.this$0.h();
                    Aii aii = new Aii(null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (Eu7.g(h, aii, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.v().isEmpty()) {
                this.this$0.i.e(null, new Bii(this));
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = NotificationContactsPresenter.class.getSimpleName();
        Wg6.b(simpleName, "NotificationContactsPres\u2026er::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public NotificationContactsPresenter(J16 j16, Uq4 uq4, D26 d26, G26 g26, LoaderManager loaderManager) {
        Wg6.c(j16, "mView");
        Wg6.c(uq4, "mUseCaseHandler");
        Wg6.c(d26, "mGetAllContactGroup");
        Wg6.c(g26, "mSaveContactGroupsNotification");
        Wg6.c(loaderManager, "mLoaderManager");
        this.g = j16;
        this.h = uq4;
        this.i = d26;
        this.j = g26;
        this.k = loaderManager;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.At0, java.lang.Object] */
    @Override // androidx.loader.app.LoaderManager.a
    public /* bridge */ /* synthetic */ void a(At0<Cursor> at0, Cursor cursor) {
        x(at0, cursor);
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public At0<Cursor> d(int i2, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onCreateLoader, selection = has_phone_number!=0 AND mimetype=?");
        return new Zs0(PortfolioApp.get.instance(), ContactsContract.Data.CONTENT_URI, new String[]{"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"}, "has_phone_number!=0 AND mimetype=?", new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public void g(At0<Cursor> at0) {
        Wg6.c(at0, "loader");
        FLogger.INSTANCE.getLocal().d(l, ".Inside onLoaderReset");
        this.g.V();
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(l, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Jn5 jn5 = Jn5.b;
        J16 j16 = this.g;
        if (j16 == null) {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsFragment");
        } else if (Jn5.c(jn5, ((K16) j16).getContext(), Jn5.Ai.NOTIFICATION_GET_CONTACTS, false, true, false, null, 52, null)) {
            Rm6 unused = Gu7.d(k(), null, null, new Ci(this, null), 3, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(l, "stop");
    }

    @DexIgnore
    @Override // com.fossil.I16
    public void n() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        List V = Pm7.V(this.f, this.e);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : V) {
            Contact contact = ((J06) obj).getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Object obj2 = linkedHashMap.get(valueOf);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(valueOf, obj2);
            }
            ((List) obj2).add(obj);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            if (((List) entry.getValue()).size() == 1) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList<J06> arrayList3 = new ArrayList();
        for (Map.Entry entry2 : linkedHashMap2.entrySet()) {
            Mm7.s(arrayList3, (List) entry2.getValue());
        }
        for (J06 j06 : arrayList3) {
            Iterator<T> it = this.f.iterator();
            boolean z = false;
            while (it.hasNext()) {
                Contact contact2 = it.next().getContact();
                Integer valueOf2 = contact2 != null ? Integer.valueOf(contact2.getContactId()) : null;
                Contact contact3 = j06.getContact();
                if (Wg6.a(valueOf2, contact3 != null ? Integer.valueOf(contact3.getContactId()) : null)) {
                    arrayList.add(j06);
                    z = true;
                }
            }
            if (!z) {
                arrayList2.add(j06);
            }
        }
        this.h.a(this.j, new G26.Ai(arrayList, arrayList2), new Bi(this));
    }

    @DexIgnore
    @Override // com.fossil.I16
    public void o() {
        FLogger.INSTANCE.getLocal().d(l, "onListContactWrapperChanged");
        List V = Pm7.V(this.f, this.e);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : V) {
            Contact contact = ((J06) obj).getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Object obj2 = linkedHashMap.get(valueOf);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(valueOf, obj2);
            }
            ((List) obj2).add(obj);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            if (((List) entry.getValue()).size() == 1) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList arrayList = new ArrayList();
        for (Map.Entry entry2 : linkedHashMap2.entrySet()) {
            Mm7.s(arrayList, (List) entry2.getValue());
        }
        if (arrayList.isEmpty()) {
            this.g.f6(false);
        } else {
            this.g.f6(true);
        }
    }

    @DexIgnore
    public final List<J06> v() {
        return this.e;
    }

    @DexIgnore
    public final List<J06> w() {
        return this.f;
    }

    @DexIgnore
    public void x(At0<Cursor> at0, Cursor cursor) {
        Wg6.c(at0, "loader");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onLoadFinished cursor=" + cursor);
        this.g.T(cursor);
    }

    @DexIgnore
    public void y() {
        this.g.M5(this);
    }
}
