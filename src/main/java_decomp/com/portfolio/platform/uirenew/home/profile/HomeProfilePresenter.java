package com.portfolio.platform.uirenew.home.profile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.Ao6;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Fj1;
import com.fossil.Gu7;
import com.fossil.H47;
import com.fossil.Hk5;
import com.fossil.I37;
import com.fossil.It4;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Ps4;
import com.fossil.Ry5;
import com.fossil.Tj5;
import com.fossil.Tt4;
import com.fossil.U08;
import com.fossil.Vk5;
import com.fossil.Vt7;
import com.fossil.W08;
import com.fossil.Wc1;
import com.fossil.Xh5;
import com.fossil.Xy4;
import com.fossil.Yn6;
import com.fossil.Yn7;
import com.fossil.Zn6;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.HomeProfileFragment;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.domain.ProfileRepository;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.user.information.domain.usecase.GetUser;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.util.DeviceUtils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeProfilePresenter extends Zn6 {
    @DexIgnore
    public static /* final */ String G;
    @DexIgnore
    public static /* final */ Ai H; // = new Ai(null);
    @DexIgnore
    public /* final */ SummariesRepository A;
    @DexIgnore
    public /* final */ SleepSummariesRepository B;
    @DexIgnore
    public /* final */ DeleteLogoutUserUseCase C;
    @DexIgnore
    public /* final */ ProfileRepository D;
    @DexIgnore
    public /* final */ Tt4 E;
    @DexIgnore
    public /* final */ An4 F;
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.get.instance().J());
    @DexIgnore
    public /* final */ ArrayList<Bi> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Device> g; // = new ArrayList<>();
    @DexIgnore
    public MFUser h;
    @DexIgnore
    public LiveData<H47<ActivityStatistic>> i; // = new MutableLiveData();
    @DexIgnore
    public LiveData<H47<ActivitySummary>> j; // = new MutableLiveData();
    @DexIgnore
    public LiveData<H47<SleepStatistic>> k; // = new MutableLiveData();
    @DexIgnore
    public /* final */ LiveData<List<Device>> l; // = this.y.getAllDeviceAsLiveData();
    @DexIgnore
    public ActivityStatistic.ActivityDailyBest m;
    @DexIgnore
    public long n;
    @DexIgnore
    public /* final */ U08 o; // = W08.b(false, 1, null);
    @DexIgnore
    public /* final */ Handler p; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public boolean q; // = true;
    @DexIgnore
    public String r;
    @DexIgnore
    public /* final */ Runnable s; // = new Hi(this);
    @DexIgnore
    public /* final */ Gi t; // = new Gi(this);
    @DexIgnore
    public /* final */ WeakReference<Ao6> u;
    @DexIgnore
    public /* final */ PortfolioApp v;
    @DexIgnore
    public /* final */ GetUser w;
    @DexIgnore
    public /* final */ UpdateUser x;
    @DexIgnore
    public /* final */ DeviceRepository y;
    @DexIgnore
    public /* final */ UserRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return HomeProfilePresenter.G;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public String a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public String c;
        @DexIgnore
        public int d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public Bi(String str, boolean z, String str2, int i, boolean z2, boolean z3) {
            Wg6.c(str, "serial");
            Wg6.c(str2, "deviceName");
            this.a = str;
            this.b = z;
            this.c = str2;
            this.d = i;
            this.e = z2;
            this.f = z3;
        }

        @DexIgnore
        public final int a() {
            return this.d;
        }

        @DexIgnore
        public final String b() {
            return this.c;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }

        @DexIgnore
        public final boolean d() {
            return this.e;
        }

        @DexIgnore
        public final boolean e() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Bi) {
                    Bi bi = (Bi) obj;
                    if (!(Wg6.a(this.a, bi.a) && this.b == bi.b && Wg6.a(this.c, bi.c) && this.d == bi.d && this.e == bi.e && this.f == bi.f)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final void f(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            int i2 = 1;
            String str = this.a;
            int hashCode = str != null ? str.hashCode() : 0;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            String str2 = this.c;
            if (str2 != null) {
                i = str2.hashCode();
            }
            int i3 = this.d;
            boolean z2 = this.e;
            if (z2) {
                z2 = true;
            }
            boolean z3 = this.f;
            if (!z3) {
                i2 = z3 ? 1 : 0;
            }
            int i4 = z ? 1 : 0;
            int i5 = z ? 1 : 0;
            int i6 = z ? 1 : 0;
            int i7 = z2 ? 1 : 0;
            int i8 = z2 ? 1 : 0;
            int i9 = z2 ? 1 : 0;
            return (((((((((hashCode * 31) + i4) * 31) + i) * 31) + i3) * 31) + i7) * 31) + i2;
        }

        @DexIgnore
        public String toString() {
            return "DeviceWrapper(serial=" + this.a + ", isConnected=" + this.b + ", deviceName=" + this.c + ", batteryLevel=" + this.d + ", isActive=" + this.e + ", isLatestFw=" + this.f + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$checkCurrentChallenge$1", f = "HomeProfilePresenter.kt", l = {430}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isAddNewDevice;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$checkCurrentChallenge$1$challenge$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Ps4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ps4> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.E.g(new String[]{"running", "waiting"}, Xy4.a.a());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(HomeProfilePresenter homeProfilePresenter, boolean z, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeProfilePresenter;
            this.$isAddNewDevice = z;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$isAddNewDevice, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                if (this.this$0.q) {
                    Dv7 b = Bw7.b();
                    Aii aii = new Aii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    g = Eu7.g(b, aii, this);
                    if (g == d) {
                        return d;
                    }
                } else {
                    Ao6 ao6 = this.this$0.d0().get();
                    if (ao6 != null) {
                        ao6.H3(false, null, this.$isAddNewDevice);
                    }
                    return Cd6.a;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Ps4 ps4 = (Ps4) g;
            if (ps4 == null) {
                this.this$0.F.m0(Ao7.a(false));
                Ao6 ao62 = this.this$0.d0().get();
                if (ao62 != null) {
                    ao62.H3(false, ps4, this.$isAddNewDevice);
                }
            } else {
                Ao6 ao63 = this.this$0.d0().get();
                if (ao63 != null) {
                    ao63.H3(true, ps4, this.$isAddNewDevice);
                }
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$loadSocialProfile$1", f = "HomeProfilePresenter.kt", l = {289}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$loadSocialProfile$1$socialProfile$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super It4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super It4> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.D.d();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(HomeProfilePresenter homeProfilePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeProfilePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                HomeProfilePresenter homeProfilePresenter = this.this$0;
                Boolean b = homeProfilePresenter.F.b();
                Wg6.b(b, "sharedPreferencesManager.bcStatus()");
                homeProfilePresenter.q = b.booleanValue();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = HomeProfilePresenter.H.a();
                local.e(a2, "loadProfile - isBCOn: " + this.this$0.q);
                if (this.this$0.q) {
                    Dv7 b2 = Bw7.b();
                    Aii aii = new Aii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    g = Eu7.g(b2, aii, this);
                    if (g == d) {
                        return d;
                    }
                } else {
                    Ao6 ao6 = this.this$0.d0().get();
                    if (ao6 != null) {
                        ao6.i5(null);
                    }
                    return Cd6.a;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            It4 it4 = (It4) g;
            Ao6 ao62 = this.this$0.d0().get();
            if (ao62 != null) {
                ao62.i5(it4 != null ? it4.g() : null);
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements CoroutineUseCase.Ei<GetUser.Ai, CoroutineUseCase.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(CoroutineUseCase.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(CoroutineUseCase.Ai ai) {
            Wg6.c(ai, "errorValue");
        }

        @DexIgnore
        public void c(GetUser.Ai ai) {
            Wg6.c(ai, "responseValue");
            MFUser a2 = ai.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = HomeProfilePresenter.H.a();
            local.d(a3, "loadUser " + a2);
            if (a2 != null) {
                this.a.h0(a2);
                Ao6 ao6 = this.a.d0().get();
                if (ao6 != null) {
                    ao6.updateUser(a2);
                }
                Ao6 ao62 = this.a.d0().get();
                if (ao62 != null) {
                    Ao6.Ai.a(ao62, a2, null, 2, null);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(GetUser.Ai ai) {
            c(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements CoroutineUseCase.Ei<DeleteLogoutUserUseCase.Di, DeleteLogoutUserUseCase.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        public Fi(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(DeleteLogoutUserUseCase.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(DeleteLogoutUserUseCase.Ci ci) {
            Wg6.c(ci, "errorValue");
            Ao6 ao6 = this.a.d0().get();
            if (ao6 != null) {
                ao6.k();
                ao6.o(ci.a(), "");
            }
        }

        @DexIgnore
        public void c(DeleteLogoutUserUseCase.Di di) {
            Wg6.c(di, "responseValue");
            FLogger.INSTANCE.getLocal().d(HomeProfilePresenter.H.a(), "logOut success, hide loading}");
            Ao6 ao6 = this.a.d0().get();
            if (ao6 != null) {
                ao6.k();
                ao6.i3();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(DeleteLogoutUserUseCase.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Gi(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            T t;
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, ConnectionStateChange.GATT_OFF.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeProfilePresenter.H.a();
            local.d(a2, "mConnectionStateChangeReceiver: serial = " + stringExtra + ", state = " + intExtra);
            if (Vt7.j(stringExtra, this.a.v.J(), true) && (!this.a.b0().isEmpty())) {
                Iterator<T> it = this.a.b0().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (Wg6.a(next.c(), stringExtra)) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (t2 == null) {
                    return;
                }
                if (intExtra == ConnectionStateChange.GATT_ON.ordinal() || intExtra == ConnectionStateChange.GATT_OFF.ordinal()) {
                    t2.f(intExtra == ConnectionStateChange.GATT_ON.ordinal());
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = HomeProfilePresenter.H.a();
                    local2.d(a3, "active device status change connected=" + t2.e());
                    Ao6 ao6 = this.a.d0().get();
                    if (ao6 != null) {
                        ao6.G4(this.a.b0());
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter b;

        @DexIgnore
        public Hi(HomeProfilePresenter homeProfilePresenter) {
            this.b = homeProfilePresenter;
        }

        @DexIgnore
        public final void run() {
            Ao6 ao6 = this.b.d0().get();
            if (ao6 != null) {
                Wg6.b(ao6, "it");
                if (ao6.isActive() && (!this.b.b0().isEmpty())) {
                    ao6.z2();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Ii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Uri $imageUri;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(HomeProfilePresenter homeProfilePresenter, Uri uri, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeProfilePresenter;
            this.$imageUri = uri;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ii ii = new Ii(this.this$0, this.$imageUri, xe6);
            ii.p$ = (Il6) obj;
            throw null;
            //return ii;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ii) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                try {
                    this.this$0.r = I37.f(!TextUtils.equals(this.$imageUri.getLastPathSegment(), "pickerImage.jpg") ? (Bitmap) Tj5.a(PortfolioApp.get.instance()).D().d1(this.$imageUri).T0(((Fj1) ((Fj1) new Fj1().l(Wc1.a)).m0(true)).o0(new Hk5())).R0(200, 200).get() : Ry5.q(this.$imageUri.getPath(), 256, true));
                    MFUser c0 = this.this$0.c0();
                    if (c0 != null) {
                        String str = this.this$0.r;
                        if (str != null) {
                            c0.setProfilePicture(str);
                            this.this$0.k0(this.this$0.c0());
                            return Cd6.a;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                } catch (Exception e) {
                    e.printStackTrace();
                    Ao6 ao6 = this.this$0.d0().get();
                    if (ao6 != null) {
                        ao6.E0();
                    }
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1", f = "HomeProfilePresenter.kt", l = {416}, m = "invokeSuspend")
    public static final class Ji extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1$1$1$pickerImageIntent$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Intent>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ FragmentActivity $it;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(FragmentActivity fragmentActivity, Xe6 xe6) {
                super(2, xe6);
                this.$it = fragmentActivity;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$it, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Intent> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return Vk5.f(this.$it);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(HomeProfilePresenter homeProfilePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeProfilePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ji ji = new Ji(this.this$0, xe6);
            ji.p$ = (Il6) obj;
            throw null;
            //return ji;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ji) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Ao6 ao6;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Ao6 ao62 = this.this$0.d0().get();
                if (ao62 != null) {
                    if (ao62 != null) {
                        FragmentActivity activity = ((HomeProfileFragment) ao62).getActivity();
                        if (activity != null) {
                            Dv7 h = this.this$0.h();
                            Aii aii = new Aii(activity, null);
                            this.L$0 = il6;
                            this.L$1 = ao62;
                            this.L$2 = activity;
                            this.L$3 = activity;
                            this.label = 1;
                            g = Eu7.g(h, aii, this);
                            if (g == d) {
                                return d;
                            }
                            ao6 = ao62;
                        }
                    } else {
                        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                    }
                }
                return Cd6.a;
            } else if (i == 1) {
                FragmentActivity fragmentActivity = (FragmentActivity) this.L$3;
                FragmentActivity fragmentActivity2 = (FragmentActivity) this.L$2;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                ao6 = (Ao6) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Intent intent = (Intent) g;
            if (intent != null) {
                ((HomeProfileFragment) ao6).startActivityForResult(intent, 1234);
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$1", f = "HomeProfilePresenter.kt", l = {162, 165}, m = "invokeSuspend")
    public static final class Ki extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii<T> implements Ls0<H47<? extends ActivitySummary>> {
            @DexIgnore
            public /* final */ /* synthetic */ Ki a;

            @DexIgnore
            public Aii(Ki ki) {
                this.a = ki;
            }

            @DexIgnore
            public final void a(H47<ActivitySummary> h47) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = HomeProfilePresenter.H.a();
                local.d(a2, "XXX- summaryChanged -- summary=" + h47);
                if ((h47 != null ? h47.d() : null) != Xh5.DATABASE_LOADING) {
                    ActivitySummary c = h47 != null ? h47.c() : null;
                    this.a.this$0.n = c != null ? (long) c.getSteps() : 0;
                    HomeProfilePresenter homeProfilePresenter = this.a.this$0;
                    homeProfilePresenter.j0(homeProfilePresenter.m, this.a.this$0.n);
                }
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(H47<? extends ActivitySummary> h47) {
                a(h47);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii<T> implements Ls0<H47<? extends ActivityStatistic>> {
            @DexIgnore
            public /* final */ /* synthetic */ Ki a;

            @DexIgnore
            public Bii(Ki ki) {
                this.a = ki;
            }

            @DexIgnore
            public final void a(H47<ActivityStatistic> h47) {
                ActivityStatistic.ActivityDailyBest activityDailyBest = null;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = HomeProfilePresenter.H.a();
                local.d(a2, "start - mActivityStatisticLiveData -- resource=" + h47);
                if ((h47 != null ? h47.d() : null) != Xh5.DATABASE_LOADING) {
                    ActivityStatistic c = h47 != null ? h47.c() : null;
                    Ao6 ao6 = this.a.this$0.d0().get();
                    if (ao6 != null) {
                        ao6.X3(c);
                    }
                    HomeProfilePresenter homeProfilePresenter = this.a.this$0;
                    if (c != null) {
                        activityDailyBest = c.getStepsBestDay();
                    }
                    homeProfilePresenter.m = activityDailyBest;
                    HomeProfilePresenter homeProfilePresenter2 = this.a.this$0;
                    homeProfilePresenter2.j0(homeProfilePresenter2.m, this.a.this$0.n);
                }
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(H47<? extends ActivityStatistic> h47) {
                a(h47);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii<T> implements Ls0<H47<? extends SleepStatistic>> {
            @DexIgnore
            public /* final */ /* synthetic */ Ki a;

            @DexIgnore
            public Cii(Ki ki) {
                this.a = ki;
            }

            @DexIgnore
            public final void a(H47<SleepStatistic> h47) {
                SleepStatistic sleepStatistic = null;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = HomeProfilePresenter.H.a();
                local.d(a2, "start - mSleepStatisticLiveData -- resource=" + h47);
                if ((h47 != null ? h47.d() : null) != Xh5.DATABASE_LOADING) {
                    if (h47 != null) {
                        h47.c();
                    }
                    Ao6 ao6 = this.a.this$0.d0().get();
                    if (ao6 != null) {
                        if (h47 != null) {
                            sleepStatistic = h47.c();
                        }
                        ao6.U2(sleepStatistic);
                    }
                }
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(H47<? extends SleepStatistic> h47) {
                a(h47);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Dii<T> implements Ls0<List<? extends Device>> {
            @DexIgnore
            public /* final */ /* synthetic */ Ki a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ ArrayList $deviceWrappers;
                @DexIgnore
                public /* final */ /* synthetic */ List $devices;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public Object L$3;
                @DexIgnore
                public Object L$4;
                @DexIgnore
                public Object L$5;
                @DexIgnore
                public boolean Z$0;
                @DexIgnore
                public boolean Z$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Dii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii extends Ko7 implements Coroutine<Il6, Xe6<? super String>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ Device $deviceModel;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Device device, Xe6 xe6, Aiii aiii) {
                        super(2, xe6);
                        this.$deviceModel = device;
                        this.this$0 = aiii;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        Aiiii aiiii = new Aiiii(this.$deviceModel, xe6, this.this$0);
                        aiiii.p$ = (Il6) obj;
                        throw null;
                        //return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super String> xe6) {
                        throw null;
                        //return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Object invokeSuspend(Object obj) {
                        Yn7.d();
                        if (this.label == 0) {
                            El7.b(obj);
                            return this.this$0.this$0.a.this$0.y.getDeviceNameBySerial(this.$deviceModel.getDeviceId());
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Biiii extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ String $activeSerial;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Biiii(String str, Xe6 xe6) {
                        super(2, xe6);
                        this.$activeSerial = str;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        Biiii biiii = new Biiii(this.$activeSerial, xe6);
                        biiii.p$ = (Il6) obj;
                        throw null;
                        //return biiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
                        throw null;
                        //return ((Biiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = Yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            El7.b(obj);
                            Il6 il6 = this.p$;
                            DeviceUtils a2 = DeviceUtils.h.a();
                            String str = this.$activeSerial;
                            this.L$0 = il6;
                            this.label = 1;
                            Object j = a2.j(str, null, this);
                            return j == d ? d : j;
                        } else if (i == 1) {
                            Il6 il62 = (Il6) this.L$0;
                            El7.b(obj);
                            return obj;
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Dii dii, List list, ArrayList arrayList, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = dii;
                    this.$devices = list;
                    this.$deviceWrappers = arrayList;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$devices, this.$deviceWrappers, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:30:0x00c5 A[Catch:{ all -> 0x0276 }] */
                /* JADX WARNING: Removed duplicated region for block: B:39:0x00ee A[Catch:{ all -> 0x03b0 }] */
                /* JADX WARNING: Removed duplicated region for block: B:65:0x024a  */
                /* JADX WARNING: Removed duplicated region for block: B:84:0x02a4  */
                /* JADX WARNING: Removed duplicated region for block: B:93:0x02f6  */
                /* JADX WARNING: Removed duplicated region for block: B:96:0x0358  */
                /* JADX WARNING: Removed duplicated region for block: B:99:0x0382  */
                @Override // com.fossil.Zn7
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final java.lang.Object invokeSuspend(java.lang.Object r20) {
                    /*
                    // Method dump skipped, instructions count: 948
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter.Ki.Dii.Aiii.invokeSuspend(java.lang.Object):java.lang.Object");
                }
            }

            @DexIgnore
            public Dii(Ki ki) {
                this.a = ki;
            }

            @DexIgnore
            public final void a(List<Device> list) {
                ArrayList arrayList = new ArrayList();
                this.a.this$0.g.clear();
                ArrayList arrayList2 = this.a.this$0.g;
                if (list != null) {
                    arrayList2.addAll(list);
                    Rm6 unused = Gu7.d(this.a.this$0.k(), null, null, new Aiii(this, list, arrayList, null), 3, null);
                    return;
                }
                Wg6.i();
                throw null;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(List<? extends Device> list) {
                a(list);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(HomeProfilePresenter homeProfilePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeProfilePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ki ki = new Ki(this.this$0, xe6);
            ki.p$ = (Il6) obj;
            throw null;
            //return ki;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ki) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00d1  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00f1  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
            // Method dump skipped, instructions count: 254
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter.Ki.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements CoroutineUseCase.Ei<UpdateUser.Ci, UpdateUser.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfilePresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Li this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        UserRepository userRepository = this.this$0.this$0.a.z;
                        this.L$0 = il6;
                        this.label = 1;
                        Object currentUser = userRepository.getCurrentUser(this);
                        return currentUser == d ? d : currentUser;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Xe6 xe6, Li li) {
                super(2, xe6);
                this.this$0 = li;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Ao6 ao6;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Dv7 i2 = this.this$0.a.i();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    g = Eu7.g(i2, aiii, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                MFUser mFUser = (MFUser) g;
                if (!(mFUser == null || (ao6 = this.this$0.a.d0().get()) == null)) {
                    ao6.R4(mFUser, this.this$0.a.r);
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Li(HomeProfilePresenter homeProfilePresenter) {
            this.a = homeProfilePresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(UpdateUser.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(UpdateUser.Bi bi) {
            Wg6.c(bi, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeProfilePresenter.H.a();
            local.d(a2, ".Inside updateUser onError errorCode=" + bi.a());
            Ao6 ao6 = this.a.d0().get();
            if (ao6 != null) {
                Wg6.b(ao6, "view");
                if (ao6.isActive()) {
                    ao6.k();
                    ao6.o(bi.a(), "");
                }
            }
        }

        @DexIgnore
        public void c(UpdateUser.Ci ci) {
            Wg6.c(ci, "responseValue");
            FLogger.INSTANCE.getLocal().d(HomeProfilePresenter.H.a(), ".Inside updateUser onSuccess");
            Ao6 ao6 = this.a.d0().get();
            if (ao6 != null) {
                Wg6.b(ao6, "view");
                if (ao6.isActive()) {
                    ao6.k();
                    Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(null, this), 3, null);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(UpdateUser.Ci ci) {
            c(ci);
        }
    }

    /*
    static {
        String simpleName = HomeProfilePresenter.class.getSimpleName();
        Wg6.b(simpleName, "HomeProfilePresenter::class.java.simpleName");
        G = simpleName;
    }
    */

    @DexIgnore
    public HomeProfilePresenter(WeakReference<Ao6> weakReference, PortfolioApp portfolioApp, GetUser getUser, UpdateUser updateUser, DeviceRepository deviceRepository, UserRepository userRepository, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, DeleteLogoutUserUseCase deleteLogoutUserUseCase, ProfileRepository profileRepository, Tt4 tt4, An4 an4) {
        Wg6.c(weakReference, "mWeakRefView");
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(getUser, "mGetUser");
        Wg6.c(updateUser, "mUpdateUser");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(summariesRepository, "mSummariesRepository");
        Wg6.c(sleepSummariesRepository, "mSleepSummariesRepository");
        Wg6.c(deleteLogoutUserUseCase, "mDeleteLogoutUserUseCase");
        Wg6.c(profileRepository, "socialProfileRepository");
        Wg6.c(tt4, "challengeRepository");
        Wg6.c(an4, "sharedPreferencesManager");
        this.u = weakReference;
        this.v = portfolioApp;
        this.w = getUser;
        this.x = updateUser;
        this.y = deviceRepository;
        this.z = userRepository;
        this.A = summariesRepository;
        this.B = sleepSummariesRepository;
        this.C = deleteLogoutUserUseCase;
        this.D = profileRepository;
        this.E = tt4;
        this.F = an4;
    }

    @DexIgnore
    public final void a0(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = G;
        local.d(str2, "checkToHideWorkoutSettings, serial = " + str);
        boolean w2 = DeviceHelper.o.w(FossilDeviceSerialPatternUtil.getDeviceBySerial(str));
        Ao6 ao6 = this.u.get();
        if (ao6 != null) {
            ao6.Q2(!w2);
        }
    }

    @DexIgnore
    public final ArrayList<Bi> b0() {
        return this.f;
    }

    @DexIgnore
    public final MFUser c0() {
        return this.h;
    }

    @DexIgnore
    public final WeakReference<Ao6> d0() {
        return this.u;
    }

    @DexIgnore
    public final void e0() {
        this.p.removeCallbacksAndMessages(null);
        this.p.postDelayed(this.s, 60000);
    }

    @DexIgnore
    public final void f0() {
        this.w.e(null, new Ei(this));
    }

    @DexIgnore
    public void g0(Intent intent) {
        Wg6.c(intent, "intent");
        Ao6 ao6 = this.u.get();
        if (ao6 != null) {
            Wg6.b(ao6, "it");
            if (!ao6.isActive()) {
                return;
            }
        }
        String stringExtra = intent.getStringExtra("SERIAL");
        if (!TextUtils.isEmpty(stringExtra) && Vt7.j(stringExtra, PortfolioApp.get.instance().J(), true)) {
            Ao6 ao62 = this.u.get();
            if (ao62 != null) {
                ao62.z2();
            }
            e0();
        }
    }

    @DexIgnore
    public final void h0(MFUser mFUser) {
        this.h = mFUser;
    }

    @DexIgnore
    public void i0() {
        Ao6 ao6 = this.u.get();
        if (ao6 != null) {
            ao6.M5(this);
        }
    }

    @DexIgnore
    public final void j0(ActivityStatistic.ActivityDailyBest activityDailyBest, long j2) {
        if (j2 > (activityDailyBest != null ? (long) activityDailyBest.getValue() : 0)) {
            Yn6 yn6 = new Yn6(new Date(), j2);
            Ao6 ao6 = this.u.get();
            if (ao6 != null) {
                ao6.J4(yn6);
            }
        }
    }

    @DexIgnore
    public final void k0(MFUser mFUser) {
        if (mFUser != null) {
            Ao6 ao6 = this.u.get();
            if (ao6 != null) {
                ao6.m();
            }
            this.x.e(new UpdateUser.Ai(mFUser), new Li(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        PortfolioApp portfolioApp = this.v;
        Gi gi = this.t;
        portfolioApp.registerReceiver(gi, new IntentFilter(this.v.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        f0();
        p();
        Rm6 unused = Gu7.d(k(), null, null, new Ki(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        Ao6 ao6 = this.u.get();
        if (ao6 != null) {
            LiveData<H47<ActivitySummary>> liveData = this.j;
            if (ao6 != null) {
                liveData.n((HomeProfileFragment) ao6);
                LifecycleOwner lifecycleOwner = (LifecycleOwner) ao6;
                this.l.n(lifecycleOwner);
                this.i.n(lifecycleOwner);
                this.k.n(lifecycleOwner);
            } else {
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
            }
        }
        this.p.removeCallbacksAndMessages(null);
        try {
            this.v.unregisterReceiver(this.t);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = G;
            local.d(str, "stop with " + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Zn6
    public void n(boolean z2) {
        Rm6 unused = Gu7.d(k(), null, null, new Ci(this, z2, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Zn6
    public FossilDeviceSerialPatternUtil.DEVICE o() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        Wg6.b(device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    @Override // com.fossil.Zn6
    public void p() {
        Rm6 unused = Gu7.d(k(), null, null, new Di(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Zn6
    public void q() {
        Ao6 ao6 = this.u.get();
        if (ao6 != null) {
            ao6.m();
            DeleteLogoutUserUseCase deleteLogoutUserUseCase = this.C;
            if (ao6 != null) {
                FragmentActivity activity = ((HomeProfileFragment) ao6).getActivity();
                if (activity != null) {
                    deleteLogoutUserUseCase.e(new DeleteLogoutUserUseCase.Bi(1, new WeakReference(activity)), new Fi(this));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type android.app.Activity");
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
        }
    }

    @DexIgnore
    @Override // com.fossil.Zn6
    public void r(Intent intent) {
        Uri g2 = Vk5.g(intent, PortfolioApp.get.instance());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = G;
        StringBuilder sb = new StringBuilder();
        sb.append("Inside .onActivityResult imageUri=");
        if (g2 != null) {
            sb.append(g2);
            local.d(str, sb.toString());
            if (PortfolioApp.get.instance().w0(intent, g2)) {
                Rm6 unused = Gu7.d(k(), Bw7.b(), null, new Ii(this, g2, null), 2, null);
                return;
            }
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zn6
    public void s() {
        Rm6 unused = Gu7.d(k(), null, null, new Ji(this, null), 3, null);
    }
}
