package com.portfolio.platform.uirenew.home.dashboard;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.H47;
import com.fossil.Hs0;
import com.fossil.Kd6;
import com.fossil.Ko7;
import com.fossil.Ld6;
import com.fossil.Ls0;
import com.fossil.Or0;
import com.fossil.Ss0;
import com.fossil.Tt4;
import com.fossil.Uh5;
import com.fossil.Vt7;
import com.fossil.Xh5;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.HomeDashboardFragment;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeDashboardPresenter extends Kd6 {
    @DexIgnore
    public /* final */ Cj4 A;
    @DexIgnore
    public /* final */ SummariesRepository B;
    @DexIgnore
    public /* final */ GoalTrackingRepository C;
    @DexIgnore
    public /* final */ SleepSummariesRepository D;
    @DexIgnore
    public /* final */ An4 E;
    @DexIgnore
    public /* final */ HeartRateSampleRepository F;
    @DexIgnore
    public /* final */ Tt4 G;
    @DexIgnore
    public Date e;
    @DexIgnore
    public int f;
    @DexIgnore
    public String g;
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE h;
    @DexIgnore
    public MFSleepDay i;
    @DexIgnore
    public ActivitySummary j;
    @DexIgnore
    public GoalTrackingSummary k;
    @DexIgnore
    public Integer l;
    @DexIgnore
    public List<HeartRateSample> m;
    @DexIgnore
    public HashMap<Integer, Boolean> n; // = new HashMap<>();
    @DexIgnore
    public boolean o;
    @DexIgnore
    public volatile boolean p;
    @DexIgnore
    public /* final */ Bi q; // = new Bi();
    @DexIgnore
    public /* final */ MutableLiveData<Date> r;
    @DexIgnore
    public /* final */ LiveData<H47<ActivitySummary>> s;
    @DexIgnore
    public /* final */ LiveData<H47<MFSleepDay>> t;
    @DexIgnore
    public /* final */ LiveData<H47<GoalTrackingSummary>> u;
    @DexIgnore
    public /* final */ LiveData<H47<Integer>> v;
    @DexIgnore
    public LiveData<H47<List<HeartRateSample>>> w;
    @DexIgnore
    public /* final */ Ld6 x;
    @DexIgnore
    public /* final */ PortfolioApp y;
    @DexIgnore
    public /* final */ DeviceRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1", f = "HomeDashboardPresenter.kt", l = {125, 126, 127, 128}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$activityStatistic$1", f = "HomeDashboardPresenter.kt", l = {125}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super ActivityStatistic>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super ActivityStatistic> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    SummariesRepository summariesRepository = this.this$0.this$0.B;
                    this.L$0 = il6;
                    this.label = 1;
                    Object activityStatisticAwait = summariesRepository.getActivityStatisticAwait(this);
                    return activityStatisticAwait == d ? d : activityStatisticAwait;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$deviceName$1", f = "HomeDashboardPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super String>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super String> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.z.getDeviceNameBySerial(this.this$0.this$0.g);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$sleepStatistic$1", f = "HomeDashboardPresenter.kt", l = {126}, m = "invokeSuspend")
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super SleepStatistic>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super SleepStatistic> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    SleepSummariesRepository sleepSummariesRepository = this.this$0.this$0.D;
                    this.L$0 = il6;
                    this.label = 1;
                    Object sleepStatisticAwait = sleepSummariesRepository.getSleepStatisticAwait(this);
                    return sleepStatisticAwait == d ? d : sleepStatisticAwait;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$totalDevices$1", f = "HomeDashboardPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Dii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Device>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Dii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Dii dii = new Dii(this.this$0, xe6);
                dii.p$ = (Il6) obj;
                throw null;
                //return dii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends Device>> xe6) {
                throw null;
                //return ((Dii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.z.getAllDevice();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(HomeDashboardPresenter homeDashboardPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDashboardPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:45:0x0181  */
        /* JADX WARNING: Removed duplicated region for block: B:49:0x01ac  */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x01d1  */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x01f1  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x01f5  */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x0244  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 587
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, ConnectionStateChange.GATT_OFF.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mConnectionStateChangeReceiver: serial = " + stringExtra + ", state = " + intExtra);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mGoalSettingTargetLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {103, 103}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends Integer>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends Integer>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 2
                    r2 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r6.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r2) goto L_0x0020
                    if (r0 != r5) goto L_0x0018
                    java.lang.Object r0 = r6.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r7)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r6.L$1
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r6.L$0
                    com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                    com.fossil.El7.b(r7)
                    r3 = r0
                    r2 = r7
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r6.L$0 = r1
                    r6.label = r5
                    java.lang.Object r0 = r3.a(r0, r6)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.El7.b(r7)
                    com.fossil.Hs0 r0 = r6.p$
                    com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$Ci r1 = r6.this$0
                    com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter r1 = r1.a
                    com.portfolio.platform.data.source.GoalTrackingRepository r1 = com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter.z(r1)
                    r6.L$0 = r0
                    r6.L$1 = r0
                    r6.label = r2
                    java.lang.Object r2 = r1.getLastGoalSettingLiveData(r6)
                    if (r2 != r4) goto L_0x0057
                    r0 = r4
                    goto L_0x0017
                L_0x0057:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter.Ci.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Ci(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final LiveData<H47<Integer>> a(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mGoalSettingTargetLiveData on date changed " + date);
            return Or0.c(null, 0, new Aii(this, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mGoalTrackingSummaryLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {97, 97}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends GoalTrackingSummary>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Date date, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$it, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends GoalTrackingSummary>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r8)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                    com.fossil.El7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.El7.b(r8)
                    com.fossil.Hs0 r0 = r7.p$
                    com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$Di r1 = r7.this$0
                    com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter r1 = r1.a
                    com.portfolio.platform.data.source.GoalTrackingRepository r1 = com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter.z(r1)
                    java.util.Date r2 = r7.$it
                    java.lang.String r3 = "it"
                    com.mapped.Wg6.b(r2, r3)
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getSummary(r2, r7)
                    if (r2 != r4) goto L_0x005e
                    r0 = r4
                    goto L_0x0017
                L_0x005e:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter.Di.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Di(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final LiveData<H47<GoalTrackingSummary>> a(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mGoalTrackingSummaryLiveData on date changed " + date);
            return Or0.c(null, 0, new Aii(this, date, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mHeartRateSampleLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {109, 109}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<HeartRateSample>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Date date, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$it, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<HeartRateSample>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r6) goto L_0x0020
                    if (r0 != r7) goto L_0x0018
                    java.lang.Object r0 = r8.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r9)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r8.L$1
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                    com.fossil.El7.b(r9)
                    r2 = r9
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r1
                    r8.label = r7
                    java.lang.Object r0 = r3.a(r0, r8)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.El7.b(r9)
                    com.fossil.Hs0 r0 = r8.p$
                    com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$Ei r1 = r8.this$0
                    com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter r1 = r1.a
                    com.portfolio.platform.data.source.HeartRateSampleRepository r1 = com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter.B(r1)
                    java.util.Date r2 = r8.$it
                    java.lang.String r3 = "it"
                    com.mapped.Wg6.b(r2, r3)
                    java.util.Date r3 = r8.$it
                    java.lang.String r5 = "it"
                    com.mapped.Wg6.b(r3, r5)
                    r8.L$0 = r0
                    r8.L$1 = r0
                    r8.label = r6
                    java.lang.Object r2 = r1.getHeartRateSamples(r2, r3, r6, r8)
                    if (r2 != r4) goto L_0x0065
                    r0 = r4
                    goto L_0x0017
                L_0x0065:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter.Ei.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Ei(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<HeartRateSample>>> a(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mHeartRateSampleLiveData on date changed " + date);
            return Or0.c(null, 0, new Aii(this, date, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mSleepSummaryLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {91, 91}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends MFSleepDay>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Date date, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$it, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends MFSleepDay>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r8)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                    com.fossil.El7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.El7.b(r8)
                    com.fossil.Hs0 r0 = r7.p$
                    com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$Fi r1 = r7.this$0
                    com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter r1 = r1.a
                    com.portfolio.platform.data.source.SleepSummariesRepository r1 = com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter.G(r1)
                    java.util.Date r2 = r7.$it
                    java.lang.String r3 = "it"
                    com.mapped.Wg6.b(r2, r3)
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getSleepSummary(r2, r7)
                    if (r2 != r4) goto L_0x005e
                    r0 = r4
                    goto L_0x0017
                L_0x005e:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter.Fi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Fi(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final LiveData<H47<MFSleepDay>> a(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mSleepSummaryLiveData on date changed " + date);
            return Or0.c(null, 0, new Aii(this, date, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mSummaryLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {85, 85}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends ActivitySummary>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Gi gi, Date date, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = gi;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$it, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends ActivitySummary>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r8)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                    com.fossil.El7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.El7.b(r8)
                    com.fossil.Hs0 r0 = r7.p$
                    com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$Gi r1 = r7.this$0
                    com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter r1 = r1.a
                    com.portfolio.platform.data.source.SummariesRepository r1 = com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter.H(r1)
                    java.util.Date r2 = r7.$it
                    java.lang.String r3 = "it"
                    com.mapped.Wg6.b(r2, r3)
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getSummary(r2, r7)
                    if (r2 != r4) goto L_0x005e
                    r0 = r4
                    goto L_0x0017
                L_0x005e:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter.Gi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Gi(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final LiveData<H47<ActivitySummary>> a(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mSummaryLiveData on date changed " + date);
            return Or0.c(null, 0, new Aii(this, date, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$onRetrieveSyncEvent$1", f = "HomeDashboardPresenter.kt", l = {371}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(HomeDashboardPresenter homeDashboardPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDashboardPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Boolean l0 = this.this$0.E.l0();
                Boolean b = this.this$0.E.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("HomeDashboardPresenter", "consider retry sync data after sync successfully - isNeedGetSyncDataForWatch: " + l0);
                if (Wg6.a(l0, Ao7.a(true))) {
                    Wg6.b(b, "isBcOn");
                    if (b.booleanValue()) {
                        HomeDashboardPresenter homeDashboardPresenter = this.this$0;
                        this.L$0 = il6;
                        this.L$1 = l0;
                        this.L$2 = b;
                        this.label = 1;
                        if (homeDashboardPresenter.W(this) == d) {
                            return d;
                        }
                    }
                }
            } else if (i == 1) {
                Boolean bool = (Boolean) this.L$2;
                Boolean bool2 = (Boolean) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$onRetrieveSyncEvent$2", f = "HomeDashboardPresenter.kt", l = {404}, m = "invokeSuspend")
    public static final class Ii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(HomeDashboardPresenter homeDashboardPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDashboardPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ii ii = new Ii(this.this$0, xe6);
            ii.p$ = (Il6) obj;
            throw null;
            //return ii;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ii) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Boolean l0 = this.this$0.E.l0();
                Boolean b = this.this$0.E.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("HomeDashboardPresenter", "consider retry sync data after sync failed - isNeedGetSyncDataForWatch: " + l0);
                if (Wg6.a(l0, Ao7.a(true))) {
                    Wg6.b(b, "isBcOn");
                    if (b.booleanValue() && PortfolioApp.get.instance().L()) {
                        HomeDashboardPresenter homeDashboardPresenter = this.this$0;
                        this.L$0 = il6;
                        this.L$1 = l0;
                        this.L$2 = b;
                        this.label = 1;
                        if (homeDashboardPresenter.W(this) == d) {
                            return d;
                        }
                    }
                }
            } else if (i == 1) {
                Boolean bool = (Boolean) this.L$2;
                Boolean bool2 = (Boolean) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter", f = "HomeDashboardPresenter.kt", l = {425}, m = "retrySetSyncData")
    public static final class Ji extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(HomeDashboardPresenter homeDashboardPresenter, Xe6 xe6) {
            super(xe6);
            this.this$0 = homeDashboardPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.W(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$1", f = "HomeDashboardPresenter.kt", l = {159}, m = "invokeSuspend")
    public static final class Ki extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$1$activeDevice$1", f = "HomeDashboardPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Device>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ki this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ki ki, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ki;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Device> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.z.getDeviceBySerial(PortfolioApp.get.instance().J());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(HomeDashboardPresenter homeDashboardPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDashboardPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ki ki = new Ki(this.this$0, xe6);
            ki.p$ = (Il6) obj;
            throw null;
            //return ki;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ki) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(i2, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Device device = (Device) g;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "showLayoutBattery device " + device);
            if (device != null) {
                if (Wg6.a("release", "debug") || Wg6.a("release", "staging")) {
                    if (FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId()) || device.getBatteryLevel() >= PortfolioApp.get.instance().k0().M() || device.getBatteryLevel() <= 0) {
                        this.this$0.x.x3(false);
                    } else {
                        this.this$0.x.x3(true);
                    }
                } else if (FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId()) || device.getBatteryLevel() >= 25 || device.getBatteryLevel() <= 0) {
                    this.this$0.x.x3(false);
                } else {
                    this.this$0.x.x3(true);
                }
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li<T> implements Ls0<H47<? extends ActivitySummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public Li(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final void a(H47<ActivitySummary> h47) {
            ActivitySummary activitySummary = null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "XXX- summaryChanged -- summary=" + h47 + ", mFirstDBLoad=" + ((Boolean) this.a.n.get(0)));
            if ((h47 != null ? h47.d() : null) != Xh5.DATABASE_LOADING) {
                if (h47 != null) {
                    activitySummary = h47.c();
                }
                if ((!Wg6.a((Boolean) this.a.n.get(0), Boolean.TRUE)) || (!Wg6.a(this.a.j, activitySummary))) {
                    this.a.j = activitySummary;
                    this.a.Z();
                    this.a.n.put(0, Boolean.TRUE);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends ActivitySummary> h47) {
            a(h47);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi<T> implements Ls0<H47<? extends MFSleepDay>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public Mi(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final void a(H47<MFSleepDay> h47) {
            MFSleepDay mFSleepDay = null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mSleepSummaryLiveData -- summary=" + h47 + ", mFirstDBLoad=" + ((Boolean) this.a.n.get(5)));
            if ((h47 != null ? h47.d() : null) != Xh5.DATABASE_LOADING) {
                if (h47 != null) {
                    mFSleepDay = h47.c();
                }
                if ((!Wg6.a((Boolean) this.a.n.get(5), Boolean.TRUE)) || (!Wg6.a(this.a.i, mFSleepDay))) {
                    this.a.i = mFSleepDay;
                    this.a.Z();
                    this.a.n.put(5, Boolean.TRUE);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends MFSleepDay> h47) {
            a(h47);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni<T> implements Ls0<H47<? extends GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public Ni(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final void a(H47<GoalTrackingSummary> h47) {
            GoalTrackingSummary goalTrackingSummary = null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mGoalTrackingSummaryLiveData -- summary=" + h47 + ", mFirstDBLoad=" + ((Boolean) this.a.n.get(4)));
            if ((h47 != null ? h47.d() : null) != Xh5.DATABASE_LOADING) {
                if (h47 != null) {
                    goalTrackingSummary = h47.c();
                }
                if ((!Wg6.a((Boolean) this.a.n.get(4), Boolean.TRUE)) || (!Wg6.a(this.a.k, goalTrackingSummary))) {
                    this.a.k = goalTrackingSummary;
                    this.a.Z();
                    this.a.n.put(4, Boolean.TRUE);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends GoalTrackingSummary> h47) {
            a(h47);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi<T> implements Ls0<H47<? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public Oi(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final void a(H47<Integer> h47) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mGoalSettingTargetLiveData -- resource=" + h47);
            if ((h47 != null ? h47.d() : null) != Xh5.DATABASE_LOADING) {
                Integer c = h47 != null ? h47.c() : null;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("HomeDashboardPresenter", "start - mGoalSettingTargetLiveData -- goal=" + c);
                if (!Wg6.a(this.a.l, c)) {
                    this.a.l = c;
                    this.a.Z();
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends Integer> h47) {
            a(h47);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi<T> implements Ls0<H47<? extends List<HeartRateSample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardPresenter a;

        @DexIgnore
        public Pi(HomeDashboardPresenter homeDashboardPresenter) {
            this.a = homeDashboardPresenter;
        }

        @DexIgnore
        public final void a(H47<? extends List<HeartRateSample>> h47) {
            Xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mHeartRateSampleLiveData -- resource.status=");
            sb.append(a2);
            sb.append(", ");
            sb.append("data.size=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", mFirstDBLoad=");
            sb.append((Boolean) this.a.n.get(3));
            local.d("HomeDashboardPresenter", sb.toString());
            if (a2 == Xh5.DATABASE_LOADING) {
                return;
            }
            if ((!Wg6.a((Boolean) this.a.n.get(3), Boolean.TRUE)) || (!Wg6.a(this.a.m, list))) {
                this.a.m = list;
                this.a.Z();
                this.a.n.put(3, Boolean.TRUE);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<HeartRateSample>> h47) {
            a(h47);
        }
    }

    @DexIgnore
    public HomeDashboardPresenter(Ld6 ld6, PortfolioApp portfolioApp, DeviceRepository deviceRepository, Cj4 cj4, SummariesRepository summariesRepository, GoalTrackingRepository goalTrackingRepository, SleepSummariesRepository sleepSummariesRepository, An4 an4, HeartRateSampleRepository heartRateSampleRepository, Tt4 tt4) {
        Wg6.c(ld6, "mView");
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(cj4, "mDeviceSettingFactory");
        Wg6.c(summariesRepository, "mSummaryRepository");
        Wg6.c(goalTrackingRepository, "mGoalTrackingRepository");
        Wg6.c(sleepSummariesRepository, "mSleepSummaryRepository");
        Wg6.c(an4, "sharedPreferencesManager");
        Wg6.c(heartRateSampleRepository, "mHeartRateSampleRepository");
        Wg6.c(tt4, "challengeRepository");
        this.x = ld6;
        this.y = portfolioApp;
        this.z = deviceRepository;
        this.A = cj4;
        this.B = summariesRepository;
        this.C = goalTrackingRepository;
        this.D = sleepSummariesRepository;
        this.E = an4;
        this.F = heartRateSampleRepository;
        this.G = tt4;
        String J = portfolioApp.J();
        this.g = J;
        this.h = FossilDeviceSerialPatternUtil.getDeviceBySerial(J);
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.r = mutableLiveData;
        LiveData<H47<ActivitySummary>> c = Ss0.c(mutableLiveData, new Gi(this));
        Wg6.b(c, "Transformations.switchMa\u2026mary(it))\n        }\n    }");
        this.s = c;
        LiveData<H47<MFSleepDay>> c2 = Ss0.c(this.r, new Fi(this));
        Wg6.b(c2, "Transformations.switchMa\u2026mary(it))\n        }\n    }");
        this.t = c2;
        LiveData<H47<GoalTrackingSummary>> c3 = Ss0.c(this.r, new Di(this));
        Wg6.b(c3, "Transformations.switchMa\u2026mary(it))\n        }\n    }");
        this.u = c3;
        LiveData<H47<Integer>> c4 = Ss0.c(this.r, new Ci(this));
        Wg6.b(c4, "Transformations.switchMa\u2026veData())\n        }\n    }");
        this.v = c4;
        LiveData<H47<List<HeartRateSample>>> c5 = Ss0.c(this.r, new Ei(this));
        Wg6.b(c5, "Transformations.switchMa\u2026t, true))\n        }\n    }");
        this.w = c5;
    }

    @DexIgnore
    public final Rm6 R() {
        return Gu7.d(k(), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    public final void S() {
        PortfolioApp portfolioApp = this.y;
        if (portfolioApp.A0(portfolioApp.J()) && !this.p) {
            this.p = true;
            this.x.t6();
        }
    }

    @DexIgnore
    public void T(Intent intent) {
        Wg6.c(intent, "intent");
        if (this.x.isActive()) {
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && Vt7.j(stringExtra, this.g, true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), 10);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDashboardPresenter", "mSyncReceiver - syncStatus: " + intExtra + " - mIntendingSync: " + this.p);
                if (intExtra != 0) {
                    if (intExtra == 1) {
                        this.p = false;
                        this.x.Z1(true);
                        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "sync complete, check for low battery");
                        Y();
                        Rm6 unused = Gu7.d(k(), Bw7.b(), null, new Hi(this, null), 2, null);
                    } else if (intExtra == 2 || intExtra == 4) {
                        this.p = false;
                        this.x.Z1(false);
                        if (intExtra2 == 12) {
                            int intExtra3 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                            if (integerArrayListExtra == null) {
                                integerArrayListExtra = new ArrayList<>();
                            }
                            if (intExtra3 != 1101) {
                                if (intExtra3 == 1603) {
                                    this.x.Z1(false);
                                    return;
                                } else if (!(intExtra3 == 1112 || intExtra3 == 1113)) {
                                    this.x.q6();
                                    Rm6 unused2 = Gu7.d(k(), Bw7.b(), null, new Ii(this, null), 2, null);
                                    return;
                                }
                            }
                            List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(integerArrayListExtra);
                            Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
                            Ld6 ld6 = this.x;
                            Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                            if (array != null) {
                                Uh5[] uh5Arr = (Uh5[]) array;
                                ld6.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                                return;
                            }
                            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                    } else if (intExtra == 5) {
                        this.x.t1();
                    } else if (intExtra == 6) {
                        this.p = false;
                        this.x.Z1(false);
                        this.x.U0();
                    }
                } else if (!this.p) {
                    this.p = true;
                    this.x.t6();
                }
            }
        }
    }

    @DexIgnore
    public void U(boolean z2) {
        this.x.A(z2);
    }

    @DexIgnore
    public void V() {
        this.x.Q5();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object W(com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r6 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter.Ji
            if (r0 == 0) goto L_0x0042
            r0 = r7
            com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$Ji r0 = (com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter.Ji) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0042
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0050
            if (r3 != r4) goto L_0x0048
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter r0 = (com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter) r0
            com.fossil.El7.b(r1)
            r6 = r0
        L_0x0027:
            r0 = r1
            com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
            java.lang.Object r0 = r0.c()
            com.fossil.Pt4 r0 = (com.fossil.Pt4) r0
            if (r0 == 0) goto L_0x0061
            com.portfolio.platform.PortfolioApp$inner r1 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r1 = r1.instance()
            java.lang.String r0 = r0.a()
            r1.m1(r0)
        L_0x003f:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0041:
            return r0
        L_0x0042:
            com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$Ji r0 = new com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$Ji
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x0048:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0050:
            com.fossil.El7.b(r1)
            com.fossil.Tt4 r1 = r6.G
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r1 = r1.v(r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0041
        L_0x0061:
            com.mapped.An4 r0 = r6.E
            java.lang.Long r0 = r0.d()
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "retrySetSyncData - endTimeOfUnsetChallenge: "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r3 = " - exact: "
            r2.append(r3)
            com.fossil.Xy4 r3 = com.fossil.Xy4.a
            long r4 = r3.b()
            r2.append(r4)
            java.lang.String r3 = "HomeDashboardPresenter"
            java.lang.String r2 = r2.toString()
            r1.e(r3, r2)
            long r0 = r0.longValue()
            com.fossil.Xy4 r2 = com.fossil.Xy4.a
            long r2 = r2.b()
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x00a5
            com.fossil.Cl5 r0 = com.fossil.Cl5.c
            r0.j()
            goto L_0x003f
        L_0x00a5:
            com.mapped.An4 r0 = r6.E
            r2 = 0
            java.lang.Long r1 = com.fossil.Ao7.f(r2)
            r0.e(r1)
            com.mapped.An4 r0 = r6.E
            r1 = 0
            java.lang.Boolean r1 = com.fossil.Ao7.a(r1)
            r0.m0(r1)
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter.W(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public void X() {
        this.x.M5(this);
    }

    @DexIgnore
    public final void Y() {
        Rm6 unused = Gu7.d(k(), null, null, new Ki(this, null), 3, null);
    }

    @DexIgnore
    public final void Z() {
        Integer num;
        HeartRateSample heartRateSample;
        Resting resting;
        boolean z2;
        boolean z3 = true;
        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "updateHomeInfo");
        this.x.g5(this.j, this.i, this.k);
        List<HeartRateSample> list = this.m;
        if (list != null) {
            ListIterator<HeartRateSample> listIterator = list.listIterator(list.size());
            while (true) {
                if (!listIterator.hasPrevious()) {
                    heartRateSample = null;
                    break;
                }
                HeartRateSample previous = listIterator.previous();
                if (previous.getResting() != null) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    heartRateSample = previous;
                    break;
                }
            }
            HeartRateSample heartRateSample2 = heartRateSample;
            num = Integer.valueOf((heartRateSample2 == null || (resting = heartRateSample2.getResting()) == null) ? 0 : resting.getValue());
        } else {
            num = null;
        }
        if (!(this.g.length() == 0) || !this.o) {
            z3 = false;
        }
        this.x.J1(this.j, this.i, this.k, this.l, num, z3);
        Ld6 ld6 = this.x;
        Date date = this.e;
        if (date != null) {
            ld6.r2(date);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void a0(int i2) {
        this.x.o0(i2);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardPresenter", "start - this=" + hashCode());
        Date date = new Date();
        Date date2 = this.e;
        if (date2 == null || !TimeUtils.m0(date, date2)) {
            this.e = date;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("HomeDashboardPresenter", "date change " + this.e);
            this.r.l(this.e);
        }
        PortfolioApp portfolioApp = this.y;
        Bi bi = this.q;
        portfolioApp.registerReceiver(bi, new IntentFilter(this.y.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        R();
        LiveData<H47<ActivitySummary>> liveData = this.s;
        Ld6 ld6 = this.x;
        if (ld6 != null) {
            liveData.h(((HomeDashboardFragment) ld6).getViewLifecycleOwner(), new Li(this));
            this.t.h(((HomeDashboardFragment) this.x).getViewLifecycleOwner(), new Mi(this));
            this.u.h(((HomeDashboardFragment) this.x).getViewLifecycleOwner(), new Ni(this));
            this.v.h(((HomeDashboardFragment) this.x).getViewLifecycleOwner(), new Oi(this));
            this.w.h(((HomeDashboardFragment) this.x).getViewLifecycleOwner(), new Pi(this));
            S();
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDashboardFragment");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "stop");
        try {
            this.y.unregisterReceiver(this.q);
            Ld6 ld6 = this.x;
            if (ld6 != null) {
                LifecycleOwner viewLifecycleOwner = ((HomeDashboardFragment) ld6).getViewLifecycleOwner();
                this.s.n(viewLifecycleOwner);
                this.t.n(viewLifecycleOwner);
                this.u.n(viewLifecycleOwner);
                this.v.n(viewLifecycleOwner);
                this.w.n(viewLifecycleOwner);
                PortfolioApp.get.instance().K().n(viewLifecycleOwner);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDashboardFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("HomeDashboardPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Kd6
    public FossilDeviceSerialPatternUtil.DEVICE n() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.h;
        Wg6.b(device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    @Override // com.fossil.Kd6
    public int o() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.Kd6
    public void p(String str, boolean z2) {
        Wg6.c(str, "activeId");
        this.y.x(str, z2);
    }

    @DexIgnore
    @Override // com.fossil.Kd6
    public void q(int i2) {
        this.f = i2;
    }

    @DexIgnore
    @Override // com.fossil.Kd6
    public void r() {
        String J = this.y.J();
        int R = this.y.R(J);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardPresenter", "Inside .onRefresh currentDeviceSession=" + R);
        if (TextUtils.isEmpty(J) || R == CommunicateMode.OTA.getValue()) {
            this.x.Z1(false);
            return;
        }
        this.p = true;
        this.x.t6();
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.y.J(), "HomeDashboardPresenter", "[Sync Start] PULL TO SYNC");
        this.y.S1(this.A, false, 12);
    }

    @DexIgnore
    @Override // com.fossil.Kd6
    public void s(int i2) {
        if (i2 == this.f) {
            this.x.G0();
        }
    }

    @DexIgnore
    @Override // com.fossil.Kd6
    public void t(boolean z2) {
        this.p = z2;
    }
}
