package com.portfolio.platform.uirenew.home.dashboard.activetime.overview;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Aq0;
import com.fossil.De6;
import com.fossil.G37;
import com.fossil.G67;
import com.fossil.Mo0;
import com.fossil.Mv0;
import com.mapped.ActiveTimeOverviewDayFragment;
import com.mapped.ActiveTimeOverviewMonthFragment;
import com.mapped.ActiveTimeOverviewWeekFragment;
import com.mapped.Iface;
import com.mapped.R54;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActiveTimeOverviewFragment extends BaseFragment {
    @DexIgnore
    public G37<R54> g;
    @DexIgnore
    public ActiveTimeOverviewDayPresenter h;
    @DexIgnore
    public ActiveTimeOverviewWeekPresenter i;
    @DexIgnore
    public ActiveTimeOverviewMonthPresenter j;
    @DexIgnore
    public ActiveTimeOverviewDayFragment k;
    @DexIgnore
    public ActiveTimeOverviewWeekFragment l;
    @DexIgnore
    public ActiveTimeOverviewMonthFragment m;
    @DexIgnore
    public int s; // = 7;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment b;

        @DexIgnore
        public a(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.b = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.b;
            G37 g37 = activeTimeOverviewFragment.g;
            activeTimeOverviewFragment.O6(7, g37 != null ? (R54) g37.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment b;

        @DexIgnore
        public b(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.b = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.b;
            G37 g37 = activeTimeOverviewFragment.g;
            activeTimeOverviewFragment.O6(4, g37 != null ? (R54) g37.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment b;

        @DexIgnore
        public c(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.b = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.b;
            G37 g37 = activeTimeOverviewFragment.g;
            activeTimeOverviewFragment.O6(2, g37 != null ? (R54) g37.a() : null);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "ActiveTimeOverviewFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void M6(R54 r54) {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "initUI");
        this.k = (ActiveTimeOverviewDayFragment) getChildFragmentManager().Z("ActiveTimeOverviewDayFragment");
        this.l = (ActiveTimeOverviewWeekFragment) getChildFragmentManager().Z("ActiveTimeOverviewWeekFragment");
        this.m = (ActiveTimeOverviewMonthFragment) getChildFragmentManager().Z("ActiveTimeOverviewMonthFragment");
        if (this.k == null) {
            this.k = new ActiveTimeOverviewDayFragment();
        }
        if (this.l == null) {
            this.l = new ActiveTimeOverviewWeekFragment();
        }
        if (this.m == null) {
            this.m = new ActiveTimeOverviewMonthFragment();
        }
        ArrayList arrayList = new ArrayList();
        ActiveTimeOverviewDayFragment activeTimeOverviewDayFragment = this.k;
        if (activeTimeOverviewDayFragment != null) {
            arrayList.add(activeTimeOverviewDayFragment);
            ActiveTimeOverviewWeekFragment activeTimeOverviewWeekFragment = this.l;
            if (activeTimeOverviewWeekFragment != null) {
                arrayList.add(activeTimeOverviewWeekFragment);
                ActiveTimeOverviewMonthFragment activeTimeOverviewMonthFragment = this.m;
                if (activeTimeOverviewMonthFragment != null) {
                    arrayList.add(activeTimeOverviewMonthFragment);
                    RecyclerView recyclerView = r54.w;
                    Wg6.b(recyclerView, "it");
                    recyclerView.setAdapter(new G67(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new ActiveTimeOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new Mv0().b(recyclerView);
                    O6(this.s, r54);
                    Iface iface = PortfolioApp.get.instance().getIface();
                    ActiveTimeOverviewDayFragment activeTimeOverviewDayFragment2 = this.k;
                    if (activeTimeOverviewDayFragment2 != null) {
                        ActiveTimeOverviewWeekFragment activeTimeOverviewWeekFragment2 = this.l;
                        if (activeTimeOverviewWeekFragment2 != null) {
                            ActiveTimeOverviewMonthFragment activeTimeOverviewMonthFragment2 = this.m;
                            if (activeTimeOverviewMonthFragment2 != null) {
                                iface.E(new De6(activeTimeOverviewDayFragment2, activeTimeOverviewWeekFragment2, activeTimeOverviewMonthFragment2)).a(this);
                                r54.u.setOnClickListener(new a(this));
                                r54.r.setOnClickListener(new b(this));
                                r54.s.setOnClickListener(new c(this));
                                return;
                            }
                            Wg6.i();
                            throw null;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void N6() {
        G37<R54> g37;
        R54 a2;
        G37<R54> g372;
        R54 a3;
        ConstraintLayout constraintLayout;
        G37<R54> g373;
        R54 a4;
        FlexibleTextView flexibleTextView;
        G37<R54> g374;
        R54 a5;
        DeviceHelper.Ai ai = DeviceHelper.o;
        ActiveTimeOverviewDayPresenter activeTimeOverviewDayPresenter = this.h;
        if (activeTimeOverviewDayPresenter != null) {
            String d = ai.w(activeTimeOverviewDayPresenter.n()) ? ThemeManager.l.a().d("dianaActiveMinutesTab") : ThemeManager.l.a().d("hybridActiveMinutesTab");
            DeviceHelper.Ai ai2 = DeviceHelper.o;
            ActiveTimeOverviewDayPresenter activeTimeOverviewDayPresenter2 = this.h;
            if (activeTimeOverviewDayPresenter2 != null) {
                String d2 = ai2.w(activeTimeOverviewDayPresenter2.n()) ? ThemeManager.l.a().d("onDianaActiveMinutesTab") : ThemeManager.l.a().d("onHybridActiveMinutesTab");
                if (!(d == null || (g374 = this.g) == null || (a5 = g374.a()) == null)) {
                    a5.x.setBackgroundColor(Color.parseColor(d));
                    a5.y.setBackgroundColor(Color.parseColor(d));
                }
                if (!(d2 == null || (g373 = this.g) == null || (a4 = g373.a()) == null || (flexibleTextView = a4.t) == null)) {
                    flexibleTextView.setTextColor(Color.parseColor(d2));
                }
                DeviceHelper.Ai ai3 = DeviceHelper.o;
                ActiveTimeOverviewDayPresenter activeTimeOverviewDayPresenter3 = this.h;
                if (activeTimeOverviewDayPresenter3 != null) {
                    this.t = ai3.w(activeTimeOverviewDayPresenter3.n()) ? ThemeManager.l.a().d("onDianaInactiveTab") : ThemeManager.l.a().d("onHybridInactiveTab");
                    String d3 = ThemeManager.l.a().d("nonBrandSurface");
                    this.u = ThemeManager.l.a().d("primaryText");
                    if (!(d3 == null || (g372 = this.g) == null || (a3 = g372.a()) == null || (constraintLayout = a3.q) == null)) {
                        constraintLayout.setBackgroundColor(Color.parseColor(d3));
                    }
                    if (!TextUtils.isEmpty(this.t) && !TextUtils.isEmpty(this.u) && (g37 = this.g) != null && (a2 = g37.a()) != null) {
                        FlexibleTextView flexibleTextView2 = a2.u;
                        Wg6.b(flexibleTextView2, "it.ftvToday");
                        if (flexibleTextView2.isSelected()) {
                            a2.u.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.u.setTextColor(Color.parseColor(this.t));
                        }
                        FlexibleTextView flexibleTextView3 = a2.r;
                        Wg6.b(flexibleTextView3, "it.ftv7Days");
                        if (flexibleTextView3.isSelected()) {
                            a2.r.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.r.setTextColor(Color.parseColor(this.t));
                        }
                        FlexibleTextView flexibleTextView4 = a2.s;
                        Wg6.b(flexibleTextView4, "it.ftvMonth");
                        if (flexibleTextView4.isSelected()) {
                            a2.s.setTextColor(Color.parseColor(this.u));
                        } else {
                            a2.s.setTextColor(Color.parseColor(this.t));
                        }
                    }
                } else {
                    Wg6.n("mActiveTimeOverviewDayPresenter");
                    throw null;
                }
            } else {
                Wg6.n("mActiveTimeOverviewDayPresenter");
                throw null;
            }
        } else {
            Wg6.n("mActiveTimeOverviewDayPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void O6(int i2, R54 r54) {
        R54 a2;
        RecyclerView recyclerView;
        G37<R54> g37;
        R54 a3;
        R54 a4;
        RecyclerView recyclerView2;
        R54 a5;
        RecyclerView recyclerView3;
        R54 a6;
        RecyclerView recyclerView4;
        if (r54 != null) {
            FlexibleTextView flexibleTextView = r54.u;
            Wg6.b(flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = r54.r;
            Wg6.b(flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = r54.s;
            Wg6.b(flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = r54.u;
            Wg6.b(flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = r54.r;
            Wg6.b(flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = r54.s;
            Wg6.b(flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i2 == 2) {
                FlexibleTextView flexibleTextView7 = r54.s;
                Wg6.b(flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = r54.s;
                Wg6.b(flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = r54.r;
                Wg6.b(flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                G37<R54> g372 = this.g;
                if (!(g372 == null || (a2 = g372.a()) == null || (recyclerView = a2.w) == null)) {
                    recyclerView.scrollToPosition(2);
                }
            } else if (i2 == 4) {
                FlexibleTextView flexibleTextView10 = r54.r;
                Wg6.b(flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = r54.r;
                Wg6.b(flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = r54.r;
                Wg6.b(flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                G37<R54> g373 = this.g;
                if (!(g373 == null || (a4 = g373.a()) == null || (recyclerView2 = a4.w) == null)) {
                    recyclerView2.scrollToPosition(1);
                }
            } else if (i2 != 7) {
                FlexibleTextView flexibleTextView13 = r54.u;
                Wg6.b(flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = r54.u;
                Wg6.b(flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = r54.r;
                Wg6.b(flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                G37<R54> g374 = this.g;
                if (!(g374 == null || (a6 = g374.a()) == null || (recyclerView4 = a6.w) == null)) {
                    recyclerView4.scrollToPosition(0);
                }
            } else {
                FlexibleTextView flexibleTextView16 = r54.u;
                Wg6.b(flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = r54.u;
                Wg6.b(flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = r54.r;
                Wg6.b(flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                G37<R54> g375 = this.g;
                if (!(g375 == null || (a5 = g375.a()) == null || (recyclerView3 = a5.w) == null)) {
                    recyclerView3.scrollToPosition(0);
                }
            }
            if (!TextUtils.isEmpty(this.t) && !TextUtils.isEmpty(this.u) && (g37 = this.g) != null && (a3 = g37.a()) != null) {
                FlexibleTextView flexibleTextView19 = a3.u;
                Wg6.b(flexibleTextView19, "it.ftvToday");
                if (flexibleTextView19.isSelected()) {
                    a3.u.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.u.setTextColor(Color.parseColor(this.t));
                }
                FlexibleTextView flexibleTextView20 = a3.r;
                Wg6.b(flexibleTextView20, "it.ftv7Days");
                if (flexibleTextView20.isSelected()) {
                    a3.r.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.r.setTextColor(Color.parseColor(this.t));
                }
                FlexibleTextView flexibleTextView21 = a3.s;
                Wg6.b(flexibleTextView21, "it.ftvMonth");
                if (flexibleTextView21.isSelected()) {
                    a3.s.setTextColor(Color.parseColor(this.u));
                } else {
                    a3.s.setTextColor(Color.parseColor(this.t));
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        R54 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "onCreateView");
        R54 r54 = (R54) Aq0.f(layoutInflater, 2131558492, viewGroup, false, A6());
        Mo0.y0(r54.w, false);
        if (bundle != null) {
            this.s = bundle.getInt("CURRENT_TAB", 7);
        }
        Wg6.b(r54, "binding");
        M6(r54);
        this.g = new G37<>(this, r54);
        N6();
        G37<R54> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.c(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.s);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
