package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import android.location.Location;
import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.model.PlaceFields;
import com.fossil.Ao7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Nt3;
import com.fossil.Ts0;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc3;
import com.mapped.Lf6;
import com.mapped.Mc3;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsDetailViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public PlacesClient a;
    @DexIgnore
    public AddressWrapper b;
    @DexIgnore
    public AddressWrapper c;
    @DexIgnore
    public List<String> d;
    @DexIgnore
    public MutableLiveData<Ai> e; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public AddressWrapper.AddressType a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Boolean d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public Boolean f;
        @DexIgnore
        public PlacesClient g;
        @DexIgnore
        public Boolean h;
        @DexIgnore
        public Boolean i;

        @DexIgnore
        public Ai(AddressWrapper.AddressType addressType, String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4, Boolean bool5) {
            this.a = addressType;
            this.b = str;
            this.c = str2;
            this.d = bool;
            this.e = bool2;
            this.f = bool3;
            this.g = placesClient;
            this.h = bool4;
            this.i = bool5;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final Boolean b() {
            return this.d;
        }

        @DexIgnore
        public final String c() {
            return this.b;
        }

        @DexIgnore
        public final PlacesClient d() {
            return this.g;
        }

        @DexIgnore
        public final AddressWrapper.AddressType e() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!Wg6.a(this.a, ai.a) || !Wg6.a(this.b, ai.b) || !Wg6.a(this.c, ai.c) || !Wg6.a(this.d, ai.d) || !Wg6.a(this.e, ai.e) || !Wg6.a(this.f, ai.f) || !Wg6.a(this.g, ai.g) || !Wg6.a(this.h, ai.h) || !Wg6.a(this.i, ai.i)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final Boolean f() {
            return this.h;
        }

        @DexIgnore
        public final Boolean g() {
            return this.i;
        }

        @DexIgnore
        public final Boolean h() {
            return this.e;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = 0;
            AddressWrapper.AddressType addressType = this.a;
            int hashCode = addressType != null ? addressType.hashCode() : 0;
            String str = this.b;
            int hashCode2 = str != null ? str.hashCode() : 0;
            String str2 = this.c;
            int hashCode3 = str2 != null ? str2.hashCode() : 0;
            Boolean bool = this.d;
            int hashCode4 = bool != null ? bool.hashCode() : 0;
            Boolean bool2 = this.e;
            int hashCode5 = bool2 != null ? bool2.hashCode() : 0;
            Boolean bool3 = this.f;
            int hashCode6 = bool3 != null ? bool3.hashCode() : 0;
            PlacesClient placesClient = this.g;
            int hashCode7 = placesClient != null ? placesClient.hashCode() : 0;
            Boolean bool4 = this.h;
            int hashCode8 = bool4 != null ? bool4.hashCode() : 0;
            Boolean bool5 = this.i;
            if (bool5 != null) {
                i2 = bool5.hashCode();
            }
            return (((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + i2;
        }

        @DexIgnore
        public final Boolean i() {
            return this.f;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(type=" + this.a + ", name=" + this.b + ", address=" + this.c + ", avoidTolls=" + this.d + ", isNameEditable=" + this.e + ", isValid=" + this.f + ", placesClient=" + this.g + ", isLoading=" + this.h + ", isLocationUnavailable=" + this.i + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<TResult> implements Mc3<FetchPlaceResponse> {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailViewModel a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public Bi(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel, String str) {
            this.a = commuteTimeSettingsDetailViewModel;
            this.b = str;
        }

        @DexIgnore
        public final void a(FetchPlaceResponse fetchPlaceResponse) {
            AddressWrapper addressWrapper = this.a.b;
            if (addressWrapper != null) {
                Wg6.b(fetchPlaceResponse, "response");
                Place place = fetchPlaceResponse.getPlace();
                Wg6.b(place, "response.place");
                LatLng latLng = place.getLatLng();
                if (latLng != null) {
                    addressWrapper.setLat(latLng.b);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            AddressWrapper addressWrapper2 = this.a.b;
            if (addressWrapper2 != null) {
                Wg6.b(fetchPlaceResponse, "response");
                Place place2 = fetchPlaceResponse.getPlace();
                Wg6.b(place2, "response.place");
                LatLng latLng2 = place2.getLatLng();
                if (latLng2 != null) {
                    addressWrapper2.setLng(latLng2.c);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            AddressWrapper addressWrapper3 = this.a.b;
            if (addressWrapper3 != null) {
                addressWrapper3.setAddress(this.b);
            }
            CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.a;
            CommuteTimeSettingsDetailViewModel.f(commuteTimeSettingsDetailViewModel, null, null, null, null, null, Boolean.valueOf(commuteTimeSettingsDetailViewModel.l()), null, Boolean.FALSE, Boolean.FALSE, 95, null);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Mc3
        public /* bridge */ /* synthetic */ void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
            a(fetchPlaceResponse);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Lc3 {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailViewModel a;

        @DexIgnore
        public Ci(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel) {
            this.a = commuteTimeSettingsDetailViewModel;
        }

        @DexIgnore
        @Override // com.mapped.Lc3
        public final void onFailure(Exception exc) {
            Wg6.c(exc, "exception");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = CommuteTimeSettingsDetailViewModel.f;
            local.e(str, "FetchPlaceRequest - exception=" + exc);
            CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.a;
            CommuteTimeSettingsDetailViewModel.f(commuteTimeSettingsDetailViewModel, null, null, null, null, null, Boolean.valueOf(commuteTimeSettingsDetailViewModel.l()), null, Boolean.FALSE, Boolean.TRUE, 95, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel$start$1", f = "CommuteTimeSettingsDetailViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = commuteTimeSettingsDetailViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.a = Places.createClient(PortfolioApp.get.instance());
                AddressWrapper addressWrapper = this.this$0.b;
                boolean z = (addressWrapper != null ? addressWrapper.getType() : null) == AddressWrapper.AddressType.OTHER;
                CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.this$0;
                AddressWrapper addressWrapper2 = commuteTimeSettingsDetailViewModel.b;
                AddressWrapper.AddressType type = addressWrapper2 != null ? addressWrapper2.getType() : null;
                AddressWrapper addressWrapper3 = this.this$0.b;
                String name = addressWrapper3 != null ? addressWrapper3.getName() : null;
                Boolean a2 = Ao7.a(z);
                AddressWrapper addressWrapper4 = this.this$0.b;
                String address = addressWrapper4 != null ? addressWrapper4.getAddress() : null;
                AddressWrapper addressWrapper5 = this.this$0.b;
                CommuteTimeSettingsDetailViewModel.f(commuteTimeSettingsDetailViewModel, type, name, address, addressWrapper5 != null ? Ao7.a(addressWrapper5.getAvoidTolls()) : null, a2, null, this.this$0.a, null, null, 416, null);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = CommuteTimeSettingsDetailViewModel.class.getSimpleName();
        Wg6.b(simpleName, "CommuteTimeSettingsDetai\u2026el::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public CommuteTimeSettingsDetailViewModel(An4 an4) {
        Wg6.c(an4, "mSharedPreferencesManager");
    }

    @DexIgnore
    public static /* synthetic */ void f(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel, AddressWrapper.AddressType addressType, String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4, Boolean bool5, int i, Object obj) {
        commuteTimeSettingsDetailViewModel.e((i & 1) != 0 ? null : addressType, (i & 2) != 0 ? null : str, (i & 4) != 0 ? null : str2, (i & 8) != 0 ? null : bool, (i & 16) != 0 ? null : bool2, (i & 32) != 0 ? null : bool3, (i & 64) != 0 ? null : placesClient, (i & 128) != 0 ? null : bool4, (i & 256) != 0 ? null : bool5);
    }

    @DexIgnore
    public static /* synthetic */ void n(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel, String str, String str2, AutocompleteSessionToken autocompleteSessionToken, int i, Object obj) {
        if ((i & 1) != 0) {
            str = "";
        }
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            autocompleteSessionToken = null;
        }
        commuteTimeSettingsDetailViewModel.m(str, str2, autocompleteSessionToken);
    }

    @DexIgnore
    public final void e(AddressWrapper.AddressType addressType, String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4, Boolean bool5) {
        this.e.l(new Ai(addressType, str, str2, bool, bool2, bool3, placesClient, bool4, bool5));
    }

    @DexIgnore
    public final AddressWrapper g() {
        return this.b;
    }

    @DexIgnore
    public final void h(String str, AutocompleteSessionToken autocompleteSessionToken, String str2) {
        if (this.a != null && this.b != null) {
            f(this, null, null, null, null, null, null, null, Boolean.TRUE, null, 383, null);
            ArrayList arrayList = new ArrayList();
            arrayList.add(Place.Field.ADDRESS);
            arrayList.add(Place.Field.LAT_LNG);
            FetchPlaceRequest.Builder builder = FetchPlaceRequest.builder(str, arrayList);
            Wg6.b(builder, "FetchPlaceRequest.builder(placeId, placeFields)");
            builder.setSessionToken(autocompleteSessionToken);
            PlacesClient placesClient = this.a;
            if (placesClient != null) {
                Nt3<FetchPlaceResponse> fetchPlace = placesClient.fetchPlace(builder.build());
                Wg6.b(fetchPlace, "mPlacesClient!!.fetchPlace(request.build())");
                fetchPlace.f(new Bi(this, str2));
                fetchPlace.d(new Ci(this));
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final MutableLiveData<Ai> i() {
        return this.e;
    }

    @DexIgnore
    public final void j(AddressWrapper addressWrapper, ArrayList<String> arrayList) {
        this.c = addressWrapper;
        this.b = addressWrapper != null ? addressWrapper.clone() : null;
        this.d = arrayList;
    }

    @DexIgnore
    public final boolean k() {
        String str;
        String name;
        List<String> list = this.d;
        if (list != null) {
            for (String str2 : list) {
                AddressWrapper addressWrapper = this.b;
                if (addressWrapper == null || (name = addressWrapper.getName()) == null) {
                    str = null;
                } else if (name != null) {
                    str = name.toUpperCase();
                    Wg6.b(str, "(this as java.lang.String).toUpperCase()");
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
                if (str2 != null) {
                    String upperCase = str2.toUpperCase();
                    Wg6.b(upperCase, "(this as java.lang.String).toUpperCase()");
                    if (Wg6.a(str, upperCase)) {
                        return true;
                    }
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            return false;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final boolean l() {
        boolean z;
        String str;
        String str2;
        String name;
        String name2;
        String str3 = null;
        AddressWrapper addressWrapper = this.b;
        if (TextUtils.isEmpty(addressWrapper != null ? addressWrapper.getAddress() : null)) {
            return false;
        }
        AddressWrapper addressWrapper2 = this.b;
        if (TextUtils.isEmpty(addressWrapper2 != null ? addressWrapper2.getName() : null)) {
            return false;
        }
        AddressWrapper addressWrapper3 = this.b;
        Boolean valueOf = addressWrapper3 != null ? Boolean.valueOf(addressWrapper3.getAvoidTolls()) : null;
        AddressWrapper addressWrapper4 = this.c;
        if (!(!Wg6.a(valueOf, addressWrapper4 != null ? Boolean.valueOf(addressWrapper4.getAvoidTolls()) : null))) {
            AddressWrapper addressWrapper5 = this.b;
            AddressWrapper.AddressType type = addressWrapper5 != null ? addressWrapper5.getType() : null;
            AddressWrapper addressWrapper6 = this.c;
            if (type == (addressWrapper6 != null ? addressWrapper6.getType() : null)) {
                AddressWrapper addressWrapper7 = this.b;
                if (addressWrapper7 == null || (name2 = addressWrapper7.getName()) == null) {
                    str = null;
                } else if (name2 != null) {
                    str = name2.toUpperCase();
                    Wg6.b(str, "(this as java.lang.String).toUpperCase()");
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
                AddressWrapper addressWrapper8 = this.c;
                if (addressWrapper8 == null || (name = addressWrapper8.getName()) == null) {
                    str2 = null;
                } else if (name != null) {
                    str2 = name.toUpperCase();
                    Wg6.b(str2, "(this as java.lang.String).toUpperCase()");
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
                if (!(!Wg6.a(str, str2))) {
                    AddressWrapper addressWrapper9 = this.b;
                    String address = addressWrapper9 != null ? addressWrapper9.getAddress() : null;
                    AddressWrapper addressWrapper10 = this.c;
                    if (addressWrapper10 != null) {
                        str3 = addressWrapper10.getAddress();
                    }
                    if (!(!Wg6.a(address, str3))) {
                        z = false;
                        return z;
                    }
                }
            }
        }
        z = true;
        return z;
    }

    @DexIgnore
    public final void m(String str, String str2, AutocompleteSessionToken autocompleteSessionToken) {
        Wg6.c(str, "address");
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2) && autocompleteSessionToken != null) {
            if (str2 != null) {
                h(str2, autocompleteSessionToken, str);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void o(String str, Location location) {
        Wg6.c(str, "address");
        Wg6.c(location, PlaceFields.LOCATION);
        AddressWrapper addressWrapper = this.b;
        if (addressWrapper != null) {
            addressWrapper.setAddress(str);
        }
        AddressWrapper addressWrapper2 = this.b;
        if (addressWrapper2 != null) {
            addressWrapper2.setLat(location.getLatitude());
        }
        AddressWrapper addressWrapper3 = this.b;
        if (addressWrapper3 != null) {
            addressWrapper3.setLng(location.getLongitude());
        }
        f(this, null, null, null, null, null, Boolean.valueOf(l()), null, null, null, 479, null);
    }

    @DexIgnore
    public final void p(boolean z) {
        AddressWrapper addressWrapper = this.b;
        if (addressWrapper != null) {
            addressWrapper.setAvoidTolls(z);
        }
        f(this, null, null, null, null, null, Boolean.valueOf(l()), null, null, null, 479, null);
    }

    @DexIgnore
    public final void q(String str) {
        Wg6.c(str, "name");
        AddressWrapper addressWrapper = this.b;
        if (addressWrapper != null) {
            addressWrapper.setName(str);
        }
        f(this, null, null, null, null, null, Boolean.valueOf(l()), null, null, null, 479, null);
    }

    @DexIgnore
    public final void r() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Di(this, null), 3, null);
    }

    @DexIgnore
    public final void s() {
        this.a = null;
    }
}
