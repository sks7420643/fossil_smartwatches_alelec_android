package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Ao7;
import com.fossil.El7;
import com.fossil.Gl7;
import com.fossil.Gu7;
import com.fossil.H47;
import com.fossil.Hr7;
import com.fossil.Hs0;
import com.fossil.Kl5;
import com.fossil.Ko7;
import com.fossil.Lm7;
import com.fossil.Ls0;
import com.fossil.Mn7;
import com.fossil.Ni6;
import com.fossil.Oi6;
import com.fossil.Or0;
import com.fossil.Pi6;
import com.fossil.Pm7;
import com.fossil.Ss0;
import com.fossil.W57;
import com.fossil.Xh5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hh6;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateOverviewDayPresenter extends Ni6 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f;
    @DexIgnore
    public LiveData<H47<List<HeartRateSample>>> g;
    @DexIgnore
    public LiveData<H47<List<WorkoutSession>>> h;
    @DexIgnore
    public /* final */ Oi6 i;
    @DexIgnore
    public /* final */ HeartRateSampleRepository j;
    @DexIgnore
    public /* final */ WorkoutSessionRepository k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewDayPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$mHeartRateSamples$1$1", f = "HeartRateOverviewDayPresenter.kt", l = {35, 35}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<HeartRateSample>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Date date, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$it, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<HeartRateSample>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r6) goto L_0x0020
                    if (r0 != r7) goto L_0x0018
                    java.lang.Object r0 = r8.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r9)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r8.L$1
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                    com.fossil.El7.b(r9)
                    r2 = r9
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r1
                    r8.label = r7
                    java.lang.Object r0 = r3.a(r0, r8)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.El7.b(r9)
                    com.fossil.Hs0 r0 = r8.p$
                    com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$Ai r1 = r8.this$0
                    com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter r1 = r1.a
                    com.portfolio.platform.data.source.HeartRateSampleRepository r1 = com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter.p(r1)
                    java.util.Date r2 = r8.$it
                    java.lang.String r3 = "it"
                    com.mapped.Wg6.b(r2, r3)
                    java.util.Date r3 = r8.$it
                    java.lang.String r5 = "it"
                    com.mapped.Wg6.b(r3, r5)
                    r8.L$0 = r0
                    r8.L$1 = r0
                    r8.label = r6
                    r5 = 0
                    java.lang.Object r2 = r1.getHeartRateSamples(r2, r3, r5, r8)
                    if (r2 != r4) goto L_0x0066
                    r0 = r4
                    goto L_0x0017
                L_0x0066:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter.Ai.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Ai(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
            this.a = heartRateOverviewDayPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<HeartRateSample>>> a(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewDayPresenter", "mHeartRateSamples onDateChange " + date);
            return Or0.c(null, 0, new Aii(this, date, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewDayPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$mWorkoutSessions$1$1", f = "HeartRateOverviewDayPresenter.kt", l = {41, 41}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<WorkoutSession>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Date date, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$it, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<WorkoutSession>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r6) goto L_0x0020
                    if (r0 != r7) goto L_0x0018
                    java.lang.Object r0 = r8.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r9)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r8.L$1
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                    com.fossil.El7.b(r9)
                    r2 = r9
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r1
                    r8.label = r7
                    java.lang.Object r0 = r3.a(r0, r8)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.El7.b(r9)
                    com.fossil.Hs0 r0 = r8.p$
                    com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$Bi r1 = r8.this$0
                    com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter r1 = r1.a
                    com.portfolio.platform.data.source.WorkoutSessionRepository r1 = com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter.r(r1)
                    java.util.Date r2 = r8.$it
                    java.lang.String r3 = "it"
                    com.mapped.Wg6.b(r2, r3)
                    java.util.Date r3 = r8.$it
                    java.lang.String r5 = "it"
                    com.mapped.Wg6.b(r3, r5)
                    r8.L$0 = r0
                    r8.L$1 = r0
                    r8.label = r6
                    java.lang.Object r2 = r1.getWorkoutSessions(r2, r3, r6, r8)
                    if (r2 != r4) goto L_0x0065
                    r0 = r4
                    goto L_0x0017
                L_0x0065:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter.Bi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Bi(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
            this.a = heartRateOverviewDayPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<WorkoutSession>>> a(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewDayPresenter", "mWorkoutSessions onDateChange " + date);
            return Or0.c(null, 0, new Aii(this, date, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<H47<? extends List<HeartRateSample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewDayPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1", f = "HeartRateOverviewDayPresenter.kt", l = {63, 71, 78}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public long J$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$1", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii<T> implements Comparator<T> {
                    @DexIgnore
                    @Override // java.util.Comparator
                    public final int compare(T t, T t2) {
                        return Mn7.c(Long.valueOf(t.getStartTimeId().getMillis()), Long.valueOf(t2.getStartTimeId().getMillis()));
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        List list = this.this$0.$data;
                        if (list == null) {
                            return null;
                        }
                        if (list.size() > 1) {
                            Lm7.r(list, new Aiiii());
                        }
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$2", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Biii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $listTimeZoneChange;
                @DexIgnore
                public /* final */ /* synthetic */ List $listTodayHeartRateModel;
                @DexIgnore
                public /* final */ /* synthetic */ int $maxHR;
                @DexIgnore
                public /* final */ /* synthetic */ Hh6 $previousTimeZoneOffset;
                @DexIgnore
                public /* final */ /* synthetic */ long $startOfDay;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Aii aii, long j, Hh6 hh6, List list, List list2, int i, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                    this.$startOfDay = j;
                    this.$previousTimeZoneOffset = hh6;
                    this.$listTimeZoneChange = list;
                    this.$listTodayHeartRateModel = list2;
                    this.$maxHR = i;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Biii biii = new Biii(this.this$0, this.$startOfDay, this.$previousTimeZoneOffset, this.$listTimeZoneChange, this.$listTodayHeartRateModel, this.$maxHR, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
                    //return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    StringBuilder sb;
                    char c;
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        List list = this.this$0.$data;
                        if (list == null) {
                            return null;
                        }
                        for (T t : list) {
                            long millis = (t.getStartTimeId().getMillis() - this.$startOfDay) / 60000;
                            long millis2 = (t.getEndTime().getMillis() - this.$startOfDay) / 60000;
                            long j = (millis2 + millis) / ((long) 2);
                            if (t.getTimezoneOffsetInSecond() != this.$previousTimeZoneOffset.element) {
                                int hourOfDay = t.getStartTimeId().getHourOfDay();
                                String c2 = Kl5.c(hourOfDay);
                                Hr7 hr7 = Hr7.a;
                                String format = String.format("%02d:%02d", Arrays.copyOf(new Object[]{Ao7.e(Math.abs(t.getTimezoneOffsetInSecond() / 3600)), Ao7.e(Math.abs((t.getTimezoneOffsetInSecond() / 60) % 60))}, 2));
                                Wg6.b(format, "java.lang.String.format(format, *args)");
                                List list2 = this.$listTimeZoneChange;
                                Integer e = Ao7.e((int) j);
                                Lc6 lc6 = new Lc6(Ao7.e(hourOfDay), Ao7.d(((float) t.getTimezoneOffsetInSecond()) / 3600.0f));
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(c2);
                                if (t.getTimezoneOffsetInSecond() >= 0) {
                                    sb = new StringBuilder();
                                    c = '+';
                                } else {
                                    sb = new StringBuilder();
                                    c = '-';
                                }
                                sb.append(c);
                                sb.append(format);
                                sb2.append(sb.toString());
                                list2.add(new Gl7(e, lc6, sb2.toString()));
                                this.$previousTimeZoneOffset.element = t.getTimezoneOffsetInSecond();
                            }
                            if (!this.$listTodayHeartRateModel.isEmpty()) {
                                W57 w57 = (W57) Pm7.P(this.$listTodayHeartRateModel);
                                if (millis - ((long) w57.a()) > 1) {
                                    this.$listTodayHeartRateModel.add(new W57(0, 0, 0, w57.a(), (int) millis, (int) j));
                                }
                            }
                            int average = (int) t.getAverage();
                            if (t.getMax() == this.$maxHR) {
                                average = t.getMax();
                            }
                            this.$listTodayHeartRateModel.add(new W57(average, t.getMin(), t.getMax(), (int) millis, (int) millis2, (int) j));
                        }
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$maxHR$1", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Ciii extends Ko7 implements Coroutine<Il6, Xe6<? super Integer>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Ciii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Ciii ciii = new Ciii(this.this$0, xe6);
                    ciii.p$ = (Il6) obj;
                    throw null;
                    //return ciii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Integer> xe6) {
                    throw null;
                    //return ((Ciii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                /* JADX WARN: Multi-variable type inference failed */
                /* JADX WARN: Type inference failed for: r0v13, types: [java.lang.Comparable] */
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    int i;
                    T t;
                    Integer e;
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        List list = this.this$0.$data;
                        if (list != null) {
                            Iterator<T> it = list.iterator();
                            if (!it.hasNext()) {
                                t = null;
                            } else {
                                T next = it.next();
                                if (it.hasNext()) {
                                    Integer num = Ao7.e(next.getMax());
                                    while (true) {
                                        next = it.next();
                                        Integer e2 = Ao7.e(next.getMax());
                                        int compareTo = num.compareTo(e2);
                                        Integer num2 = e2;
                                        if (compareTo >= 0) {
                                            num2 = num;
                                            next = next;
                                        }
                                        if (!it.hasNext()) {
                                            break;
                                        }
                                        num = num2;
                                    }
                                }
                                t = next;
                            }
                            T t2 = t;
                            if (!(t2 == null || (e = Ao7.e(t2.getMax())) == null)) {
                                i = e.intValue();
                                return Ao7.e(i);
                            }
                        }
                        i = 0;
                        return Ao7.e(i);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$data = list;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$data, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r0v136, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r0v138, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r0v141, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r1v39, types: [java.util.List] */
            /* JADX WARNING: Removed duplicated region for block: B:36:0x0129  */
            /* JADX WARNING: Removed duplicated region for block: B:45:0x017a  */
            /* JADX WARNING: Removed duplicated region for block: B:50:0x01b9  */
            /* JADX WARNING: Unknown variable types count: 4 */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r13) {
                /*
                // Method dump skipped, instructions count: 884
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter.Ci.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Ci(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
            this.a = heartRateOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(H47<? extends List<HeartRateSample>> h47) {
            Xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mHeartRateSamples -- heartRateSamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("HeartRateOverviewDayPresenter", sb.toString());
            if (a2 != Xh5.DATABASE_LOADING) {
                Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, list, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<HeartRateSample>> h47) {
            a(h47);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<H47<? extends List<WorkoutSession>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewDayPresenter a;

        @DexIgnore
        public Di(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
            this.a = heartRateOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(H47<? extends List<WorkoutSession>> h47) {
            Xh5 a2 = h47.a();
            List<WorkoutSession> list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mWorkoutSessions -- workoutSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("HeartRateOverviewDayPresenter", sb.toString());
            if (a2 == Xh5.DATABASE_LOADING) {
                return;
            }
            if (list == null || list.isEmpty()) {
                this.a.i.v(false, new ArrayList());
            } else {
                this.a.i.v(true, list);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<WorkoutSession>> h47) {
            a(h47);
        }
    }

    @DexIgnore
    public HeartRateOverviewDayPresenter(Oi6 oi6, HeartRateSampleRepository heartRateSampleRepository, WorkoutSessionRepository workoutSessionRepository) {
        Wg6.c(oi6, "mView");
        Wg6.c(heartRateSampleRepository, "mHeartRateSampleRepository");
        Wg6.c(workoutSessionRepository, "mWorkoutSessionRepository");
        this.i = oi6;
        this.j = heartRateSampleRepository;
        this.k = workoutSessionRepository;
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.f = mutableLiveData;
        LiveData<H47<List<HeartRateSample>>> c = Ss0.c(mutableLiveData, new Ai(this));
        Wg6.b(c, "Transformations.switchMa\u2026, false))\n        }\n    }");
        this.g = c;
        LiveData<H47<List<WorkoutSession>>> c2 = Ss0.c(this.f, new Bi(this));
        Wg6.b(c2, "Transformations.switchMa\u2026t, true))\n        }\n    }");
        this.h = c2;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date date = this.e;
        if (date == null || !TimeUtils.p0(date).booleanValue()) {
            Date date2 = new Date();
            this.e = date2;
            this.f.l(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewDayPresenter", "loadData - mDate=" + this.e);
        LiveData<H47<List<HeartRateSample>>> liveData = this.g;
        Oi6 oi6 = this.i;
        if (oi6 != null) {
            liveData.h((Pi6) oi6, new Ci(this));
            this.h.h((LifecycleOwner) this.i, new Di(this));
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayFragment");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewDayPresenter", "stop");
        try {
            LiveData<H47<List<HeartRateSample>>> liveData = this.g;
            Oi6 oi6 = this.i;
            if (oi6 != null) {
                liveData.n((Pi6) oi6);
                this.h.n((LifecycleOwner) this.i);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewDayPresenter", "stop ex " + e2);
        }
    }

    @DexIgnore
    public final String u(Float f2) {
        if (f2 == null) {
            return "";
        }
        Hr7 hr7 = Hr7.a;
        String format = String.format("%02d:%02d", Arrays.copyOf(new Object[]{Integer.valueOf((int) Math.abs(f2.floatValue())), Integer.valueOf(((int) Math.abs(f2.floatValue() - (f2.floatValue() % ((float) 10)))) * 60)}, 2));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        if (f2.floatValue() >= ((float) 0)) {
            return '+' + format;
        }
        return '-' + format;
    }

    @DexIgnore
    public void v() {
        this.i.M5(this);
    }
}
