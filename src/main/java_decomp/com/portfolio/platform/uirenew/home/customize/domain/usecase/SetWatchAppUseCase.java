package com.portfolio.platform.uirenew.home.customize.domain.usecase;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Mo5;
import com.fossil.Oo5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetWatchAppUseCase extends CoroutineUseCase<Bi, Ci, Ai> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public Bi e;
    @DexIgnore
    public /* final */ Di f; // = new Di();
    @DexIgnore
    public Mo5 g;
    @DexIgnore
    public ArrayList<Oo5> h; // = new ArrayList<>();
    @DexIgnore
    public /* final */ DianaPresetRepository i;
    @DexIgnore
    public /* final */ FileRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public Ai(int i, ArrayList<Integer> arrayList) {
            Wg6.c(arrayList, "mBLEErrorCodes");
            this.a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ List<Oo5> b;
        @DexIgnore
        public /* final */ List<DianaAppSetting> c;

        @DexIgnore
        public Bi(String str, List<Oo5> list, List<DianaAppSetting> list2) {
            Wg6.c(str, "presetId");
            Wg6.c(list, "presetButtons");
            Wg6.c(list2, Constants.USER_SETTING);
            this.a = str;
            this.b = list;
            this.c = list2;
        }

        @DexIgnore
        public final List<Oo5> a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }

        @DexIgnore
        public final List<DianaAppSetting> c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di implements BleCommandResultManager.Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase$SetPresetBroadcastReceiver$receive$1", f = "SetWatchAppUseCase.kt", l = {52}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Intent $intent;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Intent intent, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
                this.$intent = intent;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$intent, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    SetWatchAppUseCase setWatchAppUseCase = SetWatchAppUseCase.this;
                    this.L$0 = il6;
                    this.label = 1;
                    if (setWatchAppUseCase.s(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (this.$intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    FLogger.INSTANCE.getLocal().d("SetWatchAppUseCase", "onReceive - success");
                    SetWatchAppUseCase.this.j(new Ci());
                } else {
                    FLogger.INSTANCE.getLocal().d("SetWatchAppUseCase", "onReceive - failed isSettingChangedOnly");
                    int intExtra = this.$intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                    ArrayList<Integer> integerArrayListExtra = this.$intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>(intExtra);
                    }
                    SetWatchAppUseCase.this.i(new Ai(intExtra, integerArrayListExtra));
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetWatchAppUseCase", "onReceive - communicateMode=" + communicateMode);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            if (SetWatchAppUseCase.this.n()) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("SetWatchAppUseCase", "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
                if (communicateMode == CommunicateMode.SET_WATCH_APPS) {
                    SetWatchAppUseCase.this.q(false);
                    Rm6 unused = Gu7.d(SetWatchAppUseCase.this.g(), null, null, new Aii(this, intent, null), 3, null);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase", f = "SetWatchAppUseCase.kt", l = {92, 107, 111}, m = "run")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ SetWatchAppUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(SetWatchAppUseCase setWatchAppUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = setWatchAppUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.p(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase", f = "SetWatchAppUseCase.kt", l = {77, 81}, m = "updatePresetButtons")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ SetWatchAppUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(SetWatchAppUseCase setWatchAppUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = setWatchAppUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(this);
        }
    }

    @DexIgnore
    public SetWatchAppUseCase(DianaPresetRepository dianaPresetRepository, FileRepository fileRepository, DianaAppSettingRepository dianaAppSettingRepository, DianaAppSettingRepository dianaAppSettingRepository2) {
        Wg6.c(dianaPresetRepository, "mDianaPresetRepository");
        Wg6.c(fileRepository, "mFileRepository");
        Wg6.c(dianaAppSettingRepository, "mDianaAppSetting");
        Wg6.c(dianaAppSettingRepository2, "mDianaAppSettingRepository");
        this.i = dianaPresetRepository;
        this.j = fileRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "SetWatchAppUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return p(bi, xe6);
    }

    @DexIgnore
    public final boolean n() {
        return this.d;
    }

    @DexIgnore
    public final void o() {
        BleCommandResultManager.d.e(this.f, CommunicateMode.SET_WATCH_APPS);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x016b  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01fd  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x021b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object p(com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase.Bi r13, com.mapped.Xe6<java.lang.Object> r14) {
        /*
        // Method dump skipped, instructions count: 561
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase.p(com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase$Bi, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void q(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final void r() {
        BleCommandResultManager.d.j(this.f, CommunicateMode.SET_WATCH_APPS);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object s(com.mapped.Xe6<? super com.mapped.Cd6> r9) {
        /*
            r8 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 2
            boolean r0 = r9 instanceof com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase.Fi
            if (r0 == 0) goto L_0x0049
            r0 = r9
            com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase$Fi r0 = (com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase.Fi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0049
            int r1 = r1 + r3
            r0.label = r1
            r3 = r0
        L_0x0015:
            java.lang.Object r2 = r3.result
            java.lang.Object r5 = com.fossil.Yn7.d()
            int r0 = r3.label
            if (r0 == 0) goto L_0x0084
            if (r0 == r4) goto L_0x0058
            if (r0 != r7) goto L_0x0050
            java.lang.Object r0 = r3.L$3
            com.portfolio.platform.data.model.ServerError r0 = (com.portfolio.platform.data.model.ServerError) r0
            java.lang.Object r0 = r3.L$2
            com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
            java.lang.Object r0 = r3.L$1
            com.fossil.Mo5 r0 = (com.fossil.Mo5) r0
            java.lang.Object r1 = r3.L$0
            com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase r1 = (com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r4 = r0
        L_0x0038:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.fossil.Qo5 r0 = r0.getDianaPresetDao()
            long r0 = r0.l(r4)
            com.fossil.Ao7.f(r0)
        L_0x0046:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0048:
            return r0
        L_0x0049:
            com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase$Fi r0 = new com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase$Fi
            r0.<init>(r8, r9)
            r3 = r0
            goto L_0x0015
        L_0x0050:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0058:
            java.lang.Object r0 = r3.L$1
            com.fossil.Mo5 r0 = (com.fossil.Mo5) r0
            java.lang.Object r1 = r3.L$0
            com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase r1 = (com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase) r1
            com.fossil.El7.b(r2)
            r4 = r0
        L_0x0064:
            r0 = r2
            com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
            com.portfolio.platform.data.model.ServerError r2 = r0.a()
            if (r2 == 0) goto L_0x0046
            r4.s(r7)
            com.portfolio.platform.manager.EncryptedDatabaseManager r6 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r3.L$0 = r1
            r3.L$1 = r4
            r3.L$2 = r0
            r3.L$3 = r2
            r3.label = r7
            java.lang.Object r1 = r6.v(r3)
            if (r1 != r5) goto L_0x0038
            r0 = r5
            goto L_0x0048
        L_0x0084:
            com.fossil.El7.b(r2)
            com.fossil.Mo5 r0 = r8.g
            if (r0 == 0) goto L_0x0046
            java.util.List r1 = r0.a()
            if (r1 == 0) goto L_0x0094
            r1.clear()
        L_0x0094:
            java.util.List r1 = r0.a()
            if (r1 == 0) goto L_0x00a3
            java.util.ArrayList<com.fossil.Oo5> r2 = r8.h
            boolean r1 = r1.addAll(r2)
            com.fossil.Ao7.a(r1)
        L_0x00a3:
            com.portfolio.platform.preset.data.source.DianaPresetRepository r1 = r8.i
            com.portfolio.platform.data.source.FileRepository r2 = r8.j
            com.fossil.Jo5 r2 = com.fossil.Lo5.c(r0, r2)
            r3.L$0 = r8
            r3.L$1 = r0
            r3.label = r4
            java.lang.Object r2 = r1.q(r2, r3)
            if (r2 != r5) goto L_0x00b9
            r0 = r5
            goto L_0x0048
        L_0x00b9:
            r1 = r8
            r4 = r0
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase.s(com.mapped.Xe6):java.lang.Object");
    }
}
