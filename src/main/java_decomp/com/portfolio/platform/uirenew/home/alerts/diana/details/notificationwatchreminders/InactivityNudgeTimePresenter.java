package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import android.text.format.DateFormat;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.P26;
import com.fossil.Q26;
import com.fossil.Um5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InactivityNudgeTimePresenter extends P26 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public InactivityNudgeTimeModel g;
    @DexIgnore
    public InactivityNudgeTimeModel h;
    @DexIgnore
    public /* final */ Q26 i;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$1", f = "InactivityNudgeTimePresenter.kt", l = {100}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ InactivityNudgeTimePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$1$1", f = "InactivityNudgeTimePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ Aii b;

                @DexIgnore
                public Aiii(Aii aii) {
                    this.b = aii;
                }

                @DexIgnore
                public final void run() {
                    InactivityNudgeTimeDao inactivityNudgeTimeDao = this.b.this$0.this$0.j.getInactivityNudgeTimeDao();
                    InactivityNudgeTimeModel inactivityNudgeTimeModel = this.b.this$0.this$0.g;
                    if (inactivityNudgeTimeModel != null) {
                        inactivityNudgeTimeDao.upsertInactivityNudgeTime(inactivityNudgeTimeModel);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.j.runInTransaction(new Aiii(this));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(InactivityNudgeTimePresenter inactivityNudgeTimePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = inactivityNudgeTimePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(i2, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.i.close();
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$2", f = "InactivityNudgeTimePresenter.kt", l = {115}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ InactivityNudgeTimePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$2$1", f = "InactivityNudgeTimePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ Aii b;

                @DexIgnore
                public Aiii(Aii aii) {
                    this.b = aii;
                }

                @DexIgnore
                public final void run() {
                    InactivityNudgeTimeDao inactivityNudgeTimeDao = this.b.this$0.this$0.j.getInactivityNudgeTimeDao();
                    InactivityNudgeTimeModel inactivityNudgeTimeModel = this.b.this$0.this$0.h;
                    if (inactivityNudgeTimeModel != null) {
                        inactivityNudgeTimeDao.upsertInactivityNudgeTime(inactivityNudgeTimeModel);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.j.runInTransaction(new Aiii(this));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(InactivityNudgeTimePresenter inactivityNudgeTimePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = inactivityNudgeTimePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(i2, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.i.close();
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$start$1", f = "InactivityNudgeTimePresenter.kt", l = {35}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ InactivityNudgeTimePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$start$1$listInactivityNudgeTimeModel$1", f = "InactivityNudgeTimePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends InactivityNudgeTimeModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends InactivityNudgeTimeModel>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.j.getInactivityNudgeTimeDao().getListInactivityNudgeTimeModel();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(InactivityNudgeTimePresenter inactivityNudgeTimePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = inactivityNudgeTimePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(i2, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            for (InactivityNudgeTimeModel inactivityNudgeTimeModel : (List) g) {
                if (inactivityNudgeTimeModel.getNudgeTimeType() == this.this$0.f) {
                    boolean is24HourFormat = DateFormat.is24HourFormat(PortfolioApp.get.instance());
                    this.this$0.i.B5(is24HourFormat);
                    Q26 q26 = this.this$0.i;
                    InactivityNudgeTimePresenter inactivityNudgeTimePresenter = this.this$0;
                    q26.t(inactivityNudgeTimePresenter.z(inactivityNudgeTimePresenter.f));
                    this.this$0.i.h6(inactivityNudgeTimeModel.getMinutes(), is24HourFormat);
                }
                if (inactivityNudgeTimeModel.getNudgeTimeType() == 0) {
                    this.this$0.g = inactivityNudgeTimeModel;
                } else {
                    this.this$0.h = inactivityNudgeTimeModel;
                }
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = InactivityNudgeTimePresenter.class.getSimpleName();
        Wg6.b(simpleName, "InactivityNudgeTimePrese\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public InactivityNudgeTimePresenter(Q26 q26, RemindersSettingsDatabase remindersSettingsDatabase) {
        Wg6.c(q26, "mView");
        Wg6.c(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        this.i = q26;
        this.j = remindersSettingsDatabase;
    }

    @DexIgnore
    public void A() {
        this.i.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(k, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Rm6 unused = Gu7.d(k(), null, null, new Ci(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    @Override // com.fossil.P26
    public void n() {
        if (this.f == 0) {
            int i2 = this.e;
            InactivityNudgeTimeModel inactivityNudgeTimeModel = this.h;
            if (inactivityNudgeTimeModel == null) {
                Wg6.i();
                throw null;
            } else if (i2 > inactivityNudgeTimeModel.getMinutes()) {
                String c = Um5.c(PortfolioApp.get.instance(), 2131886115);
                Q26 q26 = this.i;
                Wg6.b(c, "des");
                q26.Y0(c);
            } else {
                InactivityNudgeTimeModel inactivityNudgeTimeModel2 = this.g;
                if (inactivityNudgeTimeModel2 != null) {
                    inactivityNudgeTimeModel2.setMinutes(this.e);
                    Rm6 unused = Gu7.d(k(), null, null, new Ai(this, null), 3, null);
                    return;
                }
                Wg6.i();
                throw null;
            }
        } else {
            int i3 = this.e;
            InactivityNudgeTimeModel inactivityNudgeTimeModel3 = this.g;
            if (inactivityNudgeTimeModel3 == null) {
                Wg6.i();
                throw null;
            } else if (i3 < inactivityNudgeTimeModel3.getMinutes()) {
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131886115);
                Q26 q262 = this.i;
                Wg6.b(c2, "des");
                q262.Y0(c2);
            } else {
                InactivityNudgeTimeModel inactivityNudgeTimeModel4 = this.h;
                if (inactivityNudgeTimeModel4 != null) {
                    inactivityNudgeTimeModel4.setMinutes(this.e);
                    Rm6 unused2 = Gu7.d(k(), null, null, new Bi(this, null), 3, null);
                    return;
                }
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.P26
    public void o(int i2) {
        this.f = i2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x005f  */
    @Override // com.fossil.P26
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void p(java.lang.String r9, java.lang.String r10, boolean r11) {
        /*
            r8 = this;
            r3 = 12
            r1 = 0
            java.lang.String r0 = "hourValue"
            com.mapped.Wg6.c(r9, r0)
            java.lang.String r0 = "minuteValue"
            com.mapped.Wg6.c(r10, r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter.k
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "updateTime: hourValue = "
            r4.append(r5)
            r4.append(r9)
            java.lang.String r5 = ", minuteValue = "
            r4.append(r5)
            r4.append(r10)
            java.lang.String r5 = ", isPM = "
            r4.append(r5)
            r4.append(r11)
            java.lang.String r4 = r4.toString()
            r0.d(r2, r4)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0065 }
            java.lang.String r2 = "Integer.valueOf(hourValue)"
            com.mapped.Wg6.b(r0, r2)     // Catch:{ Exception -> 0x0065 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0065 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r4 = "Integer.valueOf(minuteValue)"
            com.mapped.Wg6.b(r2, r4)     // Catch:{ Exception -> 0x0097 }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x0097 }
        L_0x0053:
            com.portfolio.platform.PortfolioApp$inner r4 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r4 = r4.instance()
            boolean r4 = android.text.format.DateFormat.is24HourFormat(r4)
            if (r4 == 0) goto L_0x0085
            int r0 = r0 * 60
            int r0 = r0 + r2
            r8.e = r0
        L_0x0064:
            return
        L_0x0065:
            r2 = move-exception
            r0 = r1
        L_0x0067:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter.k
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Exception when parse time e="
            r6.append(r7)
            r6.append(r2)
            java.lang.String r2 = r6.toString()
            r4.e(r5, r2)
            r2 = r1
            goto L_0x0053
        L_0x0085:
            if (r11 == 0) goto L_0x0093
            if (r0 != r3) goto L_0x0090
            r1 = r3
        L_0x008a:
            int r0 = r1 * 60
            int r0 = r0 + r2
            r8.e = r0
            goto L_0x0064
        L_0x0090:
            int r1 = r0 + 12
            goto L_0x008a
        L_0x0093:
            if (r0 == r3) goto L_0x008a
            r1 = r0
            goto L_0x008a
        L_0x0097:
            r2 = move-exception
            goto L_0x0067
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter.p(java.lang.String, java.lang.String, boolean):void");
    }

    @DexIgnore
    public final String z(int i2) {
        if (i2 == 0) {
            String c = Um5.c(PortfolioApp.get.instance(), 2131886122);
            Wg6.b(c, "LanguageHelper.getString\u2026eAlerts_Main_Text__Start)");
            return c;
        }
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886120);
        Wg6.b(c2, "LanguageHelper.getString\u2026oveAlerts_Main_Text__End)");
        return c2;
    }
}
