package com.portfolio.platform.uirenew.home.profile.help.deleteaccount;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.Kp6;
import com.fossil.Zp6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeleteAccountActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public DeleteAccountPresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            Wg6.c(context, "context");
            context.startActivity(new Intent(context, DeleteAccountActivity.class));
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        Kp6 kp6 = (Kp6) getSupportFragmentManager().Y(2131362158);
        if (kp6 == null) {
            kp6 = Kp6.m.a();
            i(kp6, 2131362158);
        }
        PortfolioApp.get.instance().getIface().G1(new Zp6(kp6)).a(this);
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onResume() {
        super.onResume();
        l(false);
    }
}
