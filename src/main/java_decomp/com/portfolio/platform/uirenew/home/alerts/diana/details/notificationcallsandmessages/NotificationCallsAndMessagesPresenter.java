package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages;

import android.content.Intent;
import android.text.TextUtils;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.D26;
import com.fossil.D47;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.F26;
import com.fossil.G26;
import com.fossil.Gu7;
import com.fossil.J06;
import com.fossil.Jn5;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Pm7;
import com.fossil.R16;
import com.fossil.Tq4;
import com.fossil.Uh5;
import com.fossil.Um5;
import com.fossil.Uq4;
import com.fossil.V36;
import com.fossil.X06;
import com.fossil.Y06;
import com.fossil.Yn7;
import com.fossil.Z06;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.mapped.An4;
import com.mapped.AppWrapper;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationCallsAndMessagesPresenter extends X06 {
    @DexIgnore
    public static /* final */ String D;
    @DexIgnore
    public static /* final */ Ai E; // = new Ai(null);
    @DexIgnore
    public /* final */ SetReplyMessageMappingUseCase A;
    @DexIgnore
    public /* final */ QuickResponseRepository B;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase C;
    @DexIgnore
    public List<AppNotificationFilter> e; // = new ArrayList();
    @DexIgnore
    public List<AppWrapper> f; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> g; // = new ArrayList();
    @DexIgnore
    public /* final */ Bi h; // = new Bi();
    @DexIgnore
    public volatile boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean m; // = true;
    @DexIgnore
    public boolean n; // = true;
    @DexIgnore
    public /* final */ List<J06> o; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> p; // = new ArrayList();
    @DexIgnore
    public /* final */ LiveData<List<NotificationSettingsModel>> q; // = this.z.getListNotificationSettings();
    @DexIgnore
    public boolean r; // = true;
    @DexIgnore
    public R16 s;
    @DexIgnore
    public /* final */ Y06 t;
    @DexIgnore
    public /* final */ Uq4 u;
    @DexIgnore
    public /* final */ D26 v;
    @DexIgnore
    public /* final */ F26 w;
    @DexIgnore
    public /* final */ V36 x;
    @DexIgnore
    public /* final */ An4 y;
    @DexIgnore
    public /* final */ NotificationSettingsDao z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return NotificationCallsAndMessagesPresenter.D;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi implements BleCommandResultManager.Bi {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "SetNotificationFilterReceiver");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationCallsAndMessagesPresenter.E.a();
            local.d(a2, "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode + " isSettingRules " + NotificationCallsAndMessagesPresenter.this.i);
            if (communicateMode == CommunicateMode.SET_NOTIFICATION_FILTERS && NotificationCallsAndMessagesPresenter.this.i) {
                NotificationCallsAndMessagesPresenter.this.i = false;
                NotificationCallsAndMessagesPresenter.this.t.a();
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "onReceive - success");
                    NotificationCallsAndMessagesPresenter.this.t.close();
                    return;
                }
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "onReceive - failed");
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = NotificationCallsAndMessagesPresenter.E.a();
                local2.d(a3, "mAllowMessagesFromFirsLoad=" + NotificationCallsAndMessagesPresenter.this.U() + " mAlowCallsFromFirstLoad=" + NotificationCallsAndMessagesPresenter.this.V() + " mListFavoriteContactFirstLoad=" + NotificationCallsAndMessagesPresenter.this.Y() + " size=" + NotificationCallsAndMessagesPresenter.this.Y().size());
                ArrayList arrayList = new ArrayList();
                NotificationSettingsModel notificationSettingsModel = new NotificationSettingsModel("AllowCallsFrom", NotificationCallsAndMessagesPresenter.this.V(), true);
                NotificationSettingsModel notificationSettingsModel2 = new NotificationSettingsModel("AllowMessagesFrom", NotificationCallsAndMessagesPresenter.this.U(), false);
                arrayList.add(notificationSettingsModel);
                arrayList.add(notificationSettingsModel2);
                NotificationCallsAndMessagesPresenter.this.c0(arrayList);
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                ArrayList<Integer> arrayList2 = integerArrayListExtra != null ? integerArrayListExtra : new ArrayList<>(intExtra2);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = NotificationCallsAndMessagesPresenter.E.a();
                local3.d(a4, "permissionErrorCodes=" + arrayList2 + " , size=" + arrayList2.size());
                int size = arrayList2.size();
                for (int i = 0; i < size; i++) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String a5 = NotificationCallsAndMessagesPresenter.E.a();
                    local4.d(a5, "error code " + i + " =" + arrayList2.get(i));
                }
                if (intExtra2 == 1101 || intExtra2 == 1112 || intExtra2 == 1113) {
                    List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(arrayList2);
                    Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
                    Y06 y06 = NotificationCallsAndMessagesPresenter.this.t;
                    Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                    if (array != null) {
                        Uh5[] uh5Arr = (Uh5[]) array;
                        y06.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                        return;
                    }
                    throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                }
                NotificationCallsAndMessagesPresenter.this.t.c();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Tq4.Di<F26.Bi, Tq4.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ ContactGroup b;

        @DexIgnore
        public Ci(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, ContactGroup contactGroup) {
            this.a = notificationCallsAndMessagesPresenter;
            this.b = contactGroup;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void a(Tq4.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(Tq4.Ai ai) {
            Wg6.c(ai, "errorResponse");
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), ".Inside mRemoveContactGroup onError");
        }

        @DexIgnore
        public void c(F26.Bi bi) {
            Wg6.c(bi, "successResponse");
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), ".Inside mRemoveContactGroup onSuccess");
            this.a.t.h3(this.b);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void onSuccess(F26.Bi bi) {
            c(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$saveNotificationSettings$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {499}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $settings;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$saveNotificationSettings$1$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ Aii b;

                @DexIgnore
                public Aiii(Aii aii) {
                    this.b = aii;
                }

                @DexIgnore
                public final void run() {
                    this.b.this$0.this$0.C.getNotificationSettingsDao().insertListNotificationSettings(this.b.this$0.$settings);
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.C.runInTransaction(new Aiii(this));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, List list, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationCallsAndMessagesPresenter;
            this.$settings = list;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$settings, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(i2, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setReplyMessageToDevice$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {428}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements CoroutineUseCase.Ei<SetReplyMessageMappingUseCase.Ci, SetReplyMessageMappingUseCase.Bi> {
            @DexIgnore
            public /* final */ /* synthetic */ Ei a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Aii(Ei ei) {
                this.a = ei;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void a(SetReplyMessageMappingUseCase.Bi bi) {
                b(bi);
            }

            @DexIgnore
            public void b(SetReplyMessageMappingUseCase.Bi bi) {
                Wg6.c(bi, "errorValue");
                FLogger.INSTANCE.getLocal().e(NotificationCallsAndMessagesPresenter.E.a(), "Set reply message to device fail");
                this.a.this$0.t.a();
                this.a.this$0.t.C2(false);
                if (!Jn5.b.m(PortfolioApp.get.instance().getApplicationContext(), Jn5.Ci.BLUETOOTH_CONNECTION)) {
                    Jn5 jn5 = Jn5.b;
                    Y06 y06 = this.a.this$0.t;
                    if (y06 != null) {
                        Jn5.c(jn5, ((Z06) y06).getContext(), Jn5.Ai.SET_BLE_COMMAND, false, false, false, null, 60, null);
                        return;
                    }
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
                }
                this.a.this$0.t.E2();
            }

            @DexIgnore
            public void c(SetReplyMessageMappingUseCase.Ci ci) {
                Wg6.c(ci, "responseValue");
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "Set reply message to device success");
                this.a.this$0.t.a();
                this.a.this$0.t.C2(true);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void onSuccess(SetReplyMessageMappingUseCase.Ci ci) {
                c(ci);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setReplyMessageToDevice$1$messages$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends QuickResponseMessage>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends QuickResponseMessage>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.B.getAllQuickResponse();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationCallsAndMessagesPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.t.b();
                Dv7 i2 = this.this$0.i();
                Bii bii = new Bii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(i2, bii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.A.e(new SetReplyMessageMappingUseCase.Ai((List) g), new Aii(this));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {393, 395}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {333}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends AppNotificationFilter>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends AppNotificationFilter>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX DEBUG: Failed to insert an additional move for type inference into block B:42:0x00e3 */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Object] */
            /* JADX WARN: Type inference failed for: r2v1 */
            /* JADX WARN: Type inference failed for: r2v2, types: [java.lang.Object] */
            /* JADX WARN: Type inference failed for: r2v3 */
            /* JADX WARN: Type inference failed for: r2v4, types: [java.util.List, java.util.ArrayList] */
            /* JADX WARN: Type inference failed for: r2v5 */
            /* JADX WARNING: Unknown variable types count: 1 */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r13) {
                /*
                // Method dump skipped, instructions count: 722
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.Fi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1$otherAppDeffer$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends AppNotificationFilter>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends AppNotificationFilter>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return D47.c(this.this$0.this$0.X(), false, 1, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationCallsAndMessagesPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00e0  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 534
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.Fi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> implements Ls0<R16.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter a;

        @DexIgnore
        public Gi(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter) {
            this.a = notificationCallsAndMessagesPresenter;
        }

        @DexIgnore
        public final void a(R16.Ai ai) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationCallsAndMessagesPresenter.E.a();
            local.d(a2, "NotificationSettingChanged value = " + ai);
            String Z = this.a.Z(ai.a());
            if (ai.b()) {
                this.a.t.m4(Z);
            } else {
                this.a.t.w1(Z);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(R16.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2", f = "NotificationCallsAndMessagesPresenter.kt", l = {158, 165}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {159}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    PortfolioApp instance = PortfolioApp.get.instance();
                    this.L$0 = il6;
                    this.label = 1;
                    if (instance.V0(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements CoroutineUseCase.Ei<D26.Ci, D26.Ai> {
            @DexIgnore
            public /* final */ /* synthetic */ Hi a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Bii(Hi hi) {
                this.a = hi;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void a(D26.Ai ai) {
                b(ai);
            }

            @DexIgnore
            public void b(D26.Ai ai) {
                Wg6.c(ai, "errorValue");
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "GetAllContactGroup onError");
            }

            @DexIgnore
            public void c(D26.Ci ci) {
                Wg6.c(ci, "responseValue");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = NotificationCallsAndMessagesPresenter.E.a();
                local.d(a2, "inside start, GetAllContactGroup onSuccess, size = " + ci.a().size());
                this.a.this$0.t.D4(Pm7.j0(ci.a()));
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = NotificationCallsAndMessagesPresenter.E.a();
                local2.d(a3, "mFlagGetListFavoriteFirstLoad=" + this.a.this$0.W());
                if (this.a.this$0.W()) {
                    this.a.this$0.g0(Pm7.j0(ci.a()));
                    for (T t : ci.a()) {
                        List<Contact> contacts = t.getContacts();
                        Wg6.b(contacts, "contactGroup.contacts");
                        if (!contacts.isEmpty()) {
                            Contact contact = t.getContacts().get(0);
                            J06 j06 = new J06(contact, null, 2, null);
                            j06.setAdded(true);
                            Contact contact2 = j06.getContact();
                            if (contact2 != null) {
                                Wg6.b(contact, "contact");
                                contact2.setDbRowId(contact.getDbRowId());
                            }
                            Contact contact3 = j06.getContact();
                            if (contact3 != null) {
                                Wg6.b(contact, "contact");
                                contact3.setUseSms(contact.isUseSms());
                            }
                            Contact contact4 = j06.getContact();
                            if (contact4 != null) {
                                Wg6.b(contact, "contact");
                                contact4.setUseCall(contact.isUseCall());
                            }
                            Wg6.b(contact, "contact");
                            List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                            Wg6.b(phoneNumbers, "contact.phoneNumbers");
                            if (!phoneNumbers.isEmpty()) {
                                PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                                Wg6.b(phoneNumber, "contact.phoneNumbers[0]");
                                if (!TextUtils.isEmpty(phoneNumber.getNumber())) {
                                    j06.setHasPhoneNumber(true);
                                    PhoneNumber phoneNumber2 = contact.getPhoneNumbers().get(0);
                                    Wg6.b(phoneNumber2, "contact.phoneNumbers[0]");
                                    j06.setPhoneNumber(phoneNumber2.getNumber());
                                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                    String a4 = NotificationCallsAndMessagesPresenter.E.a();
                                    StringBuilder sb = new StringBuilder();
                                    sb.append(".Inside loadContactData filter selected contact, phoneNumber=");
                                    PhoneNumber phoneNumber3 = contact.getPhoneNumbers().get(0);
                                    Wg6.b(phoneNumber3, "contact.phoneNumbers[0]");
                                    sb.append(phoneNumber3.getNumber());
                                    local3.d(a4, sb.toString());
                                }
                            }
                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                            String a5 = NotificationCallsAndMessagesPresenter.E.a();
                            local4.d(a5, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                            this.a.this$0.Y().add(j06);
                        }
                    }
                    this.a.this$0.f0(false);
                }
                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                String a6 = NotificationCallsAndMessagesPresenter.E.a();
                local5.d(a6, "start, mListFavoriteContactWrapperFirstLoad=" + this.a.this$0.Y() + " size=" + this.a.this$0.Y().size());
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void onSuccess(D26.Ci ci) {
                c(ci);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii implements CoroutineUseCase.Ei<V36.Ai, CoroutineUseCase.Ai> {
            @DexIgnore
            public /* final */ /* synthetic */ Hi a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Cii(Hi hi) {
                this.a = hi;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void a(CoroutineUseCase.Ai ai) {
                b(ai);
            }

            @DexIgnore
            public void b(CoroutineUseCase.Ai ai) {
                Wg6.c(ai, "errorValue");
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "GetApps onError");
            }

            @DexIgnore
            public void c(V36.Ai ai) {
                Wg6.c(ai, "responseValue");
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "GetApps onSuccess");
                this.a.this$0.X().clear();
                this.a.this$0.X().addAll(ai.a());
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void onSuccess(V36.Ai ai) {
                c(ai);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$settings$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Dii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends NotificationSettingsModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Hi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Dii(Hi hi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = hi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Dii dii = new Dii(this.this$0, xe6);
                dii.p$ = (Il6) obj;
                throw null;
                //return dii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends NotificationSettingsModel>> xe6) {
                throw null;
                //return ((Dii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.z.getListNotificationSettingsNoLiveData();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationCallsAndMessagesPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x008c  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
            // Method dump skipped, instructions count: 383
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter.Hi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = NotificationCallsAndMessagesPresenter.class.getSimpleName();
        Wg6.b(simpleName, "NotificationCallsAndMess\u2026er::class.java.simpleName");
        D = simpleName;
    }
    */

    @DexIgnore
    public NotificationCallsAndMessagesPresenter(Y06 y06, Uq4 uq4, D26 d26, F26 f26, G26 g26, V36 v36, An4 an4, NotificationSettingsDao notificationSettingsDao, SetReplyMessageMappingUseCase setReplyMessageMappingUseCase, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        Wg6.c(y06, "mView");
        Wg6.c(uq4, "mUseCaseHandler");
        Wg6.c(d26, "mGetAllContactGroup");
        Wg6.c(f26, "mRemoveContactGroup");
        Wg6.c(g26, "mSaveContactGroupsNotification");
        Wg6.c(v36, "mGetApps");
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(notificationSettingsDao, "mNotificationSettingDao");
        Wg6.c(setReplyMessageMappingUseCase, "mReplyMessageUseCase");
        Wg6.c(quickResponseRepository, "mQRRepository");
        Wg6.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.t = y06;
        this.u = uq4;
        this.v = d26;
        this.w = f26;
        this.x = v36;
        this.y = an4;
        this.z = notificationSettingsDao;
        this.A = setReplyMessageMappingUseCase;
        this.B = quickResponseRepository;
        this.C = notificationSettingsDatabase;
    }

    @DexIgnore
    public final int U() {
        return this.l;
    }

    @DexIgnore
    public final int V() {
        return this.j;
    }

    @DexIgnore
    public final boolean W() {
        return this.n;
    }

    @DexIgnore
    public final List<AppWrapper> X() {
        return this.f;
    }

    @DexIgnore
    public final List<J06> Y() {
        return this.o;
    }

    @DexIgnore
    public final String Z(int i2) {
        if (i2 == 0) {
            String c = Um5.c(PortfolioApp.get.instance(), 2131886089);
            Wg6.b(c, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return c;
        } else if (i2 != 1) {
            String c2 = Um5.c(PortfolioApp.get.instance(), 2131886091);
            Wg6.b(c2, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return c2;
        } else {
            String c3 = Um5.c(PortfolioApp.get.instance(), 2131886090);
            Wg6.b(c3, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return c3;
        }
    }

    @DexIgnore
    public final boolean a0() {
        return (this.t.L1() == this.j && this.t.O2() == this.l && !(Wg6.a(this.p, this.t.f5()) ^ true)) ? false : true;
    }

    @DexIgnore
    public final void b0() {
        FLogger.INSTANCE.getLocal().d(D, "registerBroadcastReceiver");
        BleCommandResultManager.d.e(this.h, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public final void c0(List<NotificationSettingsModel> list) {
        FLogger.INSTANCE.getLocal().d(D, "saveNotificationSettings");
        Rm6 unused = Gu7.d(k(), null, null, new Di(this, list, null), 3, null);
    }

    @DexIgnore
    public final void d0(int i2) {
        this.l = i2;
    }

    @DexIgnore
    public final void e0(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public final void f0(boolean z2) {
        this.n = z2;
    }

    @DexIgnore
    public final void g0(List<ContactGroup> list) {
        Wg6.c(list, "<set-?>");
        this.p = list;
    }

    @DexIgnore
    public void h0() {
        this.t.M5(this);
    }

    @DexIgnore
    public final void i0() {
        FLogger.INSTANCE.getLocal().d(D, "unregisterBroadcastReceiver");
        BleCommandResultManager.d.j(this.h, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(D, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        b0();
        BleCommandResultManager.d.g(CommunicateMode.SET_NOTIFICATION_FILTERS);
        Y06 y06 = this.t;
        if (y06 != null) {
            FragmentActivity activity = ((Z06) y06).getActivity();
            R16 r16 = this.s;
            if (r16 != null) {
                r16.a().h((LifecycleOwner) this.t, new Gi(this));
                if (!this.t.O5()) {
                    Jn5 jn5 = Jn5.b;
                    if (activity == null) {
                        Wg6.i();
                        throw null;
                    } else if (Jn5.c(jn5, activity, Jn5.Ai.NOTIFICATION_GET_CONTACTS, false, true, false, null, 52, null)) {
                        Rm6 unused = Gu7.d(k(), null, null, new Hi(this, null), 3, null);
                    }
                }
                Y06 y062 = this.t;
                y062.F(!Jn5.c(Jn5.b, ((Z06) y062).getContext(), Jn5.Ai.QUICK_RESPONSE, false, false, false, null, 56, null));
                return;
            }
            Wg6.n("mNotificationSettingViewModel");
            throw null;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        i0();
        LiveData<List<NotificationSettingsModel>> liveData = this.q;
        Y06 y06 = this.t;
        if (y06 != null) {
            liveData.n((Z06) y06);
            R16 r16 = this.s;
            if (r16 != null) {
                r16.a().n((LifecycleOwner) this.t);
                FLogger.INSTANCE.getLocal().d(D, "stop");
                return;
            }
            Wg6.n("mNotificationSettingViewModel");
            throw null;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
    }

    @DexIgnore
    @Override // com.fossil.X06
    public void n() {
        Jn5 jn5 = Jn5.b;
        Y06 y06 = this.t;
        if (y06 != null) {
            Jn5.c(jn5, ((Z06) y06).getContext(), Jn5.Ai.QUICK_RESPONSE, false, false, false, null, 60, null);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
    }

    @DexIgnore
    @Override // com.fossil.X06
    public boolean o() {
        Boolean r0 = this.y.r0();
        Wg6.b(r0, "mSharedPreferencesManager.isQuickResponseEnabled");
        return r0.booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.X06
    public void p() {
        this.t.a();
        this.t.close();
    }

    @DexIgnore
    @Override // com.fossil.X06
    public void q(ContactGroup contactGroup) {
        Wg6.c(contactGroup, "contactGroup");
        this.u.a(this.w, new F26.Ai(contactGroup), new Ci(this, contactGroup));
    }

    @DexIgnore
    @Override // com.fossil.X06
    public void r(boolean z2) {
        this.y.K1(Boolean.valueOf(z2));
    }

    @DexIgnore
    @Override // com.fossil.X06
    public void s() {
        this.y.Z0(PortfolioApp.get.instance().J(), 0, false);
    }

    @DexIgnore
    @Override // com.fossil.X06
    public void t(R16 r16) {
        Wg6.c(r16, "viewModel");
        this.s = r16;
    }

    @DexIgnore
    @Override // com.fossil.X06
    public void u(boolean z2) {
        if (z2) {
            Jn5 jn5 = Jn5.b;
            Y06 y06 = this.t;
            if (y06 == null) {
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
            } else if (!Jn5.c(jn5, ((Z06) y06).getContext(), Jn5.Ai.QUICK_RESPONSE, false, false, true, 112, 12, null)) {
                return;
            }
        }
        Rm6 unused = Gu7.d(k(), null, null, new Ei(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.X06
    public void v(boolean z2) {
        if (!a0()) {
            FLogger.INSTANCE.getLocal().d(D, "setRuleToDevice, nothing changed");
            this.t.close();
            return;
        }
        Jn5 jn5 = Jn5.b;
        Y06 y06 = this.t;
        if (y06 == null) {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
        } else if (!Jn5.c(jn5, ((Z06) y06).getContext(), Jn5.Ai.SET_BLE_COMMAND, false, true, false, null, 52, null)) {
        } else {
            if (!z2 || Jn5.c(Jn5.b, ((Z06) this.t).getContext(), Jn5.Ai.NOTIFICATION_DIANA, false, false, true, null, 44, null)) {
                Rm6 unused = Gu7.d(k(), null, null, new Fi(this, null), 3, null);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.X06
    public void w(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = D;
        local.d(str, "updateNotificationSetting isCall=" + z2);
        Y06 y06 = this.t;
        R16.Ai ai = new R16.Ai(z2 ? y06.L1() : y06.O2(), z2);
        R16 r16 = this.s;
        if (r16 != null) {
            r16.a().l(ai);
            this.t.V4();
            return;
        }
        Wg6.n("mNotificationSettingViewModel");
        throw null;
    }
}
