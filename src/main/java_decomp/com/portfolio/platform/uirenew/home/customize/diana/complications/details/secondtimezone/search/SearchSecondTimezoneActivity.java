package com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.O76;
import com.fossil.V86;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchSecondTimezoneActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public SearchSecondTimezonePresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            Wg6.c(fragment, "fragment");
            Wg6.c(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), SearchSecondTimezoneActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 100);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        String str;
        FLogger.INSTANCE.getLocal().d(r(), "onCreate");
        super.onCreate(bundle);
        setContentView(2131558428);
        O76 o76 = (O76) getSupportFragmentManager().Y(2131362158);
        if (o76 == null) {
            o76 = O76.l.a();
            k(o76, O76.l.b(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (o76 != null) {
            iface.J(new V86(o76)).a(this);
            Intent intent = getIntent();
            if (intent == null || (str = intent.getStringExtra(Constants.USER_SETTING)) == null) {
                str = "";
            }
            SearchSecondTimezonePresenter searchSecondTimezonePresenter = this.A;
            if (searchSecondTimezonePresenter != null) {
                searchSecondTimezonePresenter.q(str);
            } else {
                Wg6.n("mSearchSecondTimezonePresenter");
                throw null;
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneContract.View");
        }
    }
}
