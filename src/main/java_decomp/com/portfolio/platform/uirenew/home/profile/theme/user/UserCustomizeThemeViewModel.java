package com.portfolio.platform.uirenew.home.profile.theme.user;

import androidx.lifecycle.MutableLiveData;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Ts0;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jh6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.UserCustomizeThemeFragment;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserCustomizeThemeViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public MutableLiveData<Ai> a; // = new MutableLiveData<>();
    @DexIgnore
    public Ai b; // = new Ai(null, 1, null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public String a;

        @DexIgnore
        public Ai() {
            this(null, 1, null);
        }

        @DexIgnore
        public Ai(String str) {
            this.a = str;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(String str, int i, Qg6 qg6) {
            this((i & 1) != 0 ? null : str);
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final void b(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = UserCustomizeThemeViewModel.d;
            local.d(str2, "update themeName=" + str);
            this.a = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$updateCurrentThemeName$1", f = "UserCustomizeThemeViewModel.kt", l = {30}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UserCustomizeThemeViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$updateCurrentThemeName$1$name$1", f = "UserCustomizeThemeViewModel.kt", l = {31}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super String>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super String> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object themeById;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    this.L$0 = this.p$;
                    this.label = 1;
                    themeById = this.this$0.this$0.c.getThemeById(this.this$0.$id.element, this);
                    if (themeById == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il6 = (Il6) this.L$0;
                    El7.b(obj);
                    themeById = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (themeById != null) {
                    return ((Theme) themeById).getName();
                }
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(UserCustomizeThemeViewModel userCustomizeThemeViewModel, Jh6 jh6, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = userCustomizeThemeViewModel;
            this.$id = jh6;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$id, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.b((String) g);
            this.this$0.e();
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$updateNewName$1", f = "UserCustomizeThemeViewModel.kt", l = {42}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $name;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UserCustomizeThemeViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$updateNewName$1$1", f = "UserCustomizeThemeViewModel.kt", l = {43, 45}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:10:0x002f  */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x0070  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 2
                    r4 = 1
                    java.lang.Object r2 = com.fossil.Yn7.d()
                    int r0 = r6.label
                    if (r0 == 0) goto L_0x004f
                    if (r0 == r4) goto L_0x0024
                    if (r0 != r5) goto L_0x001c
                    java.lang.Object r0 = r6.L$1
                    com.portfolio.platform.data.model.Theme r0 = (com.portfolio.platform.data.model.Theme) r0
                    java.lang.Object r0 = r6.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r7)
                L_0x0019:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x001b:
                    return r0
                L_0x001c:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0024:
                    java.lang.Object r0 = r6.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r7)
                    r3 = r0
                    r1 = r7
                L_0x002d:
                    if (r1 == 0) goto L_0x0070
                    r0 = r1
                    com.portfolio.platform.data.model.Theme r0 = (com.portfolio.platform.data.model.Theme) r0
                    com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$Ci r1 = r6.this$0
                    java.lang.String r1 = r1.$name
                    r0.setName(r1)
                    com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$Ci r1 = r6.this$0
                    com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel r1 = r1.this$0
                    com.portfolio.platform.data.source.ThemeRepository r1 = com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel.b(r1)
                    r6.L$0 = r3
                    r6.L$1 = r0
                    r6.label = r5
                    java.lang.Object r0 = r1.upsertUserTheme(r0, r6)
                    if (r0 != r2) goto L_0x0019
                    r0 = r2
                    goto L_0x001b
                L_0x004f:
                    com.fossil.El7.b(r7)
                    com.mapped.Il6 r3 = r6.p$
                    com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$Ci r0 = r6.this$0
                    com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel r0 = r0.this$0
                    com.portfolio.platform.data.source.ThemeRepository r1 = com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel.b(r0)
                    com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$Ci r0 = r6.this$0
                    com.mapped.Jh6 r0 = r0.$id
                    T r0 = r0.element
                    java.lang.String r0 = (java.lang.String) r0
                    r6.L$0 = r3
                    r6.label = r4
                    java.lang.Object r1 = r1.getThemeById(r0, r6)
                    if (r1 != r2) goto L_0x002d
                    r0 = r2
                    goto L_0x001b
                L_0x0070:
                    com.mapped.Wg6.i()
                    r0 = 0
                    throw r0
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel.Ci.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(UserCustomizeThemeViewModel userCustomizeThemeViewModel, Jh6 jh6, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = userCustomizeThemeViewModel;
            this.$id = jh6;
            this.$name = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$id, this.$name, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.b(this.$name);
            this.this$0.e();
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = UserCustomizeThemeViewModel.class.getSimpleName();
        Wg6.b(simpleName, "UserCustomizeThemeViewModel::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public UserCustomizeThemeViewModel(ThemeRepository themeRepository) {
        Wg6.c(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void e() {
        this.a.l(this.b);
    }

    @DexIgnore
    public final MutableLiveData<Ai> f() {
        return this.a;
    }

    @DexIgnore
    public final void g() {
        h();
    }

    @DexIgnore
    public final void h() {
        Jh6 jh6 = new Jh6();
        jh6.element = (T) UserCustomizeThemeFragment.m.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        local.d(str, "updateCurrentThemeName id=" + ((String) jh6.element));
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Bi(this, jh6, null), 3, null);
    }

    @DexIgnore
    public final void i(String str) {
        Wg6.c(str, "name");
        Jh6 jh6 = new Jh6();
        jh6.element = (T) UserCustomizeThemeFragment.m.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = d;
        local.d(str2, "updateNewName id=" + ((String) jh6.element) + " name=" + str);
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ci(this, jh6, str, null), 3, null);
    }
}
