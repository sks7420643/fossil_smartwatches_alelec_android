package com.portfolio.platform.uirenew.home.details.workout;

import androidx.lifecycle.MutableLiveData;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gi5;
import com.fossil.Gl7;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Ii5;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Mi5;
import com.fossil.Oi5;
import com.fossil.Pm7;
import com.fossil.Ts0;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.diana.workout.WorkoutSessionUpdateWrapper;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutEditViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public WorkoutSession a;
    @DexIgnore
    public WorkoutSession b;
    @DexIgnore
    public MutableLiveData<Ai> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ WorkoutSessionRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public List<Oi5> a;
        @DexIgnore
        public WorkoutSession b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public Lc6<Integer, String> d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public Ai(List<Oi5> list, WorkoutSession workoutSession, Boolean bool, Lc6<Integer, String> lc6, Boolean bool2, boolean z) {
            this.a = list;
            this.b = workoutSession;
            this.c = bool;
            this.d = lc6;
            this.e = bool2;
            this.f = z;
        }

        @DexIgnore
        public final boolean a() {
            return this.f;
        }

        @DexIgnore
        public final Lc6<Integer, String> b() {
            return this.d;
        }

        @DexIgnore
        public final Boolean c() {
            return this.e;
        }

        @DexIgnore
        public final WorkoutSession d() {
            return this.b;
        }

        @DexIgnore
        public final List<Oi5> e() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!Wg6.a(this.a, ai.a) || !Wg6.a(this.b, ai.b) || !Wg6.a(this.c, ai.c) || !Wg6.a(this.d, ai.d) || !Wg6.a(this.e, ai.e) || this.f != ai.f) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final Boolean f() {
            return this.c;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            List<Oi5> list = this.a;
            int hashCode = list != null ? list.hashCode() : 0;
            WorkoutSession workoutSession = this.b;
            int hashCode2 = workoutSession != null ? workoutSession.hashCode() : 0;
            Boolean bool = this.c;
            int hashCode3 = bool != null ? bool.hashCode() : 0;
            Lc6<Integer, String> lc6 = this.d;
            int hashCode4 = lc6 != null ? lc6.hashCode() : 0;
            Boolean bool2 = this.e;
            if (bool2 != null) {
                i = bool2.hashCode();
            }
            boolean z = this.f;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            return (((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i) * 31) + i2;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(workoutWrapperTypes=" + this.a + ", workoutSession=" + this.b + ", isWorkoutSessionChanged=" + this.c + ", showServerError=" + this.d + ", showSuccess=" + this.e + ", showLoading=" + this.f + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.workout.WorkoutEditViewModel$start$1", f = "WorkoutEditViewModel.kt", l = {40}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $workoutSessionId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutEditViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.workout.WorkoutEditViewModel$start$1$2", f = "WorkoutEditViewModel.kt", l = {41}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object workoutSessionById;
                WorkoutEditViewModel workoutEditViewModel;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WorkoutEditViewModel workoutEditViewModel2 = this.this$0.this$0;
                    WorkoutSessionRepository workoutSessionRepository = workoutEditViewModel2.d;
                    String str = this.this$0.$workoutSessionId;
                    this.L$0 = il6;
                    this.L$1 = workoutEditViewModel2;
                    this.label = 1;
                    workoutSessionById = workoutSessionRepository.getWorkoutSessionById(str, this);
                    if (workoutSessionById == d) {
                        return d;
                    }
                    workoutEditViewModel = workoutEditViewModel2;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    workoutSessionById = obj;
                    workoutEditViewModel = (WorkoutEditViewModel) this.L$1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                workoutEditViewModel.a = (WorkoutSession) workoutSessionById;
                WorkoutEditViewModel workoutEditViewModel3 = this.this$0.this$0;
                WorkoutSession workoutSession = workoutEditViewModel3.a;
                workoutEditViewModel3.b = workoutSession != null ? WorkoutSession.copy$default(workoutSession, null, null, null, null, null, null, null, null, null, null, null, 0, 0, 0, 0, null, null, null, null, null, null, null, null, null, 16777215, null) : null;
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(WorkoutEditViewModel workoutEditViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = workoutEditViewModel;
            this.$workoutSessionId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$workoutSessionId, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                WorkoutEditViewModel workoutEditViewModel = this.this$0;
                Oi5[] values = Oi5.values();
                ArrayList arrayList = new ArrayList();
                int length = values.length;
                for (int i2 = 0; i2 < length; i2++) {
                    Oi5 oi5 = values[i2];
                    if (Ao7.a(oi5 != Oi5.UNKNOWN).booleanValue()) {
                        arrayList.add(oi5);
                    }
                }
                WorkoutEditViewModel.g(workoutEditViewModel, Pm7.j0(arrayList), null, null, null, null, true, 30, null);
                Dv7 a2 = Bw7.a();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(a2, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            WorkoutEditViewModel workoutEditViewModel2 = this.this$0;
            WorkoutEditViewModel.g(workoutEditViewModel2, null, workoutEditViewModel2.b, null, null, null, false, 29, null);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutEditViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object updateWorkoutSession;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WorkoutSession workoutSession = this.this$0.this$0.b;
                    if (workoutSession != null) {
                        WorkoutSessionUpdateWrapper workoutSessionUpdateWrapper = new WorkoutSessionUpdateWrapper(workoutSession);
                        WorkoutSessionRepository workoutSessionRepository = this.this$0.this$0.d;
                        List<WorkoutSessionUpdateWrapper> j0 = Pm7.j0(Hm7.c(workoutSessionUpdateWrapper));
                        this.L$0 = il6;
                        this.L$1 = workoutSessionUpdateWrapper;
                        this.label = 1;
                        updateWorkoutSession = workoutSessionRepository.updateWorkoutSession(j0, this);
                        if (updateWorkoutSession == d) {
                            return d;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else if (i == 1) {
                    WorkoutSessionUpdateWrapper workoutSessionUpdateWrapper2 = (WorkoutSessionUpdateWrapper) this.L$1;
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    updateWorkoutSession = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                Kz4 kz4 = (Kz4) updateWorkoutSession;
                if (kz4.c() != null) {
                    WorkoutEditViewModel.g(this.this$0.this$0, null, null, null, null, Ao7.a(true), false, 15, null);
                } else {
                    ServerError a2 = kz4.a();
                    if (a2 != null) {
                        WorkoutEditViewModel.g(this.this$0.this$0, null, null, null, new Lc6(a2.getCode(), ""), Ao7.a(false), false, 7, null);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Xe6 xe6, WorkoutEditViewModel workoutEditViewModel) {
            super(2, xe6);
            this.this$0 = workoutEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(xe6, this.this$0);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = WorkoutEditViewModel.class.getSimpleName();
        Wg6.b(simpleName, "WorkoutEditViewModel::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public WorkoutEditViewModel(WorkoutSessionRepository workoutSessionRepository) {
        Wg6.c(workoutSessionRepository, "mWorkoutSessionRepository");
        this.d = workoutSessionRepository;
    }

    @DexIgnore
    public static /* synthetic */ void g(WorkoutEditViewModel workoutEditViewModel, List list, WorkoutSession workoutSession, Boolean bool, Lc6 lc6, Boolean bool2, boolean z, int i, Object obj) {
        Boolean bool3 = null;
        List list2 = (i & 1) != 0 ? null : list;
        WorkoutSession workoutSession2 = (i & 2) != 0 ? null : workoutSession;
        Boolean bool4 = (i & 4) != 0 ? null : bool;
        Lc6 lc62 = (i & 8) != 0 ? null : lc6;
        if ((i & 16) == 0) {
            bool3 = bool2;
        }
        workoutEditViewModel.f(list2, workoutSession2, bool4, lc62, bool3, (i & 32) != 0 ? false : z);
    }

    @DexIgnore
    public final void f(List<Oi5> list, WorkoutSession workoutSession, Boolean bool, Lc6<Integer, String> lc6, Boolean bool2, boolean z) {
        this.c.l(new Ai(list, workoutSession, bool, lc6, bool2, z));
    }

    @DexIgnore
    public final Lc6<Ii5, Ii5> h() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession == null) {
            return null;
        }
        DateTime editedEndTime = workoutSession.getEditedEndTime();
        if (editedEndTime != null) {
            long millis = editedEndTime.getMillis();
            DateTime editedStartTime = workoutSession.getEditedStartTime();
            if (editedStartTime != null) {
                Gl7<Integer, Integer, Integer> e0 = TimeUtils.e0((int) ((millis - editedStartTime.getMillis()) / ((long) 1000)));
                Wg6.b(e0, "DateHelper.getTimeValues(duration.toInt())");
                Integer first = e0.getFirst();
                Wg6.b(first, "timeValues.first");
                Ii5 ii5 = new Ii5(0, 23, first.intValue(), 1.0f);
                Integer second = e0.getSecond();
                Wg6.b(second, "timeValues.second");
                return new Lc6<>(ii5, new Ii5(0, 59, second.intValue(), 1.0f));
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Lc6<Ii5, Ii5> i() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession == null) {
            return null;
        }
        Calendar instance = Calendar.getInstance();
        TimeZone timeZone = TimeZone.getDefault();
        Wg6.b(timeZone, "timezone");
        timeZone.setRawOffset(workoutSession.getTimezoneOffsetInSecond() * 1000);
        Wg6.b(instance, "calendar");
        instance.setTimeZone(timeZone);
        DateTime editedStartTime = workoutSession.getEditedStartTime();
        if (editedStartTime != null) {
            instance.setTimeInMillis(editedStartTime.getMillis());
            return new Lc6<>(new Ii5(0, 23, instance.get(11), 1.0f), new Ii5(0, 59, instance.get(12), 1.0f));
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final MutableLiveData<Ai> j() {
        return this.c;
    }

    @DexIgnore
    public final Long k() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            return Long.valueOf(workoutSession.getDate().getTime());
        }
        return null;
    }

    @DexIgnore
    public final Long l() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            return Long.valueOf(workoutSession.getEditedEndTime().getMillis() - workoutSession.getEditedStartTime().getMillis());
        }
        return null;
    }

    @DexIgnore
    public final Long m() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            return Long.valueOf(workoutSession.getEditedStartTime().getMillis());
        }
        return null;
    }

    @DexIgnore
    public final boolean n() {
        Long l = null;
        WorkoutSession workoutSession = this.a;
        if (workoutSession == null || this.b == null) {
            return false;
        }
        if (workoutSession != null) {
            Mi5 editedType = workoutSession.getEditedType();
            Integer valueOf = editedType != null ? Integer.valueOf(editedType.ordinal()) : null;
            WorkoutSession workoutSession2 = this.b;
            if (workoutSession2 != null) {
                Mi5 editedType2 = workoutSession2.getEditedType();
                if (!(!Wg6.a(valueOf, editedType2 != null ? Integer.valueOf(editedType2.ordinal()) : null))) {
                    WorkoutSession workoutSession3 = this.a;
                    if (workoutSession3 != null) {
                        Gi5 editedMode = workoutSession3.getEditedMode();
                        Integer valueOf2 = editedMode != null ? Integer.valueOf(editedMode.ordinal()) : null;
                        WorkoutSession workoutSession4 = this.b;
                        if (workoutSession4 != null) {
                            Gi5 editedMode2 = workoutSession4.getEditedMode();
                            if (!(!Wg6.a(valueOf2, editedMode2 != null ? Integer.valueOf(editedMode2.ordinal()) : null))) {
                                WorkoutSession workoutSession5 = this.a;
                                if (workoutSession5 != null) {
                                    DateTime editedStartTime = workoutSession5.getEditedStartTime();
                                    if (editedStartTime != null) {
                                        long millis = editedStartTime.getMillis();
                                        WorkoutSession workoutSession6 = this.b;
                                        if (workoutSession6 != null) {
                                            DateTime editedStartTime2 = workoutSession6.getEditedStartTime();
                                            if (millis == (editedStartTime2 != null ? Long.valueOf(editedStartTime2.getMillis()) : null).longValue()) {
                                                WorkoutSession workoutSession7 = this.a;
                                                if (workoutSession7 != null) {
                                                    DateTime editedEndTime = workoutSession7.getEditedEndTime();
                                                    if (editedEndTime != null) {
                                                        long millis2 = editedEndTime.getMillis();
                                                        WorkoutSession workoutSession8 = this.b;
                                                        if (workoutSession8 != null) {
                                                            DateTime editedEndTime2 = workoutSession8.getEditedEndTime();
                                                            if (editedEndTime2 != null) {
                                                                l = Long.valueOf(editedEndTime2.getMillis());
                                                            }
                                                            if (millis2 == l.longValue()) {
                                                                return false;
                                                            }
                                                        } else {
                                                            Wg6.i();
                                                            throw null;
                                                        }
                                                    }
                                                } else {
                                                    Wg6.i();
                                                    throw null;
                                                }
                                            }
                                        } else {
                                            Wg6.i();
                                            throw null;
                                        }
                                    }
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                return true;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void o(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onCaloriesChanged calories=" + i);
        WorkoutSession workoutSession = this.b;
    }

    @DexIgnore
    public final void p(double d2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onDistanceChanged distance=" + d2);
        WorkoutSession workoutSession = this.b;
    }

    @DexIgnore
    public final void q(int i, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onDurationChanged hour=" + i + " min=" + i2);
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            Calendar instance = Calendar.getInstance();
            TimeZone timeZone = TimeZone.getDefault();
            Wg6.b(timeZone, "timezone");
            timeZone.setRawOffset(workoutSession.getTimezoneOffsetInSecond() * 1000);
            Wg6.b(instance, "calendar");
            instance.setTimeZone(timeZone);
            DateTime editedStartTime = workoutSession.getEditedStartTime();
            if (editedStartTime != null) {
                instance.setTimeInMillis(editedStartTime.getMillis());
                instance.add(12, (i * 60) + i2);
                workoutSession.setEditedEndTime(new DateTime(instance.getTimeInMillis()));
                g(this, null, workoutSession, Boolean.valueOf(n()), null, null, false, 57, null);
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void r(int i, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onStartTimeChanged hour=" + i + " min=" + i2);
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            DateTime editedEndTime = workoutSession.getEditedEndTime();
            if (editedEndTime != null) {
                long millis = editedEndTime.getMillis();
                DateTime editedStartTime = workoutSession.getEditedStartTime();
                if (editedStartTime != null) {
                    long millis2 = (millis - editedStartTime.getMillis()) / ((long) 1000);
                    Calendar instance = Calendar.getInstance();
                    TimeZone timeZone = TimeZone.getDefault();
                    Wg6.b(timeZone, "timezone");
                    timeZone.setRawOffset(workoutSession.getTimezoneOffsetInSecond() * 1000);
                    Wg6.b(instance, "calendar");
                    instance.setTimeZone(timeZone);
                    DateTime editedStartTime2 = workoutSession.getEditedStartTime();
                    if (editedStartTime2 != null) {
                        instance.setTimeInMillis(editedStartTime2.getMillis());
                        instance.set(11, i);
                        instance.set(12, i2);
                        workoutSession.setEditedStartTime(new DateTime(instance.getTimeInMillis()));
                        instance.add(13, (int) millis2);
                        workoutSession.setEditedEndTime(new DateTime(instance.getTimeInMillis()));
                        g(this, null, this.b, Boolean.valueOf(n()), null, null, false, 57, null);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void s(Oi5 oi5) {
        Wg6.c(oi5, "workoutWrapperTypes");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onWorkoutTypeChanged workoutTypes=" + oi5);
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            workoutSession.setEditedType(Oi5.Companion.c(oi5));
            workoutSession.setEditedMode(Oi5.Companion.b(oi5));
            if (workoutSession.getEditedMode() == null) {
                Gi5 mode = workoutSession.getMode();
                if (mode == null) {
                    mode = Gi5.INDOOR;
                }
                workoutSession.setEditedMode(mode);
            }
            g(this, null, this.b, Boolean.valueOf(n()), null, null, false, 57, null);
        }
    }

    @DexIgnore
    public final void t(String str) {
        Wg6.c(str, "workoutSessionId");
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Bi(this, str, null), 3, null);
    }

    @DexIgnore
    public final void u() {
        if (this.b != null) {
            g(this, null, null, null, null, null, true, 31, null);
            Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ci(null, this), 3, null);
            g(this, null, null, null, null, null, true, 31, null);
        }
    }
}
