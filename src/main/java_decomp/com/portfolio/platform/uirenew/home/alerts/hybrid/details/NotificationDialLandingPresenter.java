package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.os.Bundle;
import android.util.SparseArray;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.At0;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.R46;
import com.fossil.S46;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationDialLandingPresenter extends R46 implements LoaderManager.a<SparseArray<List<? extends BaseFeatureModel>>> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ S46 e;
    @DexIgnore
    public /* final */ NotificationsLoader f;
    @DexIgnore
    public /* final */ LoaderManager g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingPresenter$start$1", f = "NotificationDialLandingPresenter.kt", l = {33}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationDialLandingPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingPresenter$start$1$1", f = "NotificationDialLandingPresenter.kt", l = {34}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    PortfolioApp instance = PortfolioApp.get.instance();
                    this.L$0 = il6;
                    this.label = 1;
                    if (instance.V0(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(NotificationDialLandingPresenter notificationDialLandingPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationDialLandingPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 h = this.this$0.h();
                Aii aii = new Aii(null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(h, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = NotificationDialLandingPresenter.class.getSimpleName();
        Wg6.b(simpleName, "NotificationDialLandingP\u2026er::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public NotificationDialLandingPresenter(S46 s46, NotificationsLoader notificationsLoader, LoaderManager loaderManager) {
        Wg6.c(s46, "mView");
        Wg6.c(notificationsLoader, "mNotificationLoader");
        Wg6.c(loaderManager, "mLoaderManager");
        this.e = s46;
        this.f = notificationsLoader;
        this.g = loaderManager;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.At0, java.lang.Object] */
    @Override // androidx.loader.app.LoaderManager.a
    public /* bridge */ /* synthetic */ void a(At0<SparseArray<List<? extends BaseFeatureModel>>> at0, SparseArray<List<? extends BaseFeatureModel>> sparseArray) {
        o(at0, sparseArray);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.At0<android.util.SparseArray<java.util.List<com.fossil.wearables.fsl.shared.BaseFeatureModel>>>' to match base method */
    @Override // androidx.loader.app.LoaderManager.a
    public At0<SparseArray<List<? extends BaseFeatureModel>>> d(int i, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "onCreateLoader id=" + i);
        return this.f;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.At0<android.util.SparseArray<java.util.List<com.fossil.wearables.fsl.shared.BaseFeatureModel>>>] */
    @Override // androidx.loader.app.LoaderManager.a
    public void g(At0<SparseArray<List<? extends BaseFeatureModel>>> at0) {
        Wg6.c(at0, "loader");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(h, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!PortfolioApp.get.instance().k0().s0()) {
            Rm6 unused = Gu7.d(k(), null, null, new Ai(this, null), 3, null);
        }
        this.g.d(3, null, this);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        this.g.a(3);
        FLogger.INSTANCE.getLocal().d(h, "stop");
    }

    @DexIgnore
    public void o(At0<SparseArray<List<BaseFeatureModel>>> at0, SparseArray<List<BaseFeatureModel>> sparseArray) {
        Wg6.c(at0, "loader");
        if (sparseArray != null) {
            this.e.k5(sparseArray);
        } else {
            FLogger.INSTANCE.getLocal().d(h, "onLoadFinished, data=null");
        }
    }

    @DexIgnore
    public void p() {
        this.e.M5(this);
    }
}
