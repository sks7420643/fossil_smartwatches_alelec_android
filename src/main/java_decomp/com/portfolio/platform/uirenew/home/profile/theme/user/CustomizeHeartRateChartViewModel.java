package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.graphics.Color;
import androidx.lifecycle.MutableLiveData;
import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Qs6;
import com.fossil.Ts0;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeHeartRateChartViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e; // = "#bdbdbd";
    @DexIgnore
    public MutableLiveData<Ai> a; // = new MutableLiveData<>();
    @DexIgnore
    public Ai b; // = new Ai(null, null, null, null, 15, null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Integer d;

        @DexIgnore
        public Ai() {
            this(null, null, null, null, 15, null);
        }

        @DexIgnore
        public Ai(Integer num, Integer num2, Integer num3, Integer num4) {
            this.a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(Integer num, Integer num2, Integer num3, Integer num4, int i, Qg6 qg6) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2, (i & 4) != 0 ? null : num3, (i & 8) != 0 ? null : num4);
        }

        @DexIgnore
        public static /* synthetic */ void f(Ai ai, Integer num, Integer num2, Integer num3, Integer num4, int i, Object obj) {
            if ((i & 1) != 0) {
                num = null;
            }
            if ((i & 2) != 0) {
                num2 = null;
            }
            if ((i & 4) != 0) {
                num3 = null;
            }
            if ((i & 8) != 0) {
                num4 = null;
            }
            ai.e(num, num2, num3, num4);
        }

        @DexIgnore
        public final Integer a() {
            return this.b;
        }

        @DexIgnore
        public final Integer b() {
            return this.d;
        }

        @DexIgnore
        public final Integer c() {
            return this.a;
        }

        @DexIgnore
        public final Integer d() {
            return this.c;
        }

        @DexIgnore
        public final void e(Integer num, Integer num2, Integer num3, Integer num4) {
            this.a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeHeartRateChartViewModel$saveColor$1", f = "CustomizeHeartRateChartViewModel.kt", l = {55, 56, 76}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $colorMax;
        @DexIgnore
        public /* final */ /* synthetic */ String $colorMin;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeHeartRateChartViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(CustomizeHeartRateChartViewModel customizeHeartRateChartViewModel, String str, String str2, String str3, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = customizeHeartRateChartViewModel;
            this.$id = str;
            this.$colorMax = str2;
            this.$colorMin = str3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$id, this.$colorMax, this.$colorMin, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0048  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0061  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00ad  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00d6  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00f1  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0158  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x015b  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 351
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeHeartRateChartViewModel.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = CustomizeHeartRateChartViewModel.class.getSimpleName();
        Wg6.b(simpleName, "CustomizeHeartRateChartV\u2026el::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public CustomizeHeartRateChartViewModel(ThemeRepository themeRepository) {
        Wg6.c(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void d() {
        this.a.l(this.b);
    }

    @DexIgnore
    public final MutableLiveData<Ai> e() {
        return this.a;
    }

    @DexIgnore
    public final void f(String str, String str2, String str3) {
        Wg6.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = d;
        local.d(str4, "saveColor colorMax=" + str2 + " colorMin=" + str3);
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Bi(this, str, str2, str3, null), 3, null);
    }

    @DexIgnore
    public final void g() {
        String a2 = Qs6.s.a();
        int parseColor = a2 != null ? Color.parseColor(a2) : Color.parseColor(e);
        String b2 = Qs6.s.b();
        int parseColor2 = b2 != null ? Color.parseColor(b2) : Color.parseColor(e);
        this.b.e(Integer.valueOf(parseColor), Integer.valueOf(parseColor), Integer.valueOf(parseColor2), Integer.valueOf(parseColor2));
        d();
    }

    @DexIgnore
    public final void h(int i, int i2) {
        if (i == 601) {
            Ai.f(this.b, Integer.valueOf(i2), Integer.valueOf(i2), null, null, 12, null);
            d();
        } else if (i == 602) {
            Ai.f(this.b, null, null, Integer.valueOf(i2), Integer.valueOf(i2), 3, null);
            d();
        }
    }
}
