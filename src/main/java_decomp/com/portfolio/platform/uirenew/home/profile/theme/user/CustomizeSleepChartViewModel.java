package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.graphics.Color;
import androidx.lifecycle.MutableLiveData;
import com.fossil.Bw7;
import com.fossil.Ct6;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Ts0;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeSleepChartViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e; // = "#bdbdbd";
    @DexIgnore
    public MutableLiveData<Ai> a; // = new MutableLiveData<>();
    @DexIgnore
    public Ai b; // = new Ai(null, null, null, null, null, null, 63, null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public Integer e;
        @DexIgnore
        public Integer f;

        @DexIgnore
        public Ai() {
            this(null, null, null, null, null, null, 63, null);
        }

        @DexIgnore
        public Ai(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6) {
            this.a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
            this.e = num5;
            this.f = num6;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, int i, Qg6 qg6) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2, (i & 4) != 0 ? null : num3, (i & 8) != 0 ? null : num4, (i & 16) != 0 ? null : num5, (i & 32) == 0 ? num6 : null);
        }

        @DexIgnore
        public static /* synthetic */ void h(Ai ai, Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, int i, Object obj) {
            Integer num7 = null;
            Integer num8 = (i & 1) != 0 ? null : num;
            Integer num9 = (i & 2) != 0 ? null : num2;
            Integer num10 = (i & 4) != 0 ? null : num3;
            Integer num11 = (i & 8) != 0 ? null : num4;
            Integer num12 = (i & 16) != 0 ? null : num5;
            if ((i & 32) == 0) {
                num7 = num6;
            }
            ai.g(num8, num9, num10, num11, num12, num7);
        }

        @DexIgnore
        public final Integer a() {
            return this.b;
        }

        @DexIgnore
        public final Integer b() {
            return this.a;
        }

        @DexIgnore
        public final Integer c() {
            return this.f;
        }

        @DexIgnore
        public final Integer d() {
            return this.e;
        }

        @DexIgnore
        public final Integer e() {
            return this.d;
        }

        @DexIgnore
        public final Integer f() {
            return this.c;
        }

        @DexIgnore
        public final void g(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6) {
            this.a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
            this.e = num5;
            this.f = num6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeSleepChartViewModel$saveColor$1", f = "CustomizeSleepChartViewModel.kt", l = {66, 67, 100}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $awakeColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $deepColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $lightColor;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeSleepChartViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(CustomizeSleepChartViewModel customizeSleepChartViewModel, String str, String str2, String str3, String str4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = customizeSleepChartViewModel;
            this.$id = str;
            this.$awakeColor = str2;
            this.$lightColor = str3;
            this.$deepColor = str4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$id, this.$awakeColor, this.$lightColor, this.$deepColor, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x004c  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x006c  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00b8  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00e1  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00fc  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0136  */
        /* JADX WARNING: Removed duplicated region for block: B:49:0x01a6  */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x01a9  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 429
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeSleepChartViewModel.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = CustomizeSleepChartViewModel.class.getSimpleName();
        Wg6.b(simpleName, "CustomizeSleepChartViewM\u2026el::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public CustomizeSleepChartViewModel(ThemeRepository themeRepository) {
        Wg6.c(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void d() {
        this.a.l(this.b);
    }

    @DexIgnore
    public final MutableLiveData<Ai> e() {
        return this.a;
    }

    @DexIgnore
    public final void f(String str, String str2, String str3, String str4) {
        Wg6.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str5 = d;
        local.d(str5, "saveColor awakeColor=" + str2 + " lightColor=" + str3 + " deepColor=" + str4);
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Bi(this, str, str2, str3, str4, null), 3, null);
    }

    @DexIgnore
    public final void g() {
        String a2 = Ct6.t.a();
        int parseColor = a2 != null ? Color.parseColor(a2) : Color.parseColor(e);
        String c2 = Ct6.t.c();
        int parseColor2 = c2 != null ? Color.parseColor(c2) : Color.parseColor(e);
        String b2 = Ct6.t.b();
        int parseColor3 = b2 != null ? Color.parseColor(b2) : Color.parseColor(e);
        this.b.g(Integer.valueOf(parseColor), Integer.valueOf(parseColor), Integer.valueOf(parseColor2), Integer.valueOf(parseColor2), Integer.valueOf(parseColor3), Integer.valueOf(parseColor3));
        d();
    }

    @DexIgnore
    public final void h(int i, int i2) {
        switch (i) {
            case 701:
                Ai.h(this.b, Integer.valueOf(i2), Integer.valueOf(i2), null, null, null, null, 60, null);
                d();
                return;
            case 702:
                Ai.h(this.b, null, null, Integer.valueOf(i2), Integer.valueOf(i2), null, null, 51, null);
                d();
                return;
            case 703:
                Ai.h(this.b, null, null, null, null, Integer.valueOf(i2), Integer.valueOf(i2), 15, null);
                d();
                return;
            default:
                return;
        }
    }
}
