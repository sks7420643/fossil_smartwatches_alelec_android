package com.portfolio.platform.uirenew.home.details.sleep;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.Vm6;
import com.mapped.Qg6;
import com.mapped.SleepDetailFragment;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDetailActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a(null);
    @DexIgnore
    public SleepDetailPresenter A;
    @DexIgnore
    public Date B; // = new Date();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Date date, Context context) {
            Wg6.c(date, "date");
            Wg6.c(context, "context");
            Intent intent = new Intent(context, SleepDetailActivity.class);
            intent.putExtra("KEY_LONG_TIME", date.getTime());
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        SleepDetailFragment sleepDetailFragment = (SleepDetailFragment) getSupportFragmentManager().Y(2131362158);
        Intent intent = getIntent();
        if (intent != null) {
            this.B = new Date(intent.getLongExtra("KEY_LONG_TIME", new Date().getTime()));
        }
        if (sleepDetailFragment == null) {
            sleepDetailFragment = SleepDetailFragment.D.a(this.B);
            i(sleepDetailFragment, 2131362158);
        }
        PortfolioApp.get.instance().getIface().K(new Vm6(sleepDetailFragment)).a(this);
    }
}
