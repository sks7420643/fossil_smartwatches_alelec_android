package com.portfolio.platform.uirenew.home.customize.diana.watchapps;

import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gl7;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Ol5;
import com.fossil.Oo5;
import com.fossil.Ss0;
import com.fossil.Um5;
import com.fossil.V96;
import com.fossil.W96;
import com.fossil.X96;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.CustomizeConfigurationExt;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppsPresenter extends V96 {
    @DexIgnore
    public WatchAppEditViewModel e;
    @DexIgnore
    public /* final */ MutableLiveData<WatchApp> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<WatchApp>> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Gl7<String, Boolean, Parcelable>> i; // = new MutableLiveData<>();
    @DexIgnore
    public ArrayList<Category> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ LiveData<String> k;
    @DexIgnore
    public /* final */ Ls0<String> l;
    @DexIgnore
    public /* final */ LiveData<List<WatchApp>> m;
    @DexIgnore
    public /* final */ Ls0<List<WatchApp>> n;
    @DexIgnore
    public /* final */ Ls0<Gl7<String, Boolean, Parcelable>> o;
    @DexIgnore
    public /* final */ W96 p;
    @DexIgnore
    public /* final */ CategoryRepository q;
    @DexIgnore
    public /* final */ An4 r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1", f = "WatchAppsPresenter.kt", l = {237}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1$1", f = "WatchAppsPresenter.kt", l = {237}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Parcelable>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Parcelable> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WatchAppEditViewModel D = WatchAppsPresenter.D(this.this$0.this$0);
                    String str = this.this$0.$id;
                    this.L$0 = il6;
                    this.label = 1;
                    Object w = D.w(str, this);
                    return w == d ? d : w;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(WatchAppsPresenter watchAppsPresenter, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchAppsPresenter;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, this.$id, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            boolean g;
            Object g2;
            Parcelable parcelable = null;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                g = Ol5.c.g(this.$id);
                if (g) {
                    Dv7 h = this.this$0.h();
                    Aii aii = new Aii(this, null);
                    this.L$0 = il6;
                    this.L$1 = null;
                    this.Z$0 = g;
                    this.label = 1;
                    g2 = Eu7.g(h, aii, this);
                    if (g2 == d) {
                        return d;
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppsPresenter", "checkSettingOfSelectedWatchApp id=" + this.$id + " settings=" + parcelable);
                this.this$0.i.l(new Gl7(this.$id, Ao7.a(g), parcelable));
                return Cd6.a;
            } else if (i == 1) {
                g = this.Z$0;
                Parcelable parcelable2 = (Parcelable) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g2 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            parcelable = (Parcelable) g2;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("WatchAppsPresenter", "checkSettingOfSelectedWatchApp id=" + this.$id + " settings=" + parcelable);
            this.this$0.i.l(new Gl7(this.$id, Ao7.a(g), parcelable));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $watchappId;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(String str, Xe6 xe6, WatchAppsPresenter watchAppsPresenter) {
            super(2, xe6);
            this.$watchappId = str;
            this.this$0 = watchAppsPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.$watchappId, xe6, this.this$0);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            T t;
            String setting;
            String str;
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                Iterator<T> it = WatchAppsPresenter.D(this.this$0).x().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (Ao7.a(Wg6.a(next.getAppId(), this.$watchappId)).booleanValue()) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                String str2 = this.$watchappId;
                int hashCode = str2.hashCode();
                String str3 = "";
                if (hashCode != -829740640) {
                    if (hashCode == 1223440372 && str2.equals("weather")) {
                        W96 w96 = this.this$0.p;
                        if (t2 == null || (str = t2.getSetting()) == null) {
                            str = str3;
                        }
                        w96.d6(str);
                    }
                } else if (str2.equals("commute-time")) {
                    W96 w962 = this.this$0.p;
                    if (!(t2 == null || (setting = t2.getSetting()) == null)) {
                        str3 = setting;
                    }
                    w962.f0(str3);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public Ci(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "onLiveDataChanged category=" + str);
            if (str != null) {
                this.a.p.u0(str);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mCategoryOfSelectedWatchAppTransformation$1$3$1", f = "WatchAppsPresenter.kt", l = {81}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ WatchAppsPresenter $this_run;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mCategoryOfSelectedWatchAppTransformation$1$3$1$allCategory$1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Category>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super List<? extends Category>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        return this.this$0.$this_run.q.getAllCategories();
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(WatchAppsPresenter watchAppsPresenter, Xe6 xe6) {
                super(2, xe6);
                this.$this_run = watchAppsPresenter;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$this_run, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Dv7 b = Bw7.b();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    g = Eu7.g(b, aiii, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) g;
                if (!list.isEmpty()) {
                    this.$this_run.g.l(((Category) list.get(0)).getId());
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Di(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        public final MutableLiveData<String> a(WatchApp watchApp) {
            if (watchApp != null) {
                this.a.G(watchApp.getWatchappId());
            }
            String str = (String) this.a.g.e();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("transform from selected WatchApp to category");
            sb.append(" currentCategory=");
            sb.append(str);
            sb.append(" watchAppCategories=");
            sb.append(watchApp != null ? watchApp.getCategories() : null);
            local.d("WatchAppsPresenter", sb.toString());
            if (watchApp != null) {
                ArrayList<String> categories = watchApp.getCategories();
                if (str == null || !categories.contains(str)) {
                    this.a.g.l(categories.get(0));
                } else {
                    this.a.g.l(str);
                }
            } else {
                WatchAppsPresenter watchAppsPresenter = this.a;
                Rm6 unused = Gu7.d(watchAppsPresenter.k(), null, null, new Aii(watchAppsPresenter, null), 3, null);
            }
            return this.a.g;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((WatchApp) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<Gl7<? extends String, ? extends Boolean, ? extends Parcelable>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public Ei(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        public final void a(Gl7<String, Boolean, ? extends Parcelable> gl7) {
            String str;
            Exception e;
            String c;
            String name;
            if (gl7 != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppsPresenter", "onLiveDataChanged setting of " + gl7.getFirst() + " isSettingRequired " + gl7.getSecond().booleanValue() + " setting " + ((Parcelable) gl7.getThird()) + ' ');
                String str2 = "";
                if (gl7.getSecond().booleanValue()) {
                    Parcelable parcelable = (Parcelable) gl7.getThird();
                    if (parcelable == null) {
                        str = "";
                        str2 = Ol5.c.b(gl7.getFirst());
                    } else {
                        try {
                            String first = gl7.getFirst();
                            int hashCode = first.hashCode();
                            if (hashCode == -829740640) {
                                if (first.equals("commute-time")) {
                                    c = Um5.c(PortfolioApp.get.instance(), 2131886363);
                                    Wg6.b(c, "LanguageHelper.getString\u2026Title__SavedDestinations)");
                                }
                                str = "";
                            } else if (hashCode != 1223440372) {
                                c = "";
                            } else {
                                if (first.equals("weather")) {
                                    List<WeatherLocationWrapper> locations = ((WeatherWatchAppSetting) parcelable).getLocations();
                                    Iterator<WeatherLocationWrapper> it = locations.iterator();
                                    str = "";
                                    while (it.hasNext()) {
                                        try {
                                            WeatherLocationWrapper next = it.next();
                                            StringBuilder sb = new StringBuilder();
                                            sb.append(str);
                                            if (locations.indexOf(next) < Hm7.g(locations)) {
                                                StringBuilder sb2 = new StringBuilder();
                                                sb2.append(next != null ? next.getName() : null);
                                                sb2.append("; ");
                                                name = sb2.toString();
                                            } else {
                                                name = next != null ? next.getName() : null;
                                            }
                                            sb.append(name);
                                            str = sb.toString();
                                        } catch (Exception e2) {
                                            e = e2;
                                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                            local2.d("WatchAppsPresenter", "exception when parse micro app setting " + e);
                                            this.a.p.g0(true, gl7.getFirst(), str2, str);
                                            return;
                                        }
                                    }
                                }
                                str = "";
                            }
                            str = c;
                        } catch (Exception e3) {
                            e = e3;
                            str = "";
                            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                            local22.d("WatchAppsPresenter", "exception when parse micro app setting " + e);
                            this.a.p.g0(true, gl7.getFirst(), str2, str);
                            return;
                        }
                    }
                    this.a.p.g0(true, gl7.getFirst(), str2, str);
                    return;
                }
                this.a.p.g0(false, gl7.getFirst(), "", null);
                WatchApp e4 = WatchAppsPresenter.D(this.a).u().e();
                if (e4 != null) {
                    W96 w96 = this.a.p;
                    String d = Um5.d(PortfolioApp.get.instance(), e4.getDescriptionKey(), e4.getDescription());
                    Wg6.b(d, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    w96.U4(d);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Gl7<? extends String, ? extends Boolean, ? extends Parcelable> gl7) {
            a(gl7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<List<? extends WatchApp>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public Fi(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        public final void a(List<WatchApp> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onLiveDataChanged WatchApps by category value=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("WatchAppsPresenter", sb.toString());
            if (list != null) {
                this.a.p.S5(list);
                WatchApp e = WatchAppsPresenter.D(this.a).u().e();
                if (e != null) {
                    this.a.p.o5(e);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends WatchApp> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public Gi(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        public final MutableLiveData<List<WatchApp>> a(String str) {
            T t;
            boolean z;
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "transform from category to list watchapps with category=" + str);
            WatchAppEditViewModel D = WatchAppsPresenter.D(this.a);
            Wg6.b(str, "category");
            List<WatchApp> n = D.n(str);
            ArrayList arrayList = new ArrayList();
            List<Oo5> e = WatchAppsPresenter.D(this.a).q().e();
            WatchApp e2 = WatchAppsPresenter.D(this.a).u().e();
            String watchappId = e2 != null ? e2.getWatchappId() : null;
            if (e != null) {
                for (WatchApp watchApp : n) {
                    Iterator<T> it = e.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        T t2 = next;
                        if (!Wg6.a(t2.a(), watchApp.getWatchappId()) || !(!Wg6.a(t2.a(), watchappId))) {
                            z = false;
                            continue;
                        } else {
                            z = true;
                            continue;
                        }
                        if (z) {
                            t = next;
                            break;
                        }
                    }
                    if (t == null || Wg6.a(watchApp.getWatchappId(), "empty")) {
                        arrayList.add(watchApp);
                    }
                }
            }
            this.a.h.l(arrayList);
            return this.a.h;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((String) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$start$1", f = "WatchAppsPresenter.kt", l = {194}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$start$1$allCategory$1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Category>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Hi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Hi hi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = hi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends Category>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.q.getAllCategories();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(WatchAppsPresenter watchAppsPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchAppsPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(i2, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ArrayList arrayList = new ArrayList();
            for (Category category : (List) g) {
                if (!WatchAppsPresenter.D(this.this$0).n(category.getId()).isEmpty()) {
                    arrayList.add(category);
                }
            }
            this.this$0.j.clear();
            this.this$0.j.addAll(arrayList);
            this.this$0.p.S(this.this$0.j);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii<T> implements Ls0<WatchApp> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsPresenter a;

        @DexIgnore
        public Ii(WatchAppsPresenter watchAppsPresenter) {
            this.a = watchAppsPresenter;
        }

        @DexIgnore
        public final void a(WatchApp watchApp) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "onLiveDataChanged selectedWatchApp value=" + watchApp);
            this.a.f.l(watchApp);
            if (watchApp != null) {
                this.a.p.c1(Ol5.c.f(watchApp.getWatchappId()));
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(WatchApp watchApp) {
            a(watchApp);
        }
    }

    @DexIgnore
    public WatchAppsPresenter(W96 w96, CategoryRepository categoryRepository, An4 an4) {
        Wg6.c(w96, "mView");
        Wg6.c(categoryRepository, "mCategoryRepository");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.p = w96;
        this.q = categoryRepository;
        this.r = an4;
        new Gson();
        LiveData<String> c = Ss0.c(this.f, new Di(this));
        Wg6.b(c, "Transformations.switchMa\u2026tedWatchAppLiveData\n    }");
        this.k = c;
        this.l = new Ci(this);
        LiveData<List<WatchApp>> c2 = Ss0.c(this.k, new Gi(this));
        Wg6.b(c2, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.m = c2;
        this.n = new Fi(this);
        this.o = new Ei(this);
    }

    @DexIgnore
    public static final /* synthetic */ WatchAppEditViewModel D(WatchAppsPresenter watchAppsPresenter) {
        WatchAppEditViewModel watchAppEditViewModel = watchAppsPresenter.e;
        if (watchAppEditViewModel != null) {
            return watchAppEditViewModel;
        }
        Wg6.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    public final Rm6 G(String str) {
        return Gu7.d(k(), null, null, new Ai(this, str, null), 3, null);
    }

    @DexIgnore
    public void H() {
        this.p.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onStart");
        this.k.i(this.l);
        this.m.i(this.n);
        this.i.i(this.o);
        if (this.j.isEmpty()) {
            Rm6 unused = Gu7.d(k(), null, null, new Hi(this, null), 3, null);
        } else {
            this.p.S(this.j);
        }
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            LiveData<WatchApp> u = watchAppEditViewModel.u();
            W96 w96 = this.p;
            if (w96 != null) {
                u.h((X96) w96, new Ii(this));
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsFragment");
        }
        Wg6.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            LiveData<WatchApp> u = watchAppEditViewModel.u();
            W96 w96 = this.p;
            if (w96 != null) {
                u.n((X96) w96);
                this.h.m(this.n);
                this.g.m(this.l);
                this.i.m(this.o);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsFragment");
        }
        Wg6.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.V96
    public void n() {
        T t;
        T t2;
        T t3;
        String str;
        String a2;
        String a3;
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            List<Oo5> e2 = watchAppEditViewModel.q().e();
            if (e2 != null) {
                Iterator<T> it = e2.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (Wg6.a(next.b(), ViewHierarchy.DIMENSION_TOP_KEY)) {
                        t = next;
                        break;
                    }
                }
                T t4 = t;
                String str2 = (t4 == null || (a3 = t4.a()) == null) ? "empty" : a3;
                Iterator<T> it2 = e2.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next2 = it2.next();
                    if (Wg6.a(next2.b(), "middle")) {
                        t2 = next2;
                        break;
                    }
                }
                T t5 = t2;
                String str3 = (t5 == null || (a2 = t5.a()) == null) ? "empty" : a2;
                Iterator<T> it3 = e2.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        t3 = null;
                        break;
                    }
                    T next3 = it3.next();
                    if (Wg6.a(next3.b(), "bottom")) {
                        t3 = next3;
                        break;
                    }
                }
                T t6 = t3;
                if (t6 == null || (str = t6.a()) == null) {
                    str = "empty";
                }
                this.p.a0(str2, str3, str);
                return;
            }
            this.p.a0("empty", "empty", "empty");
            return;
        }
        Wg6.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.V96
    public void o() {
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            WatchApp e2 = watchAppEditViewModel.u().e();
            if (e2 != null) {
                Rm6 unused = Gu7.d(k(), null, null, new Bi(e2.component1(), null, this), 3, null);
                return;
            }
            return;
        }
        Wg6.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.V96
    public void p() {
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            WatchApp e2 = watchAppEditViewModel.u().e();
            if (e2 != null) {
                String component1 = e2.component1();
                if (Ol5.c.f(component1)) {
                    this.p.i0(component1);
                    return;
                }
                return;
            }
            return;
        }
        Wg6.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.V96
    public void q(Category category) {
        Wg6.c(category, "category");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsPresenter", "category change " + category);
        this.g.l(category.getId());
    }

    @DexIgnore
    @Override // com.fossil.V96
    public void r(String str) {
        T t;
        Object obj;
        Wg6.c(str, "watchappId");
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            WatchApp o2 = watchAppEditViewModel.o(str);
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onUserChooseWatchApp " + o2);
            if (o2 != null) {
                WatchAppEditViewModel watchAppEditViewModel2 = this.e;
                if (watchAppEditViewModel2 != null) {
                    List<Oo5> e2 = watchAppEditViewModel2.q().e();
                    if (e2 != null) {
                        Wg6.b(e2, "presetButtons");
                        ArrayList<Oo5> a2 = CustomizeConfigurationExt.a(e2);
                        ArrayList arrayList = new ArrayList();
                        WatchAppEditViewModel watchAppEditViewModel3 = this.e;
                        if (watchAppEditViewModel3 != null) {
                            String e3 = watchAppEditViewModel3.v().e();
                            if (e3 != null) {
                                Wg6.b(e3, "mWatchAppEditViewModel.g\u2026ctedWatchAppPos().value!!");
                                String str2 = e3;
                                WatchAppEditViewModel watchAppEditViewModel4 = this.e;
                                if (watchAppEditViewModel4 != null) {
                                    if (!watchAppEditViewModel4.D(str)) {
                                        Iterator<Oo5> it = a2.iterator();
                                        while (it.hasNext()) {
                                            Oo5 next = it.next();
                                            if (Wg6.a(next.b(), str2)) {
                                                arrayList.add(new Oo5(str, str2, next.c()));
                                            } else {
                                                arrayList.add(next);
                                            }
                                        }
                                        Iterator it2 = arrayList.iterator();
                                        while (true) {
                                            if (!it2.hasNext()) {
                                                obj = null;
                                                break;
                                            }
                                            obj = it2.next();
                                            if (Wg6.a(((Oo5) obj).b(), str2)) {
                                                break;
                                            }
                                        }
                                        if (obj == null) {
                                            SimpleDateFormat simpleDateFormat = TimeUtils.n.get();
                                            if (simpleDateFormat != null) {
                                                String format = simpleDateFormat.format(new Date());
                                                Wg6.b(format, "DateHelper.LOCAL_DATE_SP\u2026AT.get()!!.format(Date())");
                                                arrayList.add(new Oo5(str2, str, format));
                                            } else {
                                                Wg6.i();
                                                throw null;
                                            }
                                        }
                                        a2.clear();
                                        a2.addAll(arrayList);
                                        WatchAppEditViewModel watchAppEditViewModel5 = this.e;
                                        if (watchAppEditViewModel5 != null) {
                                            watchAppEditViewModel5.G(a2);
                                        } else {
                                            Wg6.n("mWatchAppEditViewModel");
                                            throw null;
                                        }
                                    } else {
                                        Iterator<T> it3 = e2.iterator();
                                        while (true) {
                                            if (!it3.hasNext()) {
                                                t = null;
                                                break;
                                            }
                                            T next2 = it3.next();
                                            if (Wg6.a(next2.a(), str)) {
                                                t = next2;
                                                break;
                                            }
                                        }
                                        T t2 = t;
                                        if (t2 != null) {
                                            WatchAppEditViewModel watchAppEditViewModel6 = this.e;
                                            if (watchAppEditViewModel6 != null) {
                                                watchAppEditViewModel6.F(t2.b());
                                            } else {
                                                Wg6.n("mWatchAppEditViewModel");
                                                throw null;
                                            }
                                        }
                                    }
                                    String watchappId = o2.getWatchappId();
                                    if (watchappId.hashCode() == -829740640 && watchappId.equals("commute-time") && !this.r.t()) {
                                        this.r.s1(true);
                                        this.p.i0("commute-time");
                                        return;
                                    }
                                    return;
                                }
                                Wg6.n("mWatchAppEditViewModel");
                                throw null;
                            }
                            Wg6.i();
                            throw null;
                        }
                        Wg6.n("mWatchAppEditViewModel");
                        throw null;
                    }
                    return;
                }
                Wg6.n("mWatchAppEditViewModel");
                throw null;
            }
            return;
        }
        Wg6.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.V96
    public void s(WatchAppEditViewModel watchAppEditViewModel) {
        Wg6.c(watchAppEditViewModel, "viewModel");
        this.e = watchAppEditViewModel;
    }

    @DexIgnore
    @Override // com.fossil.V96
    public void t(String str, Parcelable parcelable) {
        Wg6.c(str, "watchAppId");
        Wg6.c(parcelable, MicroAppSetting.SETTING);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsPresenter", "updateSettingForWatchApp watchAppId " + str + " setting " + parcelable);
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            watchAppEditViewModel.H(str, parcelable);
            G(str);
            return;
        }
        Wg6.n("mWatchAppEditViewModel");
        throw null;
    }
}
