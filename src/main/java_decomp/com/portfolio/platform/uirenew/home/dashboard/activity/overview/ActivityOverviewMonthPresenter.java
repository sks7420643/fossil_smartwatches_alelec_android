package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.H47;
import com.fossil.Hs0;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Mf6;
import com.fossil.Nf6;
import com.fossil.Or0;
import com.fossil.Ss0;
import com.fossil.Xh5;
import com.fossil.Yn7;
import com.mapped.ActivityOverviewMonthFragment;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityOverviewMonthPresenter extends Mf6 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f; // = new MutableLiveData<>();
    @DexIgnore
    public Date g;
    @DexIgnore
    public Date h;
    @DexIgnore
    public List<ActivitySummary> i; // = new ArrayList();
    @DexIgnore
    public /* final */ LiveData<H47<List<ActivitySummary>>> j;
    @DexIgnore
    public TreeMap<Long, Float> k;
    @DexIgnore
    public /* final */ Nf6 l;
    @DexIgnore
    public /* final */ UserRepository m;
    @DexIgnore
    public /* final */ SummariesRepository n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter$loadData$1", f = "ActivityOverviewMonthPresenter.kt", l = {103}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewMonthPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter$loadData$1$currentUser$1", f = "ActivityOverviewMonthPresenter.kt", l = {103}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.m;
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(ActivityOverviewMonthPresenter activityOverviewMonthPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = activityOverviewMonthPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(i2, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) g;
            if (mFUser != null) {
                this.this$0.h = TimeUtils.q0(mFUser.getCreatedAt());
                Nf6 nf6 = this.this$0.l;
                Date date = this.this$0.g;
                if (date != null) {
                    Date date2 = this.this$0.h;
                    if (date2 == null) {
                        date2 = new Date();
                    }
                    nf6.g(date, date2);
                    MutableLiveData mutableLiveData = this.this$0.f;
                    Date date3 = this.this$0.g;
                    if (date3 != null) {
                        mutableLiveData.l(date3);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewMonthPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter$mActivitySummaries$1$1", f = "ActivityOverviewMonthPresenter.kt", l = {50, 50}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<ActivitySummary>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Date date, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$it, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<ActivitySummary>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x004e  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r14) {
                /*
                // Method dump skipped, instructions count: 289
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter.Bi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Bi(ActivityOverviewMonthPresenter activityOverviewMonthPresenter) {
            this.a = activityOverviewMonthPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<ActivitySummary>>> a(Date date) {
            return Or0.c(null, 0, new Aii(this, date, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<H47<? extends List<ActivitySummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewMonthPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter$start$1$1", f = "ActivityOverviewMonthPresenter.kt", l = {75}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter$start$1$1$1", f = "ActivityOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super TreeMap<Long, Float>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super TreeMap<Long, Float>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        ActivityOverviewMonthPresenter activityOverviewMonthPresenter = this.this$0.this$0.a;
                        Object e = activityOverviewMonthPresenter.f.e();
                        if (e != null) {
                            Wg6.b(e, "mDateLiveData.value!!");
                            return activityOverviewMonthPresenter.G((Date) e, this.this$0.$data);
                        }
                        Wg6.i();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$data = list;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$data, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                ActivityOverviewMonthPresenter activityOverviewMonthPresenter;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    this.this$0.a.i = this.$data;
                    ActivityOverviewMonthPresenter activityOverviewMonthPresenter2 = this.this$0.a;
                    Dv7 h = activityOverviewMonthPresenter2.h();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.L$1 = activityOverviewMonthPresenter2;
                    this.label = 1;
                    g = Eu7.g(h, aiii, this);
                    if (g == d) {
                        return d;
                    }
                    activityOverviewMonthPresenter = activityOverviewMonthPresenter2;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    g = obj;
                    activityOverviewMonthPresenter = (ActivityOverviewMonthPresenter) this.L$1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                activityOverviewMonthPresenter.k = (TreeMap) g;
                Nf6 nf6 = this.this$0.a.l;
                TreeMap<Long, Float> treeMap = this.this$0.a.k;
                if (treeMap == null) {
                    treeMap = new TreeMap<>();
                }
                nf6.e(treeMap);
                return Cd6.a;
            }
        }

        @DexIgnore
        public Ci(ActivityOverviewMonthPresenter activityOverviewMonthPresenter) {
            this.a = activityOverviewMonthPresenter;
        }

        @DexIgnore
        public final void a(H47<? extends List<ActivitySummary>> h47) {
            List list;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("mDateTransformations - status=");
            sb.append(h47 != null ? h47.d() : null);
            sb.append(" -- data.size=");
            sb.append((h47 == null || (list = (List) h47.c()) == null) ? null : Integer.valueOf(list.size()));
            local.d("ActivityOverviewMonthPresenter", sb.toString());
            if ((h47 != null ? h47.d() : null) != Xh5.DATABASE_LOADING) {
                List list2 = h47 != null ? (List) h47.c() : null;
                if (list2 != null && (!Wg6.a(this.a.i, list2))) {
                    Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, list2, null), 3, null);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<ActivitySummary>> h47) {
            a(h47);
        }
    }

    @DexIgnore
    public ActivityOverviewMonthPresenter(Nf6 nf6, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        Wg6.c(nf6, "mView");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(summariesRepository, "mSummariesRepository");
        Wg6.c(portfolioApp, "mApp");
        this.l = nf6;
        this.m = userRepository;
        this.n = summariesRepository;
        this.e = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.J());
        LiveData<H47<List<ActivitySummary>>> c = Ss0.c(this.f, new Bi(this));
        Wg6.b(c, "Transformations.switchMa\u2026        }\n        }\n    }");
        this.j = c;
    }

    @DexIgnore
    public void E() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthPresenter", "loadData");
        Date date = this.g;
        if (date == null || !TimeUtils.p0(date).booleanValue()) {
            this.g = new Date();
            Rm6 unused = Gu7.d(k(), null, null, new Ai(this, null), 3, null);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityOverviewMonthPresenter", "loadData - mDateLiveData=" + this.g);
    }

    @DexIgnore
    public void F() {
        this.l.M5(this);
    }

    @DexIgnore
    public final TreeMap<Long, Float> G(Date date, List<ActivitySummary> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("ActivityOverviewMonthPresenter", sb.toString());
        TreeMap<Long, Float> treeMap = new TreeMap<>();
        Calendar instance = Calendar.getInstance();
        if (list != null) {
            for (ActivitySummary activitySummary : list) {
                instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay(), 0, 0, 0);
                instance.set(14, 0);
                if (activitySummary.getStepGoal() > 0) {
                    Wg6.b(instance, "calendar");
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf(((float) activitySummary.getSteps()) / ((float) activitySummary.getStepGoal())));
                } else {
                    Wg6.b(instance, "calendar");
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                }
            }
        }
        return treeMap;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        E();
        LiveData<H47<List<ActivitySummary>>> liveData = this.j;
        Nf6 nf6 = this.l;
        if (nf6 != null) {
            liveData.h((ActivityOverviewMonthFragment) nf6, new Ci(this));
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthFragment");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthPresenter", "stop");
        try {
            LiveData<H47<List<ActivitySummary>>> liveData = this.j;
            Nf6 nf6 = this.l;
            if (nf6 != null) {
                liveData.n((ActivityOverviewMonthFragment) nf6);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActivityOverviewMonthPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Mf6
    public FossilDeviceSerialPatternUtil.DEVICE n() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        Wg6.b(device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    @Override // com.fossil.Mf6
    public void o(Date date) {
        Wg6.c(date, "date");
        if (this.f.e() == null || !TimeUtils.m0(this.f.e(), date)) {
            this.f.l(date);
        }
    }
}
